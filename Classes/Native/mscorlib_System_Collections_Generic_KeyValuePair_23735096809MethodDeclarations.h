﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<CEvent.EventFunc`1<System.Object>>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m755354651(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3735096809 *, String_t*, List_1_t3015897733 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<CEvent.EventFunc`1<System.Object>>>::get_Key()
#define KeyValuePair_2_get_Key_m308933997(__this, method) ((  String_t* (*) (KeyValuePair_2_t3735096809 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<CEvent.EventFunc`1<System.Object>>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1968908590(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3735096809 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<CEvent.EventFunc`1<System.Object>>>::get_Value()
#define KeyValuePair_2_get_Value_m2383943277(__this, method) ((  List_1_t3015897733 * (*) (KeyValuePair_2_t3735096809 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<CEvent.EventFunc`1<System.Object>>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2290234926(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3735096809 *, List_1_t3015897733 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<CEvent.EventFunc`1<System.Object>>>::ToString()
#define KeyValuePair_2_ToString_m2726638324(__this, method) ((  String_t* (*) (KeyValuePair_2_t3735096809 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
