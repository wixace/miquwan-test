﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraMove
struct  CameraMove_t4276106422  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Transform CameraMove::target
	Transform_t1659122786 * ___target_3;
	// System.Single CameraMove::nearTime
	float ___nearTime_4;
	// System.Single CameraMove::farTime
	float ___farTime_5;
	// System.Single CameraMove::mNearTime
	float ___mNearTime_6;
	// System.Single CameraMove::mFarTime
	float ___mFarTime_7;
	// System.Boolean CameraMove::bZoomIn
	bool ___bZoomIn_8;
	// System.Single CameraMove::curtime
	float ___curtime_9;
	// UnityEngine.Vector3 CameraMove::speed_near
	Vector3_t4282066566  ___speed_near_10;
	// UnityEngine.Vector3 CameraMove::speed_far
	Vector3_t4282066566  ___speed_far_11;
	// UnityEngine.Vector3 CameraMove::cameraTarget
	Vector3_t4282066566  ___cameraTarget_12;
	// UnityEngine.Vector3 CameraMove::initOffset
	Vector3_t4282066566  ___initOffset_13;

public:
	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(CameraMove_t4276106422, ___target_3)); }
	inline Transform_t1659122786 * get_target_3() const { return ___target_3; }
	inline Transform_t1659122786 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(Transform_t1659122786 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier(&___target_3, value);
	}

	inline static int32_t get_offset_of_nearTime_4() { return static_cast<int32_t>(offsetof(CameraMove_t4276106422, ___nearTime_4)); }
	inline float get_nearTime_4() const { return ___nearTime_4; }
	inline float* get_address_of_nearTime_4() { return &___nearTime_4; }
	inline void set_nearTime_4(float value)
	{
		___nearTime_4 = value;
	}

	inline static int32_t get_offset_of_farTime_5() { return static_cast<int32_t>(offsetof(CameraMove_t4276106422, ___farTime_5)); }
	inline float get_farTime_5() const { return ___farTime_5; }
	inline float* get_address_of_farTime_5() { return &___farTime_5; }
	inline void set_farTime_5(float value)
	{
		___farTime_5 = value;
	}

	inline static int32_t get_offset_of_mNearTime_6() { return static_cast<int32_t>(offsetof(CameraMove_t4276106422, ___mNearTime_6)); }
	inline float get_mNearTime_6() const { return ___mNearTime_6; }
	inline float* get_address_of_mNearTime_6() { return &___mNearTime_6; }
	inline void set_mNearTime_6(float value)
	{
		___mNearTime_6 = value;
	}

	inline static int32_t get_offset_of_mFarTime_7() { return static_cast<int32_t>(offsetof(CameraMove_t4276106422, ___mFarTime_7)); }
	inline float get_mFarTime_7() const { return ___mFarTime_7; }
	inline float* get_address_of_mFarTime_7() { return &___mFarTime_7; }
	inline void set_mFarTime_7(float value)
	{
		___mFarTime_7 = value;
	}

	inline static int32_t get_offset_of_bZoomIn_8() { return static_cast<int32_t>(offsetof(CameraMove_t4276106422, ___bZoomIn_8)); }
	inline bool get_bZoomIn_8() const { return ___bZoomIn_8; }
	inline bool* get_address_of_bZoomIn_8() { return &___bZoomIn_8; }
	inline void set_bZoomIn_8(bool value)
	{
		___bZoomIn_8 = value;
	}

	inline static int32_t get_offset_of_curtime_9() { return static_cast<int32_t>(offsetof(CameraMove_t4276106422, ___curtime_9)); }
	inline float get_curtime_9() const { return ___curtime_9; }
	inline float* get_address_of_curtime_9() { return &___curtime_9; }
	inline void set_curtime_9(float value)
	{
		___curtime_9 = value;
	}

	inline static int32_t get_offset_of_speed_near_10() { return static_cast<int32_t>(offsetof(CameraMove_t4276106422, ___speed_near_10)); }
	inline Vector3_t4282066566  get_speed_near_10() const { return ___speed_near_10; }
	inline Vector3_t4282066566 * get_address_of_speed_near_10() { return &___speed_near_10; }
	inline void set_speed_near_10(Vector3_t4282066566  value)
	{
		___speed_near_10 = value;
	}

	inline static int32_t get_offset_of_speed_far_11() { return static_cast<int32_t>(offsetof(CameraMove_t4276106422, ___speed_far_11)); }
	inline Vector3_t4282066566  get_speed_far_11() const { return ___speed_far_11; }
	inline Vector3_t4282066566 * get_address_of_speed_far_11() { return &___speed_far_11; }
	inline void set_speed_far_11(Vector3_t4282066566  value)
	{
		___speed_far_11 = value;
	}

	inline static int32_t get_offset_of_cameraTarget_12() { return static_cast<int32_t>(offsetof(CameraMove_t4276106422, ___cameraTarget_12)); }
	inline Vector3_t4282066566  get_cameraTarget_12() const { return ___cameraTarget_12; }
	inline Vector3_t4282066566 * get_address_of_cameraTarget_12() { return &___cameraTarget_12; }
	inline void set_cameraTarget_12(Vector3_t4282066566  value)
	{
		___cameraTarget_12 = value;
	}

	inline static int32_t get_offset_of_initOffset_13() { return static_cast<int32_t>(offsetof(CameraMove_t4276106422, ___initOffset_13)); }
	inline Vector3_t4282066566  get_initOffset_13() const { return ___initOffset_13; }
	inline Vector3_t4282066566 * get_address_of_initOffset_13() { return &___initOffset_13; }
	inline void set_initOffset_13(Vector3_t4282066566  value)
	{
		___initOffset_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
