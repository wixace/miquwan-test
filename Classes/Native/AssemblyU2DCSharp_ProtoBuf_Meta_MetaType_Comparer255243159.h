﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ProtoBuf.Meta.MetaType/Comparer
struct Comparer_t255243159;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProtoBuf.Meta.MetaType/Comparer
struct  Comparer_t255243159  : public Il2CppObject
{
public:

public:
};

struct Comparer_t255243159_StaticFields
{
public:
	// ProtoBuf.Meta.MetaType/Comparer ProtoBuf.Meta.MetaType/Comparer::Default
	Comparer_t255243159 * ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(Comparer_t255243159_StaticFields, ___Default_0)); }
	inline Comparer_t255243159 * get_Default_0() const { return ___Default_0; }
	inline Comparer_t255243159 ** get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(Comparer_t255243159 * value)
	{
		___Default_0 = value;
		Il2CppCodeGenWriteBarrier(&___Default_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
