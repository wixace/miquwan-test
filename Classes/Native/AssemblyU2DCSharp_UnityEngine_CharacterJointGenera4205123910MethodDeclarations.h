﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_CharacterJointGenerated
struct UnityEngine_CharacterJointGenerated_t4205123910;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_CharacterJointGenerated::.ctor()
extern "C"  void UnityEngine_CharacterJointGenerated__ctor_m2933340389 (UnityEngine_CharacterJointGenerated_t4205123910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CharacterJointGenerated::CharacterJoint_CharacterJoint1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CharacterJointGenerated_CharacterJoint_CharacterJoint1_m1956005261 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterJointGenerated::CharacterJoint_swingAxis(JSVCall)
extern "C"  void UnityEngine_CharacterJointGenerated_CharacterJoint_swingAxis_m1859503431 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterJointGenerated::CharacterJoint_twistLimitSpring(JSVCall)
extern "C"  void UnityEngine_CharacterJointGenerated_CharacterJoint_twistLimitSpring_m1406634501 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterJointGenerated::CharacterJoint_swingLimitSpring(JSVCall)
extern "C"  void UnityEngine_CharacterJointGenerated_CharacterJoint_swingLimitSpring_m2336628828 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterJointGenerated::CharacterJoint_lowTwistLimit(JSVCall)
extern "C"  void UnityEngine_CharacterJointGenerated_CharacterJoint_lowTwistLimit_m1054277598 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterJointGenerated::CharacterJoint_highTwistLimit(JSVCall)
extern "C"  void UnityEngine_CharacterJointGenerated_CharacterJoint_highTwistLimit_m1436127632 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterJointGenerated::CharacterJoint_swing1Limit(JSVCall)
extern "C"  void UnityEngine_CharacterJointGenerated_CharacterJoint_swing1Limit_m3353624734 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterJointGenerated::CharacterJoint_swing2Limit(JSVCall)
extern "C"  void UnityEngine_CharacterJointGenerated_CharacterJoint_swing2Limit_m566209247 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterJointGenerated::CharacterJoint_enableProjection(JSVCall)
extern "C"  void UnityEngine_CharacterJointGenerated_CharacterJoint_enableProjection_m2166526196 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterJointGenerated::CharacterJoint_projectionDistance(JSVCall)
extern "C"  void UnityEngine_CharacterJointGenerated_CharacterJoint_projectionDistance_m3033414146 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterJointGenerated::CharacterJoint_projectionAngle(JSVCall)
extern "C"  void UnityEngine_CharacterJointGenerated_CharacterJoint_projectionAngle_m2499712738 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterJointGenerated::__Register()
extern "C"  void UnityEngine_CharacterJointGenerated___Register_m2069039874 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_CharacterJointGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_CharacterJointGenerated_ilo_getObject1_m2851319153 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterJointGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_CharacterJointGenerated_ilo_addJSCSRel2_m295364002 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_CharacterJointGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_CharacterJointGenerated_ilo_setObject3_m2319159659 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_CharacterJointGenerated::ilo_getSingle4(System.Int32)
extern "C"  float UnityEngine_CharacterJointGenerated_ilo_getSingle4_m189990157 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
