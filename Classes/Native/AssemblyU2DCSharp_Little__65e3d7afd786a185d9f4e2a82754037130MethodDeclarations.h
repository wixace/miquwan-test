﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._65e3d7afd786a185d9f4e2a8e4ce5b8a
struct _65e3d7afd786a185d9f4e2a8e4ce5b8a_t2754037130;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._65e3d7afd786a185d9f4e2a8e4ce5b8a::.ctor()
extern "C"  void _65e3d7afd786a185d9f4e2a8e4ce5b8a__ctor_m1318091747 (_65e3d7afd786a185d9f4e2a8e4ce5b8a_t2754037130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._65e3d7afd786a185d9f4e2a8e4ce5b8a::_65e3d7afd786a185d9f4e2a8e4ce5b8am2(System.Int32)
extern "C"  int32_t _65e3d7afd786a185d9f4e2a8e4ce5b8a__65e3d7afd786a185d9f4e2a8e4ce5b8am2_m3689272665 (_65e3d7afd786a185d9f4e2a8e4ce5b8a_t2754037130 * __this, int32_t ____65e3d7afd786a185d9f4e2a8e4ce5b8aa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._65e3d7afd786a185d9f4e2a8e4ce5b8a::_65e3d7afd786a185d9f4e2a8e4ce5b8am(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _65e3d7afd786a185d9f4e2a8e4ce5b8a__65e3d7afd786a185d9f4e2a8e4ce5b8am_m2590935421 (_65e3d7afd786a185d9f4e2a8e4ce5b8a_t2754037130 * __this, int32_t ____65e3d7afd786a185d9f4e2a8e4ce5b8aa0, int32_t ____65e3d7afd786a185d9f4e2a8e4ce5b8a411, int32_t ____65e3d7afd786a185d9f4e2a8e4ce5b8ac2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
