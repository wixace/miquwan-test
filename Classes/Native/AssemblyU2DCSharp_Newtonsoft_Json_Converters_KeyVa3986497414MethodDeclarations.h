﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Converters.KeyValuePairConverter
struct KeyValuePairConverter_t3986497414;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// System.Type
struct Type_t;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer251850770.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader816925123.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"
#include "mscorlib_System_String7231557.h"

// System.Void Newtonsoft.Json.Converters.KeyValuePairConverter::.ctor()
extern "C"  void KeyValuePairConverter__ctor_m1202590477 (KeyValuePairConverter_t3986497414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.KeyValuePairConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  void KeyValuePairConverter_WriteJson_m3180007975 (KeyValuePairConverter_t3986497414 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, JsonSerializer_t251850770 * ___serializer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Converters.KeyValuePairConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  Il2CppObject * KeyValuePairConverter_ReadJson_m2386734092 (KeyValuePairConverter_t3986497414 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, Il2CppObject * ___existingValue2, JsonSerializer_t251850770 * ___serializer3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.KeyValuePairConverter::CanConvert(System.Type)
extern "C"  bool KeyValuePairConverter_CanConvert_m1493042155 (KeyValuePairConverter_t3986497414 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Converters.KeyValuePairConverter::ilo_GetMemberValue1(System.Reflection.MemberInfo,System.Object)
extern "C"  Il2CppObject * KeyValuePairConverter_ilo_GetMemberValue1_m269946997 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___member0, Il2CppObject * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.KeyValuePairConverter::ilo_WritePropertyName2(Newtonsoft.Json.JsonWriter,System.String)
extern "C"  void KeyValuePairConverter_ilo_WritePropertyName2_m3113610569 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.KeyValuePairConverter::ilo_Serialize3(Newtonsoft.Json.JsonSerializer,Newtonsoft.Json.JsonWriter,System.Object)
extern "C"  void KeyValuePairConverter_ilo_Serialize3_m84069558 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, JsonWriter_t972330355 * ___jsonWriter1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.KeyValuePairConverter::ilo_Read4(Newtonsoft.Json.JsonReader)
extern "C"  bool KeyValuePairConverter_ilo_Read4_m2987255010 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Converters.KeyValuePairConverter::ilo_Deserialize5(Newtonsoft.Json.JsonSerializer,Newtonsoft.Json.JsonReader,System.Type)
extern "C"  Il2CppObject * KeyValuePairConverter_ilo_Deserialize5_m3593790803 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, JsonReader_t816925123 * ___reader1, Type_t * ___objectType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
