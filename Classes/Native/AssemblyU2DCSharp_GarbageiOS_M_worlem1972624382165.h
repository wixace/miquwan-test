﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_worlem197
struct  M_worlem197_t2624382165  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_worlem197::_jastiniReyar
	uint32_t ____jastiniReyar_0;
	// System.UInt32 GarbageiOS.M_worlem197::_salpel
	uint32_t ____salpel_1;
	// System.Single GarbageiOS.M_worlem197::_bairraifi
	float ____bairraifi_2;
	// System.Int32 GarbageiOS.M_worlem197::_sirwhi
	int32_t ____sirwhi_3;
	// System.Int32 GarbageiOS.M_worlem197::_marlir
	int32_t ____marlir_4;
	// System.Single GarbageiOS.M_worlem197::_temqanou
	float ____temqanou_5;
	// System.Single GarbageiOS.M_worlem197::_verci
	float ____verci_6;
	// System.Single GarbageiOS.M_worlem197::_barirxal
	float ____barirxal_7;
	// System.Single GarbageiOS.M_worlem197::_jalwaWassu
	float ____jalwaWassu_8;
	// System.Single GarbageiOS.M_worlem197::_febeaJojaspear
	float ____febeaJojaspear_9;
	// System.Boolean GarbageiOS.M_worlem197::_koumiriFapaizou
	bool ____koumiriFapaizou_10;
	// System.Boolean GarbageiOS.M_worlem197::_seto
	bool ____seto_11;
	// System.Int32 GarbageiOS.M_worlem197::_jellalmooRoonas
	int32_t ____jellalmooRoonas_12;
	// System.Single GarbageiOS.M_worlem197::_pirxapas
	float ____pirxapas_13;
	// System.Int32 GarbageiOS.M_worlem197::_nooperTemsor
	int32_t ____nooperTemsor_14;
	// System.String GarbageiOS.M_worlem197::_hamarooSisre
	String_t* ____hamarooSisre_15;
	// System.Single GarbageiOS.M_worlem197::_stougearLiherkou
	float ____stougearLiherkou_16;

public:
	inline static int32_t get_offset_of__jastiniReyar_0() { return static_cast<int32_t>(offsetof(M_worlem197_t2624382165, ____jastiniReyar_0)); }
	inline uint32_t get__jastiniReyar_0() const { return ____jastiniReyar_0; }
	inline uint32_t* get_address_of__jastiniReyar_0() { return &____jastiniReyar_0; }
	inline void set__jastiniReyar_0(uint32_t value)
	{
		____jastiniReyar_0 = value;
	}

	inline static int32_t get_offset_of__salpel_1() { return static_cast<int32_t>(offsetof(M_worlem197_t2624382165, ____salpel_1)); }
	inline uint32_t get__salpel_1() const { return ____salpel_1; }
	inline uint32_t* get_address_of__salpel_1() { return &____salpel_1; }
	inline void set__salpel_1(uint32_t value)
	{
		____salpel_1 = value;
	}

	inline static int32_t get_offset_of__bairraifi_2() { return static_cast<int32_t>(offsetof(M_worlem197_t2624382165, ____bairraifi_2)); }
	inline float get__bairraifi_2() const { return ____bairraifi_2; }
	inline float* get_address_of__bairraifi_2() { return &____bairraifi_2; }
	inline void set__bairraifi_2(float value)
	{
		____bairraifi_2 = value;
	}

	inline static int32_t get_offset_of__sirwhi_3() { return static_cast<int32_t>(offsetof(M_worlem197_t2624382165, ____sirwhi_3)); }
	inline int32_t get__sirwhi_3() const { return ____sirwhi_3; }
	inline int32_t* get_address_of__sirwhi_3() { return &____sirwhi_3; }
	inline void set__sirwhi_3(int32_t value)
	{
		____sirwhi_3 = value;
	}

	inline static int32_t get_offset_of__marlir_4() { return static_cast<int32_t>(offsetof(M_worlem197_t2624382165, ____marlir_4)); }
	inline int32_t get__marlir_4() const { return ____marlir_4; }
	inline int32_t* get_address_of__marlir_4() { return &____marlir_4; }
	inline void set__marlir_4(int32_t value)
	{
		____marlir_4 = value;
	}

	inline static int32_t get_offset_of__temqanou_5() { return static_cast<int32_t>(offsetof(M_worlem197_t2624382165, ____temqanou_5)); }
	inline float get__temqanou_5() const { return ____temqanou_5; }
	inline float* get_address_of__temqanou_5() { return &____temqanou_5; }
	inline void set__temqanou_5(float value)
	{
		____temqanou_5 = value;
	}

	inline static int32_t get_offset_of__verci_6() { return static_cast<int32_t>(offsetof(M_worlem197_t2624382165, ____verci_6)); }
	inline float get__verci_6() const { return ____verci_6; }
	inline float* get_address_of__verci_6() { return &____verci_6; }
	inline void set__verci_6(float value)
	{
		____verci_6 = value;
	}

	inline static int32_t get_offset_of__barirxal_7() { return static_cast<int32_t>(offsetof(M_worlem197_t2624382165, ____barirxal_7)); }
	inline float get__barirxal_7() const { return ____barirxal_7; }
	inline float* get_address_of__barirxal_7() { return &____barirxal_7; }
	inline void set__barirxal_7(float value)
	{
		____barirxal_7 = value;
	}

	inline static int32_t get_offset_of__jalwaWassu_8() { return static_cast<int32_t>(offsetof(M_worlem197_t2624382165, ____jalwaWassu_8)); }
	inline float get__jalwaWassu_8() const { return ____jalwaWassu_8; }
	inline float* get_address_of__jalwaWassu_8() { return &____jalwaWassu_8; }
	inline void set__jalwaWassu_8(float value)
	{
		____jalwaWassu_8 = value;
	}

	inline static int32_t get_offset_of__febeaJojaspear_9() { return static_cast<int32_t>(offsetof(M_worlem197_t2624382165, ____febeaJojaspear_9)); }
	inline float get__febeaJojaspear_9() const { return ____febeaJojaspear_9; }
	inline float* get_address_of__febeaJojaspear_9() { return &____febeaJojaspear_9; }
	inline void set__febeaJojaspear_9(float value)
	{
		____febeaJojaspear_9 = value;
	}

	inline static int32_t get_offset_of__koumiriFapaizou_10() { return static_cast<int32_t>(offsetof(M_worlem197_t2624382165, ____koumiriFapaizou_10)); }
	inline bool get__koumiriFapaizou_10() const { return ____koumiriFapaizou_10; }
	inline bool* get_address_of__koumiriFapaizou_10() { return &____koumiriFapaizou_10; }
	inline void set__koumiriFapaizou_10(bool value)
	{
		____koumiriFapaizou_10 = value;
	}

	inline static int32_t get_offset_of__seto_11() { return static_cast<int32_t>(offsetof(M_worlem197_t2624382165, ____seto_11)); }
	inline bool get__seto_11() const { return ____seto_11; }
	inline bool* get_address_of__seto_11() { return &____seto_11; }
	inline void set__seto_11(bool value)
	{
		____seto_11 = value;
	}

	inline static int32_t get_offset_of__jellalmooRoonas_12() { return static_cast<int32_t>(offsetof(M_worlem197_t2624382165, ____jellalmooRoonas_12)); }
	inline int32_t get__jellalmooRoonas_12() const { return ____jellalmooRoonas_12; }
	inline int32_t* get_address_of__jellalmooRoonas_12() { return &____jellalmooRoonas_12; }
	inline void set__jellalmooRoonas_12(int32_t value)
	{
		____jellalmooRoonas_12 = value;
	}

	inline static int32_t get_offset_of__pirxapas_13() { return static_cast<int32_t>(offsetof(M_worlem197_t2624382165, ____pirxapas_13)); }
	inline float get__pirxapas_13() const { return ____pirxapas_13; }
	inline float* get_address_of__pirxapas_13() { return &____pirxapas_13; }
	inline void set__pirxapas_13(float value)
	{
		____pirxapas_13 = value;
	}

	inline static int32_t get_offset_of__nooperTemsor_14() { return static_cast<int32_t>(offsetof(M_worlem197_t2624382165, ____nooperTemsor_14)); }
	inline int32_t get__nooperTemsor_14() const { return ____nooperTemsor_14; }
	inline int32_t* get_address_of__nooperTemsor_14() { return &____nooperTemsor_14; }
	inline void set__nooperTemsor_14(int32_t value)
	{
		____nooperTemsor_14 = value;
	}

	inline static int32_t get_offset_of__hamarooSisre_15() { return static_cast<int32_t>(offsetof(M_worlem197_t2624382165, ____hamarooSisre_15)); }
	inline String_t* get__hamarooSisre_15() const { return ____hamarooSisre_15; }
	inline String_t** get_address_of__hamarooSisre_15() { return &____hamarooSisre_15; }
	inline void set__hamarooSisre_15(String_t* value)
	{
		____hamarooSisre_15 = value;
		Il2CppCodeGenWriteBarrier(&____hamarooSisre_15, value);
	}

	inline static int32_t get_offset_of__stougearLiherkou_16() { return static_cast<int32_t>(offsetof(M_worlem197_t2624382165, ____stougearLiherkou_16)); }
	inline float get__stougearLiherkou_16() const { return ____stougearLiherkou_16; }
	inline float* get_address_of__stougearLiherkou_16() { return &____stougearLiherkou_16; }
	inline void set__stougearLiherkou_16(float value)
	{
		____stougearLiherkou_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
