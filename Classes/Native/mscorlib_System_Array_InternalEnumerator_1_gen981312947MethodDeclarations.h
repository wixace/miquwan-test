﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen981312947.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_Rendering_RenderBufferStor2198970271.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rendering.RenderBufferStoreAction>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3054298317_gshared (InternalEnumerator_1_t981312947 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3054298317(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t981312947 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3054298317_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rendering.RenderBufferStoreAction>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1648851891_gshared (InternalEnumerator_1_t981312947 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1648851891(__this, method) ((  void (*) (InternalEnumerator_1_t981312947 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1648851891_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Rendering.RenderBufferStoreAction>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3980050729_gshared (InternalEnumerator_1_t981312947 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3980050729(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t981312947 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3980050729_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rendering.RenderBufferStoreAction>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m699308772_gshared (InternalEnumerator_1_t981312947 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m699308772(__this, method) ((  void (*) (InternalEnumerator_1_t981312947 *, const MethodInfo*))InternalEnumerator_1_Dispose_m699308772_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Rendering.RenderBufferStoreAction>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3017175907_gshared (InternalEnumerator_1_t981312947 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3017175907(__this, method) ((  bool (*) (InternalEnumerator_1_t981312947 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3017175907_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Rendering.RenderBufferStoreAction>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3208951158_gshared (InternalEnumerator_1_t981312947 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3208951158(__this, method) ((  int32_t (*) (InternalEnumerator_1_t981312947 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3208951158_gshared)(__this, method)
