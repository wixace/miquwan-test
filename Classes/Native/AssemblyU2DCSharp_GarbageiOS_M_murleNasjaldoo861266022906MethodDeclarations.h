﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_murleNasjaldoo86
struct M_murleNasjaldoo86_t1266022906;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_murleNasjaldoo861266022906.h"

// System.Void GarbageiOS.M_murleNasjaldoo86::.ctor()
extern "C"  void M_murleNasjaldoo86__ctor_m846142841 (M_murleNasjaldoo86_t1266022906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_murleNasjaldoo86::M_reedaipar0(System.String[],System.Int32)
extern "C"  void M_murleNasjaldoo86_M_reedaipar0_m3903274397 (M_murleNasjaldoo86_t1266022906 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_murleNasjaldoo86::M_ketearMokearser1(System.String[],System.Int32)
extern "C"  void M_murleNasjaldoo86_M_ketearMokearser1_m3447415118 (M_murleNasjaldoo86_t1266022906 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_murleNasjaldoo86::M_hidear2(System.String[],System.Int32)
extern "C"  void M_murleNasjaldoo86_M_hidear2_m1612391771 (M_murleNasjaldoo86_t1266022906 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_murleNasjaldoo86::M_jamalMisruke3(System.String[],System.Int32)
extern "C"  void M_murleNasjaldoo86_M_jamalMisruke3_m3219473884 (M_murleNasjaldoo86_t1266022906 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_murleNasjaldoo86::ilo_M_ketearMokearser11(GarbageiOS.M_murleNasjaldoo86,System.String[],System.Int32)
extern "C"  void M_murleNasjaldoo86_ilo_M_ketearMokearser11_m4014816902 (Il2CppObject * __this /* static, unused */, M_murleNasjaldoo86_t1266022906 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
