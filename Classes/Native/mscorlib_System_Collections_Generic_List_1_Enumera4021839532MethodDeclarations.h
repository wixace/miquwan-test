﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>
struct List_1_t4002166762;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4021839532.h"
#include "AssemblyU2DCSharp_Mihua_Assets_SubAssetMgr_ErrorIn2633981210.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2381542061_gshared (Enumerator_t4021839532 * __this, List_1_t4002166762 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m2381542061(__this, ___l0, method) ((  void (*) (Enumerator_t4021839532 *, List_1_t4002166762 *, const MethodInfo*))Enumerator__ctor_m2381542061_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2235558277_gshared (Enumerator_t4021839532 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2235558277(__this, method) ((  void (*) (Enumerator_t4021839532 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2235558277_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1649019259_gshared (Enumerator_t4021839532 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1649019259(__this, method) ((  Il2CppObject * (*) (Enumerator_t4021839532 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1649019259_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Mihua.Assets.SubAssetMgr/ErrorInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m194100178_gshared (Enumerator_t4021839532 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m194100178(__this, method) ((  void (*) (Enumerator_t4021839532 *, const MethodInfo*))Enumerator_Dispose_m194100178_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Mihua.Assets.SubAssetMgr/ErrorInfo>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2600419851_gshared (Enumerator_t4021839532 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2600419851(__this, method) ((  void (*) (Enumerator_t4021839532 *, const MethodInfo*))Enumerator_VerifyState_m2600419851_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Mihua.Assets.SubAssetMgr/ErrorInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m240611381_gshared (Enumerator_t4021839532 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m240611381(__this, method) ((  bool (*) (Enumerator_t4021839532 *, const MethodInfo*))Enumerator_MoveNext_m240611381_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Current()
extern "C"  ErrorInfo_t2633981210  Enumerator_get_Current_m3350310756_gshared (Enumerator_t4021839532 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3350310756(__this, method) ((  ErrorInfo_t2633981210  (*) (Enumerator_t4021839532 *, const MethodInfo*))Enumerator_get_Current_m3350310756_gshared)(__this, method)
