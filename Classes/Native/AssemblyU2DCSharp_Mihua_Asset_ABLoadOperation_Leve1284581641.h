﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AsyncOperation
struct AsyncOperation_t3699081103;

#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_ABOp1894014760.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mihua.Asset.ABLoadOperation.LevelOperation
struct  LevelOperation_t1284581641  : public ABOperation_t1894014760
{
public:
	// UnityEngine.AsyncOperation Mihua.Asset.ABLoadOperation.LevelOperation::m_Request
	AsyncOperation_t3699081103 * ___m_Request_0;

public:
	inline static int32_t get_offset_of_m_Request_0() { return static_cast<int32_t>(offsetof(LevelOperation_t1284581641, ___m_Request_0)); }
	inline AsyncOperation_t3699081103 * get_m_Request_0() const { return ___m_Request_0; }
	inline AsyncOperation_t3699081103 ** get_address_of_m_Request_0() { return &___m_Request_0; }
	inline void set_m_Request_0(AsyncOperation_t3699081103 * value)
	{
		___m_Request_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Request_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
