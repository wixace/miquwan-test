﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Core_USingleton_1_gen2177109755MethodDeclarations.h"

// System.Void Core.USingleton`1<Core.GameManager>::.ctor()
#define USingleton_1__ctor_m3531137439(__this, method) ((  void (*) (USingleton_1_t1690052985 *, const MethodInfo*))USingleton_1__ctor_m3702815765_gshared)(__this, method)
// System.Void Core.USingleton`1<Core.GameManager>::.cctor()
#define USingleton_1__cctor_m1608981998(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))USingleton_1__cctor_m2636042808_gshared)(__this /* static, unused */, method)
// T Core.USingleton`1<Core.GameManager>::get_Instance()
#define USingleton_1_get_Instance_m3817925060(__this /* static, unused */, method) ((  GameManager_t3683759601 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))USingleton_1_get_Instance_m899663340_gshared)(__this /* static, unused */, method)
// System.Void Core.USingleton`1<Core.GameManager>::set_Instance(T)
#define USingleton_1_set_Instance_m1160090919(__this /* static, unused */, ___value0, method) ((  void (*) (Il2CppObject * /* static, unused */, GameManager_t3683759601 *, const MethodInfo*))USingleton_1_set_Instance_m1722964381_gshared)(__this /* static, unused */, ___value0, method)
