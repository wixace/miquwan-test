﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1361267471.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Encode2578924795.h"

// System.Void System.Array/InternalEnumerator`1<SevenZip.Compression.LZMA.Encoder/LiteralEncoder/Encoder2>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4215372608_gshared (InternalEnumerator_1_t1361267471 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m4215372608(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1361267471 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4215372608_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<SevenZip.Compression.LZMA.Encoder/LiteralEncoder/Encoder2>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1776448096_gshared (InternalEnumerator_1_t1361267471 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1776448096(__this, method) ((  void (*) (InternalEnumerator_1_t1361267471 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1776448096_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<SevenZip.Compression.LZMA.Encoder/LiteralEncoder/Encoder2>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1398561430_gshared (InternalEnumerator_1_t1361267471 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1398561430(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1361267471 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1398561430_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<SevenZip.Compression.LZMA.Encoder/LiteralEncoder/Encoder2>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1717835031_gshared (InternalEnumerator_1_t1361267471 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1717835031(__this, method) ((  void (*) (InternalEnumerator_1_t1361267471 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1717835031_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<SevenZip.Compression.LZMA.Encoder/LiteralEncoder/Encoder2>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4054994064_gshared (InternalEnumerator_1_t1361267471 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m4054994064(__this, method) ((  bool (*) (InternalEnumerator_1_t1361267471 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4054994064_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<SevenZip.Compression.LZMA.Encoder/LiteralEncoder/Encoder2>::get_Current()
extern "C"  Encoder2_t2578924795  InternalEnumerator_1_get_Current_m615896233_gshared (InternalEnumerator_1_t1361267471 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m615896233(__this, method) ((  Encoder2_t2578924795  (*) (InternalEnumerator_1_t1361267471 *, const MethodInfo*))InternalEnumerator_1_get_Current_m615896233_gshared)(__this, method)
