﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonLinqContract
struct JsonLinqContract_t1458265766;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"

// System.Void Newtonsoft.Json.Serialization.JsonLinqContract::.ctor(System.Type)
extern "C"  void JsonLinqContract__ctor_m3626991339 (JsonLinqContract_t1458265766 * __this, Type_t * ___underlyingType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
