﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_sedre190
struct  M_sedre190_t3586298931  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_sedre190::_jallyiri
	float ____jallyiri_0;
	// System.Single GarbageiOS.M_sedre190::_whefarQougas
	float ____whefarQougas_1;
	// System.Single GarbageiOS.M_sedre190::_whode
	float ____whode_2;
	// System.String GarbageiOS.M_sedre190::_semre
	String_t* ____semre_3;
	// System.UInt32 GarbageiOS.M_sedre190::_risti
	uint32_t ____risti_4;
	// System.Single GarbageiOS.M_sedre190::_wamuSidemda
	float ____wamuSidemda_5;
	// System.String GarbageiOS.M_sedre190::_kergislall
	String_t* ____kergislall_6;
	// System.Boolean GarbageiOS.M_sedre190::_rerefas
	bool ____rerefas_7;
	// System.Single GarbageiOS.M_sedre190::_curoo
	float ____curoo_8;

public:
	inline static int32_t get_offset_of__jallyiri_0() { return static_cast<int32_t>(offsetof(M_sedre190_t3586298931, ____jallyiri_0)); }
	inline float get__jallyiri_0() const { return ____jallyiri_0; }
	inline float* get_address_of__jallyiri_0() { return &____jallyiri_0; }
	inline void set__jallyiri_0(float value)
	{
		____jallyiri_0 = value;
	}

	inline static int32_t get_offset_of__whefarQougas_1() { return static_cast<int32_t>(offsetof(M_sedre190_t3586298931, ____whefarQougas_1)); }
	inline float get__whefarQougas_1() const { return ____whefarQougas_1; }
	inline float* get_address_of__whefarQougas_1() { return &____whefarQougas_1; }
	inline void set__whefarQougas_1(float value)
	{
		____whefarQougas_1 = value;
	}

	inline static int32_t get_offset_of__whode_2() { return static_cast<int32_t>(offsetof(M_sedre190_t3586298931, ____whode_2)); }
	inline float get__whode_2() const { return ____whode_2; }
	inline float* get_address_of__whode_2() { return &____whode_2; }
	inline void set__whode_2(float value)
	{
		____whode_2 = value;
	}

	inline static int32_t get_offset_of__semre_3() { return static_cast<int32_t>(offsetof(M_sedre190_t3586298931, ____semre_3)); }
	inline String_t* get__semre_3() const { return ____semre_3; }
	inline String_t** get_address_of__semre_3() { return &____semre_3; }
	inline void set__semre_3(String_t* value)
	{
		____semre_3 = value;
		Il2CppCodeGenWriteBarrier(&____semre_3, value);
	}

	inline static int32_t get_offset_of__risti_4() { return static_cast<int32_t>(offsetof(M_sedre190_t3586298931, ____risti_4)); }
	inline uint32_t get__risti_4() const { return ____risti_4; }
	inline uint32_t* get_address_of__risti_4() { return &____risti_4; }
	inline void set__risti_4(uint32_t value)
	{
		____risti_4 = value;
	}

	inline static int32_t get_offset_of__wamuSidemda_5() { return static_cast<int32_t>(offsetof(M_sedre190_t3586298931, ____wamuSidemda_5)); }
	inline float get__wamuSidemda_5() const { return ____wamuSidemda_5; }
	inline float* get_address_of__wamuSidemda_5() { return &____wamuSidemda_5; }
	inline void set__wamuSidemda_5(float value)
	{
		____wamuSidemda_5 = value;
	}

	inline static int32_t get_offset_of__kergislall_6() { return static_cast<int32_t>(offsetof(M_sedre190_t3586298931, ____kergislall_6)); }
	inline String_t* get__kergislall_6() const { return ____kergislall_6; }
	inline String_t** get_address_of__kergislall_6() { return &____kergislall_6; }
	inline void set__kergislall_6(String_t* value)
	{
		____kergislall_6 = value;
		Il2CppCodeGenWriteBarrier(&____kergislall_6, value);
	}

	inline static int32_t get_offset_of__rerefas_7() { return static_cast<int32_t>(offsetof(M_sedre190_t3586298931, ____rerefas_7)); }
	inline bool get__rerefas_7() const { return ____rerefas_7; }
	inline bool* get_address_of__rerefas_7() { return &____rerefas_7; }
	inline void set__rerefas_7(bool value)
	{
		____rerefas_7 = value;
	}

	inline static int32_t get_offset_of__curoo_8() { return static_cast<int32_t>(offsetof(M_sedre190_t3586298931, ____curoo_8)); }
	inline float get__curoo_8() const { return ____curoo_8; }
	inline float* get_address_of__curoo_8() { return &____curoo_8; }
	inline void set__curoo_8(float value)
	{
		____curoo_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
