﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_FixedArra526322725MethodDeclarations.h"

// System.Collections.IEnumerator Pathfinding.Poly2Tri.FixedArray3`1<Pathfinding.Poly2Tri.TriangulationPoint>::System.Collections.IEnumerable.GetEnumerator()
#define FixedArray3_1_System_Collections_IEnumerable_GetEnumerator_m4159533615(__this, method) ((  Il2CppObject * (*) (FixedArray3_1_t165589287 *, const MethodInfo*))FixedArray3_1_System_Collections_IEnumerable_GetEnumerator_m4035815537_gshared)(__this, method)
// T Pathfinding.Poly2Tri.FixedArray3`1<Pathfinding.Poly2Tri.TriangulationPoint>::get_Item(System.Int32)
#define FixedArray3_1_get_Item_m7701314(__this, ___index0, method) ((  TriangulationPoint_t3810082933 * (*) (FixedArray3_1_t165589287 *, int32_t, const MethodInfo*))FixedArray3_1_get_Item_m2156685124_gshared)(__this, ___index0, method)
// System.Void Pathfinding.Poly2Tri.FixedArray3`1<Pathfinding.Poly2Tri.TriangulationPoint>::set_Item(System.Int32,T)
#define FixedArray3_1_set_Item_m461223341(__this, ___index0, ___value1, method) ((  void (*) (FixedArray3_1_t165589287 *, int32_t, TriangulationPoint_t3810082933 *, const MethodInfo*))FixedArray3_1_set_Item_m851048943_gshared)(__this, ___index0, ___value1, method)
// System.Boolean Pathfinding.Poly2Tri.FixedArray3`1<Pathfinding.Poly2Tri.TriangulationPoint>::Contains(T)
#define FixedArray3_1_Contains_m2839867457(__this, ___value0, method) ((  bool (*) (FixedArray3_1_t165589287 *, TriangulationPoint_t3810082933 *, const MethodInfo*))FixedArray3_1_Contains_m872077375_gshared)(__this, ___value0, method)
// System.Int32 Pathfinding.Poly2Tri.FixedArray3`1<Pathfinding.Poly2Tri.TriangulationPoint>::IndexOf(T)
#define FixedArray3_1_IndexOf_m584960535(__this, ___value0, method) ((  int32_t (*) (FixedArray3_1_t165589287 *, TriangulationPoint_t3810082933 *, const MethodInfo*))FixedArray3_1_IndexOf_m548310489_gshared)(__this, ___value0, method)
// System.Void Pathfinding.Poly2Tri.FixedArray3`1<Pathfinding.Poly2Tri.TriangulationPoint>::Clear()
#define FixedArray3_1_Clear_m1250374843(__this, method) ((  void (*) (FixedArray3_1_t165589287 *, const MethodInfo*))FixedArray3_1_Clear_m1266883641_gshared)(__this, method)
// System.Collections.Generic.IEnumerable`1<T> Pathfinding.Poly2Tri.FixedArray3`1<Pathfinding.Poly2Tri.TriangulationPoint>::Enumerate()
#define FixedArray3_1_Enumerate_m841985202(__this, method) ((  Il2CppObject* (*) (FixedArray3_1_t165589287 *, const MethodInfo*))FixedArray3_1_Enumerate_m2474609968_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> Pathfinding.Poly2Tri.FixedArray3`1<Pathfinding.Poly2Tri.TriangulationPoint>::GetEnumerator()
#define FixedArray3_1_GetEnumerator_m2886123268(__this, method) ((  Il2CppObject* (*) (FixedArray3_1_t165589287 *, const MethodInfo*))FixedArray3_1_GetEnumerator_m983331714_gshared)(__this, method)
