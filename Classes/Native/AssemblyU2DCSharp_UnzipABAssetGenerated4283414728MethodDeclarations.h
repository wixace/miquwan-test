﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnzipABAssetGenerated
struct UnzipABAssetGenerated_t4283414728;
// JSVCall
struct JSVCall_t3708497963;
// UnzipABAsset
struct UnzipABAsset_t414492807;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// FileInfoRes
struct FileInfoRes_t1217059798;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_UnzipABAsset414492807.h"
#include "AssemblyU2DCSharp_FileInfoRes1217059798.h"

// System.Void UnzipABAssetGenerated::.ctor()
extern "C"  void UnzipABAssetGenerated__ctor_m2946933155 (UnzipABAssetGenerated_t4283414728 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnzipABAssetGenerated::UnzipABAsset_UnzipABAsset1(JSVCall,System.Int32)
extern "C"  bool UnzipABAssetGenerated_UnzipABAsset_UnzipABAsset1_m1963617103 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnzipABAssetGenerated::UnzipABAsset_isPause(JSVCall)
extern "C"  void UnzipABAssetGenerated_UnzipABAsset_isPause_m2478218930 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnzipABAssetGenerated::UnzipABAsset_isChecked(JSVCall)
extern "C"  void UnzipABAssetGenerated_UnzipABAsset_isChecked_m1719548833 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnzipABAssetGenerated::UnzipABAsset_IsUnZipFinish(JSVCall)
extern "C"  void UnzipABAssetGenerated_UnzipABAsset_IsUnZipFinish_m2499445869 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnzipABAssetGenerated::UnzipABAsset_Instance(JSVCall)
extern "C"  void UnzipABAssetGenerated_UnzipABAsset_Instance_m3716925657 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnzipABAssetGenerated::UnzipABAsset_Progress(JSVCall)
extern "C"  void UnzipABAssetGenerated_UnzipABAsset_Progress_m4102776385 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnzipABAssetGenerated::UnzipABAsset_AddAsset__FileInfoRes(JSVCall,System.Int32)
extern "C"  bool UnzipABAssetGenerated_UnzipABAsset_AddAsset__FileInfoRes_m1937823852 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnzipABAssetGenerated::UnzipABAsset_Start(JSVCall,System.Int32)
extern "C"  bool UnzipABAssetGenerated_UnzipABAsset_Start_m726675719 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnzipABAssetGenerated::UnzipABAsset_ZUpdate(JSVCall,System.Int32)
extern "C"  bool UnzipABAssetGenerated_UnzipABAsset_ZUpdate_m2180469448 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnzipABAssetGenerated::__Register()
extern "C"  void UnzipABAssetGenerated___Register_m1612540164 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnzipABAssetGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnzipABAssetGenerated_ilo_getObject1_m436291315 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnzipABAssetGenerated::ilo_getBooleanS2(System.Int32)
extern "C"  bool UnzipABAssetGenerated_ilo_getBooleanS2_m313851170 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnzipABAssetGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void UnzipABAssetGenerated_ilo_setBooleanS3_m2884372754 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnzipABAsset UnzipABAssetGenerated::ilo_get_Instance4()
extern "C"  UnzipABAsset_t414492807 * UnzipABAssetGenerated_ilo_get_Instance4_m1875808858 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnzipABAssetGenerated::ilo_setObject5(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnzipABAssetGenerated_ilo_setObject5_m1210511215 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnzipABAssetGenerated::ilo_AddAsset6(UnzipABAsset,FileInfoRes)
extern "C"  void UnzipABAssetGenerated_ilo_AddAsset6_m1357411364 (Il2CppObject * __this /* static, unused */, UnzipABAsset_t414492807 * ____this0, FileInfoRes_t1217059798 * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
