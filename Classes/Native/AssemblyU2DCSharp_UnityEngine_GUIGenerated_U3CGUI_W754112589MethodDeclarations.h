﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUIGenerated/<GUI_Window_GetDelegate_member105_arg2>c__AnonStoreyED
struct U3CGUI_Window_GetDelegate_member105_arg2U3Ec__AnonStoreyED_t754112589;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_GUIGenerated/<GUI_Window_GetDelegate_member105_arg2>c__AnonStoreyED::.ctor()
extern "C"  void U3CGUI_Window_GetDelegate_member105_arg2U3Ec__AnonStoreyED__ctor_m4086543806 (U3CGUI_Window_GetDelegate_member105_arg2U3Ec__AnonStoreyED_t754112589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated/<GUI_Window_GetDelegate_member105_arg2>c__AnonStoreyED::<>m__1D0(System.Int32)
extern "C"  void U3CGUI_Window_GetDelegate_member105_arg2U3Ec__AnonStoreyED_U3CU3Em__1D0_m1881976873 (U3CGUI_Window_GetDelegate_member105_arg2U3Ec__AnonStoreyED_t754112589 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
