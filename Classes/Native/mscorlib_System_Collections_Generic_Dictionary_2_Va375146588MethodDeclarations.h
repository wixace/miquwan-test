﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Object>
struct ValueCollection_t375146588;
// System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>
struct Dictionary_2_t1674540875;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3901341579.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m1424081231_gshared (ValueCollection_t375146588 * __this, Dictionary_2_t1674540875 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m1424081231(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t375146588 *, Dictionary_2_t1674540875 *, const MethodInfo*))ValueCollection__ctor_m1424081231_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1162319107_gshared (ValueCollection_t375146588 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1162319107(__this, ___item0, method) ((  void (*) (ValueCollection_t375146588 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1162319107_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3447618380_gshared (ValueCollection_t375146588 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3447618380(__this, method) ((  void (*) (ValueCollection_t375146588 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3447618380_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3595086663_gshared (ValueCollection_t375146588 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3595086663(__this, ___item0, method) ((  bool (*) (ValueCollection_t375146588 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3595086663_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3423725996_gshared (ValueCollection_t375146588 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3423725996(__this, ___item0, method) ((  bool (*) (ValueCollection_t375146588 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3423725996_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m781163340_gshared (ValueCollection_t375146588 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m781163340(__this, method) ((  Il2CppObject* (*) (ValueCollection_t375146588 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m781163340_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m3925634384_gshared (ValueCollection_t375146588 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m3925634384(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t375146588 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3925634384_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3307136479_gshared (ValueCollection_t375146588 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3307136479(__this, method) ((  Il2CppObject * (*) (ValueCollection_t375146588 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3307136479_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1870262522_gshared (ValueCollection_t375146588 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1870262522(__this, method) ((  bool (*) (ValueCollection_t375146588 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1870262522_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2926992858_gshared (ValueCollection_t375146588 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2926992858(__this, method) ((  bool (*) (ValueCollection_t375146588 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2926992858_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m2796159372_gshared (ValueCollection_t375146588 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2796159372(__this, method) ((  Il2CppObject * (*) (ValueCollection_t375146588 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2796159372_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m512190934_gshared (ValueCollection_t375146588 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m512190934(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t375146588 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m512190934_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3901341579  ValueCollection_GetEnumerator_m1457321599_gshared (ValueCollection_t375146588 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1457321599(__this, method) ((  Enumerator_t3901341579  (*) (ValueCollection_t375146588 *, const MethodInfo*))ValueCollection_GetEnumerator_m1457321599_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1377673108_gshared (ValueCollection_t375146588 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1377673108(__this, method) ((  int32_t (*) (ValueCollection_t375146588 *, const MethodInfo*))ValueCollection_get_Count_m1377673108_gshared)(__this, method)
