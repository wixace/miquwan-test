﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_SpringJointGenerated
struct UnityEngine_SpringJointGenerated_t1216210170;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_SpringJointGenerated::.ctor()
extern "C"  void UnityEngine_SpringJointGenerated__ctor_m669059809 (UnityEngine_SpringJointGenerated_t1216210170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SpringJointGenerated::SpringJoint_SpringJoint1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SpringJointGenerated_SpringJoint_SpringJoint1_m1538583165 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpringJointGenerated::SpringJoint_spring(JSVCall)
extern "C"  void UnityEngine_SpringJointGenerated_SpringJoint_spring_m3189505109 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpringJointGenerated::SpringJoint_damper(JSVCall)
extern "C"  void UnityEngine_SpringJointGenerated_SpringJoint_damper_m246147797 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpringJointGenerated::SpringJoint_minDistance(JSVCall)
extern "C"  void UnityEngine_SpringJointGenerated_SpringJoint_minDistance_m452214851 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpringJointGenerated::SpringJoint_maxDistance(JSVCall)
extern "C"  void UnityEngine_SpringJointGenerated_SpringJoint_maxDistance_m1532233585 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpringJointGenerated::SpringJoint_tolerance(JSVCall)
extern "C"  void UnityEngine_SpringJointGenerated_SpringJoint_tolerance_m3872632925 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpringJointGenerated::__Register()
extern "C"  void UnityEngine_SpringJointGenerated___Register_m2462652038 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_SpringJointGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_SpringJointGenerated_ilo_getObject1_m4037836497 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpringJointGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_SpringJointGenerated_ilo_addJSCSRel2_m3607390622 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpringJointGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void UnityEngine_SpringJointGenerated_ilo_setSingle3_m998908661 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_SpringJointGenerated::ilo_getSingle4(System.Int32)
extern "C"  float UnityEngine_SpringJointGenerated_ilo_getSingle4_m260582313 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
