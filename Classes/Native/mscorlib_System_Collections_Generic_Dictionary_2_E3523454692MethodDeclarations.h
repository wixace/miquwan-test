﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2628444638MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2542048336(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3523454692 *, Dictionary_2_t2206131300 *, const MethodInfo*))Enumerator__ctor_m1371663652_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3945052305(__this, method) ((  Il2CppObject * (*) (Enumerator_t3523454692 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1188600509_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3250514533(__this, method) ((  void (*) (Enumerator_t3523454692 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1846359313_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4204535534(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t3523454692 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1515285018_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1051821229(__this, method) ((  Il2CppObject * (*) (Enumerator_t3523454692 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m537269721_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2531322879(__this, method) ((  Il2CppObject * (*) (Enumerator_t3523454692 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1968562731_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::MoveNext()
#define Enumerator_MoveNext_m1448539985(__this, method) ((  bool (*) (Enumerator_t3523454692 *, const MethodInfo*))Enumerator_MoveNext_m2288691453_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::get_Current()
#define Enumerator_get_Current_m1351811263(__this, method) ((  KeyValuePair_2_t2104912006  (*) (Enumerator_t3523454692 *, const MethodInfo*))Enumerator_get_Current_m3934484115_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3239736286(__this, method) ((  uint8_t (*) (Enumerator_t3523454692 *, const MethodInfo*))Enumerator_get_CurrentKey_m2729682826_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m691193474(__this, method) ((  IBehavior_t770859129 * (*) (Enumerator_t3523454692 *, const MethodInfo*))Enumerator_get_CurrentValue_m1468074542_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::Reset()
#define Enumerator_Reset_m1255650978(__this, method) ((  void (*) (Enumerator_t3523454692 *, const MethodInfo*))Enumerator_Reset_m3314749814_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::VerifyState()
#define Enumerator_VerifyState_m3883664683(__this, method) ((  void (*) (Enumerator_t3523454692 *, const MethodInfo*))Enumerator_VerifyState_m3916755199_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2620051923(__this, method) ((  void (*) (Enumerator_t3523454692 *, const MethodInfo*))Enumerator_VerifyCurrent_m60299431_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::Dispose()
#define Enumerator_Dispose_m2318208754(__this, method) ((  void (*) (Enumerator_t3523454692 *, const MethodInfo*))Enumerator_Dispose_m1132266694_gshared)(__this, method)
