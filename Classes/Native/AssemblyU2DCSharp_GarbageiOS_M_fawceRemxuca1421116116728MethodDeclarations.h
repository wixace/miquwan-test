﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_fawceRemxuca142
struct M_fawceRemxuca142_t1116116728;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_fawceRemxuca1421116116728.h"

// System.Void GarbageiOS.M_fawceRemxuca142::.ctor()
extern "C"  void M_fawceRemxuca142__ctor_m2362535787 (M_fawceRemxuca142_t1116116728 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fawceRemxuca142::M_berkastouFurcee0(System.String[],System.Int32)
extern "C"  void M_fawceRemxuca142_M_berkastouFurcee0_m3808407622 (M_fawceRemxuca142_t1116116728 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fawceRemxuca142::M_kearbouhas1(System.String[],System.Int32)
extern "C"  void M_fawceRemxuca142_M_kearbouhas1_m592507170 (M_fawceRemxuca142_t1116116728 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fawceRemxuca142::M_sibooRadrearki2(System.String[],System.Int32)
extern "C"  void M_fawceRemxuca142_M_sibooRadrearki2_m43809589 (M_fawceRemxuca142_t1116116728 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fawceRemxuca142::M_kama3(System.String[],System.Int32)
extern "C"  void M_fawceRemxuca142_M_kama3_m1574732215 (M_fawceRemxuca142_t1116116728 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fawceRemxuca142::ilo_M_kearbouhas11(GarbageiOS.M_fawceRemxuca142,System.String[],System.Int32)
extern "C"  void M_fawceRemxuca142_ilo_M_kearbouhas11_m55237074 (Il2CppObject * __this /* static, unused */, M_fawceRemxuca142_t1116116728 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
