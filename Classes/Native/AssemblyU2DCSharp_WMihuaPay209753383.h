﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WMihuaPay
struct  WMihuaPay_t209753383  : public Il2CppObject
{
public:
	// System.String WMihuaPay::referer
	String_t* ___referer_0;

public:
	inline static int32_t get_offset_of_referer_0() { return static_cast<int32_t>(offsetof(WMihuaPay_t209753383, ___referer_0)); }
	inline String_t* get_referer_0() const { return ___referer_0; }
	inline String_t** get_address_of_referer_0() { return &___referer_0; }
	inline void set_referer_0(String_t* value)
	{
		___referer_0 = value;
		Il2CppCodeGenWriteBarrier(&___referer_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
