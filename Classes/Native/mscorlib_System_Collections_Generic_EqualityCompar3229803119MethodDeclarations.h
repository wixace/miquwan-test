﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>
struct EqualityComparer_1_t3229803119;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m571569469_gshared (EqualityComparer_1_t3229803119 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m571569469(__this, method) ((  void (*) (EqualityComparer_1_t3229803119 *, const MethodInfo*))EqualityComparer_1__ctor_m571569469_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m56688144_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m56688144(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m56688144_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m885778710_gshared (EqualityComparer_1_t3229803119 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m885778710(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t3229803119 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m885778710_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2749542836_gshared (EqualityComparer_1_t3229803119 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2749542836(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t3229803119 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2749542836_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::get_Default()
extern "C"  EqualityComparer_1_t3229803119 * EqualityComparer_1_get_Default_m1564676647_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m1564676647(__this /* static, unused */, method) ((  EqualityComparer_1_t3229803119 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m1564676647_gshared)(__this /* static, unused */, method)
