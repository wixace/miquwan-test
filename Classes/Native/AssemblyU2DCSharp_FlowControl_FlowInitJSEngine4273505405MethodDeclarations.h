﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FlowControl.FlowInitJSEngine
struct FlowInitJSEngine_t4273505405;
// FlowControl.FlowBase
struct FlowBase_t3680091731;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FlowControl_FlowBase3680091731.h"

// System.Void FlowControl.FlowInitJSEngine::.ctor()
extern "C"  void FlowInitJSEngine__ctor_m39899475 (FlowInitJSEngine_t4273505405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowInitJSEngine::Activate()
extern "C"  void FlowInitJSEngine_Activate_m2934931908 (FlowInitJSEngine_t4273505405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowInitJSEngine::ilo_set_isComplete1(FlowControl.FlowBase,System.Boolean)
extern "C"  void FlowInitJSEngine_ilo_set_isComplete1_m4188321420 (Il2CppObject * __this /* static, unused */, FlowBase_t3680091731 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
