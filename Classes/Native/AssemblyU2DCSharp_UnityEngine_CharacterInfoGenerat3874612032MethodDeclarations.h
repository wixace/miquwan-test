﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_CharacterInfoGenerated
struct UnityEngine_CharacterInfoGenerated_t3874612032;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UnityEngine_CharacterInfoGenerated::.ctor()
extern "C"  void UnityEngine_CharacterInfoGenerated__ctor_m3153892699 (UnityEngine_CharacterInfoGenerated_t3874612032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterInfoGenerated::.cctor()
extern "C"  void UnityEngine_CharacterInfoGenerated__cctor_m2799296946 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CharacterInfoGenerated::CharacterInfo_CharacterInfo1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CharacterInfoGenerated_CharacterInfo_CharacterInfo1_m4093593679 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterInfoGenerated::CharacterInfo_index(JSVCall)
extern "C"  void UnityEngine_CharacterInfoGenerated_CharacterInfo_index_m1723748420 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterInfoGenerated::CharacterInfo_size(JSVCall)
extern "C"  void UnityEngine_CharacterInfoGenerated_CharacterInfo_size_m4119139669 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterInfoGenerated::CharacterInfo_style(JSVCall)
extern "C"  void UnityEngine_CharacterInfoGenerated_CharacterInfo_style_m1658480197 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterInfoGenerated::CharacterInfo_advance(JSVCall)
extern "C"  void UnityEngine_CharacterInfoGenerated_CharacterInfo_advance_m2208686740 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterInfoGenerated::CharacterInfo_glyphWidth(JSVCall)
extern "C"  void UnityEngine_CharacterInfoGenerated_CharacterInfo_glyphWidth_m2576703868 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterInfoGenerated::CharacterInfo_glyphHeight(JSVCall)
extern "C"  void UnityEngine_CharacterInfoGenerated_CharacterInfo_glyphHeight_m3223852227 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterInfoGenerated::CharacterInfo_bearing(JSVCall)
extern "C"  void UnityEngine_CharacterInfoGenerated_CharacterInfo_bearing_m914580712 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterInfoGenerated::CharacterInfo_minY(JSVCall)
extern "C"  void UnityEngine_CharacterInfoGenerated_CharacterInfo_minY_m4031332303 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterInfoGenerated::CharacterInfo_maxY(JSVCall)
extern "C"  void UnityEngine_CharacterInfoGenerated_CharacterInfo_maxY_m2209026145 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterInfoGenerated::CharacterInfo_minX(JSVCall)
extern "C"  void UnityEngine_CharacterInfoGenerated_CharacterInfo_minX_m4227845808 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterInfoGenerated::CharacterInfo_maxX(JSVCall)
extern "C"  void UnityEngine_CharacterInfoGenerated_CharacterInfo_maxX_m2405539650 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterInfoGenerated::CharacterInfo_uvBottomLeft(JSVCall)
extern "C"  void UnityEngine_CharacterInfoGenerated_CharacterInfo_uvBottomLeft_m4007709507 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterInfoGenerated::CharacterInfo_uvBottomRight(JSVCall)
extern "C"  void UnityEngine_CharacterInfoGenerated_CharacterInfo_uvBottomRight_m2907349382 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterInfoGenerated::CharacterInfo_uvTopRight(JSVCall)
extern "C"  void UnityEngine_CharacterInfoGenerated_CharacterInfo_uvTopRight_m4056865198 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterInfoGenerated::CharacterInfo_uvTopLeft(JSVCall)
extern "C"  void UnityEngine_CharacterInfoGenerated_CharacterInfo_uvTopLeft_m442560027 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterInfoGenerated::__Register()
extern "C"  void UnityEngine_CharacterInfoGenerated___Register_m2760050764 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_CharacterInfoGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_CharacterInfoGenerated_ilo_getObject1_m3859085591 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterInfoGenerated::ilo_setInt322(System.Int32,System.Int32)
extern "C"  void UnityEngine_CharacterInfoGenerated_ilo_setInt322_m393567546 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterInfoGenerated::ilo_setEnum3(System.Int32,System.Int32)
extern "C"  void UnityEngine_CharacterInfoGenerated_ilo_setEnum3_m3290465722 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_CharacterInfoGenerated::ilo_getEnum4(System.Int32)
extern "C"  int32_t UnityEngine_CharacterInfoGenerated_ilo_getEnum4_m2094951064 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterInfoGenerated::ilo_changeJSObj5(System.Int32,System.Object)
extern "C"  void UnityEngine_CharacterInfoGenerated_ilo_changeJSObj5_m877981426 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine_CharacterInfoGenerated::ilo_getVector2S6(System.Int32)
extern "C"  Vector2_t4282066565  UnityEngine_CharacterInfoGenerated_ilo_getVector2S6_m3455813988 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterInfoGenerated::ilo_setVector2S7(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_CharacterInfoGenerated_ilo_setVector2S7_m22175986 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
