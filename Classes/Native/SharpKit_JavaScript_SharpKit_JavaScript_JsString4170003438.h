﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "SharpKit_JavaScript_SharpKit_JavaScript_JsObjectBas555886221.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SharpKit.JavaScript.JsString
struct  JsString_t4170003438  : public JsObjectBase_t555886221
{
public:
	// System.String SharpKit.JavaScript.JsString::_Value
	String_t* ____Value_0;

public:
	inline static int32_t get_offset_of__Value_0() { return static_cast<int32_t>(offsetof(JsString_t4170003438, ____Value_0)); }
	inline String_t* get__Value_0() const { return ____Value_0; }
	inline String_t** get_address_of__Value_0() { return &____Value_0; }
	inline void set__Value_0(String_t* value)
	{
		____Value_0 = value;
		Il2CppCodeGenWriteBarrier(&____Value_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
