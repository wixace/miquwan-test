﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iTween/<TweenDelay>c__Iterator2B
struct U3CTweenDelayU3Ec__Iterator2B_t1287629706;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void iTween/<TweenDelay>c__Iterator2B::.ctor()
extern "C"  void U3CTweenDelayU3Ec__Iterator2B__ctor_m2475533905 (U3CTweenDelayU3Ec__Iterator2B_t1287629706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<TweenDelay>c__Iterator2B::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTweenDelayU3Ec__Iterator2B_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2352694315 (U3CTweenDelayU3Ec__Iterator2B_t1287629706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<TweenDelay>c__Iterator2B::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTweenDelayU3Ec__Iterator2B_System_Collections_IEnumerator_get_Current_m102301119 (U3CTweenDelayU3Ec__Iterator2B_t1287629706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTween/<TweenDelay>c__Iterator2B::MoveNext()
extern "C"  bool U3CTweenDelayU3Ec__Iterator2B_MoveNext_m2985021931 (U3CTweenDelayU3Ec__Iterator2B_t1287629706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<TweenDelay>c__Iterator2B::Dispose()
extern "C"  void U3CTweenDelayU3Ec__Iterator2B_Dispose_m3769451086 (U3CTweenDelayU3Ec__Iterator2B_t1287629706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<TweenDelay>c__Iterator2B::Reset()
extern "C"  void U3CTweenDelayU3Ec__Iterator2B_Reset_m121966846 (U3CTweenDelayU3Ec__Iterator2B_t1287629706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
