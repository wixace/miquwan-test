﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginWanYu
struct PluginWanYu_t2557377933;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// Mihua.SDK.CreatRoleInfo
struct CreatRoleInfo_t2858846543;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_PluginWanYu2557377933.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void PluginWanYu::.ctor()
extern "C"  void PluginWanYu__ctor_m3207392382 (PluginWanYu_t2557377933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanYu::Init()
extern "C"  void PluginWanYu_Init_m2964494870 (PluginWanYu_t2557377933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginWanYu::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginWanYu_ReqSDKHttpLogin_m1016915055 (PluginWanYu_t2557377933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginWanYu::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginWanYu_IsLoginSuccess_m1843797293 (PluginWanYu_t2557377933 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanYu::OpenUserLogin()
extern "C"  void PluginWanYu_OpenUserLogin_m485011664 (PluginWanYu_t2557377933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanYu::UserPay(CEvent.ZEvent)
extern "C"  void PluginWanYu_UserPay_m424237218 (PluginWanYu_t2557377933 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PluginWanYu::GetTimeStamp(System.DateTime)
extern "C"  int32_t PluginWanYu_GetTimeStamp_m2161300416 (PluginWanYu_t2557377933 * __this, DateTime_t4283661327  ___dt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanYu::CreateRole(CEvent.ZEvent)
extern "C"  void PluginWanYu_CreateRole_m497346339 (PluginWanYu_t2557377933 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanYu::EnterGame(CEvent.ZEvent)
extern "C"  void PluginWanYu_EnterGame_m1886620085 (PluginWanYu_t2557377933 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanYu::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginWanYu_RoleUpgrade_m901165849 (PluginWanYu_t2557377933 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanYu::OnLoginSuccess(System.String)
extern "C"  void PluginWanYu_OnLoginSuccess_m3229307139 (PluginWanYu_t2557377933 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanYu::OnLogoutSuccess(System.String)
extern "C"  void PluginWanYu_OnLogoutSuccess_m1327816172 (PluginWanYu_t2557377933 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanYu::OnLogout(System.String)
extern "C"  void PluginWanYu_OnLogout_m2005340883 (PluginWanYu_t2557377933 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanYu::initSdk(System.String)
extern "C"  void PluginWanYu_initSdk_m3965544252 (PluginWanYu_t2557377933 * __this, String_t* ___appKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanYu::login()
extern "C"  void PluginWanYu_login_m2729407205 (PluginWanYu_t2557377933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanYu::logout()
extern "C"  void PluginWanYu_logout_m3013067440 (PluginWanYu_t2557377933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanYu::updateRoleInfo(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginWanYu_updateRoleInfo_m1595054139 (PluginWanYu_t2557377933 * __this, String_t* ___roleName0, String_t* ___roleLv1, String_t* ___serverId2, String_t* ___defaultSet3, String_t* ___Balance4, String_t* ___Power5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanYu::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginWanYu_pay_m1756121986 (PluginWanYu_t2557377933 * __this, String_t* ___productId0, String_t* ___productName1, String_t* ___amount2, String_t* ___orderId3, String_t* ___extra4, String_t* ___serverId5, String_t* ___roleName6, String_t* ___roleLv7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanYu::<OnLogoutSuccess>m__461()
extern "C"  void PluginWanYu_U3COnLogoutSuccessU3Em__461_m3628485278 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanYu::<OnLogout>m__462()
extern "C"  void PluginWanYu_U3COnLogoutU3Em__462_m175906210 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanYu::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginWanYu_ilo_AddEventListener1_m1777953974 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginWanYu::ilo_get_isSdkLogin2(VersionMgr)
extern "C"  bool PluginWanYu_ilo_get_isSdkLogin2_m860977261 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginWanYu::ilo_get_DeviceID3(PluginsSdkMgr)
extern "C"  String_t* PluginWanYu_ilo_get_DeviceID3_m811052367 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginWanYu::ilo_get_Instance4()
extern "C"  VersionMgr_t1322950208 * PluginWanYu_ilo_get_Instance4_m485167836 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanYu::ilo_OpenUserLogin5(PluginWanYu)
extern "C"  void PluginWanYu_ilo_OpenUserLogin5_m202549259 (Il2CppObject * __this /* static, unused */, PluginWanYu_t2557377933 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanYu::ilo_login6(PluginWanYu)
extern "C"  void PluginWanYu_ilo_login6_m164723007 (Il2CppObject * __this /* static, unused */, PluginWanYu_t2557377933 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.PayInfo PluginWanYu::ilo_Parse7(System.Object[])
extern "C"  PayInfo_t1775308120 * PluginWanYu_ilo_Parse7_m1744128060 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___payOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginWanYu::ilo_GetProductID8(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginWanYu_ilo_GetProductID8_m1943677460 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.CreatRoleInfo PluginWanYu::ilo_Parse9(System.Object[])
extern "C"  CreatRoleInfo_t2858846543 * PluginWanYu_ilo_Parse9_m1988375075 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanYu::ilo_updateRoleInfo10(PluginWanYu,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginWanYu_ilo_updateRoleInfo10_m3353929960 (Il2CppObject * __this /* static, unused */, PluginWanYu_t2557377933 * ____this0, String_t* ___roleName1, String_t* ___roleLv2, String_t* ___serverId3, String_t* ___defaultSet4, String_t* ___Balance5, String_t* ___Power6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginWanYu::ilo_get_PluginsSdkMgr11()
extern "C"  PluginsSdkMgr_t3884624670 * PluginWanYu_ilo_get_PluginsSdkMgr11_m801371123 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanYu::ilo_Log12(System.Object,System.Boolean)
extern "C"  void PluginWanYu_ilo_Log12_m459880471 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanYu::ilo_logout13(PluginWanYu)
extern "C"  void PluginWanYu_ilo_logout13_m2358725216 (Il2CppObject * __this /* static, unused */, PluginWanYu_t2557377933 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanYu::ilo_ReqSDKHttpLogin14(PluginsSdkMgr)
extern "C"  void PluginWanYu_ilo_ReqSDKHttpLogin14_m455111715 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
