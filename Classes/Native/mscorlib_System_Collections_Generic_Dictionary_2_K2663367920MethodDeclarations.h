﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>
struct Dictionary_2_t2048431866;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2663367920.h"
#include "AssemblyU2DCSharp_SoundTypeID3626616740.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<SoundTypeID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m342809337_gshared (Enumerator_t2663367920 * __this, Dictionary_2_t2048431866 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m342809337(__this, ___host0, method) ((  void (*) (Enumerator_t2663367920 *, Dictionary_2_t2048431866 *, const MethodInfo*))Enumerator__ctor_m342809337_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<SoundTypeID,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3840867400_gshared (Enumerator_t2663367920 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3840867400(__this, method) ((  Il2CppObject * (*) (Enumerator_t2663367920 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3840867400_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<SoundTypeID,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2082450460_gshared (Enumerator_t2663367920 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2082450460(__this, method) ((  void (*) (Enumerator_t2663367920 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2082450460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<SoundTypeID,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1909047771_gshared (Enumerator_t2663367920 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1909047771(__this, method) ((  void (*) (Enumerator_t2663367920 *, const MethodInfo*))Enumerator_Dispose_m1909047771_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<SoundTypeID,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2388084232_gshared (Enumerator_t2663367920 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2388084232(__this, method) ((  bool (*) (Enumerator_t2663367920 *, const MethodInfo*))Enumerator_MoveNext_m2388084232_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<SoundTypeID,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2176924620_gshared (Enumerator_t2663367920 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2176924620(__this, method) ((  int32_t (*) (Enumerator_t2663367920 *, const MethodInfo*))Enumerator_get_Current_m2176924620_gshared)(__this, method)
