﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_lusearmi90
struct  M_lusearmi90_t1515270671  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_lusearmi90::_mishasBaskearku
	float ____mishasBaskearku_0;
	// System.Single GarbageiOS.M_lusearmi90::_pafe
	float ____pafe_1;
	// System.Boolean GarbageiOS.M_lusearmi90::_qirlosasTelco
	bool ____qirlosasTelco_2;
	// System.UInt32 GarbageiOS.M_lusearmi90::_yahouKoufa
	uint32_t ____yahouKoufa_3;
	// System.Int32 GarbageiOS.M_lusearmi90::_dadow
	int32_t ____dadow_4;
	// System.String GarbageiOS.M_lusearmi90::_jeahascai
	String_t* ____jeahascai_5;
	// System.Int32 GarbageiOS.M_lusearmi90::_zaperwa
	int32_t ____zaperwa_6;
	// System.Boolean GarbageiOS.M_lusearmi90::_gairrudor
	bool ____gairrudor_7;
	// System.Boolean GarbageiOS.M_lusearmi90::_soufeesaLismu
	bool ____soufeesaLismu_8;
	// System.Int32 GarbageiOS.M_lusearmi90::_trersear
	int32_t ____trersear_9;
	// System.Boolean GarbageiOS.M_lusearmi90::_sawjowLacewel
	bool ____sawjowLacewel_10;
	// System.String GarbageiOS.M_lusearmi90::_xawro
	String_t* ____xawro_11;
	// System.Boolean GarbageiOS.M_lusearmi90::_touyis
	bool ____touyis_12;
	// System.String GarbageiOS.M_lusearmi90::_carbishe
	String_t* ____carbishe_13;
	// System.UInt32 GarbageiOS.M_lusearmi90::_telhota
	uint32_t ____telhota_14;
	// System.UInt32 GarbageiOS.M_lusearmi90::_tapepiTeehas
	uint32_t ____tapepiTeehas_15;
	// System.Int32 GarbageiOS.M_lusearmi90::_zerserFechu
	int32_t ____zerserFechu_16;
	// System.String GarbageiOS.M_lusearmi90::_waikawair
	String_t* ____waikawair_17;
	// System.String GarbageiOS.M_lusearmi90::_sorkoLarroji
	String_t* ____sorkoLarroji_18;
	// System.UInt32 GarbageiOS.M_lusearmi90::_jallher
	uint32_t ____jallher_19;
	// System.String GarbageiOS.M_lusearmi90::_jaryereja
	String_t* ____jaryereja_20;

public:
	inline static int32_t get_offset_of__mishasBaskearku_0() { return static_cast<int32_t>(offsetof(M_lusearmi90_t1515270671, ____mishasBaskearku_0)); }
	inline float get__mishasBaskearku_0() const { return ____mishasBaskearku_0; }
	inline float* get_address_of__mishasBaskearku_0() { return &____mishasBaskearku_0; }
	inline void set__mishasBaskearku_0(float value)
	{
		____mishasBaskearku_0 = value;
	}

	inline static int32_t get_offset_of__pafe_1() { return static_cast<int32_t>(offsetof(M_lusearmi90_t1515270671, ____pafe_1)); }
	inline float get__pafe_1() const { return ____pafe_1; }
	inline float* get_address_of__pafe_1() { return &____pafe_1; }
	inline void set__pafe_1(float value)
	{
		____pafe_1 = value;
	}

	inline static int32_t get_offset_of__qirlosasTelco_2() { return static_cast<int32_t>(offsetof(M_lusearmi90_t1515270671, ____qirlosasTelco_2)); }
	inline bool get__qirlosasTelco_2() const { return ____qirlosasTelco_2; }
	inline bool* get_address_of__qirlosasTelco_2() { return &____qirlosasTelco_2; }
	inline void set__qirlosasTelco_2(bool value)
	{
		____qirlosasTelco_2 = value;
	}

	inline static int32_t get_offset_of__yahouKoufa_3() { return static_cast<int32_t>(offsetof(M_lusearmi90_t1515270671, ____yahouKoufa_3)); }
	inline uint32_t get__yahouKoufa_3() const { return ____yahouKoufa_3; }
	inline uint32_t* get_address_of__yahouKoufa_3() { return &____yahouKoufa_3; }
	inline void set__yahouKoufa_3(uint32_t value)
	{
		____yahouKoufa_3 = value;
	}

	inline static int32_t get_offset_of__dadow_4() { return static_cast<int32_t>(offsetof(M_lusearmi90_t1515270671, ____dadow_4)); }
	inline int32_t get__dadow_4() const { return ____dadow_4; }
	inline int32_t* get_address_of__dadow_4() { return &____dadow_4; }
	inline void set__dadow_4(int32_t value)
	{
		____dadow_4 = value;
	}

	inline static int32_t get_offset_of__jeahascai_5() { return static_cast<int32_t>(offsetof(M_lusearmi90_t1515270671, ____jeahascai_5)); }
	inline String_t* get__jeahascai_5() const { return ____jeahascai_5; }
	inline String_t** get_address_of__jeahascai_5() { return &____jeahascai_5; }
	inline void set__jeahascai_5(String_t* value)
	{
		____jeahascai_5 = value;
		Il2CppCodeGenWriteBarrier(&____jeahascai_5, value);
	}

	inline static int32_t get_offset_of__zaperwa_6() { return static_cast<int32_t>(offsetof(M_lusearmi90_t1515270671, ____zaperwa_6)); }
	inline int32_t get__zaperwa_6() const { return ____zaperwa_6; }
	inline int32_t* get_address_of__zaperwa_6() { return &____zaperwa_6; }
	inline void set__zaperwa_6(int32_t value)
	{
		____zaperwa_6 = value;
	}

	inline static int32_t get_offset_of__gairrudor_7() { return static_cast<int32_t>(offsetof(M_lusearmi90_t1515270671, ____gairrudor_7)); }
	inline bool get__gairrudor_7() const { return ____gairrudor_7; }
	inline bool* get_address_of__gairrudor_7() { return &____gairrudor_7; }
	inline void set__gairrudor_7(bool value)
	{
		____gairrudor_7 = value;
	}

	inline static int32_t get_offset_of__soufeesaLismu_8() { return static_cast<int32_t>(offsetof(M_lusearmi90_t1515270671, ____soufeesaLismu_8)); }
	inline bool get__soufeesaLismu_8() const { return ____soufeesaLismu_8; }
	inline bool* get_address_of__soufeesaLismu_8() { return &____soufeesaLismu_8; }
	inline void set__soufeesaLismu_8(bool value)
	{
		____soufeesaLismu_8 = value;
	}

	inline static int32_t get_offset_of__trersear_9() { return static_cast<int32_t>(offsetof(M_lusearmi90_t1515270671, ____trersear_9)); }
	inline int32_t get__trersear_9() const { return ____trersear_9; }
	inline int32_t* get_address_of__trersear_9() { return &____trersear_9; }
	inline void set__trersear_9(int32_t value)
	{
		____trersear_9 = value;
	}

	inline static int32_t get_offset_of__sawjowLacewel_10() { return static_cast<int32_t>(offsetof(M_lusearmi90_t1515270671, ____sawjowLacewel_10)); }
	inline bool get__sawjowLacewel_10() const { return ____sawjowLacewel_10; }
	inline bool* get_address_of__sawjowLacewel_10() { return &____sawjowLacewel_10; }
	inline void set__sawjowLacewel_10(bool value)
	{
		____sawjowLacewel_10 = value;
	}

	inline static int32_t get_offset_of__xawro_11() { return static_cast<int32_t>(offsetof(M_lusearmi90_t1515270671, ____xawro_11)); }
	inline String_t* get__xawro_11() const { return ____xawro_11; }
	inline String_t** get_address_of__xawro_11() { return &____xawro_11; }
	inline void set__xawro_11(String_t* value)
	{
		____xawro_11 = value;
		Il2CppCodeGenWriteBarrier(&____xawro_11, value);
	}

	inline static int32_t get_offset_of__touyis_12() { return static_cast<int32_t>(offsetof(M_lusearmi90_t1515270671, ____touyis_12)); }
	inline bool get__touyis_12() const { return ____touyis_12; }
	inline bool* get_address_of__touyis_12() { return &____touyis_12; }
	inline void set__touyis_12(bool value)
	{
		____touyis_12 = value;
	}

	inline static int32_t get_offset_of__carbishe_13() { return static_cast<int32_t>(offsetof(M_lusearmi90_t1515270671, ____carbishe_13)); }
	inline String_t* get__carbishe_13() const { return ____carbishe_13; }
	inline String_t** get_address_of__carbishe_13() { return &____carbishe_13; }
	inline void set__carbishe_13(String_t* value)
	{
		____carbishe_13 = value;
		Il2CppCodeGenWriteBarrier(&____carbishe_13, value);
	}

	inline static int32_t get_offset_of__telhota_14() { return static_cast<int32_t>(offsetof(M_lusearmi90_t1515270671, ____telhota_14)); }
	inline uint32_t get__telhota_14() const { return ____telhota_14; }
	inline uint32_t* get_address_of__telhota_14() { return &____telhota_14; }
	inline void set__telhota_14(uint32_t value)
	{
		____telhota_14 = value;
	}

	inline static int32_t get_offset_of__tapepiTeehas_15() { return static_cast<int32_t>(offsetof(M_lusearmi90_t1515270671, ____tapepiTeehas_15)); }
	inline uint32_t get__tapepiTeehas_15() const { return ____tapepiTeehas_15; }
	inline uint32_t* get_address_of__tapepiTeehas_15() { return &____tapepiTeehas_15; }
	inline void set__tapepiTeehas_15(uint32_t value)
	{
		____tapepiTeehas_15 = value;
	}

	inline static int32_t get_offset_of__zerserFechu_16() { return static_cast<int32_t>(offsetof(M_lusearmi90_t1515270671, ____zerserFechu_16)); }
	inline int32_t get__zerserFechu_16() const { return ____zerserFechu_16; }
	inline int32_t* get_address_of__zerserFechu_16() { return &____zerserFechu_16; }
	inline void set__zerserFechu_16(int32_t value)
	{
		____zerserFechu_16 = value;
	}

	inline static int32_t get_offset_of__waikawair_17() { return static_cast<int32_t>(offsetof(M_lusearmi90_t1515270671, ____waikawair_17)); }
	inline String_t* get__waikawair_17() const { return ____waikawair_17; }
	inline String_t** get_address_of__waikawair_17() { return &____waikawair_17; }
	inline void set__waikawair_17(String_t* value)
	{
		____waikawair_17 = value;
		Il2CppCodeGenWriteBarrier(&____waikawair_17, value);
	}

	inline static int32_t get_offset_of__sorkoLarroji_18() { return static_cast<int32_t>(offsetof(M_lusearmi90_t1515270671, ____sorkoLarroji_18)); }
	inline String_t* get__sorkoLarroji_18() const { return ____sorkoLarroji_18; }
	inline String_t** get_address_of__sorkoLarroji_18() { return &____sorkoLarroji_18; }
	inline void set__sorkoLarroji_18(String_t* value)
	{
		____sorkoLarroji_18 = value;
		Il2CppCodeGenWriteBarrier(&____sorkoLarroji_18, value);
	}

	inline static int32_t get_offset_of__jallher_19() { return static_cast<int32_t>(offsetof(M_lusearmi90_t1515270671, ____jallher_19)); }
	inline uint32_t get__jallher_19() const { return ____jallher_19; }
	inline uint32_t* get_address_of__jallher_19() { return &____jallher_19; }
	inline void set__jallher_19(uint32_t value)
	{
		____jallher_19 = value;
	}

	inline static int32_t get_offset_of__jaryereja_20() { return static_cast<int32_t>(offsetof(M_lusearmi90_t1515270671, ____jaryereja_20)); }
	inline String_t* get__jaryereja_20() const { return ____jaryereja_20; }
	inline String_t** get_address_of__jaryereja_20() { return &____jaryereja_20; }
	inline void set__jaryereja_20(String_t* value)
	{
		____jaryereja_20 = value;
		Il2CppCodeGenWriteBarrier(&____jaryereja_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
