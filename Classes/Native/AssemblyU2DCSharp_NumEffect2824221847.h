﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Transform
struct Transform_t1659122786;
// UILabel
struct UILabel_t291504320;
// UISprite
struct UISprite_t661437049;
// TweenPosition
struct TweenPosition_t3684358292;
// TweenScale
struct TweenScale_t2936666559;
// TweenAlpha
struct TweenAlpha_t2920325587;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_HeadUpBloodMgr_LabelType4067300664.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NumEffect
struct  NumEffect_t2824221847  : public Il2CppObject
{
public:
	// UnityEngine.GameObject NumEffect::go
	GameObject_t3674682005 * ___go_0;
	// UnityEngine.Vector3 NumEffect::pos3D
	Vector3_t4282066566  ___pos3D_1;
	// UnityEngine.Transform NumEffect::parentTr
	Transform_t1659122786 * ___parentTr_2;
	// System.Single NumEffect::sendTime
	float ___sendTime_3;
	// UnityEngine.GameObject NumEffect::parent
	GameObject_t3674682005 * ___parent_4;
	// UnityEngine.Transform NumEffect::tr
	Transform_t1659122786 * ___tr_5;
	// UILabel NumEffect::label
	UILabel_t291504320 * ___label_6;
	// UISprite NumEffect::sprite
	UISprite_t661437049 * ___sprite_7;
	// TweenPosition NumEffect::tPos1
	TweenPosition_t3684358292 * ___tPos1_8;
	// TweenPosition NumEffect::tPos2
	TweenPosition_t3684358292 * ___tPos2_9;
	// TweenScale NumEffect::ts1
	TweenScale_t2936666559 * ___ts1_10;
	// TweenScale NumEffect::ts2
	TweenScale_t2936666559 * ___ts2_11;
	// TweenAlpha NumEffect::ta1
	TweenAlpha_t2920325587 * ___ta1_12;
	// TweenPosition NumEffect::buffTp
	TweenPosition_t3684358292 * ___buffTp_13;
	// UnityEngine.Vector3 NumEffect::pos
	Vector3_t4282066566  ___pos_14;
	// UnityEngine.Vector3 NumEffect::scale
	Vector3_t4282066566  ___scale_15;
	// System.UInt32 NumEffect::delayID
	uint32_t ___delayID_16;
	// System.Boolean NumEffect::isbuff
	bool ___isbuff_17;
	// HeadUpBloodMgr/LabelType NumEffect::labelType
	int32_t ___labelType_18;
	// System.Boolean NumEffect::ishero
	bool ___ishero_19;
	// System.Int32 NumEffect::index
	int32_t ___index_20;

public:
	inline static int32_t get_offset_of_go_0() { return static_cast<int32_t>(offsetof(NumEffect_t2824221847, ___go_0)); }
	inline GameObject_t3674682005 * get_go_0() const { return ___go_0; }
	inline GameObject_t3674682005 ** get_address_of_go_0() { return &___go_0; }
	inline void set_go_0(GameObject_t3674682005 * value)
	{
		___go_0 = value;
		Il2CppCodeGenWriteBarrier(&___go_0, value);
	}

	inline static int32_t get_offset_of_pos3D_1() { return static_cast<int32_t>(offsetof(NumEffect_t2824221847, ___pos3D_1)); }
	inline Vector3_t4282066566  get_pos3D_1() const { return ___pos3D_1; }
	inline Vector3_t4282066566 * get_address_of_pos3D_1() { return &___pos3D_1; }
	inline void set_pos3D_1(Vector3_t4282066566  value)
	{
		___pos3D_1 = value;
	}

	inline static int32_t get_offset_of_parentTr_2() { return static_cast<int32_t>(offsetof(NumEffect_t2824221847, ___parentTr_2)); }
	inline Transform_t1659122786 * get_parentTr_2() const { return ___parentTr_2; }
	inline Transform_t1659122786 ** get_address_of_parentTr_2() { return &___parentTr_2; }
	inline void set_parentTr_2(Transform_t1659122786 * value)
	{
		___parentTr_2 = value;
		Il2CppCodeGenWriteBarrier(&___parentTr_2, value);
	}

	inline static int32_t get_offset_of_sendTime_3() { return static_cast<int32_t>(offsetof(NumEffect_t2824221847, ___sendTime_3)); }
	inline float get_sendTime_3() const { return ___sendTime_3; }
	inline float* get_address_of_sendTime_3() { return &___sendTime_3; }
	inline void set_sendTime_3(float value)
	{
		___sendTime_3 = value;
	}

	inline static int32_t get_offset_of_parent_4() { return static_cast<int32_t>(offsetof(NumEffect_t2824221847, ___parent_4)); }
	inline GameObject_t3674682005 * get_parent_4() const { return ___parent_4; }
	inline GameObject_t3674682005 ** get_address_of_parent_4() { return &___parent_4; }
	inline void set_parent_4(GameObject_t3674682005 * value)
	{
		___parent_4 = value;
		Il2CppCodeGenWriteBarrier(&___parent_4, value);
	}

	inline static int32_t get_offset_of_tr_5() { return static_cast<int32_t>(offsetof(NumEffect_t2824221847, ___tr_5)); }
	inline Transform_t1659122786 * get_tr_5() const { return ___tr_5; }
	inline Transform_t1659122786 ** get_address_of_tr_5() { return &___tr_5; }
	inline void set_tr_5(Transform_t1659122786 * value)
	{
		___tr_5 = value;
		Il2CppCodeGenWriteBarrier(&___tr_5, value);
	}

	inline static int32_t get_offset_of_label_6() { return static_cast<int32_t>(offsetof(NumEffect_t2824221847, ___label_6)); }
	inline UILabel_t291504320 * get_label_6() const { return ___label_6; }
	inline UILabel_t291504320 ** get_address_of_label_6() { return &___label_6; }
	inline void set_label_6(UILabel_t291504320 * value)
	{
		___label_6 = value;
		Il2CppCodeGenWriteBarrier(&___label_6, value);
	}

	inline static int32_t get_offset_of_sprite_7() { return static_cast<int32_t>(offsetof(NumEffect_t2824221847, ___sprite_7)); }
	inline UISprite_t661437049 * get_sprite_7() const { return ___sprite_7; }
	inline UISprite_t661437049 ** get_address_of_sprite_7() { return &___sprite_7; }
	inline void set_sprite_7(UISprite_t661437049 * value)
	{
		___sprite_7 = value;
		Il2CppCodeGenWriteBarrier(&___sprite_7, value);
	}

	inline static int32_t get_offset_of_tPos1_8() { return static_cast<int32_t>(offsetof(NumEffect_t2824221847, ___tPos1_8)); }
	inline TweenPosition_t3684358292 * get_tPos1_8() const { return ___tPos1_8; }
	inline TweenPosition_t3684358292 ** get_address_of_tPos1_8() { return &___tPos1_8; }
	inline void set_tPos1_8(TweenPosition_t3684358292 * value)
	{
		___tPos1_8 = value;
		Il2CppCodeGenWriteBarrier(&___tPos1_8, value);
	}

	inline static int32_t get_offset_of_tPos2_9() { return static_cast<int32_t>(offsetof(NumEffect_t2824221847, ___tPos2_9)); }
	inline TweenPosition_t3684358292 * get_tPos2_9() const { return ___tPos2_9; }
	inline TweenPosition_t3684358292 ** get_address_of_tPos2_9() { return &___tPos2_9; }
	inline void set_tPos2_9(TweenPosition_t3684358292 * value)
	{
		___tPos2_9 = value;
		Il2CppCodeGenWriteBarrier(&___tPos2_9, value);
	}

	inline static int32_t get_offset_of_ts1_10() { return static_cast<int32_t>(offsetof(NumEffect_t2824221847, ___ts1_10)); }
	inline TweenScale_t2936666559 * get_ts1_10() const { return ___ts1_10; }
	inline TweenScale_t2936666559 ** get_address_of_ts1_10() { return &___ts1_10; }
	inline void set_ts1_10(TweenScale_t2936666559 * value)
	{
		___ts1_10 = value;
		Il2CppCodeGenWriteBarrier(&___ts1_10, value);
	}

	inline static int32_t get_offset_of_ts2_11() { return static_cast<int32_t>(offsetof(NumEffect_t2824221847, ___ts2_11)); }
	inline TweenScale_t2936666559 * get_ts2_11() const { return ___ts2_11; }
	inline TweenScale_t2936666559 ** get_address_of_ts2_11() { return &___ts2_11; }
	inline void set_ts2_11(TweenScale_t2936666559 * value)
	{
		___ts2_11 = value;
		Il2CppCodeGenWriteBarrier(&___ts2_11, value);
	}

	inline static int32_t get_offset_of_ta1_12() { return static_cast<int32_t>(offsetof(NumEffect_t2824221847, ___ta1_12)); }
	inline TweenAlpha_t2920325587 * get_ta1_12() const { return ___ta1_12; }
	inline TweenAlpha_t2920325587 ** get_address_of_ta1_12() { return &___ta1_12; }
	inline void set_ta1_12(TweenAlpha_t2920325587 * value)
	{
		___ta1_12 = value;
		Il2CppCodeGenWriteBarrier(&___ta1_12, value);
	}

	inline static int32_t get_offset_of_buffTp_13() { return static_cast<int32_t>(offsetof(NumEffect_t2824221847, ___buffTp_13)); }
	inline TweenPosition_t3684358292 * get_buffTp_13() const { return ___buffTp_13; }
	inline TweenPosition_t3684358292 ** get_address_of_buffTp_13() { return &___buffTp_13; }
	inline void set_buffTp_13(TweenPosition_t3684358292 * value)
	{
		___buffTp_13 = value;
		Il2CppCodeGenWriteBarrier(&___buffTp_13, value);
	}

	inline static int32_t get_offset_of_pos_14() { return static_cast<int32_t>(offsetof(NumEffect_t2824221847, ___pos_14)); }
	inline Vector3_t4282066566  get_pos_14() const { return ___pos_14; }
	inline Vector3_t4282066566 * get_address_of_pos_14() { return &___pos_14; }
	inline void set_pos_14(Vector3_t4282066566  value)
	{
		___pos_14 = value;
	}

	inline static int32_t get_offset_of_scale_15() { return static_cast<int32_t>(offsetof(NumEffect_t2824221847, ___scale_15)); }
	inline Vector3_t4282066566  get_scale_15() const { return ___scale_15; }
	inline Vector3_t4282066566 * get_address_of_scale_15() { return &___scale_15; }
	inline void set_scale_15(Vector3_t4282066566  value)
	{
		___scale_15 = value;
	}

	inline static int32_t get_offset_of_delayID_16() { return static_cast<int32_t>(offsetof(NumEffect_t2824221847, ___delayID_16)); }
	inline uint32_t get_delayID_16() const { return ___delayID_16; }
	inline uint32_t* get_address_of_delayID_16() { return &___delayID_16; }
	inline void set_delayID_16(uint32_t value)
	{
		___delayID_16 = value;
	}

	inline static int32_t get_offset_of_isbuff_17() { return static_cast<int32_t>(offsetof(NumEffect_t2824221847, ___isbuff_17)); }
	inline bool get_isbuff_17() const { return ___isbuff_17; }
	inline bool* get_address_of_isbuff_17() { return &___isbuff_17; }
	inline void set_isbuff_17(bool value)
	{
		___isbuff_17 = value;
	}

	inline static int32_t get_offset_of_labelType_18() { return static_cast<int32_t>(offsetof(NumEffect_t2824221847, ___labelType_18)); }
	inline int32_t get_labelType_18() const { return ___labelType_18; }
	inline int32_t* get_address_of_labelType_18() { return &___labelType_18; }
	inline void set_labelType_18(int32_t value)
	{
		___labelType_18 = value;
	}

	inline static int32_t get_offset_of_ishero_19() { return static_cast<int32_t>(offsetof(NumEffect_t2824221847, ___ishero_19)); }
	inline bool get_ishero_19() const { return ___ishero_19; }
	inline bool* get_address_of_ishero_19() { return &___ishero_19; }
	inline void set_ishero_19(bool value)
	{
		___ishero_19 = value;
	}

	inline static int32_t get_offset_of_index_20() { return static_cast<int32_t>(offsetof(NumEffect_t2824221847, ___index_20)); }
	inline int32_t get_index_20() const { return ___index_20; }
	inline int32_t* get_address_of_index_20() { return &___index_20; }
	inline void set_index_20(int32_t value)
	{
		___index_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
