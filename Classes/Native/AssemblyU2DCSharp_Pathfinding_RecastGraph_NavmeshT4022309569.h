﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// Pathfinding.Int3[]
struct Int3U5BU5D_t516284607;
// Pathfinding.TriangleMeshNode[]
struct TriangleMeshNodeU5BU5D_t2064970368;
// Pathfinding.BBTree
struct BBTree_t1216325332;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RecastGraph/NavmeshTile
struct  NavmeshTile_t4022309569  : public Il2CppObject
{
public:
	// System.Int32[] Pathfinding.RecastGraph/NavmeshTile::tris
	Int32U5BU5D_t3230847821* ___tris_0;
	// Pathfinding.Int3[] Pathfinding.RecastGraph/NavmeshTile::verts
	Int3U5BU5D_t516284607* ___verts_1;
	// System.Int32 Pathfinding.RecastGraph/NavmeshTile::x
	int32_t ___x_2;
	// System.Int32 Pathfinding.RecastGraph/NavmeshTile::z
	int32_t ___z_3;
	// System.Int32 Pathfinding.RecastGraph/NavmeshTile::w
	int32_t ___w_4;
	// System.Int32 Pathfinding.RecastGraph/NavmeshTile::d
	int32_t ___d_5;
	// Pathfinding.TriangleMeshNode[] Pathfinding.RecastGraph/NavmeshTile::nodes
	TriangleMeshNodeU5BU5D_t2064970368* ___nodes_6;
	// Pathfinding.BBTree Pathfinding.RecastGraph/NavmeshTile::bbTree
	BBTree_t1216325332 * ___bbTree_7;
	// System.Boolean Pathfinding.RecastGraph/NavmeshTile::flag
	bool ___flag_8;

public:
	inline static int32_t get_offset_of_tris_0() { return static_cast<int32_t>(offsetof(NavmeshTile_t4022309569, ___tris_0)); }
	inline Int32U5BU5D_t3230847821* get_tris_0() const { return ___tris_0; }
	inline Int32U5BU5D_t3230847821** get_address_of_tris_0() { return &___tris_0; }
	inline void set_tris_0(Int32U5BU5D_t3230847821* value)
	{
		___tris_0 = value;
		Il2CppCodeGenWriteBarrier(&___tris_0, value);
	}

	inline static int32_t get_offset_of_verts_1() { return static_cast<int32_t>(offsetof(NavmeshTile_t4022309569, ___verts_1)); }
	inline Int3U5BU5D_t516284607* get_verts_1() const { return ___verts_1; }
	inline Int3U5BU5D_t516284607** get_address_of_verts_1() { return &___verts_1; }
	inline void set_verts_1(Int3U5BU5D_t516284607* value)
	{
		___verts_1 = value;
		Il2CppCodeGenWriteBarrier(&___verts_1, value);
	}

	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(NavmeshTile_t4022309569, ___x_2)); }
	inline int32_t get_x_2() const { return ___x_2; }
	inline int32_t* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(int32_t value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(NavmeshTile_t4022309569, ___z_3)); }
	inline int32_t get_z_3() const { return ___z_3; }
	inline int32_t* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(int32_t value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(NavmeshTile_t4022309569, ___w_4)); }
	inline int32_t get_w_4() const { return ___w_4; }
	inline int32_t* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(int32_t value)
	{
		___w_4 = value;
	}

	inline static int32_t get_offset_of_d_5() { return static_cast<int32_t>(offsetof(NavmeshTile_t4022309569, ___d_5)); }
	inline int32_t get_d_5() const { return ___d_5; }
	inline int32_t* get_address_of_d_5() { return &___d_5; }
	inline void set_d_5(int32_t value)
	{
		___d_5 = value;
	}

	inline static int32_t get_offset_of_nodes_6() { return static_cast<int32_t>(offsetof(NavmeshTile_t4022309569, ___nodes_6)); }
	inline TriangleMeshNodeU5BU5D_t2064970368* get_nodes_6() const { return ___nodes_6; }
	inline TriangleMeshNodeU5BU5D_t2064970368** get_address_of_nodes_6() { return &___nodes_6; }
	inline void set_nodes_6(TriangleMeshNodeU5BU5D_t2064970368* value)
	{
		___nodes_6 = value;
		Il2CppCodeGenWriteBarrier(&___nodes_6, value);
	}

	inline static int32_t get_offset_of_bbTree_7() { return static_cast<int32_t>(offsetof(NavmeshTile_t4022309569, ___bbTree_7)); }
	inline BBTree_t1216325332 * get_bbTree_7() const { return ___bbTree_7; }
	inline BBTree_t1216325332 ** get_address_of_bbTree_7() { return &___bbTree_7; }
	inline void set_bbTree_7(BBTree_t1216325332 * value)
	{
		___bbTree_7 = value;
		Il2CppCodeGenWriteBarrier(&___bbTree_7, value);
	}

	inline static int32_t get_offset_of_flag_8() { return static_cast<int32_t>(offsetof(NavmeshTile_t4022309569, ___flag_8)); }
	inline bool get_flag_8() const { return ___flag_8; }
	inline bool* get_address_of_flag_8() { return &___flag_8; }
	inline void set_flag_8(bool value)
	{
		___flag_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
