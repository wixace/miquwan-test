﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DoorController
struct DoorController_t2858937866;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DoorController2858937866.h"

// System.Void DoorController::.ctor()
extern "C"  void DoorController__ctor_m1368918481 (DoorController_t2858937866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DoorController::Start()
extern "C"  void DoorController_Start_m316056273 (DoorController_t2858937866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DoorController::OnGUI()
extern "C"  void DoorController_OnGUI_m864317131 (DoorController_t2858937866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DoorController::SetState(System.Boolean)
extern "C"  void DoorController_SetState_m4137769945 (DoorController_t2858937866 * __this, bool ___open0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DoorController::Update()
extern "C"  void DoorController_Update_m1213662044 (DoorController_t2858937866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DoorController::ilo_SetState1(DoorController,System.Boolean)
extern "C"  void DoorController_ilo_SetState1_m2135905641 (Il2CppObject * __this /* static, unused */, DoorController_t2858937866 * ____this0, bool ___open1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
