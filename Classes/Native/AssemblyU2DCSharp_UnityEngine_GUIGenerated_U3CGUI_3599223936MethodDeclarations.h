﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUIGenerated/<GUI_Window_GetDelegate_member110_arg2>c__AnonStoreyF2
struct U3CGUI_Window_GetDelegate_member110_arg2U3Ec__AnonStoreyF2_t3599223936;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_GUIGenerated/<GUI_Window_GetDelegate_member110_arg2>c__AnonStoreyF2::.ctor()
extern "C"  void U3CGUI_Window_GetDelegate_member110_arg2U3Ec__AnonStoreyF2__ctor_m2062732011 (U3CGUI_Window_GetDelegate_member110_arg2U3Ec__AnonStoreyF2_t3599223936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated/<GUI_Window_GetDelegate_member110_arg2>c__AnonStoreyF2::<>m__1DA(System.Int32)
extern "C"  void U3CGUI_Window_GetDelegate_member110_arg2U3Ec__AnonStoreyF2_U3CU3Em__1DA_m4099344813 (U3CGUI_Window_GetDelegate_member110_arg2U3Ec__AnonStoreyF2_t3599223936 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
