﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SwipeRecognizer
struct SwipeRecognizer_t2690489438;
// System.String
struct String_t;
// SwipeGesture
struct SwipeGesture_t529355983;
// FingerGestures/IFingerList
struct IFingerList_t1026994100;
// GestureRecognizer
struct GestureRecognizer_t3512875949;
// Gesture
struct Gesture_t1589572905;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SwipeGesture529355983.h"
#include "AssemblyU2DCSharp_GestureRecognitionState3604239971.h"
#include "AssemblyU2DCSharp_FingerGestures_SwipeDirection1218055201.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_GestureRecognizer3512875949.h"
#include "AssemblyU2DCSharp_Gesture1589572905.h"

// System.Void SwipeRecognizer::.ctor()
extern "C"  void SwipeRecognizer__ctor_m3192992973 (SwipeRecognizer_t2690489438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SwipeRecognizer::GetDefaultEventMessageName()
extern "C"  String_t* SwipeRecognizer_GetDefaultEventMessageName_m1535361207 (SwipeRecognizer_t2690489438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SwipeRecognizer::CanBegin(SwipeGesture,FingerGestures/IFingerList)
extern "C"  bool SwipeRecognizer_CanBegin_m2355353413 (SwipeRecognizer_t2690489438 * __this, SwipeGesture_t529355983 * ___gesture0, Il2CppObject * ___touches1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeRecognizer::OnBegin(SwipeGesture,FingerGestures/IFingerList)
extern "C"  void SwipeRecognizer_OnBegin_m978475852 (SwipeRecognizer_t2690489438 * __this, SwipeGesture_t529355983 * ___gesture0, Il2CppObject * ___touches1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GestureRecognitionState SwipeRecognizer::OnRecognize(SwipeGesture,FingerGestures/IFingerList)
extern "C"  int32_t SwipeRecognizer_OnRecognize_m3880585079 (SwipeRecognizer_t2690489438 * __this, SwipeGesture_t529355983 * ___gesture0, Il2CppObject * ___touches1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SwipeRecognizer::IsValidDirection(FingerGestures/SwipeDirection)
extern "C"  bool SwipeRecognizer_IsValidDirection_m1043563425 (SwipeRecognizer_t2690489438 * __this, int32_t ___dir0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SwipeRecognizer::ilo_GetAverageDistanceFromStart1(FingerGestures/IFingerList)
extern "C"  float SwipeRecognizer_ilo_GetAverageDistanceFromStart1_m2474808831 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SwipeRecognizer::ilo_MovingInSameDirection2(FingerGestures/IFingerList,System.Single)
extern "C"  bool SwipeRecognizer_ilo_MovingInSameDirection2_m229315795 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, float ___tolerance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 SwipeRecognizer::ilo_GetAverageStartPosition3(FingerGestures/IFingerList)
extern "C"  Vector2_t4282066565  SwipeRecognizer_ilo_GetAverageStartPosition3_m1420535050 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 SwipeRecognizer::ilo_GetAveragePosition4(FingerGestures/IFingerList)
extern "C"  Vector2_t4282066565  SwipeRecognizer_ilo_GetAveragePosition4_m1692052739 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeRecognizer::ilo_set_Move5(SwipeGesture,UnityEngine.Vector2)
extern "C"  void SwipeRecognizer_ilo_set_Move5_m1155875844 (Il2CppObject * __this /* static, unused */, SwipeGesture_t529355983 * ____this0, Vector2_t4282066565  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SwipeRecognizer::ilo_ToPixels6(GestureRecognizer,System.Single)
extern "C"  float SwipeRecognizer_ilo_ToPixels6_m3281215928 (Il2CppObject * __this /* static, unused */, GestureRecognizer_t3512875949 * ____this0, float ___distance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SwipeRecognizer::ilo_get_Count7(FingerGestures/IFingerList)
extern "C"  int32_t SwipeRecognizer_ilo_get_Count7_m1003125849 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures/SwipeDirection SwipeRecognizer::ilo_GetSwipeDirection8(UnityEngine.Vector2)
extern "C"  int32_t SwipeRecognizer_ilo_GetSwipeDirection8_m3599234597 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___dir0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeRecognizer::ilo_set_Direction9(SwipeGesture,FingerGestures/SwipeDirection)
extern "C"  void SwipeRecognizer_ilo_set_Direction9_m1723115997 (Il2CppObject * __this /* static, unused */, SwipeGesture_t529355983 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 SwipeRecognizer::ilo_get_Position10(Gesture)
extern "C"  Vector2_t4282066565  SwipeRecognizer_ilo_get_Position10_m996514569 (Il2CppObject * __this /* static, unused */, Gesture_t1589572905 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 SwipeRecognizer::ilo_get_StartPosition11(Gesture)
extern "C"  Vector2_t4282066565  SwipeRecognizer_ilo_get_StartPosition11_m3412406388 (Il2CppObject * __this /* static, unused */, Gesture_t1589572905 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeRecognizer::ilo_set_Velocity12(SwipeGesture,System.Single)
extern "C"  void SwipeRecognizer_ilo_set_Velocity12_m3698245883 (Il2CppObject * __this /* static, unused */, SwipeGesture_t529355983 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SwipeRecognizer::ilo_get_Velocity13(SwipeGesture)
extern "C"  float SwipeRecognizer_ilo_get_Velocity13_m454216471 (Il2CppObject * __this /* static, unused */, SwipeGesture_t529355983 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
