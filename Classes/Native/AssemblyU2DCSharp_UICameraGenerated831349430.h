﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// JSDataExchangeMgr/DGetV`1<UICamera/GetKeyStateFunc>
struct DGetV_1_t2623386115;
// JSDataExchangeMgr/DGetV`1<UICamera/GetAxisFunc>
struct DGetV_1_t661266610;
// JSDataExchangeMgr/DGetV`1<UICamera/OnScreenResize>
struct DGetV_1_t3706230114;
// JSDataExchangeMgr/DGetV`1<UICamera/OnCustomInput>
struct DGetV_1_t613957169;
// JSDataExchangeMgr/DGetV`1<UICamera/VoidDelegate>
struct DGetV_1_t3012693596;
// JSDataExchangeMgr/DGetV`1<UICamera/BoolDelegate>
struct DGetV_1_t1972191666;
// JSDataExchangeMgr/DGetV`1<UICamera/FloatDelegate>
struct DGetV_1_t1024172728;
// JSDataExchangeMgr/DGetV`1<UICamera/VectorDelegate>
struct DGetV_1_t2625565067;
// JSDataExchangeMgr/DGetV`1<UICamera/ObjectDelegate>
struct DGetV_1_t3581015943;
// JSDataExchangeMgr/DGetV`1<UICamera/KeyCodeDelegate>
struct DGetV_1_t3998107336;
// JSDataExchangeMgr/DGetV`1<UICamera/MoveDelegate>
struct DGetV_1_t1783786393;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICameraGenerated
struct  UICameraGenerated_t831349430  : public Il2CppObject
{
public:

public:
};

struct UICameraGenerated_t831349430_StaticFields
{
public:
	// JSDataExchangeMgr/DGetV`1<UICamera/GetKeyStateFunc> UICameraGenerated::<>f__am$cache0
	DGetV_1_t2623386115 * ___U3CU3Ef__amU24cache0_0;
	// JSDataExchangeMgr/DGetV`1<UICamera/GetKeyStateFunc> UICameraGenerated::<>f__am$cache1
	DGetV_1_t2623386115 * ___U3CU3Ef__amU24cache1_1;
	// JSDataExchangeMgr/DGetV`1<UICamera/GetKeyStateFunc> UICameraGenerated::<>f__am$cache2
	DGetV_1_t2623386115 * ___U3CU3Ef__amU24cache2_2;
	// JSDataExchangeMgr/DGetV`1<UICamera/GetAxisFunc> UICameraGenerated::<>f__am$cache3
	DGetV_1_t661266610 * ___U3CU3Ef__amU24cache3_3;
	// JSDataExchangeMgr/DGetV`1<UICamera/OnScreenResize> UICameraGenerated::<>f__am$cache4
	DGetV_1_t3706230114 * ___U3CU3Ef__amU24cache4_4;
	// JSDataExchangeMgr/DGetV`1<UICamera/OnCustomInput> UICameraGenerated::<>f__am$cache5
	DGetV_1_t613957169 * ___U3CU3Ef__amU24cache5_5;
	// JSDataExchangeMgr/DGetV`1<UICamera/VoidDelegate> UICameraGenerated::<>f__am$cache6
	DGetV_1_t3012693596 * ___U3CU3Ef__amU24cache6_6;
	// JSDataExchangeMgr/DGetV`1<UICamera/VoidDelegate> UICameraGenerated::<>f__am$cache7
	DGetV_1_t3012693596 * ___U3CU3Ef__amU24cache7_7;
	// JSDataExchangeMgr/DGetV`1<UICamera/BoolDelegate> UICameraGenerated::<>f__am$cache8
	DGetV_1_t1972191666 * ___U3CU3Ef__amU24cache8_8;
	// JSDataExchangeMgr/DGetV`1<UICamera/BoolDelegate> UICameraGenerated::<>f__am$cache9
	DGetV_1_t1972191666 * ___U3CU3Ef__amU24cache9_9;
	// JSDataExchangeMgr/DGetV`1<UICamera/BoolDelegate> UICameraGenerated::<>f__am$cacheA
	DGetV_1_t1972191666 * ___U3CU3Ef__amU24cacheA_10;
	// JSDataExchangeMgr/DGetV`1<UICamera/FloatDelegate> UICameraGenerated::<>f__am$cacheB
	DGetV_1_t1024172728 * ___U3CU3Ef__amU24cacheB_11;
	// JSDataExchangeMgr/DGetV`1<UICamera/VectorDelegate> UICameraGenerated::<>f__am$cacheC
	DGetV_1_t2625565067 * ___U3CU3Ef__amU24cacheC_12;
	// JSDataExchangeMgr/DGetV`1<UICamera/VoidDelegate> UICameraGenerated::<>f__am$cacheD
	DGetV_1_t3012693596 * ___U3CU3Ef__amU24cacheD_13;
	// JSDataExchangeMgr/DGetV`1<UICamera/ObjectDelegate> UICameraGenerated::<>f__am$cacheE
	DGetV_1_t3581015943 * ___U3CU3Ef__amU24cacheE_14;
	// JSDataExchangeMgr/DGetV`1<UICamera/ObjectDelegate> UICameraGenerated::<>f__am$cacheF
	DGetV_1_t3581015943 * ___U3CU3Ef__amU24cacheF_15;
	// JSDataExchangeMgr/DGetV`1<UICamera/VoidDelegate> UICameraGenerated::<>f__am$cache10
	DGetV_1_t3012693596 * ___U3CU3Ef__amU24cache10_16;
	// JSDataExchangeMgr/DGetV`1<UICamera/ObjectDelegate> UICameraGenerated::<>f__am$cache11
	DGetV_1_t3581015943 * ___U3CU3Ef__amU24cache11_17;
	// JSDataExchangeMgr/DGetV`1<UICamera/KeyCodeDelegate> UICameraGenerated::<>f__am$cache12
	DGetV_1_t3998107336 * ___U3CU3Ef__amU24cache12_18;
	// JSDataExchangeMgr/DGetV`1<UICamera/BoolDelegate> UICameraGenerated::<>f__am$cache13
	DGetV_1_t1972191666 * ___U3CU3Ef__amU24cache13_19;
	// JSDataExchangeMgr/DGetV`1<UICamera/MoveDelegate> UICameraGenerated::<>f__am$cache14
	DGetV_1_t1783786393 * ___U3CU3Ef__amU24cache14_20;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(UICameraGenerated_t831349430_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline DGetV_1_t2623386115 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline DGetV_1_t2623386115 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(DGetV_1_t2623386115 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(UICameraGenerated_t831349430_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline DGetV_1_t2623386115 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline DGetV_1_t2623386115 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(DGetV_1_t2623386115 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(UICameraGenerated_t831349430_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline DGetV_1_t2623386115 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline DGetV_1_t2623386115 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(DGetV_1_t2623386115 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(UICameraGenerated_t831349430_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline DGetV_1_t661266610 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline DGetV_1_t661266610 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(DGetV_1_t661266610 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(UICameraGenerated_t831349430_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline DGetV_1_t3706230114 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline DGetV_1_t3706230114 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(DGetV_1_t3706230114 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(UICameraGenerated_t831349430_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline DGetV_1_t613957169 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline DGetV_1_t613957169 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(DGetV_1_t613957169 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(UICameraGenerated_t831349430_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline DGetV_1_t3012693596 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline DGetV_1_t3012693596 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(DGetV_1_t3012693596 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(UICameraGenerated_t831349430_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline DGetV_1_t3012693596 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline DGetV_1_t3012693596 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(DGetV_1_t3012693596 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_8() { return static_cast<int32_t>(offsetof(UICameraGenerated_t831349430_StaticFields, ___U3CU3Ef__amU24cache8_8)); }
	inline DGetV_1_t1972191666 * get_U3CU3Ef__amU24cache8_8() const { return ___U3CU3Ef__amU24cache8_8; }
	inline DGetV_1_t1972191666 ** get_address_of_U3CU3Ef__amU24cache8_8() { return &___U3CU3Ef__amU24cache8_8; }
	inline void set_U3CU3Ef__amU24cache8_8(DGetV_1_t1972191666 * value)
	{
		___U3CU3Ef__amU24cache8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_9() { return static_cast<int32_t>(offsetof(UICameraGenerated_t831349430_StaticFields, ___U3CU3Ef__amU24cache9_9)); }
	inline DGetV_1_t1972191666 * get_U3CU3Ef__amU24cache9_9() const { return ___U3CU3Ef__amU24cache9_9; }
	inline DGetV_1_t1972191666 ** get_address_of_U3CU3Ef__amU24cache9_9() { return &___U3CU3Ef__amU24cache9_9; }
	inline void set_U3CU3Ef__amU24cache9_9(DGetV_1_t1972191666 * value)
	{
		___U3CU3Ef__amU24cache9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_10() { return static_cast<int32_t>(offsetof(UICameraGenerated_t831349430_StaticFields, ___U3CU3Ef__amU24cacheA_10)); }
	inline DGetV_1_t1972191666 * get_U3CU3Ef__amU24cacheA_10() const { return ___U3CU3Ef__amU24cacheA_10; }
	inline DGetV_1_t1972191666 ** get_address_of_U3CU3Ef__amU24cacheA_10() { return &___U3CU3Ef__amU24cacheA_10; }
	inline void set_U3CU3Ef__amU24cacheA_10(DGetV_1_t1972191666 * value)
	{
		___U3CU3Ef__amU24cacheA_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_11() { return static_cast<int32_t>(offsetof(UICameraGenerated_t831349430_StaticFields, ___U3CU3Ef__amU24cacheB_11)); }
	inline DGetV_1_t1024172728 * get_U3CU3Ef__amU24cacheB_11() const { return ___U3CU3Ef__amU24cacheB_11; }
	inline DGetV_1_t1024172728 ** get_address_of_U3CU3Ef__amU24cacheB_11() { return &___U3CU3Ef__amU24cacheB_11; }
	inline void set_U3CU3Ef__amU24cacheB_11(DGetV_1_t1024172728 * value)
	{
		___U3CU3Ef__amU24cacheB_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_12() { return static_cast<int32_t>(offsetof(UICameraGenerated_t831349430_StaticFields, ___U3CU3Ef__amU24cacheC_12)); }
	inline DGetV_1_t2625565067 * get_U3CU3Ef__amU24cacheC_12() const { return ___U3CU3Ef__amU24cacheC_12; }
	inline DGetV_1_t2625565067 ** get_address_of_U3CU3Ef__amU24cacheC_12() { return &___U3CU3Ef__amU24cacheC_12; }
	inline void set_U3CU3Ef__amU24cacheC_12(DGetV_1_t2625565067 * value)
	{
		___U3CU3Ef__amU24cacheC_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheC_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_13() { return static_cast<int32_t>(offsetof(UICameraGenerated_t831349430_StaticFields, ___U3CU3Ef__amU24cacheD_13)); }
	inline DGetV_1_t3012693596 * get_U3CU3Ef__amU24cacheD_13() const { return ___U3CU3Ef__amU24cacheD_13; }
	inline DGetV_1_t3012693596 ** get_address_of_U3CU3Ef__amU24cacheD_13() { return &___U3CU3Ef__amU24cacheD_13; }
	inline void set_U3CU3Ef__amU24cacheD_13(DGetV_1_t3012693596 * value)
	{
		___U3CU3Ef__amU24cacheD_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheD_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheE_14() { return static_cast<int32_t>(offsetof(UICameraGenerated_t831349430_StaticFields, ___U3CU3Ef__amU24cacheE_14)); }
	inline DGetV_1_t3581015943 * get_U3CU3Ef__amU24cacheE_14() const { return ___U3CU3Ef__amU24cacheE_14; }
	inline DGetV_1_t3581015943 ** get_address_of_U3CU3Ef__amU24cacheE_14() { return &___U3CU3Ef__amU24cacheE_14; }
	inline void set_U3CU3Ef__amU24cacheE_14(DGetV_1_t3581015943 * value)
	{
		___U3CU3Ef__amU24cacheE_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheE_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheF_15() { return static_cast<int32_t>(offsetof(UICameraGenerated_t831349430_StaticFields, ___U3CU3Ef__amU24cacheF_15)); }
	inline DGetV_1_t3581015943 * get_U3CU3Ef__amU24cacheF_15() const { return ___U3CU3Ef__amU24cacheF_15; }
	inline DGetV_1_t3581015943 ** get_address_of_U3CU3Ef__amU24cacheF_15() { return &___U3CU3Ef__amU24cacheF_15; }
	inline void set_U3CU3Ef__amU24cacheF_15(DGetV_1_t3581015943 * value)
	{
		___U3CU3Ef__amU24cacheF_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheF_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache10_16() { return static_cast<int32_t>(offsetof(UICameraGenerated_t831349430_StaticFields, ___U3CU3Ef__amU24cache10_16)); }
	inline DGetV_1_t3012693596 * get_U3CU3Ef__amU24cache10_16() const { return ___U3CU3Ef__amU24cache10_16; }
	inline DGetV_1_t3012693596 ** get_address_of_U3CU3Ef__amU24cache10_16() { return &___U3CU3Ef__amU24cache10_16; }
	inline void set_U3CU3Ef__amU24cache10_16(DGetV_1_t3012693596 * value)
	{
		___U3CU3Ef__amU24cache10_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache10_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache11_17() { return static_cast<int32_t>(offsetof(UICameraGenerated_t831349430_StaticFields, ___U3CU3Ef__amU24cache11_17)); }
	inline DGetV_1_t3581015943 * get_U3CU3Ef__amU24cache11_17() const { return ___U3CU3Ef__amU24cache11_17; }
	inline DGetV_1_t3581015943 ** get_address_of_U3CU3Ef__amU24cache11_17() { return &___U3CU3Ef__amU24cache11_17; }
	inline void set_U3CU3Ef__amU24cache11_17(DGetV_1_t3581015943 * value)
	{
		___U3CU3Ef__amU24cache11_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache11_17, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache12_18() { return static_cast<int32_t>(offsetof(UICameraGenerated_t831349430_StaticFields, ___U3CU3Ef__amU24cache12_18)); }
	inline DGetV_1_t3998107336 * get_U3CU3Ef__amU24cache12_18() const { return ___U3CU3Ef__amU24cache12_18; }
	inline DGetV_1_t3998107336 ** get_address_of_U3CU3Ef__amU24cache12_18() { return &___U3CU3Ef__amU24cache12_18; }
	inline void set_U3CU3Ef__amU24cache12_18(DGetV_1_t3998107336 * value)
	{
		___U3CU3Ef__amU24cache12_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache12_18, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache13_19() { return static_cast<int32_t>(offsetof(UICameraGenerated_t831349430_StaticFields, ___U3CU3Ef__amU24cache13_19)); }
	inline DGetV_1_t1972191666 * get_U3CU3Ef__amU24cache13_19() const { return ___U3CU3Ef__amU24cache13_19; }
	inline DGetV_1_t1972191666 ** get_address_of_U3CU3Ef__amU24cache13_19() { return &___U3CU3Ef__amU24cache13_19; }
	inline void set_U3CU3Ef__amU24cache13_19(DGetV_1_t1972191666 * value)
	{
		___U3CU3Ef__amU24cache13_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache13_19, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache14_20() { return static_cast<int32_t>(offsetof(UICameraGenerated_t831349430_StaticFields, ___U3CU3Ef__amU24cache14_20)); }
	inline DGetV_1_t1783786393 * get_U3CU3Ef__amU24cache14_20() const { return ___U3CU3Ef__amU24cache14_20; }
	inline DGetV_1_t1783786393 ** get_address_of_U3CU3Ef__amU24cache14_20() { return &___U3CU3Ef__amU24cache14_20; }
	inline void set_U3CU3Ef__amU24cache14_20(DGetV_1_t1783786393 * value)
	{
		___U3CU3Ef__amU24cache14_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache14_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
