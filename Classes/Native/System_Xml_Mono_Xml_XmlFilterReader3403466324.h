﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.XmlReader
struct XmlReader_t4123196108;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t4229224207;
// System.Xml.IXmlLineInfo
struct IXmlLineInfo_t2849465358;

#include "System_Xml_System_Xml_XmlReader4123196108.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.XmlFilterReader
struct  XmlFilterReader_t3403466324  : public XmlReader_t4123196108
{
public:
	// System.Xml.XmlReader Mono.Xml.XmlFilterReader::reader
	XmlReader_t4123196108 * ___reader_2;
	// System.Xml.XmlReaderSettings Mono.Xml.XmlFilterReader::settings
	XmlReaderSettings_t4229224207 * ___settings_3;
	// System.Xml.IXmlLineInfo Mono.Xml.XmlFilterReader::lineInfo
	Il2CppObject * ___lineInfo_4;

public:
	inline static int32_t get_offset_of_reader_2() { return static_cast<int32_t>(offsetof(XmlFilterReader_t3403466324, ___reader_2)); }
	inline XmlReader_t4123196108 * get_reader_2() const { return ___reader_2; }
	inline XmlReader_t4123196108 ** get_address_of_reader_2() { return &___reader_2; }
	inline void set_reader_2(XmlReader_t4123196108 * value)
	{
		___reader_2 = value;
		Il2CppCodeGenWriteBarrier(&___reader_2, value);
	}

	inline static int32_t get_offset_of_settings_3() { return static_cast<int32_t>(offsetof(XmlFilterReader_t3403466324, ___settings_3)); }
	inline XmlReaderSettings_t4229224207 * get_settings_3() const { return ___settings_3; }
	inline XmlReaderSettings_t4229224207 ** get_address_of_settings_3() { return &___settings_3; }
	inline void set_settings_3(XmlReaderSettings_t4229224207 * value)
	{
		___settings_3 = value;
		Il2CppCodeGenWriteBarrier(&___settings_3, value);
	}

	inline static int32_t get_offset_of_lineInfo_4() { return static_cast<int32_t>(offsetof(XmlFilterReader_t3403466324, ___lineInfo_4)); }
	inline Il2CppObject * get_lineInfo_4() const { return ___lineInfo_4; }
	inline Il2CppObject ** get_address_of_lineInfo_4() { return &___lineInfo_4; }
	inline void set_lineInfo_4(Il2CppObject * value)
	{
		___lineInfo_4 = value;
		Il2CppCodeGenWriteBarrier(&___lineInfo_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
