﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CameraHelper
struct CameraHelper_t3196871507;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraHelper/<MoveCamera>c__AnonStorey14D
struct  U3CMoveCameraU3Ec__AnonStorey14D_t2872051618  : public Il2CppObject
{
public:
	// System.Boolean CameraHelper/<MoveCamera>c__AnonStorey14D::isMove
	bool ___isMove_0;
	// System.Int32 CameraHelper/<MoveCamera>c__AnonStorey14D::_AIid
	int32_t ____AIid_1;
	// CameraHelper CameraHelper/<MoveCamera>c__AnonStorey14D::<>f__this
	CameraHelper_t3196871507 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_isMove_0() { return static_cast<int32_t>(offsetof(U3CMoveCameraU3Ec__AnonStorey14D_t2872051618, ___isMove_0)); }
	inline bool get_isMove_0() const { return ___isMove_0; }
	inline bool* get_address_of_isMove_0() { return &___isMove_0; }
	inline void set_isMove_0(bool value)
	{
		___isMove_0 = value;
	}

	inline static int32_t get_offset_of__AIid_1() { return static_cast<int32_t>(offsetof(U3CMoveCameraU3Ec__AnonStorey14D_t2872051618, ____AIid_1)); }
	inline int32_t get__AIid_1() const { return ____AIid_1; }
	inline int32_t* get_address_of__AIid_1() { return &____AIid_1; }
	inline void set__AIid_1(int32_t value)
	{
		____AIid_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CMoveCameraU3Ec__AnonStorey14D_t2872051618, ___U3CU3Ef__this_2)); }
	inline CameraHelper_t3196871507 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline CameraHelper_t3196871507 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(CameraHelper_t3196871507 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
