﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.DebugUtility
struct DebugUtility_t1152695055;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.DebugUtility::.ctor()
extern "C"  void DebugUtility__ctor_m39668792 (DebugUtility_t1152695055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.DebugUtility::Awake()
extern "C"  void DebugUtility_Awake_m277274011 (DebugUtility_t1152695055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.DebugUtility::DrawCubes(UnityEngine.Vector3[],UnityEngine.Vector3[],UnityEngine.Color[],System.Single)
extern "C"  void DebugUtility_DrawCubes_m1952129275 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t215400611* ___topVerts0, Vector3U5BU5D_t215400611* ___bottomVerts1, ColorU5BU5D_t2441545636* ___vertexColors2, float ___width3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.DebugUtility::DrawQuads(UnityEngine.Vector3[],System.Single)
extern "C"  void DebugUtility_DrawQuads_m514246814 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t215400611* ___verts0, float ___width1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.DebugUtility::TestMeshLimit()
extern "C"  void DebugUtility_TestMeshLimit_m2499311282 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.DebugUtility::ilo_DrawCubes1(UnityEngine.Vector3[],UnityEngine.Vector3[],UnityEngine.Color[],System.Single)
extern "C"  void DebugUtility_ilo_DrawCubes1_m156299093 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t215400611* ___topVerts0, Vector3U5BU5D_t215400611* ___bottomVerts1, ColorU5BU5D_t2441545636* ___vertexColors2, float ___width3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
