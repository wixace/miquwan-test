﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_WebCamDeviceGenerated
struct UnityEngine_WebCamDeviceGenerated_t3017301526;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"

// System.Void UnityEngine_WebCamDeviceGenerated::.ctor()
extern "C"  void UnityEngine_WebCamDeviceGenerated__ctor_m3086874645 (UnityEngine_WebCamDeviceGenerated_t3017301526 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WebCamDeviceGenerated::.cctor()
extern "C"  void UnityEngine_WebCamDeviceGenerated__cctor_m721737272 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WebCamDeviceGenerated::WebCamDevice_WebCamDevice1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WebCamDeviceGenerated_WebCamDevice_WebCamDevice1_m43605725 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WebCamDeviceGenerated::WebCamDevice_name(JSVCall)
extern "C"  void UnityEngine_WebCamDeviceGenerated_WebCamDevice_name_m3191426107 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WebCamDeviceGenerated::WebCamDevice_isFrontFacing(JSVCall)
extern "C"  void UnityEngine_WebCamDeviceGenerated_WebCamDevice_isFrontFacing_m2307575021 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WebCamDeviceGenerated::__Register()
extern "C"  void UnityEngine_WebCamDeviceGenerated___Register_m2037664210 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WebCamDeviceGenerated::ilo_setStringS1(System.Int32,System.String)
extern "C"  void UnityEngine_WebCamDeviceGenerated_ilo_setStringS1_m3441337438 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WebCamDeviceGenerated::ilo_setBooleanS2(System.Int32,System.Boolean)
extern "C"  void UnityEngine_WebCamDeviceGenerated_ilo_setBooleanS2_m2413055589 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
