﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite[]
struct UISpriteU5BU5D_t3727562628;
// UITexture[]
struct UITextureU5BU5D_t795227294;
// TweenPosition[]
struct TweenPositionU5BU5D_t2332880541;
// UISprite
struct UISprite_t661437049;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UILabel
struct UILabel_t291504320;
// TweenScale
struct TweenScale_t2936666559;
// TweenAlpha
struct TweenAlpha_t2920325587;

#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Float_Fighting_tips
struct  Float_Fighting_tips_t3838985378  : public FloatTextUnit_t2362298029
{
public:
	// UISprite[] Float_Fighting_tips::numSprite
	UISpriteU5BU5D_t3727562628* ___numSprite_3;
	// UITexture[] Float_Fighting_tips::numTex
	UITextureU5BU5D_t795227294* ___numTex_4;
	// TweenPosition[] Float_Fighting_tips::tp
	TweenPositionU5BU5D_t2332880541* ___tp_5;
	// System.Int32 Float_Fighting_tips::beforNum
	int32_t ___beforNum_6;
	// System.Int32 Float_Fighting_tips::curNum
	int32_t ___curNum_7;
	// System.Int32 Float_Fighting_tips::n
	int32_t ___n_8;
	// System.Int32 Float_Fighting_tips::playIndex
	int32_t ___playIndex_9;
	// System.Single Float_Fighting_tips::playTime
	float ___playTime_10;
	// System.Int32 Float_Fighting_tips::endIndex
	int32_t ___endIndex_11;
	// System.Single Float_Fighting_tips::endTime
	float ___endTime_12;
	// UISprite Float_Fighting_tips::arrow
	UISprite_t661437049 * ___arrow_13;
	// UnityEngine.GameObject Float_Fighting_tips::arrow_go
	GameObject_t3674682005 * ___arrow_go_14;
	// System.Single Float_Fighting_tips::upRate
	float ___upRate_15;
	// UILabel Float_Fighting_tips::Label_little_txt
	UILabel_t291504320 * ___Label_little_txt_16;
	// TweenScale Float_Fighting_tips::littleTs
	TweenScale_t2936666559 * ___littleTs_17;
	// TweenAlpha Float_Fighting_tips::littleTa
	TweenAlpha_t2920325587 * ___littleTa_18;
	// System.Single Float_Fighting_tips::time
	float ___time_19;

public:
	inline static int32_t get_offset_of_numSprite_3() { return static_cast<int32_t>(offsetof(Float_Fighting_tips_t3838985378, ___numSprite_3)); }
	inline UISpriteU5BU5D_t3727562628* get_numSprite_3() const { return ___numSprite_3; }
	inline UISpriteU5BU5D_t3727562628** get_address_of_numSprite_3() { return &___numSprite_3; }
	inline void set_numSprite_3(UISpriteU5BU5D_t3727562628* value)
	{
		___numSprite_3 = value;
		Il2CppCodeGenWriteBarrier(&___numSprite_3, value);
	}

	inline static int32_t get_offset_of_numTex_4() { return static_cast<int32_t>(offsetof(Float_Fighting_tips_t3838985378, ___numTex_4)); }
	inline UITextureU5BU5D_t795227294* get_numTex_4() const { return ___numTex_4; }
	inline UITextureU5BU5D_t795227294** get_address_of_numTex_4() { return &___numTex_4; }
	inline void set_numTex_4(UITextureU5BU5D_t795227294* value)
	{
		___numTex_4 = value;
		Il2CppCodeGenWriteBarrier(&___numTex_4, value);
	}

	inline static int32_t get_offset_of_tp_5() { return static_cast<int32_t>(offsetof(Float_Fighting_tips_t3838985378, ___tp_5)); }
	inline TweenPositionU5BU5D_t2332880541* get_tp_5() const { return ___tp_5; }
	inline TweenPositionU5BU5D_t2332880541** get_address_of_tp_5() { return &___tp_5; }
	inline void set_tp_5(TweenPositionU5BU5D_t2332880541* value)
	{
		___tp_5 = value;
		Il2CppCodeGenWriteBarrier(&___tp_5, value);
	}

	inline static int32_t get_offset_of_beforNum_6() { return static_cast<int32_t>(offsetof(Float_Fighting_tips_t3838985378, ___beforNum_6)); }
	inline int32_t get_beforNum_6() const { return ___beforNum_6; }
	inline int32_t* get_address_of_beforNum_6() { return &___beforNum_6; }
	inline void set_beforNum_6(int32_t value)
	{
		___beforNum_6 = value;
	}

	inline static int32_t get_offset_of_curNum_7() { return static_cast<int32_t>(offsetof(Float_Fighting_tips_t3838985378, ___curNum_7)); }
	inline int32_t get_curNum_7() const { return ___curNum_7; }
	inline int32_t* get_address_of_curNum_7() { return &___curNum_7; }
	inline void set_curNum_7(int32_t value)
	{
		___curNum_7 = value;
	}

	inline static int32_t get_offset_of_n_8() { return static_cast<int32_t>(offsetof(Float_Fighting_tips_t3838985378, ___n_8)); }
	inline int32_t get_n_8() const { return ___n_8; }
	inline int32_t* get_address_of_n_8() { return &___n_8; }
	inline void set_n_8(int32_t value)
	{
		___n_8 = value;
	}

	inline static int32_t get_offset_of_playIndex_9() { return static_cast<int32_t>(offsetof(Float_Fighting_tips_t3838985378, ___playIndex_9)); }
	inline int32_t get_playIndex_9() const { return ___playIndex_9; }
	inline int32_t* get_address_of_playIndex_9() { return &___playIndex_9; }
	inline void set_playIndex_9(int32_t value)
	{
		___playIndex_9 = value;
	}

	inline static int32_t get_offset_of_playTime_10() { return static_cast<int32_t>(offsetof(Float_Fighting_tips_t3838985378, ___playTime_10)); }
	inline float get_playTime_10() const { return ___playTime_10; }
	inline float* get_address_of_playTime_10() { return &___playTime_10; }
	inline void set_playTime_10(float value)
	{
		___playTime_10 = value;
	}

	inline static int32_t get_offset_of_endIndex_11() { return static_cast<int32_t>(offsetof(Float_Fighting_tips_t3838985378, ___endIndex_11)); }
	inline int32_t get_endIndex_11() const { return ___endIndex_11; }
	inline int32_t* get_address_of_endIndex_11() { return &___endIndex_11; }
	inline void set_endIndex_11(int32_t value)
	{
		___endIndex_11 = value;
	}

	inline static int32_t get_offset_of_endTime_12() { return static_cast<int32_t>(offsetof(Float_Fighting_tips_t3838985378, ___endTime_12)); }
	inline float get_endTime_12() const { return ___endTime_12; }
	inline float* get_address_of_endTime_12() { return &___endTime_12; }
	inline void set_endTime_12(float value)
	{
		___endTime_12 = value;
	}

	inline static int32_t get_offset_of_arrow_13() { return static_cast<int32_t>(offsetof(Float_Fighting_tips_t3838985378, ___arrow_13)); }
	inline UISprite_t661437049 * get_arrow_13() const { return ___arrow_13; }
	inline UISprite_t661437049 ** get_address_of_arrow_13() { return &___arrow_13; }
	inline void set_arrow_13(UISprite_t661437049 * value)
	{
		___arrow_13 = value;
		Il2CppCodeGenWriteBarrier(&___arrow_13, value);
	}

	inline static int32_t get_offset_of_arrow_go_14() { return static_cast<int32_t>(offsetof(Float_Fighting_tips_t3838985378, ___arrow_go_14)); }
	inline GameObject_t3674682005 * get_arrow_go_14() const { return ___arrow_go_14; }
	inline GameObject_t3674682005 ** get_address_of_arrow_go_14() { return &___arrow_go_14; }
	inline void set_arrow_go_14(GameObject_t3674682005 * value)
	{
		___arrow_go_14 = value;
		Il2CppCodeGenWriteBarrier(&___arrow_go_14, value);
	}

	inline static int32_t get_offset_of_upRate_15() { return static_cast<int32_t>(offsetof(Float_Fighting_tips_t3838985378, ___upRate_15)); }
	inline float get_upRate_15() const { return ___upRate_15; }
	inline float* get_address_of_upRate_15() { return &___upRate_15; }
	inline void set_upRate_15(float value)
	{
		___upRate_15 = value;
	}

	inline static int32_t get_offset_of_Label_little_txt_16() { return static_cast<int32_t>(offsetof(Float_Fighting_tips_t3838985378, ___Label_little_txt_16)); }
	inline UILabel_t291504320 * get_Label_little_txt_16() const { return ___Label_little_txt_16; }
	inline UILabel_t291504320 ** get_address_of_Label_little_txt_16() { return &___Label_little_txt_16; }
	inline void set_Label_little_txt_16(UILabel_t291504320 * value)
	{
		___Label_little_txt_16 = value;
		Il2CppCodeGenWriteBarrier(&___Label_little_txt_16, value);
	}

	inline static int32_t get_offset_of_littleTs_17() { return static_cast<int32_t>(offsetof(Float_Fighting_tips_t3838985378, ___littleTs_17)); }
	inline TweenScale_t2936666559 * get_littleTs_17() const { return ___littleTs_17; }
	inline TweenScale_t2936666559 ** get_address_of_littleTs_17() { return &___littleTs_17; }
	inline void set_littleTs_17(TweenScale_t2936666559 * value)
	{
		___littleTs_17 = value;
		Il2CppCodeGenWriteBarrier(&___littleTs_17, value);
	}

	inline static int32_t get_offset_of_littleTa_18() { return static_cast<int32_t>(offsetof(Float_Fighting_tips_t3838985378, ___littleTa_18)); }
	inline TweenAlpha_t2920325587 * get_littleTa_18() const { return ___littleTa_18; }
	inline TweenAlpha_t2920325587 ** get_address_of_littleTa_18() { return &___littleTa_18; }
	inline void set_littleTa_18(TweenAlpha_t2920325587 * value)
	{
		___littleTa_18 = value;
		Il2CppCodeGenWriteBarrier(&___littleTa_18, value);
	}

	inline static int32_t get_offset_of_time_19() { return static_cast<int32_t>(offsetof(Float_Fighting_tips_t3838985378, ___time_19)); }
	inline float get_time_19() const { return ___time_19; }
	inline float* get_address_of_time_19() { return &___time_19; }
	inline void set_time_19(float value)
	{
		___time_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
