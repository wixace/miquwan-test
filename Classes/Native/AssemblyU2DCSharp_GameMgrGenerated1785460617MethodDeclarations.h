﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameMgrGenerated
struct GameMgrGenerated_t1785460617;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// CSCheckPointUnit
struct CSCheckPointUnit_t3129216924;
// GameMgr
struct GameMgr_t1469029542;
// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;
// CombatEntity
struct CombatEntity_t684137495;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"
#include "AssemblyU2DCSharp_NpcMgr_EditorNpcType2400575222.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"

// System.Void GameMgrGenerated::.ctor()
extern "C"  void GameMgrGenerated__ctor_m2646382898 (GameMgrGenerated_t1785460617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_GameMgr1(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_GameMgr1_m4277700922 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_OPEN_FINISH_PANLE(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_OPEN_FINISH_PANLE_m170482929 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_BATTLE_START(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_BATTLE_START_m1753644865 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_BATTLE_ENDED(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_BATTLE_ENDED_m3123977193 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_COMBAT_WIN(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_COMBAT_WIN_m503604107 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_COMBAT_LOSE(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_COMBAT_LOSE_m1306938352 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_BATTLE_HEROORNPC_DEAD(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_BATTLE_HEROORNPC_DEAD_m3007889354 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_BATTLE_HEROHPORMP_DATA(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_BATTLE_HEROHPORMP_DATA_m3962533090 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_REPLAY_WIN(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_REPLAY_WIN_m2823978360 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_REPLAY_LOST(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_REPLAY_LOST_m1571360884 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_SHOW_END(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_SHOW_END_m1877789059 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_GET_COPPER(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_GET_COPPER_m26361946 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_FRIEND_NPC_STATE(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_FRIEND_NPC_STATE_m3381535498 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_RECORD_PLAYER_DATA(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_RECORD_PLAYER_DATA_m618645570 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_REQCHECKFINISH(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_REQCHECKFINISH_m606121407 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_LOST_REQCHECKFINISH(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_LOST_REQCHECKFINISH_m3878720440 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_SETTING_GAME_SPEED(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_SETTING_GAME_SPEED_m3101856115 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_GAME_PAUSE(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_GAME_PAUSE_m1900308275 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_UPDATE_GAME_SPEED(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_UPDATE_GAME_SPEED_m1860204832 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_PUBLIC_CD_END(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_PUBLIC_CD_END_m2420764125 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_OPEN_PlotMgr(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_OPEN_PlotMgr_m3971510458 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_CLOSE_PlotMgr(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_CLOSE_PlotMgr_m3355016960 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_DailyTurntableMgr(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_DailyTurntableMgr_m3030004336 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_GAMEINIT(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_GAMEINIT_m163980314 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_GAMEWIN(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_GAMEWIN_m2867424038 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_GAMELOST(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_GAMELOST_m2918176902 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_OPENCOMBATPANEL(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_OPENCOMBATPANEL_m2483679306 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_COMBATHURT(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_COMBATHURT_m2526966457 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_SEND_REPLAYDATA(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_SEND_REPLAYDATA_m748699720 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_GET_REPLAYDATA(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_GET_REPLAYDATA_m656545250 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_OTHERDEAD(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_OTHERDEAD_m1012025116 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_UPDATEOnOffEffect(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_UPDATEOnOffEffect_m1464675512 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_instance(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_instance_m3755695335 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_是否一刀切(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_U662FU5426U4E00U5200U5207_m1995605024 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_是否战斗不掉血(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_U662FU5426U6218U6597U4E0DU6389U8840_m2643751394 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_functionType(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_functionType_m3228904426 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_UIskillHurtShow(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_UIskillHurtShow_m2377146247 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_isSaveHpOrMp(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_isSaveHpOrMp_m1353955783 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_passType(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_passType_m2245147473 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_panelType(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_panelType_m2669005394 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_finishPanelType(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_finishPanelType_m460602565 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_IsPlayPuGongEffect(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_IsPlayPuGongEffect_m1296111047 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_missionTwo(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_missionTwo_m4139832124 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_missionThree(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_missionThree_m415888298 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_isFinishEnd(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_isFinishEnd_m1126483442 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_bossAniMgr(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_bossAniMgr_m2144995859 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_cameraAnimationMgr(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_cameraAnimationMgr_m794692291 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_checkPoint(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_checkPoint_m443878388 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_taixuTip(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_taixuTip_m3193595354 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_time(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_time_m3997284527 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_maxTime(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_maxTime_m3106656063 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_IsTimeLock(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_IsTimeLock_m1750007194 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_isAuto(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_isAuto_m3444974979 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_isAutoSkill(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_isAutoSkill_m3055466648 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_isPublicCD(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_isPublicCD_m2469035784 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_isFrozen(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_isFrozen_m3762547890 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_isFrozenIdle(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_isFrozenIdle_m686748990 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_isBattleStart(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_isBattleStart_m1189843952 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_isBattleEnd(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_isBattleEnd_m3637344919 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_isBattleEndBeforWin(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_isBattleEndBeforWin_m1505650145 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_isCameraScale(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_isCameraScale_m1379697589 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_isWin(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_isWin_m352046814 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_isStart(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_isStart_m2866573336 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_level(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_level_m1640507884 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_navAIType(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_navAIType_m2656269099 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_IsOnTap(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_IsOnTap_m1102863766 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_IsPlayPloting(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_IsPlayPloting_m4083879021 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_IsLeaderPlusAtt(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_IsLeaderPlusAtt_m769335996 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::GameMgr_IsReinPlusAtt(JSVCall)
extern "C"  void GameMgrGenerated_GameMgr_IsReinPlusAtt_m3729612619 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_AllAwait(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_AllAwait_m2224479684 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_AllDaed(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_AllDaed_m2180938352 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_CaptainViewRange__EditorNpcType(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_CaptainViewRange__EditorNpcType_m3333285837 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_EnterPublicCD__Single(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_EnterPublicCD__Single_m331482589 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_FreezeSkill__CombatEntity__Boolean(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_FreezeSkill__CombatEntity__Boolean_m3664561510 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_GetAllAlly__CombatEntity__Boolean(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_GetAllAlly__CombatEntity__Boolean_m263196865 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_GetAllAlly__CombatEntity(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_GetAllAlly__CombatEntity_m1321506441 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_GetAllEnemy__CombatEntity__Boolean(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_GetAllEnemy__CombatEntity__Boolean_m2237866083 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_GetAllEnemy__CombatEntity(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_GetAllEnemy__CombatEntity_m4279044263 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_GetAllFriendsEntity(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_GetAllFriendsEntity_m2407586176 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_GetAllHostilEntity(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_GetAllHostilEntity_m3641725864 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_GetCameraShotNearestEnemy__CombatEntity(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_GetCameraShotNearestEnemy__CombatEntity_m2663488105 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_GetEntityByID__Boolean__Int32(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_GetEntityByID__Boolean__Int32_m2354347108 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_GetLeadPlusAttStrList(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_GetLeadPlusAttStrList_m2861411917 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_GetListenStar(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_GetListenStar_m2072049922 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_GetNearestAtkRangedEnemy__CombatEntity__Single(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_GetNearestAtkRangedEnemy__CombatEntity__Single_m1678820575 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_GetNearestAtkRangedEnemy__CombatEntity(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_GetNearestAtkRangedEnemy__CombatEntity_m3322444951 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_GetNearestEnemy__CombatEntity(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_GetNearestEnemy__CombatEntity_m777490058 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_GetNearestEntity__CombatEntity__Boolean(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_GetNearestEntity__CombatEntity__Boolean_m3482449433 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_GetReinPlusAttStrList(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_GetReinPlusAttStrList_m2221177737 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_GetSpeCheckPlusAttStrList(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_GetSpeCheckPlusAttStrList_m2378182961 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_IsGradeStar1(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_IsGradeStar1_m2214665761 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_IsGradeStar2(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_IsGradeStar2_m3459430242 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_IsGradeStar3(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_IsGradeStar3_m409227427 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_LockNpcBehaviour(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_LockNpcBehaviour_m2042838454 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_LockNpcBehaviourIdle(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_LockNpcBehaviourIdle_m3229018154 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_OnDestroy(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_OnDestroy_m1988611438 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_PlayCameraAnimationEnd__ZEvent(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_PlayCameraAnimationEnd__ZEvent_m1794353119 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_ReActiveGameSpeedBtn__Boolean(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_ReActiveGameSpeedBtn__Boolean_m1393657691 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_ReqCheckFinish(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_ReqCheckFinish_m1644825932 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_SetAutoMode__Boolean(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_SetAutoMode__Boolean_m3807416195 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_SetFightCtrlUpdateState__Boolean(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_SetFightCtrlUpdateState__Boolean_m1832203880 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_TriggerBattle__CombatEntity(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_TriggerBattle__CombatEntity_m3074259802 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_UnLockNpcBehaviour(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_UnLockNpcBehaviour_m1256842799 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::GameMgr_UnLockNpcBehaviourIdle(JSVCall,System.Int32)
extern "C"  bool GameMgrGenerated_GameMgr_UnLockNpcBehaviourIdle_m3568479267 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::__Register()
extern "C"  void GameMgrGenerated___Register_m1747899093 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::ilo_setStringS1(System.Int32,System.String)
extern "C"  void GameMgrGenerated_ilo_setStringS1_m4058233595 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameMgrGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t GameMgrGenerated_ilo_setObject2_m2092554969 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::ilo_getBooleanS3(System.Int32)
extern "C"  bool GameMgrGenerated_ilo_getBooleanS3_m2419440604 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameMgrGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * GameMgrGenerated_ilo_getObject4_m485600384 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameMgrGenerated::ilo_getEnum5(System.Int32)
extern "C"  int32_t GameMgrGenerated_ilo_getEnum5_m3009274658 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::ilo_setEnum6(System.Int32,System.Int32)
extern "C"  void GameMgrGenerated_ilo_setEnum6_m2947974062 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::ilo_setBooleanS7(System.Int32,System.Boolean)
extern "C"  void GameMgrGenerated_ilo_setBooleanS7_m2601356253 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSCheckPointUnit GameMgrGenerated::ilo_get_checkPoint8(GameMgr)
extern "C"  CSCheckPointUnit_t3129216924 * GameMgrGenerated_ilo_get_checkPoint8_m1124423913 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameMgrGenerated::ilo_get_time9(GameMgr)
extern "C"  int32_t GameMgrGenerated_ilo_get_time9_m1721926938 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::ilo_setInt3210(System.Int32,System.Int32)
extern "C"  void GameMgrGenerated_ilo_setInt3210_m3989466966 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameMgrGenerated::ilo_getInt3211(System.Int32)
extern "C"  int32_t GameMgrGenerated_ilo_getInt3211_m3830971516 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::ilo_set_maxTime12(GameMgr,System.Int32)
extern "C"  void GameMgrGenerated_ilo_set_maxTime12_m3958455165 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::ilo_get_isAuto13(GameMgr)
extern "C"  bool GameMgrGenerated_ilo_get_isAuto13_m2388565103 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::ilo_set_isAuto14(GameMgr,System.Boolean)
extern "C"  void GameMgrGenerated_ilo_set_isAuto14_m3172726511 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::ilo_get_isPublicCD15(GameMgr)
extern "C"  bool GameMgrGenerated_ilo_get_isPublicCD15_m963628658 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::ilo_get_isCameraScale16(GameMgr)
extern "C"  bool GameMgrGenerated_ilo_get_isCameraScale16_m1642502924 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::ilo_get_isWin17()
extern "C"  bool GameMgrGenerated_ilo_get_isWin17_m665180536 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::ilo_get_isStart18(GameMgr)
extern "C"  bool GameMgrGenerated_ilo_get_isStart18_m4180963885 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::ilo_get_IsLeaderPlusAtt19(GameMgr)
extern "C"  bool GameMgrGenerated_ilo_get_IsLeaderPlusAtt19_m3448542704 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::ilo_get_IsReinPlusAtt20(GameMgr)
extern "C"  bool GameMgrGenerated_ilo_get_IsReinPlusAtt20_m140178729 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::ilo_AllAwait21(GameMgr)
extern "C"  void GameMgrGenerated_ilo_AllAwait21_m2304029683 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::ilo_AllDaed22(GameMgr)
extern "C"  void GameMgrGenerated_ilo_AllDaed22_m4112437554 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GameMgrGenerated::ilo_CaptainViewRange23(GameMgr,NpcMgr/EditorNpcType)
extern "C"  float GameMgrGenerated_ilo_CaptainViewRange23_m712258730 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::ilo_setSingle24(System.Int32,System.Single)
extern "C"  void GameMgrGenerated_ilo_setSingle24_m2934035871 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> GameMgrGenerated::ilo_GetAllEnemy25(GameMgr,CombatEntity,System.Boolean)
extern "C"  List_1_t2052323047 * GameMgrGenerated_ilo_GetAllEnemy25_m1306147015 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, CombatEntity_t684137495 * ___entity1, bool ___isshaix2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> GameMgrGenerated::ilo_GetAllEnemy26(GameMgr,CombatEntity)
extern "C"  List_1_t2052323047 * GameMgrGenerated_ilo_GetAllEnemy26_m441911607 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, CombatEntity_t684137495 * ___entity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> GameMgrGenerated::ilo_GetAllHostilEntity27(GameMgr)
extern "C"  List_1_t2052323047 * GameMgrGenerated_ilo_GetAllHostilEntity27_m3883648223 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity GameMgrGenerated::ilo_GetEntityByID28(GameMgr,System.Boolean,System.Int32)
extern "C"  CombatEntity_t684137495 * GameMgrGenerated_ilo_GetEntityByID28_m3281038878 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, bool ___isHero1, int32_t ___id2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GameMgrGenerated::ilo_GetListenStar29(GameMgr)
extern "C"  uint32_t GameMgrGenerated_ilo_GetListenStar29_m715360788 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::ilo_setUInt3230(System.Int32,System.UInt32)
extern "C"  void GameMgrGenerated_ilo_setUInt3230_m834815386 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GameMgrGenerated::ilo_getSingle31(System.Int32)
extern "C"  float GameMgrGenerated_ilo_getSingle31_m954572030 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity GameMgrGenerated::ilo_GetNearestAtkRangedEnemy32(GameMgr,CombatEntity)
extern "C"  CombatEntity_t684137495 * GameMgrGenerated_ilo_GetNearestAtkRangedEnemy32_m2114024638 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, CombatEntity_t684137495 * ___unit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity GameMgrGenerated::ilo_GetNearestEnemy33(GameMgr,CombatEntity)
extern "C"  CombatEntity_t684137495 * GameMgrGenerated_ilo_GetNearestEnemy33_m653837530 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, CombatEntity_t684137495 * ___unit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity GameMgrGenerated::ilo_GetNearestEntity34(GameMgr,CombatEntity,System.Boolean)
extern "C"  CombatEntity_t684137495 * GameMgrGenerated_ilo_GetNearestEntity34_m3186428003 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, CombatEntity_t684137495 * ___unit1, bool ___isCanMove2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> GameMgrGenerated::ilo_GetReinPlusAttStrList35(GameMgr)
extern "C"  List_1_t1375417109 * GameMgrGenerated_ilo_GetReinPlusAttStrList35_m2880931976 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> GameMgrGenerated::ilo_GetSpeCheckPlusAttStrList36(GameMgr)
extern "C"  List_1_t1375417109 * GameMgrGenerated_ilo_GetSpeCheckPlusAttStrList36_m1207798143 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgrGenerated::ilo_IsGradeStar337(GameMgr)
extern "C"  bool GameMgrGenerated_ilo_IsGradeStar337_m3489310907 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::ilo_LockNpcBehaviourIdle38(GameMgr)
extern "C"  void GameMgrGenerated_ilo_LockNpcBehaviourIdle38_m3537711335 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::ilo_OnDestroy39(GameMgr)
extern "C"  void GameMgrGenerated_ilo_OnDestroy39_m2377093646 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::ilo_TriggerBattle40(GameMgr,CombatEntity)
extern "C"  void GameMgrGenerated_ilo_TriggerBattle40_m2996084364 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, CombatEntity_t684137495 * ___src1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgrGenerated::ilo_UnLockNpcBehaviour41(GameMgr)
extern "C"  void GameMgrGenerated_ilo_UnLockNpcBehaviour41_m2092680554 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
