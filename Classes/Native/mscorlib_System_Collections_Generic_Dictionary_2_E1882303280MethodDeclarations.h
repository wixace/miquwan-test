﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En653211827MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1773606379(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1882303280 *, Dictionary_2_t564979888 *, const MethodInfo*))Enumerator__ctor_m1928248751_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3225570912(__this, method) ((  Il2CppObject * (*) (Enumerator_t1882303280 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m601538706_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3123199466(__this, method) ((  void (*) (Enumerator_t1882303280 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m677036966_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1543159841(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1882303280 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1544419567_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3501540156(__this, method) ((  Il2CppObject * (*) (Enumerator_t1882303280 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4255631342_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3069133518(__this, method) ((  Il2CppObject * (*) (Enumerator_t1882303280 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1901290240_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::MoveNext()
#define Enumerator_MoveNext_m2043530394(__this, method) ((  bool (*) (Enumerator_t1882303280 *, const MethodInfo*))Enumerator_MoveNext_m2962790162_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::get_Current()
#define Enumerator_get_Current_m3105443810(__this, method) ((  KeyValuePair_2_t463760594  (*) (Enumerator_t1882303280 *, const MethodInfo*))Enumerator_get_Current_m2315292254_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1843216803(__this, method) ((  uint8_t (*) (Enumerator_t1882303280 *, const MethodInfo*))Enumerator_get_CurrentKey_m3377861599_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3655937315(__this, method) ((  List_1_t1104940528 * (*) (Enumerator_t1882303280 *, const MethodInfo*))Enumerator_get_CurrentValue_m3676464963_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::Reset()
#define Enumerator_Reset_m624689085(__this, method) ((  void (*) (Enumerator_t1882303280 *, const MethodInfo*))Enumerator_Reset_m3044163969_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::VerifyState()
#define Enumerator_VerifyState_m2718813702(__this, method) ((  void (*) (Enumerator_t1882303280 *, const MethodInfo*))Enumerator_VerifyState_m2803150026_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m4184723438(__this, method) ((  void (*) (Enumerator_t1882303280 *, const MethodInfo*))Enumerator_VerifyCurrent_m3627552178_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::Dispose()
#define Enumerator_Dispose_m1554218317(__this, method) ((  void (*) (Enumerator_t1882303280 *, const MethodInfo*))Enumerator_Dispose_m3092274705_gshared)(__this, method)
