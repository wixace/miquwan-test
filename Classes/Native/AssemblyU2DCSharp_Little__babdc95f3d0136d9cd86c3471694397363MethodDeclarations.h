﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._babdc95f3d0136d9cd86c347ec215bf2
struct _babdc95f3d0136d9cd86c347ec215bf2_t1694397363;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__babdc95f3d0136d9cd86c3471694397363.h"

// System.Void Little._babdc95f3d0136d9cd86c347ec215bf2::.ctor()
extern "C"  void _babdc95f3d0136d9cd86c347ec215bf2__ctor_m3786419162 (_babdc95f3d0136d9cd86c347ec215bf2_t1694397363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._babdc95f3d0136d9cd86c347ec215bf2::_babdc95f3d0136d9cd86c347ec215bf2m2(System.Int32)
extern "C"  int32_t _babdc95f3d0136d9cd86c347ec215bf2__babdc95f3d0136d9cd86c347ec215bf2m2_m2836159673 (_babdc95f3d0136d9cd86c347ec215bf2_t1694397363 * __this, int32_t ____babdc95f3d0136d9cd86c347ec215bf2a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._babdc95f3d0136d9cd86c347ec215bf2::_babdc95f3d0136d9cd86c347ec215bf2m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _babdc95f3d0136d9cd86c347ec215bf2__babdc95f3d0136d9cd86c347ec215bf2m_m274533917 (_babdc95f3d0136d9cd86c347ec215bf2_t1694397363 * __this, int32_t ____babdc95f3d0136d9cd86c347ec215bf2a0, int32_t ____babdc95f3d0136d9cd86c347ec215bf2161, int32_t ____babdc95f3d0136d9cd86c347ec215bf2c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._babdc95f3d0136d9cd86c347ec215bf2::ilo__babdc95f3d0136d9cd86c347ec215bf2m21(Little._babdc95f3d0136d9cd86c347ec215bf2,System.Int32)
extern "C"  int32_t _babdc95f3d0136d9cd86c347ec215bf2_ilo__babdc95f3d0136d9cd86c347ec215bf2m21_m3584698586 (Il2CppObject * __this /* static, unused */, _babdc95f3d0136d9cd86c347ec215bf2_t1694397363 * ____this0, int32_t ____babdc95f3d0136d9cd86c347ec215bf2a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
