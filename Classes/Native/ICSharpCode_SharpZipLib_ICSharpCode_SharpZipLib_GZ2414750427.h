﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Sh2520014981.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.GZip.GZipException
struct  GZipException_t2414750427  : public SharpZipBaseException_t2520014981
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
