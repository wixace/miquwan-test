﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_nearcis116
struct  M_nearcis116_t1667613601  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_nearcis116::_jouhaimou
	String_t* ____jouhaimou_0;
	// System.UInt32 GarbageiOS.M_nearcis116::_sordaiteCalmair
	uint32_t ____sordaiteCalmair_1;
	// System.String GarbageiOS.M_nearcis116::_mearsootearNepeari
	String_t* ____mearsootearNepeari_2;
	// System.String GarbageiOS.M_nearcis116::_dishai
	String_t* ____dishai_3;
	// System.String GarbageiOS.M_nearcis116::_binowqaDovereqa
	String_t* ____binowqaDovereqa_4;
	// System.UInt32 GarbageiOS.M_nearcis116::_galtemwhur
	uint32_t ____galtemwhur_5;
	// System.Boolean GarbageiOS.M_nearcis116::_hatairjereJerooje
	bool ____hatairjereJerooje_6;
	// System.String GarbageiOS.M_nearcis116::_bidukearJadreasas
	String_t* ____bidukearJadreasas_7;
	// System.Boolean GarbageiOS.M_nearcis116::_troosallmouMustaigor
	bool ____troosallmouMustaigor_8;
	// System.UInt32 GarbageiOS.M_nearcis116::_hisnurNaha
	uint32_t ____hisnurNaha_9;
	// System.Single GarbageiOS.M_nearcis116::_leahirow
	float ____leahirow_10;
	// System.Int32 GarbageiOS.M_nearcis116::_behallcou
	int32_t ____behallcou_11;
	// System.String GarbageiOS.M_nearcis116::_rownel
	String_t* ____rownel_12;
	// System.Boolean GarbageiOS.M_nearcis116::_gajafeJowho
	bool ____gajafeJowho_13;
	// System.UInt32 GarbageiOS.M_nearcis116::_torhiXaytar
	uint32_t ____torhiXaytar_14;
	// System.UInt32 GarbageiOS.M_nearcis116::_celxomai
	uint32_t ____celxomai_15;
	// System.Single GarbageiOS.M_nearcis116::_ralchesoo
	float ____ralchesoo_16;
	// System.Single GarbageiOS.M_nearcis116::_dasorra
	float ____dasorra_17;
	// System.Int32 GarbageiOS.M_nearcis116::_selje
	int32_t ____selje_18;
	// System.Boolean GarbageiOS.M_nearcis116::_jairmisru
	bool ____jairmisru_19;
	// System.UInt32 GarbageiOS.M_nearcis116::_zoomero
	uint32_t ____zoomero_20;
	// System.String GarbageiOS.M_nearcis116::_hifismem
	String_t* ____hifismem_21;
	// System.UInt32 GarbageiOS.M_nearcis116::_fojicu
	uint32_t ____fojicu_22;
	// System.Int32 GarbageiOS.M_nearcis116::_merpor
	int32_t ____merpor_23;
	// System.Int32 GarbageiOS.M_nearcis116::_lerlem
	int32_t ____lerlem_24;
	// System.Boolean GarbageiOS.M_nearcis116::_sapow
	bool ____sapow_25;
	// System.UInt32 GarbageiOS.M_nearcis116::_tikear
	uint32_t ____tikear_26;
	// System.String GarbageiOS.M_nearcis116::_yemfallsurRaystohi
	String_t* ____yemfallsurRaystohi_27;
	// System.UInt32 GarbageiOS.M_nearcis116::_merete
	uint32_t ____merete_28;
	// System.Single GarbageiOS.M_nearcis116::_larosuYiskai
	float ____larosuYiskai_29;
	// System.String GarbageiOS.M_nearcis116::_wijawHogai
	String_t* ____wijawHogai_30;
	// System.String GarbageiOS.M_nearcis116::_caijarcou
	String_t* ____caijarcou_31;
	// System.UInt32 GarbageiOS.M_nearcis116::_yisaigaTremsi
	uint32_t ____yisaigaTremsi_32;
	// System.Int32 GarbageiOS.M_nearcis116::_sadraweeCisnoo
	int32_t ____sadraweeCisnoo_33;
	// System.Int32 GarbageiOS.M_nearcis116::_dafawja
	int32_t ____dafawja_34;
	// System.Int32 GarbageiOS.M_nearcis116::_trajewa
	int32_t ____trajewa_35;
	// System.Single GarbageiOS.M_nearcis116::_couneCeadayko
	float ____couneCeadayko_36;
	// System.Single GarbageiOS.M_nearcis116::_rasmayke
	float ____rasmayke_37;
	// System.Single GarbageiOS.M_nearcis116::_dralsewherePewar
	float ____dralsewherePewar_38;
	// System.Single GarbageiOS.M_nearcis116::_jallhe
	float ____jallhe_39;
	// System.UInt32 GarbageiOS.M_nearcis116::_melsarsi
	uint32_t ____melsarsi_40;
	// System.Boolean GarbageiOS.M_nearcis116::_lurmoo
	bool ____lurmoo_41;

public:
	inline static int32_t get_offset_of__jouhaimou_0() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____jouhaimou_0)); }
	inline String_t* get__jouhaimou_0() const { return ____jouhaimou_0; }
	inline String_t** get_address_of__jouhaimou_0() { return &____jouhaimou_0; }
	inline void set__jouhaimou_0(String_t* value)
	{
		____jouhaimou_0 = value;
		Il2CppCodeGenWriteBarrier(&____jouhaimou_0, value);
	}

	inline static int32_t get_offset_of__sordaiteCalmair_1() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____sordaiteCalmair_1)); }
	inline uint32_t get__sordaiteCalmair_1() const { return ____sordaiteCalmair_1; }
	inline uint32_t* get_address_of__sordaiteCalmair_1() { return &____sordaiteCalmair_1; }
	inline void set__sordaiteCalmair_1(uint32_t value)
	{
		____sordaiteCalmair_1 = value;
	}

	inline static int32_t get_offset_of__mearsootearNepeari_2() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____mearsootearNepeari_2)); }
	inline String_t* get__mearsootearNepeari_2() const { return ____mearsootearNepeari_2; }
	inline String_t** get_address_of__mearsootearNepeari_2() { return &____mearsootearNepeari_2; }
	inline void set__mearsootearNepeari_2(String_t* value)
	{
		____mearsootearNepeari_2 = value;
		Il2CppCodeGenWriteBarrier(&____mearsootearNepeari_2, value);
	}

	inline static int32_t get_offset_of__dishai_3() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____dishai_3)); }
	inline String_t* get__dishai_3() const { return ____dishai_3; }
	inline String_t** get_address_of__dishai_3() { return &____dishai_3; }
	inline void set__dishai_3(String_t* value)
	{
		____dishai_3 = value;
		Il2CppCodeGenWriteBarrier(&____dishai_3, value);
	}

	inline static int32_t get_offset_of__binowqaDovereqa_4() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____binowqaDovereqa_4)); }
	inline String_t* get__binowqaDovereqa_4() const { return ____binowqaDovereqa_4; }
	inline String_t** get_address_of__binowqaDovereqa_4() { return &____binowqaDovereqa_4; }
	inline void set__binowqaDovereqa_4(String_t* value)
	{
		____binowqaDovereqa_4 = value;
		Il2CppCodeGenWriteBarrier(&____binowqaDovereqa_4, value);
	}

	inline static int32_t get_offset_of__galtemwhur_5() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____galtemwhur_5)); }
	inline uint32_t get__galtemwhur_5() const { return ____galtemwhur_5; }
	inline uint32_t* get_address_of__galtemwhur_5() { return &____galtemwhur_5; }
	inline void set__galtemwhur_5(uint32_t value)
	{
		____galtemwhur_5 = value;
	}

	inline static int32_t get_offset_of__hatairjereJerooje_6() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____hatairjereJerooje_6)); }
	inline bool get__hatairjereJerooje_6() const { return ____hatairjereJerooje_6; }
	inline bool* get_address_of__hatairjereJerooje_6() { return &____hatairjereJerooje_6; }
	inline void set__hatairjereJerooje_6(bool value)
	{
		____hatairjereJerooje_6 = value;
	}

	inline static int32_t get_offset_of__bidukearJadreasas_7() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____bidukearJadreasas_7)); }
	inline String_t* get__bidukearJadreasas_7() const { return ____bidukearJadreasas_7; }
	inline String_t** get_address_of__bidukearJadreasas_7() { return &____bidukearJadreasas_7; }
	inline void set__bidukearJadreasas_7(String_t* value)
	{
		____bidukearJadreasas_7 = value;
		Il2CppCodeGenWriteBarrier(&____bidukearJadreasas_7, value);
	}

	inline static int32_t get_offset_of__troosallmouMustaigor_8() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____troosallmouMustaigor_8)); }
	inline bool get__troosallmouMustaigor_8() const { return ____troosallmouMustaigor_8; }
	inline bool* get_address_of__troosallmouMustaigor_8() { return &____troosallmouMustaigor_8; }
	inline void set__troosallmouMustaigor_8(bool value)
	{
		____troosallmouMustaigor_8 = value;
	}

	inline static int32_t get_offset_of__hisnurNaha_9() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____hisnurNaha_9)); }
	inline uint32_t get__hisnurNaha_9() const { return ____hisnurNaha_9; }
	inline uint32_t* get_address_of__hisnurNaha_9() { return &____hisnurNaha_9; }
	inline void set__hisnurNaha_9(uint32_t value)
	{
		____hisnurNaha_9 = value;
	}

	inline static int32_t get_offset_of__leahirow_10() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____leahirow_10)); }
	inline float get__leahirow_10() const { return ____leahirow_10; }
	inline float* get_address_of__leahirow_10() { return &____leahirow_10; }
	inline void set__leahirow_10(float value)
	{
		____leahirow_10 = value;
	}

	inline static int32_t get_offset_of__behallcou_11() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____behallcou_11)); }
	inline int32_t get__behallcou_11() const { return ____behallcou_11; }
	inline int32_t* get_address_of__behallcou_11() { return &____behallcou_11; }
	inline void set__behallcou_11(int32_t value)
	{
		____behallcou_11 = value;
	}

	inline static int32_t get_offset_of__rownel_12() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____rownel_12)); }
	inline String_t* get__rownel_12() const { return ____rownel_12; }
	inline String_t** get_address_of__rownel_12() { return &____rownel_12; }
	inline void set__rownel_12(String_t* value)
	{
		____rownel_12 = value;
		Il2CppCodeGenWriteBarrier(&____rownel_12, value);
	}

	inline static int32_t get_offset_of__gajafeJowho_13() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____gajafeJowho_13)); }
	inline bool get__gajafeJowho_13() const { return ____gajafeJowho_13; }
	inline bool* get_address_of__gajafeJowho_13() { return &____gajafeJowho_13; }
	inline void set__gajafeJowho_13(bool value)
	{
		____gajafeJowho_13 = value;
	}

	inline static int32_t get_offset_of__torhiXaytar_14() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____torhiXaytar_14)); }
	inline uint32_t get__torhiXaytar_14() const { return ____torhiXaytar_14; }
	inline uint32_t* get_address_of__torhiXaytar_14() { return &____torhiXaytar_14; }
	inline void set__torhiXaytar_14(uint32_t value)
	{
		____torhiXaytar_14 = value;
	}

	inline static int32_t get_offset_of__celxomai_15() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____celxomai_15)); }
	inline uint32_t get__celxomai_15() const { return ____celxomai_15; }
	inline uint32_t* get_address_of__celxomai_15() { return &____celxomai_15; }
	inline void set__celxomai_15(uint32_t value)
	{
		____celxomai_15 = value;
	}

	inline static int32_t get_offset_of__ralchesoo_16() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____ralchesoo_16)); }
	inline float get__ralchesoo_16() const { return ____ralchesoo_16; }
	inline float* get_address_of__ralchesoo_16() { return &____ralchesoo_16; }
	inline void set__ralchesoo_16(float value)
	{
		____ralchesoo_16 = value;
	}

	inline static int32_t get_offset_of__dasorra_17() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____dasorra_17)); }
	inline float get__dasorra_17() const { return ____dasorra_17; }
	inline float* get_address_of__dasorra_17() { return &____dasorra_17; }
	inline void set__dasorra_17(float value)
	{
		____dasorra_17 = value;
	}

	inline static int32_t get_offset_of__selje_18() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____selje_18)); }
	inline int32_t get__selje_18() const { return ____selje_18; }
	inline int32_t* get_address_of__selje_18() { return &____selje_18; }
	inline void set__selje_18(int32_t value)
	{
		____selje_18 = value;
	}

	inline static int32_t get_offset_of__jairmisru_19() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____jairmisru_19)); }
	inline bool get__jairmisru_19() const { return ____jairmisru_19; }
	inline bool* get_address_of__jairmisru_19() { return &____jairmisru_19; }
	inline void set__jairmisru_19(bool value)
	{
		____jairmisru_19 = value;
	}

	inline static int32_t get_offset_of__zoomero_20() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____zoomero_20)); }
	inline uint32_t get__zoomero_20() const { return ____zoomero_20; }
	inline uint32_t* get_address_of__zoomero_20() { return &____zoomero_20; }
	inline void set__zoomero_20(uint32_t value)
	{
		____zoomero_20 = value;
	}

	inline static int32_t get_offset_of__hifismem_21() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____hifismem_21)); }
	inline String_t* get__hifismem_21() const { return ____hifismem_21; }
	inline String_t** get_address_of__hifismem_21() { return &____hifismem_21; }
	inline void set__hifismem_21(String_t* value)
	{
		____hifismem_21 = value;
		Il2CppCodeGenWriteBarrier(&____hifismem_21, value);
	}

	inline static int32_t get_offset_of__fojicu_22() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____fojicu_22)); }
	inline uint32_t get__fojicu_22() const { return ____fojicu_22; }
	inline uint32_t* get_address_of__fojicu_22() { return &____fojicu_22; }
	inline void set__fojicu_22(uint32_t value)
	{
		____fojicu_22 = value;
	}

	inline static int32_t get_offset_of__merpor_23() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____merpor_23)); }
	inline int32_t get__merpor_23() const { return ____merpor_23; }
	inline int32_t* get_address_of__merpor_23() { return &____merpor_23; }
	inline void set__merpor_23(int32_t value)
	{
		____merpor_23 = value;
	}

	inline static int32_t get_offset_of__lerlem_24() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____lerlem_24)); }
	inline int32_t get__lerlem_24() const { return ____lerlem_24; }
	inline int32_t* get_address_of__lerlem_24() { return &____lerlem_24; }
	inline void set__lerlem_24(int32_t value)
	{
		____lerlem_24 = value;
	}

	inline static int32_t get_offset_of__sapow_25() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____sapow_25)); }
	inline bool get__sapow_25() const { return ____sapow_25; }
	inline bool* get_address_of__sapow_25() { return &____sapow_25; }
	inline void set__sapow_25(bool value)
	{
		____sapow_25 = value;
	}

	inline static int32_t get_offset_of__tikear_26() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____tikear_26)); }
	inline uint32_t get__tikear_26() const { return ____tikear_26; }
	inline uint32_t* get_address_of__tikear_26() { return &____tikear_26; }
	inline void set__tikear_26(uint32_t value)
	{
		____tikear_26 = value;
	}

	inline static int32_t get_offset_of__yemfallsurRaystohi_27() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____yemfallsurRaystohi_27)); }
	inline String_t* get__yemfallsurRaystohi_27() const { return ____yemfallsurRaystohi_27; }
	inline String_t** get_address_of__yemfallsurRaystohi_27() { return &____yemfallsurRaystohi_27; }
	inline void set__yemfallsurRaystohi_27(String_t* value)
	{
		____yemfallsurRaystohi_27 = value;
		Il2CppCodeGenWriteBarrier(&____yemfallsurRaystohi_27, value);
	}

	inline static int32_t get_offset_of__merete_28() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____merete_28)); }
	inline uint32_t get__merete_28() const { return ____merete_28; }
	inline uint32_t* get_address_of__merete_28() { return &____merete_28; }
	inline void set__merete_28(uint32_t value)
	{
		____merete_28 = value;
	}

	inline static int32_t get_offset_of__larosuYiskai_29() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____larosuYiskai_29)); }
	inline float get__larosuYiskai_29() const { return ____larosuYiskai_29; }
	inline float* get_address_of__larosuYiskai_29() { return &____larosuYiskai_29; }
	inline void set__larosuYiskai_29(float value)
	{
		____larosuYiskai_29 = value;
	}

	inline static int32_t get_offset_of__wijawHogai_30() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____wijawHogai_30)); }
	inline String_t* get__wijawHogai_30() const { return ____wijawHogai_30; }
	inline String_t** get_address_of__wijawHogai_30() { return &____wijawHogai_30; }
	inline void set__wijawHogai_30(String_t* value)
	{
		____wijawHogai_30 = value;
		Il2CppCodeGenWriteBarrier(&____wijawHogai_30, value);
	}

	inline static int32_t get_offset_of__caijarcou_31() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____caijarcou_31)); }
	inline String_t* get__caijarcou_31() const { return ____caijarcou_31; }
	inline String_t** get_address_of__caijarcou_31() { return &____caijarcou_31; }
	inline void set__caijarcou_31(String_t* value)
	{
		____caijarcou_31 = value;
		Il2CppCodeGenWriteBarrier(&____caijarcou_31, value);
	}

	inline static int32_t get_offset_of__yisaigaTremsi_32() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____yisaigaTremsi_32)); }
	inline uint32_t get__yisaigaTremsi_32() const { return ____yisaigaTremsi_32; }
	inline uint32_t* get_address_of__yisaigaTremsi_32() { return &____yisaigaTremsi_32; }
	inline void set__yisaigaTremsi_32(uint32_t value)
	{
		____yisaigaTremsi_32 = value;
	}

	inline static int32_t get_offset_of__sadraweeCisnoo_33() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____sadraweeCisnoo_33)); }
	inline int32_t get__sadraweeCisnoo_33() const { return ____sadraweeCisnoo_33; }
	inline int32_t* get_address_of__sadraweeCisnoo_33() { return &____sadraweeCisnoo_33; }
	inline void set__sadraweeCisnoo_33(int32_t value)
	{
		____sadraweeCisnoo_33 = value;
	}

	inline static int32_t get_offset_of__dafawja_34() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____dafawja_34)); }
	inline int32_t get__dafawja_34() const { return ____dafawja_34; }
	inline int32_t* get_address_of__dafawja_34() { return &____dafawja_34; }
	inline void set__dafawja_34(int32_t value)
	{
		____dafawja_34 = value;
	}

	inline static int32_t get_offset_of__trajewa_35() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____trajewa_35)); }
	inline int32_t get__trajewa_35() const { return ____trajewa_35; }
	inline int32_t* get_address_of__trajewa_35() { return &____trajewa_35; }
	inline void set__trajewa_35(int32_t value)
	{
		____trajewa_35 = value;
	}

	inline static int32_t get_offset_of__couneCeadayko_36() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____couneCeadayko_36)); }
	inline float get__couneCeadayko_36() const { return ____couneCeadayko_36; }
	inline float* get_address_of__couneCeadayko_36() { return &____couneCeadayko_36; }
	inline void set__couneCeadayko_36(float value)
	{
		____couneCeadayko_36 = value;
	}

	inline static int32_t get_offset_of__rasmayke_37() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____rasmayke_37)); }
	inline float get__rasmayke_37() const { return ____rasmayke_37; }
	inline float* get_address_of__rasmayke_37() { return &____rasmayke_37; }
	inline void set__rasmayke_37(float value)
	{
		____rasmayke_37 = value;
	}

	inline static int32_t get_offset_of__dralsewherePewar_38() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____dralsewherePewar_38)); }
	inline float get__dralsewherePewar_38() const { return ____dralsewherePewar_38; }
	inline float* get_address_of__dralsewherePewar_38() { return &____dralsewherePewar_38; }
	inline void set__dralsewherePewar_38(float value)
	{
		____dralsewherePewar_38 = value;
	}

	inline static int32_t get_offset_of__jallhe_39() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____jallhe_39)); }
	inline float get__jallhe_39() const { return ____jallhe_39; }
	inline float* get_address_of__jallhe_39() { return &____jallhe_39; }
	inline void set__jallhe_39(float value)
	{
		____jallhe_39 = value;
	}

	inline static int32_t get_offset_of__melsarsi_40() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____melsarsi_40)); }
	inline uint32_t get__melsarsi_40() const { return ____melsarsi_40; }
	inline uint32_t* get_address_of__melsarsi_40() { return &____melsarsi_40; }
	inline void set__melsarsi_40(uint32_t value)
	{
		____melsarsi_40 = value;
	}

	inline static int32_t get_offset_of__lurmoo_41() { return static_cast<int32_t>(offsetof(M_nearcis116_t1667613601, ____lurmoo_41)); }
	inline bool get__lurmoo_41() const { return ____lurmoo_41; }
	inline bool* get_address_of__lurmoo_41() { return &____lurmoo_41; }
	inline void set__lurmoo_41(bool value)
	{
		____lurmoo_41 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
