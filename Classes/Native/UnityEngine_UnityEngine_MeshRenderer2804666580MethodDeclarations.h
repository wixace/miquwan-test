﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.MeshRenderer
struct MeshRenderer_t2804666580;
// UnityEngine.Mesh
struct Mesh_t4241756145;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"

// System.Void UnityEngine.MeshRenderer::.ctor()
extern "C"  void MeshRenderer__ctor_m1053290653 (MeshRenderer_t2804666580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh UnityEngine.MeshRenderer::get_additionalVertexStreams()
extern "C"  Mesh_t4241756145 * MeshRenderer_get_additionalVertexStreams_m12375951 (MeshRenderer_t2804666580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MeshRenderer::set_additionalVertexStreams(UnityEngine.Mesh)
extern "C"  void MeshRenderer_set_additionalVertexStreams_m1436512098 (MeshRenderer_t2804666580 * __this, Mesh_t4241756145 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
