﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._e7a59e7de407be428e469c9b519472a7
struct _e7a59e7de407be428e469c9b519472a7_t2685196953;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__e7a59e7de407be428e469c9b2685196953.h"

// System.Void Little._e7a59e7de407be428e469c9b519472a7::.ctor()
extern "C"  void _e7a59e7de407be428e469c9b519472a7__ctor_m4084213684 (_e7a59e7de407be428e469c9b519472a7_t2685196953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e7a59e7de407be428e469c9b519472a7::_e7a59e7de407be428e469c9b519472a7m2(System.Int32)
extern "C"  int32_t _e7a59e7de407be428e469c9b519472a7__e7a59e7de407be428e469c9b519472a7m2_m58376953 (_e7a59e7de407be428e469c9b519472a7_t2685196953 * __this, int32_t ____e7a59e7de407be428e469c9b519472a7a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e7a59e7de407be428e469c9b519472a7::_e7a59e7de407be428e469c9b519472a7m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _e7a59e7de407be428e469c9b519472a7__e7a59e7de407be428e469c9b519472a7m_m3139579869 (_e7a59e7de407be428e469c9b519472a7_t2685196953 * __this, int32_t ____e7a59e7de407be428e469c9b519472a7a0, int32_t ____e7a59e7de407be428e469c9b519472a7941, int32_t ____e7a59e7de407be428e469c9b519472a7c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e7a59e7de407be428e469c9b519472a7::ilo__e7a59e7de407be428e469c9b519472a7m21(Little._e7a59e7de407be428e469c9b519472a7,System.Int32)
extern "C"  int32_t _e7a59e7de407be428e469c9b519472a7_ilo__e7a59e7de407be428e469c9b519472a7m21_m61677184 (Il2CppObject * __this /* static, unused */, _e7a59e7de407be428e469c9b519472a7_t2685196953 * ____this0, int32_t ____e7a59e7de407be428e469c9b519472a7a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
