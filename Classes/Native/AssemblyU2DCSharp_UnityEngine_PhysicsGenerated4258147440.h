﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]>
struct DGetV_1_t406302184;
// JSDataExchangeMgr/DGetV`1<UnityEngine.Collider[]>
struct DGetV_1_t2574801974;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine_PhysicsGenerated
struct  UnityEngine_PhysicsGenerated_t4258147440  : public Il2CppObject
{
public:

public:
};

struct UnityEngine_PhysicsGenerated_t4258147440_StaticFields
{
public:
	// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]> UnityEngine_PhysicsGenerated::<>f__am$cache0
	DGetV_1_t406302184 * ___U3CU3Ef__amU24cache0_0;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]> UnityEngine_PhysicsGenerated::<>f__am$cache1
	DGetV_1_t406302184 * ___U3CU3Ef__amU24cache1_1;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]> UnityEngine_PhysicsGenerated::<>f__am$cache2
	DGetV_1_t406302184 * ___U3CU3Ef__amU24cache2_2;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]> UnityEngine_PhysicsGenerated::<>f__am$cache3
	DGetV_1_t406302184 * ___U3CU3Ef__amU24cache3_3;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]> UnityEngine_PhysicsGenerated::<>f__am$cache4
	DGetV_1_t406302184 * ___U3CU3Ef__amU24cache4_4;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]> UnityEngine_PhysicsGenerated::<>f__am$cache5
	DGetV_1_t406302184 * ___U3CU3Ef__amU24cache5_5;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]> UnityEngine_PhysicsGenerated::<>f__am$cache6
	DGetV_1_t406302184 * ___U3CU3Ef__amU24cache6_6;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]> UnityEngine_PhysicsGenerated::<>f__am$cache7
	DGetV_1_t406302184 * ___U3CU3Ef__amU24cache7_7;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]> UnityEngine_PhysicsGenerated::<>f__am$cache8
	DGetV_1_t406302184 * ___U3CU3Ef__amU24cache8_8;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Collider[]> UnityEngine_PhysicsGenerated::<>f__am$cache9
	DGetV_1_t2574801974 * ___U3CU3Ef__amU24cache9_9;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Collider[]> UnityEngine_PhysicsGenerated::<>f__am$cacheA
	DGetV_1_t2574801974 * ___U3CU3Ef__amU24cacheA_10;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Collider[]> UnityEngine_PhysicsGenerated::<>f__am$cacheB
	DGetV_1_t2574801974 * ___U3CU3Ef__amU24cacheB_11;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Collider[]> UnityEngine_PhysicsGenerated::<>f__am$cacheC
	DGetV_1_t2574801974 * ___U3CU3Ef__amU24cacheC_12;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Collider[]> UnityEngine_PhysicsGenerated::<>f__am$cacheD
	DGetV_1_t2574801974 * ___U3CU3Ef__amU24cacheD_13;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Collider[]> UnityEngine_PhysicsGenerated::<>f__am$cacheE
	DGetV_1_t2574801974 * ___U3CU3Ef__amU24cacheE_14;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Collider[]> UnityEngine_PhysicsGenerated::<>f__am$cacheF
	DGetV_1_t2574801974 * ___U3CU3Ef__amU24cacheF_15;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]> UnityEngine_PhysicsGenerated::<>f__am$cache10
	DGetV_1_t406302184 * ___U3CU3Ef__amU24cache10_16;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]> UnityEngine_PhysicsGenerated::<>f__am$cache11
	DGetV_1_t406302184 * ___U3CU3Ef__amU24cache11_17;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]> UnityEngine_PhysicsGenerated::<>f__am$cache12
	DGetV_1_t406302184 * ___U3CU3Ef__amU24cache12_18;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]> UnityEngine_PhysicsGenerated::<>f__am$cache13
	DGetV_1_t406302184 * ___U3CU3Ef__amU24cache13_19;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]> UnityEngine_PhysicsGenerated::<>f__am$cache14
	DGetV_1_t406302184 * ___U3CU3Ef__amU24cache14_20;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]> UnityEngine_PhysicsGenerated::<>f__am$cache15
	DGetV_1_t406302184 * ___U3CU3Ef__amU24cache15_21;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]> UnityEngine_PhysicsGenerated::<>f__am$cache16
	DGetV_1_t406302184 * ___U3CU3Ef__amU24cache16_22;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]> UnityEngine_PhysicsGenerated::<>f__am$cache17
	DGetV_1_t406302184 * ___U3CU3Ef__amU24cache17_23;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]> UnityEngine_PhysicsGenerated::<>f__am$cache18
	DGetV_1_t406302184 * ___U3CU3Ef__amU24cache18_24;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]> UnityEngine_PhysicsGenerated::<>f__am$cache19
	DGetV_1_t406302184 * ___U3CU3Ef__amU24cache19_25;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]> UnityEngine_PhysicsGenerated::<>f__am$cache1A
	DGetV_1_t406302184 * ___U3CU3Ef__amU24cache1A_26;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]> UnityEngine_PhysicsGenerated::<>f__am$cache1B
	DGetV_1_t406302184 * ___U3CU3Ef__amU24cache1B_27;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]> UnityEngine_PhysicsGenerated::<>f__am$cache1C
	DGetV_1_t406302184 * ___U3CU3Ef__amU24cache1C_28;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]> UnityEngine_PhysicsGenerated::<>f__am$cache1D
	DGetV_1_t406302184 * ___U3CU3Ef__amU24cache1D_29;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]> UnityEngine_PhysicsGenerated::<>f__am$cache1E
	DGetV_1_t406302184 * ___U3CU3Ef__amU24cache1E_30;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.RaycastHit[]> UnityEngine_PhysicsGenerated::<>f__am$cache1F
	DGetV_1_t406302184 * ___U3CU3Ef__amU24cache1F_31;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline DGetV_1_t406302184 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline DGetV_1_t406302184 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(DGetV_1_t406302184 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline DGetV_1_t406302184 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline DGetV_1_t406302184 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(DGetV_1_t406302184 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline DGetV_1_t406302184 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline DGetV_1_t406302184 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(DGetV_1_t406302184 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline DGetV_1_t406302184 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline DGetV_1_t406302184 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(DGetV_1_t406302184 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline DGetV_1_t406302184 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline DGetV_1_t406302184 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(DGetV_1_t406302184 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline DGetV_1_t406302184 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline DGetV_1_t406302184 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(DGetV_1_t406302184 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline DGetV_1_t406302184 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline DGetV_1_t406302184 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(DGetV_1_t406302184 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline DGetV_1_t406302184 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline DGetV_1_t406302184 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(DGetV_1_t406302184 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_8() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache8_8)); }
	inline DGetV_1_t406302184 * get_U3CU3Ef__amU24cache8_8() const { return ___U3CU3Ef__amU24cache8_8; }
	inline DGetV_1_t406302184 ** get_address_of_U3CU3Ef__amU24cache8_8() { return &___U3CU3Ef__amU24cache8_8; }
	inline void set_U3CU3Ef__amU24cache8_8(DGetV_1_t406302184 * value)
	{
		___U3CU3Ef__amU24cache8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_9() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache9_9)); }
	inline DGetV_1_t2574801974 * get_U3CU3Ef__amU24cache9_9() const { return ___U3CU3Ef__amU24cache9_9; }
	inline DGetV_1_t2574801974 ** get_address_of_U3CU3Ef__amU24cache9_9() { return &___U3CU3Ef__amU24cache9_9; }
	inline void set_U3CU3Ef__amU24cache9_9(DGetV_1_t2574801974 * value)
	{
		___U3CU3Ef__amU24cache9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_10() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cacheA_10)); }
	inline DGetV_1_t2574801974 * get_U3CU3Ef__amU24cacheA_10() const { return ___U3CU3Ef__amU24cacheA_10; }
	inline DGetV_1_t2574801974 ** get_address_of_U3CU3Ef__amU24cacheA_10() { return &___U3CU3Ef__amU24cacheA_10; }
	inline void set_U3CU3Ef__amU24cacheA_10(DGetV_1_t2574801974 * value)
	{
		___U3CU3Ef__amU24cacheA_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_11() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cacheB_11)); }
	inline DGetV_1_t2574801974 * get_U3CU3Ef__amU24cacheB_11() const { return ___U3CU3Ef__amU24cacheB_11; }
	inline DGetV_1_t2574801974 ** get_address_of_U3CU3Ef__amU24cacheB_11() { return &___U3CU3Ef__amU24cacheB_11; }
	inline void set_U3CU3Ef__amU24cacheB_11(DGetV_1_t2574801974 * value)
	{
		___U3CU3Ef__amU24cacheB_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_12() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cacheC_12)); }
	inline DGetV_1_t2574801974 * get_U3CU3Ef__amU24cacheC_12() const { return ___U3CU3Ef__amU24cacheC_12; }
	inline DGetV_1_t2574801974 ** get_address_of_U3CU3Ef__amU24cacheC_12() { return &___U3CU3Ef__amU24cacheC_12; }
	inline void set_U3CU3Ef__amU24cacheC_12(DGetV_1_t2574801974 * value)
	{
		___U3CU3Ef__amU24cacheC_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheC_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_13() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cacheD_13)); }
	inline DGetV_1_t2574801974 * get_U3CU3Ef__amU24cacheD_13() const { return ___U3CU3Ef__amU24cacheD_13; }
	inline DGetV_1_t2574801974 ** get_address_of_U3CU3Ef__amU24cacheD_13() { return &___U3CU3Ef__amU24cacheD_13; }
	inline void set_U3CU3Ef__amU24cacheD_13(DGetV_1_t2574801974 * value)
	{
		___U3CU3Ef__amU24cacheD_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheD_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheE_14() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cacheE_14)); }
	inline DGetV_1_t2574801974 * get_U3CU3Ef__amU24cacheE_14() const { return ___U3CU3Ef__amU24cacheE_14; }
	inline DGetV_1_t2574801974 ** get_address_of_U3CU3Ef__amU24cacheE_14() { return &___U3CU3Ef__amU24cacheE_14; }
	inline void set_U3CU3Ef__amU24cacheE_14(DGetV_1_t2574801974 * value)
	{
		___U3CU3Ef__amU24cacheE_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheE_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheF_15() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cacheF_15)); }
	inline DGetV_1_t2574801974 * get_U3CU3Ef__amU24cacheF_15() const { return ___U3CU3Ef__amU24cacheF_15; }
	inline DGetV_1_t2574801974 ** get_address_of_U3CU3Ef__amU24cacheF_15() { return &___U3CU3Ef__amU24cacheF_15; }
	inline void set_U3CU3Ef__amU24cacheF_15(DGetV_1_t2574801974 * value)
	{
		___U3CU3Ef__amU24cacheF_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheF_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache10_16() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache10_16)); }
	inline DGetV_1_t406302184 * get_U3CU3Ef__amU24cache10_16() const { return ___U3CU3Ef__amU24cache10_16; }
	inline DGetV_1_t406302184 ** get_address_of_U3CU3Ef__amU24cache10_16() { return &___U3CU3Ef__amU24cache10_16; }
	inline void set_U3CU3Ef__amU24cache10_16(DGetV_1_t406302184 * value)
	{
		___U3CU3Ef__amU24cache10_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache10_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache11_17() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache11_17)); }
	inline DGetV_1_t406302184 * get_U3CU3Ef__amU24cache11_17() const { return ___U3CU3Ef__amU24cache11_17; }
	inline DGetV_1_t406302184 ** get_address_of_U3CU3Ef__amU24cache11_17() { return &___U3CU3Ef__amU24cache11_17; }
	inline void set_U3CU3Ef__amU24cache11_17(DGetV_1_t406302184 * value)
	{
		___U3CU3Ef__amU24cache11_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache11_17, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache12_18() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache12_18)); }
	inline DGetV_1_t406302184 * get_U3CU3Ef__amU24cache12_18() const { return ___U3CU3Ef__amU24cache12_18; }
	inline DGetV_1_t406302184 ** get_address_of_U3CU3Ef__amU24cache12_18() { return &___U3CU3Ef__amU24cache12_18; }
	inline void set_U3CU3Ef__amU24cache12_18(DGetV_1_t406302184 * value)
	{
		___U3CU3Ef__amU24cache12_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache12_18, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache13_19() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache13_19)); }
	inline DGetV_1_t406302184 * get_U3CU3Ef__amU24cache13_19() const { return ___U3CU3Ef__amU24cache13_19; }
	inline DGetV_1_t406302184 ** get_address_of_U3CU3Ef__amU24cache13_19() { return &___U3CU3Ef__amU24cache13_19; }
	inline void set_U3CU3Ef__amU24cache13_19(DGetV_1_t406302184 * value)
	{
		___U3CU3Ef__amU24cache13_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache13_19, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache14_20() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache14_20)); }
	inline DGetV_1_t406302184 * get_U3CU3Ef__amU24cache14_20() const { return ___U3CU3Ef__amU24cache14_20; }
	inline DGetV_1_t406302184 ** get_address_of_U3CU3Ef__amU24cache14_20() { return &___U3CU3Ef__amU24cache14_20; }
	inline void set_U3CU3Ef__amU24cache14_20(DGetV_1_t406302184 * value)
	{
		___U3CU3Ef__amU24cache14_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache14_20, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache15_21() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache15_21)); }
	inline DGetV_1_t406302184 * get_U3CU3Ef__amU24cache15_21() const { return ___U3CU3Ef__amU24cache15_21; }
	inline DGetV_1_t406302184 ** get_address_of_U3CU3Ef__amU24cache15_21() { return &___U3CU3Ef__amU24cache15_21; }
	inline void set_U3CU3Ef__amU24cache15_21(DGetV_1_t406302184 * value)
	{
		___U3CU3Ef__amU24cache15_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache15_21, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache16_22() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache16_22)); }
	inline DGetV_1_t406302184 * get_U3CU3Ef__amU24cache16_22() const { return ___U3CU3Ef__amU24cache16_22; }
	inline DGetV_1_t406302184 ** get_address_of_U3CU3Ef__amU24cache16_22() { return &___U3CU3Ef__amU24cache16_22; }
	inline void set_U3CU3Ef__amU24cache16_22(DGetV_1_t406302184 * value)
	{
		___U3CU3Ef__amU24cache16_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache16_22, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache17_23() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache17_23)); }
	inline DGetV_1_t406302184 * get_U3CU3Ef__amU24cache17_23() const { return ___U3CU3Ef__amU24cache17_23; }
	inline DGetV_1_t406302184 ** get_address_of_U3CU3Ef__amU24cache17_23() { return &___U3CU3Ef__amU24cache17_23; }
	inline void set_U3CU3Ef__amU24cache17_23(DGetV_1_t406302184 * value)
	{
		___U3CU3Ef__amU24cache17_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache17_23, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache18_24() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache18_24)); }
	inline DGetV_1_t406302184 * get_U3CU3Ef__amU24cache18_24() const { return ___U3CU3Ef__amU24cache18_24; }
	inline DGetV_1_t406302184 ** get_address_of_U3CU3Ef__amU24cache18_24() { return &___U3CU3Ef__amU24cache18_24; }
	inline void set_U3CU3Ef__amU24cache18_24(DGetV_1_t406302184 * value)
	{
		___U3CU3Ef__amU24cache18_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache18_24, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache19_25() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache19_25)); }
	inline DGetV_1_t406302184 * get_U3CU3Ef__amU24cache19_25() const { return ___U3CU3Ef__amU24cache19_25; }
	inline DGetV_1_t406302184 ** get_address_of_U3CU3Ef__amU24cache19_25() { return &___U3CU3Ef__amU24cache19_25; }
	inline void set_U3CU3Ef__amU24cache19_25(DGetV_1_t406302184 * value)
	{
		___U3CU3Ef__amU24cache19_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache19_25, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1A_26() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache1A_26)); }
	inline DGetV_1_t406302184 * get_U3CU3Ef__amU24cache1A_26() const { return ___U3CU3Ef__amU24cache1A_26; }
	inline DGetV_1_t406302184 ** get_address_of_U3CU3Ef__amU24cache1A_26() { return &___U3CU3Ef__amU24cache1A_26; }
	inline void set_U3CU3Ef__amU24cache1A_26(DGetV_1_t406302184 * value)
	{
		___U3CU3Ef__amU24cache1A_26 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1A_26, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1B_27() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache1B_27)); }
	inline DGetV_1_t406302184 * get_U3CU3Ef__amU24cache1B_27() const { return ___U3CU3Ef__amU24cache1B_27; }
	inline DGetV_1_t406302184 ** get_address_of_U3CU3Ef__amU24cache1B_27() { return &___U3CU3Ef__amU24cache1B_27; }
	inline void set_U3CU3Ef__amU24cache1B_27(DGetV_1_t406302184 * value)
	{
		___U3CU3Ef__amU24cache1B_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1B_27, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1C_28() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache1C_28)); }
	inline DGetV_1_t406302184 * get_U3CU3Ef__amU24cache1C_28() const { return ___U3CU3Ef__amU24cache1C_28; }
	inline DGetV_1_t406302184 ** get_address_of_U3CU3Ef__amU24cache1C_28() { return &___U3CU3Ef__amU24cache1C_28; }
	inline void set_U3CU3Ef__amU24cache1C_28(DGetV_1_t406302184 * value)
	{
		___U3CU3Ef__amU24cache1C_28 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1C_28, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1D_29() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache1D_29)); }
	inline DGetV_1_t406302184 * get_U3CU3Ef__amU24cache1D_29() const { return ___U3CU3Ef__amU24cache1D_29; }
	inline DGetV_1_t406302184 ** get_address_of_U3CU3Ef__amU24cache1D_29() { return &___U3CU3Ef__amU24cache1D_29; }
	inline void set_U3CU3Ef__amU24cache1D_29(DGetV_1_t406302184 * value)
	{
		___U3CU3Ef__amU24cache1D_29 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1D_29, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1E_30() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache1E_30)); }
	inline DGetV_1_t406302184 * get_U3CU3Ef__amU24cache1E_30() const { return ___U3CU3Ef__amU24cache1E_30; }
	inline DGetV_1_t406302184 ** get_address_of_U3CU3Ef__amU24cache1E_30() { return &___U3CU3Ef__amU24cache1E_30; }
	inline void set_U3CU3Ef__amU24cache1E_30(DGetV_1_t406302184 * value)
	{
		___U3CU3Ef__amU24cache1E_30 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1E_30, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1F_31() { return static_cast<int32_t>(offsetof(UnityEngine_PhysicsGenerated_t4258147440_StaticFields, ___U3CU3Ef__amU24cache1F_31)); }
	inline DGetV_1_t406302184 * get_U3CU3Ef__amU24cache1F_31() const { return ___U3CU3Ef__amU24cache1F_31; }
	inline DGetV_1_t406302184 ** get_address_of_U3CU3Ef__amU24cache1F_31() { return &___U3CU3Ef__amU24cache1F_31; }
	inline void set_U3CU3Ef__amU24cache1F_31(DGetV_1_t406302184 * value)
	{
		___U3CU3Ef__amU24cache1F_31 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1F_31, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
