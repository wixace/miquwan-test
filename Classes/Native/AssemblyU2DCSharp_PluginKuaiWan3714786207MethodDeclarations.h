﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginKuaiWan
struct PluginKuaiWan_t3714786207;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// VersionInfo
struct VersionInfo_t2356638086;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// Mihua.SDK.SdkwwwCallBack
struct SdkwwwCallBack_t3004559384;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// System.Object
struct Il2CppObject;
// Mihua.SDK.LvUpInfo
struct LvUpInfo_t411687177;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "UnityEngine_UnityEngine_WWWForm461342257.h"
#include "AssemblyU2DCSharp_Mihua_SDK_SdkwwwCallBack3004559384.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PluginKuaiWan3714786207.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginKuaiWan::.ctor()
extern "C"  void PluginKuaiWan__ctor_m3049513132 (PluginKuaiWan_t3714786207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWan::Init()
extern "C"  void PluginKuaiWan_Init_m4067780648 (PluginKuaiWan_t3714786207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginKuaiWan::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginKuaiWan_ReqSDKHttpLogin_m3294778269 (PluginKuaiWan_t3714786207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginKuaiWan::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginKuaiWan_IsLoginSuccess_m3100463679 (PluginKuaiWan_t3714786207 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWan::OpenUserLogin()
extern "C"  void PluginKuaiWan_OpenUserLogin_m4221015806 (PluginKuaiWan_t3714786207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWan::UserPay(CEvent.ZEvent)
extern "C"  void PluginKuaiWan_UserPay_m4087464628 (PluginKuaiWan_t3714786207 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWan::SignCallBack(System.Boolean,System.String)
extern "C"  void PluginKuaiWan_SignCallBack_m1650235213 (PluginKuaiWan_t3714786207 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginKuaiWan::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginKuaiWan_BuildOrderParam2WWWForm_m3392096695 (PluginKuaiWan_t3714786207 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWan::CreateRole(CEvent.ZEvent)
extern "C"  void PluginKuaiWan_CreateRole_m881093585 (PluginKuaiWan_t3714786207 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWan::EnterGame(CEvent.ZEvent)
extern "C"  void PluginKuaiWan_EnterGame_m374978375 (PluginKuaiWan_t3714786207 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWan::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginKuaiWan_RoleUpgrade_m4207395883 (PluginKuaiWan_t3714786207 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWan::OnLoginSuccess(System.String)
extern "C"  void PluginKuaiWan_OnLoginSuccess_m2643250865 (PluginKuaiWan_t3714786207 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWan::OnLogoutSuccess(System.String)
extern "C"  void PluginKuaiWan_OnLogoutSuccess_m339940862 (PluginKuaiWan_t3714786207 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWan::OnLogout(System.String)
extern "C"  void PluginKuaiWan_OnLogout_m3896240897 (PluginKuaiWan_t3714786207 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWan::initSdk(System.String,System.String,System.String)
extern "C"  void PluginKuaiWan_initSdk_m3595773894 (PluginKuaiWan_t3714786207 * __this, String_t* ___gameId0, String_t* ___gameName1, String_t* ___appKey2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWan::login()
extern "C"  void PluginKuaiWan_login_m2571527955 (PluginKuaiWan_t3714786207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWan::logout()
extern "C"  void PluginKuaiWan_logout_m2413777986 (PluginKuaiWan_t3714786207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWan::creatRole(System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginKuaiWan_creatRole_m594520905 (PluginKuaiWan_t3714786207 * __this, String_t* ___server_id0, String_t* ___server_name1, String_t* ___role_id2, String_t* ___role_level3, String_t* ___role_name4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWan::roleUpgrade(System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginKuaiWan_roleUpgrade_m396844930 (PluginKuaiWan_t3714786207 * __this, String_t* ___server_id0, String_t* ___server_name1, String_t* ___role_id2, String_t* ___role_level3, String_t* ___role_name4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWan::pay(System.String,System.String,System.String,System.String)
extern "C"  void PluginKuaiWan_pay_m3816712996 (PluginKuaiWan_t3714786207 * __this, String_t* ___orderId0, String_t* ___productName1, String_t* ___productId2, String_t* ___price3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWan::<OnLogoutSuccess>m__437()
extern "C"  void PluginKuaiWan_U3COnLogoutSuccessU3Em__437_m4012148917 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWan::<OnLogout>m__438()
extern "C"  void PluginKuaiWan_U3COnLogoutU3Em__438_m3987675677 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginKuaiWan::ilo_get_currentVS1(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginKuaiWan_ilo_get_currentVS1_m2880750594 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginKuaiWan::ilo_get_PluginsSdkMgr2()
extern "C"  PluginsSdkMgr_t3884624670 * PluginKuaiWan_ilo_get_PluginsSdkMgr2_m731077747 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginKuaiWan::ilo_get_Instance3()
extern "C"  VersionMgr_t1322950208 * PluginKuaiWan_ilo_get_Instance3_m2396967689 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginKuaiWan::ilo_get_isSdkLogin4(VersionMgr)
extern "C"  bool PluginKuaiWan_ilo_get_isSdkLogin4_m576123549 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWan::ilo_Request5(System.String,UnityEngine.WWWForm,Mihua.SDK.SdkwwwCallBack)
extern "C"  void PluginKuaiWan_ilo_Request5_m993315649 (Il2CppObject * __this /* static, unused */, String_t* ___url0, WWWForm_t461342257 * ___wwwForm1, SdkwwwCallBack_t3004559384 * ___callBack2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginKuaiWan::ilo_ContainsKey6(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  bool PluginKuaiWan_ilo_ContainsKey6_m2757731193 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProductsCfgMgr PluginKuaiWan::ilo_get_ProductsCfgMgr7()
extern "C"  ProductsCfgMgr_t2493714872 * PluginKuaiWan_ilo_get_ProductsCfgMgr7_m2326296498 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWan::ilo_Log8(System.Object,System.Boolean)
extern "C"  void PluginKuaiWan_ilo_Log8_m2857298664 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWan::ilo_creatRole9(PluginKuaiWan,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginKuaiWan_ilo_creatRole9_m2201093594 (Il2CppObject * __this /* static, unused */, PluginKuaiWan_t3714786207 * ____this0, String_t* ___server_id1, String_t* ___server_name2, String_t* ___role_id3, String_t* ___role_level4, String_t* ___role_name5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.LvUpInfo PluginKuaiWan::ilo_Parse10(System.Object[])
extern "C"  LvUpInfo_t411687177 * PluginKuaiWan_ilo_Parse10_m2565387433 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWan::ilo_ReqSDKHttpLogin11(PluginsSdkMgr)
extern "C"  void PluginKuaiWan_ilo_ReqSDKHttpLogin11_m2184133912 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWan::ilo_logout12(PluginKuaiWan)
extern "C"  void PluginKuaiWan_ilo_logout12_m3984695517 (Il2CppObject * __this /* static, unused */, PluginKuaiWan_t3714786207 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWan::ilo_Logout13(System.Action)
extern "C"  void PluginKuaiWan_ilo_Logout13_m3870791206 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
