﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_SocialPlatforms_RangeGenerated
struct UnityEngine_SocialPlatforms_RangeGenerated_t3387770022;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_SocialPlatforms_RangeGenerated::.ctor()
extern "C"  void UnityEngine_SocialPlatforms_RangeGenerated__ctor_m438554293 (UnityEngine_SocialPlatforms_RangeGenerated_t3387770022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SocialPlatforms_RangeGenerated::.cctor()
extern "C"  void UnityEngine_SocialPlatforms_RangeGenerated__cctor_m228184984 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SocialPlatforms_RangeGenerated::Range_Range1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SocialPlatforms_RangeGenerated_Range_Range1_m1858170409 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SocialPlatforms_RangeGenerated::Range_Range2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SocialPlatforms_RangeGenerated_Range_Range2_m3102934890 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SocialPlatforms_RangeGenerated::Range_from(JSVCall)
extern "C"  void UnityEngine_SocialPlatforms_RangeGenerated_Range_from_m1008416108 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SocialPlatforms_RangeGenerated::Range_count(JSVCall)
extern "C"  void UnityEngine_SocialPlatforms_RangeGenerated_Range_count_m1192331015 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SocialPlatforms_RangeGenerated::__Register()
extern "C"  void UnityEngine_SocialPlatforms_RangeGenerated___Register_m3834353458 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SocialPlatforms_RangeGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_SocialPlatforms_RangeGenerated_ilo_attachFinalizerObject1_m1058888394 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_SocialPlatforms_RangeGenerated::ilo_getInt322(System.Int32)
extern "C"  int32_t UnityEngine_SocialPlatforms_RangeGenerated_ilo_getInt322_m2998548029 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SocialPlatforms_RangeGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void UnityEngine_SocialPlatforms_RangeGenerated_ilo_addJSCSRel3_m2527901683 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
