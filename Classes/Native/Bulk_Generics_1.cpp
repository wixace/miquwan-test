﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Newtonsoft.Json.Utilities.EnumValues`1<System.UInt64>
struct EnumValues_1_t713366133;
// System.String
struct String_t;
// Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>
struct EnumValue_1_t3851137816;
// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateDefaultConstructor>c__AnonStorey142`1<System.Object>
struct U3CCreateDefaultConstructorU3Ec__AnonStorey142_1_t983517794;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateGet>c__AnonStorey143`1<System.Object>
struct U3CCreateGetU3Ec__AnonStorey143_1_t406276174;
// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateGet>c__AnonStorey144`1<System.Object>
struct U3CCreateGetU3Ec__AnonStorey144_1_t4256218471;
// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateMethodCall>c__AnonStorey141`1<System.Object>
struct U3CCreateMethodCallU3Ec__AnonStorey141_1_t1149710483;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateSet>c__AnonStorey145`1<System.Object>
struct U3CCreateSetU3Ec__AnonStorey145_1_t2404806100;
// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateSet>c__AnonStorey146`1<System.Object>
struct U3CCreateSetU3Ec__AnonStorey146_1_t1959781101;
// Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>
struct ListWrapper_1_t267912380;
// System.Collections.IList
struct IList_t1751339649;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t2570496278;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>
struct MethodCall_2_t1809280638;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14B`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_t3536942057;
// Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14B`1<System.Object>
struct U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_t1468122155;
// Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14C`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_t3091917058;
// Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14C`1<System.Object>
struct U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_t1023097156;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>
struct ThreadSafeStore_2_t648438005;
// System.Func`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>
struct Func_2_t2253398629;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>
struct ThreadSafeStore_2_t3745804690;
// System.Func`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>
struct Func_2_t1055798018;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Object,System.Object>
struct ThreadSafeStore_2_t2874570697;
// System.Func`2<System.Object,System.Object>
struct Func_2_t184564025;
// Pathfinding.Path
struct Path_t1974241691;
// Pathfinding.Poly2Tri.FixedArray3`1/<Enumerate>c__Iterator0<System.Object>
struct U3CEnumerateU3Ec__Iterator0_t181911054;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3176762032;
// Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.DateTime>
struct WriteDelegate_1_t4036178619;
// Pathfinding.Serialization.JsonFx.JsonWriter
struct JsonWriter_t541860733;
// Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.Object>
struct WriteDelegate_1_t3923333663;
// System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>
struct List_1_t399344435;
// System.Collections.Generic.List`1<Pathfinding.Int2>
struct List_1_t3342231145;
// System.Collections.Generic.List`1<Pathfinding.Int3>
struct List_1_t3342231146;
// System.Collections.Generic.List`1<Pathfinding.IntRect>
struct List_1_t88276517;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t1844984270;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1244034627;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1365137228;
// System.Collections.Generic.List`1<System.UInt32>
struct List_1_t1392853533;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t2974409999;
// Pomelo.DotNetClient.ObjPool`1<System.Object>
struct ObjPool_1_t286528818;
// ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2E`1<System.Object>
struct U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907;
// ProtoBuf.Meta.TypeModel/DeserializeItemsIterator`1<System.Object>
struct DeserializeItemsIterator_1_t2272739822;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.IO.Stream
struct Stream_t1561764144;
// ProtoBuf.SerializationContext
struct SerializationContext_t3997850667;
// SharpKit.JavaScript.JsFunc`3<System.Object,System.Object,System.Object>
struct JsFunc_3_t2543207488;
// Singleton`1<System.Object>
struct Singleton_1_t128664468;
// System.Action`1<Pomelo.DotNetClient.NetWorkState>
struct Action_1_t3724939911;
// System.Action`1<System.Boolean>
struct Action_1_t872614854;
// System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Action_1_t2340485113;
// System.Action`1<System.Int32>
struct Action_1_t1549654636;
// System.Action`1<System.Object>
struct Action_1_t271665211;
// System.Action`1<UnityEngine.Bounds>
struct Action_1_t3107457985;
// System.Action`2<System.Int32,System.Int32>
struct Action_2_t3398277931;
// System.Action`2<System.Object,System.Boolean>
struct Action_2_t599046810;
// System.Action`2<System.Object,System.Object>
struct Action_2_t4293064463;
// System.Action`2<System.Object,System.Single>
struct Action_2_t119199768;
// System.Action`2<System.Single,System.Single>
struct Action_2_t1327107275;
// System.Action`3<System.Boolean,System.Object,System.Int32>
struct Action_3_t1721707197;
// System.Action`3<System.Int32,System.Object,UnityEngine.Vector3>
struct Action_3_t3896231253;
// System.Action`3<System.Object,LogLevel,System.Boolean>
struct Action_3_t1023436963;
// System.Action`3<System.Object,System.Int32,System.Int32>
struct Action_3_t2073482325;
// System.Action`3<System.Object,System.Object,System.Object>
struct Action_3_t2968268857;
// System.Action`3<System.Object,UnityEngine.Vector3,System.Object>
struct Action_3_t2075012986;
// System.Action`3<System.Single,System.Single,System.Single>
struct Action_3_t563912982;
// System.Action`4<System.Int32,System.Object,FightEnum.EDamageType,System.Int32>
struct Action_4_t1491902808;
// System.Action`4<System.Int32,System.Object,UnityEngine.Vector3,System.Object>
struct Action_4_t3253885269;
// System.Action`4<System.Object,System.Int32,System.Object,System.Single>
struct Action_4_t2668625267;
// System.Action`4<System.Object,System.Object,System.Object,System.Object>
struct Action_4_t1730810465;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator0_t3206263253;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t2095059871;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t2336740304;
// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t1756209413;
// System.Exception
struct Exception_t3991598821;
// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>
struct ArrayReadOnlyList_1_t645006031;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1983528240;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t676510742;
// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>
struct ArrayReadOnlyList_1_t886686464;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t2088020251;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t918191175;
// System.Array
struct Il2CppArray;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_EnumVal713366133.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_EnumVal713366133MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Collections_ObjectModel_KeyedColle2366576526MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_EnumVa3851137816.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_EnumVa3851137816MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_LateBou983517794.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_LateBou983517794MethodDeclarations.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Reflec3905956676MethodDeclarations.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Reflection_ConstructorInfo4136801618MethodDeclarations.h"
#include "mscorlib_System_Reflection_ConstructorInfo4136801618.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_LateBou406276174.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_LateBou406276174MethodDeclarations.h"
#include "mscorlib_System_Reflection_PropertyInfo924268725.h"
#include "mscorlib_System_Reflection_PropertyInfo924268725MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_LateBo4256218471.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_LateBo4256218471MethodDeclarations.h"
#include "mscorlib_System_Reflection_FieldInfo3973053266.h"
#include "mscorlib_System_Reflection_FieldInfo3973053266MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_LateBo1149710483.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_LateBo1149710483MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodBase318515428MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodBase318515428.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_LateBo2404806100.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_LateBo2404806100MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_LateBo1959781101.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_LateBo1959781101MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_ListWra267912380.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_ListWra267912380MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Valida3410775990MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Collec3035453308MethodDeclarations.h"
#include "mscorlib_System_Int321153838500.h"
#include "AssemblyU2DCSharp___IL_EX_BreakUtil771728311MethodDeclarations.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Single4291918972.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Collec3035453308.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Method1809280638.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Method1809280638MethodDeclarations.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_String3536942057.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_String3536942057MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1690470805.h"
#include "System_Core_System_Func_2_gen1690470805MethodDeclarations.h"
#include "mscorlib_System_StringComparison4173268078.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3312854529.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3312854529MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_String1468122155.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_String1468122155MethodDeclarations.h"
#include "System_Core_System_Func_2_gen315946507.h"
#include "System_Core_System_Func_2_gen315946507MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_String3091917058.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_String3091917058MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_String1023097156.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_String1023097156MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_ThreadS648438005.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_ThreadS648438005MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2253398629.h"
#include "mscorlib_System_ArgumentNullException3573189601MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3573189601.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_De2971844791.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4114722875.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4114722875MethodDeclarations.h"
#include "mscorlib_System_Threading_Monitor2734674376MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2253398629MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Thread3745804690.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Thread3745804690MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1055798018.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Convert866134174.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2917122264.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2917122264MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1055798018MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Thread2874570697.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Thread2874570697MethodDeclarations.h"
#include "System_Core_System_Func_2_gen184564025.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2045888271.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2045888271MethodDeclarations.h"
#include "System_Core_System_Func_2_gen184564025MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_PathPool_1_gen4134265632.h"
#include "AssemblyU2DCSharp_Pathfinding_PathPool_1_gen4134265632MethodDeclarations.h"
#include "System_System_Collections_Generic_Stack_1_gen2974409999.h"
#include "System_System_Collections_Generic_Stack_1_gen2974409999MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ListPool_1_gen2604082720MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ListPool_1_gen2567569620MethodDeclarations.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "mscorlib_System_Activator2714366379MethodDeclarations.h"
#include "mscorlib_System_Activator2714366379.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_FixedArra181911054.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_FixedArra181911054MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked373807572MethodDeclarations.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_FixedArra526322725.h"
#include "mscorlib_System_UInt3224667981.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_FixedArra526322725MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1732551818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1732551818.h"
#include "mscorlib_System_IndexOutOfRangeException3456360697MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3456360697.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF4036178619.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF4036178619MethodDeclarations.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonFx541860733.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF3923333663.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF3923333663MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ListPool_1_gen1611629233.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ListPool_1_gen1611629233MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1767529987.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1767529987MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen399344435.h"
#include "mscorlib_System_Collections_Generic_List_1_gen399344435MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException1589641621MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException1589641621.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ListPool_1_gen259548647.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ListPool_1_gen259548647MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen415449401.h"
#include "mscorlib_System_Collections_Generic_List_1_gen415449401MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3342231145.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3342231145MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ListPool_1_gen259548648.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ListPool_1_gen259548648MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen415449402.h"
#include "mscorlib_System_Collections_Generic_List_1_gen415449402MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3342231146.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3342231146MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ListPool_1_gen1300561315.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ListPool_1_gen1300561315MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1456462069.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1456462069MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen88276517.h"
#include "mscorlib_System_Collections_Generic_List_1_gen88276517MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ListPool_1_gen3057269068.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ListPool_1_gen3057269068MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3213169822.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3213169822MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1844984270.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1844984270MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ListPool_1_gen3734308850.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ListPool_1_gen3734308850MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3890209604.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3890209604MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2522024052.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2522024052MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ListPool_1_gen2456319425.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ListPool_1_gen2456319425MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2612220179.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2612220179MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ListPool_1_gen2577422026.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ListPool_1_gen2577422026MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2733322780.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2733322780MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1365137228.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1365137228MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ListPool_1_gen2605138331.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ListPool_1_gen2605138331MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2761039085.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2761039085MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1392853533.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1392853533MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ListPool_1_gen2567569620.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2723470374.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2723470374MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1355284822.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1355284822MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ObjectPool_1_ge1443416954.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ObjectPool_1_ge1443416954MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_StackPool_1_gen2103378743.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_StackPool_1_gen2103378743MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen47628255.h"
#include "mscorlib_System_Collections_Generic_List_1_gen47628255MethodDeclarations.h"
#include "System_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_ObjPool_1_gen286528818.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_ObjPool_1_gen286528818MethodDeclarations.h"
#include "AssemblyU2DCSharp_ProtoBuf_ExtensibleUtil_U3CGetEx2058630907.h"
#include "AssemblyU2DCSharp_ProtoBuf_ExtensibleUtil_U3CGetEx2058630907MethodDeclarations.h"
#include "AssemblyU2DCSharp_ProtoBuf_DataFormat274207885.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_RuntimeTypeModel242172789MethodDeclarations.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "AssemblyU2DCSharp_ProtoBuf_ExtensibleUtil3052333161MethodDeclarations.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_RuntimeTypeModel242172789.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel_Deserial2272739822.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel_Deserial2272739822MethodDeclarations.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "AssemblyU2DCSharp_ProtoBuf_PrefixStyle1974492709.h"
#include "AssemblyU2DCSharp_ProtoBuf_SerializationContext3997850667.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105MethodDeclarations.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel_Deserial1187605077MethodDeclarations.h"
#include "AssemblyU2DCSharp_ProtoBuf_Serializer_TypeResolver356109658.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel_Deserial1187605077.h"
#include "SharpKit_JavaScript_SharpKit_JavaScript_JsFunc_3_g2543207488.h"
#include "SharpKit_JavaScript_SharpKit_JavaScript_JsFunc_3_g2543207488MethodDeclarations.h"
#include "AssemblyU2DCSharp_Singleton_1_gen128664468.h"
#include "AssemblyU2DCSharp_Singleton_1_gen128664468MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3724939911.h"
#include "mscorlib_System_Action_1_gen3724939911MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_NetWorkState3329123775.h"
#include "mscorlib_System_Action_1_gen872614854.h"
#include "mscorlib_System_Action_1_gen872614854MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2340485113.h"
#include "mscorlib_System_Action_1_gen2340485113MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1549654636.h"
#include "mscorlib_System_Action_1_gen1549654636MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen271665211.h"
#include "mscorlib_System_Action_1_gen271665211MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3107457985.h"
#include "mscorlib_System_Action_1_gen3107457985MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "System_Core_System_Action_2_gen3398277931.h"
#include "System_Core_System_Action_2_gen3398277931MethodDeclarations.h"
#include "System_Core_System_Action_2_gen599046810.h"
#include "System_Core_System_Action_2_gen599046810MethodDeclarations.h"
#include "System_Core_System_Action_2_gen4293064463.h"
#include "System_Core_System_Action_2_gen4293064463MethodDeclarations.h"
#include "System_Core_System_Action_2_gen119199768.h"
#include "System_Core_System_Action_2_gen119199768MethodDeclarations.h"
#include "System_Core_System_Action_2_gen1327107275.h"
#include "System_Core_System_Action_2_gen1327107275MethodDeclarations.h"
#include "System_Core_System_Action_3_gen1721707197.h"
#include "System_Core_System_Action_3_gen1721707197MethodDeclarations.h"
#include "System_Core_System_Action_3_gen3896231253.h"
#include "System_Core_System_Action_3_gen3896231253MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "System_Core_System_Action_3_gen1023436963.h"
#include "System_Core_System_Action_3_gen1023436963MethodDeclarations.h"
#include "AssemblyU2DCSharp_LogLevel2060375744.h"
#include "System_Core_System_Action_3_gen2073482325.h"
#include "System_Core_System_Action_3_gen2073482325MethodDeclarations.h"
#include "System_Core_System_Action_3_gen2968268857.h"
#include "System_Core_System_Action_3_gen2968268857MethodDeclarations.h"
#include "System_Core_System_Action_3_gen2075012986.h"
#include "System_Core_System_Action_3_gen2075012986MethodDeclarations.h"
#include "System_Core_System_Action_3_gen563912982.h"
#include "System_Core_System_Action_3_gen563912982MethodDeclarations.h"
#include "System_Core_System_Action_4_gen1491902808.h"
#include "System_Core_System_Action_4_gen1491902808MethodDeclarations.h"
#include "AssemblyU2DCSharp_FightEnum_EDamageType2535357340.h"
#include "System_Core_System_Action_4_gen3253885269.h"
#include "System_Core_System_Action_4_gen3253885269MethodDeclarations.h"
#include "System_Core_System_Action_4_gen2668625267.h"
#include "System_Core_System_Action_4_gen2668625267MethodDeclarations.h"
#include "System_Core_System_Action_4_gen1730810465.h"
#include "System_Core_System_Action_4_gen1730810465MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn3206263253.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn3206263253MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen1756209413.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2095059871.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2095059871MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArg3059612989.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen645006031.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2336740304.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2336740304MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg3301293422.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen886686464.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen1756209413MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3816648464MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3816648464.h"
#include "mscorlib_System_Exception3991598821.h"
#include "mscorlib_System_Array1146569071MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen645006031MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen886686464MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2678631987.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2678631987MethodDeclarations.h"
#include "AssemblyU2DCSharp_AIEnum_EAIEventtype3896289311.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4083548623.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4083548623MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniType1006238651.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4068395185.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4068395185MethodDeclarations.h"
#include "AssemblyU2DCSharp_AstarDebugger_GraphPoint991085213.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2810415885.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2810415885MethodDeclarations.h"
#include "AssemblyU2DCSharp_AstarDebugger_PathTypeDebug4028073209.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1349036564.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1349036564MethodDeclarations.h"
#include "AssemblyU2DCSharp_AstarPath_AstarWorkItem2566693888.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2439682662.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2439682662MethodDeclarations.h"
#include "AssemblyU2DCSharp_AstarPath_GUOSingle3657339986.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3130107104.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3130107104MethodDeclarations.h"
#include "AssemblyU2DCSharp_Core_RpsChoice52797132.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3556904796.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3556904796MethodDeclarations.h"
#include "AssemblyU2DCSharp_Core_RpsResult479594824.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen20875812.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen20875812MethodDeclarations.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3543166237.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3543166237MethodDeclarations.h"
#include "AssemblyU2DCSharp_FightEnum_ESkillBuffEvent465856265.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1224156876.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1224156876MethodDeclarations.h"
#include "AssemblyU2DCSharp_FightEnum_ESkillBuffTarget2441814200.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen397877.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen397877MethodDeclarations.h"
#include "AssemblyU2DCSharp_FingerGestures_SwipeDirection1218055201.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen981688446.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen981688446MethodDeclarations.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2208288086.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2208288086MethodDeclarations.h"
#include "AssemblyU2DCSharp_HatredCtrl_stValue3425945410.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1416323886.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1416323886MethodDeclarations.h"
#include "AssemblyU2DCSharp_Mihua_Assets_SubAssetMgr_ErrorIn2633981210.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2155190829.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2155190829MethodDeclarations.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndex3372848153.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1949385224.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1949385224MethodDeclarations.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3167042548.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen798349321.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen798349321MethodDeclarations.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_TagName2016006645.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2257284226.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2257284226MethodDeclarations.h"
#include "AssemblyU2DCSharp_MScrollView_MoveWay3474941550.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3749301894.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3749301894MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter_State671991922.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2699240237.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2699240237MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType3916897561.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen897758497.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen897758497MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem2115415821.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1754187467.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1754187467MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1674672166.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1674672166MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js2892329490.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3943444146.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3943444146MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3542505350.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3542505350MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_AdvancedSmooth_Turn465195378.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen740738874.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen740738874MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_BBTree_BBTreeBox1958396198.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1298039651.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1298039651MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_BinaryHeapM_Tuple2515696975.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2108468855.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2108468855MethodDeclarations.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_IntP3326126179.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2017522227.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2017522227MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_GridGraph_TextureDat3235179551.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen756388269.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen756388269MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen756388270.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen756388270MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1797400937.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1797400937MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_IntRect3015058261.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1603662595.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1603662595MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_Inter2821319919.h"

// !!0 System.Activator::CreateInstance<System.Object>()
extern "C"  Il2CppObject * Activator_CreateInstance_TisIl2CppObject_m4157987083_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisIl2CppObject_m4157987083(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisIl2CppObject_m4157987083_gshared)(__this /* static, unused */, method)
// System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisIl2CppObject_m2661005505_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* p0, Il2CppObject * p1, const MethodInfo* method);
#define Array_IndexOf_TisIl2CppObject_m2661005505(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1108656482*, Il2CppObject *, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeNamedArgument_t3059612989_m3345236030_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t1983528240* p0, CustomAttributeNamedArgument_t3059612989  p1, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeNamedArgument_t3059612989_m3345236030(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t1983528240*, CustomAttributeNamedArgument_t3059612989 , const MethodInfo*))Array_IndexOf_TisCustomAttributeNamedArgument_t3059612989_m3345236030_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeTypedArgument_t3301293422_m858079087_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2088020251* p0, CustomAttributeTypedArgument_t3301293422  p1, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeTypedArgument_t3301293422_m858079087(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2088020251*, CustomAttributeTypedArgument_t3301293422 , const MethodInfo*))Array_IndexOf_TisCustomAttributeTypedArgument_t3301293422_m858079087_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Array::InternalArray__get_Item<AIEnum.EAIEventtype>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisEAIEventtype_t3896289311_m503381778_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisEAIEventtype_t3896289311_m503381778(__this, p0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisEAIEventtype_t3896289311_m503381778_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<AnimationRunner/AniType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisAniType_t1006238651_m2022647075_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisAniType_t1006238651_m2022647075(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisAniType_t1006238651_m2022647075_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<AstarDebugger/GraphPoint>(System.Int32)
extern "C"  GraphPoint_t991085213  Array_InternalArray__get_Item_TisGraphPoint_t991085213_m2582433893_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGraphPoint_t991085213_m2582433893(__this, p0, method) ((  GraphPoint_t991085213  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGraphPoint_t991085213_m2582433893_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<AstarDebugger/PathTypeDebug>(System.Int32)
extern "C"  PathTypeDebug_t4028073209  Array_InternalArray__get_Item_TisPathTypeDebug_t4028073209_m3688214821_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPathTypeDebug_t4028073209_m3688214821(__this, p0, method) ((  PathTypeDebug_t4028073209  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPathTypeDebug_t4028073209_m3688214821_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<AstarPath/AstarWorkItem>(System.Int32)
extern "C"  AstarWorkItem_t2566693888  Array_InternalArray__get_Item_TisAstarWorkItem_t2566693888_m3174582846_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisAstarWorkItem_t2566693888_m3174582846(__this, p0, method) ((  AstarWorkItem_t2566693888  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisAstarWorkItem_t2566693888_m3174582846_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<AstarPath/GUOSingle>(System.Int32)
extern "C"  GUOSingle_t3657339986  Array_InternalArray__get_Item_TisGUOSingle_t3657339986_m137982636_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGUOSingle_t3657339986_m137982636(__this, p0, method) ((  GUOSingle_t3657339986  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGUOSingle_t3657339986_m137982636_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Core.RpsChoice>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisRpsChoice_t52797132_m2357716955_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRpsChoice_t52797132_m2357716955(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRpsChoice_t52797132_m2357716955_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Core.RpsResult>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisRpsResult_t479594824_m228439519_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRpsResult_t479594824_m228439519(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRpsResult_t479594824_m228439519_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Entity.Behavior.EBehaviorID>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisEBehaviorID_t1238533136_m600950909_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisEBehaviorID_t1238533136_m600950909(__this, p0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisEBehaviorID_t1238533136_m600950909_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<FightEnum.ESkillBuffEvent>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisESkillBuffEvent_t465856265_m555903872_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisESkillBuffEvent_t465856265_m555903872(__this, p0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisESkillBuffEvent_t465856265_m555903872_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<FightEnum.ESkillBuffTarget>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisESkillBuffTarget_t2441814200_m296978261_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisESkillBuffTarget_t2441814200_m296978261(__this, p0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisESkillBuffTarget_t2441814200_m296978261_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<FingerGestures/SwipeDirection>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisSwipeDirection_t1218055201_m227475837_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSwipeDirection_t1218055201_m227475837(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSwipeDirection_t1218055201_m227475837_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<FLOAT_TEXT_ID>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisFLOAT_TEXT_ID_t2199345770_m1403278356_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisFLOAT_TEXT_ID_t2199345770_m1403278356(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisFLOAT_TEXT_ID_t2199345770_m1403278356_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<HatredCtrl/stValue>(System.Int32)
extern "C"  stValue_t3425945410  Array_InternalArray__get_Item_TisstValue_t3425945410_m1854423712_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisstValue_t3425945410_m1854423712(__this, p0, method) ((  stValue_t3425945410  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisstValue_t3425945410_m1854423712_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mihua.Assets.SubAssetMgr/ErrorInfo>(System.Int32)
extern "C"  ErrorInfo_t2633981210  Array_InternalArray__get_Item_TisErrorInfo_t2633981210_m3856784840_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisErrorInfo_t2633981210_m3856784840(__this, p0, method) ((  ErrorInfo_t2633981210  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisErrorInfo_t2633981210_m3856784840_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32)
extern "C"  TableRange_t3372848153  Array_InternalArray__get_Item_TisTableRange_t3372848153_m3354422249_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTableRange_t3372848153_m3354422249(__this, p0, method) ((  TableRange_t3372848153  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTableRange_t3372848153_m3354422249_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisClientCertificateType_t3167042548_m115939321_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisClientCertificateType_t3167042548_m115939321(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisClientCertificateType_t3167042548_m115939321_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Xml2.XmlTextReader/TagName>(System.Int32)
extern "C"  TagName_t2016006645  Array_InternalArray__get_Item_TisTagName_t2016006645_m2189903273_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTagName_t2016006645_m2189903273(__this, p0, method) ((  TagName_t2016006645  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTagName_t2016006645_m2189903273_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<MScrollView/MoveWay>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisMoveWay_t3474941550_m2198775056_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisMoveWay_t3474941550_m2198775056(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisMoveWay_t3474941550_m2198775056_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Newtonsoft.Json.JsonWriter/State>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisState_t671991922_m806392816_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisState_t671991922_m806392816(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisState_t671991922_m806392816_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Newtonsoft.Json.Linq.JTokenType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisJTokenType_t3916897561_m3146714076_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisJTokenType_t3916897561_m3146714076(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisJTokenType_t3916897561_m3146714076_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Newtonsoft.Json.Schema.JsonSchemaType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisJsonSchemaType_t2115415821_m826810179_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisJsonSchemaType_t2115415821_m826810179(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisJsonSchemaType_t2115415821_m826810179_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>(System.Int32)
extern "C"  TypeNameKey_t2971844791  Array_InternalArray__get_Item_TisTypeNameKey_t2971844791_m94980107_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTypeNameKey_t2971844791_m94980107(__this, p0, method) ((  TypeNameKey_t2971844791  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTypeNameKey_t2971844791_m94980107_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisPropertyPresence_t2892329490_m702118636_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPropertyPresence_t2892329490_m702118636(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPropertyPresence_t2892329490_m702118636_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>(System.Int32)
extern "C"  TypeConvertKey_t866134174  Array_InternalArray__get_Item_TisTypeConvertKey_t866134174_m2084540768_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTypeConvertKey_t866134174_m2084540768(__this, p0, method) ((  TypeConvertKey_t866134174  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTypeConvertKey_t866134174_m2084540768_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Pathfinding.AdvancedSmooth/Turn>(System.Int32)
extern "C"  Turn_t465195378  Array_InternalArray__get_Item_TisTurn_t465195378_m3460447372_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTurn_t465195378_m3460447372(__this, p0, method) ((  Turn_t465195378  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTurn_t465195378_m3460447372_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Pathfinding.BBTree/BBTreeBox>(System.Int32)
extern "C"  BBTreeBox_t1958396198  Array_InternalArray__get_Item_TisBBTreeBox_t1958396198_m689206716_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisBBTreeBox_t1958396198_m689206716(__this, p0, method) ((  BBTreeBox_t1958396198  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisBBTreeBox_t1958396198_m689206716_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Pathfinding.BinaryHeapM/Tuple>(System.Int32)
extern "C"  Tuple_t2515696975  Array_InternalArray__get_Item_TisTuple_t2515696975_m1348226703_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTuple_t2515696975_m1348226703(__this, p0, method) ((  Tuple_t2515696975  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTuple_t2515696975_m1348226703_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Pathfinding.ClipperLib.IntPoint>(System.Int32)
extern "C"  IntPoint_t3326126179  Array_InternalArray__get_Item_TisIntPoint_t3326126179_m4245689681_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisIntPoint_t3326126179_m4245689681(__this, p0, method) ((  IntPoint_t3326126179  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisIntPoint_t3326126179_m4245689681_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Pathfinding.GridGraph/TextureData/ChannelUse>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisChannelUse_t3235179551_m2898097571_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisChannelUse_t3235179551_m2898097571(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisChannelUse_t3235179551_m2898097571_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Pathfinding.Int2>(System.Int32)
extern "C"  Int2_t1974045593  Array_InternalArray__get_Item_TisInt2_t1974045593_m594284725_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt2_t1974045593_m594284725(__this, p0, method) ((  Int2_t1974045593  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt2_t1974045593_m594284725_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Pathfinding.Int3>(System.Int32)
extern "C"  Int3_t1974045594  Array_InternalArray__get_Item_TisInt3_t1974045594_m83750548_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt3_t1974045594_m83750548(__this, p0, method) ((  Int3_t1974045594  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt3_t1974045594_m83750548_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Pathfinding.IntRect>(System.Int32)
extern "C"  IntRect_t3015058261  Array_InternalArray__get_Item_TisIntRect_t3015058261_m866782229_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisIntRect_t3015058261_m866782229(__this, p0, method) ((  IntRect_t3015058261  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisIntRect_t3015058261_m866782229_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Pathfinding.LocalAvoidance/IntersectionPair>(System.Int32)
extern "C"  IntersectionPair_t2821319919  Array_InternalArray__get_Item_TisIntersectionPair_t2821319919_m173310575_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisIntersectionPair_t2821319919_m173310575(__this, p0, method) ((  IntersectionPair_t2821319919  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisIntersectionPair_t2821319919_m173310575_gshared)(__this, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Newtonsoft.Json.Utilities.EnumValues`1<System.UInt64>::.ctor()
extern "C"  void EnumValues_1__ctor_m1471327349_gshared (EnumValues_1_t713366133 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyedCollection_2_t2366576526 *)__this);
		((  void (*) (KeyedCollection_2_t2366576526 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((KeyedCollection_2_t2366576526 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.String Newtonsoft.Json.Utilities.EnumValues`1<System.UInt64>::GetKeyForItem(Newtonsoft.Json.Utilities.EnumValue`1<T>)
extern "C"  String_t* EnumValues_1_GetKeyForItem_m2282326315_gshared (EnumValues_1_t713366133 * __this, EnumValue_1_t3851137816 * ___item0, const MethodInfo* method)
{
	{
		EnumValue_1_t3851137816 * L_0 = ___item0;
		NullCheck((EnumValue_1_t3851137816 *)L_0);
		String_t* L_1 = ((  String_t* (*) (EnumValue_1_t3851137816 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((EnumValue_1_t3851137816 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_1;
	}
}
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateDefaultConstructor>c__AnonStorey142`1<System.Object>::.ctor()
extern "C"  void U3CCreateDefaultConstructorU3Ec__AnonStorey142_1__ctor_m960944953_gshared (U3CCreateDefaultConstructorU3Ec__AnonStorey142_1_t983517794 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateDefaultConstructor>c__AnonStorey142`1<System.Object>::<>m__3A8()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateDefaultConstructorU3Ec__AnonStorey142_1_U3CU3Em__3A8_m2535618409_MetadataUsageId;
extern "C"  Il2CppObject * U3CCreateDefaultConstructorU3Ec__AnonStorey142_1_U3CU3Em__3A8_m2535618409_gshared (U3CCreateDefaultConstructorU3Ec__AnonStorey142_1_t983517794 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateDefaultConstructorU3Ec__AnonStorey142_1_U3CU3Em__3A8_m2535618409_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = (Type_t *)__this->get_type_0();
		Il2CppObject * L_1 = ReflectionUtils_CreateInstance_m2555227011(NULL /*static, unused*/, (Type_t *)L_0, (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
	}
}
// T Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateDefaultConstructor>c__AnonStorey142`1<System.Object>::<>m__3A9()
extern "C"  Il2CppObject * U3CCreateDefaultConstructorU3Ec__AnonStorey142_1_U3CU3Em__3A9_m2535619370_gshared (U3CCreateDefaultConstructorU3Ec__AnonStorey142_1_t983517794 * __this, const MethodInfo* method)
{
	{
		ConstructorInfo_t4136801618 * L_0 = (ConstructorInfo_t4136801618 *)__this->get_constructorInfo_1();
		NullCheck((ConstructorInfo_t4136801618 *)L_0);
		Il2CppObject * L_1 = ConstructorInfo_Invoke_m759007899((ConstructorInfo_t4136801618 *)L_0, (ObjectU5BU5D_t1108656482*)(ObjectU5BU5D_t1108656482*)NULL, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
	}
}
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateGet>c__AnonStorey143`1<System.Object>::.ctor()
extern "C"  void U3CCreateGetU3Ec__AnonStorey143_1__ctor_m3981334997_gshared (U3CCreateGetU3Ec__AnonStorey143_1_t406276174 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateGet>c__AnonStorey143`1<System.Object>::<>m__3AA(T)
extern "C"  Il2CppObject * U3CCreateGetU3Ec__AnonStorey143_1_U3CU3Em__3AA_m3471393308_gshared (U3CCreateGetU3Ec__AnonStorey143_1_t406276174 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	{
		PropertyInfo_t * L_0 = (PropertyInfo_t *)__this->get_propertyInfo_0();
		Il2CppObject * L_1 = ___o0;
		NullCheck((PropertyInfo_t *)L_0);
		Il2CppObject * L_2 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t1108656482* >::Invoke(24 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, (PropertyInfo_t *)L_0, (Il2CppObject *)L_1, (ObjectU5BU5D_t1108656482*)(ObjectU5BU5D_t1108656482*)NULL);
		return L_2;
	}
}
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateGet>c__AnonStorey144`1<System.Object>::.ctor()
extern "C"  void U3CCreateGetU3Ec__AnonStorey144_1__ctor_m647981718_gshared (U3CCreateGetU3Ec__AnonStorey144_1_t4256218471 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateGet>c__AnonStorey144`1<System.Object>::<>m__3AB(T)
extern "C"  Il2CppObject * U3CCreateGetU3Ec__AnonStorey144_1_U3CU3Em__3AB_m3822223036_gshared (U3CCreateGetU3Ec__AnonStorey144_1_t4256218471 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	{
		FieldInfo_t * L_0 = (FieldInfo_t *)__this->get_fieldInfo_0();
		Il2CppObject * L_1 = ___o0;
		NullCheck((FieldInfo_t *)L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(17 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, (FieldInfo_t *)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateMethodCall>c__AnonStorey141`1<System.Object>::.ctor()
extern "C"  void U3CCreateMethodCallU3Ec__AnonStorey141_1__ctor_m1978002130_gshared (U3CCreateMethodCallU3Ec__AnonStorey141_1_t1149710483 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateMethodCall>c__AnonStorey141`1<System.Object>::<>m__3A6(T,System.Object[])
extern "C"  Il2CppObject * U3CCreateMethodCallU3Ec__AnonStorey141_1_U3CU3Em__3A6_m3580767078_gshared (U3CCreateMethodCallU3Ec__AnonStorey141_1_t1149710483 * __this, Il2CppObject * ___o0, ObjectU5BU5D_t1108656482* ___a1, const MethodInfo* method)
{
	{
		ConstructorInfo_t4136801618 * L_0 = (ConstructorInfo_t4136801618 *)__this->get_c_0();
		ObjectU5BU5D_t1108656482* L_1 = ___a1;
		NullCheck((ConstructorInfo_t4136801618 *)L_0);
		Il2CppObject * L_2 = ConstructorInfo_Invoke_m759007899((ConstructorInfo_t4136801618 *)L_0, (ObjectU5BU5D_t1108656482*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateMethodCall>c__AnonStorey141`1<System.Object>::<>m__3A7(T,System.Object[])
extern "C"  Il2CppObject * U3CCreateMethodCallU3Ec__AnonStorey141_1_U3CU3Em__3A7_m3171942853_gshared (U3CCreateMethodCallU3Ec__AnonStorey141_1_t1149710483 * __this, Il2CppObject * ___o0, ObjectU5BU5D_t1108656482* ___a1, const MethodInfo* method)
{
	{
		MethodBase_t318515428 * L_0 = (MethodBase_t318515428 *)__this->get_method_1();
		Il2CppObject * L_1 = ___o0;
		ObjectU5BU5D_t1108656482* L_2 = ___a1;
		NullCheck((MethodBase_t318515428 *)L_0);
		Il2CppObject * L_3 = MethodBase_Invoke_m3435166155((MethodBase_t318515428 *)L_0, (Il2CppObject *)L_1, (ObjectU5BU5D_t1108656482*)L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateSet>c__AnonStorey145`1<System.Object>::.ctor()
extern "C"  void U3CCreateSetU3Ec__AnonStorey145_1__ctor_m294330059_gshared (U3CCreateSetU3Ec__AnonStorey145_1_t2404806100 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateSet>c__AnonStorey145`1<System.Object>::<>m__3AC(T,System.Object)
extern "C"  void U3CCreateSetU3Ec__AnonStorey145_1_U3CU3Em__3AC_m806484329_gshared (U3CCreateSetU3Ec__AnonStorey145_1_t2404806100 * __this, Il2CppObject * ___o0, Il2CppObject * ___v1, const MethodInfo* method)
{
	{
		FieldInfo_t * L_0 = (FieldInfo_t *)__this->get_fieldInfo_0();
		Il2CppObject * L_1 = ___o0;
		Il2CppObject * L_2 = ___v1;
		NullCheck((FieldInfo_t *)L_0);
		FieldInfo_SetValue_m1669444927((FieldInfo_t *)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateSet>c__AnonStorey146`1<System.Object>::.ctor()
extern "C"  void U3CCreateSetU3Ec__AnonStorey146_1__ctor_m1255944076_gshared (U3CCreateSetU3Ec__AnonStorey146_1_t1959781101 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateSet>c__AnonStorey146`1<System.Object>::<>m__3AD(T,System.Object)
extern "C"  void U3CCreateSetU3Ec__AnonStorey146_1_U3CU3Em__3AD_m2403638793_gshared (U3CCreateSetU3Ec__AnonStorey146_1_t1959781101 * __this, Il2CppObject * ___o0, Il2CppObject * ___v1, const MethodInfo* method)
{
	{
		PropertyInfo_t * L_0 = (PropertyInfo_t *)__this->get_propertyInfo_0();
		Il2CppObject * L_1 = ___o0;
		Il2CppObject * L_2 = ___v1;
		NullCheck((PropertyInfo_t *)L_0);
		VirtActionInvoker3< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t1108656482* >::Invoke(26 /* System.Void System.Reflection.PropertyInfo::SetValue(System.Object,System.Object,System.Object[]) */, (PropertyInfo_t *)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2, (ObjectU5BU5D_t1108656482*)(ObjectU5BU5D_t1108656482*)NULL);
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::.ctor(System.Collections.IList)
extern Il2CppCodeGenString* _stringLiteral3322014;
extern const uint32_t ListWrapper_1__ctor_m3345849629_MetadataUsageId;
extern "C"  void ListWrapper_1__ctor_m3345849629_gshared (ListWrapper_1_t267912380 * __this, Il2CppObject * ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListWrapper_1__ctor_m3345849629_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___list0;
		NullCheck((CollectionWrapper_1_t3035453308 *)__this);
		((  void (*) (CollectionWrapper_1_t3035453308 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((CollectionWrapper_1_t3035453308 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Il2CppObject * L_1 = ___list0;
		ValidationUtils_ArgumentNotNull_m3978868425(NULL /*static, unused*/, (Il2CppObject *)L_1, (String_t*)_stringLiteral3322014, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___list0;
		if (!((Il2CppObject*)IsInst(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2))))
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject * L_3 = ___list0;
		__this->set__genericList_3(((Il2CppObject*)Castclass(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2))));
	}

IL_0029:
	{
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::.ctor(System.Collections.Generic.IList`1<T>)
extern Il2CppCodeGenString* _stringLiteral3322014;
extern const uint32_t ListWrapper_1__ctor_m1463683433_MetadataUsageId;
extern "C"  void ListWrapper_1__ctor_m1463683433_gshared (ListWrapper_1_t267912380 * __this, Il2CppObject* ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListWrapper_1__ctor_m1463683433_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___list0;
		NullCheck((CollectionWrapper_1_t3035453308 *)__this);
		((  void (*) (CollectionWrapper_1_t3035453308 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((CollectionWrapper_1_t3035453308 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		Il2CppObject* L_1 = ___list0;
		ValidationUtils_ArgumentNotNull_m3978868425(NULL /*static, unused*/, (Il2CppObject *)L_1, (String_t*)_stringLiteral3322014, /*hidden argument*/NULL);
		Il2CppObject* L_2 = ___list0;
		__this->set__genericList_3(L_2);
		return;
	}
}
// System.Int32 Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::IndexOf(T)
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern const uint32_t ListWrapper_1_IndexOf_m469317177_MetadataUsageId;
extern "C"  int32_t ListWrapper_1_IndexOf_m469317177_gshared (ListWrapper_1_t267912380 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListWrapper_1_IndexOf_m469317177_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericList_3();
		Il2CppObject * L_2 = ___item0;
		NullCheck((Il2CppObject*)L_1);
		int32_t L_3 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Object>::IndexOf(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
		return L_3;
	}

IL_0018:
	{
		Il2CppObject * L_4 = ___item0;
		NullCheck((Il2CppObject *)__this);
		int32_t L_5 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(7 /* System.Int32 System.Collections.IList::IndexOf(System.Object) */, IList_t1751339649_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (Il2CppObject *)L_4);
		return L_5;
	}
}
// System.Void Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::Insert(System.Int32,T)
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern const uint32_t ListWrapper_1_Insert_m2253469242_MetadataUsageId;
extern "C"  void ListWrapper_1_Insert_m2253469242_gshared (ListWrapper_1_t267912380 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListWrapper_1_Insert_m2253469242_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericList_3();
		int32_t L_2 = ___index0;
		Il2CppObject * L_3 = ___item1;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(1 /* System.Void System.Collections.Generic.IList`1<System.Object>::Insert(System.Int32,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_1, (int32_t)L_2, (Il2CppObject *)L_3);
		goto IL_002a;
	}

IL_001d:
	{
		int32_t L_4 = ___index0;
		Il2CppObject * L_5 = ___item1;
		NullCheck((Il2CppObject *)__this);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(8 /* System.Void System.Collections.IList::Insert(System.Int32,System.Object) */, IList_t1751339649_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (int32_t)L_4, (Il2CppObject *)L_5);
	}

IL_002a:
	{
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::RemoveAt(System.Int32)
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern const uint32_t ListWrapper_1_RemoveAt_m127322112_MetadataUsageId;
extern "C"  void ListWrapper_1_RemoveAt_m127322112_gshared (ListWrapper_1_t267912380 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListWrapper_1_RemoveAt_m127322112_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_00d6;
		}
	}
	{
		bool L_1 = BreakUtil_IsBreak9_m1885801120(NULL /*static, unused*/, (float)(-2.14963123E+09f), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_00c5;
		}
	}
	{
		V_2 = (int32_t)((int32_t)15211566);
		V_3 = (int32_t)((int32_t)15211572);
		int32_t L_2 = V_2;
		int32_t L_3 = V_3;
		int32_t L_4 = V_2;
		int32_t L_5 = V_2;
		int32_t L_6 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2^(int32_t)L_3))*(int32_t)L_4))-(int32_t)L_5))+(int32_t)L_6));
		int32_t L_7 = V_2;
		int32_t L_8 = V_3;
		int32_t L_9 = V_3;
		int32_t L_10 = V_2;
		int32_t L_11 = V_3;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_7|(int32_t)L_8))%(int32_t)L_9))*(int32_t)L_10))-(int32_t)L_11));
		int32_t L_12 = V_2;
		int32_t L_13 = V_3;
		int32_t L_14 = V_2;
		int32_t L_15 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_12%(int32_t)L_13))&(int32_t)L_14))/(int32_t)L_15));
		int32_t L_16 = V_2;
		int32_t L_17 = V_3;
		int32_t L_18 = V_3;
		int32_t L_19 = V_2;
		V_3 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_16^(int32_t)L_17))|(int32_t)L_18))|(int32_t)L_19));
		int32_t L_20 = V_2;
		int32_t L_21 = V_3;
		int32_t L_22 = V_3;
		int32_t L_23 = V_2;
		int32_t L_24 = V_3;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_20^(int32_t)L_21))%(int32_t)L_22))-(int32_t)L_23))&(int32_t)L_24));
		int32_t L_25 = V_2;
		int32_t L_26 = V_3;
		int32_t L_27 = V_3;
		int32_t L_28 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_25+(int32_t)L_26))-(int32_t)L_27))%(int32_t)L_28));
	}

IL_00c5:
	{
		Il2CppObject* L_29 = (Il2CppObject*)__this->get__genericList_3();
		int32_t L_30 = ___index0;
		NullCheck((Il2CppObject*)L_29);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void System.Collections.Generic.IList`1<System.Object>::RemoveAt(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_29, (int32_t)L_30);
		goto IL_0190;
	}

IL_00d6:
	{
		bool L_31 = BreakUtil_IsBreak9_m1885801120(NULL /*static, unused*/, (float)(-2.14963123E+09f), /*hidden argument*/NULL);
		if (L_31)
		{
			goto IL_0189;
		}
	}
	{
		V_0 = (int32_t)((int32_t)15211499);
		V_1 = (int32_t)((int32_t)15211580);
		int32_t L_32 = V_0;
		int32_t L_33 = V_1;
		int32_t L_34 = V_1;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_32%(int32_t)L_33))%(int32_t)L_34));
		int32_t L_35 = V_0;
		int32_t L_36 = V_1;
		int32_t L_37 = V_0;
		int32_t L_38 = V_1;
		int32_t L_39 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_35+(int32_t)L_36))^(int32_t)L_37))&(int32_t)L_38))&(int32_t)L_39));
		int32_t L_40 = V_0;
		int32_t L_41 = V_1;
		int32_t L_42 = V_0;
		int32_t L_43 = V_0;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_40|(int32_t)L_41))&(int32_t)L_42))*(int32_t)L_43));
		int32_t L_44 = V_0;
		int32_t L_45 = V_1;
		int32_t L_46 = V_1;
		int32_t L_47 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_44/(int32_t)L_45))&(int32_t)L_46))^(int32_t)L_47));
		int32_t L_48 = V_0;
		int32_t L_49 = V_1;
		int32_t L_50 = V_0;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_48&(int32_t)L_49))&(int32_t)L_50));
		int32_t L_51 = V_0;
		int32_t L_52 = V_1;
		int32_t L_53 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_51-(int32_t)L_52))/(int32_t)L_53));
		int32_t L_54 = V_0;
		int32_t L_55 = V_1;
		int32_t L_56 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_54&(int32_t)L_55))&(int32_t)L_56));
	}

IL_0189:
	{
		int32_t L_57 = ___index0;
		NullCheck((Il2CppObject *)__this);
		InterfaceActionInvoker1< int32_t >::Invoke(10 /* System.Void System.Collections.IList::RemoveAt(System.Int32) */, IList_t1751339649_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (int32_t)L_57);
	}

IL_0190:
	{
		return;
	}
}
// T Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::get_Item(System.Int32)
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral44816;
extern const uint32_t ListWrapper_1_get_Item_m3673329412_MetadataUsageId;
extern "C"  Il2CppObject * ListWrapper_1_get_Item_m3673329412_gshared (ListWrapper_1_t267912380 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListWrapper_1_get_Item_m3673329412_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_00b1;
		}
	}
	{
		bool L_1 = BreakUtil_IsBreak8_m2715011608(NULL /*static, unused*/, (String_t*)_stringLiteral44816, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_00a4;
		}
	}
	{
		V_0 = (int32_t)((int32_t)15211573);
		V_1 = (int32_t)((int32_t)15211572);
		int32_t L_2 = V_0;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		int32_t L_5 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2-(int32_t)L_3))&(int32_t)L_4))^(int32_t)L_5));
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		int32_t L_8 = V_1;
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6|(int32_t)L_7))-(int32_t)L_8))|(int32_t)L_9));
		int32_t L_10 = V_0;
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		int32_t L_13 = V_1;
		int32_t L_14 = V_0;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_10+(int32_t)L_11))%(int32_t)L_12))-(int32_t)L_13))+(int32_t)L_14));
		int32_t L_15 = V_0;
		int32_t L_16 = V_1;
		int32_t L_17 = V_0;
		int32_t L_18 = V_0;
		int32_t L_19 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_15/(int32_t)L_16))^(int32_t)L_17))%(int32_t)L_18))+(int32_t)L_19));
		int32_t L_20 = V_0;
		int32_t L_21 = V_1;
		int32_t L_22 = V_0;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_20/(int32_t)L_21))-(int32_t)L_22));
	}

IL_00a4:
	{
		Il2CppObject* L_23 = (Il2CppObject*)__this->get__genericList_3();
		int32_t L_24 = ___index0;
		NullCheck((Il2CppObject*)L_23);
		Il2CppObject * L_25 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_23, (int32_t)L_24);
		return L_25;
	}

IL_00b1:
	{
		int32_t L_26 = ___index0;
		NullCheck((Il2CppObject *)__this);
		Il2CppObject * L_27 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(2 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_t1751339649_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (int32_t)L_26);
		return ((Il2CppObject *)Castclass(L_27, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4)));
	}
}
// System.Void Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::set_Item(System.Int32,T)
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern const uint32_t ListWrapper_1_set_Item_m2730021009_MetadataUsageId;
extern "C"  void ListWrapper_1_set_Item_m2730021009_gshared (ListWrapper_1_t267912380 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListWrapper_1_set_Item_m2730021009_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_00da;
		}
	}
	{
		bool L_1 = BreakUtil_IsBreak1_m1675107240(NULL /*static, unused*/, (float)(-2.14963123E+09f), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_00c8;
		}
	}
	{
		V_2 = (int32_t)((int32_t)15211568);
		V_3 = (int32_t)((int32_t)15211512);
		int32_t L_2 = V_2;
		int32_t L_3 = V_3;
		int32_t L_4 = V_3;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2*(int32_t)L_3))%(int32_t)L_4));
		int32_t L_5 = V_2;
		int32_t L_6 = V_3;
		int32_t L_7 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5*(int32_t)L_6))^(int32_t)L_7));
		int32_t L_8 = V_2;
		int32_t L_9 = V_3;
		int32_t L_10 = V_2;
		int32_t L_11 = V_3;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_8^(int32_t)L_9))/(int32_t)L_10))%(int32_t)L_11));
		int32_t L_12 = V_2;
		int32_t L_13 = V_3;
		int32_t L_14 = V_3;
		int32_t L_15 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_12|(int32_t)L_13))/(int32_t)L_14))*(int32_t)L_15));
		int32_t L_16 = V_2;
		int32_t L_17 = V_3;
		int32_t L_18 = V_2;
		int32_t L_19 = V_2;
		int32_t L_20 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_16&(int32_t)L_17))&(int32_t)L_18))%(int32_t)L_19))&(int32_t)L_20));
		int32_t L_21 = V_2;
		int32_t L_22 = V_3;
		int32_t L_23 = V_2;
		V_3 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_21^(int32_t)L_22))*(int32_t)L_23));
		int32_t L_24 = V_2;
		int32_t L_25 = V_3;
		int32_t L_26 = V_3;
		int32_t L_27 = V_3;
		int32_t L_28 = V_3;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_24-(int32_t)L_25))-(int32_t)L_26))/(int32_t)L_27))*(int32_t)L_28));
	}

IL_00c8:
	{
		Il2CppObject* L_29 = (Il2CppObject*)__this->get__genericList_3();
		int32_t L_30 = ___index0;
		Il2CppObject * L_31 = ___value1;
		NullCheck((Il2CppObject*)L_29);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(4 /* System.Void System.Collections.Generic.IList`1<System.Object>::set_Item(System.Int32,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_29, (int32_t)L_30, (Il2CppObject *)L_31);
		goto IL_013b;
	}

IL_00da:
	{
		bool L_32 = BreakUtil_IsBreak37_m253756901(NULL /*static, unused*/, (int32_t)((int32_t)-2147483648LL), /*hidden argument*/NULL);
		if (L_32)
		{
			goto IL_012e;
		}
	}
	{
		V_0 = (int32_t)((int32_t)15211563);
		V_1 = (int32_t)((int32_t)15211491);
		int32_t L_33 = V_0;
		int32_t L_34 = V_1;
		int32_t L_35 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_33/(int32_t)L_34))^(int32_t)L_35));
		int32_t L_36 = V_0;
		int32_t L_37 = V_1;
		int32_t L_38 = V_0;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_36/(int32_t)L_37))/(int32_t)L_38));
		int32_t L_39 = V_0;
		int32_t L_40 = V_1;
		int32_t L_41 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_39^(int32_t)L_40))*(int32_t)L_41));
	}

IL_012e:
	{
		int32_t L_42 = ___index0;
		Il2CppObject * L_43 = ___value1;
		NullCheck((Il2CppObject *)__this);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(3 /* System.Void System.Collections.IList::set_Item(System.Int32,System.Object) */, IList_t1751339649_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (int32_t)L_42, (Il2CppObject *)L_43);
	}

IL_013b:
	{
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::Add(T)
extern "C"  void ListWrapper_1_Add_m2322858451_gshared (ListWrapper_1_t267912380 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericList_3();
		Il2CppObject * L_2 = ___item0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::Add(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
		goto IL_0023;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___item0;
		NullCheck((CollectionWrapper_1_t3035453308 *)__this);
		((  void (*) (CollectionWrapper_1_t3035453308 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((CollectionWrapper_1_t3035453308 *)__this, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0023:
	{
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::Clear()
extern "C"  void ListWrapper_1_Clear_m1000858967_gshared (ListWrapper_1_t267912380 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericList_3();
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::Clear() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_1);
		goto IL_0021;
	}

IL_001b:
	{
		NullCheck((CollectionWrapper_1_t3035453308 *)__this);
		((  void (*) (CollectionWrapper_1_t3035453308 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((CollectionWrapper_1_t3035453308 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
	}

IL_0021:
	{
		return;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::Contains(T)
extern "C"  bool ListWrapper_1_Contains_m3107521427_gshared (ListWrapper_1_t267912380 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericList_3();
		Il2CppObject * L_2 = ___item0;
		NullCheck((Il2CppObject*)L_1);
		bool L_3 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Contains(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
		return L_3;
	}

IL_0018:
	{
		Il2CppObject * L_4 = ___item0;
		NullCheck((CollectionWrapper_1_t3035453308 *)__this);
		bool L_5 = ((  bool (*) (CollectionWrapper_1_t3035453308 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((CollectionWrapper_1_t3035453308 *)__this, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_5;
	}
}
// System.Void Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void ListWrapper_1_CopyTo_m1349139203_gshared (ListWrapper_1_t267912380 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericList_3();
		ObjectU5BU5D_t1108656482* L_2 = ___array0;
		int32_t L_3 = ___arrayIndex1;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker2< ObjectU5BU5D_t1108656482*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_1, (ObjectU5BU5D_t1108656482*)L_2, (int32_t)L_3);
		goto IL_0025;
	}

IL_001d:
	{
		ObjectU5BU5D_t1108656482* L_4 = ___array0;
		int32_t L_5 = ___arrayIndex1;
		NullCheck((CollectionWrapper_1_t3035453308 *)__this);
		((  void (*) (CollectionWrapper_1_t3035453308 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((CollectionWrapper_1_t3035453308 *)__this, (ObjectU5BU5D_t1108656482*)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
	}

IL_0025:
	{
		return;
	}
}
// System.Int32 Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::get_Count()
extern "C"  int32_t ListWrapper_1_get_Count_m1481238114_gshared (ListWrapper_1_t267912380 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericList_3();
		NullCheck((Il2CppObject*)L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_1);
		return L_2;
	}

IL_0017:
	{
		NullCheck((CollectionWrapper_1_t3035453308 *)__this);
		int32_t L_3 = ((  int32_t (*) (CollectionWrapper_1_t3035453308 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((CollectionWrapper_1_t3035453308 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return L_3;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::get_IsReadOnly()
extern "C"  bool ListWrapper_1_get_IsReadOnly_m659447169_gshared (ListWrapper_1_t267912380 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_008a;
		}
	}
	{
		bool L_1 = BreakUtil_IsBreak15_m2023967703(NULL /*static, unused*/, (float)(-2.14963123E+09f), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_007e;
		}
	}
	{
		V_0 = (int32_t)((int32_t)15211522);
		V_1 = (int32_t)((int32_t)15211579);
		int32_t L_2 = V_0;
		int32_t L_3 = V_1;
		int32_t L_4 = V_1;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2%(int32_t)L_3))-(int32_t)L_4));
		int32_t L_5 = V_0;
		int32_t L_6 = V_1;
		int32_t L_7 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5*(int32_t)L_6))|(int32_t)L_7));
		int32_t L_8 = V_0;
		int32_t L_9 = V_1;
		int32_t L_10 = V_0;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_8&(int32_t)L_9))%(int32_t)L_10));
		int32_t L_11 = V_0;
		int32_t L_12 = V_1;
		int32_t L_13 = V_1;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_11|(int32_t)L_12))|(int32_t)L_13));
		int32_t L_14 = V_0;
		int32_t L_15 = V_1;
		V_0 = (int32_t)((int32_t)((int32_t)L_14|(int32_t)L_15));
	}

IL_007e:
	{
		Il2CppObject* L_16 = (Il2CppObject*)__this->get__genericList_3();
		NullCheck((Il2CppObject*)L_16);
		bool L_17 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_16);
		return L_17;
	}

IL_008a:
	{
		NullCheck((CollectionWrapper_1_t3035453308 *)__this);
		bool L_18 = ((  bool (*) (CollectionWrapper_1_t3035453308 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((CollectionWrapper_1_t3035453308 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_18;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::Remove(T)
extern "C"  bool ListWrapper_1_Remove_m1441762382_gshared (ListWrapper_1_t267912380 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_00a2;
		}
	}
	{
		bool L_1 = BreakUtil_IsBreak14_m1047137124(NULL /*static, unused*/, (int32_t)((int32_t)-2147483648LL), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0095;
		}
	}
	{
		V_3 = (int32_t)((int32_t)15211494);
		V_4 = (int32_t)((int32_t)15211553);
		int32_t L_2 = V_3;
		int32_t L_3 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)L_2/(int32_t)L_3));
		int32_t L_4 = V_3;
		int32_t L_5 = V_4;
		int32_t L_6 = V_3;
		V_4 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_4+(int32_t)L_5))^(int32_t)L_6));
		int32_t L_7 = V_3;
		int32_t L_8 = V_4;
		int32_t L_9 = V_3;
		int32_t L_10 = V_3;
		V_4 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_7-(int32_t)L_8))*(int32_t)L_9))-(int32_t)L_10));
		int32_t L_11 = V_3;
		int32_t L_12 = V_4;
		V_3 = (int32_t)((int32_t)((int32_t)L_11%(int32_t)L_12));
		int32_t L_13 = V_3;
		int32_t L_14 = V_4;
		int32_t L_15 = V_4;
		int32_t L_16 = V_3;
		V_4 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_13%(int32_t)L_14))*(int32_t)L_15))+(int32_t)L_16));
		int32_t L_17 = V_3;
		int32_t L_18 = V_4;
		int32_t L_19 = V_3;
		V_4 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_17^(int32_t)L_18))^(int32_t)L_19));
	}

IL_0095:
	{
		Il2CppObject* L_20 = (Il2CppObject*)__this->get__genericList_3();
		Il2CppObject * L_21 = ___item0;
		NullCheck((Il2CppObject*)L_20);
		bool L_22 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(6 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Remove(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_20, (Il2CppObject *)L_21);
		return L_22;
	}

IL_00a2:
	{
		Il2CppObject * L_23 = ___item0;
		NullCheck((CollectionWrapper_1_t3035453308 *)__this);
		bool L_24 = ((  bool (*) (CollectionWrapper_1_t3035453308 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((CollectionWrapper_1_t3035453308 *)__this, (Il2CppObject *)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_0 = (bool)L_24;
		bool L_25 = V_0;
		if (!L_25)
		{
			goto IL_015e;
		}
	}
	{
		bool L_26 = BreakUtil_IsBreak30_m2988290686(NULL /*static, unused*/, (float)(-2.14963123E+09f), /*hidden argument*/NULL);
		if (L_26)
		{
			goto IL_0156;
		}
	}
	{
		V_1 = (int32_t)((int32_t)15211579);
		V_2 = (int32_t)((int32_t)15211523);
		int32_t L_27 = V_1;
		int32_t L_28 = V_2;
		int32_t L_29 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_27%(int32_t)L_28))^(int32_t)L_29));
		int32_t L_30 = V_1;
		int32_t L_31 = V_2;
		int32_t L_32 = V_1;
		int32_t L_33 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_30&(int32_t)L_31))+(int32_t)L_32))-(int32_t)L_33));
		int32_t L_34 = V_1;
		int32_t L_35 = V_2;
		int32_t L_36 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_34|(int32_t)L_35))+(int32_t)L_36));
		int32_t L_37 = V_1;
		int32_t L_38 = V_2;
		int32_t L_39 = V_2;
		int32_t L_40 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_37-(int32_t)L_38))-(int32_t)L_39))|(int32_t)L_40));
		int32_t L_41 = V_1;
		int32_t L_42 = V_2;
		int32_t L_43 = V_2;
		int32_t L_44 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_41*(int32_t)L_42))^(int32_t)L_43))^(int32_t)L_44));
		int32_t L_45 = V_1;
		int32_t L_46 = V_2;
		int32_t L_47 = V_2;
		int32_t L_48 = V_2;
		int32_t L_49 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_45%(int32_t)L_46))+(int32_t)L_47))^(int32_t)L_48))+(int32_t)L_49));
	}

IL_0156:
	{
		Il2CppObject * L_50 = ___item0;
		NullCheck((CollectionWrapper_1_t3035453308 *)__this);
		((  bool (*) (CollectionWrapper_1_t3035453308 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CollectionWrapper_1_t3035453308 *)__this, (Il2CppObject *)L_50, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_015e:
	{
		bool L_51 = V_0;
		return L_51;
	}
}
// System.Collections.Generic.IEnumerator`1<T> Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ListWrapper_1_GetEnumerator_m656946152_gshared (ListWrapper_1_t267912380 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_006b;
		}
	}
	{
		bool L_1 = BreakUtil_IsBreak37_m253756901(NULL /*static, unused*/, (int32_t)((int32_t)-2147483648LL), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_005f;
		}
	}
	{
		V_0 = (int32_t)((int32_t)15211504);
		V_1 = (int32_t)((int32_t)15211542);
		int32_t L_2 = V_0;
		int32_t L_3 = V_1;
		int32_t L_4 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2+(int32_t)L_3))%(int32_t)L_4));
		int32_t L_5 = V_0;
		int32_t L_6 = V_1;
		V_0 = (int32_t)((int32_t)((int32_t)L_5-(int32_t)L_6));
		int32_t L_7 = V_0;
		int32_t L_8 = V_1;
		int32_t L_9 = V_0;
		int32_t L_10 = V_0;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_7^(int32_t)L_8))/(int32_t)L_9))|(int32_t)L_10));
	}

IL_005f:
	{
		Il2CppObject* L_11 = (Il2CppObject*)__this->get__genericList_3();
		NullCheck((Il2CppObject*)L_11);
		Il2CppObject* L_12 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13), (Il2CppObject*)L_11);
		return L_12;
	}

IL_006b:
	{
		NullCheck((CollectionWrapper_1_t3035453308 *)__this);
		Il2CppObject* L_13 = ((  Il2CppObject* (*) (CollectionWrapper_1_t3035453308 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((CollectionWrapper_1_t3035453308 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return L_13;
	}
}
// System.Object Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::get_UnderlyingList()
extern "C"  Il2CppObject * ListWrapper_1_get_UnderlyingList_m4165344305_gshared (ListWrapper_1_t267912380 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_00bb;
		}
	}
	{
		bool L_1 = BreakUtil_IsBreak5_m3927937828(NULL /*static, unused*/, (float)(-2.14963123E+09f), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_00b4;
		}
	}
	{
		V_0 = (int32_t)((int32_t)15211489);
		V_1 = (int32_t)((int32_t)15211527);
		int32_t L_2 = V_0;
		int32_t L_3 = V_1;
		int32_t L_4 = V_1;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2|(int32_t)L_3))-(int32_t)L_4));
		int32_t L_5 = V_0;
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5%(int32_t)L_6));
		int32_t L_7 = V_0;
		int32_t L_8 = V_1;
		int32_t L_9 = V_0;
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_7+(int32_t)L_8))*(int32_t)L_9))/(int32_t)L_10));
		int32_t L_11 = V_0;
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_11*(int32_t)L_12))|(int32_t)L_13));
		int32_t L_14 = V_0;
		int32_t L_15 = V_1;
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_14^(int32_t)L_15))^(int32_t)L_16));
		int32_t L_17 = V_0;
		int32_t L_18 = V_1;
		int32_t L_19 = V_1;
		int32_t L_20 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_17%(int32_t)L_18))/(int32_t)L_19))+(int32_t)L_20));
		int32_t L_21 = V_0;
		int32_t L_22 = V_1;
		int32_t L_23 = V_0;
		int32_t L_24 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_21%(int32_t)L_22))/(int32_t)L_23))%(int32_t)L_24));
	}

IL_00b4:
	{
		Il2CppObject* L_25 = (Il2CppObject*)__this->get__genericList_3();
		return L_25;
	}

IL_00bb:
	{
		NullCheck((CollectionWrapper_1_t3035453308 *)__this);
		Il2CppObject * L_26 = ((  Il2CppObject * (*) (CollectionWrapper_1_t3035453308 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((CollectionWrapper_1_t3035453308 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return L_26;
	}
}
// System.Void Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void MethodCall_2__ctor_m42691767_gshared (MethodCall_2_t1809280638 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>::Invoke(T,System.Object[])
extern "C"  Il2CppObject * MethodCall_2_Invoke_m393306683_gshared (MethodCall_2_t1809280638 * __this, Il2CppObject * ___target0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		MethodCall_2_Invoke_m393306683((MethodCall_2_t1809280638 *)__this->get_prev_9(),___target0, ___args1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___target0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___target0, ___args1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___target0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___target0, ___args1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___target0, ___args1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>::BeginInvoke(T,System.Object[],System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * MethodCall_2_BeginInvoke_m1081648150_gshared (MethodCall_2_t1809280638 * __this, Il2CppObject * ___target0, ObjectU5BU5D_t1108656482* ___args1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___target0;
	__d_args[1] = ___args1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * MethodCall_2_EndInvoke_m643072357_gshared (MethodCall_2_t1809280638 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14B`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1__ctor_m1410811691_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_t3536942057 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14B`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::<>m__3B7(TSource)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_U3CU3Em__3B7_m3791373613_MetadataUsageId;
extern "C"  void U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_U3CU3Em__3B7_m3791373613_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_t3536942057 * __this, KeyValuePair_2_t1944668977  ___itm0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_U3CU3Em__3B7_m3791373613_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Func_2_t1690470805 * L_0 = (Func_2_t1690470805 *)__this->get_valueSelector_0();
		KeyValuePair_2_t1944668977  L_1 = ___itm0;
		NullCheck((Func_2_t1690470805 *)L_0);
		String_t* L_2 = ((  String_t* (*) (Func_2_t1690470805 *, KeyValuePair_2_t1944668977 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Func_2_t1690470805 *)L_0, (KeyValuePair_2_t1944668977 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		String_t* L_3 = (String_t*)__this->get_testValue_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_4 = String_Compare_m3128699950(NULL /*static, unused*/, (String_t*)L_2, (String_t*)L_3, (int32_t)5, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		List_1_t3312854530 * L_5 = (List_1_t3312854530 *)__this->get_caseInsensitiveResults_2();
		KeyValuePair_2_t1944668977  L_6 = ___itm0;
		NullCheck((List_1_t3312854530 *)L_5);
		((  void (*) (List_1_t3312854530 *, KeyValuePair_2_t1944668977 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t3312854530 *)L_5, (KeyValuePair_2_t1944668977 )L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
	}

IL_0029:
	{
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14B`1<System.Object>::.ctor()
extern "C"  void U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1__ctor_m240006394_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_t1468122155 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14B`1<System.Object>::<>m__3B7(TSource)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_U3CU3Em__3B7_m4011523132_MetadataUsageId;
extern "C"  void U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_U3CU3Em__3B7_m4011523132_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_t1468122155 * __this, Il2CppObject * ___itm0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_U3CU3Em__3B7_m4011523132_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Func_2_t315946507 * L_0 = (Func_2_t315946507 *)__this->get_valueSelector_0();
		Il2CppObject * L_1 = ___itm0;
		NullCheck((Func_2_t315946507 *)L_0);
		String_t* L_2 = ((  String_t* (*) (Func_2_t315946507 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Func_2_t315946507 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		String_t* L_3 = (String_t*)__this->get_testValue_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_4 = String_Compare_m3128699950(NULL /*static, unused*/, (String_t*)L_2, (String_t*)L_3, (int32_t)5, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		List_1_t1244034627 * L_5 = (List_1_t1244034627 *)__this->get_caseInsensitiveResults_2();
		Il2CppObject * L_6 = ___itm0;
		NullCheck((List_1_t1244034627 *)L_5);
		((  void (*) (List_1_t1244034627 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t1244034627 *)L_5, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
	}

IL_0029:
	{
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14C`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1__ctor_m2618729866_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_t3091917058 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14C`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::<>m__3B8(TSource)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_U3CU3Em__3B8_m2785372651_MetadataUsageId;
extern "C"  void U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_U3CU3Em__3B8_m2785372651_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_t3091917058 * __this, KeyValuePair_2_t1944668977  ___itm0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_U3CU3Em__3B8_m2785372651_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_t3536942057 * L_0 = (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_t3536942057 *)__this->get_U3CU3Ef__refU24331_1();
		NullCheck(L_0);
		Func_2_t1690470805 * L_1 = (Func_2_t1690470805 *)L_0->get_valueSelector_0();
		KeyValuePair_2_t1944668977  L_2 = ___itm0;
		NullCheck((Func_2_t1690470805 *)L_1);
		String_t* L_3 = ((  String_t* (*) (Func_2_t1690470805 *, KeyValuePair_2_t1944668977 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Func_2_t1690470805 *)L_1, (KeyValuePair_2_t1944668977 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_t3536942057 * L_4 = (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_t3536942057 *)__this->get_U3CU3Ef__refU24331_1();
		NullCheck(L_4);
		String_t* L_5 = (String_t*)L_4->get_testValue_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_6 = String_Compare_m3128699950(NULL /*static, unused*/, (String_t*)L_3, (String_t*)L_5, (int32_t)4, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0033;
		}
	}
	{
		List_1_t3312854530 * L_7 = (List_1_t3312854530 *)__this->get_caseSensitiveResults_0();
		KeyValuePair_2_t1944668977  L_8 = ___itm0;
		NullCheck((List_1_t3312854530 *)L_7);
		((  void (*) (List_1_t3312854530 *, KeyValuePair_2_t1944668977 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t3312854530 *)L_7, (KeyValuePair_2_t1944668977 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
	}

IL_0033:
	{
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14C`1<System.Object>::.ctor()
extern "C"  void U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1__ctor_m1201620411_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_t1023097156 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14C`1<System.Object>::<>m__3B8(TSource)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_U3CU3Em__3B8_m4144775388_MetadataUsageId;
extern "C"  void U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_U3CU3Em__3B8_m4144775388_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_t1023097156 * __this, Il2CppObject * ___itm0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_U3CU3Em__3B8_m4144775388_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_t1468122155 * L_0 = (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_t1468122155 *)__this->get_U3CU3Ef__refU24331_1();
		NullCheck(L_0);
		Func_2_t315946507 * L_1 = (Func_2_t315946507 *)L_0->get_valueSelector_0();
		Il2CppObject * L_2 = ___itm0;
		NullCheck((Func_2_t315946507 *)L_1);
		String_t* L_3 = ((  String_t* (*) (Func_2_t315946507 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Func_2_t315946507 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_t1468122155 * L_4 = (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_t1468122155 *)__this->get_U3CU3Ef__refU24331_1();
		NullCheck(L_4);
		String_t* L_5 = (String_t*)L_4->get_testValue_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_6 = String_Compare_m3128699950(NULL /*static, unused*/, (String_t*)L_3, (String_t*)L_5, (int32_t)4, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0033;
		}
	}
	{
		List_1_t1244034627 * L_7 = (List_1_t1244034627 *)__this->get_caseSensitiveResults_0();
		Il2CppObject * L_8 = ___itm0;
		NullCheck((List_1_t1244034627 *)L_7);
		((  void (*) (List_1_t1244034627 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t1244034627 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
	}

IL_0033:
	{
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::.ctor(System.Func`2<TKey,TValue>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1028554796;
extern const uint32_t ThreadSafeStore_2__ctor_m3447883656_MetadataUsageId;
extern "C"  void ThreadSafeStore_2__ctor_m3447883656_gshared (ThreadSafeStore_2_t648438005 * __this, Func_2_t2253398629 * ___creator0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeStore_2__ctor_m3447883656_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set__lock_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Func_2_t2253398629 * L_1 = ___creator0;
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_2 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_2, (String_t*)_stringLiteral1028554796, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		Func_2_t2253398629 * L_3 = ___creator0;
		__this->set__creator_2(L_3);
		return;
	}
}
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::Get(TKey)
extern "C"  Il2CppObject * ThreadSafeStore_2_Get_m1825001592_gshared (ThreadSafeStore_2_t648438005 * __this, TypeNameKey_t2971844791  ___key0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		Dictionary_2_t4114722875 * L_0 = (Dictionary_2_t4114722875 *)__this->get__store_1();
		if (L_0)
		{
			goto IL_00b7;
		}
	}
	{
		bool L_1 = BreakUtil_IsBreak15_m2554688933(NULL /*static, unused*/, (int32_t)((int32_t)-2147483648LL), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_00af;
		}
	}
	{
		V_3 = (int32_t)((int32_t)15211493);
		V_4 = (int32_t)((int32_t)15211559);
		int32_t L_2 = V_3;
		int32_t L_3 = V_4;
		int32_t L_4 = V_4;
		V_3 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2/(int32_t)L_3))%(int32_t)L_4));
		int32_t L_5 = V_3;
		int32_t L_6 = V_4;
		int32_t L_7 = V_3;
		V_4 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5^(int32_t)L_6))&(int32_t)L_7));
		int32_t L_8 = V_3;
		int32_t L_9 = V_4;
		int32_t L_10 = V_4;
		int32_t L_11 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_8^(int32_t)L_9))^(int32_t)L_10))-(int32_t)L_11));
		int32_t L_12 = V_3;
		int32_t L_13 = V_4;
		int32_t L_14 = V_3;
		V_4 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_12+(int32_t)L_13))/(int32_t)L_14));
		int32_t L_15 = V_3;
		int32_t L_16 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)L_16));
		int32_t L_17 = V_3;
		int32_t L_18 = V_4;
		int32_t L_19 = V_4;
		int32_t L_20 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_17/(int32_t)L_18))/(int32_t)L_19))+(int32_t)L_20));
		int32_t L_21 = V_3;
		int32_t L_22 = V_4;
		int32_t L_23 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_21+(int32_t)L_22))-(int32_t)L_23));
	}

IL_00af:
	{
		TypeNameKey_t2971844791  L_24 = ___key0;
		NullCheck((ThreadSafeStore_2_t648438005 *)__this);
		Il2CppObject * L_25 = ((  Il2CppObject * (*) (ThreadSafeStore_2_t648438005 *, TypeNameKey_t2971844791 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ThreadSafeStore_2_t648438005 *)__this, (TypeNameKey_t2971844791 )L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_25;
	}

IL_00b7:
	{
		Dictionary_2_t4114722875 * L_26 = (Dictionary_2_t4114722875 *)__this->get__store_1();
		TypeNameKey_t2971844791  L_27 = ___key0;
		NullCheck((Dictionary_2_t4114722875 *)L_26);
		bool L_28 = ((  bool (*) (Dictionary_2_t4114722875 *, TypeNameKey_t2971844791 , Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Dictionary_2_t4114722875 *)L_26, (TypeNameKey_t2971844791 )L_27, (Il2CppObject **)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		if (L_28)
		{
			goto IL_0154;
		}
	}
	{
		bool L_29 = BreakUtil_IsBreak32_m1967222332(NULL /*static, unused*/, (float)(-2.14963123E+09f), /*hidden argument*/NULL);
		if (L_29)
		{
			goto IL_014c;
		}
	}
	{
		V_1 = (int32_t)((int32_t)15211565);
		V_2 = (int32_t)((int32_t)15211542);
		int32_t L_30 = V_1;
		int32_t L_31 = V_2;
		int32_t L_32 = V_1;
		int32_t L_33 = V_2;
		int32_t L_34 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_30^(int32_t)L_31))^(int32_t)L_32))*(int32_t)L_33))|(int32_t)L_34));
		int32_t L_35 = V_1;
		int32_t L_36 = V_2;
		int32_t L_37 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_35&(int32_t)L_36))+(int32_t)L_37));
		int32_t L_38 = V_1;
		int32_t L_39 = V_2;
		int32_t L_40 = V_1;
		int32_t L_41 = V_2;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_38&(int32_t)L_39))*(int32_t)L_40))/(int32_t)L_41));
		int32_t L_42 = V_1;
		int32_t L_43 = V_2;
		int32_t L_44 = V_1;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_42^(int32_t)L_43))&(int32_t)L_44));
		int32_t L_45 = V_1;
		int32_t L_46 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_45^(int32_t)L_46));
	}

IL_014c:
	{
		TypeNameKey_t2971844791  L_47 = ___key0;
		NullCheck((ThreadSafeStore_2_t648438005 *)__this);
		Il2CppObject * L_48 = ((  Il2CppObject * (*) (ThreadSafeStore_2_t648438005 *, TypeNameKey_t2971844791 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ThreadSafeStore_2_t648438005 *)__this, (TypeNameKey_t2971844791 )L_47, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_48;
	}

IL_0154:
	{
		Il2CppObject * L_49 = V_0;
		return L_49;
	}
}
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::AddValue(TKey)
extern "C"  Il2CppObject * ThreadSafeStore_2_AddValue_m3087662362_gshared (ThreadSafeStore_2_t648438005 * __this, TypeNameKey_t2971844791  ___key0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Dictionary_2_t4114722875 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Func_2_t2253398629 * L_0 = (Func_2_t2253398629 *)__this->get__creator_2();
		TypeNameKey_t2971844791  L_1 = ___key0;
		NullCheck((Func_2_t2253398629 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_2_t2253398629 *, TypeNameKey_t2971844791 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Func_2_t2253398629 *)L_0, (TypeNameKey_t2971844791 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__lock_0();
		V_1 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t4114722875 * L_5 = (Dictionary_2_t4114722875 *)__this->get__store_1();
			if (L_5)
			{
				goto IL_0042;
			}
		}

IL_0025:
		{
			Dictionary_2_t4114722875 * L_6 = (Dictionary_2_t4114722875 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
			((  void (*) (Dictionary_2_t4114722875 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
			__this->set__store_1(L_6);
			Dictionary_2_t4114722875 * L_7 = (Dictionary_2_t4114722875 *)__this->get__store_1();
			TypeNameKey_t2971844791  L_8 = ___key0;
			Il2CppObject * L_9 = V_0;
			NullCheck((Dictionary_2_t4114722875 *)L_7);
			((  void (*) (Dictionary_2_t4114722875 *, TypeNameKey_t2971844791 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t4114722875 *)L_7, (TypeNameKey_t2971844791 )L_8, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			goto IL_0078;
		}

IL_0042:
		{
			Dictionary_2_t4114722875 * L_10 = (Dictionary_2_t4114722875 *)__this->get__store_1();
			TypeNameKey_t2971844791  L_11 = ___key0;
			NullCheck((Dictionary_2_t4114722875 *)L_10);
			bool L_12 = ((  bool (*) (Dictionary_2_t4114722875 *, TypeNameKey_t2971844791 , Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Dictionary_2_t4114722875 *)L_10, (TypeNameKey_t2971844791 )L_11, (Il2CppObject **)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
			if (!L_12)
			{
				goto IL_005d;
			}
		}

IL_0055:
		{
			Il2CppObject * L_13 = V_2;
			V_4 = (Il2CppObject *)L_13;
			IL2CPP_LEAVE(0x8C, FINALLY_0085);
		}

IL_005d:
		{
			Dictionary_2_t4114722875 * L_14 = (Dictionary_2_t4114722875 *)__this->get__store_1();
			Dictionary_2_t4114722875 * L_15 = (Dictionary_2_t4114722875 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
			((  void (*) (Dictionary_2_t4114722875 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_15, (Il2CppObject*)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			V_3 = (Dictionary_2_t4114722875 *)L_15;
			Dictionary_2_t4114722875 * L_16 = V_3;
			TypeNameKey_t2971844791  L_17 = ___key0;
			Il2CppObject * L_18 = V_0;
			NullCheck((Dictionary_2_t4114722875 *)L_16);
			((  void (*) (Dictionary_2_t4114722875 *, TypeNameKey_t2971844791 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t4114722875 *)L_16, (TypeNameKey_t2971844791 )L_17, (Il2CppObject *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			Dictionary_2_t4114722875 * L_19 = V_3;
			__this->set__store_1(L_19);
		}

IL_0078:
		{
			Il2CppObject * L_20 = V_0;
			V_4 = (Il2CppObject *)L_20;
			IL2CPP_LEAVE(0x8C, FINALLY_0085);
		}

IL_0080:
		{
			; // IL_0080: leave IL_008c
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0085;
	}

FINALLY_0085:
	{ // begin finally (depth: 1)
		Il2CppObject * L_21 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_21, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(133)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(133)
	{
		IL2CPP_JUMP_TBL(0x8C, IL_008c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_008c:
	{
		Il2CppObject * L_22 = V_4;
		return L_22;
	}
}
// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::.ctor(System.Func`2<TKey,TValue>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1028554796;
extern const uint32_t ThreadSafeStore_2__ctor_m1318482509_MetadataUsageId;
extern "C"  void ThreadSafeStore_2__ctor_m1318482509_gshared (ThreadSafeStore_2_t3745804690 * __this, Func_2_t1055798018 * ___creator0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeStore_2__ctor_m1318482509_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set__lock_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Func_2_t1055798018 * L_1 = ___creator0;
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_2 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_2, (String_t*)_stringLiteral1028554796, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		Func_2_t1055798018 * L_3 = ___creator0;
		__this->set__creator_2(L_3);
		return;
	}
}
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::Get(TKey)
extern "C"  Il2CppObject * ThreadSafeStore_2_Get_m1258554877_gshared (ThreadSafeStore_2_t3745804690 * __this, TypeConvertKey_t866134174  ___key0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		Dictionary_2_t2917122264 * L_0 = (Dictionary_2_t2917122264 *)__this->get__store_1();
		if (L_0)
		{
			goto IL_00b7;
		}
	}
	{
		bool L_1 = BreakUtil_IsBreak15_m2554688933(NULL /*static, unused*/, (int32_t)((int32_t)-2147483648LL), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_00af;
		}
	}
	{
		V_3 = (int32_t)((int32_t)15211493);
		V_4 = (int32_t)((int32_t)15211559);
		int32_t L_2 = V_3;
		int32_t L_3 = V_4;
		int32_t L_4 = V_4;
		V_3 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2/(int32_t)L_3))%(int32_t)L_4));
		int32_t L_5 = V_3;
		int32_t L_6 = V_4;
		int32_t L_7 = V_3;
		V_4 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5^(int32_t)L_6))&(int32_t)L_7));
		int32_t L_8 = V_3;
		int32_t L_9 = V_4;
		int32_t L_10 = V_4;
		int32_t L_11 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_8^(int32_t)L_9))^(int32_t)L_10))-(int32_t)L_11));
		int32_t L_12 = V_3;
		int32_t L_13 = V_4;
		int32_t L_14 = V_3;
		V_4 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_12+(int32_t)L_13))/(int32_t)L_14));
		int32_t L_15 = V_3;
		int32_t L_16 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)L_16));
		int32_t L_17 = V_3;
		int32_t L_18 = V_4;
		int32_t L_19 = V_4;
		int32_t L_20 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_17/(int32_t)L_18))/(int32_t)L_19))+(int32_t)L_20));
		int32_t L_21 = V_3;
		int32_t L_22 = V_4;
		int32_t L_23 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_21+(int32_t)L_22))-(int32_t)L_23));
	}

IL_00af:
	{
		TypeConvertKey_t866134174  L_24 = ___key0;
		NullCheck((ThreadSafeStore_2_t3745804690 *)__this);
		Il2CppObject * L_25 = ((  Il2CppObject * (*) (ThreadSafeStore_2_t3745804690 *, TypeConvertKey_t866134174 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ThreadSafeStore_2_t3745804690 *)__this, (TypeConvertKey_t866134174 )L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_25;
	}

IL_00b7:
	{
		Dictionary_2_t2917122264 * L_26 = (Dictionary_2_t2917122264 *)__this->get__store_1();
		TypeConvertKey_t866134174  L_27 = ___key0;
		NullCheck((Dictionary_2_t2917122264 *)L_26);
		bool L_28 = ((  bool (*) (Dictionary_2_t2917122264 *, TypeConvertKey_t866134174 , Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Dictionary_2_t2917122264 *)L_26, (TypeConvertKey_t866134174 )L_27, (Il2CppObject **)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		if (L_28)
		{
			goto IL_0154;
		}
	}
	{
		bool L_29 = BreakUtil_IsBreak32_m1967222332(NULL /*static, unused*/, (float)(-2.14963123E+09f), /*hidden argument*/NULL);
		if (L_29)
		{
			goto IL_014c;
		}
	}
	{
		V_1 = (int32_t)((int32_t)15211565);
		V_2 = (int32_t)((int32_t)15211542);
		int32_t L_30 = V_1;
		int32_t L_31 = V_2;
		int32_t L_32 = V_1;
		int32_t L_33 = V_2;
		int32_t L_34 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_30^(int32_t)L_31))^(int32_t)L_32))*(int32_t)L_33))|(int32_t)L_34));
		int32_t L_35 = V_1;
		int32_t L_36 = V_2;
		int32_t L_37 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_35&(int32_t)L_36))+(int32_t)L_37));
		int32_t L_38 = V_1;
		int32_t L_39 = V_2;
		int32_t L_40 = V_1;
		int32_t L_41 = V_2;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_38&(int32_t)L_39))*(int32_t)L_40))/(int32_t)L_41));
		int32_t L_42 = V_1;
		int32_t L_43 = V_2;
		int32_t L_44 = V_1;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_42^(int32_t)L_43))&(int32_t)L_44));
		int32_t L_45 = V_1;
		int32_t L_46 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_45^(int32_t)L_46));
	}

IL_014c:
	{
		TypeConvertKey_t866134174  L_47 = ___key0;
		NullCheck((ThreadSafeStore_2_t3745804690 *)__this);
		Il2CppObject * L_48 = ((  Il2CppObject * (*) (ThreadSafeStore_2_t3745804690 *, TypeConvertKey_t866134174 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ThreadSafeStore_2_t3745804690 *)__this, (TypeConvertKey_t866134174 )L_47, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_48;
	}

IL_0154:
	{
		Il2CppObject * L_49 = V_0;
		return L_49;
	}
}
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::AddValue(TKey)
extern "C"  Il2CppObject * ThreadSafeStore_2_AddValue_m527102645_gshared (ThreadSafeStore_2_t3745804690 * __this, TypeConvertKey_t866134174  ___key0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Dictionary_2_t2917122264 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Func_2_t1055798018 * L_0 = (Func_2_t1055798018 *)__this->get__creator_2();
		TypeConvertKey_t866134174  L_1 = ___key0;
		NullCheck((Func_2_t1055798018 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_2_t1055798018 *, TypeConvertKey_t866134174 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Func_2_t1055798018 *)L_0, (TypeConvertKey_t866134174 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__lock_0();
		V_1 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t2917122264 * L_5 = (Dictionary_2_t2917122264 *)__this->get__store_1();
			if (L_5)
			{
				goto IL_0042;
			}
		}

IL_0025:
		{
			Dictionary_2_t2917122264 * L_6 = (Dictionary_2_t2917122264 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
			((  void (*) (Dictionary_2_t2917122264 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
			__this->set__store_1(L_6);
			Dictionary_2_t2917122264 * L_7 = (Dictionary_2_t2917122264 *)__this->get__store_1();
			TypeConvertKey_t866134174  L_8 = ___key0;
			Il2CppObject * L_9 = V_0;
			NullCheck((Dictionary_2_t2917122264 *)L_7);
			((  void (*) (Dictionary_2_t2917122264 *, TypeConvertKey_t866134174 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2917122264 *)L_7, (TypeConvertKey_t866134174 )L_8, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			goto IL_0078;
		}

IL_0042:
		{
			Dictionary_2_t2917122264 * L_10 = (Dictionary_2_t2917122264 *)__this->get__store_1();
			TypeConvertKey_t866134174  L_11 = ___key0;
			NullCheck((Dictionary_2_t2917122264 *)L_10);
			bool L_12 = ((  bool (*) (Dictionary_2_t2917122264 *, TypeConvertKey_t866134174 , Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Dictionary_2_t2917122264 *)L_10, (TypeConvertKey_t866134174 )L_11, (Il2CppObject **)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
			if (!L_12)
			{
				goto IL_005d;
			}
		}

IL_0055:
		{
			Il2CppObject * L_13 = V_2;
			V_4 = (Il2CppObject *)L_13;
			IL2CPP_LEAVE(0x8C, FINALLY_0085);
		}

IL_005d:
		{
			Dictionary_2_t2917122264 * L_14 = (Dictionary_2_t2917122264 *)__this->get__store_1();
			Dictionary_2_t2917122264 * L_15 = (Dictionary_2_t2917122264 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
			((  void (*) (Dictionary_2_t2917122264 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_15, (Il2CppObject*)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			V_3 = (Dictionary_2_t2917122264 *)L_15;
			Dictionary_2_t2917122264 * L_16 = V_3;
			TypeConvertKey_t866134174  L_17 = ___key0;
			Il2CppObject * L_18 = V_0;
			NullCheck((Dictionary_2_t2917122264 *)L_16);
			((  void (*) (Dictionary_2_t2917122264 *, TypeConvertKey_t866134174 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2917122264 *)L_16, (TypeConvertKey_t866134174 )L_17, (Il2CppObject *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			Dictionary_2_t2917122264 * L_19 = V_3;
			__this->set__store_1(L_19);
		}

IL_0078:
		{
			Il2CppObject * L_20 = V_0;
			V_4 = (Il2CppObject *)L_20;
			IL2CPP_LEAVE(0x8C, FINALLY_0085);
		}

IL_0080:
		{
			; // IL_0080: leave IL_008c
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0085;
	}

FINALLY_0085:
	{ // begin finally (depth: 1)
		Il2CppObject * L_21 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_21, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(133)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(133)
	{
		IL2CPP_JUMP_TBL(0x8C, IL_008c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_008c:
	{
		Il2CppObject * L_22 = V_4;
		return L_22;
	}
}
// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Object,System.Object>::.ctor(System.Func`2<TKey,TValue>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1028554796;
extern const uint32_t ThreadSafeStore_2__ctor_m835646605_MetadataUsageId;
extern "C"  void ThreadSafeStore_2__ctor_m835646605_gshared (ThreadSafeStore_2_t2874570697 * __this, Func_2_t184564025 * ___creator0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeStore_2__ctor_m835646605_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set__lock_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Func_2_t184564025 * L_1 = ___creator0;
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_2 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_2, (String_t*)_stringLiteral1028554796, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		Func_2_t184564025 * L_3 = ___creator0;
		__this->set__creator_2(L_3);
		return;
	}
}
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Object,System.Object>::Get(TKey)
extern "C"  Il2CppObject * ThreadSafeStore_2_Get_m2306648125_gshared (ThreadSafeStore_2_t2874570697 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		Dictionary_2_t2045888271 * L_0 = (Dictionary_2_t2045888271 *)__this->get__store_1();
		if (L_0)
		{
			goto IL_00b7;
		}
	}
	{
		bool L_1 = BreakUtil_IsBreak15_m2554688933(NULL /*static, unused*/, (int32_t)((int32_t)-2147483648LL), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_00af;
		}
	}
	{
		V_3 = (int32_t)((int32_t)15211493);
		V_4 = (int32_t)((int32_t)15211559);
		int32_t L_2 = V_3;
		int32_t L_3 = V_4;
		int32_t L_4 = V_4;
		V_3 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2/(int32_t)L_3))%(int32_t)L_4));
		int32_t L_5 = V_3;
		int32_t L_6 = V_4;
		int32_t L_7 = V_3;
		V_4 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5^(int32_t)L_6))&(int32_t)L_7));
		int32_t L_8 = V_3;
		int32_t L_9 = V_4;
		int32_t L_10 = V_4;
		int32_t L_11 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_8^(int32_t)L_9))^(int32_t)L_10))-(int32_t)L_11));
		int32_t L_12 = V_3;
		int32_t L_13 = V_4;
		int32_t L_14 = V_3;
		V_4 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_12+(int32_t)L_13))/(int32_t)L_14));
		int32_t L_15 = V_3;
		int32_t L_16 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)L_16));
		int32_t L_17 = V_3;
		int32_t L_18 = V_4;
		int32_t L_19 = V_4;
		int32_t L_20 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_17/(int32_t)L_18))/(int32_t)L_19))+(int32_t)L_20));
		int32_t L_21 = V_3;
		int32_t L_22 = V_4;
		int32_t L_23 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_21+(int32_t)L_22))-(int32_t)L_23));
	}

IL_00af:
	{
		Il2CppObject * L_24 = ___key0;
		NullCheck((ThreadSafeStore_2_t2874570697 *)__this);
		Il2CppObject * L_25 = ((  Il2CppObject * (*) (ThreadSafeStore_2_t2874570697 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ThreadSafeStore_2_t2874570697 *)__this, (Il2CppObject *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_25;
	}

IL_00b7:
	{
		Dictionary_2_t2045888271 * L_26 = (Dictionary_2_t2045888271 *)__this->get__store_1();
		Il2CppObject * L_27 = ___key0;
		NullCheck((Dictionary_2_t2045888271 *)L_26);
		bool L_28 = ((  bool (*) (Dictionary_2_t2045888271 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Dictionary_2_t2045888271 *)L_26, (Il2CppObject *)L_27, (Il2CppObject **)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		if (L_28)
		{
			goto IL_0154;
		}
	}
	{
		bool L_29 = BreakUtil_IsBreak32_m1967222332(NULL /*static, unused*/, (float)(-2.14963123E+09f), /*hidden argument*/NULL);
		if (L_29)
		{
			goto IL_014c;
		}
	}
	{
		V_1 = (int32_t)((int32_t)15211565);
		V_2 = (int32_t)((int32_t)15211542);
		int32_t L_30 = V_1;
		int32_t L_31 = V_2;
		int32_t L_32 = V_1;
		int32_t L_33 = V_2;
		int32_t L_34 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_30^(int32_t)L_31))^(int32_t)L_32))*(int32_t)L_33))|(int32_t)L_34));
		int32_t L_35 = V_1;
		int32_t L_36 = V_2;
		int32_t L_37 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_35&(int32_t)L_36))+(int32_t)L_37));
		int32_t L_38 = V_1;
		int32_t L_39 = V_2;
		int32_t L_40 = V_1;
		int32_t L_41 = V_2;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_38&(int32_t)L_39))*(int32_t)L_40))/(int32_t)L_41));
		int32_t L_42 = V_1;
		int32_t L_43 = V_2;
		int32_t L_44 = V_1;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_42^(int32_t)L_43))&(int32_t)L_44));
		int32_t L_45 = V_1;
		int32_t L_46 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_45^(int32_t)L_46));
	}

IL_014c:
	{
		Il2CppObject * L_47 = ___key0;
		NullCheck((ThreadSafeStore_2_t2874570697 *)__this);
		Il2CppObject * L_48 = ((  Il2CppObject * (*) (ThreadSafeStore_2_t2874570697 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ThreadSafeStore_2_t2874570697 *)__this, (Il2CppObject *)L_47, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_48;
	}

IL_0154:
	{
		Il2CppObject * L_49 = V_0;
		return L_49;
	}
}
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Object,System.Object>::AddValue(TKey)
extern "C"  Il2CppObject * ThreadSafeStore_2_AddValue_m171817077_gshared (ThreadSafeStore_2_t2874570697 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Dictionary_2_t2045888271 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Func_2_t184564025 * L_0 = (Func_2_t184564025 *)__this->get__creator_2();
		Il2CppObject * L_1 = ___key0;
		NullCheck((Func_2_t184564025 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_2_t184564025 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Func_2_t184564025 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__lock_0();
		V_1 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t2045888271 * L_5 = (Dictionary_2_t2045888271 *)__this->get__store_1();
			if (L_5)
			{
				goto IL_0042;
			}
		}

IL_0025:
		{
			Dictionary_2_t2045888271 * L_6 = (Dictionary_2_t2045888271 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
			((  void (*) (Dictionary_2_t2045888271 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
			__this->set__store_1(L_6);
			Dictionary_2_t2045888271 * L_7 = (Dictionary_2_t2045888271 *)__this->get__store_1();
			Il2CppObject * L_8 = ___key0;
			Il2CppObject * L_9 = V_0;
			NullCheck((Dictionary_2_t2045888271 *)L_7);
			((  void (*) (Dictionary_2_t2045888271 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2045888271 *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			goto IL_0078;
		}

IL_0042:
		{
			Dictionary_2_t2045888271 * L_10 = (Dictionary_2_t2045888271 *)__this->get__store_1();
			Il2CppObject * L_11 = ___key0;
			NullCheck((Dictionary_2_t2045888271 *)L_10);
			bool L_12 = ((  bool (*) (Dictionary_2_t2045888271 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Dictionary_2_t2045888271 *)L_10, (Il2CppObject *)L_11, (Il2CppObject **)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
			if (!L_12)
			{
				goto IL_005d;
			}
		}

IL_0055:
		{
			Il2CppObject * L_13 = V_2;
			V_4 = (Il2CppObject *)L_13;
			IL2CPP_LEAVE(0x8C, FINALLY_0085);
		}

IL_005d:
		{
			Dictionary_2_t2045888271 * L_14 = (Dictionary_2_t2045888271 *)__this->get__store_1();
			Dictionary_2_t2045888271 * L_15 = (Dictionary_2_t2045888271 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
			((  void (*) (Dictionary_2_t2045888271 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_15, (Il2CppObject*)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			V_3 = (Dictionary_2_t2045888271 *)L_15;
			Dictionary_2_t2045888271 * L_16 = V_3;
			Il2CppObject * L_17 = ___key0;
			Il2CppObject * L_18 = V_0;
			NullCheck((Dictionary_2_t2045888271 *)L_16);
			((  void (*) (Dictionary_2_t2045888271 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2045888271 *)L_16, (Il2CppObject *)L_17, (Il2CppObject *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			Dictionary_2_t2045888271 * L_19 = V_3;
			__this->set__store_1(L_19);
		}

IL_0078:
		{
			Il2CppObject * L_20 = V_0;
			V_4 = (Il2CppObject *)L_20;
			IL2CPP_LEAVE(0x8C, FINALLY_0085);
		}

IL_0080:
		{
			; // IL_0080: leave IL_008c
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0085;
	}

FINALLY_0085:
	{ // begin finally (depth: 1)
		Il2CppObject * L_21 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_21, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(133)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(133)
	{
		IL2CPP_JUMP_TBL(0x8C, IL_008c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_008c:
	{
		Il2CppObject * L_22 = V_4;
		return L_22;
	}
}
// System.Void Pathfinding.PathPool`1<System.Object>::.cctor()
extern "C"  void PathPool_1__cctor_m4111071586_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Stack_1_t2974409999 * L_0 = (Stack_1_t2974409999 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Stack_1_t2974409999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((PathPool_1_t4134265632_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_pool_0(L_0);
		return;
	}
}
// System.Void Pathfinding.PathPool`1<System.Object>::Recycle(T)
extern "C"  void PathPool_1_Recycle_m1360667714_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___path0, const MethodInfo* method)
{
	Stack_1_t2974409999 * V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Stack_1_t2974409999 * L_0 = ((PathPool_1_t4134265632_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
		V_0 = (Stack_1_t2974409999 *)L_0;
		Stack_1_t2974409999 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		Il2CppObject * L_2 = ___path0;
		NullCheck(L_2);
		((Path_t1974241691 *)L_2)->set_recycled_17((bool)1);
		NullCheck((Path_t1974241691 *)(*(&___path0)));
		VirtActionInvoker0::Invoke(6 /* System.Void Pathfinding.Path::OnEnterPool() */, (Path_t1974241691 *)(*(&___path0)));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Stack_1_t2974409999 * L_3 = ((PathPool_1_t4134265632_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
		Il2CppObject * L_4 = ___path0;
		NullCheck((Stack_1_t2974409999 *)L_3);
		((  void (*) (Stack_1_t2974409999 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((Stack_1_t2974409999 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		IL2CPP_LEAVE(0x3C, FINALLY_0035);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0035;
	}

FINALLY_0035:
	{ // begin finally (depth: 1)
		Stack_1_t2974409999 * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(53)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(53)
	{
		IL2CPP_JUMP_TBL(0x3C, IL_003c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_003c:
	{
		return;
	}
}
// System.Void Pathfinding.PathPool`1<System.Object>::Warmup(System.Int32,System.Int32)
extern Il2CppClass* ListPool_1_t2604082720_il2cpp_TypeInfo_var;
extern Il2CppClass* ListPool_1_t2567569620_il2cpp_TypeInfo_var;
extern Il2CppClass* PathU5BU5D_t789302682_il2cpp_TypeInfo_var;
extern Il2CppClass* PathPool_1_t4134265632_il2cpp_TypeInfo_var;
extern const MethodInfo* ListPool_1_Warmup_m2621473270_MethodInfo_var;
extern const MethodInfo* ListPool_1_Warmup_m707677801_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1447;
extern const uint32_t PathPool_1_Warmup_m4243013421_MetadataUsageId;
extern "C"  void PathPool_1_Warmup_m4243013421_gshared (Il2CppObject * __this /* static, unused */, int32_t ___count0, int32_t ___length1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PathPool_1_Warmup_m4243013421_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	PathU5BU5D_t789302682* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	{
		int32_t L_0 = ___count0;
		int32_t L_1 = ___length1;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t2604082720_il2cpp_TypeInfo_var);
		ListPool_1_Warmup_m2621473270(NULL /*static, unused*/, (int32_t)L_0, (int32_t)L_1, /*hidden argument*/ListPool_1_Warmup_m2621473270_MethodInfo_var);
		int32_t L_2 = ___count0;
		int32_t L_3 = ___length1;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t2567569620_il2cpp_TypeInfo_var);
		ListPool_1_Warmup_m707677801(NULL /*static, unused*/, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/ListPool_1_Warmup_m707677801_MethodInfo_var);
		int32_t L_4 = ___count0;
		V_0 = (PathU5BU5D_t789302682*)((PathU5BU5D_t789302682*)SZArrayNew(PathU5BU5D_t789302682_il2cpp_TypeInfo_var, (uint32_t)L_4));
		V_1 = (int32_t)0;
		goto IL_00d5;
	}
	// Dead block : IL_001c: ldstr "9"
	// Dead block : IL_002b: ldc.i4 15211484

IL_00bb:
	{
		PathU5BU5D_t789302682* L_5 = V_0;
		int32_t L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Path_t1974241691 *)L_7);
		PathU5BU5D_t789302682* L_8 = V_0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		Path_t1974241691 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		PathU5BU5D_t789302682* L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PathPool_1_t4134265632_il2cpp_TypeInfo_var);
		PathPool_1_ilo_Claim1_m992502912(NULL /*static, unused*/, (Path_t1974241691 *)L_11, (Il2CppObject *)(Il2CppObject *)L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_00d5:
	{
		int32_t L_14 = V_1;
		int32_t L_15 = ___count0;
		if ((((int32_t)L_14) < ((int32_t)L_15)))
		{
			goto IL_00bb;
		}
	}
	{
		bool L_16 = BreakUtil_IsBreak4_m462181020(NULL /*static, unused*/, (String_t*)_stringLiteral1447, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_0147;
		}
	}
	{
		V_7 = (int32_t)((int32_t)15211485);
		V_8 = (int32_t)((int32_t)15211531);
		int32_t L_17 = V_7;
		int32_t L_18 = V_8;
		int32_t L_19 = V_7;
		int32_t L_20 = V_8;
		V_7 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_17^(int32_t)L_18))%(int32_t)L_19))/(int32_t)L_20));
		int32_t L_21 = V_7;
		int32_t L_22 = V_8;
		int32_t L_23 = V_8;
		V_7 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_21^(int32_t)L_22))&(int32_t)L_23));
		int32_t L_24 = V_7;
		int32_t L_25 = V_8;
		V_8 = (int32_t)((int32_t)((int32_t)L_24|(int32_t)L_25));
		int32_t L_26 = V_7;
		int32_t L_27 = V_8;
		int32_t L_28 = V_7;
		int32_t L_29 = V_8;
		V_7 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_26&(int32_t)L_27))^(int32_t)L_28))*(int32_t)L_29));
	}

IL_0147:
	{
		V_2 = (int32_t)0;
		goto IL_01b4;
	}
	// Dead block : IL_014e: ldstr "-38"
	// Dead block : IL_015a: ldc.i4 15211526

IL_01a7:
	{
		PathU5BU5D_t789302682* L_30 = V_0;
		int32_t L_31 = V_2;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, L_31);
		int32_t L_32 = L_31;
		Path_t1974241691 * L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		PathU5BU5D_t789302682* L_34 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PathPool_1_t4134265632_il2cpp_TypeInfo_var);
		PathPool_1_ilo_Release2_m2573264726(NULL /*static, unused*/, (Path_t1974241691 *)L_33, (Il2CppObject *)(Il2CppObject *)L_34, /*hidden argument*/NULL);
		int32_t L_35 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_01b4:
	{
		int32_t L_36 = V_2;
		int32_t L_37 = ___count0;
		if ((((int32_t)L_36) < ((int32_t)L_37)))
		{
			goto IL_01a7;
		}
	}
	{
		bool L_38 = BreakUtil_IsBreak35_m1533620579(NULL /*static, unused*/, (int32_t)((int32_t)-2147483648LL), /*hidden argument*/NULL);
		if (L_38)
		{
			goto IL_0278;
		}
	}
	{
		V_3 = (int32_t)((int32_t)15211527);
		V_4 = (int32_t)((int32_t)15211550);
		int32_t L_39 = V_3;
		int32_t L_40 = V_4;
		int32_t L_41 = V_3;
		int32_t L_42 = V_4;
		V_3 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_39*(int32_t)L_40))-(int32_t)L_41))*(int32_t)L_42));
		int32_t L_43 = V_3;
		int32_t L_44 = V_4;
		int32_t L_45 = V_3;
		int32_t L_46 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_43*(int32_t)L_44))+(int32_t)L_45))-(int32_t)L_46));
		int32_t L_47 = V_3;
		int32_t L_48 = V_4;
		int32_t L_49 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_47*(int32_t)L_48))*(int32_t)L_49));
		int32_t L_50 = V_3;
		int32_t L_51 = V_4;
		int32_t L_52 = V_3;
		int32_t L_53 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_50|(int32_t)L_51))%(int32_t)L_52))-(int32_t)L_53));
		int32_t L_54 = V_3;
		int32_t L_55 = V_4;
		int32_t L_56 = V_3;
		int32_t L_57 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_54-(int32_t)L_55))%(int32_t)L_56))-(int32_t)L_57));
		int32_t L_58 = V_3;
		int32_t L_59 = V_4;
		int32_t L_60 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_58%(int32_t)L_59))&(int32_t)L_60));
		int32_t L_61 = V_3;
		int32_t L_62 = V_4;
		int32_t L_63 = V_3;
		int32_t L_64 = V_4;
		int32_t L_65 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_61-(int32_t)L_62))/(int32_t)L_63))-(int32_t)L_64))^(int32_t)L_65));
	}

IL_0278:
	{
		return;
	}
}
// System.Int32 Pathfinding.PathPool`1<System.Object>::GetTotalCreated()
extern "C"  int32_t PathPool_1_GetTotalCreated_m3938489809_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		int32_t L_0 = ((PathPool_1_t4134265632_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_totalCreated_1();
		return L_0;
	}
}
// System.Int32 Pathfinding.PathPool`1<System.Object>::GetSize()
extern "C"  int32_t PathPool_1_GetSize_m2941120398_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Stack_1_t2974409999 * L_0 = ((PathPool_1_t4134265632_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
		NullCheck((Stack_1_t2974409999 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t2974409999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)((Stack_1_t2974409999 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_1;
	}
}
// T Pathfinding.PathPool`1<System.Object>::GetPath()
extern "C"  Il2CppObject * PathPool_1_GetPath_m3122724387_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Stack_1_t2974409999 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Stack_1_t2974409999 * L_0 = ((PathPool_1_t4134265632_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
		V_0 = (Stack_1_t2974409999 *)L_0;
		Stack_1_t2974409999 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			Stack_1_t2974409999 * L_2 = ((PathPool_1_t4134265632_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
			NullCheck((Stack_1_t2974409999 *)L_2);
			int32_t L_3 = ((  int32_t (*) (Stack_1_t2974409999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)((Stack_1_t2974409999 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			if ((((int32_t)L_3) <= ((int32_t)0)))
			{
				goto IL_002c;
			}
		}

IL_001c:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			Stack_1_t2974409999 * L_4 = ((PathPool_1_t4134265632_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
			NullCheck((Stack_1_t2974409999 *)L_4);
			Il2CppObject * L_5 = ((  Il2CppObject * (*) (Stack_1_t2974409999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)((Stack_1_t2974409999 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			V_1 = (Il2CppObject *)L_5;
			goto IL_003e;
		}

IL_002c:
		{
			Il2CppObject * L_6 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			V_1 = (Il2CppObject *)L_6;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			int32_t L_7 = ((PathPool_1_t4134265632_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_totalCreated_1();
			((PathPool_1_t4134265632_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_totalCreated_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		}

IL_003e:
		{
			Il2CppObject * L_8 = V_1;
			NullCheck(L_8);
			((Path_t1974241691 *)L_8)->set_recycled_17((bool)0);
			NullCheck((Path_t1974241691 *)(*(&V_1)));
			VirtActionInvoker0::Invoke(7 /* System.Void Pathfinding.Path::Reset() */, (Path_t1974241691 *)(*(&V_1)));
			Il2CppObject * L_9 = V_1;
			V_2 = (Il2CppObject *)L_9;
			IL2CPP_LEAVE(0x6A, FINALLY_0063);
		}

IL_005e:
		{
			; // IL_005e: leave IL_006a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0063;
	}

FINALLY_0063:
	{ // begin finally (depth: 1)
		Stack_1_t2974409999 * L_10 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_10, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(99)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(99)
	{
		IL2CPP_JUMP_TBL(0x6A, IL_006a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_006a:
	{
		Il2CppObject * L_11 = V_2;
		return L_11;
	}
}
// System.Void Pathfinding.PathPool`1<System.Object>::ilo_Claim1(Pathfinding.Path,System.Object)
extern "C"  void PathPool_1_ilo_Claim1_m992502912_gshared (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, Il2CppObject * ___o1, const MethodInfo* method)
{
	{
		Path_t1974241691 * L_0 = ____this0;
		Il2CppObject * L_1 = ___o1;
		NullCheck((Path_t1974241691 *)L_0);
		Path_Claim_m587811278((Path_t1974241691 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.PathPool`1<System.Object>::ilo_Release2(Pathfinding.Path,System.Object)
extern "C"  void PathPool_1_ilo_Release2_m2573264726_gshared (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, Il2CppObject * ___o1, const MethodInfo* method)
{
	{
		Path_t1974241691 * L_0 = ____this0;
		Il2CppObject * L_1 = ___o1;
		NullCheck((Path_t1974241691 *)L_0);
		Path_Release_m3110428899((Path_t1974241691 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.FixedArray3`1/<Enumerate>c__Iterator0<System.Object>::.ctor()
extern "C"  void U3CEnumerateU3Ec__Iterator0__ctor_m71689204_gshared (U3CEnumerateU3Ec__Iterator0_t181911054 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T Pathfinding.Poly2Tri.FixedArray3`1/<Enumerate>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CEnumerateU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m3596731643_gshared (U3CEnumerateU3Ec__Iterator0_t181911054 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object Pathfinding.Poly2Tri.FixedArray3`1/<Enumerate>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEnumerateU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1485434226_gshared (U3CEnumerateU3Ec__Iterator0_t181911054 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Collections.IEnumerator Pathfinding.Poly2Tri.FixedArray3`1/<Enumerate>c__Iterator0<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CEnumerateU3Ec__Iterator0_System_Collections_IEnumerable_GetEnumerator_m1705069515_gshared (U3CEnumerateU3Ec__Iterator0_t181911054 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CEnumerateU3Ec__Iterator0_t181911054 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CEnumerateU3Ec__Iterator0_t181911054 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CEnumerateU3Ec__Iterator0_t181911054 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<T> Pathfinding.Poly2Tri.FixedArray3`1/<Enumerate>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CEnumerateU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m410823166_gshared (U3CEnumerateU3Ec__Iterator0_t181911054 * __this, const MethodInfo* method)
{
	U3CEnumerateU3Ec__Iterator0_t181911054 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_1();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CEnumerateU3Ec__Iterator0_t181911054 * L_2 = (U3CEnumerateU3Ec__Iterator0_t181911054 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CEnumerateU3Ec__Iterator0_t181911054 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CEnumerateU3Ec__Iterator0_t181911054 *)L_2;
		U3CEnumerateU3Ec__Iterator0_t181911054 * L_3 = V_0;
		FixedArray3_1_t526322725  L_4 = (FixedArray3_1_t526322725 )__this->get_U3CU3Ef__this_3();
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_3(L_4);
		U3CEnumerateU3Ec__Iterator0_t181911054 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean Pathfinding.Poly2Tri.FixedArray3`1/<Enumerate>c__Iterator0<System.Object>::MoveNext()
extern "C"  bool U3CEnumerateU3Ec__Iterator0_MoveNext_m2412220510_gshared (U3CEnumerateU3Ec__Iterator0_t181911054 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0050;
		}
	}
	{
		goto IL_0071;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_005e;
	}

IL_002d:
	{
		FixedArray3_1_t526322725 * L_2 = (FixedArray3_1_t526322725 *)__this->get_address_of_U3CU3Ef__this_3();
		int32_t L_3 = (int32_t)__this->get_U3CiU3E__0_0();
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((FixedArray3_1_t526322725 *)L_2), (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		__this->set_U24current_2(L_4);
		__this->set_U24PC_1(1);
		goto IL_0073;
	}

IL_0050:
	{
		int32_t L_5 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_5+(int32_t)1)));
	}

IL_005e:
	{
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		if ((((int32_t)L_6) < ((int32_t)3)))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0071:
	{
		return (bool)0;
	}

IL_0073:
	{
		return (bool)1;
	}
}
// System.Void Pathfinding.Poly2Tri.FixedArray3`1/<Enumerate>c__Iterator0<System.Object>::Dispose()
extern "C"  void U3CEnumerateU3Ec__Iterator0_Dispose_m72131377_gshared (U3CEnumerateU3Ec__Iterator0_t181911054 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.FixedArray3`1/<Enumerate>c__Iterator0<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CEnumerateU3Ec__Iterator0_Reset_m2013089441_MetadataUsageId;
extern "C"  void U3CEnumerateU3Ec__Iterator0_Reset_m2013089441_gshared (U3CEnumerateU3Ec__Iterator0_t181911054 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CEnumerateU3Ec__Iterator0_Reset_m2013089441_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.IEnumerator Pathfinding.Poly2Tri.FixedArray3`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * FixedArray3_1_System_Collections_IEnumerable_GetEnumerator_m4035815537_gshared (FixedArray3_1_t526322725 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((FixedArray3_1_t526322725 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
extern "C"  Il2CppObject * FixedArray3_1_System_Collections_IEnumerable_GetEnumerator_m4035815537_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	FixedArray3_1_t526322725 * _thisAdjusted = reinterpret_cast<FixedArray3_1_t526322725 *>(__this + 1);
	return FixedArray3_1_System_Collections_IEnumerable_GetEnumerator_m4035815537(_thisAdjusted, method);
}
// T Pathfinding.Poly2Tri.FixedArray3`1<System.Object>::get_Item(System.Int32)
extern Il2CppClass* IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var;
extern const uint32_t FixedArray3_1_get_Item_m2156685124_MetadataUsageId;
extern "C"  Il2CppObject * FixedArray3_1_get_Item_m2156685124_gshared (FixedArray3_1_t526322725 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FixedArray3_1_get_Item_m2156685124_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0019;
		}
		if (L_1 == 1)
		{
			goto IL_0020;
		}
		if (L_1 == 2)
		{
			goto IL_0027;
		}
	}
	{
		goto IL_002e;
	}

IL_0019:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get__0_0();
		return L_2;
	}

IL_0020:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__1_1();
		return L_3;
	}

IL_0027:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__2_2();
		return L_4;
	}

IL_002e:
	{
		IndexOutOfRangeException_t3456360697 * L_5 = (IndexOutOfRangeException_t3456360697 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m707236112(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}
}
extern "C"  Il2CppObject * FixedArray3_1_get_Item_m2156685124_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, const MethodInfo* method)
{
	FixedArray3_1_t526322725 * _thisAdjusted = reinterpret_cast<FixedArray3_1_t526322725 *>(__this + 1);
	return FixedArray3_1_get_Item_m2156685124(_thisAdjusted, ___index0, method);
}
// System.Void Pathfinding.Poly2Tri.FixedArray3`1<System.Object>::set_Item(System.Int32,T)
extern Il2CppClass* IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var;
extern const uint32_t FixedArray3_1_set_Item_m851048943_MetadataUsageId;
extern "C"  void FixedArray3_1_set_Item_m851048943_gshared (FixedArray3_1_t526322725 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FixedArray3_1_set_Item_m851048943_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0019;
		}
		if (L_1 == 1)
		{
			goto IL_0025;
		}
		if (L_1 == 2)
		{
			goto IL_0031;
		}
	}
	{
		goto IL_003d;
	}

IL_0019:
	{
		Il2CppObject * L_2 = ___value1;
		__this->set__0_0(L_2);
		goto IL_0043;
	}

IL_0025:
	{
		Il2CppObject * L_3 = ___value1;
		__this->set__1_1(L_3);
		goto IL_0043;
	}

IL_0031:
	{
		Il2CppObject * L_4 = ___value1;
		__this->set__2_2(L_4);
		goto IL_0043;
	}

IL_003d:
	{
		IndexOutOfRangeException_t3456360697 * L_5 = (IndexOutOfRangeException_t3456360697 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m707236112(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0043:
	{
		return;
	}
}
extern "C"  void FixedArray3_1_set_Item_m851048943_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	FixedArray3_1_t526322725 * _thisAdjusted = reinterpret_cast<FixedArray3_1_t526322725 *>(__this + 1);
	FixedArray3_1_set_Item_m851048943(_thisAdjusted, ___index0, ___value1, method);
}
// System.Boolean Pathfinding.Poly2Tri.FixedArray3`1<System.Object>::Contains(T)
extern "C"  bool FixedArray3_1_Contains_m872077375_gshared (FixedArray3_1_t526322725 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_0024;
	}

IL_0007:
	{
		int32_t L_0 = V_0;
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((FixedArray3_1_t526322725 *)__this), (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_2 = ___value0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0020;
		}
	}
	{
		return (bool)1;
	}

IL_0020:
	{
		int32_t L_3 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_0024:
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)3)))
		{
			goto IL_0007;
		}
	}
	{
		return (bool)0;
	}
}
extern "C"  bool FixedArray3_1_Contains_m872077375_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	FixedArray3_1_t526322725 * _thisAdjusted = reinterpret_cast<FixedArray3_1_t526322725 *>(__this + 1);
	return FixedArray3_1_Contains_m872077375(_thisAdjusted, ___value0, method);
}
// System.Int32 Pathfinding.Poly2Tri.FixedArray3`1<System.Object>::IndexOf(T)
extern "C"  int32_t FixedArray3_1_IndexOf_m548310489_gshared (FixedArray3_1_t526322725 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_0024;
	}

IL_0007:
	{
		int32_t L_0 = V_0;
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((FixedArray3_1_t526322725 *)__this), (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_2 = ___value0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0020:
	{
		int32_t L_4 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0024:
	{
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) < ((int32_t)3)))
		{
			goto IL_0007;
		}
	}
	{
		return (-1);
	}
}
extern "C"  int32_t FixedArray3_1_IndexOf_m548310489_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	FixedArray3_1_t526322725 * _thisAdjusted = reinterpret_cast<FixedArray3_1_t526322725 *>(__this + 1);
	return FixedArray3_1_IndexOf_m548310489(_thisAdjusted, ___value0, method);
}
// System.Void Pathfinding.Poly2Tri.FixedArray3`1<System.Object>::Clear()
extern "C"  void FixedArray3_1_Clear_m1266883641_gshared (FixedArray3_1_t526322725 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		V_0 = (Il2CppObject *)L_0;
		__this->set__2_2(L_0);
		Il2CppObject * L_1 = V_0;
		Il2CppObject * L_2 = (Il2CppObject *)L_1;
		V_0 = (Il2CppObject *)L_2;
		__this->set__1_1(L_2);
		Il2CppObject * L_3 = V_0;
		__this->set__0_0(L_3);
		return;
	}
}
extern "C"  void FixedArray3_1_Clear_m1266883641_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	FixedArray3_1_t526322725 * _thisAdjusted = reinterpret_cast<FixedArray3_1_t526322725 *>(__this + 1);
	FixedArray3_1_Clear_m1266883641(_thisAdjusted, method);
}
// System.Collections.Generic.IEnumerable`1<T> Pathfinding.Poly2Tri.FixedArray3`1<System.Object>::Enumerate()
extern "C"  Il2CppObject* FixedArray3_1_Enumerate_m2474609968_gshared (FixedArray3_1_t526322725 * __this, const MethodInfo* method)
{
	U3CEnumerateU3Ec__Iterator0_t181911054 * V_0 = NULL;
	{
		U3CEnumerateU3Ec__Iterator0_t181911054 * L_0 = (U3CEnumerateU3Ec__Iterator0_t181911054 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (U3CEnumerateU3Ec__Iterator0_t181911054 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_0 = (U3CEnumerateU3Ec__Iterator0_t181911054 *)L_0;
		U3CEnumerateU3Ec__Iterator0_t181911054 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3((*(FixedArray3_1_t526322725 *)__this));
		U3CEnumerateU3Ec__Iterator0_t181911054 * L_2 = V_0;
		U3CEnumerateU3Ec__Iterator0_t181911054 * L_3 = (U3CEnumerateU3Ec__Iterator0_t181911054 *)L_2;
		NullCheck(L_3);
		L_3->set_U24PC_1(((int32_t)-2));
		return L_3;
	}
}
extern "C"  Il2CppObject* FixedArray3_1_Enumerate_m2474609968_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	FixedArray3_1_t526322725 * _thisAdjusted = reinterpret_cast<FixedArray3_1_t526322725 *>(__this + 1);
	return FixedArray3_1_Enumerate_m2474609968(_thisAdjusted, method);
}
// System.Collections.Generic.IEnumerator`1<T> Pathfinding.Poly2Tri.FixedArray3`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* FixedArray3_1_GetEnumerator_m983331714_gshared (FixedArray3_1_t526322725 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((FixedArray3_1_t526322725 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_0);
		return L_1;
	}
}
extern "C"  Il2CppObject* FixedArray3_1_GetEnumerator_m983331714_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	FixedArray3_1_t526322725 * _thisAdjusted = reinterpret_cast<FixedArray3_1_t526322725 *>(__this + 1);
	return FixedArray3_1_GetEnumerator_m983331714(_thisAdjusted, method);
}
// System.Void Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.DateTime>::.ctor(System.Object,System.IntPtr)
extern "C"  void WriteDelegate_1__ctor_m3341752708_gshared (WriteDelegate_1_t4036178619 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.DateTime>::Invoke(Pathfinding.Serialization.JsonFx.JsonWriter,T)
extern "C"  void WriteDelegate_1_Invoke_m3448592485_gshared (WriteDelegate_1_t4036178619 * __this, JsonWriter_t541860733 * ___writer0, DateTime_t4283661327  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		WriteDelegate_1_Invoke_m3448592485((WriteDelegate_1_t4036178619 *)__this->get_prev_9(),___writer0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, JsonWriter_t541860733 * ___writer0, DateTime_t4283661327  ___value1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___writer0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, JsonWriter_t541860733 * ___writer0, DateTime_t4283661327  ___value1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___writer0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, DateTime_t4283661327  ___value1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___writer0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.DateTime>::BeginInvoke(Pathfinding.Serialization.JsonFx.JsonWriter,T,System.AsyncCallback,System.Object)
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern const uint32_t WriteDelegate_1_BeginInvoke_m2054724560_MetadataUsageId;
extern "C"  Il2CppObject * WriteDelegate_1_BeginInvoke_m2054724560_gshared (WriteDelegate_1_t4036178619 * __this, JsonWriter_t541860733 * ___writer0, DateTime_t4283661327  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WriteDelegate_1_BeginInvoke_m2054724560_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___writer0;
	__d_args[1] = Box(DateTime_t4283661327_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.DateTime>::EndInvoke(System.IAsyncResult)
extern "C"  void WriteDelegate_1_EndInvoke_m854846868_gshared (WriteDelegate_1_t4036178619 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void WriteDelegate_1__ctor_m2655339744_gshared (WriteDelegate_1_t3923333663 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.Object>::Invoke(Pathfinding.Serialization.JsonFx.JsonWriter,T)
extern "C"  void WriteDelegate_1_Invoke_m1877104521_gshared (WriteDelegate_1_t3923333663 * __this, JsonWriter_t541860733 * ___writer0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		WriteDelegate_1_Invoke_m1877104521((WriteDelegate_1_t3923333663 *)__this->get_prev_9(),___writer0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, JsonWriter_t541860733 * ___writer0, Il2CppObject * ___value1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___writer0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, JsonWriter_t541860733 * ___writer0, Il2CppObject * ___value1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___writer0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___value1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___writer0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.Object>::BeginInvoke(Pathfinding.Serialization.JsonFx.JsonWriter,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * WriteDelegate_1_BeginInvoke_m2874685428_gshared (WriteDelegate_1_t3923333663 * __this, JsonWriter_t541860733 * ___writer0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___writer0;
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void WriteDelegate_1_EndInvoke_m3602786032_gshared (WriteDelegate_1_t3923333663 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.ClipperLib.IntPoint>::.cctor()
extern "C"  void ListPool_1__cctor_m2090068626_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		List_1_t1767529987 * L_0 = (List_1_t1767529987 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (List_1_t1767529987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_pool_1(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<Pathfinding.ClipperLib.IntPoint>::Claim()
extern "C"  List_1_t399344435 * ListPool_1_Claim_m3142459616_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t1767529987 * V_0 = NULL;
	List_1_t399344435 * V_1 = NULL;
	List_1_t399344435 * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t1767529987 * L_0 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t1767529987 *)L_0;
		List_1_t1767529987 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t1767529987 * L_2 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t1767529987 *)L_2);
			int32_t L_3 = ((  int32_t (*) (List_1_t1767529987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1767529987 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_3) <= ((int32_t)0)))
			{
				goto IL_0050;
			}
		}

IL_001c:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t1767529987 * L_4 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t1767529987 * L_5 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t1767529987 *)L_5);
			int32_t L_6 = ((  int32_t (*) (List_1_t1767529987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1767529987 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t1767529987 *)L_4);
			List_1_t399344435 * L_7 = ((  List_1_t399344435 * (*) (List_1_t1767529987 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t1767529987 *)L_4, (int32_t)((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			V_1 = (List_1_t399344435 *)L_7;
			List_1_t1767529987 * L_8 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t1767529987 * L_9 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t1767529987 *)L_9);
			int32_t L_10 = ((  int32_t (*) (List_1_t1767529987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1767529987 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t1767529987 *)L_8);
			((  void (*) (List_1_t1767529987 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t1767529987 *)L_8, (int32_t)((int32_t)((int32_t)L_10-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			List_1_t399344435 * L_11 = V_1;
			V_2 = (List_1_t399344435 *)L_11;
			IL2CPP_LEAVE(0x67, FINALLY_0060);
		}

IL_0050:
		{
			List_1_t399344435 * L_12 = (List_1_t399344435 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t399344435 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			V_2 = (List_1_t399344435 *)L_12;
			IL2CPP_LEAVE(0x67, FINALLY_0060);
		}

IL_005b:
		{
			; // IL_005b: leave IL_0067
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0060;
	}

FINALLY_0060:
	{ // begin finally (depth: 1)
		List_1_t1767529987 * L_13 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_13, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(96)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(96)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0067:
	{
		List_1_t399344435 * L_14 = V_2;
		return L_14;
	}
}
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<Pathfinding.ClipperLib.IntPoint>::Claim(System.Int32)
extern "C"  List_1_t399344435 * ListPool_1_Claim_m1615955505_gshared (Il2CppObject * __this /* static, unused */, int32_t ___capacity0, const MethodInfo* method)
{
	List_1_t1767529987 * V_0 = NULL;
	List_1_t399344435 * V_1 = NULL;
	int32_t V_2 = 0;
	List_1_t399344435 * V_3 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t1767529987 * L_0 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t1767529987 *)L_0;
		List_1_t1767529987 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t1767529987 * L_2 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t1767529987 *)L_2);
			int32_t L_3 = ((  int32_t (*) (List_1_t1767529987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1767529987 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_3) <= ((int32_t)0)))
			{
				goto IL_00e6;
			}
		}

IL_001c:
		{
			V_1 = (List_1_t399344435 *)NULL;
			V_2 = (int32_t)0;
			goto IL_006d;
		}

IL_0025:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t1767529987 * L_4 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t1767529987 * L_5 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t1767529987 *)L_5);
			int32_t L_6 = ((  int32_t (*) (List_1_t1767529987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1767529987 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_7 = V_2;
			NullCheck((List_1_t1767529987 *)L_4);
			List_1_t399344435 * L_8 = ((  List_1_t399344435 * (*) (List_1_t1767529987 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t1767529987 *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			V_1 = (List_1_t399344435 *)L_8;
			List_1_t399344435 * L_9 = V_1;
			NullCheck((List_1_t399344435 *)L_9);
			int32_t L_10 = ((  int32_t (*) (List_1_t399344435 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t399344435 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			int32_t L_11 = ___capacity0;
			if ((((int32_t)L_10) < ((int32_t)L_11)))
			{
				goto IL_0069;
			}
		}

IL_004a:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t1767529987 * L_12 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t1767529987 * L_13 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t1767529987 *)L_13);
			int32_t L_14 = ((  int32_t (*) (List_1_t1767529987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1767529987 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_15 = V_2;
			NullCheck((List_1_t1767529987 *)L_12);
			((  void (*) (List_1_t1767529987 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t1767529987 *)L_12, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_14-(int32_t)1))-(int32_t)L_15)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			List_1_t399344435 * L_16 = V_1;
			V_3 = (List_1_t399344435 *)L_16;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_0069:
		{
			int32_t L_17 = V_2;
			V_2 = (int32_t)((int32_t)((int32_t)L_17+(int32_t)1));
		}

IL_006d:
		{
			int32_t L_18 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t1767529987 * L_19 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t1767529987 *)L_19);
			int32_t L_20 = ((  int32_t (*) (List_1_t1767529987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1767529987 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_18) >= ((int32_t)L_20)))
			{
				goto IL_0084;
			}
		}

IL_007d:
		{
			int32_t L_21 = V_2;
			if ((((int32_t)L_21) < ((int32_t)8)))
			{
				goto IL_0025;
			}
		}

IL_0084:
		{
			List_1_t399344435 * L_22 = V_1;
			if (L_22)
			{
				goto IL_0096;
			}
		}

IL_008a:
		{
			int32_t L_23 = ___capacity0;
			List_1_t399344435 * L_24 = (List_1_t399344435 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t399344435 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(L_24, (int32_t)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			V_1 = (List_1_t399344435 *)L_24;
			goto IL_00df;
		}

IL_0096:
		{
			List_1_t399344435 * L_25 = V_1;
			int32_t L_26 = ___capacity0;
			NullCheck((List_1_t399344435 *)L_25);
			((  void (*) (List_1_t399344435 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->methodPointer)((List_1_t399344435 *)L_25, (int32_t)L_26, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t1767529987 * L_27 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t1767529987 * L_28 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t1767529987 *)L_28);
			int32_t L_29 = ((  int32_t (*) (List_1_t1767529987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1767529987 *)L_28, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_30 = V_2;
			List_1_t1767529987 * L_31 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t1767529987 * L_32 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t1767529987 *)L_32);
			int32_t L_33 = ((  int32_t (*) (List_1_t1767529987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1767529987 *)L_32, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t1767529987 *)L_31);
			List_1_t399344435 * L_34 = ((  List_1_t399344435 * (*) (List_1_t1767529987 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t1767529987 *)L_31, (int32_t)((int32_t)((int32_t)L_33-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			NullCheck((List_1_t1767529987 *)L_27);
			((  void (*) (List_1_t1767529987 *, int32_t, List_1_t399344435 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->methodPointer)((List_1_t1767529987 *)L_27, (int32_t)((int32_t)((int32_t)L_29-(int32_t)L_30)), (List_1_t399344435 *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
			List_1_t1767529987 * L_35 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t1767529987 * L_36 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t1767529987 *)L_36);
			int32_t L_37 = ((  int32_t (*) (List_1_t1767529987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1767529987 *)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t1767529987 *)L_35);
			((  void (*) (List_1_t1767529987 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t1767529987 *)L_35, (int32_t)((int32_t)((int32_t)L_37-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		}

IL_00df:
		{
			List_1_t399344435 * L_38 = V_1;
			V_3 = (List_1_t399344435 *)L_38;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_00e6:
		{
			int32_t L_39 = ___capacity0;
			List_1_t399344435 * L_40 = (List_1_t399344435 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t399344435 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(L_40, (int32_t)L_39, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			V_3 = (List_1_t399344435 *)L_40;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_00f2:
		{
			; // IL_00f2: leave IL_00fe
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00f7;
	}

FINALLY_00f7:
	{ // begin finally (depth: 1)
		List_1_t1767529987 * L_41 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_41, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(247)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(247)
	{
		IL2CPP_JUMP_TBL(0xFE, IL_00fe)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00fe:
	{
		List_1_t399344435 * L_42 = V_3;
		return L_42;
	}
}
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.ClipperLib.IntPoint>::Warmup(System.Int32,System.Int32)
extern "C"  void ListPool_1_Warmup_m3246261245_gshared (Il2CppObject * __this /* static, unused */, int32_t ___count0, int32_t ___size1, const MethodInfo* method)
{
	List_1_t1767529987 * V_0 = NULL;
	List_1U5BU5D_t497255074* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t1767529987 * L_0 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t1767529987 *)L_0;
		List_1_t1767529987 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_2 = ___count0;
			V_1 = (List_1U5BU5D_t497255074*)((List_1U5BU5D_t497255074*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (uint32_t)L_2));
			V_2 = (int32_t)0;
			goto IL_0027;
		}

IL_001a:
		{
			List_1U5BU5D_t497255074* L_3 = V_1;
			int32_t L_4 = V_2;
			int32_t L_5 = ___size1;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t399344435 * L_6 = ((  List_1_t399344435 * (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->methodPointer)(NULL /*static, unused*/, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
			NullCheck(L_3);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
			ArrayElementTypeCheck (L_3, L_6);
			(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (List_1_t399344435 *)L_6);
			int32_t L_7 = V_2;
			V_2 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
		}

IL_0027:
		{
			int32_t L_8 = V_2;
			int32_t L_9 = ___count0;
			if ((((int32_t)L_8) < ((int32_t)L_9)))
			{
				goto IL_001a;
			}
		}

IL_002e:
		{
			V_3 = (int32_t)0;
			goto IL_0041;
		}

IL_0035:
		{
			List_1U5BU5D_t497255074* L_10 = V_1;
			int32_t L_11 = V_3;
			NullCheck(L_10);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
			int32_t L_12 = L_11;
			List_1_t399344435 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			((  void (*) (Il2CppObject * /* static, unused */, List_1_t399344435 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->methodPointer)(NULL /*static, unused*/, (List_1_t399344435 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
			int32_t L_14 = V_3;
			V_3 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
		}

IL_0041:
		{
			int32_t L_15 = V_3;
			int32_t L_16 = ___count0;
			if ((((int32_t)L_15) < ((int32_t)L_16)))
			{
				goto IL_0035;
			}
		}

IL_0048:
		{
			IL2CPP_LEAVE(0x54, FINALLY_004d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_004d;
	}

FINALLY_004d:
	{ // begin finally (depth: 1)
		List_1_t1767529987 * L_17 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_17, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(77)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(77)
	{
		IL2CPP_JUMP_TBL(0x54, IL_0054)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0054:
	{
		return;
	}
}
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.ClipperLib.IntPoint>::Release(System.Collections.Generic.List`1<T>)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3527666854;
extern const uint32_t ListPool_1_Release_m837364280_MetadataUsageId;
extern "C"  void ListPool_1_Release_m837364280_gshared (Il2CppObject * __this /* static, unused */, List_1_t399344435 * ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListPool_1_Release_m837364280_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1767529987 * V_0 = NULL;
	int32_t V_1 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t399344435 * L_0 = ___list0;
		NullCheck((List_1_t399344435 *)L_0);
		((  void (*) (List_1_t399344435 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->methodPointer)((List_1_t399344435 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t1767529987 * L_1 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t1767529987 *)L_1;
		List_1_t1767529987 * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			V_1 = (int32_t)0;
			goto IL_0039;
		}

IL_0019:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t1767529987 * L_3 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			int32_t L_4 = V_1;
			NullCheck((List_1_t1767529987 *)L_3);
			List_1_t399344435 * L_5 = ((  List_1_t399344435 * (*) (List_1_t1767529987 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t1767529987 *)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			List_1_t399344435 * L_6 = ___list0;
			if ((!(((Il2CppObject*)(List_1_t399344435 *)L_5) == ((Il2CppObject*)(List_1_t399344435 *)L_6))))
			{
				goto IL_0035;
			}
		}

IL_002a:
		{
			InvalidOperationException_t1589641621 * L_7 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_m1485483280(L_7, (String_t*)_stringLiteral3527666854, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
		}

IL_0035:
		{
			int32_t L_8 = V_1;
			V_1 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
		}

IL_0039:
		{
			int32_t L_9 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t1767529987 * L_10 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t1767529987 *)L_10);
			int32_t L_11 = ((  int32_t (*) (List_1_t1767529987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1767529987 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_9) < ((int32_t)L_11)))
			{
				goto IL_0019;
			}
		}

IL_0049:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t1767529987 * L_12 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t399344435 * L_13 = ___list0;
			NullCheck((List_1_t1767529987 *)L_12);
			((  void (*) (List_1_t1767529987 *, List_1_t399344435 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->methodPointer)((List_1_t1767529987 *)L_12, (List_1_t399344435 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
			IL2CPP_LEAVE(0x60, FINALLY_0059);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0059;
	}

FINALLY_0059:
	{ // begin finally (depth: 1)
		List_1_t1767529987 * L_14 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(89)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(89)
	{
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0060:
	{
		return;
	}
}
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.ClipperLib.IntPoint>::Clear()
extern "C"  void ListPool_1_Clear_m1506978982_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t1767529987 * V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t1767529987 * L_0 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t1767529987 *)L_0;
		List_1_t1767529987 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t1767529987 * L_2 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		NullCheck((List_1_t1767529987 *)L_2);
		((  void (*) (List_1_t1767529987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->methodPointer)((List_1_t1767529987 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		IL2CPP_LEAVE(0x22, FINALLY_001b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_001b;
	}

FINALLY_001b:
	{ // begin finally (depth: 1)
		List_1_t1767529987 * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(27)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(27)
	{
		IL2CPP_JUMP_TBL(0x22, IL_0022)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0022:
	{
		return;
	}
}
// System.Int32 Pathfinding.Util.ListPool`1<Pathfinding.ClipperLib.IntPoint>::GetSize()
extern "C"  int32_t ListPool_1_GetSize_m1975326850_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t1767529987 * L_0 = ((ListPool_1_t1611629233_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		NullCheck((List_1_t1767529987 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t1767529987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1767529987 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.Int2>::.cctor()
extern "C"  void ListPool_1__cctor_m136990980_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		List_1_t415449401 * L_0 = (List_1_t415449401 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (List_1_t415449401 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_pool_1(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<Pathfinding.Int2>::Claim()
extern "C"  List_1_t3342231145 * ListPool_1_Claim_m1156262776_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t415449401 * V_0 = NULL;
	List_1_t3342231145 * V_1 = NULL;
	List_1_t3342231145 * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t415449401 * L_0 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t415449401 *)L_0;
		List_1_t415449401 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t415449401 * L_2 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t415449401 *)L_2);
			int32_t L_3 = ((  int32_t (*) (List_1_t415449401 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t415449401 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_3) <= ((int32_t)0)))
			{
				goto IL_0050;
			}
		}

IL_001c:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t415449401 * L_4 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t415449401 * L_5 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t415449401 *)L_5);
			int32_t L_6 = ((  int32_t (*) (List_1_t415449401 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t415449401 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t415449401 *)L_4);
			List_1_t3342231145 * L_7 = ((  List_1_t3342231145 * (*) (List_1_t415449401 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t415449401 *)L_4, (int32_t)((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			V_1 = (List_1_t3342231145 *)L_7;
			List_1_t415449401 * L_8 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t415449401 * L_9 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t415449401 *)L_9);
			int32_t L_10 = ((  int32_t (*) (List_1_t415449401 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t415449401 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t415449401 *)L_8);
			((  void (*) (List_1_t415449401 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t415449401 *)L_8, (int32_t)((int32_t)((int32_t)L_10-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			List_1_t3342231145 * L_11 = V_1;
			V_2 = (List_1_t3342231145 *)L_11;
			IL2CPP_LEAVE(0x67, FINALLY_0060);
		}

IL_0050:
		{
			List_1_t3342231145 * L_12 = (List_1_t3342231145 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t3342231145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			V_2 = (List_1_t3342231145 *)L_12;
			IL2CPP_LEAVE(0x67, FINALLY_0060);
		}

IL_005b:
		{
			; // IL_005b: leave IL_0067
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0060;
	}

FINALLY_0060:
	{ // begin finally (depth: 1)
		List_1_t415449401 * L_13 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_13, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(96)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(96)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0067:
	{
		List_1_t3342231145 * L_14 = V_2;
		return L_14;
	}
}
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<Pathfinding.Int2>::Claim(System.Int32)
extern "C"  List_1_t3342231145 * ListPool_1_Claim_m578514121_gshared (Il2CppObject * __this /* static, unused */, int32_t ___capacity0, const MethodInfo* method)
{
	List_1_t415449401 * V_0 = NULL;
	List_1_t3342231145 * V_1 = NULL;
	int32_t V_2 = 0;
	List_1_t3342231145 * V_3 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t415449401 * L_0 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t415449401 *)L_0;
		List_1_t415449401 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t415449401 * L_2 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t415449401 *)L_2);
			int32_t L_3 = ((  int32_t (*) (List_1_t415449401 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t415449401 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_3) <= ((int32_t)0)))
			{
				goto IL_00e6;
			}
		}

IL_001c:
		{
			V_1 = (List_1_t3342231145 *)NULL;
			V_2 = (int32_t)0;
			goto IL_006d;
		}

IL_0025:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t415449401 * L_4 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t415449401 * L_5 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t415449401 *)L_5);
			int32_t L_6 = ((  int32_t (*) (List_1_t415449401 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t415449401 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_7 = V_2;
			NullCheck((List_1_t415449401 *)L_4);
			List_1_t3342231145 * L_8 = ((  List_1_t3342231145 * (*) (List_1_t415449401 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t415449401 *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			V_1 = (List_1_t3342231145 *)L_8;
			List_1_t3342231145 * L_9 = V_1;
			NullCheck((List_1_t3342231145 *)L_9);
			int32_t L_10 = ((  int32_t (*) (List_1_t3342231145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t3342231145 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			int32_t L_11 = ___capacity0;
			if ((((int32_t)L_10) < ((int32_t)L_11)))
			{
				goto IL_0069;
			}
		}

IL_004a:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t415449401 * L_12 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t415449401 * L_13 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t415449401 *)L_13);
			int32_t L_14 = ((  int32_t (*) (List_1_t415449401 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t415449401 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_15 = V_2;
			NullCheck((List_1_t415449401 *)L_12);
			((  void (*) (List_1_t415449401 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t415449401 *)L_12, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_14-(int32_t)1))-(int32_t)L_15)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			List_1_t3342231145 * L_16 = V_1;
			V_3 = (List_1_t3342231145 *)L_16;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_0069:
		{
			int32_t L_17 = V_2;
			V_2 = (int32_t)((int32_t)((int32_t)L_17+(int32_t)1));
		}

IL_006d:
		{
			int32_t L_18 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t415449401 * L_19 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t415449401 *)L_19);
			int32_t L_20 = ((  int32_t (*) (List_1_t415449401 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t415449401 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_18) >= ((int32_t)L_20)))
			{
				goto IL_0084;
			}
		}

IL_007d:
		{
			int32_t L_21 = V_2;
			if ((((int32_t)L_21) < ((int32_t)8)))
			{
				goto IL_0025;
			}
		}

IL_0084:
		{
			List_1_t3342231145 * L_22 = V_1;
			if (L_22)
			{
				goto IL_0096;
			}
		}

IL_008a:
		{
			int32_t L_23 = ___capacity0;
			List_1_t3342231145 * L_24 = (List_1_t3342231145 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t3342231145 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(L_24, (int32_t)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			V_1 = (List_1_t3342231145 *)L_24;
			goto IL_00df;
		}

IL_0096:
		{
			List_1_t3342231145 * L_25 = V_1;
			int32_t L_26 = ___capacity0;
			NullCheck((List_1_t3342231145 *)L_25);
			((  void (*) (List_1_t3342231145 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->methodPointer)((List_1_t3342231145 *)L_25, (int32_t)L_26, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t415449401 * L_27 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t415449401 * L_28 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t415449401 *)L_28);
			int32_t L_29 = ((  int32_t (*) (List_1_t415449401 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t415449401 *)L_28, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_30 = V_2;
			List_1_t415449401 * L_31 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t415449401 * L_32 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t415449401 *)L_32);
			int32_t L_33 = ((  int32_t (*) (List_1_t415449401 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t415449401 *)L_32, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t415449401 *)L_31);
			List_1_t3342231145 * L_34 = ((  List_1_t3342231145 * (*) (List_1_t415449401 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t415449401 *)L_31, (int32_t)((int32_t)((int32_t)L_33-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			NullCheck((List_1_t415449401 *)L_27);
			((  void (*) (List_1_t415449401 *, int32_t, List_1_t3342231145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->methodPointer)((List_1_t415449401 *)L_27, (int32_t)((int32_t)((int32_t)L_29-(int32_t)L_30)), (List_1_t3342231145 *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
			List_1_t415449401 * L_35 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t415449401 * L_36 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t415449401 *)L_36);
			int32_t L_37 = ((  int32_t (*) (List_1_t415449401 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t415449401 *)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t415449401 *)L_35);
			((  void (*) (List_1_t415449401 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t415449401 *)L_35, (int32_t)((int32_t)((int32_t)L_37-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		}

IL_00df:
		{
			List_1_t3342231145 * L_38 = V_1;
			V_3 = (List_1_t3342231145 *)L_38;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_00e6:
		{
			int32_t L_39 = ___capacity0;
			List_1_t3342231145 * L_40 = (List_1_t3342231145 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t3342231145 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(L_40, (int32_t)L_39, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			V_3 = (List_1_t3342231145 *)L_40;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_00f2:
		{
			; // IL_00f2: leave IL_00fe
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00f7;
	}

FINALLY_00f7:
	{ // begin finally (depth: 1)
		List_1_t415449401 * L_41 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_41, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(247)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(247)
	{
		IL2CPP_JUMP_TBL(0xFE, IL_00fe)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00fe:
	{
		List_1_t3342231145 * L_42 = V_3;
		return L_42;
	}
}
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.Int2>::Warmup(System.Int32,System.Int32)
extern "C"  void ListPool_1_Warmup_m3029164491_gshared (Il2CppObject * __this /* static, unused */, int32_t ___count0, int32_t ___size1, const MethodInfo* method)
{
	List_1_t415449401 * V_0 = NULL;
	List_1U5BU5D_t1457655252* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t415449401 * L_0 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t415449401 *)L_0;
		List_1_t415449401 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_2 = ___count0;
			V_1 = (List_1U5BU5D_t1457655252*)((List_1U5BU5D_t1457655252*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (uint32_t)L_2));
			V_2 = (int32_t)0;
			goto IL_0027;
		}

IL_001a:
		{
			List_1U5BU5D_t1457655252* L_3 = V_1;
			int32_t L_4 = V_2;
			int32_t L_5 = ___size1;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t3342231145 * L_6 = ((  List_1_t3342231145 * (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->methodPointer)(NULL /*static, unused*/, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
			NullCheck(L_3);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
			ArrayElementTypeCheck (L_3, L_6);
			(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (List_1_t3342231145 *)L_6);
			int32_t L_7 = V_2;
			V_2 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
		}

IL_0027:
		{
			int32_t L_8 = V_2;
			int32_t L_9 = ___count0;
			if ((((int32_t)L_8) < ((int32_t)L_9)))
			{
				goto IL_001a;
			}
		}

IL_002e:
		{
			V_3 = (int32_t)0;
			goto IL_0041;
		}

IL_0035:
		{
			List_1U5BU5D_t1457655252* L_10 = V_1;
			int32_t L_11 = V_3;
			NullCheck(L_10);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
			int32_t L_12 = L_11;
			List_1_t3342231145 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			((  void (*) (Il2CppObject * /* static, unused */, List_1_t3342231145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->methodPointer)(NULL /*static, unused*/, (List_1_t3342231145 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
			int32_t L_14 = V_3;
			V_3 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
		}

IL_0041:
		{
			int32_t L_15 = V_3;
			int32_t L_16 = ___count0;
			if ((((int32_t)L_15) < ((int32_t)L_16)))
			{
				goto IL_0035;
			}
		}

IL_0048:
		{
			IL2CPP_LEAVE(0x54, FINALLY_004d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_004d;
	}

FINALLY_004d:
	{ // begin finally (depth: 1)
		List_1_t415449401 * L_17 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_17, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(77)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(77)
	{
		IL2CPP_JUMP_TBL(0x54, IL_0054)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0054:
	{
		return;
	}
}
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.Int2>::Release(System.Collections.Generic.List`1<T>)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3527666854;
extern const uint32_t ListPool_1_Release_m2370061062_MetadataUsageId;
extern "C"  void ListPool_1_Release_m2370061062_gshared (Il2CppObject * __this /* static, unused */, List_1_t3342231145 * ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListPool_1_Release_m2370061062_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t415449401 * V_0 = NULL;
	int32_t V_1 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t3342231145 * L_0 = ___list0;
		NullCheck((List_1_t3342231145 *)L_0);
		((  void (*) (List_1_t3342231145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->methodPointer)((List_1_t3342231145 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t415449401 * L_1 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t415449401 *)L_1;
		List_1_t415449401 * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			V_1 = (int32_t)0;
			goto IL_0039;
		}

IL_0019:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t415449401 * L_3 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			int32_t L_4 = V_1;
			NullCheck((List_1_t415449401 *)L_3);
			List_1_t3342231145 * L_5 = ((  List_1_t3342231145 * (*) (List_1_t415449401 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t415449401 *)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			List_1_t3342231145 * L_6 = ___list0;
			if ((!(((Il2CppObject*)(List_1_t3342231145 *)L_5) == ((Il2CppObject*)(List_1_t3342231145 *)L_6))))
			{
				goto IL_0035;
			}
		}

IL_002a:
		{
			InvalidOperationException_t1589641621 * L_7 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_m1485483280(L_7, (String_t*)_stringLiteral3527666854, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
		}

IL_0035:
		{
			int32_t L_8 = V_1;
			V_1 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
		}

IL_0039:
		{
			int32_t L_9 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t415449401 * L_10 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t415449401 *)L_10);
			int32_t L_11 = ((  int32_t (*) (List_1_t415449401 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t415449401 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_9) < ((int32_t)L_11)))
			{
				goto IL_0019;
			}
		}

IL_0049:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t415449401 * L_12 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t3342231145 * L_13 = ___list0;
			NullCheck((List_1_t415449401 *)L_12);
			((  void (*) (List_1_t415449401 *, List_1_t3342231145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->methodPointer)((List_1_t415449401 *)L_12, (List_1_t3342231145 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
			IL2CPP_LEAVE(0x60, FINALLY_0059);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0059;
	}

FINALLY_0059:
	{ // begin finally (depth: 1)
		List_1_t415449401 * L_14 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(89)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(89)
	{
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0060:
	{
		return;
	}
}
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.Int2>::Clear()
extern "C"  void ListPool_1_Clear_m58503156_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t415449401 * V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t415449401 * L_0 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t415449401 *)L_0;
		List_1_t415449401 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t415449401 * L_2 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		NullCheck((List_1_t415449401 *)L_2);
		((  void (*) (List_1_t415449401 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->methodPointer)((List_1_t415449401 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		IL2CPP_LEAVE(0x22, FINALLY_001b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_001b;
	}

FINALLY_001b:
	{ // begin finally (depth: 1)
		List_1_t415449401 * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(27)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(27)
	{
		IL2CPP_JUMP_TBL(0x22, IL_0022)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0022:
	{
		return;
	}
}
// System.Int32 Pathfinding.Util.ListPool`1<Pathfinding.Int2>::GetSize()
extern "C"  int32_t ListPool_1_GetSize_m3297287724_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t415449401 * L_0 = ((ListPool_1_t259548647_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		NullCheck((List_1_t415449401 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t415449401 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t415449401 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.Int3>::.cctor()
extern "C"  void ListPool_1__cctor_m266073699_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		List_1_t415449402 * L_0 = (List_1_t415449402 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (List_1_t415449402 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_pool_1(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<Pathfinding.Int3>::Claim()
extern "C"  List_1_t3342231146 * ListPool_1_Claim_m3654278713_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t415449402 * V_0 = NULL;
	List_1_t3342231146 * V_1 = NULL;
	List_1_t3342231146 * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t415449402 * L_0 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t415449402 *)L_0;
		List_1_t415449402 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t415449402 * L_2 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t415449402 *)L_2);
			int32_t L_3 = ((  int32_t (*) (List_1_t415449402 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t415449402 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_3) <= ((int32_t)0)))
			{
				goto IL_0050;
			}
		}

IL_001c:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t415449402 * L_4 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t415449402 * L_5 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t415449402 *)L_5);
			int32_t L_6 = ((  int32_t (*) (List_1_t415449402 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t415449402 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t415449402 *)L_4);
			List_1_t3342231146 * L_7 = ((  List_1_t3342231146 * (*) (List_1_t415449402 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t415449402 *)L_4, (int32_t)((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			V_1 = (List_1_t3342231146 *)L_7;
			List_1_t415449402 * L_8 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t415449402 * L_9 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t415449402 *)L_9);
			int32_t L_10 = ((  int32_t (*) (List_1_t415449402 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t415449402 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t415449402 *)L_8);
			((  void (*) (List_1_t415449402 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t415449402 *)L_8, (int32_t)((int32_t)((int32_t)L_10-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			List_1_t3342231146 * L_11 = V_1;
			V_2 = (List_1_t3342231146 *)L_11;
			IL2CPP_LEAVE(0x67, FINALLY_0060);
		}

IL_0050:
		{
			List_1_t3342231146 * L_12 = (List_1_t3342231146 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t3342231146 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			V_2 = (List_1_t3342231146 *)L_12;
			IL2CPP_LEAVE(0x67, FINALLY_0060);
		}

IL_005b:
		{
			; // IL_005b: leave IL_0067
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0060;
	}

FINALLY_0060:
	{ // begin finally (depth: 1)
		List_1_t415449402 * L_13 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_13, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(96)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(96)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0067:
	{
		List_1_t3342231146 * L_14 = V_2;
		return L_14;
	}
}
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<Pathfinding.Int3>::Claim(System.Int32)
extern "C"  List_1_t3342231146 * ListPool_1_Claim_m1823278602_gshared (Il2CppObject * __this /* static, unused */, int32_t ___capacity0, const MethodInfo* method)
{
	List_1_t415449402 * V_0 = NULL;
	List_1_t3342231146 * V_1 = NULL;
	int32_t V_2 = 0;
	List_1_t3342231146 * V_3 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t415449402 * L_0 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t415449402 *)L_0;
		List_1_t415449402 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t415449402 * L_2 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t415449402 *)L_2);
			int32_t L_3 = ((  int32_t (*) (List_1_t415449402 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t415449402 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_3) <= ((int32_t)0)))
			{
				goto IL_00e6;
			}
		}

IL_001c:
		{
			V_1 = (List_1_t3342231146 *)NULL;
			V_2 = (int32_t)0;
			goto IL_006d;
		}

IL_0025:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t415449402 * L_4 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t415449402 * L_5 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t415449402 *)L_5);
			int32_t L_6 = ((  int32_t (*) (List_1_t415449402 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t415449402 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_7 = V_2;
			NullCheck((List_1_t415449402 *)L_4);
			List_1_t3342231146 * L_8 = ((  List_1_t3342231146 * (*) (List_1_t415449402 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t415449402 *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			V_1 = (List_1_t3342231146 *)L_8;
			List_1_t3342231146 * L_9 = V_1;
			NullCheck((List_1_t3342231146 *)L_9);
			int32_t L_10 = ((  int32_t (*) (List_1_t3342231146 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t3342231146 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			int32_t L_11 = ___capacity0;
			if ((((int32_t)L_10) < ((int32_t)L_11)))
			{
				goto IL_0069;
			}
		}

IL_004a:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t415449402 * L_12 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t415449402 * L_13 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t415449402 *)L_13);
			int32_t L_14 = ((  int32_t (*) (List_1_t415449402 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t415449402 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_15 = V_2;
			NullCheck((List_1_t415449402 *)L_12);
			((  void (*) (List_1_t415449402 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t415449402 *)L_12, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_14-(int32_t)1))-(int32_t)L_15)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			List_1_t3342231146 * L_16 = V_1;
			V_3 = (List_1_t3342231146 *)L_16;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_0069:
		{
			int32_t L_17 = V_2;
			V_2 = (int32_t)((int32_t)((int32_t)L_17+(int32_t)1));
		}

IL_006d:
		{
			int32_t L_18 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t415449402 * L_19 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t415449402 *)L_19);
			int32_t L_20 = ((  int32_t (*) (List_1_t415449402 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t415449402 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_18) >= ((int32_t)L_20)))
			{
				goto IL_0084;
			}
		}

IL_007d:
		{
			int32_t L_21 = V_2;
			if ((((int32_t)L_21) < ((int32_t)8)))
			{
				goto IL_0025;
			}
		}

IL_0084:
		{
			List_1_t3342231146 * L_22 = V_1;
			if (L_22)
			{
				goto IL_0096;
			}
		}

IL_008a:
		{
			int32_t L_23 = ___capacity0;
			List_1_t3342231146 * L_24 = (List_1_t3342231146 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t3342231146 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(L_24, (int32_t)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			V_1 = (List_1_t3342231146 *)L_24;
			goto IL_00df;
		}

IL_0096:
		{
			List_1_t3342231146 * L_25 = V_1;
			int32_t L_26 = ___capacity0;
			NullCheck((List_1_t3342231146 *)L_25);
			((  void (*) (List_1_t3342231146 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->methodPointer)((List_1_t3342231146 *)L_25, (int32_t)L_26, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t415449402 * L_27 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t415449402 * L_28 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t415449402 *)L_28);
			int32_t L_29 = ((  int32_t (*) (List_1_t415449402 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t415449402 *)L_28, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_30 = V_2;
			List_1_t415449402 * L_31 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t415449402 * L_32 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t415449402 *)L_32);
			int32_t L_33 = ((  int32_t (*) (List_1_t415449402 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t415449402 *)L_32, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t415449402 *)L_31);
			List_1_t3342231146 * L_34 = ((  List_1_t3342231146 * (*) (List_1_t415449402 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t415449402 *)L_31, (int32_t)((int32_t)((int32_t)L_33-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			NullCheck((List_1_t415449402 *)L_27);
			((  void (*) (List_1_t415449402 *, int32_t, List_1_t3342231146 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->methodPointer)((List_1_t415449402 *)L_27, (int32_t)((int32_t)((int32_t)L_29-(int32_t)L_30)), (List_1_t3342231146 *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
			List_1_t415449402 * L_35 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t415449402 * L_36 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t415449402 *)L_36);
			int32_t L_37 = ((  int32_t (*) (List_1_t415449402 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t415449402 *)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t415449402 *)L_35);
			((  void (*) (List_1_t415449402 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t415449402 *)L_35, (int32_t)((int32_t)((int32_t)L_37-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		}

IL_00df:
		{
			List_1_t3342231146 * L_38 = V_1;
			V_3 = (List_1_t3342231146 *)L_38;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_00e6:
		{
			int32_t L_39 = ___capacity0;
			List_1_t3342231146 * L_40 = (List_1_t3342231146 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t3342231146 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(L_40, (int32_t)L_39, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			V_3 = (List_1_t3342231146 *)L_40;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_00f2:
		{
			; // IL_00f2: leave IL_00fe
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00f7;
	}

FINALLY_00f7:
	{ // begin finally (depth: 1)
		List_1_t415449402 * L_41 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_41, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(247)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(247)
	{
		IL2CPP_JUMP_TBL(0xFE, IL_00fe)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00fe:
	{
		List_1_t3342231146 * L_42 = V_3;
		return L_42;
	}
}
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.Int3>::Warmup(System.Int32,System.Int32)
extern "C"  void ListPool_1_Warmup_m3358930252_gshared (Il2CppObject * __this /* static, unused */, int32_t ___count0, int32_t ___size1, const MethodInfo* method)
{
	List_1_t415449402 * V_0 = NULL;
	List_1U5BU5D_t1943842991* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t415449402 * L_0 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t415449402 *)L_0;
		List_1_t415449402 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_2 = ___count0;
			V_1 = (List_1U5BU5D_t1943842991*)((List_1U5BU5D_t1943842991*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (uint32_t)L_2));
			V_2 = (int32_t)0;
			goto IL_0027;
		}

IL_001a:
		{
			List_1U5BU5D_t1943842991* L_3 = V_1;
			int32_t L_4 = V_2;
			int32_t L_5 = ___size1;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t3342231146 * L_6 = ((  List_1_t3342231146 * (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->methodPointer)(NULL /*static, unused*/, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
			NullCheck(L_3);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
			ArrayElementTypeCheck (L_3, L_6);
			(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (List_1_t3342231146 *)L_6);
			int32_t L_7 = V_2;
			V_2 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
		}

IL_0027:
		{
			int32_t L_8 = V_2;
			int32_t L_9 = ___count0;
			if ((((int32_t)L_8) < ((int32_t)L_9)))
			{
				goto IL_001a;
			}
		}

IL_002e:
		{
			V_3 = (int32_t)0;
			goto IL_0041;
		}

IL_0035:
		{
			List_1U5BU5D_t1943842991* L_10 = V_1;
			int32_t L_11 = V_3;
			NullCheck(L_10);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
			int32_t L_12 = L_11;
			List_1_t3342231146 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			((  void (*) (Il2CppObject * /* static, unused */, List_1_t3342231146 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->methodPointer)(NULL /*static, unused*/, (List_1_t3342231146 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
			int32_t L_14 = V_3;
			V_3 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
		}

IL_0041:
		{
			int32_t L_15 = V_3;
			int32_t L_16 = ___count0;
			if ((((int32_t)L_15) < ((int32_t)L_16)))
			{
				goto IL_0035;
			}
		}

IL_0048:
		{
			IL2CPP_LEAVE(0x54, FINALLY_004d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_004d;
	}

FINALLY_004d:
	{ // begin finally (depth: 1)
		List_1_t415449402 * L_17 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_17, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(77)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(77)
	{
		IL2CPP_JUMP_TBL(0x54, IL_0054)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0054:
	{
		return;
	}
}
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.Int3>::Release(System.Collections.Generic.List`1<T>)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3527666854;
extern const uint32_t ListPool_1_Release_m2362301703_MetadataUsageId;
extern "C"  void ListPool_1_Release_m2362301703_gshared (Il2CppObject * __this /* static, unused */, List_1_t3342231146 * ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListPool_1_Release_m2362301703_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t415449402 * V_0 = NULL;
	int32_t V_1 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t3342231146 * L_0 = ___list0;
		NullCheck((List_1_t3342231146 *)L_0);
		((  void (*) (List_1_t3342231146 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->methodPointer)((List_1_t3342231146 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t415449402 * L_1 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t415449402 *)L_1;
		List_1_t415449402 * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			V_1 = (int32_t)0;
			goto IL_0039;
		}

IL_0019:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t415449402 * L_3 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			int32_t L_4 = V_1;
			NullCheck((List_1_t415449402 *)L_3);
			List_1_t3342231146 * L_5 = ((  List_1_t3342231146 * (*) (List_1_t415449402 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t415449402 *)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			List_1_t3342231146 * L_6 = ___list0;
			if ((!(((Il2CppObject*)(List_1_t3342231146 *)L_5) == ((Il2CppObject*)(List_1_t3342231146 *)L_6))))
			{
				goto IL_0035;
			}
		}

IL_002a:
		{
			InvalidOperationException_t1589641621 * L_7 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_m1485483280(L_7, (String_t*)_stringLiteral3527666854, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
		}

IL_0035:
		{
			int32_t L_8 = V_1;
			V_1 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
		}

IL_0039:
		{
			int32_t L_9 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t415449402 * L_10 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t415449402 *)L_10);
			int32_t L_11 = ((  int32_t (*) (List_1_t415449402 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t415449402 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_9) < ((int32_t)L_11)))
			{
				goto IL_0019;
			}
		}

IL_0049:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t415449402 * L_12 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t3342231146 * L_13 = ___list0;
			NullCheck((List_1_t415449402 *)L_12);
			((  void (*) (List_1_t415449402 *, List_1_t3342231146 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->methodPointer)((List_1_t415449402 *)L_12, (List_1_t3342231146 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
			IL2CPP_LEAVE(0x60, FINALLY_0059);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0059;
	}

FINALLY_0059:
	{ // begin finally (depth: 1)
		List_1_t415449402 * L_14 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(89)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(89)
	{
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0060:
	{
		return;
	}
}
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.Int3>::Clear()
extern "C"  void ListPool_1_Clear_m2556519093_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t415449402 * V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t415449402 * L_0 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t415449402 *)L_0;
		List_1_t415449402 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t415449402 * L_2 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		NullCheck((List_1_t415449402 *)L_2);
		((  void (*) (List_1_t415449402 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->methodPointer)((List_1_t415449402 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		IL2CPP_LEAVE(0x22, FINALLY_001b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_001b;
	}

FINALLY_001b:
	{ // begin finally (depth: 1)
		List_1_t415449402 * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(27)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(27)
	{
		IL2CPP_JUMP_TBL(0x22, IL_0022)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0022:
	{
		return;
	}
}
// System.Int32 Pathfinding.Util.ListPool`1<Pathfinding.Int3>::GetSize()
extern "C"  int32_t ListPool_1_GetSize_m3003884717_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t415449402 * L_0 = ((ListPool_1_t259548648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		NullCheck((List_1_t415449402 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t415449402 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t415449402 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.IntRect>::.cctor()
extern "C"  void ListPool_1__cctor_m1103718614_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		List_1_t1456462069 * L_0 = (List_1_t1456462069 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (List_1_t1456462069 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_pool_1(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<Pathfinding.IntRect>::Claim()
extern "C"  List_1_t88276517 * ListPool_1_Claim_m1160790684_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t1456462069 * V_0 = NULL;
	List_1_t88276517 * V_1 = NULL;
	List_1_t88276517 * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t1456462069 * L_0 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t1456462069 *)L_0;
		List_1_t1456462069 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t1456462069 * L_2 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t1456462069 *)L_2);
			int32_t L_3 = ((  int32_t (*) (List_1_t1456462069 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1456462069 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_3) <= ((int32_t)0)))
			{
				goto IL_0050;
			}
		}

IL_001c:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t1456462069 * L_4 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t1456462069 * L_5 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t1456462069 *)L_5);
			int32_t L_6 = ((  int32_t (*) (List_1_t1456462069 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1456462069 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t1456462069 *)L_4);
			List_1_t88276517 * L_7 = ((  List_1_t88276517 * (*) (List_1_t1456462069 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t1456462069 *)L_4, (int32_t)((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			V_1 = (List_1_t88276517 *)L_7;
			List_1_t1456462069 * L_8 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t1456462069 * L_9 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t1456462069 *)L_9);
			int32_t L_10 = ((  int32_t (*) (List_1_t1456462069 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1456462069 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t1456462069 *)L_8);
			((  void (*) (List_1_t1456462069 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t1456462069 *)L_8, (int32_t)((int32_t)((int32_t)L_10-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			List_1_t88276517 * L_11 = V_1;
			V_2 = (List_1_t88276517 *)L_11;
			IL2CPP_LEAVE(0x67, FINALLY_0060);
		}

IL_0050:
		{
			List_1_t88276517 * L_12 = (List_1_t88276517 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t88276517 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			V_2 = (List_1_t88276517 *)L_12;
			IL2CPP_LEAVE(0x67, FINALLY_0060);
		}

IL_005b:
		{
			; // IL_005b: leave IL_0067
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0060;
	}

FINALLY_0060:
	{ // begin finally (depth: 1)
		List_1_t1456462069 * L_13 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_13, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(96)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(96)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0067:
	{
		List_1_t88276517 * L_14 = V_2;
		return L_14;
	}
}
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<Pathfinding.IntRect>::Claim(System.Int32)
extern "C"  List_1_t88276517 * ListPool_1_Claim_m860024301_gshared (Il2CppObject * __this /* static, unused */, int32_t ___capacity0, const MethodInfo* method)
{
	List_1_t1456462069 * V_0 = NULL;
	List_1_t88276517 * V_1 = NULL;
	int32_t V_2 = 0;
	List_1_t88276517 * V_3 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t1456462069 * L_0 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t1456462069 *)L_0;
		List_1_t1456462069 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t1456462069 * L_2 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t1456462069 *)L_2);
			int32_t L_3 = ((  int32_t (*) (List_1_t1456462069 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1456462069 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_3) <= ((int32_t)0)))
			{
				goto IL_00e6;
			}
		}

IL_001c:
		{
			V_1 = (List_1_t88276517 *)NULL;
			V_2 = (int32_t)0;
			goto IL_006d;
		}

IL_0025:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t1456462069 * L_4 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t1456462069 * L_5 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t1456462069 *)L_5);
			int32_t L_6 = ((  int32_t (*) (List_1_t1456462069 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1456462069 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_7 = V_2;
			NullCheck((List_1_t1456462069 *)L_4);
			List_1_t88276517 * L_8 = ((  List_1_t88276517 * (*) (List_1_t1456462069 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t1456462069 *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			V_1 = (List_1_t88276517 *)L_8;
			List_1_t88276517 * L_9 = V_1;
			NullCheck((List_1_t88276517 *)L_9);
			int32_t L_10 = ((  int32_t (*) (List_1_t88276517 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t88276517 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			int32_t L_11 = ___capacity0;
			if ((((int32_t)L_10) < ((int32_t)L_11)))
			{
				goto IL_0069;
			}
		}

IL_004a:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t1456462069 * L_12 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t1456462069 * L_13 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t1456462069 *)L_13);
			int32_t L_14 = ((  int32_t (*) (List_1_t1456462069 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1456462069 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_15 = V_2;
			NullCheck((List_1_t1456462069 *)L_12);
			((  void (*) (List_1_t1456462069 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t1456462069 *)L_12, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_14-(int32_t)1))-(int32_t)L_15)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			List_1_t88276517 * L_16 = V_1;
			V_3 = (List_1_t88276517 *)L_16;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_0069:
		{
			int32_t L_17 = V_2;
			V_2 = (int32_t)((int32_t)((int32_t)L_17+(int32_t)1));
		}

IL_006d:
		{
			int32_t L_18 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t1456462069 * L_19 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t1456462069 *)L_19);
			int32_t L_20 = ((  int32_t (*) (List_1_t1456462069 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1456462069 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_18) >= ((int32_t)L_20)))
			{
				goto IL_0084;
			}
		}

IL_007d:
		{
			int32_t L_21 = V_2;
			if ((((int32_t)L_21) < ((int32_t)8)))
			{
				goto IL_0025;
			}
		}

IL_0084:
		{
			List_1_t88276517 * L_22 = V_1;
			if (L_22)
			{
				goto IL_0096;
			}
		}

IL_008a:
		{
			int32_t L_23 = ___capacity0;
			List_1_t88276517 * L_24 = (List_1_t88276517 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t88276517 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(L_24, (int32_t)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			V_1 = (List_1_t88276517 *)L_24;
			goto IL_00df;
		}

IL_0096:
		{
			List_1_t88276517 * L_25 = V_1;
			int32_t L_26 = ___capacity0;
			NullCheck((List_1_t88276517 *)L_25);
			((  void (*) (List_1_t88276517 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->methodPointer)((List_1_t88276517 *)L_25, (int32_t)L_26, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t1456462069 * L_27 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t1456462069 * L_28 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t1456462069 *)L_28);
			int32_t L_29 = ((  int32_t (*) (List_1_t1456462069 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1456462069 *)L_28, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_30 = V_2;
			List_1_t1456462069 * L_31 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t1456462069 * L_32 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t1456462069 *)L_32);
			int32_t L_33 = ((  int32_t (*) (List_1_t1456462069 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1456462069 *)L_32, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t1456462069 *)L_31);
			List_1_t88276517 * L_34 = ((  List_1_t88276517 * (*) (List_1_t1456462069 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t1456462069 *)L_31, (int32_t)((int32_t)((int32_t)L_33-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			NullCheck((List_1_t1456462069 *)L_27);
			((  void (*) (List_1_t1456462069 *, int32_t, List_1_t88276517 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->methodPointer)((List_1_t1456462069 *)L_27, (int32_t)((int32_t)((int32_t)L_29-(int32_t)L_30)), (List_1_t88276517 *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
			List_1_t1456462069 * L_35 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t1456462069 * L_36 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t1456462069 *)L_36);
			int32_t L_37 = ((  int32_t (*) (List_1_t1456462069 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1456462069 *)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t1456462069 *)L_35);
			((  void (*) (List_1_t1456462069 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t1456462069 *)L_35, (int32_t)((int32_t)((int32_t)L_37-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		}

IL_00df:
		{
			List_1_t88276517 * L_38 = V_1;
			V_3 = (List_1_t88276517 *)L_38;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_00e6:
		{
			int32_t L_39 = ___capacity0;
			List_1_t88276517 * L_40 = (List_1_t88276517 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t88276517 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(L_40, (int32_t)L_39, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			V_3 = (List_1_t88276517 *)L_40;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_00f2:
		{
			; // IL_00f2: leave IL_00fe
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00f7;
	}

FINALLY_00f7:
	{ // begin finally (depth: 1)
		List_1_t1456462069 * L_41 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_41, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(247)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(247)
	{
		IL2CPP_JUMP_TBL(0xFE, IL_00fe)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00fe:
	{
		List_1_t88276517 * L_42 = V_3;
		return L_42;
	}
}
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.IntRect>::Warmup(System.Int32,System.Int32)
extern "C"  void ListPool_1_Warmup_m1183603257_gshared (Il2CppObject * __this /* static, unused */, int32_t ___count0, int32_t ___size1, const MethodInfo* method)
{
	List_1_t1456462069 * V_0 = NULL;
	List_1U5BU5D_t558158760* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t1456462069 * L_0 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t1456462069 *)L_0;
		List_1_t1456462069 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_2 = ___count0;
			V_1 = (List_1U5BU5D_t558158760*)((List_1U5BU5D_t558158760*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (uint32_t)L_2));
			V_2 = (int32_t)0;
			goto IL_0027;
		}

IL_001a:
		{
			List_1U5BU5D_t558158760* L_3 = V_1;
			int32_t L_4 = V_2;
			int32_t L_5 = ___size1;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t88276517 * L_6 = ((  List_1_t88276517 * (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->methodPointer)(NULL /*static, unused*/, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
			NullCheck(L_3);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
			ArrayElementTypeCheck (L_3, L_6);
			(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (List_1_t88276517 *)L_6);
			int32_t L_7 = V_2;
			V_2 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
		}

IL_0027:
		{
			int32_t L_8 = V_2;
			int32_t L_9 = ___count0;
			if ((((int32_t)L_8) < ((int32_t)L_9)))
			{
				goto IL_001a;
			}
		}

IL_002e:
		{
			V_3 = (int32_t)0;
			goto IL_0041;
		}

IL_0035:
		{
			List_1U5BU5D_t558158760* L_10 = V_1;
			int32_t L_11 = V_3;
			NullCheck(L_10);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
			int32_t L_12 = L_11;
			List_1_t88276517 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			((  void (*) (Il2CppObject * /* static, unused */, List_1_t88276517 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->methodPointer)(NULL /*static, unused*/, (List_1_t88276517 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
			int32_t L_14 = V_3;
			V_3 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
		}

IL_0041:
		{
			int32_t L_15 = V_3;
			int32_t L_16 = ___count0;
			if ((((int32_t)L_15) < ((int32_t)L_16)))
			{
				goto IL_0035;
			}
		}

IL_0048:
		{
			IL2CPP_LEAVE(0x54, FINALLY_004d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_004d;
	}

FINALLY_004d:
	{ // begin finally (depth: 1)
		List_1_t1456462069 * L_17 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_17, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(77)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(77)
	{
		IL2CPP_JUMP_TBL(0x54, IL_0054)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0054:
	{
		return;
	}
}
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.IntRect>::Release(System.Collections.Generic.List`1<T>)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3527666854;
extern const uint32_t ListPool_1_Release_m2094794356_MetadataUsageId;
extern "C"  void ListPool_1_Release_m2094794356_gshared (Il2CppObject * __this /* static, unused */, List_1_t88276517 * ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListPool_1_Release_m2094794356_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1456462069 * V_0 = NULL;
	int32_t V_1 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t88276517 * L_0 = ___list0;
		NullCheck((List_1_t88276517 *)L_0);
		((  void (*) (List_1_t88276517 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->methodPointer)((List_1_t88276517 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t1456462069 * L_1 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t1456462069 *)L_1;
		List_1_t1456462069 * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			V_1 = (int32_t)0;
			goto IL_0039;
		}

IL_0019:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t1456462069 * L_3 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			int32_t L_4 = V_1;
			NullCheck((List_1_t1456462069 *)L_3);
			List_1_t88276517 * L_5 = ((  List_1_t88276517 * (*) (List_1_t1456462069 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t1456462069 *)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			List_1_t88276517 * L_6 = ___list0;
			if ((!(((Il2CppObject*)(List_1_t88276517 *)L_5) == ((Il2CppObject*)(List_1_t88276517 *)L_6))))
			{
				goto IL_0035;
			}
		}

IL_002a:
		{
			InvalidOperationException_t1589641621 * L_7 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_m1485483280(L_7, (String_t*)_stringLiteral3527666854, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
		}

IL_0035:
		{
			int32_t L_8 = V_1;
			V_1 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
		}

IL_0039:
		{
			int32_t L_9 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t1456462069 * L_10 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t1456462069 *)L_10);
			int32_t L_11 = ((  int32_t (*) (List_1_t1456462069 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1456462069 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_9) < ((int32_t)L_11)))
			{
				goto IL_0019;
			}
		}

IL_0049:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t1456462069 * L_12 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t88276517 * L_13 = ___list0;
			NullCheck((List_1_t1456462069 *)L_12);
			((  void (*) (List_1_t1456462069 *, List_1_t88276517 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->methodPointer)((List_1_t1456462069 *)L_12, (List_1_t88276517 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
			IL2CPP_LEAVE(0x60, FINALLY_0059);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0059;
	}

FINALLY_0059:
	{ // begin finally (depth: 1)
		List_1_t1456462069 * L_14 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(89)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(89)
	{
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0060:
	{
		return;
	}
}
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.IntRect>::Clear()
extern "C"  void ListPool_1_Clear_m3969013218_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t1456462069 * V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t1456462069 * L_0 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t1456462069 *)L_0;
		List_1_t1456462069 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t1456462069 * L_2 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		NullCheck((List_1_t1456462069 *)L_2);
		((  void (*) (List_1_t1456462069 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->methodPointer)((List_1_t1456462069 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		IL2CPP_LEAVE(0x22, FINALLY_001b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_001b;
	}

FINALLY_001b:
	{ // begin finally (depth: 1)
		List_1_t1456462069 * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(27)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(27)
	{
		IL2CPP_JUMP_TBL(0x22, IL_0022)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0022:
	{
		return;
	}
}
// System.Int32 Pathfinding.Util.ListPool`1<Pathfinding.IntRect>::GetSize()
extern "C"  int32_t ListPool_1_GetSize_m1674812862_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t1456462069 * L_0 = ((ListPool_1_t1300561315_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		NullCheck((List_1_t1456462069 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t1456462069 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1456462069 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void Pathfinding.Util.ListPool`1<System.Boolean>::.cctor()
extern "C"  void ListPool_1__cctor_m1350248616_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		List_1_t3213169822 * L_0 = (List_1_t3213169822 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (List_1_t3213169822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_pool_1(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<System.Boolean>::Claim()
extern "C"  List_1_t1844984270 * ListPool_1_Claim_m3398115476_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t3213169822 * V_0 = NULL;
	List_1_t1844984270 * V_1 = NULL;
	List_1_t1844984270 * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t3213169822 * L_0 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t3213169822 *)L_0;
		List_1_t3213169822 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t3213169822 * L_2 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t3213169822 *)L_2);
			int32_t L_3 = ((  int32_t (*) (List_1_t3213169822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t3213169822 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_3) <= ((int32_t)0)))
			{
				goto IL_0050;
			}
		}

IL_001c:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t3213169822 * L_4 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t3213169822 * L_5 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t3213169822 *)L_5);
			int32_t L_6 = ((  int32_t (*) (List_1_t3213169822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t3213169822 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t3213169822 *)L_4);
			List_1_t1844984270 * L_7 = ((  List_1_t1844984270 * (*) (List_1_t3213169822 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t3213169822 *)L_4, (int32_t)((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			V_1 = (List_1_t1844984270 *)L_7;
			List_1_t3213169822 * L_8 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t3213169822 * L_9 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t3213169822 *)L_9);
			int32_t L_10 = ((  int32_t (*) (List_1_t3213169822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t3213169822 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t3213169822 *)L_8);
			((  void (*) (List_1_t3213169822 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t3213169822 *)L_8, (int32_t)((int32_t)((int32_t)L_10-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			List_1_t1844984270 * L_11 = V_1;
			V_2 = (List_1_t1844984270 *)L_11;
			IL2CPP_LEAVE(0x67, FINALLY_0060);
		}

IL_0050:
		{
			List_1_t1844984270 * L_12 = (List_1_t1844984270 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t1844984270 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			V_2 = (List_1_t1844984270 *)L_12;
			IL2CPP_LEAVE(0x67, FINALLY_0060);
		}

IL_005b:
		{
			; // IL_005b: leave IL_0067
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0060;
	}

FINALLY_0060:
	{ // begin finally (depth: 1)
		List_1_t3213169822 * L_13 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_13, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(96)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(96)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0067:
	{
		List_1_t1844984270 * L_14 = V_2;
		return L_14;
	}
}
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<System.Boolean>::Claim(System.Int32)
extern "C"  List_1_t1844984270 * ListPool_1_Claim_m2905118693_gshared (Il2CppObject * __this /* static, unused */, int32_t ___capacity0, const MethodInfo* method)
{
	List_1_t3213169822 * V_0 = NULL;
	List_1_t1844984270 * V_1 = NULL;
	int32_t V_2 = 0;
	List_1_t1844984270 * V_3 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t3213169822 * L_0 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t3213169822 *)L_0;
		List_1_t3213169822 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t3213169822 * L_2 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t3213169822 *)L_2);
			int32_t L_3 = ((  int32_t (*) (List_1_t3213169822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t3213169822 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_3) <= ((int32_t)0)))
			{
				goto IL_00e6;
			}
		}

IL_001c:
		{
			V_1 = (List_1_t1844984270 *)NULL;
			V_2 = (int32_t)0;
			goto IL_006d;
		}

IL_0025:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t3213169822 * L_4 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t3213169822 * L_5 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t3213169822 *)L_5);
			int32_t L_6 = ((  int32_t (*) (List_1_t3213169822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t3213169822 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_7 = V_2;
			NullCheck((List_1_t3213169822 *)L_4);
			List_1_t1844984270 * L_8 = ((  List_1_t1844984270 * (*) (List_1_t3213169822 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t3213169822 *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			V_1 = (List_1_t1844984270 *)L_8;
			List_1_t1844984270 * L_9 = V_1;
			NullCheck((List_1_t1844984270 *)L_9);
			int32_t L_10 = ((  int32_t (*) (List_1_t1844984270 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t1844984270 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			int32_t L_11 = ___capacity0;
			if ((((int32_t)L_10) < ((int32_t)L_11)))
			{
				goto IL_0069;
			}
		}

IL_004a:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t3213169822 * L_12 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t3213169822 * L_13 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t3213169822 *)L_13);
			int32_t L_14 = ((  int32_t (*) (List_1_t3213169822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t3213169822 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_15 = V_2;
			NullCheck((List_1_t3213169822 *)L_12);
			((  void (*) (List_1_t3213169822 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t3213169822 *)L_12, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_14-(int32_t)1))-(int32_t)L_15)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			List_1_t1844984270 * L_16 = V_1;
			V_3 = (List_1_t1844984270 *)L_16;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_0069:
		{
			int32_t L_17 = V_2;
			V_2 = (int32_t)((int32_t)((int32_t)L_17+(int32_t)1));
		}

IL_006d:
		{
			int32_t L_18 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t3213169822 * L_19 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t3213169822 *)L_19);
			int32_t L_20 = ((  int32_t (*) (List_1_t3213169822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t3213169822 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_18) >= ((int32_t)L_20)))
			{
				goto IL_0084;
			}
		}

IL_007d:
		{
			int32_t L_21 = V_2;
			if ((((int32_t)L_21) < ((int32_t)8)))
			{
				goto IL_0025;
			}
		}

IL_0084:
		{
			List_1_t1844984270 * L_22 = V_1;
			if (L_22)
			{
				goto IL_0096;
			}
		}

IL_008a:
		{
			int32_t L_23 = ___capacity0;
			List_1_t1844984270 * L_24 = (List_1_t1844984270 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t1844984270 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(L_24, (int32_t)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			V_1 = (List_1_t1844984270 *)L_24;
			goto IL_00df;
		}

IL_0096:
		{
			List_1_t1844984270 * L_25 = V_1;
			int32_t L_26 = ___capacity0;
			NullCheck((List_1_t1844984270 *)L_25);
			((  void (*) (List_1_t1844984270 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->methodPointer)((List_1_t1844984270 *)L_25, (int32_t)L_26, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t3213169822 * L_27 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t3213169822 * L_28 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t3213169822 *)L_28);
			int32_t L_29 = ((  int32_t (*) (List_1_t3213169822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t3213169822 *)L_28, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_30 = V_2;
			List_1_t3213169822 * L_31 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t3213169822 * L_32 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t3213169822 *)L_32);
			int32_t L_33 = ((  int32_t (*) (List_1_t3213169822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t3213169822 *)L_32, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t3213169822 *)L_31);
			List_1_t1844984270 * L_34 = ((  List_1_t1844984270 * (*) (List_1_t3213169822 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t3213169822 *)L_31, (int32_t)((int32_t)((int32_t)L_33-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			NullCheck((List_1_t3213169822 *)L_27);
			((  void (*) (List_1_t3213169822 *, int32_t, List_1_t1844984270 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->methodPointer)((List_1_t3213169822 *)L_27, (int32_t)((int32_t)((int32_t)L_29-(int32_t)L_30)), (List_1_t1844984270 *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
			List_1_t3213169822 * L_35 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t3213169822 * L_36 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t3213169822 *)L_36);
			int32_t L_37 = ((  int32_t (*) (List_1_t3213169822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t3213169822 *)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t3213169822 *)L_35);
			((  void (*) (List_1_t3213169822 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t3213169822 *)L_35, (int32_t)((int32_t)((int32_t)L_37-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		}

IL_00df:
		{
			List_1_t1844984270 * L_38 = V_1;
			V_3 = (List_1_t1844984270 *)L_38;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_00e6:
		{
			int32_t L_39 = ___capacity0;
			List_1_t1844984270 * L_40 = (List_1_t1844984270 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t1844984270 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(L_40, (int32_t)L_39, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			V_3 = (List_1_t1844984270 *)L_40;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_00f2:
		{
			; // IL_00f2: leave IL_00fe
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00f7;
	}

FINALLY_00f7:
	{ // begin finally (depth: 1)
		List_1_t3213169822 * L_41 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_41, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(247)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(247)
	{
		IL2CPP_JUMP_TBL(0xFE, IL_00fe)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00fe:
	{
		List_1_t1844984270 * L_42 = V_3;
		return L_42;
	}
}
// System.Void Pathfinding.Util.ListPool`1<System.Boolean>::Warmup(System.Int32,System.Int32)
extern "C"  void ListPool_1_Warmup_m440773287_gshared (Il2CppObject * __this /* static, unused */, int32_t ___count0, int32_t ___size1, const MethodInfo* method)
{
	List_1_t3213169822 * V_0 = NULL;
	List_1U5BU5D_t588894011* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t3213169822 * L_0 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t3213169822 *)L_0;
		List_1_t3213169822 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_2 = ___count0;
			V_1 = (List_1U5BU5D_t588894011*)((List_1U5BU5D_t588894011*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (uint32_t)L_2));
			V_2 = (int32_t)0;
			goto IL_0027;
		}

IL_001a:
		{
			List_1U5BU5D_t588894011* L_3 = V_1;
			int32_t L_4 = V_2;
			int32_t L_5 = ___size1;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t1844984270 * L_6 = ((  List_1_t1844984270 * (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->methodPointer)(NULL /*static, unused*/, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
			NullCheck(L_3);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
			ArrayElementTypeCheck (L_3, L_6);
			(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (List_1_t1844984270 *)L_6);
			int32_t L_7 = V_2;
			V_2 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
		}

IL_0027:
		{
			int32_t L_8 = V_2;
			int32_t L_9 = ___count0;
			if ((((int32_t)L_8) < ((int32_t)L_9)))
			{
				goto IL_001a;
			}
		}

IL_002e:
		{
			V_3 = (int32_t)0;
			goto IL_0041;
		}

IL_0035:
		{
			List_1U5BU5D_t588894011* L_10 = V_1;
			int32_t L_11 = V_3;
			NullCheck(L_10);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
			int32_t L_12 = L_11;
			List_1_t1844984270 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			((  void (*) (Il2CppObject * /* static, unused */, List_1_t1844984270 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->methodPointer)(NULL /*static, unused*/, (List_1_t1844984270 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
			int32_t L_14 = V_3;
			V_3 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
		}

IL_0041:
		{
			int32_t L_15 = V_3;
			int32_t L_16 = ___count0;
			if ((((int32_t)L_15) < ((int32_t)L_16)))
			{
				goto IL_0035;
			}
		}

IL_0048:
		{
			IL2CPP_LEAVE(0x54, FINALLY_004d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_004d;
	}

FINALLY_004d:
	{ // begin finally (depth: 1)
		List_1_t3213169822 * L_17 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_17, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(77)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(77)
	{
		IL2CPP_JUMP_TBL(0x54, IL_0054)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0054:
	{
		return;
	}
}
// System.Void Pathfinding.Util.ListPool`1<System.Boolean>::Release(System.Collections.Generic.List`1<T>)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3527666854;
extern const uint32_t ListPool_1_Release_m4050264546_MetadataUsageId;
extern "C"  void ListPool_1_Release_m4050264546_gshared (Il2CppObject * __this /* static, unused */, List_1_t1844984270 * ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListPool_1_Release_m4050264546_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t3213169822 * V_0 = NULL;
	int32_t V_1 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1844984270 * L_0 = ___list0;
		NullCheck((List_1_t1844984270 *)L_0);
		((  void (*) (List_1_t1844984270 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->methodPointer)((List_1_t1844984270 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t3213169822 * L_1 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t3213169822 *)L_1;
		List_1_t3213169822 * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			V_1 = (int32_t)0;
			goto IL_0039;
		}

IL_0019:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t3213169822 * L_3 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			int32_t L_4 = V_1;
			NullCheck((List_1_t3213169822 *)L_3);
			List_1_t1844984270 * L_5 = ((  List_1_t1844984270 * (*) (List_1_t3213169822 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t3213169822 *)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			List_1_t1844984270 * L_6 = ___list0;
			if ((!(((Il2CppObject*)(List_1_t1844984270 *)L_5) == ((Il2CppObject*)(List_1_t1844984270 *)L_6))))
			{
				goto IL_0035;
			}
		}

IL_002a:
		{
			InvalidOperationException_t1589641621 * L_7 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_m1485483280(L_7, (String_t*)_stringLiteral3527666854, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
		}

IL_0035:
		{
			int32_t L_8 = V_1;
			V_1 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
		}

IL_0039:
		{
			int32_t L_9 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t3213169822 * L_10 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t3213169822 *)L_10);
			int32_t L_11 = ((  int32_t (*) (List_1_t3213169822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t3213169822 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_9) < ((int32_t)L_11)))
			{
				goto IL_0019;
			}
		}

IL_0049:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t3213169822 * L_12 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t1844984270 * L_13 = ___list0;
			NullCheck((List_1_t3213169822 *)L_12);
			((  void (*) (List_1_t3213169822 *, List_1_t1844984270 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->methodPointer)((List_1_t3213169822 *)L_12, (List_1_t1844984270 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
			IL2CPP_LEAVE(0x60, FINALLY_0059);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0059;
	}

FINALLY_0059:
	{ // begin finally (depth: 1)
		List_1_t3213169822 * L_14 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(89)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(89)
	{
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0060:
	{
		return;
	}
}
// System.Void Pathfinding.Util.ListPool`1<System.Boolean>::Clear()
extern "C"  void ListPool_1_Clear_m1067471824_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t3213169822 * V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t3213169822 * L_0 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t3213169822 *)L_0;
		List_1_t3213169822 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t3213169822 * L_2 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		NullCheck((List_1_t3213169822 *)L_2);
		((  void (*) (List_1_t3213169822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->methodPointer)((List_1_t3213169822 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		IL2CPP_LEAVE(0x22, FINALLY_001b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_001b;
	}

FINALLY_001b:
	{ // begin finally (depth: 1)
		List_1_t3213169822 * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(27)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(27)
	{
		IL2CPP_JUMP_TBL(0x22, IL_0022)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0022:
	{
		return;
	}
}
// System.Int32 Pathfinding.Util.ListPool`1<System.Boolean>::GetSize()
extern "C"  int32_t ListPool_1_GetSize_m699885192_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t3213169822 * L_0 = ((ListPool_1_t3057269068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		NullCheck((List_1_t3213169822 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t3213169822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t3213169822 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void Pathfinding.Util.ListPool`1<System.Int32>::.cctor()
extern "C"  void ListPool_1__cctor_m4123663490_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		List_1_t3890209604 * L_0 = (List_1_t3890209604 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (List_1_t3890209604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_pool_1(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<System.Int32>::Claim()
extern "C"  List_1_t2522024052 * ListPool_1_Claim_m3605064378_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t3890209604 * V_0 = NULL;
	List_1_t2522024052 * V_1 = NULL;
	List_1_t2522024052 * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t3890209604 * L_0 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t3890209604 *)L_0;
		List_1_t3890209604 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t3890209604 * L_2 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t3890209604 *)L_2);
			int32_t L_3 = ((  int32_t (*) (List_1_t3890209604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t3890209604 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_3) <= ((int32_t)0)))
			{
				goto IL_0050;
			}
		}

IL_001c:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t3890209604 * L_4 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t3890209604 * L_5 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t3890209604 *)L_5);
			int32_t L_6 = ((  int32_t (*) (List_1_t3890209604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t3890209604 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t3890209604 *)L_4);
			List_1_t2522024052 * L_7 = ((  List_1_t2522024052 * (*) (List_1_t3890209604 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t3890209604 *)L_4, (int32_t)((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			V_1 = (List_1_t2522024052 *)L_7;
			List_1_t3890209604 * L_8 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t3890209604 * L_9 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t3890209604 *)L_9);
			int32_t L_10 = ((  int32_t (*) (List_1_t3890209604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t3890209604 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t3890209604 *)L_8);
			((  void (*) (List_1_t3890209604 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t3890209604 *)L_8, (int32_t)((int32_t)((int32_t)L_10-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			List_1_t2522024052 * L_11 = V_1;
			V_2 = (List_1_t2522024052 *)L_11;
			IL2CPP_LEAVE(0x67, FINALLY_0060);
		}

IL_0050:
		{
			List_1_t2522024052 * L_12 = (List_1_t2522024052 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t2522024052 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			V_2 = (List_1_t2522024052 *)L_12;
			IL2CPP_LEAVE(0x67, FINALLY_0060);
		}

IL_005b:
		{
			; // IL_005b: leave IL_0067
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0060;
	}

FINALLY_0060:
	{ // begin finally (depth: 1)
		List_1_t3890209604 * L_13 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_13, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(96)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(96)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0067:
	{
		List_1_t2522024052 * L_14 = V_2;
		return L_14;
	}
}
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<System.Int32>::Claim(System.Int32)
extern "C"  List_1_t2522024052 * ListPool_1_Claim_m2895272203_gshared (Il2CppObject * __this /* static, unused */, int32_t ___capacity0, const MethodInfo* method)
{
	List_1_t3890209604 * V_0 = NULL;
	List_1_t2522024052 * V_1 = NULL;
	int32_t V_2 = 0;
	List_1_t2522024052 * V_3 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t3890209604 * L_0 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t3890209604 *)L_0;
		List_1_t3890209604 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t3890209604 * L_2 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t3890209604 *)L_2);
			int32_t L_3 = ((  int32_t (*) (List_1_t3890209604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t3890209604 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_3) <= ((int32_t)0)))
			{
				goto IL_00e6;
			}
		}

IL_001c:
		{
			V_1 = (List_1_t2522024052 *)NULL;
			V_2 = (int32_t)0;
			goto IL_006d;
		}

IL_0025:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t3890209604 * L_4 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t3890209604 * L_5 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t3890209604 *)L_5);
			int32_t L_6 = ((  int32_t (*) (List_1_t3890209604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t3890209604 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_7 = V_2;
			NullCheck((List_1_t3890209604 *)L_4);
			List_1_t2522024052 * L_8 = ((  List_1_t2522024052 * (*) (List_1_t3890209604 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t3890209604 *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			V_1 = (List_1_t2522024052 *)L_8;
			List_1_t2522024052 * L_9 = V_1;
			NullCheck((List_1_t2522024052 *)L_9);
			int32_t L_10 = ((  int32_t (*) (List_1_t2522024052 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t2522024052 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			int32_t L_11 = ___capacity0;
			if ((((int32_t)L_10) < ((int32_t)L_11)))
			{
				goto IL_0069;
			}
		}

IL_004a:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t3890209604 * L_12 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t3890209604 * L_13 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t3890209604 *)L_13);
			int32_t L_14 = ((  int32_t (*) (List_1_t3890209604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t3890209604 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_15 = V_2;
			NullCheck((List_1_t3890209604 *)L_12);
			((  void (*) (List_1_t3890209604 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t3890209604 *)L_12, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_14-(int32_t)1))-(int32_t)L_15)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			List_1_t2522024052 * L_16 = V_1;
			V_3 = (List_1_t2522024052 *)L_16;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_0069:
		{
			int32_t L_17 = V_2;
			V_2 = (int32_t)((int32_t)((int32_t)L_17+(int32_t)1));
		}

IL_006d:
		{
			int32_t L_18 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t3890209604 * L_19 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t3890209604 *)L_19);
			int32_t L_20 = ((  int32_t (*) (List_1_t3890209604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t3890209604 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_18) >= ((int32_t)L_20)))
			{
				goto IL_0084;
			}
		}

IL_007d:
		{
			int32_t L_21 = V_2;
			if ((((int32_t)L_21) < ((int32_t)8)))
			{
				goto IL_0025;
			}
		}

IL_0084:
		{
			List_1_t2522024052 * L_22 = V_1;
			if (L_22)
			{
				goto IL_0096;
			}
		}

IL_008a:
		{
			int32_t L_23 = ___capacity0;
			List_1_t2522024052 * L_24 = (List_1_t2522024052 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(L_24, (int32_t)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			V_1 = (List_1_t2522024052 *)L_24;
			goto IL_00df;
		}

IL_0096:
		{
			List_1_t2522024052 * L_25 = V_1;
			int32_t L_26 = ___capacity0;
			NullCheck((List_1_t2522024052 *)L_25);
			((  void (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->methodPointer)((List_1_t2522024052 *)L_25, (int32_t)L_26, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t3890209604 * L_27 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t3890209604 * L_28 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t3890209604 *)L_28);
			int32_t L_29 = ((  int32_t (*) (List_1_t3890209604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t3890209604 *)L_28, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_30 = V_2;
			List_1_t3890209604 * L_31 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t3890209604 * L_32 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t3890209604 *)L_32);
			int32_t L_33 = ((  int32_t (*) (List_1_t3890209604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t3890209604 *)L_32, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t3890209604 *)L_31);
			List_1_t2522024052 * L_34 = ((  List_1_t2522024052 * (*) (List_1_t3890209604 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t3890209604 *)L_31, (int32_t)((int32_t)((int32_t)L_33-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			NullCheck((List_1_t3890209604 *)L_27);
			((  void (*) (List_1_t3890209604 *, int32_t, List_1_t2522024052 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->methodPointer)((List_1_t3890209604 *)L_27, (int32_t)((int32_t)((int32_t)L_29-(int32_t)L_30)), (List_1_t2522024052 *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
			List_1_t3890209604 * L_35 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t3890209604 * L_36 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t3890209604 *)L_36);
			int32_t L_37 = ((  int32_t (*) (List_1_t3890209604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t3890209604 *)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t3890209604 *)L_35);
			((  void (*) (List_1_t3890209604 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t3890209604 *)L_35, (int32_t)((int32_t)((int32_t)L_37-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		}

IL_00df:
		{
			List_1_t2522024052 * L_38 = V_1;
			V_3 = (List_1_t2522024052 *)L_38;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_00e6:
		{
			int32_t L_39 = ___capacity0;
			List_1_t2522024052 * L_40 = (List_1_t2522024052 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(L_40, (int32_t)L_39, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			V_3 = (List_1_t2522024052 *)L_40;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_00f2:
		{
			; // IL_00f2: leave IL_00fe
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00f7;
	}

FINALLY_00f7:
	{ // begin finally (depth: 1)
		List_1_t3890209604 * L_41 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_41, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(247)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(247)
	{
		IL2CPP_JUMP_TBL(0xFE, IL_00fe)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00fe:
	{
		List_1_t2522024052 * L_42 = V_3;
		return L_42;
	}
}
// System.Void Pathfinding.Util.ListPool`1<System.Int32>::Warmup(System.Int32,System.Int32)
extern "C"  void ListPool_1_Warmup_m1169088525_gshared (Il2CppObject * __this /* static, unused */, int32_t ___count0, int32_t ___size1, const MethodInfo* method)
{
	List_1_t3890209604 * V_0 = NULL;
	List_1U5BU5D_t363438909* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t3890209604 * L_0 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t3890209604 *)L_0;
		List_1_t3890209604 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_2 = ___count0;
			V_1 = (List_1U5BU5D_t363438909*)((List_1U5BU5D_t363438909*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (uint32_t)L_2));
			V_2 = (int32_t)0;
			goto IL_0027;
		}

IL_001a:
		{
			List_1U5BU5D_t363438909* L_3 = V_1;
			int32_t L_4 = V_2;
			int32_t L_5 = ___size1;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2522024052 * L_6 = ((  List_1_t2522024052 * (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->methodPointer)(NULL /*static, unused*/, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
			NullCheck(L_3);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
			ArrayElementTypeCheck (L_3, L_6);
			(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (List_1_t2522024052 *)L_6);
			int32_t L_7 = V_2;
			V_2 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
		}

IL_0027:
		{
			int32_t L_8 = V_2;
			int32_t L_9 = ___count0;
			if ((((int32_t)L_8) < ((int32_t)L_9)))
			{
				goto IL_001a;
			}
		}

IL_002e:
		{
			V_3 = (int32_t)0;
			goto IL_0041;
		}

IL_0035:
		{
			List_1U5BU5D_t363438909* L_10 = V_1;
			int32_t L_11 = V_3;
			NullCheck(L_10);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
			int32_t L_12 = L_11;
			List_1_t2522024052 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			((  void (*) (Il2CppObject * /* static, unused */, List_1_t2522024052 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->methodPointer)(NULL /*static, unused*/, (List_1_t2522024052 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
			int32_t L_14 = V_3;
			V_3 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
		}

IL_0041:
		{
			int32_t L_15 = V_3;
			int32_t L_16 = ___count0;
			if ((((int32_t)L_15) < ((int32_t)L_16)))
			{
				goto IL_0035;
			}
		}

IL_0048:
		{
			IL2CPP_LEAVE(0x54, FINALLY_004d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_004d;
	}

FINALLY_004d:
	{ // begin finally (depth: 1)
		List_1_t3890209604 * L_17 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_17, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(77)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(77)
	{
		IL2CPP_JUMP_TBL(0x54, IL_0054)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0054:
	{
		return;
	}
}
// System.Void Pathfinding.Util.ListPool`1<System.Int32>::Release(System.Collections.Generic.List`1<T>)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3527666854;
extern const uint32_t ListPool_1_Release_m1876979272_MetadataUsageId;
extern "C"  void ListPool_1_Release_m1876979272_gshared (Il2CppObject * __this /* static, unused */, List_1_t2522024052 * ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListPool_1_Release_m1876979272_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t3890209604 * V_0 = NULL;
	int32_t V_1 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t2522024052 * L_0 = ___list0;
		NullCheck((List_1_t2522024052 *)L_0);
		((  void (*) (List_1_t2522024052 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->methodPointer)((List_1_t2522024052 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t3890209604 * L_1 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t3890209604 *)L_1;
		List_1_t3890209604 * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			V_1 = (int32_t)0;
			goto IL_0039;
		}

IL_0019:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t3890209604 * L_3 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			int32_t L_4 = V_1;
			NullCheck((List_1_t3890209604 *)L_3);
			List_1_t2522024052 * L_5 = ((  List_1_t2522024052 * (*) (List_1_t3890209604 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t3890209604 *)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			List_1_t2522024052 * L_6 = ___list0;
			if ((!(((Il2CppObject*)(List_1_t2522024052 *)L_5) == ((Il2CppObject*)(List_1_t2522024052 *)L_6))))
			{
				goto IL_0035;
			}
		}

IL_002a:
		{
			InvalidOperationException_t1589641621 * L_7 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_m1485483280(L_7, (String_t*)_stringLiteral3527666854, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
		}

IL_0035:
		{
			int32_t L_8 = V_1;
			V_1 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
		}

IL_0039:
		{
			int32_t L_9 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t3890209604 * L_10 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t3890209604 *)L_10);
			int32_t L_11 = ((  int32_t (*) (List_1_t3890209604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t3890209604 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_9) < ((int32_t)L_11)))
			{
				goto IL_0019;
			}
		}

IL_0049:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t3890209604 * L_12 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2522024052 * L_13 = ___list0;
			NullCheck((List_1_t3890209604 *)L_12);
			((  void (*) (List_1_t3890209604 *, List_1_t2522024052 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->methodPointer)((List_1_t3890209604 *)L_12, (List_1_t2522024052 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
			IL2CPP_LEAVE(0x60, FINALLY_0059);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0059;
	}

FINALLY_0059:
	{ // begin finally (depth: 1)
		List_1_t3890209604 * L_14 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(89)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(89)
	{
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0060:
	{
		return;
	}
}
// System.Void Pathfinding.Util.ListPool`1<System.Int32>::Clear()
extern "C"  void ListPool_1_Clear_m3373694134_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t3890209604 * V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t3890209604 * L_0 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t3890209604 *)L_0;
		List_1_t3890209604 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t3890209604 * L_2 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		NullCheck((List_1_t3890209604 *)L_2);
		((  void (*) (List_1_t3890209604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->methodPointer)((List_1_t3890209604 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		IL2CPP_LEAVE(0x22, FINALLY_001b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_001b;
	}

FINALLY_001b:
	{ // begin finally (depth: 1)
		List_1_t3890209604 * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(27)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(27)
	{
		IL2CPP_JUMP_TBL(0x22, IL_0022)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0022:
	{
		return;
	}
}
// System.Int32 Pathfinding.Util.ListPool`1<System.Int32>::GetSize()
extern "C"  int32_t ListPool_1_GetSize_m1373665646_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t3890209604 * L_0 = ((ListPool_1_t3734308850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		NullCheck((List_1_t3890209604 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t3890209604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t3890209604 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void Pathfinding.Util.ListPool`1<System.Object>::.cctor()
extern "C"  void ListPool_1__cctor_m2656574753_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		List_1_t2612220179 * L_0 = (List_1_t2612220179 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (List_1_t2612220179 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_pool_1(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<System.Object>::Claim()
extern "C"  List_1_t1244034627 * ListPool_1_Claim_m2151275825_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t2612220179 * V_0 = NULL;
	List_1_t1244034627 * V_1 = NULL;
	List_1_t1244034627 * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2612220179 * L_0 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t2612220179 *)L_0;
		List_1_t2612220179 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2612220179 * L_2 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2612220179 *)L_2);
			int32_t L_3 = ((  int32_t (*) (List_1_t2612220179 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2612220179 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_3) <= ((int32_t)0)))
			{
				goto IL_0050;
			}
		}

IL_001c:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2612220179 * L_4 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2612220179 * L_5 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2612220179 *)L_5);
			int32_t L_6 = ((  int32_t (*) (List_1_t2612220179 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2612220179 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t2612220179 *)L_4);
			List_1_t1244034627 * L_7 = ((  List_1_t1244034627 * (*) (List_1_t2612220179 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t2612220179 *)L_4, (int32_t)((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			V_1 = (List_1_t1244034627 *)L_7;
			List_1_t2612220179 * L_8 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2612220179 * L_9 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2612220179 *)L_9);
			int32_t L_10 = ((  int32_t (*) (List_1_t2612220179 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2612220179 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t2612220179 *)L_8);
			((  void (*) (List_1_t2612220179 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t2612220179 *)L_8, (int32_t)((int32_t)((int32_t)L_10-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			List_1_t1244034627 * L_11 = V_1;
			V_2 = (List_1_t1244034627 *)L_11;
			IL2CPP_LEAVE(0x67, FINALLY_0060);
		}

IL_0050:
		{
			List_1_t1244034627 * L_12 = (List_1_t1244034627 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			V_2 = (List_1_t1244034627 *)L_12;
			IL2CPP_LEAVE(0x67, FINALLY_0060);
		}

IL_005b:
		{
			; // IL_005b: leave IL_0067
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0060;
	}

FINALLY_0060:
	{ // begin finally (depth: 1)
		List_1_t2612220179 * L_13 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_13, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(96)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(96)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0067:
	{
		List_1_t1244034627 * L_14 = V_2;
		return L_14;
	}
}
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<System.Object>::Claim(System.Int32)
extern "C"  List_1_t1244034627 * ListPool_1_Claim_m3438825730_gshared (Il2CppObject * __this /* static, unused */, int32_t ___capacity0, const MethodInfo* method)
{
	List_1_t2612220179 * V_0 = NULL;
	List_1_t1244034627 * V_1 = NULL;
	int32_t V_2 = 0;
	List_1_t1244034627 * V_3 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2612220179 * L_0 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t2612220179 *)L_0;
		List_1_t2612220179 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2612220179 * L_2 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2612220179 *)L_2);
			int32_t L_3 = ((  int32_t (*) (List_1_t2612220179 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2612220179 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_3) <= ((int32_t)0)))
			{
				goto IL_00e6;
			}
		}

IL_001c:
		{
			V_1 = (List_1_t1244034627 *)NULL;
			V_2 = (int32_t)0;
			goto IL_006d;
		}

IL_0025:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2612220179 * L_4 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2612220179 * L_5 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2612220179 *)L_5);
			int32_t L_6 = ((  int32_t (*) (List_1_t2612220179 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2612220179 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_7 = V_2;
			NullCheck((List_1_t2612220179 *)L_4);
			List_1_t1244034627 * L_8 = ((  List_1_t1244034627 * (*) (List_1_t2612220179 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t2612220179 *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			V_1 = (List_1_t1244034627 *)L_8;
			List_1_t1244034627 * L_9 = V_1;
			NullCheck((List_1_t1244034627 *)L_9);
			int32_t L_10 = ((  int32_t (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t1244034627 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			int32_t L_11 = ___capacity0;
			if ((((int32_t)L_10) < ((int32_t)L_11)))
			{
				goto IL_0069;
			}
		}

IL_004a:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2612220179 * L_12 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2612220179 * L_13 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2612220179 *)L_13);
			int32_t L_14 = ((  int32_t (*) (List_1_t2612220179 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2612220179 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_15 = V_2;
			NullCheck((List_1_t2612220179 *)L_12);
			((  void (*) (List_1_t2612220179 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t2612220179 *)L_12, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_14-(int32_t)1))-(int32_t)L_15)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			List_1_t1244034627 * L_16 = V_1;
			V_3 = (List_1_t1244034627 *)L_16;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_0069:
		{
			int32_t L_17 = V_2;
			V_2 = (int32_t)((int32_t)((int32_t)L_17+(int32_t)1));
		}

IL_006d:
		{
			int32_t L_18 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2612220179 * L_19 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2612220179 *)L_19);
			int32_t L_20 = ((  int32_t (*) (List_1_t2612220179 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2612220179 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_18) >= ((int32_t)L_20)))
			{
				goto IL_0084;
			}
		}

IL_007d:
		{
			int32_t L_21 = V_2;
			if ((((int32_t)L_21) < ((int32_t)8)))
			{
				goto IL_0025;
			}
		}

IL_0084:
		{
			List_1_t1244034627 * L_22 = V_1;
			if (L_22)
			{
				goto IL_0096;
			}
		}

IL_008a:
		{
			int32_t L_23 = ___capacity0;
			List_1_t1244034627 * L_24 = (List_1_t1244034627 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t1244034627 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(L_24, (int32_t)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			V_1 = (List_1_t1244034627 *)L_24;
			goto IL_00df;
		}

IL_0096:
		{
			List_1_t1244034627 * L_25 = V_1;
			int32_t L_26 = ___capacity0;
			NullCheck((List_1_t1244034627 *)L_25);
			((  void (*) (List_1_t1244034627 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->methodPointer)((List_1_t1244034627 *)L_25, (int32_t)L_26, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2612220179 * L_27 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2612220179 * L_28 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2612220179 *)L_28);
			int32_t L_29 = ((  int32_t (*) (List_1_t2612220179 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2612220179 *)L_28, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_30 = V_2;
			List_1_t2612220179 * L_31 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2612220179 * L_32 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2612220179 *)L_32);
			int32_t L_33 = ((  int32_t (*) (List_1_t2612220179 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2612220179 *)L_32, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t2612220179 *)L_31);
			List_1_t1244034627 * L_34 = ((  List_1_t1244034627 * (*) (List_1_t2612220179 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t2612220179 *)L_31, (int32_t)((int32_t)((int32_t)L_33-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			NullCheck((List_1_t2612220179 *)L_27);
			((  void (*) (List_1_t2612220179 *, int32_t, List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->methodPointer)((List_1_t2612220179 *)L_27, (int32_t)((int32_t)((int32_t)L_29-(int32_t)L_30)), (List_1_t1244034627 *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
			List_1_t2612220179 * L_35 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2612220179 * L_36 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2612220179 *)L_36);
			int32_t L_37 = ((  int32_t (*) (List_1_t2612220179 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2612220179 *)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t2612220179 *)L_35);
			((  void (*) (List_1_t2612220179 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t2612220179 *)L_35, (int32_t)((int32_t)((int32_t)L_37-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		}

IL_00df:
		{
			List_1_t1244034627 * L_38 = V_1;
			V_3 = (List_1_t1244034627 *)L_38;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_00e6:
		{
			int32_t L_39 = ___capacity0;
			List_1_t1244034627 * L_40 = (List_1_t1244034627 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t1244034627 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(L_40, (int32_t)L_39, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			V_3 = (List_1_t1244034627 *)L_40;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_00f2:
		{
			; // IL_00f2: leave IL_00fe
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00f7;
	}

FINALLY_00f7:
	{ // begin finally (depth: 1)
		List_1_t2612220179 * L_41 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_41, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(247)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(247)
	{
		IL2CPP_JUMP_TBL(0xFE, IL_00fe)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00fe:
	{
		List_1_t1244034627 * L_42 = V_3;
		return L_42;
	}
}
// System.Void Pathfinding.Util.ListPool`1<System.Object>::Warmup(System.Int32,System.Int32)
extern "C"  void ListPool_1_Warmup_m3738927182_gshared (Il2CppObject * __this /* static, unused */, int32_t ___count0, int32_t ___size1, const MethodInfo* method)
{
	List_1_t2612220179 * V_0 = NULL;
	List_1U5BU5D_t2536214866* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2612220179 * L_0 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t2612220179 *)L_0;
		List_1_t2612220179 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_2 = ___count0;
			V_1 = (List_1U5BU5D_t2536214866*)((List_1U5BU5D_t2536214866*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (uint32_t)L_2));
			V_2 = (int32_t)0;
			goto IL_0027;
		}

IL_001a:
		{
			List_1U5BU5D_t2536214866* L_3 = V_1;
			int32_t L_4 = V_2;
			int32_t L_5 = ___size1;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t1244034627 * L_6 = ((  List_1_t1244034627 * (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->methodPointer)(NULL /*static, unused*/, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
			NullCheck(L_3);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
			ArrayElementTypeCheck (L_3, L_6);
			(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (List_1_t1244034627 *)L_6);
			int32_t L_7 = V_2;
			V_2 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
		}

IL_0027:
		{
			int32_t L_8 = V_2;
			int32_t L_9 = ___count0;
			if ((((int32_t)L_8) < ((int32_t)L_9)))
			{
				goto IL_001a;
			}
		}

IL_002e:
		{
			V_3 = (int32_t)0;
			goto IL_0041;
		}

IL_0035:
		{
			List_1U5BU5D_t2536214866* L_10 = V_1;
			int32_t L_11 = V_3;
			NullCheck(L_10);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
			int32_t L_12 = L_11;
			List_1_t1244034627 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			((  void (*) (Il2CppObject * /* static, unused */, List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->methodPointer)(NULL /*static, unused*/, (List_1_t1244034627 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
			int32_t L_14 = V_3;
			V_3 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
		}

IL_0041:
		{
			int32_t L_15 = V_3;
			int32_t L_16 = ___count0;
			if ((((int32_t)L_15) < ((int32_t)L_16)))
			{
				goto IL_0035;
			}
		}

IL_0048:
		{
			IL2CPP_LEAVE(0x54, FINALLY_004d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_004d;
	}

FINALLY_004d:
	{ // begin finally (depth: 1)
		List_1_t2612220179 * L_17 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_17, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(77)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(77)
	{
		IL2CPP_JUMP_TBL(0x54, IL_0054)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0054:
	{
		return;
	}
}
// System.Void Pathfinding.Util.ListPool`1<System.Object>::Release(System.Collections.Generic.List`1<T>)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3527666854;
extern const uint32_t ListPool_1_Release_m442610953_MetadataUsageId;
extern "C"  void ListPool_1_Release_m442610953_gshared (Il2CppObject * __this /* static, unused */, List_1_t1244034627 * ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListPool_1_Release_m442610953_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t2612220179 * V_0 = NULL;
	int32_t V_1 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1244034627 * L_0 = ___list0;
		NullCheck((List_1_t1244034627 *)L_0);
		((  void (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->methodPointer)((List_1_t1244034627 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2612220179 * L_1 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t2612220179 *)L_1;
		List_1_t2612220179 * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			V_1 = (int32_t)0;
			goto IL_0039;
		}

IL_0019:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2612220179 * L_3 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			int32_t L_4 = V_1;
			NullCheck((List_1_t2612220179 *)L_3);
			List_1_t1244034627 * L_5 = ((  List_1_t1244034627 * (*) (List_1_t2612220179 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t2612220179 *)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			List_1_t1244034627 * L_6 = ___list0;
			if ((!(((Il2CppObject*)(List_1_t1244034627 *)L_5) == ((Il2CppObject*)(List_1_t1244034627 *)L_6))))
			{
				goto IL_0035;
			}
		}

IL_002a:
		{
			InvalidOperationException_t1589641621 * L_7 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_m1485483280(L_7, (String_t*)_stringLiteral3527666854, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
		}

IL_0035:
		{
			int32_t L_8 = V_1;
			V_1 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
		}

IL_0039:
		{
			int32_t L_9 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2612220179 * L_10 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2612220179 *)L_10);
			int32_t L_11 = ((  int32_t (*) (List_1_t2612220179 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2612220179 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_9) < ((int32_t)L_11)))
			{
				goto IL_0019;
			}
		}

IL_0049:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2612220179 * L_12 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t1244034627 * L_13 = ___list0;
			NullCheck((List_1_t2612220179 *)L_12);
			((  void (*) (List_1_t2612220179 *, List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->methodPointer)((List_1_t2612220179 *)L_12, (List_1_t1244034627 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
			IL2CPP_LEAVE(0x60, FINALLY_0059);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0059;
	}

FINALLY_0059:
	{ // begin finally (depth: 1)
		List_1_t2612220179 * L_14 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(89)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(89)
	{
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0060:
	{
		return;
	}
}
// System.Void Pathfinding.Util.ListPool`1<System.Object>::Clear()
extern "C"  void ListPool_1_Clear_m3464916023_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t2612220179 * V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2612220179 * L_0 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t2612220179 *)L_0;
		List_1_t2612220179 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2612220179 * L_2 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		NullCheck((List_1_t2612220179 *)L_2);
		((  void (*) (List_1_t2612220179 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->methodPointer)((List_1_t2612220179 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		IL2CPP_LEAVE(0x22, FINALLY_001b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_001b;
	}

FINALLY_001b:
	{ // begin finally (depth: 1)
		List_1_t2612220179 * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(27)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(27)
	{
		IL2CPP_JUMP_TBL(0x22, IL_0022)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0022:
	{
		return;
	}
}
// System.Int32 Pathfinding.Util.ListPool`1<System.Object>::GetSize()
extern "C"  int32_t ListPool_1_GetSize_m4059414611_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2612220179 * L_0 = ((ListPool_1_t2456319425_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		NullCheck((List_1_t2612220179 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t2612220179 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2612220179 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void Pathfinding.Util.ListPool`1<System.Single>::.cctor()
extern "C"  void ListPool_1__cctor_m643329144_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		List_1_t2733322780 * L_0 = (List_1_t2733322780 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (List_1_t2733322780 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_pool_1(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<System.Single>::Claim()
extern "C"  List_1_t1365137228 * ListPool_1_Claim_m1809237754_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t2733322780 * V_0 = NULL;
	List_1_t1365137228 * V_1 = NULL;
	List_1_t1365137228 * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2733322780 * L_0 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t2733322780 *)L_0;
		List_1_t2733322780 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2733322780 * L_2 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2733322780 *)L_2);
			int32_t L_3 = ((  int32_t (*) (List_1_t2733322780 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2733322780 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_3) <= ((int32_t)0)))
			{
				goto IL_0050;
			}
		}

IL_001c:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2733322780 * L_4 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2733322780 * L_5 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2733322780 *)L_5);
			int32_t L_6 = ((  int32_t (*) (List_1_t2733322780 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2733322780 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t2733322780 *)L_4);
			List_1_t1365137228 * L_7 = ((  List_1_t1365137228 * (*) (List_1_t2733322780 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t2733322780 *)L_4, (int32_t)((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			V_1 = (List_1_t1365137228 *)L_7;
			List_1_t2733322780 * L_8 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2733322780 * L_9 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2733322780 *)L_9);
			int32_t L_10 = ((  int32_t (*) (List_1_t2733322780 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2733322780 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t2733322780 *)L_8);
			((  void (*) (List_1_t2733322780 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t2733322780 *)L_8, (int32_t)((int32_t)((int32_t)L_10-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			List_1_t1365137228 * L_11 = V_1;
			V_2 = (List_1_t1365137228 *)L_11;
			IL2CPP_LEAVE(0x67, FINALLY_0060);
		}

IL_0050:
		{
			List_1_t1365137228 * L_12 = (List_1_t1365137228 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t1365137228 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			V_2 = (List_1_t1365137228 *)L_12;
			IL2CPP_LEAVE(0x67, FINALLY_0060);
		}

IL_005b:
		{
			; // IL_005b: leave IL_0067
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0060;
	}

FINALLY_0060:
	{ // begin finally (depth: 1)
		List_1_t2733322780 * L_13 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_13, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(96)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(96)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0067:
	{
		List_1_t1365137228 * L_14 = V_2;
		return L_14;
	}
}
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<System.Single>::Claim(System.Int32)
extern "C"  List_1_t1365137228 * ListPool_1_Claim_m3026179403_gshared (Il2CppObject * __this /* static, unused */, int32_t ___capacity0, const MethodInfo* method)
{
	List_1_t2733322780 * V_0 = NULL;
	List_1_t1365137228 * V_1 = NULL;
	int32_t V_2 = 0;
	List_1_t1365137228 * V_3 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2733322780 * L_0 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t2733322780 *)L_0;
		List_1_t2733322780 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2733322780 * L_2 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2733322780 *)L_2);
			int32_t L_3 = ((  int32_t (*) (List_1_t2733322780 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2733322780 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_3) <= ((int32_t)0)))
			{
				goto IL_00e6;
			}
		}

IL_001c:
		{
			V_1 = (List_1_t1365137228 *)NULL;
			V_2 = (int32_t)0;
			goto IL_006d;
		}

IL_0025:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2733322780 * L_4 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2733322780 * L_5 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2733322780 *)L_5);
			int32_t L_6 = ((  int32_t (*) (List_1_t2733322780 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2733322780 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_7 = V_2;
			NullCheck((List_1_t2733322780 *)L_4);
			List_1_t1365137228 * L_8 = ((  List_1_t1365137228 * (*) (List_1_t2733322780 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t2733322780 *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			V_1 = (List_1_t1365137228 *)L_8;
			List_1_t1365137228 * L_9 = V_1;
			NullCheck((List_1_t1365137228 *)L_9);
			int32_t L_10 = ((  int32_t (*) (List_1_t1365137228 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t1365137228 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			int32_t L_11 = ___capacity0;
			if ((((int32_t)L_10) < ((int32_t)L_11)))
			{
				goto IL_0069;
			}
		}

IL_004a:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2733322780 * L_12 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2733322780 * L_13 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2733322780 *)L_13);
			int32_t L_14 = ((  int32_t (*) (List_1_t2733322780 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2733322780 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_15 = V_2;
			NullCheck((List_1_t2733322780 *)L_12);
			((  void (*) (List_1_t2733322780 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t2733322780 *)L_12, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_14-(int32_t)1))-(int32_t)L_15)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			List_1_t1365137228 * L_16 = V_1;
			V_3 = (List_1_t1365137228 *)L_16;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_0069:
		{
			int32_t L_17 = V_2;
			V_2 = (int32_t)((int32_t)((int32_t)L_17+(int32_t)1));
		}

IL_006d:
		{
			int32_t L_18 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2733322780 * L_19 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2733322780 *)L_19);
			int32_t L_20 = ((  int32_t (*) (List_1_t2733322780 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2733322780 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_18) >= ((int32_t)L_20)))
			{
				goto IL_0084;
			}
		}

IL_007d:
		{
			int32_t L_21 = V_2;
			if ((((int32_t)L_21) < ((int32_t)8)))
			{
				goto IL_0025;
			}
		}

IL_0084:
		{
			List_1_t1365137228 * L_22 = V_1;
			if (L_22)
			{
				goto IL_0096;
			}
		}

IL_008a:
		{
			int32_t L_23 = ___capacity0;
			List_1_t1365137228 * L_24 = (List_1_t1365137228 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t1365137228 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(L_24, (int32_t)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			V_1 = (List_1_t1365137228 *)L_24;
			goto IL_00df;
		}

IL_0096:
		{
			List_1_t1365137228 * L_25 = V_1;
			int32_t L_26 = ___capacity0;
			NullCheck((List_1_t1365137228 *)L_25);
			((  void (*) (List_1_t1365137228 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->methodPointer)((List_1_t1365137228 *)L_25, (int32_t)L_26, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2733322780 * L_27 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2733322780 * L_28 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2733322780 *)L_28);
			int32_t L_29 = ((  int32_t (*) (List_1_t2733322780 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2733322780 *)L_28, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_30 = V_2;
			List_1_t2733322780 * L_31 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2733322780 * L_32 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2733322780 *)L_32);
			int32_t L_33 = ((  int32_t (*) (List_1_t2733322780 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2733322780 *)L_32, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t2733322780 *)L_31);
			List_1_t1365137228 * L_34 = ((  List_1_t1365137228 * (*) (List_1_t2733322780 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t2733322780 *)L_31, (int32_t)((int32_t)((int32_t)L_33-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			NullCheck((List_1_t2733322780 *)L_27);
			((  void (*) (List_1_t2733322780 *, int32_t, List_1_t1365137228 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->methodPointer)((List_1_t2733322780 *)L_27, (int32_t)((int32_t)((int32_t)L_29-(int32_t)L_30)), (List_1_t1365137228 *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
			List_1_t2733322780 * L_35 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2733322780 * L_36 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2733322780 *)L_36);
			int32_t L_37 = ((  int32_t (*) (List_1_t2733322780 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2733322780 *)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t2733322780 *)L_35);
			((  void (*) (List_1_t2733322780 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t2733322780 *)L_35, (int32_t)((int32_t)((int32_t)L_37-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		}

IL_00df:
		{
			List_1_t1365137228 * L_38 = V_1;
			V_3 = (List_1_t1365137228 *)L_38;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_00e6:
		{
			int32_t L_39 = ___capacity0;
			List_1_t1365137228 * L_40 = (List_1_t1365137228 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t1365137228 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(L_40, (int32_t)L_39, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			V_3 = (List_1_t1365137228 *)L_40;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_00f2:
		{
			; // IL_00f2: leave IL_00fe
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00f7;
	}

FINALLY_00f7:
	{ // begin finally (depth: 1)
		List_1_t2733322780 * L_41 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_41, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(247)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(247)
	{
		IL2CPP_JUMP_TBL(0xFE, IL_00fe)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00fe:
	{
		List_1_t1365137228 * L_42 = V_3;
		return L_42;
	}
}
// System.Void Pathfinding.Util.ListPool`1<System.Single>::Warmup(System.Int32,System.Int32)
extern "C"  void ListPool_1_Warmup_m140333271_gshared (Il2CppObject * __this /* static, unused */, int32_t ___count0, int32_t ___size1, const MethodInfo* method)
{
	List_1_t2733322780 * V_0 = NULL;
	List_1U5BU5D_t3744122373* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2733322780 * L_0 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t2733322780 *)L_0;
		List_1_t2733322780 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_2 = ___count0;
			V_1 = (List_1U5BU5D_t3744122373*)((List_1U5BU5D_t3744122373*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (uint32_t)L_2));
			V_2 = (int32_t)0;
			goto IL_0027;
		}

IL_001a:
		{
			List_1U5BU5D_t3744122373* L_3 = V_1;
			int32_t L_4 = V_2;
			int32_t L_5 = ___size1;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t1365137228 * L_6 = ((  List_1_t1365137228 * (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->methodPointer)(NULL /*static, unused*/, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
			NullCheck(L_3);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
			ArrayElementTypeCheck (L_3, L_6);
			(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (List_1_t1365137228 *)L_6);
			int32_t L_7 = V_2;
			V_2 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
		}

IL_0027:
		{
			int32_t L_8 = V_2;
			int32_t L_9 = ___count0;
			if ((((int32_t)L_8) < ((int32_t)L_9)))
			{
				goto IL_001a;
			}
		}

IL_002e:
		{
			V_3 = (int32_t)0;
			goto IL_0041;
		}

IL_0035:
		{
			List_1U5BU5D_t3744122373* L_10 = V_1;
			int32_t L_11 = V_3;
			NullCheck(L_10);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
			int32_t L_12 = L_11;
			List_1_t1365137228 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			((  void (*) (Il2CppObject * /* static, unused */, List_1_t1365137228 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->methodPointer)(NULL /*static, unused*/, (List_1_t1365137228 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
			int32_t L_14 = V_3;
			V_3 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
		}

IL_0041:
		{
			int32_t L_15 = V_3;
			int32_t L_16 = ___count0;
			if ((((int32_t)L_15) < ((int32_t)L_16)))
			{
				goto IL_0035;
			}
		}

IL_0048:
		{
			IL2CPP_LEAVE(0x54, FINALLY_004d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_004d;
	}

FINALLY_004d:
	{ // begin finally (depth: 1)
		List_1_t2733322780 * L_17 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_17, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(77)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(77)
	{
		IL2CPP_JUMP_TBL(0x54, IL_0054)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0054:
	{
		return;
	}
}
// System.Void Pathfinding.Util.ListPool`1<System.Single>::Release(System.Collections.Generic.List`1<T>)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3527666854;
extern const uint32_t ListPool_1_Release_m600440850_MetadataUsageId;
extern "C"  void ListPool_1_Release_m600440850_gshared (Il2CppObject * __this /* static, unused */, List_1_t1365137228 * ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListPool_1_Release_m600440850_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t2733322780 * V_0 = NULL;
	int32_t V_1 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1365137228 * L_0 = ___list0;
		NullCheck((List_1_t1365137228 *)L_0);
		((  void (*) (List_1_t1365137228 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->methodPointer)((List_1_t1365137228 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2733322780 * L_1 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t2733322780 *)L_1;
		List_1_t2733322780 * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			V_1 = (int32_t)0;
			goto IL_0039;
		}

IL_0019:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2733322780 * L_3 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			int32_t L_4 = V_1;
			NullCheck((List_1_t2733322780 *)L_3);
			List_1_t1365137228 * L_5 = ((  List_1_t1365137228 * (*) (List_1_t2733322780 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t2733322780 *)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			List_1_t1365137228 * L_6 = ___list0;
			if ((!(((Il2CppObject*)(List_1_t1365137228 *)L_5) == ((Il2CppObject*)(List_1_t1365137228 *)L_6))))
			{
				goto IL_0035;
			}
		}

IL_002a:
		{
			InvalidOperationException_t1589641621 * L_7 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_m1485483280(L_7, (String_t*)_stringLiteral3527666854, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
		}

IL_0035:
		{
			int32_t L_8 = V_1;
			V_1 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
		}

IL_0039:
		{
			int32_t L_9 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2733322780 * L_10 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2733322780 *)L_10);
			int32_t L_11 = ((  int32_t (*) (List_1_t2733322780 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2733322780 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_9) < ((int32_t)L_11)))
			{
				goto IL_0019;
			}
		}

IL_0049:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2733322780 * L_12 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t1365137228 * L_13 = ___list0;
			NullCheck((List_1_t2733322780 *)L_12);
			((  void (*) (List_1_t2733322780 *, List_1_t1365137228 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->methodPointer)((List_1_t2733322780 *)L_12, (List_1_t1365137228 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
			IL2CPP_LEAVE(0x60, FINALLY_0059);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0059;
	}

FINALLY_0059:
	{ // begin finally (depth: 1)
		List_1_t2733322780 * L_14 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(89)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(89)
	{
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0060:
	{
		return;
	}
}
// System.Void Pathfinding.Util.ListPool`1<System.Single>::Clear()
extern "C"  void ListPool_1_Clear_m3122877952_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t2733322780 * V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2733322780 * L_0 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t2733322780 *)L_0;
		List_1_t2733322780 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2733322780 * L_2 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		NullCheck((List_1_t2733322780 *)L_2);
		((  void (*) (List_1_t2733322780 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->methodPointer)((List_1_t2733322780 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		IL2CPP_LEAVE(0x22, FINALLY_001b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_001b;
	}

FINALLY_001b:
	{ // begin finally (depth: 1)
		List_1_t2733322780 * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(27)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(27)
	{
		IL2CPP_JUMP_TBL(0x22, IL_0022)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0022:
	{
		return;
	}
}
// System.Int32 Pathfinding.Util.ListPool`1<System.Single>::GetSize()
extern "C"  int32_t ListPool_1_GetSize_m1778342876_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2733322780 * L_0 = ((ListPool_1_t2577422026_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		NullCheck((List_1_t2733322780 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t2733322780 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2733322780 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void Pathfinding.Util.ListPool`1<System.UInt32>::.cctor()
extern "C"  void ListPool_1__cctor_m1780696327_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		List_1_t2761039085 * L_0 = (List_1_t2761039085 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (List_1_t2761039085 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_pool_1(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<System.UInt32>::Claim()
extern "C"  List_1_t1392853533 * ListPool_1_Claim_m2954305675_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t2761039085 * V_0 = NULL;
	List_1_t1392853533 * V_1 = NULL;
	List_1_t1392853533 * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2761039085 * L_0 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t2761039085 *)L_0;
		List_1_t2761039085 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2761039085 * L_2 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2761039085 *)L_2);
			int32_t L_3 = ((  int32_t (*) (List_1_t2761039085 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2761039085 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_3) <= ((int32_t)0)))
			{
				goto IL_0050;
			}
		}

IL_001c:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2761039085 * L_4 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2761039085 * L_5 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2761039085 *)L_5);
			int32_t L_6 = ((  int32_t (*) (List_1_t2761039085 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2761039085 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t2761039085 *)L_4);
			List_1_t1392853533 * L_7 = ((  List_1_t1392853533 * (*) (List_1_t2761039085 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t2761039085 *)L_4, (int32_t)((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			V_1 = (List_1_t1392853533 *)L_7;
			List_1_t2761039085 * L_8 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2761039085 * L_9 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2761039085 *)L_9);
			int32_t L_10 = ((  int32_t (*) (List_1_t2761039085 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2761039085 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t2761039085 *)L_8);
			((  void (*) (List_1_t2761039085 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t2761039085 *)L_8, (int32_t)((int32_t)((int32_t)L_10-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			List_1_t1392853533 * L_11 = V_1;
			V_2 = (List_1_t1392853533 *)L_11;
			IL2CPP_LEAVE(0x67, FINALLY_0060);
		}

IL_0050:
		{
			List_1_t1392853533 * L_12 = (List_1_t1392853533 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t1392853533 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			V_2 = (List_1_t1392853533 *)L_12;
			IL2CPP_LEAVE(0x67, FINALLY_0060);
		}

IL_005b:
		{
			; // IL_005b: leave IL_0067
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0060;
	}

FINALLY_0060:
	{ // begin finally (depth: 1)
		List_1_t2761039085 * L_13 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_13, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(96)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(96)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0067:
	{
		List_1_t1392853533 * L_14 = V_2;
		return L_14;
	}
}
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<System.UInt32>::Claim(System.Int32)
extern "C"  List_1_t1392853533 * ListPool_1_Claim_m1041849692_gshared (Il2CppObject * __this /* static, unused */, int32_t ___capacity0, const MethodInfo* method)
{
	List_1_t2761039085 * V_0 = NULL;
	List_1_t1392853533 * V_1 = NULL;
	int32_t V_2 = 0;
	List_1_t1392853533 * V_3 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2761039085 * L_0 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t2761039085 *)L_0;
		List_1_t2761039085 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2761039085 * L_2 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2761039085 *)L_2);
			int32_t L_3 = ((  int32_t (*) (List_1_t2761039085 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2761039085 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_3) <= ((int32_t)0)))
			{
				goto IL_00e6;
			}
		}

IL_001c:
		{
			V_1 = (List_1_t1392853533 *)NULL;
			V_2 = (int32_t)0;
			goto IL_006d;
		}

IL_0025:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2761039085 * L_4 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2761039085 * L_5 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2761039085 *)L_5);
			int32_t L_6 = ((  int32_t (*) (List_1_t2761039085 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2761039085 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_7 = V_2;
			NullCheck((List_1_t2761039085 *)L_4);
			List_1_t1392853533 * L_8 = ((  List_1_t1392853533 * (*) (List_1_t2761039085 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t2761039085 *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			V_1 = (List_1_t1392853533 *)L_8;
			List_1_t1392853533 * L_9 = V_1;
			NullCheck((List_1_t1392853533 *)L_9);
			int32_t L_10 = ((  int32_t (*) (List_1_t1392853533 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t1392853533 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			int32_t L_11 = ___capacity0;
			if ((((int32_t)L_10) < ((int32_t)L_11)))
			{
				goto IL_0069;
			}
		}

IL_004a:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2761039085 * L_12 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2761039085 * L_13 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2761039085 *)L_13);
			int32_t L_14 = ((  int32_t (*) (List_1_t2761039085 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2761039085 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_15 = V_2;
			NullCheck((List_1_t2761039085 *)L_12);
			((  void (*) (List_1_t2761039085 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t2761039085 *)L_12, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_14-(int32_t)1))-(int32_t)L_15)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			List_1_t1392853533 * L_16 = V_1;
			V_3 = (List_1_t1392853533 *)L_16;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_0069:
		{
			int32_t L_17 = V_2;
			V_2 = (int32_t)((int32_t)((int32_t)L_17+(int32_t)1));
		}

IL_006d:
		{
			int32_t L_18 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2761039085 * L_19 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2761039085 *)L_19);
			int32_t L_20 = ((  int32_t (*) (List_1_t2761039085 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2761039085 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_18) >= ((int32_t)L_20)))
			{
				goto IL_0084;
			}
		}

IL_007d:
		{
			int32_t L_21 = V_2;
			if ((((int32_t)L_21) < ((int32_t)8)))
			{
				goto IL_0025;
			}
		}

IL_0084:
		{
			List_1_t1392853533 * L_22 = V_1;
			if (L_22)
			{
				goto IL_0096;
			}
		}

IL_008a:
		{
			int32_t L_23 = ___capacity0;
			List_1_t1392853533 * L_24 = (List_1_t1392853533 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t1392853533 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(L_24, (int32_t)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			V_1 = (List_1_t1392853533 *)L_24;
			goto IL_00df;
		}

IL_0096:
		{
			List_1_t1392853533 * L_25 = V_1;
			int32_t L_26 = ___capacity0;
			NullCheck((List_1_t1392853533 *)L_25);
			((  void (*) (List_1_t1392853533 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->methodPointer)((List_1_t1392853533 *)L_25, (int32_t)L_26, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2761039085 * L_27 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2761039085 * L_28 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2761039085 *)L_28);
			int32_t L_29 = ((  int32_t (*) (List_1_t2761039085 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2761039085 *)L_28, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_30 = V_2;
			List_1_t2761039085 * L_31 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2761039085 * L_32 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2761039085 *)L_32);
			int32_t L_33 = ((  int32_t (*) (List_1_t2761039085 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2761039085 *)L_32, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t2761039085 *)L_31);
			List_1_t1392853533 * L_34 = ((  List_1_t1392853533 * (*) (List_1_t2761039085 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t2761039085 *)L_31, (int32_t)((int32_t)((int32_t)L_33-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			NullCheck((List_1_t2761039085 *)L_27);
			((  void (*) (List_1_t2761039085 *, int32_t, List_1_t1392853533 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->methodPointer)((List_1_t2761039085 *)L_27, (int32_t)((int32_t)((int32_t)L_29-(int32_t)L_30)), (List_1_t1392853533 *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
			List_1_t2761039085 * L_35 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2761039085 * L_36 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2761039085 *)L_36);
			int32_t L_37 = ((  int32_t (*) (List_1_t2761039085 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2761039085 *)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t2761039085 *)L_35);
			((  void (*) (List_1_t2761039085 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t2761039085 *)L_35, (int32_t)((int32_t)((int32_t)L_37-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		}

IL_00df:
		{
			List_1_t1392853533 * L_38 = V_1;
			V_3 = (List_1_t1392853533 *)L_38;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_00e6:
		{
			int32_t L_39 = ___capacity0;
			List_1_t1392853533 * L_40 = (List_1_t1392853533 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t1392853533 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(L_40, (int32_t)L_39, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			V_3 = (List_1_t1392853533 *)L_40;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_00f2:
		{
			; // IL_00f2: leave IL_00fe
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00f7;
	}

FINALLY_00f7:
	{ // begin finally (depth: 1)
		List_1_t2761039085 * L_41 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_41, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(247)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(247)
	{
		IL2CPP_JUMP_TBL(0xFE, IL_00fe)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00fe:
	{
		List_1_t1392853533 * L_42 = V_3;
		return L_42;
	}
}
// System.Void Pathfinding.Util.ListPool`1<System.UInt32>::Warmup(System.Int32,System.Int32)
extern "C"  void ListPool_1_Warmup_m576382760_gshared (Il2CppObject * __this /* static, unused */, int32_t ___count0, int32_t ___size1, const MethodInfo* method)
{
	List_1_t2761039085 * V_0 = NULL;
	List_1U5BU5D_t363325648* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2761039085 * L_0 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t2761039085 *)L_0;
		List_1_t2761039085 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_2 = ___count0;
			V_1 = (List_1U5BU5D_t363325648*)((List_1U5BU5D_t363325648*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (uint32_t)L_2));
			V_2 = (int32_t)0;
			goto IL_0027;
		}

IL_001a:
		{
			List_1U5BU5D_t363325648* L_3 = V_1;
			int32_t L_4 = V_2;
			int32_t L_5 = ___size1;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t1392853533 * L_6 = ((  List_1_t1392853533 * (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->methodPointer)(NULL /*static, unused*/, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
			NullCheck(L_3);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
			ArrayElementTypeCheck (L_3, L_6);
			(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (List_1_t1392853533 *)L_6);
			int32_t L_7 = V_2;
			V_2 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
		}

IL_0027:
		{
			int32_t L_8 = V_2;
			int32_t L_9 = ___count0;
			if ((((int32_t)L_8) < ((int32_t)L_9)))
			{
				goto IL_001a;
			}
		}

IL_002e:
		{
			V_3 = (int32_t)0;
			goto IL_0041;
		}

IL_0035:
		{
			List_1U5BU5D_t363325648* L_10 = V_1;
			int32_t L_11 = V_3;
			NullCheck(L_10);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
			int32_t L_12 = L_11;
			List_1_t1392853533 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			((  void (*) (Il2CppObject * /* static, unused */, List_1_t1392853533 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->methodPointer)(NULL /*static, unused*/, (List_1_t1392853533 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
			int32_t L_14 = V_3;
			V_3 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
		}

IL_0041:
		{
			int32_t L_15 = V_3;
			int32_t L_16 = ___count0;
			if ((((int32_t)L_15) < ((int32_t)L_16)))
			{
				goto IL_0035;
			}
		}

IL_0048:
		{
			IL2CPP_LEAVE(0x54, FINALLY_004d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_004d;
	}

FINALLY_004d:
	{ // begin finally (depth: 1)
		List_1_t2761039085 * L_17 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_17, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(77)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(77)
	{
		IL2CPP_JUMP_TBL(0x54, IL_0054)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0054:
	{
		return;
	}
}
// System.Void Pathfinding.Util.ListPool`1<System.UInt32>::Release(System.Collections.Generic.List`1<T>)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3527666854;
extern const uint32_t ListPool_1_Release_m1737204963_MetadataUsageId;
extern "C"  void ListPool_1_Release_m1737204963_gshared (Il2CppObject * __this /* static, unused */, List_1_t1392853533 * ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListPool_1_Release_m1737204963_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t2761039085 * V_0 = NULL;
	int32_t V_1 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1392853533 * L_0 = ___list0;
		NullCheck((List_1_t1392853533 *)L_0);
		((  void (*) (List_1_t1392853533 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->methodPointer)((List_1_t1392853533 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2761039085 * L_1 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t2761039085 *)L_1;
		List_1_t2761039085 * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			V_1 = (int32_t)0;
			goto IL_0039;
		}

IL_0019:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2761039085 * L_3 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			int32_t L_4 = V_1;
			NullCheck((List_1_t2761039085 *)L_3);
			List_1_t1392853533 * L_5 = ((  List_1_t1392853533 * (*) (List_1_t2761039085 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t2761039085 *)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			List_1_t1392853533 * L_6 = ___list0;
			if ((!(((Il2CppObject*)(List_1_t1392853533 *)L_5) == ((Il2CppObject*)(List_1_t1392853533 *)L_6))))
			{
				goto IL_0035;
			}
		}

IL_002a:
		{
			InvalidOperationException_t1589641621 * L_7 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_m1485483280(L_7, (String_t*)_stringLiteral3527666854, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
		}

IL_0035:
		{
			int32_t L_8 = V_1;
			V_1 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
		}

IL_0039:
		{
			int32_t L_9 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2761039085 * L_10 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2761039085 *)L_10);
			int32_t L_11 = ((  int32_t (*) (List_1_t2761039085 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2761039085 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_9) < ((int32_t)L_11)))
			{
				goto IL_0019;
			}
		}

IL_0049:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2761039085 * L_12 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t1392853533 * L_13 = ___list0;
			NullCheck((List_1_t2761039085 *)L_12);
			((  void (*) (List_1_t2761039085 *, List_1_t1392853533 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->methodPointer)((List_1_t2761039085 *)L_12, (List_1_t1392853533 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
			IL2CPP_LEAVE(0x60, FINALLY_0059);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0059;
	}

FINALLY_0059:
	{ // begin finally (depth: 1)
		List_1_t2761039085 * L_14 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(89)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(89)
	{
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0060:
	{
		return;
	}
}
// System.Void Pathfinding.Util.ListPool`1<System.UInt32>::Clear()
extern "C"  void ListPool_1_Clear_m4267945873_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t2761039085 * V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2761039085 * L_0 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t2761039085 *)L_0;
		List_1_t2761039085 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2761039085 * L_2 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		NullCheck((List_1_t2761039085 *)L_2);
		((  void (*) (List_1_t2761039085 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->methodPointer)((List_1_t2761039085 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		IL2CPP_LEAVE(0x22, FINALLY_001b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_001b;
	}

FINALLY_001b:
	{ // begin finally (depth: 1)
		List_1_t2761039085 * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(27)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(27)
	{
		IL2CPP_JUMP_TBL(0x22, IL_0022)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0022:
	{
		return;
	}
}
// System.Int32 Pathfinding.Util.ListPool`1<System.UInt32>::GetSize()
extern "C"  int32_t ListPool_1_GetSize_m2676987181_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2761039085 * L_0 = ((ListPool_1_t2605138331_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		NullCheck((List_1_t2761039085 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t2761039085 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2761039085 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void Pathfinding.Util.ListPool`1<UnityEngine.Vector3>::.cctor()
extern "C"  void ListPool_1__cctor_m1120968358_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		List_1_t2723470374 * L_0 = (List_1_t2723470374 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (List_1_t2723470374 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_pool_1(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<UnityEngine.Vector3>::Claim()
extern "C"  List_1_t1355284822 * ListPool_1_Claim_m1854083788_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t2723470374 * V_0 = NULL;
	List_1_t1355284822 * V_1 = NULL;
	List_1_t1355284822 * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2723470374 * L_0 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t2723470374 *)L_0;
		List_1_t2723470374 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2723470374 * L_2 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2723470374 *)L_2);
			int32_t L_3 = ((  int32_t (*) (List_1_t2723470374 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2723470374 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_3) <= ((int32_t)0)))
			{
				goto IL_0050;
			}
		}

IL_001c:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2723470374 * L_4 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2723470374 * L_5 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2723470374 *)L_5);
			int32_t L_6 = ((  int32_t (*) (List_1_t2723470374 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2723470374 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t2723470374 *)L_4);
			List_1_t1355284822 * L_7 = ((  List_1_t1355284822 * (*) (List_1_t2723470374 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t2723470374 *)L_4, (int32_t)((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			V_1 = (List_1_t1355284822 *)L_7;
			List_1_t2723470374 * L_8 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2723470374 * L_9 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2723470374 *)L_9);
			int32_t L_10 = ((  int32_t (*) (List_1_t2723470374 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2723470374 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t2723470374 *)L_8);
			((  void (*) (List_1_t2723470374 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t2723470374 *)L_8, (int32_t)((int32_t)((int32_t)L_10-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			List_1_t1355284822 * L_11 = V_1;
			V_2 = (List_1_t1355284822 *)L_11;
			IL2CPP_LEAVE(0x67, FINALLY_0060);
		}

IL_0050:
		{
			List_1_t1355284822 * L_12 = (List_1_t1355284822 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t1355284822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			V_2 = (List_1_t1355284822 *)L_12;
			IL2CPP_LEAVE(0x67, FINALLY_0060);
		}

IL_005b:
		{
			; // IL_005b: leave IL_0067
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0060;
	}

FINALLY_0060:
	{ // begin finally (depth: 1)
		List_1_t2723470374 * L_13 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_13, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(96)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(96)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0067:
	{
		List_1_t1355284822 * L_14 = V_2;
		return L_14;
	}
}
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<UnityEngine.Vector3>::Claim(System.Int32)
extern "C"  List_1_t1355284822 * ListPool_1_Claim_m527365661_gshared (Il2CppObject * __this /* static, unused */, int32_t ___capacity0, const MethodInfo* method)
{
	List_1_t2723470374 * V_0 = NULL;
	List_1_t1355284822 * V_1 = NULL;
	int32_t V_2 = 0;
	List_1_t1355284822 * V_3 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2723470374 * L_0 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t2723470374 *)L_0;
		List_1_t2723470374 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2723470374 * L_2 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2723470374 *)L_2);
			int32_t L_3 = ((  int32_t (*) (List_1_t2723470374 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2723470374 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_3) <= ((int32_t)0)))
			{
				goto IL_00e6;
			}
		}

IL_001c:
		{
			V_1 = (List_1_t1355284822 *)NULL;
			V_2 = (int32_t)0;
			goto IL_006d;
		}

IL_0025:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2723470374 * L_4 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2723470374 * L_5 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2723470374 *)L_5);
			int32_t L_6 = ((  int32_t (*) (List_1_t2723470374 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2723470374 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_7 = V_2;
			NullCheck((List_1_t2723470374 *)L_4);
			List_1_t1355284822 * L_8 = ((  List_1_t1355284822 * (*) (List_1_t2723470374 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t2723470374 *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			V_1 = (List_1_t1355284822 *)L_8;
			List_1_t1355284822 * L_9 = V_1;
			NullCheck((List_1_t1355284822 *)L_9);
			int32_t L_10 = ((  int32_t (*) (List_1_t1355284822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t1355284822 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			int32_t L_11 = ___capacity0;
			if ((((int32_t)L_10) < ((int32_t)L_11)))
			{
				goto IL_0069;
			}
		}

IL_004a:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2723470374 * L_12 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2723470374 * L_13 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2723470374 *)L_13);
			int32_t L_14 = ((  int32_t (*) (List_1_t2723470374 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2723470374 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_15 = V_2;
			NullCheck((List_1_t2723470374 *)L_12);
			((  void (*) (List_1_t2723470374 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t2723470374 *)L_12, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_14-(int32_t)1))-(int32_t)L_15)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			List_1_t1355284822 * L_16 = V_1;
			V_3 = (List_1_t1355284822 *)L_16;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_0069:
		{
			int32_t L_17 = V_2;
			V_2 = (int32_t)((int32_t)((int32_t)L_17+(int32_t)1));
		}

IL_006d:
		{
			int32_t L_18 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2723470374 * L_19 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2723470374 *)L_19);
			int32_t L_20 = ((  int32_t (*) (List_1_t2723470374 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2723470374 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_18) >= ((int32_t)L_20)))
			{
				goto IL_0084;
			}
		}

IL_007d:
		{
			int32_t L_21 = V_2;
			if ((((int32_t)L_21) < ((int32_t)8)))
			{
				goto IL_0025;
			}
		}

IL_0084:
		{
			List_1_t1355284822 * L_22 = V_1;
			if (L_22)
			{
				goto IL_0096;
			}
		}

IL_008a:
		{
			int32_t L_23 = ___capacity0;
			List_1_t1355284822 * L_24 = (List_1_t1355284822 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t1355284822 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(L_24, (int32_t)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			V_1 = (List_1_t1355284822 *)L_24;
			goto IL_00df;
		}

IL_0096:
		{
			List_1_t1355284822 * L_25 = V_1;
			int32_t L_26 = ___capacity0;
			NullCheck((List_1_t1355284822 *)L_25);
			((  void (*) (List_1_t1355284822 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->methodPointer)((List_1_t1355284822 *)L_25, (int32_t)L_26, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2723470374 * L_27 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2723470374 * L_28 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2723470374 *)L_28);
			int32_t L_29 = ((  int32_t (*) (List_1_t2723470374 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2723470374 *)L_28, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			int32_t L_30 = V_2;
			List_1_t2723470374 * L_31 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2723470374 * L_32 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2723470374 *)L_32);
			int32_t L_33 = ((  int32_t (*) (List_1_t2723470374 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2723470374 *)L_32, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t2723470374 *)L_31);
			List_1_t1355284822 * L_34 = ((  List_1_t1355284822 * (*) (List_1_t2723470374 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t2723470374 *)L_31, (int32_t)((int32_t)((int32_t)L_33-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			NullCheck((List_1_t2723470374 *)L_27);
			((  void (*) (List_1_t2723470374 *, int32_t, List_1_t1355284822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->methodPointer)((List_1_t2723470374 *)L_27, (int32_t)((int32_t)((int32_t)L_29-(int32_t)L_30)), (List_1_t1355284822 *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
			List_1_t2723470374 * L_35 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t2723470374 * L_36 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2723470374 *)L_36);
			int32_t L_37 = ((  int32_t (*) (List_1_t2723470374 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2723470374 *)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			NullCheck((List_1_t2723470374 *)L_35);
			((  void (*) (List_1_t2723470374 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t2723470374 *)L_35, (int32_t)((int32_t)((int32_t)L_37-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		}

IL_00df:
		{
			List_1_t1355284822 * L_38 = V_1;
			V_3 = (List_1_t1355284822 *)L_38;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_00e6:
		{
			int32_t L_39 = ___capacity0;
			List_1_t1355284822 * L_40 = (List_1_t1355284822 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (List_1_t1355284822 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(L_40, (int32_t)L_39, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			V_3 = (List_1_t1355284822 *)L_40;
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_00f2:
		{
			; // IL_00f2: leave IL_00fe
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00f7;
	}

FINALLY_00f7:
	{ // begin finally (depth: 1)
		List_1_t2723470374 * L_41 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_41, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(247)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(247)
	{
		IL2CPP_JUMP_TBL(0xFE, IL_00fe)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00fe:
	{
		List_1_t1355284822 * L_42 = V_3;
		return L_42;
	}
}
// System.Void Pathfinding.Util.ListPool`1<UnityEngine.Vector3>::Warmup(System.Int32,System.Int32)
extern "C"  void ListPool_1_Warmup_m707677801_gshared (Il2CppObject * __this /* static, unused */, int32_t ___count0, int32_t ___size1, const MethodInfo* method)
{
	List_1_t2723470374 * V_0 = NULL;
	List_1U5BU5D_t1642958995* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2723470374 * L_0 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t2723470374 *)L_0;
		List_1_t2723470374 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_2 = ___count0;
			V_1 = (List_1U5BU5D_t1642958995*)((List_1U5BU5D_t1642958995*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (uint32_t)L_2));
			V_2 = (int32_t)0;
			goto IL_0027;
		}

IL_001a:
		{
			List_1U5BU5D_t1642958995* L_3 = V_1;
			int32_t L_4 = V_2;
			int32_t L_5 = ___size1;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t1355284822 * L_6 = ((  List_1_t1355284822 * (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->methodPointer)(NULL /*static, unused*/, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
			NullCheck(L_3);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
			ArrayElementTypeCheck (L_3, L_6);
			(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (List_1_t1355284822 *)L_6);
			int32_t L_7 = V_2;
			V_2 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
		}

IL_0027:
		{
			int32_t L_8 = V_2;
			int32_t L_9 = ___count0;
			if ((((int32_t)L_8) < ((int32_t)L_9)))
			{
				goto IL_001a;
			}
		}

IL_002e:
		{
			V_3 = (int32_t)0;
			goto IL_0041;
		}

IL_0035:
		{
			List_1U5BU5D_t1642958995* L_10 = V_1;
			int32_t L_11 = V_3;
			NullCheck(L_10);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
			int32_t L_12 = L_11;
			List_1_t1355284822 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			((  void (*) (Il2CppObject * /* static, unused */, List_1_t1355284822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->methodPointer)(NULL /*static, unused*/, (List_1_t1355284822 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
			int32_t L_14 = V_3;
			V_3 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
		}

IL_0041:
		{
			int32_t L_15 = V_3;
			int32_t L_16 = ___count0;
			if ((((int32_t)L_15) < ((int32_t)L_16)))
			{
				goto IL_0035;
			}
		}

IL_0048:
		{
			IL2CPP_LEAVE(0x54, FINALLY_004d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_004d;
	}

FINALLY_004d:
	{ // begin finally (depth: 1)
		List_1_t2723470374 * L_17 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_17, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(77)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(77)
	{
		IL2CPP_JUMP_TBL(0x54, IL_0054)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0054:
	{
		return;
	}
}
// System.Void Pathfinding.Util.ListPool`1<UnityEngine.Vector3>::Release(System.Collections.Generic.List`1<T>)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3527666854;
extern const uint32_t ListPool_1_Release_m2445357732_MetadataUsageId;
extern "C"  void ListPool_1_Release_m2445357732_gshared (Il2CppObject * __this /* static, unused */, List_1_t1355284822 * ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListPool_1_Release_m2445357732_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t2723470374 * V_0 = NULL;
	int32_t V_1 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1355284822 * L_0 = ___list0;
		NullCheck((List_1_t1355284822 *)L_0);
		((  void (*) (List_1_t1355284822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->methodPointer)((List_1_t1355284822 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2723470374 * L_1 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t2723470374 *)L_1;
		List_1_t2723470374 * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			V_1 = (int32_t)0;
			goto IL_0039;
		}

IL_0019:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2723470374 * L_3 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			int32_t L_4 = V_1;
			NullCheck((List_1_t2723470374 *)L_3);
			List_1_t1355284822 * L_5 = ((  List_1_t1355284822 * (*) (List_1_t2723470374 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t2723470374 *)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			List_1_t1355284822 * L_6 = ___list0;
			if ((!(((Il2CppObject*)(List_1_t1355284822 *)L_5) == ((Il2CppObject*)(List_1_t1355284822 *)L_6))))
			{
				goto IL_0035;
			}
		}

IL_002a:
		{
			InvalidOperationException_t1589641621 * L_7 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_m1485483280(L_7, (String_t*)_stringLiteral3527666854, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
		}

IL_0035:
		{
			int32_t L_8 = V_1;
			V_1 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
		}

IL_0039:
		{
			int32_t L_9 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2723470374 * L_10 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			NullCheck((List_1_t2723470374 *)L_10);
			int32_t L_11 = ((  int32_t (*) (List_1_t2723470374 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2723470374 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			if ((((int32_t)L_9) < ((int32_t)L_11)))
			{
				goto IL_0019;
			}
		}

IL_0049:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			List_1_t2723470374 * L_12 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
			List_1_t1355284822 * L_13 = ___list0;
			NullCheck((List_1_t2723470374 *)L_12);
			((  void (*) (List_1_t2723470374 *, List_1_t1355284822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->methodPointer)((List_1_t2723470374 *)L_12, (List_1_t1355284822 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
			IL2CPP_LEAVE(0x60, FINALLY_0059);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0059;
	}

FINALLY_0059:
	{ // begin finally (depth: 1)
		List_1_t2723470374 * L_14 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(89)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(89)
	{
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0060:
	{
		return;
	}
}
// System.Void Pathfinding.Util.ListPool`1<UnityEngine.Vector3>::Clear()
extern "C"  void ListPool_1_Clear_m367339026_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t2723470374 * V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2723470374 * L_0 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		V_0 = (List_1_t2723470374 *)L_0;
		List_1_t2723470374 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2723470374 * L_2 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		NullCheck((List_1_t2723470374 *)L_2);
		((  void (*) (List_1_t2723470374 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->methodPointer)((List_1_t2723470374 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		IL2CPP_LEAVE(0x22, FINALLY_001b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_001b;
	}

FINALLY_001b:
	{ // begin finally (depth: 1)
		List_1_t2723470374 * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(27)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(27)
	{
		IL2CPP_JUMP_TBL(0x22, IL_0022)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0022:
	{
		return;
	}
}
// System.Int32 Pathfinding.Util.ListPool`1<UnityEngine.Vector3>::GetSize()
extern "C"  int32_t ListPool_1_GetSize_m2209554926_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t2723470374 * L_0 = ((ListPool_1_t2567569620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_1();
		NullCheck((List_1_t2723470374 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t2723470374 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t2723470374 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void Pathfinding.Util.ObjectPool`1<System.Object>::.cctor()
extern "C"  void ObjectPool_1__cctor_m3373051104_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		List_1_t1244034627 * L_0 = (List_1_t1244034627 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((ObjectPool_1_t1443416954_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_pool_0(L_0);
		return;
	}
}
// T Pathfinding.Util.ObjectPool`1<System.Object>::Claim()
extern "C"  Il2CppObject * ObjectPool_1_Claim_m1866735784_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t1244034627 * L_0 = ((ObjectPool_1_t1443416954_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
		NullCheck((List_1_t1244034627 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1244034627 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_009d;
		}
	}
	{
		bool L_2 = BreakUtil_IsBreak17_m1274825255(NULL /*static, unused*/, (int32_t)((int32_t)-2147483648LL), /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_006e;
		}
	}
	{
		V_1 = (int32_t)((int32_t)15211572);
		V_2 = (int32_t)((int32_t)15211518);
		int32_t L_3 = V_1;
		int32_t L_4 = V_2;
		int32_t L_5 = V_2;
		int32_t L_6 = V_2;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_3/(int32_t)L_4))|(int32_t)L_5))|(int32_t)L_6));
		int32_t L_7 = V_1;
		int32_t L_8 = V_2;
		int32_t L_9 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_7/(int32_t)L_8))+(int32_t)L_9));
		int32_t L_10 = V_1;
		int32_t L_11 = V_2;
		int32_t L_12 = V_1;
		int32_t L_13 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_10^(int32_t)L_11))%(int32_t)L_12))^(int32_t)L_13));
	}

IL_006e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t1244034627 * L_14 = ((ObjectPool_1_t1443416954_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
		List_1_t1244034627 * L_15 = ((ObjectPool_1_t1443416954_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
		NullCheck((List_1_t1244034627 *)L_15);
		int32_t L_16 = ((  int32_t (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1244034627 *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((List_1_t1244034627 *)L_14);
		Il2CppObject * L_17 = ((  Il2CppObject * (*) (List_1_t1244034627 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t1244034627 *)L_14, (int32_t)((int32_t)((int32_t)L_16-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_0 = (Il2CppObject *)L_17;
		List_1_t1244034627 * L_18 = ((ObjectPool_1_t1443416954_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
		List_1_t1244034627 * L_19 = ((ObjectPool_1_t1443416954_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
		NullCheck((List_1_t1244034627 *)L_19);
		int32_t L_20 = ((  int32_t (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1244034627 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((List_1_t1244034627 *)L_18);
		((  void (*) (List_1_t1244034627 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t1244034627 *)L_18, (int32_t)((int32_t)((int32_t)L_20-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Il2CppObject * L_21 = V_0;
		return L_21;
	}

IL_009d:
	{
		Il2CppObject * L_22 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_22;
	}
}
// System.Void Pathfinding.Util.ObjectPool`1<System.Object>::Warmup(System.Int32)
extern "C"  void ObjectPool_1_Warmup_m3419418728_gshared (Il2CppObject * __this /* static, unused */, int32_t ___count0, const MethodInfo* method)
{
	ObjectU5BU5D_t1108656482* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	{
		int32_t L_0 = ___count0;
		V_0 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), (uint32_t)L_0));
		V_1 = (int32_t)0;
		goto IL_00a0;
	}
	// Dead block : IL_000e: ldc.r4 -2.149631E+09
	// Dead block : IL_001a: ldc.i4 15211534

IL_0090:
	{
		ObjectU5BU5D_t1108656482* L_1 = V_0;
		int32_t L_2 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (Il2CppObject *)L_3);
		int32_t L_4 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_00a0:
	{
		int32_t L_5 = V_1;
		int32_t L_6 = ___count0;
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_0090;
		}
	}
	{
		bool L_7 = BreakUtil_IsBreak25_m2044154756(NULL /*static, unused*/, (int32_t)((int32_t)-2147483648LL), /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0121;
		}
	}
	{
		V_7 = (int32_t)((int32_t)15211572);
		V_8 = (int32_t)((int32_t)15211561);
		int32_t L_8 = V_7;
		int32_t L_9 = V_8;
		int32_t L_10 = V_7;
		V_7 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_8%(int32_t)L_9))/(int32_t)L_10));
		int32_t L_11 = V_7;
		int32_t L_12 = V_8;
		int32_t L_13 = V_8;
		int32_t L_14 = V_7;
		int32_t L_15 = V_8;
		V_7 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_11|(int32_t)L_12))|(int32_t)L_13))*(int32_t)L_14))/(int32_t)L_15));
		int32_t L_16 = V_7;
		int32_t L_17 = V_8;
		int32_t L_18 = V_7;
		int32_t L_19 = V_8;
		V_8 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_16^(int32_t)L_17))-(int32_t)L_18))^(int32_t)L_19));
		int32_t L_20 = V_7;
		int32_t L_21 = V_8;
		int32_t L_22 = V_7;
		int32_t L_23 = V_7;
		V_8 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_20^(int32_t)L_21))/(int32_t)L_22))-(int32_t)L_23));
	}

IL_0121:
	{
		V_2 = (int32_t)0;
		goto IL_01e6;
	}
	// Dead block : IL_0128: ldc.i4 -2147483648
	// Dead block : IL_0137: ldc.i4 15211482

IL_01d6:
	{
		ObjectU5BU5D_t1108656482* L_24 = V_0;
		int32_t L_25 = V_2;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		int32_t L_26 = L_25;
		Il2CppObject * L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_28 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_01e6:
	{
		int32_t L_29 = V_2;
		int32_t L_30 = ___count0;
		if ((((int32_t)L_29) < ((int32_t)L_30)))
		{
			goto IL_01d6;
		}
	}
	{
		bool L_31 = BreakUtil_IsBreak24_m536602947(NULL /*static, unused*/, (int32_t)((int32_t)-2147483648LL), /*hidden argument*/NULL);
		if (L_31)
		{
			goto IL_02a0;
		}
	}
	{
		V_3 = (int32_t)((int32_t)15211522);
		V_4 = (int32_t)((int32_t)15211518);
		int32_t L_32 = V_3;
		int32_t L_33 = V_4;
		int32_t L_34 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_32|(int32_t)L_33))&(int32_t)L_34));
		int32_t L_35 = V_3;
		int32_t L_36 = V_4;
		int32_t L_37 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_35&(int32_t)L_36))^(int32_t)L_37));
		int32_t L_38 = V_3;
		int32_t L_39 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)L_38%(int32_t)L_39));
		int32_t L_40 = V_3;
		int32_t L_41 = V_4;
		int32_t L_42 = V_3;
		int32_t L_43 = V_3;
		int32_t L_44 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_40-(int32_t)L_41))^(int32_t)L_42))%(int32_t)L_43))-(int32_t)L_44));
		int32_t L_45 = V_3;
		int32_t L_46 = V_4;
		int32_t L_47 = V_3;
		int32_t L_48 = V_4;
		V_3 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_45|(int32_t)L_46))|(int32_t)L_47))|(int32_t)L_48));
		int32_t L_49 = V_3;
		int32_t L_50 = V_4;
		int32_t L_51 = V_3;
		V_4 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_49-(int32_t)L_50))*(int32_t)L_51));
		int32_t L_52 = V_3;
		int32_t L_53 = V_4;
		int32_t L_54 = V_3;
		int32_t L_55 = V_4;
		int32_t L_56 = V_4;
		V_3 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_52+(int32_t)L_53))+(int32_t)L_54))^(int32_t)L_55))&(int32_t)L_56));
	}

IL_02a0:
	{
		return;
	}
}
// System.Void Pathfinding.Util.ObjectPool`1<System.Object>::Release(T)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppClass* IAstarPooledObject_t898458634_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3048068458;
extern const uint32_t ObjectPool_1_Release_m2971981388_MetadataUsageId;
extern "C"  void ObjectPool_1_Release_m2971981388_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObjectPool_1_Release_m2971981388_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_0031;
	}

IL_0007:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t1244034627 * L_0 = ((ObjectPool_1_t1443416954_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
		int32_t L_1 = V_0;
		NullCheck((List_1_t1244034627 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (List_1_t1244034627 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t1244034627 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_3 = ___obj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_2) == ((Il2CppObject*)(Il2CppObject *)L_3))))
		{
			goto IL_002d;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_4 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_4, (String_t*)_stringLiteral3048068458, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_002d:
	{
		int32_t L_5 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t1244034627 * L_7 = ((ObjectPool_1_t1443416954_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
		NullCheck((List_1_t1244034627 *)L_7);
		int32_t L_8 = ((  int32_t (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1244034627 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		if ((((int32_t)L_6) < ((int32_t)L_8)))
		{
			goto IL_0007;
		}
	}
	{
		NullCheck((Il2CppObject *)(*(&___obj0)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void Pathfinding.Util.IAstarPooledObject::OnEnterPool() */, IAstarPooledObject_t898458634_il2cpp_TypeInfo_var, (Il2CppObject *)(*(&___obj0)));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t1244034627 * L_9 = ((ObjectPool_1_t1443416954_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
		Il2CppObject * L_10 = ___obj0;
		NullCheck((List_1_t1244034627 *)L_9);
		((  void (*) (List_1_t1244034627 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->methodPointer)((List_1_t1244034627 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return;
	}
}
// System.Void Pathfinding.Util.ObjectPool`1<System.Object>::Clear()
extern "C"  void ObjectPool_1_Clear_m4180764824_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t1244034627 * L_0 = ((ObjectPool_1_t1443416954_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
		NullCheck((List_1_t1244034627 *)L_0);
		((  void (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->methodPointer)((List_1_t1244034627 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return;
	}
}
// System.Int32 Pathfinding.Util.ObjectPool`1<System.Object>::GetSize()
extern "C"  int32_t ObjectPool_1_GetSize_m3459990516_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t1244034627 * L_0 = ((ObjectPool_1_t1443416954_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
		NullCheck((List_1_t1244034627 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t1244034627 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void Pathfinding.Util.StackPool`1<System.Object>::.cctor()
extern "C"  void StackPool_1__cctor_m3413873323_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		List_1_t47628256 * L_0 = (List_1_t47628256 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (List_1_t47628256 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((StackPool_1_t2103378743_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_pool_0(L_0);
		return;
	}
}
// System.Collections.Generic.Stack`1<T> Pathfinding.Util.StackPool`1<System.Object>::Claim()
extern "C"  Stack_1_t2974409999 * StackPool_1_Claim_m3952253811_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Stack_1_t2974409999 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t47628256 * L_0 = ((StackPool_1_t2103378743_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
		NullCheck((List_1_t47628256 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t47628256 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t47628256 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_003f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t47628256 * L_2 = ((StackPool_1_t2103378743_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
		List_1_t47628256 * L_3 = ((StackPool_1_t2103378743_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
		NullCheck((List_1_t47628256 *)L_3);
		int32_t L_4 = ((  int32_t (*) (List_1_t47628256 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t47628256 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((List_1_t47628256 *)L_2);
		Stack_1_t2974409999 * L_5 = ((  Stack_1_t2974409999 * (*) (List_1_t47628256 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t47628256 *)L_2, (int32_t)((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_0 = (Stack_1_t2974409999 *)L_5;
		List_1_t47628256 * L_6 = ((StackPool_1_t2103378743_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
		List_1_t47628256 * L_7 = ((StackPool_1_t2103378743_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
		NullCheck((List_1_t47628256 *)L_7);
		int32_t L_8 = ((  int32_t (*) (List_1_t47628256 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t47628256 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((List_1_t47628256 *)L_6);
		((  void (*) (List_1_t47628256 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((List_1_t47628256 *)L_6, (int32_t)((int32_t)((int32_t)L_8-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Stack_1_t2974409999 * L_9 = V_0;
		return L_9;
	}

IL_003f:
	{
		Stack_1_t2974409999 * L_10 = (Stack_1_t2974409999 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (Stack_1_t2974409999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_10;
	}
}
// System.Void Pathfinding.Util.StackPool`1<System.Object>::Warmup(System.Int32)
extern "C"  void StackPool_1_Warmup_m4025859507_gshared (Il2CppObject * __this /* static, unused */, int32_t ___count0, const MethodInfo* method)
{
	Stack_1U5BU5D_t3251663318* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___count0;
		V_0 = (Stack_1U5BU5D_t3251663318*)((Stack_1U5BU5D_t3251663318*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (uint32_t)L_0));
		V_1 = (int32_t)0;
		goto IL_001a;
	}

IL_000e:
	{
		Stack_1U5BU5D_t3251663318* L_1 = V_0;
		int32_t L_2 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Stack_1_t2974409999 * L_3 = ((  Stack_1_t2974409999 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (Stack_1_t2974409999 *)L_3);
		int32_t L_4 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_001a:
	{
		int32_t L_5 = V_1;
		int32_t L_6 = ___count0;
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_000e;
		}
	}
	{
		V_2 = (int32_t)0;
		goto IL_0034;
	}

IL_0028:
	{
		Stack_1U5BU5D_t3251663318* L_7 = V_0;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		Stack_1_t2974409999 * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Il2CppObject * /* static, unused */, Stack_1_t2974409999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->methodPointer)(NULL /*static, unused*/, (Stack_1_t2974409999 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		int32_t L_11 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_12 = V_2;
		int32_t L_13 = ___count0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0028;
		}
	}
	{
		return;
	}
}
// System.Void Pathfinding.Util.StackPool`1<System.Object>::Release(System.Collections.Generic.Stack`1<T>)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3619448155;
extern const uint32_t StackPool_1_Release_m206937007_MetadataUsageId;
extern "C"  void StackPool_1_Release_m206937007_gshared (Il2CppObject * __this /* static, unused */, Stack_1_t2974409999 * ___stack0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StackPool_1_Release_m206937007_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_00fe;
	}
	// Dead block : IL_0007: ldc.r4 -2.149631E+09
	// Dead block : IL_0013: ldc.i4 15211577

IL_007c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t47628256 * L_0 = ((StackPool_1_t2103378743_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
		int32_t L_1 = V_0;
		NullCheck((List_1_t47628256 *)L_0);
		Stack_1_t2974409999 * L_2 = ((  Stack_1_t2974409999 * (*) (List_1_t47628256 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((List_1_t47628256 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Stack_1_t2974409999 * L_3 = ___stack0;
		if ((!(((Il2CppObject*)(Stack_1_t2974409999 *)L_2) == ((Il2CppObject*)(Stack_1_t2974409999 *)L_3))))
		{
			goto IL_00fa;
		}
	}
	{
		bool L_4 = BreakUtil_IsBreak6_m3985111801(NULL /*static, unused*/, (int32_t)((int32_t)-2147483648LL), /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_00f0;
		}
	}
	{
		V_3 = (int32_t)((int32_t)15211575);
		V_4 = (int32_t)((int32_t)15211568);
		int32_t L_5 = V_3;
		int32_t L_6 = V_4;
		int32_t L_7 = V_4;
		int32_t L_8 = V_4;
		V_3 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)L_6))%(int32_t)L_7))&(int32_t)L_8));
		int32_t L_9 = V_3;
		int32_t L_10 = V_4;
		int32_t L_11 = V_3;
		int32_t L_12 = V_3;
		V_4 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_9/(int32_t)L_10))+(int32_t)L_11))^(int32_t)L_12));
		int32_t L_13 = V_3;
		int32_t L_14 = V_4;
		int32_t L_15 = V_3;
		int32_t L_16 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_13/(int32_t)L_14))/(int32_t)L_15))^(int32_t)L_16));
	}

IL_00f0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral3619448155, /*hidden argument*/NULL);
	}

IL_00fa:
	{
		int32_t L_17 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_00fe:
	{
		int32_t L_18 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t47628256 * L_19 = ((StackPool_1_t2103378743_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
		NullCheck((List_1_t47628256 *)L_19);
		int32_t L_20 = ((  int32_t (*) (List_1_t47628256 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t47628256 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		if ((((int32_t)L_18) < ((int32_t)L_20)))
		{
			goto IL_007c;
		}
	}
	{
		bool L_21 = BreakUtil_IsBreak33_m2813484257(NULL /*static, unused*/, (int32_t)((int32_t)-2147483648LL), /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_01a0;
		}
	}
	{
		V_1 = (int32_t)((int32_t)15211519);
		V_2 = (int32_t)((int32_t)15211540);
		int32_t L_22 = V_1;
		int32_t L_23 = V_2;
		V_1 = (int32_t)((int32_t)((int32_t)L_22&(int32_t)L_23));
		int32_t L_24 = V_1;
		int32_t L_25 = V_2;
		int32_t L_26 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_24%(int32_t)L_25))/(int32_t)L_26));
		int32_t L_27 = V_1;
		int32_t L_28 = V_2;
		int32_t L_29 = V_1;
		int32_t L_30 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_27*(int32_t)L_28))+(int32_t)L_29))&(int32_t)L_30));
		int32_t L_31 = V_1;
		int32_t L_32 = V_2;
		int32_t L_33 = V_1;
		int32_t L_34 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_31^(int32_t)L_32))-(int32_t)L_33))/(int32_t)L_34));
		int32_t L_35 = V_1;
		int32_t L_36 = V_2;
		int32_t L_37 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_35|(int32_t)L_36))*(int32_t)L_37));
		int32_t L_38 = V_1;
		int32_t L_39 = V_2;
		int32_t L_40 = V_2;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_38%(int32_t)L_39))^(int32_t)L_40));
	}

IL_01a0:
	{
		Stack_1_t2974409999 * L_41 = ___stack0;
		NullCheck((Stack_1_t2974409999 *)L_41);
		((  void (*) (Stack_1_t2974409999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->methodPointer)((Stack_1_t2974409999 *)L_41, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t47628256 * L_42 = ((StackPool_1_t2103378743_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
		Stack_1_t2974409999 * L_43 = ___stack0;
		NullCheck((List_1_t47628256 *)L_42);
		((  void (*) (List_1_t47628256 *, Stack_1_t2974409999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->methodPointer)((List_1_t47628256 *)L_42, (Stack_1_t2974409999 *)L_43, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return;
	}
}
// System.Void Pathfinding.Util.StackPool`1<System.Object>::Clear()
extern "C"  void StackPool_1_Clear_m3350797677_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t47628256 * L_0 = ((StackPool_1_t2103378743_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
		NullCheck((List_1_t47628256 *)L_0);
		((  void (*) (List_1_t47628256 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->methodPointer)((List_1_t47628256 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		return;
	}
}
// System.Int32 Pathfinding.Util.StackPool`1<System.Object>::GetSize()
extern "C"  int32_t StackPool_1_GetSize_m247741669_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		List_1_t47628256 * L_0 = ((StackPool_1_t2103378743_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_pool_0();
		NullCheck((List_1_t47628256 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t47628256 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((List_1_t47628256 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void Pomelo.DotNetClient.ObjPool`1<System.Object>::.ctor(System.Int32)
extern "C"  void ObjPool_1__ctor_m2650734553_gshared (ObjPool_1_t286528818 * __this, int32_t ___maxNum0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___maxNum0;
		__this->set_maxNum_1(L_0);
		Stack_1_t2974409999 * L_1 = (Stack_1_t2974409999 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Stack_1_t2974409999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_idleList_0(L_1);
		return;
	}
}
// T Pomelo.DotNetClient.ObjPool`1<System.Object>::PopObj()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ObjPool_1_PopObj_m3392359585_MetadataUsageId;
extern "C"  Il2CppObject * ObjPool_1_PopObj_m3392359585_gshared (ObjPool_1_t286528818 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObjPool_1_PopObj_m3392359585_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	Il2CppObject * G_B11_0 = NULL;
	{
		Stack_1_t2974409999 * L_0 = (Stack_1_t2974409999 *)__this->get_idleList_0();
		NullCheck((Stack_1_t2974409999 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t2974409999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Stack_1_t2974409999 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_00d0;
		}
	}
	{
		bool L_2 = BreakUtil_IsBreak29_m3779394696(NULL /*static, unused*/, (int32_t)((int32_t)-2147483648LL), /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_00c4;
		}
	}
	{
		V_5 = (int32_t)((int32_t)15211533);
		V_6 = (int32_t)((int32_t)15211491);
		int32_t L_3 = V_5;
		int32_t L_4 = V_6;
		int32_t L_5 = V_5;
		V_5 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_3&(int32_t)L_4))*(int32_t)L_5));
		int32_t L_6 = V_5;
		int32_t L_7 = V_6;
		int32_t L_8 = V_6;
		V_5 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6%(int32_t)L_7))&(int32_t)L_8));
		int32_t L_9 = V_5;
		int32_t L_10 = V_6;
		int32_t L_11 = V_5;
		int32_t L_12 = V_6;
		V_5 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_9*(int32_t)L_10))^(int32_t)L_11))*(int32_t)L_12));
		int32_t L_13 = V_5;
		int32_t L_14 = V_6;
		int32_t L_15 = V_5;
		V_5 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_13+(int32_t)L_14))&(int32_t)L_15));
		int32_t L_16 = V_5;
		int32_t L_17 = V_6;
		int32_t L_18 = V_5;
		int32_t L_19 = V_5;
		V_6 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_16&(int32_t)L_17))|(int32_t)L_18))-(int32_t)L_19));
		int32_t L_20 = V_5;
		int32_t L_21 = V_6;
		int32_t L_22 = V_6;
		int32_t L_23 = V_5;
		V_5 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_20-(int32_t)L_21))/(int32_t)L_22))+(int32_t)L_23));
		int32_t L_24 = V_5;
		int32_t L_25 = V_6;
		int32_t L_26 = V_5;
		int32_t L_27 = V_6;
		V_5 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_24/(int32_t)L_25))%(int32_t)L_26))&(int32_t)L_27));
	}

IL_00c4:
	{
		Stack_1_t2974409999 * L_28 = (Stack_1_t2974409999 *)__this->get_idleList_0();
		NullCheck((Stack_1_t2974409999 *)L_28);
		Il2CppObject * L_29 = ((  Il2CppObject * (*) (Stack_1_t2974409999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Stack_1_t2974409999 *)L_28, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_29;
	}

IL_00d0:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_30 = V_0;
		if (!L_30)
		{
			goto IL_0178;
		}
	}
	{
		bool L_31 = BreakUtil_IsBreak2_m2249871861(NULL /*static, unused*/, (int32_t)((int32_t)-2147483648LL), /*hidden argument*/NULL);
		if (L_31)
		{
			goto IL_016a;
		}
	}
	{
		V_3 = (int32_t)((int32_t)15211515);
		V_4 = (int32_t)((int32_t)15211500);
		int32_t L_32 = V_3;
		int32_t L_33 = V_4;
		int32_t L_34 = V_3;
		int32_t L_35 = V_3;
		V_4 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_32*(int32_t)L_33))%(int32_t)L_34))-(int32_t)L_35));
		int32_t L_36 = V_3;
		int32_t L_37 = V_4;
		int32_t L_38 = V_4;
		int32_t L_39 = V_3;
		V_4 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_36-(int32_t)L_37))*(int32_t)L_38))/(int32_t)L_39));
		int32_t L_40 = V_3;
		int32_t L_41 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)L_40*(int32_t)L_41));
		int32_t L_42 = V_3;
		int32_t L_43 = V_4;
		int32_t L_44 = V_3;
		int32_t L_45 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_42-(int32_t)L_43))-(int32_t)L_44))*(int32_t)L_45));
		int32_t L_46 = V_3;
		int32_t L_47 = V_4;
		int32_t L_48 = V_3;
		int32_t L_49 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_46&(int32_t)L_47))-(int32_t)L_48))|(int32_t)L_49));
	}

IL_016a:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_50 = V_0;
		G_B11_0 = L_50;
		goto IL_020c;
	}

IL_0178:
	{
		bool L_51 = BreakUtil_IsBreak6_m3985111801(NULL /*static, unused*/, (int32_t)((int32_t)-2147483648LL), /*hidden argument*/NULL);
		if (L_51)
		{
			goto IL_0207;
		}
	}
	{
		V_1 = (int32_t)((int32_t)15211548);
		V_2 = (int32_t)((int32_t)15211567);
		int32_t L_52 = V_1;
		int32_t L_53 = V_2;
		int32_t L_54 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_52|(int32_t)L_53))-(int32_t)L_54));
		int32_t L_55 = V_1;
		int32_t L_56 = V_2;
		int32_t L_57 = V_1;
		int32_t L_58 = V_2;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_55/(int32_t)L_56))%(int32_t)L_57))*(int32_t)L_58));
		int32_t L_59 = V_1;
		int32_t L_60 = V_2;
		int32_t L_61 = V_2;
		int32_t L_62 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_59%(int32_t)L_60))|(int32_t)L_61))^(int32_t)L_62));
		int32_t L_63 = V_1;
		int32_t L_64 = V_2;
		int32_t L_65 = V_1;
		int32_t L_66 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_63|(int32_t)L_64))+(int32_t)L_65))^(int32_t)L_66));
		int32_t L_67 = V_1;
		int32_t L_68 = V_2;
		int32_t L_69 = V_2;
		int32_t L_70 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_67%(int32_t)L_68))^(int32_t)L_69))&(int32_t)L_70));
	}

IL_0207:
	{
		Il2CppObject * L_71 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		G_B11_0 = L_71;
	}

IL_020c:
	{
		return G_B11_0;
	}
}
// System.Void Pomelo.DotNetClient.ObjPool`1<System.Object>::PushObj(T)
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t ObjPool_1_PushObj_m2019108635_MetadataUsageId;
extern "C"  void ObjPool_1_PushObj_m2019108635_gshared (ObjPool_1_t286528818 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObjPool_1_PushObj_m2019108635_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		NullCheck((Il2CppObject *)(*(&___obj0)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)(*(&___obj0)));
		Stack_1_t2974409999 * L_0 = (Stack_1_t2974409999 *)__this->get_idleList_0();
		NullCheck((Stack_1_t2974409999 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t2974409999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Stack_1_t2974409999 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		int32_t L_2 = (int32_t)__this->get_maxNum_1();
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_00c1;
		}
	}
	{
		bool L_3 = BreakUtil_IsBreak13_m3834552611(NULL /*static, unused*/, (int32_t)((int32_t)-2147483648LL), /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_00b5;
		}
	}
	{
		V_0 = (int32_t)((int32_t)15211520);
		V_1 = (int32_t)((int32_t)15211575);
		int32_t L_4 = V_0;
		int32_t L_5 = V_1;
		int32_t L_6 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_4^(int32_t)L_5))/(int32_t)L_6));
		int32_t L_7 = V_0;
		int32_t L_8 = V_1;
		int32_t L_9 = V_0;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_7/(int32_t)L_8))-(int32_t)L_9));
		int32_t L_10 = V_0;
		int32_t L_11 = V_1;
		int32_t L_12 = V_1;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_10&(int32_t)L_11))&(int32_t)L_12));
		int32_t L_13 = V_0;
		int32_t L_14 = V_1;
		int32_t L_15 = V_1;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_13|(int32_t)L_14))+(int32_t)L_15));
		int32_t L_16 = V_0;
		int32_t L_17 = V_1;
		int32_t L_18 = V_1;
		int32_t L_19 = V_1;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_16|(int32_t)L_17))-(int32_t)L_18))*(int32_t)L_19));
		int32_t L_20 = V_0;
		int32_t L_21 = V_1;
		int32_t L_22 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_20/(int32_t)L_21))^(int32_t)L_22));
	}

IL_00b5:
	{
		Stack_1_t2974409999 * L_23 = (Stack_1_t2974409999 *)__this->get_idleList_0();
		Il2CppObject * L_24 = ___obj0;
		NullCheck((Stack_1_t2974409999 *)L_23);
		((  void (*) (Stack_1_t2974409999 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Stack_1_t2974409999 *)L_23, (Il2CppObject *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_00c1:
	{
		return;
	}
}
// System.Void Pomelo.DotNetClient.ObjPool`1<System.Object>::Dispose()
extern "C"  void ObjPool_1_Dispose_m4009443525_gshared (ObjPool_1_t286528818 * __this, const MethodInfo* method)
{
	{
		Stack_1_t2974409999 * L_0 = (Stack_1_t2974409999 *)__this->get_idleList_0();
		NullCheck((Stack_1_t2974409999 *)L_0);
		((  void (*) (Stack_1_t2974409999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Stack_1_t2974409999 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Void ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2E`1<System.Object>::.ctor()
extern "C"  void U3CGetExtendedValuesU3Ec__Iterator2E_1__ctor_m1543935562_gshared (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TValue ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2E`1<System.Object>::System.Collections.Generic.IEnumerator<TValue>.get_Current()
extern "C"  Il2CppObject * U3CGetExtendedValuesU3Ec__Iterator2E_1_System_Collections_Generic_IEnumeratorU3CTValueU3E_get_Current_m1377785279_gshared (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_8();
		return L_0;
	}
}
// System.Object ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2E`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetExtendedValuesU3Ec__Iterator2E_1_System_Collections_IEnumerator_get_Current_m925193500_gshared (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_8();
		return L_0;
	}
}
// System.Collections.IEnumerator ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2E`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CGetExtendedValuesU3Ec__Iterator2E_1_System_Collections_IEnumerable_GetEnumerator_m1009020887_gshared (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TValue> ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2E`1<System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* U3CGetExtendedValuesU3Ec__Iterator2E_1_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2968560516_gshared (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 * __this, const MethodInfo* method)
{
	U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_7();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 * L_2 = (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 *)L_2;
		U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 * L_3 = V_0;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CU24U3Einstance_9();
		NullCheck(L_3);
		L_3->set_instance_0(L_4);
		U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 * L_5 = V_0;
		int32_t L_6 = (int32_t)__this->get_U3CU24U3Etag_10();
		NullCheck(L_5);
		L_5->set_tag_1(L_6);
		U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 * L_7 = V_0;
		int32_t L_8 = (int32_t)__this->get_U3CU24U3Eformat_11();
		NullCheck(L_7);
		L_7->set_format_2(L_8);
		U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 * L_9 = V_0;
		bool L_10 = (bool)__this->get_U3CU24U3Esingleton_12();
		NullCheck(L_9);
		L_9->set_singleton_3(L_10);
		U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 * L_11 = V_0;
		bool L_12 = (bool)__this->get_U3CU24U3EallowDefinedTag_13();
		NullCheck(L_11);
		L_11->set_allowDefinedTag_4(L_12);
		U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 * L_13 = V_0;
		return L_13;
	}
}
// System.Boolean ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2E`1<System.Object>::MoveNext()
extern Il2CppClass* RuntimeTypeModel_t242172789_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetExtendedValuesU3Ec__Iterator2E_1_MoveNext_m1975364714_MetadataUsageId;
extern "C"  bool U3CGetExtendedValuesU3Ec__Iterator2E_1_MoveNext_m1975364714_gshared (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetExtendedValuesU3Ec__Iterator2E_1_MoveNext_m1975364714_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Il2CppObject * V_2 = NULL;
	bool V_3 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_7();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_7((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0063;
		}
	}
	{
		goto IL_00db;
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeTypeModel_t242172789_il2cpp_TypeInfo_var);
		RuntimeTypeModel_t242172789 * L_2 = RuntimeTypeModel_get_Default_m173113300(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 4)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_instance_0();
		int32_t L_5 = (int32_t)__this->get_tag_1();
		int32_t L_6 = (int32_t)__this->get_format_2();
		bool L_7 = (bool)__this->get_singleton_3();
		bool L_8 = (bool)__this->get_allowDefinedTag_4();
		Il2CppObject * L_9 = ExtensibleUtil_GetExtendedValues_m2668747672(NULL /*static, unused*/, (TypeModel_t2730011105 *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, (int32_t)L_5, (int32_t)L_6, (bool)L_7, (bool)L_8, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_9);
		Il2CppObject * L_10 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t3464557803_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
		__this->set_U3CU24s_237U3E__0_5(L_10);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0063:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_11 = V_0;
			if (((int32_t)((int32_t)L_11-(int32_t)1)) == 0)
			{
				goto IL_00a4;
			}
		}

IL_006f:
		{
			goto IL_00a4;
		}

IL_0074:
		{
			Il2CppObject * L_12 = (Il2CppObject *)__this->get_U3CU24s_237U3E__0_5();
			NullCheck((Il2CppObject *)L_12);
			Il2CppObject * L_13 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
			__this->set_U3CvalueU3E__1_6(((Il2CppObject *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))));
			Il2CppObject * L_14 = (Il2CppObject *)__this->get_U3CvalueU3E__1_6();
			__this->set_U24current_8(L_14);
			__this->set_U24PC_7(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xDD, FINALLY_00b9);
		}

IL_00a4:
		{
			Il2CppObject * L_15 = (Il2CppObject *)__this->get_U3CU24s_237U3E__0_5();
			NullCheck((Il2CppObject *)L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			if (L_16)
			{
				goto IL_0074;
			}
		}

IL_00b4:
		{
			IL2CPP_LEAVE(0xD4, FINALLY_00b9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00b9;
	}

FINALLY_00b9:
	{ // begin finally (depth: 1)
		{
			bool L_17 = V_1;
			if (!L_17)
			{
				goto IL_00bd;
			}
		}

IL_00bc:
		{
			IL2CPP_END_FINALLY(185)
		}

IL_00bd:
		{
			Il2CppObject * L_18 = (Il2CppObject *)__this->get_U3CU24s_237U3E__0_5();
			V_2 = (Il2CppObject *)((Il2CppObject *)IsInst(L_18, IDisposable_t1423340799_il2cpp_TypeInfo_var));
			Il2CppObject * L_19 = V_2;
			if (L_19)
			{
				goto IL_00cd;
			}
		}

IL_00cc:
		{
			IL2CPP_END_FINALLY(185)
		}

IL_00cd:
		{
			Il2CppObject * L_20 = V_2;
			NullCheck((Il2CppObject *)L_20);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_20);
			IL2CPP_END_FINALLY(185)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(185)
	{
		IL2CPP_JUMP_TBL(0xDD, IL_00dd)
		IL2CPP_JUMP_TBL(0xD4, IL_00d4)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00d4:
	{
		__this->set_U24PC_7((-1));
	}

IL_00db:
	{
		return (bool)0;
	}

IL_00dd:
	{
		return (bool)1;
	}
	// Dead block : IL_00df: ldloc.3
}
// System.Void ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2E`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetExtendedValuesU3Ec__Iterator2E_1_Dispose_m1856641031_MetadataUsageId;
extern "C"  void U3CGetExtendedValuesU3Ec__Iterator2E_1_Dispose_m1856641031_gshared (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetExtendedValuesU3Ec__Iterator2E_1_Dispose_m1856641031_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_7();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003d;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003d;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3D, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_2 = (Il2CppObject *)__this->get_U3CU24s_237U3E__0_5();
			V_1 = (Il2CppObject *)((Il2CppObject *)IsInst(L_2, IDisposable_t1423340799_il2cpp_TypeInfo_var));
			Il2CppObject * L_3 = V_1;
			if (L_3)
			{
				goto IL_0036;
			}
		}

IL_0035:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_0036:
		{
			Il2CppObject * L_4 = V_1;
			NullCheck((Il2CppObject *)L_4);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_003d:
	{
		return;
	}
}
// System.Void ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2E`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetExtendedValuesU3Ec__Iterator2E_1_Reset_m3485335799_MetadataUsageId;
extern "C"  void U3CGetExtendedValuesU3Ec__Iterator2E_1_Reset_m3485335799_gshared (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetExtendedValuesU3Ec__Iterator2E_1_Reset_m3485335799_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ProtoBuf.Meta.TypeModel/DeserializeItemsIterator`1<System.Object>::.ctor(ProtoBuf.Meta.TypeModel,System.IO.Stream,ProtoBuf.PrefixStyle,System.Int32,ProtoBuf.SerializationContext)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t DeserializeItemsIterator_1__ctor_m4149891648_MetadataUsageId;
extern "C"  void DeserializeItemsIterator_1__ctor_m4149891648_gshared (DeserializeItemsIterator_1_t2272739822 * __this, TypeModel_t2730011105 * ___model0, Stream_t1561764144 * ___source1, int32_t ___style2, int32_t ___expectedField3, SerializationContext_t3997850667 * ___context4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeserializeItemsIterator_1__ctor_m4149891648_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeModel_t2730011105 * L_0 = ___model0;
		Stream_t1561764144 * L_1 = ___source1;
		TypeModel_t2730011105 * L_2 = ___model0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((TypeModel_t2730011105 *)L_2);
		Type_t * L_4 = TypeModel_MapType_m2509999337((TypeModel_t2730011105 *)L_2, (Type_t *)L_3, /*hidden argument*/NULL);
		int32_t L_5 = ___style2;
		int32_t L_6 = ___expectedField3;
		SerializationContext_t3997850667 * L_7 = ___context4;
		NullCheck((DeserializeItemsIterator_t1187605077 *)__this);
		DeserializeItemsIterator__ctor_m2411945372((DeserializeItemsIterator_t1187605077 *)__this, (TypeModel_t2730011105 *)L_0, (Stream_t1561764144 *)L_1, (Type_t *)L_4, (int32_t)L_5, (int32_t)L_6, (TypeResolver_t356109658 *)NULL, (SerializationContext_t3997850667 *)L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> ProtoBuf.Meta.TypeModel/DeserializeItemsIterator`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* DeserializeItemsIterator_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2056884287_gshared (DeserializeItemsIterator_1_t2272739822 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Void ProtoBuf.Meta.TypeModel/DeserializeItemsIterator`1<System.Object>::System.IDisposable.Dispose()
extern "C"  void DeserializeItemsIterator_1_System_IDisposable_Dispose_m171632202_gshared (DeserializeItemsIterator_1_t2272739822 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// T ProtoBuf.Meta.TypeModel/DeserializeItemsIterator`1<System.Object>::get_Current()
extern "C"  Il2CppObject * DeserializeItemsIterator_1_get_Current_m3483484962_gshared (DeserializeItemsIterator_1_t2272739822 * __this, const MethodInfo* method)
{
	{
		NullCheck((DeserializeItemsIterator_t1187605077 *)__this);
		Il2CppObject * L_0 = DeserializeItemsIterator_get_Current_m660778239((DeserializeItemsIterator_t1187605077 *)__this, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1)));
	}
}
// System.Void SharpKit.JavaScript.JsFunc`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void JsFunc_3__ctor_m3119373905_gshared (JsFunc_3_t2543207488 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult SharpKit.JavaScript.JsFunc`3<System.Object,System.Object,System.Object>::Invoke(T1,T2)
extern "C"  Il2CppObject * JsFunc_3_Invoke_m366452956_gshared (JsFunc_3_t2543207488 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		JsFunc_3_Invoke_m366452956((JsFunc_3_t2543207488 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult SharpKit.JavaScript.JsFunc`3<System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * JsFunc_3_BeginInvoke_m1181892065_gshared (JsFunc_3_t2543207488 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult SharpKit.JavaScript.JsFunc`3<System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * JsFunc_3_EndInvoke_m3327674751_gshared (JsFunc_3_t2543207488 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void Singleton`1<System.Object>::.ctor()
extern "C"  void Singleton_1__ctor_m3958676923_gshared (Singleton_1_t128664468 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T Singleton`1<System.Object>::get_Instance()
extern "C"  Il2CppObject * Singleton_1_get_Instance_m1020946630_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Il2CppObject * L_0 = ((Singleton_1_t128664468_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get__instance_0();
		if (L_0)
		{
			goto IL_007c;
		}
	}
	{
		bool L_1 = BreakUtil_IsBreak3_m654038886(NULL /*static, unused*/, (float)(-2.14963123E+09f), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0072;
		}
	}
	{
		V_0 = (int32_t)((int32_t)15211492);
		V_1 = (int32_t)((int32_t)15211574);
		int32_t L_2 = V_0;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2/(int32_t)L_3))/(int32_t)L_4));
		int32_t L_5 = V_0;
		int32_t L_6 = V_1;
		int32_t L_7 = V_1;
		int32_t L_8 = V_0;
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5%(int32_t)L_6))&(int32_t)L_7))+(int32_t)L_8))^(int32_t)L_9));
		int32_t L_10 = V_0;
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		int32_t L_13 = V_0;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_10%(int32_t)L_11))/(int32_t)L_12))%(int32_t)L_13));
	}

IL_0072:
	{
		Il2CppObject * L_14 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((Singleton_1_t128664468_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set__instance_0(L_14);
	}

IL_007c:
	{
		Il2CppObject * L_15 = ((Singleton_1_t128664468_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get__instance_0();
		return L_15;
	}
}
// System.Void System.Action`1<Pomelo.DotNetClient.NetWorkState>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3333529409_gshared (Action_1_t3724939911 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<Pomelo.DotNetClient.NetWorkState>::Invoke(T)
extern "C"  void Action_1_Invoke_m4209178636_gshared (Action_1_t3724939911 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m4209178636((Action_1_t3724939911 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<Pomelo.DotNetClient.NetWorkState>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* NetWorkState_t3329123775_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3316817217_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3316817217_gshared (Action_1_t3724939911 * __this, int32_t ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3316817217_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(NetWorkState_t3329123775_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<Pomelo.DotNetClient.NetWorkState>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m140314984_gshared (Action_1_t3724939911 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m377969142_gshared (Action_1_t872614854 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Boolean>::Invoke(T)
extern "C"  void Action_1_Invoke_m3594021162_gshared (Action_1_t872614854 * __this, bool ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3594021162((Action_1_t872614854 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m647183148_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m647183148_gshared (Action_1_t872614854 * __this, bool ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m647183148_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1601629789_gshared (Action_1_t872614854 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m372701337_gshared (Action_1_t2340485113 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Invoke(T)
extern "C"  void Action_1_Invoke_m2500958603_gshared (Action_1_t2340485113 * __this, KeyValuePair_2_t1944668977  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2500958603((Action_1_t2340485113 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, KeyValuePair_2_t1944668977  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, KeyValuePair_2_t1944668977  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* KeyValuePair_2_t1944668977_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m192152288_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m192152288_gshared (Action_1_t2340485113 * __this, KeyValuePair_2_t1944668977  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m192152288_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(KeyValuePair_2_t1944668977_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m629710633_gshared (Action_1_t2340485113 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m375071632_gshared (Action_1_t1549654636 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Int32>::Invoke(T)
extern "C"  void Action_1_Invoke_m4053252443_gshared (Action_1_t1549654636 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m4053252443((Action_1_t1549654636 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3640105362_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3640105362_gshared (Action_1_t1549654636 * __this, int32_t ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3640105362_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2973204151_gshared (Action_1_t1549654636 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m881151526_gshared (Action_1_t271665211 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Object>::Invoke(T)
extern "C"  void Action_1_Invoke_m663971678_gshared (Action_1_t271665211 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m663971678((Action_1_t271665211 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m917692971_gshared (Action_1_t271665211 * __this, Il2CppObject * ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3562128182_gshared (Action_1_t271665211 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.Bounds>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3450290067_gshared (Action_1_t3107457985 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.Bounds>::Invoke(T)
extern "C"  void Action_1_Invoke_m1516367454_gshared (Action_1_t3107457985 * __this, Bounds_t2711641849  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1516367454((Action_1_t3107457985 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Bounds_t2711641849  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Bounds_t2711641849  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.Bounds>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Bounds_t2711641849_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1189779823_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1189779823_gshared (Action_1_t3107457985 * __this, Bounds_t2711641849  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1189779823_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Bounds_t2711641849_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.Bounds>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2006099706_gshared (Action_1_t3107457985 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m2556970446_gshared (Action_2_t3398277931 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Int32,System.Int32>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m1038621325_gshared (Action_2_t3398277931 * __this, int32_t ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m1038621325((Action_2_t3398277931 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Int32,System.Int32>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m1465935788_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m1465935788_gshared (Action_2_t3398277931 * __this, int32_t ___arg10, int32_t ___arg21, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m1465935788_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m2634434542_gshared (Action_2_t3398277931 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m3309597785_gshared (Action_2_t599046810 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Object,System.Boolean>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m923698738_gshared (Action_2_t599046810 * __this, Il2CppObject * ___arg10, bool ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m923698738((Action_2_t599046810 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, bool ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, bool ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Object,System.Boolean>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m2135774937_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m2135774937_gshared (Action_2_t599046810 * __this, Il2CppObject * ___arg10, bool ___arg21, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m2135774937_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Object,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m2751209_gshared (Action_2_t599046810 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m4171654682_gshared (Action_2_t4293064463 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Object,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m1325815329_gshared (Action_2_t4293064463 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m1325815329((Action_2_t4293064463 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Object,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_2_BeginInvoke_m3413657584_gshared (Action_2_t4293064463 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m3926193450_gshared (Action_2_t4293064463 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Object,System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m4284393137_gshared (Action_2_t119199768 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Object,System.Single>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m1544981722_gshared (Action_2_t119199768 * __this, Il2CppObject * ___arg10, float ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m1544981722((Action_2_t119199768 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, float ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, float ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, float ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Object,System.Single>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m801995257_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m801995257_gshared (Action_2_t119199768 * __this, Il2CppObject * ___arg10, float ___arg21, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m801995257_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Object,System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m4146796865_gshared (Action_2_t119199768 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Single,System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m2287609336_gshared (Action_2_t1327107275 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Single,System.Single>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m2101196835_gshared (Action_2_t1327107275 * __this, float ___arg10, float ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m2101196835((Action_2_t1327107275 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, float ___arg10, float ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, float ___arg10, float ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Single,System.Single>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m3738822722_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m3738822722_gshared (Action_2_t1327107275 * __this, float ___arg10, float ___arg21, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m3738822722_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Single,System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m4151888152_gshared (Action_2_t1327107275 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`3<System.Boolean,System.Object,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m3521016969_gshared (Action_3_t1721707197 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`3<System.Boolean,System.Object,System.Int32>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m2964307845_gshared (Action_3_t1721707197 * __this, bool ___arg10, Il2CppObject * ___arg21, int32_t ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_3_Invoke_m2964307845((Action_3_t1721707197 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, Il2CppObject * ___arg21, int32_t ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg10, Il2CppObject * ___arg21, int32_t ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`3<System.Boolean,System.Object,System.Int32>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Action_3_BeginInvoke_m2072042172_MetadataUsageId;
extern "C"  Il2CppObject * Action_3_BeginInvoke_m2072042172_gshared (Action_3_t1721707197 * __this, bool ___arg10, Il2CppObject * ___arg21, int32_t ___arg32, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_3_BeginInvoke_m2072042172_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	__d_args[2] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___arg32);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.Action`3<System.Boolean,System.Object,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m3568014105_gshared (Action_3_t1721707197 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`3<System.Int32,System.Object,UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m1323149667_gshared (Action_3_t3896231253 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`3<System.Int32,System.Object,UnityEngine.Vector3>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m1545623263_gshared (Action_3_t3896231253 * __this, int32_t ___arg10, Il2CppObject * ___arg21, Vector3_t4282066566  ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_3_Invoke_m1545623263((Action_3_t3896231253 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, Il2CppObject * ___arg21, Vector3_t4282066566  ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg10, Il2CppObject * ___arg21, Vector3_t4282066566  ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`3<System.Int32,System.Object,UnityEngine.Vector3>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern const uint32_t Action_3_BeginInvoke_m333125518_MetadataUsageId;
extern "C"  Il2CppObject * Action_3_BeginInvoke_m333125518_gshared (Action_3_t3896231253 * __this, int32_t ___arg10, Il2CppObject * ___arg21, Vector3_t4282066566  ___arg32, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_3_BeginInvoke_m333125518_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	__d_args[2] = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &___arg32);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.Action`3<System.Int32,System.Object,UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m3203089651_gshared (Action_3_t3896231253 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`3<System.Object,LogLevel,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m3139791906_gshared (Action_3_t1023436963 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`3<System.Object,LogLevel,System.Boolean>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m2104138590_gshared (Action_3_t1023436963 * __this, Il2CppObject * ___arg10, int32_t ___arg21, bool ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_3_Invoke_m2104138590((Action_3_t1023436963 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, int32_t ___arg21, bool ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, int32_t ___arg21, bool ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg21, bool ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`3<System.Object,LogLevel,System.Boolean>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern Il2CppClass* LogLevel_t2060375744_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t Action_3_BeginInvoke_m2296417173_MetadataUsageId;
extern "C"  Il2CppObject * Action_3_BeginInvoke_m2296417173_gshared (Action_3_t1023436963 * __this, Il2CppObject * ___arg10, int32_t ___arg21, bool ___arg32, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_3_BeginInvoke_m2296417173_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = Box(LogLevel_t2060375744_il2cpp_TypeInfo_var, &___arg21);
	__d_args[2] = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &___arg32);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.Action`3<System.Object,LogLevel,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m2999467826_gshared (Action_3_t1023436963 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`3<System.Object,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m3769249099_gshared (Action_3_t2073482325 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`3<System.Object,System.Int32,System.Int32>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m2878702791_gshared (Action_3_t2073482325 * __this, Il2CppObject * ___arg10, int32_t ___arg21, int32_t ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_3_Invoke_m2878702791((Action_3_t2073482325 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, int32_t ___arg21, int32_t ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, int32_t ___arg21, int32_t ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg21, int32_t ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`3<System.Object,System.Int32,System.Int32>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Action_3_BeginInvoke_m1610368766_MetadataUsageId;
extern "C"  Il2CppObject * Action_3_BeginInvoke_m1610368766_gshared (Action_3_t2073482325 * __this, Il2CppObject * ___arg10, int32_t ___arg21, int32_t ___arg32, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_3_BeginInvoke_m1610368766_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___arg21);
	__d_args[2] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___arg32);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.Action`3<System.Object,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m1579547867_gshared (Action_3_t2073482325 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m22279495_gshared (Action_3_t2968268857 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`3<System.Object,System.Object,System.Object>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m3949892547_gshared (Action_3_t2968268857 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_3_Invoke_m3949892547((Action_3_t2968268857 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`3<System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_3_BeginInvoke_m1386617082_gshared (Action_3_t2968268857 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	void *__d_args[4] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.Action`3<System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m3792380631_gshared (Action_3_t2968268857 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`3<System.Object,UnityEngine.Vector3,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m1521137548_gshared (Action_3_t2075012986 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`3<System.Object,UnityEngine.Vector3,System.Object>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m50122056_gshared (Action_3_t2075012986 * __this, Il2CppObject * ___arg10, Vector3_t4282066566  ___arg21, Il2CppObject * ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_3_Invoke_m50122056((Action_3_t2075012986 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Vector3_t4282066566  ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Vector3_t4282066566  ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector3_t4282066566  ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`3<System.Object,UnityEngine.Vector3,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern const uint32_t Action_3_BeginInvoke_m2347005567_MetadataUsageId;
extern "C"  Il2CppObject * Action_3_BeginInvoke_m2347005567_gshared (Action_3_t2075012986 * __this, Il2CppObject * ___arg10, Vector3_t4282066566  ___arg21, Il2CppObject * ___arg32, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_3_BeginInvoke_m2347005567_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &___arg21);
	__d_args[2] = ___arg32;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.Action`3<System.Object,UnityEngine.Vector3,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m851431324_gshared (Action_3_t2075012986 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`3<System.Single,System.Single,System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m1124849980_gshared (Action_3_t563912982 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`3<System.Single,System.Single,System.Single>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m40202248_gshared (Action_3_t563912982 * __this, float ___arg10, float ___arg21, float ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_3_Invoke_m40202248((Action_3_t563912982 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, float ___arg10, float ___arg21, float ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, float ___arg10, float ___arg21, float ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`3<System.Single,System.Single,System.Single>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t Action_3_BeginInvoke_m1203539007_MetadataUsageId;
extern "C"  Il2CppObject * Action_3_BeginInvoke_m1203539007_gshared (Action_3_t563912982 * __this, float ___arg10, float ___arg21, float ___arg32, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_3_BeginInvoke_m1203539007_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___arg21);
	__d_args[2] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___arg32);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.Action`3<System.Single,System.Single,System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m1509204572_gshared (Action_3_t563912982 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`4<System.Int32,System.Object,FightEnum.EDamageType,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_4__ctor_m1038436083_gshared (Action_4_t1491902808 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`4<System.Int32,System.Object,FightEnum.EDamageType,System.Int32>::Invoke(T1,T2,T3,T4)
extern "C"  void Action_4_Invoke_m3680689175_gshared (Action_4_t1491902808 * __this, int32_t ___arg10, Il2CppObject * ___arg21, int32_t ___arg32, int32_t ___arg43, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_4_Invoke_m3680689175((Action_4_t1491902808 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, ___arg43, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, Il2CppObject * ___arg21, int32_t ___arg32, int32_t ___arg43, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg10, Il2CppObject * ___arg21, int32_t ___arg32, int32_t ___arg43, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`4<System.Int32,System.Object,FightEnum.EDamageType,System.Int32>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* EDamageType_t2535357340_il2cpp_TypeInfo_var;
extern const uint32_t Action_4_BeginInvoke_m995807384_MetadataUsageId;
extern "C"  Il2CppObject * Action_4_BeginInvoke_m995807384_gshared (Action_4_t1491902808 * __this, int32_t ___arg10, Il2CppObject * ___arg21, int32_t ___arg32, int32_t ___arg43, AsyncCallback_t1369114871 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_4_BeginInvoke_m995807384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	__d_args[2] = Box(EDamageType_t2535357340_il2cpp_TypeInfo_var, &___arg32);
	__d_args[3] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___arg43);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.Void System.Action`4<System.Int32,System.Object,FightEnum.EDamageType,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_4_EndInvoke_m889203331_gshared (Action_4_t1491902808 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`4<System.Int32,System.Object,UnityEngine.Vector3,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_4__ctor_m4168226482_gshared (Action_4_t3253885269 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`4<System.Int32,System.Object,UnityEngine.Vector3,System.Object>::Invoke(T1,T2,T3,T4)
extern "C"  void Action_4_Invoke_m3504236088_gshared (Action_4_t3253885269 * __this, int32_t ___arg10, Il2CppObject * ___arg21, Vector3_t4282066566  ___arg32, Il2CppObject * ___arg43, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_4_Invoke_m3504236088((Action_4_t3253885269 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, ___arg43, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, Il2CppObject * ___arg21, Vector3_t4282066566  ___arg32, Il2CppObject * ___arg43, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg10, Il2CppObject * ___arg21, Vector3_t4282066566  ___arg32, Il2CppObject * ___arg43, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`4<System.Int32,System.Object,UnityEngine.Vector3,System.Object>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern const uint32_t Action_4_BeginInvoke_m1187015233_MetadataUsageId;
extern "C"  Il2CppObject * Action_4_BeginInvoke_m1187015233_gshared (Action_4_t3253885269 * __this, int32_t ___arg10, Il2CppObject * ___arg21, Vector3_t4282066566  ___arg32, Il2CppObject * ___arg43, AsyncCallback_t1369114871 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_4_BeginInvoke_m1187015233_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	__d_args[2] = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &___arg32);
	__d_args[3] = ___arg43;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.Void System.Action`4<System.Int32,System.Object,UnityEngine.Vector3,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_4_EndInvoke_m1437643202_gshared (Action_4_t3253885269 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`4<System.Object,System.Int32,System.Object,System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_4__ctor_m2555180776_gshared (Action_4_t2668625267 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`4<System.Object,System.Int32,System.Object,System.Single>::Invoke(T1,T2,T3,T4)
extern "C"  void Action_4_Invoke_m1661067458_gshared (Action_4_t2668625267 * __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, float ___arg43, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_4_Invoke_m1661067458((Action_4_t2668625267 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, ___arg43, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, float ___arg43, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, float ___arg43, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg21, Il2CppObject * ___arg32, float ___arg43, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`4<System.Object,System.Int32,System.Object,System.Single>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t Action_4_BeginInvoke_m3540796875_MetadataUsageId;
extern "C"  Il2CppObject * Action_4_BeginInvoke_m3540796875_gshared (Action_4_t2668625267 * __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, float ___arg43, AsyncCallback_t1369114871 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_4_BeginInvoke_m3540796875_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___arg21);
	__d_args[2] = ___arg32;
	__d_args[3] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___arg43);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.Void System.Action`4<System.Object,System.Int32,System.Object,System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_4_EndInvoke_m3997179128_gshared (Action_4_t2668625267 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_4__ctor_m620347252_gshared (Action_4_t1730810465 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`4<System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4)
extern "C"  void Action_4_Invoke_m157716918_gshared (Action_4_t1730810465 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_4_Invoke_m157716918((Action_4_t1730810465 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, ___arg43, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`4<System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_4_BeginInvoke_m1414752311_gshared (Action_4_t1730810465 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, AsyncCallback_t1369114871 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	void *__d_args[5] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	__d_args[3] = ___arg43;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.Void System.Action`4<System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_4_EndInvoke_m2556365700_gshared (Action_4_t1730810465 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m4262077373_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2204526468_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4054268681_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m4201941769_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t1756209413 * L_2 = (ArrayReadOnlyList_1_t1756209413 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t1756209413 * L_9 = (ArrayReadOnlyList_1_t1756209413 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		ObjectU5BU5D_t1108656482* L_10 = (ObjectU5BU5D_t1108656482*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2650805434_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m1908510314_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m1908510314_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m1908510314_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m2799549978_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeNamedArgument_t3059612989  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2027929923_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t3059612989  L_0 = (CustomAttributeNamedArgument_t3059612989 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1186685398_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t3059612989  L_0 = (CustomAttributeNamedArgument_t3059612989 )__this->get_U24current_2();
		CustomAttributeNamedArgument_t3059612989  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m4084754896_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t645006031 * L_2 = (ArrayReadOnlyList_1_t645006031 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_3 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		CustomAttributeNamedArgument_t3059612989  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t645006031 * L_9 = (ArrayReadOnlyList_1_t645006031 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_10 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m1616284631_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m445982919_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m445982919_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m445982919_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m643544331_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeTypedArgument_t3301293422  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m4108562996_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t3301293422  L_0 = (CustomAttributeTypedArgument_t3301293422 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2728697221_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t3301293422  L_0 = (CustomAttributeTypedArgument_t3301293422 )__this->get_U24current_2();
		CustomAttributeTypedArgument_t3301293422  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m1461469503_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t886686464 * L_2 = (ArrayReadOnlyList_1_t886686464 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_3 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		CustomAttributeTypedArgument_t3301293422  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t886686464 * L_9 = (ArrayReadOnlyList_1_t886686464 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_10 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m4164061832_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m2584944568_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m2584944568_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m2584944568_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m240689135_gshared (ArrayReadOnlyList_1_t1756209413 * __this, ObjectU5BU5D_t1108656482* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m2499499206_gshared (ArrayReadOnlyList_1_t1756209413 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t1756209413 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t1756209413 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t1756209413 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m535939909_MetadataUsageId;
extern "C"  Il2CppObject * ArrayReadOnlyList_1_get_Item_m535939909_gshared (ArrayReadOnlyList_1_t1756209413 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m535939909_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		ObjectU5BU5D_t1108656482* L_1 = (ObjectU5BU5D_t1108656482*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m2625374704_gshared (ArrayReadOnlyList_1_t1756209413 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m3813449101_gshared (ArrayReadOnlyList_1_t1756209413 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m1486920810_gshared (ArrayReadOnlyList_1_t1756209413 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m976758002_gshared (ArrayReadOnlyList_1_t1756209413 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m2221418008_gshared (ArrayReadOnlyList_1_t1756209413 * __this, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m158553866_gshared (ArrayReadOnlyList_1_t1756209413 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1108656482*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t1108656482*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m1244492898_gshared (ArrayReadOnlyList_1_t1756209413 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_array_0();
		ObjectU5BU5D_t1108656482* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m910233845((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m1770413409_gshared (ArrayReadOnlyList_1_t1756209413 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1098739118_gshared (ArrayReadOnlyList_1_t1756209413 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1108656482*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t1108656482*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m738278233_gshared (ArrayReadOnlyList_1_t1756209413 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m12996997_gshared (ArrayReadOnlyList_1_t1756209413 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m2907098399_gshared (ArrayReadOnlyList_1_t1756209413 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral823020769;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m740547916_MetadataUsageId;
extern "C"  Exception_t3991598821 * ArrayReadOnlyList_1_ReadOnlyError_m740547916_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m740547916_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, (String_t*)_stringLiteral823020769, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m3527945938_gshared (ArrayReadOnlyList_1_t645006031 * __this, CustomAttributeNamedArgumentU5BU5D_t1983528240* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m932360725_gshared (ArrayReadOnlyList_1_t645006031 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t645006031 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t645006031 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t645006031 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m3165498630_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t3059612989  ArrayReadOnlyList_1_get_Item_m3165498630_gshared (ArrayReadOnlyList_1_t645006031 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m3165498630_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_1 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_3 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		CustomAttributeNamedArgument_t3059612989  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m3031060371_gshared (ArrayReadOnlyList_1_t645006031 * __this, int32_t ___index0, CustomAttributeNamedArgument_t3059612989  ___value1, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m2401585746_gshared (ArrayReadOnlyList_1_t645006031 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_0 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m2438902353_gshared (ArrayReadOnlyList_1_t645006031 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m3652230485_gshared (ArrayReadOnlyList_1_t645006031 * __this, CustomAttributeNamedArgument_t3059612989  ___item0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m3556686357_gshared (ArrayReadOnlyList_1_t645006031 * __this, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m2530929859_gshared (ArrayReadOnlyList_1_t645006031 * __this, CustomAttributeNamedArgument_t3059612989  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_0 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get_array_0();
		CustomAttributeNamedArgument_t3059612989  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t1983528240*, CustomAttributeNamedArgument_t3059612989 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t1983528240*)L_0, (CustomAttributeNamedArgument_t3059612989 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m1650178565_gshared (ArrayReadOnlyList_1_t645006031 * __this, CustomAttributeNamedArgumentU5BU5D_t1983528240* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_0 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get_array_0();
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m910233845((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m2319686054_gshared (ArrayReadOnlyList_1_t645006031 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m3408499785_gshared (ArrayReadOnlyList_1_t645006031 * __this, CustomAttributeNamedArgument_t3059612989  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_0 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get_array_0();
		CustomAttributeNamedArgument_t3059612989  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t1983528240*, CustomAttributeNamedArgument_t3059612989 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t1983528240*)L_0, (CustomAttributeNamedArgument_t3059612989 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m2906295740_gshared (ArrayReadOnlyList_1_t645006031 * __this, int32_t ___index0, CustomAttributeNamedArgument_t3059612989  ___item1, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m609878398_gshared (ArrayReadOnlyList_1_t645006031 * __this, CustomAttributeNamedArgument_t3059612989  ___item0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m780148610_gshared (ArrayReadOnlyList_1_t645006031 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral823020769;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m467076531_MetadataUsageId;
extern "C"  Exception_t3991598821 * ArrayReadOnlyList_1_ReadOnlyError_m467076531_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m467076531_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, (String_t*)_stringLiteral823020769, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m904660545_gshared (ArrayReadOnlyList_1_t886686464 * __this, CustomAttributeTypedArgumentU5BU5D_t2088020251* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m1042005508_gshared (ArrayReadOnlyList_1_t886686464 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t886686464 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t886686464 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t886686464 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m957797877_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t3301293422  ArrayReadOnlyList_1_get_Item_m957797877_gshared (ArrayReadOnlyList_1_t886686464 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m957797877_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_1 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_3 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		CustomAttributeTypedArgument_t3301293422  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m3144480962_gshared (ArrayReadOnlyList_1_t886686464 * __this, int32_t ___index0, CustomAttributeTypedArgument_t3301293422  ___value1, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m2684117187_gshared (ArrayReadOnlyList_1_t886686464 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_0 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m3126393472_gshared (ArrayReadOnlyList_1_t886686464 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m3859776580_gshared (ArrayReadOnlyList_1_t886686464 * __this, CustomAttributeTypedArgument_t3301293422  ___item0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m1400680710_gshared (ArrayReadOnlyList_1_t886686464 * __this, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m2813461300_gshared (ArrayReadOnlyList_1_t886686464 * __this, CustomAttributeTypedArgument_t3301293422  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_0 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get_array_0();
		CustomAttributeTypedArgument_t3301293422  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2088020251*, CustomAttributeTypedArgument_t3301293422 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2088020251*)L_0, (CustomAttributeTypedArgument_t3301293422 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m1763599156_gshared (ArrayReadOnlyList_1_t886686464 * __this, CustomAttributeTypedArgumentU5BU5D_t2088020251* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_0 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get_array_0();
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m910233845((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m2480410519_gshared (ArrayReadOnlyList_1_t886686464 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m785214392_gshared (ArrayReadOnlyList_1_t886686464 * __this, CustomAttributeTypedArgument_t3301293422  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_0 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get_array_0();
		CustomAttributeTypedArgument_t3301293422  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2088020251*, CustomAttributeTypedArgument_t3301293422 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2088020251*)L_0, (CustomAttributeTypedArgument_t3301293422 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m698594987_gshared (ArrayReadOnlyList_1_t886686464 * __this, int32_t ___index0, CustomAttributeTypedArgument_t3301293422  ___item1, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m3157655599_gshared (ArrayReadOnlyList_1_t886686464 * __this, CustomAttributeTypedArgument_t3301293422  ___item0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m2867415153_gshared (ArrayReadOnlyList_1_t886686464 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral823020769;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m627800996_MetadataUsageId;
extern "C"  Exception_t3991598821 * ArrayReadOnlyList_1_ReadOnlyError_m627800996_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m627800996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, (String_t*)_stringLiteral823020769, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/InternalEnumerator`1<AIEnum.EAIEventtype>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2605660081_gshared (InternalEnumerator_1_t2678631987 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2605660081_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2678631987 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2678631987 *>(__this + 1);
	InternalEnumerator_1__ctor_m2605660081(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<AIEnum.EAIEventtype>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3297957455_gshared (InternalEnumerator_1_t2678631987 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3297957455_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2678631987 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2678631987 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3297957455(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<AIEnum.EAIEventtype>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m829058821_gshared (InternalEnumerator_1_t2678631987 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = ((  uint8_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t2678631987 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m829058821_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2678631987 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2678631987 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m829058821(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<AIEnum.EAIEventtype>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2076998344_gshared (InternalEnumerator_1_t2678631987 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2076998344_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2678631987 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2678631987 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2076998344(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<AIEnum.EAIEventtype>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2889453183_gshared (InternalEnumerator_1_t2678631987 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2889453183_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2678631987 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2678631987 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2889453183(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<AIEnum.EAIEventtype>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2007595674_MetadataUsageId;
extern "C"  uint8_t InternalEnumerator_1_get_Current_m2007595674_gshared (InternalEnumerator_1_t2678631987 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2007595674_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint8_t L_8 = ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint8_t InternalEnumerator_1_get_Current_m2007595674_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2678631987 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2678631987 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2007595674(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<AnimationRunner/AniType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m15960192_gshared (InternalEnumerator_1_t4083548623 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m15960192_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4083548623 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4083548623 *>(__this + 1);
	InternalEnumerator_1__ctor_m15960192(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<AnimationRunner/AniType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1934155040_gshared (InternalEnumerator_1_t4083548623 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1934155040_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4083548623 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4083548623 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1934155040(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<AnimationRunner/AniType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m554652310_gshared (InternalEnumerator_1_t4083548623 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t4083548623 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m554652310_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4083548623 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4083548623 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m554652310(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<AnimationRunner/AniType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1535750743_gshared (InternalEnumerator_1_t4083548623 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1535750743_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4083548623 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4083548623 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1535750743(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<AnimationRunner/AniType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m194000848_gshared (InternalEnumerator_1_t4083548623 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m194000848_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4083548623 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4083548623 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m194000848(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<AnimationRunner/AniType>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3171752489_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m3171752489_gshared (InternalEnumerator_1_t4083548623 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3171752489_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3171752489_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4083548623 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4083548623 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3171752489(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<AstarDebugger/GraphPoint>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2159274478_gshared (InternalEnumerator_1_t4068395185 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2159274478_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4068395185 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4068395185 *>(__this + 1);
	InternalEnumerator_1__ctor_m2159274478(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<AstarDebugger/GraphPoint>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m894236914_gshared (InternalEnumerator_1_t4068395185 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m894236914_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4068395185 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4068395185 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m894236914(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<AstarDebugger/GraphPoint>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1269841054_gshared (InternalEnumerator_1_t4068395185 * __this, const MethodInfo* method)
{
	{
		GraphPoint_t991085213  L_0 = ((  GraphPoint_t991085213  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t4068395185 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		GraphPoint_t991085213  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1269841054_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4068395185 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4068395185 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1269841054(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<AstarDebugger/GraphPoint>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2556585285_gshared (InternalEnumerator_1_t4068395185 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2556585285_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4068395185 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4068395185 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2556585285(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<AstarDebugger/GraphPoint>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2950531038_gshared (InternalEnumerator_1_t4068395185 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2950531038_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4068395185 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4068395185 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2950531038(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<AstarDebugger/GraphPoint>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m858427893_MetadataUsageId;
extern "C"  GraphPoint_t991085213  InternalEnumerator_1_get_Current_m858427893_gshared (InternalEnumerator_1_t4068395185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m858427893_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		GraphPoint_t991085213  L_8 = ((  GraphPoint_t991085213  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  GraphPoint_t991085213  InternalEnumerator_1_get_Current_m858427893_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4068395185 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4068395185 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m858427893(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<AstarDebugger/PathTypeDebug>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3683717694_gshared (InternalEnumerator_1_t2810415885 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3683717694_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2810415885 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2810415885 *>(__this + 1);
	InternalEnumerator_1__ctor_m3683717694(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<AstarDebugger/PathTypeDebug>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1466027682_gshared (InternalEnumerator_1_t2810415885 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1466027682_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2810415885 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2810415885 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1466027682(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<AstarDebugger/PathTypeDebug>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m70506520_gshared (InternalEnumerator_1_t2810415885 * __this, const MethodInfo* method)
{
	{
		PathTypeDebug_t4028073209  L_0 = ((  PathTypeDebug_t4028073209  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t2810415885 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		PathTypeDebug_t4028073209  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m70506520_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2810415885 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2810415885 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m70506520(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<AstarDebugger/PathTypeDebug>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2679550357_gshared (InternalEnumerator_1_t2810415885 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2679550357_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2810415885 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2810415885 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2679550357(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<AstarDebugger/PathTypeDebug>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m643862354_gshared (InternalEnumerator_1_t2810415885 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m643862354_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2810415885 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2810415885 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m643862354(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<AstarDebugger/PathTypeDebug>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1670861287_MetadataUsageId;
extern "C"  PathTypeDebug_t4028073209  InternalEnumerator_1_get_Current_m1670861287_gshared (InternalEnumerator_1_t2810415885 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1670861287_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		PathTypeDebug_t4028073209  L_8 = ((  PathTypeDebug_t4028073209  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  PathTypeDebug_t4028073209  InternalEnumerator_1_get_Current_m1670861287_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2810415885 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2810415885 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1670861287(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<AstarPath/AstarWorkItem>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1550221061_gshared (InternalEnumerator_1_t1349036564 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1550221061_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1349036564 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1349036564 *>(__this + 1);
	InternalEnumerator_1__ctor_m1550221061(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<AstarPath/AstarWorkItem>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2309654139_gshared (InternalEnumerator_1_t1349036564 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2309654139_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1349036564 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1349036564 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2309654139(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<AstarPath/AstarWorkItem>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4121250737_gshared (InternalEnumerator_1_t1349036564 * __this, const MethodInfo* method)
{
	{
		AstarWorkItem_t2566693888  L_0 = ((  AstarWorkItem_t2566693888  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t1349036564 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		AstarWorkItem_t2566693888  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4121250737_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1349036564 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1349036564 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4121250737(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<AstarPath/AstarWorkItem>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2334202652_gshared (InternalEnumerator_1_t1349036564 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2334202652_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1349036564 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1349036564 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2334202652(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<AstarPath/AstarWorkItem>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3471173547_gshared (InternalEnumerator_1_t1349036564 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3471173547_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1349036564 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1349036564 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3471173547(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<AstarPath/AstarWorkItem>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m227055726_MetadataUsageId;
extern "C"  AstarWorkItem_t2566693888  InternalEnumerator_1_get_Current_m227055726_gshared (InternalEnumerator_1_t1349036564 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m227055726_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		AstarWorkItem_t2566693888  L_8 = ((  AstarWorkItem_t2566693888  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  AstarWorkItem_t2566693888  InternalEnumerator_1_get_Current_m227055726_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1349036564 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1349036564 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m227055726(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<AstarPath/GUOSingle>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3870581463_gshared (InternalEnumerator_1_t2439682662 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3870581463_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2439682662 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2439682662 *>(__this + 1);
	InternalEnumerator_1__ctor_m3870581463(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<AstarPath/GUOSingle>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3189087593_gshared (InternalEnumerator_1_t2439682662 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3189087593_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2439682662 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2439682662 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3189087593(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<AstarPath/GUOSingle>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3638426271_gshared (InternalEnumerator_1_t2439682662 * __this, const MethodInfo* method)
{
	{
		GUOSingle_t3657339986  L_0 = ((  GUOSingle_t3657339986  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t2439682662 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		GUOSingle_t3657339986  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3638426271_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2439682662 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2439682662 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3638426271(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<AstarPath/GUOSingle>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2335194478_gshared (InternalEnumerator_1_t2439682662 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2335194478_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2439682662 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2439682662 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2335194478(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<AstarPath/GUOSingle>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2303598745_gshared (InternalEnumerator_1_t2439682662 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2303598745_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2439682662 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2439682662 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2303598745(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<AstarPath/GUOSingle>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3565124160_MetadataUsageId;
extern "C"  GUOSingle_t3657339986  InternalEnumerator_1_get_Current_m3565124160_gshared (InternalEnumerator_1_t2439682662 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3565124160_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		GUOSingle_t3657339986  L_8 = ((  GUOSingle_t3657339986  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  GUOSingle_t3657339986  InternalEnumerator_1_get_Current_m3565124160_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2439682662 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2439682662 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3565124160(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Core.RpsChoice>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m518141880_gshared (InternalEnumerator_1_t3130107104 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m518141880_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3130107104 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3130107104 *>(__this + 1);
	InternalEnumerator_1__ctor_m518141880(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Core.RpsChoice>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3482104552_gshared (InternalEnumerator_1_t3130107104 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3482104552_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3130107104 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3130107104 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3482104552(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Core.RpsChoice>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2250149844_gshared (InternalEnumerator_1_t3130107104 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t3130107104 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2250149844_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3130107104 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3130107104 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2250149844(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Core.RpsChoice>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1123518351_gshared (InternalEnumerator_1_t3130107104 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1123518351_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3130107104 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3130107104 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1123518351(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Core.RpsChoice>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2971755604_gshared (InternalEnumerator_1_t3130107104 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2971755604_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3130107104 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3130107104 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2971755604(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Core.RpsChoice>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m265855487_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m265855487_gshared (InternalEnumerator_1_t3130107104 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m265855487_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m265855487_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3130107104 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3130107104 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m265855487(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Core.RpsResult>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4258094388_gshared (InternalEnumerator_1_t3556904796 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4258094388_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3556904796 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3556904796 *>(__this + 1);
	InternalEnumerator_1__ctor_m4258094388(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Core.RpsResult>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2434876396_gshared (InternalEnumerator_1_t3556904796 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2434876396_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3556904796 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3556904796 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2434876396(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Core.RpsResult>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m64703960_gshared (InternalEnumerator_1_t3556904796 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t3556904796 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m64703960_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3556904796 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3556904796 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m64703960(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Core.RpsResult>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2089818123_gshared (InternalEnumerator_1_t3556904796 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2089818123_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3556904796 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3556904796 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2089818123(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Core.RpsResult>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2862277464_gshared (InternalEnumerator_1_t3556904796 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2862277464_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3556904796 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3556904796 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2862277464(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Core.RpsResult>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2977731707_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m2977731707_gshared (InternalEnumerator_1_t3556904796 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2977731707_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m2977731707_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3556904796 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3556904796 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2977731707(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Entity.Behavior.EBehaviorID>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3498346470_gshared (InternalEnumerator_1_t20875812 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3498346470_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t20875812 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t20875812 *>(__this + 1);
	InternalEnumerator_1__ctor_m3498346470(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Entity.Behavior.EBehaviorID>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1488775162_gshared (InternalEnumerator_1_t20875812 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1488775162_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t20875812 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t20875812 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1488775162(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Entity.Behavior.EBehaviorID>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3968880_gshared (InternalEnumerator_1_t20875812 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = ((  uint8_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t20875812 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3968880_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t20875812 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t20875812 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3968880(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Entity.Behavior.EBehaviorID>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4022966589_gshared (InternalEnumerator_1_t20875812 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4022966589_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t20875812 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t20875812 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4022966589(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Entity.Behavior.EBehaviorID>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3635059882_gshared (InternalEnumerator_1_t20875812 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3635059882_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t20875812 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t20875812 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3635059882(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Entity.Behavior.EBehaviorID>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m454960527_MetadataUsageId;
extern "C"  uint8_t InternalEnumerator_1_get_Current_m454960527_gshared (InternalEnumerator_1_t20875812 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m454960527_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint8_t L_8 = ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint8_t InternalEnumerator_1_get_Current_m454960527_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t20875812 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t20875812 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m454960527(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<FightEnum.ESkillBuffEvent>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3270389123_gshared (InternalEnumerator_1_t3543166237 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3270389123_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3543166237 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3543166237 *>(__this + 1);
	InternalEnumerator_1__ctor_m3270389123(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<FightEnum.ESkillBuffEvent>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1897896765_gshared (InternalEnumerator_1_t3543166237 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1897896765_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3543166237 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3543166237 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1897896765(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<FightEnum.ESkillBuffEvent>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2491596211_gshared (InternalEnumerator_1_t3543166237 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = ((  uint8_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t3543166237 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2491596211_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3543166237 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3543166237 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2491596211(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<FightEnum.ESkillBuffEvent>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2247831322_gshared (InternalEnumerator_1_t3543166237 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2247831322_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3543166237 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3543166237 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2247831322(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<FightEnum.ESkillBuffEvent>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4047698669_gshared (InternalEnumerator_1_t3543166237 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4047698669_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3543166237 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3543166237 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4047698669(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<FightEnum.ESkillBuffEvent>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2677668140_MetadataUsageId;
extern "C"  uint8_t InternalEnumerator_1_get_Current_m2677668140_gshared (InternalEnumerator_1_t3543166237 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2677668140_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint8_t L_8 = ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint8_t InternalEnumerator_1_get_Current_m2677668140_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3543166237 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3543166237 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2677668140(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<FightEnum.ESkillBuffTarget>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3686721278_gshared (InternalEnumerator_1_t1224156876 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3686721278_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1224156876 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1224156876 *>(__this + 1);
	InternalEnumerator_1__ctor_m3686721278(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<FightEnum.ESkillBuffTarget>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3304866274_gshared (InternalEnumerator_1_t1224156876 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3304866274_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1224156876 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1224156876 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3304866274(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<FightEnum.ESkillBuffTarget>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2791404238_gshared (InternalEnumerator_1_t1224156876 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = ((  uint8_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t1224156876 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2791404238_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1224156876 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1224156876 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2791404238(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<FightEnum.ESkillBuffTarget>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3934484053_gshared (InternalEnumerator_1_t1224156876 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3934484053_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1224156876 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1224156876 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3934484053(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<FightEnum.ESkillBuffTarget>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m511631694_gshared (InternalEnumerator_1_t1224156876 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m511631694_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1224156876 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1224156876 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m511631694(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<FightEnum.ESkillBuffTarget>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1382979397_MetadataUsageId;
extern "C"  uint8_t InternalEnumerator_1_get_Current_m1382979397_gshared (InternalEnumerator_1_t1224156876 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1382979397_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint8_t L_8 = ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint8_t InternalEnumerator_1_get_Current_m1382979397_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1224156876 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1224156876 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1382979397(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<FingerGestures/SwipeDirection>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2837548262_gshared (InternalEnumerator_1_t397877 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2837548262_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t397877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t397877 *>(__this + 1);
	InternalEnumerator_1__ctor_m2837548262(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<FingerGestures/SwipeDirection>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m573555450_gshared (InternalEnumerator_1_t397877 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m573555450_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t397877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t397877 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m573555450(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<FingerGestures/SwipeDirection>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m55853360_gshared (InternalEnumerator_1_t397877 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t397877 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m55853360_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t397877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t397877 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m55853360(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<FingerGestures/SwipeDirection>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m318299709_gshared (InternalEnumerator_1_t397877 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m318299709_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t397877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t397877 *>(__this + 1);
	InternalEnumerator_1_Dispose_m318299709(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<FingerGestures/SwipeDirection>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4223196970_gshared (InternalEnumerator_1_t397877 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4223196970_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t397877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t397877 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4223196970(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<FingerGestures/SwipeDirection>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2667349583_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m2667349583_gshared (InternalEnumerator_1_t397877 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2667349583_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m2667349583_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t397877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t397877 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2667349583(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<FLOAT_TEXT_ID>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m482902639_gshared (InternalEnumerator_1_t981688446 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m482902639_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t981688446 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t981688446 *>(__this + 1);
	InternalEnumerator_1__ctor_m482902639(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<FLOAT_TEXT_ID>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2827904721_gshared (InternalEnumerator_1_t981688446 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2827904721_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t981688446 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t981688446 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2827904721(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<FLOAT_TEXT_ID>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2206495175_gshared (InternalEnumerator_1_t981688446 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t981688446 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2206495175_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t981688446 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t981688446 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2206495175(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<FLOAT_TEXT_ID>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1902279430_gshared (InternalEnumerator_1_t981688446 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1902279430_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t981688446 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t981688446 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1902279430(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<FLOAT_TEXT_ID>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2876186497_gshared (InternalEnumerator_1_t981688446 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2876186497_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t981688446 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t981688446 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2876186497(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<FLOAT_TEXT_ID>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2326624408_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m2326624408_gshared (InternalEnumerator_1_t981688446 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2326624408_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m2326624408_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t981688446 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t981688446 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2326624408(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<HatredCtrl/stValue>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1682270547_gshared (InternalEnumerator_1_t2208288086 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1682270547_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2208288086 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2208288086 *>(__this + 1);
	InternalEnumerator_1__ctor_m1682270547(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<HatredCtrl/stValue>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2320412013_gshared (InternalEnumerator_1_t2208288086 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2320412013_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2208288086 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2208288086 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2320412013(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<HatredCtrl/stValue>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3569116441_gshared (InternalEnumerator_1_t2208288086 * __this, const MethodInfo* method)
{
	{
		stValue_t3425945410  L_0 = ((  stValue_t3425945410  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t2208288086 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		stValue_t3425945410  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3569116441_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2208288086 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2208288086 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3569116441(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<HatredCtrl/stValue>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2653155050_gshared (InternalEnumerator_1_t2208288086 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2653155050_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2208288086 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2208288086 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2653155050(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<HatredCtrl/stValue>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1367054681_gshared (InternalEnumerator_1_t2208288086 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1367054681_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2208288086 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2208288086 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1367054681(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<HatredCtrl/stValue>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2843896922_MetadataUsageId;
extern "C"  stValue_t3425945410  InternalEnumerator_1_get_Current_m2843896922_gshared (InternalEnumerator_1_t2208288086 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2843896922_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		stValue_t3425945410  L_8 = ((  stValue_t3425945410  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  stValue_t3425945410  InternalEnumerator_1_get_Current_m2843896922_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2208288086 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2208288086 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2843896922(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3437723947_gshared (InternalEnumerator_1_t1416323886 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3437723947_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1416323886 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1416323886 *>(__this + 1);
	InternalEnumerator_1__ctor_m3437723947(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3309049493_gshared (InternalEnumerator_1_t1416323886 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3309049493_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1416323886 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1416323886 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3309049493(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m427170369_gshared (InternalEnumerator_1_t1416323886 * __this, const MethodInfo* method)
{
	{
		ErrorInfo_t2633981210  L_0 = ((  ErrorInfo_t2633981210  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t1416323886 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ErrorInfo_t2633981210  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m427170369_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1416323886 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1416323886 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m427170369(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1093760194_gshared (InternalEnumerator_1_t1416323886 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1093760194_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1416323886 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1416323886 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1093760194(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2483065473_gshared (InternalEnumerator_1_t1416323886 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2483065473_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1416323886 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1416323886 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2483065473(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1590749234_MetadataUsageId;
extern "C"  ErrorInfo_t2633981210  InternalEnumerator_1_get_Current_m1590749234_gshared (InternalEnumerator_1_t1416323886 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1590749234_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ErrorInfo_t2633981210  L_8 = ((  ErrorInfo_t2633981210  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ErrorInfo_t2633981210  InternalEnumerator_1_get_Current_m1590749234_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1416323886 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1416323886 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1590749234(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2510835690_gshared (InternalEnumerator_1_t2155190829 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2510835690_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2155190829 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2155190829 *>(__this + 1);
	InternalEnumerator_1__ctor_m2510835690(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3501947254_gshared (InternalEnumerator_1_t2155190829 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3501947254_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2155190829 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2155190829 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3501947254(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4292553954_gshared (InternalEnumerator_1_t2155190829 * __this, const MethodInfo* method)
{
	{
		TableRange_t3372848153  L_0 = ((  TableRange_t3372848153  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t2155190829 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TableRange_t3372848153  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4292553954_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2155190829 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2155190829 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4292553954(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2516768321_gshared (InternalEnumerator_1_t2155190829 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2516768321_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2155190829 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2155190829 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2516768321(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1609192930_gshared (InternalEnumerator_1_t2155190829 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1609192930_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2155190829 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2155190829 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1609192930(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2306301105_MetadataUsageId;
extern "C"  TableRange_t3372848153  InternalEnumerator_1_get_Current_m2306301105_gshared (InternalEnumerator_1_t2155190829 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2306301105_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TableRange_t3372848153  L_8 = ((  TableRange_t3372848153  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TableRange_t3372848153  InternalEnumerator_1_get_Current_m2306301105_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2155190829 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2155190829 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2306301105(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4155127258_gshared (InternalEnumerator_1_t1949385224 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4155127258_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1949385224 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1949385224 *>(__this + 1);
	InternalEnumerator_1__ctor_m4155127258(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4053167494_gshared (InternalEnumerator_1_t1949385224 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4053167494_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1949385224 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1949385224 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4053167494(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3385911154_gshared (InternalEnumerator_1_t1949385224 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t1949385224 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3385911154_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1949385224 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1949385224 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3385911154(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4263405617_gshared (InternalEnumerator_1_t1949385224 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4263405617_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1949385224 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1949385224 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4263405617(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m569750258_gshared (InternalEnumerator_1_t1949385224 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m569750258_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1949385224 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1949385224 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m569750258(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3784081185_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m3784081185_gshared (InternalEnumerator_1_t1949385224 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3784081185_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3784081185_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1949385224 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1949385224 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3784081185(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3894762810_gshared (InternalEnumerator_1_t798349321 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3894762810_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t798349321 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t798349321 *>(__this + 1);
	InternalEnumerator_1__ctor_m3894762810(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2241088038_gshared (InternalEnumerator_1_t798349321 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2241088038_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t798349321 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t798349321 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2241088038(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2786336284_gshared (InternalEnumerator_1_t798349321 * __this, const MethodInfo* method)
{
	{
		TagName_t2016006645  L_0 = ((  TagName_t2016006645  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t798349321 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TagName_t2016006645  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2786336284_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t798349321 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t798349321 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2786336284(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1537296273_gshared (InternalEnumerator_1_t798349321 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1537296273_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t798349321 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t798349321 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1537296273(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3872827350_gshared (InternalEnumerator_1_t798349321 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3872827350_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t798349321 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t798349321 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3872827350(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1405735267_MetadataUsageId;
extern "C"  TagName_t2016006645  InternalEnumerator_1_get_Current_m1405735267_gshared (InternalEnumerator_1_t798349321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1405735267_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TagName_t2016006645  L_8 = ((  TagName_t2016006645  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TagName_t2016006645  InternalEnumerator_1_get_Current_m1405735267_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t798349321 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t798349321 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1405735267(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<MScrollView/MoveWay>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m284719091_gshared (InternalEnumerator_1_t2257284226 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m284719091_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2257284226 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2257284226 *>(__this + 1);
	InternalEnumerator_1__ctor_m284719091(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<MScrollView/MoveWay>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3785022157_gshared (InternalEnumerator_1_t2257284226 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3785022157_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2257284226 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2257284226 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3785022157(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<MScrollView/MoveWay>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m179346691_gshared (InternalEnumerator_1_t2257284226 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t2257284226 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m179346691_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2257284226 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2257284226 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m179346691(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<MScrollView/MoveWay>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1643824522_gshared (InternalEnumerator_1_t2257284226 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1643824522_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2257284226 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2257284226 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1643824522(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<MScrollView/MoveWay>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2345966589_gshared (InternalEnumerator_1_t2257284226 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2345966589_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2257284226 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2257284226 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2345966589(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<MScrollView/MoveWay>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3025179740_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m3025179740_gshared (InternalEnumerator_1_t2257284226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3025179740_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3025179740_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2257284226 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2257284226 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3025179740(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonWriter/State>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2097196035_gshared (InternalEnumerator_1_t3749301894 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2097196035_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3749301894 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3749301894 *>(__this + 1);
	InternalEnumerator_1__ctor_m2097196035(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonWriter/State>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m37457085_gshared (InternalEnumerator_1_t3749301894 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m37457085_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3749301894 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3749301894 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m37457085(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonWriter/State>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1301492009_gshared (InternalEnumerator_1_t3749301894 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t3749301894 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1301492009_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3749301894 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3749301894 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1301492009(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonWriter/State>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m249679258_gshared (InternalEnumerator_1_t3749301894 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m249679258_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3749301894 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3749301894 *>(__this + 1);
	InternalEnumerator_1_Dispose_m249679258(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonWriter/State>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1045138473_gshared (InternalEnumerator_1_t3749301894 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1045138473_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3749301894 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3749301894 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1045138473(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonWriter/State>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m904353994_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m904353994_gshared (InternalEnumerator_1_t3749301894 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m904353994_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m904353994_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3749301894 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3749301894 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m904353994(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Linq.JTokenType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2748237735_gshared (InternalEnumerator_1_t2699240237 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2748237735_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2699240237 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2699240237 *>(__this + 1);
	InternalEnumerator_1__ctor_m2748237735(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m301393561_gshared (InternalEnumerator_1_t2699240237 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m301393561_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2699240237 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2699240237 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m301393561(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4034811983_gshared (InternalEnumerator_1_t2699240237 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t2699240237 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4034811983_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2699240237 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2699240237 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4034811983(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Linq.JTokenType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4165690942_gshared (InternalEnumerator_1_t2699240237 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4165690942_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2699240237 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2699240237 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4165690942(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.Linq.JTokenType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3748683465_gshared (InternalEnumerator_1_t2699240237 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3748683465_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2699240237 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2699240237 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3748683465(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.Linq.JTokenType>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1002099088_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m1002099088_gshared (InternalEnumerator_1_t2699240237 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1002099088_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m1002099088_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2699240237 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2699240237 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1002099088(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2818374752_gshared (InternalEnumerator_1_t897758497 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2818374752_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t897758497 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897758497 *>(__this + 1);
	InternalEnumerator_1__ctor_m2818374752(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1451760960_gshared (InternalEnumerator_1_t897758497 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1451760960_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t897758497 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897758497 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1451760960(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2522270198_gshared (InternalEnumerator_1_t897758497 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t897758497 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2522270198_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t897758497 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897758497 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2522270198(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Schema.JsonSchemaType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1647869495_gshared (InternalEnumerator_1_t897758497 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1647869495_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t897758497 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897758497 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1647869495(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.Schema.JsonSchemaType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3126467696_gshared (InternalEnumerator_1_t897758497 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3126467696_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t897758497 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897758497 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3126467696(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.Schema.JsonSchemaType>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1210493001_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m1210493001_gshared (InternalEnumerator_1_t897758497 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1210493001_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m1210493001_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t897758497 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897758497 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1210493001(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1440454536_gshared (InternalEnumerator_1_t1754187467 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1440454536_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1754187467 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1754187467 *>(__this + 1);
	InternalEnumerator_1__ctor_m1440454536(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2650743064_gshared (InternalEnumerator_1_t1754187467 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2650743064_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1754187467 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1754187467 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2650743064(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20732868_gshared (InternalEnumerator_1_t1754187467 * __this, const MethodInfo* method)
{
	{
		TypeNameKey_t2971844791  L_0 = ((  TypeNameKey_t2971844791  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t1754187467 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TypeNameKey_t2971844791  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20732868_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1754187467 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1754187467 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20732868(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2321509215_gshared (InternalEnumerator_1_t1754187467 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2321509215_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1754187467 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1754187467 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2321509215(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3367673860_gshared (InternalEnumerator_1_t1754187467 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3367673860_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1754187467 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1754187467 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3367673860(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3704502415_MetadataUsageId;
extern "C"  TypeNameKey_t2971844791  InternalEnumerator_1_get_Current_m3704502415_gshared (InternalEnumerator_1_t1754187467 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3704502415_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TypeNameKey_t2971844791  L_8 = ((  TypeNameKey_t2971844791  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TypeNameKey_t2971844791  InternalEnumerator_1_get_Current_m3704502415_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1754187467 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1754187467 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3704502415(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m589683351_gshared (InternalEnumerator_1_t1674672166 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m589683351_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1674672166 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1674672166 *>(__this + 1);
	InternalEnumerator_1__ctor_m589683351(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2517339561_gshared (InternalEnumerator_1_t1674672166 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2517339561_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1674672166 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1674672166 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2517339561(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2188531679_gshared (InternalEnumerator_1_t1674672166 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t1674672166 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2188531679_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1674672166 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1674672166 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2188531679(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2699060526_gshared (InternalEnumerator_1_t1674672166 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2699060526_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1674672166 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1674672166 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2699060526(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m655721177_gshared (InternalEnumerator_1_t1674672166 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m655721177_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1674672166 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1674672166 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m655721177(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4237519616_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m4237519616_gshared (InternalEnumerator_1_t1674672166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4237519616_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m4237519616_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1674672166 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1674672166 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4237519616(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m447238051_gshared (InternalEnumerator_1_t3943444146 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m447238051_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3943444146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3943444146 *>(__this + 1);
	InternalEnumerator_1__ctor_m447238051(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3990422813_gshared (InternalEnumerator_1_t3943444146 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3990422813_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3943444146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3943444146 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3990422813(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m966888979_gshared (InternalEnumerator_1_t3943444146 * __this, const MethodInfo* method)
{
	{
		TypeConvertKey_t866134174  L_0 = ((  TypeConvertKey_t866134174  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t3943444146 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TypeConvertKey_t866134174  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m966888979_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3943444146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3943444146 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m966888979(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2608036154_gshared (InternalEnumerator_1_t3943444146 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2608036154_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3943444146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3943444146 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2608036154(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2535341517_gshared (InternalEnumerator_1_t3943444146 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2535341517_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3943444146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3943444146 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2535341517(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m624396236_MetadataUsageId;
extern "C"  TypeConvertKey_t866134174  InternalEnumerator_1_get_Current_m624396236_gshared (InternalEnumerator_1_t3943444146 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m624396236_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TypeConvertKey_t866134174  L_8 = ((  TypeConvertKey_t866134174  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TypeConvertKey_t866134174  InternalEnumerator_1_get_Current_m624396236_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3943444146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3943444146 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m624396236(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.AdvancedSmooth/Turn>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1425305847_gshared (InternalEnumerator_1_t3542505350 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1425305847_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3542505350 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3542505350 *>(__this + 1);
	InternalEnumerator_1__ctor_m1425305847(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4123630409_gshared (InternalEnumerator_1_t3542505350 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4123630409_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3542505350 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3542505350 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4123630409(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1858259199_gshared (InternalEnumerator_1_t3542505350 * __this, const MethodInfo* method)
{
	{
		Turn_t465195378  L_0 = ((  Turn_t465195378  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t3542505350 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Turn_t465195378  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1858259199_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3542505350 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3542505350 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1858259199(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.AdvancedSmooth/Turn>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2640671630_gshared (InternalEnumerator_1_t3542505350 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2640671630_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3542505350 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3542505350 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2640671630(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.AdvancedSmooth/Turn>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3717725049_gshared (InternalEnumerator_1_t3542505350 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3717725049_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3542505350 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3542505350 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3717725049(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Pathfinding.AdvancedSmooth/Turn>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2137896672_MetadataUsageId;
extern "C"  Turn_t465195378  InternalEnumerator_1_get_Current_m2137896672_gshared (InternalEnumerator_1_t3542505350 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2137896672_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Turn_t465195378  L_8 = ((  Turn_t465195378  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Turn_t465195378  InternalEnumerator_1_get_Current_m2137896672_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3542505350 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3542505350 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2137896672(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.BBTree/BBTreeBox>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2108631223_gshared (InternalEnumerator_1_t740738874 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2108631223_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t740738874 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t740738874 *>(__this + 1);
	InternalEnumerator_1__ctor_m2108631223(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.BBTree/BBTreeBox>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4167977865_gshared (InternalEnumerator_1_t740738874 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4167977865_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t740738874 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t740738874 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4167977865(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Pathfinding.BBTree/BBTreeBox>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1506178165_gshared (InternalEnumerator_1_t740738874 * __this, const MethodInfo* method)
{
	{
		BBTreeBox_t1958396198  L_0 = ((  BBTreeBox_t1958396198  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t740738874 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		BBTreeBox_t1958396198  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1506178165_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t740738874 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t740738874 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1506178165(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.BBTree/BBTreeBox>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1844370254_gshared (InternalEnumerator_1_t740738874 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1844370254_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t740738874 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t740738874 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1844370254(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.BBTree/BBTreeBox>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3727705077_gshared (InternalEnumerator_1_t740738874 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3727705077_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t740738874 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t740738874 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3727705077(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Pathfinding.BBTree/BBTreeBox>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m132170750_MetadataUsageId;
extern "C"  BBTreeBox_t1958396198  InternalEnumerator_1_get_Current_m132170750_gshared (InternalEnumerator_1_t740738874 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m132170750_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		BBTreeBox_t1958396198  L_8 = ((  BBTreeBox_t1958396198  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  BBTreeBox_t1958396198  InternalEnumerator_1_get_Current_m132170750_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t740738874 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t740738874 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m132170750(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.BinaryHeapM/Tuple>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m503891860_gshared (InternalEnumerator_1_t1298039651 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m503891860_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1298039651 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1298039651 *>(__this + 1);
	InternalEnumerator_1__ctor_m503891860(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.BinaryHeapM/Tuple>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3095002508_gshared (InternalEnumerator_1_t1298039651 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3095002508_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1298039651 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1298039651 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3095002508(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Pathfinding.BinaryHeapM/Tuple>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2396571202_gshared (InternalEnumerator_1_t1298039651 * __this, const MethodInfo* method)
{
	{
		Tuple_t2515696975  L_0 = ((  Tuple_t2515696975  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t1298039651 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Tuple_t2515696975  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2396571202_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1298039651 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1298039651 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2396571202(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.BinaryHeapM/Tuple>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3451583083_gshared (InternalEnumerator_1_t1298039651 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3451583083_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1298039651 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1298039651 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3451583083(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.BinaryHeapM/Tuple>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2570733756_gshared (InternalEnumerator_1_t1298039651 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2570733756_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1298039651 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1298039651 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2570733756(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Pathfinding.BinaryHeapM/Tuple>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3050888061_MetadataUsageId;
extern "C"  Tuple_t2515696975  InternalEnumerator_1_get_Current_m3050888061_gshared (InternalEnumerator_1_t1298039651 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3050888061_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Tuple_t2515696975  L_8 = ((  Tuple_t2515696975  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Tuple_t2515696975  InternalEnumerator_1_get_Current_m3050888061_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1298039651 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1298039651 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3050888061(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.ClipperLib.IntPoint>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1135288466_gshared (InternalEnumerator_1_t2108468855 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1135288466_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2108468855 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2108468855 *>(__this + 1);
	InternalEnumerator_1__ctor_m1135288466(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2413516750_gshared (InternalEnumerator_1_t2108468855 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2413516750_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2108468855 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2108468855 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2413516750(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m120868292_gshared (InternalEnumerator_1_t2108468855 * __this, const MethodInfo* method)
{
	{
		IntPoint_t3326126179  L_0 = ((  IntPoint_t3326126179  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t2108468855 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IntPoint_t3326126179  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m120868292_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2108468855 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2108468855 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m120868292(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.ClipperLib.IntPoint>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3465205993_gshared (InternalEnumerator_1_t2108468855 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3465205993_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2108468855 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2108468855 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3465205993(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.ClipperLib.IntPoint>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3508486526_gshared (InternalEnumerator_1_t2108468855 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3508486526_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2108468855 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2108468855 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3508486526(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Pathfinding.ClipperLib.IntPoint>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m710604475_MetadataUsageId;
extern "C"  IntPoint_t3326126179  InternalEnumerator_1_get_Current_m710604475_gshared (InternalEnumerator_1_t2108468855 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m710604475_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		IntPoint_t3326126179  L_8 = ((  IntPoint_t3326126179  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  IntPoint_t3326126179  InternalEnumerator_1_get_Current_m710604475_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2108468855 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2108468855 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m710604475(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.GridGraph/TextureData/ChannelUse>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3080050416_gshared (InternalEnumerator_1_t2017522227 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3080050416_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2017522227 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2017522227 *>(__this + 1);
	InternalEnumerator_1__ctor_m3080050416(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.GridGraph/TextureData/ChannelUse>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1267046576_gshared (InternalEnumerator_1_t2017522227 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1267046576_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2017522227 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2017522227 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1267046576(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Pathfinding.GridGraph/TextureData/ChannelUse>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1855214172_gshared (InternalEnumerator_1_t2017522227 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t2017522227 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1855214172_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2017522227 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2017522227 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1855214172(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.GridGraph/TextureData/ChannelUse>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2039893191_gshared (InternalEnumerator_1_t2017522227 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2039893191_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2017522227 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2017522227 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2039893191(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.GridGraph/TextureData/ChannelUse>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1056163228_gshared (InternalEnumerator_1_t2017522227 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1056163228_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2017522227 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2017522227 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1056163228(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Pathfinding.GridGraph/TextureData/ChannelUse>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4262851319_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m4262851319_gshared (InternalEnumerator_1_t2017522227 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4262851319_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m4262851319_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2017522227 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2017522227 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4262851319(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.Int2>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m763635614_gshared (InternalEnumerator_1_t756388269 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m763635614_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t756388269 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t756388269 *>(__this + 1);
	InternalEnumerator_1__ctor_m763635614(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.Int2>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m97844546_gshared (InternalEnumerator_1_t756388269 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m97844546_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t756388269 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t756388269 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m97844546(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Pathfinding.Int2>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1740972014_gshared (InternalEnumerator_1_t756388269 * __this, const MethodInfo* method)
{
	{
		Int2_t1974045593  L_0 = ((  Int2_t1974045593  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t756388269 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Int2_t1974045593  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1740972014_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t756388269 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t756388269 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1740972014(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.Int2>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m690159349_gshared (InternalEnumerator_1_t756388269 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m690159349_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t756388269 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t756388269 *>(__this + 1);
	InternalEnumerator_1_Dispose_m690159349(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.Int2>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2460860974_gshared (InternalEnumerator_1_t756388269 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2460860974_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t756388269 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t756388269 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2460860974(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Pathfinding.Int2>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2466398885_MetadataUsageId;
extern "C"  Int2_t1974045593  InternalEnumerator_1_get_Current_m2466398885_gshared (InternalEnumerator_1_t756388269 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2466398885_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Int2_t1974045593  L_8 = ((  Int2_t1974045593  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Int2_t1974045593  InternalEnumerator_1_get_Current_m2466398885_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t756388269 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t756388269 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2466398885(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.Int3>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2008400095_gshared (InternalEnumerator_1_t756388270 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2008400095_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t756388270 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t756388270 *>(__this + 1);
	InternalEnumerator_1__ctor_m2008400095(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.Int3>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m221917793_gshared (InternalEnumerator_1_t756388270 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m221917793_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t756388270 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t756388270 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m221917793(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Pathfinding.Int3>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2710553037_gshared (InternalEnumerator_1_t756388270 * __this, const MethodInfo* method)
{
	{
		Int3_t1974045594  L_0 = ((  Int3_t1974045594  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t756388270 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Int3_t1974045594  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2710553037_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t756388270 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t756388270 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2710553037(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.Int3>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m396756342_gshared (InternalEnumerator_1_t756388270 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m396756342_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t756388270 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t756388270 *>(__this + 1);
	InternalEnumerator_1_Dispose_m396756342(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.Int3>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1955302349_gshared (InternalEnumerator_1_t756388270 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1955302349_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t756388270 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t756388270 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1955302349(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Pathfinding.Int3>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3819708582_MetadataUsageId;
extern "C"  Int3_t1974045594  InternalEnumerator_1_get_Current_m3819708582_gshared (InternalEnumerator_1_t756388270 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3819708582_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Int3_t1974045594  L_8 = ((  Int3_t1974045594  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Int3_t1974045594  InternalEnumerator_1_get_Current_m3819708582_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t756388270 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t756388270 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3819708582(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.IntRect>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m745607502_gshared (InternalEnumerator_1_t1797400937 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m745607502_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1797400937 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1797400937 *>(__this + 1);
	InternalEnumerator_1__ctor_m745607502(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.IntRect>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1925764498_gshared (InternalEnumerator_1_t1797400937 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1925764498_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1797400937 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1797400937 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1925764498(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Pathfinding.IntRect>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3508973576_gshared (InternalEnumerator_1_t1797400937 * __this, const MethodInfo* method)
{
	{
		IntRect_t3015058261  L_0 = ((  IntRect_t3015058261  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t1797400937 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IntRect_t3015058261  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3508973576_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1797400937 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1797400937 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3508973576(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.IntRect>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2074559653_gshared (InternalEnumerator_1_t1797400937 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2074559653_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1797400937 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1797400937 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2074559653(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.IntRect>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2813853762_gshared (InternalEnumerator_1_t1797400937 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2813853762_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1797400937 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1797400937 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2813853762(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Pathfinding.IntRect>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m388107767_MetadataUsageId;
extern "C"  IntRect_t3015058261  InternalEnumerator_1_get_Current_m388107767_gshared (InternalEnumerator_1_t1797400937 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m388107767_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		IntRect_t3015058261  L_8 = ((  IntRect_t3015058261  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  IntRect_t3015058261  InternalEnumerator_1_get_Current_m388107767_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1797400937 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1797400937 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m388107767(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.LocalAvoidance/IntersectionPair>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2877098420_gshared (InternalEnumerator_1_t1603662595 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2877098420_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1603662595 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1603662595 *>(__this + 1);
	InternalEnumerator_1__ctor_m2877098420(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3691333484_gshared (InternalEnumerator_1_t1603662595 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3691333484_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1603662595 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1603662595 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3691333484(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4289038178_gshared (InternalEnumerator_1_t1603662595 * __this, const MethodInfo* method)
{
	{
		IntersectionPair_t2821319919  L_0 = ((  IntersectionPair_t2821319919  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((InternalEnumerator_1_t1603662595 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IntersectionPair_t2821319919  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4289038178_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1603662595 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1603662595 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4289038178(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Pathfinding.LocalAvoidance/IntersectionPair>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4111819915_gshared (InternalEnumerator_1_t1603662595 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4111819915_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1603662595 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1603662595 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4111819915(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.LocalAvoidance/IntersectionPair>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m11728668_gshared (InternalEnumerator_1_t1603662595 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m11728668_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1603662595 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1603662595 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m11728668(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Pathfinding.LocalAvoidance/IntersectionPair>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m916862941_MetadataUsageId;
extern "C"  IntersectionPair_t2821319919  InternalEnumerator_1_get_Current_m916862941_gshared (InternalEnumerator_1_t1603662595 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m916862941_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		IntersectionPair_t2821319919  L_8 = ((  IntersectionPair_t2821319919  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  IntersectionPair_t2821319919  InternalEnumerator_1_get_Current_m916862941_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1603662595 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1603662595 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m916862941(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
