﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginYiwan
struct  PluginYiwan_t2559472193  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginYiwan::uid
	String_t* ___uid_2;
	// System.String PluginYiwan::token
	String_t* ___token_3;

public:
	inline static int32_t get_offset_of_uid_2() { return static_cast<int32_t>(offsetof(PluginYiwan_t2559472193, ___uid_2)); }
	inline String_t* get_uid_2() const { return ___uid_2; }
	inline String_t** get_address_of_uid_2() { return &___uid_2; }
	inline void set_uid_2(String_t* value)
	{
		___uid_2 = value;
		Il2CppCodeGenWriteBarrier(&___uid_2, value);
	}

	inline static int32_t get_offset_of_token_3() { return static_cast<int32_t>(offsetof(PluginYiwan_t2559472193, ___token_3)); }
	inline String_t* get_token_3() const { return ___token_3; }
	inline String_t** get_address_of_token_3() { return &___token_3; }
	inline void set_token_3(String_t* value)
	{
		___token_3 = value;
		Il2CppCodeGenWriteBarrier(&___token_3, value);
	}
};

struct PluginYiwan_t2559472193_StaticFields
{
public:
	// System.Action PluginYiwan::<>f__am$cache2
	Action_t3771233898 * ___U3CU3Ef__amU24cache2_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_4() { return static_cast<int32_t>(offsetof(PluginYiwan_t2559472193_StaticFields, ___U3CU3Ef__amU24cache2_4)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache2_4() const { return ___U3CU3Ef__amU24cache2_4; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache2_4() { return &___U3CU3Ef__amU24cache2_4; }
	inline void set_U3CU3Ef__amU24cache2_4(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
