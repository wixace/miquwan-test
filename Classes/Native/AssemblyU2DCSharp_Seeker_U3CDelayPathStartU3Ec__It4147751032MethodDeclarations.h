﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Seeker/<DelayPathStart>c__Iterator7
struct U3CDelayPathStartU3Ec__Iterator7_t4147751032;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Seeker/<DelayPathStart>c__Iterator7::.ctor()
extern "C"  void U3CDelayPathStartU3Ec__Iterator7__ctor_m1431020019 (U3CDelayPathStartU3Ec__Iterator7_t4147751032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Seeker/<DelayPathStart>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayPathStartU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2042525119 (U3CDelayPathStartU3Ec__Iterator7_t4147751032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Seeker/<DelayPathStart>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayPathStartU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m2486818643 (U3CDelayPathStartU3Ec__Iterator7_t4147751032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Seeker/<DelayPathStart>c__Iterator7::MoveNext()
extern "C"  bool U3CDelayPathStartU3Ec__Iterator7_MoveNext_m4270460193 (U3CDelayPathStartU3Ec__Iterator7_t4147751032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Seeker/<DelayPathStart>c__Iterator7::Dispose()
extern "C"  void U3CDelayPathStartU3Ec__Iterator7_Dispose_m718986608 (U3CDelayPathStartU3Ec__Iterator7_t4147751032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Seeker/<DelayPathStart>c__Iterator7::Reset()
extern "C"  void U3CDelayPathStartU3Ec__Iterator7_Reset_m3372420256 (U3CDelayPathStartU3Ec__Iterator7_t4147751032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
