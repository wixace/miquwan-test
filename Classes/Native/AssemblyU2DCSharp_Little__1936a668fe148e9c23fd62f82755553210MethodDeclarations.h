﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._1936a668fe148e9c23fd62f8dec3fbd0
struct _1936a668fe148e9c23fd62f8dec3fbd0_t2755553210;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._1936a668fe148e9c23fd62f8dec3fbd0::.ctor()
extern "C"  void _1936a668fe148e9c23fd62f8dec3fbd0__ctor_m119852979 (_1936a668fe148e9c23fd62f8dec3fbd0_t2755553210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1936a668fe148e9c23fd62f8dec3fbd0::_1936a668fe148e9c23fd62f8dec3fbd0m2(System.Int32)
extern "C"  int32_t _1936a668fe148e9c23fd62f8dec3fbd0__1936a668fe148e9c23fd62f8dec3fbd0m2_m3103336281 (_1936a668fe148e9c23fd62f8dec3fbd0_t2755553210 * __this, int32_t ____1936a668fe148e9c23fd62f8dec3fbd0a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1936a668fe148e9c23fd62f8dec3fbd0::_1936a668fe148e9c23fd62f8dec3fbd0m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _1936a668fe148e9c23fd62f8dec3fbd0__1936a668fe148e9c23fd62f8dec3fbd0m_m626751357 (_1936a668fe148e9c23fd62f8dec3fbd0_t2755553210 * __this, int32_t ____1936a668fe148e9c23fd62f8dec3fbd0a0, int32_t ____1936a668fe148e9c23fd62f8dec3fbd091, int32_t ____1936a668fe148e9c23fd62f8dec3fbd0c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
