﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ExitGUIExceptionGenerated
struct UnityEngine_ExitGUIExceptionGenerated_t1270961077;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_ExitGUIExceptionGenerated::.ctor()
extern "C"  void UnityEngine_ExitGUIExceptionGenerated__ctor_m460403030 (UnityEngine_ExitGUIExceptionGenerated_t1270961077 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ExitGUIExceptionGenerated::ExitGUIException_ExitGUIException1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ExitGUIExceptionGenerated_ExitGUIException_ExitGUIException1_m1375130780 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ExitGUIExceptionGenerated::__Register()
extern "C"  void UnityEngine_ExitGUIExceptionGenerated___Register_m4178030897 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ExitGUIExceptionGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_ExitGUIExceptionGenerated_ilo_getObject1_m2623992096 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
