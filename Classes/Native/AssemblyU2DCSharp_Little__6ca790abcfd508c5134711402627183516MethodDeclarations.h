﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._6ca790abcfd508c5134711408a64b323
struct _6ca790abcfd508c5134711408a64b323_t2627183516;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._6ca790abcfd508c5134711408a64b323::.ctor()
extern "C"  void _6ca790abcfd508c5134711408a64b323__ctor_m4174631441 (_6ca790abcfd508c5134711408a64b323_t2627183516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6ca790abcfd508c5134711408a64b323::_6ca790abcfd508c5134711408a64b323m2(System.Int32)
extern "C"  int32_t _6ca790abcfd508c5134711408a64b323__6ca790abcfd508c5134711408a64b323m2_m1397066777 (_6ca790abcfd508c5134711408a64b323_t2627183516 * __this, int32_t ____6ca790abcfd508c5134711408a64b323a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6ca790abcfd508c5134711408a64b323::_6ca790abcfd508c5134711408a64b323m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _6ca790abcfd508c5134711408a64b323__6ca790abcfd508c5134711408a64b323m_m3221613245 (_6ca790abcfd508c5134711408a64b323_t2627183516 * __this, int32_t ____6ca790abcfd508c5134711408a64b323a0, int32_t ____6ca790abcfd508c5134711408a64b323681, int32_t ____6ca790abcfd508c5134711408a64b323c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
