﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.TypeExtensions/<GetGenericMethod>c__AnonStorey13F
struct U3CGetGenericMethodU3Ec__AnonStorey13F_t190219077;
// System.Reflection.MethodInfo
struct MethodInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"

// System.Void Newtonsoft.Json.Utilities.TypeExtensions/<GetGenericMethod>c__AnonStorey13F::.ctor()
extern "C"  void U3CGetGenericMethodU3Ec__AnonStorey13F__ctor_m2318964166 (U3CGetGenericMethodU3Ec__AnonStorey13F_t190219077 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.TypeExtensions/<GetGenericMethod>c__AnonStorey13F::<>m__39E(System.Reflection.MethodInfo)
extern "C"  bool U3CGetGenericMethodU3Ec__AnonStorey13F_U3CU3Em__39E_m160094923 (U3CGetGenericMethodU3Ec__AnonStorey13F_t190219077 * __this, MethodInfo_t * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
