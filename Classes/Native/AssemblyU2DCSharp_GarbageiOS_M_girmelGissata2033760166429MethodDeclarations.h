﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_girmelGissata203
struct M_girmelGissata203_t3760166429;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_girmelGissata203::.ctor()
extern "C"  void M_girmelGissata203__ctor_m4074731702 (M_girmelGissata203_t3760166429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_girmelGissata203::M_neewhutasTaxaistor0(System.String[],System.Int32)
extern "C"  void M_girmelGissata203_M_neewhutasTaxaistor0_m4195623938 (M_girmelGissata203_t3760166429 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_girmelGissata203::M_bapoCallsibi1(System.String[],System.Int32)
extern "C"  void M_girmelGissata203_M_bapoCallsibi1_m3005970097 (M_girmelGissata203_t3760166429 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_girmelGissata203::M_bemfurta2(System.String[],System.Int32)
extern "C"  void M_girmelGissata203_M_bemfurta2_m2295661061 (M_girmelGissata203_t3760166429 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_girmelGissata203::M_lercairdre3(System.String[],System.Int32)
extern "C"  void M_girmelGissata203_M_lercairdre3_m287830741 (M_girmelGissata203_t3760166429 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_girmelGissata203::M_nearfi4(System.String[],System.Int32)
extern "C"  void M_girmelGissata203_M_nearfi4_m1830949922 (M_girmelGissata203_t3760166429 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_girmelGissata203::M_sekurhorJesowsi5(System.String[],System.Int32)
extern "C"  void M_girmelGissata203_M_sekurhorJesowsi5_m109064555 (M_girmelGissata203_t3760166429 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
