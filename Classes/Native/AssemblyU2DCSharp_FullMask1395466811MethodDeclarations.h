﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FullMask
struct FullMask_t1395466811;
// CEvent.ZEvent
struct ZEvent_t3638018500;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"

// System.Void FullMask::.ctor()
extern "C"  void FullMask__ctor_m99020480 (FullMask_t1395466811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FullMask::Awake()
extern "C"  void FullMask_Awake_m336625699 (FullMask_t1395466811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FullMask::Full()
extern "C"  void FullMask_Full_m2092147027 (FullMask_t1395466811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FullMask::OnDestroy()
extern "C"  void FullMask_OnDestroy_m1461937081 (FullMask_t1395466811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FullMask::OnScreenSizeChanged(CEvent.ZEvent)
extern "C"  void FullMask_OnScreenSizeChanged_m1680510933 (FullMask_t1395466811 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FullMask::Start()
extern "C"  void FullMask_Start_m3341125568 (FullMask_t1395466811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
