﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_traisawhelHawsi50
struct M_traisawhelHawsi50_t2612772739;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_traisawhelHawsi502612772739.h"

// System.Void GarbageiOS.M_traisawhelHawsi50::.ctor()
extern "C"  void M_traisawhelHawsi50__ctor_m673418560 (M_traisawhelHawsi50_t2612772739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_traisawhelHawsi50::M_zayee0(System.String[],System.Int32)
extern "C"  void M_traisawhelHawsi50_M_zayee0_m2768892907 (M_traisawhelHawsi50_t2612772739 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_traisawhelHawsi50::M_louvallKoota1(System.String[],System.Int32)
extern "C"  void M_traisawhelHawsi50_M_louvallKoota1_m3956784921 (M_traisawhelHawsi50_t2612772739 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_traisawhelHawsi50::M_cissozou2(System.String[],System.Int32)
extern "C"  void M_traisawhelHawsi50_M_cissozou2_m651404158 (M_traisawhelHawsi50_t2612772739 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_traisawhelHawsi50::M_rarevou3(System.String[],System.Int32)
extern "C"  void M_traisawhelHawsi50_M_rarevou3_m1043981254 (M_traisawhelHawsi50_t2612772739 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_traisawhelHawsi50::ilo_M_cissozou21(GarbageiOS.M_traisawhelHawsi50,System.String[],System.Int32)
extern "C"  void M_traisawhelHawsi50_ilo_M_cissozou21_m4140900537 (Il2CppObject * __this /* static, unused */, M_traisawhelHawsi50_t2612772739 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
