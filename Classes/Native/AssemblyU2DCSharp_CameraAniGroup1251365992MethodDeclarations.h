﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraAniGroup
struct CameraAniGroup_t1251365992;
// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "codegen/il2cpp-codegen.h"

// System.Void CameraAniGroup::.ctor()
extern "C"  void CameraAniGroup__ctor_m4214484275 (CameraAniGroup_t1251365992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension CameraAniGroup::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * CameraAniGroup_ProtoBuf_IExtensible_GetExtensionObject_m1737745027 (CameraAniGroup_t1251365992 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraAniGroup::get_startID()
extern "C"  int32_t CameraAniGroup_get_startID_m2135306039 (CameraAniGroup_t1251365992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraAniGroup::set_startID(System.Int32)
extern "C"  void CameraAniGroup_set_startID_m1132159074 (CameraAniGroup_t1251365992 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraAniGroup::get_eventType()
extern "C"  int32_t CameraAniGroup_get_eventType_m1374105390 (CameraAniGroup_t1251365992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraAniGroup::set_eventType(System.Int32)
extern "C"  void CameraAniGroup_set_eventType_m1953822553 (CameraAniGroup_t1251365992 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraAniGroup::get_round()
extern "C"  int32_t CameraAniGroup_get_round_m3955264968 (CameraAniGroup_t1251365992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraAniGroup::set_round(System.Int32)
extern "C"  void CameraAniGroup_set_round_m760560627 (CameraAniGroup_t1251365992 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraAniGroup::get_id()
extern "C"  int32_t CameraAniGroup_get_id_m3778402115 (CameraAniGroup_t1251365992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraAniGroup::set_id(System.Int32)
extern "C"  void CameraAniGroup_set_id_m3939637626 (CameraAniGroup_t1251365992 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension CameraAniGroup::ilo_GetExtensionObject1(ProtoBuf.IExtension&,System.Boolean)
extern "C"  Il2CppObject * CameraAniGroup_ilo_GetExtensionObject1_m3917488733 (Il2CppObject * __this /* static, unused */, Il2CppObject ** ___extensionObject0, bool ___createIfMissing1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
