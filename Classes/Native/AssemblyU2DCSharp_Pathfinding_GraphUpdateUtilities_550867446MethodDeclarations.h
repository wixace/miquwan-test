﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.GraphUpdateUtilities/<UpdateGraphsNoBlock>c__AnonStorey124
struct U3CUpdateGraphsNoBlockU3Ec__AnonStorey124_t550867446;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.GraphUpdateUtilities/<UpdateGraphsNoBlock>c__AnonStorey124::.ctor()
extern "C"  void U3CUpdateGraphsNoBlockU3Ec__AnonStorey124__ctor_m2546657125 (U3CUpdateGraphsNoBlockU3Ec__AnonStorey124_t550867446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateUtilities/<UpdateGraphsNoBlock>c__AnonStorey124::<>m__35A()
extern "C"  void U3CUpdateGraphsNoBlockU3Ec__AnonStorey124_U3CU3Em__35A_m3991876883 (U3CUpdateGraphsNoBlockU3Ec__AnonStorey124_t550867446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
