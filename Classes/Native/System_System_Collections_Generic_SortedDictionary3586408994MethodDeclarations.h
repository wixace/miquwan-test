﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>
struct SortedDictionary_2_t3586408994;
// System.Collections.Generic.IComparer`1<System.Int32>
struct IComparer_1_t3728852542;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t2048428487;
// System.Object
struct Il2CppObject;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct IEnumerator_1_t2961747494;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
struct KeyValuePair_2U5BU5D_t1127117024;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21049882445.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"

// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::.ctor()
extern "C"  void SortedDictionary_2__ctor_m3699749762_gshared (SortedDictionary_2_t3586408994 * __this, const MethodInfo* method);
#define SortedDictionary_2__ctor_m3699749762(__this, method) ((  void (*) (SortedDictionary_2_t3586408994 *, const MethodInfo*))SortedDictionary_2__ctor_m3699749762_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.IComparer`1<TKey>)
extern "C"  void SortedDictionary_2__ctor_m159153619_gshared (SortedDictionary_2_t3586408994 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define SortedDictionary_2__ctor_m159153619(__this, ___comparer0, method) ((  void (*) (SortedDictionary_2_t3586408994 *, Il2CppObject*, const MethodInfo*))SortedDictionary_2__ctor_m159153619_gshared)(__this, ___comparer0, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1329930656_gshared (SortedDictionary_2_t3586408994 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1329930656(__this, method) ((  Il2CppObject* (*) (SortedDictionary_2_t3586408994 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1329930656_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m786026848_gshared (SortedDictionary_2_t3586408994 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m786026848(__this, method) ((  Il2CppObject* (*) (SortedDictionary_2_t3586408994 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m786026848_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1546103103_gshared (SortedDictionary_2_t3586408994 * __this, KeyValuePair_2_t1049882445  ___item0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1546103103(__this, ___item0, method) ((  void (*) (SortedDictionary_2_t3586408994 *, KeyValuePair_2_t1049882445 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1546103103_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m4019229263_gshared (SortedDictionary_2_t3586408994 * __this, KeyValuePair_2_t1049882445  ___item0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m4019229263(__this, ___item0, method) ((  bool (*) (SortedDictionary_2_t3586408994 *, KeyValuePair_2_t1049882445 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m4019229263_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1201146324_gshared (SortedDictionary_2_t3586408994 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1201146324(__this, method) ((  bool (*) (SortedDictionary_2_t3586408994 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1201146324_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1600979252_gshared (SortedDictionary_2_t3586408994 * __this, KeyValuePair_2_t1049882445  ___item0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1600979252(__this, ___item0, method) ((  bool (*) (SortedDictionary_2_t3586408994 *, KeyValuePair_2_t1049882445 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1600979252_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void SortedDictionary_2_System_Collections_IDictionary_Add_m3685868166_gshared (SortedDictionary_2_t3586408994 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_Add_m3685868166(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t3586408994 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Add_m3685868166_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool SortedDictionary_2_System_Collections_IDictionary_Contains_m1619529796_gshared (SortedDictionary_2_t3586408994 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_Contains_m1619529796(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t3586408994 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Contains_m1619529796_gshared)(__this, ___key0, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m3486921513_gshared (SortedDictionary_2_t3586408994 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m3486921513(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t3586408994 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m3486921513_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool SortedDictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2833400499_gshared (SortedDictionary_2_t3586408994 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2833400499(__this, method) ((  bool (*) (SortedDictionary_2_t3586408994 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2833400499_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool SortedDictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1238145510_gshared (SortedDictionary_2_t3586408994 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1238145510(__this, method) ((  bool (*) (SortedDictionary_2_t3586408994 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1238145510_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IDictionary_get_Keys_m2283559604_gshared (SortedDictionary_2_t3586408994 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_Keys_m2283559604(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t3586408994 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Keys_m2283559604_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void SortedDictionary_2_System_Collections_IDictionary_Remove_m2382587177_gshared (SortedDictionary_2_t3586408994 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_Remove_m2382587177(__this, ___key0, method) ((  void (*) (SortedDictionary_2_t3586408994 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Remove_m2382587177_gshared)(__this, ___key0, method)
// System.Collections.ICollection System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IDictionary_get_Values_m2140045986_gshared (SortedDictionary_2_t3586408994 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_Values_m2140045986(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t3586408994 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Values_m2140045986_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IDictionary_get_Item_m400609990_gshared (SortedDictionary_2_t3586408994 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_Item_m400609990(__this, ___key0, method) ((  Il2CppObject * (*) (SortedDictionary_2_t3586408994 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Item_m400609990_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void SortedDictionary_2_System_Collections_IDictionary_set_Item_m3257644267_gshared (SortedDictionary_2_t3586408994 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_set_Item_m3257644267(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t3586408994 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_set_Item_m3257644267_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void SortedDictionary_2_System_Collections_ICollection_CopyTo_m3388508162_gshared (SortedDictionary_2_t3586408994 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_ICollection_CopyTo_m3388508162(__this, ___array0, ___index1, method) ((  void (*) (SortedDictionary_2_t3586408994 *, Il2CppArray *, int32_t, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_CopyTo_m3388508162_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m353969552_gshared (SortedDictionary_2_t3586408994 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m353969552(__this, method) ((  bool (*) (SortedDictionary_2_t3586408994 *, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m353969552_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m778853904_gshared (SortedDictionary_2_t3586408994 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m778853904(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t3586408994 *, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m778853904_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m3741868305_gshared (SortedDictionary_2_t3586408994 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m3741868305(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t3586408994 *, const MethodInfo*))SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m3741868305_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2352960078_gshared (SortedDictionary_2_t3586408994 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2352960078(__this, method) ((  Il2CppObject* (*) (SortedDictionary_2_t3586408994 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2352960078_gshared)(__this, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::get_Count()
extern "C"  int32_t SortedDictionary_2_get_Count_m2841579606_gshared (SortedDictionary_2_t3586408994 * __this, const MethodInfo* method);
#define SortedDictionary_2_get_Count_m2841579606(__this, method) ((  int32_t (*) (SortedDictionary_2_t3586408994 *, const MethodInfo*))SortedDictionary_2_get_Count_m2841579606_gshared)(__this, method)
// TValue System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::get_Item(TKey)
extern "C"  int32_t SortedDictionary_2_get_Item_m370531029_gshared (SortedDictionary_2_t3586408994 * __this, int32_t ___key0, const MethodInfo* method);
#define SortedDictionary_2_get_Item_m370531029(__this, ___key0, method) ((  int32_t (*) (SortedDictionary_2_t3586408994 *, int32_t, const MethodInfo*))SortedDictionary_2_get_Item_m370531029_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::set_Item(TKey,TValue)
extern "C"  void SortedDictionary_2_set_Item_m4235238850_gshared (SortedDictionary_2_t3586408994 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define SortedDictionary_2_set_Item_m4235238850(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t3586408994 *, int32_t, int32_t, const MethodInfo*))SortedDictionary_2_set_Item_m4235238850_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::Add(TKey,TValue)
extern "C"  void SortedDictionary_2_Add_m1015436147_gshared (SortedDictionary_2_t3586408994 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define SortedDictionary_2_Add_m1015436147(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t3586408994 *, int32_t, int32_t, const MethodInfo*))SortedDictionary_2_Add_m1015436147_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::Clear()
extern "C"  void SortedDictionary_2_Clear_m1105883053_gshared (SortedDictionary_2_t3586408994 * __this, const MethodInfo* method);
#define SortedDictionary_2_Clear_m1105883053(__this, method) ((  void (*) (SortedDictionary_2_t3586408994 *, const MethodInfo*))SortedDictionary_2_Clear_m1105883053_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::ContainsKey(TKey)
extern "C"  bool SortedDictionary_2_ContainsKey_m4117945151_gshared (SortedDictionary_2_t3586408994 * __this, int32_t ___key0, const MethodInfo* method);
#define SortedDictionary_2_ContainsKey_m4117945151(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t3586408994 *, int32_t, const MethodInfo*))SortedDictionary_2_ContainsKey_m4117945151_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::ContainsValue(TValue)
extern "C"  bool SortedDictionary_2_ContainsValue_m4066408383_gshared (SortedDictionary_2_t3586408994 * __this, int32_t ___value0, const MethodInfo* method);
#define SortedDictionary_2_ContainsValue_m4066408383(__this, ___value0, method) ((  bool (*) (SortedDictionary_2_t3586408994 *, int32_t, const MethodInfo*))SortedDictionary_2_ContainsValue_m4066408383_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void SortedDictionary_2_CopyTo_m1255218294_gshared (SortedDictionary_2_t3586408994 * __this, KeyValuePair_2U5BU5D_t1127117024* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define SortedDictionary_2_CopyTo_m1255218294(__this, ___array0, ___arrayIndex1, method) ((  void (*) (SortedDictionary_2_t3586408994 *, KeyValuePair_2U5BU5D_t1127117024*, int32_t, const MethodInfo*))SortedDictionary_2_CopyTo_m1255218294_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::Remove(TKey)
extern "C"  bool SortedDictionary_2_Remove_m1565725265_gshared (SortedDictionary_2_t3586408994 * __this, int32_t ___key0, const MethodInfo* method);
#define SortedDictionary_2_Remove_m1565725265(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t3586408994 *, int32_t, const MethodInfo*))SortedDictionary_2_Remove_m1565725265_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::TryGetValue(TKey,TValue&)
extern "C"  bool SortedDictionary_2_TryGetValue_m2060158104_gshared (SortedDictionary_2_t3586408994 * __this, int32_t ___key0, int32_t* ___value1, const MethodInfo* method);
#define SortedDictionary_2_TryGetValue_m2060158104(__this, ___key0, ___value1, method) ((  bool (*) (SortedDictionary_2_t3586408994 *, int32_t, int32_t*, const MethodInfo*))SortedDictionary_2_TryGetValue_m2060158104_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::ToKey(System.Object)
extern "C"  int32_t SortedDictionary_2_ToKey_m1915736548_gshared (SortedDictionary_2_t3586408994 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_ToKey_m1915736548(__this, ___key0, method) ((  int32_t (*) (SortedDictionary_2_t3586408994 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ToKey_m1915736548_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::ToValue(System.Object)
extern "C"  int32_t SortedDictionary_2_ToValue_m318434624_gshared (SortedDictionary_2_t3586408994 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SortedDictionary_2_ToValue_m318434624(__this, ___value0, method) ((  int32_t (*) (SortedDictionary_2_t3586408994 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ToValue_m318434624_gshared)(__this, ___value0, method)
