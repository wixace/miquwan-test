﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator1F`2<System.Object,System.Object>
struct U3CConvertU3Ec__Iterator1F_2_t62520852;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator1F`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CConvertU3Ec__Iterator1F_2__ctor_m787375412_gshared (U3CConvertU3Ec__Iterator1F_2_t62520852 * __this, const MethodInfo* method);
#define U3CConvertU3Ec__Iterator1F_2__ctor_m787375412(__this, method) ((  void (*) (U3CConvertU3Ec__Iterator1F_2_t62520852 *, const MethodInfo*))U3CConvertU3Ec__Iterator1F_2__ctor_m787375412_gshared)(__this, method)
// U Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator1F`2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<U>.get_Current()
extern "C"  Il2CppObject * U3CConvertU3Ec__Iterator1F_2_System_Collections_Generic_IEnumeratorU3CUU3E_get_Current_m534188473_gshared (U3CConvertU3Ec__Iterator1F_2_t62520852 * __this, const MethodInfo* method);
#define U3CConvertU3Ec__Iterator1F_2_System_Collections_Generic_IEnumeratorU3CUU3E_get_Current_m534188473(__this, method) ((  Il2CppObject * (*) (U3CConvertU3Ec__Iterator1F_2_t62520852 *, const MethodInfo*))U3CConvertU3Ec__Iterator1F_2_System_Collections_Generic_IEnumeratorU3CUU3E_get_Current_m534188473_gshared)(__this, method)
// System.Object Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator1F`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CConvertU3Ec__Iterator1F_2_System_Collections_IEnumerator_get_Current_m3833248370_gshared (U3CConvertU3Ec__Iterator1F_2_t62520852 * __this, const MethodInfo* method);
#define U3CConvertU3Ec__Iterator1F_2_System_Collections_IEnumerator_get_Current_m3833248370(__this, method) ((  Il2CppObject * (*) (U3CConvertU3Ec__Iterator1F_2_t62520852 *, const MethodInfo*))U3CConvertU3Ec__Iterator1F_2_System_Collections_IEnumerator_get_Current_m3833248370_gshared)(__this, method)
// System.Collections.IEnumerator Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator1F`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CConvertU3Ec__Iterator1F_2_System_Collections_IEnumerable_GetEnumerator_m643997101_gshared (U3CConvertU3Ec__Iterator1F_2_t62520852 * __this, const MethodInfo* method);
#define U3CConvertU3Ec__Iterator1F_2_System_Collections_IEnumerable_GetEnumerator_m643997101(__this, method) ((  Il2CppObject * (*) (U3CConvertU3Ec__Iterator1F_2_t62520852 *, const MethodInfo*))U3CConvertU3Ec__Iterator1F_2_System_Collections_IEnumerable_GetEnumerator_m643997101_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<U> Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator1F`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<U>.GetEnumerator()
extern "C"  Il2CppObject* U3CConvertU3Ec__Iterator1F_2_System_Collections_Generic_IEnumerableU3CUU3E_GetEnumerator_m740641438_gshared (U3CConvertU3Ec__Iterator1F_2_t62520852 * __this, const MethodInfo* method);
#define U3CConvertU3Ec__Iterator1F_2_System_Collections_Generic_IEnumerableU3CUU3E_GetEnumerator_m740641438(__this, method) ((  Il2CppObject* (*) (U3CConvertU3Ec__Iterator1F_2_t62520852 *, const MethodInfo*))U3CConvertU3Ec__Iterator1F_2_System_Collections_Generic_IEnumerableU3CUU3E_GetEnumerator_m740641438_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator1F`2<System.Object,System.Object>::MoveNext()
extern "C"  bool U3CConvertU3Ec__Iterator1F_2_MoveNext_m2356176192_gshared (U3CConvertU3Ec__Iterator1F_2_t62520852 * __this, const MethodInfo* method);
#define U3CConvertU3Ec__Iterator1F_2_MoveNext_m2356176192(__this, method) ((  bool (*) (U3CConvertU3Ec__Iterator1F_2_t62520852 *, const MethodInfo*))U3CConvertU3Ec__Iterator1F_2_MoveNext_m2356176192_gshared)(__this, method)
// System.Void Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator1F`2<System.Object,System.Object>::Dispose()
extern "C"  void U3CConvertU3Ec__Iterator1F_2_Dispose_m651809905_gshared (U3CConvertU3Ec__Iterator1F_2_t62520852 * __this, const MethodInfo* method);
#define U3CConvertU3Ec__Iterator1F_2_Dispose_m651809905(__this, method) ((  void (*) (U3CConvertU3Ec__Iterator1F_2_t62520852 *, const MethodInfo*))U3CConvertU3Ec__Iterator1F_2_Dispose_m651809905_gshared)(__this, method)
// System.Void Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator1F`2<System.Object,System.Object>::Reset()
extern "C"  void U3CConvertU3Ec__Iterator1F_2_Reset_m2728775649_gshared (U3CConvertU3Ec__Iterator1F_2_t62520852 * __this, const MethodInfo* method);
#define U3CConvertU3Ec__Iterator1F_2_Reset_m2728775649(__this, method) ((  void (*) (U3CConvertU3Ec__Iterator1F_2_t62520852 *, const MethodInfo*))U3CConvertU3Ec__Iterator1F_2_Reset_m2728775649_gshared)(__this, method)
