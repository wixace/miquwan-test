﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.AstarSplines
struct AstarSplines_t4029212805;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void Pathfinding.AstarSplines::.ctor()
extern "C"  void AstarSplines__ctor_m2082857858 (AstarSplines_t4029212805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.AstarSplines::CatmullRom(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t4282066566  AstarSplines_CatmullRom_m1191721865 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___previous0, Vector3_t4282066566  ___start1, Vector3_t4282066566  ___end2, Vector3_t4282066566  ___next3, float ___elapsedTime4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.AstarSplines::CatmullRomOLD(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t4282066566  AstarSplines_CatmullRomOLD_m206574044 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___previous0, Vector3_t4282066566  ___start1, Vector3_t4282066566  ___end2, Vector3_t4282066566  ___next3, float ___elapsedTime4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
