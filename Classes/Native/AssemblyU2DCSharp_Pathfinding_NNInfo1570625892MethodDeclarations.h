﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.GraphNode
struct GraphNode_t23612370;
// Pathfinding.NNInfo
struct NNInfo_t1570625892;
struct NNInfo_t1570625892_marshaled_pinvoke;
struct NNInfo_t1570625892_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_NNInfo1570625892.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void Pathfinding.NNInfo::.ctor(Pathfinding.GraphNode)
extern "C"  void NNInfo__ctor_m399392585 (NNInfo_t1570625892 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NNInfo::SetConstrained(Pathfinding.GraphNode,UnityEngine.Vector3)
extern "C"  void NNInfo_SetConstrained_m1067764522 (NNInfo_t1570625892 * __this, GraphNode_t23612370 * ___constrainedNode0, Vector3_t4282066566  ___clampedPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NNInfo::UpdateInfo()
extern "C"  void NNInfo_UpdateInfo_m1006310584 (NNInfo_t1570625892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.NNInfo::op_Explicit(Pathfinding.NNInfo)
extern "C"  Vector3_t4282066566  NNInfo_op_Explicit_m1042667489 (Il2CppObject * __this /* static, unused */, NNInfo_t1570625892  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.GraphNode Pathfinding.NNInfo::op_Explicit(Pathfinding.NNInfo)
extern "C"  GraphNode_t23612370 * NNInfo_op_Explicit_m2783312494 (Il2CppObject * __this /* static, unused */, NNInfo_t1570625892  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.NNInfo::op_Explicit(Pathfinding.GraphNode)
extern "C"  NNInfo_t1570625892  NNInfo_op_Explicit_m1026387262 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct NNInfo_t1570625892;
struct NNInfo_t1570625892_marshaled_pinvoke;

extern "C" void NNInfo_t1570625892_marshal_pinvoke(const NNInfo_t1570625892& unmarshaled, NNInfo_t1570625892_marshaled_pinvoke& marshaled);
extern "C" void NNInfo_t1570625892_marshal_pinvoke_back(const NNInfo_t1570625892_marshaled_pinvoke& marshaled, NNInfo_t1570625892& unmarshaled);
extern "C" void NNInfo_t1570625892_marshal_pinvoke_cleanup(NNInfo_t1570625892_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct NNInfo_t1570625892;
struct NNInfo_t1570625892_marshaled_com;

extern "C" void NNInfo_t1570625892_marshal_com(const NNInfo_t1570625892& unmarshaled, NNInfo_t1570625892_marshaled_com& marshaled);
extern "C" void NNInfo_t1570625892_marshal_com_back(const NNInfo_t1570625892_marshaled_com& marshaled, NNInfo_t1570625892& unmarshaled);
extern "C" void NNInfo_t1570625892_marshal_com_cleanup(NNInfo_t1570625892_marshaled_com& marshaled);
