﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_IO_Compression_DeflateStream2030147241MethodDeclarations.h"
#include "AssemblyU2DCSharp_JSMgr70890511MethodDeclarations.h"

extern const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[7] = 
{
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_DeflateStream_UnmanagedRead_m404803083),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_DeflateStream_UnmanagedWrite_m223973422),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_JSMgr_errorReporter_m3068528493),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_JSMgr_CSEntry_m758593083),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_JSMgr_require_m3358638179),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_JSMgr_print_m3573834459),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_JSMgr_onObjCollected_m1422890442),
};
