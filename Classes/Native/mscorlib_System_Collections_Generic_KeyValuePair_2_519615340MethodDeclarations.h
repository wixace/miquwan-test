﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_519615340.h"
#include "AssemblyU2DCSharp_PushType1840642452.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.KeyValuePair`2<PushType,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1781480841_gshared (KeyValuePair_2_t519615340 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m1781480841(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t519615340 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m1781480841_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<PushType,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1078751295_gshared (KeyValuePair_2_t519615340 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1078751295(__this, method) ((  int32_t (*) (KeyValuePair_2_t519615340 *, const MethodInfo*))KeyValuePair_2_get_Key_m1078751295_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<PushType,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2104494848_gshared (KeyValuePair_2_t519615340 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m2104494848(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t519615340 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m2104494848_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<PushType,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1445662143_gshared (KeyValuePair_2_t519615340 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1445662143(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t519615340 *, const MethodInfo*))KeyValuePair_2_get_Value_m1445662143_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<PushType,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3570261760_gshared (KeyValuePair_2_t519615340 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m3570261760(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t519615340 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m3570261760_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<PushType,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1871991778_gshared (KeyValuePair_2_t519615340 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m1871991778(__this, method) ((  String_t* (*) (KeyValuePair_2_t519615340 *, const MethodInfo*))KeyValuePair_2_ToString_m1871991778_gshared)(__this, method)
