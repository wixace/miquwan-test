﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GestureRecognizerTS`1/<FindGestureByCluster>c__AnonStorey127<System.Object>
struct U3CFindGestureByClusterU3Ec__AnonStorey127_t1396053992;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void GestureRecognizerTS`1/<FindGestureByCluster>c__AnonStorey127<System.Object>::.ctor()
extern "C"  void U3CFindGestureByClusterU3Ec__AnonStorey127__ctor_m3761067263_gshared (U3CFindGestureByClusterU3Ec__AnonStorey127_t1396053992 * __this, const MethodInfo* method);
#define U3CFindGestureByClusterU3Ec__AnonStorey127__ctor_m3761067263(__this, method) ((  void (*) (U3CFindGestureByClusterU3Ec__AnonStorey127_t1396053992 *, const MethodInfo*))U3CFindGestureByClusterU3Ec__AnonStorey127__ctor_m3761067263_gshared)(__this, method)
// System.Boolean GestureRecognizerTS`1/<FindGestureByCluster>c__AnonStorey127<System.Object>::<>m__360(T)
extern "C"  bool U3CFindGestureByClusterU3Ec__AnonStorey127_U3CU3Em__360_m4079637291_gshared (U3CFindGestureByClusterU3Ec__AnonStorey127_t1396053992 * __this, Il2CppObject * ___g0, const MethodInfo* method);
#define U3CFindGestureByClusterU3Ec__AnonStorey127_U3CU3Em__360_m4079637291(__this, ___g0, method) ((  bool (*) (U3CFindGestureByClusterU3Ec__AnonStorey127_t1396053992 *, Il2CppObject *, const MethodInfo*))U3CFindGestureByClusterU3Ec__AnonStorey127_U3CU3Em__360_m4079637291_gshared)(__this, ___g0, method)
