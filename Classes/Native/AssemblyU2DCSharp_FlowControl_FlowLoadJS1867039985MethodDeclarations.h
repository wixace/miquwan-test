﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FlowControl.FlowLoadJS
struct FlowLoadJS_t1867039985;
// LoadingBoardMgr
struct LoadingBoardMgr_t303632014;
// System.String
struct String_t;
// JSAssetMgr
struct JSAssetMgr_t2634428785;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LoadingBoardMgr303632014.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSAssetMgr2634428785.h"

// System.Void FlowControl.FlowLoadJS::.ctor()
extern "C"  void FlowLoadJS__ctor_m3434825055 (FlowLoadJS_t1867039985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowLoadJS::Activate()
extern "C"  void FlowLoadJS_Activate_m3272999480 (FlowLoadJS_t1867039985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowLoadJS::FlowUpdate(System.Single)
extern "C"  void FlowLoadJS_FlowUpdate_m2479560783 (FlowLoadJS_t1867039985 * __this, float ___duringTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowLoadJS::ilo_SetDescri1(LoadingBoardMgr,System.String)
extern "C"  void FlowLoadJS_ilo_SetDescri1_m407887713 (Il2CppObject * __this /* static, unused */, LoadingBoardMgr_t303632014 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowLoadJS::ilo_SetPercent2(LoadingBoardMgr,System.Single,System.String)
extern "C"  void FlowLoadJS_ilo_SetPercent2_m2188546540 (Il2CppObject * __this /* static, unused */, LoadingBoardMgr_t303632014 * ____this0, float ___value1, String_t* ___msg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowLoadJS::ilo_LoadJs3(JSAssetMgr)
extern "C"  void FlowLoadJS_ilo_LoadJs3_m3030069085 (Il2CppObject * __this /* static, unused */, JSAssetMgr_t2634428785 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
