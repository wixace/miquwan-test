﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Projector
struct Projector_t4006592434;
// UnityEngine.Material
struct Material_t3870600107;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"

// System.Void UnityEngine.Projector::.ctor()
extern "C"  void Projector__ctor_m2675483037 (Projector_t4006592434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Projector::get_nearClipPlane()
extern "C"  float Projector_get_nearClipPlane_m289968546 (Projector_t4006592434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Projector::set_nearClipPlane(System.Single)
extern "C"  void Projector_set_nearClipPlane_m2945715785 (Projector_t4006592434 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Projector::get_farClipPlane()
extern "C"  float Projector_get_farClipPlane_m1097904057 (Projector_t4006592434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Projector::set_farClipPlane(System.Single)
extern "C"  void Projector_set_farClipPlane_m1618485138 (Projector_t4006592434 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Projector::get_fieldOfView()
extern "C"  float Projector_get_fieldOfView_m4105876852 (Projector_t4006592434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Projector::set_fieldOfView(System.Single)
extern "C"  void Projector_set_fieldOfView_m3721392055 (Projector_t4006592434 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Projector::get_aspectRatio()
extern "C"  float Projector_get_aspectRatio_m2979664401 (Projector_t4006592434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Projector::set_aspectRatio(System.Single)
extern "C"  void Projector_set_aspectRatio_m2093660730 (Projector_t4006592434 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Projector::get_orthographic()
extern "C"  bool Projector_get_orthographic_m278912762 (Projector_t4006592434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Projector::set_orthographic(System.Boolean)
extern "C"  void Projector_set_orthographic_m1966511883 (Projector_t4006592434 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Projector::get_orthographicSize()
extern "C"  float Projector_get_orthographicSize_m806034421 (Projector_t4006592434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Projector::set_orthographicSize(System.Single)
extern "C"  void Projector_set_orthographicSize_m3877893334 (Projector_t4006592434 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Projector::get_ignoreLayers()
extern "C"  int32_t Projector_get_ignoreLayers_m273429284 (Projector_t4006592434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Projector::set_ignoreLayers(System.Int32)
extern "C"  void Projector_set_ignoreLayers_m785104489 (Projector_t4006592434 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.Projector::get_material()
extern "C"  Material_t3870600107 * Projector_get_material_m245636454 (Projector_t4006592434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Projector::set_material(UnityEngine.Material)
extern "C"  void Projector_set_material_m3870003373 (Projector_t4006592434 * __this, Material_t3870600107 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
