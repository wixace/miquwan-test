﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_mirsi131
struct M_mirsi131_t1376269555;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_mirsi1311376269555.h"

// System.Void GarbageiOS.M_mirsi131::.ctor()
extern "C"  void M_mirsi131__ctor_m479761056 (M_mirsi131_t1376269555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_mirsi131::M_boyiLasmeral0(System.String[],System.Int32)
extern "C"  void M_mirsi131_M_boyiLasmeral0_m2257906863 (M_mirsi131_t1376269555 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_mirsi131::M_xurnay1(System.String[],System.Int32)
extern "C"  void M_mirsi131_M_xurnay1_m1122202499 (M_mirsi131_t1376269555 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_mirsi131::M_nasteTurkaw2(System.String[],System.Int32)
extern "C"  void M_mirsi131_M_nasteTurkaw2_m2903612062 (M_mirsi131_t1376269555 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_mirsi131::M_callwaDurlamor3(System.String[],System.Int32)
extern "C"  void M_mirsi131_M_callwaDurlamor3_m580209908 (M_mirsi131_t1376269555 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_mirsi131::M_kourirdror4(System.String[],System.Int32)
extern "C"  void M_mirsi131_M_kourirdror4_m3355695004 (M_mirsi131_t1376269555 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_mirsi131::M_searjalsouJazelstere5(System.String[],System.Int32)
extern "C"  void M_mirsi131_M_searjalsouJazelstere5_m2394396356 (M_mirsi131_t1376269555 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_mirsi131::ilo_M_kourirdror41(GarbageiOS.M_mirsi131,System.String[],System.Int32)
extern "C"  void M_mirsi131_ilo_M_kourirdror41_m3913342519 (Il2CppObject * __this /* static, unused */, M_mirsi131_t1376269555 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
