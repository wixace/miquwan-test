﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZMG
struct ZMG_t88948;
// GlobalGOMgr
struct GlobalGOMgr_t803081773;
// SceneMgr
struct SceneMgr_t3584105996;
// TimeMgr
struct TimeMgr_t350708715;
// TimeUpdateMgr
struct TimeUpdateMgr_t880289826;
// EffectMgr
struct EffectMgr_t535289511;
// SoundMgr
struct SoundMgr_t1807284905;
// TeamSkillMgr
struct TeamSkillMgr_t2030650276;
// UIEffectMgr
struct UIEffectMgr_t952662675;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// CSGameDataMgr
struct CSGameDataMgr_t2623305516;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// StoryMgr
struct StoryMgr_t1782380803;
// ZiShiYingMgr
struct ZiShiYingMgr_t4004457962;
// Mihua.Assets.SubAssetMgr
struct SubAssetMgr_t3564963414;
// GameQutilyCtr
struct GameQutilyCtr_t3963256169;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// CameraShotMgr
struct CameraShotMgr_t1580128697;
// AntiCheatMgr
struct AntiCheatMgr_t3474304487;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// MihuaPayMgr
struct MihuaPayMgr_t1240399912;
// CaptureScreenshotMgr
struct CaptureScreenshotMgr_t3951887692;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZMG
struct  ZMG_t88948  : public MonoBehaviour_t667441552
{
public:
	// GlobalGOMgr ZMG::globalGOMgr
	GlobalGOMgr_t803081773 * ___globalGOMgr_3;
	// SceneMgr ZMG::sceneMgr
	SceneMgr_t3584105996 * ___sceneMgr_4;
	// TimeMgr ZMG::timeMgr
	TimeMgr_t350708715 * ___timeMgr_5;
	// TimeUpdateMgr ZMG::timeUpdateMgr
	TimeUpdateMgr_t880289826 * ___timeUpdateMgr_6;
	// EffectMgr ZMG::effectMgr
	EffectMgr_t535289511 * ___effectMgr_7;
	// SoundMgr ZMG::soundMgr
	SoundMgr_t1807284905 * ___soundMgr_8;
	// TeamSkillMgr ZMG::teamSkillMgr
	TeamSkillMgr_t2030650276 * ___teamSkillMgr_9;
	// UIEffectMgr ZMG::uiEffectMgr
	UIEffectMgr_t952662675 * ___uiEffectMgr_10;
	// FloatTextMgr ZMG::floatTextMgr
	FloatTextMgr_t630384591 * ___floatTextMgr_11;
	// CSGameDataMgr ZMG::csGameDataMgr
	CSGameDataMgr_t2623305516 * ___csGameDataMgr_12;
	// CSDatacfgManager ZMG::csDatacfgMgr
	CSDatacfgManager_t1565254243 * ___csDatacfgMgr_13;
	// StoryMgr ZMG::storyMgr
	StoryMgr_t1782380803 * ___storyMgr_14;
	// ZiShiYingMgr ZMG::ziShiYingMgr
	ZiShiYingMgr_t4004457962 * ___ziShiYingMgr_15;
	// Mihua.Assets.SubAssetMgr ZMG::subAssetMgr
	SubAssetMgr_t3564963414 * ___subAssetMgr_16;
	// GameQutilyCtr ZMG::gameQutilyCtr
	GameQutilyCtr_t3963256169 * ___gameQutilyCtr_17;
	// PluginsSdkMgr ZMG::pluginsSdkMgr
	PluginsSdkMgr_t3884624670 * ___pluginsSdkMgr_18;
	// CameraShotMgr ZMG::cameraShotMgr
	CameraShotMgr_t1580128697 * ___cameraShotMgr_19;
	// AntiCheatMgr ZMG::antiCheatMgr
	AntiCheatMgr_t3474304487 * ___antiCheatMgr_20;
	// ProductsCfgMgr ZMG::productsCfgMgr
	ProductsCfgMgr_t2493714872 * ___productsCfgMgr_21;
	// MihuaPayMgr ZMG::mihuaPayMgr
	MihuaPayMgr_t1240399912 * ___mihuaPayMgr_22;
	// CaptureScreenshotMgr ZMG::captureScreenshotMgr
	CaptureScreenshotMgr_t3951887692 * ___captureScreenshotMgr_23;
	// System.Boolean ZMG::isInit
	bool ___isInit_24;

public:
	inline static int32_t get_offset_of_globalGOMgr_3() { return static_cast<int32_t>(offsetof(ZMG_t88948, ___globalGOMgr_3)); }
	inline GlobalGOMgr_t803081773 * get_globalGOMgr_3() const { return ___globalGOMgr_3; }
	inline GlobalGOMgr_t803081773 ** get_address_of_globalGOMgr_3() { return &___globalGOMgr_3; }
	inline void set_globalGOMgr_3(GlobalGOMgr_t803081773 * value)
	{
		___globalGOMgr_3 = value;
		Il2CppCodeGenWriteBarrier(&___globalGOMgr_3, value);
	}

	inline static int32_t get_offset_of_sceneMgr_4() { return static_cast<int32_t>(offsetof(ZMG_t88948, ___sceneMgr_4)); }
	inline SceneMgr_t3584105996 * get_sceneMgr_4() const { return ___sceneMgr_4; }
	inline SceneMgr_t3584105996 ** get_address_of_sceneMgr_4() { return &___sceneMgr_4; }
	inline void set_sceneMgr_4(SceneMgr_t3584105996 * value)
	{
		___sceneMgr_4 = value;
		Il2CppCodeGenWriteBarrier(&___sceneMgr_4, value);
	}

	inline static int32_t get_offset_of_timeMgr_5() { return static_cast<int32_t>(offsetof(ZMG_t88948, ___timeMgr_5)); }
	inline TimeMgr_t350708715 * get_timeMgr_5() const { return ___timeMgr_5; }
	inline TimeMgr_t350708715 ** get_address_of_timeMgr_5() { return &___timeMgr_5; }
	inline void set_timeMgr_5(TimeMgr_t350708715 * value)
	{
		___timeMgr_5 = value;
		Il2CppCodeGenWriteBarrier(&___timeMgr_5, value);
	}

	inline static int32_t get_offset_of_timeUpdateMgr_6() { return static_cast<int32_t>(offsetof(ZMG_t88948, ___timeUpdateMgr_6)); }
	inline TimeUpdateMgr_t880289826 * get_timeUpdateMgr_6() const { return ___timeUpdateMgr_6; }
	inline TimeUpdateMgr_t880289826 ** get_address_of_timeUpdateMgr_6() { return &___timeUpdateMgr_6; }
	inline void set_timeUpdateMgr_6(TimeUpdateMgr_t880289826 * value)
	{
		___timeUpdateMgr_6 = value;
		Il2CppCodeGenWriteBarrier(&___timeUpdateMgr_6, value);
	}

	inline static int32_t get_offset_of_effectMgr_7() { return static_cast<int32_t>(offsetof(ZMG_t88948, ___effectMgr_7)); }
	inline EffectMgr_t535289511 * get_effectMgr_7() const { return ___effectMgr_7; }
	inline EffectMgr_t535289511 ** get_address_of_effectMgr_7() { return &___effectMgr_7; }
	inline void set_effectMgr_7(EffectMgr_t535289511 * value)
	{
		___effectMgr_7 = value;
		Il2CppCodeGenWriteBarrier(&___effectMgr_7, value);
	}

	inline static int32_t get_offset_of_soundMgr_8() { return static_cast<int32_t>(offsetof(ZMG_t88948, ___soundMgr_8)); }
	inline SoundMgr_t1807284905 * get_soundMgr_8() const { return ___soundMgr_8; }
	inline SoundMgr_t1807284905 ** get_address_of_soundMgr_8() { return &___soundMgr_8; }
	inline void set_soundMgr_8(SoundMgr_t1807284905 * value)
	{
		___soundMgr_8 = value;
		Il2CppCodeGenWriteBarrier(&___soundMgr_8, value);
	}

	inline static int32_t get_offset_of_teamSkillMgr_9() { return static_cast<int32_t>(offsetof(ZMG_t88948, ___teamSkillMgr_9)); }
	inline TeamSkillMgr_t2030650276 * get_teamSkillMgr_9() const { return ___teamSkillMgr_9; }
	inline TeamSkillMgr_t2030650276 ** get_address_of_teamSkillMgr_9() { return &___teamSkillMgr_9; }
	inline void set_teamSkillMgr_9(TeamSkillMgr_t2030650276 * value)
	{
		___teamSkillMgr_9 = value;
		Il2CppCodeGenWriteBarrier(&___teamSkillMgr_9, value);
	}

	inline static int32_t get_offset_of_uiEffectMgr_10() { return static_cast<int32_t>(offsetof(ZMG_t88948, ___uiEffectMgr_10)); }
	inline UIEffectMgr_t952662675 * get_uiEffectMgr_10() const { return ___uiEffectMgr_10; }
	inline UIEffectMgr_t952662675 ** get_address_of_uiEffectMgr_10() { return &___uiEffectMgr_10; }
	inline void set_uiEffectMgr_10(UIEffectMgr_t952662675 * value)
	{
		___uiEffectMgr_10 = value;
		Il2CppCodeGenWriteBarrier(&___uiEffectMgr_10, value);
	}

	inline static int32_t get_offset_of_floatTextMgr_11() { return static_cast<int32_t>(offsetof(ZMG_t88948, ___floatTextMgr_11)); }
	inline FloatTextMgr_t630384591 * get_floatTextMgr_11() const { return ___floatTextMgr_11; }
	inline FloatTextMgr_t630384591 ** get_address_of_floatTextMgr_11() { return &___floatTextMgr_11; }
	inline void set_floatTextMgr_11(FloatTextMgr_t630384591 * value)
	{
		___floatTextMgr_11 = value;
		Il2CppCodeGenWriteBarrier(&___floatTextMgr_11, value);
	}

	inline static int32_t get_offset_of_csGameDataMgr_12() { return static_cast<int32_t>(offsetof(ZMG_t88948, ___csGameDataMgr_12)); }
	inline CSGameDataMgr_t2623305516 * get_csGameDataMgr_12() const { return ___csGameDataMgr_12; }
	inline CSGameDataMgr_t2623305516 ** get_address_of_csGameDataMgr_12() { return &___csGameDataMgr_12; }
	inline void set_csGameDataMgr_12(CSGameDataMgr_t2623305516 * value)
	{
		___csGameDataMgr_12 = value;
		Il2CppCodeGenWriteBarrier(&___csGameDataMgr_12, value);
	}

	inline static int32_t get_offset_of_csDatacfgMgr_13() { return static_cast<int32_t>(offsetof(ZMG_t88948, ___csDatacfgMgr_13)); }
	inline CSDatacfgManager_t1565254243 * get_csDatacfgMgr_13() const { return ___csDatacfgMgr_13; }
	inline CSDatacfgManager_t1565254243 ** get_address_of_csDatacfgMgr_13() { return &___csDatacfgMgr_13; }
	inline void set_csDatacfgMgr_13(CSDatacfgManager_t1565254243 * value)
	{
		___csDatacfgMgr_13 = value;
		Il2CppCodeGenWriteBarrier(&___csDatacfgMgr_13, value);
	}

	inline static int32_t get_offset_of_storyMgr_14() { return static_cast<int32_t>(offsetof(ZMG_t88948, ___storyMgr_14)); }
	inline StoryMgr_t1782380803 * get_storyMgr_14() const { return ___storyMgr_14; }
	inline StoryMgr_t1782380803 ** get_address_of_storyMgr_14() { return &___storyMgr_14; }
	inline void set_storyMgr_14(StoryMgr_t1782380803 * value)
	{
		___storyMgr_14 = value;
		Il2CppCodeGenWriteBarrier(&___storyMgr_14, value);
	}

	inline static int32_t get_offset_of_ziShiYingMgr_15() { return static_cast<int32_t>(offsetof(ZMG_t88948, ___ziShiYingMgr_15)); }
	inline ZiShiYingMgr_t4004457962 * get_ziShiYingMgr_15() const { return ___ziShiYingMgr_15; }
	inline ZiShiYingMgr_t4004457962 ** get_address_of_ziShiYingMgr_15() { return &___ziShiYingMgr_15; }
	inline void set_ziShiYingMgr_15(ZiShiYingMgr_t4004457962 * value)
	{
		___ziShiYingMgr_15 = value;
		Il2CppCodeGenWriteBarrier(&___ziShiYingMgr_15, value);
	}

	inline static int32_t get_offset_of_subAssetMgr_16() { return static_cast<int32_t>(offsetof(ZMG_t88948, ___subAssetMgr_16)); }
	inline SubAssetMgr_t3564963414 * get_subAssetMgr_16() const { return ___subAssetMgr_16; }
	inline SubAssetMgr_t3564963414 ** get_address_of_subAssetMgr_16() { return &___subAssetMgr_16; }
	inline void set_subAssetMgr_16(SubAssetMgr_t3564963414 * value)
	{
		___subAssetMgr_16 = value;
		Il2CppCodeGenWriteBarrier(&___subAssetMgr_16, value);
	}

	inline static int32_t get_offset_of_gameQutilyCtr_17() { return static_cast<int32_t>(offsetof(ZMG_t88948, ___gameQutilyCtr_17)); }
	inline GameQutilyCtr_t3963256169 * get_gameQutilyCtr_17() const { return ___gameQutilyCtr_17; }
	inline GameQutilyCtr_t3963256169 ** get_address_of_gameQutilyCtr_17() { return &___gameQutilyCtr_17; }
	inline void set_gameQutilyCtr_17(GameQutilyCtr_t3963256169 * value)
	{
		___gameQutilyCtr_17 = value;
		Il2CppCodeGenWriteBarrier(&___gameQutilyCtr_17, value);
	}

	inline static int32_t get_offset_of_pluginsSdkMgr_18() { return static_cast<int32_t>(offsetof(ZMG_t88948, ___pluginsSdkMgr_18)); }
	inline PluginsSdkMgr_t3884624670 * get_pluginsSdkMgr_18() const { return ___pluginsSdkMgr_18; }
	inline PluginsSdkMgr_t3884624670 ** get_address_of_pluginsSdkMgr_18() { return &___pluginsSdkMgr_18; }
	inline void set_pluginsSdkMgr_18(PluginsSdkMgr_t3884624670 * value)
	{
		___pluginsSdkMgr_18 = value;
		Il2CppCodeGenWriteBarrier(&___pluginsSdkMgr_18, value);
	}

	inline static int32_t get_offset_of_cameraShotMgr_19() { return static_cast<int32_t>(offsetof(ZMG_t88948, ___cameraShotMgr_19)); }
	inline CameraShotMgr_t1580128697 * get_cameraShotMgr_19() const { return ___cameraShotMgr_19; }
	inline CameraShotMgr_t1580128697 ** get_address_of_cameraShotMgr_19() { return &___cameraShotMgr_19; }
	inline void set_cameraShotMgr_19(CameraShotMgr_t1580128697 * value)
	{
		___cameraShotMgr_19 = value;
		Il2CppCodeGenWriteBarrier(&___cameraShotMgr_19, value);
	}

	inline static int32_t get_offset_of_antiCheatMgr_20() { return static_cast<int32_t>(offsetof(ZMG_t88948, ___antiCheatMgr_20)); }
	inline AntiCheatMgr_t3474304487 * get_antiCheatMgr_20() const { return ___antiCheatMgr_20; }
	inline AntiCheatMgr_t3474304487 ** get_address_of_antiCheatMgr_20() { return &___antiCheatMgr_20; }
	inline void set_antiCheatMgr_20(AntiCheatMgr_t3474304487 * value)
	{
		___antiCheatMgr_20 = value;
		Il2CppCodeGenWriteBarrier(&___antiCheatMgr_20, value);
	}

	inline static int32_t get_offset_of_productsCfgMgr_21() { return static_cast<int32_t>(offsetof(ZMG_t88948, ___productsCfgMgr_21)); }
	inline ProductsCfgMgr_t2493714872 * get_productsCfgMgr_21() const { return ___productsCfgMgr_21; }
	inline ProductsCfgMgr_t2493714872 ** get_address_of_productsCfgMgr_21() { return &___productsCfgMgr_21; }
	inline void set_productsCfgMgr_21(ProductsCfgMgr_t2493714872 * value)
	{
		___productsCfgMgr_21 = value;
		Il2CppCodeGenWriteBarrier(&___productsCfgMgr_21, value);
	}

	inline static int32_t get_offset_of_mihuaPayMgr_22() { return static_cast<int32_t>(offsetof(ZMG_t88948, ___mihuaPayMgr_22)); }
	inline MihuaPayMgr_t1240399912 * get_mihuaPayMgr_22() const { return ___mihuaPayMgr_22; }
	inline MihuaPayMgr_t1240399912 ** get_address_of_mihuaPayMgr_22() { return &___mihuaPayMgr_22; }
	inline void set_mihuaPayMgr_22(MihuaPayMgr_t1240399912 * value)
	{
		___mihuaPayMgr_22 = value;
		Il2CppCodeGenWriteBarrier(&___mihuaPayMgr_22, value);
	}

	inline static int32_t get_offset_of_captureScreenshotMgr_23() { return static_cast<int32_t>(offsetof(ZMG_t88948, ___captureScreenshotMgr_23)); }
	inline CaptureScreenshotMgr_t3951887692 * get_captureScreenshotMgr_23() const { return ___captureScreenshotMgr_23; }
	inline CaptureScreenshotMgr_t3951887692 ** get_address_of_captureScreenshotMgr_23() { return &___captureScreenshotMgr_23; }
	inline void set_captureScreenshotMgr_23(CaptureScreenshotMgr_t3951887692 * value)
	{
		___captureScreenshotMgr_23 = value;
		Il2CppCodeGenWriteBarrier(&___captureScreenshotMgr_23, value);
	}

	inline static int32_t get_offset_of_isInit_24() { return static_cast<int32_t>(offsetof(ZMG_t88948, ___isInit_24)); }
	inline bool get_isInit_24() const { return ___isInit_24; }
	inline bool* get_address_of_isInit_24() { return &___isInit_24; }
	inline void set_isInit_24(bool value)
	{
		___isInit_24 = value;
	}
};

struct ZMG_t88948_StaticFields
{
public:
	// ZMG ZMG::_zmg
	ZMG_t88948 * ____zmg_2;

public:
	inline static int32_t get_offset_of__zmg_2() { return static_cast<int32_t>(offsetof(ZMG_t88948_StaticFields, ____zmg_2)); }
	inline ZMG_t88948 * get__zmg_2() const { return ____zmg_2; }
	inline ZMG_t88948 ** get_address_of__zmg_2() { return &____zmg_2; }
	inline void set__zmg_2(ZMG_t88948 * value)
	{
		____zmg_2 = value;
		Il2CppCodeGenWriteBarrier(&____zmg_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
