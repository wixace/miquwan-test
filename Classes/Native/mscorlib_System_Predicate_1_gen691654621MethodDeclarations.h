﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Predicate`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>
struct Predicate_1_t691654621;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeB1080597738.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Predicate`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m593948232_gshared (Predicate_1_t691654621 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Predicate_1__ctor_m593948232(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t691654621 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m593948232_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3531364666_gshared (Predicate_1_t691654621 * __this, ReflectionProbeBlendInfo_t1080597738  ___obj0, const MethodInfo* method);
#define Predicate_1_Invoke_m3531364666(__this, ___obj0, method) ((  bool (*) (Predicate_1_t691654621 *, ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))Predicate_1_Invoke_m3531364666_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2225368009_gshared (Predicate_1_t691654621 * __this, ReflectionProbeBlendInfo_t1080597738  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m2225368009(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t691654621 *, ReflectionProbeBlendInfo_t1080597738 , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m2225368009_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m168153818_gshared (Predicate_1_t691654621 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Predicate_1_EndInvoke_m168153818(__this, ___result0, method) ((  bool (*) (Predicate_1_t691654621 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m168153818_gshared)(__this, ___result0, method)
