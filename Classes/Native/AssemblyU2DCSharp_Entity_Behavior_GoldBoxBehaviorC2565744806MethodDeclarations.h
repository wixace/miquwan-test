﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.GoldBoxBehaviorCtrl
struct GoldBoxBehaviorCtrl_t2565744806;
// CombatEntity
struct CombatEntity_t684137495;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"

// System.Void Entity.Behavior.GoldBoxBehaviorCtrl::.ctor(CombatEntity)
extern "C"  void GoldBoxBehaviorCtrl__ctor_m2542387805 (GoldBoxBehaviorCtrl_t2565744806 * __this, CombatEntity_t684137495 * ___entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.GoldBoxBehaviorCtrl::Init()
extern "C"  void GoldBoxBehaviorCtrl_Init_m2796836800 (GoldBoxBehaviorCtrl_t2565744806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.GoldBoxBehaviorCtrl::TranBehavior(Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void GoldBoxBehaviorCtrl_TranBehavior_m2653124784 (GoldBoxBehaviorCtrl_t2565744806 * __this, uint8_t ___id0, ObjectU5BU5D_t1108656482* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.GoldBoxBehaviorCtrl::ilo_AddBehavior1(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.IBehavior)
extern "C"  void GoldBoxBehaviorCtrl_ilo_AddBehavior1_m1297796318 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, IBehavior_t770859129 * ___behavior1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.GoldBoxBehaviorCtrl::ilo_set_curBehavior2(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.IBehavior)
extern "C"  void GoldBoxBehaviorCtrl_ilo_set_curBehavior2_m865444063 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, IBehavior_t770859129 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.GoldBoxBehaviorCtrl::ilo_set_entity3(Entity.Behavior.IBehavior,CombatEntity)
extern "C"  void GoldBoxBehaviorCtrl_ilo_set_entity3_m2505674937 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, CombatEntity_t684137495 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehavior Entity.Behavior.GoldBoxBehaviorCtrl::ilo_get_curBehavior4(Entity.Behavior.IBehaviorCtrl)
extern "C"  IBehavior_t770859129 * GoldBoxBehaviorCtrl_ilo_get_curBehavior4_m2690556600 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
