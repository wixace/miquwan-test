﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Char[]
struct CharU5BU5D_t3324145743;
// UIRoot
struct UIRoot_t2503447958;
// UnityEngine.Camera
struct Camera_t2727095145;
// UICamera
struct UICamera_t189364953;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// UITexture
struct UITexture_t3903132647;
// TweenAlpha
struct TweenAlpha_t2920325587;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlobalGOMgr
struct  GlobalGOMgr_t803081773  : public Il2CppObject
{
public:
	// System.Char[] GlobalGOMgr::separator
	CharU5BU5D_t3324145743* ___separator_0;
	// UIRoot GlobalGOMgr::uiroot
	UIRoot_t2503447958 * ___uiroot_1;
	// UnityEngine.Camera GlobalGOMgr::uiCamera
	Camera_t2727095145 * ___uiCamera_2;
	// UICamera GlobalGOMgr::uiNguiCamera
	UICamera_t189364953 * ___uiNguiCamera_3;
	// UnityEngine.Transform GlobalGOMgr::uiparent
	Transform_t1659122786 * ___uiparent_4;
	// UnityEngine.Camera GlobalGOMgr::uiBy3dCamera
	Camera_t2727095145 * ___uiBy3dCamera_5;
	// UnityEngine.GameObject GlobalGOMgr::maskGO
	GameObject_t3674682005 * ___maskGO_6;
	// UnityEngine.GameObject GlobalGOMgr::uiby3d
	GameObject_t3674682005 * ___uiby3d_7;
	// UnityEngine.GameObject GlobalGOMgr::herosGO
	GameObject_t3674682005 * ___herosGO_8;
	// UnityEngine.GameObject GlobalGOMgr::SceneCamera
	GameObject_t3674682005 * ___SceneCamera_9;
	// UnityEngine.GameObject GlobalGOMgr::PlayerCamera
	GameObject_t3674682005 * ___PlayerCamera_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> GlobalGOMgr::key2dDic
	Dictionary_2_t827649927 * ___key2dDic_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> GlobalGOMgr::key3dDic
	Dictionary_2_t827649927 * ___key3dDic_12;
	// UITexture GlobalGOMgr::over_effect
	UITexture_t3903132647 * ___over_effect_13;
	// TweenAlpha GlobalGOMgr::over_tweenAlpha
	TweenAlpha_t2920325587 * ___over_tweenAlpha_14;

public:
	inline static int32_t get_offset_of_separator_0() { return static_cast<int32_t>(offsetof(GlobalGOMgr_t803081773, ___separator_0)); }
	inline CharU5BU5D_t3324145743* get_separator_0() const { return ___separator_0; }
	inline CharU5BU5D_t3324145743** get_address_of_separator_0() { return &___separator_0; }
	inline void set_separator_0(CharU5BU5D_t3324145743* value)
	{
		___separator_0 = value;
		Il2CppCodeGenWriteBarrier(&___separator_0, value);
	}

	inline static int32_t get_offset_of_uiroot_1() { return static_cast<int32_t>(offsetof(GlobalGOMgr_t803081773, ___uiroot_1)); }
	inline UIRoot_t2503447958 * get_uiroot_1() const { return ___uiroot_1; }
	inline UIRoot_t2503447958 ** get_address_of_uiroot_1() { return &___uiroot_1; }
	inline void set_uiroot_1(UIRoot_t2503447958 * value)
	{
		___uiroot_1 = value;
		Il2CppCodeGenWriteBarrier(&___uiroot_1, value);
	}

	inline static int32_t get_offset_of_uiCamera_2() { return static_cast<int32_t>(offsetof(GlobalGOMgr_t803081773, ___uiCamera_2)); }
	inline Camera_t2727095145 * get_uiCamera_2() const { return ___uiCamera_2; }
	inline Camera_t2727095145 ** get_address_of_uiCamera_2() { return &___uiCamera_2; }
	inline void set_uiCamera_2(Camera_t2727095145 * value)
	{
		___uiCamera_2 = value;
		Il2CppCodeGenWriteBarrier(&___uiCamera_2, value);
	}

	inline static int32_t get_offset_of_uiNguiCamera_3() { return static_cast<int32_t>(offsetof(GlobalGOMgr_t803081773, ___uiNguiCamera_3)); }
	inline UICamera_t189364953 * get_uiNguiCamera_3() const { return ___uiNguiCamera_3; }
	inline UICamera_t189364953 ** get_address_of_uiNguiCamera_3() { return &___uiNguiCamera_3; }
	inline void set_uiNguiCamera_3(UICamera_t189364953 * value)
	{
		___uiNguiCamera_3 = value;
		Il2CppCodeGenWriteBarrier(&___uiNguiCamera_3, value);
	}

	inline static int32_t get_offset_of_uiparent_4() { return static_cast<int32_t>(offsetof(GlobalGOMgr_t803081773, ___uiparent_4)); }
	inline Transform_t1659122786 * get_uiparent_4() const { return ___uiparent_4; }
	inline Transform_t1659122786 ** get_address_of_uiparent_4() { return &___uiparent_4; }
	inline void set_uiparent_4(Transform_t1659122786 * value)
	{
		___uiparent_4 = value;
		Il2CppCodeGenWriteBarrier(&___uiparent_4, value);
	}

	inline static int32_t get_offset_of_uiBy3dCamera_5() { return static_cast<int32_t>(offsetof(GlobalGOMgr_t803081773, ___uiBy3dCamera_5)); }
	inline Camera_t2727095145 * get_uiBy3dCamera_5() const { return ___uiBy3dCamera_5; }
	inline Camera_t2727095145 ** get_address_of_uiBy3dCamera_5() { return &___uiBy3dCamera_5; }
	inline void set_uiBy3dCamera_5(Camera_t2727095145 * value)
	{
		___uiBy3dCamera_5 = value;
		Il2CppCodeGenWriteBarrier(&___uiBy3dCamera_5, value);
	}

	inline static int32_t get_offset_of_maskGO_6() { return static_cast<int32_t>(offsetof(GlobalGOMgr_t803081773, ___maskGO_6)); }
	inline GameObject_t3674682005 * get_maskGO_6() const { return ___maskGO_6; }
	inline GameObject_t3674682005 ** get_address_of_maskGO_6() { return &___maskGO_6; }
	inline void set_maskGO_6(GameObject_t3674682005 * value)
	{
		___maskGO_6 = value;
		Il2CppCodeGenWriteBarrier(&___maskGO_6, value);
	}

	inline static int32_t get_offset_of_uiby3d_7() { return static_cast<int32_t>(offsetof(GlobalGOMgr_t803081773, ___uiby3d_7)); }
	inline GameObject_t3674682005 * get_uiby3d_7() const { return ___uiby3d_7; }
	inline GameObject_t3674682005 ** get_address_of_uiby3d_7() { return &___uiby3d_7; }
	inline void set_uiby3d_7(GameObject_t3674682005 * value)
	{
		___uiby3d_7 = value;
		Il2CppCodeGenWriteBarrier(&___uiby3d_7, value);
	}

	inline static int32_t get_offset_of_herosGO_8() { return static_cast<int32_t>(offsetof(GlobalGOMgr_t803081773, ___herosGO_8)); }
	inline GameObject_t3674682005 * get_herosGO_8() const { return ___herosGO_8; }
	inline GameObject_t3674682005 ** get_address_of_herosGO_8() { return &___herosGO_8; }
	inline void set_herosGO_8(GameObject_t3674682005 * value)
	{
		___herosGO_8 = value;
		Il2CppCodeGenWriteBarrier(&___herosGO_8, value);
	}

	inline static int32_t get_offset_of_SceneCamera_9() { return static_cast<int32_t>(offsetof(GlobalGOMgr_t803081773, ___SceneCamera_9)); }
	inline GameObject_t3674682005 * get_SceneCamera_9() const { return ___SceneCamera_9; }
	inline GameObject_t3674682005 ** get_address_of_SceneCamera_9() { return &___SceneCamera_9; }
	inline void set_SceneCamera_9(GameObject_t3674682005 * value)
	{
		___SceneCamera_9 = value;
		Il2CppCodeGenWriteBarrier(&___SceneCamera_9, value);
	}

	inline static int32_t get_offset_of_PlayerCamera_10() { return static_cast<int32_t>(offsetof(GlobalGOMgr_t803081773, ___PlayerCamera_10)); }
	inline GameObject_t3674682005 * get_PlayerCamera_10() const { return ___PlayerCamera_10; }
	inline GameObject_t3674682005 ** get_address_of_PlayerCamera_10() { return &___PlayerCamera_10; }
	inline void set_PlayerCamera_10(GameObject_t3674682005 * value)
	{
		___PlayerCamera_10 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerCamera_10, value);
	}

	inline static int32_t get_offset_of_key2dDic_11() { return static_cast<int32_t>(offsetof(GlobalGOMgr_t803081773, ___key2dDic_11)); }
	inline Dictionary_2_t827649927 * get_key2dDic_11() const { return ___key2dDic_11; }
	inline Dictionary_2_t827649927 ** get_address_of_key2dDic_11() { return &___key2dDic_11; }
	inline void set_key2dDic_11(Dictionary_2_t827649927 * value)
	{
		___key2dDic_11 = value;
		Il2CppCodeGenWriteBarrier(&___key2dDic_11, value);
	}

	inline static int32_t get_offset_of_key3dDic_12() { return static_cast<int32_t>(offsetof(GlobalGOMgr_t803081773, ___key3dDic_12)); }
	inline Dictionary_2_t827649927 * get_key3dDic_12() const { return ___key3dDic_12; }
	inline Dictionary_2_t827649927 ** get_address_of_key3dDic_12() { return &___key3dDic_12; }
	inline void set_key3dDic_12(Dictionary_2_t827649927 * value)
	{
		___key3dDic_12 = value;
		Il2CppCodeGenWriteBarrier(&___key3dDic_12, value);
	}

	inline static int32_t get_offset_of_over_effect_13() { return static_cast<int32_t>(offsetof(GlobalGOMgr_t803081773, ___over_effect_13)); }
	inline UITexture_t3903132647 * get_over_effect_13() const { return ___over_effect_13; }
	inline UITexture_t3903132647 ** get_address_of_over_effect_13() { return &___over_effect_13; }
	inline void set_over_effect_13(UITexture_t3903132647 * value)
	{
		___over_effect_13 = value;
		Il2CppCodeGenWriteBarrier(&___over_effect_13, value);
	}

	inline static int32_t get_offset_of_over_tweenAlpha_14() { return static_cast<int32_t>(offsetof(GlobalGOMgr_t803081773, ___over_tweenAlpha_14)); }
	inline TweenAlpha_t2920325587 * get_over_tweenAlpha_14() const { return ___over_tweenAlpha_14; }
	inline TweenAlpha_t2920325587 ** get_address_of_over_tweenAlpha_14() { return &___over_tweenAlpha_14; }
	inline void set_over_tweenAlpha_14(TweenAlpha_t2920325587 * value)
	{
		___over_tweenAlpha_14 = value;
		Il2CppCodeGenWriteBarrier(&___over_tweenAlpha_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
