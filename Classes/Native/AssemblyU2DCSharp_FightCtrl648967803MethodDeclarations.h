﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FightCtrl
struct FightCtrl_t648967803;
// CombatEntity
struct CombatEntity_t684137495;
// Skill
struct Skill_t79944241;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// skillCfg
struct skillCfg_t2142425171;
// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;
// DamageSeparationSkill
struct DamageSeparationSkill_t2001134364;
// System.Action
struct Action_t3771233898;
// CSSkillData
struct CSSkillData_t432288523;
// BuffCtrl
struct BuffCtrl_t2836564350;
// System.Object
struct Il2CppObject;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// SkillHurtShow
struct SkillHurtShow_t1728264445;
// JSCLevelMonsterConfig
struct JSCLevelMonsterConfig_t1924079698;
// Monster
struct Monster_t2901270458;
// NpcMgr
struct NpcMgr_t2339534743;
// System.String
struct String_t;
// ReplayMgr
struct ReplayMgr_t1549183121;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// EffectMgr
struct EffectMgr_t535289511;
// EffectPara
struct EffectPara_t3709156657;
// effectCfg
struct effectCfg_t2826279187;
// AnimationRunner
struct AnimationRunner_t1015409588;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// ProgressBar
struct ProgressBar_t2799378054;
// Blood
struct Blood_t64280026;
// FightEnum.ESkillBuffTarget[]
struct ESkillBuffTargetU5BU5D_t2691263849;
// GameMgr
struct GameMgr_t1469029542;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// CameraShotMgr
struct CameraShotMgr_t1580128697;
// HeroMgr
struct HeroMgr_t2475965342;
// Hero
struct Hero_t2245658;
// HatredCtrl
struct HatredCtrl_t891253697;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Skill79944241.h"
#include "AssemblyU2DCSharp_FightEnum_EFightRst3627658870.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_skillCfg2142425171.h"
#include "AssemblyU2DCSharp_FightEnum_EMoveEvent3662091020.h"
#include "AssemblyU2DCSharp_FightEnum_ESkillStateEvent785526141.h"
#include "AssemblyU2DCSharp_DamageSeparationSkill2001134364.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_FightEnum_ESkillState1611731219.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitType643359572.h"
#include "AssemblyU2DCSharp_CSSkillData432288523.h"
#include "AssemblyU2DCSharp_FightCtrl648967803.h"
#include "AssemblyU2DCSharp_BuffCtrl2836564350.h"
#include "AssemblyU2DCSharp_FightEnum_ESkillBuffEvent465856265.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_FloatTextMgr630384591.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitCamp642829979.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "AssemblyU2DCSharp_SkillHurtShow1728264445.h"
#include "AssemblyU2DCSharp_JSCLevelMonsterConfig1924079698.h"
#include "AssemblyU2DCSharp_Monster2901270458.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_EffectMgr535289511.h"
#include "AssemblyU2DCSharp_EffectPara3709156657.h"
#include "AssemblyU2DCSharp_CSDatacfgManager1565254243.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitTag299076696.h"
#include "AssemblyU2DCSharp_AnimationRunner1015409588.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_ProgressBar2799378054.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"
#include "AssemblyU2DCSharp_Blood64280026.h"
#include "AssemblyU2DCSharp_HeroMgr2475965342.h"
#include "AssemblyU2DCSharp_HatredCtrl891253697.h"

// System.Void FightCtrl::.ctor(CombatEntity)
extern "C"  void FightCtrl__ctor_m2455739801 (FightCtrl_t648967803 * __this, CombatEntity_t684137495 * ___entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Skill FightCtrl::get_activeSkill()
extern "C"  Skill_t79944241 * FightCtrl_get_activeSkill_m411172370 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::set_activeSkill(Skill)
extern "C"  void FightCtrl_set_activeSkill_m547552965 (FightCtrl_t648967803 * __this, Skill_t79944241 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Skill FightCtrl::get_normalAttackSkill()
extern "C"  Skill_t79944241 * FightCtrl_get_normalAttackSkill_m1087331945 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::set_normalAttackSkill(Skill)
extern "C"  void FightCtrl_set_normalAttackSkill_m2165812302 (FightCtrl_t648967803 * __this, Skill_t79944241 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Skill FightCtrl::get_deathSkill()
extern "C"  Skill_t79944241 * FightCtrl_get_deathSkill_m544648280 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::set_deathSkill(Skill)
extern "C"  void FightCtrl_set_deathSkill_m3454142355 (FightCtrl_t648967803 * __this, Skill_t79944241 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Skill FightCtrl::get_wakeSkill()
extern "C"  Skill_t79944241 * FightCtrl_get_wakeSkill_m2168479540 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::set_wakeSkill(Skill)
extern "C"  void FightCtrl_set_wakeSkill_m3027191907 (FightCtrl_t648967803 * __this, Skill_t79944241 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrl::get_isUsingSkill()
extern "C"  bool FightCtrl_get_isUsingSkill_m2992310880 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrl::get_isUsingActiveSkill()
extern "C"  bool FightCtrl_get_isUsingActiveSkill_m2459966970 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrl::get_isUsingNormalSkill()
extern "C"  bool FightCtrl_get_isUsingNormalSkill_m1619177721 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::Init(System.String[])
extern "C"  void FightCtrl_Init_m3151747612 (FightCtrl_t648967803 * __this, StringU5BU5D_t4054002952* ___skillIDs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::AddSkill(System.Int32[])
extern "C"  void FightCtrl_AddSkill_m3966805971 (FightCtrl_t648967803 * __this, Int32U5BU5D_t3230847821* ___skillIDS0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::TeamAddSkill(System.Int32[])
extern "C"  void FightCtrl_TeamAddSkill_m1339209168 (FightCtrl_t648967803 * __this, Int32U5BU5D_t3230847821* ___skillIDS0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ForceAddSkill(System.Int32)
extern "C"  void FightCtrl_ForceAddSkill_m3445149882 (FightCtrl_t648967803 * __this, int32_t ___skillID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::AddSkill(System.Int32)
extern "C"  void FightCtrl_AddSkill_m2095745525 (FightCtrl_t648967803 * __this, int32_t ___skillID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::AwakAddSkillBuff()
extern "C"  void FightCtrl_AwakAddSkillBuff_m746621527 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::DeathUsingSkill()
extern "C"  void FightCtrl_DeathUsingSkill_m3776310191 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Skill FightCtrl::GetSkill(System.Int32)
extern "C"  Skill_t79944241 * FightCtrl_GetSkill_m1139391902 (FightCtrl_t648967803 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ReduceCDBySkillID(System.Int32,System.Int32)
extern "C"  void FightCtrl_ReduceCDBySkillID_m3532449002 (FightCtrl_t648967803 * __this, int32_t ___id0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::DelSkill(System.Int32[])
extern "C"  void FightCtrl_DelSkill_m1591001769 (FightCtrl_t648967803 * __this, Int32U5BU5D_t3230847821* ___skillIDS0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::DelSkill(System.Int32)
extern "C"  void FightCtrl_DelSkill_m1512268363 (FightCtrl_t648967803 * __this, int32_t ___skillID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::Update()
extern "C"  void FightCtrl_Update_m2796176317 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::Fight()
extern "C"  void FightCtrl_Fight_m850592382 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightCtrl::GetNextSkillIndex()
extern "C"  int32_t FightCtrl_GetNextSkillIndex_m823551814 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightEnum.EFightRst FightCtrl::CanUseSkill(System.Int32)
extern "C"  int32_t FightCtrl_CanUseSkill_m2886369153 (FightCtrl_t648967803 * __this, int32_t ___skillIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::SetSkillIndex(System.Int32&,System.Int32&,System.Int32)
extern "C"  void FightCtrl_SetSkillIndex_m102050392 (FightCtrl_t648967803 * __this, int32_t* ___skillIndex0, int32_t* ___retSkillIndex1, int32_t ___newSkillIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::UpdateRealUseSkill()
extern "C"  void FightCtrl_UpdateRealUseSkill_m2670359717 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::RealUseSkill()
extern "C"  void FightCtrl_RealUseSkill_m3925968316 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::UpdateCalcuSkillRst()
extern "C"  void FightCtrl_UpdateCalcuSkillRst_m673943111 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::UpdateCalcuShootSkillRst()
extern "C"  void FightCtrl_UpdateCalcuShootSkillRst_m325556030 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::CalcuSkillRst(System.Boolean)
extern "C"  void FightCtrl_CalcuSkillRst_m1681652103 (FightCtrl_t648967803 * __this, bool ___isNormalSkill0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::SummonMonsterAction()
extern "C"  void FightCtrl_SummonMonsterAction_m878710589 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightCtrl::GetSummonMonsterRot(System.Int32,UnityEngine.Vector3)
extern "C"  float FightCtrl_GetSummonMonsterRot_m445384370 (FightCtrl_t648967803 * __this, int32_t ___summonidTarget0, Vector3_t4282066566  ___targetPos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightCtrl::VectorAngle(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float FightCtrl_VectorAngle_m990480610 (FightCtrl_t648967803 * __this, Vector3_t4282066566  ___from0, Vector3_t4282066566  ___to1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::SummonMonster(Skill,UnityEngine.Vector3,System.Int32[])
extern "C"  void FightCtrl_SummonMonster_m1761548776 (FightCtrl_t648967803 * __this, Skill_t79944241 * ___skill0, Vector3_t4282066566  ___targetPos1, Int32U5BU5D_t3230847821* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::SkillShift(skillCfg,FightEnum.EMoveEvent,System.Collections.Generic.List`1<CombatEntity>)
extern "C"  void FightCtrl_SkillShift_m3201459574 (FightCtrl_t648967803 * __this, skillCfg_t2142425171 * ___skillInfo0, int32_t ___moveEvent1, List_1_t2052323047 * ___hitEntitys2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::SkillEffect(Skill,FightEnum.ESkillStateEvent,DamageSeparationSkill)
extern "C"  void FightCtrl_SkillEffect_m3884145363 (FightCtrl_t648967803 * __this, Skill_t79944241 * ___skill0, int32_t ____skillEffectState1, DamageSeparationSkill_t2001134364 * ___skillShoot2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ShootEffect(System.Int32,Skill,UnityEngine.Vector3)
extern "C"  void FightCtrl_ShootEffect_m1047197373 (FightCtrl_t648967803 * __this, int32_t ___effID0, Skill_t79944241 * ___skill1, Vector3_t4282066566  ___pos2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ShootEffect(System.Int32,Skill,UnityEngine.Vector3,CombatEntity)
extern "C"  void FightCtrl_ShootEffect_m1946708018 (FightCtrl_t648967803 * __this, int32_t ___effID0, Skill_t79944241 * ___skill1, Vector3_t4282066566  ___pos2, CombatEntity_t684137495 * ___targetTf3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::UpdatePostRealUseSkill()
extern "C"  void FightCtrl_UpdatePostRealUseSkill_m2853335909 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::SkillClear()
extern "C"  void FightCtrl_SkillClear_m1520754480 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::UpdateSkillAction()
extern "C"  void FightCtrl_UpdateSkillAction_m4062101196 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::UpdatePlaySound()
extern "C"  void FightCtrl_UpdatePlaySound_m812502304 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::UpdateProgressSound()
extern "C"  void FightCtrl_UpdateProgressSound_m2656209415 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::BreakSkill(System.Boolean)
extern "C"  void FightCtrl_BreakSkill_m1725159101 (FightCtrl_t648967803 * __this, bool ___isForce0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::BreakBuff()
extern "C"  void FightCtrl_BreakBuff_m1548185952 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::RemoveLoopEff()
extern "C"  void FightCtrl_RemoveLoopEff_m808767403 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::SetDelayPauseState(System.Boolean)
extern "C"  void FightCtrl_SetDelayPauseState_m1885325447 (FightCtrl_t648967803 * __this, bool ___isPause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightEnum.EFightRst FightCtrl::SetNextSkillID(System.Int32,System.Boolean)
extern "C"  int32_t FightCtrl_SetNextSkillID_m3877485769 (FightCtrl_t648967803 * __this, int32_t ___skillid0, bool ___isClearCD1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightEnum.EFightRst FightCtrl::SetNextSkillIDByAI(System.Int32)
extern "C"  int32_t FightCtrl_SetNextSkillIDByAI_m2776171475 (FightCtrl_t648967803 * __this, int32_t ___skillid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightEnum.EFightRst FightCtrl::UseSkillByID(System.Int32)
extern "C"  int32_t FightCtrl_UseSkillByID_m203942681 (FightCtrl_t648967803 * __this, int32_t ___skillID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightEnum.EFightRst FightCtrl::UseSkill(System.Int32)
extern "C"  int32_t FightCtrl_UseSkill_m246052167 (FightCtrl_t648967803 * __this, int32_t ___skillIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ForceUsingSkill(System.Int32,System.Int32,System.Int32)
extern "C"  void FightCtrl_ForceUsingSkill_m3197244567 (FightCtrl_t648967803 * __this, int32_t ___skillId0, int32_t ___hostId1, int32_t ___effectId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::UseSkillUI(System.Int32,System.Action)
extern "C"  void FightCtrl_UseSkillUI_m3142048026 (FightCtrl_t648967803 * __this, int32_t ___index0, Action_t3771233898 * ___call1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrl::UseSkillAuto(Skill,System.Action)
extern "C"  bool FightCtrl_UseSkillAuto_m257055903 (FightCtrl_t648967803 * __this, Skill_t79944241 * ___skill0, Action_t3771233898 * ___call1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrl::ActiveSkillFillConsume(Skill)
extern "C"  bool FightCtrl_ActiveSkillFillConsume_m2774100435 (FightCtrl_t648967803 * __this, Skill_t79944241 * ___skill0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrl::ActiveSkillForbid(Skill)
extern "C"  bool FightCtrl_ActiveSkillForbid_m1900308992 (FightCtrl_t648967803 * __this, Skill_t79944241 * ___skill0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::UseingSkill()
extern "C"  void FightCtrl_UseingSkill_m2289998148 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ChangeTarget(Skill)
extern "C"  void FightCtrl_ChangeTarget_m2629795756 (FightCtrl_t648967803 * __this, Skill_t79944241 * ___skill0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::SkillShake(skillCfg)
extern "C"  void FightCtrl_SkillShake_m2976049078 (FightCtrl_t648967803 * __this, skillCfg_t2142425171 * ___mpJson0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::SkillCastingEffect(Skill)
extern "C"  void FightCtrl_SkillCastingEffect_m676165674 (FightCtrl_t648967803 * __this, Skill_t79944241 * ___skill0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::SkillHitEffect(Skill)
extern "C"  void FightCtrl_SkillHitEffect_m3650250202 (FightCtrl_t648967803 * __this, Skill_t79944241 * ___skill0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::GetSkillHitList(DamageSeparationSkill)
extern "C"  void FightCtrl_GetSkillHitList_m281597234 (FightCtrl_t648967803 * __this, DamageSeparationSkill_t2001134364 * ___damageSeparationSkill0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::PlayAction()
extern "C"  void FightCtrl_PlayAction_m4118612926 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::PlaySound()
extern "C"  void FightCtrl_PlaySound_m2320706409 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrl::HasSkillUseState(FightEnum.ESkillState)
extern "C"  bool FightCtrl_HasSkillUseState_m1706081161 (FightCtrl_t648967803 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::SetSkillUseState(FightEnum.ESkillState)
extern "C"  void FightCtrl_SetSkillUseState_m1891780509 (FightCtrl_t648967803 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ClearSkillUseState(FightEnum.ESkillState)
extern "C"  void FightCtrl_ClearSkillUseState_m1945714888 (FightCtrl_t648967803 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ClearAllSkillUseState()
extern "C"  void FightCtrl_ClearAllSkillUseState_m2842565557 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Skill FightCtrl::FindSkillByIndex(System.Int32)
extern "C"  Skill_t79944241 * FightCtrl_FindSkillByIndex_m1168088454 (FightCtrl_t648967803 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::Clear()
extern "C"  void FightCtrl_Clear_m2566878587 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ClearmLastUseTime()
extern "C"  void FightCtrl_ClearmLastUseTime_m3782717708 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightCtrl::GetCurSkillID()
extern "C"  int32_t FightCtrl_GetCurSkillID_m4274982462 (FightCtrl_t648967803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitType FightCtrl::ilo_get_unitType1(CombatEntity)
extern "C"  int32_t FightCtrl_ilo_get_unitType1_m2535762937 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrl::ilo_get_IsUpgrade2(CSSkillData)
extern "C"  bool FightCtrl_ilo_get_IsUpgrade2_m1554655723 (Il2CppObject * __this /* static, unused */, CSSkillData_t432288523 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrl::ilo_get_isActive3(Skill)
extern "C"  bool FightCtrl_ilo_get_isActive3_m2711441112 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_set_activeSkill4(FightCtrl,Skill)
extern "C"  void FightCtrl_ilo_set_activeSkill4_m2491167461 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, Skill_t79944241 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Skill FightCtrl::ilo_get_wakeSkill5(FightCtrl)
extern "C"  Skill_t79944241 * FightCtrl_ilo_get_wakeSkill5_m4044540929 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_AddSkillBuff6(BuffCtrl,Skill,FightEnum.ESkillBuffEvent,System.Boolean)
extern "C"  void FightCtrl_ilo_AddSkillBuff6_m3071240900 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, Skill_t79944241 * ___skill1, uint8_t ___buffEvent2, bool ___isMutex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Skill FightCtrl::ilo_get_deathSkill7(FightCtrl)
extern "C"  Skill_t79944241 * FightCtrl_ilo_get_deathSkill7_m495180073 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightCtrl::ilo_get_index8(Skill)
extern "C"  int32_t FightCtrl_ilo_get_index8_m2284758969 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_LogError9(System.Object,System.Boolean)
extern "C"  void FightCtrl_ilo_LogError9_m902061849 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightCtrl::ilo_get_mTplID10(Skill)
extern "C"  int32_t FightCtrl_ilo_get_mTplID10_m876827906 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrl::ilo_get_canAttack11(CombatEntity)
extern "C"  bool FightCtrl_ilo_get_canAttack11_m3108473639 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrl::ilo_get_isUsingSkill12(FightCtrl)
extern "C"  bool FightCtrl_ilo_get_isUsingSkill12_m1496872419 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightCtrl::ilo_GetNextSkillIndex13(FightCtrl)
extern "C"  int32_t FightCtrl_ilo_GetNextSkillIndex13_m1590092002 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_FloatText14(FloatTextMgr,FLOAT_TEXT_ID,System.Int32,System.Object[])
extern "C"  void FightCtrl_ilo_FloatText14_m254759241 (Il2CppObject * __this /* static, unused */, FloatTextMgr_t630384591 * ____this0, int32_t ___textId1, int32_t ___offsetY2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrl::ilo_get_isInCD15(Skill)
extern "C"  bool FightCtrl_ilo_get_isInCD15_m2477798993 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// skillCfg FightCtrl::ilo_get_mpJson16(Skill)
extern "C"  skillCfg_t2142425171 * FightCtrl_ilo_get_mpJson16_m1724165003 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightCtrl::ilo_get_maxRage17(CombatEntity)
extern "C"  float FightCtrl_ilo_get_maxRage17_m2721880222 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitCamp FightCtrl::ilo_get_unitCamp18(CombatEntity)
extern "C"  int32_t FightCtrl_ilo_get_unitCamp18_m821215681 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_SetSkillIndex19(FightCtrl,System.Int32&,System.Int32&,System.Int32)
extern "C"  void FightCtrl_ilo_SetSkillIndex19_m4137410300 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, int32_t* ___skillIndex1, int32_t* ___retSkillIndex2, int32_t ___newSkillIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrl::ilo_get_isChangeSkill20(CombatEntity)
extern "C"  bool FightCtrl_ilo_get_isChangeSkill20_m2230421764 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrl::ilo_ActiveSkillForbid21(FightCtrl,Skill)
extern "C"  bool FightCtrl_ilo_ActiveSkillForbid21_m1441880357 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, Skill_t79944241 * ___skill1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 FightCtrl::ilo_get_position22(CombatEntity)
extern "C"  Vector3_t4282066566  FightCtrl_ilo_get_position22_m2490196744 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrl::ilo_CheckUseDistance23(Skill,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  bool FightCtrl_ilo_CheckUseDistance23_m907569092 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, Vector3_t4282066566  ___selfPos1, Vector3_t4282066566  ___targetPos2, float ___halfwidth3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID FightCtrl::ilo_get_curBehaviorID24(Entity.Behavior.IBehaviorCtrl)
extern "C"  uint8_t FightCtrl_ilo_get_curBehaviorID24_m2010390442 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrl::ilo_HasSkillUseState25(FightCtrl,FightEnum.ESkillState)
extern "C"  bool FightCtrl_ilo_HasSkillUseState25_m1072511076 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, int32_t ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_GetSkillHitList26(FightCtrl,DamageSeparationSkill)
extern "C"  void FightCtrl_ilo_GetSkillHitList26_m2338550322 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, DamageSeparationSkill_t2001134364 * ___damageSeparationSkill1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_CalcuSkillRst27(FightCtrl,System.Boolean)
extern "C"  void FightCtrl_ilo_CalcuSkillRst27_m2017649288 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, bool ___isNormalSkill1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] FightCtrl::ilo_get_activeSpacing28(Skill)
extern "C"  SingleU5BU5D_t2316563989* FightCtrl_ilo_get_activeSpacing28_m270560610 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrl::ilo_get_isDeath29(CombatEntity)
extern "C"  bool FightCtrl_ilo_get_isDeath29_m4270790016 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_AddSkillHurt30(SkillHurtShow,CombatEntity,CombatEntity,System.Int32,System.Int32)
extern "C"  void FightCtrl_ilo_AddSkillHurt30_m1663293202 (Il2CppObject * __this /* static, unused */, SkillHurtShow_t1728264445 * ____this0, CombatEntity_t684137495 * ___hero1, CombatEntity_t684137495 * ___monster2, int32_t ___hurt3, int32_t ___skillId4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_AddHP31(CombatEntity,System.Single)
extern "C"  void FightCtrl_ilo_AddHP31_m2943772816 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___delta1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Skill FightCtrl::ilo_get_normalAttackSkill32(FightCtrl)
extern "C"  Skill_t79944241 * FightCtrl_ilo_get_normalAttackSkill32_m1748489730 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrl::ilo_get_isAddedAnger33(Skill)
extern "C"  bool FightCtrl_ilo_get_isAddedAnger33_m1580228820 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightCtrl::ilo_get_AddAngerRatio34(CombatEntity)
extern "C"  float FightCtrl_ilo_get_AddAngerRatio34_m1355031725 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_SkillShift35(FightCtrl,skillCfg,FightEnum.EMoveEvent,System.Collections.Generic.List`1<CombatEntity>)
extern "C"  void FightCtrl_ilo_SkillShift35_m1323262930 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, skillCfg_t2142425171 * ___skillInfo1, int32_t ___moveEvent2, List_1_t2052323047 * ___hitEntitys3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] FightCtrl::ilo_get_summonid36(Skill)
extern "C"  Int32U5BU5D_t3230847821* FightCtrl_ilo_get_summonid36_m437089022 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_set_isFingdPos37(Skill,System.Boolean)
extern "C"  void FightCtrl_ilo_set_isFingdPos37_m2397190458 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_SkillEffect38(FightCtrl,Skill,FightEnum.ESkillStateEvent,DamageSeparationSkill)
extern "C"  void FightCtrl_ilo_SkillEffect38_m2730548788 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, Skill_t79944241 * ___skill1, int32_t ____skillEffectState2, DamageSeparationSkill_t2001134364 * ___skillShoot3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] FightCtrl::ilo_get_summonTime39(Skill)
extern "C"  SingleU5BU5D_t2316563989* FightCtrl_ilo_get_summonTime39_m3295359431 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity FightCtrl::ilo_get_atkTarget40(CombatEntity)
extern "C"  CombatEntity_t684137495 * FightCtrl_ilo_get_atkTarget40_m2671175654 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightCtrl::ilo_VectorAngle41(FightCtrl,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float FightCtrl_ilo_VectorAngle41_m1735353737 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, Vector3_t4282066566  ___from1, Vector3_t4282066566  ___to2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightCtrl::ilo_get_level42(CombatEntity)
extern "C"  int32_t FightCtrl_ilo_get_level42_m2017685547 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_set_x43(JSCLevelMonsterConfig,System.Single)
extern "C"  void FightCtrl_ilo_set_x43_m1934553168 (Il2CppObject * __this /* static, unused */, JSCLevelMonsterConfig_t1924079698 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_set_rot44(JSCLevelMonsterConfig,System.Single)
extern "C"  void FightCtrl_ilo_set_rot44_m1218617840 (Il2CppObject * __this /* static, unused */, JSCLevelMonsterConfig_t1924079698 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_set_z45(JSCLevelMonsterConfig,System.Single)
extern "C"  void FightCtrl_ilo_set_z45_m3828353868 (Il2CppObject * __this /* static, unused */, JSCLevelMonsterConfig_t1924079698 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightCtrl::ilo_get_mapEditorType46(Monster)
extern "C"  int32_t FightCtrl_ilo_get_mapEditorType46_m2822204339 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NpcMgr FightCtrl::ilo_get_instance47()
extern "C"  NpcMgr_t2339534743 * FightCtrl_ilo_get_instance47_m966788322 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrl::ilo_get_isFingdPos48(Skill)
extern "C"  bool FightCtrl_ilo_get_isFingdPos48_m270052459 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_SetMosterAttribute49(Monster,System.String,System.Int32)
extern "C"  void FightCtrl_ilo_SetMosterAttribute49_m4127146649 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, String_t* ___key1, int32_t ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FightCtrl::ilo_get_callAttributeKey150(Skill)
extern "C"  String_t* FightCtrl_ilo_get_callAttributeKey150_m2789020449 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightCtrl::ilo_get_callAttributeValue251(Skill)
extern "C"  int32_t FightCtrl_ilo_get_callAttributeValue251_m4198869734 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReplayMgr FightCtrl::ilo_get_Instance52()
extern "C"  ReplayMgr_t1549183121 * FightCtrl_ilo_get_Instance52_m2507041920 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrl::ilo_get_forceShift53(CombatEntity)
extern "C"  bool FightCtrl_ilo_get_forceShift53_m4026030554 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] FightCtrl::ilo_get_effcetEvent54(Skill)
extern "C"  Int32U5BU5D_t3230847821* FightCtrl_ilo_get_effcetEvent54_m539961655 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSDatacfgManager FightCtrl::ilo_get_CfgDataMgr55()
extern "C"  CSDatacfgManager_t1565254243 * FightCtrl_ilo_get_CfgDataMgr55_m2387292064 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightCtrl::ilo_get_atkSpeed56(CombatEntity)
extern "C"  float FightCtrl_ilo_get_atkSpeed56_m3857591933 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_ShootEffect57(FightCtrl,System.Int32,Skill,UnityEngine.Vector3)
extern "C"  void FightCtrl_ilo_ShootEffect57_m154005019 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, int32_t ___effID1, Skill_t79944241 * ___skill2, Vector3_t4282066566  ___pos3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_ShootEffect58(FightCtrl,System.Int32,Skill,UnityEngine.Vector3,CombatEntity)
extern "C"  void FightCtrl_ilo_ShootEffect58_m534764915 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, int32_t ___effID1, Skill_t79944241 * ___skill2, Vector3_t4282066566  ___pos3, CombatEntity_t684137495 * ___targetTf4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] FightCtrl::ilo_get_effectTime59(Skill)
extern "C"  SingleU5BU5D_t2316563989* FightCtrl_ilo_get_effectTime59_m4241241369 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EffectMgr FightCtrl::ilo_get_EffectMgr60()
extern "C"  EffectMgr_t535289511 * FightCtrl_ilo_get_EffectMgr60_m4199652543 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_PlayEffect61(EffectMgr,EffectPara)
extern "C"  void FightCtrl_ilo_PlayEffect61_m2904393777 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, EffectPara_t3709156657 * ___paras1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// effectCfg FightCtrl::ilo_GeteffectCfg62(CSDatacfgManager,System.Int32)
extern "C"  effectCfg_t2826279187 * FightCtrl_ilo_GeteffectCfg62_m1999118074 (Il2CppObject * __this /* static, unused */, CSDatacfgManager_t1565254243 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitTag FightCtrl::ilo_get_atkUnitTag63(CombatEntity)
extern "C"  int32_t FightCtrl_ilo_get_atkUnitTag63_m3885908995 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_PlayShoot64(EffectMgr,System.Int32,CombatEntity,CombatEntity,System.Single,System.Boolean,UnityEngine.Vector3,EntityEnum.UnitTag,System.Int32)
extern "C"  void FightCtrl_ilo_PlayShoot64_m2035811672 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, int32_t ___id1, CombatEntity_t684137495 * ___srcEntity2, CombatEntity_t684137495 * ___targetTf3, float ___speed4, bool ___isatk5, Vector3_t4282066566  ___targetPosition6, int32_t ___tag7, int32_t ___skillID8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AnimationRunner FightCtrl::ilo_get_characterAnim65(CombatEntity)
extern "C"  AnimationRunner_t1015409588 * FightCtrl_ilo_get_characterAnim65_m2173354451 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_Play66(AnimationRunner,System.String,System.Single,System.Boolean)
extern "C"  void FightCtrl_ilo_Play66_m2504685913 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, String_t* ___aniName1, float ___playTime2, bool ___isSkill3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_DispatchEvent67(CEvent.ZEvent)
extern "C"  void FightCtrl_ilo_DispatchEvent67_m2002781023 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] FightCtrl::ilo_get_castingEffect68(Skill)
extern "C"  Int32U5BU5D_t3230847821* FightCtrl_ilo_get_castingEffect68_m3626722157 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] FightCtrl::ilo_get_effect69(Skill)
extern "C"  Int32U5BU5D_t3230847821* FightCtrl_ilo_get_effect69_m1790620073 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_ClearLoopEffect70(EffectMgr,System.Int32,System.Int32)
extern "C"  void FightCtrl_ilo_ClearLoopEffect70_m286188501 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, int32_t ___srcID1, int32_t ___effectID2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_ClearSkillUseState71(FightCtrl,FightEnum.ESkillState)
extern "C"  void FightCtrl_ilo_ClearSkillUseState71_m2993887276 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, int32_t ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_Hide72(ProgressBar)
extern "C"  void FightCtrl_ilo_Hide72_m1986381928 (Il2CppObject * __this /* static, unused */, ProgressBar_t2799378054 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Blood FightCtrl::ilo_get_blood73(CombatEntity)
extern "C"  Blood_t64280026 * FightCtrl_ilo_get_blood73_m1834191530 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrl::ilo_get_antiBreak74(CombatEntity)
extern "C"  bool FightCtrl_ilo_get_antiBreak74_m4268328873 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_SkillClear75(FightCtrl)
extern "C"  void FightCtrl_ilo_SkillClear75_m2324821494 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] FightCtrl::ilo_get_buffid76(Skill)
extern "C"  Int32U5BU5D_t3230847821* FightCtrl_ilo_get_buffid76_m910094224 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightEnum.ESkillBuffTarget[] FightCtrl::ilo_get_bufftarget77(Skill)
extern "C"  ESkillBuffTargetU5BU5D_t2691263849* FightCtrl_ilo_get_bufftarget77_m2761210267 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrl::ilo_get_disNormalattack78(CombatEntity)
extern "C"  bool FightCtrl_ilo_get_disNormalattack78_m566852557 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl FightCtrl::ilo_get_bvrCtrl79(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * FightCtrl_ilo_get_bvrCtrl79_m3398117188 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_SetSkillUseState80(FightCtrl,FightEnum.ESkillState)
extern "C"  void FightCtrl_ilo_SetSkillUseState80_m1677978211 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, int32_t ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_UseingSkill81(FightCtrl)
extern "C"  void FightCtrl_ilo_UseingSkill81_m3244229165 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity FightCtrl::ilo_GetEntityByID82(GameMgr,System.Boolean,System.Int32)
extern "C"  CombatEntity_t684137495 * FightCtrl_ilo_GetEntityByID82_m2537037120 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, bool ___isHero1, int32_t ___id2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_set_atkTarget83(CombatEntity,CombatEntity)
extern "C"  void FightCtrl_ilo_set_atkTarget83_m421635460 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, CombatEntity_t684137495 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject FightCtrl::ilo_SetCameraShotViewEffect84(EffectMgr,System.Int32,System.Int32)
extern "C"  GameObject_t3674682005 * FightCtrl_ilo_SetCameraShotViewEffect84_m1795185576 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, int32_t ___srcID1, int32_t ___effid2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightCtrl::ilo_get_id85(CombatEntity)
extern "C"  int32_t FightCtrl_ilo_get_id85_m874203299 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTextMgr FightCtrl::ilo_get_FloatTextMgr86()
extern "C"  FloatTextMgr_t630384591 * FightCtrl_ilo_get_FloatTextMgr86_m82903119 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FightCtrl::ilo_FormatUIString87(System.Int32,System.Object[])
extern "C"  String_t* FightCtrl_ilo_FormatUIString87_m86353116 (Il2CppObject * __this /* static, unused */, int32_t ___key0, ObjectU5BU5D_t1108656482* ___datas1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightCtrl::ilo_get_maxHp88(CombatEntity)
extern "C"  float FightCtrl_ilo_get_maxHp88_m434668531 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightEnum.EFightRst FightCtrl::ilo_UseSkill89(FightCtrl,System.Int32)
extern "C"  int32_t FightCtrl_ilo_UseSkill89_m2591044170 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, int32_t ___skillIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_set_rage90(CombatEntity,System.Single)
extern "C"  void FightCtrl_ilo_set_rage90_m3888769686 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_Log91(System.Object,System.Boolean)
extern "C"  void FightCtrl_ilo_Log91_m3979899808 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CameraShotMgr FightCtrl::ilo_get_CameraShotMgr92()
extern "C"  CameraShotMgr_t1580128697 * FightCtrl_ilo_get_CameraShotMgr92_m3446296478 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_Init93(ProgressBar,System.Boolean)
extern "C"  void FightCtrl_ilo_Init93_m3060290850 (Il2CppObject * __this /* static, unused */, ProgressBar_t2799378054 * ____this0, bool ___isbreak1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_HideFocus94(Blood)
extern "C"  void FightCtrl_ilo_HideFocus94_m1362792108 (Il2CppObject * __this /* static, unused */, Blood_t64280026 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_SkillCastingEffect95(FightCtrl,Skill)
extern "C"  void FightCtrl_ilo_SkillCastingEffect95_m4172143372 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, Skill_t79944241 * ___skill1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_CheckFierceWind96(BuffCtrl,Skill)
extern "C"  void FightCtrl_ilo_CheckFierceWind96_m1970373559 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, Skill_t79944241 * ___skill1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroMgr FightCtrl::ilo_get_instance97()
extern "C"  HeroMgr_t2475965342 * FightCtrl_ilo_get_instance97_m1993554158 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero FightCtrl::ilo_get_captain98(HeroMgr)
extern "C"  Hero_t2245658 * FightCtrl_ilo_get_captain98_m487690460 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity FightCtrl::ilo_GetMinHatredEntity99(HatredCtrl,Skill)
extern "C"  CombatEntity_t684137495 * FightCtrl_ilo_GetMinHatredEntity99_m1310072528 (Il2CppObject * __this /* static, unused */, HatredCtrl_t891253697 * ____this0, Skill_t79944241 * ___skill1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightCtrl::ilo_get_atkPower100(CombatEntity)
extern "C"  float FightCtrl_ilo_get_atkPower100_m2871329643 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_LookAt101(CombatEntity,UnityEngine.Vector3)
extern "C"  void FightCtrl_ilo_LookAt101_m2894352951 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, Vector3_t4282066566  ___pos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] FightCtrl::ilo_get_castingEffectTime102(Skill)
extern "C"  SingleU5BU5D_t2316563989* FightCtrl_ilo_get_castingEffectTime102_m4019158409 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrl::ilo_isInBlackByEffectID103(Skill,System.Int32)
extern "C"  bool FightCtrl_ilo_isInBlackByEffectID103_m2409759722 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, int32_t ___effectID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] FightCtrl::ilo_get_castingEffectPlaySpeed104(Skill)
extern "C"  SingleU5BU5D_t2316563989* FightCtrl_ilo_get_castingEffectPlaySpeed104_m1720027055 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightCtrl::ilo_get_sputter_radius105(Skill)
extern "C"  float FightCtrl_ilo_get_sputter_radius105_m1462489337 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_CircleHit106(UnityEngine.Vector3,System.Single,CombatEntity,System.Collections.Generic.List`1<CombatEntity>,System.Boolean)
extern "C"  void FightCtrl_ilo_CircleHit106_m2874419118 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___centre0, float ___radius1, CombatEntity_t684137495 * ___entity2, List_1_t2052323047 * ___hitList3, bool ___iswish4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_CrossHit107(System.Single,System.Single,System.Single,CombatEntity,System.Collections.Generic.List`1<CombatEntity>,System.Boolean)
extern "C"  void FightCtrl_ilo_CrossHit107_m4243048160 (Il2CppObject * __this /* static, unused */, float ___radius0, float ___length1, float ___width2, CombatEntity_t684137495 * ___entity3, List_1_t2052323047 * ___hitList4, bool ___iswish5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrl::ilo_get_isNotSelected108(CombatEntity)
extern "C"  bool FightCtrl_ilo_get_isNotSelected108_m4186939290 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> FightCtrl::ilo_GetAllAlly109(GameMgr,CombatEntity,System.Boolean)
extern "C"  List_1_t2052323047 * FightCtrl_ilo_GetAllAlly109_m3263698686 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, CombatEntity_t684137495 * ___entity1, bool ___isshaix2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightCtrl::ilo_get_PerHP110(CombatEntity)
extern "C"  int32_t FightCtrl_ilo_get_PerHP110_m2439007926 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightCtrl::ilo_get_PD111(CombatEntity)
extern "C"  float FightCtrl_ilo_get_PD111_m1699453828 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrl::ilo_get_isThumpState112(CombatEntity)
extern "C"  bool FightCtrl_ilo_get_isThumpState112_m371750378 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightCtrl::ilo_get_thumpAddValue113(CombatEntity)
extern "C"  float FightCtrl_ilo_get_thumpAddValue113_m1376746700 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_FreezeSkill114(GameMgr,CombatEntity,System.Boolean)
extern "C"  void FightCtrl_ilo_FreezeSkill114_m2186119801 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, CombatEntity_t684137495 * ___src1, bool ___bFreeze2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrl::ilo_Clear115(ProgressBar)
extern "C"  void FightCtrl_ilo_Clear115_m2356328125 (Il2CppObject * __this /* static, unused */, ProgressBar_t2799378054 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
