﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HttpsWebRequest
struct  HttpsWebRequest_t3736652742  : public Il2CppObject
{
public:

public:
};

struct HttpsWebRequest_t3736652742_StaticFields
{
public:
	// System.String HttpsWebRequest::DefaultUserAgent
	String_t* ___DefaultUserAgent_0;
	// System.Boolean HttpsWebRequest::IsHTTPS
	bool ___IsHTTPS_1;

public:
	inline static int32_t get_offset_of_DefaultUserAgent_0() { return static_cast<int32_t>(offsetof(HttpsWebRequest_t3736652742_StaticFields, ___DefaultUserAgent_0)); }
	inline String_t* get_DefaultUserAgent_0() const { return ___DefaultUserAgent_0; }
	inline String_t** get_address_of_DefaultUserAgent_0() { return &___DefaultUserAgent_0; }
	inline void set_DefaultUserAgent_0(String_t* value)
	{
		___DefaultUserAgent_0 = value;
		Il2CppCodeGenWriteBarrier(&___DefaultUserAgent_0, value);
	}

	inline static int32_t get_offset_of_IsHTTPS_1() { return static_cast<int32_t>(offsetof(HttpsWebRequest_t3736652742_StaticFields, ___IsHTTPS_1)); }
	inline bool get_IsHTTPS_1() const { return ___IsHTTPS_1; }
	inline bool* get_address_of_IsHTTPS_1() { return &___IsHTTPS_1; }
	inline void set_IsHTTPS_1(bool value)
	{
		___IsHTTPS_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
