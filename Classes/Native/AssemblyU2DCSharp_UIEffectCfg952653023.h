﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIEffectCfg
struct  UIEffectCfg_t952653023  : public CsCfgBase_t69924517
{
public:
	// System.Int32 UIEffectCfg::id
	int32_t ___id_0;
	// System.String UIEffectCfg::name
	String_t* ___name_1;
	// System.String UIEffectCfg::rageres
	String_t* ___rageres_2;
	// System.Single UIEffectCfg::lasttime
	float ___lasttime_3;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(UIEffectCfg_t952653023, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(UIEffectCfg_t952653023, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_rageres_2() { return static_cast<int32_t>(offsetof(UIEffectCfg_t952653023, ___rageres_2)); }
	inline String_t* get_rageres_2() const { return ___rageres_2; }
	inline String_t** get_address_of_rageres_2() { return &___rageres_2; }
	inline void set_rageres_2(String_t* value)
	{
		___rageres_2 = value;
		Il2CppCodeGenWriteBarrier(&___rageres_2, value);
	}

	inline static int32_t get_offset_of_lasttime_3() { return static_cast<int32_t>(offsetof(UIEffectCfg_t952653023, ___lasttime_3)); }
	inline float get_lasttime_3() const { return ___lasttime_3; }
	inline float* get_address_of_lasttime_3() { return &___lasttime_3; }
	inline void set_lasttime_3(float value)
	{
		___lasttime_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
