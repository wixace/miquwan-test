﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_MarshalByRefObjectGenerated
struct System_MarshalByRefObjectGenerated_t4274197346;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_MarshalByRefObjectGenerated::.ctor()
extern "C"  void System_MarshalByRefObjectGenerated__ctor_m373451641 (System_MarshalByRefObjectGenerated_t4274197346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MarshalByRefObjectGenerated::MarshalByRefObject_CreateObjRef__Type(JSVCall,System.Int32)
extern "C"  bool System_MarshalByRefObjectGenerated_MarshalByRefObject_CreateObjRef__Type_m2751623655 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MarshalByRefObjectGenerated::MarshalByRefObject_GetLifetimeService(JSVCall,System.Int32)
extern "C"  bool System_MarshalByRefObjectGenerated_MarshalByRefObject_GetLifetimeService_m363146731 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MarshalByRefObjectGenerated::MarshalByRefObject_InitializeLifetimeService(JSVCall,System.Int32)
extern "C"  bool System_MarshalByRefObjectGenerated_MarshalByRefObject_InitializeLifetimeService_m63230889 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_MarshalByRefObjectGenerated::__Register()
extern "C"  void System_MarshalByRefObjectGenerated___Register_m1302615278 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_MarshalByRefObjectGenerated::ilo_setWhatever1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  void System_MarshalByRefObjectGenerated_ilo_setWhatever1_m350340960 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
