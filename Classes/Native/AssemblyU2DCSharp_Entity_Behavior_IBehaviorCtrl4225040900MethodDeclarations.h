﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// CombatEntity
struct CombatEntity_t684137495;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EGamePattern238230393.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"

// System.Void Entity.Behavior.IBehaviorCtrl::.ctor()
extern "C"  void IBehaviorCtrl__ctor_m1264058998 (IBehaviorCtrl_t4225040900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EGamePattern Entity.Behavior.IBehaviorCtrl::get_GamePattern()
extern "C"  int32_t IBehaviorCtrl_get_GamePattern_m1662280156 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.IBehaviorCtrl::set_GamePattern(Entity.Behavior.EGamePattern)
extern "C"  void IBehaviorCtrl_set_GamePattern_m4211653035 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehavior Entity.Behavior.IBehaviorCtrl::get_lastBvr()
extern "C"  IBehavior_t770859129 * IBehaviorCtrl_get_lastBvr_m2964127996 (IBehaviorCtrl_t4225040900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.IBehaviorCtrl::set_lastBvr(Entity.Behavior.IBehavior)
extern "C"  void IBehaviorCtrl_set_lastBvr_m815930697 (IBehaviorCtrl_t4225040900 * __this, IBehavior_t770859129 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.IBehaviorCtrl::set_curBehavior(Entity.Behavior.IBehavior)
extern "C"  void IBehaviorCtrl_set_curBehavior_m2174655871 (IBehaviorCtrl_t4225040900 * __this, IBehavior_t770859129 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehavior Entity.Behavior.IBehaviorCtrl::get_curBehavior()
extern "C"  IBehavior_t770859129 * IBehaviorCtrl_get_curBehavior_m682219910 (IBehaviorCtrl_t4225040900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.IBehaviorCtrl::set_curBehaviorID(Entity.Behavior.EBehaviorID)
extern "C"  void IBehaviorCtrl_set_curBehaviorID_m4122507149 (IBehaviorCtrl_t4225040900 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID Entity.Behavior.IBehaviorCtrl::get_curBehaviorID()
extern "C"  uint8_t IBehaviorCtrl_get_curBehaviorID_m2471090922 (IBehaviorCtrl_t4225040900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.IBehaviorCtrl::ChangeGamePatern(Entity.Behavior.EGamePattern)
extern "C"  void IBehaviorCtrl_ChangeGamePatern_m2729332868 (Il2CppObject * __this /* static, unused */, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.IBehaviorCtrl::FixedUpdate()
extern "C"  void IBehaviorCtrl_FixedUpdate_m1844948849 (IBehaviorCtrl_t4225040900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.IBehaviorCtrl::SetCurBehavior(Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void IBehaviorCtrl_SetCurBehavior_m1048074335 (IBehaviorCtrl_t4225040900 * __this, uint8_t ___id0, ObjectU5BU5D_t1108656482* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.IBehaviorCtrl::TranBehavior(Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void IBehaviorCtrl_TranBehavior_m3247864466 (IBehaviorCtrl_t4225040900 * __this, uint8_t ___id0, ObjectU5BU5D_t1108656482* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.IBehaviorCtrl::ChangeBehavior(Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void IBehaviorCtrl_ChangeBehavior_m2919496301 (IBehaviorCtrl_t4225040900 * __this, uint8_t ___id0, ObjectU5BU5D_t1108656482* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.IBehaviorCtrl::PauseBvr()
extern "C"  void IBehaviorCtrl_PauseBvr_m1188870230 (IBehaviorCtrl_t4225040900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.IBehaviorCtrl::PlayBvr()
extern "C"  void IBehaviorCtrl_PlayBvr_m1323090718 (IBehaviorCtrl_t4225040900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.IBehaviorCtrl::AddBehavior(Entity.Behavior.IBehavior)
extern "C"  void IBehaviorCtrl_AddBehavior_m2229941153 (IBehaviorCtrl_t4225040900 * __this, IBehavior_t770859129 * ___behavior0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.IBehaviorCtrl::DelBehavior(Entity.Behavior.EBehaviorID)
extern "C"  void IBehaviorCtrl_DelBehavior_m2908055872 (IBehaviorCtrl_t4225040900 * __this, uint8_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.IBehaviorCtrl::Clear()
extern "C"  void IBehaviorCtrl_Clear_m2965159585 (IBehaviorCtrl_t4225040900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Entity.Behavior.IBehaviorCtrl::ilo_IsLockingBehavior1(Entity.Behavior.IBehavior)
extern "C"  bool IBehaviorCtrl_ilo_IsLockingBehavior1_m1899596073 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Entity.Behavior.IBehaviorCtrl::ilo_get_isDeath2(CombatEntity)
extern "C"  bool IBehaviorCtrl_ilo_get_isDeath2_m1971008775 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.IBehaviorCtrl::ilo_ChangeBehavior3(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void IBehaviorCtrl_ilo_ChangeBehavior3_m2958152578 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, uint8_t ___id1, ObjectU5BU5D_t1108656482* ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.IBehaviorCtrl::ilo_set_curBehavior4(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.IBehavior)
extern "C"  void IBehaviorCtrl_ilo_set_curBehavior4_m4049669119 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, IBehavior_t770859129 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehavior Entity.Behavior.IBehaviorCtrl::ilo_get_curBehavior5(Entity.Behavior.IBehaviorCtrl)
extern "C"  IBehavior_t770859129 * IBehaviorCtrl_ilo_get_curBehavior5_m539857081 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.IBehaviorCtrl::ilo_set_lastBvr6(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.IBehavior)
extern "C"  void IBehaviorCtrl_ilo_set_lastBvr6_m2994216883 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, IBehavior_t770859129 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.IBehaviorCtrl::ilo_set_entity7(Entity.Behavior.IBehavior,CombatEntity)
extern "C"  void IBehaviorCtrl_ilo_set_entity7_m3733475103 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, CombatEntity_t684137495 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.IBehaviorCtrl::ilo_set_curBehaviorID8(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.EBehaviorID)
extern "C"  void IBehaviorCtrl_ilo_set_curBehaviorID8_m3546271263 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, uint8_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.IBehaviorCtrl::ilo_Pause9(Entity.Behavior.IBehavior)
extern "C"  void IBehaviorCtrl_ilo_Pause9_m315087652 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
