﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IO.DirectoryInfo
struct DirectoryInfo_t89154617;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.IO.FileInfo[]
struct FileInfoU5BU5D_t3358688287;
// System.IO.DirectoryInfo[]
struct DirectoryInfoU5BU5D_t2262552260;
// System.IO.FileSystemInfo[]
struct FileSystemInfoU5BU5D_t1190771700;
// System.Collections.ArrayList
struct ArrayList_t3948406897;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_IO_SearchOption4025787181.h"
#include "mscorlib_System_Collections_ArrayList3948406897.h"

// System.Void System.IO.DirectoryInfo::.ctor(System.String)
extern "C"  void DirectoryInfo__ctor_m4029233824 (DirectoryInfo_t89154617 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.DirectoryInfo::.ctor(System.String,System.Boolean)
extern "C"  void DirectoryInfo__ctor_m2875622621 (DirectoryInfo_t89154617 * __this, String_t* ___path0, bool ___simpleOriginalPath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.DirectoryInfo::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void DirectoryInfo__ctor_m944219619 (DirectoryInfo_t89154617 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.DirectoryInfo::Initialize()
extern "C"  void DirectoryInfo_Initialize_m287338546 (DirectoryInfo_t89154617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.DirectoryInfo::get_Exists()
extern "C"  bool DirectoryInfo_get_Exists_m1761829637 (DirectoryInfo_t89154617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.DirectoryInfo::get_Name()
extern "C"  String_t* DirectoryInfo_get_Name_m3299775219 (DirectoryInfo_t89154617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.DirectoryInfo System.IO.DirectoryInfo::get_Parent()
extern "C"  DirectoryInfo_t89154617 * DirectoryInfo_get_Parent_m1129672180 (DirectoryInfo_t89154617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.DirectoryInfo System.IO.DirectoryInfo::get_Root()
extern "C"  DirectoryInfo_t89154617 * DirectoryInfo_get_Root_m1604245100 (DirectoryInfo_t89154617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.DirectoryInfo::Create()
extern "C"  void DirectoryInfo_Create_m153588510 (DirectoryInfo_t89154617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.DirectoryInfo System.IO.DirectoryInfo::CreateSubdirectory(System.String)
extern "C"  DirectoryInfo_t89154617 * DirectoryInfo_CreateSubdirectory_m1247106360 (DirectoryInfo_t89154617 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileInfo[] System.IO.DirectoryInfo::GetFiles()
extern "C"  FileInfoU5BU5D_t3358688287* DirectoryInfo_GetFiles_m3669397613 (DirectoryInfo_t89154617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileInfo[] System.IO.DirectoryInfo::GetFiles(System.String)
extern "C"  FileInfoU5BU5D_t3358688287* DirectoryInfo_GetFiles_m2151933877 (DirectoryInfo_t89154617 * __this, String_t* ___searchPattern0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.DirectoryInfo[] System.IO.DirectoryInfo::GetDirectories()
extern "C"  DirectoryInfoU5BU5D_t2262552260* DirectoryInfo_GetDirectories_m1790857144 (DirectoryInfo_t89154617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.DirectoryInfo[] System.IO.DirectoryInfo::GetDirectories(System.String)
extern "C"  DirectoryInfoU5BU5D_t2262552260* DirectoryInfo_GetDirectories_m419732042 (DirectoryInfo_t89154617 * __this, String_t* ___searchPattern0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileSystemInfo[] System.IO.DirectoryInfo::GetFileSystemInfos()
extern "C"  FileSystemInfoU5BU5D_t1190771700* DirectoryInfo_GetFileSystemInfos_m1930684031 (DirectoryInfo_t89154617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileSystemInfo[] System.IO.DirectoryInfo::GetFileSystemInfos(System.String)
extern "C"  FileSystemInfoU5BU5D_t1190771700* DirectoryInfo_GetFileSystemInfos_m2321186531 (DirectoryInfo_t89154617 * __this, String_t* ___searchPattern0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.DirectoryInfo::Delete()
extern "C"  void DirectoryInfo_Delete_m3447851021 (DirectoryInfo_t89154617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.DirectoryInfo::Delete(System.Boolean)
extern "C"  void DirectoryInfo_Delete_m1876894404 (DirectoryInfo_t89154617 * __this, bool ___recursive0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.DirectoryInfo::MoveTo(System.String)
extern "C"  void DirectoryInfo_MoveTo_m864129396 (DirectoryInfo_t89154617 * __this, String_t* ___destDirName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.DirectoryInfo::ToString()
extern "C"  String_t* DirectoryInfo_ToString_m2661500299 (DirectoryInfo_t89154617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.DirectoryInfo[] System.IO.DirectoryInfo::GetDirectories(System.String,System.IO.SearchOption)
extern "C"  DirectoryInfoU5BU5D_t2262552260* DirectoryInfo_GetDirectories_m735916406 (DirectoryInfo_t89154617 * __this, String_t* ___searchPattern0, int32_t ___searchOption1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IO.DirectoryInfo::GetFilesSubdirs(System.Collections.ArrayList,System.String)
extern "C"  int32_t DirectoryInfo_GetFilesSubdirs_m2772150110 (DirectoryInfo_t89154617 * __this, ArrayList_t3948406897 * ___l0, String_t* ___pattern1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileInfo[] System.IO.DirectoryInfo::GetFiles(System.String,System.IO.SearchOption)
extern "C"  FileInfoU5BU5D_t3358688287* DirectoryInfo_GetFiles_m3663592363 (DirectoryInfo_t89154617 * __this, String_t* ___searchPattern0, int32_t ___searchOption1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
