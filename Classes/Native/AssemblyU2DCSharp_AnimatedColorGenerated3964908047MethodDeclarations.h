﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimatedColorGenerated
struct AnimatedColorGenerated_t3964908047;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void AnimatedColorGenerated::.ctor()
extern "C"  void AnimatedColorGenerated__ctor_m1090413420 (AnimatedColorGenerated_t3964908047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimatedColorGenerated::AnimatedColor_AnimatedColor1(JSVCall,System.Int32)
extern "C"  bool AnimatedColorGenerated_AnimatedColor_AnimatedColor1_m2734724876 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatedColorGenerated::AnimatedColor_color(JSVCall)
extern "C"  void AnimatedColorGenerated_AnimatedColor_color_m546763833 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatedColorGenerated::__Register()
extern "C"  void AnimatedColorGenerated___Register_m3144965339 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AnimatedColorGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t AnimatedColorGenerated_ilo_getObject1_m1234269350 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatedColorGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void AnimatedColorGenerated_ilo_addJSCSRel2_m3262486697 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AnimatedColorGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t AnimatedColorGenerated_ilo_setObject3_m916931744 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
