﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_EventFunc_1_gen1647712181MethodDeclarations.h"

// System.Void CEvent.EventFunc`1<CEvent.ZEvent>::.ctor(System.Object,System.IntPtr)
#define EventFunc_1__ctor_m1847299138(__this, ___object0, ___method1, method) ((  void (*) (EventFunc_1_t1114914310 *, Il2CppObject *, IntPtr_t, const MethodInfo*))EventFunc_1__ctor_m2817712347_gshared)(__this, ___object0, ___method1, method)
// System.Void CEvent.EventFunc`1<CEvent.ZEvent>::Invoke(T)
#define EventFunc_1_Invoke_m4074693058(__this, ___ev0, method) ((  void (*) (EventFunc_1_t1114914310 *, ZEvent_t3638018500 *, const MethodInfo*))EventFunc_1_Invoke_m205765385_gshared)(__this, ___ev0, method)
// System.IAsyncResult CEvent.EventFunc`1<CEvent.ZEvent>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define EventFunc_1_BeginInvoke_m2793188375(__this, ___ev0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (EventFunc_1_t1114914310 *, ZEvent_t3638018500 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))EventFunc_1_BeginInvoke_m1354789470_gshared)(__this, ___ev0, ___callback1, ___object2, method)
// System.Void CEvent.EventFunc`1<CEvent.ZEvent>::EndInvoke(System.IAsyncResult)
#define EventFunc_1_EndInvoke_m3709038418(__this, ___result0, method) ((  void (*) (EventFunc_1_t1114914310 *, Il2CppObject *, const MethodInfo*))EventFunc_1_EndInvoke_m4101437547_gshared)(__this, ___result0, method)
