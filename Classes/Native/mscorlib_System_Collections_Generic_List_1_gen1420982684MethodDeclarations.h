﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Core.RpsChoice>
struct List_1_t1420982684;
// System.Collections.Generic.IEnumerable`1<Core.RpsChoice>
struct IEnumerable_1_t3353710089;
// System.Collections.Generic.IEnumerator`1<Core.RpsChoice>
struct IEnumerator_1_t1964662181;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Core.RpsChoice>
struct ICollection_1_t947387119;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>
struct ReadOnlyCollection_1_t1609874668;
// Core.RpsChoice[]
struct RpsChoiceU5BU5D_t3806589061;
// System.Predicate`1<Core.RpsChoice>
struct Predicate_1_t3958821311;
// System.Collections.Generic.IComparer`1<Core.RpsChoice>
struct IComparer_1_t2627811174;
// System.Comparison`1<Core.RpsChoice>
struct Comparison_1_t3064125615;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Core_RpsChoice52797132.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1440655454.h"

// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::.ctor()
extern "C"  void List_1__ctor_m4242389714_gshared (List_1_t1420982684 * __this, const MethodInfo* method);
#define List_1__ctor_m4242389714(__this, method) ((  void (*) (List_1_t1420982684 *, const MethodInfo*))List_1__ctor_m4242389714_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m4261273732_gshared (List_1_t1420982684 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m4261273732(__this, ___collection0, method) ((  void (*) (List_1_t1420982684 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m4261273732_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m4111822316_gshared (List_1_t1420982684 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m4111822316(__this, ___capacity0, method) ((  void (*) (List_1_t1420982684 *, int32_t, const MethodInfo*))List_1__ctor_m4111822316_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::.cctor()
extern "C"  void List_1__cctor_m569992690_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m569992690(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m569992690_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Core.RpsChoice>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4086498149_gshared (List_1_t1420982684 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4086498149(__this, method) ((  Il2CppObject* (*) (List_1_t1420982684 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4086498149_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m2505254025_gshared (List_1_t1420982684 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m2505254025(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1420982684 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2505254025_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Core.RpsChoice>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1670585176_gshared (List_1_t1420982684 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1670585176(__this, method) ((  Il2CppObject * (*) (List_1_t1420982684 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1670585176_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Core.RpsChoice>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m4035886949_gshared (List_1_t1420982684 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m4035886949(__this, ___item0, method) ((  int32_t (*) (List_1_t1420982684 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m4035886949_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Core.RpsChoice>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m2798918011_gshared (List_1_t1420982684 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m2798918011(__this, ___item0, method) ((  bool (*) (List_1_t1420982684 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2798918011_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Core.RpsChoice>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m837204797_gshared (List_1_t1420982684 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m837204797(__this, ___item0, method) ((  int32_t (*) (List_1_t1420982684 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m837204797_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m962555440_gshared (List_1_t1420982684 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m962555440(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1420982684 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m962555440_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m1645014264_gshared (List_1_t1420982684 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1645014264(__this, ___item0, method) ((  void (*) (List_1_t1420982684 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1645014264_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Core.RpsChoice>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2899749820_gshared (List_1_t1420982684 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2899749820(__this, method) ((  bool (*) (List_1_t1420982684 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2899749820_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Core.RpsChoice>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m66357249_gshared (List_1_t1420982684 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m66357249(__this, method) ((  bool (*) (List_1_t1420982684 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m66357249_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Core.RpsChoice>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m897860595_gshared (List_1_t1420982684 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m897860595(__this, method) ((  Il2CppObject * (*) (List_1_t1420982684 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m897860595_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Core.RpsChoice>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m4133437738_gshared (List_1_t1420982684 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m4133437738(__this, method) ((  bool (*) (List_1_t1420982684 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m4133437738_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Core.RpsChoice>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1141534863_gshared (List_1_t1420982684 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1141534863(__this, method) ((  bool (*) (List_1_t1420982684 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1141534863_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Core.RpsChoice>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m621216954_gshared (List_1_t1420982684 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m621216954(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1420982684 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m621216954_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m2211761671_gshared (List_1_t1420982684 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m2211761671(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1420982684 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m2211761671_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::Add(T)
extern "C"  void List_1_Add_m3936750530_gshared (List_1_t1420982684 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m3936750530(__this, ___item0, method) ((  void (*) (List_1_t1420982684 *, int32_t, const MethodInfo*))List_1_Add_m3936750530_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m4166741055_gshared (List_1_t1420982684 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m4166741055(__this, ___newCount0, method) ((  void (*) (List_1_t1420982684 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4166741055_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m2007689800_gshared (List_1_t1420982684 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m2007689800(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t1420982684 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m2007689800_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m4105373501_gshared (List_1_t1420982684 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m4105373501(__this, ___collection0, method) ((  void (*) (List_1_t1420982684 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m4105373501_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3366345725_gshared (List_1_t1420982684 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3366345725(__this, ___enumerable0, method) ((  void (*) (List_1_t1420982684 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3366345725_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m1013336186_gshared (List_1_t1420982684 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m1013336186(__this, ___collection0, method) ((  void (*) (List_1_t1420982684 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m1013336186_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Core.RpsChoice>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1609874668 * List_1_AsReadOnly_m1244710603_gshared (List_1_t1420982684 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1244710603(__this, method) ((  ReadOnlyCollection_1_t1609874668 * (*) (List_1_t1420982684 *, const MethodInfo*))List_1_AsReadOnly_m1244710603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Core.RpsChoice>::BinarySearch(T)
extern "C"  int32_t List_1_BinarySearch_m1559024936_gshared (List_1_t1420982684 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_BinarySearch_m1559024936(__this, ___item0, method) ((  int32_t (*) (List_1_t1420982684 *, int32_t, const MethodInfo*))List_1_BinarySearch_m1559024936_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::Clear()
extern "C"  void List_1_Clear_m1042302278_gshared (List_1_t1420982684 * __this, const MethodInfo* method);
#define List_1_Clear_m1042302278(__this, method) ((  void (*) (List_1_t1420982684 *, const MethodInfo*))List_1_Clear_m1042302278_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Core.RpsChoice>::Contains(T)
extern "C"  bool List_1_Contains_m1933850296_gshared (List_1_t1420982684 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m1933850296(__this, ___item0, method) ((  bool (*) (List_1_t1420982684 *, int32_t, const MethodInfo*))List_1_Contains_m1933850296_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m873322228_gshared (List_1_t1420982684 * __this, RpsChoiceU5BU5D_t3806589061* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m873322228(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1420982684 *, RpsChoiceU5BU5D_t3806589061*, int32_t, const MethodInfo*))List_1_CopyTo_m873322228_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Core.RpsChoice>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m2166446418_gshared (List_1_t1420982684 * __this, Predicate_1_t3958821311 * ___match0, const MethodInfo* method);
#define List_1_Find_m2166446418(__this, ___match0, method) ((  int32_t (*) (List_1_t1420982684 *, Predicate_1_t3958821311 *, const MethodInfo*))List_1_Find_m2166446418_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m3009731183_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t3958821311 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m3009731183(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3958821311 *, const MethodInfo*))List_1_CheckMatch_m3009731183_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Core.RpsChoice>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m4056613964_gshared (List_1_t1420982684 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t3958821311 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m4056613964(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1420982684 *, int32_t, int32_t, Predicate_1_t3958821311 *, const MethodInfo*))List_1_GetIndex_m4056613964_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Core.RpsChoice>::GetEnumerator()
extern "C"  Enumerator_t1440655454  List_1_GetEnumerator_m1907448821_gshared (List_1_t1420982684 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1907448821(__this, method) ((  Enumerator_t1440655454  (*) (List_1_t1420982684 *, const MethodInfo*))List_1_GetEnumerator_m1907448821_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Core.RpsChoice>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1148374720_gshared (List_1_t1420982684 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m1148374720(__this, ___item0, method) ((  int32_t (*) (List_1_t1420982684 *, int32_t, const MethodInfo*))List_1_IndexOf_m1148374720_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3379132747_gshared (List_1_t1420982684 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3379132747(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1420982684 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3379132747_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m619689412_gshared (List_1_t1420982684 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m619689412(__this, ___index0, method) ((  void (*) (List_1_t1420982684 *, int32_t, const MethodInfo*))List_1_CheckIndex_m619689412_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m4250737259_gshared (List_1_t1420982684 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m4250737259(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1420982684 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m4250737259_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m3010625184_gshared (List_1_t1420982684 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m3010625184(__this, ___collection0, method) ((  void (*) (List_1_t1420982684 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3010625184_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Core.RpsChoice>::Remove(T)
extern "C"  bool List_1_Remove_m4144448691_gshared (List_1_t1420982684 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m4144448691(__this, ___item0, method) ((  bool (*) (List_1_t1420982684 *, int32_t, const MethodInfo*))List_1_Remove_m4144448691_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Core.RpsChoice>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1318192771_gshared (List_1_t1420982684 * __this, Predicate_1_t3958821311 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1318192771(__this, ___match0, method) ((  int32_t (*) (List_1_t1420982684 *, Predicate_1_t3958821311 *, const MethodInfo*))List_1_RemoveAll_m1318192771_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2124590129_gshared (List_1_t1420982684 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m2124590129(__this, ___index0, method) ((  void (*) (List_1_t1420982684 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2124590129_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m1844053588_gshared (List_1_t1420982684 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m1844053588(__this, ___index0, ___count1, method) ((  void (*) (List_1_t1420982684 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1844053588_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::Reverse()
extern "C"  void List_1_Reverse_m2587796123_gshared (List_1_t1420982684 * __this, const MethodInfo* method);
#define List_1_Reverse_m2587796123(__this, method) ((  void (*) (List_1_t1420982684 *, const MethodInfo*))List_1_Reverse_m2587796123_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::Sort()
extern "C"  void List_1_Sort_m2850167527_gshared (List_1_t1420982684 * __this, const MethodInfo* method);
#define List_1_Sort_m2850167527(__this, method) ((  void (*) (List_1_t1420982684 *, const MethodInfo*))List_1_Sort_m2850167527_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3753597661_gshared (List_1_t1420982684 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m3753597661(__this, ___comparer0, method) ((  void (*) (List_1_t1420982684 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3753597661_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m3661230074_gshared (List_1_t1420982684 * __this, Comparison_1_t3064125615 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m3661230074(__this, ___comparison0, method) ((  void (*) (List_1_t1420982684 *, Comparison_1_t3064125615 *, const MethodInfo*))List_1_Sort_m3661230074_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Core.RpsChoice>::ToArray()
extern "C"  RpsChoiceU5BU5D_t3806589061* List_1_ToArray_m2395125620_gshared (List_1_t1420982684 * __this, const MethodInfo* method);
#define List_1_ToArray_m2395125620(__this, method) ((  RpsChoiceU5BU5D_t3806589061* (*) (List_1_t1420982684 *, const MethodInfo*))List_1_ToArray_m2395125620_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::TrimExcess()
extern "C"  void List_1_TrimExcess_m1045770560_gshared (List_1_t1420982684 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1045770560(__this, method) ((  void (*) (List_1_t1420982684 *, const MethodInfo*))List_1_TrimExcess_m1045770560_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Core.RpsChoice>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m4245513968_gshared (List_1_t1420982684 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m4245513968(__this, method) ((  int32_t (*) (List_1_t1420982684 *, const MethodInfo*))List_1_get_Capacity_m4245513968_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m1376929873_gshared (List_1_t1420982684 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m1376929873(__this, ___value0, method) ((  void (*) (List_1_t1420982684 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1376929873_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Core.RpsChoice>::get_Count()
extern "C"  int32_t List_1_get_Count_m903784292_gshared (List_1_t1420982684 * __this, const MethodInfo* method);
#define List_1_get_Count_m903784292(__this, method) ((  int32_t (*) (List_1_t1420982684 *, const MethodInfo*))List_1_get_Count_m903784292_gshared)(__this, method)
// T System.Collections.Generic.List`1<Core.RpsChoice>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m2993791217_gshared (List_1_t1420982684 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m2993791217(__this, ___index0, method) ((  int32_t (*) (List_1_t1420982684 *, int32_t, const MethodInfo*))List_1_get_Item_m2993791217_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsChoice>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m2254204034_gshared (List_1_t1420982684 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m2254204034(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1420982684 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m2254204034_gshared)(__this, ___index0, ___value1, method)
