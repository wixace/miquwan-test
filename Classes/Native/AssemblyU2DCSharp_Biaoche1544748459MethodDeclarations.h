﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Biaoche
struct Biaoche_t1544748459;
// CombatEntity
struct CombatEntity_t684137495;
// monstersCfg
struct monstersCfg_t1542396363;
// JSCLevelMonsterConfig
struct JSCLevelMonsterConfig_t1924079698;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// Monster
struct Monster_t2901270458;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// GameMgr
struct GameMgr_t1469029542;
// HatredCtrl
struct HatredCtrl_t891253697;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_monstersCfg1542396363.h"
#include "AssemblyU2DCSharp_JSCLevelMonsterConfig1924079698.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_Monster2901270458.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_Biaoche1544748459.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"
#include "AssemblyU2DCSharp_HatredCtrl891253697.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Biaoche::.ctor()
extern "C"  void Biaoche__ctor_m2352046624 (Biaoche_t1544748459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Biaoche::set_MoveJindu(System.Single)
extern "C"  void Biaoche_set_MoveJindu_m3073677051 (Biaoche_t1544748459 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Biaoche::get_MoveJindu()
extern "C"  float Biaoche_get_MoveJindu_m221087344 (Biaoche_t1544748459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Biaoche::OnTriggerBehaivir(CombatEntity)
extern "C"  void Biaoche_OnTriggerBehaivir_m3643883346 (Biaoche_t1544748459 * __this, CombatEntity_t684137495 * ___main0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Biaoche::OnTriggerFun(System.Int32)
extern "C"  void Biaoche_OnTriggerFun_m2849778907 (Biaoche_t1544748459 * __this, int32_t ___effectId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Biaoche::Init()
extern "C"  void Biaoche_Init_m3629639732 (Biaoche_t1544748459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Biaoche::Create(monstersCfg,JSCLevelMonsterConfig)
extern "C"  void Biaoche_Create_m1499863489 (Biaoche_t1544748459 * __this, monstersCfg_t1542396363 * ___mcfg0, JSCLevelMonsterConfig_t1924079698 * ___lmcfg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Biaoche::Update()
extern "C"  void Biaoche_Update_m1625863405 (Biaoche_t1544748459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Biaoche::BattleStart(CEvent.ZEvent)
extern "C"  void Biaoche_BattleStart_m3175624691 (Biaoche_t1544748459 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Biaoche::Dead()
extern "C"  void Biaoche_Dead_m3477928584 (Biaoche_t1544748459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Biaoche::BattleWin(CEvent.ZEvent)
extern "C"  void Biaoche_BattleWin_m2331551737 (Biaoche_t1544748459 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Biaoche::MoveCallback(CEvent.ZEvent)
extern "C"  void Biaoche_MoveCallback_m2355513569 (Biaoche_t1544748459 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Biaoche::Clear()
extern "C"  void Biaoche_Clear_m4053147211 (Biaoche_t1544748459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Biaoche::ilo_OnTriggerFun1(Monster,System.Int32)
extern "C"  void Biaoche_ilo_OnTriggerFun1_m1607773269 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, int32_t ___effectId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl Biaoche::ilo_get_bvrCtrl2(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * Biaoche_ilo_get_bvrCtrl2_m1851823892 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Biaoche::ilo_Init3(Entity.Behavior.IBehaviorCtrl)
extern "C"  void Biaoche_ilo_Init3_m1719312111 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Biaoche::ilo_AddEventListener4(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void Biaoche_ilo_AddEventListener4_m832290097 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Biaoche::ilo_set_isAttack5(CombatEntity,System.Boolean)
extern "C"  void Biaoche_ilo_set_isAttack5_m329177827 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Biaoche::ilo_Update6(Monster)
extern "C"  void Biaoche_ilo_Update6_m1842548480 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Biaoche::ilo_TranBehavior7(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void Biaoche_ilo_TranBehavior7_m1117807503 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, uint8_t ___id1, ObjectU5BU5D_t1108656482* ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Biaoche::ilo_set_MoveJindu8(Biaoche,System.Single)
extern "C"  void Biaoche_ilo_set_MoveJindu8_m389045967 (Il2CppObject * __this /* static, unused */, Biaoche_t1544748459 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Biaoche::ilo_TriggerBattle9(GameMgr,CombatEntity)
extern "C"  void Biaoche_ilo_TriggerBattle9_m1215469635 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, CombatEntity_t684137495 * ___src1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Biaoche::ilo_AddHatred10(HatredCtrl,CombatEntity,System.Single)
extern "C"  void Biaoche_ilo_AddHatred10_m1916736372 (Il2CppObject * __this /* static, unused */, HatredCtrl_t891253697 * ____this0, CombatEntity_t684137495 * ___entity1, float ___hatred2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Biaoche::ilo_Log11(System.Object,System.Boolean)
extern "C"  void Biaoche_ilo_Log11_m4277525880 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
