﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.AdvancedSmooth/Turn>
struct DefaultComparer_t3207939341;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_AdvancedSmooth_Turn465195378.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.AdvancedSmooth/Turn>::.ctor()
extern "C"  void DefaultComparer__ctor_m3939103792_gshared (DefaultComparer_t3207939341 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3939103792(__this, method) ((  void (*) (DefaultComparer_t3207939341 *, const MethodInfo*))DefaultComparer__ctor_m3939103792_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.AdvancedSmooth/Turn>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3864893307_gshared (DefaultComparer_t3207939341 * __this, Turn_t465195378  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3864893307(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3207939341 *, Turn_t465195378 , const MethodInfo*))DefaultComparer_GetHashCode_m3864893307_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.AdvancedSmooth/Turn>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m4275672065_gshared (DefaultComparer_t3207939341 * __this, Turn_t465195378  ___x0, Turn_t465195378  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m4275672065(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3207939341 *, Turn_t465195378 , Turn_t465195378 , const MethodInfo*))DefaultComparer_Equals_m4275672065_gshared)(__this, ___x0, ___y1, method)
