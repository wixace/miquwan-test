﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ContinuousGestureRecognizer_1_ge4049978014.h"
#include "AssemblyU2DCSharp_TwistMethod1692697288.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TwistRecognizer
struct  TwistRecognizer_t3400822795  : public ContinuousGestureRecognizer_1_t4049978014
{
public:
	// TwistMethod TwistRecognizer::Method
	int32_t ___Method_19;
	// System.Single TwistRecognizer::MinDOT
	float ___MinDOT_20;
	// System.Single TwistRecognizer::MinRotation
	float ___MinRotation_21;
	// System.Single TwistRecognizer::PivotMoveTolerance
	float ___PivotMoveTolerance_22;

public:
	inline static int32_t get_offset_of_Method_19() { return static_cast<int32_t>(offsetof(TwistRecognizer_t3400822795, ___Method_19)); }
	inline int32_t get_Method_19() const { return ___Method_19; }
	inline int32_t* get_address_of_Method_19() { return &___Method_19; }
	inline void set_Method_19(int32_t value)
	{
		___Method_19 = value;
	}

	inline static int32_t get_offset_of_MinDOT_20() { return static_cast<int32_t>(offsetof(TwistRecognizer_t3400822795, ___MinDOT_20)); }
	inline float get_MinDOT_20() const { return ___MinDOT_20; }
	inline float* get_address_of_MinDOT_20() { return &___MinDOT_20; }
	inline void set_MinDOT_20(float value)
	{
		___MinDOT_20 = value;
	}

	inline static int32_t get_offset_of_MinRotation_21() { return static_cast<int32_t>(offsetof(TwistRecognizer_t3400822795, ___MinRotation_21)); }
	inline float get_MinRotation_21() const { return ___MinRotation_21; }
	inline float* get_address_of_MinRotation_21() { return &___MinRotation_21; }
	inline void set_MinRotation_21(float value)
	{
		___MinRotation_21 = value;
	}

	inline static int32_t get_offset_of_PivotMoveTolerance_22() { return static_cast<int32_t>(offsetof(TwistRecognizer_t3400822795, ___PivotMoveTolerance_22)); }
	inline float get_PivotMoveTolerance_22() const { return ___PivotMoveTolerance_22; }
	inline float* get_address_of_PivotMoveTolerance_22() { return &___PivotMoveTolerance_22; }
	inline void set_PivotMoveTolerance_22(float value)
	{
		___PivotMoveTolerance_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
