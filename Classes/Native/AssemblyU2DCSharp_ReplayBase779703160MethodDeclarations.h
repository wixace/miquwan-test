﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReplayBase
struct ReplayBase_t779703160;
// CombatEntity
struct CombatEntity_t684137495;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.String
struct String_t;
// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;
// HeroMgr
struct HeroMgr_t2475965342;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ReplayBase779703160.h"
#include "AssemblyU2DCSharp_HeroMgr2475965342.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"

// System.Void ReplayBase::.ctor()
extern "C"  void ReplayBase__ctor_m4153191459 (ReplayBase_t779703160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity ReplayBase::get_Entity()
extern "C"  CombatEntity_t684137495 * ReplayBase_get_Entity_m3503178163 (ReplayBase_t779703160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity ReplayBase::GetEntity(System.Int32,System.Int32)
extern "C"  CombatEntity_t684137495 * ReplayBase_GetEntity_m2290056082 (ReplayBase_t779703160 * __this, int32_t ___heroId0, int32_t ___IsEnemy1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] ReplayBase::ParserJsonStr(System.String)
extern "C"  StringU5BU5D_t4054002952* ReplayBase_ParserJsonStr_m1238156888 (ReplayBase_t779703160 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReplayBase::GetJsonStr()
extern "C"  String_t* ReplayBase_GetJsonStr_m354106647 (ReplayBase_t779703160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayBase::Execute()
extern "C"  void ReplayBase_Execute_m4043763574 (ReplayBase_t779703160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity ReplayBase::ilo_GetEntity1(ReplayBase,System.Int32,System.Int32)
extern "C"  CombatEntity_t684137495 * ReplayBase_ilo_GetEntity1_m2752809498 (Il2CppObject * __this /* static, unused */, ReplayBase_t779703160 * ____this0, int32_t ___heroId1, int32_t ___IsEnemy2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> ReplayBase::ilo_GetAllHero2(HeroMgr)
extern "C"  List_1_t2052323047 * ReplayBase_ilo_GetAllHero2_m2528695907 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ReplayBase::ilo_get_id3(CombatEntity)
extern "C"  int32_t ReplayBase_ilo_get_id3_m2881205848 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
