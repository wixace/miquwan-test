﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen3781873254MethodDeclarations.h"

// System.Void System.Predicate`1<Pathfinding.ClipperLib.OutRec>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m840421937(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t2856862167 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m231416842_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<Pathfinding.ClipperLib.OutRec>::Invoke(T)
#define Predicate_1_Invoke_m164889845(__this, ___obj0, method) ((  bool (*) (Predicate_1_t2856862167 *, OutRec_t3245805284 *, const MethodInfo*))Predicate_1_Invoke_m1328901537_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<Pathfinding.ClipperLib.OutRec>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m2570930504(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t2856862167 *, OutRec_t3245805284 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m2038073176_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<Pathfinding.ClipperLib.OutRec>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m912677375(__this, ___result0, method) ((  bool (*) (Predicate_1_t2856862167 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3970497007_gshared)(__this, ___result0, method)
