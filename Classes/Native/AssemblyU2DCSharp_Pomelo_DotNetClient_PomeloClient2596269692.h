﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3771233898;
// Pomelo.DotNetClient.PomeloClient
struct PomeloClient_t4215271137;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pomelo.DotNetClient.PomeloClient/<initClient>c__AnonStorey162
struct  U3CinitClientU3Ec__AnonStorey162_t2596269692  : public Il2CppObject
{
public:
	// System.UInt32 Pomelo.DotNetClient.PomeloClient/<initClient>c__AnonStorey162::socketID
	uint32_t ___socketID_0;
	// System.Action Pomelo.DotNetClient.PomeloClient/<initClient>c__AnonStorey162::callback
	Action_t3771233898 * ___callback_1;
	// Pomelo.DotNetClient.PomeloClient Pomelo.DotNetClient.PomeloClient/<initClient>c__AnonStorey162::<>f__this
	PomeloClient_t4215271137 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_socketID_0() { return static_cast<int32_t>(offsetof(U3CinitClientU3Ec__AnonStorey162_t2596269692, ___socketID_0)); }
	inline uint32_t get_socketID_0() const { return ___socketID_0; }
	inline uint32_t* get_address_of_socketID_0() { return &___socketID_0; }
	inline void set_socketID_0(uint32_t value)
	{
		___socketID_0 = value;
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(U3CinitClientU3Ec__AnonStorey162_t2596269692, ___callback_1)); }
	inline Action_t3771233898 * get_callback_1() const { return ___callback_1; }
	inline Action_t3771233898 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(Action_t3771233898 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier(&___callback_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CinitClientU3Ec__AnonStorey162_t2596269692, ___U3CU3Ef__this_2)); }
	inline PomeloClient_t4215271137 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline PomeloClient_t4215271137 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(PomeloClient_t4215271137 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
