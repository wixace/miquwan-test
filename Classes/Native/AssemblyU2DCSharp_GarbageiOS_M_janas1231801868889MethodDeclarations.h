﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_janas123
struct M_janas123_t1801868889;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_janas123::.ctor()
extern "C"  void M_janas123__ctor_m2043847674 (M_janas123_t1801868889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_janas123::M_riwouNisraw0(System.String[],System.Int32)
extern "C"  void M_janas123_M_riwouNisraw0_m2797343181 (M_janas123_t1801868889 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_janas123::M_stihokea1(System.String[],System.Int32)
extern "C"  void M_janas123_M_stihokea1_m1779920886 (M_janas123_t1801868889 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
