﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.BinaryHeapM
struct BinaryHeapM_t946855490;
// Pathfinding.PathNode
struct PathNode_t417131581;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_PathNode417131581.h"

// System.Void Pathfinding.BinaryHeapM::.ctor(System.Int32)
extern "C"  void BinaryHeapM__ctor_m2128179462 (BinaryHeapM_t946855490 * __this, int32_t ___numberOfElements0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.BinaryHeapM::Clear()
extern "C"  void BinaryHeapM_Clear_m1615634272 (BinaryHeapM_t946855490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.PathNode Pathfinding.BinaryHeapM::GetNode(System.Int32)
extern "C"  PathNode_t417131581 * BinaryHeapM_GetNode_m156172424 (BinaryHeapM_t946855490 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.BinaryHeapM::SetF(System.Int32,System.UInt32)
extern "C"  void BinaryHeapM_SetF_m3335262872 (BinaryHeapM_t946855490 * __this, int32_t ___i0, uint32_t ___F1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.BinaryHeapM::Add(Pathfinding.PathNode)
extern "C"  void BinaryHeapM_Add_m52262115 (BinaryHeapM_t946855490 * __this, PathNode_t417131581 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.PathNode Pathfinding.BinaryHeapM::Remove()
extern "C"  PathNode_t417131581 * BinaryHeapM_Remove_m3452314759 (BinaryHeapM_t946855490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.BinaryHeapM::Validate()
extern "C"  void BinaryHeapM_Validate_m2699449349 (BinaryHeapM_t946855490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.BinaryHeapM::Rebuild()
extern "C"  void BinaryHeapM_Rebuild_m3683369262 (BinaryHeapM_t946855490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.BinaryHeapM::ilo_get_G1(Pathfinding.PathNode)
extern "C"  uint32_t BinaryHeapM_ilo_get_G1_m417369865 (Il2CppObject * __this /* static, unused */, PathNode_t417131581 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
