﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_lowmoutouDroneemar149
struct  M_lowmoutouDroneemar149_t3142486912  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_lowmoutouDroneemar149::_weaka
	int32_t ____weaka_0;
	// System.Single GarbageiOS.M_lowmoutouDroneemar149::_vearwifairPastal
	float ____vearwifairPastal_1;
	// System.Boolean GarbageiOS.M_lowmoutouDroneemar149::_learpisRomemstair
	bool ____learpisRomemstair_2;
	// System.Single GarbageiOS.M_lowmoutouDroneemar149::_jibawjeCocamou
	float ____jibawjeCocamou_3;
	// System.Boolean GarbageiOS.M_lowmoutouDroneemar149::_tasrecisHeme
	bool ____tasrecisHeme_4;
	// System.Boolean GarbageiOS.M_lowmoutouDroneemar149::_rawdihor
	bool ____rawdihor_5;
	// System.Single GarbageiOS.M_lowmoutouDroneemar149::_yoomoBatea
	float ____yoomoBatea_6;
	// System.UInt32 GarbageiOS.M_lowmoutouDroneemar149::_seaseyel
	uint32_t ____seaseyel_7;
	// System.Int32 GarbageiOS.M_lowmoutouDroneemar149::_yemjare
	int32_t ____yemjare_8;
	// System.String GarbageiOS.M_lowmoutouDroneemar149::_maiceTemfu
	String_t* ____maiceTemfu_9;
	// System.String GarbageiOS.M_lowmoutouDroneemar149::_baytooyu
	String_t* ____baytooyu_10;
	// System.String GarbageiOS.M_lowmoutouDroneemar149::_saraiKawtrarror
	String_t* ____saraiKawtrarror_11;
	// System.Int32 GarbageiOS.M_lowmoutouDroneemar149::_sebi
	int32_t ____sebi_12;
	// System.Int32 GarbageiOS.M_lowmoutouDroneemar149::_tutobaDerall
	int32_t ____tutobaDerall_13;
	// System.Int32 GarbageiOS.M_lowmoutouDroneemar149::_rairfecarJaivi
	int32_t ____rairfecarJaivi_14;
	// System.Int32 GarbageiOS.M_lowmoutouDroneemar149::_jerdur
	int32_t ____jerdur_15;
	// System.Boolean GarbageiOS.M_lowmoutouDroneemar149::_gairtujas
	bool ____gairtujas_16;
	// System.String GarbageiOS.M_lowmoutouDroneemar149::_soubereCisjorzir
	String_t* ____soubereCisjorzir_17;

public:
	inline static int32_t get_offset_of__weaka_0() { return static_cast<int32_t>(offsetof(M_lowmoutouDroneemar149_t3142486912, ____weaka_0)); }
	inline int32_t get__weaka_0() const { return ____weaka_0; }
	inline int32_t* get_address_of__weaka_0() { return &____weaka_0; }
	inline void set__weaka_0(int32_t value)
	{
		____weaka_0 = value;
	}

	inline static int32_t get_offset_of__vearwifairPastal_1() { return static_cast<int32_t>(offsetof(M_lowmoutouDroneemar149_t3142486912, ____vearwifairPastal_1)); }
	inline float get__vearwifairPastal_1() const { return ____vearwifairPastal_1; }
	inline float* get_address_of__vearwifairPastal_1() { return &____vearwifairPastal_1; }
	inline void set__vearwifairPastal_1(float value)
	{
		____vearwifairPastal_1 = value;
	}

	inline static int32_t get_offset_of__learpisRomemstair_2() { return static_cast<int32_t>(offsetof(M_lowmoutouDroneemar149_t3142486912, ____learpisRomemstair_2)); }
	inline bool get__learpisRomemstair_2() const { return ____learpisRomemstair_2; }
	inline bool* get_address_of__learpisRomemstair_2() { return &____learpisRomemstair_2; }
	inline void set__learpisRomemstair_2(bool value)
	{
		____learpisRomemstair_2 = value;
	}

	inline static int32_t get_offset_of__jibawjeCocamou_3() { return static_cast<int32_t>(offsetof(M_lowmoutouDroneemar149_t3142486912, ____jibawjeCocamou_3)); }
	inline float get__jibawjeCocamou_3() const { return ____jibawjeCocamou_3; }
	inline float* get_address_of__jibawjeCocamou_3() { return &____jibawjeCocamou_3; }
	inline void set__jibawjeCocamou_3(float value)
	{
		____jibawjeCocamou_3 = value;
	}

	inline static int32_t get_offset_of__tasrecisHeme_4() { return static_cast<int32_t>(offsetof(M_lowmoutouDroneemar149_t3142486912, ____tasrecisHeme_4)); }
	inline bool get__tasrecisHeme_4() const { return ____tasrecisHeme_4; }
	inline bool* get_address_of__tasrecisHeme_4() { return &____tasrecisHeme_4; }
	inline void set__tasrecisHeme_4(bool value)
	{
		____tasrecisHeme_4 = value;
	}

	inline static int32_t get_offset_of__rawdihor_5() { return static_cast<int32_t>(offsetof(M_lowmoutouDroneemar149_t3142486912, ____rawdihor_5)); }
	inline bool get__rawdihor_5() const { return ____rawdihor_5; }
	inline bool* get_address_of__rawdihor_5() { return &____rawdihor_5; }
	inline void set__rawdihor_5(bool value)
	{
		____rawdihor_5 = value;
	}

	inline static int32_t get_offset_of__yoomoBatea_6() { return static_cast<int32_t>(offsetof(M_lowmoutouDroneemar149_t3142486912, ____yoomoBatea_6)); }
	inline float get__yoomoBatea_6() const { return ____yoomoBatea_6; }
	inline float* get_address_of__yoomoBatea_6() { return &____yoomoBatea_6; }
	inline void set__yoomoBatea_6(float value)
	{
		____yoomoBatea_6 = value;
	}

	inline static int32_t get_offset_of__seaseyel_7() { return static_cast<int32_t>(offsetof(M_lowmoutouDroneemar149_t3142486912, ____seaseyel_7)); }
	inline uint32_t get__seaseyel_7() const { return ____seaseyel_7; }
	inline uint32_t* get_address_of__seaseyel_7() { return &____seaseyel_7; }
	inline void set__seaseyel_7(uint32_t value)
	{
		____seaseyel_7 = value;
	}

	inline static int32_t get_offset_of__yemjare_8() { return static_cast<int32_t>(offsetof(M_lowmoutouDroneemar149_t3142486912, ____yemjare_8)); }
	inline int32_t get__yemjare_8() const { return ____yemjare_8; }
	inline int32_t* get_address_of__yemjare_8() { return &____yemjare_8; }
	inline void set__yemjare_8(int32_t value)
	{
		____yemjare_8 = value;
	}

	inline static int32_t get_offset_of__maiceTemfu_9() { return static_cast<int32_t>(offsetof(M_lowmoutouDroneemar149_t3142486912, ____maiceTemfu_9)); }
	inline String_t* get__maiceTemfu_9() const { return ____maiceTemfu_9; }
	inline String_t** get_address_of__maiceTemfu_9() { return &____maiceTemfu_9; }
	inline void set__maiceTemfu_9(String_t* value)
	{
		____maiceTemfu_9 = value;
		Il2CppCodeGenWriteBarrier(&____maiceTemfu_9, value);
	}

	inline static int32_t get_offset_of__baytooyu_10() { return static_cast<int32_t>(offsetof(M_lowmoutouDroneemar149_t3142486912, ____baytooyu_10)); }
	inline String_t* get__baytooyu_10() const { return ____baytooyu_10; }
	inline String_t** get_address_of__baytooyu_10() { return &____baytooyu_10; }
	inline void set__baytooyu_10(String_t* value)
	{
		____baytooyu_10 = value;
		Il2CppCodeGenWriteBarrier(&____baytooyu_10, value);
	}

	inline static int32_t get_offset_of__saraiKawtrarror_11() { return static_cast<int32_t>(offsetof(M_lowmoutouDroneemar149_t3142486912, ____saraiKawtrarror_11)); }
	inline String_t* get__saraiKawtrarror_11() const { return ____saraiKawtrarror_11; }
	inline String_t** get_address_of__saraiKawtrarror_11() { return &____saraiKawtrarror_11; }
	inline void set__saraiKawtrarror_11(String_t* value)
	{
		____saraiKawtrarror_11 = value;
		Il2CppCodeGenWriteBarrier(&____saraiKawtrarror_11, value);
	}

	inline static int32_t get_offset_of__sebi_12() { return static_cast<int32_t>(offsetof(M_lowmoutouDroneemar149_t3142486912, ____sebi_12)); }
	inline int32_t get__sebi_12() const { return ____sebi_12; }
	inline int32_t* get_address_of__sebi_12() { return &____sebi_12; }
	inline void set__sebi_12(int32_t value)
	{
		____sebi_12 = value;
	}

	inline static int32_t get_offset_of__tutobaDerall_13() { return static_cast<int32_t>(offsetof(M_lowmoutouDroneemar149_t3142486912, ____tutobaDerall_13)); }
	inline int32_t get__tutobaDerall_13() const { return ____tutobaDerall_13; }
	inline int32_t* get_address_of__tutobaDerall_13() { return &____tutobaDerall_13; }
	inline void set__tutobaDerall_13(int32_t value)
	{
		____tutobaDerall_13 = value;
	}

	inline static int32_t get_offset_of__rairfecarJaivi_14() { return static_cast<int32_t>(offsetof(M_lowmoutouDroneemar149_t3142486912, ____rairfecarJaivi_14)); }
	inline int32_t get__rairfecarJaivi_14() const { return ____rairfecarJaivi_14; }
	inline int32_t* get_address_of__rairfecarJaivi_14() { return &____rairfecarJaivi_14; }
	inline void set__rairfecarJaivi_14(int32_t value)
	{
		____rairfecarJaivi_14 = value;
	}

	inline static int32_t get_offset_of__jerdur_15() { return static_cast<int32_t>(offsetof(M_lowmoutouDroneemar149_t3142486912, ____jerdur_15)); }
	inline int32_t get__jerdur_15() const { return ____jerdur_15; }
	inline int32_t* get_address_of__jerdur_15() { return &____jerdur_15; }
	inline void set__jerdur_15(int32_t value)
	{
		____jerdur_15 = value;
	}

	inline static int32_t get_offset_of__gairtujas_16() { return static_cast<int32_t>(offsetof(M_lowmoutouDroneemar149_t3142486912, ____gairtujas_16)); }
	inline bool get__gairtujas_16() const { return ____gairtujas_16; }
	inline bool* get_address_of__gairtujas_16() { return &____gairtujas_16; }
	inline void set__gairtujas_16(bool value)
	{
		____gairtujas_16 = value;
	}

	inline static int32_t get_offset_of__soubereCisjorzir_17() { return static_cast<int32_t>(offsetof(M_lowmoutouDroneemar149_t3142486912, ____soubereCisjorzir_17)); }
	inline String_t* get__soubereCisjorzir_17() const { return ____soubereCisjorzir_17; }
	inline String_t** get_address_of__soubereCisjorzir_17() { return &____soubereCisjorzir_17; }
	inline void set__soubereCisjorzir_17(String_t* value)
	{
		____soubereCisjorzir_17 = value;
		Il2CppCodeGenWriteBarrier(&____soubereCisjorzir_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
