﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIDragDropRootGenerated
struct UIDragDropRootGenerated_t2915639062;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UIDragDropRootGenerated::.ctor()
extern "C"  void UIDragDropRootGenerated__ctor_m1567086869 (UIDragDropRootGenerated_t2915639062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDragDropRootGenerated::UIDragDropRoot_UIDragDropRoot1(JSVCall,System.Int32)
extern "C"  bool UIDragDropRootGenerated_UIDragDropRoot_UIDragDropRoot1_m84296797 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropRootGenerated::UIDragDropRoot_root(JSVCall)
extern "C"  void UIDragDropRootGenerated_UIDragDropRoot_root_m231746956 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropRootGenerated::__Register()
extern "C"  void UIDragDropRootGenerated___Register_m3222210770 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDragDropRootGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UIDragDropRootGenerated_ilo_attachFinalizerObject1_m2300032898 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropRootGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UIDragDropRootGenerated_ilo_addJSCSRel2_m1272131026 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIDragDropRootGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIDragDropRootGenerated_ilo_setObject3_m3664718395 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
