﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,HatredCtrl/stValue>
struct ShimEnumerator_t1016795337;
// System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>
struct Dictionary_2_t1301017310;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,HatredCtrl/stValue>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m3274661222_gshared (ShimEnumerator_t1016795337 * __this, Dictionary_2_t1301017310 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m3274661222(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1016795337 *, Dictionary_2_t1301017310 *, const MethodInfo*))ShimEnumerator__ctor_m3274661222_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,HatredCtrl/stValue>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2059458367_gshared (ShimEnumerator_t1016795337 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m2059458367(__this, method) ((  bool (*) (ShimEnumerator_t1016795337 *, const MethodInfo*))ShimEnumerator_MoveNext_m2059458367_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,HatredCtrl/stValue>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m3126916491_gshared (ShimEnumerator_t1016795337 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m3126916491(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t1016795337 *, const MethodInfo*))ShimEnumerator_get_Entry_m3126916491_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,HatredCtrl/stValue>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2790582538_gshared (ShimEnumerator_t1016795337 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m2790582538(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1016795337 *, const MethodInfo*))ShimEnumerator_get_Key_m2790582538_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,HatredCtrl/stValue>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2738662684_gshared (ShimEnumerator_t1016795337 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m2738662684(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1016795337 *, const MethodInfo*))ShimEnumerator_get_Value_m2738662684_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,HatredCtrl/stValue>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m464577700_gshared (ShimEnumerator_t1016795337 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m464577700(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1016795337 *, const MethodInfo*))ShimEnumerator_get_Current_m464577700_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,HatredCtrl/stValue>::Reset()
extern "C"  void ShimEnumerator_Reset_m1487617720_gshared (ShimEnumerator_t1016795337 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1487617720(__this, method) ((  void (*) (ShimEnumerator_t1016795337 *, const MethodInfo*))ShimEnumerator_Reset_m1487617720_gshared)(__this, method)
