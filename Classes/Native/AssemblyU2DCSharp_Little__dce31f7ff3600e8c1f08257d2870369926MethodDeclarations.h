﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._dce31f7ff3600e8c1f08257d562515ff
struct _dce31f7ff3600e8c1f08257d562515ff_t2870369926;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._dce31f7ff3600e8c1f08257d562515ff::.ctor()
extern "C"  void _dce31f7ff3600e8c1f08257d562515ff__ctor_m4284989031 (_dce31f7ff3600e8c1f08257d562515ff_t2870369926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._dce31f7ff3600e8c1f08257d562515ff::_dce31f7ff3600e8c1f08257d562515ffm2(System.Int32)
extern "C"  int32_t _dce31f7ff3600e8c1f08257d562515ff__dce31f7ff3600e8c1f08257d562515ffm2_m1094538201 (_dce31f7ff3600e8c1f08257d562515ff_t2870369926 * __this, int32_t ____dce31f7ff3600e8c1f08257d562515ffa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._dce31f7ff3600e8c1f08257d562515ff::_dce31f7ff3600e8c1f08257d562515ffm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _dce31f7ff3600e8c1f08257d562515ff__dce31f7ff3600e8c1f08257d562515ffm_m4169816829 (_dce31f7ff3600e8c1f08257d562515ff_t2870369926 * __this, int32_t ____dce31f7ff3600e8c1f08257d562515ffa0, int32_t ____dce31f7ff3600e8c1f08257d562515ff881, int32_t ____dce31f7ff3600e8c1f08257d562515ffc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
