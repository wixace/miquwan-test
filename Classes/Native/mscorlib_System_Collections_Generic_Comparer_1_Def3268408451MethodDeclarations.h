﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.AdvancedSmooth/Turn>
struct DefaultComparer_t3268408451;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_AdvancedSmooth_Turn465195378.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.AdvancedSmooth/Turn>::.ctor()
extern "C"  void DefaultComparer__ctor_m3042362902_gshared (DefaultComparer_t3268408451 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3042362902(__this, method) ((  void (*) (DefaultComparer_t3268408451 *, const MethodInfo*))DefaultComparer__ctor_m3042362902_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.AdvancedSmooth/Turn>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m1985885401_gshared (DefaultComparer_t3268408451 * __this, Turn_t465195378  ___x0, Turn_t465195378  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m1985885401(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t3268408451 *, Turn_t465195378 , Turn_t465195378 , const MethodInfo*))DefaultComparer_Compare_m1985885401_gshared)(__this, ___x0, ___y1, method)
