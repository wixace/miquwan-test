﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo1432926611MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m4096198806(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t622389263 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1366664402_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m4152950656(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t622389263 *, DTSweepConstraint_t3360279023 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2541166012_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1899142474(__this, method) ((  void (*) (ReadOnlyCollection_1_t622389263 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3473426062_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m258744935(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t622389263 *, int32_t, DTSweepConstraint_t3360279023 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3496388003_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m787252919(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t622389263 *, DTSweepConstraint_t3360279023 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m348744375_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2427565101(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t622389263 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1370240873_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m4153821267(__this, ___index0, method) ((  DTSweepConstraint_t3360279023 * (*) (ReadOnlyCollection_1_t622389263 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3534609325_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1355375998(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t622389263 *, int32_t, DTSweepConstraint_t3360279023 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3174042042_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4227828472(__this, method) ((  bool (*) (ReadOnlyCollection_1_t622389263 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2459576056_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2687808581(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t622389263 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1945557633_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1311640980(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t622389263 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3330065468_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3836456745(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t622389263 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1628967861_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2879133523(__this, method) ((  void (*) (ReadOnlyCollection_1_t622389263 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m514207119_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m770937783(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t622389263 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m736178103_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3938330881(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t622389263 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3658311565_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1241118964(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t622389263 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2823806264_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1356084148(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t622389263 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2498539760_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m674291332(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t622389263 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1730676936_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1812337733(__this, method) ((  bool (*) (ReadOnlyCollection_1_t622389263 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1373829189_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3142079351(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t622389263 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m918746289_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1078335590(__this, method) ((  bool (*) (ReadOnlyCollection_1_t622389263 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m932754534_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3398287827(__this, method) ((  bool (*) (ReadOnlyCollection_1_t622389263 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2423760339_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1241858878(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t622389263 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3512499704_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3623335883(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t622389263 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m4167408399_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::Contains(T)
#define ReadOnlyCollection_1_Contains_m426399484(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t622389263 *, DTSweepConstraint_t3360279023 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m687553276_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m3878081200(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t622389263 *, DTSweepConstraintU5BU5D_t1615789430*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m475587820_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m1367070035(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t622389263 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m809369055_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m1342490236(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t622389263 *, DTSweepConstraint_t3360279023 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m817393776_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::get_Count()
#define ReadOnlyCollection_1_get_Count_m2779799167(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t622389263 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3681678091_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Poly2Tri.DTSweepConstraint>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m1728321427(__this, ___index0, method) ((  DTSweepConstraint_t3360279023 * (*) (ReadOnlyCollection_1_t622389263 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2421641197_gshared)(__this, ___index0, method)
