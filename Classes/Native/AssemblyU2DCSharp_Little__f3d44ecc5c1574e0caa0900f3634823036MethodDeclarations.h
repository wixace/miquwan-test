﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._f3d44ecc5c1574e0caa0900fcaee3028
struct _f3d44ecc5c1574e0caa0900fcaee3028_t3634823036;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._f3d44ecc5c1574e0caa0900fcaee3028::.ctor()
extern "C"  void _f3d44ecc5c1574e0caa0900fcaee3028__ctor_m3105739313 (_f3d44ecc5c1574e0caa0900fcaee3028_t3634823036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f3d44ecc5c1574e0caa0900fcaee3028::_f3d44ecc5c1574e0caa0900fcaee3028m2(System.Int32)
extern "C"  int32_t _f3d44ecc5c1574e0caa0900fcaee3028__f3d44ecc5c1574e0caa0900fcaee3028m2_m4091084825 (_f3d44ecc5c1574e0caa0900fcaee3028_t3634823036 * __this, int32_t ____f3d44ecc5c1574e0caa0900fcaee3028a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f3d44ecc5c1574e0caa0900fcaee3028::_f3d44ecc5c1574e0caa0900fcaee3028m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _f3d44ecc5c1574e0caa0900fcaee3028__f3d44ecc5c1574e0caa0900fcaee3028m_m1417475773 (_f3d44ecc5c1574e0caa0900fcaee3028_t3634823036 * __this, int32_t ____f3d44ecc5c1574e0caa0900fcaee3028a0, int32_t ____f3d44ecc5c1574e0caa0900fcaee3028641, int32_t ____f3d44ecc5c1574e0caa0900fcaee3028c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
