﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_stawnofeMelgallmou12
struct M_stawnofeMelgallmou12_t433472909;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_stawnofeMelgallmou12433472909.h"

// System.Void GarbageiOS.M_stawnofeMelgallmou12::.ctor()
extern "C"  void M_stawnofeMelgallmou12__ctor_m660632390 (M_stawnofeMelgallmou12_t433472909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_stawnofeMelgallmou12::M_bipu0(System.String[],System.Int32)
extern "C"  void M_stawnofeMelgallmou12_M_bipu0_m462999597 (M_stawnofeMelgallmou12_t433472909 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_stawnofeMelgallmou12::M_fallgaci1(System.String[],System.Int32)
extern "C"  void M_stawnofeMelgallmou12_M_fallgaci1_m2304735711 (M_stawnofeMelgallmou12_t433472909 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_stawnofeMelgallmou12::M_keartrarSalljoco2(System.String[],System.Int32)
extern "C"  void M_stawnofeMelgallmou12_M_keartrarSalljoco2_m4009222530 (M_stawnofeMelgallmou12_t433472909 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_stawnofeMelgallmou12::M_dorma3(System.String[],System.Int32)
extern "C"  void M_stawnofeMelgallmou12_M_dorma3_m1276530175 (M_stawnofeMelgallmou12_t433472909 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_stawnofeMelgallmou12::M_jartasWhirxe4(System.String[],System.Int32)
extern "C"  void M_stawnofeMelgallmou12_M_jartasWhirxe4_m2296524875 (M_stawnofeMelgallmou12_t433472909 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_stawnofeMelgallmou12::M_trerejerjiCallzem5(System.String[],System.Int32)
extern "C"  void M_stawnofeMelgallmou12_M_trerejerjiCallzem5_m3837957076 (M_stawnofeMelgallmou12_t433472909 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_stawnofeMelgallmou12::M_pearvuleeWarrowhoo6(System.String[],System.Int32)
extern "C"  void M_stawnofeMelgallmou12_M_pearvuleeWarrowhoo6_m271468816 (M_stawnofeMelgallmou12_t433472909 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_stawnofeMelgallmou12::M_stalri7(System.String[],System.Int32)
extern "C"  void M_stawnofeMelgallmou12_M_stalri7_m11062749 (M_stawnofeMelgallmou12_t433472909 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_stawnofeMelgallmou12::M_neaseafawGadassem8(System.String[],System.Int32)
extern "C"  void M_stawnofeMelgallmou12_M_neaseafawGadassem8_m2854412425 (M_stawnofeMelgallmou12_t433472909 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_stawnofeMelgallmou12::M_wirya9(System.String[],System.Int32)
extern "C"  void M_stawnofeMelgallmou12_M_wirya9_m3880026136 (M_stawnofeMelgallmou12_t433472909 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_stawnofeMelgallmou12::M_mekesere10(System.String[],System.Int32)
extern "C"  void M_stawnofeMelgallmou12_M_mekesere10_m2967269341 (M_stawnofeMelgallmou12_t433472909 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_stawnofeMelgallmou12::ilo_M_bipu01(GarbageiOS.M_stawnofeMelgallmou12,System.String[],System.Int32)
extern "C"  void M_stawnofeMelgallmou12_ilo_M_bipu01_m1601252448 (Il2CppObject * __this /* static, unused */, M_stawnofeMelgallmou12_t433472909 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_stawnofeMelgallmou12::ilo_M_pearvuleeWarrowhoo62(GarbageiOS.M_stawnofeMelgallmou12,System.String[],System.Int32)
extern "C"  void M_stawnofeMelgallmou12_ilo_M_pearvuleeWarrowhoo62_m1279619934 (Il2CppObject * __this /* static, unused */, M_stawnofeMelgallmou12_t433472909 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_stawnofeMelgallmou12::ilo_M_stalri73(GarbageiOS.M_stawnofeMelgallmou12,System.String[],System.Int32)
extern "C"  void M_stawnofeMelgallmou12_ilo_M_stalri73_m1924489202 (Il2CppObject * __this /* static, unused */, M_stawnofeMelgallmou12_t433472909 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
