﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HeroGenerated/<Hero_UpdateTargetPos_GetDelegate_member24_arg1>c__AnonStorey66
struct U3CHero_UpdateTargetPos_GetDelegate_member24_arg1U3Ec__AnonStorey66_t4048408251;

#include "codegen/il2cpp-codegen.h"

// System.Void HeroGenerated/<Hero_UpdateTargetPos_GetDelegate_member24_arg1>c__AnonStorey66::.ctor()
extern "C"  void U3CHero_UpdateTargetPos_GetDelegate_member24_arg1U3Ec__AnonStorey66__ctor_m2977871120 (U3CHero_UpdateTargetPos_GetDelegate_member24_arg1U3Ec__AnonStorey66_t4048408251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated/<Hero_UpdateTargetPos_GetDelegate_member24_arg1>c__AnonStorey66::<>m__60()
extern "C"  void U3CHero_UpdateTargetPos_GetDelegate_member24_arg1U3Ec__AnonStorey66_U3CU3Em__60_m3594116115 (U3CHero_UpdateTargetPos_GetDelegate_member24_arg1U3Ec__AnonStorey66_t4048408251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
