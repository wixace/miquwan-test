﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;

#include "AssemblyU2DCSharp_UnitStateBase1040218558.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitStateFlash
struct  UnitStateFlash_t2186009251  : public UnitStateBase_t1040218558
{
public:
	// UnityEngine.Transform UnitStateFlash::target
	Transform_t1659122786 * ___target_5;
	// System.Single UnitStateFlash::distance
	float ___distance_6;
	// UnityEngine.Vector3 UnitStateFlash::targetPos
	Vector3_t4282066566  ___targetPos_7;

public:
	inline static int32_t get_offset_of_target_5() { return static_cast<int32_t>(offsetof(UnitStateFlash_t2186009251, ___target_5)); }
	inline Transform_t1659122786 * get_target_5() const { return ___target_5; }
	inline Transform_t1659122786 ** get_address_of_target_5() { return &___target_5; }
	inline void set_target_5(Transform_t1659122786 * value)
	{
		___target_5 = value;
		Il2CppCodeGenWriteBarrier(&___target_5, value);
	}

	inline static int32_t get_offset_of_distance_6() { return static_cast<int32_t>(offsetof(UnitStateFlash_t2186009251, ___distance_6)); }
	inline float get_distance_6() const { return ___distance_6; }
	inline float* get_address_of_distance_6() { return &___distance_6; }
	inline void set_distance_6(float value)
	{
		___distance_6 = value;
	}

	inline static int32_t get_offset_of_targetPos_7() { return static_cast<int32_t>(offsetof(UnitStateFlash_t2186009251, ___targetPos_7)); }
	inline Vector3_t4282066566  get_targetPos_7() const { return ___targetPos_7; }
	inline Vector3_t4282066566 * get_address_of_targetPos_7() { return &___targetPos_7; }
	inline void set_targetPos_7(Vector3_t4282066566  value)
	{
		___targetPos_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
