﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.AlternativePath
struct AlternativePath_t823703636;
// Pathfinding.Path
struct Path_t1974241691;
// Pathfinding.MonoModifier
struct MonoModifier_t200043088;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_ModifierData4046421655.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_Pathfinding_MonoModifier200043088.h"
#include "AssemblyU2DCSharp_Pathfinding_AlternativePath823703636.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.AlternativePath::.ctor()
extern "C"  void AlternativePath__ctor_m3249047907 (AlternativePath_t823703636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ModifierData Pathfinding.AlternativePath::get_input()
extern "C"  int32_t AlternativePath_get_input_m3236771028 (AlternativePath_t823703636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ModifierData Pathfinding.AlternativePath::get_output()
extern "C"  int32_t AlternativePath_get_output_m1155174489 (AlternativePath_t823703636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AlternativePath::Apply(Pathfinding.Path,Pathfinding.ModifierData)
extern "C"  void AlternativePath_Apply_m2885853595 (AlternativePath_t823703636 * __this, Path_t1974241691 * ___p0, int32_t ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AlternativePath::OnDestroy()
extern "C"  void AlternativePath_OnDestroy_m3447780572 (AlternativePath_t823703636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AlternativePath::ClearOnDestroy(Pathfinding.Path)
extern "C"  void AlternativePath_ClearOnDestroy_m1557275360 (AlternativePath_t823703636 * __this, Path_t1974241691 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AlternativePath::InversePrevious()
extern "C"  void AlternativePath_InversePrevious_m2005295880 (AlternativePath_t823703636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AlternativePath::ApplyNow(Pathfinding.Path)
extern "C"  void AlternativePath_ApplyNow_m2276222874 (AlternativePath_t823703636 * __this, Path_t1974241691 * ___somePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AlternativePath::ilo_OnDestroy1(Pathfinding.MonoModifier)
extern "C"  void AlternativePath_ilo_OnDestroy1_m4272059046 (Il2CppObject * __this /* static, unused */, MonoModifier_t200043088 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AlternativePath::ilo_InversePrevious2(Pathfinding.AlternativePath)
extern "C"  void AlternativePath_ilo_InversePrevious2_m657847691 (Il2CppObject * __this /* static, unused */, AlternativePath_t823703636 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AlternativePath::ilo_set_Penalty3(Pathfinding.GraphNode,System.UInt32)
extern "C"  void AlternativePath_ilo_set_Penalty3_m1004543877 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
