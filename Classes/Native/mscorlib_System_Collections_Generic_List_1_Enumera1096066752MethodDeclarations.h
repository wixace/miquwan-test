﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.RaycastHit>
struct List_1_t1076393982;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1096066752.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RaycastHit>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3004001076_gshared (Enumerator_t1096066752 * __this, List_1_t1076393982 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3004001076(__this, ___l0, method) ((  void (*) (Enumerator_t1096066752 *, List_1_t1076393982 *, const MethodInfo*))Enumerator__ctor_m3004001076_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RaycastHit>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3922753694_gshared (Enumerator_t1096066752 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3922753694(__this, method) ((  void (*) (Enumerator_t1096066752 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3922753694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.RaycastHit>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3094169940_gshared (Enumerator_t1096066752 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3094169940(__this, method) ((  Il2CppObject * (*) (Enumerator_t1096066752 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3094169940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RaycastHit>::Dispose()
extern "C"  void Enumerator_Dispose_m2531329561_gshared (Enumerator_t1096066752 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2531329561(__this, method) ((  void (*) (Enumerator_t1096066752 *, const MethodInfo*))Enumerator_Dispose_m2531329561_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RaycastHit>::VerifyState()
extern "C"  void Enumerator_VerifyState_m4253159634_gshared (Enumerator_t1096066752 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m4253159634(__this, method) ((  void (*) (Enumerator_t1096066752 *, const MethodInfo*))Enumerator_VerifyState_m4253159634_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.RaycastHit>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1747182030_gshared (Enumerator_t1096066752 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1747182030(__this, method) ((  bool (*) (Enumerator_t1096066752 *, const MethodInfo*))Enumerator_MoveNext_m1747182030_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.RaycastHit>::get_Current()
extern "C"  RaycastHit_t4003175726  Enumerator_get_Current_m379373483_gshared (Enumerator_t1096066752 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m379373483(__this, method) ((  RaycastHit_t4003175726  (*) (Enumerator_t1096066752 *, const MethodInfo*))Enumerator_get_Current_m379373483_gshared)(__this, method)
