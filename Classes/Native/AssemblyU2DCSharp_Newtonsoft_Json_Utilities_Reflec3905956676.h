﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<System.Reflection.MemberInfo,System.String>
struct Func_2_t4177213320;
// System.Func`2<System.Linq.IGrouping`2<System.String,System.Reflection.MemberInfo>,<>__AnonType0`2<System.Int32,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>>>
struct Func_2_t1253806793;
// System.Func`3<System.Type,System.Collections.Generic.IList`1<System.Object>,System.Object>
struct Func_3_t1067326363;
// System.Func`2<System.Reflection.ParameterInfo,System.Type>
struct Func_2_t4078497774;
// System.Func`2<System.Reflection.FieldInfo,System.Boolean>
struct Func_2_t2244419209;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionUtils
struct  ReflectionUtils_t3905956676  : public Il2CppObject
{
public:

public:
};

struct ReflectionUtils_t3905956676_StaticFields
{
public:
	// System.Func`2<System.Reflection.MemberInfo,System.String> Newtonsoft.Json.Utilities.ReflectionUtils::<>f__am$cache0
	Func_2_t4177213320 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<System.Linq.IGrouping`2<System.String,System.Reflection.MemberInfo>,<>__AnonType0`2<System.Int32,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>>> Newtonsoft.Json.Utilities.ReflectionUtils::<>f__am$cache1
	Func_2_t1253806793 * ___U3CU3Ef__amU24cache1_1;
	// System.Func`3<System.Type,System.Collections.Generic.IList`1<System.Object>,System.Object> Newtonsoft.Json.Utilities.ReflectionUtils::<>f__am$cache2
	Func_3_t1067326363 * ___U3CU3Ef__amU24cache2_2;
	// System.Func`2<System.Reflection.ParameterInfo,System.Type> Newtonsoft.Json.Utilities.ReflectionUtils::<>f__am$cache3
	Func_2_t4078497774 * ___U3CU3Ef__amU24cache3_3;
	// System.Func`2<System.Reflection.FieldInfo,System.Boolean> Newtonsoft.Json.Utilities.ReflectionUtils::<>f__am$cache4
	Func_2_t2244419209 * ___U3CU3Ef__amU24cache4_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(ReflectionUtils_t3905956676_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t4177213320 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t4177213320 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t4177213320 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(ReflectionUtils_t3905956676_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t1253806793 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t1253806793 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t1253806793 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(ReflectionUtils_t3905956676_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_3_t1067326363 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_3_t1067326363 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_3_t1067326363 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(ReflectionUtils_t3905956676_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_2_t4078497774 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_2_t4078497774 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_2_t4078497774 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(ReflectionUtils_t3905956676_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline Func_2_t2244419209 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline Func_2_t2244419209 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(Func_2_t2244419209 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
