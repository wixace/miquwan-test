﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.ProtoContractAttribute
struct ProtoContractAttribute_t4198300782;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ProtoBuf_ImplicitFields1595963338.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoContractAttribute4198300782.h"

// System.Void ProtoBuf.ProtoContractAttribute::.ctor()
extern "C"  void ProtoContractAttribute__ctor_m555235974 (ProtoContractAttribute_t4198300782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.ProtoContractAttribute::get_Name()
extern "C"  String_t* ProtoContractAttribute_get_Name_m2361615951 (ProtoContractAttribute_t4198300782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoContractAttribute::set_Name(System.String)
extern "C"  void ProtoContractAttribute_set_Name_m2314955516 (ProtoContractAttribute_t4198300782 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoContractAttribute::get_ImplicitFirstTag()
extern "C"  int32_t ProtoContractAttribute_get_ImplicitFirstTag_m1687862728 (ProtoContractAttribute_t4198300782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoContractAttribute::set_ImplicitFirstTag(System.Int32)
extern "C"  void ProtoContractAttribute_set_ImplicitFirstTag_m2502544731 (ProtoContractAttribute_t4198300782 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoContractAttribute::get_UseProtoMembersOnly()
extern "C"  bool ProtoContractAttribute_get_UseProtoMembersOnly_m2655710835 (ProtoContractAttribute_t4198300782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoContractAttribute::set_UseProtoMembersOnly(System.Boolean)
extern "C"  void ProtoContractAttribute_set_UseProtoMembersOnly_m1438849794 (ProtoContractAttribute_t4198300782 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoContractAttribute::get_IgnoreListHandling()
extern "C"  bool ProtoContractAttribute_get_IgnoreListHandling_m2036364072 (ProtoContractAttribute_t4198300782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoContractAttribute::set_IgnoreListHandling(System.Boolean)
extern "C"  void ProtoContractAttribute_set_IgnoreListHandling_m2328648519 (ProtoContractAttribute_t4198300782 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.ImplicitFields ProtoBuf.ProtoContractAttribute::get_ImplicitFields()
extern "C"  int32_t ProtoContractAttribute_get_ImplicitFields_m3700104307 (ProtoContractAttribute_t4198300782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoContractAttribute::set_ImplicitFields(ProtoBuf.ImplicitFields)
extern "C"  void ProtoContractAttribute_set_ImplicitFields_m1749586328 (ProtoContractAttribute_t4198300782 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoContractAttribute::get_InferTagFromName()
extern "C"  bool ProtoContractAttribute_get_InferTagFromName_m4154418164 (ProtoContractAttribute_t4198300782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoContractAttribute::set_InferTagFromName(System.Boolean)
extern "C"  void ProtoContractAttribute_set_InferTagFromName_m4016963859 (ProtoContractAttribute_t4198300782 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoContractAttribute::get_InferTagFromNameHasValue()
extern "C"  bool ProtoContractAttribute_get_InferTagFromNameHasValue_m3290061387 (ProtoContractAttribute_t4198300782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoContractAttribute::get_DataMemberOffset()
extern "C"  int32_t ProtoContractAttribute_get_DataMemberOffset_m281037968 (ProtoContractAttribute_t4198300782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoContractAttribute::set_DataMemberOffset(System.Int32)
extern "C"  void ProtoContractAttribute_set_DataMemberOffset_m902262819 (ProtoContractAttribute_t4198300782 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoContractAttribute::get_SkipConstructor()
extern "C"  bool ProtoContractAttribute_get_SkipConstructor_m1909577642 (ProtoContractAttribute_t4198300782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoContractAttribute::set_SkipConstructor(System.Boolean)
extern "C"  void ProtoContractAttribute_set_SkipConstructor_m1922958329 (ProtoContractAttribute_t4198300782 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoContractAttribute::get_AsReferenceDefault()
extern "C"  bool ProtoContractAttribute_get_AsReferenceDefault_m4218320027 (ProtoContractAttribute_t4198300782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoContractAttribute::set_AsReferenceDefault(System.Boolean)
extern "C"  void ProtoContractAttribute_set_AsReferenceDefault_m2505398138 (ProtoContractAttribute_t4198300782 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoContractAttribute::HasFlag(System.Byte)
extern "C"  bool ProtoContractAttribute_HasFlag_m914762861 (ProtoContractAttribute_t4198300782 * __this, uint8_t ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoContractAttribute::SetFlag(System.Byte,System.Boolean)
extern "C"  void ProtoContractAttribute_SetFlag_m1860916356 (ProtoContractAttribute_t4198300782 * __this, uint8_t ___flag0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoContractAttribute::get_EnumPassthru()
extern "C"  bool ProtoContractAttribute_get_EnumPassthru_m2873057404 (ProtoContractAttribute_t4198300782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoContractAttribute::set_EnumPassthru(System.Boolean)
extern "C"  void ProtoContractAttribute_set_EnumPassthru_m4150330267 (ProtoContractAttribute_t4198300782 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoContractAttribute::get_EnumPassthruHasValue()
extern "C"  bool ProtoContractAttribute_get_EnumPassthruHasValue_m2321108691 (ProtoContractAttribute_t4198300782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoContractAttribute::ilo_HasFlag1(ProtoBuf.ProtoContractAttribute,System.Byte)
extern "C"  bool ProtoContractAttribute_ilo_HasFlag1_m2982609642 (Il2CppObject * __this /* static, unused */, ProtoContractAttribute_t4198300782 * ____this0, uint8_t ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoContractAttribute::ilo_SetFlag2(ProtoBuf.ProtoContractAttribute,System.Byte,System.Boolean)
extern "C"  void ProtoContractAttribute_ilo_SetFlag2_m2992112448 (Il2CppObject * __this /* static, unused */, ProtoContractAttribute_t4198300782 * ____this0, uint8_t ___flag1, bool ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
