﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_TreePrototypeGenerated
struct UnityEngine_TreePrototypeGenerated_t235833907;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_TreePrototypeGenerated::.ctor()
extern "C"  void UnityEngine_TreePrototypeGenerated__ctor_m1433472968 (UnityEngine_TreePrototypeGenerated_t235833907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TreePrototypeGenerated::TreePrototype_TreePrototype1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TreePrototypeGenerated_TreePrototype_TreePrototype1_m3847289320 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TreePrototypeGenerated::TreePrototype_prefab(JSVCall)
extern "C"  void UnityEngine_TreePrototypeGenerated_TreePrototype_prefab_m2276509580 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TreePrototypeGenerated::TreePrototype_bendFactor(JSVCall)
extern "C"  void UnityEngine_TreePrototypeGenerated_TreePrototype_bendFactor_m2638866600 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TreePrototypeGenerated::__Register()
extern "C"  void UnityEngine_TreePrototypeGenerated___Register_m3167422975 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TreePrototypeGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_TreePrototypeGenerated_ilo_addJSCSRel1_m2936169092 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_TreePrototypeGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_TreePrototypeGenerated_ilo_setObject2_m971198083 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_TreePrototypeGenerated::ilo_getSingle3(System.Int32)
extern "C"  float UnityEngine_TreePrototypeGenerated_ilo_getSingle3_m1859248097 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
