﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ObjectPool_1_ge1443416954MethodDeclarations.h"

// System.Void Pathfinding.Util.ObjectPool`1<Pathfinding.RichSpecial>::.cctor()
#define ObjectPool_1__cctor_m1521217867(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1__cctor_m3373051104_gshared)(__this /* static, unused */, method)
// T Pathfinding.Util.ObjectPool`1<Pathfinding.RichSpecial>::Claim()
#define ObjectPool_1_Claim_m1860435357(__this /* static, unused */, method) ((  RichSpecial_t2303562271 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1_Claim_m1866735784_gshared)(__this /* static, unused */, method)
// System.Void Pathfinding.Util.ObjectPool`1<Pathfinding.RichSpecial>::Warmup(System.Int32)
#define ObjectPool_1_Warmup_m1701653587(__this /* static, unused */, ___count0, method) ((  void (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))ObjectPool_1_Warmup_m3419418728_gshared)(__this /* static, unused */, ___count0, method)
// System.Void Pathfinding.Util.ObjectPool`1<Pathfinding.RichSpecial>::Release(T)
#define ObjectPool_1_Release_m1476701175(__this /* static, unused */, ___obj0, method) ((  void (*) (Il2CppObject * /* static, unused */, RichSpecial_t2303562271 *, const MethodInfo*))ObjectPool_1_Release_m2971981388_gshared)(__this /* static, unused */, ___obj0, method)
// System.Void Pathfinding.Util.ObjectPool`1<Pathfinding.RichSpecial>::Clear()
#define ObjectPool_1_Clear_m934439629(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1_Clear_m4180764824_gshared)(__this /* static, unused */, method)
// System.Int32 Pathfinding.Util.ObjectPool`1<Pathfinding.RichSpecial>::GetSize()
#define ObjectPool_1_GetSize_m3875010153(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1_GetSize_m3459990516_gshared)(__this /* static, unused */, method)
