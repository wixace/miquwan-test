﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SoundStatus,SoundStatus>
struct Transform_1_t2074406482;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_SoundStatus3592938945.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SoundStatus,SoundStatus>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1138848668_gshared (Transform_1_t2074406482 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m1138848668(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t2074406482 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m1138848668_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SoundStatus,SoundStatus>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m2302132188_gshared (Transform_1_t2074406482 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m2302132188(__this, ___key0, ___value1, method) ((  int32_t (*) (Transform_1_t2074406482 *, int32_t, int32_t, const MethodInfo*))Transform_1_Invoke_m2302132188_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SoundStatus,SoundStatus>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2576733191_gshared (Transform_1_t2074406482 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m2576733191(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t2074406482 *, int32_t, int32_t, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m2576733191_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SoundStatus,SoundStatus>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m2417991470_gshared (Transform_1_t2074406482 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m2417991470(__this, ___result0, method) ((  int32_t (*) (Transform_1_t2074406482 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m2417991470_gshared)(__this, ___result0, method)
