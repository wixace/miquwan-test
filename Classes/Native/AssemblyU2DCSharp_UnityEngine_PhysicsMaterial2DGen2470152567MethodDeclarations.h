﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_PhysicsMaterial2DGenerated
struct UnityEngine_PhysicsMaterial2DGenerated_t2470152567;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_PhysicsMaterial2DGenerated::.ctor()
extern "C"  void UnityEngine_PhysicsMaterial2DGenerated__ctor_m3370332420 (UnityEngine_PhysicsMaterial2DGenerated_t2470152567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsMaterial2DGenerated::PhysicsMaterial2D_PhysicsMaterial2D1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsMaterial2DGenerated_PhysicsMaterial2D_PhysicsMaterial2D1_m1896970292 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsMaterial2DGenerated::PhysicsMaterial2D_PhysicsMaterial2D2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsMaterial2DGenerated_PhysicsMaterial2D_PhysicsMaterial2D2_m3141734773 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicsMaterial2DGenerated::PhysicsMaterial2D_bounciness(JSVCall)
extern "C"  void UnityEngine_PhysicsMaterial2DGenerated_PhysicsMaterial2D_bounciness_m1296348165 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicsMaterial2DGenerated::PhysicsMaterial2D_friction(JSVCall)
extern "C"  void UnityEngine_PhysicsMaterial2DGenerated_PhysicsMaterial2D_friction_m2218183246 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicsMaterial2DGenerated::__Register()
extern "C"  void UnityEngine_PhysicsMaterial2DGenerated___Register_m1458064963 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsMaterial2DGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_PhysicsMaterial2DGenerated_ilo_attachFinalizerObject1_m1843017819 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicsMaterial2DGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_PhysicsMaterial2DGenerated_ilo_addJSCSRel2_m945650241 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_PhysicsMaterial2DGenerated::ilo_getObject3(System.Int32)
extern "C"  int32_t UnityEngine_PhysicsMaterial2DGenerated_ilo_getObject3_m511217296 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_PhysicsMaterial2DGenerated::ilo_getSingle4(System.Int32)
extern "C"  float UnityEngine_PhysicsMaterial2DGenerated_ilo_getSingle4_m610156646 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
