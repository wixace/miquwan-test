﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._df271b7f2a0b1f3ba4ae4ea1317db4f3
struct _df271b7f2a0b1f3ba4ae4ea1317db4f3_t656539399;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__df271b7f2a0b1f3ba4ae4ea13656539399.h"

// System.Void Little._df271b7f2a0b1f3ba4ae4ea1317db4f3::.ctor()
extern "C"  void _df271b7f2a0b1f3ba4ae4ea1317db4f3__ctor_m3748798214 (_df271b7f2a0b1f3ba4ae4ea1317db4f3_t656539399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._df271b7f2a0b1f3ba4ae4ea1317db4f3::_df271b7f2a0b1f3ba4ae4ea1317db4f3m2(System.Int32)
extern "C"  int32_t _df271b7f2a0b1f3ba4ae4ea1317db4f3__df271b7f2a0b1f3ba4ae4ea1317db4f3m2_m945290297 (_df271b7f2a0b1f3ba4ae4ea1317db4f3_t656539399 * __this, int32_t ____df271b7f2a0b1f3ba4ae4ea1317db4f3a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._df271b7f2a0b1f3ba4ae4ea1317db4f3::_df271b7f2a0b1f3ba4ae4ea1317db4f3m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _df271b7f2a0b1f3ba4ae4ea1317db4f3__df271b7f2a0b1f3ba4ae4ea1317db4f3m_m2730221213 (_df271b7f2a0b1f3ba4ae4ea1317db4f3_t656539399 * __this, int32_t ____df271b7f2a0b1f3ba4ae4ea1317db4f3a0, int32_t ____df271b7f2a0b1f3ba4ae4ea1317db4f3661, int32_t ____df271b7f2a0b1f3ba4ae4ea1317db4f3c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._df271b7f2a0b1f3ba4ae4ea1317db4f3::ilo__df271b7f2a0b1f3ba4ae4ea1317db4f3m21(Little._df271b7f2a0b1f3ba4ae4ea1317db4f3,System.Int32)
extern "C"  int32_t _df271b7f2a0b1f3ba4ae4ea1317db4f3_ilo__df271b7f2a0b1f3ba4ae4ea1317db4f3m21_m2676693678 (Il2CppObject * __this /* static, unused */, _df271b7f2a0b1f3ba4ae4ea1317db4f3_t656539399 * ____this0, int32_t ____df271b7f2a0b1f3ba4ae4ea1317db4f3a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
