﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr_DGetV_1_gen4048467712MethodDeclarations.h"

// System.Void JSDataExchangeMgr/DGetV`1<SpringPosition/OnFinished>::.ctor(System.Object,System.IntPtr)
#define DGetV_1__ctor_m3298377551(__this, ___object0, ___method1, method) ((  void (*) (DGetV_1_t2277488727 *, Il2CppObject *, IntPtr_t, const MethodInfo*))DGetV_1__ctor_m3356914363_gshared)(__this, ___object0, ___method1, method)
// T JSDataExchangeMgr/DGetV`1<SpringPosition/OnFinished>::Invoke()
#define DGetV_1_Invoke_m1797879720(__this, method) ((  OnFinished_t2399837386 * (*) (DGetV_1_t2277488727 *, const MethodInfo*))DGetV_1_Invoke_m1509146772_gshared)(__this, method)
// System.IAsyncResult JSDataExchangeMgr/DGetV`1<SpringPosition/OnFinished>::BeginInvoke(System.AsyncCallback,System.Object)
#define DGetV_1_BeginInvoke_m1899580026(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (DGetV_1_t2277488727 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))DGetV_1_BeginInvoke_m2681184398_gshared)(__this, ___callback0, ___object1, method)
// T JSDataExchangeMgr/DGetV`1<SpringPosition/OnFinished>::EndInvoke(System.IAsyncResult)
#define DGetV_1_EndInvoke_m3049696990(__this, ___result0, method) ((  OnFinished_t2399837386 * (*) (DGetV_1_t2277488727 *, Il2CppObject *, const MethodInfo*))DGetV_1_EndInvoke_m1776565962_gshared)(__this, ___result0, method)
