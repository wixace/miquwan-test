﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.WrapperMethodBuilder
struct WrapperMethodBuilder_t1311238135;
// System.Type
struct Type_t;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t1918497079;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.Emit.ILGenerator
struct ILGenerator_t1499877190;
// System.Reflection.MethodBase
struct MethodBase_t318515428;
// System.Type[]
struct TypeU5BU5D_t3339007067;
// System.Collections.Generic.ICollection`1<System.Reflection.ParameterInfo>
struct ICollection_1_t3130064036;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.ParameterInfo
struct ParameterInfo_t2235474049;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Reflection_Emit_TypeBuilder1918497079.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator1499877190.h"
#include "mscorlib_System_Reflection_MethodBase318515428.h"
#include "mscorlib_System_Reflection_FieldInfo3973053266.h"
#include "mscorlib_System_Reflection_ParameterInfo2235474049.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Wrappe1311238135.h"

// System.Void Newtonsoft.Json.Utilities.WrapperMethodBuilder::.ctor(System.Type,System.Reflection.Emit.TypeBuilder)
extern "C"  void WrapperMethodBuilder__ctor_m497383230 (WrapperMethodBuilder_t1311238135 * __this, Type_t * ___realObjectType0, TypeBuilder_t1918497079 * ___proxyBuilder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.WrapperMethodBuilder::Generate(System.Reflection.MethodInfo)
extern "C"  void WrapperMethodBuilder_Generate_m2674289387 (WrapperMethodBuilder_t1311238135 * __this, MethodInfo_t * ___newMethod0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.WrapperMethodBuilder::Return(System.Reflection.Emit.ILGenerator)
extern "C"  void WrapperMethodBuilder_Return_m1214274816 (Il2CppObject * __this /* static, unused */, ILGenerator_t1499877190 * ___ilGenerator0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.WrapperMethodBuilder::ExecuteMethod(System.Reflection.MethodBase,System.Type[],System.Reflection.Emit.ILGenerator)
extern "C"  void WrapperMethodBuilder_ExecuteMethod_m2048490403 (WrapperMethodBuilder_t1311238135 * __this, MethodBase_t318515428 * ___newMethod0, TypeU5BU5D_t3339007067* ___parameterTypes1, ILGenerator_t1499877190 * ___ilGenerator2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo Newtonsoft.Json.Utilities.WrapperMethodBuilder::GetMethod(System.Reflection.MethodBase,System.Type[])
extern "C"  MethodInfo_t * WrapperMethodBuilder_GetMethod_m2052040353 (WrapperMethodBuilder_t1311238135 * __this, MethodBase_t318515428 * ___realMethod0, TypeU5BU5D_t3339007067* ___parameterTypes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.WrapperMethodBuilder::PushParameters(System.Collections.Generic.ICollection`1<System.Reflection.ParameterInfo>,System.Reflection.Emit.ILGenerator)
extern "C"  void WrapperMethodBuilder_PushParameters_m3976918598 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___parameters0, ILGenerator_t1499877190 * ___ilGenerator1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.WrapperMethodBuilder::LoadUnderlyingObject(System.Reflection.Emit.ILGenerator,System.Reflection.FieldInfo)
extern "C"  void WrapperMethodBuilder_LoadUnderlyingObject_m2407136506 (Il2CppObject * __this /* static, unused */, ILGenerator_t1499877190 * ___ilGenerator0, FieldInfo_t * ___srcField1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Utilities.WrapperMethodBuilder::<Generate>m__39C(System.Reflection.ParameterInfo)
extern "C"  Type_t * WrapperMethodBuilder_U3CGenerateU3Em__39C_m3712410847 (Il2CppObject * __this /* static, unused */, ParameterInfo_t2235474049 * ___parameter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.WrapperMethodBuilder::<Generate>m__39D(System.Type)
extern "C"  String_t* WrapperMethodBuilder_U3CGenerateU3Em__39D_m1044971503 (Il2CppObject * __this /* static, unused */, Type_t * ___arg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.WrapperMethodBuilder::ilo_LoadUnderlyingObject1(System.Reflection.Emit.ILGenerator,System.Reflection.FieldInfo)
extern "C"  void WrapperMethodBuilder_ilo_LoadUnderlyingObject1_m1431687044 (Il2CppObject * __this /* static, unused */, ILGenerator_t1499877190 * ___ilGenerator0, FieldInfo_t * ___srcField1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.WrapperMethodBuilder::ilo_ExecuteMethod2(Newtonsoft.Json.Utilities.WrapperMethodBuilder,System.Reflection.MethodBase,System.Type[],System.Reflection.Emit.ILGenerator)
extern "C"  void WrapperMethodBuilder_ilo_ExecuteMethod2_m2492634852 (Il2CppObject * __this /* static, unused */, WrapperMethodBuilder_t1311238135 * ____this0, MethodBase_t318515428 * ___newMethod1, TypeU5BU5D_t3339007067* ___parameterTypes2, ILGenerator_t1499877190 * ___ilGenerator3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo Newtonsoft.Json.Utilities.WrapperMethodBuilder::ilo_GetMethod3(Newtonsoft.Json.Utilities.WrapperMethodBuilder,System.Reflection.MethodBase,System.Type[])
extern "C"  MethodInfo_t * WrapperMethodBuilder_ilo_GetMethod3_m3443048415 (Il2CppObject * __this /* static, unused */, WrapperMethodBuilder_t1311238135 * ____this0, MethodBase_t318515428 * ___realMethod1, TypeU5BU5D_t3339007067* ___parameterTypes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
