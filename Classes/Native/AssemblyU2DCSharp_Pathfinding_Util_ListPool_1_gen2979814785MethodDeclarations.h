﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ListPool_1_gen2456319425MethodDeclarations.h"

// System.Void Pathfinding.Util.ListPool`1<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>::.cctor()
#define ListPool_1__cctor_m2100959490(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1__cctor_m2656574753_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>::Claim()
#define ListPool_1_Claim_m3637513338(__this /* static, unused */, method) ((  List_1_t1767529987 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1_Claim_m2151275825_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>::Claim(System.Int32)
#define ListPool_1_Claim_m102554315(__this /* static, unused */, ___capacity0, method) ((  List_1_t1767529987 * (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))ListPool_1_Claim_m3438825730_gshared)(__this /* static, unused */, ___capacity0, method)
// System.Void Pathfinding.Util.ListPool`1<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>::Warmup(System.Int32,System.Int32)
#define ListPool_1_Warmup_m2208035725(__this /* static, unused */, ___count0, ___size1, method) ((  void (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))ListPool_1_Warmup_m3738927182_gshared)(__this /* static, unused */, ___count0, ___size1, method)
// System.Void Pathfinding.Util.ListPool`1<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>::Release(System.Collections.Generic.List`1<T>)
#define ListPool_1_Release_m759972296(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, List_1_t1767529987 *, const MethodInfo*))ListPool_1_Release_m442610953_gshared)(__this /* static, unused */, ___list0, method)
// System.Void Pathfinding.Util.ListPool`1<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>::Clear()
#define ListPool_1_Clear_m3446992950(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1_Clear_m3464916023_gshared)(__this /* static, unused */, method)
// System.Int32 Pathfinding.Util.ListPool`1<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>::GetSize()
#define ListPool_1_GetSize_m140867182(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1_GetSize_m4059414611_gshared)(__this /* static, unused */, method)
