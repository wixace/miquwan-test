﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_OcclusionAreaGenerated
struct UnityEngine_OcclusionAreaGenerated_t1562294877;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_OcclusionAreaGenerated::.ctor()
extern "C"  void UnityEngine_OcclusionAreaGenerated__ctor_m3517148894 (UnityEngine_OcclusionAreaGenerated_t1562294877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_OcclusionAreaGenerated::OcclusionArea_OcclusionArea1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_OcclusionAreaGenerated_OcclusionArea_OcclusionArea1_m1732875494 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_OcclusionAreaGenerated::OcclusionArea_center(JSVCall)
extern "C"  void UnityEngine_OcclusionAreaGenerated_OcclusionArea_center_m4209295111 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_OcclusionAreaGenerated::OcclusionArea_size(JSVCall)
extern "C"  void UnityEngine_OcclusionAreaGenerated_OcclusionArea_size_m1867063899 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_OcclusionAreaGenerated::__Register()
extern "C"  void UnityEngine_OcclusionAreaGenerated___Register_m1371973801 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_OcclusionAreaGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_OcclusionAreaGenerated_ilo_attachFinalizerObject1_m2213156801 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_OcclusionAreaGenerated::ilo_getVector3S2(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_OcclusionAreaGenerated_ilo_getVector3S2_m2317802973 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_OcclusionAreaGenerated::ilo_setVector3S3(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_OcclusionAreaGenerated_ilo_setVector3S3_m3162286123 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
