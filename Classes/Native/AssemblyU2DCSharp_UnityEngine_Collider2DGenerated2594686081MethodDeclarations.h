﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Collider2DGenerated
struct UnityEngine_Collider2DGenerated_t2594686081;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_Collider2DGenerated::.ctor()
extern "C"  void UnityEngine_Collider2DGenerated__ctor_m3132339210 (UnityEngine_Collider2DGenerated_t2594686081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Collider2DGenerated::Collider2D_Collider2D1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Collider2DGenerated_Collider2D_Collider2D1_m901764008 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Collider2DGenerated::Collider2D_density(JSVCall)
extern "C"  void UnityEngine_Collider2DGenerated_Collider2D_density_m1084489246 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Collider2DGenerated::Collider2D_isTrigger(JSVCall)
extern "C"  void UnityEngine_Collider2DGenerated_Collider2D_isTrigger_m389268632 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Collider2DGenerated::Collider2D_usedByEffector(JSVCall)
extern "C"  void UnityEngine_Collider2DGenerated_Collider2D_usedByEffector_m3606265278 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Collider2DGenerated::Collider2D_offset(JSVCall)
extern "C"  void UnityEngine_Collider2DGenerated_Collider2D_offset_m3417558483 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Collider2DGenerated::Collider2D_attachedRigidbody(JSVCall)
extern "C"  void UnityEngine_Collider2DGenerated_Collider2D_attachedRigidbody_m3125698333 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Collider2DGenerated::Collider2D_shapeCount(JSVCall)
extern "C"  void UnityEngine_Collider2DGenerated_Collider2D_shapeCount_m3657439960 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Collider2DGenerated::Collider2D_bounds(JSVCall)
extern "C"  void UnityEngine_Collider2DGenerated_Collider2D_bounds_m1986167281 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Collider2DGenerated::Collider2D_sharedMaterial(JSVCall)
extern "C"  void UnityEngine_Collider2DGenerated_Collider2D_sharedMaterial_m3219193306 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Collider2DGenerated::Collider2D_IsTouching__Collider2D(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Collider2DGenerated_Collider2D_IsTouching__Collider2D_m379595128 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Collider2DGenerated::Collider2D_IsTouchingLayers__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Collider2DGenerated_Collider2D_IsTouchingLayers__Int32_m1513686908 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Collider2DGenerated::Collider2D_IsTouchingLayers(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Collider2DGenerated_Collider2D_IsTouchingLayers_m3193583956 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Collider2DGenerated::Collider2D_OverlapPoint__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Collider2DGenerated_Collider2D_OverlapPoint__Vector2_m2358579875 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Collider2DGenerated::__Register()
extern "C"  void UnityEngine_Collider2DGenerated___Register_m2620309245 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Collider2DGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_Collider2DGenerated_ilo_attachFinalizerObject1_m3014083373 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Collider2DGenerated::ilo_setSingle2(System.Int32,System.Single)
extern "C"  void UnityEngine_Collider2DGenerated_ilo_setSingle2_m3632200875 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Collider2DGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void UnityEngine_Collider2DGenerated_ilo_setBooleanS3_m4127135289 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Collider2DGenerated::ilo_getBooleanS4(System.Int32)
extern "C"  bool UnityEngine_Collider2DGenerated_ilo_getBooleanS4_m560868445 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine_Collider2DGenerated::ilo_getVector2S5(System.Int32)
extern "C"  Vector2_t4282066565  UnityEngine_Collider2DGenerated_ilo_getVector2S5_m1978139710 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Collider2DGenerated::ilo_setObject6(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_Collider2DGenerated_ilo_setObject6_m4182335273 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_Collider2DGenerated::ilo_getObject7(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_Collider2DGenerated_ilo_getObject7_m3726227617 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
