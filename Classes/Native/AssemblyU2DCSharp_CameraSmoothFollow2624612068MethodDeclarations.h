﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraSmoothFollow
struct CameraSmoothFollow_t2624612068;
// UnityEngine.Transform
struct Transform_t1659122786;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.Action
struct Action_t3771233898;
// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t1407064410;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "System_Core_System_Action3771233898.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_CameraSmoothFollow2624612068.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_Collections_Hashtable1407064410.h"

// System.Void CameraSmoothFollow::.ctor()
extern "C"  void CameraSmoothFollow__ctor_m238098999 (CameraSmoothFollow_t2624612068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraSmoothFollow::set_IsOpeningMoveUpdate(System.Boolean)
extern "C"  void CameraSmoothFollow_set_IsOpeningMoveUpdate_m3467844887 (CameraSmoothFollow_t2624612068 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraSmoothFollow::get_IsOpeningMoveUpdate()
extern "C"  bool CameraSmoothFollow_get_IsOpeningMoveUpdate_m3597207904 (CameraSmoothFollow_t2624612068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform CameraSmoothFollow::get_target()
extern "C"  Transform_t1659122786 * CameraSmoothFollow_get_target_m4211449733 (CameraSmoothFollow_t2624612068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraSmoothFollow::set_target(UnityEngine.Transform)
extern "C"  void CameraSmoothFollow_set_target_m851545730 (CameraSmoothFollow_t2624612068 * __this, Transform_t1659122786 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CameraSmoothFollow::DoConfigDataCamera(System.Single)
extern "C"  Il2CppObject * CameraSmoothFollow_DoConfigDataCamera_m1090822106 (CameraSmoothFollow_t2624612068 * __this, float ___timer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraSmoothFollow::Awake()
extern "C"  void CameraSmoothFollow_Awake_m475704218 (CameraSmoothFollow_t2624612068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraSmoothFollow::BattleStart(CEvent.ZEvent)
extern "C"  void CameraSmoothFollow_BattleStart_m1183300220 (CameraSmoothFollow_t2624612068 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraSmoothFollow::BattelEnd(CEvent.ZEvent)
extern "C"  void CameraSmoothFollow_BattelEnd_m638834065 (CameraSmoothFollow_t2624612068 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraSmoothFollow::AddItween(CEvent.ZEvent)
extern "C"  void CameraSmoothFollow_AddItween_m2002652995 (CameraSmoothFollow_t2624612068 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraSmoothFollow::ChangeTarget(UnityEngine.Transform,System.Single,System.Action)
extern "C"  void CameraSmoothFollow_ChangeTarget_m1290094347 (CameraSmoothFollow_t2624612068 * __this, Transform_t1659122786 * ____target0, float ___time1, Action_t3771233898 * ____onFinished2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraSmoothFollow::ChangeTarget(UnityEngine.Transform)
extern "C"  void CameraSmoothFollow_ChangeTarget_m751775407 (CameraSmoothFollow_t2624612068 * __this, Transform_t1659122786 * ____target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraSmoothFollow::PlayMove(UnityEngine.Transform,System.Action)
extern "C"  void CameraSmoothFollow_PlayMove_m866983906 (CameraSmoothFollow_t2624612068 * __this, Transform_t1659122786 * ____target0, Action_t3771233898 * ____onFinished1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraSmoothFollow::LateUpdate()
extern "C"  void CameraSmoothFollow_LateUpdate_m1562311292 (CameraSmoothFollow_t2624612068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CameraSmoothFollow::OffsetPos()
extern "C"  Vector3_t4282066566  CameraSmoothFollow_OffsetPos_m1275858204 (CameraSmoothFollow_t2624612068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraSmoothFollow::InitDamping()
extern "C"  void CameraSmoothFollow_InitDamping_m3490996647 (CameraSmoothFollow_t2624612068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraSmoothFollow::ilo_ChangeTarget1(CameraSmoothFollow,UnityEngine.Transform)
extern "C"  void CameraSmoothFollow_ilo_ChangeTarget1_m2683113221 (Il2CppObject * __this /* static, unused */, CameraSmoothFollow_t2624612068 * ____this0, Transform_t1659122786 * ____target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CameraSmoothFollow::ilo_FormatVector3ByStr2(System.String,System.Char)
extern "C"  Vector3_t4282066566  CameraSmoothFollow_ilo_FormatVector3ByStr2_m3674601568 (Il2CppObject * __this /* static, unused */, String_t* ___str0, Il2CppChar ___c1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable CameraSmoothFollow::ilo_Hash3(System.Object[])
extern "C"  Hashtable_t1407064410 * CameraSmoothFollow_ilo_Hash3_m1130805792 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraSmoothFollow::ilo_MoveTo4(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void CameraSmoothFollow_ilo_MoveTo4_m4139234940 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Hashtable_t1407064410 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraSmoothFollow::ilo_set_target5(CameraSmoothFollow,UnityEngine.Transform)
extern "C"  void CameraSmoothFollow_ilo_set_target5_m3720067484 (Il2CppObject * __this /* static, unused */, CameraSmoothFollow_t2624612068 * ____this0, Transform_t1659122786 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraSmoothFollow::ilo_get_isshake6()
extern "C"  bool CameraSmoothFollow_ilo_get_isshake6_m1701458007 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform CameraSmoothFollow::ilo_get_target7(CameraSmoothFollow)
extern "C"  Transform_t1659122786 * CameraSmoothFollow_ilo_get_target7_m2686083773 (Il2CppObject * __this /* static, unused */, CameraSmoothFollow_t2624612068 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
