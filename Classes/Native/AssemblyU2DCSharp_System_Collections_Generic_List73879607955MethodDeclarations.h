﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_Exists_GetDelegate_member12_arg0>c__AnonStorey7D`1<System.Object>
struct U3CListA1_Exists_GetDelegate_member12_arg0U3Ec__AnonStorey7D_1_t3879607955;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_Exists_GetDelegate_member12_arg0>c__AnonStorey7D`1<System.Object>::.ctor()
extern "C"  void U3CListA1_Exists_GetDelegate_member12_arg0U3Ec__AnonStorey7D_1__ctor_m3933060306_gshared (U3CListA1_Exists_GetDelegate_member12_arg0U3Ec__AnonStorey7D_1_t3879607955 * __this, const MethodInfo* method);
#define U3CListA1_Exists_GetDelegate_member12_arg0U3Ec__AnonStorey7D_1__ctor_m3933060306(__this, method) ((  void (*) (U3CListA1_Exists_GetDelegate_member12_arg0U3Ec__AnonStorey7D_1_t3879607955 *, const MethodInfo*))U3CListA1_Exists_GetDelegate_member12_arg0U3Ec__AnonStorey7D_1__ctor_m3933060306_gshared)(__this, method)
// System.Boolean System_Collections_Generic_List77Generated/<ListA1_Exists_GetDelegate_member12_arg0>c__AnonStorey7D`1<System.Object>::<>m__A2(T)
extern "C"  bool U3CListA1_Exists_GetDelegate_member12_arg0U3Ec__AnonStorey7D_1_U3CU3Em__A2_m2196246182_gshared (U3CListA1_Exists_GetDelegate_member12_arg0U3Ec__AnonStorey7D_1_t3879607955 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CListA1_Exists_GetDelegate_member12_arg0U3Ec__AnonStorey7D_1_U3CU3Em__A2_m2196246182(__this, ___obj0, method) ((  bool (*) (U3CListA1_Exists_GetDelegate_member12_arg0U3Ec__AnonStorey7D_1_t3879607955 *, Il2CppObject *, const MethodInfo*))U3CListA1_Exists_GetDelegate_member12_arg0U3Ec__AnonStorey7D_1_U3CU3Em__A2_m2196246182_gshared)(__this, ___obj0, method)
