﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4273597365MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.AnimationClip,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m2185105440(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3056101378 *, Dictionary_2_t1429341927 *, const MethodInfo*))KeyCollection__ctor_m1198833407_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.AnimationClip,System.Boolean>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m635811126(__this, ___item0, method) ((  void (*) (KeyCollection_t3056101378 *, AnimationClip_t2007702890 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1168570103_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.AnimationClip,System.Boolean>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1868804141(__this, method) ((  void (*) (KeyCollection_t3056101378 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1784442414_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.AnimationClip,System.Boolean>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1822193368(__this, ___item0, method) ((  bool (*) (KeyCollection_t3056101378 *, AnimationClip_t2007702890 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m535664823_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.AnimationClip,System.Boolean>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2630401597(__this, ___item0, method) ((  bool (*) (KeyCollection_t3056101378 *, AnimationClip_t2007702890 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2137443292_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.AnimationClip,System.Boolean>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3802771775(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3056101378 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3572571840_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.AnimationClip,System.Boolean>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m885373215(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3056101378 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2836692384_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.AnimationClip,System.Boolean>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m199593966(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3056101378 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1208832431_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.AnimationClip,System.Boolean>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1662228537(__this, method) ((  bool (*) (KeyCollection_t3056101378 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2258878040_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.AnimationClip,System.Boolean>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1095951403(__this, method) ((  bool (*) (KeyCollection_t3056101378 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1081562378_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.AnimationClip,System.Boolean>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2024109661(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3056101378 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3445305084_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.AnimationClip,System.Boolean>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m2022383431(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3056101378 *, AnimationClipU5BU5D_t4186127791*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1676009908_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.AnimationClip,System.Boolean>::GetEnumerator()
#define KeyCollection_GetEnumerator_m2994814946(__this, method) ((  Enumerator_t2044277981  (*) (KeyCollection_t3056101378 *, const MethodInfo*))KeyCollection_GetEnumerator_m3924361409_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.AnimationClip,System.Boolean>::get_Count()
#define KeyCollection_get_Count_m1779986277(__this, method) ((  int32_t (*) (KeyCollection_t3056101378 *, const MethodInfo*))KeyCollection_get_Count_m2539602500_gshared)(__this, method)
