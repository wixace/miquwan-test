﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_caicemnir124
struct M_caicemnir124_t4200694444;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_caicemnir1244200694444.h"

// System.Void GarbageiOS.M_caicemnir124::.ctor()
extern "C"  void M_caicemnir124__ctor_m1099297031 (M_caicemnir124_t4200694444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_caicemnir124::M_nojallheeQugall0(System.String[],System.Int32)
extern "C"  void M_caicemnir124_M_nojallheeQugall0_m3412647592 (M_caicemnir124_t4200694444 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_caicemnir124::M_bervowNepu1(System.String[],System.Int32)
extern "C"  void M_caicemnir124_M_bervowNepu1_m3048648976 (M_caicemnir124_t4200694444 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_caicemnir124::M_furdi2(System.String[],System.Int32)
extern "C"  void M_caicemnir124_M_furdi2_m2054677008 (M_caicemnir124_t4200694444 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_caicemnir124::M_qestor3(System.String[],System.Int32)
extern "C"  void M_caicemnir124_M_qestor3_m2801703269 (M_caicemnir124_t4200694444 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_caicemnir124::M_tihouseKozor4(System.String[],System.Int32)
extern "C"  void M_caicemnir124_M_tihouseKozor4_m1269084304 (M_caicemnir124_t4200694444 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_caicemnir124::M_gadelhouFowjirme5(System.String[],System.Int32)
extern "C"  void M_caicemnir124_M_gadelhouFowjirme5_m4185376389 (M_caicemnir124_t4200694444 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_caicemnir124::M_tayhay6(System.String[],System.Int32)
extern "C"  void M_caicemnir124_M_tayhay6_m2999996236 (M_caicemnir124_t4200694444 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_caicemnir124::M_perawrarSudrir7(System.String[],System.Int32)
extern "C"  void M_caicemnir124_M_perawrarSudrir7_m1418601848 (M_caicemnir124_t4200694444 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_caicemnir124::ilo_M_furdi21(GarbageiOS.M_caicemnir124,System.String[],System.Int32)
extern "C"  void M_caicemnir124_ilo_M_furdi21_m302810870 (Il2CppObject * __this /* static, unused */, M_caicemnir124_t4200694444 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_caicemnir124::ilo_M_qestor32(GarbageiOS.M_caicemnir124,System.String[],System.Int32)
extern "C"  void M_caicemnir124_ilo_M_qestor32_m28914888 (Il2CppObject * __this /* static, unused */, M_caicemnir124_t4200694444 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_caicemnir124::ilo_M_gadelhouFowjirme53(GarbageiOS.M_caicemnir124,System.String[],System.Int32)
extern "C"  void M_caicemnir124_ilo_M_gadelhouFowjirme53_m2868930793 (Il2CppObject * __this /* static, unused */, M_caicemnir124_t4200694444 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
