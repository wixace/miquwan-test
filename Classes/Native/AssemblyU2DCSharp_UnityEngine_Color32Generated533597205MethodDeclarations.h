﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Color32Generated
struct UnityEngine_Color32Generated_t533597205;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_Color32Generated::.ctor()
extern "C"  void UnityEngine_Color32Generated__ctor_m1457946150 (UnityEngine_Color32Generated_t533597205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Color32Generated::.cctor()
extern "C"  void UnityEngine_Color32Generated__cctor_m1764561479 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Color32Generated::Color32_Color321(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Color32Generated_Color32_Color321_m1751663694 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Color32Generated::Color32_Color322(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Color32Generated_Color32_Color322_m2996428175 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Color32Generated::Color32_r(JSVCall)
extern "C"  void UnityEngine_Color32Generated_Color32_r_m1778077326 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Color32Generated::Color32_g(JSVCall)
extern "C"  void UnityEngine_Color32Generated_Color32_g_m3939725881 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Color32Generated::Color32_b(JSVCall)
extern "C"  void UnityEngine_Color32Generated_Color32_b_m627326110 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Color32Generated::Color32_a(JSVCall)
extern "C"  void UnityEngine_Color32Generated_Color32_a_m823839615 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Color32Generated::Color32_ToString__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Color32Generated_Color32_ToString__String_m2009290460 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Color32Generated::Color32_ToString(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Color32Generated_Color32_ToString_m2802799499 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Color32Generated::Color32_Lerp__Color32__Color32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Color32Generated_Color32_Lerp__Color32__Color32__Single_m931885982 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Color32Generated::Color32_LerpUnclamped__Color32__Color32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Color32Generated_Color32_LerpUnclamped__Color32__Color32__Single_m1063085301 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Color32Generated::Color32_op_Implicit__Color32_to_Color(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Color32Generated_Color32_op_Implicit__Color32_to_Color_m3992678050 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Color32Generated::Color32_op_Implicit__Color_to_Color32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Color32Generated_Color32_op_Implicit__Color_to_Color32_m2308015744 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Color32Generated::__Register()
extern "C"  void UnityEngine_Color32Generated___Register_m2985420385 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Color32Generated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_Color32Generated_ilo_getObject1_m2732666796 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Color32Generated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_Color32Generated_ilo_attachFinalizerObject2_m3819115194 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte UnityEngine_Color32Generated::ilo_getByte3(System.Int32)
extern "C"  uint8_t UnityEngine_Color32Generated_ilo_getByte3_m178735171 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Color32Generated::ilo_setByte4(System.Int32,System.Byte)
extern "C"  void UnityEngine_Color32Generated_ilo_setByte4_m2311592977 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint8_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Color32Generated::ilo_changeJSObj5(System.Int32,System.Object)
extern "C"  void UnityEngine_Color32Generated_ilo_changeJSObj5_m1904097543 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Color32Generated::ilo_setStringS6(System.Int32,System.String)
extern "C"  void UnityEngine_Color32Generated_ilo_setStringS6_m225487988 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Color32Generated::ilo_setObject7(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_Color32Generated_ilo_setObject7_m4081189034 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_Color32Generated::ilo_getObject8(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_Color32Generated_ilo_getObject8_m4199376272 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
