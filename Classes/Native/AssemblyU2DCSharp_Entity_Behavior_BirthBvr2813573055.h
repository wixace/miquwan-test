﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entity.Behavior.BirthBvr
struct  BirthBvr_t2813573055  : public IBehavior_t770859129
{
public:
	// System.Single Entity.Behavior.BirthBvr::birth_time
	float ___birth_time_3;
	// System.Single Entity.Behavior.BirthBvr::time
	float ___time_4;

public:
	inline static int32_t get_offset_of_birth_time_3() { return static_cast<int32_t>(offsetof(BirthBvr_t2813573055, ___birth_time_3)); }
	inline float get_birth_time_3() const { return ___birth_time_3; }
	inline float* get_address_of_birth_time_3() { return &___birth_time_3; }
	inline void set_birth_time_3(float value)
	{
		___birth_time_3 = value;
	}

	inline static int32_t get_offset_of_time_4() { return static_cast<int32_t>(offsetof(BirthBvr_t2813573055, ___time_4)); }
	inline float get_time_4() const { return ___time_4; }
	inline float* get_address_of_time_4() { return &___time_4; }
	inline void set_time_4(float value)
	{
		___time_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
