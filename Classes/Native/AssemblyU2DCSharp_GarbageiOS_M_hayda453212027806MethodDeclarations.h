﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_hayda45
struct M_hayda45_t3212027806;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_hayda453212027806.h"

// System.Void GarbageiOS.M_hayda45::.ctor()
extern "C"  void M_hayda45__ctor_m1287475589 (M_hayda45_t3212027806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hayda45::M_peeziMudor0(System.String[],System.Int32)
extern "C"  void M_hayda45_M_peeziMudor0_m144597656 (M_hayda45_t3212027806 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hayda45::M_houmayBearfo1(System.String[],System.Int32)
extern "C"  void M_hayda45_M_houmayBearfo1_m1235154245 (M_hayda45_t3212027806 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hayda45::ilo_M_peeziMudor01(GarbageiOS.M_hayda45,System.String[],System.Int32)
extern "C"  void M_hayda45_ilo_M_peeziMudor01_m1529350638 (Il2CppObject * __this /* static, unused */, M_hayda45_t3212027806 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
