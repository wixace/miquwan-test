﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

// UnityEngine.GameObject GameObject_Instantiate::Instantiate(UnityEngine.GameObject)
extern "C"  GameObject_t3674682005 * GameObject_Instantiate_Instantiate_m1285607306 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject GameObject_Instantiate::Instantiate(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  GameObject_t3674682005 * GameObject_Instantiate_Instantiate_m4237330196 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, Vector3_t4282066566  ___position1, Quaternion_t1553702882  ___rotation2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
