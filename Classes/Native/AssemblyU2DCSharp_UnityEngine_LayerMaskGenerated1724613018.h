﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConstructorID
struct ConstructorID_t3348888181;
// JSDataExchangeMgr/DGetV`1<System.String[]>
struct DGetV_1_t3931654293;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine_LayerMaskGenerated
struct  UnityEngine_LayerMaskGenerated_t1724613018  : public Il2CppObject
{
public:

public:
};

struct UnityEngine_LayerMaskGenerated_t1724613018_StaticFields
{
public:
	// ConstructorID UnityEngine_LayerMaskGenerated::constructorID0
	ConstructorID_t3348888181 * ___constructorID0_0;
	// JSDataExchangeMgr/DGetV`1<System.String[]> UnityEngine_LayerMaskGenerated::<>f__am$cache1
	DGetV_1_t3931654293 * ___U3CU3Ef__amU24cache1_1;

public:
	inline static int32_t get_offset_of_constructorID0_0() { return static_cast<int32_t>(offsetof(UnityEngine_LayerMaskGenerated_t1724613018_StaticFields, ___constructorID0_0)); }
	inline ConstructorID_t3348888181 * get_constructorID0_0() const { return ___constructorID0_0; }
	inline ConstructorID_t3348888181 ** get_address_of_constructorID0_0() { return &___constructorID0_0; }
	inline void set_constructorID0_0(ConstructorID_t3348888181 * value)
	{
		___constructorID0_0 = value;
		Il2CppCodeGenWriteBarrier(&___constructorID0_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(UnityEngine_LayerMaskGenerated_t1724613018_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline DGetV_1_t3931654293 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline DGetV_1_t3931654293 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(DGetV_1_t3931654293 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
