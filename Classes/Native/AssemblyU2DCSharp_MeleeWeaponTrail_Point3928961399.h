﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MeleeWeaponTrail/Point
struct  Point_t3928961399  : public Il2CppObject
{
public:
	// System.Single MeleeWeaponTrail/Point::timeCreated
	float ___timeCreated_0;
	// UnityEngine.Vector3 MeleeWeaponTrail/Point::basePosition
	Vector3_t4282066566  ___basePosition_1;
	// UnityEngine.Vector3 MeleeWeaponTrail/Point::tipPosition
	Vector3_t4282066566  ___tipPosition_2;

public:
	inline static int32_t get_offset_of_timeCreated_0() { return static_cast<int32_t>(offsetof(Point_t3928961399, ___timeCreated_0)); }
	inline float get_timeCreated_0() const { return ___timeCreated_0; }
	inline float* get_address_of_timeCreated_0() { return &___timeCreated_0; }
	inline void set_timeCreated_0(float value)
	{
		___timeCreated_0 = value;
	}

	inline static int32_t get_offset_of_basePosition_1() { return static_cast<int32_t>(offsetof(Point_t3928961399, ___basePosition_1)); }
	inline Vector3_t4282066566  get_basePosition_1() const { return ___basePosition_1; }
	inline Vector3_t4282066566 * get_address_of_basePosition_1() { return &___basePosition_1; }
	inline void set_basePosition_1(Vector3_t4282066566  value)
	{
		___basePosition_1 = value;
	}

	inline static int32_t get_offset_of_tipPosition_2() { return static_cast<int32_t>(offsetof(Point_t3928961399, ___tipPosition_2)); }
	inline Vector3_t4282066566  get_tipPosition_2() const { return ___tipPosition_2; }
	inline Vector3_t4282066566 * get_address_of_tipPosition_2() { return &___tipPosition_2; }
	inline void set_tipPosition_2(Vector3_t4282066566  value)
	{
		___tipPosition_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
