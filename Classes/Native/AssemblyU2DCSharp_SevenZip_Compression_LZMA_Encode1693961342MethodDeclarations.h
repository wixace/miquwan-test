﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.Compression.LZMA.Encoder
struct Encoder_t1693961342;
// System.IO.Stream
struct Stream_t1561764144;
// SevenZip.ICodeProgress
struct ICodeProgress_t453587813;
// System.String
struct String_t;
// SevenZip.CoderPropID[]
struct CoderPropIDU5BU5D_t3009125352;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// SevenZip.Compression.LZ.BinTree
struct BinTree_t161206647;
// SevenZip.Compression.LZMA.Encoder/LenEncoder
struct LenEncoder_t3446674564;
// SevenZip.Compression.LZ.IInWindowStream
struct IInWindowStream_t3171376208;
// SevenZip.Compression.LZMA.Encoder/LiteralEncoder
struct LiteralEncoder_t1395322410;
// SevenZip.Compression.RangeCoder.Encoder
struct Encoder_t2248006694;
// SevenZip.Compression.LZMA.Encoder/LenPriceTableEncoder
struct LenPriceTableEncoder_t2154457791;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Base_St344225277.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZ_BinTree161206647.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Encode1693961342.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Encode3446674564.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_1052492065.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Encode2578924795.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Encode1395322410.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_2248006694.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_3875796003.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Encode2154457791.h"

// System.Void SevenZip.Compression.LZMA.Encoder::.ctor()
extern "C"  void Encoder__ctor_m901181081 (Encoder_t1693961342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::.cctor()
extern "C"  void Encoder__cctor_m1684713524 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Encoder::GetPosSlot(System.UInt32)
extern "C"  uint32_t Encoder_GetPosSlot_m2457197230 (Il2CppObject * __this /* static, unused */, uint32_t ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Encoder::GetPosSlot2(System.UInt32)
extern "C"  uint32_t Encoder_GetPosSlot2_m2024196274 (Il2CppObject * __this /* static, unused */, uint32_t ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::BaseInit()
extern "C"  void Encoder_BaseInit_m89581004 (Encoder_t1693961342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::Create()
extern "C"  void Encoder_Create_m96526087 (Encoder_t1693961342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::SetWriteEndMarkerMode(System.Boolean)
extern "C"  void Encoder_SetWriteEndMarkerMode_m1656397193 (Encoder_t1693961342 * __this, bool ___writeEndMarker0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::Init()
extern "C"  void Encoder_Init_m673343643 (Encoder_t1693961342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ReadMatchDistances(System.UInt32&,System.UInt32&)
extern "C"  void Encoder_ReadMatchDistances_m2347153836 (Encoder_t1693961342 * __this, uint32_t* ___lenRes0, uint32_t* ___numDistancePairs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::MovePos(System.UInt32)
extern "C"  void Encoder_MovePos_m2705581312 (Encoder_t1693961342 * __this, uint32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Encoder::GetRepLen1Price(SevenZip.Compression.LZMA.Base/State,System.UInt32)
extern "C"  uint32_t Encoder_GetRepLen1Price_m3168409775 (Encoder_t1693961342 * __this, State_t344225277  ___state0, uint32_t ___posState1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Encoder::GetPureRepPrice(System.UInt32,SevenZip.Compression.LZMA.Base/State,System.UInt32)
extern "C"  uint32_t Encoder_GetPureRepPrice_m4001498479 (Encoder_t1693961342 * __this, uint32_t ___repIndex0, State_t344225277  ___state1, uint32_t ___posState2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Encoder::GetRepPrice(System.UInt32,System.UInt32,SevenZip.Compression.LZMA.Base/State,System.UInt32)
extern "C"  uint32_t Encoder_GetRepPrice_m4038631331 (Encoder_t1693961342 * __this, uint32_t ___repIndex0, uint32_t ___len1, State_t344225277  ___state2, uint32_t ___posState3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Encoder::GetPosLenPrice(System.UInt32,System.UInt32,System.UInt32)
extern "C"  uint32_t Encoder_GetPosLenPrice_m1577947552 (Encoder_t1693961342 * __this, uint32_t ___pos0, uint32_t ___len1, uint32_t ___posState2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Encoder::Backward(System.UInt32&,System.UInt32)
extern "C"  uint32_t Encoder_Backward_m4162042809 (Encoder_t1693961342 * __this, uint32_t* ___backRes0, uint32_t ___cur1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Encoder::GetOptimum(System.UInt32,System.UInt32&)
extern "C"  uint32_t Encoder_GetOptimum_m657768551 (Encoder_t1693961342 * __this, uint32_t ___position0, uint32_t* ___backRes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SevenZip.Compression.LZMA.Encoder::ChangePair(System.UInt32,System.UInt32)
extern "C"  bool Encoder_ChangePair_m1931094541 (Encoder_t1693961342 * __this, uint32_t ___smallDist0, uint32_t ___bigDist1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::WriteEndMarker(System.UInt32)
extern "C"  void Encoder_WriteEndMarker_m1275413817 (Encoder_t1693961342 * __this, uint32_t ___posState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::Flush(System.UInt32)
extern "C"  void Encoder_Flush_m2438134559 (Encoder_t1693961342 * __this, uint32_t ___nowPos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::CodeOneBlock(System.Int64&,System.Int64&,System.Boolean&)
extern "C"  void Encoder_CodeOneBlock_m436749662 (Encoder_t1693961342 * __this, int64_t* ___inSize0, int64_t* ___outSize1, bool* ___finished2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ReleaseMFStream()
extern "C"  void Encoder_ReleaseMFStream_m3790546871 (Encoder_t1693961342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::SetOutStream(System.IO.Stream)
extern "C"  void Encoder_SetOutStream_m3657426254 (Encoder_t1693961342 * __this, Stream_t1561764144 * ___outStream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ReleaseOutStream()
extern "C"  void Encoder_ReleaseOutStream_m2553668338 (Encoder_t1693961342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ReleaseStreams()
extern "C"  void Encoder_ReleaseStreams_m3964936951 (Encoder_t1693961342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::SetStreams(System.IO.Stream,System.IO.Stream,System.Int64,System.Int64)
extern "C"  void Encoder_SetStreams_m3293366634 (Encoder_t1693961342 * __this, Stream_t1561764144 * ___inStream0, Stream_t1561764144 * ___outStream1, int64_t ___inSize2, int64_t ___outSize3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::Code(System.IO.Stream,System.IO.Stream,System.Int64,System.Int64,SevenZip.ICodeProgress)
extern "C"  void Encoder_Code_m45927631 (Encoder_t1693961342 * __this, Stream_t1561764144 * ___inStream0, Stream_t1561764144 * ___outStream1, int64_t ___inSize2, int64_t ___outSize3, Il2CppObject * ___progress4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::WriteCoderProperties(System.IO.Stream)
extern "C"  void Encoder_WriteCoderProperties_m4066530619 (Encoder_t1693961342 * __this, Stream_t1561764144 * ___outStream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::FillDistancesPrices()
extern "C"  void Encoder_FillDistancesPrices_m2793465084 (Encoder_t1693961342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::FillAlignPrices()
extern "C"  void Encoder_FillAlignPrices_m1676157795 (Encoder_t1693961342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SevenZip.Compression.LZMA.Encoder::FindMatchFinder(System.String)
extern "C"  int32_t Encoder_FindMatchFinder_m1440886475 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::SetCoderProperties(SevenZip.CoderPropID[],System.Object[])
extern "C"  void Encoder_SetCoderProperties_m2518496898 (Encoder_t1693961342 * __this, CoderPropIDU5BU5D_t3009125352* ___propIDs0, ObjectU5BU5D_t1108656482* ___properties1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::SetTrainSize(System.UInt32)
extern "C"  void Encoder_SetTrainSize_m1059573384 (Encoder_t1693961342 * __this, uint32_t ___trainSize0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ilo_SetType1(SevenZip.Compression.LZ.BinTree,System.Int32)
extern "C"  void Encoder_ilo_SetType1_m2937771327 (Il2CppObject * __this /* static, unused */, BinTree_t161206647 * ____this0, int32_t ___numHashBytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ilo_BaseInit2(SevenZip.Compression.LZMA.Encoder)
extern "C"  void Encoder_ilo_BaseInit2_m3125583563 (Il2CppObject * __this /* static, unused */, Encoder_t1693961342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ilo_Init3(SevenZip.Compression.LZMA.Encoder/LenEncoder,System.UInt32)
extern "C"  void Encoder_ilo_Init3_m3621423991 (Il2CppObject * __this /* static, unused */, LenEncoder_t3446674564 * ____this0, uint32_t ___numPosStates1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Encoder::ilo_GetPrice04(SevenZip.Compression.RangeCoder.BitEncoder&)
extern "C"  uint32_t Encoder_ilo_GetPrice04_m371307941 (Il2CppObject * __this /* static, unused */, BitEncoder_t1052492065 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Encoder::ilo_GetPrice15(SevenZip.Compression.RangeCoder.BitEncoder&)
extern "C"  uint32_t Encoder_ilo_GetPrice15_m1158900101 (Il2CppObject * __this /* static, unused */, BitEncoder_t1052492065 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Encoder::ilo_GetPureRepPrice6(SevenZip.Compression.LZMA.Encoder,System.UInt32,SevenZip.Compression.LZMA.Base/State,System.UInt32)
extern "C"  uint32_t Encoder_ilo_GetPureRepPrice6_m3770397896 (Il2CppObject * __this /* static, unused */, Encoder_t1693961342 * ____this0, uint32_t ___repIndex1, State_t344225277  ___state2, uint32_t ___posState3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Encoder::ilo_GetNumAvailableBytes7(SevenZip.Compression.LZ.IInWindowStream)
extern "C"  uint32_t Encoder_ilo_GetNumAvailableBytes7_m3383704014 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Encoder::ilo_GetMatchLen8(SevenZip.Compression.LZ.IInWindowStream,System.Int32,System.UInt32,System.UInt32)
extern "C"  uint32_t Encoder_ilo_GetMatchLen8_m2097430850 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, int32_t ___index1, uint32_t ___distance2, uint32_t ___limit3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ilo_MovePos9(SevenZip.Compression.LZMA.Encoder,System.UInt32)
extern "C"  void Encoder_ilo_MovePos9_m1912939456 (Il2CppObject * __this /* static, unused */, Encoder_t1693961342 * ____this0, uint32_t ___num1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte SevenZip.Compression.LZMA.Encoder::ilo_GetIndexByte10(SevenZip.Compression.LZ.IInWindowStream,System.Int32)
extern "C"  uint8_t Encoder_ilo_GetIndexByte10_m1845266938 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SevenZip.Compression.LZMA.Encoder/LiteralEncoder/Encoder2 SevenZip.Compression.LZMA.Encoder::ilo_GetSubCoder11(SevenZip.Compression.LZMA.Encoder/LiteralEncoder,System.UInt32,System.Byte)
extern "C"  Encoder2_t2578924795  Encoder_ilo_GetSubCoder11_m3740585190 (Il2CppObject * __this /* static, unused */, LiteralEncoder_t1395322410 * ____this0, uint32_t ___pos1, uint8_t ___prevByte2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SevenZip.Compression.LZMA.Encoder::ilo_IsCharState12(SevenZip.Compression.LZMA.Base/State&)
extern "C"  bool Encoder_ilo_IsCharState12_m1095075391 (Il2CppObject * __this /* static, unused */, State_t344225277 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Encoder::ilo_GetRepLen1Price13(SevenZip.Compression.LZMA.Encoder,SevenZip.Compression.LZMA.Base/State,System.UInt32)
extern "C"  uint32_t Encoder_ilo_GetRepLen1Price13_m260214820 (Il2CppObject * __this /* static, unused */, Encoder_t1693961342 * ____this0, State_t344225277  ___state1, uint32_t ___posState2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Encoder::ilo_GetPosLenPrice14(SevenZip.Compression.LZMA.Encoder,System.UInt32,System.UInt32,System.UInt32)
extern "C"  uint32_t Encoder_ilo_GetPosLenPrice14_m3141001188 (Il2CppObject * __this /* static, unused */, Encoder_t1693961342 * ____this0, uint32_t ___pos1, uint32_t ___len2, uint32_t ___posState3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Encoder::ilo_GetRepPrice15(SevenZip.Compression.LZMA.Encoder,System.UInt32,System.UInt32,SevenZip.Compression.LZMA.Base/State,System.UInt32)
extern "C"  uint32_t Encoder_ilo_GetRepPrice15_m3274994074 (Il2CppObject * __this /* static, unused */, Encoder_t1693961342 * ____this0, uint32_t ___repIndex1, uint32_t ___len2, State_t344225277  ___state3, uint32_t ___posState4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ilo_UpdateRep16(SevenZip.Compression.LZMA.Base/State&)
extern "C"  void Encoder_ilo_UpdateRep16_m1071390028 (Il2CppObject * __this /* static, unused */, State_t344225277 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Encoder::ilo_GetPrice17(SevenZip.Compression.LZMA.Encoder/LiteralEncoder/Encoder2&,System.Boolean,System.Byte,System.Byte)
extern "C"  uint32_t Encoder_ilo_GetPrice17_m251747734 (Il2CppObject * __this /* static, unused */, Encoder2_t2578924795 * ____this0, bool ___matchMode1, uint8_t ___matchByte2, uint8_t ___symbol3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ilo_Encode18(SevenZip.Compression.RangeCoder.BitEncoder&,SevenZip.Compression.RangeCoder.Encoder,System.UInt32)
extern "C"  void Encoder_ilo_Encode18_m1010684620 (Il2CppObject * __this /* static, unused */, BitEncoder_t1052492065 * ____this0, Encoder_t2248006694 * ___encoder1, uint32_t ___symbol2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ilo_UpdateMatch19(SevenZip.Compression.LZMA.Base/State&)
extern "C"  void Encoder_ilo_UpdateMatch19_m119503425 (Il2CppObject * __this /* static, unused */, State_t344225277 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ilo_Encode20(SevenZip.Compression.RangeCoder.BitTreeEncoder&,SevenZip.Compression.RangeCoder.Encoder,System.UInt32)
extern "C"  void Encoder_ilo_Encode20_m3400518871 (Il2CppObject * __this /* static, unused */, BitTreeEncoder_t3875796003 * ____this0, Encoder_t2248006694 * ___rangeEncoder1, uint32_t ___symbol2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ilo_EncodeDirectBits21(SevenZip.Compression.RangeCoder.Encoder,System.UInt32,System.Int32)
extern "C"  void Encoder_ilo_EncodeDirectBits21_m1993569003 (Il2CppObject * __this /* static, unused */, Encoder_t2248006694 * ____this0, uint32_t ___v1, int32_t ___numTotalBits2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ilo_ReverseEncode22(SevenZip.Compression.RangeCoder.BitTreeEncoder&,SevenZip.Compression.RangeCoder.Encoder,System.UInt32)
extern "C"  void Encoder_ilo_ReverseEncode22_m56328109 (Il2CppObject * __this /* static, unused */, BitTreeEncoder_t3875796003 * ____this0, Encoder_t2248006694 * ___rangeEncoder1, uint32_t ___symbol2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ilo_WriteEndMarker23(SevenZip.Compression.LZMA.Encoder,System.UInt32)
extern "C"  void Encoder_ilo_WriteEndMarker23_m1906471455 (Il2CppObject * __this /* static, unused */, Encoder_t1693961342 * ____this0, uint32_t ___posState1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ilo_FlushStream24(SevenZip.Compression.RangeCoder.Encoder)
extern "C"  void Encoder_ilo_FlushStream24_m1953208240 (Il2CppObject * __this /* static, unused */, Encoder_t2248006694 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ilo_Encode25(SevenZip.Compression.LZMA.Encoder/LiteralEncoder/Encoder2&,SevenZip.Compression.RangeCoder.Encoder,System.Byte)
extern "C"  void Encoder_ilo_Encode25_m400352325 (Il2CppObject * __this /* static, unused */, Encoder2_t2578924795 * ____this0, Encoder_t2248006694 * ___rangeEncoder1, uint8_t ___symbol2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ilo_EncodeMatched26(SevenZip.Compression.LZMA.Encoder/LiteralEncoder/Encoder2&,SevenZip.Compression.RangeCoder.Encoder,System.Byte,System.Byte)
extern "C"  void Encoder_ilo_EncodeMatched26_m2100236905 (Il2CppObject * __this /* static, unused */, Encoder2_t2578924795 * ____this0, Encoder_t2248006694 * ___rangeEncoder1, uint8_t ___matchByte2, uint8_t ___symbol3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ilo_UpdateChar27(SevenZip.Compression.LZMA.Base/State&)
extern "C"  void Encoder_ilo_UpdateChar27_m434463879 (Il2CppObject * __this /* static, unused */, State_t344225277 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ilo_FillDistancesPrices28(SevenZip.Compression.LZMA.Encoder)
extern "C"  void Encoder_ilo_FillDistancesPrices28_m782296017 (Il2CppObject * __this /* static, unused */, Encoder_t1693961342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ilo_ReleaseStream29(SevenZip.Compression.RangeCoder.Encoder)
extern "C"  void Encoder_ilo_ReleaseStream29_m1468291944 (Il2CppObject * __this /* static, unused */, Encoder_t2248006694 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ilo_ReleaseMFStream30(SevenZip.Compression.LZMA.Encoder)
extern "C"  void Encoder_ilo_ReleaseMFStream30_m631222591 (Il2CppObject * __this /* static, unused */, Encoder_t1693961342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ilo_Create31(SevenZip.Compression.LZMA.Encoder)
extern "C"  void Encoder_ilo_Create31_m3429318536 (Il2CppObject * __this /* static, unused */, Encoder_t1693961342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ilo_SetOutStream32(SevenZip.Compression.LZMA.Encoder,System.IO.Stream)
extern "C"  void Encoder_ilo_SetOutStream32_m3089133958 (Il2CppObject * __this /* static, unused */, Encoder_t1693961342 * ____this0, Stream_t1561764144 * ___outStream1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ilo_UpdateTables33(SevenZip.Compression.LZMA.Encoder/LenPriceTableEncoder,System.UInt32)
extern "C"  void Encoder_ilo_UpdateTables33_m3325092481 (Il2CppObject * __this /* static, unused */, LenPriceTableEncoder_t2154457791 * ____this0, uint32_t ___numPosStates1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ilo_SetProgressPercent34(SevenZip.ICodeProgress,System.Int64,System.Int64)
extern "C"  void Encoder_ilo_SetProgressPercent34_m3532183212 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, int64_t ___fileSize1, int64_t ___processSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder::ilo_ReleaseStreams35(SevenZip.Compression.LZMA.Encoder)
extern "C"  void Encoder_ilo_ReleaseStreams35_m4211972628 (Il2CppObject * __this /* static, unused */, Encoder_t1693961342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Encoder::ilo_GetPosSlot36(System.UInt32)
extern "C"  uint32_t Encoder_ilo_GetPosSlot36_m3043265048 (Il2CppObject * __this /* static, unused */, uint32_t ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SevenZip.Compression.LZMA.Encoder::ilo_FindMatchFinder37(System.String)
extern "C"  int32_t Encoder_ilo_FindMatchFinder37_m1399838106 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
