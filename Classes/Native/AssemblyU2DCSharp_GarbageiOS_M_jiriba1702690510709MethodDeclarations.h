﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_jiriba170
struct M_jiriba170_t2690510709;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_jiriba1702690510709.h"

// System.Void GarbageiOS.M_jiriba170::.ctor()
extern "C"  void M_jiriba170__ctor_m3807692174 (M_jiriba170_t2690510709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jiriba170::M_misdiCemsear0(System.String[],System.Int32)
extern "C"  void M_jiriba170_M_misdiCemsear0_m1708812079 (M_jiriba170_t2690510709 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jiriba170::M_terremsiWeselyere1(System.String[],System.Int32)
extern "C"  void M_jiriba170_M_terremsiWeselyere1_m1368167892 (M_jiriba170_t2690510709 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jiriba170::M_sawchudra2(System.String[],System.Int32)
extern "C"  void M_jiriba170_M_sawchudra2_m466132453 (M_jiriba170_t2690510709 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jiriba170::ilo_M_misdiCemsear01(GarbageiOS.M_jiriba170,System.String[],System.Int32)
extern "C"  void M_jiriba170_ilo_M_misdiCemsear01_m3226522204 (Il2CppObject * __this /* static, unused */, M_jiriba170_t2690510709 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
