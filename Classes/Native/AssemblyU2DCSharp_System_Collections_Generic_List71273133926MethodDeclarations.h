﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_FindIndex__Int32__PredicateT1_T>c__AnonStorey86
struct U3CListA1_FindIndex__Int32__PredicateT1_TU3Ec__AnonStorey86_t1273133926;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_FindIndex__Int32__PredicateT1_T>c__AnonStorey86::.ctor()
extern "C"  void U3CListA1_FindIndex__Int32__PredicateT1_TU3Ec__AnonStorey86__ctor_m651233269 (U3CListA1_FindIndex__Int32__PredicateT1_TU3Ec__AnonStorey86_t1273133926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Collections_Generic_List77Generated/<ListA1_FindIndex__Int32__PredicateT1_T>c__AnonStorey86::<>m__AB()
extern "C"  Il2CppObject * U3CListA1_FindIndex__Int32__PredicateT1_TU3Ec__AnonStorey86_U3CU3Em__AB_m1141979850 (U3CListA1_FindIndex__Int32__PredicateT1_TU3Ec__AnonStorey86_t1273133926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
