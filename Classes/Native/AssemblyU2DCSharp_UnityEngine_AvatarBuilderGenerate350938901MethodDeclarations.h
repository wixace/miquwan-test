﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AvatarBuilderGenerated
struct UnityEngine_AvatarBuilderGenerated_t350938901;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_AvatarBuilderGenerated::.ctor()
extern "C"  void UnityEngine_AvatarBuilderGenerated__ctor_m1402081574 (UnityEngine_AvatarBuilderGenerated_t350938901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AvatarBuilderGenerated::AvatarBuilder_AvatarBuilder1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AvatarBuilderGenerated_AvatarBuilder_AvatarBuilder1_m3540601102 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AvatarBuilderGenerated::AvatarBuilder_BuildGenericAvatar__GameObject__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AvatarBuilderGenerated_AvatarBuilder_BuildGenericAvatar__GameObject__String_m2610800387 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AvatarBuilderGenerated::AvatarBuilder_BuildHumanAvatar__GameObject__HumanDescription(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AvatarBuilderGenerated_AvatarBuilder_BuildHumanAvatar__GameObject__HumanDescription_m2791365527 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AvatarBuilderGenerated::__Register()
extern "C"  void UnityEngine_AvatarBuilderGenerated___Register_m3230282593 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_AvatarBuilderGenerated::ilo_getObject1(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_AvatarBuilderGenerated_ilo_getObject1_m556323529 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
