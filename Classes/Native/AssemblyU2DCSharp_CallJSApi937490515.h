﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CSRepresentedObject
struct CSRepresentedObject_t3994124630;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CallJSApi
struct  CallJSApi_t937490515  : public Il2CppObject
{
public:

public:
};

struct CallJSApi_t937490515_StaticFields
{
public:
	// CSRepresentedObject CallJSApi::mhJSApi
	CSRepresentedObject_t3994124630 * ___mhJSApi_0;

public:
	inline static int32_t get_offset_of_mhJSApi_0() { return static_cast<int32_t>(offsetof(CallJSApi_t937490515_StaticFields, ___mhJSApi_0)); }
	inline CSRepresentedObject_t3994124630 * get_mhJSApi_0() const { return ___mhJSApi_0; }
	inline CSRepresentedObject_t3994124630 ** get_address_of_mhJSApi_0() { return &___mhJSApi_0; }
	inline void set_mhJSApi_0(CSRepresentedObject_t3994124630 * value)
	{
		___mhJSApi_0 = value;
		Il2CppCodeGenWriteBarrier(&___mhJSApi_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
