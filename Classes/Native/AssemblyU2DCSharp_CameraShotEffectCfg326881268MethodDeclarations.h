﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraShotEffectCfg
struct CameraShotEffectCfg_t326881268;
// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "codegen/il2cpp-codegen.h"

// System.Void CameraShotEffectCfg::.ctor()
extern "C"  void CameraShotEffectCfg__ctor_m2172319223 (CameraShotEffectCfg_t326881268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension CameraShotEffectCfg::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * CameraShotEffectCfg_ProtoBuf_IExtensible_GetExtensionObject_m756815995 (CameraShotEffectCfg_t326881268 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotEffectCfg::get_id()
extern "C"  int32_t CameraShotEffectCfg_get_id_m3540520867 (CameraShotEffectCfg_t326881268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotEffectCfg::set_id(System.Int32)
extern "C"  void CameraShotEffectCfg_set_id_m3959844662 (CameraShotEffectCfg_t326881268 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotEffectCfg::get_heroOrNpcId()
extern "C"  int32_t CameraShotEffectCfg_get_heroOrNpcId_m3287933753 (CameraShotEffectCfg_t326881268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotEffectCfg::set_heroOrNpcId(System.Int32)
extern "C"  void CameraShotEffectCfg_set_heroOrNpcId_m31719048 (CameraShotEffectCfg_t326881268 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotEffectCfg::get_isHero()
extern "C"  int32_t CameraShotEffectCfg_get_isHero_m1056771340 (CameraShotEffectCfg_t326881268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotEffectCfg::set_isHero(System.Int32)
extern "C"  void CameraShotEffectCfg_set_isHero_m425747999 (CameraShotEffectCfg_t326881268 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotEffectCfg::get_lifeTime()
extern "C"  float CameraShotEffectCfg_get_lifeTime_m2069965363 (CameraShotEffectCfg_t326881268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotEffectCfg::set_lifeTime(System.Single)
extern "C"  void CameraShotEffectCfg_set_lifeTime_m2086418072 (CameraShotEffectCfg_t326881268 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotEffectCfg::get_posX()
extern "C"  float CameraShotEffectCfg_get_posX_m2532220046 (CameraShotEffectCfg_t326881268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotEffectCfg::set_posX(System.Single)
extern "C"  void CameraShotEffectCfg_set_posX_m62050333 (CameraShotEffectCfg_t326881268 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotEffectCfg::get_posY()
extern "C"  float CameraShotEffectCfg_get_posY_m2532221007 (CameraShotEffectCfg_t326881268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotEffectCfg::set_posY(System.Single)
extern "C"  void CameraShotEffectCfg_set_posY_m3846483452 (CameraShotEffectCfg_t326881268 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotEffectCfg::get_posZ()
extern "C"  float CameraShotEffectCfg_get_posZ_m2532221968 (CameraShotEffectCfg_t326881268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotEffectCfg::set_posZ(System.Single)
extern "C"  void CameraShotEffectCfg_set_posZ_m3335949275 (CameraShotEffectCfg_t326881268 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotEffectCfg::get_hostId()
extern "C"  int32_t CameraShotEffectCfg_get_hostId_m1302587883 (CameraShotEffectCfg_t326881268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotEffectCfg::set_hostId(System.Int32)
extern "C"  void CameraShotEffectCfg_set_hostId_m3166694526 (CameraShotEffectCfg_t326881268 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotEffectCfg::get_isShowInBlack()
extern "C"  int32_t CameraShotEffectCfg_get_isShowInBlack_m1614521901 (CameraShotEffectCfg_t326881268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotEffectCfg::set_isShowInBlack(System.Int32)
extern "C"  void CameraShotEffectCfg_set_isShowInBlack_m3676703740 (CameraShotEffectCfg_t326881268 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotEffectCfg::get_playSpeed()
extern "C"  float CameraShotEffectCfg_get_playSpeed_m806381387 (CameraShotEffectCfg_t326881268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotEffectCfg::set_playSpeed(System.Single)
extern "C"  void CameraShotEffectCfg_set_playSpeed_m2547228672 (CameraShotEffectCfg_t326881268 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
