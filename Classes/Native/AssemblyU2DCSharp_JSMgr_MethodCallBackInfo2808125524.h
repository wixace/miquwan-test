﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// JSMgr/CSCallbackMethod
struct CSCallbackMethod_t4136956950;
// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSMgr/MethodCallBackInfo
struct  MethodCallBackInfo_t2808125524  : public Il2CppObject
{
public:
	// JSMgr/CSCallbackMethod JSMgr/MethodCallBackInfo::fun
	CSCallbackMethod_t4136956950 * ___fun_0;
	// System.String JSMgr/MethodCallBackInfo::methodName
	String_t* ___methodName_1;

public:
	inline static int32_t get_offset_of_fun_0() { return static_cast<int32_t>(offsetof(MethodCallBackInfo_t2808125524, ___fun_0)); }
	inline CSCallbackMethod_t4136956950 * get_fun_0() const { return ___fun_0; }
	inline CSCallbackMethod_t4136956950 ** get_address_of_fun_0() { return &___fun_0; }
	inline void set_fun_0(CSCallbackMethod_t4136956950 * value)
	{
		___fun_0 = value;
		Il2CppCodeGenWriteBarrier(&___fun_0, value);
	}

	inline static int32_t get_offset_of_methodName_1() { return static_cast<int32_t>(offsetof(MethodCallBackInfo_t2808125524, ___methodName_1)); }
	inline String_t* get_methodName_1() const { return ___methodName_1; }
	inline String_t** get_address_of_methodName_1() { return &___methodName_1; }
	inline void set_methodName_1(String_t* value)
	{
		___methodName_1 = value;
		Il2CppCodeGenWriteBarrier(&___methodName_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
