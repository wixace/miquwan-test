﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.UInt32>
struct List_1_t1392853533;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1412526303.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.UInt32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1478123679_gshared (Enumerator_t1412526303 * __this, List_1_t1392853533 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m1478123679(__this, ___l0, method) ((  void (*) (Enumerator_t1412526303 *, List_1_t1392853533 *, const MethodInfo*))Enumerator__ctor_m1478123679_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.UInt32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3291778131_gshared (Enumerator_t1412526303 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3291778131(__this, method) ((  void (*) (Enumerator_t1412526303 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3291778131_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.UInt32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m278528831_gshared (Enumerator_t1412526303 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m278528831(__this, method) ((  Il2CppObject * (*) (Enumerator_t1412526303 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m278528831_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.UInt32>::Dispose()
extern "C"  void Enumerator_Dispose_m1521862212_gshared (Enumerator_t1412526303 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1521862212(__this, method) ((  void (*) (Enumerator_t1412526303 *, const MethodInfo*))Enumerator_Dispose_m1521862212_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.UInt32>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1263846269_gshared (Enumerator_t1412526303 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1263846269(__this, method) ((  void (*) (Enumerator_t1412526303 *, const MethodInfo*))Enumerator_VerifyState_m1263846269_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.UInt32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m938886847_gshared (Enumerator_t1412526303 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m938886847(__this, method) ((  bool (*) (Enumerator_t1412526303 *, const MethodInfo*))Enumerator_MoveNext_m938886847_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.UInt32>::get_Current()
extern "C"  uint32_t Enumerator_get_Current_m3939982196_gshared (Enumerator_t1412526303 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3939982196(__this, method) ((  uint32_t (*) (Enumerator_t1412526303 *, const MethodInfo*))Enumerator_get_Current_m3939982196_gshared)(__this, method)
