﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Scripts.JSBindingClasses.JsRepresentClass
struct JsRepresentClass_t2913492551;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Scripts.JSBindingClasses.JsRepresentClass/<GeneratedJSFuntion2>c__AnonStoreyFE
struct  U3CGeneratedJSFuntion2U3Ec__AnonStoreyFE_t1428480283  : public Il2CppObject
{
public:
	// System.String Scripts.JSBindingClasses.JsRepresentClass/<GeneratedJSFuntion2>c__AnonStoreyFE::funcitonName
	String_t* ___funcitonName_0;
	// Scripts.JSBindingClasses.JsRepresentClass Scripts.JSBindingClasses.JsRepresentClass/<GeneratedJSFuntion2>c__AnonStoreyFE::<>f__this
	JsRepresentClass_t2913492551 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_funcitonName_0() { return static_cast<int32_t>(offsetof(U3CGeneratedJSFuntion2U3Ec__AnonStoreyFE_t1428480283, ___funcitonName_0)); }
	inline String_t* get_funcitonName_0() const { return ___funcitonName_0; }
	inline String_t** get_address_of_funcitonName_0() { return &___funcitonName_0; }
	inline void set_funcitonName_0(String_t* value)
	{
		___funcitonName_0 = value;
		Il2CppCodeGenWriteBarrier(&___funcitonName_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CGeneratedJSFuntion2U3Ec__AnonStoreyFE_t1428480283, ___U3CU3Ef__this_1)); }
	inline JsRepresentClass_t2913492551 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline JsRepresentClass_t2913492551 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(JsRepresentClass_t2913492551 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
