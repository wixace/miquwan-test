﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CombatEntity
struct CombatEntity_t684137495;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReplayBase
struct  ReplayBase_t779703160  : public Il2CppObject
{
public:
	// System.Int32 ReplayBase::replayType
	int32_t ___replayType_0;
	// System.Int32 ReplayBase::id
	int32_t ___id_1;
	// System.Int32 ReplayBase::frameNum
	int32_t ___frameNum_2;
	// System.Single ReplayBase::doTime
	float ___doTime_3;
	// System.Int32 ReplayBase::heroId
	int32_t ___heroId_4;
	// System.Int32 ReplayBase::IsEnemy
	int32_t ___IsEnemy_5;
	// UnityEngine.Vector3 ReplayBase::currentPoint
	Vector3_t4282066566  ___currentPoint_6;
	// CombatEntity ReplayBase::_entity
	CombatEntity_t684137495 * ____entity_7;

public:
	inline static int32_t get_offset_of_replayType_0() { return static_cast<int32_t>(offsetof(ReplayBase_t779703160, ___replayType_0)); }
	inline int32_t get_replayType_0() const { return ___replayType_0; }
	inline int32_t* get_address_of_replayType_0() { return &___replayType_0; }
	inline void set_replayType_0(int32_t value)
	{
		___replayType_0 = value;
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(ReplayBase_t779703160, ___id_1)); }
	inline int32_t get_id_1() const { return ___id_1; }
	inline int32_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int32_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_frameNum_2() { return static_cast<int32_t>(offsetof(ReplayBase_t779703160, ___frameNum_2)); }
	inline int32_t get_frameNum_2() const { return ___frameNum_2; }
	inline int32_t* get_address_of_frameNum_2() { return &___frameNum_2; }
	inline void set_frameNum_2(int32_t value)
	{
		___frameNum_2 = value;
	}

	inline static int32_t get_offset_of_doTime_3() { return static_cast<int32_t>(offsetof(ReplayBase_t779703160, ___doTime_3)); }
	inline float get_doTime_3() const { return ___doTime_3; }
	inline float* get_address_of_doTime_3() { return &___doTime_3; }
	inline void set_doTime_3(float value)
	{
		___doTime_3 = value;
	}

	inline static int32_t get_offset_of_heroId_4() { return static_cast<int32_t>(offsetof(ReplayBase_t779703160, ___heroId_4)); }
	inline int32_t get_heroId_4() const { return ___heroId_4; }
	inline int32_t* get_address_of_heroId_4() { return &___heroId_4; }
	inline void set_heroId_4(int32_t value)
	{
		___heroId_4 = value;
	}

	inline static int32_t get_offset_of_IsEnemy_5() { return static_cast<int32_t>(offsetof(ReplayBase_t779703160, ___IsEnemy_5)); }
	inline int32_t get_IsEnemy_5() const { return ___IsEnemy_5; }
	inline int32_t* get_address_of_IsEnemy_5() { return &___IsEnemy_5; }
	inline void set_IsEnemy_5(int32_t value)
	{
		___IsEnemy_5 = value;
	}

	inline static int32_t get_offset_of_currentPoint_6() { return static_cast<int32_t>(offsetof(ReplayBase_t779703160, ___currentPoint_6)); }
	inline Vector3_t4282066566  get_currentPoint_6() const { return ___currentPoint_6; }
	inline Vector3_t4282066566 * get_address_of_currentPoint_6() { return &___currentPoint_6; }
	inline void set_currentPoint_6(Vector3_t4282066566  value)
	{
		___currentPoint_6 = value;
	}

	inline static int32_t get_offset_of__entity_7() { return static_cast<int32_t>(offsetof(ReplayBase_t779703160, ____entity_7)); }
	inline CombatEntity_t684137495 * get__entity_7() const { return ____entity_7; }
	inline CombatEntity_t684137495 ** get_address_of__entity_7() { return &____entity_7; }
	inline void set__entity_7(CombatEntity_t684137495 * value)
	{
		____entity_7 = value;
		Il2CppCodeGenWriteBarrier(&____entity_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
