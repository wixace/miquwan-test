﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraShotsCfg
struct CameraShotsCfg_t1740446544;
// ProtoBuf.IExtension
struct IExtension_t1606339106;
// System.Collections.Generic.List`1<CurCameraShotCfg>
struct List_1_t1593047221;

#include "codegen/il2cpp-codegen.h"

// System.Void CameraShotsCfg::.ctor()
extern "C"  void CameraShotsCfg__ctor_m2027580235 (CameraShotsCfg_t1740446544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension CameraShotsCfg::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * CameraShotsCfg_ProtoBuf_IExtensible_GetExtensionObject_m1877790875 (CameraShotsCfg_t1740446544 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotsCfg::get_id()
extern "C"  int32_t CameraShotsCfg_get_id_m408886315 (CameraShotsCfg_t1740446544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotsCfg::set_id(System.Int32)
extern "C"  void CameraShotsCfg_set_id_m2700296802 (CameraShotsCfg_t1740446544 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotsCfg::get_waitTime()
extern "C"  float CameraShotsCfg_get_waitTime_m1582666288 (CameraShotsCfg_t1740446544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotsCfg::set_waitTime(System.Single)
extern "C"  void CameraShotsCfg_set_waitTime_m3436833491 (CameraShotsCfg_t1740446544 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CurCameraShotCfg> CameraShotsCfg::get_curCameraShot()
extern "C"  List_1_t1593047221 * CameraShotsCfg_get_curCameraShot_m880898439 (CameraShotsCfg_t1740446544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
