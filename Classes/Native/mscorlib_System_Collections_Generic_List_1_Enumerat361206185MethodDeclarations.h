﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<CSPlusAtt>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3781456830(__this, ___l0, method) ((  void (*) (Enumerator_t361206185 *, List_1_t341533415 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<CSPlusAtt>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m367313364(__this, method) ((  void (*) (Enumerator_t361206185 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<CSPlusAtt>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m973578624(__this, method) ((  Il2CppObject * (*) (Enumerator_t361206185 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<CSPlusAtt>::Dispose()
#define Enumerator_Dispose_m1293111587(__this, method) ((  void (*) (Enumerator_t361206185 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<CSPlusAtt>::VerifyState()
#define Enumerator_VerifyState_m1814283996(__this, method) ((  void (*) (Enumerator_t361206185 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<CSPlusAtt>::MoveNext()
#define Enumerator_MoveNext_m3944294080(__this, method) ((  bool (*) (Enumerator_t361206185 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<CSPlusAtt>::get_Current()
#define Enumerator_get_Current_m3783800403(__this, method) ((  CSPlusAtt_t3268315159 * (*) (Enumerator_t361206185 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
