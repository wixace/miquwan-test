﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIPnl
struct UIPnl_t80754426;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// UIPanel
struct UIPanel_t295209936;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIPanel295209936.h"

// System.Void UIPnl::.ctor()
extern "C"  void UIPnl__ctor_m2595933105 (UIPnl_t80754426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPnl::Awake()
extern "C"  void UIPnl_Awake_m2833538324 (UIPnl_t80754426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPnl::SetData(System.Object[])
extern "C"  void UIPnl_SetData_m1228502231 (UIPnl_t80754426 * __this, ObjectU5BU5D_t1108656482* ___datas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPnl::ilo_set_depth1(UIPanel,System.Int32)
extern "C"  void UIPnl_ilo_set_depth1_m3285270278 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
