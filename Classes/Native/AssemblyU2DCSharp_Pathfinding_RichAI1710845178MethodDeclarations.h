﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RichAI
struct RichAI_t1710845178;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// Pathfinding.Path
struct Path_t1974241691;
// Pathfinding.RichFunnel
struct RichFunnel_t2453525928;
// Pathfinding.RichSpecial
struct RichSpecial_t2303562271;
// Seeker
struct Seeker_t2472610117;
// Pathfinding.RichPath
struct RichPath_t1926198167;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_Pathfinding_RichFunnel2453525928.h"
#include "AssemblyU2DCSharp_Pathfinding_RichSpecial2303562271.h"
#include "AssemblyU2DCSharp_Pathfinding_RichAI1710845178.h"
#include "AssemblyU2DCSharp_Seeker2472610117.h"
#include "AssemblyU2DCSharp_Pathfinding_RichPath1926198167.h"

// System.Void Pathfinding.RichAI::.ctor()
extern "C"  void RichAI__ctor_m1797194925 (RichAI_t1710845178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichAI::.cctor()
extern "C"  void RichAI__cctor_m3691338912 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RichAI::get_Velocity()
extern "C"  Vector3_t4282066566  RichAI_get_Velocity_m1585913367 (RichAI_t1710845178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichAI::Awake()
extern "C"  void RichAI_Awake_m2034800144 (RichAI_t1710845178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichAI::Start()
extern "C"  void RichAI_Start_m744332717 (RichAI_t1710845178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichAI::OnEnable()
extern "C"  void RichAI_OnEnable_m1243277241 (RichAI_t1710845178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichAI::OnDisable()
extern "C"  void RichAI_OnDisable_m327826068 (RichAI_t1710845178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichAI::UpdatePath()
extern "C"  void RichAI_UpdatePath_m3102386053 (RichAI_t1710845178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Pathfinding.RichAI::SearchPaths()
extern "C"  Il2CppObject * RichAI_SearchPaths_m2197305257 (RichAI_t1710845178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichAI::OnPathComplete(Pathfinding.Path)
extern "C"  void RichAI_OnPathComplete_m2471501765 (RichAI_t1710845178 * __this, Path_t1974241691 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RichAI::get_TraversingSpecial()
extern "C"  bool RichAI_get_TraversingSpecial_m3642769848 (RichAI_t1710845178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RichAI::get_TargetPoint()
extern "C"  Vector3_t4282066566  RichAI_get_TargetPoint_m1317317031 (RichAI_t1710845178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RichAI::get_ApproachingPartEndpoint()
extern "C"  bool RichAI_get_ApproachingPartEndpoint_m2830009584 (RichAI_t1710845178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RichAI::get_ApproachingPathEndpoint()
extern "C"  bool RichAI_get_ApproachingPathEndpoint_m3176754850 (RichAI_t1710845178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RichAI::get_DistanceToNextWaypoint()
extern "C"  float RichAI_get_DistanceToNextWaypoint_m1289970832 (RichAI_t1710845178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichAI::NextPart()
extern "C"  void RichAI_NextPart_m4053621757 (RichAI_t1710845178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichAI::OnTargetReached()
extern "C"  void RichAI_OnTargetReached_m2999119789 (RichAI_t1710845178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RichAI::UpdateTarget(Pathfinding.RichFunnel)
extern "C"  Vector3_t4282066566  RichAI_UpdateTarget_m2388789967 (RichAI_t1710845178 * __this, RichFunnel_t2453525928 * ___fn0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichAI::Update()
extern "C"  void RichAI_Update_m1605329920 (RichAI_t1710845178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RichAI::RaycastPosition(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t4282066566  RichAI_RaycastPosition_m3956550939 (RichAI_t1710845178 * __this, Vector3_t4282066566  ___position0, float ___lasty1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RichAI::RotateTowards(UnityEngine.Vector3)
extern "C"  bool RichAI_RotateTowards_m1165923969 (RichAI_t1710845178 * __this, Vector3_t4282066566  ___trotdir0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichAI::OnDrawGizmos()
extern "C"  void RichAI_OnDrawGizmos_m1139400659 (RichAI_t1710845178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Pathfinding.RichAI::TraverseSpecial(Pathfinding.RichSpecial)
extern "C"  Il2CppObject * RichAI_TraverseSpecial_m2084310933 (RichAI_t1710845178 * __this, RichSpecial_t2303562271 * ___rs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichAI::ilo_OnEnable1(Pathfinding.RichAI)
extern "C"  void RichAI_ilo_OnEnable1_m3151917753 (Il2CppObject * __this /* static, unused */, RichAI_t1710845178 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RichAI::ilo_IsDone2(Seeker)
extern "C"  bool RichAI_ilo_IsDone2_m4070576197 (Il2CppObject * __this /* static, unused */, Seeker_t2472610117 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichAI::ilo_Error3(Pathfinding.Path)
extern "C"  void RichAI_ilo_Error3_m3759161254 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Path Pathfinding.RichAI::ilo_StartPath4(Seeker,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Path_t1974241691 * RichAI_ilo_StartPath4_m3949304742 (Il2CppObject * __this /* static, unused */, Seeker_t2472610117 * ____this0, Vector3_t4282066566  ___start1, Vector3_t4282066566  ___end2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RichAI::ilo_get_error5(Pathfinding.Path)
extern "C"  bool RichAI_ilo_get_error5_m3479830533 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RichAI::ilo_get_ApproachingPartEndpoint6(Pathfinding.RichAI)
extern "C"  bool RichAI_ilo_get_ApproachingPartEndpoint6_m3659479341 (Il2CppObject * __this /* static, unused */, RichAI_t1710845178 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RichAI::ilo_PartsLeft7(Pathfinding.RichPath)
extern "C"  bool RichAI_ilo_PartsLeft7_m999820931 (Il2CppObject * __this /* static, unused */, RichPath_t1926198167 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichAI::ilo_UpdatePath8(Pathfinding.RichAI)
extern "C"  void RichAI_ilo_UpdatePath8_m3595118260 (Il2CppObject * __this /* static, unused */, RichAI_t1710845178 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RichAI::ilo_RotateTowards9(Pathfinding.RichAI,UnityEngine.Vector3)
extern "C"  bool RichAI_ilo_RotateTowards9_m334340635 (Il2CppObject * __this /* static, unused */, RichAI_t1710845178 * ____this0, Vector3_t4282066566  ___trotdir1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RichAI::ilo_RaycastPosition10(Pathfinding.RichAI,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t4282066566  RichAI_ilo_RaycastPosition10_m1337964185 (Il2CppObject * __this /* static, unused */, RichAI_t1710845178 * ____this0, Vector3_t4282066566  ___position1, float ___lasty2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
