﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_kone77
struct  M_kone77_t548720011  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_kone77::_bistrem
	float ____bistrem_0;
	// System.Boolean GarbageiOS.M_kone77::_norsairNouqirlay
	bool ____norsairNouqirlay_1;
	// System.UInt32 GarbageiOS.M_kone77::_gallhimo
	uint32_t ____gallhimo_2;
	// System.Single GarbageiOS.M_kone77::_reewaifee
	float ____reewaifee_3;
	// System.Boolean GarbageiOS.M_kone77::_rarneturJerexarda
	bool ____rarneturJerexarda_4;
	// System.Boolean GarbageiOS.M_kone77::_haidallteXaysidair
	bool ____haidallteXaysidair_5;
	// System.Single GarbageiOS.M_kone77::_fasemker
	float ____fasemker_6;
	// System.Single GarbageiOS.M_kone77::_searmonooTairwho
	float ____searmonooTairwho_7;
	// System.UInt32 GarbageiOS.M_kone77::_jurrarKuleca
	uint32_t ____jurrarKuleca_8;
	// System.Single GarbageiOS.M_kone77::_touveardree
	float ____touveardree_9;
	// System.Single GarbageiOS.M_kone77::_mouqasirRasval
	float ____mouqasirRasval_10;
	// System.Single GarbageiOS.M_kone77::_doojuMearxerrair
	float ____doojuMearxerrair_11;
	// System.Single GarbageiOS.M_kone77::_zewizem
	float ____zewizem_12;
	// System.String GarbageiOS.M_kone77::_rurnairje
	String_t* ____rurnairje_13;
	// System.Int32 GarbageiOS.M_kone77::_woomer
	int32_t ____woomer_14;
	// System.String GarbageiOS.M_kone77::_halneenearTrarremi
	String_t* ____halneenearTrarremi_15;
	// System.Boolean GarbageiOS.M_kone77::_balmaiRoupukou
	bool ____balmaiRoupukou_16;
	// System.UInt32 GarbageiOS.M_kone77::_kaiche
	uint32_t ____kaiche_17;
	// System.Int32 GarbageiOS.M_kone77::_sedruCamirjere
	int32_t ____sedruCamirjere_18;
	// System.Int32 GarbageiOS.M_kone77::_duloosee
	int32_t ____duloosee_19;
	// System.UInt32 GarbageiOS.M_kone77::_vemcairDiyicee
	uint32_t ____vemcairDiyicee_20;
	// System.Single GarbageiOS.M_kone77::_sallda
	float ____sallda_21;
	// System.Boolean GarbageiOS.M_kone77::_heltousow
	bool ____heltousow_22;
	// System.String GarbageiOS.M_kone77::_mikeabe
	String_t* ____mikeabe_23;
	// System.UInt32 GarbageiOS.M_kone77::_nemwo
	uint32_t ____nemwo_24;
	// System.Single GarbageiOS.M_kone77::_harcooJeredrair
	float ____harcooJeredrair_25;
	// System.Single GarbageiOS.M_kone77::_lairgemkall
	float ____lairgemkall_26;
	// System.Int32 GarbageiOS.M_kone77::_hirjooje
	int32_t ____hirjooje_27;
	// System.Boolean GarbageiOS.M_kone77::_trirnooru
	bool ____trirnooru_28;

public:
	inline static int32_t get_offset_of__bistrem_0() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____bistrem_0)); }
	inline float get__bistrem_0() const { return ____bistrem_0; }
	inline float* get_address_of__bistrem_0() { return &____bistrem_0; }
	inline void set__bistrem_0(float value)
	{
		____bistrem_0 = value;
	}

	inline static int32_t get_offset_of__norsairNouqirlay_1() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____norsairNouqirlay_1)); }
	inline bool get__norsairNouqirlay_1() const { return ____norsairNouqirlay_1; }
	inline bool* get_address_of__norsairNouqirlay_1() { return &____norsairNouqirlay_1; }
	inline void set__norsairNouqirlay_1(bool value)
	{
		____norsairNouqirlay_1 = value;
	}

	inline static int32_t get_offset_of__gallhimo_2() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____gallhimo_2)); }
	inline uint32_t get__gallhimo_2() const { return ____gallhimo_2; }
	inline uint32_t* get_address_of__gallhimo_2() { return &____gallhimo_2; }
	inline void set__gallhimo_2(uint32_t value)
	{
		____gallhimo_2 = value;
	}

	inline static int32_t get_offset_of__reewaifee_3() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____reewaifee_3)); }
	inline float get__reewaifee_3() const { return ____reewaifee_3; }
	inline float* get_address_of__reewaifee_3() { return &____reewaifee_3; }
	inline void set__reewaifee_3(float value)
	{
		____reewaifee_3 = value;
	}

	inline static int32_t get_offset_of__rarneturJerexarda_4() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____rarneturJerexarda_4)); }
	inline bool get__rarneturJerexarda_4() const { return ____rarneturJerexarda_4; }
	inline bool* get_address_of__rarneturJerexarda_4() { return &____rarneturJerexarda_4; }
	inline void set__rarneturJerexarda_4(bool value)
	{
		____rarneturJerexarda_4 = value;
	}

	inline static int32_t get_offset_of__haidallteXaysidair_5() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____haidallteXaysidair_5)); }
	inline bool get__haidallteXaysidair_5() const { return ____haidallteXaysidair_5; }
	inline bool* get_address_of__haidallteXaysidair_5() { return &____haidallteXaysidair_5; }
	inline void set__haidallteXaysidair_5(bool value)
	{
		____haidallteXaysidair_5 = value;
	}

	inline static int32_t get_offset_of__fasemker_6() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____fasemker_6)); }
	inline float get__fasemker_6() const { return ____fasemker_6; }
	inline float* get_address_of__fasemker_6() { return &____fasemker_6; }
	inline void set__fasemker_6(float value)
	{
		____fasemker_6 = value;
	}

	inline static int32_t get_offset_of__searmonooTairwho_7() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____searmonooTairwho_7)); }
	inline float get__searmonooTairwho_7() const { return ____searmonooTairwho_7; }
	inline float* get_address_of__searmonooTairwho_7() { return &____searmonooTairwho_7; }
	inline void set__searmonooTairwho_7(float value)
	{
		____searmonooTairwho_7 = value;
	}

	inline static int32_t get_offset_of__jurrarKuleca_8() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____jurrarKuleca_8)); }
	inline uint32_t get__jurrarKuleca_8() const { return ____jurrarKuleca_8; }
	inline uint32_t* get_address_of__jurrarKuleca_8() { return &____jurrarKuleca_8; }
	inline void set__jurrarKuleca_8(uint32_t value)
	{
		____jurrarKuleca_8 = value;
	}

	inline static int32_t get_offset_of__touveardree_9() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____touveardree_9)); }
	inline float get__touveardree_9() const { return ____touveardree_9; }
	inline float* get_address_of__touveardree_9() { return &____touveardree_9; }
	inline void set__touveardree_9(float value)
	{
		____touveardree_9 = value;
	}

	inline static int32_t get_offset_of__mouqasirRasval_10() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____mouqasirRasval_10)); }
	inline float get__mouqasirRasval_10() const { return ____mouqasirRasval_10; }
	inline float* get_address_of__mouqasirRasval_10() { return &____mouqasirRasval_10; }
	inline void set__mouqasirRasval_10(float value)
	{
		____mouqasirRasval_10 = value;
	}

	inline static int32_t get_offset_of__doojuMearxerrair_11() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____doojuMearxerrair_11)); }
	inline float get__doojuMearxerrair_11() const { return ____doojuMearxerrair_11; }
	inline float* get_address_of__doojuMearxerrair_11() { return &____doojuMearxerrair_11; }
	inline void set__doojuMearxerrair_11(float value)
	{
		____doojuMearxerrair_11 = value;
	}

	inline static int32_t get_offset_of__zewizem_12() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____zewizem_12)); }
	inline float get__zewizem_12() const { return ____zewizem_12; }
	inline float* get_address_of__zewizem_12() { return &____zewizem_12; }
	inline void set__zewizem_12(float value)
	{
		____zewizem_12 = value;
	}

	inline static int32_t get_offset_of__rurnairje_13() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____rurnairje_13)); }
	inline String_t* get__rurnairje_13() const { return ____rurnairje_13; }
	inline String_t** get_address_of__rurnairje_13() { return &____rurnairje_13; }
	inline void set__rurnairje_13(String_t* value)
	{
		____rurnairje_13 = value;
		Il2CppCodeGenWriteBarrier(&____rurnairje_13, value);
	}

	inline static int32_t get_offset_of__woomer_14() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____woomer_14)); }
	inline int32_t get__woomer_14() const { return ____woomer_14; }
	inline int32_t* get_address_of__woomer_14() { return &____woomer_14; }
	inline void set__woomer_14(int32_t value)
	{
		____woomer_14 = value;
	}

	inline static int32_t get_offset_of__halneenearTrarremi_15() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____halneenearTrarremi_15)); }
	inline String_t* get__halneenearTrarremi_15() const { return ____halneenearTrarremi_15; }
	inline String_t** get_address_of__halneenearTrarremi_15() { return &____halneenearTrarremi_15; }
	inline void set__halneenearTrarremi_15(String_t* value)
	{
		____halneenearTrarremi_15 = value;
		Il2CppCodeGenWriteBarrier(&____halneenearTrarremi_15, value);
	}

	inline static int32_t get_offset_of__balmaiRoupukou_16() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____balmaiRoupukou_16)); }
	inline bool get__balmaiRoupukou_16() const { return ____balmaiRoupukou_16; }
	inline bool* get_address_of__balmaiRoupukou_16() { return &____balmaiRoupukou_16; }
	inline void set__balmaiRoupukou_16(bool value)
	{
		____balmaiRoupukou_16 = value;
	}

	inline static int32_t get_offset_of__kaiche_17() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____kaiche_17)); }
	inline uint32_t get__kaiche_17() const { return ____kaiche_17; }
	inline uint32_t* get_address_of__kaiche_17() { return &____kaiche_17; }
	inline void set__kaiche_17(uint32_t value)
	{
		____kaiche_17 = value;
	}

	inline static int32_t get_offset_of__sedruCamirjere_18() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____sedruCamirjere_18)); }
	inline int32_t get__sedruCamirjere_18() const { return ____sedruCamirjere_18; }
	inline int32_t* get_address_of__sedruCamirjere_18() { return &____sedruCamirjere_18; }
	inline void set__sedruCamirjere_18(int32_t value)
	{
		____sedruCamirjere_18 = value;
	}

	inline static int32_t get_offset_of__duloosee_19() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____duloosee_19)); }
	inline int32_t get__duloosee_19() const { return ____duloosee_19; }
	inline int32_t* get_address_of__duloosee_19() { return &____duloosee_19; }
	inline void set__duloosee_19(int32_t value)
	{
		____duloosee_19 = value;
	}

	inline static int32_t get_offset_of__vemcairDiyicee_20() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____vemcairDiyicee_20)); }
	inline uint32_t get__vemcairDiyicee_20() const { return ____vemcairDiyicee_20; }
	inline uint32_t* get_address_of__vemcairDiyicee_20() { return &____vemcairDiyicee_20; }
	inline void set__vemcairDiyicee_20(uint32_t value)
	{
		____vemcairDiyicee_20 = value;
	}

	inline static int32_t get_offset_of__sallda_21() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____sallda_21)); }
	inline float get__sallda_21() const { return ____sallda_21; }
	inline float* get_address_of__sallda_21() { return &____sallda_21; }
	inline void set__sallda_21(float value)
	{
		____sallda_21 = value;
	}

	inline static int32_t get_offset_of__heltousow_22() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____heltousow_22)); }
	inline bool get__heltousow_22() const { return ____heltousow_22; }
	inline bool* get_address_of__heltousow_22() { return &____heltousow_22; }
	inline void set__heltousow_22(bool value)
	{
		____heltousow_22 = value;
	}

	inline static int32_t get_offset_of__mikeabe_23() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____mikeabe_23)); }
	inline String_t* get__mikeabe_23() const { return ____mikeabe_23; }
	inline String_t** get_address_of__mikeabe_23() { return &____mikeabe_23; }
	inline void set__mikeabe_23(String_t* value)
	{
		____mikeabe_23 = value;
		Il2CppCodeGenWriteBarrier(&____mikeabe_23, value);
	}

	inline static int32_t get_offset_of__nemwo_24() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____nemwo_24)); }
	inline uint32_t get__nemwo_24() const { return ____nemwo_24; }
	inline uint32_t* get_address_of__nemwo_24() { return &____nemwo_24; }
	inline void set__nemwo_24(uint32_t value)
	{
		____nemwo_24 = value;
	}

	inline static int32_t get_offset_of__harcooJeredrair_25() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____harcooJeredrair_25)); }
	inline float get__harcooJeredrair_25() const { return ____harcooJeredrair_25; }
	inline float* get_address_of__harcooJeredrair_25() { return &____harcooJeredrair_25; }
	inline void set__harcooJeredrair_25(float value)
	{
		____harcooJeredrair_25 = value;
	}

	inline static int32_t get_offset_of__lairgemkall_26() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____lairgemkall_26)); }
	inline float get__lairgemkall_26() const { return ____lairgemkall_26; }
	inline float* get_address_of__lairgemkall_26() { return &____lairgemkall_26; }
	inline void set__lairgemkall_26(float value)
	{
		____lairgemkall_26 = value;
	}

	inline static int32_t get_offset_of__hirjooje_27() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____hirjooje_27)); }
	inline int32_t get__hirjooje_27() const { return ____hirjooje_27; }
	inline int32_t* get_address_of__hirjooje_27() { return &____hirjooje_27; }
	inline void set__hirjooje_27(int32_t value)
	{
		____hirjooje_27 = value;
	}

	inline static int32_t get_offset_of__trirnooru_28() { return static_cast<int32_t>(offsetof(M_kone77_t548720011, ____trirnooru_28)); }
	inline bool get__trirnooru_28() const { return ____trirnooru_28; }
	inline bool* get_address_of__trirnooru_28() { return &____trirnooru_28; }
	inline void set__trirnooru_28(bool value)
	{
		____trirnooru_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
