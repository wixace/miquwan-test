﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Artngame.SKYMASTER.PlanarReflectionSM
struct PlanarReflectionSM_t1051979681;
// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_LayerMask3236759763.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Artngame_SKYMASTER_PlanarReflect1051979681.h"

// System.Void Artngame.SKYMASTER.PlanarReflectionSM::.ctor()
extern "C"  void PlanarReflectionSM__ctor_m2670549296 (PlanarReflectionSM_t1051979681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Artngame.SKYMASTER.PlanarReflectionSM::Start()
extern "C"  void PlanarReflectionSM_Start_m1617687088 (PlanarReflectionSM_t1051979681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera Artngame.SKYMASTER.PlanarReflectionSM::CreateReflectionCameraFor(UnityEngine.Camera)
extern "C"  Camera_t2727095145 * PlanarReflectionSM_CreateReflectionCameraFor_m2169449392 (PlanarReflectionSM_t1051979681 * __this, Camera_t2727095145 * ___cam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Artngame.SKYMASTER.PlanarReflectionSM::SetStandardCameraParameter(UnityEngine.Camera,UnityEngine.LayerMask)
extern "C"  void PlanarReflectionSM_SetStandardCameraParameter_m4090330147 (PlanarReflectionSM_t1051979681 * __this, Camera_t2727095145 * ___cam0, LayerMask_t3236759763  ___mask1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTexture Artngame.SKYMASTER.PlanarReflectionSM::CreateTextureFor(UnityEngine.Camera)
extern "C"  RenderTexture_t1963041563 * PlanarReflectionSM_CreateTextureFor_m1844948893 (PlanarReflectionSM_t1051979681 * __this, Camera_t2727095145 * ___cam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Artngame.SKYMASTER.PlanarReflectionSM::RenderHelpCameras(UnityEngine.Camera)
extern "C"  void PlanarReflectionSM_RenderHelpCameras_m2489240169 (PlanarReflectionSM_t1051979681 * __this, Camera_t2727095145 * ___currentCam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Artngame.SKYMASTER.PlanarReflectionSM::LateUpdate()
extern "C"  void PlanarReflectionSM_LateUpdate_m2130718499 (PlanarReflectionSM_t1051979681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Artngame.SKYMASTER.PlanarReflectionSM::WaterTileBeingRendered(UnityEngine.Transform,UnityEngine.Camera)
extern "C"  void PlanarReflectionSM_WaterTileBeingRendered_m2662723600 (PlanarReflectionSM_t1051979681 * __this, Transform_t1659122786 * ___tr0, Camera_t2727095145 * ___currentCam1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Artngame.SKYMASTER.PlanarReflectionSM::OnEnable()
extern "C"  void PlanarReflectionSM_OnEnable_m431464534 (PlanarReflectionSM_t1051979681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Artngame.SKYMASTER.PlanarReflectionSM::OnDisable()
extern "C"  void PlanarReflectionSM_OnDisable_m931435927 (PlanarReflectionSM_t1051979681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Artngame.SKYMASTER.PlanarReflectionSM::RenderReflectionFor(UnityEngine.Camera,UnityEngine.Camera)
extern "C"  void PlanarReflectionSM_RenderReflectionFor_m1226783248 (PlanarReflectionSM_t1051979681 * __this, Camera_t2727095145 * ___cam0, Camera_t2727095145 * ___reflectCamera1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Artngame.SKYMASTER.PlanarReflectionSM::SaneCameraSettings(UnityEngine.Camera)
extern "C"  void PlanarReflectionSM_SaneCameraSettings_m3105390149 (PlanarReflectionSM_t1051979681 * __this, Camera_t2727095145 * ___helperCam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 Artngame.SKYMASTER.PlanarReflectionSM::CalculateObliqueMatrix(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern "C"  Matrix4x4_t1651859333  PlanarReflectionSM_CalculateObliqueMatrix_m2471393255 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1651859333  ___projection0, Vector4_t4282066567  ___clipPlane1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 Artngame.SKYMASTER.PlanarReflectionSM::CalculateReflectionMatrix(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern "C"  Matrix4x4_t1651859333  PlanarReflectionSM_CalculateReflectionMatrix_m2637859857 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1651859333  ___reflectionMat0, Vector4_t4282066567  ___plane1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Artngame.SKYMASTER.PlanarReflectionSM::Sgn(System.Single)
extern "C"  float PlanarReflectionSM_Sgn_m3769522103 (Il2CppObject * __this /* static, unused */, float ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 Artngame.SKYMASTER.PlanarReflectionSM::CameraSpacePlane(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector4_t4282066567  PlanarReflectionSM_CameraSpacePlane_m212522245 (PlanarReflectionSM_t1051979681 * __this, Camera_t2727095145 * ___cam0, Vector3_t4282066566  ___pos1, Vector3_t4282066566  ___normal2, float ___sideSign3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTexture Artngame.SKYMASTER.PlanarReflectionSM::ilo_CreateTextureFor1(Artngame.SKYMASTER.PlanarReflectionSM,UnityEngine.Camera)
extern "C"  RenderTexture_t1963041563 * PlanarReflectionSM_ilo_CreateTextureFor1_m3147281786 (Il2CppObject * __this /* static, unused */, PlanarReflectionSM_t1051979681 * ____this0, Camera_t2727095145 * ___cam1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera Artngame.SKYMASTER.PlanarReflectionSM::ilo_CreateReflectionCameraFor2(Artngame.SKYMASTER.PlanarReflectionSM,UnityEngine.Camera)
extern "C"  Camera_t2727095145 * PlanarReflectionSM_ilo_CreateReflectionCameraFor2_m3379366286 (Il2CppObject * __this /* static, unused */, PlanarReflectionSM_t1051979681 * ____this0, Camera_t2727095145 * ___cam1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Artngame.SKYMASTER.PlanarReflectionSM::ilo_Sgn3(System.Single)
extern "C"  float PlanarReflectionSM_ilo_Sgn3_m2281039959 (Il2CppObject * __this /* static, unused */, float ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
