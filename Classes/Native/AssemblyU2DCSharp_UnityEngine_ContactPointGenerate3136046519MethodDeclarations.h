﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ContactPointGenerated
struct UnityEngine_ContactPointGenerated_t3136046519;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_ContactPointGenerated::.ctor()
extern "C"  void UnityEngine_ContactPointGenerated__ctor_m3717076372 (UnityEngine_ContactPointGenerated_t3136046519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ContactPointGenerated::.cctor()
extern "C"  void UnityEngine_ContactPointGenerated__cctor_m3078121625 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ContactPointGenerated::ContactPoint_ContactPoint1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ContactPointGenerated_ContactPoint_ContactPoint1_m3776844254 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ContactPointGenerated::ContactPoint_point(JSVCall)
extern "C"  void UnityEngine_ContactPointGenerated_ContactPoint_point_m625679350 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ContactPointGenerated::ContactPoint_normal(JSVCall)
extern "C"  void UnityEngine_ContactPointGenerated_ContactPoint_normal_m702562943 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ContactPointGenerated::ContactPoint_thisCollider(JSVCall)
extern "C"  void UnityEngine_ContactPointGenerated_ContactPoint_thisCollider_m1091061524 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ContactPointGenerated::ContactPoint_otherCollider(JSVCall)
extern "C"  void UnityEngine_ContactPointGenerated_ContactPoint_otherCollider_m168365282 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ContactPointGenerated::__Register()
extern "C"  void UnityEngine_ContactPointGenerated___Register_m2737161139 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ContactPointGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_ContactPointGenerated_ilo_getObject1_m3945605282 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ContactPointGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_ContactPointGenerated_ilo_attachFinalizerObject2_m1879597604 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ContactPointGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void UnityEngine_ContactPointGenerated_ilo_addJSCSRel3_m1880963922 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ContactPointGenerated::ilo_setVector3S4(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_ContactPointGenerated_ilo_setVector3S4_m1771170678 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
