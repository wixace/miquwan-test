﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22065771578.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2908028374_gshared (KeyValuePair_2_t2065771578 * __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m2908028374(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2065771578 *, Il2CppObject *, float, const MethodInfo*))KeyValuePair_2__ctor_m2908028374_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m975404242_gshared (KeyValuePair_2_t2065771578 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m975404242(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t2065771578 *, const MethodInfo*))KeyValuePair_2_get_Key_m975404242_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m662474387_gshared (KeyValuePair_2_t2065771578 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m662474387(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2065771578 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m662474387_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::get_Value()
extern "C"  float KeyValuePair_2_get_Value_m2222463222_gshared (KeyValuePair_2_t2065771578 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m2222463222(__this, method) ((  float (*) (KeyValuePair_2_t2065771578 *, const MethodInfo*))KeyValuePair_2_get_Value_m2222463222_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3606602003_gshared (KeyValuePair_2_t2065771578 * __this, float ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m3606602003(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2065771578 *, float, const MethodInfo*))KeyValuePair_2_set_Value_m3606602003_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3615079765_gshared (KeyValuePair_2_t2065771578 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m3615079765(__this, method) ((  String_t* (*) (KeyValuePair_2_t2065771578 *, const MethodInfo*))KeyValuePair_2_ToString_m3615079765_gshared)(__this, method)
