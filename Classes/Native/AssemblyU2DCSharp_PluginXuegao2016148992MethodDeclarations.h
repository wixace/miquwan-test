﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginXuegao
struct PluginXuegao_t2016148992;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionInfo
struct VersionInfo_t2356638086;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_PluginXuegao2016148992.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void PluginXuegao::.ctor()
extern "C"  void PluginXuegao__ctor_m3996759195 (PluginXuegao_t2016148992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXuegao::Init()
extern "C"  void PluginXuegao_Init_m219011673 (PluginXuegao_t2016148992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginXuegao::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginXuegao_ReqSDKHttpLogin_m3271665456 (PluginXuegao_t2016148992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXuegao::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginXuegao_IsLoginSuccess_m1096010840 (PluginXuegao_t2016148992 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXuegao::OpenUserLogin()
extern "C"  void PluginXuegao_OpenUserLogin_m3261991917 (PluginXuegao_t2016148992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXuegao::UserPay(CEvent.ZEvent)
extern "C"  void PluginXuegao_UserPay_m4086936293 (PluginXuegao_t2016148992 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXuegao::OnInitSuccess(System.String)
extern "C"  void PluginXuegao_OnInitSuccess_m3454292469 (PluginXuegao_t2016148992 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXuegao::OnInitFail(System.String)
extern "C"  void PluginXuegao_OnInitFail_m3438439276 (PluginXuegao_t2016148992 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXuegao::OnLoginSuccess(System.String)
extern "C"  void PluginXuegao_OnLoginSuccess_m98596192 (PluginXuegao_t2016148992 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXuegao::OnLoginFail(System.String)
extern "C"  void PluginXuegao_OnLoginFail_m2142238689 (PluginXuegao_t2016148992 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXuegao::OnUserSwitchSuccess(System.String)
extern "C"  void PluginXuegao_OnUserSwitchSuccess_m3564265796 (PluginXuegao_t2016148992 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXuegao::OnUserSwitchFail(System.String)
extern "C"  void PluginXuegao_OnUserSwitchFail_m1662269053 (PluginXuegao_t2016148992 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXuegao::OnPaySuccess(System.String)
extern "C"  void PluginXuegao_OnPaySuccess_m2200219551 (PluginXuegao_t2016148992 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXuegao::OnPayFail(System.String)
extern "C"  void PluginXuegao_OnPayFail_m2400517634 (PluginXuegao_t2016148992 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXuegao::OnExitGameSuccess(System.String)
extern "C"  void PluginXuegao_OnExitGameSuccess_m491451285 (PluginXuegao_t2016148992 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXuegao::OnExitGameFail(System.String)
extern "C"  void PluginXuegao_OnExitGameFail_m700984780 (PluginXuegao_t2016148992 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXuegao::OnLogoutSuccess(System.String)
extern "C"  void PluginXuegao_OnLogoutSuccess_m3060024623 (PluginXuegao_t2016148992 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXuegao::OnLogoutFail(System.String)
extern "C"  void PluginXuegao_OnLogoutFail_m1797771890 (PluginXuegao_t2016148992 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXuegao::initSdk(System.String,System.String,System.String,System.String)
extern "C"  void PluginXuegao_initSdk_m453140595 (PluginXuegao_t2016148992 * __this, String_t* ___channel0, String_t* ___appId1, String_t* ___appKey2, String_t* ___vsnum3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXuegao::login()
extern "C"  void PluginXuegao_login_m3518774018 (PluginXuegao_t2016148992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXuegao::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginXuegao_pay_m3673002505 (PluginXuegao_t2016148992 * __this, String_t* ___mappingId0, String_t* ___subject1, String_t* ___sName2, String_t* ___rName3, String_t* ___orderId4, String_t* ___cbUrl5, String_t* ___mpext6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXuegao::<OnLogoutSuccess>m__46E()
extern "C"  void PluginXuegao_U3COnLogoutSuccessU3Em__46E_m1157525647 (PluginXuegao_t2016148992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXuegao::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginXuegao_ilo_AddEventListener1_m2775651577 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginXuegao::ilo_get_currentVS2(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginXuegao_ilo_get_currentVS2_m3367274158 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginXuegao::ilo_get_Instance3()
extern "C"  VersionMgr_t1322950208 * PluginXuegao_ilo_get_Instance3_m3143143646 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginXuegao::ilo_get_PluginsSdkMgr4()
extern "C"  PluginsSdkMgr_t3884624670 * PluginXuegao_ilo_get_PluginsSdkMgr4_m2773379248 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginXuegao::ilo_get_DeviceID5(PluginsSdkMgr)
extern "C"  String_t* PluginXuegao_ilo_get_DeviceID5_m545824182 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXuegao::ilo_get_isSdkLogin6(VersionMgr)
extern "C"  bool PluginXuegao_ilo_get_isSdkLogin6_m2275788966 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXuegao::ilo_OpenUserLogin7(PluginXuegao)
extern "C"  void PluginXuegao_ilo_OpenUserLogin7_m361381695 (Il2CppObject * __this /* static, unused */, PluginXuegao_t2016148992 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXuegao::ilo_login8(PluginXuegao)
extern "C"  void PluginXuegao_ilo_login8_m3259352971 (Il2CppObject * __this /* static, unused */, PluginXuegao_t2016148992 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProductsCfgMgr PluginXuegao::ilo_get_ProductsCfgMgr9()
extern "C"  ProductsCfgMgr_t2493714872 * PluginXuegao_ilo_get_ProductsCfgMgr9_m2842375737 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXuegao::ilo_Log10(System.Object,System.Boolean)
extern "C"  void PluginXuegao_ilo_Log10_m3170155442 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXuegao::ilo_DispatchEvent11(CEvent.ZEvent)
extern "C"  void PluginXuegao_ilo_DispatchEvent11_m2898053109 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
