﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_CharacterControllerGenerated
struct UnityEngine_CharacterControllerGenerated_t241730770;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_CharacterControllerGenerated::.ctor()
extern "C"  void UnityEngine_CharacterControllerGenerated__ctor_m1737303561 (UnityEngine_CharacterControllerGenerated_t241730770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CharacterControllerGenerated::CharacterController_CharacterController1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CharacterControllerGenerated_CharacterController_CharacterController1_m2326593541 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterControllerGenerated::CharacterController_isGrounded(JSVCall)
extern "C"  void UnityEngine_CharacterControllerGenerated_CharacterController_isGrounded_m219881058 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterControllerGenerated::CharacterController_velocity(JSVCall)
extern "C"  void UnityEngine_CharacterControllerGenerated_CharacterController_velocity_m3193036501 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterControllerGenerated::CharacterController_collisionFlags(JSVCall)
extern "C"  void UnityEngine_CharacterControllerGenerated_CharacterController_collisionFlags_m3346114365 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterControllerGenerated::CharacterController_radius(JSVCall)
extern "C"  void UnityEngine_CharacterControllerGenerated_CharacterController_radius_m3166141888 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterControllerGenerated::CharacterController_height(JSVCall)
extern "C"  void UnityEngine_CharacterControllerGenerated_CharacterController_height_m3212095691 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterControllerGenerated::CharacterController_center(JSVCall)
extern "C"  void UnityEngine_CharacterControllerGenerated_CharacterController_center_m1669278045 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterControllerGenerated::CharacterController_slopeLimit(JSVCall)
extern "C"  void UnityEngine_CharacterControllerGenerated_CharacterController_slopeLimit_m317641026 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterControllerGenerated::CharacterController_stepOffset(JSVCall)
extern "C"  void UnityEngine_CharacterControllerGenerated_CharacterController_stepOffset_m1469354099 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterControllerGenerated::CharacterController_skinWidth(JSVCall)
extern "C"  void UnityEngine_CharacterControllerGenerated_CharacterController_skinWidth_m3826244945 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterControllerGenerated::CharacterController_detectCollisions(JSVCall)
extern "C"  void UnityEngine_CharacterControllerGenerated_CharacterController_detectCollisions_m3432794382 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CharacterControllerGenerated::CharacterController_Move__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CharacterControllerGenerated_CharacterController_Move__Vector3_m119199912 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CharacterControllerGenerated::CharacterController_SimpleMove__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CharacterControllerGenerated_CharacterController_SimpleMove__Vector3_m3556489110 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterControllerGenerated::__Register()
extern "C"  void UnityEngine_CharacterControllerGenerated___Register_m2511844446 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CharacterControllerGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_CharacterControllerGenerated_ilo_attachFinalizerObject1_m1017305846 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterControllerGenerated::ilo_setBooleanS2(System.Int32,System.Boolean)
extern "C"  void UnityEngine_CharacterControllerGenerated_ilo_setBooleanS2_m3699729753 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterControllerGenerated::ilo_setVector3S3(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_CharacterControllerGenerated_ilo_setVector3S3_m2885436448 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterControllerGenerated::ilo_setEnum4(System.Int32,System.Int32)
extern "C"  void UnityEngine_CharacterControllerGenerated_ilo_setEnum4_m48489927 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_CharacterControllerGenerated::ilo_getSingle5(System.Int32)
extern "C"  float UnityEngine_CharacterControllerGenerated_ilo_getSingle5_m2338650050 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CharacterControllerGenerated::ilo_setSingle6(System.Int32,System.Single)
extern "C"  void UnityEngine_CharacterControllerGenerated_ilo_setSingle6_m3441468496 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CharacterControllerGenerated::ilo_getBooleanS7(System.Int32)
extern "C"  bool UnityEngine_CharacterControllerGenerated_ilo_getBooleanS7_m1266186793 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
