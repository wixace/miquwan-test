﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen184564025MethodDeclarations.h"

// System.Void System.Func`2<System.Type,System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m1932179225(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t1863747261 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m2987145212_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Type,System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>>::Invoke(T)
#define Func_2_Invoke_m343030461(__this, ___arg10, method) ((  Il2CppObject* (*) (Func_2_t1863747261 *, Type_t *, const MethodInfo*))Func_2_Invoke_m1069083732_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Type,System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m3271839088(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t1863747261 *, Type_t *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4281703301_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Type,System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m1654985015(__this, ___result0, method) ((  Il2CppObject* (*) (Func_2_t1863747261 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m4118168638_gshared)(__this, ___result0, method)
