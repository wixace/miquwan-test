﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSProvingGroundData
struct CSProvingGroundData_t325892090;
// CSHeroUnit
struct CSHeroUnit_t3764358446;
// System.String
struct String_t;
// Newtonsoft.Json.Linq.JContainer
struct JContainer_t3364442311;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CSHeroUnit3764358446.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JContainer3364442311.h"

// System.Void CSProvingGroundData::.ctor()
extern "C"  void CSProvingGroundData__ctor_m3083318449 (CSProvingGroundData_t325892090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSHeroUnit CSProvingGroundData::GetHeroUnit(System.UInt32)
extern "C"  CSHeroUnit_t3764358446 * CSProvingGroundData_GetHeroUnit_m4075261320 (CSProvingGroundData_t325892090 * __this, uint32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSProvingGroundData::GetCSPVPRobotNPCLevelById(System.Int32)
extern "C"  int32_t CSProvingGroundData_GetCSPVPRobotNPCLevelById_m3006376937 (CSProvingGroundData_t325892090 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSProvingGroundData::LoadData(System.String)
extern "C"  void CSProvingGroundData_LoadData_m2469322943 (CSProvingGroundData_t325892090 * __this, String_t* ___jsonStr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSProvingGroundData::UnLoadData()
extern "C"  void CSProvingGroundData_UnLoadData_m2097060380 (CSProvingGroundData_t325892090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSProvingGroundData::ilo_get_id1(CSHeroUnit)
extern "C"  uint32_t CSProvingGroundData_ilo_get_id1_m2040104342 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSProvingGroundData::ilo_get_Count2(Newtonsoft.Json.Linq.JContainer)
extern "C"  int32_t CSProvingGroundData_ilo_get_Count2_m3955016638 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
