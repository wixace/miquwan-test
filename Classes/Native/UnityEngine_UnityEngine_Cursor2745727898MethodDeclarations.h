﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Cursor
struct Cursor_t2745727898;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_CursorMode3265706717.h"
#include "UnityEngine_UnityEngine_CursorLockMode1155278888.h"

// System.Void UnityEngine.Cursor::.ctor()
extern "C"  void Cursor__ctor_m2703550807 (Cursor_t2745727898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cursor::SetCursor(UnityEngine.Texture2D,UnityEngine.Vector2,UnityEngine.CursorMode)
extern "C"  void Cursor_SetCursor_m1014269299 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, Vector2_t4282066565  ___hotspot1, int32_t ___cursorMode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cursor::INTERNAL_CALL_SetCursor(UnityEngine.Texture2D,UnityEngine.Vector2&,UnityEngine.CursorMode)
extern "C"  void Cursor_INTERNAL_CALL_SetCursor_m1785721740 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, Vector2_t4282066565 * ___hotspot1, int32_t ___cursorMode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Cursor::get_visible()
extern "C"  bool Cursor_get_visible_m3880003512 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cursor::set_visible(System.Boolean)
extern "C"  void Cursor_set_visible_m4101409761 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CursorLockMode UnityEngine.Cursor::get_lockState()
extern "C"  int32_t Cursor_get_lockState_m2795946910 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cursor::set_lockState(UnityEngine.CursorLockMode)
extern "C"  void Cursor_set_lockState_m3065915939 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
