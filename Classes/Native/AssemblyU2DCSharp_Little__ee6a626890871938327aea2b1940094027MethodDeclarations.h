﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._ee6a626890871938327aea2b1ccc8ec5
struct _ee6a626890871938327aea2b1ccc8ec5_t1940094027;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._ee6a626890871938327aea2b1ccc8ec5::.ctor()
extern "C"  void _ee6a626890871938327aea2b1ccc8ec5__ctor_m3553578562 (_ee6a626890871938327aea2b1ccc8ec5_t1940094027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._ee6a626890871938327aea2b1ccc8ec5::_ee6a626890871938327aea2b1ccc8ec5m2(System.Int32)
extern "C"  int32_t _ee6a626890871938327aea2b1ccc8ec5__ee6a626890871938327aea2b1ccc8ec5m2_m2955335097 (_ee6a626890871938327aea2b1ccc8ec5_t1940094027 * __this, int32_t ____ee6a626890871938327aea2b1ccc8ec5a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._ee6a626890871938327aea2b1ccc8ec5::_ee6a626890871938327aea2b1ccc8ec5m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _ee6a626890871938327aea2b1ccc8ec5__ee6a626890871938327aea2b1ccc8ec5m_m315405597 (_ee6a626890871938327aea2b1ccc8ec5_t1940094027 * __this, int32_t ____ee6a626890871938327aea2b1ccc8ec5a0, int32_t ____ee6a626890871938327aea2b1ccc8ec5171, int32_t ____ee6a626890871938327aea2b1ccc8ec5c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
