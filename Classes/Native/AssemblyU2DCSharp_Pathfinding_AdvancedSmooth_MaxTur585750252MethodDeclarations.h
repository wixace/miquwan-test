﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.AdvancedSmooth/MaxTurn
struct MaxTurn_t585750252;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>
struct List_1_t1833380930;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_AdvancedSmooth_Turn465195378.h"

// System.Void Pathfinding.AdvancedSmooth/MaxTurn::.ctor()
extern "C"  void MaxTurn__ctor_m3718120239 (MaxTurn_t585750252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AdvancedSmooth/MaxTurn::OnTangentUpdate()
extern "C"  void MaxTurn_OnTangentUpdate_m4283753820 (MaxTurn_t585750252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AdvancedSmooth/MaxTurn::Prepare(System.Int32,UnityEngine.Vector3[])
extern "C"  void MaxTurn_Prepare_m1210387990 (MaxTurn_t585750252 * __this, int32_t ___i0, Vector3U5BU5D_t215400611* ___vectorPath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AdvancedSmooth/MaxTurn::TangentToTangent(System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>)
extern "C"  void MaxTurn_TangentToTangent_m4093437232 (MaxTurn_t585750252 * __this, List_1_t1833380930 * ___turnList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AdvancedSmooth/MaxTurn::PointToTangent(System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>)
extern "C"  void MaxTurn_PointToTangent_m2106341573 (MaxTurn_t585750252 * __this, List_1_t1833380930 * ___turnList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AdvancedSmooth/MaxTurn::TangentToPoint(System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>)
extern "C"  void MaxTurn_TangentToPoint_m3145199611 (MaxTurn_t585750252 * __this, List_1_t1833380930 * ___turnList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AdvancedSmooth/MaxTurn::GetPath(Pathfinding.AdvancedSmooth/Turn,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  void MaxTurn_GetPath_m2074419115 (MaxTurn_t585750252 * __this, Turn_t465195378  ___turn0, List_1_t1355284822 * ___output1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
