﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_RaycastResultGenerated
struct UnityEngine_EventSystems_RaycastResultGenerated_t63843766;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_EventSystems_RaycastResultGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_RaycastResultGenerated__ctor_m3795650165 (UnityEngine_EventSystems_RaycastResultGenerated_t63843766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_RaycastResultGenerated::.cctor()
extern "C"  void UnityEngine_EventSystems_RaycastResultGenerated__cctor_m1218941912 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_RaycastResultGenerated::RaycastResult_RaycastResult1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_RaycastResultGenerated_RaycastResult_RaycastResult1_m572742575 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_RaycastResultGenerated::RaycastResult_module(JSVCall)
extern "C"  void UnityEngine_EventSystems_RaycastResultGenerated_RaycastResult_module_m90416979 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_RaycastResultGenerated::RaycastResult_distance(JSVCall)
extern "C"  void UnityEngine_EventSystems_RaycastResultGenerated_RaycastResult_distance_m185475498 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_RaycastResultGenerated::RaycastResult_index(JSVCall)
extern "C"  void UnityEngine_EventSystems_RaycastResultGenerated_RaycastResult_index_m2891181851 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_RaycastResultGenerated::RaycastResult_depth(JSVCall)
extern "C"  void UnityEngine_EventSystems_RaycastResultGenerated_RaycastResult_depth_m2978409546 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_RaycastResultGenerated::RaycastResult_sortingLayer(JSVCall)
extern "C"  void UnityEngine_EventSystems_RaycastResultGenerated_RaycastResult_sortingLayer_m1470490098 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_RaycastResultGenerated::RaycastResult_sortingOrder(JSVCall)
extern "C"  void UnityEngine_EventSystems_RaycastResultGenerated_RaycastResult_sortingOrder_m845160597 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_RaycastResultGenerated::RaycastResult_worldPosition(JSVCall)
extern "C"  void UnityEngine_EventSystems_RaycastResultGenerated_RaycastResult_worldPosition_m1991136306 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_RaycastResultGenerated::RaycastResult_worldNormal(JSVCall)
extern "C"  void UnityEngine_EventSystems_RaycastResultGenerated_RaycastResult_worldNormal_m3424808820 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_RaycastResultGenerated::RaycastResult_screenPosition(JSVCall)
extern "C"  void UnityEngine_EventSystems_RaycastResultGenerated_RaycastResult_screenPosition_m2266338602 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_RaycastResultGenerated::RaycastResult_gameObject(JSVCall)
extern "C"  void UnityEngine_EventSystems_RaycastResultGenerated_RaycastResult_gameObject_m516238606 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_RaycastResultGenerated::RaycastResult_isValid(JSVCall)
extern "C"  void UnityEngine_EventSystems_RaycastResultGenerated_RaycastResult_isValid_m1219465531 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_RaycastResultGenerated::RaycastResult_Clear(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_RaycastResultGenerated_RaycastResult_Clear_m2383527531 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_RaycastResultGenerated::RaycastResult_ToString(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_RaycastResultGenerated_RaycastResult_ToString_m3994070160 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_RaycastResultGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_RaycastResultGenerated___Register_m1715418994 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_EventSystems_RaycastResultGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_EventSystems_RaycastResultGenerated_ilo_getObject1_m2182414049 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_RaycastResultGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_EventSystems_RaycastResultGenerated_ilo_attachFinalizerObject2_m1480162403 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_RaycastResultGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void UnityEngine_EventSystems_RaycastResultGenerated_ilo_addJSCSRel3_m3532984243 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_RaycastResultGenerated::ilo_changeJSObj4(System.Int32,System.Object)
extern "C"  void UnityEngine_EventSystems_RaycastResultGenerated_ilo_changeJSObj4_m642458007 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_RaycastResultGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void UnityEngine_EventSystems_RaycastResultGenerated_ilo_setSingle5_m3468732387 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_RaycastResultGenerated::ilo_setInt326(System.Int32,System.Int32)
extern "C"  void UnityEngine_EventSystems_RaycastResultGenerated_ilo_setInt326_m3074757852 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_EventSystems_RaycastResultGenerated::ilo_getInt327(System.Int32)
extern "C"  int32_t UnityEngine_EventSystems_RaycastResultGenerated_ilo_getInt327_m234204190 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_RaycastResultGenerated::ilo_setVector3S8(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_EventSystems_RaycastResultGenerated_ilo_setVector3S8_m337155833 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_EventSystems_RaycastResultGenerated::ilo_getVector3S9(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_EventSystems_RaycastResultGenerated_ilo_getVector3S9_m1275911289 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
