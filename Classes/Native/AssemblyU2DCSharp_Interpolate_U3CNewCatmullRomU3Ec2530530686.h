﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IList
struct IList_t1751339649;
// Interpolate/ToVector3`1<UnityEngine.Vector3>
struct ToVector3_1_t684749283;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Interpolate/<NewCatmullRom>c__Iterator25`1<UnityEngine.Vector3>
struct  U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686  : public Il2CppObject
{
public:
	// System.Collections.IList Interpolate/<NewCatmullRom>c__Iterator25`1::nodes
	Il2CppObject * ___nodes_0;
	// Interpolate/ToVector3`1<T> Interpolate/<NewCatmullRom>c__Iterator25`1::toVector3
	ToVector3_1_t684749283 * ___toVector3_1;
	// System.Int32 Interpolate/<NewCatmullRom>c__Iterator25`1::<last>__0
	int32_t ___U3ClastU3E__0_2;
	// System.Int32 Interpolate/<NewCatmullRom>c__Iterator25`1::<current>__1
	int32_t ___U3CcurrentU3E__1_3;
	// System.Boolean Interpolate/<NewCatmullRom>c__Iterator25`1::loop
	bool ___loop_4;
	// System.Int32 Interpolate/<NewCatmullRom>c__Iterator25`1::<previous>__2
	int32_t ___U3CpreviousU3E__2_5;
	// System.Int32 Interpolate/<NewCatmullRom>c__Iterator25`1::<start>__3
	int32_t ___U3CstartU3E__3_6;
	// System.Int32 Interpolate/<NewCatmullRom>c__Iterator25`1::<end>__4
	int32_t ___U3CendU3E__4_7;
	// System.Int32 Interpolate/<NewCatmullRom>c__Iterator25`1::<next>__5
	int32_t ___U3CnextU3E__5_8;
	// System.Int32 Interpolate/<NewCatmullRom>c__Iterator25`1::slices
	int32_t ___slices_9;
	// System.Int32 Interpolate/<NewCatmullRom>c__Iterator25`1::<stepCount>__6
	int32_t ___U3CstepCountU3E__6_10;
	// System.Int32 Interpolate/<NewCatmullRom>c__Iterator25`1::<step>__7
	int32_t ___U3CstepU3E__7_11;
	// System.Int32 Interpolate/<NewCatmullRom>c__Iterator25`1::$PC
	int32_t ___U24PC_12;
	// UnityEngine.Vector3 Interpolate/<NewCatmullRom>c__Iterator25`1::$current
	Vector3_t4282066566  ___U24current_13;
	// System.Collections.IList Interpolate/<NewCatmullRom>c__Iterator25`1::<$>nodes
	Il2CppObject * ___U3CU24U3Enodes_14;
	// Interpolate/ToVector3`1<T> Interpolate/<NewCatmullRom>c__Iterator25`1::<$>toVector3
	ToVector3_1_t684749283 * ___U3CU24U3EtoVector3_15;
	// System.Boolean Interpolate/<NewCatmullRom>c__Iterator25`1::<$>loop
	bool ___U3CU24U3Eloop_16;
	// System.Int32 Interpolate/<NewCatmullRom>c__Iterator25`1::<$>slices
	int32_t ___U3CU24U3Eslices_17;

public:
	inline static int32_t get_offset_of_nodes_0() { return static_cast<int32_t>(offsetof(U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686, ___nodes_0)); }
	inline Il2CppObject * get_nodes_0() const { return ___nodes_0; }
	inline Il2CppObject ** get_address_of_nodes_0() { return &___nodes_0; }
	inline void set_nodes_0(Il2CppObject * value)
	{
		___nodes_0 = value;
		Il2CppCodeGenWriteBarrier(&___nodes_0, value);
	}

	inline static int32_t get_offset_of_toVector3_1() { return static_cast<int32_t>(offsetof(U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686, ___toVector3_1)); }
	inline ToVector3_1_t684749283 * get_toVector3_1() const { return ___toVector3_1; }
	inline ToVector3_1_t684749283 ** get_address_of_toVector3_1() { return &___toVector3_1; }
	inline void set_toVector3_1(ToVector3_1_t684749283 * value)
	{
		___toVector3_1 = value;
		Il2CppCodeGenWriteBarrier(&___toVector3_1, value);
	}

	inline static int32_t get_offset_of_U3ClastU3E__0_2() { return static_cast<int32_t>(offsetof(U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686, ___U3ClastU3E__0_2)); }
	inline int32_t get_U3ClastU3E__0_2() const { return ___U3ClastU3E__0_2; }
	inline int32_t* get_address_of_U3ClastU3E__0_2() { return &___U3ClastU3E__0_2; }
	inline void set_U3ClastU3E__0_2(int32_t value)
	{
		___U3ClastU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentU3E__1_3() { return static_cast<int32_t>(offsetof(U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686, ___U3CcurrentU3E__1_3)); }
	inline int32_t get_U3CcurrentU3E__1_3() const { return ___U3CcurrentU3E__1_3; }
	inline int32_t* get_address_of_U3CcurrentU3E__1_3() { return &___U3CcurrentU3E__1_3; }
	inline void set_U3CcurrentU3E__1_3(int32_t value)
	{
		___U3CcurrentU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_loop_4() { return static_cast<int32_t>(offsetof(U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686, ___loop_4)); }
	inline bool get_loop_4() const { return ___loop_4; }
	inline bool* get_address_of_loop_4() { return &___loop_4; }
	inline void set_loop_4(bool value)
	{
		___loop_4 = value;
	}

	inline static int32_t get_offset_of_U3CpreviousU3E__2_5() { return static_cast<int32_t>(offsetof(U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686, ___U3CpreviousU3E__2_5)); }
	inline int32_t get_U3CpreviousU3E__2_5() const { return ___U3CpreviousU3E__2_5; }
	inline int32_t* get_address_of_U3CpreviousU3E__2_5() { return &___U3CpreviousU3E__2_5; }
	inline void set_U3CpreviousU3E__2_5(int32_t value)
	{
		___U3CpreviousU3E__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CstartU3E__3_6() { return static_cast<int32_t>(offsetof(U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686, ___U3CstartU3E__3_6)); }
	inline int32_t get_U3CstartU3E__3_6() const { return ___U3CstartU3E__3_6; }
	inline int32_t* get_address_of_U3CstartU3E__3_6() { return &___U3CstartU3E__3_6; }
	inline void set_U3CstartU3E__3_6(int32_t value)
	{
		___U3CstartU3E__3_6 = value;
	}

	inline static int32_t get_offset_of_U3CendU3E__4_7() { return static_cast<int32_t>(offsetof(U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686, ___U3CendU3E__4_7)); }
	inline int32_t get_U3CendU3E__4_7() const { return ___U3CendU3E__4_7; }
	inline int32_t* get_address_of_U3CendU3E__4_7() { return &___U3CendU3E__4_7; }
	inline void set_U3CendU3E__4_7(int32_t value)
	{
		___U3CendU3E__4_7 = value;
	}

	inline static int32_t get_offset_of_U3CnextU3E__5_8() { return static_cast<int32_t>(offsetof(U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686, ___U3CnextU3E__5_8)); }
	inline int32_t get_U3CnextU3E__5_8() const { return ___U3CnextU3E__5_8; }
	inline int32_t* get_address_of_U3CnextU3E__5_8() { return &___U3CnextU3E__5_8; }
	inline void set_U3CnextU3E__5_8(int32_t value)
	{
		___U3CnextU3E__5_8 = value;
	}

	inline static int32_t get_offset_of_slices_9() { return static_cast<int32_t>(offsetof(U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686, ___slices_9)); }
	inline int32_t get_slices_9() const { return ___slices_9; }
	inline int32_t* get_address_of_slices_9() { return &___slices_9; }
	inline void set_slices_9(int32_t value)
	{
		___slices_9 = value;
	}

	inline static int32_t get_offset_of_U3CstepCountU3E__6_10() { return static_cast<int32_t>(offsetof(U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686, ___U3CstepCountU3E__6_10)); }
	inline int32_t get_U3CstepCountU3E__6_10() const { return ___U3CstepCountU3E__6_10; }
	inline int32_t* get_address_of_U3CstepCountU3E__6_10() { return &___U3CstepCountU3E__6_10; }
	inline void set_U3CstepCountU3E__6_10(int32_t value)
	{
		___U3CstepCountU3E__6_10 = value;
	}

	inline static int32_t get_offset_of_U3CstepU3E__7_11() { return static_cast<int32_t>(offsetof(U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686, ___U3CstepU3E__7_11)); }
	inline int32_t get_U3CstepU3E__7_11() const { return ___U3CstepU3E__7_11; }
	inline int32_t* get_address_of_U3CstepU3E__7_11() { return &___U3CstepU3E__7_11; }
	inline void set_U3CstepU3E__7_11(int32_t value)
	{
		___U3CstepU3E__7_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}

	inline static int32_t get_offset_of_U24current_13() { return static_cast<int32_t>(offsetof(U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686, ___U24current_13)); }
	inline Vector3_t4282066566  get_U24current_13() const { return ___U24current_13; }
	inline Vector3_t4282066566 * get_address_of_U24current_13() { return &___U24current_13; }
	inline void set_U24current_13(Vector3_t4282066566  value)
	{
		___U24current_13 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Enodes_14() { return static_cast<int32_t>(offsetof(U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686, ___U3CU24U3Enodes_14)); }
	inline Il2CppObject * get_U3CU24U3Enodes_14() const { return ___U3CU24U3Enodes_14; }
	inline Il2CppObject ** get_address_of_U3CU24U3Enodes_14() { return &___U3CU24U3Enodes_14; }
	inline void set_U3CU24U3Enodes_14(Il2CppObject * value)
	{
		___U3CU24U3Enodes_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Enodes_14, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EtoVector3_15() { return static_cast<int32_t>(offsetof(U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686, ___U3CU24U3EtoVector3_15)); }
	inline ToVector3_1_t684749283 * get_U3CU24U3EtoVector3_15() const { return ___U3CU24U3EtoVector3_15; }
	inline ToVector3_1_t684749283 ** get_address_of_U3CU24U3EtoVector3_15() { return &___U3CU24U3EtoVector3_15; }
	inline void set_U3CU24U3EtoVector3_15(ToVector3_1_t684749283 * value)
	{
		___U3CU24U3EtoVector3_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EtoVector3_15, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eloop_16() { return static_cast<int32_t>(offsetof(U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686, ___U3CU24U3Eloop_16)); }
	inline bool get_U3CU24U3Eloop_16() const { return ___U3CU24U3Eloop_16; }
	inline bool* get_address_of_U3CU24U3Eloop_16() { return &___U3CU24U3Eloop_16; }
	inline void set_U3CU24U3Eloop_16(bool value)
	{
		___U3CU24U3Eloop_16 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Eslices_17() { return static_cast<int32_t>(offsetof(U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686, ___U3CU24U3Eslices_17)); }
	inline int32_t get_U3CU24U3Eslices_17() const { return ___U3CU24U3Eslices_17; }
	inline int32_t* get_address_of_U3CU24U3Eslices_17() { return &___U3CU24U3Eslices_17; }
	inline void set_U3CU24U3Eslices_17(int32_t value)
	{
		___U3CU24U3Eslices_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
