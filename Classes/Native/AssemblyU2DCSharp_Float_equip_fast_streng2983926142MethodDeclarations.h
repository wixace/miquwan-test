﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_equip_fast_streng
struct Float_equip_fast_streng_t2983926142;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.String
struct String_t;
// FloatTextUnit
struct FloatTextUnit_t2362298029;
// EventDelegate/Callback
struct Callback_t1094463061;
// UILabel
struct UILabel_t291504320;
// UIWidget
struct UIWidget_t769069560;
// System.String[]
struct StringU5BU5D_t4054002952;
// UITweener
struct UITweener_t105489188;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "AssemblyU2DCSharp_UITweener_Method1078127180.h"
#include "AssemblyU2DCSharp_UILabel291504320.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_Float_equip_fast_streng2983926142.h"
#include "AssemblyU2DCSharp_UITweener105489188.h"

// System.Void Float_equip_fast_streng::.ctor()
extern "C"  void Float_equip_fast_streng__ctor_m1388647341 (Float_equip_fast_streng_t2983926142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_fast_streng::OnAwake(UnityEngine.GameObject)
extern "C"  void Float_equip_fast_streng_OnAwake_m338821225 (Float_equip_fast_streng_t2983926142 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_fast_streng::OnDestroy()
extern "C"  void Float_equip_fast_streng_OnDestroy_m224086566 (Float_equip_fast_streng_t2983926142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_equip_fast_streng::FloatID()
extern "C"  int32_t Float_equip_fast_streng_FloatID_m5573227 (Float_equip_fast_streng_t2983926142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_fast_streng::Init()
extern "C"  void Float_equip_fast_streng_Init_m2767278343 (Float_equip_fast_streng_t2983926142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_fast_streng::Death()
extern "C"  void Float_equip_fast_streng_Death_m3773709887 (Float_equip_fast_streng_t2983926142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_fast_streng::SetFile(System.Object[])
extern "C"  void Float_equip_fast_streng_SetFile_m1458945225 (Float_equip_fast_streng_t2983926142 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_fast_streng::SetMasterValue(System.String)
extern "C"  void Float_equip_fast_streng_SetMasterValue_m1001979358 (Float_equip_fast_streng_t2983926142 * __this, String_t* ___masterStr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_fast_streng::SetStrengValue(System.String)
extern "C"  void Float_equip_fast_streng_SetStrengValue_m4053672201 (Float_equip_fast_streng_t2983926142 * __this, String_t* ___attack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_fast_streng::FloatPos()
extern "C"  void Float_equip_fast_streng_FloatPos_m991492431 (Float_equip_fast_streng_t2983926142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_fast_streng::ilo_OnDestroy1(FloatTextUnit)
extern "C"  void Float_equip_fast_streng_ilo_OnDestroy1_m3806864773 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_fast_streng::ilo_MoveAlpha2(FloatTextUnit,System.Single,System.Single,System.Single,System.Single,EventDelegate/Callback,UnityEngine.GameObject,UITweener/Method)
extern "C"  void Float_equip_fast_streng_ilo_MoveAlpha2_m638301539 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, float ___from1, float ___to2, float ___duration3, float ___delay4, Callback_t1094463061 * ___call5, GameObject_t3674682005 * ___go6, int32_t ___method7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_fast_streng::ilo_set_text3(UILabel,System.String)
extern "C"  void Float_equip_fast_streng_ilo_set_text3_m2565456877 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_fast_streng::ilo_set_height4(UIWidget,System.Int32)
extern "C"  void Float_equip_fast_streng_ilo_set_height4_m3125258919 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_fast_streng::ilo_SetStrengValue5(Float_equip_fast_streng,System.String)
extern "C"  void Float_equip_fast_streng_ilo_SetStrengValue5_m120450629 (Il2CppObject * __this /* static, unused */, Float_equip_fast_streng_t2983926142 * ____this0, String_t* ___attack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Float_equip_fast_streng::ilo_SplitStr_commn6(System.String)
extern "C"  StringU5BU5D_t4054002952* Float_equip_fast_streng_ilo_SplitStr_commn6_m3572716045 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_fast_streng::ilo_PlayForward7(UITweener)
extern "C"  void Float_equip_fast_streng_ilo_PlayForward7_m3812140478 (Il2CppObject * __this /* static, unused */, UITweener_t105489188 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_fast_streng::ilo_ResetToBeginning8(UITweener)
extern "C"  void Float_equip_fast_streng_ilo_ResetToBeginning8_m330785521 (Il2CppObject * __this /* static, unused */, UITweener_t105489188 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
