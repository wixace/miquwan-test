﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AudioSourceGenerated
struct UnityEngine_AudioSourceGenerated_t3944856166;
// JSVCall
struct JSVCall_t3708497963;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_AudioSourceGenerated::.ctor()
extern "C"  void UnityEngine_AudioSourceGenerated__ctor_m4139290357 (UnityEngine_AudioSourceGenerated_t3944856166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioSourceGenerated::AudioSource_AudioSource1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioSourceGenerated_AudioSource_AudioSource1_m116016833 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_volume(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_volume_m3872170256 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_pitch(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_pitch_m2513360994 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_time(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_time_m4272666557 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_timeSamples(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_timeSamples_m4286055846 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_clip(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_clip_m1789020826 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_outputAudioMixerGroup(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_outputAudioMixerGroup_m1040728791 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_isPlaying(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_isPlaying_m4260021886 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_loop(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_loop_m1638835654 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_ignoreListenerVolume(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_ignoreListenerVolume_m1877230730 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_playOnAwake(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_playOnAwake_m1785194960 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_ignoreListenerPause(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_ignoreListenerPause_m316680114 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_velocityUpdateMode(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_velocityUpdateMode_m3581445185 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_panStereo(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_panStereo_m660241997 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_spatialBlend(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_spatialBlend_m2560298205 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_spatialize(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_spatialize_m2480780570 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_reverbZoneMix(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_reverbZoneMix_m3009443972 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_bypassEffects(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_bypassEffects_m1399423464 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_bypassListenerEffects(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_bypassListenerEffects_m980893372 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_bypassReverbZones(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_bypassReverbZones_m3431150261 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_dopplerLevel(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_dopplerLevel_m3091366644 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_spread(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_spread_m3893242583 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_priority(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_priority_m371228870 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_mute(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_mute_m3886755313 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_minDistance(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_minDistance_m2040974875 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_maxDistance(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_maxDistance_m3120993609 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::AudioSource_rolloffMode(JSVCall)
extern "C"  void UnityEngine_AudioSourceGenerated_AudioSource_rolloffMode_m1018483117 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioSourceGenerated::AudioSource_GetCustomCurve__AudioSourceCurveType(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioSourceGenerated_AudioSource_GetCustomCurve__AudioSourceCurveType_m342978817 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioSourceGenerated::AudioSource_GetOutputData__Single_Array__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioSourceGenerated_AudioSource_GetOutputData__Single_Array__Int32_m1962963180 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioSourceGenerated::AudioSource_GetSpatializerFloat__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioSourceGenerated_AudioSource_GetSpatializerFloat__Int32__Single_m2950621191 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioSourceGenerated::AudioSource_GetSpectrumData__Single_Array__Int32__FFTWindow(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioSourceGenerated_AudioSource_GetSpectrumData__Single_Array__Int32__FFTWindow_m4124354922 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioSourceGenerated::AudioSource_Pause(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioSourceGenerated_AudioSource_Pause_m811281815 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioSourceGenerated::AudioSource_Play__UInt64(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioSourceGenerated_AudioSource_Play__UInt64_m3574770093 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioSourceGenerated::AudioSource_Play(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioSourceGenerated_AudioSource_Play_m936867893 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioSourceGenerated::AudioSource_PlayDelayed__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioSourceGenerated_AudioSource_PlayDelayed__Single_m1594434935 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioSourceGenerated::AudioSource_PlayOneShot__AudioClip__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioSourceGenerated_AudioSource_PlayOneShot__AudioClip__Single_m4234970339 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioSourceGenerated::AudioSource_PlayOneShot__AudioClip(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioSourceGenerated_AudioSource_PlayOneShot__AudioClip_m1771471003 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioSourceGenerated::AudioSource_PlayScheduled__Double(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioSourceGenerated_AudioSource_PlayScheduled__Double_m163674955 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioSourceGenerated::AudioSource_SetCustomCurve__AudioSourceCurveType__AnimationCurve(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioSourceGenerated_AudioSource_SetCustomCurve__AudioSourceCurveType__AnimationCurve_m2144443392 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioSourceGenerated::AudioSource_SetScheduledEndTime__Double(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioSourceGenerated_AudioSource_SetScheduledEndTime__Double_m846221775 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioSourceGenerated::AudioSource_SetScheduledStartTime__Double(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioSourceGenerated_AudioSource_SetScheduledStartTime__Double_m3470829142 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioSourceGenerated::AudioSource_SetSpatializerFloat__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioSourceGenerated_AudioSource_SetSpatializerFloat__Int32__Single_m3194669691 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioSourceGenerated::AudioSource_Stop(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioSourceGenerated_AudioSource_Stop_m2336048771 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioSourceGenerated::AudioSource_UnPause(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioSourceGenerated_AudioSource_UnPause_m3963562814 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioSourceGenerated::AudioSource_PlayClipAtPoint__AudioClip__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioSourceGenerated_AudioSource_PlayClipAtPoint__AudioClip__Vector3__Single_m364015180 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioSourceGenerated::AudioSource_PlayClipAtPoint__AudioClip__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioSourceGenerated_AudioSource_PlayClipAtPoint__AudioClip__Vector3_m2521621252 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::__Register()
extern "C"  void UnityEngine_AudioSourceGenerated___Register_m3559194354 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] UnityEngine_AudioSourceGenerated::<AudioSource_GetOutputData__Single_Array__Int32>m__18E()
extern "C"  SingleU5BU5D_t2316563989* UnityEngine_AudioSourceGenerated_U3CAudioSource_GetOutputData__Single_Array__Int32U3Em__18E_m3783764627 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] UnityEngine_AudioSourceGenerated::<AudioSource_GetSpectrumData__Single_Array__Int32__FFTWindow>m__18F()
extern "C"  SingleU5BU5D_t2316563989* UnityEngine_AudioSourceGenerated_U3CAudioSource_GetSpectrumData__Single_Array__Int32__FFTWindowU3Em__18F_m4194114778 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AudioSourceGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_AudioSourceGenerated_ilo_getObject1_m350359357 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_AudioSourceGenerated_ilo_addJSCSRel2_m1485206450 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void UnityEngine_AudioSourceGenerated_ilo_setSingle3_m3008661089 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_AudioSourceGenerated::ilo_getSingle4(System.Int32)
extern "C"  float UnityEngine_AudioSourceGenerated_ilo_getSingle4_m868072469 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioSourceGenerated::ilo_getBooleanS5(System.Int32)
extern "C"  bool UnityEngine_AudioSourceGenerated_ilo_getBooleanS5_m62973755 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::ilo_setBooleanS6(System.Int32,System.Boolean)
extern "C"  void UnityEngine_AudioSourceGenerated_ilo_setBooleanS6_m1736010689 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AudioSourceGenerated::ilo_getEnum7(System.Int32)
extern "C"  int32_t UnityEngine_AudioSourceGenerated_ilo_getEnum7_m768151809 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AudioSourceGenerated::ilo_getInt328(System.Int32)
extern "C"  int32_t UnityEngine_AudioSourceGenerated_ilo_getInt328_m20089475 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::ilo_setEnum9(System.Int32,System.Int32)
extern "C"  void UnityEngine_AudioSourceGenerated_ilo_setEnum9_m2170930062 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSourceGenerated::ilo_setArgIndex10(System.Int32)
extern "C"  void UnityEngine_AudioSourceGenerated_ilo_setArgIndex10_m2406467182 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 UnityEngine_AudioSourceGenerated::ilo_getUInt6411(System.Int32)
extern "C"  uint64_t UnityEngine_AudioSourceGenerated_ilo_getUInt6411_m2709381411 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double UnityEngine_AudioSourceGenerated::ilo_getDouble12(System.Int32)
extern "C"  double UnityEngine_AudioSourceGenerated_ilo_getDouble12_m3627043350 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_AudioSourceGenerated::ilo_getVector3S13(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_AudioSourceGenerated_ilo_getVector3S13_m2245719762 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AudioSourceGenerated::ilo_getElement14(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_AudioSourceGenerated_ilo_getElement14_m2830481805 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
