﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._237aee5a5ee389ca1d9e24ba27e115e7
struct _237aee5a5ee389ca1d9e24ba27e115e7_t559586452;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__237aee5a5ee389ca1d9e24ba2559586452.h"

// System.Void Little._237aee5a5ee389ca1d9e24ba27e115e7::.ctor()
extern "C"  void _237aee5a5ee389ca1d9e24ba27e115e7__ctor_m2064478233 (_237aee5a5ee389ca1d9e24ba27e115e7_t559586452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._237aee5a5ee389ca1d9e24ba27e115e7::_237aee5a5ee389ca1d9e24ba27e115e7m2(System.Int32)
extern "C"  int32_t _237aee5a5ee389ca1d9e24ba27e115e7__237aee5a5ee389ca1d9e24ba27e115e7m2_m4278659353 (_237aee5a5ee389ca1d9e24ba27e115e7_t559586452 * __this, int32_t ____237aee5a5ee389ca1d9e24ba27e115e7a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._237aee5a5ee389ca1d9e24ba27e115e7::_237aee5a5ee389ca1d9e24ba27e115e7m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _237aee5a5ee389ca1d9e24ba27e115e7__237aee5a5ee389ca1d9e24ba27e115e7m_m1734274493 (_237aee5a5ee389ca1d9e24ba27e115e7_t559586452 * __this, int32_t ____237aee5a5ee389ca1d9e24ba27e115e7a0, int32_t ____237aee5a5ee389ca1d9e24ba27e115e7341, int32_t ____237aee5a5ee389ca1d9e24ba27e115e7c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._237aee5a5ee389ca1d9e24ba27e115e7::ilo__237aee5a5ee389ca1d9e24ba27e115e7m21(Little._237aee5a5ee389ca1d9e24ba27e115e7,System.Int32)
extern "C"  int32_t _237aee5a5ee389ca1d9e24ba27e115e7_ilo__237aee5a5ee389ca1d9e24ba27e115e7m21_m3576027 (Il2CppObject * __this /* static, unused */, _237aee5a5ee389ca1d9e24ba27e115e7_t559586452 * ____this0, int32_t ____237aee5a5ee389ca1d9e24ba27e115e7a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
