﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.AstarData/<GetRaycastableGraphs>c__IteratorA
struct U3CGetRaycastableGraphsU3Ec__IteratorA_t3377333404;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.AstarData/<GetRaycastableGraphs>c__IteratorA::.ctor()
extern "C"  void U3CGetRaycastableGraphsU3Ec__IteratorA__ctor_m282761599 (U3CGetRaycastableGraphsU3Ec__IteratorA_t3377333404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Pathfinding.AstarData/<GetRaycastableGraphs>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetRaycastableGraphsU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m163553981 (U3CGetRaycastableGraphsU3Ec__IteratorA_t3377333404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Pathfinding.AstarData/<GetRaycastableGraphs>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetRaycastableGraphsU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m1988149329 (U3CGetRaycastableGraphsU3Ec__IteratorA_t3377333404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Pathfinding.AstarData/<GetRaycastableGraphs>c__IteratorA::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CGetRaycastableGraphsU3Ec__IteratorA_System_Collections_IEnumerable_GetEnumerator_m1479556754 (U3CGetRaycastableGraphsU3Ec__IteratorA_t3377333404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Object> Pathfinding.AstarData/<GetRaycastableGraphs>c__IteratorA::System.Collections.Generic.IEnumerable<object>.GetEnumerator()
extern "C"  Il2CppObject* U3CGetRaycastableGraphsU3Ec__IteratorA_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m484101852 (U3CGetRaycastableGraphsU3Ec__IteratorA_t3377333404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.AstarData/<GetRaycastableGraphs>c__IteratorA::MoveNext()
extern "C"  bool U3CGetRaycastableGraphsU3Ec__IteratorA_MoveNext_m529599485 (U3CGetRaycastableGraphsU3Ec__IteratorA_t3377333404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData/<GetRaycastableGraphs>c__IteratorA::Dispose()
extern "C"  void U3CGetRaycastableGraphsU3Ec__IteratorA_Dispose_m1049240060 (U3CGetRaycastableGraphsU3Ec__IteratorA_t3377333404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData/<GetRaycastableGraphs>c__IteratorA::Reset()
extern "C"  void U3CGetRaycastableGraphsU3Ec__IteratorA_Reset_m2224161836 (U3CGetRaycastableGraphsU3Ec__IteratorA_t3377333404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
