﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_bashisZujila9
struct M_bashisZujila9_t733218764;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_bashisZujila9733218764.h"

// System.Void GarbageiOS.M_bashisZujila9::.ctor()
extern "C"  void M_bashisZujila9__ctor_m3901444887 (M_bashisZujila9_t733218764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_bashisZujila9::M_serreNaylear0(System.String[],System.Int32)
extern "C"  void M_bashisZujila9_M_serreNaylear0_m2089632045 (M_bashisZujila9_t733218764 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_bashisZujila9::M_leatreBearhargow1(System.String[],System.Int32)
extern "C"  void M_bashisZujila9_M_leatreBearhargow1_m2851764834 (M_bashisZujila9_t733218764 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_bashisZujila9::M_sahairStepesel2(System.String[],System.Int32)
extern "C"  void M_bashisZujila9_M_sahairStepesel2_m53307 (M_bashisZujila9_t733218764 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_bashisZujila9::M_cuhairSoubaywur3(System.String[],System.Int32)
extern "C"  void M_bashisZujila9_M_cuhairSoubaywur3_m4116985418 (M_bashisZujila9_t733218764 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_bashisZujila9::M_whafoowe4(System.String[],System.Int32)
extern "C"  void M_bashisZujila9_M_whafoowe4_m939471210 (M_bashisZujila9_t733218764 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_bashisZujila9::ilo_M_serreNaylear01(GarbageiOS.M_bashisZujila9,System.String[],System.Int32)
extern "C"  void M_bashisZujila9_ilo_M_serreNaylear01_m432056177 (Il2CppObject * __this /* static, unused */, M_bashisZujila9_t733218764 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_bashisZujila9::ilo_M_leatreBearhargow12(GarbageiOS.M_bashisZujila9,System.String[],System.Int32)
extern "C"  void M_bashisZujila9_ilo_M_leatreBearhargow12_m2826659781 (Il2CppObject * __this /* static, unused */, M_bashisZujila9_t733218764 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
