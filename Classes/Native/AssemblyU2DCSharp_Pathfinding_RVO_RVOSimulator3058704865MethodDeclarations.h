﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RVO.RVOSimulator
struct RVOSimulator_t3058704865;
// Pathfinding.RVO.Simulator
struct Simulator_t2705969170;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_RVOSimulator3058704865.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_Simulator2705969170.h"

// System.Void Pathfinding.RVO.RVOSimulator::.ctor()
extern "C"  void RVOSimulator__ctor_m2412942357 (RVOSimulator_t3058704865 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RVO.Simulator Pathfinding.RVO.RVOSimulator::GetSimulator()
extern "C"  Simulator_t2705969170 * RVOSimulator_GetSimulator_m3949627417 (RVOSimulator_t3058704865 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOSimulator::Awake()
extern "C"  void RVOSimulator_Awake_m2650547576 (RVOSimulator_t3058704865 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOSimulator::Update()
extern "C"  void RVOSimulator_Update_m3513631128 (RVOSimulator_t3058704865 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOSimulator::OnDestroy()
extern "C"  void RVOSimulator_OnDestroy_m224548494 (RVOSimulator_t3058704865 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOSimulator::ilo_Awake1(Pathfinding.RVO.RVOSimulator)
extern "C"  void RVOSimulator_ilo_Awake1_m57282952 (Il2CppObject * __this /* static, unused */, RVOSimulator_t3058704865 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOSimulator::ilo_set_DesiredDeltaTime2(Pathfinding.RVO.Simulator,System.Single)
extern "C"  void RVOSimulator_ilo_set_DesiredDeltaTime2_m1478411286 (Il2CppObject * __this /* static, unused */, Simulator_t2705969170 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOSimulator::ilo_OnDestroy3(Pathfinding.RVO.Simulator)
extern "C"  void RVOSimulator_ilo_OnDestroy3_m1120289185 (Il2CppObject * __this /* static, unused */, Simulator_t2705969170 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
