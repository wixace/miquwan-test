﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_renercerMoorow232
struct  M_renercerMoorow232_t1060498076  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_renercerMoorow232::_yotruHelpem
	uint32_t ____yotruHelpem_0;
	// System.UInt32 GarbageiOS.M_renercerMoorow232::_faibeemelJurfear
	uint32_t ____faibeemelJurfear_1;
	// System.String GarbageiOS.M_renercerMoorow232::_dejeve
	String_t* ____dejeve_2;
	// System.Boolean GarbageiOS.M_renercerMoorow232::_celoutiRemyisste
	bool ____celoutiRemyisste_3;
	// System.String GarbageiOS.M_renercerMoorow232::_xerezolai
	String_t* ____xerezolai_4;
	// System.UInt32 GarbageiOS.M_renercerMoorow232::_sageake
	uint32_t ____sageake_5;
	// System.UInt32 GarbageiOS.M_renercerMoorow232::_beakiBeldas
	uint32_t ____beakiBeldas_6;
	// System.UInt32 GarbageiOS.M_renercerMoorow232::_pebearDrowsou
	uint32_t ____pebearDrowsou_7;
	// System.Int32 GarbageiOS.M_renercerMoorow232::_keqa
	int32_t ____keqa_8;
	// System.Int32 GarbageiOS.M_renercerMoorow232::_hairdir
	int32_t ____hairdir_9;
	// System.Int32 GarbageiOS.M_renercerMoorow232::_wallereChaikiju
	int32_t ____wallereChaikiju_10;
	// System.UInt32 GarbageiOS.M_renercerMoorow232::_ralsallgi
	uint32_t ____ralsallgi_11;
	// System.UInt32 GarbageiOS.M_renercerMoorow232::_neetrewhem
	uint32_t ____neetrewhem_12;
	// System.Int32 GarbageiOS.M_renercerMoorow232::_rishemHanur
	int32_t ____rishemHanur_13;
	// System.Boolean GarbageiOS.M_renercerMoorow232::_stohelbaw
	bool ____stohelbaw_14;
	// System.Int32 GarbageiOS.M_renercerMoorow232::_jeesiFisli
	int32_t ____jeesiFisli_15;
	// System.Boolean GarbageiOS.M_renercerMoorow232::_sexerParalqair
	bool ____sexerParalqair_16;
	// System.Boolean GarbageiOS.M_renercerMoorow232::_demdirmas
	bool ____demdirmas_17;
	// System.UInt32 GarbageiOS.M_renercerMoorow232::_pehurfi
	uint32_t ____pehurfi_18;
	// System.Boolean GarbageiOS.M_renercerMoorow232::_kouhehirDrartairxe
	bool ____kouhehirDrartairxe_19;

public:
	inline static int32_t get_offset_of__yotruHelpem_0() { return static_cast<int32_t>(offsetof(M_renercerMoorow232_t1060498076, ____yotruHelpem_0)); }
	inline uint32_t get__yotruHelpem_0() const { return ____yotruHelpem_0; }
	inline uint32_t* get_address_of__yotruHelpem_0() { return &____yotruHelpem_0; }
	inline void set__yotruHelpem_0(uint32_t value)
	{
		____yotruHelpem_0 = value;
	}

	inline static int32_t get_offset_of__faibeemelJurfear_1() { return static_cast<int32_t>(offsetof(M_renercerMoorow232_t1060498076, ____faibeemelJurfear_1)); }
	inline uint32_t get__faibeemelJurfear_1() const { return ____faibeemelJurfear_1; }
	inline uint32_t* get_address_of__faibeemelJurfear_1() { return &____faibeemelJurfear_1; }
	inline void set__faibeemelJurfear_1(uint32_t value)
	{
		____faibeemelJurfear_1 = value;
	}

	inline static int32_t get_offset_of__dejeve_2() { return static_cast<int32_t>(offsetof(M_renercerMoorow232_t1060498076, ____dejeve_2)); }
	inline String_t* get__dejeve_2() const { return ____dejeve_2; }
	inline String_t** get_address_of__dejeve_2() { return &____dejeve_2; }
	inline void set__dejeve_2(String_t* value)
	{
		____dejeve_2 = value;
		Il2CppCodeGenWriteBarrier(&____dejeve_2, value);
	}

	inline static int32_t get_offset_of__celoutiRemyisste_3() { return static_cast<int32_t>(offsetof(M_renercerMoorow232_t1060498076, ____celoutiRemyisste_3)); }
	inline bool get__celoutiRemyisste_3() const { return ____celoutiRemyisste_3; }
	inline bool* get_address_of__celoutiRemyisste_3() { return &____celoutiRemyisste_3; }
	inline void set__celoutiRemyisste_3(bool value)
	{
		____celoutiRemyisste_3 = value;
	}

	inline static int32_t get_offset_of__xerezolai_4() { return static_cast<int32_t>(offsetof(M_renercerMoorow232_t1060498076, ____xerezolai_4)); }
	inline String_t* get__xerezolai_4() const { return ____xerezolai_4; }
	inline String_t** get_address_of__xerezolai_4() { return &____xerezolai_4; }
	inline void set__xerezolai_4(String_t* value)
	{
		____xerezolai_4 = value;
		Il2CppCodeGenWriteBarrier(&____xerezolai_4, value);
	}

	inline static int32_t get_offset_of__sageake_5() { return static_cast<int32_t>(offsetof(M_renercerMoorow232_t1060498076, ____sageake_5)); }
	inline uint32_t get__sageake_5() const { return ____sageake_5; }
	inline uint32_t* get_address_of__sageake_5() { return &____sageake_5; }
	inline void set__sageake_5(uint32_t value)
	{
		____sageake_5 = value;
	}

	inline static int32_t get_offset_of__beakiBeldas_6() { return static_cast<int32_t>(offsetof(M_renercerMoorow232_t1060498076, ____beakiBeldas_6)); }
	inline uint32_t get__beakiBeldas_6() const { return ____beakiBeldas_6; }
	inline uint32_t* get_address_of__beakiBeldas_6() { return &____beakiBeldas_6; }
	inline void set__beakiBeldas_6(uint32_t value)
	{
		____beakiBeldas_6 = value;
	}

	inline static int32_t get_offset_of__pebearDrowsou_7() { return static_cast<int32_t>(offsetof(M_renercerMoorow232_t1060498076, ____pebearDrowsou_7)); }
	inline uint32_t get__pebearDrowsou_7() const { return ____pebearDrowsou_7; }
	inline uint32_t* get_address_of__pebearDrowsou_7() { return &____pebearDrowsou_7; }
	inline void set__pebearDrowsou_7(uint32_t value)
	{
		____pebearDrowsou_7 = value;
	}

	inline static int32_t get_offset_of__keqa_8() { return static_cast<int32_t>(offsetof(M_renercerMoorow232_t1060498076, ____keqa_8)); }
	inline int32_t get__keqa_8() const { return ____keqa_8; }
	inline int32_t* get_address_of__keqa_8() { return &____keqa_8; }
	inline void set__keqa_8(int32_t value)
	{
		____keqa_8 = value;
	}

	inline static int32_t get_offset_of__hairdir_9() { return static_cast<int32_t>(offsetof(M_renercerMoorow232_t1060498076, ____hairdir_9)); }
	inline int32_t get__hairdir_9() const { return ____hairdir_9; }
	inline int32_t* get_address_of__hairdir_9() { return &____hairdir_9; }
	inline void set__hairdir_9(int32_t value)
	{
		____hairdir_9 = value;
	}

	inline static int32_t get_offset_of__wallereChaikiju_10() { return static_cast<int32_t>(offsetof(M_renercerMoorow232_t1060498076, ____wallereChaikiju_10)); }
	inline int32_t get__wallereChaikiju_10() const { return ____wallereChaikiju_10; }
	inline int32_t* get_address_of__wallereChaikiju_10() { return &____wallereChaikiju_10; }
	inline void set__wallereChaikiju_10(int32_t value)
	{
		____wallereChaikiju_10 = value;
	}

	inline static int32_t get_offset_of__ralsallgi_11() { return static_cast<int32_t>(offsetof(M_renercerMoorow232_t1060498076, ____ralsallgi_11)); }
	inline uint32_t get__ralsallgi_11() const { return ____ralsallgi_11; }
	inline uint32_t* get_address_of__ralsallgi_11() { return &____ralsallgi_11; }
	inline void set__ralsallgi_11(uint32_t value)
	{
		____ralsallgi_11 = value;
	}

	inline static int32_t get_offset_of__neetrewhem_12() { return static_cast<int32_t>(offsetof(M_renercerMoorow232_t1060498076, ____neetrewhem_12)); }
	inline uint32_t get__neetrewhem_12() const { return ____neetrewhem_12; }
	inline uint32_t* get_address_of__neetrewhem_12() { return &____neetrewhem_12; }
	inline void set__neetrewhem_12(uint32_t value)
	{
		____neetrewhem_12 = value;
	}

	inline static int32_t get_offset_of__rishemHanur_13() { return static_cast<int32_t>(offsetof(M_renercerMoorow232_t1060498076, ____rishemHanur_13)); }
	inline int32_t get__rishemHanur_13() const { return ____rishemHanur_13; }
	inline int32_t* get_address_of__rishemHanur_13() { return &____rishemHanur_13; }
	inline void set__rishemHanur_13(int32_t value)
	{
		____rishemHanur_13 = value;
	}

	inline static int32_t get_offset_of__stohelbaw_14() { return static_cast<int32_t>(offsetof(M_renercerMoorow232_t1060498076, ____stohelbaw_14)); }
	inline bool get__stohelbaw_14() const { return ____stohelbaw_14; }
	inline bool* get_address_of__stohelbaw_14() { return &____stohelbaw_14; }
	inline void set__stohelbaw_14(bool value)
	{
		____stohelbaw_14 = value;
	}

	inline static int32_t get_offset_of__jeesiFisli_15() { return static_cast<int32_t>(offsetof(M_renercerMoorow232_t1060498076, ____jeesiFisli_15)); }
	inline int32_t get__jeesiFisli_15() const { return ____jeesiFisli_15; }
	inline int32_t* get_address_of__jeesiFisli_15() { return &____jeesiFisli_15; }
	inline void set__jeesiFisli_15(int32_t value)
	{
		____jeesiFisli_15 = value;
	}

	inline static int32_t get_offset_of__sexerParalqair_16() { return static_cast<int32_t>(offsetof(M_renercerMoorow232_t1060498076, ____sexerParalqair_16)); }
	inline bool get__sexerParalqair_16() const { return ____sexerParalqair_16; }
	inline bool* get_address_of__sexerParalqair_16() { return &____sexerParalqair_16; }
	inline void set__sexerParalqair_16(bool value)
	{
		____sexerParalqair_16 = value;
	}

	inline static int32_t get_offset_of__demdirmas_17() { return static_cast<int32_t>(offsetof(M_renercerMoorow232_t1060498076, ____demdirmas_17)); }
	inline bool get__demdirmas_17() const { return ____demdirmas_17; }
	inline bool* get_address_of__demdirmas_17() { return &____demdirmas_17; }
	inline void set__demdirmas_17(bool value)
	{
		____demdirmas_17 = value;
	}

	inline static int32_t get_offset_of__pehurfi_18() { return static_cast<int32_t>(offsetof(M_renercerMoorow232_t1060498076, ____pehurfi_18)); }
	inline uint32_t get__pehurfi_18() const { return ____pehurfi_18; }
	inline uint32_t* get_address_of__pehurfi_18() { return &____pehurfi_18; }
	inline void set__pehurfi_18(uint32_t value)
	{
		____pehurfi_18 = value;
	}

	inline static int32_t get_offset_of__kouhehirDrartairxe_19() { return static_cast<int32_t>(offsetof(M_renercerMoorow232_t1060498076, ____kouhehirDrartairxe_19)); }
	inline bool get__kouhehirDrartairxe_19() const { return ____kouhehirDrartairxe_19; }
	inline bool* get_address_of__kouhehirDrartairxe_19() { return &____kouhehirDrartairxe_19; }
	inline void set__kouhehirDrartairxe_19(bool value)
	{
		____kouhehirDrartairxe_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
