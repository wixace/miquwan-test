﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._637622168db9c2e40c39460a4b5ac3af
struct _637622168db9c2e40c39460a4b5ac3af_t402205590;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__637622168db9c2e40c39460a4402205590.h"

// System.Void Little._637622168db9c2e40c39460a4b5ac3af::.ctor()
extern "C"  void _637622168db9c2e40c39460a4b5ac3af__ctor_m83010391 (_637622168db9c2e40c39460a4b5ac3af_t402205590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._637622168db9c2e40c39460a4b5ac3af::_637622168db9c2e40c39460a4b5ac3afm2(System.Int32)
extern "C"  int32_t _637622168db9c2e40c39460a4b5ac3af__637622168db9c2e40c39460a4b5ac3afm2_m1753856473 (_637622168db9c2e40c39460a4b5ac3af_t402205590 * __this, int32_t ____637622168db9c2e40c39460a4b5ac3afa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._637622168db9c2e40c39460a4b5ac3af::_637622168db9c2e40c39460a4b5ac3afm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _637622168db9c2e40c39460a4b5ac3af__637622168db9c2e40c39460a4b5ac3afm_m4124882173 (_637622168db9c2e40c39460a4b5ac3af_t402205590 * __this, int32_t ____637622168db9c2e40c39460a4b5ac3afa0, int32_t ____637622168db9c2e40c39460a4b5ac3af941, int32_t ____637622168db9c2e40c39460a4b5ac3afc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._637622168db9c2e40c39460a4b5ac3af::ilo__637622168db9c2e40c39460a4b5ac3afm21(Little._637622168db9c2e40c39460a4b5ac3af,System.Int32)
extern "C"  int32_t _637622168db9c2e40c39460a4b5ac3af_ilo__637622168db9c2e40c39460a4b5ac3afm21_m2438268445 (Il2CppObject * __this /* static, unused */, _637622168db9c2e40c39460a4b5ac3af_t402205590 * ____this0, int32_t ____637622168db9c2e40c39460a4b5ac3afa1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
