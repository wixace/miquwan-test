﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSCheckPointUnitGenerated
struct CSCheckPointUnitGenerated_t2364176723;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void CSCheckPointUnitGenerated::.ctor()
extern "C"  void CSCheckPointUnitGenerated__ctor_m1628543352 (CSCheckPointUnitGenerated_t2364176723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSCheckPointUnitGenerated::CSCheckPointUnit_CSCheckPointUnit1(JSVCall,System.Int32)
extern "C"  bool CSCheckPointUnitGenerated_CSCheckPointUnit_CSCheckPointUnit1_m1172149626 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCheckPointUnitGenerated::CSCheckPointUnit_cfg(JSVCall)
extern "C"  void CSCheckPointUnitGenerated_CSCheckPointUnit_cfg_m3981209946 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCheckPointUnitGenerated::CSCheckPointUnit_id(JSVCall)
extern "C"  void CSCheckPointUnitGenerated_CSCheckPointUnit_id_m231392275 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCheckPointUnitGenerated::CSCheckPointUnit_stars(JSVCall)
extern "C"  void CSCheckPointUnitGenerated_CSCheckPointUnit_stars_m1677544445 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCheckPointUnitGenerated::CSCheckPointUnit_surpEnterCount(JSVCall)
extern "C"  void CSCheckPointUnitGenerated_CSCheckPointUnit_surpEnterCount_m4176414551 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCheckPointUnitGenerated::CSCheckPointUnit_state(JSVCall)
extern "C"  void CSCheckPointUnitGenerated_CSCheckPointUnit_state_m834830797 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCheckPointUnitGenerated::__Register()
extern "C"  void CSCheckPointUnitGenerated___Register_m3916238415 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSCheckPointUnitGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool CSCheckPointUnitGenerated_ilo_attachFinalizerObject1_m1496059007 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCheckPointUnitGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void CSCheckPointUnitGenerated_ilo_addJSCSRel2_m3884169397 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCheckPointUnitGenerated::ilo_setUInt323(System.Int32,System.UInt32)
extern "C"  void CSCheckPointUnitGenerated_ilo_setUInt323_m336656156 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSCheckPointUnitGenerated::ilo_getUInt324(System.Int32)
extern "C"  uint32_t CSCheckPointUnitGenerated_ilo_getUInt324_m892312634 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
