﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EventNameMgr
struct EventNameMgr_t2794511283;

#include "codegen/il2cpp-codegen.h"

// System.Void EventNameMgr::.ctor()
extern "C"  void EventNameMgr__ctor_m3864498248 (EventNameMgr_t2794511283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
