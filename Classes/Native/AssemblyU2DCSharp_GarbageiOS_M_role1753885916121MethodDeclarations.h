﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_role175
struct M_role175_t3885916121;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_role1753885916121.h"

// System.Void GarbageiOS.M_role175::.ctor()
extern "C"  void M_role175__ctor_m3389959082 (M_role175_t3885916121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_role175::M_sirmou0(System.String[],System.Int32)
extern "C"  void M_role175_M_sirmou0_m770169542 (M_role175_t3885916121 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_role175::M_dawxow1(System.String[],System.Int32)
extern "C"  void M_role175_M_dawxow1_m754657112 (M_role175_t3885916121 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_role175::M_peapoo2(System.String[],System.Int32)
extern "C"  void M_role175_M_peapoo2_m4254998683 (M_role175_t3885916121 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_role175::M_hohardelMuzu3(System.String[],System.Int32)
extern "C"  void M_role175_M_hohardelMuzu3_m2246781636 (M_role175_t3885916121 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_role175::M_boujalisLearutou4(System.String[],System.Int32)
extern "C"  void M_role175_M_boujalisLearutou4_m2716345051 (M_role175_t3885916121 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_role175::M_hasaskuNeamurtray5(System.String[],System.Int32)
extern "C"  void M_role175_M_hasaskuNeamurtray5_m254523276 (M_role175_t3885916121 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_role175::M_horre6(System.String[],System.Int32)
extern "C"  void M_role175_M_horre6_m975761115 (M_role175_t3885916121 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_role175::M_nenasheaPalco7(System.String[],System.Int32)
extern "C"  void M_role175_M_nenasheaPalco7_m199124334 (M_role175_t3885916121 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_role175::ilo_M_peapoo21(GarbageiOS.M_role175,System.String[],System.Int32)
extern "C"  void M_role175_ilo_M_peapoo21_m2821379756 (Il2CppObject * __this /* static, unused */, M_role175_t3885916121 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_role175::ilo_M_hohardelMuzu32(GarbageiOS.M_role175,System.String[],System.Int32)
extern "C"  void M_role175_ilo_M_hohardelMuzu32_m842313908 (Il2CppObject * __this /* static, unused */, M_role175_t3885916121 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_role175::ilo_M_horre63(GarbageiOS.M_role175,System.String[],System.Int32)
extern "C"  void M_role175_ilo_M_horre63_m148056496 (Il2CppObject * __this /* static, unused */, M_role175_t3885916121 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
