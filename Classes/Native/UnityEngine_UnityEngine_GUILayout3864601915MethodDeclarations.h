﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GUILayout
struct GUILayout_t3864601915;
// UnityEngine.Texture
struct Texture_t2526458961;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t2977405297;
// System.String
struct String_t;
// UnityEngine.GUIContent
struct GUIContent_t2094828418;
// UnityEngine.GUIStyle
struct GUIStyle_t2990928826;
// System.String[]
struct StringU5BU5D_t4054002952;
// UnityEngine.Texture[]
struct TextureU5BU5D_t606176076;
// UnityEngine.GUIContent[]
struct GUIContentU5BU5D_t3588725815;
// UnityEngine.GUI/WindowFunction
struct WindowFunction_t2749288659;
// UnityEngine.GUILayoutOption
struct GUILayoutOption_t331591504;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_GUIContent2094828418.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction2749288659.h"

// System.Void UnityEngine.GUILayout::.ctor()
extern "C"  void GUILayout__ctor_m3338561076 (GUILayout_t3864601915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::Label(UnityEngine.Texture,UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_Label_m1657324111 (Il2CppObject * __this /* static, unused */, Texture_t2526458961 * ___image0, GUILayoutOptionU5BU5D_t2977405297* ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::Label(System.String,UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_Label_m925445731 (Il2CppObject * __this /* static, unused */, String_t* ___text0, GUILayoutOptionU5BU5D_t2977405297* ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::Label(UnityEngine.GUIContent,UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_Label_m3931056408 (Il2CppObject * __this /* static, unused */, GUIContent_t2094828418 * ___content0, GUILayoutOptionU5BU5D_t2977405297* ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::Label(UnityEngine.Texture,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_Label_m3219475192 (Il2CppObject * __this /* static, unused */, Texture_t2526458961 * ___image0, GUIStyle_t2990928826 * ___style1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::Label(System.String,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_Label_m1421013860 (Il2CppObject * __this /* static, unused */, String_t* ___text0, GUIStyle_t2990928826 * ___style1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::Label(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_Label_m796535759 (Il2CppObject * __this /* static, unused */, GUIContent_t2094828418 * ___content0, GUIStyle_t2990928826 * ___style1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::DoLabel(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_DoLabel_m3812046522 (Il2CppObject * __this /* static, unused */, GUIContent_t2094828418 * ___content0, GUIStyle_t2990928826 * ___style1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::Box(UnityEngine.Texture,UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_Box_m2655113688 (Il2CppObject * __this /* static, unused */, Texture_t2526458961 * ___image0, GUILayoutOptionU5BU5D_t2977405297* ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::Box(System.String,UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_Box_m140695724 (Il2CppObject * __this /* static, unused */, String_t* ___text0, GUILayoutOptionU5BU5D_t2977405297* ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::Box(UnityEngine.GUIContent,UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_Box_m3611689199 (Il2CppObject * __this /* static, unused */, GUIContent_t2094828418 * ___content0, GUILayoutOptionU5BU5D_t2977405297* ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::Box(UnityEngine.Texture,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_Box_m1837923087 (Il2CppObject * __this /* static, unused */, Texture_t2526458961 * ___image0, GUIStyle_t2990928826 * ___style1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::Box(System.String,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_Box_m3676946619 (Il2CppObject * __this /* static, unused */, String_t* ___text0, GUIStyle_t2990928826 * ___style1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::Box(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_Box_m1649373272 (Il2CppObject * __this /* static, unused */, GUIContent_t2094828418 * ___content0, GUIStyle_t2990928826 * ___style1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::DoBox(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_DoBox_m2769828355 (Il2CppObject * __this /* static, unused */, GUIContent_t2094828418 * ___content0, GUIStyle_t2990928826 * ___style1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUILayout::Button(UnityEngine.Texture,UnityEngine.GUILayoutOption[])
extern "C"  bool GUILayout_Button_m2390187641 (Il2CppObject * __this /* static, unused */, Texture_t2526458961 * ___image0, GUILayoutOptionU5BU5D_t2977405297* ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUILayout::Button(System.String,UnityEngine.GUILayoutOption[])
extern "C"  bool GUILayout_Button_m6468109 (Il2CppObject * __this /* static, unused */, String_t* ___text0, GUILayoutOptionU5BU5D_t2977405297* ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUILayout::Button(UnityEngine.GUIContent,UnityEngine.GUILayoutOption[])
extern "C"  bool GUILayout_Button_m1054745774 (Il2CppObject * __this /* static, unused */, GUIContent_t2094828418 * ___content0, GUILayoutOptionU5BU5D_t2977405297* ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUILayout::Button(UnityEngine.Texture,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  bool GUILayout_Button_m3701525774 (Il2CppObject * __this /* static, unused */, Texture_t2526458961 * ___image0, GUIStyle_t2990928826 * ___style1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUILayout::Button(System.String,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  bool GUILayout_Button_m2087140858 (Il2CppObject * __this /* static, unused */, String_t* ___text0, GUIStyle_t2990928826 * ___style1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUILayout::Button(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  bool GUILayout_Button_m3489753593 (Il2CppObject * __this /* static, unused */, GUIContent_t2094828418 * ___content0, GUIStyle_t2990928826 * ___style1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUILayout::DoButton(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  bool GUILayout_DoButton_m1519327982 (Il2CppObject * __this /* static, unused */, GUIContent_t2094828418 * ___content0, GUIStyle_t2990928826 * ___style1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUILayout::RepeatButton(UnityEngine.Texture,UnityEngine.GUILayoutOption[])
extern "C"  bool GUILayout_RepeatButton_m3501415390 (Il2CppObject * __this /* static, unused */, Texture_t2526458961 * ___image0, GUILayoutOptionU5BU5D_t2977405297* ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUILayout::RepeatButton(System.String,UnityEngine.GUILayoutOption[])
extern "C"  bool GUILayout_RepeatButton_m690872114 (Il2CppObject * __this /* static, unused */, String_t* ___text0, GUILayoutOptionU5BU5D_t2977405297* ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUILayout::RepeatButton(UnityEngine.GUIContent,UnityEngine.GUILayoutOption[])
extern "C"  bool GUILayout_RepeatButton_m32698665 (Il2CppObject * __this /* static, unused */, GUIContent_t2094828418 * ___content0, GUILayoutOptionU5BU5D_t2977405297* ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUILayout::RepeatButton(UnityEngine.Texture,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  bool GUILayout_RepeatButton_m427266249 (Il2CppObject * __this /* static, unused */, Texture_t2526458961 * ___image0, GUIStyle_t2990928826 * ___style1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUILayout::RepeatButton(System.String,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  bool GUILayout_RepeatButton_m1134438901 (Il2CppObject * __this /* static, unused */, String_t* ___text0, GUIStyle_t2990928826 * ___style1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUILayout::RepeatButton(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  bool GUILayout_RepeatButton_m3026503774 (Il2CppObject * __this /* static, unused */, GUIContent_t2094828418 * ___content0, GUIStyle_t2990928826 * ___style1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUILayout::DoRepeatButton(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  bool GUILayout_DoRepeatButton_m1246047635 (Il2CppObject * __this /* static, unused */, GUIContent_t2094828418 * ___content0, GUIStyle_t2990928826 * ___style1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUILayout::TextField(System.String,UnityEngine.GUILayoutOption[])
extern "C"  String_t* GUILayout_TextField_m3370910503 (Il2CppObject * __this /* static, unused */, String_t* ___text0, GUILayoutOptionU5BU5D_t2977405297* ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUILayout::TextField(System.String,System.Int32,UnityEngine.GUILayoutOption[])
extern "C"  String_t* GUILayout_TextField_m1362880318 (Il2CppObject * __this /* static, unused */, String_t* ___text0, int32_t ___maxLength1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUILayout::TextField(System.String,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  String_t* GUILayout_TextField_m2367652128 (Il2CppObject * __this /* static, unused */, String_t* ___text0, GUIStyle_t2990928826 * ___style1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUILayout::TextField(System.String,System.Int32,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  String_t* GUILayout_TextField_m3429491049 (Il2CppObject * __this /* static, unused */, String_t* ___text0, int32_t ___maxLength1, GUIStyle_t2990928826 * ___style2, GUILayoutOptionU5BU5D_t2977405297* ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUILayout::PasswordField(System.String,System.Char,UnityEngine.GUILayoutOption[])
extern "C"  String_t* GUILayout_PasswordField_m3490804332 (Il2CppObject * __this /* static, unused */, String_t* ___password0, Il2CppChar ___maskChar1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUILayout::PasswordField(System.String,System.Char,System.Int32,UnityEngine.GUILayoutOption[])
extern "C"  String_t* GUILayout_PasswordField_m3647615001 (Il2CppObject * __this /* static, unused */, String_t* ___password0, Il2CppChar ___maskChar1, int32_t ___maxLength2, GUILayoutOptionU5BU5D_t2977405297* ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUILayout::PasswordField(System.String,System.Char,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  String_t* GUILayout_PasswordField_m487636731 (Il2CppObject * __this /* static, unused */, String_t* ___password0, Il2CppChar ___maskChar1, GUIStyle_t2990928826 * ___style2, GUILayoutOptionU5BU5D_t2977405297* ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUILayout::PasswordField(System.String,System.Char,System.Int32,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  String_t* GUILayout_PasswordField_m2111782766 (Il2CppObject * __this /* static, unused */, String_t* ___password0, Il2CppChar ___maskChar1, int32_t ___maxLength2, GUIStyle_t2990928826 * ___style3, GUILayoutOptionU5BU5D_t2977405297* ___options4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUILayout::TextArea(System.String,UnityEngine.GUILayoutOption[])
extern "C"  String_t* GUILayout_TextArea_m173372194 (Il2CppObject * __this /* static, unused */, String_t* ___text0, GUILayoutOptionU5BU5D_t2977405297* ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUILayout::TextArea(System.String,System.Int32,UnityEngine.GUILayoutOption[])
extern "C"  String_t* GUILayout_TextArea_m22251299 (Il2CppObject * __this /* static, unused */, String_t* ___text0, int32_t ___maxLength1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUILayout::TextArea(System.String,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  String_t* GUILayout_TextArea_m1482648581 (Il2CppObject * __this /* static, unused */, String_t* ___text0, GUIStyle_t2990928826 * ___style1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUILayout::TextArea(System.String,System.Int32,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  String_t* GUILayout_TextArea_m1161532068 (Il2CppObject * __this /* static, unused */, String_t* ___text0, int32_t ___maxLength1, GUIStyle_t2990928826 * ___style2, GUILayoutOptionU5BU5D_t2977405297* ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUILayout::DoTextField(System.String,System.Int32,System.Boolean,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  String_t* GUILayout_DoTextField_m3824368015 (Il2CppObject * __this /* static, unused */, String_t* ___text0, int32_t ___maxLength1, bool ___multiline2, GUIStyle_t2990928826 * ___style3, GUILayoutOptionU5BU5D_t2977405297* ___options4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUILayout::Toggle(System.Boolean,UnityEngine.Texture,UnityEngine.GUILayoutOption[])
extern "C"  bool GUILayout_Toggle_m1227803716 (Il2CppObject * __this /* static, unused */, bool ___value0, Texture_t2526458961 * ___image1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUILayout::Toggle(System.Boolean,System.String,UnityEngine.GUILayoutOption[])
extern "C"  bool GUILayout_Toggle_m3179746328 (Il2CppObject * __this /* static, unused */, bool ___value0, String_t* ___text1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUILayout::Toggle(System.Boolean,UnityEngine.GUIContent,UnityEngine.GUILayoutOption[])
extern "C"  bool GUILayout_Toggle_m2796543747 (Il2CppObject * __this /* static, unused */, bool ___value0, GUIContent_t2094828418 * ___content1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUILayout::Toggle(System.Boolean,UnityEngine.Texture,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  bool GUILayout_Toggle_m1724546083 (Il2CppObject * __this /* static, unused */, bool ___value0, Texture_t2526458961 * ___image1, GUIStyle_t2990928826 * ___style2, GUILayoutOptionU5BU5D_t2977405297* ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUILayout::Toggle(System.Boolean,System.String,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  bool GUILayout_Toggle_m385032399 (Il2CppObject * __this /* static, unused */, bool ___value0, String_t* ___text1, GUIStyle_t2990928826 * ___style2, GUILayoutOptionU5BU5D_t2977405297* ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUILayout::Toggle(System.Boolean,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  bool GUILayout_Toggle_m4174309060 (Il2CppObject * __this /* static, unused */, bool ___value0, GUIContent_t2094828418 * ___content1, GUIStyle_t2990928826 * ___style2, GUILayoutOptionU5BU5D_t2977405297* ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUILayout::DoToggle(System.Boolean,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  bool GUILayout_DoToggle_m2344914223 (Il2CppObject * __this /* static, unused */, bool ___value0, GUIContent_t2094828418 * ___content1, GUIStyle_t2990928826 * ___style2, GUILayoutOptionU5BU5D_t2977405297* ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GUILayout::Toolbar(System.Int32,System.String[],UnityEngine.GUILayoutOption[])
extern "C"  int32_t GUILayout_Toolbar_m1048546399 (Il2CppObject * __this /* static, unused */, int32_t ___selected0, StringU5BU5D_t4054002952* ___texts1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GUILayout::Toolbar(System.Int32,UnityEngine.Texture[],UnityEngine.GUILayoutOption[])
extern "C"  int32_t GUILayout_Toolbar_m672208587 (Il2CppObject * __this /* static, unused */, int32_t ___selected0, TextureU5BU5D_t606176076* ___images1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GUILayout::Toolbar(System.Int32,UnityEngine.GUIContent[],UnityEngine.GUILayoutOption[])
extern "C"  int32_t GUILayout_Toolbar_m4156742360 (Il2CppObject * __this /* static, unused */, int32_t ___selected0, GUIContentU5BU5D_t3588725815* ___content1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GUILayout::Toolbar(System.Int32,System.String[],UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  int32_t GUILayout_Toolbar_m1603037928 (Il2CppObject * __this /* static, unused */, int32_t ___selected0, StringU5BU5D_t4054002952* ___texts1, GUIStyle_t2990928826 * ___style2, GUILayoutOptionU5BU5D_t2977405297* ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GUILayout::Toolbar(System.Int32,UnityEngine.Texture[],UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  int32_t GUILayout_Toolbar_m2711608316 (Il2CppObject * __this /* static, unused */, int32_t ___selected0, TextureU5BU5D_t606176076* ___images1, GUIStyle_t2990928826 * ___style2, GUILayoutOptionU5BU5D_t2977405297* ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GUILayout::Toolbar(System.Int32,UnityEngine.GUIContent[],UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  int32_t GUILayout_Toolbar_m1916473871 (Il2CppObject * __this /* static, unused */, int32_t ___selected0, GUIContentU5BU5D_t3588725815* ___contents1, GUIStyle_t2990928826 * ___style2, GUILayoutOptionU5BU5D_t2977405297* ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GUILayout::SelectionGrid(System.Int32,System.String[],System.Int32,UnityEngine.GUILayoutOption[])
extern "C"  int32_t GUILayout_SelectionGrid_m1853214767 (Il2CppObject * __this /* static, unused */, int32_t ___selected0, StringU5BU5D_t4054002952* ___texts1, int32_t ___xCount2, GUILayoutOptionU5BU5D_t2977405297* ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GUILayout::SelectionGrid(System.Int32,UnityEngine.Texture[],System.Int32,UnityEngine.GUILayoutOption[])
extern "C"  int32_t GUILayout_SelectionGrid_m3565146755 (Il2CppObject * __this /* static, unused */, int32_t ___selected0, TextureU5BU5D_t606176076* ___images1, int32_t ___xCount2, GUILayoutOptionU5BU5D_t2977405297* ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GUILayout::SelectionGrid(System.Int32,UnityEngine.GUIContent[],System.Int32,UnityEngine.GUILayoutOption[])
extern "C"  int32_t GUILayout_SelectionGrid_m2620464676 (Il2CppObject * __this /* static, unused */, int32_t ___selected0, GUIContentU5BU5D_t3588725815* ___content1, int32_t ___xCount2, GUILayoutOptionU5BU5D_t2977405297* ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GUILayout::SelectionGrid(System.Int32,System.String[],System.Int32,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  int32_t GUILayout_SelectionGrid_m836086040 (Il2CppObject * __this /* static, unused */, int32_t ___selected0, StringU5BU5D_t4054002952* ___texts1, int32_t ___xCount2, GUIStyle_t2990928826 * ___style3, GUILayoutOptionU5BU5D_t2977405297* ___options4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GUILayout::SelectionGrid(System.Int32,UnityEngine.Texture[],System.Int32,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  int32_t GUILayout_SelectionGrid_m90317636 (Il2CppObject * __this /* static, unused */, int32_t ___selected0, TextureU5BU5D_t606176076* ___images1, int32_t ___xCount2, GUIStyle_t2990928826 * ___style3, GUILayoutOptionU5BU5D_t2977405297* ___options4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GUILayout::SelectionGrid(System.Int32,UnityEngine.GUIContent[],System.Int32,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  int32_t GUILayout_SelectionGrid_m1847072323 (Il2CppObject * __this /* static, unused */, int32_t ___selected0, GUIContentU5BU5D_t3588725815* ___contents1, int32_t ___xCount2, GUIStyle_t2990928826 * ___style3, GUILayoutOptionU5BU5D_t2977405297* ___options4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.GUILayout::HorizontalSlider(System.Single,System.Single,System.Single,UnityEngine.GUILayoutOption[])
extern "C"  float GUILayout_HorizontalSlider_m2607521747 (Il2CppObject * __this /* static, unused */, float ___value0, float ___leftValue1, float ___rightValue2, GUILayoutOptionU5BU5D_t2977405297* ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.GUILayout::HorizontalSlider(System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  float GUILayout_HorizontalSlider_m3325144691 (Il2CppObject * __this /* static, unused */, float ___value0, float ___leftValue1, float ___rightValue2, GUIStyle_t2990928826 * ___slider3, GUIStyle_t2990928826 * ___thumb4, GUILayoutOptionU5BU5D_t2977405297* ___options5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.GUILayout::DoHorizontalSlider(System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  float GUILayout_DoHorizontalSlider_m3518318248 (Il2CppObject * __this /* static, unused */, float ___value0, float ___leftValue1, float ___rightValue2, GUIStyle_t2990928826 * ___slider3, GUIStyle_t2990928826 * ___thumb4, GUILayoutOptionU5BU5D_t2977405297* ___options5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.GUILayout::VerticalSlider(System.Single,System.Single,System.Single,UnityEngine.GUILayoutOption[])
extern "C"  float GUILayout_VerticalSlider_m339552641 (Il2CppObject * __this /* static, unused */, float ___value0, float ___leftValue1, float ___rightValue2, GUILayoutOptionU5BU5D_t2977405297* ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.GUILayout::VerticalSlider(System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  float GUILayout_VerticalSlider_m2155953825 (Il2CppObject * __this /* static, unused */, float ___value0, float ___leftValue1, float ___rightValue2, GUIStyle_t2990928826 * ___slider3, GUIStyle_t2990928826 * ___thumb4, GUILayoutOptionU5BU5D_t2977405297* ___options5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.GUILayout::DoVerticalSlider(System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  float GUILayout_DoVerticalSlider_m2500288534 (Il2CppObject * __this /* static, unused */, float ___value0, float ___leftValue1, float ___rightValue2, GUIStyle_t2990928826 * ___slider3, GUIStyle_t2990928826 * ___thumb4, GUILayoutOptionU5BU5D_t2977405297* ___options5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.GUILayout::HorizontalScrollbar(System.Single,System.Single,System.Single,System.Single,UnityEngine.GUILayoutOption[])
extern "C"  float GUILayout_HorizontalScrollbar_m2983758913 (Il2CppObject * __this /* static, unused */, float ___value0, float ___size1, float ___leftValue2, float ___rightValue3, GUILayoutOptionU5BU5D_t2977405297* ___options4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.GUILayout::HorizontalScrollbar(System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  float GUILayout_HorizontalScrollbar_m3928460358 (Il2CppObject * __this /* static, unused */, float ___value0, float ___size1, float ___leftValue2, float ___rightValue3, GUIStyle_t2990928826 * ___style4, GUILayoutOptionU5BU5D_t2977405297* ___options5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.GUILayout::VerticalScrollbar(System.Single,System.Single,System.Single,System.Single,UnityEngine.GUILayoutOption[])
extern "C"  float GUILayout_VerticalScrollbar_m2186400851 (Il2CppObject * __this /* static, unused */, float ___value0, float ___size1, float ___topValue2, float ___bottomValue3, GUILayoutOptionU5BU5D_t2977405297* ___options4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.GUILayout::VerticalScrollbar(System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  float GUILayout_VerticalScrollbar_m1630692212 (Il2CppObject * __this /* static, unused */, float ___value0, float ___size1, float ___topValue2, float ___bottomValue3, GUIStyle_t2990928826 * ___style4, GUILayoutOptionU5BU5D_t2977405297* ___options5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::Space(System.Single)
extern "C"  void GUILayout_Space_m559926739 (Il2CppObject * __this /* static, unused */, float ___pixels0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::FlexibleSpace()
extern "C"  void GUILayout_FlexibleSpace_m3316706189 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::BeginHorizontal(UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_BeginHorizontal_m722450062 (Il2CppObject * __this /* static, unused */, GUILayoutOptionU5BU5D_t2977405297* ___options0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::BeginHorizontal(UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_BeginHorizontal_m2418525209 (Il2CppObject * __this /* static, unused */, GUIStyle_t2990928826 * ___style0, GUILayoutOptionU5BU5D_t2977405297* ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::BeginHorizontal(System.String,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_BeginHorizontal_m2435601053 (Il2CppObject * __this /* static, unused */, String_t* ___text0, GUIStyle_t2990928826 * ___style1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::BeginHorizontal(UnityEngine.Texture,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_BeginHorizontal_m887004017 (Il2CppObject * __this /* static, unused */, Texture_t2526458961 * ___image0, GUIStyle_t2990928826 * ___style1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::BeginHorizontal(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_BeginHorizontal_m2423643318 (Il2CppObject * __this /* static, unused */, GUIContent_t2094828418 * ___content0, GUIStyle_t2990928826 * ___style1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::EndHorizontal()
extern "C"  void GUILayout_EndHorizontal_m556624369 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::BeginVertical(UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_BeginVertical_m2155819644 (Il2CppObject * __this /* static, unused */, GUILayoutOptionU5BU5D_t2977405297* ___options0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::BeginVertical(UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_BeginVertical_m2534844651 (Il2CppObject * __this /* static, unused */, GUIStyle_t2990928826 * ___style0, GUILayoutOptionU5BU5D_t2977405297* ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::BeginVertical(System.String,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_BeginVertical_m3760111 (Il2CppObject * __this /* static, unused */, String_t* ___text0, GUIStyle_t2990928826 * ___style1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::BeginVertical(UnityEngine.Texture,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_BeginVertical_m149742403 (Il2CppObject * __this /* static, unused */, Texture_t2526458961 * ___image0, GUIStyle_t2990928826 * ___style1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::BeginVertical(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_BeginVertical_m3125652388 (Il2CppObject * __this /* static, unused */, GUIContent_t2094828418 * ___content0, GUIStyle_t2990928826 * ___style1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::EndVertical()
extern "C"  void GUILayout_EndVertical_m3685530563 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::BeginArea(UnityEngine.Rect)
extern "C"  void GUILayout_BeginArea_m4120101869 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___screenRect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::BeginArea(UnityEngine.Rect,System.String)
extern "C"  void GUILayout_BeginArea_m2598177129 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___screenRect0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::BeginArea(UnityEngine.Rect,UnityEngine.Texture)
extern "C"  void GUILayout_BeginArea_m4275248277 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___screenRect0, Texture_t2526458961 * ___image1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::BeginArea(UnityEngine.Rect,UnityEngine.GUIContent)
extern "C"  void GUILayout_BeginArea_m119836068 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___screenRect0, GUIContent_t2094828418 * ___content1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::BeginArea(UnityEngine.Rect,UnityEngine.GUIStyle)
extern "C"  void GUILayout_BeginArea_m3882119276 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___screenRect0, GUIStyle_t2990928826 * ___style1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::BeginArea(UnityEngine.Rect,System.String,UnityEngine.GUIStyle)
extern "C"  void GUILayout_BeginArea_m3452874864 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___screenRect0, String_t* ___text1, GUIStyle_t2990928826 * ___style2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::BeginArea(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.GUIStyle)
extern "C"  void GUILayout_BeginArea_m2876149956 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___screenRect0, Texture_t2526458961 * ___image1, GUIStyle_t2990928826 * ___style2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::BeginArea(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern "C"  void GUILayout_BeginArea_m2566106901 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___screenRect0, GUIContent_t2094828418 * ___content1, GUIStyle_t2990928826 * ___style2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::EndArea()
extern "C"  void GUILayout_EndArea_m755663130 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.GUILayout::BeginScrollView(UnityEngine.Vector2,UnityEngine.GUILayoutOption[])
extern "C"  Vector2_t4282066565  GUILayout_BeginScrollView_m247627737 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___scrollPosition0, GUILayoutOptionU5BU5D_t2977405297* ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.GUILayout::BeginScrollView(UnityEngine.Vector2,System.Boolean,System.Boolean,UnityEngine.GUILayoutOption[])
extern "C"  Vector2_t4282066565  GUILayout_BeginScrollView_m426646201 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___scrollPosition0, bool ___alwaysShowHorizontal1, bool ___alwaysShowVertical2, GUILayoutOptionU5BU5D_t2977405297* ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.GUILayout::BeginScrollView(UnityEngine.Vector2,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  Vector2_t4282066565  GUILayout_BeginScrollView_m1316562681 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___scrollPosition0, GUIStyle_t2990928826 * ___horizontalScrollbar1, GUIStyle_t2990928826 * ___verticalScrollbar2, GUILayoutOptionU5BU5D_t2977405297* ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.GUILayout::BeginScrollView(UnityEngine.Vector2,UnityEngine.GUIStyle)
extern "C"  Vector2_t4282066565  GUILayout_BeginScrollView_m4279067143 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___scrollPosition0, GUIStyle_t2990928826 * ___style1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.GUILayout::BeginScrollView(UnityEngine.Vector2,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  Vector2_t4282066565  GUILayout_BeginScrollView_m3093972910 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___scrollPosition0, GUIStyle_t2990928826 * ___style1, GUILayoutOptionU5BU5D_t2977405297* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.GUILayout::BeginScrollView(UnityEngine.Vector2,System.Boolean,System.Boolean,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  Vector2_t4282066565  GUILayout_BeginScrollView_m3376753113 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___scrollPosition0, bool ___alwaysShowHorizontal1, bool ___alwaysShowVertical2, GUIStyle_t2990928826 * ___horizontalScrollbar3, GUIStyle_t2990928826 * ___verticalScrollbar4, GUILayoutOptionU5BU5D_t2977405297* ___options5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.GUILayout::BeginScrollView(UnityEngine.Vector2,System.Boolean,System.Boolean,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  Vector2_t4282066565  GUILayout_BeginScrollView_m3507871150 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___scrollPosition0, bool ___alwaysShowHorizontal1, bool ___alwaysShowVertical2, GUIStyle_t2990928826 * ___horizontalScrollbar3, GUIStyle_t2990928826 * ___verticalScrollbar4, GUIStyle_t2990928826 * ___background5, GUILayoutOptionU5BU5D_t2977405297* ___options6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::EndScrollView()
extern "C"  void GUILayout_EndScrollView_m2116108639 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::EndScrollView(System.Boolean)
extern "C"  void GUILayout_EndScrollView_m3241059478 (Il2CppObject * __this /* static, unused */, bool ___handleScrollWheel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUILayout::Window(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,System.String,UnityEngine.GUILayoutOption[])
extern "C"  Rect_t4241904616  GUILayout_Window_m3742951272 (Il2CppObject * __this /* static, unused */, int32_t ___id0, Rect_t4241904616  ___screenRect1, WindowFunction_t2749288659 * ___func2, String_t* ___text3, GUILayoutOptionU5BU5D_t2977405297* ___options4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUILayout::Window(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,UnityEngine.Texture,UnityEngine.GUILayoutOption[])
extern "C"  Rect_t4241904616  GUILayout_Window_m2227757972 (Il2CppObject * __this /* static, unused */, int32_t ___id0, Rect_t4241904616  ___screenRect1, WindowFunction_t2749288659 * ___func2, Texture_t2526458961 * ___image3, GUILayoutOptionU5BU5D_t2977405297* ___options4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUILayout::Window(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUILayoutOption[])
extern "C"  Rect_t4241904616  GUILayout_Window_m2540619187 (Il2CppObject * __this /* static, unused */, int32_t ___id0, Rect_t4241904616  ___screenRect1, WindowFunction_t2749288659 * ___func2, GUIContent_t2094828418 * ___content3, GUILayoutOptionU5BU5D_t2977405297* ___options4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUILayout::Window(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,System.String,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  Rect_t4241904616  GUILayout_Window_m2359132031 (Il2CppObject * __this /* static, unused */, int32_t ___id0, Rect_t4241904616  ___screenRect1, WindowFunction_t2749288659 * ___func2, String_t* ___text3, GUIStyle_t2990928826 * ___style4, GUILayoutOptionU5BU5D_t2977405297* ___options5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUILayout::Window(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,UnityEngine.Texture,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  Rect_t4241904616  GUILayout_Window_m1560221395 (Il2CppObject * __this /* static, unused */, int32_t ___id0, Rect_t4241904616  ___screenRect1, WindowFunction_t2749288659 * ___func2, Texture_t2526458961 * ___image3, GUIStyle_t2990928826 * ___style4, GUILayoutOptionU5BU5D_t2977405297* ___options5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUILayout::Window(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  Rect_t4241904616  GUILayout_Window_m745278996 (Il2CppObject * __this /* static, unused */, int32_t ___id0, Rect_t4241904616  ___screenRect1, WindowFunction_t2749288659 * ___func2, GUIContent_t2094828418 * ___content3, GUIStyle_t2990928826 * ___style4, GUILayoutOptionU5BU5D_t2977405297* ___options5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUILayout::DoWindow(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  Rect_t4241904616  GUILayout_DoWindow_m1233349823 (Il2CppObject * __this /* static, unused */, int32_t ___id0, Rect_t4241904616  ___screenRect1, WindowFunction_t2749288659 * ___func2, GUIContent_t2094828418 * ___content3, GUIStyle_t2990928826 * ___style4, GUILayoutOptionU5BU5D_t2977405297* ___options5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::Width(System.Single)
extern "C"  GUILayoutOption_t331591504 * GUILayout_Width_m2259621411 (Il2CppObject * __this /* static, unused */, float ___width0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::MinWidth(System.Single)
extern "C"  GUILayoutOption_t331591504 * GUILayout_MinWidth_m1816706679 (Il2CppObject * __this /* static, unused */, float ___minWidth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::MaxWidth(System.Single)
extern "C"  GUILayoutOption_t331591504 * GUILayout_MaxWidth_m3054796937 (Il2CppObject * __this /* static, unused */, float ___maxWidth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::Height(System.Single)
extern "C"  GUILayoutOption_t331591504 * GUILayout_Height_m3774802884 (Il2CppObject * __this /* static, unused */, float ___height0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::MinHeight(System.Single)
extern "C"  GUILayoutOption_t331591504 * GUILayout_MinHeight_m2929348080 (Il2CppObject * __this /* static, unused */, float ___minHeight0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::MaxHeight(System.Single)
extern "C"  GUILayoutOption_t331591504 * GUILayout_MaxHeight_m2655440414 (Il2CppObject * __this /* static, unused */, float ___maxHeight0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::ExpandWidth(System.Boolean)
extern "C"  GUILayoutOption_t331591504 * GUILayout_ExpandWidth_m1724891333 (Il2CppObject * __this /* static, unused */, bool ___expand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::ExpandHeight(System.Boolean)
extern "C"  GUILayoutOption_t331591504 * GUILayout_ExpandHeight_m2691143288 (Il2CppObject * __this /* static, unused */, bool ___expand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
