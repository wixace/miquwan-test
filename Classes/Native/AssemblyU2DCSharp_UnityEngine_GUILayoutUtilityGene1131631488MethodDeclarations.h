﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUILayoutUtilityGenerated
struct UnityEngine_GUILayoutUtilityGenerated_t1131631488;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t2977405297;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_GUILayoutUtilityGenerated::.ctor()
extern "C"  void UnityEngine_GUILayoutUtilityGenerated__ctor_m415942123 (UnityEngine_GUILayoutUtilityGenerated_t1131631488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUILayoutUtilityGenerated::GUILayoutUtility_GUILayoutUtility1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUILayoutUtilityGenerated_GUILayoutUtility_GUILayoutUtility1_m3478861831 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUILayoutUtilityGenerated::GUILayoutUtility_BeginGroup__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUILayoutUtilityGenerated_GUILayoutUtility_BeginGroup__String_m2731987436 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUILayoutUtilityGenerated::GUILayoutUtility_EndGroup__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUILayoutUtilityGenerated_GUILayoutUtility_EndGroup__String_m16966010 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUILayoutUtilityGenerated::GUILayoutUtility_GetAspectRect__Single__GUIStyle__GUILayoutOption_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUILayoutUtilityGenerated_GUILayoutUtility_GetAspectRect__Single__GUIStyle__GUILayoutOption_Array_m88796745 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUILayoutUtilityGenerated::GUILayoutUtility_GetAspectRect__Single__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUILayoutUtilityGenerated_GUILayoutUtility_GetAspectRect__Single__GUIStyle_m163696685 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUILayoutUtilityGenerated::GUILayoutUtility_GetAspectRect__Single__GUILayoutOption_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUILayoutUtilityGenerated_GUILayoutUtility_GetAspectRect__Single__GUILayoutOption_Array_m2445349951 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUILayoutUtilityGenerated::GUILayoutUtility_GetAspectRect__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUILayoutUtilityGenerated_GUILayoutUtility_GetAspectRect__Single_m3868567287 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUILayoutUtilityGenerated::GUILayoutUtility_GetLastRect(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUILayoutUtilityGenerated_GUILayoutUtility_GetLastRect_m256715757 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUILayoutUtilityGenerated::GUILayoutUtility_GetRect__Single__Single__Single__Single__GUIStyle__GUILayoutOption_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUILayoutUtilityGenerated_GUILayoutUtility_GetRect__Single__Single__Single__Single__GUIStyle__GUILayoutOption_Array_m955564649 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUILayoutUtilityGenerated::GUILayoutUtility_GetRect__Single__Single__Single__Single__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUILayoutUtilityGenerated_GUILayoutUtility_GetRect__Single__Single__Single__Single__GUIStyle_m927735821 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUILayoutUtilityGenerated::GUILayoutUtility_GetRect__Single__Single__Single__Single__GUILayoutOption_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUILayoutUtilityGenerated_GUILayoutUtility_GetRect__Single__Single__Single__Single__GUILayoutOption_Array_m1203736671 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUILayoutUtilityGenerated::GUILayoutUtility_GetRect__Single__Single__GUIStyle__GUILayoutOption_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUILayoutUtilityGenerated_GUILayoutUtility_GetRect__Single__Single__GUIStyle__GUILayoutOption_Array_m4043823353 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUILayoutUtilityGenerated::GUILayoutUtility_GetRect__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUILayoutUtilityGenerated_GUILayoutUtility_GetRect__Single__Single__Single__Single_m434654935 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUILayoutUtilityGenerated::GUILayoutUtility_GetRect__GUIContent__GUIStyle__GUILayoutOption_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUILayoutUtilityGenerated_GUILayoutUtility_GetRect__GUIContent__GUIStyle__GUILayoutOption_Array_m62387211 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUILayoutUtilityGenerated::GUILayoutUtility_GetRect__Single__Single__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUILayoutUtilityGenerated_GUILayoutUtility_GetRect__Single__Single__GUIStyle_m2250976125 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUILayoutUtilityGenerated::GUILayoutUtility_GetRect__Single__Single__GUILayoutOption_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUILayoutUtilityGenerated_GUILayoutUtility_GetRect__Single__Single__GUILayoutOption_Array_m3019021039 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUILayoutUtilityGenerated::GUILayoutUtility_GetRect__GUIContent__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUILayoutUtilityGenerated_GUILayoutUtility_GetRect__GUIContent__GUIStyle_m785620139 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUILayoutUtilityGenerated::GUILayoutUtility_GetRect__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUILayoutUtilityGenerated_GUILayoutUtility_GetRect__Single__Single_m1955194439 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUILayoutUtilityGenerated::__Register()
extern "C"  void UnityEngine_GUILayoutUtilityGenerated___Register_m4140609980 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUILayoutOption[] UnityEngine_GUILayoutUtilityGenerated::<GUILayoutUtility_GetAspectRect__Single__GUIStyle__GUILayoutOption_Array>m__248()
extern "C"  GUILayoutOptionU5BU5D_t2977405297* UnityEngine_GUILayoutUtilityGenerated_U3CGUILayoutUtility_GetAspectRect__Single__GUIStyle__GUILayoutOption_ArrayU3Em__248_m1933191518 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUILayoutOption[] UnityEngine_GUILayoutUtilityGenerated::<GUILayoutUtility_GetAspectRect__Single__GUILayoutOption_Array>m__249()
extern "C"  GUILayoutOptionU5BU5D_t2977405297* UnityEngine_GUILayoutUtilityGenerated_U3CGUILayoutUtility_GetAspectRect__Single__GUILayoutOption_ArrayU3Em__249_m576375017 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUILayoutOption[] UnityEngine_GUILayoutUtilityGenerated::<GUILayoutUtility_GetRect__Single__Single__Single__Single__GUIStyle__GUILayoutOption_Array>m__24A()
extern "C"  GUILayoutOptionU5BU5D_t2977405297* UnityEngine_GUILayoutUtilityGenerated_U3CGUILayoutUtility_GetRect__Single__Single__Single__Single__GUIStyle__GUILayoutOption_ArrayU3Em__24A_m3025533831 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUILayoutOption[] UnityEngine_GUILayoutUtilityGenerated::<GUILayoutUtility_GetRect__Single__Single__Single__Single__GUILayoutOption_Array>m__24B()
extern "C"  GUILayoutOptionU5BU5D_t2977405297* UnityEngine_GUILayoutUtilityGenerated_U3CGUILayoutUtility_GetRect__Single__Single__Single__Single__GUILayoutOption_ArrayU3Em__24B_m2632291090 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUILayoutOption[] UnityEngine_GUILayoutUtilityGenerated::<GUILayoutUtility_GetRect__Single__Single__GUIStyle__GUILayoutOption_Array>m__24C()
extern "C"  GUILayoutOptionU5BU5D_t2977405297* UnityEngine_GUILayoutUtilityGenerated_U3CGUILayoutUtility_GetRect__Single__Single__GUIStyle__GUILayoutOption_ArrayU3Em__24C_m1447386745 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUILayoutOption[] UnityEngine_GUILayoutUtilityGenerated::<GUILayoutUtility_GetRect__GUIContent__GUIStyle__GUILayoutOption_Array>m__24D()
extern "C"  GUILayoutOptionU5BU5D_t2977405297* UnityEngine_GUILayoutUtilityGenerated_U3CGUILayoutUtility_GetRect__GUIContent__GUIStyle__GUILayoutOption_ArrayU3Em__24D_m3075631336 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUILayoutOption[] UnityEngine_GUILayoutUtilityGenerated::<GUILayoutUtility_GetRect__Single__Single__GUILayoutOption_Array>m__24E()
extern "C"  GUILayoutOptionU5BU5D_t2977405297* UnityEngine_GUILayoutUtilityGenerated_U3CGUILayoutUtility_GetRect__Single__Single__GUILayoutOption_ArrayU3Em__24E_m2210104773 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUILayoutUtilityGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_GUILayoutUtilityGenerated_ilo_getObject1_m1013414827 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUILayoutUtilityGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_GUILayoutUtilityGenerated_ilo_setObject2_m928895332 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_GUILayoutUtilityGenerated::ilo_getSingle3(System.Int32)
extern "C"  float UnityEngine_GUILayoutUtilityGenerated_ilo_getSingle3_m3700174982 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_GUILayoutUtilityGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_GUILayoutUtilityGenerated_ilo_getObject4_m4128054109 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUILayoutUtilityGenerated::ilo_getArrayLength5(System.Int32)
extern "C"  int32_t UnityEngine_GUILayoutUtilityGenerated_ilo_getArrayLength5_m3465161377 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUILayoutUtilityGenerated::ilo_getElement6(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_GUILayoutUtilityGenerated_ilo_getElement6_m210137490 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
