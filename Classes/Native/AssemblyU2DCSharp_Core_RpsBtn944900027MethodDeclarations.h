﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Core.RpsBtn
struct RpsBtn_t944900027;

#include "codegen/il2cpp-codegen.h"

// System.Void Core.RpsBtn::.ctor()
extern "C"  void RpsBtn__ctor_m750814933 (RpsBtn_t944900027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Core.RpsBtn::Start()
extern "C"  void RpsBtn_Start_m3992920021 (RpsBtn_t944900027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Core.RpsBtn::<Start>m__35F()
extern "C"  void RpsBtn_U3CStartU3Em__35F_m754321858 (RpsBtn_t944900027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
