﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>
struct List_1_t2606718688;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2626391458.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Entity.Behavior.EBehaviorID>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2118648904_gshared (Enumerator_t2626391458 * __this, List_1_t2606718688 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m2118648904(__this, ___l0, method) ((  void (*) (Enumerator_t2626391458 *, List_1_t2606718688 *, const MethodInfo*))Enumerator__ctor_m2118648904_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Entity.Behavior.EBehaviorID>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2142733066_gshared (Enumerator_t2626391458 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2142733066(__this, method) ((  void (*) (Enumerator_t2626391458 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2142733066_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Entity.Behavior.EBehaviorID>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3565809014_gshared (Enumerator_t2626391458 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3565809014(__this, method) ((  Il2CppObject * (*) (Enumerator_t2626391458 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3565809014_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Entity.Behavior.EBehaviorID>::Dispose()
extern "C"  void Enumerator_Dispose_m3614712877_gshared (Enumerator_t2626391458 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3614712877(__this, method) ((  void (*) (Enumerator_t2626391458 *, const MethodInfo*))Enumerator_Dispose_m3614712877_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Entity.Behavior.EBehaviorID>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1685062886_gshared (Enumerator_t2626391458 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1685062886(__this, method) ((  void (*) (Enumerator_t2626391458 *, const MethodInfo*))Enumerator_VerifyState_m1685062886_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Entity.Behavior.EBehaviorID>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1732968822_gshared (Enumerator_t2626391458 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1732968822(__this, method) ((  bool (*) (Enumerator_t2626391458 *, const MethodInfo*))Enumerator_MoveNext_m1732968822_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Entity.Behavior.EBehaviorID>::get_Current()
extern "C"  uint8_t Enumerator_get_Current_m4294259357_gshared (Enumerator_t2626391458 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4294259357(__this, method) ((  uint8_t (*) (Enumerator_t2626391458 *, const MethodInfo*))Enumerator_get_Current_m4294259357_gshared)(__this, method)
