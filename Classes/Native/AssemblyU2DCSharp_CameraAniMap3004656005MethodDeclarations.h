﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraAniMap
struct CameraAniMap_t3004656005;
// ProtoBuf.IExtension
struct IExtension_t1606339106;
// System.Collections.Generic.List`1<CameraAniGroup>
struct List_1_t2619551544;

#include "codegen/il2cpp-codegen.h"

// System.Void CameraAniMap::.ctor()
extern "C"  void CameraAniMap__ctor_m1746824886 (CameraAniMap_t3004656005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension CameraAniMap::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * CameraAniMap_ProtoBuf_IExtensible_GetExtensionObject_m416304006 (CameraAniMap_t3004656005 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraAniMap::get_id()
extern "C"  int32_t CameraAniMap_get_id_m4136513952 (CameraAniMap_t3004656005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraAniMap::set_id(System.Int32)
extern "C"  void CameraAniMap_set_id_m2705286359 (CameraAniMap_t3004656005 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CameraAniGroup> CameraAniMap::get_cameraAniGroups()
extern "C"  List_1_t2619551544 * CameraAniMap_get_cameraAniGroups_m1639329563 (CameraAniMap_t3004656005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
