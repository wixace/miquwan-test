﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSCheckFinishData
struct CSCheckFinishData_t2410628981;

#include "codegen/il2cpp-codegen.h"

// System.Void CSCheckFinishData::.ctor()
extern "C"  void CSCheckFinishData__ctor_m1089796502 (CSCheckFinishData_t2410628981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
