﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraHelper/<ChangeCameraTarget>c__AnonStorey14E
struct U3CChangeCameraTargetU3Ec__AnonStorey14E_t3032404755;

#include "codegen/il2cpp-codegen.h"

// System.Void CameraHelper/<ChangeCameraTarget>c__AnonStorey14E::.ctor()
extern "C"  void U3CChangeCameraTargetU3Ec__AnonStorey14E__ctor_m2155347896 (U3CChangeCameraTargetU3Ec__AnonStorey14E_t3032404755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper/<ChangeCameraTarget>c__AnonStorey14E::<>m__3BF()
extern "C"  void U3CChangeCameraTargetU3Ec__AnonStorey14E_U3CU3Em__3BF_m3040269176 (U3CChangeCameraTargetU3Ec__AnonStorey14E_t3032404755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
