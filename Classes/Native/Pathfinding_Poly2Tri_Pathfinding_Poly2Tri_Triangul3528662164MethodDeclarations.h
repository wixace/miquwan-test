﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Poly2Tri.TriangulationContext
struct TriangulationContext_t3528662164;
// Pathfinding.Poly2Tri.TriangulationDebugContext
struct TriangulationDebugContext_t3598090913;
// Pathfinding.Poly2Tri.Triangulatable
struct Triangulatable_t923279207;
// System.String
struct String_t;
// Pathfinding.Poly2Tri.DTSweepDebugContext
struct DTSweepDebugContext_t3829537966;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul1170581608.h"
#include "mscorlib_System_String7231557.h"

// System.Void Pathfinding.Poly2Tri.TriangulationContext::.ctor()
extern "C"  void TriangulationContext__ctor_m4137179115 (TriangulationContext_t3528662164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.TriangulationDebugContext Pathfinding.Poly2Tri.TriangulationContext::get_DebugContext()
extern "C"  TriangulationDebugContext_t3598090913 * TriangulationContext_get_DebugContext_m3106753707 (TriangulationContext_t3528662164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.TriangulationMode Pathfinding.Poly2Tri.TriangulationContext::get_TriangulationMode()
extern "C"  int32_t TriangulationContext_get_TriangulationMode_m3320394526 (TriangulationContext_t3528662164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.TriangulationContext::set_TriangulationMode(Pathfinding.Poly2Tri.TriangulationMode)
extern "C"  void TriangulationContext_set_TriangulationMode_m1869986571 (TriangulationContext_t3528662164 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.Triangulatable Pathfinding.Poly2Tri.TriangulationContext::get_Triangulatable()
extern "C"  Il2CppObject * TriangulationContext_get_Triangulatable_m1563965268 (TriangulationContext_t3528662164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.TriangulationContext::set_Triangulatable(Pathfinding.Poly2Tri.Triangulatable)
extern "C"  void TriangulationContext_set_Triangulatable_m1472563927 (TriangulationContext_t3528662164 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Poly2Tri.TriangulationContext::get_StepCount()
extern "C"  int32_t TriangulationContext_get_StepCount_m1428097075 (TriangulationContext_t3528662164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.TriangulationContext::set_StepCount(System.Int32)
extern "C"  void TriangulationContext_set_StepCount_m1610772576 (TriangulationContext_t3528662164 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.TriangulationContext::Done()
extern "C"  void TriangulationContext_Done_m1328379643 (TriangulationContext_t3528662164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.TriangulationContext::PrepareTriangulation(Pathfinding.Poly2Tri.Triangulatable)
extern "C"  void TriangulationContext_PrepareTriangulation_m1937826367 (TriangulationContext_t3528662164 * __this, Il2CppObject * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.TriangulationContext::Update(System.String)
extern "C"  void TriangulationContext_Update_m3089402304 (TriangulationContext_t3528662164 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.TriangulationContext::Clear()
extern "C"  void TriangulationContext_Clear_m1543312406 (TriangulationContext_t3528662164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Poly2Tri.TriangulationContext::get_IsDebugEnabled()
extern "C"  bool TriangulationContext_get_IsDebugEnabled_m1815371940 (TriangulationContext_t3528662164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.DTSweepDebugContext Pathfinding.Poly2Tri.TriangulationContext::get_DTDebugContext()
extern "C"  DTSweepDebugContext_t3829537966 * TriangulationContext_get_DTDebugContext_m2409356104 (TriangulationContext_t3528662164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
