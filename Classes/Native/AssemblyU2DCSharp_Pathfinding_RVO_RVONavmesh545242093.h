﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Pathfinding.RVO.ObstacleVertex>
struct List_1_t1243525355;
// Pathfinding.RVO.Simulator
struct Simulator_t2705969170;

#include "AssemblyU2DCSharp_Pathfinding_GraphModifier2555428519.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RVO.RVONavmesh
struct  RVONavmesh_t545242093  : public GraphModifier_t2555428519
{
public:
	// System.Single Pathfinding.RVO.RVONavmesh::wallHeight
	float ___wallHeight_5;
	// System.Collections.Generic.List`1<Pathfinding.RVO.ObstacleVertex> Pathfinding.RVO.RVONavmesh::obstacles
	List_1_t1243525355 * ___obstacles_6;
	// Pathfinding.RVO.Simulator Pathfinding.RVO.RVONavmesh::lastSim
	Simulator_t2705969170 * ___lastSim_7;

public:
	inline static int32_t get_offset_of_wallHeight_5() { return static_cast<int32_t>(offsetof(RVONavmesh_t545242093, ___wallHeight_5)); }
	inline float get_wallHeight_5() const { return ___wallHeight_5; }
	inline float* get_address_of_wallHeight_5() { return &___wallHeight_5; }
	inline void set_wallHeight_5(float value)
	{
		___wallHeight_5 = value;
	}

	inline static int32_t get_offset_of_obstacles_6() { return static_cast<int32_t>(offsetof(RVONavmesh_t545242093, ___obstacles_6)); }
	inline List_1_t1243525355 * get_obstacles_6() const { return ___obstacles_6; }
	inline List_1_t1243525355 ** get_address_of_obstacles_6() { return &___obstacles_6; }
	inline void set_obstacles_6(List_1_t1243525355 * value)
	{
		___obstacles_6 = value;
		Il2CppCodeGenWriteBarrier(&___obstacles_6, value);
	}

	inline static int32_t get_offset_of_lastSim_7() { return static_cast<int32_t>(offsetof(RVONavmesh_t545242093, ___lastSim_7)); }
	inline Simulator_t2705969170 * get_lastSim_7() const { return ___lastSim_7; }
	inline Simulator_t2705969170 ** get_address_of_lastSim_7() { return &___lastSim_7; }
	inline void set_lastSim_7(Simulator_t2705969170 * value)
	{
		___lastSim_7 = value;
		Il2CppCodeGenWriteBarrier(&___lastSim_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
