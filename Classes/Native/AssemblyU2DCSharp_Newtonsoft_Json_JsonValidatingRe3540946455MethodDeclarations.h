﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonValidatingReader/SchemaScope
struct SchemaScope_t3540946455;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchemaModel>
struct IList_1_t1435298045;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t1297217088;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t3308144514;
// Newtonsoft.Json.Schema.JsonSchemaModel
struct JsonSchemaModel_t3035618138;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType3916897561.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem3035618138.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23754817214.h"

// System.Void Newtonsoft.Json.JsonValidatingReader/SchemaScope::.ctor(Newtonsoft.Json.Linq.JTokenType,System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchemaModel>)
extern "C"  void SchemaScope__ctor_m3154225047 (SchemaScope_t3540946455 * __this, int32_t ___tokenType0, Il2CppObject* ___schemas1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonValidatingReader/SchemaScope::get_CurrentPropertyName()
extern "C"  String_t* SchemaScope_get_CurrentPropertyName_m695327471 (SchemaScope_t3540946455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader/SchemaScope::set_CurrentPropertyName(System.String)
extern "C"  void SchemaScope_set_CurrentPropertyName_m4230540996 (SchemaScope_t3540946455 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.JsonValidatingReader/SchemaScope::get_ArrayItemCount()
extern "C"  int32_t SchemaScope_get_ArrayItemCount_m2854769690 (SchemaScope_t3540946455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader/SchemaScope::set_ArrayItemCount(System.Int32)
extern "C"  void SchemaScope_set_ArrayItemCount_m3581224529 (SchemaScope_t3540946455 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchemaModel> Newtonsoft.Json.JsonValidatingReader/SchemaScope::get_Schemas()
extern "C"  Il2CppObject* SchemaScope_get_Schemas_m2475447121 (SchemaScope_t3540946455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> Newtonsoft.Json.JsonValidatingReader/SchemaScope::get_RequiredProperties()
extern "C"  Dictionary_2_t1297217088 * SchemaScope_get_RequiredProperties_m4015232698 (SchemaScope_t3540946455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.JsonValidatingReader/SchemaScope::get_TokenType()
extern "C"  int32_t SchemaScope_get_TokenType_m2766293275 (SchemaScope_t3540946455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.String> Newtonsoft.Json.JsonValidatingReader/SchemaScope::GetRequiredProperties(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  Il2CppObject* SchemaScope_GetRequiredProperties_m3604444294 (SchemaScope_t3540946455 * __this, JsonSchemaModel_t3035618138 * ___schema0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonValidatingReader/SchemaScope::<SchemaScope>m__371(System.String)
extern "C"  String_t* SchemaScope_U3CSchemaScopeU3Em__371_m197142396 (Il2CppObject * __this /* static, unused */, String_t* ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonValidatingReader/SchemaScope::<SchemaScope>m__372(System.String)
extern "C"  bool SchemaScope_U3CSchemaScopeU3Em__372_m341196652 (Il2CppObject * __this /* static, unused */, String_t* ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonValidatingReader/SchemaScope::<GetRequiredProperties>m__373(System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaModel>)
extern "C"  bool SchemaScope_U3CGetRequiredPropertiesU3Em__373_m2510430917 (Il2CppObject * __this /* static, unused */, KeyValuePair_2_t3754817214  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonValidatingReader/SchemaScope::<GetRequiredProperties>m__374(System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaModel>)
extern "C"  String_t* SchemaScope_U3CGetRequiredPropertiesU3Em__374_m813109331 (Il2CppObject * __this /* static, unused */, KeyValuePair_2_t3754817214  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
