﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._2ac4c422be9130c12827d32c6e6826e5
struct _2ac4c422be9130c12827d32c6e6826e5_t1262242432;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._2ac4c422be9130c12827d32c6e6826e5::.ctor()
extern "C"  void _2ac4c422be9130c12827d32c6e6826e5__ctor_m3010636717 (_2ac4c422be9130c12827d32c6e6826e5_t1262242432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._2ac4c422be9130c12827d32c6e6826e5::_2ac4c422be9130c12827d32c6e6826e5m2(System.Int32)
extern "C"  int32_t _2ac4c422be9130c12827d32c6e6826e5__2ac4c422be9130c12827d32c6e6826e5m2_m4144431513 (_2ac4c422be9130c12827d32c6e6826e5_t1262242432 * __this, int32_t ____2ac4c422be9130c12827d32c6e6826e5a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._2ac4c422be9130c12827d32c6e6826e5::_2ac4c422be9130c12827d32c6e6826e5m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _2ac4c422be9130c12827d32c6e6826e5__2ac4c422be9130c12827d32c6e6826e5m_m1728816445 (_2ac4c422be9130c12827d32c6e6826e5_t1262242432 * __this, int32_t ____2ac4c422be9130c12827d32c6e6826e5a0, int32_t ____2ac4c422be9130c12827d32c6e6826e5541, int32_t ____2ac4c422be9130c12827d32c6e6826e5c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
