﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSHeroUnit
struct CSHeroUnit_t3764358446;
// ReplayHeroUnit
struct ReplayHeroUnit_t605237701;
// herosCfg
struct herosCfg_t3676934635;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// System.String
struct String_t;
// System.UInt32[]
struct UInt32U5BU5D_t3230734560;
// AntiCheatMgr
struct AntiCheatMgr_t3474304487;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ReplayHeroUnit605237701.h"
#include "AssemblyU2DCSharp_herosCfg3676934635.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "AssemblyU2DCSharp_SEX_TYPE2342271891.h"
#include "AssemblyU2DCSharp_HERO_COUNTRY1018279985.h"
#include "AssemblyU2DCSharp_HERO_TYPE820012127.h"
#include "AssemblyU2DCSharp_HERO_ELEMENT2692579223.h"
#include "AssemblyU2DCSharp_AntiCheatMgr3474304487.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CSHeroUnit3764358446.h"
#include "AssemblyU2DCSharp_CSDatacfgManager1565254243.h"

// System.Void CSHeroUnit::.ctor(ReplayHeroUnit)
extern "C"  void CSHeroUnit__ctor_m2388881160 (CSHeroUnit_t3764358446 * __this, ReplayHeroUnit_t605237701 * ___rhu0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::.ctor(System.UInt32,herosCfg)
extern "C"  void CSHeroUnit__ctor_m2647504558 (CSHeroUnit_t3764358446 * __this, uint32_t ___id0, herosCfg_t3676934635 * ___cfg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::.ctor(Newtonsoft.Json.Linq.JToken)
extern "C"  void CSHeroUnit__ctor_m1975446237 (CSHeroUnit_t3764358446 * __this, JToken_t3412245951 * ___jToken0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_id(System.UInt32)
extern "C"  void CSHeroUnit_set_id_m1359914603 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_id()
extern "C"  uint32_t CSHeroUnit_get_id_m416226582 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_lv(System.UInt32)
extern "C"  void CSHeroUnit_set_lv_m525195804 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_lv()
extern "C"  uint32_t CSHeroUnit_get_lv_m416333253 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_exp(System.UInt32)
extern "C"  void CSHeroUnit_set_exp_m1769537583 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_exp()
extern "C"  uint32_t CSHeroUnit_get_exp_m15093092 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_starLevel(System.UInt32)
extern "C"  void CSHeroUnit_set_starLevel_m346007322 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_starLevel()
extern "C"  uint32_t CSHeroUnit_get_starLevel_m3206157785 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_soulLevel(System.UInt32)
extern "C"  void CSHeroUnit_set_soulLevel_m3632561211 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_soulLevel()
extern "C"  uint32_t CSHeroUnit_get_soulLevel_m532447000 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_soulValue(System.UInt32)
extern "C"  void CSHeroUnit_set_soulValue_m3759885998 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_soulValue()
extern "C"  uint32_t CSHeroUnit_get_soulValue_m694267333 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_power(System.UInt32)
extern "C"  void CSHeroUnit_set_power_m178654087 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_power()
extern "C"  uint32_t CSHeroUnit_get_power_m2542856396 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_location(System.UInt32)
extern "C"  void CSHeroUnit_set_location_m274390737 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_location()
extern "C"  uint32_t CSHeroUnit_get_location_m755744176 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_awakenLv(System.Int32)
extern "C"  void CSHeroUnit_set_awakenLv_m2577895256 (CSHeroUnit_t3764358446 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnit::get_awakenLv()
extern "C"  int32_t CSHeroUnit_get_awakenLv_m3830023585 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_attack(System.UInt32)
extern "C"  void CSHeroUnit_set_attack_m3555669790 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_attack()
extern "C"  uint32_t CSHeroUnit_get_attack_m1199372803 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_phy_def(System.UInt32)
extern "C"  void CSHeroUnit_set_phy_def_m1309643589 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_phy_def()
extern "C"  uint32_t CSHeroUnit_get_phy_def_m2126014990 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_mag_def(System.UInt32)
extern "C"  void CSHeroUnit_set_mag_def_m418290227 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_mag_def()
extern "C"  uint32_t CSHeroUnit_get_mag_def_m848476256 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_hp(System.UInt32)
extern "C"  void CSHeroUnit_set_hp_m2470129374 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_hp()
extern "C"  uint32_t CSHeroUnit_get_hp_m416208323 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_residual_hp(System.Int32)
extern "C"  void CSHeroUnit_set_residual_hp_m3432814433 (CSHeroUnit_t3764358446 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnit::get_residual_hp()
extern "C"  int32_t CSHeroUnit_get_residual_hp_m1869124278 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_residual_mp(System.Int32)
extern "C"  void CSHeroUnit_set_residual_mp_m880143548 (CSHeroUnit_t3764358446 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnit::get_residual_mp()
extern "C"  int32_t CSHeroUnit_get_residual_mp_m1869273233 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_residual_hpPer(System.Int32)
extern "C"  void CSHeroUnit_set_residual_hpPer_m1586914848 (CSHeroUnit_t3764358446 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnit::get_residual_hpPer()
extern "C"  int32_t CSHeroUnit_get_residual_hpPer_m3164179689 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_residual_mpPer(System.Int32)
extern "C"  void CSHeroUnit_set_residual_mpPer_m1659522789 (CSHeroUnit_t3764358446 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnit::get_residual_mpPer()
extern "C"  int32_t CSHeroUnit_get_residual_mpPer_m3306730798 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_hitrate(System.UInt32)
extern "C"  void CSHeroUnit_set_hitrate_m1856193305 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_hitrate()
extern "C"  uint32_t CSHeroUnit_get_hitrate_m1547669946 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_dodge(System.UInt32)
extern "C"  void CSHeroUnit_set_dodge_m3307712117 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_dodge()
extern "C"  uint32_t CSHeroUnit_get_dodge_m465247006 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_destroy(System.UInt32)
extern "C"  void CSHeroUnit_set_destroy_m4020204338 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_destroy()
extern "C"  uint32_t CSHeroUnit_get_destroy_m991571201 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_block(System.UInt32)
extern "C"  void CSHeroUnit_set_block_m99438335 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_block()
extern "C"  uint32_t CSHeroUnit_get_block_m2909364820 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_crit(System.UInt32)
extern "C"  void CSHeroUnit_set_crit_m1044276364 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_crit()
extern "C"  uint32_t CSHeroUnit_get_crit_m404950933 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_anticrit(System.UInt32)
extern "C"  void CSHeroUnit_set_anticrit_m321586026 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_anticrit()
extern "C"  uint32_t CSHeroUnit_get_anticrit_m2319389815 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_crit_hurt(System.UInt32)
extern "C"  void CSHeroUnit_set_crit_hurt_m136488920 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_crit_hurt()
extern "C"  uint32_t CSHeroUnit_get_crit_hurt_m3964581979 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_damage_reflect(System.UInt32)
extern "C"  void CSHeroUnit_set_damage_reflect_m704282585 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_damage_reflect()
extern "C"  uint32_t CSHeroUnit_get_damage_reflect_m2606597736 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_heal_bonus(System.UInt32)
extern "C"  void CSHeroUnit_set_heal_bonus_m1079548894 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_heal_bonus()
extern "C"  uint32_t CSHeroUnit_get_heal_bonus_m2820354499 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_damage_bonus(System.UInt32)
extern "C"  void CSHeroUnit_set_damage_bonus_m2213742967 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_damage_bonus()
extern "C"  uint32_t CSHeroUnit_get_damage_bonus_m518943178 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_damage_reduce(System.UInt32)
extern "C"  void CSHeroUnit_set_damage_reduce_m1356197174 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_damage_reduce()
extern "C"  uint32_t CSHeroUnit_get_damage_reduce_m450719165 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_real_damage(System.UInt32)
extern "C"  void CSHeroUnit_set_real_damage_m117071292 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_real_damage()
extern "C"  uint32_t CSHeroUnit_get_real_damage_m2495686071 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_firm(System.UInt32)
extern "C"  void CSHeroUnit_set_firm_m1763201960 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_firm()
extern "C"  uint32_t CSHeroUnit_get_firm_m482788089 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_pure(System.UInt32)
extern "C"  void CSHeroUnit_set_pure_m2617757614 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_pure()
extern "C"  uint32_t CSHeroUnit_get_pure_m780154163 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_stubborn(System.UInt32)
extern "C"  void CSHeroUnit_set_stubborn_m2586039631 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_stubborn()
extern "C"  uint32_t CSHeroUnit_get_stubborn_m616436338 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_hitrate_per(System.UInt32)
extern "C"  void CSHeroUnit_set_hitrate_per_m66525947 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_hitrate_per()
extern "C"  uint32_t CSHeroUnit_get_hitrate_per_m2352926232 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_dodge_per(System.UInt32)
extern "C"  void CSHeroUnit_set_dodge_per_m1633581143 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_dodge_per()
extern "C"  uint32_t CSHeroUnit_get_dodge_per_m2790196604 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_destory_per(System.UInt32)
extern "C"  void CSHeroUnit_set_destory_per_m3740616506 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_destory_per()
extern "C"  uint32_t CSHeroUnit_get_destory_per_m1964156793 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_block_per(System.UInt32)
extern "C"  void CSHeroUnit_set_block_per_m2381104097 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_block_per()
extern "C"  uint32_t CSHeroUnit_get_block_per_m2625290674 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_crit_per(System.UInt32)
extern "C"  void CSHeroUnit_set_crit_per_m1701726958 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_crit_per()
extern "C"  uint32_t CSHeroUnit_get_crit_per_m2351556211 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_anticrit_per(System.UInt32)
extern "C"  void CSHeroUnit_set_anticrit_per_m2736016076 (CSHeroUnit_t3764358446 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::get_anticrit_per()
extern "C"  uint32_t CSHeroUnit_get_anticrit_per_m3574901333 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_anger_initial(System.Int32)
extern "C"  void CSHeroUnit_set_anger_initial_m2651592171 (CSHeroUnit_t3764358446 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnit::get_anger_initial()
extern "C"  int32_t CSHeroUnit_get_anger_initial_m2703654592 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_anger_max(System.Int32)
extern "C"  void CSHeroUnit_set_anger_max_m1367269003 (CSHeroUnit_t3764358446 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnit::get_anger_max()
extern "C"  int32_t CSHeroUnit_get_anger_max_m2961619552 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_anger_growth(System.Int32)
extern "C"  void CSHeroUnit_set_anger_growth_m2265777892 (CSHeroUnit_t3764358446 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnit::get_anger_growth()
extern "C"  int32_t CSHeroUnit_get_anger_growth_m2543243821 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_beHurtAnger(System.Int32)
extern "C"  void CSHeroUnit_set_beHurtAnger_m2456491668 (CSHeroUnit_t3764358446 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnit::get_beHurtAnger()
extern "C"  int32_t CSHeroUnit_get_beHurtAnger_m321783401 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CSHeroUnit::get_name()
extern "C"  String_t* CSHeroUnit_get_name_m3201884174 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnit::get_unAdd()
extern "C"  int32_t CSHeroUnit_get_unAdd_m3345941244 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CSHeroUnit::get_ResourcesName()
extern "C"  String_t* CSHeroUnit_get_ResourcesName_m3166350895 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SEX_TYPE CSHeroUnit::get_sexType()
extern "C"  int32_t CSHeroUnit_get_sexType_m1288709048 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CSHeroUnit::get_Icon()
extern "C"  String_t* CSHeroUnit_get_Icon_m2144520860 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CSHeroUnit::get_HeadIcon()
extern "C"  String_t* CSHeroUnit_get_HeadIcon_m513416092 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnit::get_Intelligence()
extern "C"  int32_t CSHeroUnit_get_Intelligence_m349768717 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnit::get_AttackNature()
extern "C"  int32_t CSHeroUnit_get_AttackNature_m107854429 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CSHeroUnit::get_HalfWidth()
extern "C"  float CSHeroUnit_get_HalfWidth_m4188202793 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnit::get_AttackType()
extern "C"  int32_t CSHeroUnit_get_AttackType_m962628976 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HERO_COUNTRY CSHeroUnit::get_Country()
extern "C"  int32_t CSHeroUnit_get_Country_m4279135884 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HERO_TYPE CSHeroUnit::get_HeroType()
extern "C"  int32_t CSHeroUnit_get_HeroType_m3604242728 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HERO_ELEMENT CSHeroUnit::get_HeroElement()
extern "C"  int32_t CSHeroUnit_get_HeroElement_m1433873406 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CSHeroUnit::get_AtkEffectDelay()
extern "C"  float CSHeroUnit_get_AtkEffectDelay_m4115541702 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnit::get_IsLucency()
extern "C"  int32_t CSHeroUnit_get_IsLucency_m1278141859 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnit::get_ifattacked()
extern "C"  int32_t CSHeroUnit_get_ifattacked_m369460274 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CSHeroUnit::get_LucencyTime()
extern "C"  float CSHeroUnit_get_LucencyTime_m1131682844 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CSHeroUnit::get_AiId()
extern "C"  String_t* CSHeroUnit_get_AiId_m1919887110 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnit::get_NextPiece()
extern "C"  int32_t CSHeroUnit_get_NextPiece_m96598287 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnit::get_PieceCall()
extern "C"  int32_t CSHeroUnit_get_PieceCall_m796881792 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnit::get_PieceChange()
extern "C"  int32_t CSHeroUnit_get_PieceChange_m2905644050 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32[] CSHeroUnit::get_WithEage()
extern "C"  UInt32U5BU5D_t3230734560* CSHeroUnit_get_WithEage_m3543489113 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CSHeroUnit::get_Resume()
extern "C"  String_t* CSHeroUnit_get_Resume_m3998738640 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnit::get_SoundIdleId()
extern "C"  int32_t CSHeroUnit_get_SoundIdleId_m2381913554 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnit::get_SoundWinId()
extern "C"  int32_t CSHeroUnit_get_SoundWinId_m3224851606 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnit::get_SoundAttackId()
extern "C"  int32_t CSHeroUnit_get_SoundAttackId_m778394054 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnit::get_SoundTackId()
extern "C"  int32_t CSHeroUnit_get_SoundTackId_m1458325779 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CSHeroUnit::get_storyID()
extern "C"  String_t* CSHeroUnit_get_storyID_m932082511 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CSHeroUnit::get_Shadow_hover()
extern "C"  float CSHeroUnit_get_Shadow_hover_m3232613737 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CSHeroUnit::get_Shadow_size()
extern "C"  float CSHeroUnit_get_Shadow_size_m3461815574 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CSHeroUnit::get_skills()
extern "C"  String_t* CSHeroUnit_get_skills_m2117372805 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnit::get_effect_leader()
extern "C"  int32_t CSHeroUnit_get_effect_leader_m3440010347 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::set_config(herosCfg)
extern "C"  void CSHeroUnit_set_config_m1586035787 (CSHeroUnit_t3764358446 * __this, herosCfg_t3676934635 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// herosCfg CSHeroUnit::get_config()
extern "C"  herosCfg_t3676934635 * CSHeroUnit_get_config_m812314516 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::Init()
extern "C"  void CSHeroUnit_Init_m2591946119 (CSHeroUnit_t3764358446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CSHeroUnit::ilo_GetOnlyId1()
extern "C"  String_t* CSHeroUnit_ilo_GetOnlyId1_m2386233409 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::ilo_AntiCheating2()
extern "C"  uint32_t CSHeroUnit_ilo_AntiCheating2_m390203806 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::ilo_SetHeroAntiCheatCS3(AntiCheatMgr,System.String,System.String,System.UInt32,System.UInt32)
extern "C"  void CSHeroUnit_ilo_SetHeroAntiCheatCS3_m4123356287 (Il2CppObject * __this /* static, unused */, AntiCheatMgr_t3474304487 * ____this0, String_t* ___uuid1, String_t* ___attrName2, uint32_t ___value3, uint32_t ___acValue4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AntiCheatMgr CSHeroUnit::ilo_get_AntiCheatMgr4()
extern "C"  AntiCheatMgr_t3474304487 * CSHeroUnit_ilo_get_AntiCheatMgr4_m4219507558 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnit::ilo_GetHeroAntiCheatCS5(AntiCheatMgr,System.String,System.String)
extern "C"  uint32_t CSHeroUnit_ilo_GetHeroAntiCheatCS5_m799998674 (Il2CppObject * __this /* static, unused */, AntiCheatMgr_t3474304487 * ____this0, String_t* ___uuid1, String_t* ___attrName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnit::ilo_GetHeroAntiCheatCS6(AntiCheatMgr,System.String,System.String,System.Int32)
extern "C"  int32_t CSHeroUnit_ilo_GetHeroAntiCheatCS6_m2184853079 (Il2CppObject * __this /* static, unused */, AntiCheatMgr_t3474304487 * ____this0, String_t* ___uuid1, String_t* ___attrName2, int32_t ___defaultValue3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// herosCfg CSHeroUnit::ilo_get_config7(CSHeroUnit)
extern "C"  herosCfg_t3676934635 * CSHeroUnit_ilo_get_config7_m2258615620 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// herosCfg CSHeroUnit::ilo_GetherosCfg8(CSDatacfgManager,System.Int32)
extern "C"  herosCfg_t3676934635 * CSHeroUnit_ilo_GetherosCfg8_m1380665447 (Il2CppObject * __this /* static, unused */, CSDatacfgManager_t1565254243 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::ilo_set_attack9(CSHeroUnit,System.UInt32)
extern "C"  void CSHeroUnit_ilo_set_attack9_m477739570 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::ilo_set_hitrate10(CSHeroUnit,System.UInt32)
extern "C"  void CSHeroUnit_ilo_set_hitrate10_m392320467 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::ilo_set_damage_reflect11(CSHeroUnit,System.UInt32)
extern "C"  void CSHeroUnit_ilo_set_damage_reflect11_m1540008538 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::ilo_set_damage_bonus12(CSHeroUnit,System.UInt32)
extern "C"  void CSHeroUnit_ilo_set_damage_bonus12_m3075613437 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::ilo_set_damage_reduce13(CSHeroUnit,System.UInt32)
extern "C"  void CSHeroUnit_ilo_set_damage_reduce13_m2243391321 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::ilo_set_stubborn14(CSHeroUnit,System.UInt32)
extern "C"  void CSHeroUnit_ilo_set_stubborn14_m497207591 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::ilo_set_dodge_per15(CSHeroUnit,System.UInt32)
extern "C"  void CSHeroUnit_ilo_set_dodge_per15_m3228615130 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::ilo_set_destory_per16(CSHeroUnit,System.UInt32)
extern "C"  void CSHeroUnit_ilo_set_destory_per16_m2621420888 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::ilo_set_crit_per17(CSHeroUnit,System.UInt32)
extern "C"  void CSHeroUnit_ilo_set_crit_per17_m3430015019 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnit::ilo_set_anger_max18(CSHeroUnit,System.Int32)
extern "C"  void CSHeroUnit_ilo_set_anger_max18_m600965879 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
