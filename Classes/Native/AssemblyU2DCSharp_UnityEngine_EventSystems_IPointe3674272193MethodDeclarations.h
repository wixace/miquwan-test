﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_IPointerUpHandlerGenerated
struct UnityEngine_EventSystems_IPointerUpHandlerGenerated_t3674272193;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_EventSystems_IPointerUpHandlerGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_IPointerUpHandlerGenerated__ctor_m1087695562 (UnityEngine_EventSystems_IPointerUpHandlerGenerated_t3674272193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_IPointerUpHandlerGenerated::IPointerUpHandler_OnPointerUp__PointerEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_IPointerUpHandlerGenerated_IPointerUpHandler_OnPointerUp__PointerEventData_m2937046024 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_IPointerUpHandlerGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_IPointerUpHandlerGenerated___Register_m805421117 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
