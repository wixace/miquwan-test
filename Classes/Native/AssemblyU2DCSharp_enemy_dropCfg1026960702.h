﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// enemy_dropCfg
struct  enemy_dropCfg_t1026960702  : public CsCfgBase_t69924517
{
public:
	// System.Int32 enemy_dropCfg::id
	int32_t ___id_0;
	// System.Int32 enemy_dropCfg::type
	int32_t ___type_1;
	// System.Int32 enemy_dropCfg::effect3D
	int32_t ___effect3D_2;
	// System.Int32 enemy_dropCfg::effect2D
	int32_t ___effect2D_3;
	// System.Single enemy_dropCfg::moveSpeed
	float ___moveSpeed_4;
	// System.Single enemy_dropCfg::waitTimeMove
	float ___waitTimeMove_5;
	// System.Single enemy_dropCfg::waitTimeShow
	float ___waitTimeShow_6;
	// System.Int32 enemy_dropCfg::maxShowNum
	int32_t ___maxShowNum_7;
	// System.Single enemy_dropCfg::dropDistance
	float ___dropDistance_8;
	// System.Int32 enemy_dropCfg::hpBossPer
	int32_t ___hpBossPer_9;
	// System.Int32 enemy_dropCfg::hpElitePer
	int32_t ___hpElitePer_10;
	// System.Int32 enemy_dropCfg::hpNormalPer
	int32_t ___hpNormalPer_11;
	// System.Int32 enemy_dropCfg::addBuff
	int32_t ___addBuff_12;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(enemy_dropCfg_t1026960702, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(enemy_dropCfg_t1026960702, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_effect3D_2() { return static_cast<int32_t>(offsetof(enemy_dropCfg_t1026960702, ___effect3D_2)); }
	inline int32_t get_effect3D_2() const { return ___effect3D_2; }
	inline int32_t* get_address_of_effect3D_2() { return &___effect3D_2; }
	inline void set_effect3D_2(int32_t value)
	{
		___effect3D_2 = value;
	}

	inline static int32_t get_offset_of_effect2D_3() { return static_cast<int32_t>(offsetof(enemy_dropCfg_t1026960702, ___effect2D_3)); }
	inline int32_t get_effect2D_3() const { return ___effect2D_3; }
	inline int32_t* get_address_of_effect2D_3() { return &___effect2D_3; }
	inline void set_effect2D_3(int32_t value)
	{
		___effect2D_3 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_4() { return static_cast<int32_t>(offsetof(enemy_dropCfg_t1026960702, ___moveSpeed_4)); }
	inline float get_moveSpeed_4() const { return ___moveSpeed_4; }
	inline float* get_address_of_moveSpeed_4() { return &___moveSpeed_4; }
	inline void set_moveSpeed_4(float value)
	{
		___moveSpeed_4 = value;
	}

	inline static int32_t get_offset_of_waitTimeMove_5() { return static_cast<int32_t>(offsetof(enemy_dropCfg_t1026960702, ___waitTimeMove_5)); }
	inline float get_waitTimeMove_5() const { return ___waitTimeMove_5; }
	inline float* get_address_of_waitTimeMove_5() { return &___waitTimeMove_5; }
	inline void set_waitTimeMove_5(float value)
	{
		___waitTimeMove_5 = value;
	}

	inline static int32_t get_offset_of_waitTimeShow_6() { return static_cast<int32_t>(offsetof(enemy_dropCfg_t1026960702, ___waitTimeShow_6)); }
	inline float get_waitTimeShow_6() const { return ___waitTimeShow_6; }
	inline float* get_address_of_waitTimeShow_6() { return &___waitTimeShow_6; }
	inline void set_waitTimeShow_6(float value)
	{
		___waitTimeShow_6 = value;
	}

	inline static int32_t get_offset_of_maxShowNum_7() { return static_cast<int32_t>(offsetof(enemy_dropCfg_t1026960702, ___maxShowNum_7)); }
	inline int32_t get_maxShowNum_7() const { return ___maxShowNum_7; }
	inline int32_t* get_address_of_maxShowNum_7() { return &___maxShowNum_7; }
	inline void set_maxShowNum_7(int32_t value)
	{
		___maxShowNum_7 = value;
	}

	inline static int32_t get_offset_of_dropDistance_8() { return static_cast<int32_t>(offsetof(enemy_dropCfg_t1026960702, ___dropDistance_8)); }
	inline float get_dropDistance_8() const { return ___dropDistance_8; }
	inline float* get_address_of_dropDistance_8() { return &___dropDistance_8; }
	inline void set_dropDistance_8(float value)
	{
		___dropDistance_8 = value;
	}

	inline static int32_t get_offset_of_hpBossPer_9() { return static_cast<int32_t>(offsetof(enemy_dropCfg_t1026960702, ___hpBossPer_9)); }
	inline int32_t get_hpBossPer_9() const { return ___hpBossPer_9; }
	inline int32_t* get_address_of_hpBossPer_9() { return &___hpBossPer_9; }
	inline void set_hpBossPer_9(int32_t value)
	{
		___hpBossPer_9 = value;
	}

	inline static int32_t get_offset_of_hpElitePer_10() { return static_cast<int32_t>(offsetof(enemy_dropCfg_t1026960702, ___hpElitePer_10)); }
	inline int32_t get_hpElitePer_10() const { return ___hpElitePer_10; }
	inline int32_t* get_address_of_hpElitePer_10() { return &___hpElitePer_10; }
	inline void set_hpElitePer_10(int32_t value)
	{
		___hpElitePer_10 = value;
	}

	inline static int32_t get_offset_of_hpNormalPer_11() { return static_cast<int32_t>(offsetof(enemy_dropCfg_t1026960702, ___hpNormalPer_11)); }
	inline int32_t get_hpNormalPer_11() const { return ___hpNormalPer_11; }
	inline int32_t* get_address_of_hpNormalPer_11() { return &___hpNormalPer_11; }
	inline void set_hpNormalPer_11(int32_t value)
	{
		___hpNormalPer_11 = value;
	}

	inline static int32_t get_offset_of_addBuff_12() { return static_cast<int32_t>(offsetof(enemy_dropCfg_t1026960702, ___addBuff_12)); }
	inline int32_t get_addBuff_12() const { return ___addBuff_12; }
	inline int32_t* get_address_of_addBuff_12() { return &___addBuff_12; }
	inline void set_addBuff_12(int32_t value)
	{
		___addBuff_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
