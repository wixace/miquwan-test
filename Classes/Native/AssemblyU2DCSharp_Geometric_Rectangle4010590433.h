﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Geometric/Rectangle
struct  Rectangle_t4010590433 
{
public:
	// System.Single Geometric/Rectangle::x0
	float ___x0_0;
	// System.Single Geometric/Rectangle::y0
	float ___y0_1;
	// System.Single Geometric/Rectangle::x1
	float ___x1_2;
	// System.Single Geometric/Rectangle::y1
	float ___y1_3;
	// System.Single Geometric/Rectangle::x2
	float ___x2_4;
	// System.Single Geometric/Rectangle::y2
	float ___y2_5;
	// System.Single Geometric/Rectangle::x3
	float ___x3_6;
	// System.Single Geometric/Rectangle::y3
	float ___y3_7;

public:
	inline static int32_t get_offset_of_x0_0() { return static_cast<int32_t>(offsetof(Rectangle_t4010590433, ___x0_0)); }
	inline float get_x0_0() const { return ___x0_0; }
	inline float* get_address_of_x0_0() { return &___x0_0; }
	inline void set_x0_0(float value)
	{
		___x0_0 = value;
	}

	inline static int32_t get_offset_of_y0_1() { return static_cast<int32_t>(offsetof(Rectangle_t4010590433, ___y0_1)); }
	inline float get_y0_1() const { return ___y0_1; }
	inline float* get_address_of_y0_1() { return &___y0_1; }
	inline void set_y0_1(float value)
	{
		___y0_1 = value;
	}

	inline static int32_t get_offset_of_x1_2() { return static_cast<int32_t>(offsetof(Rectangle_t4010590433, ___x1_2)); }
	inline float get_x1_2() const { return ___x1_2; }
	inline float* get_address_of_x1_2() { return &___x1_2; }
	inline void set_x1_2(float value)
	{
		___x1_2 = value;
	}

	inline static int32_t get_offset_of_y1_3() { return static_cast<int32_t>(offsetof(Rectangle_t4010590433, ___y1_3)); }
	inline float get_y1_3() const { return ___y1_3; }
	inline float* get_address_of_y1_3() { return &___y1_3; }
	inline void set_y1_3(float value)
	{
		___y1_3 = value;
	}

	inline static int32_t get_offset_of_x2_4() { return static_cast<int32_t>(offsetof(Rectangle_t4010590433, ___x2_4)); }
	inline float get_x2_4() const { return ___x2_4; }
	inline float* get_address_of_x2_4() { return &___x2_4; }
	inline void set_x2_4(float value)
	{
		___x2_4 = value;
	}

	inline static int32_t get_offset_of_y2_5() { return static_cast<int32_t>(offsetof(Rectangle_t4010590433, ___y2_5)); }
	inline float get_y2_5() const { return ___y2_5; }
	inline float* get_address_of_y2_5() { return &___y2_5; }
	inline void set_y2_5(float value)
	{
		___y2_5 = value;
	}

	inline static int32_t get_offset_of_x3_6() { return static_cast<int32_t>(offsetof(Rectangle_t4010590433, ___x3_6)); }
	inline float get_x3_6() const { return ___x3_6; }
	inline float* get_address_of_x3_6() { return &___x3_6; }
	inline void set_x3_6(float value)
	{
		___x3_6 = value;
	}

	inline static int32_t get_offset_of_y3_7() { return static_cast<int32_t>(offsetof(Rectangle_t4010590433, ___y3_7)); }
	inline float get_y3_7() const { return ___y3_7; }
	inline float* get_address_of_y3_7() { return &___y3_7; }
	inline void set_y3_7(float value)
	{
		___y3_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Geometric/Rectangle
struct Rectangle_t4010590433_marshaled_pinvoke
{
	float ___x0_0;
	float ___y0_1;
	float ___x1_2;
	float ___y1_3;
	float ___x2_4;
	float ___y2_5;
	float ___x3_6;
	float ___y3_7;
};
// Native definition for marshalling of: Geometric/Rectangle
struct Rectangle_t4010590433_marshaled_com
{
	float ___x0_0;
	float ___y0_1;
	float ___x1_2;
	float ___y1_3;
	float ___x2_4;
	float ___y2_5;
	float ___x3_6;
	float ___y3_7;
};
