﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtilGenerated/<BehaviourUtil_DelayCall_GetDelegate_member4_arg1>c__AnonStorey49`1<System.Object>
struct U3CBehaviourUtil_DelayCall_GetDelegate_member4_arg1U3Ec__AnonStorey49_1_t2731694086;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void BehaviourUtilGenerated/<BehaviourUtil_DelayCall_GetDelegate_member4_arg1>c__AnonStorey49`1<System.Object>::.ctor()
extern "C"  void U3CBehaviourUtil_DelayCall_GetDelegate_member4_arg1U3Ec__AnonStorey49_1__ctor_m903757901_gshared (U3CBehaviourUtil_DelayCall_GetDelegate_member4_arg1U3Ec__AnonStorey49_1_t2731694086 * __this, const MethodInfo* method);
#define U3CBehaviourUtil_DelayCall_GetDelegate_member4_arg1U3Ec__AnonStorey49_1__ctor_m903757901(__this, method) ((  void (*) (U3CBehaviourUtil_DelayCall_GetDelegate_member4_arg1U3Ec__AnonStorey49_1_t2731694086 *, const MethodInfo*))U3CBehaviourUtil_DelayCall_GetDelegate_member4_arg1U3Ec__AnonStorey49_1__ctor_m903757901_gshared)(__this, method)
// System.Void BehaviourUtilGenerated/<BehaviourUtil_DelayCall_GetDelegate_member4_arg1>c__AnonStorey49`1<System.Object>::<>m__8(T1)
extern "C"  void U3CBehaviourUtil_DelayCall_GetDelegate_member4_arg1U3Ec__AnonStorey49_1_U3CU3Em__8_m1537681479_gshared (U3CBehaviourUtil_DelayCall_GetDelegate_member4_arg1U3Ec__AnonStorey49_1_t2731694086 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CBehaviourUtil_DelayCall_GetDelegate_member4_arg1U3Ec__AnonStorey49_1_U3CU3Em__8_m1537681479(__this, ___obj0, method) ((  void (*) (U3CBehaviourUtil_DelayCall_GetDelegate_member4_arg1U3Ec__AnonStorey49_1_t2731694086 *, Il2CppObject *, const MethodInfo*))U3CBehaviourUtil_DelayCall_GetDelegate_member4_arg1U3Ec__AnonStorey49_1_U3CU3Em__8_m1537681479_gshared)(__this, ___obj0, method)
