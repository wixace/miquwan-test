﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Client/PushCallBack
struct PushCallBack_t4203546979;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void Client/PushCallBack::.ctor(System.Object,System.IntPtr)
extern "C"  void PushCallBack__ctor_m2832241082 (PushCallBack_t4203546979 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Client/PushCallBack::Invoke(System.String,System.String)
extern "C"  void PushCallBack_Invoke_m3341380778 (PushCallBack_t4203546979 * __this, String_t* ___route0, String_t* ___jsonString1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Client/PushCallBack::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PushCallBack_BeginInvoke_m222921855 (PushCallBack_t4203546979 * __this, String_t* ___route0, String_t* ___jsonString1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Client/PushCallBack::EndInvoke(System.IAsyncResult)
extern "C"  void PushCallBack_EndInvoke_m1750462666 (PushCallBack_t4203546979 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
