﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HatredCtrl/stValue
struct stValue_t3425945410;
struct stValue_t3425945410_marshaled_pinvoke;
struct stValue_t3425945410_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct stValue_t3425945410;
struct stValue_t3425945410_marshaled_pinvoke;

extern "C" void stValue_t3425945410_marshal_pinvoke(const stValue_t3425945410& unmarshaled, stValue_t3425945410_marshaled_pinvoke& marshaled);
extern "C" void stValue_t3425945410_marshal_pinvoke_back(const stValue_t3425945410_marshaled_pinvoke& marshaled, stValue_t3425945410& unmarshaled);
extern "C" void stValue_t3425945410_marshal_pinvoke_cleanup(stValue_t3425945410_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct stValue_t3425945410;
struct stValue_t3425945410_marshaled_com;

extern "C" void stValue_t3425945410_marshal_com(const stValue_t3425945410& unmarshaled, stValue_t3425945410_marshaled_com& marshaled);
extern "C" void stValue_t3425945410_marshal_com_back(const stValue_t3425945410_marshaled_com& marshaled, stValue_t3425945410& unmarshaled);
extern "C" void stValue_t3425945410_marshal_com_cleanup(stValue_t3425945410_marshaled_com& marshaled);
