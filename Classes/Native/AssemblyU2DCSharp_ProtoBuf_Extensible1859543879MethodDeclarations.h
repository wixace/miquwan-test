﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Extensible
struct Extensible_t1859543879;
// ProtoBuf.IExtension
struct IExtension_t1606339106;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Type
struct Type_t;
// ProtoBuf.IExtensible
struct IExtensible_t1056931882;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerable
struct IEnumerable_t3464557803;
// ProtoBuf.Meta.RuntimeTypeModel
struct RuntimeTypeModel_t242172789;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_ProtoBuf_DataFormat274207885.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void ProtoBuf.Extensible::.ctor()
extern "C"  void Extensible__ctor_m3392377805 (Extensible_t1859543879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension ProtoBuf.Extensible::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * Extensible_ProtoBuf_IExtensible_GetExtensionObject_m642609745 (Extensible_t1859543879 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension ProtoBuf.Extensible::GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * Extensible_GetExtensionObject_m908904444 (Extensible_t1859543879 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension ProtoBuf.Extensible::GetExtensionObject(ProtoBuf.IExtension&,System.Boolean)
extern "C"  Il2CppObject * Extensible_GetExtensionObject_m1460384933 (Il2CppObject * __this /* static, unused */, Il2CppObject ** ___extensionObject0, bool ___createIfMissing1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Extensible::TryGetValue(ProtoBuf.Meta.TypeModel,System.Type,ProtoBuf.IExtensible,System.Int32,ProtoBuf.DataFormat,System.Boolean,System.Object&)
extern "C"  bool Extensible_TryGetValue_m1721831426 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, Type_t * ___type1, Il2CppObject * ___instance2, int32_t ___tag3, int32_t ___format4, bool ___allowDefinedTag5, Il2CppObject ** ___value6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerable ProtoBuf.Extensible::GetValues(ProtoBuf.Meta.TypeModel,System.Type,ProtoBuf.IExtensible,System.Int32,ProtoBuf.DataFormat)
extern "C"  Il2CppObject * Extensible_GetValues_m3520859599 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, Type_t * ___type1, Il2CppObject * ___instance2, int32_t ___tag3, int32_t ___format4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Extensible::AppendValue(ProtoBuf.Meta.TypeModel,ProtoBuf.IExtensible,System.Int32,ProtoBuf.DataFormat,System.Object)
extern "C"  void Extensible_AppendValue_m260945263 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, Il2CppObject * ___instance1, int32_t ___tag2, int32_t ___format3, Il2CppObject * ___value4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension ProtoBuf.Extensible::ilo_GetExtensionObject1(ProtoBuf.IExtension&,System.Boolean)
extern "C"  Il2CppObject * Extensible_ilo_GetExtensionObject1_m3673047887 (Il2CppObject * __this /* static, unused */, Il2CppObject ** ___extensionObject0, bool ___createIfMissing1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.RuntimeTypeModel ProtoBuf.Extensible::ilo_get_Default2()
extern "C"  RuntimeTypeModel_t242172789 * Extensible_ilo_get_Default2_m1204142404 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Extensible::ilo_AppendExtendValue3(ProtoBuf.Meta.TypeModel,ProtoBuf.IExtensible,System.Int32,ProtoBuf.DataFormat,System.Object)
extern "C"  void Extensible_ilo_AppendExtendValue3_m896109681 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, Il2CppObject * ___instance1, int32_t ___tag2, int32_t ___format3, Il2CppObject * ___value4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
