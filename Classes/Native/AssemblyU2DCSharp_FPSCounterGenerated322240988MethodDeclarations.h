﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FPSCounterGenerated
struct FPSCounterGenerated_t322240988;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void FPSCounterGenerated::.ctor()
extern "C"  void FPSCounterGenerated__ctor_m2467784975 (FPSCounterGenerated_t322240988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FPSCounterGenerated::FPSCounter_FPSCounter1(JSVCall,System.Int32)
extern "C"  bool FPSCounterGenerated_FPSCounter_FPSCounter1_m3398815203 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSCounterGenerated::FPSCounter_fpsInfo_txt(JSVCall)
extern "C"  void FPSCounterGenerated_FPSCounter_fpsInfo_txt_m1933045814 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSCounterGenerated::FPSCounter_updateInterval(JSVCall)
extern "C"  void FPSCounterGenerated_FPSCounter_updateInterval_m3546938496 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSCounterGenerated::FPSCounter_isShow(JSVCall)
extern "C"  void FPSCounterGenerated_FPSCounter_isShow_m3124343303 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FPSCounterGenerated::FPSCounter_Awake(JSVCall,System.Int32)
extern "C"  bool FPSCounterGenerated_FPSCounter_Awake_m4293101930 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FPSCounterGenerated::FPSCounter_OnAwake__GameObject(JSVCall,System.Int32)
extern "C"  bool FPSCounterGenerated_FPSCounter_OnAwake__GameObject_m4100865820 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FPSCounterGenerated::FPSCounter_OnDestroy(JSVCall,System.Int32)
extern "C"  bool FPSCounterGenerated_FPSCounter_OnDestroy_m2576405120 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSCounterGenerated::__Register()
extern "C"  void FPSCounterGenerated___Register_m1868326168 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FPSCounterGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t FPSCounterGenerated_ilo_getObject1_m223068039 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FPSCounterGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t FPSCounterGenerated_ilo_setObject2_m333161792 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FPSCounterGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * FPSCounterGenerated_ilo_getObject3_m786272376 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FPSCounterGenerated::ilo_get_isShow4()
extern "C"  bool FPSCounterGenerated_ilo_get_isShow4_m3175785618 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
