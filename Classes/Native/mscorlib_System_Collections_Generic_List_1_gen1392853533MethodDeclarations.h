﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.UInt32>
struct List_1_t1392853533;
// System.Collections.Generic.IEnumerable`1<System.UInt32>
struct IEnumerable_1_t3325580938;
// System.Collections.Generic.IEnumerator`1<System.UInt32>
struct IEnumerator_1_t1936533030;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<System.UInt32>
struct ICollection_1_t919257968;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>
struct ReadOnlyCollection_1_t1581745517;
// System.UInt32[]
struct UInt32U5BU5D_t3230734560;
// System.Predicate`1<System.UInt32>
struct Predicate_1_t3930692160;
// System.Collections.Generic.IComparer`1<System.UInt32>
struct IComparer_1_t2599682023;
// System.Comparison`1<System.UInt32>
struct Comparison_1_t3035996464;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1412526303.h"

// System.Void System.Collections.Generic.List`1<System.UInt32>::.ctor()
extern "C"  void List_1__ctor_m1377202647_gshared (List_1_t1392853533 * __this, const MethodInfo* method);
#define List_1__ctor_m1377202647(__this, method) ((  void (*) (List_1_t1392853533 *, const MethodInfo*))List_1__ctor_m1377202647_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m333725009_gshared (List_1_t1392853533 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m333725009(__this, ___collection0, method) ((  void (*) (List_1_t1392853533 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m333725009_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m495787176_gshared (List_1_t1392853533 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m495787176(__this, ___capacity0, method) ((  void (*) (List_1_t1392853533 *, int32_t, const MethodInfo*))List_1__ctor_m495787176_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::.cctor()
extern "C"  void List_1__cctor_m2950259455_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m2950259455(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m2950259455_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.UInt32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3988002880_gshared (List_1_t1392853533 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3988002880(__this, method) ((  Il2CppObject* (*) (List_1_t1392853533 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3988002880_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m798791318_gshared (List_1_t1392853533 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m798791318(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1392853533 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m798791318_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.UInt32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3319039249_gshared (List_1_t1392853533 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3319039249(__this, method) ((  Il2CppObject * (*) (List_1_t1392853533 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3319039249_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt32>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m4243023232_gshared (List_1_t1392853533 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m4243023232(__this, ___item0, method) ((  int32_t (*) (List_1_t1392853533 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m4243023232_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt32>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1103467084_gshared (List_1_t1392853533 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1103467084(__this, ___item0, method) ((  bool (*) (List_1_t1392853533 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1103467084_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt32>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1996020696_gshared (List_1_t1392853533 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1996020696(__this, ___item0, method) ((  int32_t (*) (List_1_t1392853533 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1996020696_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m1773152259_gshared (List_1_t1392853533 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1773152259(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1392853533 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1773152259_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m3121854533_gshared (List_1_t1392853533 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m3121854533(__this, ___item0, method) ((  void (*) (List_1_t1392853533 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3121854533_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3506967373_gshared (List_1_t1392853533 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3506967373(__this, method) ((  bool (*) (List_1_t1392853533 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3506967373_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3998159248_gshared (List_1_t1392853533 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3998159248(__this, method) ((  bool (*) (List_1_t1392853533 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3998159248_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.UInt32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1244001148_gshared (List_1_t1392853533 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1244001148(__this, method) ((  Il2CppObject * (*) (List_1_t1392853533 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1244001148_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt32>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m2664164027_gshared (List_1_t1392853533 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m2664164027(__this, method) ((  bool (*) (List_1_t1392853533 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2664164027_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt32>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m2341064926_gshared (List_1_t1392853533 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m2341064926(__this, method) ((  bool (*) (List_1_t1392853533 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2341064926_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.UInt32>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2549806595_gshared (List_1_t1392853533 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m2549806595(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1392853533 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m2549806595_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3806224154_gshared (List_1_t1392853533 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3806224154(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1392853533 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3806224154_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::Add(T)
extern "C"  void List_1_Add_m1071563463_gshared (List_1_t1392853533 * __this, uint32_t ___item0, const MethodInfo* method);
#define List_1_Add_m1071563463(__this, ___item0, method) ((  void (*) (List_1_t1392853533 *, uint32_t, const MethodInfo*))List_1_Add_m1071563463_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1086136332_gshared (List_1_t1392853533 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1086136332(__this, ___newCount0, method) ((  void (*) (List_1_t1392853533 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1086136332_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m948749403_gshared (List_1_t1392853533 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m948749403(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t1392853533 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m948749403_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m390743818_gshared (List_1_t1392853533 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m390743818(__this, ___collection0, method) ((  void (*) (List_1_t1392853533 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m390743818_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3946683338_gshared (List_1_t1392853533 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3946683338(__this, ___enumerable0, method) ((  void (*) (List_1_t1392853533 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3946683338_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3006422467_gshared (List_1_t1392853533 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3006422467(__this, ___collection0, method) ((  void (*) (List_1_t1392853533 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3006422467_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.UInt32>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1581745517 * List_1_AsReadOnly_m1315654396_gshared (List_1_t1392853533 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1315654396(__this, method) ((  ReadOnlyCollection_1_t1581745517 * (*) (List_1_t1392853533 *, const MethodInfo*))List_1_AsReadOnly_m1315654396_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt32>::BinarySearch(T)
extern "C"  int32_t List_1_BinarySearch_m2962235843_gshared (List_1_t1392853533 * __this, uint32_t ___item0, const MethodInfo* method);
#define List_1_BinarySearch_m2962235843(__this, ___item0, method) ((  int32_t (*) (List_1_t1392853533 *, uint32_t, const MethodInfo*))List_1_BinarySearch_m2962235843_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::Clear()
extern "C"  void List_1_Clear_m3078303234_gshared (List_1_t1392853533 * __this, const MethodInfo* method);
#define List_1_Clear_m3078303234(__this, method) ((  void (*) (List_1_t1392853533 *, const MethodInfo*))List_1_Clear_m3078303234_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt32>::Contains(T)
extern "C"  bool List_1_Contains_m2818227015_gshared (List_1_t1392853533 * __this, uint32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m2818227015(__this, ___item0, method) ((  bool (*) (List_1_t1392853533 *, uint32_t, const MethodInfo*))List_1_Contains_m2818227015_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m485759489_gshared (List_1_t1392853533 * __this, UInt32U5BU5D_t3230734560* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m485759489(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1392853533 *, UInt32U5BU5D_t3230734560*, int32_t, const MethodInfo*))List_1_CopyTo_m485759489_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<System.UInt32>::Find(System.Predicate`1<T>)
extern "C"  uint32_t List_1_Find_m3738653255_gshared (List_1_t1392853533 * __this, Predicate_1_t3930692160 * ___match0, const MethodInfo* method);
#define List_1_Find_m3738653255(__this, ___match0, method) ((  uint32_t (*) (List_1_t1392853533 *, Predicate_1_t3930692160 *, const MethodInfo*))List_1_Find_m3738653255_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m227849730_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t3930692160 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m227849730(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3930692160 *, const MethodInfo*))List_1_CheckMatch_m227849730_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt32>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1160601383_gshared (List_1_t1392853533 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t3930692160 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1160601383(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1392853533 *, int32_t, int32_t, Predicate_1_t3930692160 *, const MethodInfo*))List_1_GetIndex_m1160601383_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.UInt32>::GetEnumerator()
extern "C"  Enumerator_t1412526303  List_1_GetEnumerator_m2527343876_gshared (List_1_t1392853533 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m2527343876(__this, method) ((  Enumerator_t1412526303  (*) (List_1_t1392853533 *, const MethodInfo*))List_1_GetEnumerator_m2527343876_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt32>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1846725957_gshared (List_1_t1392853533 * __this, uint32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m1846725957(__this, ___item0, method) ((  int32_t (*) (List_1_t1392853533 *, uint32_t, const MethodInfo*))List_1_IndexOf_m1846725957_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3705036632_gshared (List_1_t1392853533 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3705036632(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1392853533 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3705036632_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m232126673_gshared (List_1_t1392853533 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m232126673(__this, ___index0, method) ((  void (*) (List_1_t1392853533 *, int32_t, const MethodInfo*))List_1_CheckIndex_m232126673_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3240279224_gshared (List_1_t1392853533 * __this, int32_t ___index0, uint32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m3240279224(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1392853533 *, int32_t, uint32_t, const MethodInfo*))List_1_Insert_m3240279224_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2369322797_gshared (List_1_t1392853533 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2369322797(__this, ___collection0, method) ((  void (*) (List_1_t1392853533 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2369322797_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt32>::Remove(T)
extern "C"  bool List_1_Remove_m1365483778_gshared (List_1_t1392853533 * __this, uint32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m1365483778(__this, ___item0, method) ((  bool (*) (List_1_t1392853533 *, uint32_t, const MethodInfo*))List_1_Remove_m1365483778_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt32>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m2831426376_gshared (List_1_t1392853533 * __this, Predicate_1_t3930692160 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m2831426376(__this, ___match0, method) ((  int32_t (*) (List_1_t1392853533 *, Predicate_1_t3930692160 *, const MethodInfo*))List_1_RemoveAll_m2831426376_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3191452661_gshared (List_1_t1392853533 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3191452661(__this, ___index0, method) ((  void (*) (List_1_t1392853533 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3191452661_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m3376639649_gshared (List_1_t1392853533 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m3376639649(__this, ___index0, ___count1, method) ((  void (*) (List_1_t1392853533 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3376639649_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::Reverse()
extern "C"  void List_1_Reverse_m3361621806_gshared (List_1_t1392853533 * __this, const MethodInfo* method);
#define List_1_Reverse_m3361621806(__this, method) ((  void (*) (List_1_t1392853533 *, const MethodInfo*))List_1_Reverse_m3361621806_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::Sort()
extern "C"  void List_1_Sort_m2580018996_gshared (List_1_t1392853533 * __this, const MethodInfo* method);
#define List_1_Sort_m2580018996(__this, method) ((  void (*) (List_1_t1392853533 *, const MethodInfo*))List_1_Sort_m2580018996_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m2291005744_gshared (List_1_t1392853533 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m2291005744(__this, ___comparer0, method) ((  void (*) (List_1_t1392853533 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m2291005744_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m2432558215_gshared (List_1_t1392853533 * __this, Comparison_1_t3035996464 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m2432558215(__this, ___comparison0, method) ((  void (*) (List_1_t1392853533 *, Comparison_1_t3035996464 *, const MethodInfo*))List_1_Sort_m2432558215_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<System.UInt32>::ToArray()
extern "C"  UInt32U5BU5D_t3230734560* List_1_ToArray_m3958565383_gshared (List_1_t1392853533 * __this, const MethodInfo* method);
#define List_1_ToArray_m3958565383(__this, method) ((  UInt32U5BU5D_t3230734560* (*) (List_1_t1392853533 *, const MethodInfo*))List_1_ToArray_m3958565383_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2997215181_gshared (List_1_t1392853533 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2997215181(__this, method) ((  void (*) (List_1_t1392853533 *, const MethodInfo*))List_1_TrimExcess_m2997215181_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt32>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1104190197_gshared (List_1_t1392853533 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1104190197(__this, method) ((  int32_t (*) (List_1_t1392853533 *, const MethodInfo*))List_1_get_Capacity_m1104190197_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m2591292446_gshared (List_1_t1392853533 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m2591292446(__this, ___value0, method) ((  void (*) (List_1_t1392853533 *, int32_t, const MethodInfo*))List_1_set_Capacity_m2591292446_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt32>::get_Count()
extern "C"  int32_t List_1_get_Count_m1226288909_gshared (List_1_t1392853533 * __this, const MethodInfo* method);
#define List_1_get_Count_m1226288909(__this, method) ((  int32_t (*) (List_1_t1392853533 *, const MethodInfo*))List_1_get_Count_m1226288909_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.UInt32>::get_Item(System.Int32)
extern "C"  uint32_t List_1_get_Item_m663243240_gshared (List_1_t1392853533 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m663243240(__this, ___index0, method) ((  uint32_t (*) (List_1_t1392853533 *, int32_t, const MethodInfo*))List_1_get_Item_m663243240_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m1866641295_gshared (List_1_t1392853533 * __this, int32_t ___index0, uint32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m1866641295(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1392853533 *, int32_t, uint32_t, const MethodInfo*))List_1_set_Item_m1866641295_gshared)(__this, ___index0, ___value1, method)
