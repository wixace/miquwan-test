﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginYiyou
struct PluginYiyou_t2559474556;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// VersionInfo
struct VersionInfo_t2356638086;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// System.Object
struct Il2CppObject;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_PluginYiyou2559474556.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_FloatTextMgr630384591.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginYiyou::.ctor()
extern "C"  void PluginYiyou__ctor_m390736751 (PluginYiyou_t2559474556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiyou::Init()
extern "C"  void PluginYiyou_Init_m795425029 (PluginYiyou_t2559474556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginYiyou::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginYiyou_ReqSDKHttpLogin_m1662052896 (PluginYiyou_t2559474556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginYiyou::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginYiyou_IsLoginSuccess_m3812269852 (PluginYiyou_t2559474556 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiyou::OpenUserLogin()
extern "C"  void PluginYiyou_OpenUserLogin_m794062529 (PluginYiyou_t2559474556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiyou::UserPay(CEvent.ZEvent)
extern "C"  void PluginYiyou_UserPay_m2442803601 (PluginYiyou_t2559474556 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiyou::SignCallBack(System.Boolean,System.String)
extern "C"  void PluginYiyou_SignCallBack_m3045719466 (PluginYiyou_t2559474556 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginYiyou::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginYiyou_BuildOrderParam2WWWForm_m4076857428 (PluginYiyou_t2559474556 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiyou::EnterGame(CEvent.ZEvent)
extern "C"  void PluginYiyou_EnterGame_m403696356 (PluginYiyou_t2559474556 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiyou::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginYiyou_RoleUpgrade_m1740604552 (PluginYiyou_t2559474556 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiyou::OnLoginSuccess(System.String)
extern "C"  void PluginYiyou_OnLoginSuccess_m1353143604 (PluginYiyou_t2559474556 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiyou::OnLogoutSuccess(System.String)
extern "C"  void PluginYiyou_OnLogoutSuccess_m3296288731 (PluginYiyou_t2559474556 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiyou::OnLogout(System.String)
extern "C"  void PluginYiyou_OnLogout_m156389316 (PluginYiyou_t2559474556 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiyou::initSdk(System.String,System.String,System.String,System.String)
extern "C"  void PluginYiyou_initSdk_m2163331103 (PluginYiyou_t2559474556 * __this, String_t* ___anAppid0, String_t* ___anScrectkey1, String_t* ___dataeyeAppid2, String_t* ___trackingIOAppid3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiyou::login()
extern "C"  void PluginYiyou_login_m4207718870 (PluginYiyou_t2559474556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiyou::logout()
extern "C"  void PluginYiyou_logout_m1596088799 (PluginYiyou_t2559474556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiyou::userInfo(System.String,System.String,System.String,System.Int32,System.String)
extern "C"  void PluginYiyou_userInfo_m3314939079 (PluginYiyou_t2559474556 * __this, String_t* ___serverid0, String_t* ___servername1, String_t* ___roleid2, int32_t ___rolelv3, String_t* ___rolename4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiyou::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYiyou_pay_m3447185589 (PluginYiyou_t2559474556 * __this, String_t* ___orderId0, String_t* ___serverid1, String_t* ___servername2, String_t* ___roleid3, String_t* ___rolelv4, String_t* ___rolename5, String_t* ___productid6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiyou::<OnLogoutSuccess>m__473()
extern "C"  void PluginYiyou_U3COnLogoutSuccessU3Em__473_m607554352 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiyou::<OnLogout>m__474()
extern "C"  void PluginYiyou_U3COnLogoutU3Em__474_m2995341810 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiyou::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginYiyou_ilo_AddEventListener1_m4202763173 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginYiyou::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginYiyou_ilo_get_Instance2_m1987340491 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.PayInfo PluginYiyou::ilo_Parse3(System.Object[])
extern "C"  PayInfo_t1775308120 * PluginYiyou_ilo_Parse3_m1690246193 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___payOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginYiyou::ilo_BuildOrderParam2WWWForm4(PluginYiyou,Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginYiyou_ilo_BuildOrderParam2WWWForm4_m1781756799 (Il2CppObject * __this /* static, unused */, PluginYiyou_t2559474556 * ____this0, PayInfo_t1775308120 * ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginYiyou::ilo_get_currentVS5(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginYiyou_ilo_get_currentVS5_m305438883 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginYiyou::ilo_ContainsKey6(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  bool PluginYiyou_ilo_ContainsKey6_m3128217494 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProductsCfgMgr PluginYiyou::ilo_get_ProductsCfgMgr7()
extern "C"  ProductsCfgMgr_t2493714872 * PluginYiyou_ilo_get_ProductsCfgMgr7_m2401125621 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginYiyou::ilo_GetProductID8(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginYiyou_ilo_GetProductID8_m3626624005 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiyou::ilo_Log9(System.Object,System.Boolean)
extern "C"  void PluginYiyou_ilo_Log9_m1546178566 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiyou::ilo_FloatText10(FloatTextMgr,FLOAT_TEXT_ID,System.Int32,System.Object[])
extern "C"  void PluginYiyou_ilo_FloatText10_m1812676238 (Il2CppObject * __this /* static, unused */, FloatTextMgr_t630384591 * ____this0, int32_t ___textId1, int32_t ___offsetY2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiyou::ilo_userInfo11(PluginYiyou,System.String,System.String,System.String,System.Int32,System.String)
extern "C"  void PluginYiyou_ilo_userInfo11_m2472347562 (Il2CppObject * __this /* static, unused */, PluginYiyou_t2559474556 * ____this0, String_t* ___serverid1, String_t* ___servername2, String_t* ___roleid3, int32_t ___rolelv4, String_t* ___rolename5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiyou::ilo_ReqSDKHttpLogin12(PluginsSdkMgr)
extern "C"  void PluginYiyou_ilo_ReqSDKHttpLogin12_m3843934996 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiyou::ilo_logout13(PluginYiyou)
extern "C"  void PluginYiyou_ilo_logout13_m3697725186 (Il2CppObject * __this /* static, unused */, PluginYiyou_t2559474556 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiyou::ilo_Logout14(System.Action)
extern "C"  void PluginYiyou_ilo_Logout14_m4199137096 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
