﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entity.Behavior.BiaocheMoveBvr
struct  BiaocheMoveBvr_t681848418  : public IBehavior_t770859129
{
public:
	// UnityEngine.Vector3 Entity.Behavior.BiaocheMoveBvr::targetPos
	Vector3_t4282066566  ___targetPos_2;
	// System.Single Entity.Behavior.BiaocheMoveBvr::curTime
	float ___curTime_3;
	// System.Single Entity.Behavior.BiaocheMoveBvr::intervalTime
	float ___intervalTime_4;

public:
	inline static int32_t get_offset_of_targetPos_2() { return static_cast<int32_t>(offsetof(BiaocheMoveBvr_t681848418, ___targetPos_2)); }
	inline Vector3_t4282066566  get_targetPos_2() const { return ___targetPos_2; }
	inline Vector3_t4282066566 * get_address_of_targetPos_2() { return &___targetPos_2; }
	inline void set_targetPos_2(Vector3_t4282066566  value)
	{
		___targetPos_2 = value;
	}

	inline static int32_t get_offset_of_curTime_3() { return static_cast<int32_t>(offsetof(BiaocheMoveBvr_t681848418, ___curTime_3)); }
	inline float get_curTime_3() const { return ___curTime_3; }
	inline float* get_address_of_curTime_3() { return &___curTime_3; }
	inline void set_curTime_3(float value)
	{
		___curTime_3 = value;
	}

	inline static int32_t get_offset_of_intervalTime_4() { return static_cast<int32_t>(offsetof(BiaocheMoveBvr_t681848418, ___intervalTime_4)); }
	inline float get_intervalTime_4() const { return ___intervalTime_4; }
	inline float* get_address_of_intervalTime_4() { return &___intervalTime_4; }
	inline void set_intervalTime_4(float value)
	{
		___intervalTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
