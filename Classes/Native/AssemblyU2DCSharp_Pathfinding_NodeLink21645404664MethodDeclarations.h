﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.NodeLink2
struct NodeLink2_t1645404664;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// UnityEngine.Transform
struct Transform_t1659122786;
// Pathfinding.PointNode
struct PointNode_t2761813780;
// Pathfinding.PointGraph
struct PointGraph_t468341652;
// Pathfinding.MeshNode
struct MeshNode_t3005053445;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_Pathfinding_NodeLink21645404664.h"
#include "AssemblyU2DCSharp_Pathfinding_PointGraph468341652.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "AssemblyU2DCSharp_Pathfinding_MeshNode3005053445.h"
#include "AssemblyU2DCSharp_Pathfinding_PointNode2761813780.h"

// System.Void Pathfinding.NodeLink2::.ctor()
extern "C"  void NodeLink2__ctor_m243746367 (NodeLink2_t1645404664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink2::.cctor()
extern "C"  void NodeLink2__cctor_m2779073870 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NodeLink2 Pathfinding.NodeLink2::GetNodeLink(Pathfinding.GraphNode)
extern "C"  NodeLink2_t1645404664 * NodeLink2_GetNodeLink_m366235926 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Pathfinding.NodeLink2::get_StartTransform()
extern "C"  Transform_t1659122786 * NodeLink2_get_StartTransform_m3575167354 (NodeLink2_t1645404664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Pathfinding.NodeLink2::get_EndTransform()
extern "C"  Transform_t1659122786 * NodeLink2_get_EndTransform_m3679110945 (NodeLink2_t1645404664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.GraphNode Pathfinding.NodeLink2::get_StartNode()
extern "C"  GraphNode_t23612370 * NodeLink2_get_StartNode_m3699144389 (NodeLink2_t1645404664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.GraphNode Pathfinding.NodeLink2::get_EndNode()
extern "C"  GraphNode_t23612370 * NodeLink2_get_EndNode_m1203276926 (NodeLink2_t1645404664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink2::OnPostScan()
extern "C"  void NodeLink2_OnPostScan_m1065549665 (NodeLink2_t1645404664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink2::InternalOnPostScan()
extern "C"  void NodeLink2_InternalOnPostScan_m663585470 (NodeLink2_t1645404664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink2::OnGraphsPostUpdate()
extern "C"  void NodeLink2_OnGraphsPostUpdate_m2431277362 (NodeLink2_t1645404664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink2::OnEnable()
extern "C"  void NodeLink2_OnEnable_m729900263 (NodeLink2_t1645404664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink2::OnDisable()
extern "C"  void NodeLink2_OnDisable_m1593008934 (NodeLink2_t1645404664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink2::RemoveConnections(Pathfinding.GraphNode)
extern "C"  void NodeLink2_RemoveConnections_m2558558302 (NodeLink2_t1645404664 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink2::ContextApplyForce()
extern "C"  void NodeLink2_ContextApplyForce_m1521743241 (NodeLink2_t1645404664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink2::Apply(System.Boolean)
extern "C"  void NodeLink2_Apply_m395485570 (NodeLink2_t1645404664 * __this, bool ___forceNewCheck0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink2::DrawCircle(UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.Color)
extern "C"  void NodeLink2_DrawCircle_m1566258962 (NodeLink2_t1645404664 * __this, Vector3_t4282066566  ___o0, float ___r1, int32_t ___detail2, Color_t4194546905  ___col3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink2::DrawGizmoBezier(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void NodeLink2_DrawGizmoBezier_m1166937210 (NodeLink2_t1645404664 * __this, Vector3_t4282066566  ___p10, Vector3_t4282066566  ___p21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink2::OnDrawGizmosSelected()
extern "C"  void NodeLink2_OnDrawGizmosSelected_m332835324 (NodeLink2_t1645404664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink2::OnDrawGizmos()
extern "C"  void NodeLink2_OnDrawGizmos_m3864139265 (NodeLink2_t1645404664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink2::OnDrawGizmos(System.Boolean)
extern "C"  void NodeLink2_OnDrawGizmos_m2788537784 (NodeLink2_t1645404664 * __this, bool ___selected0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NodeLink2::<OnPostScan>m__338(System.Boolean)
extern "C"  bool NodeLink2_U3COnPostScanU3Em__338_m1005591385 (NodeLink2_t1645404664 * __this, bool ___force0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Pathfinding.NodeLink2::ilo_get_EndTransform1(Pathfinding.NodeLink2)
extern "C"  Transform_t1659122786 * NodeLink2_ilo_get_EndTransform1_m628892199 (Il2CppObject * __this /* static, unused */, NodeLink2_t1645404664 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.PointNode Pathfinding.NodeLink2::ilo_AddNode2(Pathfinding.PointGraph,Pathfinding.Int3)
extern "C"  PointNode_t2761813780 * NodeLink2_ilo_AddNode2_m1958125572 (Il2CppObject * __this /* static, unused */, PointGraph_t468341652 * ____this0, Int3_t1974045594  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NodeLink2::ilo_get_Destroyed3(Pathfinding.GraphNode)
extern "C"  bool NodeLink2_ilo_get_Destroyed3_m920750885 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink2::ilo_Apply4(Pathfinding.NodeLink2,System.Boolean)
extern "C"  void NodeLink2_ilo_Apply4_m2612586133 (Il2CppObject * __this /* static, unused */, NodeLink2_t1645404664 * ____this0, bool ___forceNewCheck1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink2::ilo_RemoveConnection5(Pathfinding.MeshNode,Pathfinding.GraphNode)
extern "C"  void NodeLink2_ilo_RemoveConnection5_m382185226 (Il2CppObject * __this /* static, unused */, MeshNode_t3005053445 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink2::ilo_RemoveConnection6(Pathfinding.PointNode,Pathfinding.GraphNode)
extern "C"  void NodeLink2_ilo_RemoveConnection6_m165309586 (Il2CppObject * __this /* static, unused */, PointNode_t2761813780 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink2::ilo_RemoveConnections7(Pathfinding.NodeLink2,Pathfinding.GraphNode)
extern "C"  void NodeLink2_ilo_RemoveConnections7_m3420108014 (Il2CppObject * __this /* static, unused */, NodeLink2_t1645404664 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.NodeLink2::ilo_op_Explicit8(UnityEngine.Vector3)
extern "C"  Int3_t1974045594  NodeLink2_ilo_op_Explicit8_m3950594506 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.NodeLink2::ilo_get_costMagnitude9(Pathfinding.Int3&)
extern "C"  int32_t NodeLink2_ilo_get_costMagnitude9_m201423537 (Il2CppObject * __this /* static, unused */, Int3_t1974045594 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink2::ilo_AddConnection10(Pathfinding.PointNode,Pathfinding.GraphNode,System.UInt32)
extern "C"  void NodeLink2_ilo_AddConnection10_m1523231740 (Il2CppObject * __this /* static, unused */, PointNode_t2761813780 * ____this0, GraphNode_t23612370 * ___node1, uint32_t ___cost2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink2::ilo_AddConnection11(Pathfinding.MeshNode,Pathfinding.GraphNode,System.UInt32)
extern "C"  void NodeLink2_ilo_AddConnection11_m4251651530 (Il2CppObject * __this /* static, unused */, MeshNode_t3005053445 * ____this0, GraphNode_t23612370 * ___node1, uint32_t ___cost2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.NodeLink2::ilo_CubicBezier12(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t4282066566  NodeLink2_ilo_CubicBezier12_m1385910473 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___p00, Vector3_t4282066566  ___p11, Vector3_t4282066566  ___p22, Vector3_t4282066566  ___p33, float ___t4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink2::ilo_OnDrawGizmos13(Pathfinding.NodeLink2,System.Boolean)
extern "C"  void NodeLink2_ilo_OnDrawGizmos13_m2995058701 (Il2CppObject * __this /* static, unused */, NodeLink2_t1645404664 * ____this0, bool ___selected1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Pathfinding.NodeLink2::ilo_get_StartTransform14(Pathfinding.NodeLink2)
extern "C"  Transform_t1659122786 * NodeLink2_ilo_get_StartTransform14_m1986923510 (Il2CppObject * __this /* static, unused */, NodeLink2_t1645404664 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink2::ilo_DrawGizmoBezier15(Pathfinding.NodeLink2,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void NodeLink2_ilo_DrawGizmoBezier15_m3022267913 (Il2CppObject * __this /* static, unused */, NodeLink2_t1645404664 * ____this0, Vector3_t4282066566  ___p11, Vector3_t4282066566  ___p22, const MethodInfo* method) IL2CPP_METHOD_ATTR;
