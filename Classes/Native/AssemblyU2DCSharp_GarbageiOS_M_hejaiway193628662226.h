﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_hejaiway19
struct  M_hejaiway19_t3628662226  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_hejaiway19::_cawgischerHeher
	uint32_t ____cawgischerHeher_0;
	// System.UInt32 GarbageiOS.M_hejaiway19::_mapooBairhal
	uint32_t ____mapooBairhal_1;
	// System.String GarbageiOS.M_hejaiway19::_mayfouwe
	String_t* ____mayfouwe_2;
	// System.Single GarbageiOS.M_hejaiway19::_pissalLoupa
	float ____pissalLoupa_3;
	// System.UInt32 GarbageiOS.M_hejaiway19::_nesi
	uint32_t ____nesi_4;
	// System.UInt32 GarbageiOS.M_hejaiway19::_raikempouNusowmea
	uint32_t ____raikempouNusowmea_5;
	// System.String GarbageiOS.M_hejaiway19::_qipijoRoono
	String_t* ____qipijoRoono_6;
	// System.Int32 GarbageiOS.M_hejaiway19::_wavoumee
	int32_t ____wavoumee_7;
	// System.String GarbageiOS.M_hejaiway19::_temidea
	String_t* ____temidea_8;
	// System.Int32 GarbageiOS.M_hejaiway19::_celcoogeMinoubar
	int32_t ____celcoogeMinoubar_9;
	// System.String GarbageiOS.M_hejaiway19::_togoBisxainai
	String_t* ____togoBisxainai_10;
	// System.String GarbageiOS.M_hejaiway19::_pawraze
	String_t* ____pawraze_11;
	// System.Int32 GarbageiOS.M_hejaiway19::_staki
	int32_t ____staki_12;
	// System.UInt32 GarbageiOS.M_hejaiway19::_qadaiYedace
	uint32_t ____qadaiYedace_13;
	// System.UInt32 GarbageiOS.M_hejaiway19::_zayrudaListejea
	uint32_t ____zayrudaListejea_14;
	// System.Single GarbageiOS.M_hejaiway19::_learsihi
	float ____learsihi_15;
	// System.Single GarbageiOS.M_hejaiway19::_paldoustis
	float ____paldoustis_16;
	// System.Single GarbageiOS.M_hejaiway19::_houro
	float ____houro_17;
	// System.Single GarbageiOS.M_hejaiway19::_wazurHembile
	float ____wazurHembile_18;
	// System.String GarbageiOS.M_hejaiway19::_becallboVedow
	String_t* ____becallboVedow_19;
	// System.String GarbageiOS.M_hejaiway19::_sallqatai
	String_t* ____sallqatai_20;
	// System.Int32 GarbageiOS.M_hejaiway19::_tedrirSerjeartear
	int32_t ____tedrirSerjeartear_21;
	// System.Boolean GarbageiOS.M_hejaiway19::_zallijow
	bool ____zallijow_22;
	// System.Boolean GarbageiOS.M_hejaiway19::_sazaCisaipu
	bool ____sazaCisaipu_23;
	// System.Int32 GarbageiOS.M_hejaiway19::_sucoRaike
	int32_t ____sucoRaike_24;
	// System.Int32 GarbageiOS.M_hejaiway19::_drootasnoJererearvu
	int32_t ____drootasnoJererearvu_25;
	// System.UInt32 GarbageiOS.M_hejaiway19::_woowhooyarDatatem
	uint32_t ____woowhooyarDatatem_26;
	// System.Boolean GarbageiOS.M_hejaiway19::_roterenu
	bool ____roterenu_27;
	// System.Boolean GarbageiOS.M_hejaiway19::_stecinas
	bool ____stecinas_28;
	// System.Boolean GarbageiOS.M_hejaiway19::_nisdirTalle
	bool ____nisdirTalle_29;
	// System.Boolean GarbageiOS.M_hejaiway19::_seaperse
	bool ____seaperse_30;
	// System.UInt32 GarbageiOS.M_hejaiway19::_hecavurGooleesou
	uint32_t ____hecavurGooleesou_31;
	// System.UInt32 GarbageiOS.M_hejaiway19::_kamearjisTemea
	uint32_t ____kamearjisTemea_32;
	// System.UInt32 GarbageiOS.M_hejaiway19::_hupooSaigaltou
	uint32_t ____hupooSaigaltou_33;
	// System.String GarbageiOS.M_hejaiway19::_mowwhi
	String_t* ____mowwhi_34;
	// System.Int32 GarbageiOS.M_hejaiway19::_sairmou
	int32_t ____sairmou_35;
	// System.String GarbageiOS.M_hejaiway19::_daslatroGoocairlaw
	String_t* ____daslatroGoocairlaw_36;
	// System.Boolean GarbageiOS.M_hejaiway19::_zainerTisturris
	bool ____zainerTisturris_37;
	// System.UInt32 GarbageiOS.M_hejaiway19::_cerecheewhir
	uint32_t ____cerecheewhir_38;
	// System.Int32 GarbageiOS.M_hejaiway19::_trouseali
	int32_t ____trouseali_39;
	// System.String GarbageiOS.M_hejaiway19::_welay
	String_t* ____welay_40;
	// System.Int32 GarbageiOS.M_hejaiway19::_sairne
	int32_t ____sairne_41;
	// System.Int32 GarbageiOS.M_hejaiway19::_pursoSicir
	int32_t ____pursoSicir_42;
	// System.Single GarbageiOS.M_hejaiway19::_zokall
	float ____zokall_43;
	// System.Boolean GarbageiOS.M_hejaiway19::_chooveeje
	bool ____chooveeje_44;
	// System.Single GarbageiOS.M_hejaiway19::_rismayeLursowmear
	float ____rismayeLursowmear_45;
	// System.Boolean GarbageiOS.M_hejaiway19::_pirpairjall
	bool ____pirpairjall_46;
	// System.UInt32 GarbageiOS.M_hejaiway19::_salpouHaydomi
	uint32_t ____salpouHaydomi_47;
	// System.Int32 GarbageiOS.M_hejaiway19::_hawhowkorLasay
	int32_t ____hawhowkorLasay_48;
	// System.UInt32 GarbageiOS.M_hejaiway19::_kearceFatraji
	uint32_t ____kearceFatraji_49;
	// System.String GarbageiOS.M_hejaiway19::_kistoucir
	String_t* ____kistoucir_50;
	// System.Int32 GarbageiOS.M_hejaiway19::_maytracor
	int32_t ____maytracor_51;
	// System.Int32 GarbageiOS.M_hejaiway19::_wouserXeaqere
	int32_t ____wouserXeaqere_52;
	// System.Boolean GarbageiOS.M_hejaiway19::_murwhearjai
	bool ____murwhearjai_53;
	// System.Single GarbageiOS.M_hejaiway19::_toseaVouhel
	float ____toseaVouhel_54;

public:
	inline static int32_t get_offset_of__cawgischerHeher_0() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____cawgischerHeher_0)); }
	inline uint32_t get__cawgischerHeher_0() const { return ____cawgischerHeher_0; }
	inline uint32_t* get_address_of__cawgischerHeher_0() { return &____cawgischerHeher_0; }
	inline void set__cawgischerHeher_0(uint32_t value)
	{
		____cawgischerHeher_0 = value;
	}

	inline static int32_t get_offset_of__mapooBairhal_1() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____mapooBairhal_1)); }
	inline uint32_t get__mapooBairhal_1() const { return ____mapooBairhal_1; }
	inline uint32_t* get_address_of__mapooBairhal_1() { return &____mapooBairhal_1; }
	inline void set__mapooBairhal_1(uint32_t value)
	{
		____mapooBairhal_1 = value;
	}

	inline static int32_t get_offset_of__mayfouwe_2() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____mayfouwe_2)); }
	inline String_t* get__mayfouwe_2() const { return ____mayfouwe_2; }
	inline String_t** get_address_of__mayfouwe_2() { return &____mayfouwe_2; }
	inline void set__mayfouwe_2(String_t* value)
	{
		____mayfouwe_2 = value;
		Il2CppCodeGenWriteBarrier(&____mayfouwe_2, value);
	}

	inline static int32_t get_offset_of__pissalLoupa_3() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____pissalLoupa_3)); }
	inline float get__pissalLoupa_3() const { return ____pissalLoupa_3; }
	inline float* get_address_of__pissalLoupa_3() { return &____pissalLoupa_3; }
	inline void set__pissalLoupa_3(float value)
	{
		____pissalLoupa_3 = value;
	}

	inline static int32_t get_offset_of__nesi_4() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____nesi_4)); }
	inline uint32_t get__nesi_4() const { return ____nesi_4; }
	inline uint32_t* get_address_of__nesi_4() { return &____nesi_4; }
	inline void set__nesi_4(uint32_t value)
	{
		____nesi_4 = value;
	}

	inline static int32_t get_offset_of__raikempouNusowmea_5() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____raikempouNusowmea_5)); }
	inline uint32_t get__raikempouNusowmea_5() const { return ____raikempouNusowmea_5; }
	inline uint32_t* get_address_of__raikempouNusowmea_5() { return &____raikempouNusowmea_5; }
	inline void set__raikempouNusowmea_5(uint32_t value)
	{
		____raikempouNusowmea_5 = value;
	}

	inline static int32_t get_offset_of__qipijoRoono_6() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____qipijoRoono_6)); }
	inline String_t* get__qipijoRoono_6() const { return ____qipijoRoono_6; }
	inline String_t** get_address_of__qipijoRoono_6() { return &____qipijoRoono_6; }
	inline void set__qipijoRoono_6(String_t* value)
	{
		____qipijoRoono_6 = value;
		Il2CppCodeGenWriteBarrier(&____qipijoRoono_6, value);
	}

	inline static int32_t get_offset_of__wavoumee_7() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____wavoumee_7)); }
	inline int32_t get__wavoumee_7() const { return ____wavoumee_7; }
	inline int32_t* get_address_of__wavoumee_7() { return &____wavoumee_7; }
	inline void set__wavoumee_7(int32_t value)
	{
		____wavoumee_7 = value;
	}

	inline static int32_t get_offset_of__temidea_8() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____temidea_8)); }
	inline String_t* get__temidea_8() const { return ____temidea_8; }
	inline String_t** get_address_of__temidea_8() { return &____temidea_8; }
	inline void set__temidea_8(String_t* value)
	{
		____temidea_8 = value;
		Il2CppCodeGenWriteBarrier(&____temidea_8, value);
	}

	inline static int32_t get_offset_of__celcoogeMinoubar_9() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____celcoogeMinoubar_9)); }
	inline int32_t get__celcoogeMinoubar_9() const { return ____celcoogeMinoubar_9; }
	inline int32_t* get_address_of__celcoogeMinoubar_9() { return &____celcoogeMinoubar_9; }
	inline void set__celcoogeMinoubar_9(int32_t value)
	{
		____celcoogeMinoubar_9 = value;
	}

	inline static int32_t get_offset_of__togoBisxainai_10() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____togoBisxainai_10)); }
	inline String_t* get__togoBisxainai_10() const { return ____togoBisxainai_10; }
	inline String_t** get_address_of__togoBisxainai_10() { return &____togoBisxainai_10; }
	inline void set__togoBisxainai_10(String_t* value)
	{
		____togoBisxainai_10 = value;
		Il2CppCodeGenWriteBarrier(&____togoBisxainai_10, value);
	}

	inline static int32_t get_offset_of__pawraze_11() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____pawraze_11)); }
	inline String_t* get__pawraze_11() const { return ____pawraze_11; }
	inline String_t** get_address_of__pawraze_11() { return &____pawraze_11; }
	inline void set__pawraze_11(String_t* value)
	{
		____pawraze_11 = value;
		Il2CppCodeGenWriteBarrier(&____pawraze_11, value);
	}

	inline static int32_t get_offset_of__staki_12() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____staki_12)); }
	inline int32_t get__staki_12() const { return ____staki_12; }
	inline int32_t* get_address_of__staki_12() { return &____staki_12; }
	inline void set__staki_12(int32_t value)
	{
		____staki_12 = value;
	}

	inline static int32_t get_offset_of__qadaiYedace_13() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____qadaiYedace_13)); }
	inline uint32_t get__qadaiYedace_13() const { return ____qadaiYedace_13; }
	inline uint32_t* get_address_of__qadaiYedace_13() { return &____qadaiYedace_13; }
	inline void set__qadaiYedace_13(uint32_t value)
	{
		____qadaiYedace_13 = value;
	}

	inline static int32_t get_offset_of__zayrudaListejea_14() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____zayrudaListejea_14)); }
	inline uint32_t get__zayrudaListejea_14() const { return ____zayrudaListejea_14; }
	inline uint32_t* get_address_of__zayrudaListejea_14() { return &____zayrudaListejea_14; }
	inline void set__zayrudaListejea_14(uint32_t value)
	{
		____zayrudaListejea_14 = value;
	}

	inline static int32_t get_offset_of__learsihi_15() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____learsihi_15)); }
	inline float get__learsihi_15() const { return ____learsihi_15; }
	inline float* get_address_of__learsihi_15() { return &____learsihi_15; }
	inline void set__learsihi_15(float value)
	{
		____learsihi_15 = value;
	}

	inline static int32_t get_offset_of__paldoustis_16() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____paldoustis_16)); }
	inline float get__paldoustis_16() const { return ____paldoustis_16; }
	inline float* get_address_of__paldoustis_16() { return &____paldoustis_16; }
	inline void set__paldoustis_16(float value)
	{
		____paldoustis_16 = value;
	}

	inline static int32_t get_offset_of__houro_17() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____houro_17)); }
	inline float get__houro_17() const { return ____houro_17; }
	inline float* get_address_of__houro_17() { return &____houro_17; }
	inline void set__houro_17(float value)
	{
		____houro_17 = value;
	}

	inline static int32_t get_offset_of__wazurHembile_18() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____wazurHembile_18)); }
	inline float get__wazurHembile_18() const { return ____wazurHembile_18; }
	inline float* get_address_of__wazurHembile_18() { return &____wazurHembile_18; }
	inline void set__wazurHembile_18(float value)
	{
		____wazurHembile_18 = value;
	}

	inline static int32_t get_offset_of__becallboVedow_19() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____becallboVedow_19)); }
	inline String_t* get__becallboVedow_19() const { return ____becallboVedow_19; }
	inline String_t** get_address_of__becallboVedow_19() { return &____becallboVedow_19; }
	inline void set__becallboVedow_19(String_t* value)
	{
		____becallboVedow_19 = value;
		Il2CppCodeGenWriteBarrier(&____becallboVedow_19, value);
	}

	inline static int32_t get_offset_of__sallqatai_20() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____sallqatai_20)); }
	inline String_t* get__sallqatai_20() const { return ____sallqatai_20; }
	inline String_t** get_address_of__sallqatai_20() { return &____sallqatai_20; }
	inline void set__sallqatai_20(String_t* value)
	{
		____sallqatai_20 = value;
		Il2CppCodeGenWriteBarrier(&____sallqatai_20, value);
	}

	inline static int32_t get_offset_of__tedrirSerjeartear_21() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____tedrirSerjeartear_21)); }
	inline int32_t get__tedrirSerjeartear_21() const { return ____tedrirSerjeartear_21; }
	inline int32_t* get_address_of__tedrirSerjeartear_21() { return &____tedrirSerjeartear_21; }
	inline void set__tedrirSerjeartear_21(int32_t value)
	{
		____tedrirSerjeartear_21 = value;
	}

	inline static int32_t get_offset_of__zallijow_22() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____zallijow_22)); }
	inline bool get__zallijow_22() const { return ____zallijow_22; }
	inline bool* get_address_of__zallijow_22() { return &____zallijow_22; }
	inline void set__zallijow_22(bool value)
	{
		____zallijow_22 = value;
	}

	inline static int32_t get_offset_of__sazaCisaipu_23() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____sazaCisaipu_23)); }
	inline bool get__sazaCisaipu_23() const { return ____sazaCisaipu_23; }
	inline bool* get_address_of__sazaCisaipu_23() { return &____sazaCisaipu_23; }
	inline void set__sazaCisaipu_23(bool value)
	{
		____sazaCisaipu_23 = value;
	}

	inline static int32_t get_offset_of__sucoRaike_24() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____sucoRaike_24)); }
	inline int32_t get__sucoRaike_24() const { return ____sucoRaike_24; }
	inline int32_t* get_address_of__sucoRaike_24() { return &____sucoRaike_24; }
	inline void set__sucoRaike_24(int32_t value)
	{
		____sucoRaike_24 = value;
	}

	inline static int32_t get_offset_of__drootasnoJererearvu_25() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____drootasnoJererearvu_25)); }
	inline int32_t get__drootasnoJererearvu_25() const { return ____drootasnoJererearvu_25; }
	inline int32_t* get_address_of__drootasnoJererearvu_25() { return &____drootasnoJererearvu_25; }
	inline void set__drootasnoJererearvu_25(int32_t value)
	{
		____drootasnoJererearvu_25 = value;
	}

	inline static int32_t get_offset_of__woowhooyarDatatem_26() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____woowhooyarDatatem_26)); }
	inline uint32_t get__woowhooyarDatatem_26() const { return ____woowhooyarDatatem_26; }
	inline uint32_t* get_address_of__woowhooyarDatatem_26() { return &____woowhooyarDatatem_26; }
	inline void set__woowhooyarDatatem_26(uint32_t value)
	{
		____woowhooyarDatatem_26 = value;
	}

	inline static int32_t get_offset_of__roterenu_27() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____roterenu_27)); }
	inline bool get__roterenu_27() const { return ____roterenu_27; }
	inline bool* get_address_of__roterenu_27() { return &____roterenu_27; }
	inline void set__roterenu_27(bool value)
	{
		____roterenu_27 = value;
	}

	inline static int32_t get_offset_of__stecinas_28() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____stecinas_28)); }
	inline bool get__stecinas_28() const { return ____stecinas_28; }
	inline bool* get_address_of__stecinas_28() { return &____stecinas_28; }
	inline void set__stecinas_28(bool value)
	{
		____stecinas_28 = value;
	}

	inline static int32_t get_offset_of__nisdirTalle_29() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____nisdirTalle_29)); }
	inline bool get__nisdirTalle_29() const { return ____nisdirTalle_29; }
	inline bool* get_address_of__nisdirTalle_29() { return &____nisdirTalle_29; }
	inline void set__nisdirTalle_29(bool value)
	{
		____nisdirTalle_29 = value;
	}

	inline static int32_t get_offset_of__seaperse_30() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____seaperse_30)); }
	inline bool get__seaperse_30() const { return ____seaperse_30; }
	inline bool* get_address_of__seaperse_30() { return &____seaperse_30; }
	inline void set__seaperse_30(bool value)
	{
		____seaperse_30 = value;
	}

	inline static int32_t get_offset_of__hecavurGooleesou_31() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____hecavurGooleesou_31)); }
	inline uint32_t get__hecavurGooleesou_31() const { return ____hecavurGooleesou_31; }
	inline uint32_t* get_address_of__hecavurGooleesou_31() { return &____hecavurGooleesou_31; }
	inline void set__hecavurGooleesou_31(uint32_t value)
	{
		____hecavurGooleesou_31 = value;
	}

	inline static int32_t get_offset_of__kamearjisTemea_32() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____kamearjisTemea_32)); }
	inline uint32_t get__kamearjisTemea_32() const { return ____kamearjisTemea_32; }
	inline uint32_t* get_address_of__kamearjisTemea_32() { return &____kamearjisTemea_32; }
	inline void set__kamearjisTemea_32(uint32_t value)
	{
		____kamearjisTemea_32 = value;
	}

	inline static int32_t get_offset_of__hupooSaigaltou_33() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____hupooSaigaltou_33)); }
	inline uint32_t get__hupooSaigaltou_33() const { return ____hupooSaigaltou_33; }
	inline uint32_t* get_address_of__hupooSaigaltou_33() { return &____hupooSaigaltou_33; }
	inline void set__hupooSaigaltou_33(uint32_t value)
	{
		____hupooSaigaltou_33 = value;
	}

	inline static int32_t get_offset_of__mowwhi_34() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____mowwhi_34)); }
	inline String_t* get__mowwhi_34() const { return ____mowwhi_34; }
	inline String_t** get_address_of__mowwhi_34() { return &____mowwhi_34; }
	inline void set__mowwhi_34(String_t* value)
	{
		____mowwhi_34 = value;
		Il2CppCodeGenWriteBarrier(&____mowwhi_34, value);
	}

	inline static int32_t get_offset_of__sairmou_35() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____sairmou_35)); }
	inline int32_t get__sairmou_35() const { return ____sairmou_35; }
	inline int32_t* get_address_of__sairmou_35() { return &____sairmou_35; }
	inline void set__sairmou_35(int32_t value)
	{
		____sairmou_35 = value;
	}

	inline static int32_t get_offset_of__daslatroGoocairlaw_36() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____daslatroGoocairlaw_36)); }
	inline String_t* get__daslatroGoocairlaw_36() const { return ____daslatroGoocairlaw_36; }
	inline String_t** get_address_of__daslatroGoocairlaw_36() { return &____daslatroGoocairlaw_36; }
	inline void set__daslatroGoocairlaw_36(String_t* value)
	{
		____daslatroGoocairlaw_36 = value;
		Il2CppCodeGenWriteBarrier(&____daslatroGoocairlaw_36, value);
	}

	inline static int32_t get_offset_of__zainerTisturris_37() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____zainerTisturris_37)); }
	inline bool get__zainerTisturris_37() const { return ____zainerTisturris_37; }
	inline bool* get_address_of__zainerTisturris_37() { return &____zainerTisturris_37; }
	inline void set__zainerTisturris_37(bool value)
	{
		____zainerTisturris_37 = value;
	}

	inline static int32_t get_offset_of__cerecheewhir_38() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____cerecheewhir_38)); }
	inline uint32_t get__cerecheewhir_38() const { return ____cerecheewhir_38; }
	inline uint32_t* get_address_of__cerecheewhir_38() { return &____cerecheewhir_38; }
	inline void set__cerecheewhir_38(uint32_t value)
	{
		____cerecheewhir_38 = value;
	}

	inline static int32_t get_offset_of__trouseali_39() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____trouseali_39)); }
	inline int32_t get__trouseali_39() const { return ____trouseali_39; }
	inline int32_t* get_address_of__trouseali_39() { return &____trouseali_39; }
	inline void set__trouseali_39(int32_t value)
	{
		____trouseali_39 = value;
	}

	inline static int32_t get_offset_of__welay_40() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____welay_40)); }
	inline String_t* get__welay_40() const { return ____welay_40; }
	inline String_t** get_address_of__welay_40() { return &____welay_40; }
	inline void set__welay_40(String_t* value)
	{
		____welay_40 = value;
		Il2CppCodeGenWriteBarrier(&____welay_40, value);
	}

	inline static int32_t get_offset_of__sairne_41() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____sairne_41)); }
	inline int32_t get__sairne_41() const { return ____sairne_41; }
	inline int32_t* get_address_of__sairne_41() { return &____sairne_41; }
	inline void set__sairne_41(int32_t value)
	{
		____sairne_41 = value;
	}

	inline static int32_t get_offset_of__pursoSicir_42() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____pursoSicir_42)); }
	inline int32_t get__pursoSicir_42() const { return ____pursoSicir_42; }
	inline int32_t* get_address_of__pursoSicir_42() { return &____pursoSicir_42; }
	inline void set__pursoSicir_42(int32_t value)
	{
		____pursoSicir_42 = value;
	}

	inline static int32_t get_offset_of__zokall_43() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____zokall_43)); }
	inline float get__zokall_43() const { return ____zokall_43; }
	inline float* get_address_of__zokall_43() { return &____zokall_43; }
	inline void set__zokall_43(float value)
	{
		____zokall_43 = value;
	}

	inline static int32_t get_offset_of__chooveeje_44() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____chooveeje_44)); }
	inline bool get__chooveeje_44() const { return ____chooveeje_44; }
	inline bool* get_address_of__chooveeje_44() { return &____chooveeje_44; }
	inline void set__chooveeje_44(bool value)
	{
		____chooveeje_44 = value;
	}

	inline static int32_t get_offset_of__rismayeLursowmear_45() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____rismayeLursowmear_45)); }
	inline float get__rismayeLursowmear_45() const { return ____rismayeLursowmear_45; }
	inline float* get_address_of__rismayeLursowmear_45() { return &____rismayeLursowmear_45; }
	inline void set__rismayeLursowmear_45(float value)
	{
		____rismayeLursowmear_45 = value;
	}

	inline static int32_t get_offset_of__pirpairjall_46() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____pirpairjall_46)); }
	inline bool get__pirpairjall_46() const { return ____pirpairjall_46; }
	inline bool* get_address_of__pirpairjall_46() { return &____pirpairjall_46; }
	inline void set__pirpairjall_46(bool value)
	{
		____pirpairjall_46 = value;
	}

	inline static int32_t get_offset_of__salpouHaydomi_47() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____salpouHaydomi_47)); }
	inline uint32_t get__salpouHaydomi_47() const { return ____salpouHaydomi_47; }
	inline uint32_t* get_address_of__salpouHaydomi_47() { return &____salpouHaydomi_47; }
	inline void set__salpouHaydomi_47(uint32_t value)
	{
		____salpouHaydomi_47 = value;
	}

	inline static int32_t get_offset_of__hawhowkorLasay_48() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____hawhowkorLasay_48)); }
	inline int32_t get__hawhowkorLasay_48() const { return ____hawhowkorLasay_48; }
	inline int32_t* get_address_of__hawhowkorLasay_48() { return &____hawhowkorLasay_48; }
	inline void set__hawhowkorLasay_48(int32_t value)
	{
		____hawhowkorLasay_48 = value;
	}

	inline static int32_t get_offset_of__kearceFatraji_49() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____kearceFatraji_49)); }
	inline uint32_t get__kearceFatraji_49() const { return ____kearceFatraji_49; }
	inline uint32_t* get_address_of__kearceFatraji_49() { return &____kearceFatraji_49; }
	inline void set__kearceFatraji_49(uint32_t value)
	{
		____kearceFatraji_49 = value;
	}

	inline static int32_t get_offset_of__kistoucir_50() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____kistoucir_50)); }
	inline String_t* get__kistoucir_50() const { return ____kistoucir_50; }
	inline String_t** get_address_of__kistoucir_50() { return &____kistoucir_50; }
	inline void set__kistoucir_50(String_t* value)
	{
		____kistoucir_50 = value;
		Il2CppCodeGenWriteBarrier(&____kistoucir_50, value);
	}

	inline static int32_t get_offset_of__maytracor_51() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____maytracor_51)); }
	inline int32_t get__maytracor_51() const { return ____maytracor_51; }
	inline int32_t* get_address_of__maytracor_51() { return &____maytracor_51; }
	inline void set__maytracor_51(int32_t value)
	{
		____maytracor_51 = value;
	}

	inline static int32_t get_offset_of__wouserXeaqere_52() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____wouserXeaqere_52)); }
	inline int32_t get__wouserXeaqere_52() const { return ____wouserXeaqere_52; }
	inline int32_t* get_address_of__wouserXeaqere_52() { return &____wouserXeaqere_52; }
	inline void set__wouserXeaqere_52(int32_t value)
	{
		____wouserXeaqere_52 = value;
	}

	inline static int32_t get_offset_of__murwhearjai_53() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____murwhearjai_53)); }
	inline bool get__murwhearjai_53() const { return ____murwhearjai_53; }
	inline bool* get_address_of__murwhearjai_53() { return &____murwhearjai_53; }
	inline void set__murwhearjai_53(bool value)
	{
		____murwhearjai_53 = value;
	}

	inline static int32_t get_offset_of__toseaVouhel_54() { return static_cast<int32_t>(offsetof(M_hejaiway19_t3628662226, ____toseaVouhel_54)); }
	inline float get__toseaVouhel_54() const { return ____toseaVouhel_54; }
	inline float* get_address_of__toseaVouhel_54() { return &____toseaVouhel_54; }
	inline void set__toseaVouhel_54(float value)
	{
		____toseaVouhel_54 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
