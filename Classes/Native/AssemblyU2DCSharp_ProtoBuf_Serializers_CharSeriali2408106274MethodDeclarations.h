﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.CharSerializer
struct CharSerializer_t2408106274;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"

// System.Void ProtoBuf.Serializers.CharSerializer::.ctor(ProtoBuf.Meta.TypeModel)
extern "C"  void CharSerializer__ctor_m2606822934 (CharSerializer_t2408106274 * __this, TypeModel_t2730011105 * ___model0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.CharSerializer::.cctor()
extern "C"  void CharSerializer__cctor_m4196129306 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.CharSerializer::get_ExpectedType()
extern "C"  Type_t * CharSerializer_get_ExpectedType_m1276225586 (CharSerializer_t2408106274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.CharSerializer::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void CharSerializer_Write_m544930986 (CharSerializer_t2408106274 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.CharSerializer::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * CharSerializer_Read_m3205530588 (CharSerializer_t2408106274 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
