﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_NavMeshAgentGenerated
struct UnityEngine_NavMeshAgentGenerated_t1818064242;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_NavMeshAgentGenerated::.ctor()
extern "C"  void UnityEngine_NavMeshAgentGenerated__ctor_m682059833 (UnityEngine_NavMeshAgentGenerated_t1818064242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshAgentGenerated::NavMeshAgent_NavMeshAgent1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshAgentGenerated_NavMeshAgent_NavMeshAgent1_m1373084857 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_destination(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_destination_m2354718936 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_stoppingDistance(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_stoppingDistance_m3384309629 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_velocity(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_velocity_m1481616809 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_nextPosition(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_nextPosition_m3113668650 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_steeringTarget(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_steeringTarget_m772094660 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_desiredVelocity(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_desiredVelocity_m3443147119 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_remainingDistance(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_remainingDistance_m1134303611 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_baseOffset(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_baseOffset_m3795537826 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_isOnOffMeshLink(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_isOnOffMeshLink_m4070928921 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_currentOffMeshLinkData(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_currentOffMeshLinkData_m757630367 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_nextOffMeshLinkData(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_nextOffMeshLinkData_m2275641433 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_autoTraverseOffMeshLink(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_autoTraverseOffMeshLink_m2789935537 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_autoBraking(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_autoBraking_m1175332045 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_autoRepath(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_autoRepath_m3365757535 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_hasPath(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_hasPath_m3817351719 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_pathPending(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_pathPending_m1259916212 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_isPathStale(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_isPathStale_m511054844 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_pathStatus(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_pathStatus_m2443120463 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_pathEndPosition(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_pathEndPosition_m4137078343 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_path(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_path_m2930665313 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_areaMask(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_areaMask_m2985242861 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_speed(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_speed_m4254160063 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_angularSpeed(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_angularSpeed_m3688077313 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_acceleration(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_acceleration_m2099268966 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_updatePosition(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_updatePosition_m319107348 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_updateRotation(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_updateRotation_m4282780255 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_radius(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_radius_m3955421588 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_height(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_height_m4001375391 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_obstacleAvoidanceType(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_obstacleAvoidanceType_m1771087543 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_avoidancePriority(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_avoidancePriority_m2325527582 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::NavMeshAgent_isOnNavMesh(JSVCall)
extern "C"  void UnityEngine_NavMeshAgentGenerated_NavMeshAgent_isOnNavMesh_m454950975 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshAgentGenerated::NavMeshAgent_ActivateCurrentOffMeshLink__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshAgentGenerated_NavMeshAgent_ActivateCurrentOffMeshLink__Boolean_m1280517141 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshAgentGenerated::NavMeshAgent_CalculatePath__Vector3__NavMeshPath(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshAgentGenerated_NavMeshAgent_CalculatePath__Vector3__NavMeshPath_m2324228141 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshAgentGenerated::NavMeshAgent_CompleteOffMeshLink(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshAgentGenerated_NavMeshAgent_CompleteOffMeshLink_m2241004666 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshAgentGenerated::NavMeshAgent_FindClosestEdge__NavMeshHit(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshAgentGenerated_NavMeshAgent_FindClosestEdge__NavMeshHit_m2628027805 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshAgentGenerated::NavMeshAgent_GetAreaCost__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshAgentGenerated_NavMeshAgent_GetAreaCost__Int32_m3307704867 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshAgentGenerated::NavMeshAgent_Move__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshAgentGenerated_NavMeshAgent_Move__Vector3_m3374215036 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshAgentGenerated::NavMeshAgent_Raycast__Vector3__NavMeshHit(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshAgentGenerated_NavMeshAgent_Raycast__Vector3__NavMeshHit_m1382996815 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshAgentGenerated::NavMeshAgent_ResetPath(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshAgentGenerated_NavMeshAgent_ResetPath_m759504689 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshAgentGenerated::NavMeshAgent_Resume(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshAgentGenerated_NavMeshAgent_Resume_m1158034930 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshAgentGenerated::NavMeshAgent_SamplePathPosition__Int32__Single__NavMeshHit(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshAgentGenerated_NavMeshAgent_SamplePathPosition__Int32__Single__NavMeshHit_m3806587070 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshAgentGenerated::NavMeshAgent_SetAreaCost__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshAgentGenerated_NavMeshAgent_SetAreaCost__Int32__Single_m1010246879 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshAgentGenerated::NavMeshAgent_SetDestination__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshAgentGenerated_NavMeshAgent_SetDestination__Vector3_m3441770145 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshAgentGenerated::NavMeshAgent_SetPath__NavMeshPath(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshAgentGenerated_NavMeshAgent_SetPath__NavMeshPath_m1879488947 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshAgentGenerated::NavMeshAgent_Stop(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshAgentGenerated_NavMeshAgent_Stop_m2021672199 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshAgentGenerated::NavMeshAgent_Warp__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshAgentGenerated_NavMeshAgent_Warp__Vector3_m1485636069 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::__Register()
extern "C"  void UnityEngine_NavMeshAgentGenerated___Register_m1721716782 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_NavMeshAgentGenerated::ilo_getSingle1(System.Int32)
extern "C"  float UnityEngine_NavMeshAgentGenerated_ilo_getSingle1_m1716310134 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_NavMeshAgentGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_NavMeshAgentGenerated_ilo_setObject2_m2221436118 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void UnityEngine_NavMeshAgentGenerated_ilo_setBooleanS3_m3938004776 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshAgentGenerated::ilo_getBooleanS4(System.Int32)
extern "C"  bool UnityEngine_NavMeshAgentGenerated_ilo_getBooleanS4_m188854350 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::ilo_setEnum5(System.Int32,System.Int32)
extern "C"  void UnityEngine_NavMeshAgentGenerated_ilo_setEnum5_m62549846 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::ilo_setSingle6(System.Int32,System.Single)
extern "C"  void UnityEngine_NavMeshAgentGenerated_ilo_setSingle6_m1042920480 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::ilo_setInt327(System.Int32,System.Int32)
extern "C"  void UnityEngine_NavMeshAgentGenerated_ilo_setInt327_m747504375 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_NavMeshAgentGenerated::ilo_incArgIndex8()
extern "C"  int32_t UnityEngine_NavMeshAgentGenerated_ilo_incArgIndex8_m923829326 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshAgentGenerated::ilo_setArgIndex9(System.Int32)
extern "C"  void UnityEngine_NavMeshAgentGenerated_ilo_setArgIndex9_m2627343914 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_NavMeshAgentGenerated::ilo_getVector3S10(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_NavMeshAgentGenerated_ilo_getVector3S10_m2553490055 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
