﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WaypointCam
struct WaypointCam_t1504517102;

#include "codegen/il2cpp-codegen.h"

// System.Void WaypointCam::.ctor()
extern "C"  void WaypointCam__ctor_m1733552957 (WaypointCam_t1504517102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaypointCam::Awake()
extern "C"  void WaypointCam_Awake_m1971158176 (WaypointCam_t1504517102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaypointCam::OnDrawGizmos()
extern "C"  void WaypointCam_OnDrawGizmos_m2721019715 (WaypointCam_t1504517102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
