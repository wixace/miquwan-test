﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReplayAnimation
struct ReplayAnimation_t411606845;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.String
struct String_t;
// ReplayBase
struct ReplayBase_t779703160;
// CombatEntity
struct CombatEntity_t684137495;
// AnimationRunner
struct AnimationRunner_t1015409588;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ReplayBase779703160.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"

// System.Void ReplayAnimation::.ctor()
extern "C"  void ReplayAnimation__ctor_m2774662350 (ReplayAnimation_t411606845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayAnimation::.ctor(System.Object[])
extern "C"  void ReplayAnimation__ctor_m1564246308 (ReplayAnimation_t411606845 * __this, ObjectU5BU5D_t1108656482* ___datas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] ReplayAnimation::ParserJsonStr(System.String)
extern "C"  StringU5BU5D_t4054002952* ReplayAnimation_ParserJsonStr_m3162476715 (ReplayAnimation_t411606845 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReplayAnimation::GetJsonStr()
extern "C"  String_t* ReplayAnimation_GetJsonStr_m3306838758 (ReplayAnimation_t411606845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayAnimation::Execute()
extern "C"  void ReplayAnimation_Execute_m2127216993 (ReplayAnimation_t411606845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayAnimation::ilo_Execute1(ReplayBase)
extern "C"  void ReplayAnimation_ilo_Execute1_m4017343757 (Il2CppObject * __this /* static, unused */, ReplayBase_t779703160 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity ReplayAnimation::ilo_get_Entity2(ReplayBase)
extern "C"  CombatEntity_t684137495 * ReplayAnimation_ilo_get_Entity2_m1120394285 (Il2CppObject * __this /* static, unused */, ReplayBase_t779703160 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AnimationRunner ReplayAnimation::ilo_get_characterAnim3(CombatEntity)
extern "C"  AnimationRunner_t1015409588 * ReplayAnimation_ilo_get_characterAnim3_m2990574709 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
