﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_retriSehi65
struct M_retriSehi65_t4105382602;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_retriSehi654105382602.h"

// System.Void GarbageiOS.M_retriSehi65::.ctor()
extern "C"  void M_retriSehi65__ctor_m2314851289 (M_retriSehi65_t4105382602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_retriSehi65::M_joosiseeNowhel0(System.String[],System.Int32)
extern "C"  void M_retriSehi65_M_joosiseeNowhel0_m3281125888 (M_retriSehi65_t4105382602 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_retriSehi65::M_metadairHoutoo1(System.String[],System.Int32)
extern "C"  void M_retriSehi65_M_metadairHoutoo1_m1394557084 (M_retriSehi65_t4105382602 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_retriSehi65::M_vecharwe2(System.String[],System.Int32)
extern "C"  void M_retriSehi65_M_vecharwe2_m1586989979 (M_retriSehi65_t4105382602 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_retriSehi65::M_jairlawzearHelta3(System.String[],System.Int32)
extern "C"  void M_retriSehi65_M_jairlawzearHelta3_m1411576529 (M_retriSehi65_t4105382602 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_retriSehi65::ilo_M_metadairHoutoo11(GarbageiOS.M_retriSehi65,System.String[],System.Int32)
extern "C"  void M_retriSehi65_ilo_M_metadairHoutoo11_m2140085918 (Il2CppObject * __this /* static, unused */, M_retriSehi65_t4105382602 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_retriSehi65::ilo_M_vecharwe22(GarbageiOS.M_retriSehi65,System.String[],System.Int32)
extern "C"  void M_retriSehi65_ilo_M_vecharwe22_m1667617148 (Il2CppObject * __this /* static, unused */, M_retriSehi65_t4105382602 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
