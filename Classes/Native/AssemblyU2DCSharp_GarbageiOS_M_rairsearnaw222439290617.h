﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_rairsearnaw222
struct  M_rairsearnaw222_t439290617  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_rairsearnaw222::_whormu
	bool ____whormu_0;
	// System.Boolean GarbageiOS.M_rairsearnaw222::_relhouCearcerqar
	bool ____relhouCearcerqar_1;
	// System.String GarbageiOS.M_rairsearnaw222::_warpi
	String_t* ____warpi_2;
	// System.Boolean GarbageiOS.M_rairsearnaw222::_kisbearRemaw
	bool ____kisbearRemaw_3;
	// System.Int32 GarbageiOS.M_rairsearnaw222::_memsidur
	int32_t ____memsidur_4;
	// System.Int32 GarbageiOS.M_rairsearnaw222::_tunabu
	int32_t ____tunabu_5;
	// System.UInt32 GarbageiOS.M_rairsearnaw222::_sorchouca
	uint32_t ____sorchouca_6;
	// System.Single GarbageiOS.M_rairsearnaw222::_dabaw
	float ____dabaw_7;
	// System.Single GarbageiOS.M_rairsearnaw222::_zima
	float ____zima_8;
	// System.String GarbageiOS.M_rairsearnaw222::_jasorFija
	String_t* ____jasorFija_9;
	// System.Boolean GarbageiOS.M_rairsearnaw222::_bemico
	bool ____bemico_10;
	// System.Int32 GarbageiOS.M_rairsearnaw222::_lustalloKiryai
	int32_t ____lustalloKiryai_11;
	// System.Boolean GarbageiOS.M_rairsearnaw222::_stemdustair
	bool ____stemdustair_12;
	// System.UInt32 GarbageiOS.M_rairsearnaw222::_topahis
	uint32_t ____topahis_13;
	// System.Int32 GarbageiOS.M_rairsearnaw222::_sikeajaLote
	int32_t ____sikeajaLote_14;
	// System.UInt32 GarbageiOS.M_rairsearnaw222::_harjisparNatru
	uint32_t ____harjisparNatru_15;
	// System.Boolean GarbageiOS.M_rairsearnaw222::_cazucu
	bool ____cazucu_16;
	// System.String GarbageiOS.M_rairsearnaw222::_cejasguDriwivair
	String_t* ____cejasguDriwivair_17;
	// System.UInt32 GarbageiOS.M_rairsearnaw222::_jallmaipaXohaycha
	uint32_t ____jallmaipaXohaycha_18;
	// System.Int32 GarbageiOS.M_rairsearnaw222::_wokalmaSeremo
	int32_t ____wokalmaSeremo_19;
	// System.Int32 GarbageiOS.M_rairsearnaw222::_mawarwi
	int32_t ____mawarwi_20;
	// System.String GarbageiOS.M_rairsearnaw222::_hemroutuTabouri
	String_t* ____hemroutuTabouri_21;
	// System.Int32 GarbageiOS.M_rairsearnaw222::_halhatree
	int32_t ____halhatree_22;
	// System.Single GarbageiOS.M_rairsearnaw222::_whouseci
	float ____whouseci_23;
	// System.Int32 GarbageiOS.M_rairsearnaw222::_sairnuyeMerere
	int32_t ____sairnuyeMerere_24;
	// System.Int32 GarbageiOS.M_rairsearnaw222::_caisosowCerrisear
	int32_t ____caisosowCerrisear_25;
	// System.Boolean GarbageiOS.M_rairsearnaw222::_loceHowbeser
	bool ____loceHowbeser_26;
	// System.Boolean GarbageiOS.M_rairsearnaw222::_rairlooheMeara
	bool ____rairlooheMeara_27;
	// System.String GarbageiOS.M_rairsearnaw222::_kookall
	String_t* ____kookall_28;
	// System.String GarbageiOS.M_rairsearnaw222::_kalnaVairberere
	String_t* ____kalnaVairberere_29;
	// System.Boolean GarbageiOS.M_rairsearnaw222::_moudasurPasqouso
	bool ____moudasurPasqouso_30;
	// System.Boolean GarbageiOS.M_rairsearnaw222::_xabedorLorgel
	bool ____xabedorLorgel_31;
	// System.String GarbageiOS.M_rairsearnaw222::_coorama
	String_t* ____coorama_32;
	// System.Int32 GarbageiOS.M_rairsearnaw222::_deniskemWeharer
	int32_t ____deniskemWeharer_33;
	// System.String GarbageiOS.M_rairsearnaw222::_nigallroo
	String_t* ____nigallroo_34;
	// System.Boolean GarbageiOS.M_rairsearnaw222::_whejeSearstaisa
	bool ____whejeSearstaisa_35;
	// System.Boolean GarbageiOS.M_rairsearnaw222::_xapaw
	bool ____xapaw_36;
	// System.Single GarbageiOS.M_rairsearnaw222::_yugikasYigope
	float ____yugikasYigope_37;
	// System.String GarbageiOS.M_rairsearnaw222::_whedowdor
	String_t* ____whedowdor_38;
	// System.Single GarbageiOS.M_rairsearnaw222::_seejedri
	float ____seejedri_39;
	// System.String GarbageiOS.M_rairsearnaw222::_nafirmi
	String_t* ____nafirmi_40;

public:
	inline static int32_t get_offset_of__whormu_0() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____whormu_0)); }
	inline bool get__whormu_0() const { return ____whormu_0; }
	inline bool* get_address_of__whormu_0() { return &____whormu_0; }
	inline void set__whormu_0(bool value)
	{
		____whormu_0 = value;
	}

	inline static int32_t get_offset_of__relhouCearcerqar_1() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____relhouCearcerqar_1)); }
	inline bool get__relhouCearcerqar_1() const { return ____relhouCearcerqar_1; }
	inline bool* get_address_of__relhouCearcerqar_1() { return &____relhouCearcerqar_1; }
	inline void set__relhouCearcerqar_1(bool value)
	{
		____relhouCearcerqar_1 = value;
	}

	inline static int32_t get_offset_of__warpi_2() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____warpi_2)); }
	inline String_t* get__warpi_2() const { return ____warpi_2; }
	inline String_t** get_address_of__warpi_2() { return &____warpi_2; }
	inline void set__warpi_2(String_t* value)
	{
		____warpi_2 = value;
		Il2CppCodeGenWriteBarrier(&____warpi_2, value);
	}

	inline static int32_t get_offset_of__kisbearRemaw_3() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____kisbearRemaw_3)); }
	inline bool get__kisbearRemaw_3() const { return ____kisbearRemaw_3; }
	inline bool* get_address_of__kisbearRemaw_3() { return &____kisbearRemaw_3; }
	inline void set__kisbearRemaw_3(bool value)
	{
		____kisbearRemaw_3 = value;
	}

	inline static int32_t get_offset_of__memsidur_4() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____memsidur_4)); }
	inline int32_t get__memsidur_4() const { return ____memsidur_4; }
	inline int32_t* get_address_of__memsidur_4() { return &____memsidur_4; }
	inline void set__memsidur_4(int32_t value)
	{
		____memsidur_4 = value;
	}

	inline static int32_t get_offset_of__tunabu_5() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____tunabu_5)); }
	inline int32_t get__tunabu_5() const { return ____tunabu_5; }
	inline int32_t* get_address_of__tunabu_5() { return &____tunabu_5; }
	inline void set__tunabu_5(int32_t value)
	{
		____tunabu_5 = value;
	}

	inline static int32_t get_offset_of__sorchouca_6() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____sorchouca_6)); }
	inline uint32_t get__sorchouca_6() const { return ____sorchouca_6; }
	inline uint32_t* get_address_of__sorchouca_6() { return &____sorchouca_6; }
	inline void set__sorchouca_6(uint32_t value)
	{
		____sorchouca_6 = value;
	}

	inline static int32_t get_offset_of__dabaw_7() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____dabaw_7)); }
	inline float get__dabaw_7() const { return ____dabaw_7; }
	inline float* get_address_of__dabaw_7() { return &____dabaw_7; }
	inline void set__dabaw_7(float value)
	{
		____dabaw_7 = value;
	}

	inline static int32_t get_offset_of__zima_8() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____zima_8)); }
	inline float get__zima_8() const { return ____zima_8; }
	inline float* get_address_of__zima_8() { return &____zima_8; }
	inline void set__zima_8(float value)
	{
		____zima_8 = value;
	}

	inline static int32_t get_offset_of__jasorFija_9() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____jasorFija_9)); }
	inline String_t* get__jasorFija_9() const { return ____jasorFija_9; }
	inline String_t** get_address_of__jasorFija_9() { return &____jasorFija_9; }
	inline void set__jasorFija_9(String_t* value)
	{
		____jasorFija_9 = value;
		Il2CppCodeGenWriteBarrier(&____jasorFija_9, value);
	}

	inline static int32_t get_offset_of__bemico_10() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____bemico_10)); }
	inline bool get__bemico_10() const { return ____bemico_10; }
	inline bool* get_address_of__bemico_10() { return &____bemico_10; }
	inline void set__bemico_10(bool value)
	{
		____bemico_10 = value;
	}

	inline static int32_t get_offset_of__lustalloKiryai_11() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____lustalloKiryai_11)); }
	inline int32_t get__lustalloKiryai_11() const { return ____lustalloKiryai_11; }
	inline int32_t* get_address_of__lustalloKiryai_11() { return &____lustalloKiryai_11; }
	inline void set__lustalloKiryai_11(int32_t value)
	{
		____lustalloKiryai_11 = value;
	}

	inline static int32_t get_offset_of__stemdustair_12() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____stemdustair_12)); }
	inline bool get__stemdustair_12() const { return ____stemdustair_12; }
	inline bool* get_address_of__stemdustair_12() { return &____stemdustair_12; }
	inline void set__stemdustair_12(bool value)
	{
		____stemdustair_12 = value;
	}

	inline static int32_t get_offset_of__topahis_13() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____topahis_13)); }
	inline uint32_t get__topahis_13() const { return ____topahis_13; }
	inline uint32_t* get_address_of__topahis_13() { return &____topahis_13; }
	inline void set__topahis_13(uint32_t value)
	{
		____topahis_13 = value;
	}

	inline static int32_t get_offset_of__sikeajaLote_14() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____sikeajaLote_14)); }
	inline int32_t get__sikeajaLote_14() const { return ____sikeajaLote_14; }
	inline int32_t* get_address_of__sikeajaLote_14() { return &____sikeajaLote_14; }
	inline void set__sikeajaLote_14(int32_t value)
	{
		____sikeajaLote_14 = value;
	}

	inline static int32_t get_offset_of__harjisparNatru_15() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____harjisparNatru_15)); }
	inline uint32_t get__harjisparNatru_15() const { return ____harjisparNatru_15; }
	inline uint32_t* get_address_of__harjisparNatru_15() { return &____harjisparNatru_15; }
	inline void set__harjisparNatru_15(uint32_t value)
	{
		____harjisparNatru_15 = value;
	}

	inline static int32_t get_offset_of__cazucu_16() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____cazucu_16)); }
	inline bool get__cazucu_16() const { return ____cazucu_16; }
	inline bool* get_address_of__cazucu_16() { return &____cazucu_16; }
	inline void set__cazucu_16(bool value)
	{
		____cazucu_16 = value;
	}

	inline static int32_t get_offset_of__cejasguDriwivair_17() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____cejasguDriwivair_17)); }
	inline String_t* get__cejasguDriwivair_17() const { return ____cejasguDriwivair_17; }
	inline String_t** get_address_of__cejasguDriwivair_17() { return &____cejasguDriwivair_17; }
	inline void set__cejasguDriwivair_17(String_t* value)
	{
		____cejasguDriwivair_17 = value;
		Il2CppCodeGenWriteBarrier(&____cejasguDriwivair_17, value);
	}

	inline static int32_t get_offset_of__jallmaipaXohaycha_18() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____jallmaipaXohaycha_18)); }
	inline uint32_t get__jallmaipaXohaycha_18() const { return ____jallmaipaXohaycha_18; }
	inline uint32_t* get_address_of__jallmaipaXohaycha_18() { return &____jallmaipaXohaycha_18; }
	inline void set__jallmaipaXohaycha_18(uint32_t value)
	{
		____jallmaipaXohaycha_18 = value;
	}

	inline static int32_t get_offset_of__wokalmaSeremo_19() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____wokalmaSeremo_19)); }
	inline int32_t get__wokalmaSeremo_19() const { return ____wokalmaSeremo_19; }
	inline int32_t* get_address_of__wokalmaSeremo_19() { return &____wokalmaSeremo_19; }
	inline void set__wokalmaSeremo_19(int32_t value)
	{
		____wokalmaSeremo_19 = value;
	}

	inline static int32_t get_offset_of__mawarwi_20() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____mawarwi_20)); }
	inline int32_t get__mawarwi_20() const { return ____mawarwi_20; }
	inline int32_t* get_address_of__mawarwi_20() { return &____mawarwi_20; }
	inline void set__mawarwi_20(int32_t value)
	{
		____mawarwi_20 = value;
	}

	inline static int32_t get_offset_of__hemroutuTabouri_21() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____hemroutuTabouri_21)); }
	inline String_t* get__hemroutuTabouri_21() const { return ____hemroutuTabouri_21; }
	inline String_t** get_address_of__hemroutuTabouri_21() { return &____hemroutuTabouri_21; }
	inline void set__hemroutuTabouri_21(String_t* value)
	{
		____hemroutuTabouri_21 = value;
		Il2CppCodeGenWriteBarrier(&____hemroutuTabouri_21, value);
	}

	inline static int32_t get_offset_of__halhatree_22() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____halhatree_22)); }
	inline int32_t get__halhatree_22() const { return ____halhatree_22; }
	inline int32_t* get_address_of__halhatree_22() { return &____halhatree_22; }
	inline void set__halhatree_22(int32_t value)
	{
		____halhatree_22 = value;
	}

	inline static int32_t get_offset_of__whouseci_23() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____whouseci_23)); }
	inline float get__whouseci_23() const { return ____whouseci_23; }
	inline float* get_address_of__whouseci_23() { return &____whouseci_23; }
	inline void set__whouseci_23(float value)
	{
		____whouseci_23 = value;
	}

	inline static int32_t get_offset_of__sairnuyeMerere_24() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____sairnuyeMerere_24)); }
	inline int32_t get__sairnuyeMerere_24() const { return ____sairnuyeMerere_24; }
	inline int32_t* get_address_of__sairnuyeMerere_24() { return &____sairnuyeMerere_24; }
	inline void set__sairnuyeMerere_24(int32_t value)
	{
		____sairnuyeMerere_24 = value;
	}

	inline static int32_t get_offset_of__caisosowCerrisear_25() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____caisosowCerrisear_25)); }
	inline int32_t get__caisosowCerrisear_25() const { return ____caisosowCerrisear_25; }
	inline int32_t* get_address_of__caisosowCerrisear_25() { return &____caisosowCerrisear_25; }
	inline void set__caisosowCerrisear_25(int32_t value)
	{
		____caisosowCerrisear_25 = value;
	}

	inline static int32_t get_offset_of__loceHowbeser_26() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____loceHowbeser_26)); }
	inline bool get__loceHowbeser_26() const { return ____loceHowbeser_26; }
	inline bool* get_address_of__loceHowbeser_26() { return &____loceHowbeser_26; }
	inline void set__loceHowbeser_26(bool value)
	{
		____loceHowbeser_26 = value;
	}

	inline static int32_t get_offset_of__rairlooheMeara_27() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____rairlooheMeara_27)); }
	inline bool get__rairlooheMeara_27() const { return ____rairlooheMeara_27; }
	inline bool* get_address_of__rairlooheMeara_27() { return &____rairlooheMeara_27; }
	inline void set__rairlooheMeara_27(bool value)
	{
		____rairlooheMeara_27 = value;
	}

	inline static int32_t get_offset_of__kookall_28() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____kookall_28)); }
	inline String_t* get__kookall_28() const { return ____kookall_28; }
	inline String_t** get_address_of__kookall_28() { return &____kookall_28; }
	inline void set__kookall_28(String_t* value)
	{
		____kookall_28 = value;
		Il2CppCodeGenWriteBarrier(&____kookall_28, value);
	}

	inline static int32_t get_offset_of__kalnaVairberere_29() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____kalnaVairberere_29)); }
	inline String_t* get__kalnaVairberere_29() const { return ____kalnaVairberere_29; }
	inline String_t** get_address_of__kalnaVairberere_29() { return &____kalnaVairberere_29; }
	inline void set__kalnaVairberere_29(String_t* value)
	{
		____kalnaVairberere_29 = value;
		Il2CppCodeGenWriteBarrier(&____kalnaVairberere_29, value);
	}

	inline static int32_t get_offset_of__moudasurPasqouso_30() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____moudasurPasqouso_30)); }
	inline bool get__moudasurPasqouso_30() const { return ____moudasurPasqouso_30; }
	inline bool* get_address_of__moudasurPasqouso_30() { return &____moudasurPasqouso_30; }
	inline void set__moudasurPasqouso_30(bool value)
	{
		____moudasurPasqouso_30 = value;
	}

	inline static int32_t get_offset_of__xabedorLorgel_31() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____xabedorLorgel_31)); }
	inline bool get__xabedorLorgel_31() const { return ____xabedorLorgel_31; }
	inline bool* get_address_of__xabedorLorgel_31() { return &____xabedorLorgel_31; }
	inline void set__xabedorLorgel_31(bool value)
	{
		____xabedorLorgel_31 = value;
	}

	inline static int32_t get_offset_of__coorama_32() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____coorama_32)); }
	inline String_t* get__coorama_32() const { return ____coorama_32; }
	inline String_t** get_address_of__coorama_32() { return &____coorama_32; }
	inline void set__coorama_32(String_t* value)
	{
		____coorama_32 = value;
		Il2CppCodeGenWriteBarrier(&____coorama_32, value);
	}

	inline static int32_t get_offset_of__deniskemWeharer_33() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____deniskemWeharer_33)); }
	inline int32_t get__deniskemWeharer_33() const { return ____deniskemWeharer_33; }
	inline int32_t* get_address_of__deniskemWeharer_33() { return &____deniskemWeharer_33; }
	inline void set__deniskemWeharer_33(int32_t value)
	{
		____deniskemWeharer_33 = value;
	}

	inline static int32_t get_offset_of__nigallroo_34() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____nigallroo_34)); }
	inline String_t* get__nigallroo_34() const { return ____nigallroo_34; }
	inline String_t** get_address_of__nigallroo_34() { return &____nigallroo_34; }
	inline void set__nigallroo_34(String_t* value)
	{
		____nigallroo_34 = value;
		Il2CppCodeGenWriteBarrier(&____nigallroo_34, value);
	}

	inline static int32_t get_offset_of__whejeSearstaisa_35() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____whejeSearstaisa_35)); }
	inline bool get__whejeSearstaisa_35() const { return ____whejeSearstaisa_35; }
	inline bool* get_address_of__whejeSearstaisa_35() { return &____whejeSearstaisa_35; }
	inline void set__whejeSearstaisa_35(bool value)
	{
		____whejeSearstaisa_35 = value;
	}

	inline static int32_t get_offset_of__xapaw_36() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____xapaw_36)); }
	inline bool get__xapaw_36() const { return ____xapaw_36; }
	inline bool* get_address_of__xapaw_36() { return &____xapaw_36; }
	inline void set__xapaw_36(bool value)
	{
		____xapaw_36 = value;
	}

	inline static int32_t get_offset_of__yugikasYigope_37() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____yugikasYigope_37)); }
	inline float get__yugikasYigope_37() const { return ____yugikasYigope_37; }
	inline float* get_address_of__yugikasYigope_37() { return &____yugikasYigope_37; }
	inline void set__yugikasYigope_37(float value)
	{
		____yugikasYigope_37 = value;
	}

	inline static int32_t get_offset_of__whedowdor_38() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____whedowdor_38)); }
	inline String_t* get__whedowdor_38() const { return ____whedowdor_38; }
	inline String_t** get_address_of__whedowdor_38() { return &____whedowdor_38; }
	inline void set__whedowdor_38(String_t* value)
	{
		____whedowdor_38 = value;
		Il2CppCodeGenWriteBarrier(&____whedowdor_38, value);
	}

	inline static int32_t get_offset_of__seejedri_39() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____seejedri_39)); }
	inline float get__seejedri_39() const { return ____seejedri_39; }
	inline float* get_address_of__seejedri_39() { return &____seejedri_39; }
	inline void set__seejedri_39(float value)
	{
		____seejedri_39 = value;
	}

	inline static int32_t get_offset_of__nafirmi_40() { return static_cast<int32_t>(offsetof(M_rairsearnaw222_t439290617, ____nafirmi_40)); }
	inline String_t* get__nafirmi_40() const { return ____nafirmi_40; }
	inline String_t** get_address_of__nafirmi_40() { return &____nafirmi_40; }
	inline void set__nafirmi_40(String_t* value)
	{
		____nafirmi_40 = value;
		Il2CppCodeGenWriteBarrier(&____nafirmi_40, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
