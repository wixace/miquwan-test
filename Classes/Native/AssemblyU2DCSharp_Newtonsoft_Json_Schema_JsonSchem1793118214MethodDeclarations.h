﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Schema.JsonSchemaWriter
struct JsonSchemaWriter_t1793118214;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// Newtonsoft.Json.Schema.JsonSchemaResolver
struct JsonSchemaResolver_t2728657753;
// Newtonsoft.Json.Schema.JsonSchema
struct JsonSchema_t460567603;
// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchema>
struct IDictionary_2_t858859318;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken>
struct IList_1_t1811925858;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.JsonConverter[]
struct JsonConverterU5BU5D_t638349667;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchema>
struct IList_1_t3155214806;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem2728657753.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchema460567603.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem2115415821.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Nullable_1_gen560925241.h"
#include "mscorlib_System_Nullable_1_gen2199542344.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem1793118214.h"
#include "mscorlib_System_Nullable_1_gen3952353088.h"
#include "mscorlib_System_Nullable_1_gen1237965023.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"

// System.Void Newtonsoft.Json.Schema.JsonSchemaWriter::.ctor(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Schema.JsonSchemaResolver)
extern "C"  void JsonSchemaWriter__ctor_m3948718252 (JsonSchemaWriter_t1793118214 * __this, JsonWriter_t972330355 * ___writer0, JsonSchemaResolver_t2728657753 * ___resolver1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaWriter::ReferenceOrWriteSchema(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  void JsonSchemaWriter_ReferenceOrWriteSchema_m1671254770 (JsonSchemaWriter_t1793118214 * __this, JsonSchema_t460567603 * ___schema0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaWriter::WriteSchema(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  void JsonSchemaWriter_WriteSchema_m1999116124 (JsonSchemaWriter_t1793118214 * __this, JsonSchema_t460567603 * ___schema0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaWriter::WriteSchemaDictionaryIfNotNull(Newtonsoft.Json.JsonWriter,System.String,System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchema>)
extern "C"  void JsonSchemaWriter_WriteSchemaDictionaryIfNotNull_m2142863641 (JsonSchemaWriter_t1793118214 * __this, JsonWriter_t972330355 * ___writer0, String_t* ___propertyName1, Il2CppObject* ___properties2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaWriter::WriteItems(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  void JsonSchemaWriter_WriteItems_m3091439203 (JsonSchemaWriter_t1793118214 * __this, JsonSchema_t460567603 * ___schema0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaWriter::WriteType(System.String,Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Schema.JsonSchemaType)
extern "C"  void JsonSchemaWriter_WriteType_m488222291 (JsonSchemaWriter_t1793118214 * __this, String_t* ___propertyName0, JsonWriter_t972330355 * ___writer1, int32_t ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaWriter::WritePropertyIfNotNull(Newtonsoft.Json.JsonWriter,System.String,System.Object)
extern "C"  void JsonSchemaWriter_WritePropertyIfNotNull_m1827451376 (JsonSchemaWriter_t1793118214 * __this, JsonWriter_t972330355 * ___writer0, String_t* ___propertyName1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Schema.JsonSchemaWriter::<WriteType>m__37F(Newtonsoft.Json.Schema.JsonSchemaType)
extern "C"  bool JsonSchemaWriter_U3CWriteTypeU3Em__37F_m2326349720 (Il2CppObject * __this /* static, unused */, int32_t ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaWriter::ilo_WriteStartObject1(Newtonsoft.Json.JsonWriter)
extern "C"  void JsonSchemaWriter_ilo_WriteStartObject1_m2743756001 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaWriter::ilo_WriteEndObject2(Newtonsoft.Json.JsonWriter)
extern "C"  void JsonSchemaWriter_ilo_WriteEndObject2_m1483625033 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaWriter::ilo_ArgumentNotNull3(System.Object,System.String)
extern "C"  void JsonSchemaWriter_ilo_ArgumentNotNull3_m1139981882 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, String_t* ___parameterName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchemaWriter::ilo_get_Id4(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  String_t* JsonSchemaWriter_ilo_get_Id4_m3783936668 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchemaWriter::ilo_get_Description5(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  String_t* JsonSchemaWriter_ilo_get_Description5_m813622258 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Newtonsoft.Json.Schema.JsonSchemaWriter::ilo_get_ReadOnly6(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  Nullable_1_t560925241  JsonSchemaWriter_ilo_get_ReadOnly6_m2710293243 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Newtonsoft.Json.Schema.JsonSchemaWriter::ilo_get_Hidden7(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  Nullable_1_t560925241  JsonSchemaWriter_ilo_get_Hidden7_m3641587298 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType> Newtonsoft.Json.Schema.JsonSchemaWriter::ilo_get_Type8(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  Nullable_1_t2199542344  JsonSchemaWriter_ilo_get_Type8_m705454379 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaWriter::ilo_WriteType9(Newtonsoft.Json.Schema.JsonSchemaWriter,System.String,Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Schema.JsonSchemaType)
extern "C"  void JsonSchemaWriter_ilo_WriteType9_m4228488365 (Il2CppObject * __this /* static, unused */, JsonSchemaWriter_t1793118214 * ____this0, String_t* ___propertyName1, JsonWriter_t972330355 * ___writer2, int32_t ___type3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaWriter::ilo_WritePropertyName10(Newtonsoft.Json.JsonWriter,System.String)
extern "C"  void JsonSchemaWriter_ilo_WritePropertyName10_m3877199532 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaWriter::ilo_get_AdditionalProperties11(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  JsonSchema_t460567603 * JsonSchemaWriter_ilo_get_AdditionalProperties11_m4059400256 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaWriter::ilo_ReferenceOrWriteSchema12(Newtonsoft.Json.Schema.JsonSchemaWriter,Newtonsoft.Json.Schema.JsonSchema)
extern "C"  void JsonSchemaWriter_ilo_ReferenceOrWriteSchema12_m455247158 (Il2CppObject * __this /* static, unused */, JsonSchemaWriter_t1793118214 * ____this0, JsonSchema_t460567603 * ___schema1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchema> Newtonsoft.Json.Schema.JsonSchemaWriter::ilo_get_Properties13(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  Il2CppObject* JsonSchemaWriter_ilo_get_Properties13_m3546600787 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Double> Newtonsoft.Json.Schema.JsonSchemaWriter::ilo_get_Minimum14(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  Nullable_1_t3952353088  JsonSchemaWriter_ilo_get_Minimum14_m1229549923 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaWriter::ilo_WritePropertyIfNotNull15(Newtonsoft.Json.Schema.JsonSchemaWriter,Newtonsoft.Json.JsonWriter,System.String,System.Object)
extern "C"  void JsonSchemaWriter_ilo_WritePropertyIfNotNull15_m3081148783 (Il2CppObject * __this /* static, unused */, JsonSchemaWriter_t1793118214 * ____this0, JsonWriter_t972330355 * ___writer1, String_t* ___propertyName2, Il2CppObject * ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Newtonsoft.Json.Schema.JsonSchemaWriter::ilo_get_ExclusiveMaximum16(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  Nullable_1_t560925241  JsonSchemaWriter_ilo_get_ExclusiveMaximum16_m2769309962 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Newtonsoft.Json.Schema.JsonSchemaWriter::ilo_get_MaximumItems17(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  Nullable_1_t1237965023  JsonSchemaWriter_ilo_get_MaximumItems17_m2133120497 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchemaWriter::ilo_get_Pattern18(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  String_t* JsonSchemaWriter_ilo_get_Pattern18_m3124847326 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Schema.JsonSchemaWriter::ilo_get_Enum19(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  Il2CppObject* JsonSchemaWriter_ilo_get_Enum19_m95432649 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Schema.JsonSchemaWriter::ilo_get_Default20(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  JToken_t3412245951 * JsonSchemaWriter_ilo_get_Default20_m2159082670 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaWriter::ilo_WriteTo21(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter[])
extern "C"  void JsonSchemaWriter_ilo_WriteTo21_m2984389614 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, JsonWriter_t972330355 * ___writer1, JsonConverterU5BU5D_t638349667* ___converters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaWriter::ilo_WriteStartArray22(Newtonsoft.Json.JsonWriter)
extern "C"  void JsonSchemaWriter_ilo_WriteStartArray22_m1819665896 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchema> Newtonsoft.Json.Schema.JsonSchemaWriter::ilo_get_Items23(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  Il2CppObject* JsonSchemaWriter_ilo_get_Items23_m38115028 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
