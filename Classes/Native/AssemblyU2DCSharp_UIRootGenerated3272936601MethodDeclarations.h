﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIRootGenerated
struct UIRootGenerated_t3272936601;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// UIRoot
struct UIRoot_t2503447958;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_UIRoot_Scaling174157806.h"
#include "AssemblyU2DCSharp_UIRoot2503447958.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UIRootGenerated::.ctor()
extern "C"  void UIRootGenerated__ctor_m669308658 (UIRootGenerated_t3272936601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIRootGenerated::UIRoot_UIRoot1(JSVCall,System.Int32)
extern "C"  bool UIRootGenerated_UIRoot_UIRoot1_m188639296 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootGenerated::UIRoot_list(JSVCall)
extern "C"  void UIRootGenerated_UIRoot_list_m4147313904 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootGenerated::UIRoot_scalingStyle(JSVCall)
extern "C"  void UIRootGenerated_UIRoot_scalingStyle_m3346554020 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootGenerated::UIRoot_manualWidth(JSVCall)
extern "C"  void UIRootGenerated_UIRoot_manualWidth_m1217982622 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootGenerated::UIRoot_manualHeight(JSVCall)
extern "C"  void UIRootGenerated_UIRoot_manualHeight_m4053166561 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootGenerated::UIRoot_minimumHeight(JSVCall)
extern "C"  void UIRootGenerated_UIRoot_minimumHeight_m2081039625 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootGenerated::UIRoot_maximumHeight(JSVCall)
extern "C"  void UIRootGenerated_UIRoot_maximumHeight_m596957367 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootGenerated::UIRoot_fitWidth(JSVCall)
extern "C"  void UIRootGenerated_UIRoot_fitWidth_m1231212345 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootGenerated::UIRoot_fitHeight(JSVCall)
extern "C"  void UIRootGenerated_UIRoot_fitHeight_m168320678 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootGenerated::UIRoot_adjustByDPI(JSVCall)
extern "C"  void UIRootGenerated_UIRoot_adjustByDPI_m1418971367 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootGenerated::UIRoot_shrinkPortraitUI(JSVCall)
extern "C"  void UIRootGenerated_UIRoot_shrinkPortraitUI_m277617782 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootGenerated::UIRoot_constraint(JSVCall)
extern "C"  void UIRootGenerated_UIRoot_constraint_m1052326033 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootGenerated::UIRoot_activeScaling(JSVCall)
extern "C"  void UIRootGenerated_UIRoot_activeScaling_m686298045 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootGenerated::UIRoot_activeHeight(JSVCall)
extern "C"  void UIRootGenerated_UIRoot_activeHeight_m935881377 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootGenerated::UIRoot_pixelSizeAdjustment(JSVCall)
extern "C"  void UIRootGenerated_UIRoot_pixelSizeAdjustment_m2746372170 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIRootGenerated::UIRoot_GetPixelSizeAdjustment__Int32(JSVCall,System.Int32)
extern "C"  bool UIRootGenerated_UIRoot_GetPixelSizeAdjustment__Int32_m1898044821 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIRootGenerated::UIRoot_Broadcast__String__Object(JSVCall,System.Int32)
extern "C"  bool UIRootGenerated_UIRoot_Broadcast__String__Object_m668836438 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIRootGenerated::UIRoot_Broadcast__String(JSVCall,System.Int32)
extern "C"  bool UIRootGenerated_UIRoot_Broadcast__String_m496672343 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIRootGenerated::UIRoot_GetPixelSizeAdjustment__GameObject(JSVCall,System.Int32)
extern "C"  bool UIRootGenerated_UIRoot_GetPixelSizeAdjustment__GameObject_m3266021740 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootGenerated::__Register()
extern "C"  void UIRootGenerated___Register_m1447505173 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIRootGenerated::ilo_getObject1(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIRootGenerated_ilo_getObject1_m2736975539 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootGenerated::ilo_setEnum2(System.Int32,System.Int32)
extern "C"  void UIRootGenerated_ilo_setEnum2_m2687738866 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIRootGenerated::ilo_getEnum3(System.Int32)
extern "C"  int32_t UIRootGenerated_ilo_getEnum3_m1926960388 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootGenerated::ilo_setInt324(System.Int32,System.Int32)
extern "C"  void UIRootGenerated_ilo_setInt324_m3367500097 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIRootGenerated::ilo_getBooleanS5(System.Int32)
extern "C"  bool UIRootGenerated_ilo_getBooleanS5_m2458044086 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootGenerated::ilo_setBooleanS6(System.Int32,System.Boolean)
extern "C"  void UIRootGenerated_ilo_setBooleanS6_m1110232574 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIRoot/Scaling UIRootGenerated::ilo_get_activeScaling7(UIRoot)
extern "C"  int32_t UIRootGenerated_ilo_get_activeScaling7_m3812572575 (Il2CppObject * __this /* static, unused */, UIRoot_t2503447958 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIRootGenerated::ilo_GetPixelSizeAdjustment8(UIRoot,System.Int32)
extern "C"  float UIRootGenerated_ilo_GetPixelSizeAdjustment8_m1161023562 (Il2CppObject * __this /* static, unused */, UIRoot_t2503447958 * ____this0, int32_t ___height1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootGenerated::ilo_setSingle9(System.Int32,System.Single)
extern "C"  void UIRootGenerated_ilo_setSingle9_m4121227338 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UIRootGenerated::ilo_getStringS10(System.Int32)
extern "C"  String_t* UIRootGenerated_ilo_getStringS10_m2782577342 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIRootGenerated::ilo_GetPixelSizeAdjustment11(UnityEngine.GameObject)
extern "C"  float UIRootGenerated_ilo_GetPixelSizeAdjustment11_m2990320591 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
