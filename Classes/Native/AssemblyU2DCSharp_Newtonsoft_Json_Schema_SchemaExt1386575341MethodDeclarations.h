﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Schema.SchemaExtensions/<IsValid>c__AnonStorey130
struct U3CIsValidU3Ec__AnonStorey130_t1386575341;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Schema.ValidationEventArgs
struct ValidationEventArgs_t623166840;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_Validation623166840.h"

// System.Void Newtonsoft.Json.Schema.SchemaExtensions/<IsValid>c__AnonStorey130::.ctor()
extern "C"  void U3CIsValidU3Ec__AnonStorey130__ctor_m1071325726 (U3CIsValidU3Ec__AnonStorey130_t1386575341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.SchemaExtensions/<IsValid>c__AnonStorey130::<>m__380(System.Object,Newtonsoft.Json.Schema.ValidationEventArgs)
extern "C"  void U3CIsValidU3Ec__AnonStorey130_U3CU3Em__380_m4242913906 (U3CIsValidU3Ec__AnonStorey130_t1386575341 * __this, Il2CppObject * ___sender0, ValidationEventArgs_t623166840 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
