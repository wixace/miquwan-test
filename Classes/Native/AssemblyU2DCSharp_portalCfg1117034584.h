﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// portalCfg
struct  portalCfg_t1117034584  : public CsCfgBase_t69924517
{
public:
	// System.Int32 portalCfg::id
	int32_t ___id_0;
	// System.String portalCfg::name
	String_t* ___name_1;
	// System.Int32 portalCfg::theWay
	int32_t ___theWay_2;
	// System.Single portalCfg::bigTime
	float ___bigTime_3;
	// System.String portalCfg::enemyArr
	String_t* ___enemyArr_4;
	// System.Single portalCfg::delayTime
	float ___delayTime_5;
	// System.Int32 portalCfg::brushType
	int32_t ___brushType_6;
	// System.Single portalCfg::radius
	float ___radius_7;
	// System.Int32 portalCfg::active
	int32_t ___active_8;
	// System.Single portalCfg::cdTime
	float ___cdTime_9;
	// System.Int32 portalCfg::effectId
	int32_t ___effectId_10;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(portalCfg_t1117034584, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(portalCfg_t1117034584, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_theWay_2() { return static_cast<int32_t>(offsetof(portalCfg_t1117034584, ___theWay_2)); }
	inline int32_t get_theWay_2() const { return ___theWay_2; }
	inline int32_t* get_address_of_theWay_2() { return &___theWay_2; }
	inline void set_theWay_2(int32_t value)
	{
		___theWay_2 = value;
	}

	inline static int32_t get_offset_of_bigTime_3() { return static_cast<int32_t>(offsetof(portalCfg_t1117034584, ___bigTime_3)); }
	inline float get_bigTime_3() const { return ___bigTime_3; }
	inline float* get_address_of_bigTime_3() { return &___bigTime_3; }
	inline void set_bigTime_3(float value)
	{
		___bigTime_3 = value;
	}

	inline static int32_t get_offset_of_enemyArr_4() { return static_cast<int32_t>(offsetof(portalCfg_t1117034584, ___enemyArr_4)); }
	inline String_t* get_enemyArr_4() const { return ___enemyArr_4; }
	inline String_t** get_address_of_enemyArr_4() { return &___enemyArr_4; }
	inline void set_enemyArr_4(String_t* value)
	{
		___enemyArr_4 = value;
		Il2CppCodeGenWriteBarrier(&___enemyArr_4, value);
	}

	inline static int32_t get_offset_of_delayTime_5() { return static_cast<int32_t>(offsetof(portalCfg_t1117034584, ___delayTime_5)); }
	inline float get_delayTime_5() const { return ___delayTime_5; }
	inline float* get_address_of_delayTime_5() { return &___delayTime_5; }
	inline void set_delayTime_5(float value)
	{
		___delayTime_5 = value;
	}

	inline static int32_t get_offset_of_brushType_6() { return static_cast<int32_t>(offsetof(portalCfg_t1117034584, ___brushType_6)); }
	inline int32_t get_brushType_6() const { return ___brushType_6; }
	inline int32_t* get_address_of_brushType_6() { return &___brushType_6; }
	inline void set_brushType_6(int32_t value)
	{
		___brushType_6 = value;
	}

	inline static int32_t get_offset_of_radius_7() { return static_cast<int32_t>(offsetof(portalCfg_t1117034584, ___radius_7)); }
	inline float get_radius_7() const { return ___radius_7; }
	inline float* get_address_of_radius_7() { return &___radius_7; }
	inline void set_radius_7(float value)
	{
		___radius_7 = value;
	}

	inline static int32_t get_offset_of_active_8() { return static_cast<int32_t>(offsetof(portalCfg_t1117034584, ___active_8)); }
	inline int32_t get_active_8() const { return ___active_8; }
	inline int32_t* get_address_of_active_8() { return &___active_8; }
	inline void set_active_8(int32_t value)
	{
		___active_8 = value;
	}

	inline static int32_t get_offset_of_cdTime_9() { return static_cast<int32_t>(offsetof(portalCfg_t1117034584, ___cdTime_9)); }
	inline float get_cdTime_9() const { return ___cdTime_9; }
	inline float* get_address_of_cdTime_9() { return &___cdTime_9; }
	inline void set_cdTime_9(float value)
	{
		___cdTime_9 = value;
	}

	inline static int32_t get_offset_of_effectId_10() { return static_cast<int32_t>(offsetof(portalCfg_t1117034584, ___effectId_10)); }
	inline int32_t get_effectId_10() const { return ___effectId_10; }
	inline int32_t* get_address_of_effectId_10() { return &___effectId_10; }
	inline void set_effectId_10(int32_t value)
	{
		___effectId_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
