﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_jewe150
struct M_jewe150_t804721123;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_jewe150804721123.h"

// System.Void GarbageiOS.M_jewe150::.ctor()
extern "C"  void M_jewe150__ctor_m3974368 (M_jewe150_t804721123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jewe150::M_temjas0(System.String[],System.Int32)
extern "C"  void M_jewe150_M_temjas0_m1750506259 (M_jewe150_t804721123 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jewe150::M_reemarDawbetow1(System.String[],System.Int32)
extern "C"  void M_jewe150_M_reemarDawbetow1_m2448908521 (M_jewe150_t804721123 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jewe150::M_salchigeeChotowpo2(System.String[],System.Int32)
extern "C"  void M_jewe150_M_salchigeeChotowpo2_m1515927821 (M_jewe150_t804721123 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jewe150::M_yaicurjar3(System.String[],System.Int32)
extern "C"  void M_jewe150_M_yaicurjar3_m1843348004 (M_jewe150_t804721123 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jewe150::ilo_M_salchigeeChotowpo21(GarbageiOS.M_jewe150,System.String[],System.Int32)
extern "C"  void M_jewe150_ilo_M_salchigeeChotowpo21_m2741348526 (Il2CppObject * __this /* static, unused */, M_jewe150_t804721123 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
