﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_jebemWhisxelree61
struct M_jebemWhisxelree61_t885964312;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_jebemWhisxelree61::.ctor()
extern "C"  void M_jebemWhisxelree61__ctor_m2957846091 (M_jebemWhisxelree61_t885964312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jebemWhisxelree61::M_jeace0(System.String[],System.Int32)
extern "C"  void M_jebemWhisxelree61_M_jeace0_m2831521706 (M_jebemWhisxelree61_t885964312 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jebemWhisxelree61::M_stemdar1(System.String[],System.Int32)
extern "C"  void M_jebemWhisxelree61_M_stemdar1_m4054967847 (M_jebemWhisxelree61_t885964312 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jebemWhisxelree61::M_pemcisjar2(System.String[],System.Int32)
extern "C"  void M_jebemWhisxelree61_M_pemcisjar2_m769514382 (M_jebemWhisxelree61_t885964312 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
