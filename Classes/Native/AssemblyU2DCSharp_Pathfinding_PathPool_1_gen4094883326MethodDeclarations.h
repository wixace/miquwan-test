﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_PathPool_1_gen4134265632MethodDeclarations.h"

// System.Void Pathfinding.PathPool`1<Pathfinding.MultiTargetPath>::.cctor()
#define PathPool_1__cctor_m528357339(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))PathPool_1__cctor_m4111071586_gshared)(__this /* static, unused */, method)
// System.Void Pathfinding.PathPool`1<Pathfinding.MultiTargetPath>::Recycle(T)
#define PathPool_1_Recycle_m2936047739(__this /* static, unused */, ___path0, method) ((  void (*) (Il2CppObject * /* static, unused */, MultiTargetPath_t4131434065 *, const MethodInfo*))PathPool_1_Recycle_m1360667714_gshared)(__this /* static, unused */, ___path0, method)
// System.Void Pathfinding.PathPool`1<Pathfinding.MultiTargetPath>::Warmup(System.Int32,System.Int32)
#define PathPool_1_Warmup_m342436564(__this /* static, unused */, ___count0, ___length1, method) ((  void (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))PathPool_1_Warmup_m4243013421_gshared)(__this /* static, unused */, ___count0, ___length1, method)
// System.Int32 Pathfinding.PathPool`1<Pathfinding.MultiTargetPath>::GetTotalCreated()
#define PathPool_1_GetTotalCreated_m3597434616(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))PathPool_1_GetTotalCreated_m3938489809_gshared)(__this /* static, unused */, method)
// System.Int32 Pathfinding.PathPool`1<Pathfinding.MultiTargetPath>::GetSize()
#define PathPool_1_GetSize_m3012358069(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))PathPool_1_GetSize_m2941120398_gshared)(__this /* static, unused */, method)
// T Pathfinding.PathPool`1<Pathfinding.MultiTargetPath>::GetPath()
#define PathPool_1_GetPath_m3854083210(__this /* static, unused */, method) ((  MultiTargetPath_t4131434065 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))PathPool_1_GetPath_m3122724387_gshared)(__this /* static, unused */, method)
// System.Void Pathfinding.PathPool`1<Pathfinding.MultiTargetPath>::ilo_Claim1(Pathfinding.Path,System.Object)
#define PathPool_1_ilo_Claim1_m3414664377(__this /* static, unused */, ____this0, ___o1, method) ((  void (*) (Il2CppObject * /* static, unused */, Path_t1974241691 *, Il2CppObject *, const MethodInfo*))PathPool_1_ilo_Claim1_m992502912_gshared)(__this /* static, unused */, ____this0, ___o1, method)
// System.Void Pathfinding.PathPool`1<Pathfinding.MultiTargetPath>::ilo_Release2(Pathfinding.Path,System.Object)
#define PathPool_1_ilo_Release2_m2398158159(__this /* static, unused */, ____this0, ___o1, method) ((  void (*) (Il2CppObject * /* static, unused */, Path_t1974241691 *, Il2CppObject *, const MethodInfo*))PathPool_1_ilo_Release2_m2573264726_gshared)(__this /* static, unused */, ____this0, ___o1, method)
