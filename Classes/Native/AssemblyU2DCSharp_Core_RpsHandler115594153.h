﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<Core.RpsStatistics>
struct Action_1_t1443613718;
// Core.RpsStatistics
struct RpsStatistics_t1047797582;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.RpsHandler
struct  RpsHandler_t115594153  : public Il2CppObject
{
public:
	// System.Action`1<Core.RpsStatistics> Core.RpsHandler::OnStatisticsChange
	Action_1_t1443613718 * ___OnStatisticsChange_0;
	// Core.RpsStatistics Core.RpsHandler::_rpsStatistics
	RpsStatistics_t1047797582 * ____rpsStatistics_1;

public:
	inline static int32_t get_offset_of_OnStatisticsChange_0() { return static_cast<int32_t>(offsetof(RpsHandler_t115594153, ___OnStatisticsChange_0)); }
	inline Action_1_t1443613718 * get_OnStatisticsChange_0() const { return ___OnStatisticsChange_0; }
	inline Action_1_t1443613718 ** get_address_of_OnStatisticsChange_0() { return &___OnStatisticsChange_0; }
	inline void set_OnStatisticsChange_0(Action_1_t1443613718 * value)
	{
		___OnStatisticsChange_0 = value;
		Il2CppCodeGenWriteBarrier(&___OnStatisticsChange_0, value);
	}

	inline static int32_t get_offset_of__rpsStatistics_1() { return static_cast<int32_t>(offsetof(RpsHandler_t115594153, ____rpsStatistics_1)); }
	inline RpsStatistics_t1047797582 * get__rpsStatistics_1() const { return ____rpsStatistics_1; }
	inline RpsStatistics_t1047797582 ** get_address_of__rpsStatistics_1() { return &____rpsStatistics_1; }
	inline void set__rpsStatistics_1(RpsStatistics_t1047797582 * value)
	{
		____rpsStatistics_1 = value;
		Il2CppCodeGenWriteBarrier(&____rpsStatistics_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
