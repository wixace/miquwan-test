﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.NodeLink
struct NodeLink_t305725842;
// UnityEngine.Transform
struct Transform_t1659122786;
// AstarPath
struct AstarPath_t4090270936;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_NodeLink305725842.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "AssemblyU2DCSharp_AstarPath_AstarWorkItem2566693888.h"
#include "AssemblyU2DCSharp_Pathfinding_NNInfo1570625892.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.NodeLink::.ctor()
extern "C"  void NodeLink__ctor_m3586356501 (NodeLink_t305725842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Pathfinding.NodeLink::get_Start()
extern "C"  Transform_t1659122786 * NodeLink_get_Start_m580739214 (NodeLink_t305725842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Pathfinding.NodeLink::get_End()
extern "C"  Transform_t1659122786 * NodeLink_get_End_m3634420103 (NodeLink_t305725842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink::OnPostScan()
extern "C"  void NodeLink_OnPostScan_m2313201227 (NodeLink_t305725842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink::InternalOnPostScan()
extern "C"  void NodeLink_InternalOnPostScan_m2876415912 (NodeLink_t305725842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink::OnGraphsPostUpdate()
extern "C"  void NodeLink_OnGraphsPostUpdate_m349140508 (NodeLink_t305725842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink::Apply()
extern "C"  void NodeLink_Apply_m3637459489 (NodeLink_t305725842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink::OnDrawGizmos()
extern "C"  void NodeLink_OnDrawGizmos_m266447467 (NodeLink_t305725842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink::DrawGizmoBezier(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void NodeLink_DrawGizmoBezier_m3447487588 (NodeLink_t305725842 * __this, Vector3_t4282066566  ___p10, Vector3_t4282066566  ___p21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NodeLink::<OnPostScan>m__339(System.Boolean)
extern "C"  bool NodeLink_U3COnPostScanU3Em__339_m2071028652 (NodeLink_t305725842 * __this, bool ___force0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NodeLink::<OnGraphsPostUpdate>m__33A(System.Boolean)
extern "C"  bool NodeLink_U3COnGraphsPostUpdateU3Em__33A_m527985795 (NodeLink_t305725842 * __this, bool ___force0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink::ilo_InternalOnPostScan1(Pathfinding.NodeLink)
extern "C"  void NodeLink_ilo_InternalOnPostScan1_m1910068434 (Il2CppObject * __this /* static, unused */, NodeLink_t305725842 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink::ilo_AddWorkItem2(AstarPath,AstarPath/AstarWorkItem)
extern "C"  void NodeLink_ilo_AddWorkItem2_m62019031 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, AstarWorkItem_t2566693888  ___itm1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Pathfinding.NodeLink::ilo_get_End3(Pathfinding.NodeLink)
extern "C"  Transform_t1659122786 * NodeLink_ilo_get_End3_m2392472987 (Il2CppObject * __this /* static, unused */, NodeLink_t305725842 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.NodeLink::ilo_GetNearest4(AstarPath,UnityEngine.Vector3)
extern "C"  NNInfo_t1570625892  NodeLink_ilo_GetNearest4_m3878681830 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, Vector3_t4282066566  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.NodeLink::ilo_op_Subtraction5(Pathfinding.Int3,Pathfinding.Int3)
extern "C"  Int3_t1974045594  NodeLink_ilo_op_Subtraction5_m3828650518 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___lhs0, Int3_t1974045594  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.NodeLink::ilo_get_costMagnitude6(Pathfinding.Int3&)
extern "C"  int32_t NodeLink_ilo_get_costMagnitude6_m973083406 (Il2CppObject * __this /* static, unused */, Int3_t1974045594 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink::ilo_AddConnection7(Pathfinding.GraphNode,Pathfinding.GraphNode,System.UInt32)
extern "C"  void NodeLink_ilo_AddConnection7_m3926311692 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, GraphNode_t23612370 * ___node1, uint32_t ___cost2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
