﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ProjectorGenerated
struct UnityEngine_ProjectorGenerated_t3113162139;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_ProjectorGenerated::.ctor()
extern "C"  void UnityEngine_ProjectorGenerated__ctor_m3842680160 (UnityEngine_ProjectorGenerated_t3113162139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProjectorGenerated::Projector_Projector1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProjectorGenerated_Projector_Projector1_m4060912800 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProjectorGenerated::Projector_nearClipPlane(JSVCall)
extern "C"  void UnityEngine_ProjectorGenerated_Projector_nearClipPlane_m2886233704 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProjectorGenerated::Projector_farClipPlane(JSVCall)
extern "C"  void UnityEngine_ProjectorGenerated_Projector_farClipPlane_m2024591051 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProjectorGenerated::Projector_fieldOfView(JSVCall)
extern "C"  void UnityEngine_ProjectorGenerated_Projector_fieldOfView_m3366094614 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProjectorGenerated::Projector_aspectRatio(JSVCall)
extern "C"  void UnityEngine_ProjectorGenerated_Projector_aspectRatio_m1142697945 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProjectorGenerated::Projector_orthographic(JSVCall)
extern "C"  void UnityEngine_ProjectorGenerated_Projector_orthographic_m1365210256 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProjectorGenerated::Projector_orthographicSize(JSVCall)
extern "C"  void UnityEngine_ProjectorGenerated_Projector_orthographicSize_m2873073807 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProjectorGenerated::Projector_ignoreLayers(JSVCall)
extern "C"  void UnityEngine_ProjectorGenerated_Projector_ignoreLayers_m1145315212 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProjectorGenerated::Projector_material(JSVCall)
extern "C"  void UnityEngine_ProjectorGenerated_Projector_material_m3537842809 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProjectorGenerated::__Register()
extern "C"  void UnityEngine_ProjectorGenerated___Register_m1246180199 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ProjectorGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_ProjectorGenerated_ilo_getObject1_m718587186 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProjectorGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_ProjectorGenerated_ilo_attachFinalizerObject2_m194190784 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProjectorGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void UnityEngine_ProjectorGenerated_ilo_addJSCSRel3_m3531265310 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProjectorGenerated::ilo_setSingle4(System.Int32,System.Single)
extern "C"  void UnityEngine_ProjectorGenerated_ilo_setSingle4_m1113007255 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_ProjectorGenerated::ilo_getSingle5(System.Int32)
extern "C"  float UnityEngine_ProjectorGenerated_ilo_getSingle5_m3266008011 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_ProjectorGenerated::ilo_getObject6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_ProjectorGenerated_ilo_getObject6_m1268121556 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
