﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3749455283MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,Pathfinding.PointNode,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m4110412607(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t190157422 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m3261693269_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,Pathfinding.PointNode,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2476413405(__this, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Transform_1_t190157422 *, Int3_t1974045594 , PointNode_t2761813780 *, const MethodInfo*))Transform_1_Invoke_m1173649543_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,Pathfinding.PointNode,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m3652310332(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t190157422 *, Int3_t1974045594 , PointNode_t2761813780 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m1562534886_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,Pathfinding.PointNode,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m679916173(__this, ___result0, method) ((  DictionaryEntry_t1751606614  (*) (Transform_1_t190157422 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m2557042083_gshared)(__this, ___result0, method)
