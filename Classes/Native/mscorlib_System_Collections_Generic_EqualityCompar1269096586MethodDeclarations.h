﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.LocalAvoidance/IntersectionPair>
struct DefaultComparer_t1269096586;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_Inter2821319919.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.LocalAvoidance/IntersectionPair>::.ctor()
extern "C"  void DefaultComparer__ctor_m2337999341_gshared (DefaultComparer_t1269096586 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2337999341(__this, method) ((  void (*) (DefaultComparer_t1269096586 *, const MethodInfo*))DefaultComparer__ctor_m2337999341_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.LocalAvoidance/IntersectionPair>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2011511070_gshared (DefaultComparer_t1269096586 * __this, IntersectionPair_t2821319919  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2011511070(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1269096586 *, IntersectionPair_t2821319919 , const MethodInfo*))DefaultComparer_GetHashCode_m2011511070_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.LocalAvoidance/IntersectionPair>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m627168830_gshared (DefaultComparer_t1269096586 * __this, IntersectionPair_t2821319919  ___x0, IntersectionPair_t2821319919  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m627168830(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1269096586 *, IntersectionPair_t2821319919 , IntersectionPair_t2821319919 , const MethodInfo*))DefaultComparer_Equals_m627168830_gshared)(__this, ___x0, ___y1, method)
