﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.RVO.RVOController
struct RVOController_t1639282485;
// Pathfinding.Path
struct Path_t1974241691;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// Seeker
struct Seeker_t2472610117;
// UnityEngine.MeshRenderer[]
struct MeshRendererU5BU5D_t3685338461;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RVOExampleAgent
struct  RVOExampleAgent_t1174908390  : public MonoBehaviour_t667441552
{
public:
	// System.Single RVOExampleAgent::repathRate
	float ___repathRate_2;
	// System.Single RVOExampleAgent::nextRepath
	float ___nextRepath_3;
	// UnityEngine.Vector3 RVOExampleAgent::target
	Vector3_t4282066566  ___target_4;
	// System.Boolean RVOExampleAgent::canSearchAgain
	bool ___canSearchAgain_5;
	// Pathfinding.RVO.RVOController RVOExampleAgent::controller
	RVOController_t1639282485 * ___controller_6;
	// Pathfinding.Path RVOExampleAgent::path
	Path_t1974241691 * ___path_7;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> RVOExampleAgent::vectorPath
	List_1_t1355284822 * ___vectorPath_8;
	// System.Int32 RVOExampleAgent::wp
	int32_t ___wp_9;
	// System.Single RVOExampleAgent::moveNextDist
	float ___moveNextDist_10;
	// Seeker RVOExampleAgent::seeker
	Seeker_t2472610117 * ___seeker_11;
	// UnityEngine.MeshRenderer[] RVOExampleAgent::rends
	MeshRendererU5BU5D_t3685338461* ___rends_12;

public:
	inline static int32_t get_offset_of_repathRate_2() { return static_cast<int32_t>(offsetof(RVOExampleAgent_t1174908390, ___repathRate_2)); }
	inline float get_repathRate_2() const { return ___repathRate_2; }
	inline float* get_address_of_repathRate_2() { return &___repathRate_2; }
	inline void set_repathRate_2(float value)
	{
		___repathRate_2 = value;
	}

	inline static int32_t get_offset_of_nextRepath_3() { return static_cast<int32_t>(offsetof(RVOExampleAgent_t1174908390, ___nextRepath_3)); }
	inline float get_nextRepath_3() const { return ___nextRepath_3; }
	inline float* get_address_of_nextRepath_3() { return &___nextRepath_3; }
	inline void set_nextRepath_3(float value)
	{
		___nextRepath_3 = value;
	}

	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(RVOExampleAgent_t1174908390, ___target_4)); }
	inline Vector3_t4282066566  get_target_4() const { return ___target_4; }
	inline Vector3_t4282066566 * get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Vector3_t4282066566  value)
	{
		___target_4 = value;
	}

	inline static int32_t get_offset_of_canSearchAgain_5() { return static_cast<int32_t>(offsetof(RVOExampleAgent_t1174908390, ___canSearchAgain_5)); }
	inline bool get_canSearchAgain_5() const { return ___canSearchAgain_5; }
	inline bool* get_address_of_canSearchAgain_5() { return &___canSearchAgain_5; }
	inline void set_canSearchAgain_5(bool value)
	{
		___canSearchAgain_5 = value;
	}

	inline static int32_t get_offset_of_controller_6() { return static_cast<int32_t>(offsetof(RVOExampleAgent_t1174908390, ___controller_6)); }
	inline RVOController_t1639282485 * get_controller_6() const { return ___controller_6; }
	inline RVOController_t1639282485 ** get_address_of_controller_6() { return &___controller_6; }
	inline void set_controller_6(RVOController_t1639282485 * value)
	{
		___controller_6 = value;
		Il2CppCodeGenWriteBarrier(&___controller_6, value);
	}

	inline static int32_t get_offset_of_path_7() { return static_cast<int32_t>(offsetof(RVOExampleAgent_t1174908390, ___path_7)); }
	inline Path_t1974241691 * get_path_7() const { return ___path_7; }
	inline Path_t1974241691 ** get_address_of_path_7() { return &___path_7; }
	inline void set_path_7(Path_t1974241691 * value)
	{
		___path_7 = value;
		Il2CppCodeGenWriteBarrier(&___path_7, value);
	}

	inline static int32_t get_offset_of_vectorPath_8() { return static_cast<int32_t>(offsetof(RVOExampleAgent_t1174908390, ___vectorPath_8)); }
	inline List_1_t1355284822 * get_vectorPath_8() const { return ___vectorPath_8; }
	inline List_1_t1355284822 ** get_address_of_vectorPath_8() { return &___vectorPath_8; }
	inline void set_vectorPath_8(List_1_t1355284822 * value)
	{
		___vectorPath_8 = value;
		Il2CppCodeGenWriteBarrier(&___vectorPath_8, value);
	}

	inline static int32_t get_offset_of_wp_9() { return static_cast<int32_t>(offsetof(RVOExampleAgent_t1174908390, ___wp_9)); }
	inline int32_t get_wp_9() const { return ___wp_9; }
	inline int32_t* get_address_of_wp_9() { return &___wp_9; }
	inline void set_wp_9(int32_t value)
	{
		___wp_9 = value;
	}

	inline static int32_t get_offset_of_moveNextDist_10() { return static_cast<int32_t>(offsetof(RVOExampleAgent_t1174908390, ___moveNextDist_10)); }
	inline float get_moveNextDist_10() const { return ___moveNextDist_10; }
	inline float* get_address_of_moveNextDist_10() { return &___moveNextDist_10; }
	inline void set_moveNextDist_10(float value)
	{
		___moveNextDist_10 = value;
	}

	inline static int32_t get_offset_of_seeker_11() { return static_cast<int32_t>(offsetof(RVOExampleAgent_t1174908390, ___seeker_11)); }
	inline Seeker_t2472610117 * get_seeker_11() const { return ___seeker_11; }
	inline Seeker_t2472610117 ** get_address_of_seeker_11() { return &___seeker_11; }
	inline void set_seeker_11(Seeker_t2472610117 * value)
	{
		___seeker_11 = value;
		Il2CppCodeGenWriteBarrier(&___seeker_11, value);
	}

	inline static int32_t get_offset_of_rends_12() { return static_cast<int32_t>(offsetof(RVOExampleAgent_t1174908390, ___rends_12)); }
	inline MeshRendererU5BU5D_t3685338461* get_rends_12() const { return ___rends_12; }
	inline MeshRendererU5BU5D_t3685338461** get_address_of_rends_12() { return &___rends_12; }
	inline void set_rends_12(MeshRendererU5BU5D_t3685338461* value)
	{
		___rends_12 = value;
		Il2CppCodeGenWriteBarrier(&___rends_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
