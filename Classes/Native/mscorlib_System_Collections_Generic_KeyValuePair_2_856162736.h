﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "mscorlib_System_ValueType1744280289.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,UnityEngine.GameObject>
struct  KeyValuePair_2_t856162736 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	Int3_t1974045594  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	GameObject_t3674682005 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t856162736, ___key_0)); }
	inline Int3_t1974045594  get_key_0() const { return ___key_0; }
	inline Int3_t1974045594 * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(Int3_t1974045594  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t856162736, ___value_1)); }
	inline GameObject_t3674682005 * get_value_1() const { return ___value_1; }
	inline GameObject_t3674682005 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(GameObject_t3674682005 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
