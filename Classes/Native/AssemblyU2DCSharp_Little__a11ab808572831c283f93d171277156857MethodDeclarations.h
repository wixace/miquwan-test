﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._a11ab808572831c283f93d17c7342d01
struct _a11ab808572831c283f93d17c7342d01_t1277156857;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__a11ab808572831c283f93d171277156857.h"

// System.Void Little._a11ab808572831c283f93d17c7342d01::.ctor()
extern "C"  void _a11ab808572831c283f93d17c7342d01__ctor_m2761617492 (_a11ab808572831c283f93d17c7342d01_t1277156857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._a11ab808572831c283f93d17c7342d01::_a11ab808572831c283f93d17c7342d01m2(System.Int32)
extern "C"  int32_t _a11ab808572831c283f93d17c7342d01__a11ab808572831c283f93d17c7342d01m2_m1794180857 (_a11ab808572831c283f93d17c7342d01_t1277156857 * __this, int32_t ____a11ab808572831c283f93d17c7342d01a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._a11ab808572831c283f93d17c7342d01::_a11ab808572831c283f93d17c7342d01m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _a11ab808572831c283f93d17c7342d01__a11ab808572831c283f93d17c7342d01m_m1048603613 (_a11ab808572831c283f93d17c7342d01_t1277156857 * __this, int32_t ____a11ab808572831c283f93d17c7342d01a0, int32_t ____a11ab808572831c283f93d17c7342d01611, int32_t ____a11ab808572831c283f93d17c7342d01c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._a11ab808572831c283f93d17c7342d01::ilo__a11ab808572831c283f93d17c7342d01m21(Little._a11ab808572831c283f93d17c7342d01,System.Int32)
extern "C"  int32_t _a11ab808572831c283f93d17c7342d01_ilo__a11ab808572831c283f93d17c7342d01m21_m3571101152 (Il2CppObject * __this /* static, unused */, _a11ab808572831c283f93d17c7342d01_t1277156857 * ____this0, int32_t ____a11ab808572831c283f93d17c7342d01a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
