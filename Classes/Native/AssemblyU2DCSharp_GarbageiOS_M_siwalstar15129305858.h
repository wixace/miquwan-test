﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_siwalstar15
struct  M_siwalstar15_t129305858  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_siwalstar15::_rofurpir
	uint32_t ____rofurpir_0;
	// System.String GarbageiOS.M_siwalstar15::_deehiYelma
	String_t* ____deehiYelma_1;
	// System.String GarbageiOS.M_siwalstar15::_jelsayDoteafi
	String_t* ____jelsayDoteafi_2;
	// System.UInt32 GarbageiOS.M_siwalstar15::_xurdowLacur
	uint32_t ____xurdowLacur_3;
	// System.Int32 GarbageiOS.M_siwalstar15::_goonirqowZainas
	int32_t ____goonirqowZainas_4;
	// System.Single GarbageiOS.M_siwalstar15::_tawyoupar
	float ____tawyoupar_5;
	// System.Int32 GarbageiOS.M_siwalstar15::_merlirKemtawyis
	int32_t ____merlirKemtawyis_6;
	// System.UInt32 GarbageiOS.M_siwalstar15::_teelabas
	uint32_t ____teelabas_7;
	// System.Single GarbageiOS.M_siwalstar15::_desistreRairnouwhaw
	float ____desistreRairnouwhaw_8;
	// System.Single GarbageiOS.M_siwalstar15::_tellemjallTowtrowral
	float ____tellemjallTowtrowral_9;
	// System.Single GarbageiOS.M_siwalstar15::_pirorWernur
	float ____pirorWernur_10;
	// System.UInt32 GarbageiOS.M_siwalstar15::_kecallnu
	uint32_t ____kecallnu_11;
	// System.Boolean GarbageiOS.M_siwalstar15::_seltereHurwhallnel
	bool ____seltereHurwhallnel_12;

public:
	inline static int32_t get_offset_of__rofurpir_0() { return static_cast<int32_t>(offsetof(M_siwalstar15_t129305858, ____rofurpir_0)); }
	inline uint32_t get__rofurpir_0() const { return ____rofurpir_0; }
	inline uint32_t* get_address_of__rofurpir_0() { return &____rofurpir_0; }
	inline void set__rofurpir_0(uint32_t value)
	{
		____rofurpir_0 = value;
	}

	inline static int32_t get_offset_of__deehiYelma_1() { return static_cast<int32_t>(offsetof(M_siwalstar15_t129305858, ____deehiYelma_1)); }
	inline String_t* get__deehiYelma_1() const { return ____deehiYelma_1; }
	inline String_t** get_address_of__deehiYelma_1() { return &____deehiYelma_1; }
	inline void set__deehiYelma_1(String_t* value)
	{
		____deehiYelma_1 = value;
		Il2CppCodeGenWriteBarrier(&____deehiYelma_1, value);
	}

	inline static int32_t get_offset_of__jelsayDoteafi_2() { return static_cast<int32_t>(offsetof(M_siwalstar15_t129305858, ____jelsayDoteafi_2)); }
	inline String_t* get__jelsayDoteafi_2() const { return ____jelsayDoteafi_2; }
	inline String_t** get_address_of__jelsayDoteafi_2() { return &____jelsayDoteafi_2; }
	inline void set__jelsayDoteafi_2(String_t* value)
	{
		____jelsayDoteafi_2 = value;
		Il2CppCodeGenWriteBarrier(&____jelsayDoteafi_2, value);
	}

	inline static int32_t get_offset_of__xurdowLacur_3() { return static_cast<int32_t>(offsetof(M_siwalstar15_t129305858, ____xurdowLacur_3)); }
	inline uint32_t get__xurdowLacur_3() const { return ____xurdowLacur_3; }
	inline uint32_t* get_address_of__xurdowLacur_3() { return &____xurdowLacur_3; }
	inline void set__xurdowLacur_3(uint32_t value)
	{
		____xurdowLacur_3 = value;
	}

	inline static int32_t get_offset_of__goonirqowZainas_4() { return static_cast<int32_t>(offsetof(M_siwalstar15_t129305858, ____goonirqowZainas_4)); }
	inline int32_t get__goonirqowZainas_4() const { return ____goonirqowZainas_4; }
	inline int32_t* get_address_of__goonirqowZainas_4() { return &____goonirqowZainas_4; }
	inline void set__goonirqowZainas_4(int32_t value)
	{
		____goonirqowZainas_4 = value;
	}

	inline static int32_t get_offset_of__tawyoupar_5() { return static_cast<int32_t>(offsetof(M_siwalstar15_t129305858, ____tawyoupar_5)); }
	inline float get__tawyoupar_5() const { return ____tawyoupar_5; }
	inline float* get_address_of__tawyoupar_5() { return &____tawyoupar_5; }
	inline void set__tawyoupar_5(float value)
	{
		____tawyoupar_5 = value;
	}

	inline static int32_t get_offset_of__merlirKemtawyis_6() { return static_cast<int32_t>(offsetof(M_siwalstar15_t129305858, ____merlirKemtawyis_6)); }
	inline int32_t get__merlirKemtawyis_6() const { return ____merlirKemtawyis_6; }
	inline int32_t* get_address_of__merlirKemtawyis_6() { return &____merlirKemtawyis_6; }
	inline void set__merlirKemtawyis_6(int32_t value)
	{
		____merlirKemtawyis_6 = value;
	}

	inline static int32_t get_offset_of__teelabas_7() { return static_cast<int32_t>(offsetof(M_siwalstar15_t129305858, ____teelabas_7)); }
	inline uint32_t get__teelabas_7() const { return ____teelabas_7; }
	inline uint32_t* get_address_of__teelabas_7() { return &____teelabas_7; }
	inline void set__teelabas_7(uint32_t value)
	{
		____teelabas_7 = value;
	}

	inline static int32_t get_offset_of__desistreRairnouwhaw_8() { return static_cast<int32_t>(offsetof(M_siwalstar15_t129305858, ____desistreRairnouwhaw_8)); }
	inline float get__desistreRairnouwhaw_8() const { return ____desistreRairnouwhaw_8; }
	inline float* get_address_of__desistreRairnouwhaw_8() { return &____desistreRairnouwhaw_8; }
	inline void set__desistreRairnouwhaw_8(float value)
	{
		____desistreRairnouwhaw_8 = value;
	}

	inline static int32_t get_offset_of__tellemjallTowtrowral_9() { return static_cast<int32_t>(offsetof(M_siwalstar15_t129305858, ____tellemjallTowtrowral_9)); }
	inline float get__tellemjallTowtrowral_9() const { return ____tellemjallTowtrowral_9; }
	inline float* get_address_of__tellemjallTowtrowral_9() { return &____tellemjallTowtrowral_9; }
	inline void set__tellemjallTowtrowral_9(float value)
	{
		____tellemjallTowtrowral_9 = value;
	}

	inline static int32_t get_offset_of__pirorWernur_10() { return static_cast<int32_t>(offsetof(M_siwalstar15_t129305858, ____pirorWernur_10)); }
	inline float get__pirorWernur_10() const { return ____pirorWernur_10; }
	inline float* get_address_of__pirorWernur_10() { return &____pirorWernur_10; }
	inline void set__pirorWernur_10(float value)
	{
		____pirorWernur_10 = value;
	}

	inline static int32_t get_offset_of__kecallnu_11() { return static_cast<int32_t>(offsetof(M_siwalstar15_t129305858, ____kecallnu_11)); }
	inline uint32_t get__kecallnu_11() const { return ____kecallnu_11; }
	inline uint32_t* get_address_of__kecallnu_11() { return &____kecallnu_11; }
	inline void set__kecallnu_11(uint32_t value)
	{
		____kecallnu_11 = value;
	}

	inline static int32_t get_offset_of__seltereHurwhallnel_12() { return static_cast<int32_t>(offsetof(M_siwalstar15_t129305858, ____seltereHurwhallnel_12)); }
	inline bool get__seltereHurwhallnel_12() const { return ____seltereHurwhallnel_12; }
	inline bool* get_address_of__seltereHurwhallnel_12() { return &____seltereHurwhallnel_12; }
	inline void set__seltereHurwhallnel_12(bool value)
	{
		____seltereHurwhallnel_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
