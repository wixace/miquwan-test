﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSPlayerDataGenerated
struct CSPlayerDataGenerated_t2752339220;
// JSVCall
struct JSVCall_t3708497963;
// CSPlayerData/FightingPos[]
struct FightingPosU5BU5D_t3645161435;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.List`1<CSPlusAtt>
struct List_1_t341533415;
// CSPlayerData
struct CSPlayerData_t1029357243;
// System.Collections.Generic.List`1<CSHeroUnit>
struct List_1_t837576702;
// CSPlusAtt
struct CSPlusAtt_t3268315159;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_CSPlayerData1029357243.h"

// System.Void CSPlayerDataGenerated::.ctor()
extern "C"  void CSPlayerDataGenerated__ctor_m2714702551 (CSPlayerDataGenerated_t2752339220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSPlayerDataGenerated::CSPlayerData_CSPlayerData1(JSVCall,System.Int32)
extern "C"  bool CSPlayerDataGenerated_CSPlayerData_CSPlayerData1_m1140858907 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSPlayerDataGenerated::CSPlayerData_ID(JSVCall)
extern "C"  void CSPlayerDataGenerated_CSPlayerData_ID_m664944307 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSPlayerDataGenerated::CSPlayerData_name(JSVCall)
extern "C"  void CSPlayerDataGenerated_CSPlayerData_name_m2026639683 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSPlayerDataGenerated::CSPlayerData_icon(JSVCall)
extern "C"  void CSPlayerDataGenerated_CSPlayerData_icon_m2684263189 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSPlayerDataGenerated::CSPlayerData_rank(JSVCall)
extern "C"  void CSPlayerDataGenerated_CSPlayerData_rank_m2171962562 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSPlayerDataGenerated::CSPlayerData_lv(JSVCall)
extern "C"  void CSPlayerDataGenerated_CSPlayerData_lv_m960415524 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSPlayerDataGenerated::CSPlayerData_exp(JSVCall)
extern "C"  void CSPlayerDataGenerated_CSPlayerData_exp_m3208389921 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSPlayerDataGenerated::CSPlayerData_power(JSVCall)
extern "C"  void CSPlayerDataGenerated_CSPlayerData_power_m2178123065 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSPlayerDataGenerated::CSPlayerData_uPower(JSVCall)
extern "C"  void CSPlayerDataGenerated_CSPlayerData_uPower_m1813655006 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSPlayerDataGenerated::CSPlayerData_Fighting(JSVCall)
extern "C"  void CSPlayerDataGenerated_CSPlayerData_Fighting_m2249204156 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSPlayerDataGenerated::CSPlayerData_currentOtherPlayer(JSVCall)
extern "C"  void CSPlayerDataGenerated_CSPlayerData_currentOtherPlayer_m1907109270 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSPlayerDataGenerated::CSPlayerData_devilSkillData(JSVCall)
extern "C"  void CSPlayerDataGenerated_CSPlayerData_devilSkillData_m3408440395 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSPlayerDataGenerated::CSPlayerData_GetLeaderPlusAtt(JSVCall,System.Int32)
extern "C"  bool CSPlayerDataGenerated_CSPlayerData_GetLeaderPlusAtt_m1690792357 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSPlayerDataGenerated::CSPlayerData_GetLearerAttUnit__String(JSVCall,System.Int32)
extern "C"  bool CSPlayerDataGenerated_CSPlayerData_GetLearerAttUnit__String_m75487174 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSPlayerDataGenerated::CSPlayerData_GetMonsterUnits(JSVCall,System.Int32)
extern "C"  bool CSPlayerDataGenerated_CSPlayerData_GetMonsterUnits_m4000278160 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSPlayerDataGenerated::CSPlayerData_GetReinAttUnit__String(JSVCall,System.Int32)
extern "C"  bool CSPlayerDataGenerated_CSPlayerData_GetReinAttUnit__String_m280930533 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSPlayerDataGenerated::CSPlayerData_GetReinPlusAtts(JSVCall,System.Int32)
extern "C"  bool CSPlayerDataGenerated_CSPlayerData_GetReinPlusAtts_m299734783 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSPlayerDataGenerated::CSPlayerData_IsPVPRobat(JSVCall,System.Int32)
extern "C"  bool CSPlayerDataGenerated_CSPlayerData_IsPVPRobat_m2210379765 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSPlayerDataGenerated::CSPlayerData_LoadData__String(JSVCall,System.Int32)
extern "C"  bool CSPlayerDataGenerated_CSPlayerData_LoadData__String_m4230190334 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSPlayerDataGenerated::CSPlayerData_UnLoadData(JSVCall,System.Int32)
extern "C"  bool CSPlayerDataGenerated_CSPlayerData_UnLoadData_m3890483622 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSPlayerDataGenerated::__Register()
extern "C"  void CSPlayerDataGenerated___Register_m3008338000 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSPlayerData/FightingPos[] CSPlayerDataGenerated::<CSPlayerData_Fighting>m__28()
extern "C"  FightingPosU5BU5D_t3645161435* CSPlayerDataGenerated_U3CCSPlayerData_FightingU3Em__28_m3069488943 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSPlayerDataGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t CSPlayerDataGenerated_ilo_getObject1_m3949157951 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSPlayerDataGenerated::ilo_setUInt322(System.Int32,System.UInt32)
extern "C"  void CSPlayerDataGenerated_ilo_setUInt322_m1308550364 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSPlayerDataGenerated::ilo_getUInt323(System.Int32)
extern "C"  uint32_t CSPlayerDataGenerated_ilo_getUInt323_m2571379834 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSPlayerDataGenerated::ilo_setStringS4(System.Int32,System.String)
extern "C"  void CSPlayerDataGenerated_ilo_setStringS4_m1325970339 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CSPlayerDataGenerated::ilo_getStringS5(System.Int32)
extern "C"  String_t* CSPlayerDataGenerated_ilo_getStringS5_m3645589631 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSPlayerDataGenerated::ilo_moveSaveID2Arr6(System.Int32)
extern "C"  void CSPlayerDataGenerated_ilo_moveSaveID2Arr6_m3318621553 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSPlayerDataGenerated::ilo_setArrayS7(System.Int32,System.Int32,System.Boolean)
extern "C"  void CSPlayerDataGenerated_ilo_setArrayS7_m278928274 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSPlayerDataGenerated::ilo_setObject8(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t CSPlayerDataGenerated_ilo_setObject8_m803686782 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSPlusAtt> CSPlayerDataGenerated::ilo_GetLeaderPlusAtt9(CSPlayerData)
extern "C"  List_1_t341533415 * CSPlayerDataGenerated_ilo_GetLeaderPlusAtt9_m2056770800 (Il2CppObject * __this /* static, unused */, CSPlayerData_t1029357243 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSHeroUnit> CSPlayerDataGenerated::ilo_GetMonsterUnits10(CSPlayerData)
extern "C"  List_1_t837576702 * CSPlayerDataGenerated_ilo_GetMonsterUnits10_m3520895506 (Il2CppObject * __this /* static, unused */, CSPlayerData_t1029357243 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSPlusAtt CSPlayerDataGenerated::ilo_GetReinAttUnit11(CSPlayerData,System.String)
extern "C"  CSPlusAtt_t3268315159 * CSPlayerDataGenerated_ilo_GetReinAttUnit11_m1053890748 (Il2CppObject * __this /* static, unused */, CSPlayerData_t1029357243 * ____this0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSPlayerDataGenerated::ilo_setBooleanS12(System.Int32,System.Boolean)
extern "C"  void CSPlayerDataGenerated_ilo_setBooleanS12_m33031548 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
