﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_jowpea99
struct M_jowpea99_t1613162986;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_jowpea991613162986.h"

// System.Void GarbageiOS.M_jowpea99::.ctor()
extern "C"  void M_jowpea99__ctor_m1721795465 (M_jowpea99_t1613162986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jowpea99::M_lapal0(System.String[],System.Int32)
extern "C"  void M_jowpea99_M_lapal0_m177228110 (M_jowpea99_t1613162986 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jowpea99::M_fapalSearjano1(System.String[],System.Int32)
extern "C"  void M_jowpea99_M_fapalSearjano1_m1115698714 (M_jowpea99_t1613162986 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jowpea99::M_jolortoVarmal2(System.String[],System.Int32)
extern "C"  void M_jowpea99_M_jolortoVarmal2_m3359566272 (M_jowpea99_t1613162986 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jowpea99::M_huryeageBergiwee3(System.String[],System.Int32)
extern "C"  void M_jowpea99_M_huryeageBergiwee3_m2515003595 (M_jowpea99_t1613162986 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jowpea99::M_jijiBallbaku4(System.String[],System.Int32)
extern "C"  void M_jowpea99_M_jijiBallbaku4_m2576376378 (M_jowpea99_t1613162986 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jowpea99::ilo_M_fapalSearjano11(GarbageiOS.M_jowpea99,System.String[],System.Int32)
extern "C"  void M_jowpea99_ilo_M_fapalSearjano11_m1711554410 (Il2CppObject * __this /* static, unused */, M_jowpea99_t1613162986 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
