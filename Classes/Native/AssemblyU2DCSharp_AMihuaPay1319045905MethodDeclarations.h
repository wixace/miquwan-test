﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AMihuaPay
struct AMihuaPay_t1319045905;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "AssemblyU2DCSharp_AMihuaPay1319045905.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void AMihuaPay::.ctor(System.String)
extern "C"  void AMihuaPay__ctor_m1654908872 (AMihuaPay_t1319045905 * __this, String_t* ___referer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMihuaPay::InitPayParams()
extern "C"  void AMihuaPay_InitPayParams_m1342562614 (AMihuaPay_t1319045905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AMihuaPay::BuildOrderParam()
extern "C"  String_t* AMihuaPay_BuildOrderParam_m2757256488 (AMihuaPay_t1319045905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm AMihuaPay::BuildOrderParam2WWWForm()
extern "C"  WWWForm_t461342257 * AMihuaPay_BuildOrderParam2WWWForm_m3003749725 (AMihuaPay_t1319045905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMihuaPay::Tiaoqian(Mihua.SDK.PayInfo)
extern "C"  void AMihuaPay_Tiaoqian_m693282420 (AMihuaPay_t1319045905 * __this, PayInfo_t1775308120 * ___payInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMihuaPay::SignCallBack(System.Boolean,System.String)
extern "C"  void AMihuaPay_SignCallBack_m1064003519 (AMihuaPay_t1319045905 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMihuaPay::__IAmp(System.String,System.String,System.String)
extern "C"  void AMihuaPay___IAmp_m1626312981 (AMihuaPay_t1319045905 * __this, String_t* ___payOrder0, String_t* ___toScheme1, String_t* ___reScheme2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMihuaPay::ilo_Add1(Newtonsoft.Json.Linq.JObject,System.String,Newtonsoft.Json.Linq.JToken)
extern "C"  void AMihuaPay_ilo_Add1_m2338407741 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, JToken_t3412245951 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm AMihuaPay::ilo_BuildOrderParam2WWWForm2(AMihuaPay)
extern "C"  WWWForm_t461342257 * AMihuaPay_ilo_BuildOrderParam2WWWForm2_m3114858807 (Il2CppObject * __this /* static, unused */, AMihuaPay_t1319045905 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMihuaPay::ilo_LogError3(System.Object,System.Boolean)
extern "C"  void AMihuaPay_ilo_LogError3_m2152502461 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AMihuaPay::ilo_ContainsKey4(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  bool AMihuaPay_ilo_ContainsKey4_m2779561065 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AMihuaPay::ilo_BuildOrderParam5(AMihuaPay)
extern "C"  String_t* AMihuaPay_ilo_BuildOrderParam5_m439736927 (Il2CppObject * __this /* static, unused */, AMihuaPay_t1319045905 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMihuaPay::ilo___IAmp6(AMihuaPay,System.String,System.String,System.String)
extern "C"  void AMihuaPay_ilo___IAmp6_m1064756437 (Il2CppObject * __this /* static, unused */, AMihuaPay_t1319045905 * ____this0, String_t* ___payOrder1, String_t* ___toScheme2, String_t* ___reScheme3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMihuaPay::ilo_Log7(System.Object,System.Boolean)
extern "C"  void AMihuaPay_ilo_Log7_m1212349465 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
