﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Serialization.GraphMeta
struct GraphMeta_t513097101;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.Serialization.GraphMeta::.ctor()
extern "C"  void GraphMeta__ctor_m1540755552 (GraphMeta_t513097101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Pathfinding.Serialization.GraphMeta::GetGraphType(System.Int32)
extern "C"  Type_t * GraphMeta_GetGraphType_m335370829 (GraphMeta_t513097101 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
