﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.RVO.Simulator
struct Simulator_t2705969170;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_ThreadCount3572904197.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_Simulator_Samplin259839888.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RVO.RVOSimulator
struct  RVOSimulator_t3058704865  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean Pathfinding.RVO.RVOSimulator::doubleBuffering
	bool ___doubleBuffering_2;
	// System.Boolean Pathfinding.RVO.RVOSimulator::interpolation
	bool ___interpolation_3;
	// System.Int32 Pathfinding.RVO.RVOSimulator::desiredSimulationFPS
	int32_t ___desiredSimulationFPS_4;
	// ThreadCount Pathfinding.RVO.RVOSimulator::workerThreads
	int32_t ___workerThreads_5;
	// System.Single Pathfinding.RVO.RVOSimulator::qualityCutoff
	float ___qualityCutoff_6;
	// System.Single Pathfinding.RVO.RVOSimulator::stepScale
	float ___stepScale_7;
	// System.Single Pathfinding.RVO.RVOSimulator::incompressibility
	float ___incompressibility_8;
	// System.Single Pathfinding.RVO.RVOSimulator::wallThickness
	float ___wallThickness_9;
	// System.Single Pathfinding.RVO.RVOSimulator::desiredVelocityWeight
	float ___desiredVelocityWeight_10;
	// Pathfinding.RVO.Simulator/SamplingAlgorithm Pathfinding.RVO.RVOSimulator::algorithm
	int32_t ___algorithm_11;
	// System.Boolean Pathfinding.RVO.RVOSimulator::oversampling
	bool ___oversampling_12;
	// Pathfinding.RVO.Simulator Pathfinding.RVO.RVOSimulator::simulator
	Simulator_t2705969170 * ___simulator_13;

public:
	inline static int32_t get_offset_of_doubleBuffering_2() { return static_cast<int32_t>(offsetof(RVOSimulator_t3058704865, ___doubleBuffering_2)); }
	inline bool get_doubleBuffering_2() const { return ___doubleBuffering_2; }
	inline bool* get_address_of_doubleBuffering_2() { return &___doubleBuffering_2; }
	inline void set_doubleBuffering_2(bool value)
	{
		___doubleBuffering_2 = value;
	}

	inline static int32_t get_offset_of_interpolation_3() { return static_cast<int32_t>(offsetof(RVOSimulator_t3058704865, ___interpolation_3)); }
	inline bool get_interpolation_3() const { return ___interpolation_3; }
	inline bool* get_address_of_interpolation_3() { return &___interpolation_3; }
	inline void set_interpolation_3(bool value)
	{
		___interpolation_3 = value;
	}

	inline static int32_t get_offset_of_desiredSimulationFPS_4() { return static_cast<int32_t>(offsetof(RVOSimulator_t3058704865, ___desiredSimulationFPS_4)); }
	inline int32_t get_desiredSimulationFPS_4() const { return ___desiredSimulationFPS_4; }
	inline int32_t* get_address_of_desiredSimulationFPS_4() { return &___desiredSimulationFPS_4; }
	inline void set_desiredSimulationFPS_4(int32_t value)
	{
		___desiredSimulationFPS_4 = value;
	}

	inline static int32_t get_offset_of_workerThreads_5() { return static_cast<int32_t>(offsetof(RVOSimulator_t3058704865, ___workerThreads_5)); }
	inline int32_t get_workerThreads_5() const { return ___workerThreads_5; }
	inline int32_t* get_address_of_workerThreads_5() { return &___workerThreads_5; }
	inline void set_workerThreads_5(int32_t value)
	{
		___workerThreads_5 = value;
	}

	inline static int32_t get_offset_of_qualityCutoff_6() { return static_cast<int32_t>(offsetof(RVOSimulator_t3058704865, ___qualityCutoff_6)); }
	inline float get_qualityCutoff_6() const { return ___qualityCutoff_6; }
	inline float* get_address_of_qualityCutoff_6() { return &___qualityCutoff_6; }
	inline void set_qualityCutoff_6(float value)
	{
		___qualityCutoff_6 = value;
	}

	inline static int32_t get_offset_of_stepScale_7() { return static_cast<int32_t>(offsetof(RVOSimulator_t3058704865, ___stepScale_7)); }
	inline float get_stepScale_7() const { return ___stepScale_7; }
	inline float* get_address_of_stepScale_7() { return &___stepScale_7; }
	inline void set_stepScale_7(float value)
	{
		___stepScale_7 = value;
	}

	inline static int32_t get_offset_of_incompressibility_8() { return static_cast<int32_t>(offsetof(RVOSimulator_t3058704865, ___incompressibility_8)); }
	inline float get_incompressibility_8() const { return ___incompressibility_8; }
	inline float* get_address_of_incompressibility_8() { return &___incompressibility_8; }
	inline void set_incompressibility_8(float value)
	{
		___incompressibility_8 = value;
	}

	inline static int32_t get_offset_of_wallThickness_9() { return static_cast<int32_t>(offsetof(RVOSimulator_t3058704865, ___wallThickness_9)); }
	inline float get_wallThickness_9() const { return ___wallThickness_9; }
	inline float* get_address_of_wallThickness_9() { return &___wallThickness_9; }
	inline void set_wallThickness_9(float value)
	{
		___wallThickness_9 = value;
	}

	inline static int32_t get_offset_of_desiredVelocityWeight_10() { return static_cast<int32_t>(offsetof(RVOSimulator_t3058704865, ___desiredVelocityWeight_10)); }
	inline float get_desiredVelocityWeight_10() const { return ___desiredVelocityWeight_10; }
	inline float* get_address_of_desiredVelocityWeight_10() { return &___desiredVelocityWeight_10; }
	inline void set_desiredVelocityWeight_10(float value)
	{
		___desiredVelocityWeight_10 = value;
	}

	inline static int32_t get_offset_of_algorithm_11() { return static_cast<int32_t>(offsetof(RVOSimulator_t3058704865, ___algorithm_11)); }
	inline int32_t get_algorithm_11() const { return ___algorithm_11; }
	inline int32_t* get_address_of_algorithm_11() { return &___algorithm_11; }
	inline void set_algorithm_11(int32_t value)
	{
		___algorithm_11 = value;
	}

	inline static int32_t get_offset_of_oversampling_12() { return static_cast<int32_t>(offsetof(RVOSimulator_t3058704865, ___oversampling_12)); }
	inline bool get_oversampling_12() const { return ___oversampling_12; }
	inline bool* get_address_of_oversampling_12() { return &___oversampling_12; }
	inline void set_oversampling_12(bool value)
	{
		___oversampling_12 = value;
	}

	inline static int32_t get_offset_of_simulator_13() { return static_cast<int32_t>(offsetof(RVOSimulator_t3058704865, ___simulator_13)); }
	inline Simulator_t2705969170 * get_simulator_13() const { return ___simulator_13; }
	inline Simulator_t2705969170 ** get_address_of_simulator_13() { return &___simulator_13; }
	inline void set_simulator_13(Simulator_t2705969170 * value)
	{
		___simulator_13 = value;
		Il2CppCodeGenWriteBarrier(&___simulator_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
