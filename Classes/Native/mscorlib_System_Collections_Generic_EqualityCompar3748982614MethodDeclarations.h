﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<AnimationRunner/AniType>
struct DefaultComparer_t3748982614;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniType1006238651.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<AnimationRunner/AniType>::.ctor()
extern "C"  void DefaultComparer__ctor_m3722141753_gshared (DefaultComparer_t3748982614 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3722141753(__this, method) ((  void (*) (DefaultComparer_t3748982614 *, const MethodInfo*))DefaultComparer__ctor_m3722141753_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<AnimationRunner/AniType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3040877394_gshared (DefaultComparer_t3748982614 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3040877394(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3748982614 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m3040877394_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<AnimationRunner/AniType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2841276810_gshared (DefaultComparer_t3748982614 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2841276810(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3748982614 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m2841276810_gshared)(__this, ___x0, ___y1, method)
