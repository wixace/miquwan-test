﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.EuclideanEmbedding/<RecalculatePivots>c__AnonStorey11C
struct U3CRecalculatePivotsU3Ec__AnonStorey11C_t2905139494;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.EuclideanEmbedding/<RecalculatePivots>c__AnonStorey11C::.ctor()
extern "C"  void U3CRecalculatePivotsU3Ec__AnonStorey11C__ctor_m2247127605 (U3CRecalculatePivotsU3Ec__AnonStorey11C_t2905139494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
