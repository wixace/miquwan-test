﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2108468855.h"
#include "mscorlib_System_Array1146569071.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_IntP3326126179.h"

// System.Void System.Array/InternalEnumerator`1<Pathfinding.ClipperLib.IntPoint>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1135288466_gshared (InternalEnumerator_1_t2108468855 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1135288466(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2108468855 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1135288466_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2413516750_gshared (InternalEnumerator_1_t2108468855 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2413516750(__this, method) ((  void (*) (InternalEnumerator_1_t2108468855 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2413516750_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m120868292_gshared (InternalEnumerator_1_t2108468855 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m120868292(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2108468855 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m120868292_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.ClipperLib.IntPoint>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3465205993_gshared (InternalEnumerator_1_t2108468855 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3465205993(__this, method) ((  void (*) (InternalEnumerator_1_t2108468855 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3465205993_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.ClipperLib.IntPoint>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3508486526_gshared (InternalEnumerator_1_t2108468855 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3508486526(__this, method) ((  bool (*) (InternalEnumerator_1_t2108468855 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3508486526_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Pathfinding.ClipperLib.IntPoint>::get_Current()
extern "C"  IntPoint_t3326126179  InternalEnumerator_1_get_Current_m710604475_gshared (InternalEnumerator_1_t2108468855 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m710604475(__this, method) ((  IntPoint_t3326126179  (*) (InternalEnumerator_1_t2108468855 *, const MethodInfo*))InternalEnumerator_1_get_Current_m710604475_gshared)(__this, method)
