﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_dawchougaNezis166
struct M_dawchougaNezis166_t3164355875;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_dawchougaNezis166::.ctor()
extern "C"  void M_dawchougaNezis166__ctor_m2372297632 (M_dawchougaNezis166_t3164355875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_dawchougaNezis166::M_ballge0(System.String[],System.Int32)
extern "C"  void M_dawchougaNezis166_M_ballge0_m4057811862 (M_dawchougaNezis166_t3164355875 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_dawchougaNezis166::M_meegalallPidrufaw1(System.String[],System.Int32)
extern "C"  void M_dawchougaNezis166_M_meegalallPidrufaw1_m1849030804 (M_dawchougaNezis166_t3164355875 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
