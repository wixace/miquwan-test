﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>
struct Transform_1_t3950254943;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t540760087;
// System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>
struct Transform_1_t1605056443;
// System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Object>
struct Transform_1_t2959969844;
// System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Int32,Pathfinding.Int2>
struct Transform_1_t132544136;
// System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t4205072453;
// System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Int32,System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>>
struct Transform_1_t302597331;
// System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Int32,System.Int32>
struct Transform_1_t3607304339;
// System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Object,Pathfinding.Int2>
struct Transform_1_t2305320093;
// System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t2082881114;
// System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Object,System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>>
struct Transform_1_t1197383863;
// System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Object,System.Object>
struct Transform_1_t207123575;
// System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Int32,Pathfinding.Int3>
struct Transform_1_t1799118306;
// System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t1576679326;
// System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Int32,System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Int32>>
struct Transform_1_t2455359239;
// System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Int32,System.Int32>
struct Transform_1_t978911212;
// System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Object,Pathfinding.Int3>
struct Transform_1_t3971894263;
// System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t3749455283;
// System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Object,System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>>
struct Transform_1_t3350145771;
// System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Object,System.Object>
struct Transform_1_t1873697744;
// System.Collections.Generic.Dictionary`2/Transform`1<PushType,System.Object,PushType>
struct Transform_1_t2321255387;
// System.Collections.Generic.Dictionary`2/Transform`1<PushType,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t2232219549;
// System.Collections.Generic.Dictionary`2/Transform`1<PushType,System.Object,System.Collections.Generic.KeyValuePair`2<PushType,System.Object>>
struct Transform_1_t1000228275;
// System.Collections.Generic.Dictionary`2/Transform`1<PushType,System.Object,System.Object>
struct Transform_1_t356462010;
// System.Collections.Generic.Dictionary`2/Transform`1<SoundTypeID,System.Object,SoundTypeID>
struct Transform_1_t290614651;
// System.Collections.Generic.Dictionary`2/Transform`1<SoundTypeID,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t2710571821;
// System.Collections.Generic.Dictionary`2/Transform`1<SoundTypeID,System.Object,System.Collections.Generic.KeyValuePair`2<SoundTypeID,System.Object>>
struct Transform_1_t2906177779;
// System.Collections.Generic.Dictionary`2/Transform`1<SoundTypeID,System.Object,System.Object>
struct Transform_1_t834814282;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SoundStatus,SoundStatus>
struct Transform_1_t2074406482;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SoundStatus,System.Collections.DictionaryEntry>
struct Transform_1_t233074151;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SoundStatus,System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>>
struct Transform_1_t1970450427;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SoundStatus,System.Int32>
struct Transform_1_t3930273333;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t3765932824;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct Transform_1_t3064208655;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>
struct Transform_1_t3168164710;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t1643741485;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct Transform_1_t3958995187;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>
struct Transform_1_t1045973371;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>
struct Transform_1_t4062951242;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Single,System.Collections.DictionaryEntry>
struct Transform_1_t2851648992;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Single,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>>
struct Transform_1_t993037999;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Single,System.Int32>
struct Transform_1_t2253880878;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Single,System.Single>
struct Transform_1_t1096994054;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,HatredCtrl/stValue>
struct Transform_1_t2514112805;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,System.Collections.DictionaryEntry>
struct Transform_1_t839774009;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>>
struct Transform_1_t287965411;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,System.Object>
struct Transform_1_t3258983766;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo,Mihua.Assets.SubAssetMgr/ErrorInfo>
struct Transform_1_t3465660101;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo,System.Collections.DictionaryEntry>
struct Transform_1_t2583285505;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo,System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>>
struct Transform_1_t1239512707;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo,System.Object>
struct Transform_1_t707527966;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,Newtonsoft.Json.Schema.JsonSchemaType>
struct Transform_1_t2613442969;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Collections.DictionaryEntry>
struct Transform_1_t2249633762;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>>
struct Transform_1_t387295575;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Object>
struct Transform_1_t373876223;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>
struct Transform_1_t1036041189;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence,System.Collections.DictionaryEntry>
struct Transform_1_t4190285609;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence,System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>>
struct Transform_1_t3104861091;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence,System.Object>
struct Transform_1_t2314528070;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,ProductsCfgMgr/ProductInfo>
struct Transform_1_t3750066325;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,System.Collections.DictionaryEntry>
struct Transform_1_t4195681701;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,System.Collections.Generic.KeyValuePair`2<System.Object,ProductsCfgMgr/ProductInfo>>
struct Transform_1_t1523918931;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,System.Object>
struct Transform_1_t2319924162;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Boolean>
struct Transform_1_t1899867829;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.DictionaryEntry>
struct Transform_1_t3174675725;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct Transform_1_t3968687731;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Object>
struct Transform_1_t1298918186;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>,System.Collections.DictionaryEntry>
struct Transform_1_t4217408362;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>>
struct Transform_1_t232618487;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>
struct Transform_1_t2458765881;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>,System.Object>
struct Transform_1_t2341650823;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t2949220623;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct Transform_1_t125305115;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Int32>
struct Transform_1_t2351452509;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Object>
struct Transform_1_t1073463084;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t827029284;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Transform_1_t1020091647;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>
struct Transform_1_t3246239041;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.DictionaryEntry>
struct Transform_1_t2034936791;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>
struct Transform_1_t2349101755;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Object>
struct Transform_1_t159179252;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Single>
struct Transform_1_t280281853;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.DictionaryEntry>
struct Transform_1_t520022276;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>
struct Transform_1_t861903487;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Object>
struct Transform_1_t2939232033;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.UInt16>
struct Transform_1_t3088050881;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>
struct Transform_1_t1958177301;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>
struct Transform_1_t2126384403;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Object>
struct Transform_1_t82419762;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,UnityEngine.TextEditor/TextEditOp>
struct Transform_1_t57564501;
// System.Collections.Generic.Dictionary`2/Transform`1<System.UInt16,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t3432882628;
// System.Collections.Generic.Dictionary`2/Transform`1<System.UInt16,System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>>
struct Transform_1_t3318937983;
// System.Collections.Generic.Dictionary`2/Transform`1<System.UInt16,System.Object,System.Object>
struct Transform_1_t1557125089;
// System.Collections.Generic.Dictionary`2/Transform`1<System.UInt16,System.Object,System.UInt16>
struct Transform_1_t1705943937;
// System.Collections.Generic.Dictionary`2/Transform`1<System.UInt32,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t1309936622;
// System.Collections.Generic.Dictionary`2/Transform`1<System.UInt32,System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>
struct Transform_1_t3625077063;
// System.Collections.Generic.Dictionary`2/Transform`1<System.UInt32,System.Object,System.Object>
struct Transform_1_t3729146379;
// System.Collections.Generic.Dictionary`2/Transform`1<System.UInt32,System.Object,System.UInt32>
struct Transform_1_t3877965285;
// System.Collections.Generic.Dictionary`2/Transform`1<UIModelDisplayType,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t3029156963;
// System.Collections.Generic.Dictionary`2/Transform`1<UIModelDisplayType,System.Int32,System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>>
struct Transform_1_t4128861355;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3950254943.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3950254943MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Void2863195528.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Convert866134174.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr540760087.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr540760087MethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1605056443.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1605056443MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22815902970.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2959969844.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2959969844MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr132544136.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr132544136MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4205072453.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4205072453MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr302597331.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr302597331MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22144098788.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3607304339.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3607304339MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2305320093.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2305320093MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2082881114.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2082881114MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1197383863.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1197383863MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_866109363.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr207123575.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr207123575MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1799118306.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1799118306MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1576679326.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1576679326MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2455359239.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2455359239MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22630286527.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr978911212.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr978911212MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3971894263.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3971894263MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3749455283.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3749455283MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3350145771.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3350145771MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21352297102.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1873697744.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1873697744MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2321255387.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2321255387MethodDeclarations.h"
#include "AssemblyU2DCSharp_PushType1840642452.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2232219549.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2232219549MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1000228275.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1000228275MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_519615340.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr356462010.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr356462010MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr290614651.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr290614651MethodDeclarations.h"
#include "AssemblyU2DCSharp_SoundTypeID3626616740.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2710571821.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2710571821MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2906177779.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2906177779MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21947212572.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr834814282.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr834814282MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2074406482.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2074406482MethodDeclarations.h"
#include "AssemblyU2DCSharp_SoundStatus3592938945.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr233074151.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr233074151MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1970450427.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1970450427MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23488982890.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3930273333.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3930273333MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3765932824.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3765932824MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3064208655.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3064208655MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21049882445.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3168164710.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3168164710MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1643741485.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1643741485MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3958995187.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3958995187MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066860316.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1045973371.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1045973371MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4062951242.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4062951242MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2851648992.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2851648992MethodDeclarations.h"
#include "mscorlib_System_Single4291918972.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr993037999.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr993037999MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24187962917.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2253880878.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2253880878MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1096994054.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1096994054MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2514112805.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2514112805MethodDeclarations.h"
#include "AssemblyU2DCSharp_HatredCtrl_stValue3425945410.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr839774009.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr839774009MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr287965411.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr287965411MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21199798016.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3258983766.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3258983766MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3465660101.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3465660101MethodDeclarations.h"
#include "AssemblyU2DCSharp_Mihua_Assets_SubAssetMgr_ErrorIn2633981210.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2583285505.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2583285505MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1239512707.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1239512707MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_407833816.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr707527966.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr707527966MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2613442969.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2613442969MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem2115415821.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2249633762.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2249633762MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr387295575.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr387295575MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24184235723.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr373876223.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr373876223MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1036041189.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1036041189MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js2892329490.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4190285609.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4190285609MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3104861091.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3104861091MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_666182096.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2314528070.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2314528070MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3750066325.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3750066325MethodDeclarations.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr_ProductInfo1305991238.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4195681701.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4195681701MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1523918931.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1523918931MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23374811140.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2319924162.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2319924162MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1899867829.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1899867829MethodDeclarations.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3174675725.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3174675725MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3968687731.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3968687731MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22545618620.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1298918186.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1298918186MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4217408362.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4217408362MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24287931429.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr232618487.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr232618487MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22061784035.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2458765881.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2458765881MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2341650823.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2341650823MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2949220623.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2949220623MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr125305115.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr125305115MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23222658402.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2351452509.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2351452509MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1073463084.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1073463084MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr827029284.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr827029284MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1020091647.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1020091647MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3246239041.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3246239041MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2034936791.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2034936791MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2349101755.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2349101755MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22065771578.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr159179252.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr159179252MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr280281853.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr280281853MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr520022276.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr520022276MethodDeclarations.h"
#include "mscorlib_System_UInt1624667923.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr861903487.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr861903487MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22093487825.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2939232033.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2939232033MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3088050881.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3088050881MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1958177301.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1958177301MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp4145961110.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2126384403.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2126384403MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21919813716.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tra82419762.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tra82419762MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tra57564501.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tra57564501MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3432882628.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3432882628MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3318937983.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3318937983MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21637661969.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1557125089.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1557125089MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1705943937.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1705943937MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1309936622.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1309936622MethodDeclarations.h"
#include "mscorlib_System_UInt3224667981.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3625077063.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3625077063MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066747055.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3729146379.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3729146379MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3877965285.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3877965285MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3029156963.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3029156963MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIModelDisplayType1891752679.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4128861355.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4128861355MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22851311006.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1430393155_gshared (Transform_1_t3950254943 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::Invoke(TKey,TValue)
extern "C"  TypeConvertKey_t866134174  Transform_1_Invoke_m1444894809_gshared (Transform_1_t3950254943 * __this, TypeConvertKey_t866134174  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1444894809((Transform_1_t3950254943 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef TypeConvertKey_t866134174  (*FunctionPointerType) (Il2CppObject *, void* __this, TypeConvertKey_t866134174  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef TypeConvertKey_t866134174  (*FunctionPointerType) (void* __this, TypeConvertKey_t866134174  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* TypeConvertKey_t866134174_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1505448888_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1505448888_gshared (Transform_1_t3950254943 * __this, TypeConvertKey_t866134174  ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1505448888_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(TypeConvertKey_t866134174_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::EndInvoke(System.IAsyncResult)
extern "C"  TypeConvertKey_t866134174  Transform_1_EndInvoke_m3100034705_gshared (Transform_1_t3950254943 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(TypeConvertKey_t866134174 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4023453717_gshared (Transform_1_t540760087 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m3853287683_gshared (Transform_1_t540760087 * __this, TypeConvertKey_t866134174  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3853287683((Transform_1_t540760087 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, TypeConvertKey_t866134174  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, TypeConvertKey_t866134174  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* TypeConvertKey_t866134174_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2923829422_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2923829422_gshared (Transform_1_t540760087 * __this, TypeConvertKey_t866134174  ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2923829422_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(TypeConvertKey_t866134174_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m3169349543_gshared (Transform_1_t540760087 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m542678812_gshared (Transform_1_t1605056443 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t2815902970  Transform_1_Invoke_m3807657948_gshared (Transform_1_t1605056443 * __this, TypeConvertKey_t866134174  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3807657948((Transform_1_t1605056443 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2815902970  (*FunctionPointerType) (Il2CppObject *, void* __this, TypeConvertKey_t866134174  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t2815902970  (*FunctionPointerType) (void* __this, TypeConvertKey_t866134174  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* TypeConvertKey_t866134174_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1413603719_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1413603719_gshared (Transform_1_t1605056443 * __this, TypeConvertKey_t866134174  ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1413603719_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(TypeConvertKey_t866134174_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t2815902970  Transform_1_EndInvoke_m3683852078_gshared (Transform_1_t1605056443 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t2815902970 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1928971779_gshared (Transform_1_t2959969844 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m278095257_gshared (Transform_1_t2959969844 * __this, TypeConvertKey_t866134174  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m278095257((Transform_1_t2959969844 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, TypeConvertKey_t866134174  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, TypeConvertKey_t866134174  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* TypeConvertKey_t866134174_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1960583928_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1960583928_gshared (Transform_1_t2959969844 * __this, TypeConvertKey_t866134174  ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1960583928_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(TypeConvertKey_t866134174_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m2678190417_gshared (Transform_1_t2959969844 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Int32,Pathfinding.Int2>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3797623146_gshared (Transform_1_t132544136 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Int32,Pathfinding.Int2>::Invoke(TKey,TValue)
extern "C"  Int2_t1974045593  Transform_1_Invoke_m2329500238_gshared (Transform_1_t132544136 * __this, Int2_t1974045593  ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2329500238((Transform_1_t132544136 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Int2_t1974045593  (*FunctionPointerType) (Il2CppObject *, void* __this, Int2_t1974045593  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Int2_t1974045593  (*FunctionPointerType) (void* __this, Int2_t1974045593  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Int32,Pathfinding.Int2>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int2_t1974045593_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m970462457_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m970462457_gshared (Transform_1_t132544136 * __this, Int2_t1974045593  ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m970462457_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int2_t1974045593_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Int32,Pathfinding.Int2>::EndInvoke(System.IAsyncResult)
extern "C"  Int2_t1974045593  Transform_1_EndInvoke_m980773244_gshared (Transform_1_t132544136 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Int2_t1974045593 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Int32,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2698713043_gshared (Transform_1_t4205072453 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Int32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m263138181_gshared (Transform_1_t4205072453 * __this, Int2_t1974045593  ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m263138181((Transform_1_t4205072453 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Int2_t1974045593  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Int2_t1974045593  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Int32,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int2_t1974045593_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3391675824_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3391675824_gshared (Transform_1_t4205072453 * __this, Int2_t1974045593  ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3391675824_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int2_t1974045593_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Int32,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m2108522469_gshared (Transform_1_t4205072453 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Int32,System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3441981272_gshared (Transform_1_t302597331 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Int32,System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t2144098788  Transform_1_Invoke_m4178236448_gshared (Transform_1_t302597331 * __this, Int2_t1974045593  ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m4178236448((Transform_1_t302597331 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2144098788  (*FunctionPointerType) (Il2CppObject *, void* __this, Int2_t1974045593  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t2144098788  (*FunctionPointerType) (void* __this, Int2_t1974045593  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Int32,System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int2_t1974045593_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1740262091_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1740262091_gshared (Transform_1_t302597331 * __this, Int2_t1974045593  ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1740262091_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int2_t1974045593_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Int32,System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t2144098788  Transform_1_EndInvoke_m4203214954_gshared (Transform_1_t302597331 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t2144098788 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3210111976_gshared (Transform_1_t3607304339 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Int32,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m790753936_gshared (Transform_1_t3607304339 * __this, Int2_t1974045593  ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m790753936((Transform_1_t3607304339 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Int2_t1974045593  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Int2_t1974045593  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Int32,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int2_t1974045593_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3726300731_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3726300731_gshared (Transform_1_t3607304339 * __this, Int2_t1974045593  ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3726300731_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int2_t1974045593_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m254440442_gshared (Transform_1_t3607304339 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Object,Pathfinding.Int2>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1237824491_gshared (Transform_1_t2305320093 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Object,Pathfinding.Int2>::Invoke(TKey,TValue)
extern "C"  Int2_t1974045593  Transform_1_Invoke_m390448433_gshared (Transform_1_t2305320093 * __this, Int2_t1974045593  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m390448433((Transform_1_t2305320093 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Int2_t1974045593  (*FunctionPointerType) (Il2CppObject *, void* __this, Int2_t1974045593  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Int2_t1974045593  (*FunctionPointerType) (void* __this, Int2_t1974045593  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Object,Pathfinding.Int2>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int2_t1974045593_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2763237648_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2763237648_gshared (Transform_1_t2305320093 * __this, Int2_t1974045593  ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2763237648_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int2_t1974045593_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Object,Pathfinding.Int2>::EndInvoke(System.IAsyncResult)
extern "C"  Int2_t1974045593  Transform_1_EndInvoke_m1401447097_gshared (Transform_1_t2305320093 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Int2_t1974045593 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1407347732_gshared (Transform_1_t2082881114 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m3282682792_gshared (Transform_1_t2082881114 * __this, Int2_t1974045593  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3282682792((Transform_1_t2082881114 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Int2_t1974045593  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Int2_t1974045593  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int2_t1974045593_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1326856711_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1326856711_gshared (Transform_1_t2082881114 * __this, Int2_t1974045593  ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1326856711_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int2_t1974045593_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m4042097634_gshared (Transform_1_t2082881114 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Object,System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1244289458_gshared (Transform_1_t1197383863 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Object,System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t866109363  Transform_1_Invoke_m2238591366_gshared (Transform_1_t1197383863 * __this, Int2_t1974045593  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2238591366((Transform_1_t1197383863 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t866109363  (*FunctionPointerType) (Il2CppObject *, void* __this, Int2_t1974045593  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t866109363  (*FunctionPointerType) (void* __this, Int2_t1974045593  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Object,System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int2_t1974045593_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1260998833_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1260998833_gshared (Transform_1_t1197383863 * __this, Int2_t1974045593  ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1260998833_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int2_t1974045593_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Object,System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t866109363  Transform_1_EndInvoke_m2082496324_gshared (Transform_1_t1197383863 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t866109363 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3857383588_gshared (Transform_1_t207123575 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m2799707604_gshared (Transform_1_t207123575 * __this, Int2_t1974045593  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2799707604((Transform_1_t207123575 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Int2_t1974045593  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Int2_t1974045593  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int2_t1974045593_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2833103871_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2833103871_gshared (Transform_1_t207123575 * __this, Int2_t1974045593  ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2833103871_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int2_t1974045593_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int2,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1674464822_gshared (Transform_1_t207123575 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Int32,Pathfinding.Int3>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3759913576_gshared (Transform_1_t1799118306 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Int32,Pathfinding.Int3>::Invoke(TKey,TValue)
extern "C"  Int3_t1974045594  Transform_1_Invoke_m1648731408_gshared (Transform_1_t1799118306 * __this, Int3_t1974045594  ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1648731408((Transform_1_t1799118306 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Int3_t1974045594  (*FunctionPointerType) (Il2CppObject *, void* __this, Int3_t1974045594  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Int3_t1974045594  (*FunctionPointerType) (void* __this, Int3_t1974045594  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Int32,Pathfinding.Int3>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int3_t1974045594_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m320009659_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m320009659_gshared (Transform_1_t1799118306 * __this, Int3_t1974045594  ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m320009659_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int3_t1974045594_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Int32,Pathfinding.Int3>::EndInvoke(System.IAsyncResult)
extern "C"  Int3_t1974045594  Transform_1_EndInvoke_m4126552442_gshared (Transform_1_t1799118306 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Int3_t1974045594 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Int32,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3866909298_gshared (Transform_1_t1576679326 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Int32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m4074430150_gshared (Transform_1_t1576679326 * __this, Int3_t1974045594  ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m4074430150((Transform_1_t1576679326 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Int3_t1974045594  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Int3_t1974045594  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Int32,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int3_t1974045594_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2567994353_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2567994353_gshared (Transform_1_t1576679326 * __this, Int3_t1974045594  ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2567994353_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int3_t1974045594_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Int32,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m2891901444_gshared (Transform_1_t1576679326 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Int32,System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Int32>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m389702934_gshared (Transform_1_t2455359239 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Int32,System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Int32>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t2630286527  Transform_1_Invoke_m3988709922_gshared (Transform_1_t2455359239 * __this, Int3_t1974045594  ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3988709922((Transform_1_t2455359239 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2630286527  (*FunctionPointerType) (Il2CppObject *, void* __this, Int3_t1974045594  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t2630286527  (*FunctionPointerType) (void* __this, Int3_t1974045594  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Int32,System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Int32>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int3_t1974045594_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1946726093_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1946726093_gshared (Transform_1_t2455359239 * __this, Int3_t1974045594  ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1946726093_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int3_t1974045594_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Int32,System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Int32>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t2630286527  Transform_1_EndInvoke_m2478994728_gshared (Transform_1_t2455359239 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t2630286527 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1035431879_gshared (Transform_1_t978911212 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Int32,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m782994577_gshared (Transform_1_t978911212 * __this, Int3_t1974045594  ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m782994577((Transform_1_t978911212 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Int3_t1974045594  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Int3_t1974045594  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Int32,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int3_t1974045594_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3340933948_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3340933948_gshared (Transform_1_t978911212 * __this, Int3_t1974045594  ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3340933948_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int3_t1974045594_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m355352409_gshared (Transform_1_t978911212 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Object,Pathfinding.Int3>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2624315403_gshared (Transform_1_t3971894263 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Object,Pathfinding.Int3>::Invoke(TKey,TValue)
extern "C"  Int3_t1974045594  Transform_1_Invoke_m2073222417_gshared (Transform_1_t3971894263 * __this, Int3_t1974045594  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2073222417((Transform_1_t3971894263 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Int3_t1974045594  (*FunctionPointerType) (Il2CppObject *, void* __this, Int3_t1974045594  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Int3_t1974045594  (*FunctionPointerType) (void* __this, Int3_t1974045594  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Object,Pathfinding.Int3>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int3_t1974045594_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1745051888_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1745051888_gshared (Transform_1_t3971894263 * __this, Int3_t1974045594  ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1745051888_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int3_t1974045594_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Object,Pathfinding.Int3>::EndInvoke(System.IAsyncResult)
extern "C"  Int3_t1974045594  Transform_1_EndInvoke_m3731332825_gshared (Transform_1_t3971894263 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Int3_t1974045594 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3261693269_gshared (Transform_1_t3749455283 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m1173649543_gshared (Transform_1_t3749455283 * __this, Int3_t1974045594  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1173649543((Transform_1_t3749455283 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Int3_t1974045594  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Int3_t1974045594  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int3_t1974045594_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1562534886_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1562534886_gshared (Transform_1_t3749455283 * __this, Int3_t1974045594  ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1562534886_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int3_t1974045594_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m2557042083_gshared (Transform_1_t3749455283 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Object,System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3398894290_gshared (Transform_1_t3350145771 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Object,System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1352297102  Transform_1_Invoke_m3047713894_gshared (Transform_1_t3350145771 * __this, Int3_t1974045594  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3047713894((Transform_1_t3350145771 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1352297102  (*FunctionPointerType) (Il2CppObject *, void* __this, Int3_t1974045594  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t1352297102  (*FunctionPointerType) (void* __this, Int3_t1974045594  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Object,System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int3_t1974045594_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2403806097_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2403806097_gshared (Transform_1_t3350145771 * __this, Int3_t1974045594  ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2403806097_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int3_t1974045594_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Object,System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1352297102  Transform_1_EndInvoke_m3197016164_gshared (Transform_1_t3350145771 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t1352297102 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1343916227_gshared (Transform_1_t1873697744 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m3932898197_gshared (Transform_1_t1873697744 * __this, Int3_t1974045594  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3932898197((Transform_1_t1873697744 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Int3_t1974045594  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Int3_t1974045594  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int3_t1974045594_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1862812864_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1862812864_gshared (Transform_1_t1873697744 * __this, Int3_t1974045594  ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1862812864_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int3_t1974045594_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m4161584597_gshared (Transform_1_t1873697744 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<PushType,System.Object,PushType>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3401849547_gshared (Transform_1_t2321255387 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PushType,System.Object,PushType>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m400539217_gshared (Transform_1_t2321255387 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m400539217((Transform_1_t2321255387 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<PushType,System.Object,PushType>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* PushType_t1840642452_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2552903216_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2552903216_gshared (Transform_1_t2321255387 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2552903216_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(PushType_t1840642452_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PushType,System.Object,PushType>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m3915216281_gshared (Transform_1_t2321255387 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<PushType,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2230835227_gshared (Transform_1_t2232219549 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PushType,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m3781741697_gshared (Transform_1_t2232219549 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3781741697((Transform_1_t2232219549 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<PushType,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* PushType_t1840642452_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m683954144_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m683954144_gshared (Transform_1_t2232219549 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m683954144_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(PushType_t1840642452_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PushType,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m680438121_gshared (Transform_1_t2232219549 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<PushType,System.Object,System.Collections.Generic.KeyValuePair`2<PushType,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m892335762_gshared (Transform_1_t1000228275 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PushType,System.Object,System.Collections.Generic.KeyValuePair`2<PushType,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t519615340  Transform_1_Invoke_m2441587878_gshared (Transform_1_t1000228275 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2441587878((Transform_1_t1000228275 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t519615340  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t519615340  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<PushType,System.Object,System.Collections.Generic.KeyValuePair`2<PushType,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* PushType_t1840642452_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m4129257425_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m4129257425_gshared (Transform_1_t1000228275 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m4129257425_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(PushType_t1840642452_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PushType,System.Object,System.Collections.Generic.KeyValuePair`2<PushType,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t519615340  Transform_1_EndInvoke_m1729146916_gshared (Transform_1_t1000228275 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t519615340 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<PushType,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2879012413_gshared (Transform_1_t356462010 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PushType,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m1845569371_gshared (Transform_1_t356462010 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1845569371((Transform_1_t356462010 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<PushType,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* PushType_t1840642452_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1981911174_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1981911174_gshared (Transform_1_t356462010 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1981911174_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(PushType_t1840642452_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PushType,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1314972239_gshared (Transform_1_t356462010 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<SoundTypeID,System.Object,SoundTypeID>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m817208631_gshared (Transform_1_t290614651 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<SoundTypeID,System.Object,SoundTypeID>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m716310501_gshared (Transform_1_t290614651 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m716310501((Transform_1_t290614651 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<SoundTypeID,System.Object,SoundTypeID>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* SoundTypeID_t3626616740_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1414764612_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1414764612_gshared (Transform_1_t290614651 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1414764612_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(SoundTypeID_t3626616740_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<SoundTypeID,System.Object,SoundTypeID>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m1512515461_gshared (Transform_1_t290614651 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<SoundTypeID,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2484384923_gshared (Transform_1_t2710571821 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<SoundTypeID,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m437692093_gshared (Transform_1_t2710571821 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m437692093((Transform_1_t2710571821 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<SoundTypeID,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* SoundTypeID_t3626616740_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2951418856_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2951418856_gshared (Transform_1_t2710571821 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2951418856_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(SoundTypeID_t3626616740_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<SoundTypeID,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m3325514157_gshared (Transform_1_t2710571821 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<SoundTypeID,System.Object,System.Collections.Generic.KeyValuePair`2<SoundTypeID,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3015406184_gshared (Transform_1_t2906177779 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<SoundTypeID,System.Object,System.Collections.Generic.KeyValuePair`2<SoundTypeID,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1947212572  Transform_1_Invoke_m2098739472_gshared (Transform_1_t2906177779 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2098739472((Transform_1_t2906177779 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1947212572  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t1947212572  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<SoundTypeID,System.Object,System.Collections.Generic.KeyValuePair`2<SoundTypeID,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* SoundTypeID_t3626616740_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3714164155_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3714164155_gshared (Transform_1_t2906177779 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3714164155_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(SoundTypeID_t3626616740_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<SoundTypeID,System.Object,System.Collections.Generic.KeyValuePair`2<SoundTypeID,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1947212572  Transform_1_EndInvoke_m4152302970_gshared (Transform_1_t2906177779 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t1947212572 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<SoundTypeID,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2149355965_gshared (Transform_1_t834814282 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<SoundTypeID,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m4147896223_gshared (Transform_1_t834814282 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m4147896223((Transform_1_t834814282 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<SoundTypeID,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* SoundTypeID_t3626616740_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1258248062_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1258248062_gshared (Transform_1_t834814282 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1258248062_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(SoundTypeID_t3626616740_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<SoundTypeID,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1458771851_gshared (Transform_1_t834814282 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SoundStatus,SoundStatus>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1138848668_gshared (Transform_1_t2074406482 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SoundStatus,SoundStatus>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m2302132188_gshared (Transform_1_t2074406482 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2302132188((Transform_1_t2074406482 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SoundStatus,SoundStatus>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* SoundStatus_t3592938945_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2576733191_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2576733191_gshared (Transform_1_t2074406482 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2576733191_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(SoundStatus_t3592938945_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SoundStatus,SoundStatus>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m2417991470_gshared (Transform_1_t2074406482 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SoundStatus,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m896872441_gshared (Transform_1_t233074151 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SoundStatus,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m3433970659_gshared (Transform_1_t233074151 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3433970659((Transform_1_t233074151 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SoundStatus,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* SoundStatus_t3592938945_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3954253506_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3954253506_gshared (Transform_1_t233074151 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3954253506_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(SoundStatus_t3592938945_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SoundStatus,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m3906939079_gshared (Transform_1_t233074151 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SoundStatus,System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1253831826_gshared (Transform_1_t1970450427 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SoundStatus,System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3488982890  Transform_1_Invoke_m3365789606_gshared (Transform_1_t1970450427 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3365789606((Transform_1_t1970450427 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3488982890  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t3488982890  (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SoundStatus,System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* SoundStatus_t3592938945_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m369678289_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m369678289_gshared (Transform_1_t1970450427 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m369678289_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(SoundStatus_t3592938945_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SoundStatus,System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3488982890  Transform_1_EndInvoke_m1781308708_gshared (Transform_1_t1970450427 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t3488982890 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SoundStatus,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1696670862_gshared (Transform_1_t3930273333 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SoundStatus,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m681155950_gshared (Transform_1_t3930273333 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m681155950((Transform_1_t3930273333 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SoundStatus,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* SoundStatus_t3592938945_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2583245005_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2583245005_gshared (Transform_1_t3930273333 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2583245005_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(SoundStatus_t3592938945_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,SoundStatus,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m4180800092_gshared (Transform_1_t3930273333 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2761551185_gshared (Transform_1_t3765932824 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m1815730375_gshared (Transform_1_t3765932824 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1815730375((Transform_1_t3765932824 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2847273970_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2847273970_gshared (Transform_1_t3765932824 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2847273970_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m3480311139_gshared (Transform_1_t3765932824 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4243145684_gshared (Transform_1_t3064208655 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1049882445  Transform_1_Invoke_m3806805284_gshared (Transform_1_t3064208655 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3806805284((Transform_1_t3064208655 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1049882445  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t1049882445  (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2237662671_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2237662671_gshared (Transform_1_t3064208655 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2237662671_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1049882445  Transform_1_EndInvoke_m2328265958_gshared (Transform_1_t3064208655 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t1049882445 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3702420454_gshared (Transform_1_t3168164710 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m3961126226_gshared (Transform_1_t3168164710 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3961126226((Transform_1_t3168164710 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2680818173_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2680818173_gshared (Transform_1_t3168164710 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2680818173_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m3427236856_gshared (Transform_1_t3168164710 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3355330134_gshared (Transform_1_t1643741485 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m4168400550_gshared (Transform_1_t1643741485 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m4168400550((Transform_1_t1643741485 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1630268421_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1630268421_gshared (Transform_1_t1643741485 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1630268421_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m3617873444_gshared (Transform_1_t1643741485 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3753371890_gshared (Transform_1_t3958995187 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t4066860316  Transform_1_Invoke_m2319558726_gshared (Transform_1_t3958995187 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2319558726((Transform_1_t3958995187 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t4066860316  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t4066860316  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m365112689_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m365112689_gshared (Transform_1_t3958995187 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m365112689_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t4066860316  Transform_1_EndInvoke_m3974312068_gshared (Transform_1_t3958995187 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t4066860316 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m80961195_gshared (Transform_1_t1045973371 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m300848241_gshared (Transform_1_t1045973371 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m300848241((Transform_1_t1045973371 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1162957392_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1162957392_gshared (Transform_1_t1045973371 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1162957392_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m822184825_gshared (Transform_1_t1045973371 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m224461090_gshared (Transform_1_t4062951242 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m100698134_gshared (Transform_1_t4062951242 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m100698134((Transform_1_t4062951242 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3146712897_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3146712897_gshared (Transform_1_t4062951242 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3146712897_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1305038516_gshared (Transform_1_t4062951242 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Single,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3853010527_gshared (Transform_1_t2851648992 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Single,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m516279741_gshared (Transform_1_t2851648992 * __this, int32_t ___key0, float ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m516279741((Transform_1_t2851648992 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, int32_t ___key0, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Single,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m660985884_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m660985884_gshared (Transform_1_t2851648992 * __this, int32_t ___key0, float ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m660985884_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Single,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m1582942893_gshared (Transform_1_t2851648992 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Single,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4166432402_gshared (Transform_1_t993037999 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Single,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t4187962917  Transform_1_Invoke_m2782219942_gshared (Transform_1_t993037999 * __this, int32_t ___key0, float ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2782219942((Transform_1_t993037999 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t4187962917  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t4187962917  (*FunctionPointerType) (void* __this, int32_t ___key0, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Single,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2051601361_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2051601361_gshared (Transform_1_t993037999 * __this, int32_t ___key0, float ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2051601361_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Single,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t4187962917  Transform_1_EndInvoke_m4215918116_gshared (Transform_1_t993037999 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t4187962917 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Single,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1431636852_gshared (Transform_1_t2253880878 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Single,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m1847332552_gshared (Transform_1_t2253880878 * __this, int32_t ___key0, float ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1847332552((Transform_1_t2253880878 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Single,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1332439463_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1332439463_gshared (Transform_1_t2253880878 * __this, int32_t ___key0, float ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1332439463_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Single,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m2900559042_gshared (Transform_1_t2253880878 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Single,System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3553439248_gshared (Transform_1_t1096994054 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Single,System.Single>::Invoke(TKey,TValue)
extern "C"  float Transform_1_Invoke_m384425192_gshared (Transform_1_t1096994054 * __this, int32_t ___key0, float ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m384425192((Transform_1_t1096994054 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef float (*FunctionPointerType) (void* __this, int32_t ___key0, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Single,System.Single>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3640107539_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3640107539_gshared (Transform_1_t1096994054 * __this, int32_t ___key0, float ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3640107539_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Single,System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  float Transform_1_EndInvoke_m1530733218_gshared (Transform_1_t1096994054 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,HatredCtrl/stValue>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m88096103_gshared (Transform_1_t2514112805 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,HatredCtrl/stValue>::Invoke(TKey,TValue)
extern "C"  stValue_t3425945410  Transform_1_Invoke_m1178427957_gshared (Transform_1_t2514112805 * __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1178427957((Transform_1_t2514112805 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef stValue_t3425945410  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef stValue_t3425945410  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef stValue_t3425945410  (*FunctionPointerType) (void* __this, stValue_t3425945410  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,HatredCtrl/stValue>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* stValue_t3425945410_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3191743764_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3191743764_gshared (Transform_1_t2514112805 * __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3191743764_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(stValue_t3425945410_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,HatredCtrl/stValue>::EndInvoke(System.IAsyncResult)
extern "C"  stValue_t3425945410  Transform_1_EndInvoke_m90389301_gshared (Transform_1_t2514112805 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(stValue_t3425945410 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2683900453_gshared (Transform_1_t839774009 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m1766335799_gshared (Transform_1_t839774009 * __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1766335799((Transform_1_t839774009 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, stValue_t3425945410  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* stValue_t3425945410_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3748137494_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3748137494_gshared (Transform_1_t839774009 * __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3748137494_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(stValue_t3425945410_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m3631473395_gshared (Transform_1_t839774009 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m59148818_gshared (Transform_1_t287965411 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1199798016  Transform_1_Invoke_m4083138086_gshared (Transform_1_t287965411 * __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m4083138086((Transform_1_t287965411 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1199798016  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1199798016  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t1199798016  (*FunctionPointerType) (void* __this, stValue_t3425945410  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* stValue_t3425945410_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m369224273_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m369224273_gshared (Transform_1_t287965411 * __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m369224273_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(stValue_t3425945410_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1199798016  Transform_1_EndInvoke_m2466688676_gshared (Transform_1_t287965411 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t1199798016 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m84775411_gshared (Transform_1_t3258983766 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m1600063717_gshared (Transform_1_t3258983766 * __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1600063717((Transform_1_t3258983766 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, stValue_t3425945410  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* stValue_t3425945410_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m890314896_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m890314896_gshared (Transform_1_t3258983766 * __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m890314896_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(stValue_t3425945410_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m419399301_gshared (Transform_1_t3258983766 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo,Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m845981799_gshared (Transform_1_t3465660101 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo,Mihua.Assets.SubAssetMgr/ErrorInfo>::Invoke(TKey,TValue)
extern "C"  ErrorInfo_t2633981210  Transform_1_Invoke_m2387463989_gshared (Transform_1_t3465660101 * __this, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2387463989((Transform_1_t3465660101 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef ErrorInfo_t2633981210  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef ErrorInfo_t2633981210  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef ErrorInfo_t2633981210  (*FunctionPointerType) (void* __this, ErrorInfo_t2633981210  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo,Mihua.Assets.SubAssetMgr/ErrorInfo>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ErrorInfo_t2633981210_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3619760660_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3619760660_gshared (Transform_1_t3465660101 * __this, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3619760660_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ErrorInfo_t2633981210_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo,Mihua.Assets.SubAssetMgr/ErrorInfo>::EndInvoke(System.IAsyncResult)
extern "C"  ErrorInfo_t2633981210  Transform_1_EndInvoke_m3065818677_gshared (Transform_1_t3465660101 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(ErrorInfo_t2633981210 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m434775037_gshared (Transform_1_t2583285505 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m3507968607_gshared (Transform_1_t2583285505 * __this, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3507968607((Transform_1_t2583285505 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, ErrorInfo_t2633981210  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ErrorInfo_t2633981210_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3878339390_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3878339390_gshared (Transform_1_t2583285505 * __this, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3878339390_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ErrorInfo_t2633981210_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m3292751051_gshared (Transform_1_t2583285505 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo,System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4084780306_gshared (Transform_1_t1239512707 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo,System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t407833816  Transform_1_Invoke_m4041302822_gshared (Transform_1_t1239512707 * __this, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m4041302822((Transform_1_t1239512707 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t407833816  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t407833816  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t407833816  (*FunctionPointerType) (void* __this, ErrorInfo_t2633981210  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo,System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ErrorInfo_t2633981210_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3660707665_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3660707665_gshared (Transform_1_t1239512707 * __this, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3660707665_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ErrorInfo_t2633981210_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo,System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t407833816  Transform_1_EndInvoke_m1249634724_gshared (Transform_1_t1239512707 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t407833816 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3259107611_gshared (Transform_1_t707527966 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m2968251069_gshared (Transform_1_t707527966 * __this, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2968251069((Transform_1_t707527966 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, ErrorInfo_t2633981210  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ErrorInfo_t2633981210_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2164260456_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2164260456_gshared (Transform_1_t707527966 * __this, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2164260456_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ErrorInfo_t2633981210_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m942640557_gshared (Transform_1_t707527966 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1185491017_gshared (Transform_1_t2613442969 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,Newtonsoft.Json.Schema.JsonSchemaType>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m3682491923_gshared (Transform_1_t2613442969 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3682491923((Transform_1_t2613442969 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,Newtonsoft.Json.Schema.JsonSchemaType>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* JsonSchemaType_t2115415821_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m295632754_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m295632754_gshared (Transform_1_t2613442969 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m295632754_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(JsonSchemaType_t2115415821_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,Newtonsoft.Json.Schema.JsonSchemaType>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m2169817239_gshared (Transform_1_t2613442969 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1636139410_gshared (Transform_1_t2249633762 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m1005459750_gshared (Transform_1_t2249633762 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1005459750((Transform_1_t2249633762 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* JsonSchemaType_t2115415821_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1967578577_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1967578577_gshared (Transform_1_t2249633762 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1967578577_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(JsonSchemaType_t2115415821_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m729904036_gshared (Transform_1_t2249633762 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1096657366_gshared (Transform_1_t387295575 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t4184235723  Transform_1_Invoke_m4109029474_gshared (Transform_1_t387295575 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m4109029474((Transform_1_t387295575 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t4184235723  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t4184235723  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t4184235723  (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* JsonSchemaType_t2115415821_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3642067981_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3642067981_gshared (Transform_1_t387295575 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3642067981_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(JsonSchemaType_t2115415821_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t4184235723  Transform_1_EndInvoke_m132062952_gshared (Transform_1_t387295575 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t4184235723 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m998750310_gshared (Transform_1_t373876223 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m3348340118_gshared (Transform_1_t373876223 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3348340118((Transform_1_t373876223 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* JsonSchemaType_t2115415821_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m4149939701_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m4149939701_gshared (Transform_1_t373876223 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m4149939701_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(JsonSchemaType_t2115415821_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1304021300_gshared (Transform_1_t373876223 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m112529371_gshared (Transform_1_t1036041189 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m2937455041_gshared (Transform_1_t1036041189 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2937455041((Transform_1_t1036041189 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* PropertyPresence_t2892329490_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1042671136_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1042671136_gshared (Transform_1_t1036041189 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1042671136_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(PropertyPresence_t2892329490_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m2959584297_gshared (Transform_1_t1036041189 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4254296073_gshared (Transform_1_t4190285609 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m3521402127_gshared (Transform_1_t4190285609 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3521402127((Transform_1_t4190285609 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* PropertyPresence_t2892329490_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m242178106_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m242178106_gshared (Transform_1_t4190285609 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m242178106_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(PropertyPresence_t2892329490_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m1368906779_gshared (Transform_1_t4190285609 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence,System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3099390276_gshared (Transform_1_t3104861091 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence,System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t666182096  Transform_1_Invoke_m200965556_gshared (Transform_1_t3104861091 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m200965556((Transform_1_t3104861091 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t666182096  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t666182096  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t666182096  (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence,System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* PropertyPresence_t2892329490_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m79763039_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m79763039_gshared (Transform_1_t3104861091 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m79763039_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(PropertyPresence_t2892329490_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence,System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t666182096  Transform_1_EndInvoke_m3306703446_gshared (Transform_1_t3104861091 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t666182096 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3752383887_gshared (Transform_1_t2314528070 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m124887565_gshared (Transform_1_t2314528070 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m124887565((Transform_1_t2314528070 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* PropertyPresence_t2892329490_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1993130988_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1993130988_gshared (Transform_1_t2314528070 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1993130988_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(PropertyPresence_t2892329490_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1900356701_gshared (Transform_1_t2314528070 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,ProductsCfgMgr/ProductInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1888013799_gshared (Transform_1_t3750066325 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,ProductsCfgMgr/ProductInfo>::Invoke(TKey,TValue)
extern "C"  ProductInfo_t1305991238  Transform_1_Invoke_m1325702581_gshared (Transform_1_t3750066325 * __this, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1325702581((Transform_1_t3750066325 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef ProductInfo_t1305991238  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef ProductInfo_t1305991238  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef ProductInfo_t1305991238  (*FunctionPointerType) (void* __this, ProductInfo_t1305991238  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,ProductsCfgMgr/ProductInfo>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ProductInfo_t1305991238_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2753670292_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2753670292_gshared (Transform_1_t3750066325 * __this, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2753670292_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ProductInfo_t1305991238_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,ProductsCfgMgr/ProductInfo>::EndInvoke(System.IAsyncResult)
extern "C"  ProductInfo_t1305991238  Transform_1_EndInvoke_m1412157365_gshared (Transform_1_t3750066325 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(ProductInfo_t1305991238 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1454147881_gshared (Transform_1_t4195681701 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m357065907_gshared (Transform_1_t4195681701 * __this, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m357065907((Transform_1_t4195681701 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, ProductInfo_t1305991238  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ProductInfo_t1305991238_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2897740690_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2897740690_gshared (Transform_1_t4195681701 * __this, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2897740690_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ProductInfo_t1305991238_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m506617335_gshared (Transform_1_t4195681701 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,System.Collections.Generic.KeyValuePair`2<System.Object,ProductsCfgMgr/ProductInfo>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m992106642_gshared (Transform_1_t1523918931 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,System.Collections.Generic.KeyValuePair`2<System.Object,ProductsCfgMgr/ProductInfo>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3374811140  Transform_1_Invoke_m3189605286_gshared (Transform_1_t1523918931 * __this, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3189605286((Transform_1_t1523918931 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3374811140  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3374811140  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t3374811140  (*FunctionPointerType) (void* __this, ProductInfo_t1305991238  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,System.Collections.Generic.KeyValuePair`2<System.Object,ProductsCfgMgr/ProductInfo>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ProductInfo_t1305991238_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1205593041_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1205593041_gshared (Transform_1_t1523918931 * __this, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1205593041_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ProductInfo_t1305991238_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,System.Collections.Generic.KeyValuePair`2<System.Object,ProductsCfgMgr/ProductInfo>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3374811140  Transform_1_EndInvoke_m759068964_gshared (Transform_1_t1523918931 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t3374811140 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m953761903_gshared (Transform_1_t2319924162 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m1990890473_gshared (Transform_1_t2319924162 * __this, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1990890473((Transform_1_t2319924162 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, ProductInfo_t1305991238  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ProductInfo_t1305991238_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3614224788_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3614224788_gshared (Transform_1_t2319924162 * __this, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3614224788_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ProductInfo_t1305991238_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m2825236737_gshared (Transform_1_t2319924162 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2503808327_gshared (Transform_1_t1899867829 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Boolean>::Invoke(TKey,TValue)
extern "C"  bool Transform_1_Invoke_m159606869_gshared (Transform_1_t1899867829 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m159606869((Transform_1_t1899867829 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Boolean>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2945688884_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2945688884_gshared (Transform_1_t1899867829 * __this, Il2CppObject * ___key0, bool ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2945688884_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Transform_1_EndInvoke_m3128041749_gshared (Transform_1_t1899867829 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m199821900_gshared (Transform_1_t3174675725 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m3999618288_gshared (Transform_1_t3174675725 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3999618288((Transform_1_t3174675725 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m960240079_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m960240079_gshared (Transform_1_t3174675725 * __this, Il2CppObject * ___key0, bool ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m960240079_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m468964762_gshared (Transform_1_t3174675725 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3170899378_gshared (Transform_1_t3968687731 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t2545618620  Transform_1_Invoke_m2434466950_gshared (Transform_1_t3968687731 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2434466950((Transform_1_t3968687731 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2545618620  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2545618620  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t2545618620  (*FunctionPointerType) (void* __this, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m987788977_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m987788977_gshared (Transform_1_t3968687731 * __this, Il2CppObject * ___key0, bool ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m987788977_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t2545618620  Transform_1_EndInvoke_m406957124_gshared (Transform_1_t3968687731 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t2545618620 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2344546156_gshared (Transform_1_t1298918186 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m3218823052_gshared (Transform_1_t1298918186 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3218823052((Transform_1_t1298918186 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2152369975_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2152369975_gshared (Transform_1_t1298918186 * __this, Il2CppObject * ___key0, bool ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2152369975_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m4165556606_gshared (Transform_1_t1298918186 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m336265058_gshared (Transform_1_t4217408362 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m2719890774_gshared (Transform_1_t4217408362 * __this, Il2CppObject * ___key0, KeyValuePair_2_t4287931429  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2719890774((Transform_1_t4217408362 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, KeyValuePair_2_t4287931429  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, KeyValuePair_2_t4287931429  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, KeyValuePair_2_t4287931429  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* KeyValuePair_2_t4287931429_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3204479489_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3204479489_gshared (Transform_1_t4217408362 * __this, Il2CppObject * ___key0, KeyValuePair_2_t4287931429  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3204479489_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(KeyValuePair_2_t4287931429_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m1944179060_gshared (Transform_1_t4217408362 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1654913398_gshared (Transform_1_t232618487 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t2061784035  Transform_1_Invoke_m1505379522_gshared (Transform_1_t232618487 * __this, Il2CppObject * ___key0, KeyValuePair_2_t4287931429  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1505379522((Transform_1_t232618487 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2061784035  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, KeyValuePair_2_t4287931429  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2061784035  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, KeyValuePair_2_t4287931429  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t2061784035  (*FunctionPointerType) (void* __this, KeyValuePair_2_t4287931429  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* KeyValuePair_2_t4287931429_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m958656621_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m958656621_gshared (Transform_1_t232618487 * __this, Il2CppObject * ___key0, KeyValuePair_2_t4287931429  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m958656621_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(KeyValuePair_2_t4287931429_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t2061784035  Transform_1_EndInvoke_m1027419784_gshared (Transform_1_t232618487 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t2061784035 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m720234665_gshared (Transform_1_t2458765881 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t4287931429  Transform_1_Invoke_m2228476339_gshared (Transform_1_t2458765881 * __this, Il2CppObject * ___key0, KeyValuePair_2_t4287931429  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2228476339((Transform_1_t2458765881 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t4287931429  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, KeyValuePair_2_t4287931429  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t4287931429  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, KeyValuePair_2_t4287931429  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t4287931429  (*FunctionPointerType) (void* __this, KeyValuePair_2_t4287931429  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* KeyValuePair_2_t4287931429_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m230394642_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m230394642_gshared (Transform_1_t2458765881 * __this, Il2CppObject * ___key0, KeyValuePair_2_t4287931429  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m230394642_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(KeyValuePair_2_t4287931429_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t4287931429  Transform_1_EndInvoke_m1247529207_gshared (Transform_1_t2458765881 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t4287931429 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4018485398_gshared (Transform_1_t2341650823 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m1851734886_gshared (Transform_1_t2341650823 * __this, Il2CppObject * ___key0, KeyValuePair_2_t4287931429  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1851734886((Transform_1_t2341650823 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, KeyValuePair_2_t4287931429  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, KeyValuePair_2_t4287931429  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, KeyValuePair_2_t4287931429  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* KeyValuePair_2_t4287931429_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2963420101_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2963420101_gshared (Transform_1_t2341650823 * __this, Il2CppObject * ___key0, KeyValuePair_2_t4287931429  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2963420101_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(KeyValuePair_2_t4287931429_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1020291428_gshared (Transform_1_t2341650823 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m641310834_gshared (Transform_1_t2949220623 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m3922456586_gshared (Transform_1_t2949220623 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3922456586((Transform_1_t2949220623 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2253318505_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2253318505_gshared (Transform_1_t2949220623 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2253318505_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m2079925824_gshared (Transform_1_t2949220623 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1506220658_gshared (Transform_1_t125305115 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3222658402  Transform_1_Invoke_m417703622_gshared (Transform_1_t125305115 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m417703622((Transform_1_t125305115 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3222658402  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3222658402  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t3222658402  (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1076276209_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1076276209_gshared (Transform_1_t125305115 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1076276209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3222658402  Transform_1_EndInvoke_m88547844_gshared (Transform_1_t125305115 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t3222658402 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1991062983_gshared (Transform_1_t2351452509 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m3088902357_gshared (Transform_1_t2351452509 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3088902357((Transform_1_t2351452509 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m573338292_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m573338292_gshared (Transform_1_t2351452509 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m573338292_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m3451605141_gshared (Transform_1_t2351452509 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3603041670_gshared (Transform_1_t1073463084 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m631029810_gshared (Transform_1_t1073463084 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m631029810((Transform_1_t1073463084 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2048389981_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2048389981_gshared (Transform_1_t1073463084 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2048389981_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1212689688_gshared (Transform_1_t1073463084 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2052388693_gshared (Transform_1_t827029284 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m757436355_gshared (Transform_1_t827029284 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m757436355((Transform_1_t827029284 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m397518190_gshared (Transform_1_t827029284 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m3155601639_gshared (Transform_1_t827029284 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1310500508_gshared (Transform_1_t1020091647 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1944668977  Transform_1_Invoke_m1166627932_gshared (Transform_1_t1020091647 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1166627932((Transform_1_t1020091647 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1944668977  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1944668977  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t1944668977  (*FunctionPointerType) (void* __this, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3524588039_gshared (Transform_1_t1020091647 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1944668977  Transform_1_EndInvoke_m865876654_gshared (Transform_1_t1020091647 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t1944668977 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m582405827_gshared (Transform_1_t3246239041 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m3707150041_gshared (Transform_1_t3246239041 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3707150041((Transform_1_t3246239041 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m788143672_gshared (Transform_1_t3246239041 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m3248123921_gshared (Transform_1_t3246239041 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2550069086_gshared (Transform_1_t2034936791 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m1400282842_gshared (Transform_1_t2034936791 * __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1400282842((Transform_1_t2034936791 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3723202949_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3723202949_gshared (Transform_1_t2034936791 * __this, Il2CppObject * ___key0, float ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3723202949_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m1120671088_gshared (Transform_1_t2034936791 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3757893742_gshared (Transform_1_t2349101755 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t2065771578  Transform_1_Invoke_m4133218506_gshared (Transform_1_t2349101755 * __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m4133218506((Transform_1_t2349101755 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2065771578  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2065771578  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t2065771578  (*FunctionPointerType) (void* __this, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3465937013_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3465937013_gshared (Transform_1_t2349101755 * __this, Il2CppObject * ___key0, float ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3465937013_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t2065771578  Transform_1_EndInvoke_m762983808_gshared (Transform_1_t2349101755 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t2065771578 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3798645530_gshared (Transform_1_t159179252 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m108556130_gshared (Transform_1_t159179252 * __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m108556130((Transform_1_t159179252 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1747120577_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1747120577_gshared (Transform_1_t159179252 * __this, Il2CppObject * ___key0, float ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1747120577_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m3253215208_gshared (Transform_1_t159179252 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3911383985_gshared (Transform_1_t280281853 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Single>::Invoke(TKey,TValue)
extern "C"  float Transform_1_Invoke_m3990877099_gshared (Transform_1_t280281853 * __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3990877099((Transform_1_t280281853 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef float (*FunctionPointerType) (void* __this, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Single>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1281538314_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1281538314_gshared (Transform_1_t280281853 * __this, Il2CppObject * ___key0, float ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1281538314_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  float Transform_1_EndInvoke_m3473818623_gshared (Transform_1_t280281853 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2899387125_gshared (Transform_1_t520022276 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m1200330787_gshared (Transform_1_t520022276 * __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1200330787((Transform_1_t520022276 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt16_t24667923_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m293588430_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m293588430_gshared (Transform_1_t520022276 * __this, Il2CppObject * ___key0, uint16_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m293588430_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(UInt16_t24667923_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m3048842375_gshared (Transform_1_t520022276 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1124898268_gshared (Transform_1_t861903487 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t2093487825  Transform_1_Invoke_m3709914396_gshared (Transform_1_t861903487 * __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3709914396((Transform_1_t861903487 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2093487825  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2093487825  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t2093487825  (*FunctionPointerType) (void* __this, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt16_t24667923_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1391766215_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1391766215_gshared (Transform_1_t861903487 * __this, Il2CppObject * ___key0, uint16_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1391766215_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(UInt16_t24667923_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t2093487825  Transform_1_EndInvoke_m3529886190_gshared (Transform_1_t861903487 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t2093487825 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1659019043_gshared (Transform_1_t2939232033 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m2893027961_gshared (Transform_1_t2939232033 * __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2893027961((Transform_1_t2939232033 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt16_t24667923_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m708278744_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m708278744_gshared (Transform_1_t2939232033 * __this, Il2CppObject * ___key0, uint16_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m708278744_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(UInt16_t24667923_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m3858591857_gshared (Transform_1_t2939232033 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.UInt16>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2191038339_gshared (Transform_1_t3088050881 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.UInt16>::Invoke(TKey,TValue)
extern "C"  uint16_t Transform_1_Invoke_m1314156057_gshared (Transform_1_t3088050881 * __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1314156057((Transform_1_t3088050881 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef uint16_t (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef uint16_t (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint16_t (*FunctionPointerType) (void* __this, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.UInt16>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt16_t24667923_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3928468856_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3928468856_gshared (Transform_1_t3088050881 * __this, Il2CppObject * ___key0, uint16_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3928468856_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(UInt16_t24667923_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.UInt16>::EndInvoke(System.IAsyncResult)
extern "C"  uint16_t Transform_1_EndInvoke_m1430165713_gshared (Transform_1_t3088050881 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(uint16_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m380669709_gshared (Transform_1_t1958177301 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m2187326475_gshared (Transform_1_t1958177301 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2187326475((Transform_1_t1958177301 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* TextEditOp_t4145961110_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3824501430_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3824501430_gshared (Transform_1_t1958177301 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3824501430_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(TextEditOp_t4145961110_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m1294747551_gshared (Transform_1_t1958177301 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1812037516_gshared (Transform_1_t2126384403 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1919813716  Transform_1_Invoke_m514401132_gshared (Transform_1_t2126384403 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m514401132((Transform_1_t2126384403 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1919813716  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1919813716  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t1919813716  (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* TextEditOp_t4145961110_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3477811991_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3477811991_gshared (Transform_1_t2126384403 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3477811991_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(TextEditOp_t4145961110_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1919813716  Transform_1_EndInvoke_m3773784478_gshared (Transform_1_t2126384403 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t1919813716 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2623329291_gshared (Transform_1_t82419762 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m1148525969_gshared (Transform_1_t82419762 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1148525969((Transform_1_t82419762 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* TextEditOp_t4145961110_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2482881520_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2482881520_gshared (Transform_1_t82419762 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2482881520_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(TextEditOp_t4145961110_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1450184281_gshared (Transform_1_t82419762 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m809054291_gshared (Transform_1_t57564501 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,UnityEngine.TextEditor/TextEditOp>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m2664867145_gshared (Transform_1_t57564501 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2664867145((Transform_1_t57564501 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,UnityEngine.TextEditor/TextEditOp>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* TextEditOp_t4145961110_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1557263016_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1557263016_gshared (Transform_1_t57564501 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1557263016_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(TextEditOp_t4145961110_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,UnityEngine.TextEditor/TextEditOp>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m2605908897_gshared (Transform_1_t57564501 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.UInt16,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2815183605_gshared (Transform_1_t3432882628 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.UInt16,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m2862968867_gshared (Transform_1_t3432882628 * __this, uint16_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2862968867((Transform_1_t3432882628 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, uint16_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, uint16_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.UInt16,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt16_t24667923_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3420562894_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3420562894_gshared (Transform_1_t3432882628 * __this, uint16_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3420562894_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UInt16_t24667923_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.UInt16,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m2089340039_gshared (Transform_1_t3432882628 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.UInt16,System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3285771740_gshared (Transform_1_t3318937983 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.UInt16,System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1637661969  Transform_1_Invoke_m2269019420_gshared (Transform_1_t3318937983 * __this, uint16_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2269019420((Transform_1_t3318937983 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1637661969  (*FunctionPointerType) (Il2CppObject *, void* __this, uint16_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t1637661969  (*FunctionPointerType) (void* __this, uint16_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.UInt16,System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt16_t24667923_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m4188166855_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m4188166855_gshared (Transform_1_t3318937983 * __this, uint16_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m4188166855_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UInt16_t24667923_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.UInt16,System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1637661969  Transform_1_EndInvoke_m3319405038_gshared (Transform_1_t3318937983 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t1637661969 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.UInt16,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m876508963_gshared (Transform_1_t1557125089 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.UInt16,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m970932857_gshared (Transform_1_t1557125089 * __this, uint16_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m970932857((Transform_1_t1557125089 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, uint16_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, uint16_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.UInt16,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt16_t24667923_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1667146200_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1667146200_gshared (Transform_1_t1557125089 * __this, uint16_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1667146200_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UInt16_t24667923_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.UInt16,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m2936359025_gshared (Transform_1_t1557125089 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.UInt16,System.Object,System.UInt16>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1408528259_gshared (Transform_1_t1705943937 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.UInt16,System.Object,System.UInt16>::Invoke(TKey,TValue)
extern "C"  uint16_t Transform_1_Invoke_m3687028249_gshared (Transform_1_t1705943937 * __this, uint16_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3687028249((Transform_1_t1705943937 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef uint16_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint16_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint16_t (*FunctionPointerType) (void* __this, uint16_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.UInt16,System.Object,System.UInt16>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt16_t24667923_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m592369016_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m592369016_gshared (Transform_1_t1705943937 * __this, uint16_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m592369016_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UInt16_t24667923_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.UInt16,System.Object,System.UInt16>::EndInvoke(System.IAsyncResult)
extern "C"  uint16_t Transform_1_EndInvoke_m507932881_gshared (Transform_1_t1705943937 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(uint16_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.UInt32,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2993042351_gshared (Transform_1_t1309936622 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.UInt32,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m798124713_gshared (Transform_1_t1309936622 * __this, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m798124713((Transform_1_t1309936622 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.UInt32,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t24667981_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m4204995156_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m4204995156_gshared (Transform_1_t1309936622 * __this, uint32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m4204995156_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UInt32_t24667981_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.UInt32,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m1855464001_gshared (Transform_1_t1309936622 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.UInt32,System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2497360080_gshared (Transform_1_t3625077063 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.UInt32,System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t4066747055  Transform_1_Invoke_m1095149736_gshared (Transform_1_t3625077063 * __this, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1095149736((Transform_1_t3625077063 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t4066747055  (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t4066747055  (*FunctionPointerType) (void* __this, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.UInt32,System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t24667981_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3665496659_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3665496659_gshared (Transform_1_t3625077063 * __this, uint32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3665496659_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UInt32_t24667981_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.UInt32,System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t4066747055  Transform_1_EndInvoke_m46104290_gshared (Transform_1_t3625077063 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t4066747055 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.UInt32,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1124290089_gshared (Transform_1_t3729146379 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.UInt32,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m2271477811_gshared (Transform_1_t3729146379 * __this, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2271477811((Transform_1_t3729146379 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.UInt32,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t24667981_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1224842642_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1224842642_gshared (Transform_1_t3729146379 * __this, uint32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1224842642_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UInt32_t24667981_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.UInt32,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1160417911_gshared (Transform_1_t3729146379 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.UInt32,System.Object,System.UInt32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1869660815_gshared (Transform_1_t3877965285 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.UInt32,System.Object,System.UInt32>::Invoke(TKey,TValue)
extern "C"  uint32_t Transform_1_Invoke_m4169469069_gshared (Transform_1_t3877965285 * __this, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m4169469069((Transform_1_t3877965285 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.UInt32,System.Object,System.UInt32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t24667981_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m930465772_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m930465772_gshared (Transform_1_t3877965285 * __this, uint32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m930465772_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UInt32_t24667981_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.UInt32,System.Object,System.UInt32>::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t Transform_1_EndInvoke_m2375952861_gshared (Transform_1_t3877965285 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(uint32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<UIModelDisplayType,System.Int32,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m559210873_gshared (Transform_1_t3029156963 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UIModelDisplayType,System.Int32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m914674463_gshared (Transform_1_t3029156963 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m914674463((Transform_1_t3029156963 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<UIModelDisplayType,System.Int32,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* UIModelDisplayType_t1891752679_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3171011018_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3171011018_gshared (Transform_1_t3029156963 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3171011018_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UIModelDisplayType_t1891752679_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UIModelDisplayType,System.Int32,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m3010917387_gshared (Transform_1_t3029156963 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<UIModelDisplayType,System.Int32,System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3993918820_gshared (Transform_1_t4128861355 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UIModelDisplayType,System.Int32,System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t2851311006  Transform_1_Invoke_m2677262484_gshared (Transform_1_t4128861355 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2677262484((Transform_1_t4128861355 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2851311006  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t2851311006  (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<UIModelDisplayType,System.Int32,System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* UIModelDisplayType_t1891752679_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m205662271_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m205662271_gshared (Transform_1_t4128861355 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m205662271_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UIModelDisplayType_t1891752679_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UIModelDisplayType,System.Int32,System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t2851311006  Transform_1_EndInvoke_m1025340278_gshared (Transform_1_t4128861355 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t2851311006 *)UnBox ((Il2CppCodeGenObject*)__result);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
