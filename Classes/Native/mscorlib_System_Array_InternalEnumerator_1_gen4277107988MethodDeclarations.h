﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4277107988.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21199798016.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m813242916_gshared (InternalEnumerator_1_t4277107988 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m813242916(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4277107988 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m813242916_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1371341052_gshared (InternalEnumerator_1_t4277107988 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1371341052(__this, method) ((  void (*) (InternalEnumerator_1_t4277107988 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1371341052_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2292629746_gshared (InternalEnumerator_1_t4277107988 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2292629746(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4277107988 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2292629746_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2959491323_gshared (InternalEnumerator_1_t4277107988 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2959491323(__this, method) ((  void (*) (InternalEnumerator_1_t4277107988 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2959491323_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m139141292_gshared (InternalEnumerator_1_t4277107988 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m139141292(__this, method) ((  bool (*) (InternalEnumerator_1_t4277107988 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m139141292_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>>::get_Current()
extern "C"  KeyValuePair_2_t1199798016  InternalEnumerator_1_get_Current_m3673987149_gshared (InternalEnumerator_1_t4277107988 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3673987149(__this, method) ((  KeyValuePair_2_t1199798016  (*) (InternalEnumerator_1_t4277107988 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3673987149_gshared)(__this, method)
