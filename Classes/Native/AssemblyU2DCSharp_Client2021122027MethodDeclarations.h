﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Client
struct Client_t2021122027;
// System.String
struct String_t;
// Client/ResponseCallBack
struct ResponseCallBack_t59495818;
// Client/PushCallBack
struct PushCallBack_t4203546979;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// Pomelo.DotNetClient.PomeloClient
struct PomeloClient_t4215271137;
// System.Object
struct Il2CppObject;
// CEvent.ZEvent
struct ZEvent_t3638018500;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Client_ResponseCallBack59495818.h"
#include "AssemblyU2DCSharp_Client_PushCallBack4203546979.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_NetWorkState3329123775.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_PomeloClient4215271137.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"

// System.Void Client::.ctor()
extern "C"  void Client__ctor_m2728738064 (Client_t2021122027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Client::.cctor()
extern "C"  void Client__cctor_m2504405149 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Client::get_IsNetConnected()
extern "C"  bool Client_get_IsNetConnected_m1062007335 (Client_t2021122027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Client::CloseNetState()
extern "C"  void Client_CloseNetState_m4259512634 (Client_t2021122027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Client::Run(System.String,System.Int32,Client/ResponseCallBack,Client/PushCallBack)
extern "C"  void Client_Run_m1931402649 (Client_t2021122027 * __this, String_t* ___host0, int32_t ___port1, ResponseCallBack_t59495818 * ___responseFun2, PushCallBack_t4203546979 * ___pushFun3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Client::Request(System.String,Newtonsoft.Json.Linq.JObject)
extern "C"  void Client_Request_m2262250609 (Client_t2021122027 * __this, String_t* ___route0, JObject_t1798544199 * ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Client::Request(System.String,System.String)
extern "C"  uint32_t Client_Request_m1265248124 (Client_t2021122027 * __this, String_t* ___route0, String_t* ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Client::Notify(System.String,System.String)
extern "C"  void Client_Notify_m1455177505 (Client_t2021122027 * __this, String_t* ___route0, String_t* ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Client::Notify(System.String,Newtonsoft.Json.Linq.JObject)
extern "C"  void Client_Notify_m581543217 (Client_t2021122027 * __this, String_t* ___route0, JObject_t1798544199 * ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Client::OnStateChanged(Pomelo.DotNetClient.NetWorkState)
extern "C"  void Client_OnStateChanged_m563709784 (Client_t2021122027 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Client::OnDisconnect()
extern "C"  void Client_OnDisconnect_m2718816335 (Client_t2021122027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Client::Logout()
extern "C"  void Client_Logout_m1124329598 (Client_t2021122027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Client::<OnDisconnect>m__3FF()
extern "C"  void Client_U3COnDisconnectU3Em__3FF_m1809054849 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Client::ilo_notify1(Pomelo.DotNetClient.PomeloClient,System.String,Newtonsoft.Json.Linq.JObject)
extern "C"  void Client_ilo_notify1_m3361473591 (Il2CppObject * __this /* static, unused */, PomeloClient_t4215271137 * ____this0, String_t* ___route1, JObject_t1798544199 * ___msg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Client::ilo_Log2(System.Object,System.Boolean)
extern "C"  void Client_ilo_Log2_m1839616126 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Client::ilo_Logout3(Pomelo.DotNetClient.PomeloClient)
extern "C"  void Client_ilo_Logout3_m2854244708 (Il2CppObject * __this /* static, unused */, PomeloClient_t4215271137 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Client::ilo_DispatchEvent4(CEvent.ZEvent)
extern "C"  void Client_ilo_DispatchEvent4_m2814498368 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
