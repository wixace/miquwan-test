﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIScrollViewGenerated/<UIScrollView_onStoppedMoving_GetDelegate_member19_arg0>c__AnonStoreyCE
struct U3CUIScrollView_onStoppedMoving_GetDelegate_member19_arg0U3Ec__AnonStoreyCE_t3766231288;

#include "codegen/il2cpp-codegen.h"

// System.Void UIScrollViewGenerated/<UIScrollView_onStoppedMoving_GetDelegate_member19_arg0>c__AnonStoreyCE::.ctor()
extern "C"  void U3CUIScrollView_onStoppedMoving_GetDelegate_member19_arg0U3Ec__AnonStoreyCE__ctor_m4287481715 (U3CUIScrollView_onStoppedMoving_GetDelegate_member19_arg0U3Ec__AnonStoreyCE_t3766231288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated/<UIScrollView_onStoppedMoving_GetDelegate_member19_arg0>c__AnonStoreyCE::<>m__161()
extern "C"  void U3CUIScrollView_onStoppedMoving_GetDelegate_member19_arg0U3Ec__AnonStoreyCE_U3CU3Em__161_m3165305746 (U3CUIScrollView_onStoppedMoving_GetDelegate_member19_arg0U3Ec__AnonStoreyCE_t3766231288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
