﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_jowpea99
struct  M_jowpea99_t1613162986  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_jowpea99::_torsta
	float ____torsta_0;
	// System.Boolean GarbageiOS.M_jowpea99::_celreQearfis
	bool ____celreQearfis_1;
	// System.String GarbageiOS.M_jowpea99::_lapeldreKerepe
	String_t* ____lapeldreKerepe_2;
	// System.Boolean GarbageiOS.M_jowpea99::_bigear
	bool ____bigear_3;
	// System.Single GarbageiOS.M_jowpea99::_lestiBayjar
	float ____lestiBayjar_4;
	// System.Int32 GarbageiOS.M_jowpea99::_cooteeno
	int32_t ____cooteeno_5;
	// System.Single GarbageiOS.M_jowpea99::_tairpa
	float ____tairpa_6;
	// System.Boolean GarbageiOS.M_jowpea99::_gearvetel
	bool ____gearvetel_7;
	// System.Int32 GarbageiOS.M_jowpea99::_jehassir
	int32_t ____jehassir_8;
	// System.UInt32 GarbageiOS.M_jowpea99::_helte
	uint32_t ____helte_9;
	// System.Int32 GarbageiOS.M_jowpea99::_yucaTowdrem
	int32_t ____yucaTowdrem_10;
	// System.Single GarbageiOS.M_jowpea99::_pewhirturPeadiwhas
	float ____pewhirturPeadiwhas_11;
	// System.UInt32 GarbageiOS.M_jowpea99::_teljeremai
	uint32_t ____teljeremai_12;
	// System.Boolean GarbageiOS.M_jowpea99::_zorqoujas
	bool ____zorqoujas_13;
	// System.UInt32 GarbageiOS.M_jowpea99::_melair
	uint32_t ____melair_14;
	// System.UInt32 GarbageiOS.M_jowpea99::_fowbalDiner
	uint32_t ____fowbalDiner_15;
	// System.Int32 GarbageiOS.M_jowpea99::_sawouciNairmo
	int32_t ____sawouciNairmo_16;
	// System.Single GarbageiOS.M_jowpea99::_gemchinas
	float ____gemchinas_17;
	// System.Boolean GarbageiOS.M_jowpea99::_nairnerpeMoowhesu
	bool ____nairnerpeMoowhesu_18;
	// System.String GarbageiOS.M_jowpea99::_reza
	String_t* ____reza_19;
	// System.Boolean GarbageiOS.M_jowpea99::_disdrarra
	bool ____disdrarra_20;
	// System.Single GarbageiOS.M_jowpea99::_jiselJelkem
	float ____jiselJelkem_21;
	// System.Single GarbageiOS.M_jowpea99::_keresidarNedre
	float ____keresidarNedre_22;
	// System.String GarbageiOS.M_jowpea99::_ceehai
	String_t* ____ceehai_23;
	// System.Int32 GarbageiOS.M_jowpea99::_suco
	int32_t ____suco_24;
	// System.String GarbageiOS.M_jowpea99::_cissirjaw
	String_t* ____cissirjaw_25;
	// System.String GarbageiOS.M_jowpea99::_sirlicoo
	String_t* ____sirlicoo_26;
	// System.Single GarbageiOS.M_jowpea99::_lursocur
	float ____lursocur_27;
	// System.Boolean GarbageiOS.M_jowpea99::_houwallsir
	bool ____houwallsir_28;
	// System.String GarbageiOS.M_jowpea99::_coorerou
	String_t* ____coorerou_29;
	// System.Int32 GarbageiOS.M_jowpea99::_drowvasPartar
	int32_t ____drowvasPartar_30;
	// System.Int32 GarbageiOS.M_jowpea99::_tisjormem
	int32_t ____tisjormem_31;
	// System.UInt32 GarbageiOS.M_jowpea99::_rairjaqisNesaw
	uint32_t ____rairjaqisNesaw_32;
	// System.Single GarbageiOS.M_jowpea99::_taycelQotis
	float ____taycelQotis_33;
	// System.Int32 GarbageiOS.M_jowpea99::_callral
	int32_t ____callral_34;

public:
	inline static int32_t get_offset_of__torsta_0() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____torsta_0)); }
	inline float get__torsta_0() const { return ____torsta_0; }
	inline float* get_address_of__torsta_0() { return &____torsta_0; }
	inline void set__torsta_0(float value)
	{
		____torsta_0 = value;
	}

	inline static int32_t get_offset_of__celreQearfis_1() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____celreQearfis_1)); }
	inline bool get__celreQearfis_1() const { return ____celreQearfis_1; }
	inline bool* get_address_of__celreQearfis_1() { return &____celreQearfis_1; }
	inline void set__celreQearfis_1(bool value)
	{
		____celreQearfis_1 = value;
	}

	inline static int32_t get_offset_of__lapeldreKerepe_2() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____lapeldreKerepe_2)); }
	inline String_t* get__lapeldreKerepe_2() const { return ____lapeldreKerepe_2; }
	inline String_t** get_address_of__lapeldreKerepe_2() { return &____lapeldreKerepe_2; }
	inline void set__lapeldreKerepe_2(String_t* value)
	{
		____lapeldreKerepe_2 = value;
		Il2CppCodeGenWriteBarrier(&____lapeldreKerepe_2, value);
	}

	inline static int32_t get_offset_of__bigear_3() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____bigear_3)); }
	inline bool get__bigear_3() const { return ____bigear_3; }
	inline bool* get_address_of__bigear_3() { return &____bigear_3; }
	inline void set__bigear_3(bool value)
	{
		____bigear_3 = value;
	}

	inline static int32_t get_offset_of__lestiBayjar_4() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____lestiBayjar_4)); }
	inline float get__lestiBayjar_4() const { return ____lestiBayjar_4; }
	inline float* get_address_of__lestiBayjar_4() { return &____lestiBayjar_4; }
	inline void set__lestiBayjar_4(float value)
	{
		____lestiBayjar_4 = value;
	}

	inline static int32_t get_offset_of__cooteeno_5() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____cooteeno_5)); }
	inline int32_t get__cooteeno_5() const { return ____cooteeno_5; }
	inline int32_t* get_address_of__cooteeno_5() { return &____cooteeno_5; }
	inline void set__cooteeno_5(int32_t value)
	{
		____cooteeno_5 = value;
	}

	inline static int32_t get_offset_of__tairpa_6() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____tairpa_6)); }
	inline float get__tairpa_6() const { return ____tairpa_6; }
	inline float* get_address_of__tairpa_6() { return &____tairpa_6; }
	inline void set__tairpa_6(float value)
	{
		____tairpa_6 = value;
	}

	inline static int32_t get_offset_of__gearvetel_7() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____gearvetel_7)); }
	inline bool get__gearvetel_7() const { return ____gearvetel_7; }
	inline bool* get_address_of__gearvetel_7() { return &____gearvetel_7; }
	inline void set__gearvetel_7(bool value)
	{
		____gearvetel_7 = value;
	}

	inline static int32_t get_offset_of__jehassir_8() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____jehassir_8)); }
	inline int32_t get__jehassir_8() const { return ____jehassir_8; }
	inline int32_t* get_address_of__jehassir_8() { return &____jehassir_8; }
	inline void set__jehassir_8(int32_t value)
	{
		____jehassir_8 = value;
	}

	inline static int32_t get_offset_of__helte_9() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____helte_9)); }
	inline uint32_t get__helte_9() const { return ____helte_9; }
	inline uint32_t* get_address_of__helte_9() { return &____helte_9; }
	inline void set__helte_9(uint32_t value)
	{
		____helte_9 = value;
	}

	inline static int32_t get_offset_of__yucaTowdrem_10() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____yucaTowdrem_10)); }
	inline int32_t get__yucaTowdrem_10() const { return ____yucaTowdrem_10; }
	inline int32_t* get_address_of__yucaTowdrem_10() { return &____yucaTowdrem_10; }
	inline void set__yucaTowdrem_10(int32_t value)
	{
		____yucaTowdrem_10 = value;
	}

	inline static int32_t get_offset_of__pewhirturPeadiwhas_11() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____pewhirturPeadiwhas_11)); }
	inline float get__pewhirturPeadiwhas_11() const { return ____pewhirturPeadiwhas_11; }
	inline float* get_address_of__pewhirturPeadiwhas_11() { return &____pewhirturPeadiwhas_11; }
	inline void set__pewhirturPeadiwhas_11(float value)
	{
		____pewhirturPeadiwhas_11 = value;
	}

	inline static int32_t get_offset_of__teljeremai_12() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____teljeremai_12)); }
	inline uint32_t get__teljeremai_12() const { return ____teljeremai_12; }
	inline uint32_t* get_address_of__teljeremai_12() { return &____teljeremai_12; }
	inline void set__teljeremai_12(uint32_t value)
	{
		____teljeremai_12 = value;
	}

	inline static int32_t get_offset_of__zorqoujas_13() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____zorqoujas_13)); }
	inline bool get__zorqoujas_13() const { return ____zorqoujas_13; }
	inline bool* get_address_of__zorqoujas_13() { return &____zorqoujas_13; }
	inline void set__zorqoujas_13(bool value)
	{
		____zorqoujas_13 = value;
	}

	inline static int32_t get_offset_of__melair_14() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____melair_14)); }
	inline uint32_t get__melair_14() const { return ____melair_14; }
	inline uint32_t* get_address_of__melair_14() { return &____melair_14; }
	inline void set__melair_14(uint32_t value)
	{
		____melair_14 = value;
	}

	inline static int32_t get_offset_of__fowbalDiner_15() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____fowbalDiner_15)); }
	inline uint32_t get__fowbalDiner_15() const { return ____fowbalDiner_15; }
	inline uint32_t* get_address_of__fowbalDiner_15() { return &____fowbalDiner_15; }
	inline void set__fowbalDiner_15(uint32_t value)
	{
		____fowbalDiner_15 = value;
	}

	inline static int32_t get_offset_of__sawouciNairmo_16() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____sawouciNairmo_16)); }
	inline int32_t get__sawouciNairmo_16() const { return ____sawouciNairmo_16; }
	inline int32_t* get_address_of__sawouciNairmo_16() { return &____sawouciNairmo_16; }
	inline void set__sawouciNairmo_16(int32_t value)
	{
		____sawouciNairmo_16 = value;
	}

	inline static int32_t get_offset_of__gemchinas_17() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____gemchinas_17)); }
	inline float get__gemchinas_17() const { return ____gemchinas_17; }
	inline float* get_address_of__gemchinas_17() { return &____gemchinas_17; }
	inline void set__gemchinas_17(float value)
	{
		____gemchinas_17 = value;
	}

	inline static int32_t get_offset_of__nairnerpeMoowhesu_18() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____nairnerpeMoowhesu_18)); }
	inline bool get__nairnerpeMoowhesu_18() const { return ____nairnerpeMoowhesu_18; }
	inline bool* get_address_of__nairnerpeMoowhesu_18() { return &____nairnerpeMoowhesu_18; }
	inline void set__nairnerpeMoowhesu_18(bool value)
	{
		____nairnerpeMoowhesu_18 = value;
	}

	inline static int32_t get_offset_of__reza_19() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____reza_19)); }
	inline String_t* get__reza_19() const { return ____reza_19; }
	inline String_t** get_address_of__reza_19() { return &____reza_19; }
	inline void set__reza_19(String_t* value)
	{
		____reza_19 = value;
		Il2CppCodeGenWriteBarrier(&____reza_19, value);
	}

	inline static int32_t get_offset_of__disdrarra_20() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____disdrarra_20)); }
	inline bool get__disdrarra_20() const { return ____disdrarra_20; }
	inline bool* get_address_of__disdrarra_20() { return &____disdrarra_20; }
	inline void set__disdrarra_20(bool value)
	{
		____disdrarra_20 = value;
	}

	inline static int32_t get_offset_of__jiselJelkem_21() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____jiselJelkem_21)); }
	inline float get__jiselJelkem_21() const { return ____jiselJelkem_21; }
	inline float* get_address_of__jiselJelkem_21() { return &____jiselJelkem_21; }
	inline void set__jiselJelkem_21(float value)
	{
		____jiselJelkem_21 = value;
	}

	inline static int32_t get_offset_of__keresidarNedre_22() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____keresidarNedre_22)); }
	inline float get__keresidarNedre_22() const { return ____keresidarNedre_22; }
	inline float* get_address_of__keresidarNedre_22() { return &____keresidarNedre_22; }
	inline void set__keresidarNedre_22(float value)
	{
		____keresidarNedre_22 = value;
	}

	inline static int32_t get_offset_of__ceehai_23() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____ceehai_23)); }
	inline String_t* get__ceehai_23() const { return ____ceehai_23; }
	inline String_t** get_address_of__ceehai_23() { return &____ceehai_23; }
	inline void set__ceehai_23(String_t* value)
	{
		____ceehai_23 = value;
		Il2CppCodeGenWriteBarrier(&____ceehai_23, value);
	}

	inline static int32_t get_offset_of__suco_24() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____suco_24)); }
	inline int32_t get__suco_24() const { return ____suco_24; }
	inline int32_t* get_address_of__suco_24() { return &____suco_24; }
	inline void set__suco_24(int32_t value)
	{
		____suco_24 = value;
	}

	inline static int32_t get_offset_of__cissirjaw_25() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____cissirjaw_25)); }
	inline String_t* get__cissirjaw_25() const { return ____cissirjaw_25; }
	inline String_t** get_address_of__cissirjaw_25() { return &____cissirjaw_25; }
	inline void set__cissirjaw_25(String_t* value)
	{
		____cissirjaw_25 = value;
		Il2CppCodeGenWriteBarrier(&____cissirjaw_25, value);
	}

	inline static int32_t get_offset_of__sirlicoo_26() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____sirlicoo_26)); }
	inline String_t* get__sirlicoo_26() const { return ____sirlicoo_26; }
	inline String_t** get_address_of__sirlicoo_26() { return &____sirlicoo_26; }
	inline void set__sirlicoo_26(String_t* value)
	{
		____sirlicoo_26 = value;
		Il2CppCodeGenWriteBarrier(&____sirlicoo_26, value);
	}

	inline static int32_t get_offset_of__lursocur_27() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____lursocur_27)); }
	inline float get__lursocur_27() const { return ____lursocur_27; }
	inline float* get_address_of__lursocur_27() { return &____lursocur_27; }
	inline void set__lursocur_27(float value)
	{
		____lursocur_27 = value;
	}

	inline static int32_t get_offset_of__houwallsir_28() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____houwallsir_28)); }
	inline bool get__houwallsir_28() const { return ____houwallsir_28; }
	inline bool* get_address_of__houwallsir_28() { return &____houwallsir_28; }
	inline void set__houwallsir_28(bool value)
	{
		____houwallsir_28 = value;
	}

	inline static int32_t get_offset_of__coorerou_29() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____coorerou_29)); }
	inline String_t* get__coorerou_29() const { return ____coorerou_29; }
	inline String_t** get_address_of__coorerou_29() { return &____coorerou_29; }
	inline void set__coorerou_29(String_t* value)
	{
		____coorerou_29 = value;
		Il2CppCodeGenWriteBarrier(&____coorerou_29, value);
	}

	inline static int32_t get_offset_of__drowvasPartar_30() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____drowvasPartar_30)); }
	inline int32_t get__drowvasPartar_30() const { return ____drowvasPartar_30; }
	inline int32_t* get_address_of__drowvasPartar_30() { return &____drowvasPartar_30; }
	inline void set__drowvasPartar_30(int32_t value)
	{
		____drowvasPartar_30 = value;
	}

	inline static int32_t get_offset_of__tisjormem_31() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____tisjormem_31)); }
	inline int32_t get__tisjormem_31() const { return ____tisjormem_31; }
	inline int32_t* get_address_of__tisjormem_31() { return &____tisjormem_31; }
	inline void set__tisjormem_31(int32_t value)
	{
		____tisjormem_31 = value;
	}

	inline static int32_t get_offset_of__rairjaqisNesaw_32() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____rairjaqisNesaw_32)); }
	inline uint32_t get__rairjaqisNesaw_32() const { return ____rairjaqisNesaw_32; }
	inline uint32_t* get_address_of__rairjaqisNesaw_32() { return &____rairjaqisNesaw_32; }
	inline void set__rairjaqisNesaw_32(uint32_t value)
	{
		____rairjaqisNesaw_32 = value;
	}

	inline static int32_t get_offset_of__taycelQotis_33() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____taycelQotis_33)); }
	inline float get__taycelQotis_33() const { return ____taycelQotis_33; }
	inline float* get_address_of__taycelQotis_33() { return &____taycelQotis_33; }
	inline void set__taycelQotis_33(float value)
	{
		____taycelQotis_33 = value;
	}

	inline static int32_t get_offset_of__callral_34() { return static_cast<int32_t>(offsetof(M_jowpea99_t1613162986, ____callral_34)); }
	inline int32_t get__callral_34() const { return ____callral_34; }
	inline int32_t* get_address_of__callral_34() { return &____callral_34; }
	inline void set__callral_34(int32_t value)
	{
		____callral_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
