﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonValidatingReader
struct JsonValidatingReader_t188359606;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// Newtonsoft.Json.Schema.ValidationEventHandler
struct ValidationEventHandler_t2238986739;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// Newtonsoft.Json.JsonValidatingReader/SchemaScope
struct SchemaScope_t3540946455;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Schema.JsonSchemaModel>
struct IEnumerable_1_t2041563799;
// System.String
struct String_t;
// Newtonsoft.Json.Schema.JsonSchemaModel
struct JsonSchemaModel_t3035618138;
// Newtonsoft.Json.Schema.JsonSchemaException
struct JsonSchemaException_t4050942208;
// Newtonsoft.Json.Schema.JsonSchema
struct JsonSchema_t460567603;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// Newtonsoft.Json.IJsonLineInfo
struct IJsonLineInfo_t1008519681;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchemaModel>
struct IList_1_t1435298045;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaModel>
struct IDictionary_2_t3433909853;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Newtonsoft.Json.Schema.ValidationEventArgs
struct ValidationEventArgs_t623166840;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// Newtonsoft.Json.JsonConverter[]
struct JsonConverterU5BU5D_t638349667;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken>
struct IList_1_t1811925858;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t1297217088;
// System.Collections.Generic.IList`1<System.String>
struct IList_1_t2701878760;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader816925123.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_Validatio2238986739.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonToken4173078175.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonValidatingRe3540946455.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem3035618138.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem4050942208.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchema460567603.h"
#include "mscorlib_System_Nullable_1_gen2199542344.h"
#include "mscorlib_System_Nullable_1_gen2038477154.h"
#include "mscorlib_System_Nullable_1_gen3968840829.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem2115415821.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21195997794.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonValidatingRea188359606.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_Validation623166840.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"
#include "mscorlib_System_Nullable_1_gen1237965023.h"
#include "mscorlib_System_Nullable_1_gen3952353088.h"

// System.Void Newtonsoft.Json.JsonValidatingReader::.ctor(Newtonsoft.Json.JsonReader)
extern "C"  void JsonValidatingReader__ctor_m3133094664 (JsonValidatingReader_t188359606 * __this, JsonReader_t816925123 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::add_ValidationEventHandler(Newtonsoft.Json.Schema.ValidationEventHandler)
extern "C"  void JsonValidatingReader_add_ValidationEventHandler_m3134176654 (JsonValidatingReader_t188359606 * __this, ValidationEventHandler_t2238986739 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::remove_ValidationEventHandler(Newtonsoft.Json.Schema.ValidationEventHandler)
extern "C"  void JsonValidatingReader_remove_ValidationEventHandler_m3770901831 (JsonValidatingReader_t188359606 * __this, ValidationEventHandler_t2238986739 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonValidatingReader::Newtonsoft.Json.IJsonLineInfo.HasLineInfo()
extern "C"  bool JsonValidatingReader_Newtonsoft_Json_IJsonLineInfo_HasLineInfo_m1480527678 (JsonValidatingReader_t188359606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.JsonValidatingReader::Newtonsoft.Json.IJsonLineInfo.get_LineNumber()
extern "C"  int32_t JsonValidatingReader_Newtonsoft_Json_IJsonLineInfo_get_LineNumber_m7608672 (JsonValidatingReader_t188359606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.JsonValidatingReader::Newtonsoft.Json.IJsonLineInfo.get_LinePosition()
extern "C"  int32_t JsonValidatingReader_Newtonsoft_Json_IJsonLineInfo_get_LinePosition_m496591296 (JsonValidatingReader_t188359606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.JsonValidatingReader::get_Value()
extern "C"  Il2CppObject * JsonValidatingReader_get_Value_m2093970105 (JsonValidatingReader_t188359606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.JsonValidatingReader::get_Depth()
extern "C"  int32_t JsonValidatingReader_get_Depth_m645643090 (JsonValidatingReader_t188359606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char Newtonsoft.Json.JsonValidatingReader::get_QuoteChar()
extern "C"  Il2CppChar JsonValidatingReader_get_QuoteChar_m531972945 (JsonValidatingReader_t188359606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::set_QuoteChar(System.Char)
extern "C"  void JsonValidatingReader_set_QuoteChar_m332762594 (JsonValidatingReader_t188359606 * __this, Il2CppChar ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonToken Newtonsoft.Json.JsonValidatingReader::get_TokenType()
extern "C"  int32_t JsonValidatingReader_get_TokenType_m1199861817 (JsonValidatingReader_t188359606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.JsonValidatingReader::get_ValueType()
extern "C"  Type_t * JsonValidatingReader_get_ValueType_m2398488142 (JsonValidatingReader_t188359606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::Push(Newtonsoft.Json.JsonValidatingReader/SchemaScope)
extern "C"  void JsonValidatingReader_Push_m3536351423 (JsonValidatingReader_t188359606 * __this, SchemaScope_t3540946455 * ___scope0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonValidatingReader/SchemaScope Newtonsoft.Json.JsonValidatingReader::Pop()
extern "C"  SchemaScope_t3540946455 * JsonValidatingReader_Pop_m649612049 (JsonValidatingReader_t188359606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Schema.JsonSchemaModel> Newtonsoft.Json.JsonValidatingReader::get_CurrentSchemas()
extern "C"  Il2CppObject* JsonValidatingReader_get_CurrentSchemas_m1662983214 (JsonValidatingReader_t188359606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Schema.JsonSchemaModel> Newtonsoft.Json.JsonValidatingReader::get_CurrentMemberSchemas()
extern "C"  Il2CppObject* JsonValidatingReader_get_CurrentMemberSchemas_m3943450228 (JsonValidatingReader_t188359606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::RaiseError(System.String,Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  void JsonValidatingReader_RaiseError_m3617494196 (JsonValidatingReader_t188359606 * __this, String_t* ___message0, JsonSchemaModel_t3035618138 * ___schema1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::OnValidationEvent(Newtonsoft.Json.Schema.JsonSchemaException)
extern "C"  void JsonValidatingReader_OnValidationEvent_m1415001530 (JsonValidatingReader_t188359606 * __this, JsonSchemaException_t4050942208 * ___exception0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.JsonValidatingReader::get_Schema()
extern "C"  JsonSchema_t460567603 * JsonValidatingReader_get_Schema_m4038150136 (JsonValidatingReader_t188359606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::set_Schema(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  void JsonValidatingReader_set_Schema_m2153586327 (JsonValidatingReader_t188359606 * __this, JsonSchema_t460567603 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonReader Newtonsoft.Json.JsonValidatingReader::get_Reader()
extern "C"  JsonReader_t816925123 * JsonValidatingReader_get_Reader_m847976709 (JsonValidatingReader_t188359606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::ValidateInEnumAndNotDisallowed(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  void JsonValidatingReader_ValidateInEnumAndNotDisallowed_m3963151750 (JsonValidatingReader_t188359606 * __this, JsonSchemaModel_t3035618138 * ___schema0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType> Newtonsoft.Json.JsonValidatingReader::GetCurrentNodeSchemaType()
extern "C"  Nullable_1_t2199542344  JsonValidatingReader_GetCurrentNodeSchemaType_m2773036555 (JsonValidatingReader_t188359606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Newtonsoft.Json.JsonValidatingReader::ReadAsBytes()
extern "C"  ByteU5BU5D_t4260760469* JsonValidatingReader_ReadAsBytes_m4025087295 (JsonValidatingReader_t188359606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Decimal> Newtonsoft.Json.JsonValidatingReader::ReadAsDecimal()
extern "C"  Nullable_1_t2038477154  JsonValidatingReader_ReadAsDecimal_m3529955997 (JsonValidatingReader_t188359606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTimeOffset> Newtonsoft.Json.JsonValidatingReader::ReadAsDateTimeOffset()
extern "C"  Nullable_1_t3968840829  JsonValidatingReader_ReadAsDateTimeOffset_m2591538419 (JsonValidatingReader_t188359606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonValidatingReader::Read()
extern "C"  bool JsonValidatingReader_Read_m3627587110 (JsonValidatingReader_t188359606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::ValidateCurrentToken()
extern "C"  void JsonValidatingReader_ValidateCurrentToken_m3518454290 (JsonValidatingReader_t188359606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::ValidateEndObject(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  void JsonValidatingReader_ValidateEndObject_m431601122 (JsonValidatingReader_t188359606 * __this, JsonSchemaModel_t3035618138 * ___schema0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::ValidateEndArray(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  void JsonValidatingReader_ValidateEndArray_m464260008 (JsonValidatingReader_t188359606 * __this, JsonSchemaModel_t3035618138 * ___schema0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::ValidateNull(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  void JsonValidatingReader_ValidateNull_m2057165425 (JsonValidatingReader_t188359606 * __this, JsonSchemaModel_t3035618138 * ___schema0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::ValidateBoolean(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  void JsonValidatingReader_ValidateBoolean_m2898729936 (JsonValidatingReader_t188359606 * __this, JsonSchemaModel_t3035618138 * ___schema0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::ValidateString(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  void JsonValidatingReader_ValidateString_m795185851 (JsonValidatingReader_t188359606 * __this, JsonSchemaModel_t3035618138 * ___schema0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::ValidateInteger(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  void JsonValidatingReader_ValidateInteger_m4109099750 (JsonValidatingReader_t188359606 * __this, JsonSchemaModel_t3035618138 * ___schema0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::ProcessValue()
extern "C"  void JsonValidatingReader_ProcessValue_m4095948318 (JsonValidatingReader_t188359606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::ValidateFloat(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  void JsonValidatingReader_ValidateFloat_m4071196100 (JsonValidatingReader_t188359606 * __this, JsonSchemaModel_t3035618138 * ___schema0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonValidatingReader::IsZero(System.Double)
extern "C"  bool JsonValidatingReader_IsZero_m3978272192 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::ValidatePropertyName(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  void JsonValidatingReader_ValidatePropertyName_m532046058 (JsonValidatingReader_t188359606 * __this, JsonSchemaModel_t3035618138 * ___schema0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonValidatingReader::IsPropertyDefinied(Newtonsoft.Json.Schema.JsonSchemaModel,System.String)
extern "C"  bool JsonValidatingReader_IsPropertyDefinied_m2691295617 (JsonValidatingReader_t188359606 * __this, JsonSchemaModel_t3035618138 * ___schema0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonValidatingReader::ValidateArray(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  bool JsonValidatingReader_ValidateArray_m3825956237 (JsonValidatingReader_t188359606 * __this, JsonSchemaModel_t3035618138 * ___schema0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonValidatingReader::ValidateObject(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  bool JsonValidatingReader_ValidateObject_m1564969117 (JsonValidatingReader_t188359606 * __this, JsonSchemaModel_t3035618138 * ___schema0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonValidatingReader::TestType(Newtonsoft.Json.Schema.JsonSchemaModel,Newtonsoft.Json.Schema.JsonSchemaType)
extern "C"  bool JsonValidatingReader_TestType_m880634117 (JsonValidatingReader_t188359606 * __this, JsonSchemaModel_t3035618138 * ___currentSchema0, int32_t ___currentType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonValidatingReader::<ValidateEndObject>m__36F(System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>)
extern "C"  bool JsonValidatingReader_U3CValidateEndObjectU3Em__36F_m1911178986 (Il2CppObject * __this /* static, unused */, KeyValuePair_2_t1195997794  ___kv0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonValidatingReader::<ValidateEndObject>m__370(System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>)
extern "C"  String_t* JsonValidatingReader_U3CValidateEndObjectU3Em__370_m1170343280 (Il2CppObject * __this /* static, unused */, KeyValuePair_2_t1195997794  ___kv0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonValidatingReader::ilo_HasLineInfo1(Newtonsoft.Json.IJsonLineInfo)
extern "C"  bool JsonValidatingReader_ilo_HasLineInfo1_m3738317724 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.JsonValidatingReader::ilo_get_Value2(Newtonsoft.Json.JsonReader)
extern "C"  Il2CppObject * JsonValidatingReader_ilo_get_Value2_m209261646 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.JsonValidatingReader::ilo_get_ValueType3(Newtonsoft.Json.JsonReader)
extern "C"  Type_t * JsonValidatingReader_ilo_get_ValueType3_m2934432794 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchemaModel> Newtonsoft.Json.JsonValidatingReader::ilo_get_Schemas4(Newtonsoft.Json.JsonValidatingReader/SchemaScope)
extern "C"  Il2CppObject* JsonValidatingReader_ilo_get_Schemas4_m2151158557 (Il2CppObject * __this /* static, unused */, SchemaScope_t3540946455 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonValidatingReader::ilo_get_CurrentPropertyName5(Newtonsoft.Json.JsonValidatingReader/SchemaScope)
extern "C"  String_t* JsonValidatingReader_ilo_get_CurrentPropertyName5_m3302136704 (Il2CppObject * __this /* static, unused */, SchemaScope_t3540946455 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaModel> Newtonsoft.Json.JsonValidatingReader::ilo_get_PatternProperties6(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  Il2CppObject* JsonValidatingReader_ilo_get_PatternProperties6_m1307433920 (Il2CppObject * __this /* static, unused */, JsonSchemaModel_t3035618138 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonValidatingReader::ilo_get_AllowAdditionalProperties7(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  bool JsonValidatingReader_ilo_get_AllowAdditionalProperties7_m3262871704 (Il2CppObject * __this /* static, unused */, JsonSchemaModel_t3035618138 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Schema.JsonSchemaModel> Newtonsoft.Json.JsonValidatingReader::ilo_get_CurrentSchemas8(Newtonsoft.Json.JsonValidatingReader)
extern "C"  Il2CppObject* JsonValidatingReader_ilo_get_CurrentSchemas8_m4031779878 (Il2CppObject * __this /* static, unused */, JsonValidatingReader_t188359606 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchemaModel> Newtonsoft.Json.JsonValidatingReader::ilo_get_Items9(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  Il2CppObject* JsonValidatingReader_ilo_get_Items9_m280877539 (Il2CppObject * __this /* static, unused */, JsonSchemaModel_t3035618138 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.JsonValidatingReader::ilo_get_ArrayItemCount10(Newtonsoft.Json.JsonValidatingReader/SchemaScope)
extern "C"  int32_t JsonValidatingReader_ilo_get_ArrayItemCount10_m33208433 (Il2CppObject * __this /* static, unused */, SchemaScope_t3540946455 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaModel Newtonsoft.Json.JsonValidatingReader::ilo_get_AdditionalProperties11(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  JsonSchemaModel_t3035618138 * JsonValidatingReader_ilo_get_AdditionalProperties11_m1617550047 (Il2CppObject * __this /* static, unused */, JsonSchemaModel_t3035618138 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonValidatingReader::ilo_FormatWith12(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* JsonValidatingReader_ilo_FormatWith12_m3315466702 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::ilo_Invoke13(Newtonsoft.Json.Schema.ValidationEventHandler,System.Object,Newtonsoft.Json.Schema.ValidationEventArgs)
extern "C"  void JsonValidatingReader_ilo_Invoke13_m2315434346 (Il2CppObject * __this /* static, unused */, ValidationEventHandler_t2238986739 * ____this0, Il2CppObject * ___sender1, ValidationEventArgs_t623166840 * ___e2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::ilo_WriteTo14(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter[])
extern "C"  void JsonValidatingReader_ilo_WriteTo14_m2368914211 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, JsonWriter_t972330355 * ___writer1, JsonConverterU5BU5D_t638349667* ___converters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.JsonValidatingReader::ilo_get_Enum15(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  Il2CppObject* JsonValidatingReader_ilo_get_Enum15_m3161269503 (Il2CppObject * __this /* static, unused */, JsonSchemaModel_t3035618138 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::ilo_RaiseError16(Newtonsoft.Json.JsonValidatingReader,System.String,Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  void JsonValidatingReader_ilo_RaiseError16_m2905277125 (Il2CppObject * __this /* static, unused */, JsonValidatingReader_t188359606 * ____this0, String_t* ___message1, JsonSchemaModel_t3035618138 * ___schema2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType> Newtonsoft.Json.JsonValidatingReader::ilo_GetCurrentNodeSchemaType17(Newtonsoft.Json.JsonValidatingReader)
extern "C"  Nullable_1_t2199542344  JsonValidatingReader_ilo_GetCurrentNodeSchemaType17_m307916465 (Il2CppObject * __this /* static, unused */, JsonValidatingReader_t188359606 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaType Newtonsoft.Json.JsonValidatingReader::ilo_get_Disallow18(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  int32_t JsonValidatingReader_ilo_get_Disallow18_m1017965546 (Il2CppObject * __this /* static, unused */, JsonSchemaModel_t3035618138 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonToken Newtonsoft.Json.JsonValidatingReader::ilo_get_TokenType19(Newtonsoft.Json.JsonReader)
extern "C"  int32_t JsonValidatingReader_ilo_get_TokenType19_m1503381038 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTimeOffset> Newtonsoft.Json.JsonValidatingReader::ilo_ReadAsDateTimeOffset20(Newtonsoft.Json.JsonReader)
extern "C"  Nullable_1_t3968840829  JsonValidatingReader_ilo_ReadAsDateTimeOffset20_m2870767492 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::ilo_ValidateCurrentToken21(Newtonsoft.Json.JsonValidatingReader)
extern "C"  void JsonValidatingReader_ilo_ValidateCurrentToken21_m3679372241 (Il2CppObject * __this /* static, unused */, JsonValidatingReader_t188359606 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::ilo_ProcessValue22(Newtonsoft.Json.JsonValidatingReader)
extern "C"  void JsonValidatingReader_ilo_ProcessValue22_m3488944414 (Il2CppObject * __this /* static, unused */, JsonValidatingReader_t188359606 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::ilo_Push23(Newtonsoft.Json.JsonValidatingReader,Newtonsoft.Json.JsonValidatingReader/SchemaScope)
extern "C"  void JsonValidatingReader_ilo_Push23_m1586133752 (Il2CppObject * __this /* static, unused */, JsonValidatingReader_t188359606 * ____this0, SchemaScope_t3540946455 * ___scope1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Schema.JsonSchemaModel> Newtonsoft.Json.JsonValidatingReader::ilo_get_CurrentMemberSchemas24(Newtonsoft.Json.JsonValidatingReader)
extern "C"  Il2CppObject* JsonValidatingReader_ilo_get_CurrentMemberSchemas24_m1089323766 (Il2CppObject * __this /* static, unused */, JsonValidatingReader_t188359606 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonValidatingReader/SchemaScope Newtonsoft.Json.JsonValidatingReader::ilo_Pop25(Newtonsoft.Json.JsonValidatingReader)
extern "C"  SchemaScope_t3540946455 * JsonValidatingReader_ilo_Pop25_m1256280270 (Il2CppObject * __this /* static, unused */, JsonValidatingReader_t188359606 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::ilo_ValidateEndArray26(Newtonsoft.Json.JsonValidatingReader,Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  void JsonValidatingReader_ilo_ValidateEndArray26_m3990417802 (Il2CppObject * __this /* static, unused */, JsonValidatingReader_t188359606 * ____this0, JsonSchemaModel_t3035618138 * ___schema1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> Newtonsoft.Json.JsonValidatingReader::ilo_get_RequiredProperties27(Newtonsoft.Json.JsonValidatingReader/SchemaScope)
extern "C"  Dictionary_2_t1297217088 * JsonValidatingReader_ilo_get_RequiredProperties27_m1396510519 (Il2CppObject * __this /* static, unused */, SchemaScope_t3540946455 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonValidatingReader::ilo_get_MaximumItems28(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  Nullable_1_t1237965023  JsonValidatingReader_ilo_get_MaximumItems28_m4136367269 (Il2CppObject * __this /* static, unused */, JsonSchemaModel_t3035618138 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonValidatingReader::ilo_get_MinimumItems29(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  Nullable_1_t1237965023  JsonValidatingReader_ilo_get_MinimumItems29_m579576824 (Il2CppObject * __this /* static, unused */, JsonSchemaModel_t3035618138 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::ilo_ValidateInEnumAndNotDisallowed30(Newtonsoft.Json.JsonValidatingReader,Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  void JsonValidatingReader_ilo_ValidateInEnumAndNotDisallowed30_m2028548019 (Il2CppObject * __this /* static, unused */, JsonValidatingReader_t188359606 * ____this0, JsonSchemaModel_t3035618138 * ___schema1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonValidatingReader::ilo_get_MaximumLength31(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  Nullable_1_t1237965023  JsonValidatingReader_ilo_get_MaximumLength31_m568657175 (Il2CppObject * __this /* static, unused */, JsonSchemaModel_t3035618138 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonValidatingReader::ilo_get_MinimumLength32(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  Nullable_1_t1237965023  JsonValidatingReader_ilo_get_MinimumLength32_m4212536838 (Il2CppObject * __this /* static, unused */, JsonSchemaModel_t3035618138 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<System.String> Newtonsoft.Json.JsonValidatingReader::ilo_get_Patterns33(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  Il2CppObject* JsonValidatingReader_ilo_get_Patterns33_m4002018309 (Il2CppObject * __this /* static, unused */, JsonSchemaModel_t3035618138 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Double> Newtonsoft.Json.JsonValidatingReader::ilo_get_Maximum34(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  Nullable_1_t3952353088  JsonValidatingReader_ilo_get_Maximum34_m1974539359 (Il2CppObject * __this /* static, unused */, JsonSchemaModel_t3035618138 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonValidatingReader::ilo_get_ExclusiveMinimum35(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  bool JsonValidatingReader_ilo_get_ExclusiveMinimum35_m1818582166 (Il2CppObject * __this /* static, unused */, JsonSchemaModel_t3035618138 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Double> Newtonsoft.Json.JsonValidatingReader::ilo_get_DivisibleBy36(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  Nullable_1_t3952353088  JsonValidatingReader_ilo_get_DivisibleBy36_m799042117 (Il2CppObject * __this /* static, unused */, JsonSchemaModel_t3035618138 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader::ilo_set_ArrayItemCount37(Newtonsoft.Json.JsonValidatingReader/SchemaScope,System.Int32)
extern "C"  void JsonValidatingReader_ilo_set_ArrayItemCount37_m45218683 (Il2CppObject * __this /* static, unused */, SchemaScope_t3540946455 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonValidatingReader::ilo_ToString38(System.Double)
extern "C"  String_t* JsonValidatingReader_ilo_ToString38_m1366954847 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Double> Newtonsoft.Json.JsonValidatingReader::ilo_get_Minimum39(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  Nullable_1_t3952353088  JsonValidatingReader_ilo_get_Minimum39_m1818328018 (Il2CppObject * __this /* static, unused */, JsonSchemaModel_t3035618138 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonValidatingReader::ilo_IsZero40(System.Double)
extern "C"  bool JsonValidatingReader_ilo_IsZero40_m2110376945 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonValidatingReader::ilo_TestType41(Newtonsoft.Json.JsonValidatingReader,Newtonsoft.Json.Schema.JsonSchemaModel,Newtonsoft.Json.Schema.JsonSchemaType)
extern "C"  bool JsonValidatingReader_ilo_TestType41_m2503310262 (Il2CppObject * __this /* static, unused */, JsonValidatingReader_t188359606 * ____this0, JsonSchemaModel_t3035618138 * ___currentSchema1, int32_t ___currentType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonValidatingReader::ilo_HasFlag42(System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>,Newtonsoft.Json.Schema.JsonSchemaType)
extern "C"  bool JsonValidatingReader_ilo_HasFlag42_m605764998 (Il2CppObject * __this /* static, unused */, Nullable_1_t2199542344  ___value0, int32_t ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaType Newtonsoft.Json.JsonValidatingReader::ilo_get_Type43(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  int32_t JsonValidatingReader_ilo_get_Type43_m1850338497 (Il2CppObject * __this /* static, unused */, JsonSchemaModel_t3035618138 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
