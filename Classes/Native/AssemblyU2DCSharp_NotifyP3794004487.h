﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotifyP
struct  NotifyP_t3794004487  : public MonoBehaviour_t667441552
{
public:
	// System.UInt32 NotifyP::<ID>k__BackingField
	uint32_t ___U3CIDU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CIDU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(NotifyP_t3794004487, ___U3CIDU3Ek__BackingField_3)); }
	inline uint32_t get_U3CIDU3Ek__BackingField_3() const { return ___U3CIDU3Ek__BackingField_3; }
	inline uint32_t* get_address_of_U3CIDU3Ek__BackingField_3() { return &___U3CIDU3Ek__BackingField_3; }
	inline void set_U3CIDU3Ek__BackingField_3(uint32_t value)
	{
		___U3CIDU3Ek__BackingField_3 = value;
	}
};

struct NotifyP_t3794004487_StaticFields
{
public:
	// System.String NotifyP::SCEND_DESTROY_MESSAGE
	String_t* ___SCEND_DESTROY_MESSAGE_2;

public:
	inline static int32_t get_offset_of_SCEND_DESTROY_MESSAGE_2() { return static_cast<int32_t>(offsetof(NotifyP_t3794004487_StaticFields, ___SCEND_DESTROY_MESSAGE_2)); }
	inline String_t* get_SCEND_DESTROY_MESSAGE_2() const { return ___SCEND_DESTROY_MESSAGE_2; }
	inline String_t** get_address_of_SCEND_DESTROY_MESSAGE_2() { return &___SCEND_DESTROY_MESSAGE_2; }
	inline void set_SCEND_DESTROY_MESSAGE_2(String_t* value)
	{
		___SCEND_DESTROY_MESSAGE_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCEND_DESTROY_MESSAGE_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
