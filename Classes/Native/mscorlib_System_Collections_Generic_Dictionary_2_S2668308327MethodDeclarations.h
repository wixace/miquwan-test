﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<UIModelDisplayType,System.Int32>
struct ShimEnumerator_t2668308327;
// System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>
struct Dictionary_2_t2952530300;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<UIModelDisplayType,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1714606622_gshared (ShimEnumerator_t2668308327 * __this, Dictionary_2_t2952530300 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m1714606622(__this, ___host0, method) ((  void (*) (ShimEnumerator_t2668308327 *, Dictionary_2_t2952530300 *, const MethodInfo*))ShimEnumerator__ctor_m1714606622_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<UIModelDisplayType,System.Int32>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1906173763_gshared (ShimEnumerator_t2668308327 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1906173763(__this, method) ((  bool (*) (ShimEnumerator_t2668308327 *, const MethodInfo*))ShimEnumerator_MoveNext_m1906173763_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<UIModelDisplayType,System.Int32>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m4010570353_gshared (ShimEnumerator_t2668308327 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m4010570353(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t2668308327 *, const MethodInfo*))ShimEnumerator_get_Entry_m4010570353_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UIModelDisplayType,System.Int32>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m535594892_gshared (ShimEnumerator_t2668308327 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m535594892(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2668308327 *, const MethodInfo*))ShimEnumerator_get_Key_m535594892_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UIModelDisplayType,System.Int32>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m359052062_gshared (ShimEnumerator_t2668308327 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m359052062(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2668308327 *, const MethodInfo*))ShimEnumerator_get_Value_m359052062_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UIModelDisplayType,System.Int32>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2876338726_gshared (ShimEnumerator_t2668308327 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m2876338726(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2668308327 *, const MethodInfo*))ShimEnumerator_get_Current_m2876338726_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<UIModelDisplayType,System.Int32>::Reset()
extern "C"  void ShimEnumerator_Reset_m2309955440_gshared (ShimEnumerator_t2668308327 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m2309955440(__this, method) ((  void (*) (ShimEnumerator_t2668308327 *, const MethodInfo*))ShimEnumerator_Reset_m2309955440_gshared)(__this, method)
