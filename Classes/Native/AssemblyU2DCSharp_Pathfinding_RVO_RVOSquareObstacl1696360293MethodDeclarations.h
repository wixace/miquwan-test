﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RVO.RVOSquareObstacle
struct RVOSquareObstacle_t1696360293;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.RVO.RVOSquareObstacle::.ctor()
extern "C"  void RVOSquareObstacle__ctor_m389963233 (RVOSquareObstacle_t1696360293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RVO.RVOSquareObstacle::get_StaticObstacle()
extern "C"  bool RVOSquareObstacle_get_StaticObstacle_m453511221 (RVOSquareObstacle_t1696360293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RVO.RVOSquareObstacle::get_ExecuteInEditor()
extern "C"  bool RVOSquareObstacle_get_ExecuteInEditor_m500034641 (RVOSquareObstacle_t1696360293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RVO.RVOSquareObstacle::get_LocalCoordinates()
extern "C"  bool RVOSquareObstacle_get_LocalCoordinates_m860219432 (RVOSquareObstacle_t1696360293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RVO.RVOSquareObstacle::get_Height()
extern "C"  float RVOSquareObstacle_get_Height_m3702140327 (RVOSquareObstacle_t1696360293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RVO.RVOSquareObstacle::AreGizmosDirty()
extern "C"  bool RVOSquareObstacle_AreGizmosDirty_m151438452 (RVOSquareObstacle_t1696360293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOSquareObstacle::CreateObstacles()
extern "C"  void RVOSquareObstacle_CreateObstacles_m2192338343 (RVOSquareObstacle_t1696360293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
