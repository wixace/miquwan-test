﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Int32>
struct Action_1_t1549654636;
// Pathfinding.EuclideanEmbedding
struct EuclideanEmbedding_t908139023;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120
struct  U3CRecalculateCostsU3Ec__AnonStorey120_t3302164579  : public Il2CppObject
{
public:
	// System.Action`1<System.Int32> Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120::startCostCalculation
	Action_1_t1549654636 * ___startCostCalculation_0;
	// Pathfinding.EuclideanEmbedding Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120::<>f__this
	EuclideanEmbedding_t908139023 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_startCostCalculation_0() { return static_cast<int32_t>(offsetof(U3CRecalculateCostsU3Ec__AnonStorey120_t3302164579, ___startCostCalculation_0)); }
	inline Action_1_t1549654636 * get_startCostCalculation_0() const { return ___startCostCalculation_0; }
	inline Action_1_t1549654636 ** get_address_of_startCostCalculation_0() { return &___startCostCalculation_0; }
	inline void set_startCostCalculation_0(Action_1_t1549654636 * value)
	{
		___startCostCalculation_0 = value;
		Il2CppCodeGenWriteBarrier(&___startCostCalculation_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CRecalculateCostsU3Ec__AnonStorey120_t3302164579, ___U3CU3Ef__this_1)); }
	inline EuclideanEmbedding_t908139023 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline EuclideanEmbedding_t908139023 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(EuclideanEmbedding_t908139023 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
