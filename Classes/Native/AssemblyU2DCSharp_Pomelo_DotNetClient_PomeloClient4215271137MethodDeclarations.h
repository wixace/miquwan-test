﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pomelo.DotNetClient.PomeloClient
struct PomeloClient_t4215271137;
// System.Action`1<Pomelo.DotNetClient.NetWorkState>
struct Action_1_t3724939911;
// System.String
struct String_t;
// System.Action
struct Action_t3771233898;
// Client/ResponseCallBack
struct ResponseCallBack_t59495818;
// Client/PushCallBack
struct PushCallBack_t4203546979;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// System.Action`1<Newtonsoft.Json.Linq.JObject>
struct Action_1_t2194360335;
// Pomelo.DotNetClient.Message
struct Message_t686303821;
// System.Object
struct Il2CppObject;
// Pomelo.DotNetClient.Protocol
struct Protocol_t400511732;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Pomelo.DotNetClient.EventManager
struct EventManager_t3099234959;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_Client_ResponseCallBack59495818.h"
#include "AssemblyU2DCSharp_Client_PushCallBack4203546979.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_NetWorkState3329123775.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_Message686303821.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_PomeloClient4215271137.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_Protocol400511732.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_EventManager3099234959.h"

// System.Void Pomelo.DotNetClient.PomeloClient::.ctor()
extern "C"  void PomeloClient__ctor_m3576686267 (PomeloClient_t4215271137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::.cctor()
extern "C"  void PomeloClient__cctor_m3020995666 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::add_NetWorkStateChangedEvent(System.Action`1<Pomelo.DotNetClient.NetWorkState>)
extern "C"  void PomeloClient_add_NetWorkStateChangedEvent_m2807467512 (PomeloClient_t4215271137 * __this, Action_1_t3724939911 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::remove_NetWorkStateChangedEvent(System.Action`1<Pomelo.DotNetClient.NetWorkState>)
extern "C"  void PomeloClient_remove_NetWorkStateChangedEvent_m367511659 (PomeloClient_t4215271137 * __this, Action_1_t3724939911 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::initClient(System.String,System.Int32,System.Action,Client/ResponseCallBack,Client/PushCallBack)
extern "C"  void PomeloClient_initClient_m2349843419 (PomeloClient_t4215271137 * __this, String_t* ___host0, int32_t ___port1, Action_t3771233898 * ___callback2, ResponseCallBack_t59495818 * ___responseFun3, PushCallBack_t4203546979 * ___pushFun4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::TimeoutEvent()
extern "C"  void PomeloClient_TimeoutEvent_m1255062178 (PomeloClient_t4215271137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::NetWorkChanged(Pomelo.DotNetClient.NetWorkState)
extern "C"  void PomeloClient_NetWorkChanged_m2799885617 (PomeloClient_t4215271137 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::connect()
extern "C"  void PomeloClient_connect_m2580855907 (PomeloClient_t4215271137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::connect(Newtonsoft.Json.Linq.JObject)
extern "C"  void PomeloClient_connect_m544637203 (PomeloClient_t4215271137 * __this, JObject_t1798544199 * ___user0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::connect(System.Action`1<Newtonsoft.Json.Linq.JObject>)
extern "C"  void PomeloClient_connect_m1424041239 (PomeloClient_t4215271137 * __this, Action_1_t2194360335 * ___handshakeCallback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pomelo.DotNetClient.PomeloClient::connect(Newtonsoft.Json.Linq.JObject,System.Action`1<Newtonsoft.Json.Linq.JObject>)
extern "C"  bool PomeloClient_connect_m1377947507 (PomeloClient_t4215271137 * __this, JObject_t1798544199 * ___user0, Action_1_t2194360335 * ___handshakeCallback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::request(System.String)
extern "C"  void PomeloClient_request_m3538666298 (PomeloClient_t4215271137 * __this, String_t* ___route0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pomelo.DotNetClient.PomeloClient::request(System.String,Newtonsoft.Json.Linq.JObject)
extern "C"  uint32_t PomeloClient_request_m1175583873 (PomeloClient_t4215271137 * __this, String_t* ___route0, JObject_t1798544199 * ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::notify(System.String,Newtonsoft.Json.Linq.JObject)
extern "C"  void PomeloClient_notify_m4170089094 (PomeloClient_t4215271137 * __this, String_t* ___route0, JObject_t1798544199 * ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::processMessage(Pomelo.DotNetClient.Message)
extern "C"  void PomeloClient_processMessage_m3242787301 (PomeloClient_t4215271137 * __this, Message_t686303821 * ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::OnPushMsg(System.String,System.String)
extern "C"  void PomeloClient_OnPushMsg_m3141982749 (PomeloClient_t4215271137 * __this, String_t* ___route0, String_t* ___msgString1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::OnResponseMsg(System.UInt32,System.UInt32,System.String)
extern "C"  void PomeloClient_OnResponseMsg_m3784255216 (PomeloClient_t4215271137 * __this, uint32_t ___id0, uint32_t ___code1, String_t* ___msgString2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::Logout()
extern "C"  void PomeloClient_Logout_m1640920115 (PomeloClient_t4215271137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::disconnect()
extern "C"  void PomeloClient_disconnect_m3121451685 (PomeloClient_t4215271137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::Dispose()
extern "C"  void PomeloClient_Dispose_m1119948856 (PomeloClient_t4215271137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::Dispose(System.Boolean)
extern "C"  void PomeloClient_Dispose_m1040909231 (PomeloClient_t4215271137 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::ilo_NetWorkChanged1(Pomelo.DotNetClient.PomeloClient,Pomelo.DotNetClient.NetWorkState)
extern "C"  void PomeloClient_ilo_NetWorkChanged1_m764065239 (Il2CppObject * __this /* static, unused */, PomeloClient_t4215271137 * ____this0, int32_t ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::ilo_LogError2(System.Object,System.Boolean)
extern "C"  void PomeloClient_ilo_LogError2_m2513369405 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pomelo.DotNetClient.PomeloClient::ilo_connect3(Pomelo.DotNetClient.PomeloClient,Newtonsoft.Json.Linq.JObject,System.Action`1<Newtonsoft.Json.Linq.JObject>)
extern "C"  bool PomeloClient_ilo_connect3_m2942278097 (Il2CppObject * __this /* static, unused */, PomeloClient_t4215271137 * ____this0, JObject_t1798544199 * ___user1, Action_1_t2194360335 * ___handshakeCallback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::ilo_start4(Pomelo.DotNetClient.Protocol,Newtonsoft.Json.Linq.JObject,System.Action`1<Newtonsoft.Json.Linq.JObject>)
extern "C"  void PomeloClient_ilo_start4_m1755228783 (Il2CppObject * __this /* static, unused */, Protocol_t400511732 * ____this0, JObject_t1798544199 * ___user1, Action_1_t2194360335 * ___callback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::ilo_send5(Pomelo.DotNetClient.Protocol,System.String,System.UInt32,Newtonsoft.Json.Linq.JObject)
extern "C"  void PomeloClient_ilo_send5_m3298416390 (Il2CppObject * __this /* static, unused */, Protocol_t400511732 * ____this0, String_t* ___route1, uint32_t ___id2, JObject_t1798544199 * ___msg3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::ilo_send6(Pomelo.DotNetClient.Protocol,System.String,Newtonsoft.Json.Linq.JObject)
extern "C"  void PomeloClient_ilo_send6_m2457126105 (Il2CppObject * __this /* static, unused */, Protocol_t400511732 * ____this0, String_t* ___route1, JObject_t1798544199 * ___msg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pomelo.DotNetClient.PomeloClient::ilo_ToString7(Newtonsoft.Json.Linq.JToken)
extern "C"  String_t* PomeloClient_ilo_ToString7_m344640732 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::ilo_AddResponse8(Pomelo.DotNetClient.EventManager,System.UInt32,System.UInt32,System.String)
extern "C"  void PomeloClient_ilo_AddResponse8_m1352949736 (Il2CppObject * __this /* static, unused */, EventManager_t3099234959 * ____this0, uint32_t ___id1, uint32_t ___code2, String_t* ___msgString3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::ilo_AddPush9(Pomelo.DotNetClient.EventManager,System.String,System.String)
extern "C"  void PomeloClient_ilo_AddPush9_m3435028996 (Il2CppObject * __this /* static, unused */, EventManager_t3099234959 * ____this0, String_t* ___route1, String_t* ___msgString2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::ilo_Dispose10(Pomelo.DotNetClient.PomeloClient,System.Boolean)
extern "C"  void PomeloClient_ilo_Dispose10_m221829657 (Il2CppObject * __this /* static, unused */, PomeloClient_t4215271137 * ____this0, bool ___disposing1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient::ilo_Dispose11(Pomelo.DotNetClient.EventManager)
extern "C"  void PomeloClient_ilo_Dispose11_m908666551 (Il2CppObject * __this /* static, unused */, EventManager_t3099234959 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
