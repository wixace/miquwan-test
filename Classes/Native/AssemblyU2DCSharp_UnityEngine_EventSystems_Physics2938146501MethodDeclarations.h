﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_Physics2DRaycasterGenerated
struct UnityEngine_EventSystems_Physics2DRaycasterGenerated_t2938146501;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_EventSystems_Physics2DRaycasterGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_Physics2DRaycasterGenerated__ctor_m3049077110 (UnityEngine_EventSystems_Physics2DRaycasterGenerated_t2938146501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_Physics2DRaycasterGenerated::Physics2DRaycaster_Raycast__PointerEventData__ListT1_RaycastResult(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_Physics2DRaycasterGenerated_Physics2DRaycaster_Raycast__PointerEventData__ListT1_RaycastResult_m2709762306 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_Physics2DRaycasterGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_Physics2DRaycasterGenerated___Register_m3415981329 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_EventSystems_Physics2DRaycasterGenerated::ilo_getObject1(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_EventSystems_Physics2DRaycasterGenerated_ilo_getObject1_m3947388217 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
