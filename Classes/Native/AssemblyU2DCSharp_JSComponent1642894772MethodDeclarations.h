﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSComponent
struct JSComponent_t1642894772;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.String
struct String_t;
// JSEngine
struct JSEngine_t2847479019;
// JSSerializer
struct JSSerializer_t3534558139;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSComponent1642894772.h"
#include "AssemblyU2DCSharp_JSEngine2847479019.h"
#include "AssemblyU2DCSharp_JSSerializer3534558139.h"

// System.Void JSComponent::.ctor()
extern "C"  void JSComponent__ctor_m3711569975 (JSComponent_t1642894772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent::initMemberFunction()
extern "C"  void JSComponent_initMemberFunction_m2486722127 (JSComponent_t1642894772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent::removeIfExist(System.Int32)
extern "C"  void JSComponent_removeIfExist_m1035221884 (JSComponent_t1642894772 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent::removeMemberFunction()
extern "C"  void JSComponent_removeMemberFunction_m2507388611 (JSComponent_t1642894772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSComponent::get_jsSuccess()
extern "C"  bool JSComponent_get_jsSuccess_m2775031482 (JSComponent_t1642894772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent::set_jsSuccess(System.Boolean)
extern "C"  void JSComponent_set_jsSuccess_m3873533705 (JSComponent_t1642894772 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSComponent::get_jsFail()
extern "C"  bool JSComponent_get_jsFail_m3144532457 (JSComponent_t1642894772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent::set_jsFail(System.Boolean)
extern "C"  void JSComponent_set_jsFail_m1323737672 (JSComponent_t1642894772 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent::callIfExist(System.Int32,System.Object[])
extern "C"  void JSComponent_callIfExist_m557376846 (JSComponent_t1642894772 * __this, int32_t ___funID0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent::initJS()
extern "C"  void JSComponent_initJS_m3997266726 (JSComponent_t1642894772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent::init(System.Boolean)
extern "C"  void JSComponent_init_m2844011988 (JSComponent_t1642894772 * __this, bool ___callSerialize0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent::initSerializedData(System.Int32)
extern "C"  void JSComponent_initSerializedData_m791276252 (JSComponent_t1642894772 * __this, int32_t ___jsObjID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent::callAwake()
extern "C"  void JSComponent_callAwake_m264046460 (JSComponent_t1642894772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent::Awake()
extern "C"  void JSComponent_Awake_m3949175194 (JSComponent_t1642894772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSComponent::GetJSObjID(System.Boolean)
extern "C"  int32_t JSComponent_GetJSObjID_m2686203017 (JSComponent_t1642894772 * __this, bool ___callSerialize0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent::Start()
extern "C"  void JSComponent_Start_m2658707767 (JSComponent_t1642894772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent::OnDestroy()
extern "C"  void JSComponent_OnDestroy_m613085616 (JSComponent_t1642894772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent::StartSinking()
extern "C"  void JSComponent_StartSinking_m4191216794 (JSComponent_t1642894772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent::RestartLevel()
extern "C"  void JSComponent_RestartLevel_m200405922 (JSComponent_t1642894772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSComponent::get_jsobjID()
extern "C"  int32_t JSComponent_get_jsobjID_m2596585411 (JSComponent_t1642894772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSComponent::ilo_getObjFunction1(System.Int32,System.String)
extern "C"  int32_t JSComponent_ilo_getObjFunction1_m2394164629 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___fname1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent::ilo_set_jsFail2(JSComponent,System.Boolean)
extern "C"  void JSComponent_ilo_set_jsFail2_m763699919 (Il2CppObject * __this /* static, unused */, JSComponent_t1642894772 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSComponent::ilo_get_initSuccess3()
extern "C"  bool JSComponent_ilo_get_initSuccess3_m3185546261 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSComponent::ilo_get_initFail4()
extern "C"  bool JSComponent_ilo_get_initFail4_m982331475 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent::ilo_FirstInit5(JSEngine)
extern "C"  void JSComponent_ilo_FirstInit5_m255283306 (Il2CppObject * __this /* static, unused */, JSEngine_t2847479019 * ___jse0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSComponent::ilo_get_DataSerialized6(JSSerializer)
extern "C"  bool JSComponent_ilo_get_DataSerialized6_m2291809242 (Il2CppObject * __this /* static, unused */, JSSerializer_t3534558139 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent::ilo_initSerializedData7(JSSerializer,System.Int32)
extern "C"  void JSComponent_ilo_initSerializedData7_m1182298839 (Il2CppObject * __this /* static, unused */, JSSerializer_t3534558139 * ____this0, int32_t ___jsObjID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSComponent::ilo_get_jsSuccess8(JSComponent)
extern "C"  bool JSComponent_ilo_get_jsSuccess8_m1473286667 (Il2CppObject * __this /* static, unused */, JSComponent_t1642894772 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent::ilo_callIfExist9(JSComponent,System.Int32,System.Object[])
extern "C"  void JSComponent_ilo_callIfExist9_m370578978 (Il2CppObject * __this /* static, unused */, JSComponent_t1642894772 * ____this0, int32_t ___funID1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent::ilo_init10(JSComponent,System.Boolean)
extern "C"  void JSComponent_ilo_init10_m3260331502 (Il2CppObject * __this /* static, unused */, JSComponent_t1642894772 * ____this0, bool ___callSerialize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSComponent::ilo_get_IsShutDown11()
extern "C"  bool JSComponent_ilo_get_IsShutDown11_m1762563669 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent::ilo_removeMemberFunction12(JSComponent)
extern "C"  void JSComponent_ilo_removeMemberFunction12_m3677509895 (Il2CppObject * __this /* static, unused */, JSComponent_t1642894772 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
