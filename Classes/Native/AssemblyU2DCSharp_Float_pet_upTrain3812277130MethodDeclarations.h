﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_pet_upTrain
struct Float_pet_upTrain_t3812277130;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// FloatTextUnit
struct FloatTextUnit_t2362298029;
// EventDelegate/Callback
struct Callback_t1094463061;
// UILabel
struct UILabel_t291504320;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "AssemblyU2DCSharp_UITweener_Method1078127180.h"
#include "AssemblyU2DCSharp_UILabel291504320.h"
#include "mscorlib_System_String7231557.h"

// System.Void Float_pet_upTrain::.ctor()
extern "C"  void Float_pet_upTrain__ctor_m185748769 (Float_pet_upTrain_t3812277130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_pet_upTrain::FloatID()
extern "C"  int32_t Float_pet_upTrain_FloatID_m1432304031 (Float_pet_upTrain_t3812277130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_pet_upTrain::OnAwake(UnityEngine.GameObject)
extern "C"  void Float_pet_upTrain_OnAwake_m2626100701 (Float_pet_upTrain_t3812277130 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_pet_upTrain::OnDestroy()
extern "C"  void Float_pet_upTrain_OnDestroy_m13019546 (Float_pet_upTrain_t3812277130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_pet_upTrain::Init()
extern "C"  void Float_pet_upTrain_Init_m2451380499 (Float_pet_upTrain_t3812277130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_pet_upTrain::SetFile(System.Object[])
extern "C"  void Float_pet_upTrain_SetFile_m800856533 (Float_pet_upTrain_t3812277130 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_pet_upTrain::ilo_OnAwake1(FloatTextUnit,UnityEngine.GameObject)
extern "C"  void Float_pet_upTrain_ilo_OnAwake1_m2503593082 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, GameObject_t3674682005 * ___viewGO1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_pet_upTrain::ilo_OnDestroy2(FloatTextUnit)
extern "C"  void Float_pet_upTrain_ilo_OnDestroy2_m1050668504 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_pet_upTrain::ilo_MovePosition3(FloatTextUnit,UnityEngine.Vector3,System.Single,System.Single,System.Single,EventDelegate/Callback,UnityEngine.GameObject,UITweener/Method)
extern "C"  void Float_pet_upTrain_ilo_MovePosition3_m16956555 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, Vector3_t4282066566  ___from1, float ___toUp2, float ___duration3, float ___delay4, Callback_t1094463061 * ___call5, GameObject_t3674682005 * ___go6, int32_t ___method7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_pet_upTrain::ilo_set_text4(UILabel,System.String)
extern "C"  void Float_pet_upTrain_ilo_set_text4_m1009941976 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_pet_upTrain::ilo_Play5(FloatTextUnit)
extern "C"  void Float_pet_upTrain_ilo_Play5_m2169258200 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
