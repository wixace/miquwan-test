﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.SparseTexture
struct SparseTexture_t1507634001;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2960766953;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_TextureFormat4189619560.h"
#include "UnityEngine_UnityEngine_SparseTexture1507634001.h"

// System.Void UnityEngine.SparseTexture::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Int32)
extern "C"  void SparseTexture__ctor_m688570174 (SparseTexture_t1507634001 * __this, int32_t ___width0, int32_t ___height1, int32_t ___format2, int32_t ___mipCount3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SparseTexture::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Int32,System.Boolean)
extern "C"  void SparseTexture__ctor_m3805507711 (SparseTexture_t1507634001 * __this, int32_t ___width0, int32_t ___height1, int32_t ___format2, int32_t ___mipCount3, bool ___linear4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SparseTexture::get_tileWidth()
extern "C"  int32_t SparseTexture_get_tileWidth_m615897707 (SparseTexture_t1507634001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SparseTexture::get_tileHeight()
extern "C"  int32_t SparseTexture_get_tileHeight_m2416774084 (SparseTexture_t1507634001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SparseTexture::get_isCreated()
extern "C"  bool SparseTexture_get_isCreated_m766938743 (SparseTexture_t1507634001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SparseTexture::Internal_Create(UnityEngine.SparseTexture,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Int32,System.Boolean)
extern "C"  void SparseTexture_Internal_Create_m2691023147 (Il2CppObject * __this /* static, unused */, SparseTexture_t1507634001 * ___mono0, int32_t ___width1, int32_t ___height2, int32_t ___format3, int32_t ___mipCount4, bool ___linear5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SparseTexture::UpdateTile(System.Int32,System.Int32,System.Int32,UnityEngine.Color32[])
extern "C"  void SparseTexture_UpdateTile_m3826338285 (SparseTexture_t1507634001 * __this, int32_t ___tileX0, int32_t ___tileY1, int32_t ___miplevel2, Color32U5BU5D_t2960766953* ___data3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SparseTexture::UpdateTileRaw(System.Int32,System.Int32,System.Int32,System.Byte[])
extern "C"  void SparseTexture_UpdateTileRaw_m1392359169 (SparseTexture_t1507634001 * __this, int32_t ___tileX0, int32_t ___tileY1, int32_t ___miplevel2, ByteU5BU5D_t4260760469* ___data3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SparseTexture::UnloadTile(System.Int32,System.Int32,System.Int32)
extern "C"  void SparseTexture_UnloadTile_m1214488740 (SparseTexture_t1507634001 * __this, int32_t ___tileX0, int32_t ___tileY1, int32_t ___miplevel2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
