﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>
struct Dictionary_2_t1311121246;
// System.Collections.Generic.IEqualityComparer`1<Entity.Behavior.EBehaviorID>
struct IEqualityComparer_1_t2029567540;
// System.Collections.Generic.IDictionary`2<Entity.Behavior.EBehaviorID,System.Object>
struct IDictionary_2_t888994591;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<Entity.Behavior.EBehaviorID>
struct ICollection_1_t2133123123;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t770439062;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>[]
struct KeyValuePair_2U5BU5D_t500293249;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>>
struct IEnumerator_1_t3121767001;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,System.Object>
struct KeyCollection_t2937880697;
// System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,System.Object>
struct ValueCollection_t11726959;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21209901952.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2628444638.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m785106050_gshared (Dictionary_2_t1311121246 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m785106050(__this, method) ((  void (*) (Dictionary_2_t1311121246 *, const MethodInfo*))Dictionary_2__ctor_m785106050_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1815281145_gshared (Dictionary_2_t1311121246 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m1815281145(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t1311121246 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1815281145_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m454866518_gshared (Dictionary_2_t1311121246 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m454866518(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t1311121246 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m454866518_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m3697067731_gshared (Dictionary_2_t1311121246 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m3697067731(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t1311121246 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3697067731_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2979107687_gshared (Dictionary_2_t1311121246 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2979107687(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t1311121246 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2979107687_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m1110081219_gshared (Dictionary_2_t1311121246 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m1110081219(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1311121246 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m1110081219_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m159367500_gshared (Dictionary_2_t1311121246 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m159367500(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1311121246 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m159367500_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2082974220_gshared (Dictionary_2_t1311121246 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2082974220(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1311121246 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2082974220_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m2655177928_gshared (Dictionary_2_t1311121246 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m2655177928(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1311121246 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m2655177928_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m2782969782_gshared (Dictionary_2_t1311121246 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m2782969782(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1311121246 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m2782969782_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2332495519_gshared (Dictionary_2_t1311121246 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2332495519(__this, method) ((  bool (*) (Dictionary_2_t1311121246 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2332495519_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2468913274_gshared (Dictionary_2_t1311121246 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2468913274(__this, method) ((  bool (*) (Dictionary_2_t1311121246 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2468913274_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m799494406_gshared (Dictionary_2_t1311121246 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m799494406(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1311121246 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m799494406_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m1527727083_gshared (Dictionary_2_t1311121246 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1527727083(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1311121246 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1527727083_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m2022765446_gshared (Dictionary_2_t1311121246 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m2022765446(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1311121246 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m2022765446_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m1827093296_gshared (Dictionary_2_t1311121246 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1827093296(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1311121246 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1827093296_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m635761705_gshared (Dictionary_2_t1311121246 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m635761705(__this, ___key0, method) ((  void (*) (Dictionary_2_t1311121246 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m635761705_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2905063972_gshared (Dictionary_2_t1311121246 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2905063972(__this, method) ((  bool (*) (Dictionary_2_t1311121246 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2905063972_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3136477136_gshared (Dictionary_2_t1311121246 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3136477136(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1311121246 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3136477136_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2214987112_gshared (Dictionary_2_t1311121246 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2214987112(__this, method) ((  bool (*) (Dictionary_2_t1311121246 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2214987112_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m515225663_gshared (Dictionary_2_t1311121246 * __this, KeyValuePair_2_t1209901952  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m515225663(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t1311121246 *, KeyValuePair_2_t1209901952 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m515225663_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1669961187_gshared (Dictionary_2_t1311121246 * __this, KeyValuePair_2_t1209901952  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1669961187(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1311121246 *, KeyValuePair_2_t1209901952 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1669961187_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2613229155_gshared (Dictionary_2_t1311121246 * __this, KeyValuePair_2U5BU5D_t500293249* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2613229155(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1311121246 *, KeyValuePair_2U5BU5D_t500293249*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2613229155_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2470042056_gshared (Dictionary_2_t1311121246 * __this, KeyValuePair_2_t1209901952  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2470042056(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1311121246 *, KeyValuePair_2_t1209901952 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2470042056_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m3371931394_gshared (Dictionary_2_t1311121246 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3371931394(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1311121246 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3371931394_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2341954301_gshared (Dictionary_2_t1311121246 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2341954301(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1311121246 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2341954301_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3329027578_gshared (Dictionary_2_t1311121246 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3329027578(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1311121246 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3329027578_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1722162453_gshared (Dictionary_2_t1311121246 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1722162453(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1311121246 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1722162453_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m669334506_gshared (Dictionary_2_t1311121246 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m669334506(__this, method) ((  int32_t (*) (Dictionary_2_t1311121246 *, const MethodInfo*))Dictionary_2_get_Count_m669334506_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m1762803905_gshared (Dictionary_2_t1311121246 * __this, uint8_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m1762803905(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1311121246 *, uint8_t, const MethodInfo*))Dictionary_2_get_Item_m1762803905_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m1850011330_gshared (Dictionary_2_t1311121246 * __this, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m1850011330(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1311121246 *, uint8_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m1850011330_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m3457911802_gshared (Dictionary_2_t1311121246 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m3457911802(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t1311121246 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3457911802_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m3930303229_gshared (Dictionary_2_t1311121246 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m3930303229(__this, ___size0, method) ((  void (*) (Dictionary_2_t1311121246 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3930303229_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m2263681081_gshared (Dictionary_2_t1311121246 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m2263681081(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1311121246 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2263681081_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t1209901952  Dictionary_2_make_pair_m2389272709_gshared (Il2CppObject * __this /* static, unused */, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m2389272709(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t1209901952  (*) (Il2CppObject * /* static, unused */, uint8_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m2389272709_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::pick_key(TKey,TValue)
extern "C"  uint8_t Dictionary_2_pick_key_m3404330449_gshared (Il2CppObject * __this /* static, unused */, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m3404330449(__this /* static, unused */, ___key0, ___value1, method) ((  uint8_t (*) (Il2CppObject * /* static, unused */, uint8_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m3404330449_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m1252651217_gshared (Il2CppObject * __this /* static, unused */, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m1252651217(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, uint8_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m1252651217_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m125726070_gshared (Dictionary_2_t1311121246 * __this, KeyValuePair_2U5BU5D_t500293249* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m125726070(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1311121246 *, KeyValuePair_2U5BU5D_t500293249*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m125726070_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m2921247734_gshared (Dictionary_2_t1311121246 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m2921247734(__this, method) ((  void (*) (Dictionary_2_t1311121246 *, const MethodInfo*))Dictionary_2_Resize_m2921247734_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m4186137203_gshared (Dictionary_2_t1311121246 * __this, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m4186137203(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1311121246 *, uint8_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m4186137203_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m2486206637_gshared (Dictionary_2_t1311121246 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m2486206637(__this, method) ((  void (*) (Dictionary_2_t1311121246 *, const MethodInfo*))Dictionary_2_Clear_m2486206637_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m3211975891_gshared (Dictionary_2_t1311121246 * __this, uint8_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m3211975891(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1311121246 *, uint8_t, const MethodInfo*))Dictionary_2_ContainsKey_m3211975891_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m3533541203_gshared (Dictionary_2_t1311121246 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m3533541203(__this, ___value0, method) ((  bool (*) (Dictionary_2_t1311121246 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m3533541203_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m2536395552_gshared (Dictionary_2_t1311121246 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m2536395552(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1311121246 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m2536395552_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m1805177668_gshared (Dictionary_2_t1311121246 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1805177668(__this, ___sender0, method) ((  void (*) (Dictionary_2_t1311121246 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1805177668_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m2681562173_gshared (Dictionary_2_t1311121246 * __this, uint8_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m2681562173(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1311121246 *, uint8_t, const MethodInfo*))Dictionary_2_Remove_m2681562173_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m1087025708_gshared (Dictionary_2_t1311121246 * __this, uint8_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m1087025708(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t1311121246 *, uint8_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m1087025708_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::get_Keys()
extern "C"  KeyCollection_t2937880697 * Dictionary_2_get_Keys_m3806954067_gshared (Dictionary_2_t1311121246 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m3806954067(__this, method) ((  KeyCollection_t2937880697 * (*) (Dictionary_2_t1311121246 *, const MethodInfo*))Dictionary_2_get_Keys_m3806954067_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::get_Values()
extern "C"  ValueCollection_t11726959 * Dictionary_2_get_Values_m1453054611_gshared (Dictionary_2_t1311121246 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m1453054611(__this, method) ((  ValueCollection_t11726959 * (*) (Dictionary_2_t1311121246 *, const MethodInfo*))Dictionary_2_get_Values_m1453054611_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::ToTKey(System.Object)
extern "C"  uint8_t Dictionary_2_ToTKey_m2854189356_gshared (Dictionary_2_t1311121246 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m2854189356(__this, ___key0, method) ((  uint8_t (*) (Dictionary_2_t1311121246 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m2854189356_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m3859932652_gshared (Dictionary_2_t1311121246 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m3859932652(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t1311121246 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m3859932652_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m192293408_gshared (Dictionary_2_t1311121246 * __this, KeyValuePair_2_t1209901952  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m192293408(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t1311121246 *, KeyValuePair_2_t1209901952 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m192293408_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2628444638  Dictionary_2_GetEnumerator_m468953031_gshared (Dictionary_2_t1311121246 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m468953031(__this, method) ((  Enumerator_t2628444638  (*) (Dictionary_2_t1311121246 *, const MethodInfo*))Dictionary_2_GetEnumerator_m468953031_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m245864022_gshared (Il2CppObject * __this /* static, unused */, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m245864022(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, uint8_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m245864022_gshared)(__this /* static, unused */, ___key0, ___value1, method)
