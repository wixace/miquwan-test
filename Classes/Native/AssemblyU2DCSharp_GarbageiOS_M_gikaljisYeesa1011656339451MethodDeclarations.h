﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_gikaljisYeesa101
struct M_gikaljisYeesa101_t1656339451;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_gikaljisYeesa101::.ctor()
extern "C"  void M_gikaljisYeesa101__ctor_m848235672 (M_gikaljisYeesa101_t1656339451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gikaljisYeesa101::M_cecarPetootall0(System.String[],System.Int32)
extern "C"  void M_gikaljisYeesa101_M_cecarPetootall0_m566198449 (M_gikaljisYeesa101_t1656339451 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
