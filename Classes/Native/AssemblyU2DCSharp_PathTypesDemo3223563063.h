﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.Material
struct Material_t3870600107;
// Pathfinding.RichAI[]
struct RichAIU5BU5D_t3615124959;
// Pathfinding.Path
struct Path_t1974241691;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// Pathfinding.FloodPath
struct FloodPath_t3766979749;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PathTypesDemo
struct  PathTypesDemo_t3223563063  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 PathTypesDemo::activeDemo
	int32_t ___activeDemo_2;
	// UnityEngine.Transform PathTypesDemo::start
	Transform_t1659122786 * ___start_3;
	// UnityEngine.Transform PathTypesDemo::end
	Transform_t1659122786 * ___end_4;
	// UnityEngine.Vector3 PathTypesDemo::pathOffset
	Vector3_t4282066566  ___pathOffset_5;
	// UnityEngine.Material PathTypesDemo::lineMat
	Material_t3870600107 * ___lineMat_6;
	// UnityEngine.Material PathTypesDemo::squareMat
	Material_t3870600107 * ___squareMat_7;
	// System.Single PathTypesDemo::lineWidth
	float ___lineWidth_8;
	// Pathfinding.RichAI[] PathTypesDemo::agents
	RichAIU5BU5D_t3615124959* ___agents_9;
	// System.Int32 PathTypesDemo::searchLength
	int32_t ___searchLength_10;
	// System.Int32 PathTypesDemo::spread
	int32_t ___spread_11;
	// System.Single PathTypesDemo::aimStrength
	float ___aimStrength_12;
	// Pathfinding.Path PathTypesDemo::lastPath
	Path_t1974241691 * ___lastPath_13;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> PathTypesDemo::lastRender
	List_1_t747900261 * ___lastRender_14;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> PathTypesDemo::multipoints
	List_1_t1355284822 * ___multipoints_15;
	// UnityEngine.Vector2 PathTypesDemo::mouseDragStart
	Vector2_t4282066565  ___mouseDragStart_16;
	// System.Single PathTypesDemo::mouseDragStartTime
	float ___mouseDragStartTime_17;
	// Pathfinding.FloodPath PathTypesDemo::lastFlood
	FloodPath_t3766979749 * ___lastFlood_18;

public:
	inline static int32_t get_offset_of_activeDemo_2() { return static_cast<int32_t>(offsetof(PathTypesDemo_t3223563063, ___activeDemo_2)); }
	inline int32_t get_activeDemo_2() const { return ___activeDemo_2; }
	inline int32_t* get_address_of_activeDemo_2() { return &___activeDemo_2; }
	inline void set_activeDemo_2(int32_t value)
	{
		___activeDemo_2 = value;
	}

	inline static int32_t get_offset_of_start_3() { return static_cast<int32_t>(offsetof(PathTypesDemo_t3223563063, ___start_3)); }
	inline Transform_t1659122786 * get_start_3() const { return ___start_3; }
	inline Transform_t1659122786 ** get_address_of_start_3() { return &___start_3; }
	inline void set_start_3(Transform_t1659122786 * value)
	{
		___start_3 = value;
		Il2CppCodeGenWriteBarrier(&___start_3, value);
	}

	inline static int32_t get_offset_of_end_4() { return static_cast<int32_t>(offsetof(PathTypesDemo_t3223563063, ___end_4)); }
	inline Transform_t1659122786 * get_end_4() const { return ___end_4; }
	inline Transform_t1659122786 ** get_address_of_end_4() { return &___end_4; }
	inline void set_end_4(Transform_t1659122786 * value)
	{
		___end_4 = value;
		Il2CppCodeGenWriteBarrier(&___end_4, value);
	}

	inline static int32_t get_offset_of_pathOffset_5() { return static_cast<int32_t>(offsetof(PathTypesDemo_t3223563063, ___pathOffset_5)); }
	inline Vector3_t4282066566  get_pathOffset_5() const { return ___pathOffset_5; }
	inline Vector3_t4282066566 * get_address_of_pathOffset_5() { return &___pathOffset_5; }
	inline void set_pathOffset_5(Vector3_t4282066566  value)
	{
		___pathOffset_5 = value;
	}

	inline static int32_t get_offset_of_lineMat_6() { return static_cast<int32_t>(offsetof(PathTypesDemo_t3223563063, ___lineMat_6)); }
	inline Material_t3870600107 * get_lineMat_6() const { return ___lineMat_6; }
	inline Material_t3870600107 ** get_address_of_lineMat_6() { return &___lineMat_6; }
	inline void set_lineMat_6(Material_t3870600107 * value)
	{
		___lineMat_6 = value;
		Il2CppCodeGenWriteBarrier(&___lineMat_6, value);
	}

	inline static int32_t get_offset_of_squareMat_7() { return static_cast<int32_t>(offsetof(PathTypesDemo_t3223563063, ___squareMat_7)); }
	inline Material_t3870600107 * get_squareMat_7() const { return ___squareMat_7; }
	inline Material_t3870600107 ** get_address_of_squareMat_7() { return &___squareMat_7; }
	inline void set_squareMat_7(Material_t3870600107 * value)
	{
		___squareMat_7 = value;
		Il2CppCodeGenWriteBarrier(&___squareMat_7, value);
	}

	inline static int32_t get_offset_of_lineWidth_8() { return static_cast<int32_t>(offsetof(PathTypesDemo_t3223563063, ___lineWidth_8)); }
	inline float get_lineWidth_8() const { return ___lineWidth_8; }
	inline float* get_address_of_lineWidth_8() { return &___lineWidth_8; }
	inline void set_lineWidth_8(float value)
	{
		___lineWidth_8 = value;
	}

	inline static int32_t get_offset_of_agents_9() { return static_cast<int32_t>(offsetof(PathTypesDemo_t3223563063, ___agents_9)); }
	inline RichAIU5BU5D_t3615124959* get_agents_9() const { return ___agents_9; }
	inline RichAIU5BU5D_t3615124959** get_address_of_agents_9() { return &___agents_9; }
	inline void set_agents_9(RichAIU5BU5D_t3615124959* value)
	{
		___agents_9 = value;
		Il2CppCodeGenWriteBarrier(&___agents_9, value);
	}

	inline static int32_t get_offset_of_searchLength_10() { return static_cast<int32_t>(offsetof(PathTypesDemo_t3223563063, ___searchLength_10)); }
	inline int32_t get_searchLength_10() const { return ___searchLength_10; }
	inline int32_t* get_address_of_searchLength_10() { return &___searchLength_10; }
	inline void set_searchLength_10(int32_t value)
	{
		___searchLength_10 = value;
	}

	inline static int32_t get_offset_of_spread_11() { return static_cast<int32_t>(offsetof(PathTypesDemo_t3223563063, ___spread_11)); }
	inline int32_t get_spread_11() const { return ___spread_11; }
	inline int32_t* get_address_of_spread_11() { return &___spread_11; }
	inline void set_spread_11(int32_t value)
	{
		___spread_11 = value;
	}

	inline static int32_t get_offset_of_aimStrength_12() { return static_cast<int32_t>(offsetof(PathTypesDemo_t3223563063, ___aimStrength_12)); }
	inline float get_aimStrength_12() const { return ___aimStrength_12; }
	inline float* get_address_of_aimStrength_12() { return &___aimStrength_12; }
	inline void set_aimStrength_12(float value)
	{
		___aimStrength_12 = value;
	}

	inline static int32_t get_offset_of_lastPath_13() { return static_cast<int32_t>(offsetof(PathTypesDemo_t3223563063, ___lastPath_13)); }
	inline Path_t1974241691 * get_lastPath_13() const { return ___lastPath_13; }
	inline Path_t1974241691 ** get_address_of_lastPath_13() { return &___lastPath_13; }
	inline void set_lastPath_13(Path_t1974241691 * value)
	{
		___lastPath_13 = value;
		Il2CppCodeGenWriteBarrier(&___lastPath_13, value);
	}

	inline static int32_t get_offset_of_lastRender_14() { return static_cast<int32_t>(offsetof(PathTypesDemo_t3223563063, ___lastRender_14)); }
	inline List_1_t747900261 * get_lastRender_14() const { return ___lastRender_14; }
	inline List_1_t747900261 ** get_address_of_lastRender_14() { return &___lastRender_14; }
	inline void set_lastRender_14(List_1_t747900261 * value)
	{
		___lastRender_14 = value;
		Il2CppCodeGenWriteBarrier(&___lastRender_14, value);
	}

	inline static int32_t get_offset_of_multipoints_15() { return static_cast<int32_t>(offsetof(PathTypesDemo_t3223563063, ___multipoints_15)); }
	inline List_1_t1355284822 * get_multipoints_15() const { return ___multipoints_15; }
	inline List_1_t1355284822 ** get_address_of_multipoints_15() { return &___multipoints_15; }
	inline void set_multipoints_15(List_1_t1355284822 * value)
	{
		___multipoints_15 = value;
		Il2CppCodeGenWriteBarrier(&___multipoints_15, value);
	}

	inline static int32_t get_offset_of_mouseDragStart_16() { return static_cast<int32_t>(offsetof(PathTypesDemo_t3223563063, ___mouseDragStart_16)); }
	inline Vector2_t4282066565  get_mouseDragStart_16() const { return ___mouseDragStart_16; }
	inline Vector2_t4282066565 * get_address_of_mouseDragStart_16() { return &___mouseDragStart_16; }
	inline void set_mouseDragStart_16(Vector2_t4282066565  value)
	{
		___mouseDragStart_16 = value;
	}

	inline static int32_t get_offset_of_mouseDragStartTime_17() { return static_cast<int32_t>(offsetof(PathTypesDemo_t3223563063, ___mouseDragStartTime_17)); }
	inline float get_mouseDragStartTime_17() const { return ___mouseDragStartTime_17; }
	inline float* get_address_of_mouseDragStartTime_17() { return &___mouseDragStartTime_17; }
	inline void set_mouseDragStartTime_17(float value)
	{
		___mouseDragStartTime_17 = value;
	}

	inline static int32_t get_offset_of_lastFlood_18() { return static_cast<int32_t>(offsetof(PathTypesDemo_t3223563063, ___lastFlood_18)); }
	inline FloodPath_t3766979749 * get_lastFlood_18() const { return ___lastFlood_18; }
	inline FloodPath_t3766979749 ** get_address_of_lastFlood_18() { return &___lastFlood_18; }
	inline void set_lastFlood_18(FloodPath_t3766979749 * value)
	{
		___lastFlood_18 = value;
		Il2CppCodeGenWriteBarrier(&___lastFlood_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
