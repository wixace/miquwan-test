﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<CameraShotMoveCfg>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m700903643(__this, ___l0, method) ((  void (*) (Enumerator_t4094718854 *, List_1_t4075046084 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<CameraShotMoveCfg>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2185610903(__this, method) ((  void (*) (Enumerator_t4094718854 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<CameraShotMoveCfg>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1768746243(__this, method) ((  Il2CppObject * (*) (Enumerator_t4094718854 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<CameraShotMoveCfg>::Dispose()
#define Enumerator_Dispose_m3224184192(__this, method) ((  void (*) (Enumerator_t4094718854 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<CameraShotMoveCfg>::VerifyState()
#define Enumerator_VerifyState_m1532110009(__this, method) ((  void (*) (Enumerator_t4094718854 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<CameraShotMoveCfg>::MoveNext()
#define Enumerator_MoveNext_m3552264195(__this, method) ((  bool (*) (Enumerator_t4094718854 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<CameraShotMoveCfg>::get_Current()
#define Enumerator_get_Current_m548054320(__this, method) ((  CameraShotMoveCfg_t2706860532 * (*) (Enumerator_t4094718854 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
