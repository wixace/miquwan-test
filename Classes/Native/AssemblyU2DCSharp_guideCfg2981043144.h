﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// guideCfg
struct  guideCfg_t2981043144  : public CsCfgBase_t69924517
{
public:
	// System.Int32 guideCfg::id
	int32_t ___id_0;
	// System.UInt32 guideCfg::groupId
	uint32_t ___groupId_1;
	// System.Int32 guideCfg::priority
	int32_t ___priority_2;
	// System.Boolean guideCfg::finish
	bool ___finish_3;
	// System.Boolean guideCfg::isLast
	bool ___isLast_4;
	// System.Boolean guideCfg::finishStory
	bool ___finishStory_5;
	// System.Boolean guideCfg::isPauseFB
	bool ___isPauseFB_6;
	// System.Boolean guideCfg::isCloseLock
	bool ___isCloseLock_7;
	// System.Int32 guideCfg::postGuidId
	int32_t ___postGuidId_8;
	// System.Single guideCfg::delay
	float ___delay_9;
	// System.Single guideCfg::delayClose
	float ___delayClose_10;
	// System.Boolean guideCfg::isRelActive
	bool ___isRelActive_11;
	// System.UInt32 guideCfg::frontGroupId
	uint32_t ___frontGroupId_12;
	// System.Int32 guideCfg::frontGuideId
	int32_t ___frontGuideId_13;
	// System.Int32 guideCfg::teamLevel
	int32_t ___teamLevel_14;
	// System.Int32 guideCfg::stayFBId
	int32_t ___stayFBId_15;
	// System.Int32 guideCfg::passFBId
	int32_t ___passFBId_16;
	// System.Int32 guideCfg::taskFnish
	int32_t ___taskFnish_17;
	// System.Int32 guideCfg::npcId
	int32_t ___npcId_18;
	// System.Int32 guideCfg::npcHp
	int32_t ___npcHp_19;
	// System.Int32 guideCfg::enterFBId
	int32_t ___enterFBId_20;
	// System.Int32 guideCfg::upgradeLv
	int32_t ___upgradeLv_21;
	// System.Int32 guideCfg::usingSkillID
	int32_t ___usingSkillID_22;
	// System.Boolean guideCfg::usingHetiji
	bool ___usingHetiji_23;
	// System.Int32 guideCfg::triggerEnemyRound
	int32_t ___triggerEnemyRound_24;
	// System.Boolean guideCfg::isAnger
	bool ___isAnger_25;
	// System.Int32 guideCfg::openPanelId
	int32_t ___openPanelId_26;
	// System.UInt32 guideCfg::msssageResponseId
	uint32_t ___msssageResponseId_27;
	// System.Int32 guideCfg::funType
	int32_t ___funType_28;
	// System.String guideCfg::param1
	String_t* ___param1_29;
	// System.String guideCfg::param2
	String_t* ___param2_30;
	// System.String guideCfg::param3
	String_t* ___param3_31;
	// System.String guideCfg::param4
	String_t* ___param4_32;
	// System.String guideCfg::param5
	String_t* ___param5_33;
	// System.String guideCfg::param6
	String_t* ___param6_34;
	// System.String guideCfg::param7
	String_t* ___param7_35;
	// System.String guideCfg::param8
	String_t* ___param8_36;
	// System.String guideCfg::param9
	String_t* ___param9_37;
	// System.String guideCfg::param10
	String_t* ___param10_38;
	// System.Boolean guideCfg::mouse_move
	bool ___mouse_move_39;
	// System.Boolean guideCfg::NonMandatory
	bool ___NonMandatory_40;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_groupId_1() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___groupId_1)); }
	inline uint32_t get_groupId_1() const { return ___groupId_1; }
	inline uint32_t* get_address_of_groupId_1() { return &___groupId_1; }
	inline void set_groupId_1(uint32_t value)
	{
		___groupId_1 = value;
	}

	inline static int32_t get_offset_of_priority_2() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___priority_2)); }
	inline int32_t get_priority_2() const { return ___priority_2; }
	inline int32_t* get_address_of_priority_2() { return &___priority_2; }
	inline void set_priority_2(int32_t value)
	{
		___priority_2 = value;
	}

	inline static int32_t get_offset_of_finish_3() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___finish_3)); }
	inline bool get_finish_3() const { return ___finish_3; }
	inline bool* get_address_of_finish_3() { return &___finish_3; }
	inline void set_finish_3(bool value)
	{
		___finish_3 = value;
	}

	inline static int32_t get_offset_of_isLast_4() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___isLast_4)); }
	inline bool get_isLast_4() const { return ___isLast_4; }
	inline bool* get_address_of_isLast_4() { return &___isLast_4; }
	inline void set_isLast_4(bool value)
	{
		___isLast_4 = value;
	}

	inline static int32_t get_offset_of_finishStory_5() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___finishStory_5)); }
	inline bool get_finishStory_5() const { return ___finishStory_5; }
	inline bool* get_address_of_finishStory_5() { return &___finishStory_5; }
	inline void set_finishStory_5(bool value)
	{
		___finishStory_5 = value;
	}

	inline static int32_t get_offset_of_isPauseFB_6() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___isPauseFB_6)); }
	inline bool get_isPauseFB_6() const { return ___isPauseFB_6; }
	inline bool* get_address_of_isPauseFB_6() { return &___isPauseFB_6; }
	inline void set_isPauseFB_6(bool value)
	{
		___isPauseFB_6 = value;
	}

	inline static int32_t get_offset_of_isCloseLock_7() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___isCloseLock_7)); }
	inline bool get_isCloseLock_7() const { return ___isCloseLock_7; }
	inline bool* get_address_of_isCloseLock_7() { return &___isCloseLock_7; }
	inline void set_isCloseLock_7(bool value)
	{
		___isCloseLock_7 = value;
	}

	inline static int32_t get_offset_of_postGuidId_8() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___postGuidId_8)); }
	inline int32_t get_postGuidId_8() const { return ___postGuidId_8; }
	inline int32_t* get_address_of_postGuidId_8() { return &___postGuidId_8; }
	inline void set_postGuidId_8(int32_t value)
	{
		___postGuidId_8 = value;
	}

	inline static int32_t get_offset_of_delay_9() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___delay_9)); }
	inline float get_delay_9() const { return ___delay_9; }
	inline float* get_address_of_delay_9() { return &___delay_9; }
	inline void set_delay_9(float value)
	{
		___delay_9 = value;
	}

	inline static int32_t get_offset_of_delayClose_10() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___delayClose_10)); }
	inline float get_delayClose_10() const { return ___delayClose_10; }
	inline float* get_address_of_delayClose_10() { return &___delayClose_10; }
	inline void set_delayClose_10(float value)
	{
		___delayClose_10 = value;
	}

	inline static int32_t get_offset_of_isRelActive_11() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___isRelActive_11)); }
	inline bool get_isRelActive_11() const { return ___isRelActive_11; }
	inline bool* get_address_of_isRelActive_11() { return &___isRelActive_11; }
	inline void set_isRelActive_11(bool value)
	{
		___isRelActive_11 = value;
	}

	inline static int32_t get_offset_of_frontGroupId_12() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___frontGroupId_12)); }
	inline uint32_t get_frontGroupId_12() const { return ___frontGroupId_12; }
	inline uint32_t* get_address_of_frontGroupId_12() { return &___frontGroupId_12; }
	inline void set_frontGroupId_12(uint32_t value)
	{
		___frontGroupId_12 = value;
	}

	inline static int32_t get_offset_of_frontGuideId_13() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___frontGuideId_13)); }
	inline int32_t get_frontGuideId_13() const { return ___frontGuideId_13; }
	inline int32_t* get_address_of_frontGuideId_13() { return &___frontGuideId_13; }
	inline void set_frontGuideId_13(int32_t value)
	{
		___frontGuideId_13 = value;
	}

	inline static int32_t get_offset_of_teamLevel_14() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___teamLevel_14)); }
	inline int32_t get_teamLevel_14() const { return ___teamLevel_14; }
	inline int32_t* get_address_of_teamLevel_14() { return &___teamLevel_14; }
	inline void set_teamLevel_14(int32_t value)
	{
		___teamLevel_14 = value;
	}

	inline static int32_t get_offset_of_stayFBId_15() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___stayFBId_15)); }
	inline int32_t get_stayFBId_15() const { return ___stayFBId_15; }
	inline int32_t* get_address_of_stayFBId_15() { return &___stayFBId_15; }
	inline void set_stayFBId_15(int32_t value)
	{
		___stayFBId_15 = value;
	}

	inline static int32_t get_offset_of_passFBId_16() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___passFBId_16)); }
	inline int32_t get_passFBId_16() const { return ___passFBId_16; }
	inline int32_t* get_address_of_passFBId_16() { return &___passFBId_16; }
	inline void set_passFBId_16(int32_t value)
	{
		___passFBId_16 = value;
	}

	inline static int32_t get_offset_of_taskFnish_17() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___taskFnish_17)); }
	inline int32_t get_taskFnish_17() const { return ___taskFnish_17; }
	inline int32_t* get_address_of_taskFnish_17() { return &___taskFnish_17; }
	inline void set_taskFnish_17(int32_t value)
	{
		___taskFnish_17 = value;
	}

	inline static int32_t get_offset_of_npcId_18() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___npcId_18)); }
	inline int32_t get_npcId_18() const { return ___npcId_18; }
	inline int32_t* get_address_of_npcId_18() { return &___npcId_18; }
	inline void set_npcId_18(int32_t value)
	{
		___npcId_18 = value;
	}

	inline static int32_t get_offset_of_npcHp_19() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___npcHp_19)); }
	inline int32_t get_npcHp_19() const { return ___npcHp_19; }
	inline int32_t* get_address_of_npcHp_19() { return &___npcHp_19; }
	inline void set_npcHp_19(int32_t value)
	{
		___npcHp_19 = value;
	}

	inline static int32_t get_offset_of_enterFBId_20() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___enterFBId_20)); }
	inline int32_t get_enterFBId_20() const { return ___enterFBId_20; }
	inline int32_t* get_address_of_enterFBId_20() { return &___enterFBId_20; }
	inline void set_enterFBId_20(int32_t value)
	{
		___enterFBId_20 = value;
	}

	inline static int32_t get_offset_of_upgradeLv_21() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___upgradeLv_21)); }
	inline int32_t get_upgradeLv_21() const { return ___upgradeLv_21; }
	inline int32_t* get_address_of_upgradeLv_21() { return &___upgradeLv_21; }
	inline void set_upgradeLv_21(int32_t value)
	{
		___upgradeLv_21 = value;
	}

	inline static int32_t get_offset_of_usingSkillID_22() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___usingSkillID_22)); }
	inline int32_t get_usingSkillID_22() const { return ___usingSkillID_22; }
	inline int32_t* get_address_of_usingSkillID_22() { return &___usingSkillID_22; }
	inline void set_usingSkillID_22(int32_t value)
	{
		___usingSkillID_22 = value;
	}

	inline static int32_t get_offset_of_usingHetiji_23() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___usingHetiji_23)); }
	inline bool get_usingHetiji_23() const { return ___usingHetiji_23; }
	inline bool* get_address_of_usingHetiji_23() { return &___usingHetiji_23; }
	inline void set_usingHetiji_23(bool value)
	{
		___usingHetiji_23 = value;
	}

	inline static int32_t get_offset_of_triggerEnemyRound_24() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___triggerEnemyRound_24)); }
	inline int32_t get_triggerEnemyRound_24() const { return ___triggerEnemyRound_24; }
	inline int32_t* get_address_of_triggerEnemyRound_24() { return &___triggerEnemyRound_24; }
	inline void set_triggerEnemyRound_24(int32_t value)
	{
		___triggerEnemyRound_24 = value;
	}

	inline static int32_t get_offset_of_isAnger_25() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___isAnger_25)); }
	inline bool get_isAnger_25() const { return ___isAnger_25; }
	inline bool* get_address_of_isAnger_25() { return &___isAnger_25; }
	inline void set_isAnger_25(bool value)
	{
		___isAnger_25 = value;
	}

	inline static int32_t get_offset_of_openPanelId_26() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___openPanelId_26)); }
	inline int32_t get_openPanelId_26() const { return ___openPanelId_26; }
	inline int32_t* get_address_of_openPanelId_26() { return &___openPanelId_26; }
	inline void set_openPanelId_26(int32_t value)
	{
		___openPanelId_26 = value;
	}

	inline static int32_t get_offset_of_msssageResponseId_27() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___msssageResponseId_27)); }
	inline uint32_t get_msssageResponseId_27() const { return ___msssageResponseId_27; }
	inline uint32_t* get_address_of_msssageResponseId_27() { return &___msssageResponseId_27; }
	inline void set_msssageResponseId_27(uint32_t value)
	{
		___msssageResponseId_27 = value;
	}

	inline static int32_t get_offset_of_funType_28() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___funType_28)); }
	inline int32_t get_funType_28() const { return ___funType_28; }
	inline int32_t* get_address_of_funType_28() { return &___funType_28; }
	inline void set_funType_28(int32_t value)
	{
		___funType_28 = value;
	}

	inline static int32_t get_offset_of_param1_29() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___param1_29)); }
	inline String_t* get_param1_29() const { return ___param1_29; }
	inline String_t** get_address_of_param1_29() { return &___param1_29; }
	inline void set_param1_29(String_t* value)
	{
		___param1_29 = value;
		Il2CppCodeGenWriteBarrier(&___param1_29, value);
	}

	inline static int32_t get_offset_of_param2_30() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___param2_30)); }
	inline String_t* get_param2_30() const { return ___param2_30; }
	inline String_t** get_address_of_param2_30() { return &___param2_30; }
	inline void set_param2_30(String_t* value)
	{
		___param2_30 = value;
		Il2CppCodeGenWriteBarrier(&___param2_30, value);
	}

	inline static int32_t get_offset_of_param3_31() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___param3_31)); }
	inline String_t* get_param3_31() const { return ___param3_31; }
	inline String_t** get_address_of_param3_31() { return &___param3_31; }
	inline void set_param3_31(String_t* value)
	{
		___param3_31 = value;
		Il2CppCodeGenWriteBarrier(&___param3_31, value);
	}

	inline static int32_t get_offset_of_param4_32() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___param4_32)); }
	inline String_t* get_param4_32() const { return ___param4_32; }
	inline String_t** get_address_of_param4_32() { return &___param4_32; }
	inline void set_param4_32(String_t* value)
	{
		___param4_32 = value;
		Il2CppCodeGenWriteBarrier(&___param4_32, value);
	}

	inline static int32_t get_offset_of_param5_33() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___param5_33)); }
	inline String_t* get_param5_33() const { return ___param5_33; }
	inline String_t** get_address_of_param5_33() { return &___param5_33; }
	inline void set_param5_33(String_t* value)
	{
		___param5_33 = value;
		Il2CppCodeGenWriteBarrier(&___param5_33, value);
	}

	inline static int32_t get_offset_of_param6_34() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___param6_34)); }
	inline String_t* get_param6_34() const { return ___param6_34; }
	inline String_t** get_address_of_param6_34() { return &___param6_34; }
	inline void set_param6_34(String_t* value)
	{
		___param6_34 = value;
		Il2CppCodeGenWriteBarrier(&___param6_34, value);
	}

	inline static int32_t get_offset_of_param7_35() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___param7_35)); }
	inline String_t* get_param7_35() const { return ___param7_35; }
	inline String_t** get_address_of_param7_35() { return &___param7_35; }
	inline void set_param7_35(String_t* value)
	{
		___param7_35 = value;
		Il2CppCodeGenWriteBarrier(&___param7_35, value);
	}

	inline static int32_t get_offset_of_param8_36() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___param8_36)); }
	inline String_t* get_param8_36() const { return ___param8_36; }
	inline String_t** get_address_of_param8_36() { return &___param8_36; }
	inline void set_param8_36(String_t* value)
	{
		___param8_36 = value;
		Il2CppCodeGenWriteBarrier(&___param8_36, value);
	}

	inline static int32_t get_offset_of_param9_37() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___param9_37)); }
	inline String_t* get_param9_37() const { return ___param9_37; }
	inline String_t** get_address_of_param9_37() { return &___param9_37; }
	inline void set_param9_37(String_t* value)
	{
		___param9_37 = value;
		Il2CppCodeGenWriteBarrier(&___param9_37, value);
	}

	inline static int32_t get_offset_of_param10_38() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___param10_38)); }
	inline String_t* get_param10_38() const { return ___param10_38; }
	inline String_t** get_address_of_param10_38() { return &___param10_38; }
	inline void set_param10_38(String_t* value)
	{
		___param10_38 = value;
		Il2CppCodeGenWriteBarrier(&___param10_38, value);
	}

	inline static int32_t get_offset_of_mouse_move_39() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___mouse_move_39)); }
	inline bool get_mouse_move_39() const { return ___mouse_move_39; }
	inline bool* get_address_of_mouse_move_39() { return &___mouse_move_39; }
	inline void set_mouse_move_39(bool value)
	{
		___mouse_move_39 = value;
	}

	inline static int32_t get_offset_of_NonMandatory_40() { return static_cast<int32_t>(offsetof(guideCfg_t2981043144, ___NonMandatory_40)); }
	inline bool get_NonMandatory_40() const { return ___NonMandatory_40; }
	inline bool* get_address_of_NonMandatory_40() { return &___NonMandatory_40; }
	inline void set_NonMandatory_40(bool value)
	{
		___NonMandatory_40 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
