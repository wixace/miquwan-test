﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3542505350.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Pathfinding_AdvancedSmooth_Turn465195378.h"

// System.Void System.Array/InternalEnumerator`1<Pathfinding.AdvancedSmooth/Turn>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1425305847_gshared (InternalEnumerator_1_t3542505350 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1425305847(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3542505350 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1425305847_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4123630409_gshared (InternalEnumerator_1_t3542505350 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4123630409(__this, method) ((  void (*) (InternalEnumerator_1_t3542505350 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4123630409_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1858259199_gshared (InternalEnumerator_1_t3542505350 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1858259199(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3542505350 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1858259199_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.AdvancedSmooth/Turn>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2640671630_gshared (InternalEnumerator_1_t3542505350 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2640671630(__this, method) ((  void (*) (InternalEnumerator_1_t3542505350 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2640671630_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.AdvancedSmooth/Turn>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3717725049_gshared (InternalEnumerator_1_t3542505350 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3717725049(__this, method) ((  bool (*) (InternalEnumerator_1_t3542505350 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3717725049_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Pathfinding.AdvancedSmooth/Turn>::get_Current()
extern "C"  Turn_t465195378  InternalEnumerator_1_get_Current_m2137896672_gshared (InternalEnumerator_1_t3542505350 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2137896672(__this, method) ((  Turn_t465195378  (*) (InternalEnumerator_1_t3542505350 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2137896672_gshared)(__this, method)
