﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Nullable_1_gen560925241.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSCache/TypeInfo
struct  TypeInfo_t3198813374  : public Il2CppObject
{
public:
	// System.Type JSCache/TypeInfo::type
	Type_t * ___type_0;
	// System.Nullable`1<System.Boolean> JSCache/TypeInfo::isValueType
	Nullable_1_t560925241  ___isValueType_1;
	// System.Nullable`1<System.Boolean> JSCache/TypeInfo::isClass
	Nullable_1_t560925241  ___isClass_2;
	// System.Nullable`1<System.Boolean> JSCache/TypeInfo::isDelegate
	Nullable_1_t560925241  ___isDelegate_3;
	// System.Nullable`1<System.Boolean> JSCache/TypeInfo::isCSMonoBehaviour
	Nullable_1_t560925241  ___isCSMonoBehaviour_4;
	// System.String JSCache/TypeInfo::jsTypeFullName
	String_t* ___jsTypeFullName_5;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(TypeInfo_t3198813374, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier(&___type_0, value);
	}

	inline static int32_t get_offset_of_isValueType_1() { return static_cast<int32_t>(offsetof(TypeInfo_t3198813374, ___isValueType_1)); }
	inline Nullable_1_t560925241  get_isValueType_1() const { return ___isValueType_1; }
	inline Nullable_1_t560925241 * get_address_of_isValueType_1() { return &___isValueType_1; }
	inline void set_isValueType_1(Nullable_1_t560925241  value)
	{
		___isValueType_1 = value;
	}

	inline static int32_t get_offset_of_isClass_2() { return static_cast<int32_t>(offsetof(TypeInfo_t3198813374, ___isClass_2)); }
	inline Nullable_1_t560925241  get_isClass_2() const { return ___isClass_2; }
	inline Nullable_1_t560925241 * get_address_of_isClass_2() { return &___isClass_2; }
	inline void set_isClass_2(Nullable_1_t560925241  value)
	{
		___isClass_2 = value;
	}

	inline static int32_t get_offset_of_isDelegate_3() { return static_cast<int32_t>(offsetof(TypeInfo_t3198813374, ___isDelegate_3)); }
	inline Nullable_1_t560925241  get_isDelegate_3() const { return ___isDelegate_3; }
	inline Nullable_1_t560925241 * get_address_of_isDelegate_3() { return &___isDelegate_3; }
	inline void set_isDelegate_3(Nullable_1_t560925241  value)
	{
		___isDelegate_3 = value;
	}

	inline static int32_t get_offset_of_isCSMonoBehaviour_4() { return static_cast<int32_t>(offsetof(TypeInfo_t3198813374, ___isCSMonoBehaviour_4)); }
	inline Nullable_1_t560925241  get_isCSMonoBehaviour_4() const { return ___isCSMonoBehaviour_4; }
	inline Nullable_1_t560925241 * get_address_of_isCSMonoBehaviour_4() { return &___isCSMonoBehaviour_4; }
	inline void set_isCSMonoBehaviour_4(Nullable_1_t560925241  value)
	{
		___isCSMonoBehaviour_4 = value;
	}

	inline static int32_t get_offset_of_jsTypeFullName_5() { return static_cast<int32_t>(offsetof(TypeInfo_t3198813374, ___jsTypeFullName_5)); }
	inline String_t* get_jsTypeFullName_5() const { return ___jsTypeFullName_5; }
	inline String_t** get_address_of_jsTypeFullName_5() { return &___jsTypeFullName_5; }
	inline void set_jsTypeFullName_5(String_t* value)
	{
		___jsTypeFullName_5 = value;
		Il2CppCodeGenWriteBarrier(&___jsTypeFullName_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
