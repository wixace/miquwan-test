﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.MineBotAI
struct MineBotAI_t1861981950;
// AIPath
struct AIPath_t1930792045;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_AIPath1930792045.h"
#include "AssemblyU2DCSharp_Pathfinding_MineBotAI1861981950.h"

// System.Void Pathfinding.MineBotAI::.ctor()
extern "C"  void MineBotAI__ctor_m2907887609 (MineBotAI_t1861981950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MineBotAI::Start()
extern "C"  void MineBotAI_Start_m1855025401 (MineBotAI_t1861981950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MineBotAI::OnTargetReached()
extern "C"  void MineBotAI_OnTargetReached_m3295679481 (MineBotAI_t1861981950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.MineBotAI::GetFeetPosition()
extern "C"  Vector3_t4282066566  MineBotAI_GetFeetPosition_m485150014 (MineBotAI_t1861981950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MineBotAI::Update()
extern "C"  void MineBotAI_Update_m1677064756 (MineBotAI_t1861981950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MineBotAI::ilo_Start1(AIPath)
extern "C"  void MineBotAI_ilo_Start1_m2785829984 (Il2CppObject * __this /* static, unused */, AIPath_t1930792045 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.MineBotAI::ilo_GetFeetPosition2(Pathfinding.MineBotAI)
extern "C"  Vector3_t4282066566  MineBotAI_ilo_GetFeetPosition2_m1499511127 (Il2CppObject * __this /* static, unused */, MineBotAI_t1861981950 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
