﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.GraphUpdateObject
struct GraphUpdateObject_t430843704;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_IntRect3015058261.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.NavMeshGraph/<UpdateArea>c__AnonStorey118
struct  U3CUpdateAreaU3Ec__AnonStorey118_t3715919972  : public Il2CppObject
{
public:
	// Pathfinding.IntRect Pathfinding.NavMeshGraph/<UpdateArea>c__AnonStorey118::r2
	IntRect_t3015058261  ___r2_0;
	// UnityEngine.Rect Pathfinding.NavMeshGraph/<UpdateArea>c__AnonStorey118::r
	Rect_t4241904616  ___r_1;
	// Pathfinding.Int3 Pathfinding.NavMeshGraph/<UpdateArea>c__AnonStorey118::a
	Int3_t1974045594  ___a_2;
	// Pathfinding.Int3 Pathfinding.NavMeshGraph/<UpdateArea>c__AnonStorey118::b
	Int3_t1974045594  ___b_3;
	// Pathfinding.Int3 Pathfinding.NavMeshGraph/<UpdateArea>c__AnonStorey118::c
	Int3_t1974045594  ___c_4;
	// Pathfinding.Int3 Pathfinding.NavMeshGraph/<UpdateArea>c__AnonStorey118::d
	Int3_t1974045594  ___d_5;
	// Pathfinding.Int3 Pathfinding.NavMeshGraph/<UpdateArea>c__AnonStorey118::ia
	Int3_t1974045594  ___ia_6;
	// Pathfinding.Int3 Pathfinding.NavMeshGraph/<UpdateArea>c__AnonStorey118::ib
	Int3_t1974045594  ___ib_7;
	// Pathfinding.Int3 Pathfinding.NavMeshGraph/<UpdateArea>c__AnonStorey118::ic
	Int3_t1974045594  ___ic_8;
	// Pathfinding.Int3 Pathfinding.NavMeshGraph/<UpdateArea>c__AnonStorey118::id
	Int3_t1974045594  ___id_9;
	// Pathfinding.GraphUpdateObject Pathfinding.NavMeshGraph/<UpdateArea>c__AnonStorey118::o
	GraphUpdateObject_t430843704 * ___o_10;

public:
	inline static int32_t get_offset_of_r2_0() { return static_cast<int32_t>(offsetof(U3CUpdateAreaU3Ec__AnonStorey118_t3715919972, ___r2_0)); }
	inline IntRect_t3015058261  get_r2_0() const { return ___r2_0; }
	inline IntRect_t3015058261 * get_address_of_r2_0() { return &___r2_0; }
	inline void set_r2_0(IntRect_t3015058261  value)
	{
		___r2_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(U3CUpdateAreaU3Ec__AnonStorey118_t3715919972, ___r_1)); }
	inline Rect_t4241904616  get_r_1() const { return ___r_1; }
	inline Rect_t4241904616 * get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(Rect_t4241904616  value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_a_2() { return static_cast<int32_t>(offsetof(U3CUpdateAreaU3Ec__AnonStorey118_t3715919972, ___a_2)); }
	inline Int3_t1974045594  get_a_2() const { return ___a_2; }
	inline Int3_t1974045594 * get_address_of_a_2() { return &___a_2; }
	inline void set_a_2(Int3_t1974045594  value)
	{
		___a_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(U3CUpdateAreaU3Ec__AnonStorey118_t3715919972, ___b_3)); }
	inline Int3_t1974045594  get_b_3() const { return ___b_3; }
	inline Int3_t1974045594 * get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(Int3_t1974045594  value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_c_4() { return static_cast<int32_t>(offsetof(U3CUpdateAreaU3Ec__AnonStorey118_t3715919972, ___c_4)); }
	inline Int3_t1974045594  get_c_4() const { return ___c_4; }
	inline Int3_t1974045594 * get_address_of_c_4() { return &___c_4; }
	inline void set_c_4(Int3_t1974045594  value)
	{
		___c_4 = value;
	}

	inline static int32_t get_offset_of_d_5() { return static_cast<int32_t>(offsetof(U3CUpdateAreaU3Ec__AnonStorey118_t3715919972, ___d_5)); }
	inline Int3_t1974045594  get_d_5() const { return ___d_5; }
	inline Int3_t1974045594 * get_address_of_d_5() { return &___d_5; }
	inline void set_d_5(Int3_t1974045594  value)
	{
		___d_5 = value;
	}

	inline static int32_t get_offset_of_ia_6() { return static_cast<int32_t>(offsetof(U3CUpdateAreaU3Ec__AnonStorey118_t3715919972, ___ia_6)); }
	inline Int3_t1974045594  get_ia_6() const { return ___ia_6; }
	inline Int3_t1974045594 * get_address_of_ia_6() { return &___ia_6; }
	inline void set_ia_6(Int3_t1974045594  value)
	{
		___ia_6 = value;
	}

	inline static int32_t get_offset_of_ib_7() { return static_cast<int32_t>(offsetof(U3CUpdateAreaU3Ec__AnonStorey118_t3715919972, ___ib_7)); }
	inline Int3_t1974045594  get_ib_7() const { return ___ib_7; }
	inline Int3_t1974045594 * get_address_of_ib_7() { return &___ib_7; }
	inline void set_ib_7(Int3_t1974045594  value)
	{
		___ib_7 = value;
	}

	inline static int32_t get_offset_of_ic_8() { return static_cast<int32_t>(offsetof(U3CUpdateAreaU3Ec__AnonStorey118_t3715919972, ___ic_8)); }
	inline Int3_t1974045594  get_ic_8() const { return ___ic_8; }
	inline Int3_t1974045594 * get_address_of_ic_8() { return &___ic_8; }
	inline void set_ic_8(Int3_t1974045594  value)
	{
		___ic_8 = value;
	}

	inline static int32_t get_offset_of_id_9() { return static_cast<int32_t>(offsetof(U3CUpdateAreaU3Ec__AnonStorey118_t3715919972, ___id_9)); }
	inline Int3_t1974045594  get_id_9() const { return ___id_9; }
	inline Int3_t1974045594 * get_address_of_id_9() { return &___id_9; }
	inline void set_id_9(Int3_t1974045594  value)
	{
		___id_9 = value;
	}

	inline static int32_t get_offset_of_o_10() { return static_cast<int32_t>(offsetof(U3CUpdateAreaU3Ec__AnonStorey118_t3715919972, ___o_10)); }
	inline GraphUpdateObject_t430843704 * get_o_10() const { return ___o_10; }
	inline GraphUpdateObject_t430843704 ** get_address_of_o_10() { return &___o_10; }
	inline void set_o_10(GraphUpdateObject_t430843704 * value)
	{
		___o_10 = value;
		Il2CppCodeGenWriteBarrier(&___o_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
