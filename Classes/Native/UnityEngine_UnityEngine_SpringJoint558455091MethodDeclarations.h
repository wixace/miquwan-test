﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.SpringJoint
struct SpringJoint_t558455091;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.SpringJoint::.ctor()
extern "C"  void SpringJoint__ctor_m443280764 (SpringJoint_t558455091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.SpringJoint::get_spring()
extern "C"  float SpringJoint_get_spring_m3593829618 (SpringJoint_t558455091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpringJoint::set_spring(System.Single)
extern "C"  void SpringJoint_set_spring_m3720718841 (SpringJoint_t558455091 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.SpringJoint::get_damper()
extern "C"  float SpringJoint_get_damper_m2656886386 (SpringJoint_t558455091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpringJoint::set_damper(System.Single)
extern "C"  void SpringJoint_set_damper_m3309877369 (SpringJoint_t558455091 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.SpringJoint::get_minDistance()
extern "C"  float SpringJoint_get_minDistance_m2634841348 (SpringJoint_t558455091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpringJoint::set_minDistance(System.Single)
extern "C"  void SpringJoint_set_minDistance_m1300381479 (SpringJoint_t558455091 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.SpringJoint::get_maxDistance()
extern "C"  float SpringJoint_get_maxDistance_m812535190 (SpringJoint_t558455091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpringJoint::set_maxDistance(System.Single)
extern "C"  void SpringJoint_set_maxDistance_m68119509 (SpringJoint_t558455091 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.SpringJoint::get_tolerance()
extern "C"  float SpringJoint_get_tolerance_m256187946 (SpringJoint_t558455091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpringJoint::set_tolerance(System.Single)
extern "C"  void SpringJoint_set_tolerance_m2731551681 (SpringJoint_t558455091 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
