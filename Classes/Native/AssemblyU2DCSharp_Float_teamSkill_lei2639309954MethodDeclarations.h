﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_teamSkill_lei
struct Float_teamSkill_lei_t2639309954;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.String
struct String_t;
// FloatTextUnit
struct FloatTextUnit_t2362298029;
// EventDelegate/Callback
struct Callback_t1094463061;
// UISprite
struct UISprite_t661437049;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "AssemblyU2DCSharp_UITweener_Method1078127180.h"
#include "AssemblyU2DCSharp_UISprite661437049.h"
#include "mscorlib_System_String7231557.h"

// System.Void Float_teamSkill_lei::.ctor()
extern "C"  void Float_teamSkill_lei__ctor_m1679448361 (Float_teamSkill_lei_t2639309954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_teamSkill_lei::OnAwake(UnityEngine.GameObject)
extern "C"  void Float_teamSkill_lei_OnAwake_m3266154981 (Float_teamSkill_lei_t2639309954 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_teamSkill_lei::OnDestroy()
extern "C"  void Float_teamSkill_lei_OnDestroy_m1062826402 (Float_teamSkill_lei_t2639309954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_teamSkill_lei::FloatID()
extern "C"  int32_t Float_teamSkill_lei_FloatID_m2709388647 (Float_teamSkill_lei_t2639309954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_teamSkill_lei::Init()
extern "C"  void Float_teamSkill_lei_Init_m559901707 (Float_teamSkill_lei_t2639309954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_teamSkill_lei::SetFile(System.Object[])
extern "C"  void Float_teamSkill_lei_SetFile_m48013005 (Float_teamSkill_lei_t2639309954 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Float_teamSkill_lei::getSpriteName(System.Int32)
extern "C"  String_t* Float_teamSkill_lei_getSpriteName_m248393665 (Float_teamSkill_lei_t2639309954 * __this, int32_t ___lv0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_teamSkill_lei::ilo_OnAwake1(FloatTextUnit,UnityEngine.GameObject)
extern "C"  void Float_teamSkill_lei_ilo_OnAwake1_m327519090 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, GameObject_t3674682005 * ___viewGO1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_teamSkill_lei::ilo_MoveScale2(FloatTextUnit,System.Single,System.Single,System.Single,System.Single,EventDelegate/Callback,UnityEngine.GameObject,UITweener/Method)
extern "C"  void Float_teamSkill_lei_ilo_MoveScale2_m152494075 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, float ___from1, float ___to2, float ___duration3, float ___delay4, Callback_t1094463061 * ___call5, GameObject_t3674682005 * ___go6, int32_t ___method7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_teamSkill_lei::ilo_set_spriteName3(UISprite,System.String)
extern "C"  void Float_teamSkill_lei_ilo_set_spriteName3_m129222045 (Il2CppObject * __this /* static, unused */, UISprite_t661437049 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
