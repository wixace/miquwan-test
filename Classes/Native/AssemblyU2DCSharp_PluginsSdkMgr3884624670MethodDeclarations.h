﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t2362096582;
// System.String
struct String_t;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Action
struct Action_t3771233898;
// VersionInfo
struct VersionInfo_t2356638086;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginReYun
struct PluginReYun_t2552860172;
// PluginXiaomi
struct PluginXiaomi_t2004955630;
// PluginXuegao
struct PluginXuegao_t2016148992;
// PluginWanmi
struct PluginWanmi_t2557378541;
// PluginMihua
struct PluginMihua_t2548376133;
// PluginVivo
struct PluginVivo_t1606494879;
// Plugin360
struct Plugin360_t190334458;
// PluginFlyme
struct PluginFlyme_t2542016952;
// PluginOasis
struct PluginOasis_t2549995064;
// PluginAiWan
struct PluginAiWan_t2537276937;
// PluginZhiquyou
struct PluginZhiquyou_t1151299027;
// PluginDiYibo
struct PluginDiYibo_t1432128181;
// PluginR2
struct PluginR2_t2499992755;
// PluginYouDong
struct PluginYouDong_t3100552560;
// PluginYouwo
struct PluginYouwo_t2559649700;
// PluginMiquwan
struct PluginMiquwan_t866441041;
// PluginDianZhi
struct PluginDianZhi_t1453829974;
// PluginShoumeng
struct PluginShoumeng_t2011739967;
// PluginXiongmaowan
struct PluginXiongmaowan_t3347898093;
// PluginOMi
struct PluginOMi_t190362136;
// PluginQiZi
struct PluginQiZi_t1606345050;
// PluginShoumeng_A1
struct PluginShoumeng_A1_t4066769168;
// PluginYinHu
struct PluginYinHu_t2559462776;
// PluginNewMiquwan
struct PluginNewMiquwan_t2470753303;
// PluginQuWan
struct PluginQuWan_t2552410765;
// PluginTT
struct PluginTT_t2499992851;
// PluginGameFan
struct PluginGameFan_t3898102510;
// PluginJuFengJQ
struct PluginJuFengJQ_t970421149;
// PluginGuangdiantong
struct PluginGuangdiantong_t1625076095;
// PluginHongYou
struct PluginHongYou_t892448812;
// PluginOppo
struct PluginOppo_t1606292883;
// PluginQuick
struct PluginQuick_t2552428122;
// PluginZxhy
struct PluginZxhy_t1606628034;
// PluginJingang
struct PluginJingang_t2495688911;
// PluginIqiyi
struct PluginIqiyi_t2544921470;
// PluginYouMa
struct PluginYouMa_t2559648384;
// PluginYiwanSuper
struct PluginYiwanSuper_t2354859450;
// PluginHongyouNY
struct PluginHongyouNY_t2974371607;
// PluginInFox
struct PluginInFox_t2544798167;
// PluginFengqi
struct PluginFengqi_t1486316547;
// PluginYueplay
struct PluginYueplay_t3258858666;
// PluginGamecat
struct PluginGamecat_t3898130385;
// PluginJingqi
struct PluginJingqi_t1604527235;
// PluginJuliang
struct PluginJuliang_t2837451263;
// PluginCaoHua
struct PluginCaoHua_t1396735126;
// PluginFastPlay
struct PluginFastPlay_t3320186403;
// PluginYouxiang
struct PluginYouxiang_t1675681021;
// PluginQiangWan
struct PluginQiangWan_t3301124917;
// PluginXima
struct PluginXima_t1606554168;
// PluginWanYu
struct PluginWanYu_t2557377933;
// PluginXiaoMai
struct PluginXiaoMai_t2024051491;
// PluginZongYou
struct PluginZongYou_t3982613182;
// PluginHuoShu
struct PluginHuoShu_t1558361489;
// PluginGuoPan
struct PluginGuoPan_t1529729231;
// PluginAiLeXinYou
struct PluginAiLeXinYou_t3790076438;
// PluginSjoy
struct PluginSjoy_t1606406260;
// PluginYuewan
struct PluginYuewan_t2044793518;
// PluginFuncheer
struct PluginFuncheer_t3732253601;
// PluginChangku
struct PluginChangku_t537709740;
// PluginZhangYu
struct PluginZhangYu_t3770424661;
// PluginYuLe
struct PluginYuLe_t1606594472;
// PluginPaoJiao
struct PluginPaoJiao_t3296777368;
// PluginDolphin
struct PluginDolphin_t1635836683;
// PluginYiJie
struct PluginYiJie_t2559429187;
// PluginJinLi
struct PluginJinLi_t2545610073;
// PluginUC
struct PluginUC_t2499992865;
// PluginMoHe
struct PluginMoHe_t1606231090;
// PluginAnfeng
struct PluginAnfeng_t1351242136;
// PluginNewMH
struct PluginNewMH_t2549193640;
// PluginXiaoQi
struct PluginXiaoQi_t2004954762;
// TimeMgr
struct TimeMgr_t350708715;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AndroidJavaObject2362096582.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr_SDK2113571241.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "mscorlib_System_String7231557.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_PushType1840642452.h"
#include "AssemblyU2DCSharp_Gender2129321697.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_PluginReYun2552860172.h"
#include "AssemblyU2DCSharp_PluginXiaomi2004955630.h"
#include "AssemblyU2DCSharp_PluginXuegao2016148992.h"
#include "AssemblyU2DCSharp_PluginWanmi2557378541.h"
#include "AssemblyU2DCSharp_PluginMihua2548376133.h"
#include "AssemblyU2DCSharp_PluginVivo1606494879.h"
#include "AssemblyU2DCSharp_Plugin360190334458.h"
#include "AssemblyU2DCSharp_PluginFlyme2542016952.h"
#include "AssemblyU2DCSharp_PluginOasis2549995064.h"
#include "AssemblyU2DCSharp_PluginAiWan2537276937.h"
#include "AssemblyU2DCSharp_PluginZhiquyou1151299027.h"
#include "AssemblyU2DCSharp_PluginDiYibo1432128181.h"
#include "AssemblyU2DCSharp_PluginR22499992755.h"
#include "AssemblyU2DCSharp_PluginYouDong3100552560.h"
#include "AssemblyU2DCSharp_PluginYouwo2559649700.h"
#include "AssemblyU2DCSharp_PluginMiquwan866441041.h"
#include "AssemblyU2DCSharp_PluginDianZhi1453829974.h"
#include "AssemblyU2DCSharp_PluginShoumeng2011739967.h"
#include "AssemblyU2DCSharp_PluginXiongmaowan3347898093.h"
#include "AssemblyU2DCSharp_PluginOMi190362136.h"
#include "AssemblyU2DCSharp_PluginQiZi1606345050.h"
#include "AssemblyU2DCSharp_PluginShoumeng_A14066769168.h"
#include "AssemblyU2DCSharp_PluginYinHu2559462776.h"
#include "AssemblyU2DCSharp_PluginNewMiquwan2470753303.h"
#include "AssemblyU2DCSharp_PluginQuWan2552410765.h"
#include "AssemblyU2DCSharp_PluginTT2499992851.h"
#include "AssemblyU2DCSharp_PluginGameFan3898102510.h"
#include "AssemblyU2DCSharp_PluginJuFengJQ970421149.h"
#include "AssemblyU2DCSharp_PluginGuangdiantong1625076095.h"
#include "AssemblyU2DCSharp_PluginHongYou892448812.h"
#include "AssemblyU2DCSharp_PluginOppo1606292883.h"
#include "AssemblyU2DCSharp_PluginQuick2552428122.h"
#include "AssemblyU2DCSharp_PluginZxhy1606628034.h"
#include "AssemblyU2DCSharp_PluginJingang2495688911.h"
#include "AssemblyU2DCSharp_PluginIqiyi2544921470.h"
#include "AssemblyU2DCSharp_PluginYouMa2559648384.h"
#include "AssemblyU2DCSharp_PluginYiwanSuper2354859450.h"
#include "AssemblyU2DCSharp_PluginHongyouNY2974371607.h"
#include "AssemblyU2DCSharp_PluginInFox2544798167.h"
#include "AssemblyU2DCSharp_PluginFengqi1486316547.h"
#include "AssemblyU2DCSharp_PluginYueplay3258858666.h"
#include "AssemblyU2DCSharp_PluginGamecat3898130385.h"
#include "AssemblyU2DCSharp_PluginJingqi1604527235.h"
#include "AssemblyU2DCSharp_PluginJuliang2837451263.h"
#include "AssemblyU2DCSharp_PluginCaoHua1396735126.h"
#include "AssemblyU2DCSharp_PluginFastPlay3320186403.h"
#include "AssemblyU2DCSharp_PluginYouxiang1675681021.h"
#include "AssemblyU2DCSharp_PluginQiangWan3301124917.h"
#include "AssemblyU2DCSharp_PluginXima1606554168.h"
#include "AssemblyU2DCSharp_PluginWanYu2557377933.h"
#include "AssemblyU2DCSharp_PluginXiaoMai2024051491.h"
#include "AssemblyU2DCSharp_PluginZongYou3982613182.h"
#include "AssemblyU2DCSharp_PluginHuoShu1558361489.h"
#include "AssemblyU2DCSharp_PluginGuoPan1529729231.h"
#include "AssemblyU2DCSharp_PluginAiLeXinYou3790076438.h"
#include "AssemblyU2DCSharp_PluginSjoy1606406260.h"
#include "AssemblyU2DCSharp_PluginYuewan2044793518.h"
#include "AssemblyU2DCSharp_PluginFuncheer3732253601.h"
#include "AssemblyU2DCSharp_PluginChangku537709740.h"
#include "AssemblyU2DCSharp_PluginZhangYu3770424661.h"
#include "AssemblyU2DCSharp_PluginYuLe1606594472.h"
#include "AssemblyU2DCSharp_PluginPaoJiao3296777368.h"
#include "AssemblyU2DCSharp_PluginDolphin1635836683.h"
#include "AssemblyU2DCSharp_PluginYiJie2559429187.h"
#include "AssemblyU2DCSharp_PluginJinLi2545610073.h"
#include "AssemblyU2DCSharp_PluginUC2499992865.h"
#include "AssemblyU2DCSharp_PluginMoHe1606231090.h"
#include "AssemblyU2DCSharp_PluginAnfeng1351242136.h"
#include "AssemblyU2DCSharp_PluginNewMH2549193640.h"
#include "AssemblyU2DCSharp_PluginXiaoQi2004954762.h"
#include "AssemblyU2DCSharp_TimeMgr350708715.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void PluginsSdkMgr::.ctor()
extern "C"  void PluginsSdkMgr__ctor_m2308659213 (PluginsSdkMgr_t3884624670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AndroidJavaObject PluginsSdkMgr::get_androidJavaObject()
extern "C"  AndroidJavaObject_t2362096582 * PluginsSdkMgr_get_androidJavaObject_m596477356 (PluginsSdkMgr_t3884624670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::set_androidJavaObject(UnityEngine.AndroidJavaObject)
extern "C"  void PluginsSdkMgr_set_androidJavaObject_m3184791131 (PluginsSdkMgr_t3884624670 * __this, AndroidJavaObject_t2362096582 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginsSdkMgr::get_channelId()
extern "C"  String_t* PluginsSdkMgr_get_channelId_m4213923011 (PluginsSdkMgr_t3884624670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginsSdkMgr::get_DeviceID()
extern "C"  String_t* PluginsSdkMgr_get_DeviceID_m3732659342 (PluginsSdkMgr_t3884624670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginsSdkMgr::get_Instance()
extern "C"  PluginsSdkMgr_t3884624670 * PluginsSdkMgr_get_Instance_m2747725856 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::Awake()
extern "C"  void PluginsSdkMgr_Awake_m2546264432 (PluginsSdkMgr_t3884624670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::OnDestory()
extern "C"  void PluginsSdkMgr_OnDestory_m2895082784 (PluginsSdkMgr_t3884624670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::GetSdkStatus(PluginsSdkMgr/SDK,Newtonsoft.Json.Linq.JObject)
extern "C"  bool PluginsSdkMgr_GetSdkStatus_m4060061414 (PluginsSdkMgr_t3884624670 * __this, int32_t ___sdk0, JObject_t1798544199 * ___jobj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginsSdkMgr::GetSdkVSNum(PluginsSdkMgr/SDK)
extern "C"  String_t* PluginsSdkMgr_GetSdkVSNum_m1677669366 (PluginsSdkMgr_t3884624670 * __this, int32_t ___sdk0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::GetMainSdkStatus(PluginsSdkMgr/SDK,Newtonsoft.Json.Linq.JObject)
extern "C"  bool PluginsSdkMgr_GetMainSdkStatus_m2623723821 (PluginsSdkMgr_t3884624670 * __this, int32_t ___sdk0, JObject_t1798544199 * ___jobj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::Init()
extern "C"  void PluginsSdkMgr_Init_m303104167 (PluginsSdkMgr_t3884624670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::LoadLogo()
extern "C"  void PluginsSdkMgr_LoadLogo_m3488569352 (PluginsSdkMgr_t3884624670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::HideLogo()
extern "C"  void PluginsSdkMgr_HideLogo_m4055415652 (PluginsSdkMgr_t3884624670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::isAutoRelogin()
extern "C"  bool PluginsSdkMgr_isAutoRelogin_m3772833948 (PluginsSdkMgr_t3884624670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ReqSDKHttpLogin()
extern "C"  void PluginsSdkMgr_ReqSDKHttpLogin_m234532816 (PluginsSdkMgr_t3884624670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ReqHttpLogin(System.String,System.String)
extern "C"  void PluginsSdkMgr_ReqHttpLogin_m2633327076 (PluginsSdkMgr_t3884624670 * __this, String_t* ___username0, String_t* ___password1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ReqHttpRegister(System.String,System.String)
extern "C"  void PluginsSdkMgr_ReqHttpRegister_m1545502154 (PluginsSdkMgr_t3884624670 * __this, String_t* ___username0, String_t* ___password1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginsSdkMgr_IsLoginSuccess_m3105299902 (PluginsSdkMgr_t3884624670 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::OpenUserLogin()
extern "C"  void PluginsSdkMgr_OpenUserLogin_m100015455 (PluginsSdkMgr_t3884624670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PluginsSdkMgr::SendHttpLogin(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  Il2CppObject * PluginsSdkMgr_SendHttpLogin_m341185405 (PluginsSdkMgr_t3884624670 * __this, Dictionary_2_t827649927 * ___parameters0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::OnHideedLogo(System.String)
extern "C"  void PluginsSdkMgr_OnHideedLogo_m3314314624 (PluginsSdkMgr_t3884624670 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::OnKeyCodeBack(System.String)
extern "C"  void PluginsSdkMgr_OnKeyCodeBack_m3295611555 (PluginsSdkMgr_t3884624670 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::OnSDKQuit(System.String)
extern "C"  void PluginsSdkMgr_OnSDKQuit_m859051181 (PluginsSdkMgr_t3884624670 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::OnMH_IAPSuccess(System.String)
extern "C"  void PluginsSdkMgr_OnMH_IAPSuccess_m4240687591 (PluginsSdkMgr_t3884624670 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::StartActivityHelp(System.String,System.String,System.String,System.UInt32)
extern "C"  void PluginsSdkMgr_StartActivityHelp_m2054224241 (PluginsSdkMgr_t3884624670 * __this, String_t* ___username0, String_t* ___serverId1, String_t* ___userId2, uint32_t ___pay3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ShowDialog(System.String,System.String,System.Boolean,System.String,System.String,System.Action,System.Action)
extern "C"  void PluginsSdkMgr_ShowDialog_m2861151521 (PluginsSdkMgr_t3884624670 * __this, String_t* ___title0, String_t* ___content1, bool ___cancelable2, String_t* ___okText3, String_t* ___cancelText4, Action_t3771233898 * ___OnOK5, Action_t3771233898 * ___OnCancel6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::OnDialogOK(System.String)
extern "C"  void PluginsSdkMgr_OnDialogOK_m1433182920 (PluginsSdkMgr_t3884624670 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::OnDialogCancel(System.String)
extern "C"  void PluginsSdkMgr_OnDialogCancel_m1456882890 (PluginsSdkMgr_t3884624670 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::GetPushState(System.String,PushType)
extern "C"  bool PluginsSdkMgr_GetPushState_m202342068 (PluginsSdkMgr_t3884624670 * __this, String_t* ___account0, int32_t ___pushType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::SettingPushState(System.String,PushType,System.Boolean)
extern "C"  bool PluginsSdkMgr_SettingPushState_m729700931 (PluginsSdkMgr_t3884624670 * __this, String_t* ___account0, int32_t ___pushType1, bool ___isLoopTimer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::SendNotification(System.String,PushType,System.Int32,System.Single,System.String,System.String,System.Boolean,System.String)
extern "C"  void PluginsSdkMgr_SendNotification_m615588383 (PluginsSdkMgr_t3884624670 * __this, String_t* ___account0, int32_t ___pushType1, int32_t ___id2, float ___delay3, String_t* ___title4, String_t* ___message5, bool ___isLoopTimer6, String_t* ___integralTimes7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::Game_Login(System.String,Gender,System.String,System.String,System.Int32,System.String)
extern "C"  void PluginsSdkMgr_Game_Login_m3641949573 (PluginsSdkMgr_t3884624670 * __this, String_t* ___account0, int32_t ___genders1, String_t* ___age2, String_t* ___serverId3, int32_t ___level4, String_t* ___rolename5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::Game_SetEvent(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void PluginsSdkMgr_Game_SetEvent_m3651422725 (PluginsSdkMgr_t3884624670 * __this, String_t* ___eventName0, Dictionary_2_t827649927 * ___dict1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginsSdkMgr::Game_getDeviceId()
extern "C"  String_t* PluginsSdkMgr_Game_getDeviceId_m3049878798 (PluginsSdkMgr_t3884624670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::_showWarningBox2(System.String,System.String,System.String,System.String)
extern "C"  void PluginsSdkMgr__showWarningBox2_m2443238328 (Il2CppObject * __this /* static, unused */, String_t* ___strTitle0, String_t* ___strText1, String_t* ___leftBtnTxt2, String_t* ___rightBtnTxt3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::_showWarningBox1(System.String,System.String,System.String)
extern "C"  void PluginsSdkMgr__showWarningBox1_m3268203293 (Il2CppObject * __this /* static, unused */, String_t* ___strTitle0, String_t* ___strText1, String_t* ___leftBtnTxt2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_ContainsKey1(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  bool PluginsSdkMgr_ilo_ContainsKey1_m2953558579 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginsSdkMgr::ilo_get_currentVS2(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginsSdkMgr_ilo_get_currentVS2_m2592183810 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_GetSdkStatus3(PluginsSdkMgr,PluginsSdkMgr/SDK,Newtonsoft.Json.Linq.JObject)
extern "C"  bool PluginsSdkMgr_ilo_GetSdkStatus3_m3381470024 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, int32_t ___sdk1, JObject_t1798544199 * ___jobj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_GetMainSdkStatus4(PluginsSdkMgr,PluginsSdkMgr/SDK,Newtonsoft.Json.Linq.JObject)
extern "C"  bool PluginsSdkMgr_ilo_GetMainSdkStatus4_m35682274 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, int32_t ___sdk1, JObject_t1798544199 * ___jobj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init5(PluginReYun)
extern "C"  void PluginsSdkMgr_ilo_Init5_m2054837449 (Il2CppObject * __this /* static, unused */, PluginReYun_t2552860172 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init6(PluginXiaomi)
extern "C"  void PluginsSdkMgr_ilo_Init6_m1934785168 (Il2CppObject * __this /* static, unused */, PluginXiaomi_t2004955630 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init7(PluginXuegao)
extern "C"  void PluginsSdkMgr_ilo_Init7_m3789331199 (Il2CppObject * __this /* static, unused */, PluginXuegao_t2016148992 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init8(PluginWanmi)
extern "C"  void PluginsSdkMgr_ilo_Init8_m678231013 (Il2CppObject * __this /* static, unused */, PluginWanmi_t2557378541 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init9(PluginMihua)
extern "C"  void PluginsSdkMgr_ilo_Init9_m4188565036 (Il2CppObject * __this /* static, unused */, PluginMihua_t2548376133 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init10(PluginVivo)
extern "C"  void PluginsSdkMgr_ilo_Init10_m3325636346 (Il2CppObject * __this /* static, unused */, PluginVivo_t1606494879 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init11(Plugin360)
extern "C"  void PluginsSdkMgr_ilo_Init11_m789456702 (Il2CppObject * __this /* static, unused */, Plugin360_t190334458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init12(PluginFlyme)
extern "C"  void PluginsSdkMgr_ilo_Init12_m2844155167 (Il2CppObject * __this /* static, unused */, PluginFlyme_t2542016952 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init13(PluginOasis)
extern "C"  void PluginsSdkMgr_ilo_Init13_m2585918014 (Il2CppObject * __this /* static, unused */, PluginOasis_t2549995064 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init14(PluginAiWan)
extern "C"  void PluginsSdkMgr_ilo_Init14_m1686097452 (Il2CppObject * __this /* static, unused */, PluginAiWan_t2537276937 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init15(PluginZhiquyou)
extern "C"  void PluginsSdkMgr_ilo_Init15_m1817665611 (Il2CppObject * __this /* static, unused */, PluginZhiquyou_t1151299027 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init16(PluginDiYibo)
extern "C"  void PluginsSdkMgr_ilo_Init16_m2114136874 (Il2CppObject * __this /* static, unused */, PluginDiYibo_t1432128181 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init17(PluginR2)
extern "C"  void PluginsSdkMgr_ilo_Init17_m1158858413 (Il2CppObject * __this /* static, unused */, PluginR2_t2499992755 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init18(PluginYouDong)
extern "C"  void PluginsSdkMgr_ilo_Init18_m4129832993 (Il2CppObject * __this /* static, unused */, PluginYouDong_t3100552560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init19(PluginYouwo)
extern "C"  void PluginsSdkMgr_ilo_Init19_m4146827276 (Il2CppObject * __this /* static, unused */, PluginYouwo_t2559649700 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init20(PluginMiquwan)
extern "C"  void PluginsSdkMgr_ilo_Init20_m439501161 (Il2CppObject * __this /* static, unused */, PluginMiquwan_t866441041 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init21(PluginDianZhi)
extern "C"  void PluginsSdkMgr_ilo_Init21_m958154723 (Il2CppObject * __this /* static, unused */, PluginDianZhi_t1453829974 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init22(PluginShoumeng)
extern "C"  void PluginsSdkMgr_ilo_Init22_m1959496827 (Il2CppObject * __this /* static, unused */, PluginShoumeng_t2011739967 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init23(PluginXiongmaowan)
extern "C"  void PluginsSdkMgr_ilo_Init23_m1252134890 (Il2CppObject * __this /* static, unused */, PluginXiongmaowan_t3347898093 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init24(PluginOMi)
extern "C"  void PluginsSdkMgr_ilo_Init24_m884159870 (Il2CppObject * __this /* static, unused */, PluginOMi_t190362136 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init25(PluginQiZi)
extern "C"  void PluginsSdkMgr_ilo_Init25_m1348417987 (Il2CppObject * __this /* static, unused */, PluginQiZi_t1606345050 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init26(PluginShoumeng_A1)
extern "C"  void PluginsSdkMgr_ilo_Init26_m835829060 (Il2CppObject * __this /* static, unused */, PluginShoumeng_A1_t4066769168 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init27(PluginYinHu)
extern "C"  void PluginsSdkMgr_ilo_Init27_m2364734395 (Il2CppObject * __this /* static, unused */, PluginYinHu_t2559462776 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init28(PluginNewMiquwan)
extern "C"  void PluginsSdkMgr_ilo_Init28_m621104361 (Il2CppObject * __this /* static, unused */, PluginNewMiquwan_t2470753303 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init29(PluginQuWan)
extern "C"  void PluginsSdkMgr_ilo_Init29_m1135004804 (Il2CppObject * __this /* static, unused */, PluginQuWan_t2552410765 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init30(PluginTT)
extern "C"  void PluginsSdkMgr_ilo_Init30_m1110784452 (Il2CppObject * __this /* static, unused */, PluginTT_t2499992851 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init31(PluginGameFan)
extern "C"  void PluginsSdkMgr_ilo_Init31_m774501708 (Il2CppObject * __this /* static, unused */, PluginGameFan_t3898102510 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init32(PluginJuFengJQ)
extern "C"  void PluginsSdkMgr_ilo_Init32_m3041279484 (Il2CppObject * __this /* static, unused */, PluginJuFengJQ_t970421149 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Init33(PluginGuangdiantong)
extern "C"  void PluginsSdkMgr_ilo_Init33_m1291722073 (Il2CppObject * __this /* static, unused */, PluginGuangdiantong_t1625076095 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AndroidJavaObject PluginsSdkMgr::ilo_get_androidJavaObject34(PluginsSdkMgr)
extern "C"  AndroidJavaObject_t2362096582 * PluginsSdkMgr_ilo_get_androidJavaObject34_m1376318554 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PluginsSdkMgr::ilo_SendHttpLogin35(PluginsSdkMgr,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  Il2CppObject * PluginsSdkMgr_ilo_SendHttpLogin35_m2765412542 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, Dictionary_2_t827649927 * ___parameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin36(PluginHongYou)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin36_m2925523640 (Il2CppObject * __this /* static, unused */, PluginHongYou_t892448812 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin37(PluginVivo)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin37_m2298221712 (Il2CppObject * __this /* static, unused */, PluginVivo_t1606494879 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin38(PluginOppo)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin38_m1998556829 (Il2CppObject * __this /* static, unused */, PluginOppo_t1606292883 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin39(PluginFlyme)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin39_m1059072585 (Il2CppObject * __this /* static, unused */, PluginFlyme_t2542016952 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin40(PluginQuick)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin40_m3144430993 (Il2CppObject * __this /* static, unused */, PluginQuick_t2552428122 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin41(PluginZxhy)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin41_m3557208934 (Il2CppObject * __this /* static, unused */, PluginZxhy_t1606628034 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin42(PluginJingang)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin42_m186838266 (Il2CppObject * __this /* static, unused */, PluginJingang_t2495688911 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin43(PluginOasis)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin43_m1552330320 (Il2CppObject * __this /* static, unused */, PluginOasis_t2549995064 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin44(PluginAiWan)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin44_m652509758 (Il2CppObject * __this /* static, unused */, PluginAiWan_t2537276937 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin45(PluginIqiyi)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin45_m383931656 (Il2CppObject * __this /* static, unused */, PluginIqiyi_t2544921470 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin46(PluginYouMa)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin46_m334907365 (Il2CppObject * __this /* static, unused */, PluginYouMa_t2559648384 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin47(PluginYiwanSuper)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin47_m4239898292 (Il2CppObject * __this /* static, unused */, PluginYiwanSuper_t2354859450 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin48(PluginHongyouNY)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin48_m597161068 (Il2CppObject * __this /* static, unused */, PluginHongyouNY_t2974371607 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin49(PluginInFox)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin49_m2652842059 (Il2CppObject * __this /* static, unused */, PluginInFox_t2544798167 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin50(PluginR2)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin50_m3628047859 (Il2CppObject * __this /* static, unused */, PluginR2_t2499992755 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin51(PluginFengqi)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin51_m2359170148 (Il2CppObject * __this /* static, unused */, PluginFengqi_t1486316547 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin52(PluginYouDong)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin52_m3111051898 (Il2CppObject * __this /* static, unused */, PluginYouDong_t3100552560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin53(PluginYouwo)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin53_m3359175845 (Il2CppObject * __this /* static, unused */, PluginYouwo_t2559649700 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin54(PluginYueplay)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin54_m2702505534 (Il2CppObject * __this /* static, unused */, PluginYueplay_t3258858666 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin55(PluginGamecat)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin55_m534558166 (Il2CppObject * __this /* static, unused */, PluginGamecat_t3898130385 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin56(PluginJingqi)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin56_m676558633 (Il2CppObject * __this /* static, unused */, PluginJingqi_t1604527235 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin57(PluginJuliang)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin57_m992175398 (Il2CppObject * __this /* static, unused */, PluginJuliang_t2837451263 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin58(PluginXiongmaowan)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin58_m3523812951 (Il2CppObject * __this /* static, unused */, PluginXiongmaowan_t3347898093 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin59(PluginCaoHua)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin59_m3052625977 (Il2CppObject * __this /* static, unused */, PluginCaoHua_t1396735126 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin60(PluginFastPlay)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin60_m3467459298 (Il2CppObject * __this /* static, unused */, PluginFastPlay_t3320186403 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin61(PluginYouxiang)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin61_m1085742409 (Il2CppObject * __this /* static, unused */, PluginYouxiang_t1675681021 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin62(PluginQiangWan)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin62_m1288205330 (Il2CppObject * __this /* static, unused */, PluginQiangWan_t3301124917 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin63(PluginXima)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin63_m1956995824 (Il2CppObject * __this /* static, unused */, PluginXima_t1606554168 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin64(PluginWanYu)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin64_m4290744252 (Il2CppObject * __this /* static, unused */, PluginWanYu_t2557377933 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin65(PluginQuWan)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin65_m3631203419 (Il2CppObject * __this /* static, unused */, PluginQuWan_t2552410765 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin66(PluginXiaoMai)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin66_m3410430116 (Il2CppObject * __this /* static, unused */, PluginXiaoMai_t2024051491 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin67(PluginZongYou)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin67_m3485766216 (Il2CppObject * __this /* static, unused */, PluginZongYou_t3982613182 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin68(PluginHuoShu)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin68_m1749989948 (Il2CppObject * __this /* static, unused */, PluginHuoShu_t1558361489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin69(PluginGuoPan)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin69_m2369941759 (Il2CppObject * __this /* static, unused */, PluginGuoPan_t1529729231 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin70(PluginAiLeXinYou)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin70_m1876847790 (Il2CppObject * __this /* static, unused */, PluginAiLeXinYou_t3790076438 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsSdkMgr::ilo_ReqSDKHttpLogin71(PluginJuFengJQ)
extern "C"  Dictionary_2_t827649927 * PluginsSdkMgr_ilo_ReqSDKHttpLogin71_m3995417800 (Il2CppObject * __this /* static, unused */, PluginJuFengJQ_t970421149 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginsSdkMgr::ilo_get_Instance72()
extern "C"  VersionMgr_t1322950208 * PluginsSdkMgr_ilo_get_Instance72_m1548276582 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess73(PluginSjoy,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess73_m1020544027 (Il2CppObject * __this /* static, unused */, PluginSjoy_t1606406260 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess74(PluginHongYou,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess74_m1925866574 (Il2CppObject * __this /* static, unused */, PluginHongYou_t892448812 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess75(PluginXiaomi,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess75_m1983821843 (Il2CppObject * __this /* static, unused */, PluginXiaomi_t2004955630 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess76(PluginWanmi,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess76_m3693117073 (Il2CppObject * __this /* static, unused */, PluginWanmi_t2557378541 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess77(PluginFlyme,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess77_m3068593885 (Il2CppObject * __this /* static, unused */, PluginFlyme_t2542016952 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess78(PluginZxhy,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess78_m631926596 (Il2CppObject * __this /* static, unused */, PluginZxhy_t1606628034 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess79(PluginJingang,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess79_m2282629174 (Il2CppObject * __this /* static, unused */, PluginJingang_t2495688911 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess80(PluginIqiyi,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess80_m3778050235 (Il2CppObject * __this /* static, unused */, PluginIqiyi_t2544921470 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess81(PluginR2,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess81_m75144605 (Il2CppObject * __this /* static, unused */, PluginR2_t2499992755 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess82(PluginYuewan,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess82_m254271031 (Il2CppObject * __this /* static, unused */, PluginYuewan_t2044793518 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess83(PluginYouwo,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess83_m1447024228 (Il2CppObject * __this /* static, unused */, PluginYouwo_t2559649700 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess84(PluginFuncheer,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess84_m118828968 (Il2CppObject * __this /* static, unused */, PluginFuncheer_t3732253601 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess85(PluginYueplay,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess85_m2649963756 (Il2CppObject * __this /* static, unused */, PluginYueplay_t3258858666 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess86(PluginMiquwan,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess86_m1721659860 (Il2CppObject * __this /* static, unused */, PluginMiquwan_t866441041 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess87(PluginGamecat,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess87_m2958697109 (Il2CppObject * __this /* static, unused */, PluginGamecat_t3898130385 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess88(PluginChangku,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess88_m212853169 (Il2CppObject * __this /* static, unused */, PluginChangku_t537709740 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess89(PluginDianZhi,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess89_m1794963612 (Il2CppObject * __this /* static, unused */, PluginDianZhi_t1453829974 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess90(PluginZhangYu,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess90_m3318529585 (Il2CppObject * __this /* static, unused */, PluginZhangYu_t3770424661 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess91(PluginShoumeng,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess91_m1429224362 (Il2CppObject * __this /* static, unused */, PluginShoumeng_t2011739967 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess92(PluginYuLe,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess92_m3045653746 (Il2CppObject * __this /* static, unused */, PluginYuLe_t1606594472 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess93(PluginQiZi,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess93_m3354432003 (Il2CppObject * __this /* static, unused */, PluginQiZi_t1606345050 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess94(PluginShoumeng_A1,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess94_m1088499440 (Il2CppObject * __this /* static, unused */, PluginShoumeng_A1_t4066769168 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess95(PluginYinHu,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess95_m1766541529 (Il2CppObject * __this /* static, unused */, PluginYinHu_t2559462776 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess96(PluginQiangWan,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess96_m686899963 (Il2CppObject * __this /* static, unused */, PluginQiangWan_t3301124917 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess97(PluginPaoJiao,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess97_m3189115195 (Il2CppObject * __this /* static, unused */, PluginPaoJiao_t3296777368 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess98(PluginDolphin,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess98_m4134640111 (Il2CppObject * __this /* static, unused */, PluginDolphin_t1635836683 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess99(PluginWanYu,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess99_m3321512178 (Il2CppObject * __this /* static, unused */, PluginWanYu_t2557377933 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr::ilo_IsLoginSuccess100(PluginJuFengJQ,System.Boolean)
extern "C"  bool PluginsSdkMgr_ilo_IsLoginSuccess100_m3365708449 (Il2CppObject * __this /* static, unused */, PluginJuFengJQ_t970421149 * ____this0, bool ___isOpenUseLogin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Hg_Login101(PluginHongYou)
extern "C"  void PluginsSdkMgr_ilo_Hg_Login101_m386871365 (Il2CppObject * __this /* static, unused */, PluginHongYou_t892448812 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Yj_Login102(PluginYiJie)
extern "C"  void PluginsSdkMgr_ilo_Yj_Login102_m348880191 (Il2CppObject * __this /* static, unused */, PluginYiJie_t2559429187 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_OpenUserLogin103(PluginWanmi)
extern "C"  void PluginsSdkMgr_ilo_OpenUserLogin103_m351816219 (Il2CppObject * __this /* static, unused */, PluginWanmi_t2557378541 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_OpenUserLogin104(PluginMihua)
extern "C"  void PluginsSdkMgr_ilo_OpenUserLogin104_m3862150242 (Il2CppObject * __this /* static, unused */, PluginMihua_t2548376133 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_LoginJinLiMgr105(PluginJinLi)
extern "C"  void PluginsSdkMgr_ilo_LoginJinLiMgr105_m3274295374 (Il2CppObject * __this /* static, unused */, PluginJinLi_t2545610073 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_OpenUserLogin106(PluginUC)
extern "C"  void PluginsSdkMgr_ilo_OpenUserLogin106_m1618121612 (Il2CppObject * __this /* static, unused */, PluginUC_t2499992865 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_OpenUserLogin107(Plugin360)
extern "C"  void PluginsSdkMgr_ilo_OpenUserLogin107_m3045187306 (Il2CppObject * __this /* static, unused */, Plugin360_t190334458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_OpenUserLogin108(PluginAiWan)
extern "C"  void PluginsSdkMgr_ilo_OpenUserLogin108_m1495840666 (Il2CppObject * __this /* static, unused */, PluginAiWan_t2537276937 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_OpenUserLogin109(PluginDiYibo)
extern "C"  void PluginsSdkMgr_ilo_OpenUserLogin109_m3298559291 (Il2CppObject * __this /* static, unused */, PluginDiYibo_t1432128181 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_OpenUserLogin110(PluginYouMa)
extern "C"  void PluginsSdkMgr_ilo_OpenUserLogin110_m3446409036 (Il2CppObject * __this /* static, unused */, PluginYouMa_t2559648384 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_OpenUserLogin111(PluginYuewan)
extern "C"  void PluginsSdkMgr_ilo_OpenUserLogin111_m1130301497 (Il2CppObject * __this /* static, unused */, PluginYuewan_t2044793518 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_OpenUserLogin112(PluginYueplay)
extern "C"  void PluginsSdkMgr_ilo_OpenUserLogin112_m162926944 (Il2CppObject * __this /* static, unused */, PluginYueplay_t3258858666 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_OpenUserLogin113(PluginGamecat)
extern "C"  void PluginsSdkMgr_ilo_OpenUserLogin113_m2289946872 (Il2CppObject * __this /* static, unused */, PluginGamecat_t3898130385 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_OpenUserLogin114(PluginDianZhi)
extern "C"  void PluginsSdkMgr_ilo_OpenUserLogin114_m3315511282 (Il2CppObject * __this /* static, unused */, PluginDianZhi_t1453829974 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_OpenUserLogin115(PluginMoHe)
extern "C"  void PluginsSdkMgr_ilo_OpenUserLogin115_m18507961 (Il2CppObject * __this /* static, unused */, PluginMoHe_t1606231090 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_OpenUserLogin116(PluginZhangYu)
extern "C"  void PluginsSdkMgr_ilo_OpenUserLogin116_m1094434193 (Il2CppObject * __this /* static, unused */, PluginZhangYu_t3770424661 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_OpenUserLogin117(PluginCaoHua)
extern "C"  void PluginsSdkMgr_ilo_OpenUserLogin117_m2970704087 (Il2CppObject * __this /* static, unused */, PluginCaoHua_t1396735126 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_OpenUserLogin118(PluginYuLe)
extern "C"  void PluginsSdkMgr_ilo_OpenUserLogin118_m3444531078 (Il2CppObject * __this /* static, unused */, PluginYuLe_t1606594472 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_OpenUserLogin119(PluginOMi)
extern "C"  void PluginsSdkMgr_ilo_OpenUserLogin119_m3010807755 (Il2CppObject * __this /* static, unused */, PluginOMi_t190362136 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_OpenUserLogin120(PluginAnfeng)
extern "C"  void PluginsSdkMgr_ilo_OpenUserLogin120_m3381926445 (Il2CppObject * __this /* static, unused */, PluginAnfeng_t1351242136 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_OpenUserLogin121(PluginQiZi)
extern "C"  void PluginsSdkMgr_ilo_OpenUserLogin121_m690094124 (Il2CppObject * __this /* static, unused */, PluginQiZi_t1606345050 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_OpenUserLogin122(PluginNewMH)
extern "C"  void PluginsSdkMgr_ilo_OpenUserLogin122_m3618746531 (Il2CppObject * __this /* static, unused */, PluginNewMH_t2549193640 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_OpenUserLogin123(PluginXiaoQi)
extern "C"  void PluginsSdkMgr_ilo_OpenUserLogin123_m2399869502 (Il2CppObject * __this /* static, unused */, PluginXiaoQi_t2004954762 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_OpenUserLogin124(PluginXiaoMai)
extern "C"  void PluginsSdkMgr_ilo_OpenUserLogin124_m870851526 (Il2CppObject * __this /* static, unused */, PluginXiaoMai_t2024051491 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_OpenUserLogin125(PluginJuFengJQ)
extern "C"  void PluginsSdkMgr_ilo_OpenUserLogin125_m3104888781 (Il2CppObject * __this /* static, unused */, PluginJuFengJQ_t970421149 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double PluginsSdkMgr::ilo_get_serverTime126(TimeMgr)
extern "C"  double PluginsSdkMgr_ilo_get_serverTime126_m4204278352 (Il2CppObject * __this /* static, unused */, TimeMgr_t350708715 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginsSdkMgr::ilo_get_channelId127(PluginsSdkMgr)
extern "C"  String_t* PluginsSdkMgr_ilo_get_channelId127_m1867608748 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo_Log128(System.Object,System.Boolean)
extern "C"  void PluginsSdkMgr_ilo_Log128_m2247311334 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo__showWarningBox2129(System.String,System.String,System.String,System.String)
extern "C"  void PluginsSdkMgr_ilo__showWarningBox2129_m711247117 (Il2CppObject * __this /* static, unused */, String_t* ___strTitle0, String_t* ___strText1, String_t* ___leftBtnTxt2, String_t* ___rightBtnTxt3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr::ilo__showWarningBox1130(System.String,System.String,System.String)
extern "C"  void PluginsSdkMgr_ilo__showWarningBox1130_m867423866 (Il2CppObject * __this /* static, unused */, String_t* ___strTitle0, String_t* ___strText1, String_t* ___leftBtnTxt2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginsSdkMgr::ilo_Game_getDeviceId131(PluginReYun)
extern "C"  String_t* PluginsSdkMgr_ilo_Game_getDeviceId131_m4237404118 (Il2CppObject * __this /* static, unused */, PluginReYun_t2552860172 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
