﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.ClipperLib.IntersectNode
struct IntersectNode_t4106323947;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.ClipperLib.IntersectNode::.ctor()
extern "C"  void IntersectNode__ctor_m641175930 (IntersectNode_t4106323947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
