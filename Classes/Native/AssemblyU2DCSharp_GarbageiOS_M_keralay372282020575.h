﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_keralay37
struct  M_keralay37_t2282020575  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_keralay37::_sousalpawHalldal
	float ____sousalpawHalldal_0;
	// System.Int32 GarbageiOS.M_keralay37::_fairloJevevur
	int32_t ____fairloJevevur_1;
	// System.Int32 GarbageiOS.M_keralay37::_tedootou
	int32_t ____tedootou_2;
	// System.UInt32 GarbageiOS.M_keralay37::_rujaycirRallparcel
	uint32_t ____rujaycirRallparcel_3;
	// System.Int32 GarbageiOS.M_keralay37::_verkixiCarlo
	int32_t ____verkixiCarlo_4;
	// System.Int32 GarbageiOS.M_keralay37::_rebarNousecor
	int32_t ____rebarNousecor_5;
	// System.String GarbageiOS.M_keralay37::_salltrall
	String_t* ____salltrall_6;
	// System.Boolean GarbageiOS.M_keralay37::_beaposemCesa
	bool ____beaposemCesa_7;

public:
	inline static int32_t get_offset_of__sousalpawHalldal_0() { return static_cast<int32_t>(offsetof(M_keralay37_t2282020575, ____sousalpawHalldal_0)); }
	inline float get__sousalpawHalldal_0() const { return ____sousalpawHalldal_0; }
	inline float* get_address_of__sousalpawHalldal_0() { return &____sousalpawHalldal_0; }
	inline void set__sousalpawHalldal_0(float value)
	{
		____sousalpawHalldal_0 = value;
	}

	inline static int32_t get_offset_of__fairloJevevur_1() { return static_cast<int32_t>(offsetof(M_keralay37_t2282020575, ____fairloJevevur_1)); }
	inline int32_t get__fairloJevevur_1() const { return ____fairloJevevur_1; }
	inline int32_t* get_address_of__fairloJevevur_1() { return &____fairloJevevur_1; }
	inline void set__fairloJevevur_1(int32_t value)
	{
		____fairloJevevur_1 = value;
	}

	inline static int32_t get_offset_of__tedootou_2() { return static_cast<int32_t>(offsetof(M_keralay37_t2282020575, ____tedootou_2)); }
	inline int32_t get__tedootou_2() const { return ____tedootou_2; }
	inline int32_t* get_address_of__tedootou_2() { return &____tedootou_2; }
	inline void set__tedootou_2(int32_t value)
	{
		____tedootou_2 = value;
	}

	inline static int32_t get_offset_of__rujaycirRallparcel_3() { return static_cast<int32_t>(offsetof(M_keralay37_t2282020575, ____rujaycirRallparcel_3)); }
	inline uint32_t get__rujaycirRallparcel_3() const { return ____rujaycirRallparcel_3; }
	inline uint32_t* get_address_of__rujaycirRallparcel_3() { return &____rujaycirRallparcel_3; }
	inline void set__rujaycirRallparcel_3(uint32_t value)
	{
		____rujaycirRallparcel_3 = value;
	}

	inline static int32_t get_offset_of__verkixiCarlo_4() { return static_cast<int32_t>(offsetof(M_keralay37_t2282020575, ____verkixiCarlo_4)); }
	inline int32_t get__verkixiCarlo_4() const { return ____verkixiCarlo_4; }
	inline int32_t* get_address_of__verkixiCarlo_4() { return &____verkixiCarlo_4; }
	inline void set__verkixiCarlo_4(int32_t value)
	{
		____verkixiCarlo_4 = value;
	}

	inline static int32_t get_offset_of__rebarNousecor_5() { return static_cast<int32_t>(offsetof(M_keralay37_t2282020575, ____rebarNousecor_5)); }
	inline int32_t get__rebarNousecor_5() const { return ____rebarNousecor_5; }
	inline int32_t* get_address_of__rebarNousecor_5() { return &____rebarNousecor_5; }
	inline void set__rebarNousecor_5(int32_t value)
	{
		____rebarNousecor_5 = value;
	}

	inline static int32_t get_offset_of__salltrall_6() { return static_cast<int32_t>(offsetof(M_keralay37_t2282020575, ____salltrall_6)); }
	inline String_t* get__salltrall_6() const { return ____salltrall_6; }
	inline String_t** get_address_of__salltrall_6() { return &____salltrall_6; }
	inline void set__salltrall_6(String_t* value)
	{
		____salltrall_6 = value;
		Il2CppCodeGenWriteBarrier(&____salltrall_6, value);
	}

	inline static int32_t get_offset_of__beaposemCesa_7() { return static_cast<int32_t>(offsetof(M_keralay37_t2282020575, ____beaposemCesa_7)); }
	inline bool get__beaposemCesa_7() const { return ____beaposemCesa_7; }
	inline bool* get_address_of__beaposemCesa_7() { return &____beaposemCesa_7; }
	inline void set__beaposemCesa_7(bool value)
	{
		____beaposemCesa_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
