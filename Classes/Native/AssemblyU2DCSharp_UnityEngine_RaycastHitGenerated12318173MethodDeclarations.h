﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_RaycastHitGenerated
struct UnityEngine_RaycastHitGenerated_t12318173;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_RaycastHitGenerated::.ctor()
extern "C"  void UnityEngine_RaycastHitGenerated__ctor_m3291171374 (UnityEngine_RaycastHitGenerated_t12318173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHitGenerated::.cctor()
extern "C"  void UnityEngine_RaycastHitGenerated__cctor_m2759968575 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RaycastHitGenerated::RaycastHit_RaycastHit1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RaycastHitGenerated_RaycastHit_RaycastHit1_m1254060676 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHitGenerated::RaycastHit_point(JSVCall)
extern "C"  void UnityEngine_RaycastHitGenerated_RaycastHit_point_m1517759478 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHitGenerated::RaycastHit_normal(JSVCall)
extern "C"  void UnityEngine_RaycastHitGenerated_RaycastHit_normal_m2587243135 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHitGenerated::RaycastHit_barycentricCoordinate(JSVCall)
extern "C"  void UnityEngine_RaycastHitGenerated_RaycastHit_barycentricCoordinate_m3389965136 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHitGenerated::RaycastHit_distance(JSVCall)
extern "C"  void UnityEngine_RaycastHitGenerated_RaycastHit_distance_m2473915185 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHitGenerated::RaycastHit_triangleIndex(JSVCall)
extern "C"  void UnityEngine_RaycastHitGenerated_RaycastHit_triangleIndex_m2137230108 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHitGenerated::RaycastHit_textureCoord(JSVCall)
extern "C"  void UnityEngine_RaycastHitGenerated_RaycastHit_textureCoord_m863844908 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHitGenerated::RaycastHit_textureCoord2(JSVCall)
extern "C"  void UnityEngine_RaycastHitGenerated_RaycastHit_textureCoord2_m4011117998 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHitGenerated::RaycastHit_lightmapCoord(JSVCall)
extern "C"  void UnityEngine_RaycastHitGenerated_RaycastHit_lightmapCoord_m3332129367 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHitGenerated::RaycastHit_collider(JSVCall)
extern "C"  void UnityEngine_RaycastHitGenerated_RaycastHit_collider_m3237213714 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHitGenerated::RaycastHit_rigidbody(JSVCall)
extern "C"  void UnityEngine_RaycastHitGenerated_RaycastHit_rigidbody_m3725400025 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHitGenerated::RaycastHit_transform(JSVCall)
extern "C"  void UnityEngine_RaycastHitGenerated_RaycastHit_transform_m1545792922 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHitGenerated::__Register()
extern "C"  void UnityEngine_RaycastHitGenerated___Register_m426991449 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RaycastHitGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_RaycastHitGenerated_ilo_getObject1_m750132424 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_RaycastHitGenerated::ilo_getVector3S2(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_RaycastHitGenerated_ilo_getVector3S2_m2706824409 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHitGenerated::ilo_changeJSObj3(System.Int32,System.Object)
extern "C"  void UnityEngine_RaycastHitGenerated_ilo_changeJSObj3_m1266551037 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_RaycastHitGenerated::ilo_getSingle4(System.Int32)
extern "C"  float UnityEngine_RaycastHitGenerated_ilo_getSingle4_m565724516 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RaycastHitGenerated::ilo_setObject5(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_RaycastHitGenerated_ilo_setObject5_m3200725316 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
