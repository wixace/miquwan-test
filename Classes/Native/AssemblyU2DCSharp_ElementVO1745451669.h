﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// elementCfg
struct elementCfg_t575911880;
// System.Single[]
struct SingleU5BU5D_t2316563989;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ElementVO
struct  ElementVO_t1745451669  : public Il2CppObject
{
public:
	// elementCfg ElementVO::mpCfg
	elementCfg_t575911880 * ___mpCfg_0;
	// System.Single[] ElementVO::modulusp
	SingleU5BU5D_t2316563989* ___modulusp_1;

public:
	inline static int32_t get_offset_of_mpCfg_0() { return static_cast<int32_t>(offsetof(ElementVO_t1745451669, ___mpCfg_0)); }
	inline elementCfg_t575911880 * get_mpCfg_0() const { return ___mpCfg_0; }
	inline elementCfg_t575911880 ** get_address_of_mpCfg_0() { return &___mpCfg_0; }
	inline void set_mpCfg_0(elementCfg_t575911880 * value)
	{
		___mpCfg_0 = value;
		Il2CppCodeGenWriteBarrier(&___mpCfg_0, value);
	}

	inline static int32_t get_offset_of_modulusp_1() { return static_cast<int32_t>(offsetof(ElementVO_t1745451669, ___modulusp_1)); }
	inline SingleU5BU5D_t2316563989* get_modulusp_1() const { return ___modulusp_1; }
	inline SingleU5BU5D_t2316563989** get_address_of_modulusp_1() { return &___modulusp_1; }
	inline void set_modulusp_1(SingleU5BU5D_t2316563989* value)
	{
		___modulusp_1 = value;
		Il2CppCodeGenWriteBarrier(&___modulusp_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
