﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pomelo.DotNetClient.MessageProtocol
struct MessageProtocol_t562499845;
// Pomelo.DotNetClient.Transporter
struct Transporter_t1342168412;
// Pomelo.DotNetClient.HandShakeService
struct HandShakeService_t4068105274;
// Pomelo.DotNetClient.HeartBeatService
struct HeartBeatService_t3983407957;
// Pomelo.DotNetClient.PomeloClient
struct PomeloClient_t4215271137;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_ProtocolState901285727.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pomelo.DotNetClient.Protocol
struct  Protocol_t400511732  : public Il2CppObject
{
public:
	// Pomelo.DotNetClient.MessageProtocol Pomelo.DotNetClient.Protocol::messageProtocol
	MessageProtocol_t562499845 * ___messageProtocol_0;
	// Pomelo.DotNetClient.ProtocolState Pomelo.DotNetClient.Protocol::state
	int32_t ___state_1;
	// Pomelo.DotNetClient.Transporter Pomelo.DotNetClient.Protocol::transporter
	Transporter_t1342168412 * ___transporter_2;
	// Pomelo.DotNetClient.HandShakeService Pomelo.DotNetClient.Protocol::handshake
	HandShakeService_t4068105274 * ___handshake_3;
	// Pomelo.DotNetClient.HeartBeatService Pomelo.DotNetClient.Protocol::heartBeatService
	HeartBeatService_t3983407957 * ___heartBeatService_4;
	// Pomelo.DotNetClient.PomeloClient Pomelo.DotNetClient.Protocol::pc
	PomeloClient_t4215271137 * ___pc_5;
	// System.Object Pomelo.DotNetClient.Protocol::_lockObj
	Il2CppObject * ____lockObj_6;

public:
	inline static int32_t get_offset_of_messageProtocol_0() { return static_cast<int32_t>(offsetof(Protocol_t400511732, ___messageProtocol_0)); }
	inline MessageProtocol_t562499845 * get_messageProtocol_0() const { return ___messageProtocol_0; }
	inline MessageProtocol_t562499845 ** get_address_of_messageProtocol_0() { return &___messageProtocol_0; }
	inline void set_messageProtocol_0(MessageProtocol_t562499845 * value)
	{
		___messageProtocol_0 = value;
		Il2CppCodeGenWriteBarrier(&___messageProtocol_0, value);
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(Protocol_t400511732, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_transporter_2() { return static_cast<int32_t>(offsetof(Protocol_t400511732, ___transporter_2)); }
	inline Transporter_t1342168412 * get_transporter_2() const { return ___transporter_2; }
	inline Transporter_t1342168412 ** get_address_of_transporter_2() { return &___transporter_2; }
	inline void set_transporter_2(Transporter_t1342168412 * value)
	{
		___transporter_2 = value;
		Il2CppCodeGenWriteBarrier(&___transporter_2, value);
	}

	inline static int32_t get_offset_of_handshake_3() { return static_cast<int32_t>(offsetof(Protocol_t400511732, ___handshake_3)); }
	inline HandShakeService_t4068105274 * get_handshake_3() const { return ___handshake_3; }
	inline HandShakeService_t4068105274 ** get_address_of_handshake_3() { return &___handshake_3; }
	inline void set_handshake_3(HandShakeService_t4068105274 * value)
	{
		___handshake_3 = value;
		Il2CppCodeGenWriteBarrier(&___handshake_3, value);
	}

	inline static int32_t get_offset_of_heartBeatService_4() { return static_cast<int32_t>(offsetof(Protocol_t400511732, ___heartBeatService_4)); }
	inline HeartBeatService_t3983407957 * get_heartBeatService_4() const { return ___heartBeatService_4; }
	inline HeartBeatService_t3983407957 ** get_address_of_heartBeatService_4() { return &___heartBeatService_4; }
	inline void set_heartBeatService_4(HeartBeatService_t3983407957 * value)
	{
		___heartBeatService_4 = value;
		Il2CppCodeGenWriteBarrier(&___heartBeatService_4, value);
	}

	inline static int32_t get_offset_of_pc_5() { return static_cast<int32_t>(offsetof(Protocol_t400511732, ___pc_5)); }
	inline PomeloClient_t4215271137 * get_pc_5() const { return ___pc_5; }
	inline PomeloClient_t4215271137 ** get_address_of_pc_5() { return &___pc_5; }
	inline void set_pc_5(PomeloClient_t4215271137 * value)
	{
		___pc_5 = value;
		Il2CppCodeGenWriteBarrier(&___pc_5, value);
	}

	inline static int32_t get_offset_of__lockObj_6() { return static_cast<int32_t>(offsetof(Protocol_t400511732, ____lockObj_6)); }
	inline Il2CppObject * get__lockObj_6() const { return ____lockObj_6; }
	inline Il2CppObject ** get_address_of__lockObj_6() { return &____lockObj_6; }
	inline void set__lockObj_6(Il2CppObject * value)
	{
		____lockObj_6 = value;
		Il2CppCodeGenWriteBarrier(&____lockObj_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
