﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_EventDispatch_1_gen1067954939MethodDeclarations.h"

// System.Void CEvent.EventDispatch`1<CEvent.ZEvent>::.ctor()
#define EventDispatch_1__ctor_m3095404694(__this, method) ((  void (*) (EventDispatch_1_t535157068 *, const MethodInfo*))EventDispatch_1__ctor_m302555037_gshared)(__this, method)
// System.Void CEvent.EventDispatch`1<CEvent.ZEvent>::AddEventListener(System.Int32,CEvent.EventFunc`1<T>,System.Boolean)
#define EventDispatch_1_AddEventListener_m2060527251(__this, ___eventName0, ___func1, ___isInsert2, method) ((  void (*) (EventDispatch_1_t535157068 *, int32_t, EventFunc_1_t1114914310 *, bool, const MethodInfo*))EventDispatch_1_AddEventListener_m204507418_gshared)(__this, ___eventName0, ___func1, ___isInsert2, method)
// System.Void CEvent.EventDispatch`1<CEvent.ZEvent>::AddEventListener(System.Int32,CEvent.EventFunc`1<T>)
#define EventDispatch_1_AddEventListener_m3160548266(__this, ___eventName0, ___func1, method) ((  void (*) (EventDispatch_1_t535157068 *, int32_t, EventFunc_1_t1114914310 *, const MethodInfo*))EventDispatch_1_AddEventListener_m2707481603_gshared)(__this, ___eventName0, ___func1, method)
// System.Void CEvent.EventDispatch`1<CEvent.ZEvent>::AddEventListener(System.String,CEvent.EventFunc`1<T>)
#define EventDispatch_1_AddEventListener_m3725655173(__this, ___eventName0, ___func1, method) ((  void (*) (EventDispatch_1_t535157068 *, String_t*, EventFunc_1_t1114914310 *, const MethodInfo*))EventDispatch_1_AddEventListener_m2565490508_gshared)(__this, ___eventName0, ___func1, method)
// System.Void CEvent.EventDispatch`1<CEvent.ZEvent>::AddEventListener(System.String,CEvent.EventFunc`1<T>,System.Boolean)
#define EventDispatch_1_AddEventListener_m308288600(__this, ___eventName0, ___func1, ___isInsert2, method) ((  void (*) (EventDispatch_1_t535157068 *, String_t*, EventFunc_1_t1114914310 *, bool, const MethodInfo*))EventDispatch_1_AddEventListener_m2901215921_gshared)(__this, ___eventName0, ___func1, ___isInsert2, method)
// System.Void CEvent.EventDispatch`1<CEvent.ZEvent>::RemoveEventListener(System.Int32,CEvent.EventFunc`1<T>)
#define EventDispatch_1_RemoveEventListener_m2477161133(__this, ___eventName0, ___func1, method) ((  void (*) (EventDispatch_1_t535157068 *, int32_t, EventFunc_1_t1114914310 *, const MethodInfo*))EventDispatch_1_RemoveEventListener_m4250415028_gshared)(__this, ___eventName0, ___func1, method)
// System.Void CEvent.EventDispatch`1<CEvent.ZEvent>::RemoveEventListener(System.String,CEvent.EventFunc`1<T>)
#define EventDispatch_1_RemoveEventListener_m4015490530(__this, ___eventName0, ___func1, method) ((  void (*) (EventDispatch_1_t535157068 *, String_t*, EventFunc_1_t1114914310 *, const MethodInfo*))EventDispatch_1_RemoveEventListener_m3151786427_gshared)(__this, ___eventName0, ___func1, method)
// System.Void CEvent.EventDispatch`1<CEvent.ZEvent>::DispatchEvent(T)
#define EventDispatch_1_DispatchEvent_m217454314(__this, ___ev0, method) ((  void (*) (EventDispatch_1_t535157068 *, ZEvent_t3638018500 *, const MethodInfo*))EventDispatch_1_DispatchEvent_m160337859_gshared)(__this, ___ev0, method)
