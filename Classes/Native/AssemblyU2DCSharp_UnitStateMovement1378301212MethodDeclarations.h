﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnitStateMovement
struct UnitStateMovement_t1378301212;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Action
struct Action_t3771233898;
// CombatEntity
struct CombatEntity_t684137495;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UnitStateID1002198824.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_UnitStateMovement1378301212.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnitStateMovement::.ctor()
extern "C"  void UnitStateMovement__ctor_m2919266255 (UnitStateMovement_t1378301212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnitStateID UnitStateMovement::get_type()
extern "C"  uint8_t UnitStateMovement_get_type_m1965708717 (UnitStateMovement_t1378301212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateMovement::SetParams(System.Object[])
extern "C"  void UnitStateMovement_SetParams_m3673679261 (UnitStateMovement_t1378301212 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateMovement::EnterState()
extern "C"  void UnitStateMovement_EnterState_m943641326 (UnitStateMovement_t1378301212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateMovement::DoEnter()
extern "C"  void UnitStateMovement_DoEnter_m342928314 (UnitStateMovement_t1378301212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateMovement::OnPathComplete()
extern "C"  void UnitStateMovement_OnPathComplete_m1580522322 (UnitStateMovement_t1378301212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateMovement::LeaveState()
extern "C"  void UnitStateMovement_LeaveState_m2730017327 (UnitStateMovement_t1378301212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateMovement::Update(System.Single)
extern "C"  void UnitStateMovement_Update_m3755371853 (UnitStateMovement_t1378301212 * __this, float ___deltatime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 UnitStateMovement::ilo_DelayCall1(System.Single,System.Action)
extern "C"  uint32_t UnitStateMovement_ilo_DelayCall1_m3848469221 (Il2CppObject * __this /* static, unused */, float ___delaytime0, Action_t3771233898 * ___act1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnitStateMovement::ilo_get_isDeath2(CombatEntity)
extern "C"  bool UnitStateMovement_ilo_get_isDeath2_m1932991886 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateMovement::ilo_set_forceShift3(CombatEntity,System.Boolean)
extern "C"  void UnitStateMovement_ilo_set_forceShift3_m2196263835 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateMovement::ilo_set_isMovement4(CombatEntity,System.Boolean)
extern "C"  void UnitStateMovement_ilo_set_isMovement4_m4145156956 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateMovement::ilo_OnPathComplete5(UnitStateMovement)
extern "C"  void UnitStateMovement_ilo_OnPathComplete5_m1733497316 (Il2CppObject * __this /* static, unused */, UnitStateMovement_t1378301212 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnitStateMovement::ilo_get_position6(CombatEntity)
extern "C"  Vector3_t4282066566  UnitStateMovement_ilo_get_position6_m162901057 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateMovement::ilo_set_canMove7(CombatEntity,System.Boolean)
extern "C"  void UnitStateMovement_ilo_set_canMove7_m685395385 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl UnitStateMovement::ilo_get_bvrCtrl8(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * UnitStateMovement_ilo_get_bvrCtrl8_m2950543243 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
