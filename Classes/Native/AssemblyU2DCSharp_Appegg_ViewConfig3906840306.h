﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AppeggView
struct AppeggView_t3951466281;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Appegg/ViewConfig
struct  ViewConfig_t3906840306  : public Il2CppObject
{
public:
	// AppeggView Appegg/ViewConfig::LoadingView
	AppeggView_t3951466281 * ___LoadingView_0;
	// AppeggView Appegg/ViewConfig::ChangelogView
	AppeggView_t3951466281 * ___ChangelogView_1;
	// AppeggView Appegg/ViewConfig::ErrorView
	AppeggView_t3951466281 * ___ErrorView_2;

public:
	inline static int32_t get_offset_of_LoadingView_0() { return static_cast<int32_t>(offsetof(ViewConfig_t3906840306, ___LoadingView_0)); }
	inline AppeggView_t3951466281 * get_LoadingView_0() const { return ___LoadingView_0; }
	inline AppeggView_t3951466281 ** get_address_of_LoadingView_0() { return &___LoadingView_0; }
	inline void set_LoadingView_0(AppeggView_t3951466281 * value)
	{
		___LoadingView_0 = value;
		Il2CppCodeGenWriteBarrier(&___LoadingView_0, value);
	}

	inline static int32_t get_offset_of_ChangelogView_1() { return static_cast<int32_t>(offsetof(ViewConfig_t3906840306, ___ChangelogView_1)); }
	inline AppeggView_t3951466281 * get_ChangelogView_1() const { return ___ChangelogView_1; }
	inline AppeggView_t3951466281 ** get_address_of_ChangelogView_1() { return &___ChangelogView_1; }
	inline void set_ChangelogView_1(AppeggView_t3951466281 * value)
	{
		___ChangelogView_1 = value;
		Il2CppCodeGenWriteBarrier(&___ChangelogView_1, value);
	}

	inline static int32_t get_offset_of_ErrorView_2() { return static_cast<int32_t>(offsetof(ViewConfig_t3906840306, ___ErrorView_2)); }
	inline AppeggView_t3951466281 * get_ErrorView_2() const { return ___ErrorView_2; }
	inline AppeggView_t3951466281 ** get_address_of_ErrorView_2() { return &___ErrorView_2; }
	inline void set_ErrorView_2(AppeggView_t3951466281 * value)
	{
		___ErrorView_2 = value;
		Il2CppCodeGenWriteBarrier(&___ErrorView_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
