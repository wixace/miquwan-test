﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,SoundStatus>
struct ValueCollection_t2290807897;
// System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>
struct Dictionary_2_t3590202184;
// System.Collections.Generic.IEnumerator`1<SoundStatus>
struct IEnumerator_1_t1209836698;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// SoundStatus[]
struct SoundStatusU5BU5D_t3992956444;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SoundStatus3592938945.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1522035592.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,SoundStatus>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m836434394_gshared (ValueCollection_t2290807897 * __this, Dictionary_2_t3590202184 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m836434394(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2290807897 *, Dictionary_2_t3590202184 *, const MethodInfo*))ValueCollection__ctor_m836434394_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,SoundStatus>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3914309400_gshared (ValueCollection_t2290807897 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3914309400(__this, ___item0, method) ((  void (*) (ValueCollection_t2290807897 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3914309400_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,SoundStatus>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1507907553_gshared (ValueCollection_t2290807897 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1507907553(__this, method) ((  void (*) (ValueCollection_t2290807897 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1507907553_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,SoundStatus>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m556681298_gshared (ValueCollection_t2290807897 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m556681298(__this, ___item0, method) ((  bool (*) (ValueCollection_t2290807897 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m556681298_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,SoundStatus>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2772520311_gshared (ValueCollection_t2290807897 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2772520311(__this, ___item0, method) ((  bool (*) (ValueCollection_t2290807897 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2772520311_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,SoundStatus>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1745505249_gshared (ValueCollection_t2290807897 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1745505249(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2290807897 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1745505249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,SoundStatus>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m3174552485_gshared (ValueCollection_t2290807897 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m3174552485(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2290807897 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3174552485_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,SoundStatus>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3858067828_gshared (ValueCollection_t2290807897 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3858067828(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2290807897 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3858067828_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,SoundStatus>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3126824453_gshared (ValueCollection_t2290807897 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3126824453(__this, method) ((  bool (*) (ValueCollection_t2290807897 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3126824453_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,SoundStatus>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3916362597_gshared (ValueCollection_t2290807897 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3916362597(__this, method) ((  bool (*) (ValueCollection_t2290807897 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3916362597_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,SoundStatus>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m2772008535_gshared (ValueCollection_t2290807897 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2772008535(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2290807897 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2772008535_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,SoundStatus>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m1744213921_gshared (ValueCollection_t2290807897 * __this, SoundStatusU5BU5D_t3992956444* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m1744213921(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2290807897 *, SoundStatusU5BU5D_t3992956444*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1744213921_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,SoundStatus>::GetEnumerator()
extern "C"  Enumerator_t1522035592  ValueCollection_GetEnumerator_m3223107210_gshared (ValueCollection_t2290807897 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m3223107210(__this, method) ((  Enumerator_t1522035592  (*) (ValueCollection_t2290807897 *, const MethodInfo*))ValueCollection_GetEnumerator_m3223107210_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,SoundStatus>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m2224231967_gshared (ValueCollection_t2290807897 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m2224231967(__this, method) ((  int32_t (*) (ValueCollection_t2290807897 *, const MethodInfo*))ValueCollection_get_Count_m2224231967_gshared)(__this, method)
