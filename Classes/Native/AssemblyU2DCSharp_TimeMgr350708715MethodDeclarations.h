﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TimeMgr
struct TimeMgr_t350708715;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_TimeMgr350708715.h"

// System.Void TimeMgr::.ctor()
extern "C"  void TimeMgr__ctor_m3612083168 (TimeMgr_t350708715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeMgr::AddTimeScale(System.Single,System.String)
extern "C"  void TimeMgr_AddTimeScale_m4232174695 (TimeMgr_t350708715 * __this, float ___timeScale0, String_t* ___timeKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeMgr::DelTimeScale(System.Single,System.String)
extern "C"  void TimeMgr_DelTimeScale_m1095239761 (TimeMgr_t350708715 * __this, float ___timeScale0, String_t* ___timeKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeMgr::SetServerTime(System.Double)
extern "C"  void TimeMgr_SetServerTime_m2597007122 (TimeMgr_t350708715 * __this, double ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeMgr::SetServerStarTime(System.Double)
extern "C"  void TimeMgr_SetServerStarTime_m3321224288 (TimeMgr_t350708715 * __this, double ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double TimeMgr::get_serverTime()
extern "C"  double TimeMgr_get_serverTime_m1245675386 (TimeMgr_t350708715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double TimeMgr::get_serverStartTime()
extern "C"  double TimeMgr_get_serverStartTime_m234423332 (TimeMgr_t350708715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeMgr::set_mergeServerTime(System.Double)
extern "C"  void TimeMgr_set_mergeServerTime_m1692256537 (TimeMgr_t350708715 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double TimeMgr::get_mergeServerTime()
extern "C"  double TimeMgr_get_mergeServerTime_m140445152 (TimeMgr_t350708715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeMgr::ilo_DelTimeScale1(TimeMgr,System.Single,System.String)
extern "C"  void TimeMgr_ilo_DelTimeScale1_m927520926 (Il2CppObject * __this /* static, unused */, TimeMgr_t350708715 * ____this0, float ___timeScale1, String_t* ___timeKey2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
