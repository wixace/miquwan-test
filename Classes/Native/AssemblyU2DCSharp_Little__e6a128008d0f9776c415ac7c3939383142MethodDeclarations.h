﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._e6a128008d0f9776c415ac7cba6879cd
struct _e6a128008d0f9776c415ac7cba6879cd_t3939383142;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._e6a128008d0f9776c415ac7cba6879cd::.ctor()
extern "C"  void _e6a128008d0f9776c415ac7cba6879cd__ctor_m893772167 (_e6a128008d0f9776c415ac7cba6879cd_t3939383142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e6a128008d0f9776c415ac7cba6879cd::_e6a128008d0f9776c415ac7cba6879cdm2(System.Int32)
extern "C"  int32_t _e6a128008d0f9776c415ac7cba6879cd__e6a128008d0f9776c415ac7cba6879cdm2_m3046615001 (_e6a128008d0f9776c415ac7cba6879cd_t3939383142 * __this, int32_t ____e6a128008d0f9776c415ac7cba6879cda0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e6a128008d0f9776c415ac7cba6879cd::_e6a128008d0f9776c415ac7cba6879cdm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _e6a128008d0f9776c415ac7cba6879cd__e6a128008d0f9776c415ac7cba6879cdm_m2360772349 (_e6a128008d0f9776c415ac7cba6879cd_t3939383142 * __this, int32_t ____e6a128008d0f9776c415ac7cba6879cda0, int32_t ____e6a128008d0f9776c415ac7cba6879cd11, int32_t ____e6a128008d0f9776c415ac7cba6879cdc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
