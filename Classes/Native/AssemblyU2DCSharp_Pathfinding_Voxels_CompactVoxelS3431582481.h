﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Voxels.CompactVoxelSpan
struct  CompactVoxelSpan_t3431582481 
{
public:
	// System.UInt16 Pathfinding.Voxels.CompactVoxelSpan::y
	uint16_t ___y_0;
	// System.UInt32 Pathfinding.Voxels.CompactVoxelSpan::con
	uint32_t ___con_1;
	// System.UInt32 Pathfinding.Voxels.CompactVoxelSpan::h
	uint32_t ___h_2;
	// System.Int32 Pathfinding.Voxels.CompactVoxelSpan::reg
	int32_t ___reg_3;

public:
	inline static int32_t get_offset_of_y_0() { return static_cast<int32_t>(offsetof(CompactVoxelSpan_t3431582481, ___y_0)); }
	inline uint16_t get_y_0() const { return ___y_0; }
	inline uint16_t* get_address_of_y_0() { return &___y_0; }
	inline void set_y_0(uint16_t value)
	{
		___y_0 = value;
	}

	inline static int32_t get_offset_of_con_1() { return static_cast<int32_t>(offsetof(CompactVoxelSpan_t3431582481, ___con_1)); }
	inline uint32_t get_con_1() const { return ___con_1; }
	inline uint32_t* get_address_of_con_1() { return &___con_1; }
	inline void set_con_1(uint32_t value)
	{
		___con_1 = value;
	}

	inline static int32_t get_offset_of_h_2() { return static_cast<int32_t>(offsetof(CompactVoxelSpan_t3431582481, ___h_2)); }
	inline uint32_t get_h_2() const { return ___h_2; }
	inline uint32_t* get_address_of_h_2() { return &___h_2; }
	inline void set_h_2(uint32_t value)
	{
		___h_2 = value;
	}

	inline static int32_t get_offset_of_reg_3() { return static_cast<int32_t>(offsetof(CompactVoxelSpan_t3431582481, ___reg_3)); }
	inline int32_t get_reg_3() const { return ___reg_3; }
	inline int32_t* get_address_of_reg_3() { return &___reg_3; }
	inline void set_reg_3(int32_t value)
	{
		___reg_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Pathfinding.Voxels.CompactVoxelSpan
struct CompactVoxelSpan_t3431582481_marshaled_pinvoke
{
	uint16_t ___y_0;
	uint32_t ___con_1;
	uint32_t ___h_2;
	int32_t ___reg_3;
};
// Native definition for marshalling of: Pathfinding.Voxels.CompactVoxelSpan
struct CompactVoxelSpan_t3431582481_marshaled_com
{
	uint16_t ___y_0;
	uint32_t ___con_1;
	uint32_t ___h_2;
	int32_t ___reg_3;
};
