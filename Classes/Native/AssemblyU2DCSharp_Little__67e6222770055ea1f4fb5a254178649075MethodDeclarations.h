﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._67e6222770055ea1f4fb5a250b38cd0d
struct _67e6222770055ea1f4fb5a250b38cd0d_t4178649075;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__67e6222770055ea1f4fb5a254178649075.h"

// System.Void Little._67e6222770055ea1f4fb5a250b38cd0d::.ctor()
extern "C"  void _67e6222770055ea1f4fb5a250b38cd0d__ctor_m742082458 (_67e6222770055ea1f4fb5a250b38cd0d_t4178649075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._67e6222770055ea1f4fb5a250b38cd0d::_67e6222770055ea1f4fb5a250b38cd0dm2(System.Int32)
extern "C"  int32_t _67e6222770055ea1f4fb5a250b38cd0d__67e6222770055ea1f4fb5a250b38cd0dm2_m3914003641 (_67e6222770055ea1f4fb5a250b38cd0d_t4178649075 * __this, int32_t ____67e6222770055ea1f4fb5a250b38cd0da0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._67e6222770055ea1f4fb5a250b38cd0d::_67e6222770055ea1f4fb5a250b38cd0dm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _67e6222770055ea1f4fb5a250b38cd0d__67e6222770055ea1f4fb5a250b38cd0dm_m4214847005 (_67e6222770055ea1f4fb5a250b38cd0d_t4178649075 * __this, int32_t ____67e6222770055ea1f4fb5a250b38cd0da0, int32_t ____67e6222770055ea1f4fb5a250b38cd0d741, int32_t ____67e6222770055ea1f4fb5a250b38cd0dc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._67e6222770055ea1f4fb5a250b38cd0d::ilo__67e6222770055ea1f4fb5a250b38cd0dm21(Little._67e6222770055ea1f4fb5a250b38cd0d,System.Int32)
extern "C"  int32_t _67e6222770055ea1f4fb5a250b38cd0d_ilo__67e6222770055ea1f4fb5a250b38cd0dm21_m3929480474 (Il2CppObject * __this /* static, unused */, _67e6222770055ea1f4fb5a250b38cd0d_t4178649075 * ____this0, int32_t ____67e6222770055ea1f4fb5a250b38cd0da1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
