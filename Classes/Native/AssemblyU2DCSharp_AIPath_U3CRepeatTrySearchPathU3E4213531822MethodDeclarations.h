﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AIPath/<RepeatTrySearchPath>c__Iterator4
struct U3CRepeatTrySearchPathU3Ec__Iterator4_t4213531822;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AIPath/<RepeatTrySearchPath>c__Iterator4::.ctor()
extern "C"  void U3CRepeatTrySearchPathU3Ec__Iterator4__ctor_m4143472045 (U3CRepeatTrySearchPathU3Ec__Iterator4_t4213531822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AIPath/<RepeatTrySearchPath>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRepeatTrySearchPathU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2761664847 (U3CRepeatTrySearchPathU3Ec__Iterator4_t4213531822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AIPath/<RepeatTrySearchPath>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRepeatTrySearchPathU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m3096455907 (U3CRepeatTrySearchPathU3Ec__Iterator4_t4213531822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AIPath/<RepeatTrySearchPath>c__Iterator4::MoveNext()
extern "C"  bool U3CRepeatTrySearchPathU3Ec__Iterator4_MoveNext_m3775502607 (U3CRepeatTrySearchPathU3Ec__Iterator4_t4213531822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPath/<RepeatTrySearchPath>c__Iterator4::Dispose()
extern "C"  void U3CRepeatTrySearchPathU3Ec__Iterator4_Dispose_m340234922 (U3CRepeatTrySearchPathU3Ec__Iterator4_t4213531822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPath/<RepeatTrySearchPath>c__Iterator4::Reset()
extern "C"  void U3CRepeatTrySearchPathU3Ec__Iterator4_Reset_m1789904986 (U3CRepeatTrySearchPathU3Ec__Iterator4_t4213531822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
