﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UITextListGenerated
struct UITextListGenerated_t1478592048;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// UITextList
struct UITextList_t736798239;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_UITextList736798239.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UITextListGenerated::.ctor()
extern "C"  void UITextListGenerated__ctor_m504922939 (UITextListGenerated_t1478592048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITextListGenerated::UITextList_UITextList1(JSVCall,System.Int32)
extern "C"  bool UITextListGenerated_UITextList_UITextList1_m935559863 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextListGenerated::UITextList_textLabel(JSVCall)
extern "C"  void UITextListGenerated_UITextList_textLabel_m2960127799 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextListGenerated::UITextList_scrollBar(JSVCall)
extern "C"  void UITextListGenerated_UITextList_scrollBar_m723631576 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextListGenerated::UITextList_style(JSVCall)
extern "C"  void UITextListGenerated_UITextList_style_m1211640461 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextListGenerated::UITextList_paragraphHistory(JSVCall)
extern "C"  void UITextListGenerated_UITextList_paragraphHistory_m1438953064 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextListGenerated::UITextList_isValid(JSVCall)
extern "C"  void UITextListGenerated_UITextList_isValid_m386145900 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextListGenerated::UITextList_scrollValue(JSVCall)
extern "C"  void UITextListGenerated_UITextList_scrollValue_m1621632250 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITextListGenerated::UITextList_Add__String(JSVCall,System.Int32)
extern "C"  bool UITextListGenerated_UITextList_Add__String_m2425965655 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITextListGenerated::UITextList_Clear(JSVCall,System.Int32)
extern "C"  bool UITextListGenerated_UITextList_Clear_m1313043378 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITextListGenerated::UITextList_OnDrag__Vector2(JSVCall,System.Int32)
extern "C"  bool UITextListGenerated_UITextList_OnDrag__Vector2_m571998241 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITextListGenerated::UITextList_OnScroll__Single(JSVCall,System.Int32)
extern "C"  bool UITextListGenerated_UITextList_OnScroll__Single_m3942262609 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextListGenerated::__Register()
extern "C"  void UITextListGenerated___Register_m1175504492 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UITextListGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UITextListGenerated_ilo_getObject1_m3504011483 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UITextListGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UITextListGenerated_ilo_setObject2_m789614228 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextListGenerated::ilo_setInt323(System.Int32,System.Int32)
extern "C"  void UITextListGenerated_ilo_setInt323_m2045227705 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITextListGenerated::ilo_get_isValid4(UITextList)
extern "C"  bool UITextListGenerated_ilo_get_isValid4_m1091839252 (Il2CppObject * __this /* static, unused */, UITextList_t736798239 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UITextListGenerated::ilo_get_scrollValue5(UITextList)
extern "C"  float UITextListGenerated_ilo_get_scrollValue5_m3734360075 (Il2CppObject * __this /* static, unused */, UITextList_t736798239 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UITextListGenerated::ilo_getSingle6(System.Int32)
extern "C"  float UITextListGenerated_ilo_getSingle6_m1843357945 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextListGenerated::ilo_Add7(UITextList,System.String)
extern "C"  void UITextListGenerated_ilo_Add7_m460914255 (Il2CppObject * __this /* static, unused */, UITextList_t736798239 * ____this0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UITextListGenerated::ilo_getVector2S8(System.Int32)
extern "C"  Vector2_t4282066565  UITextListGenerated_ilo_getVector2S8_m1366551600 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
