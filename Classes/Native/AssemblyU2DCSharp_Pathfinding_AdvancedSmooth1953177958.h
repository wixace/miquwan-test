﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.AdvancedSmooth/MaxTurn
struct MaxTurn_t585750252;
// Pathfinding.AdvancedSmooth/ConstantTurn
struct ConstantTurn_t1658810934;

#include "AssemblyU2DCSharp_Pathfinding_MonoModifier200043088.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.AdvancedSmooth
struct  AdvancedSmooth_t1953177958  : public MonoModifier_t200043088
{
public:
	// System.Single Pathfinding.AdvancedSmooth::turningRadius
	float ___turningRadius_4;
	// Pathfinding.AdvancedSmooth/MaxTurn Pathfinding.AdvancedSmooth::turnConstruct1
	MaxTurn_t585750252 * ___turnConstruct1_5;
	// Pathfinding.AdvancedSmooth/ConstantTurn Pathfinding.AdvancedSmooth::turnConstruct2
	ConstantTurn_t1658810934 * ___turnConstruct2_6;

public:
	inline static int32_t get_offset_of_turningRadius_4() { return static_cast<int32_t>(offsetof(AdvancedSmooth_t1953177958, ___turningRadius_4)); }
	inline float get_turningRadius_4() const { return ___turningRadius_4; }
	inline float* get_address_of_turningRadius_4() { return &___turningRadius_4; }
	inline void set_turningRadius_4(float value)
	{
		___turningRadius_4 = value;
	}

	inline static int32_t get_offset_of_turnConstruct1_5() { return static_cast<int32_t>(offsetof(AdvancedSmooth_t1953177958, ___turnConstruct1_5)); }
	inline MaxTurn_t585750252 * get_turnConstruct1_5() const { return ___turnConstruct1_5; }
	inline MaxTurn_t585750252 ** get_address_of_turnConstruct1_5() { return &___turnConstruct1_5; }
	inline void set_turnConstruct1_5(MaxTurn_t585750252 * value)
	{
		___turnConstruct1_5 = value;
		Il2CppCodeGenWriteBarrier(&___turnConstruct1_5, value);
	}

	inline static int32_t get_offset_of_turnConstruct2_6() { return static_cast<int32_t>(offsetof(AdvancedSmooth_t1953177958, ___turnConstruct2_6)); }
	inline ConstantTurn_t1658810934 * get_turnConstruct2_6() const { return ___turnConstruct2_6; }
	inline ConstantTurn_t1658810934 ** get_address_of_turnConstruct2_6() { return &___turnConstruct2_6; }
	inline void set_turnConstruct2_6(ConstantTurn_t1658810934 * value)
	{
		___turnConstruct2_6 = value;
		Il2CppCodeGenWriteBarrier(&___turnConstruct2_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
