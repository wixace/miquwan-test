﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.QuadtreeGraph
struct QuadtreeGraph_t1924141899;
// Pathfinding.GraphNodeDelegateCancelable
struct GraphNodeDelegateCancelable_t3591372971;
// OnScanStatus
struct OnScanStatus_t2412749870;
// Pathfinding.QuadtreeNodeHolder
struct QuadtreeNodeHolder_t2134093193;
// System.Collections.Generic.List`1<Pathfinding.QuadtreeNode>
struct List_1_t2791487309;
// Pathfinding.QuadtreeNode
struct QuadtreeNode_t1423301757;
// Pathfinding.NavGraph
struct NavGraph_t1254319713;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNodeDelegateCan3591372971.h"
#include "AssemblyU2DCSharp_OnScanStatus2412749870.h"
#include "AssemblyU2DCSharp_Pathfinding_QuadtreeNodeHolder2134093193.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_IntRect3015058261.h"
#include "AssemblyU2DCSharp_Pathfinding_QuadtreeNode1423301757.h"
#include "AssemblyU2DCSharp_Pathfinding_QuadtreeGraph1924141899.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "AssemblyU2DCSharp_Pathfinding_NavGraph1254319713.h"

// System.Void Pathfinding.QuadtreeGraph::.ctor()
extern "C"  void QuadtreeGraph__ctor_m2047749708 (QuadtreeGraph_t1924141899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.QuadtreeGraph::get_Width()
extern "C"  int32_t QuadtreeGraph_get_Width_m597795541 (QuadtreeGraph_t1924141899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeGraph::set_Width(System.Int32)
extern "C"  void QuadtreeGraph_set_Width_m3276909860 (QuadtreeGraph_t1924141899 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.QuadtreeGraph::get_Height()
extern "C"  int32_t QuadtreeGraph_get_Height_m1855606938 (QuadtreeGraph_t1924141899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeGraph::set_Height(System.Int32)
extern "C"  void QuadtreeGraph_set_Height_m4120810029 (QuadtreeGraph_t1924141899 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeGraph::GetNodes(Pathfinding.GraphNodeDelegateCancelable)
extern "C"  void QuadtreeGraph_GetNodes_m3092425056 (QuadtreeGraph_t1924141899 * __this, GraphNodeDelegateCancelable_t3591372971 * ___del0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.QuadtreeGraph::CheckCollision(System.Int32,System.Int32)
extern "C"  bool QuadtreeGraph_CheckCollision_m1802143960 (QuadtreeGraph_t1924141899 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.QuadtreeGraph::CheckNode(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t QuadtreeGraph_CheckNode_m1780198675 (QuadtreeGraph_t1924141899 * __this, int32_t ___xs0, int32_t ___ys1, int32_t ___width2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeGraph::ScanInternal(OnScanStatus)
extern "C"  void QuadtreeGraph_ScanInternal_m1283293284 (QuadtreeGraph_t1924141899 * __this, OnScanStatus_t2412749870 * ___statusCallback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeGraph::RecalculateConnectionsRec(Pathfinding.QuadtreeNodeHolder,System.Int32,System.Int32,System.Int32)
extern "C"  void QuadtreeGraph_RecalculateConnectionsRec_m498937372 (QuadtreeGraph_t1924141899 * __this, QuadtreeNodeHolder_t2134093193 * ___holder0, int32_t ___depth1, int32_t ___x2, int32_t ___y3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.QuadtreeGraph::LocalToWorldPosition(System.Int32,System.Int32,System.Int32)
extern "C"  Vector3_t4282066566  QuadtreeGraph_LocalToWorldPosition_m649005668 (QuadtreeGraph_t1924141899 * __this, int32_t ___x0, int32_t ___y1, int32_t ___width2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeGraph::CreateNodeRec(Pathfinding.QuadtreeNodeHolder,System.Int32,System.Int32,System.Int32)
extern "C"  void QuadtreeGraph_CreateNodeRec_m2092043768 (QuadtreeGraph_t1924141899 * __this, QuadtreeNodeHolder_t2134093193 * ___holder0, int32_t ___depth1, int32_t ___x2, int32_t ___y3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeGraph::RecalculateConnections(Pathfinding.QuadtreeNodeHolder,System.Int32,System.Int32,System.Int32)
extern "C"  void QuadtreeGraph_RecalculateConnections_m2521337658 (QuadtreeGraph_t1924141899 * __this, QuadtreeNodeHolder_t2134093193 * ___holder0, int32_t ___depth1, int32_t ___x2, int32_t ___y3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeGraph::AddNeighboursRec(System.Collections.Generic.List`1<Pathfinding.QuadtreeNode>,Pathfinding.QuadtreeNodeHolder,System.Int32,System.Int32,System.Int32,Pathfinding.IntRect,Pathfinding.QuadtreeNode)
extern "C"  void QuadtreeGraph_AddNeighboursRec_m4005364530 (QuadtreeGraph_t1924141899 * __this, List_1_t2791487309 * ___arr0, QuadtreeNodeHolder_t2134093193 * ___holder1, int32_t ___depth2, int32_t ___x3, int32_t ___y4, IntRect_t3015058261  ___bounds5, QuadtreeNode_t1423301757 * ___dontInclude6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.QuadtreeNode Pathfinding.QuadtreeGraph::QueryPoint(System.Int32,System.Int32)
extern "C"  QuadtreeNode_t1423301757 * QuadtreeGraph_QueryPoint_m1340831986 (QuadtreeGraph_t1924141899 * __this, int32_t ___qx0, int32_t ___qy1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeGraph::OnDrawGizmos(System.Boolean)
extern "C"  void QuadtreeGraph_OnDrawGizmos_m3495577547 (QuadtreeGraph_t1924141899 * __this, bool ___drawNodes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeGraph::DrawRec(Pathfinding.QuadtreeNodeHolder,System.Int32,System.Int32,System.Int32,UnityEngine.Vector3)
extern "C"  void QuadtreeGraph_DrawRec_m531738225 (QuadtreeGraph_t1924141899 * __this, QuadtreeNodeHolder_t2134093193 * ___h0, int32_t ___depth1, int32_t ___x2, int32_t ___y3, Vector3_t4282066566  ___parentPos4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.QuadtreeGraph::ilo_LocalToWorldPosition1(Pathfinding.QuadtreeGraph,System.Int32,System.Int32,System.Int32)
extern "C"  Vector3_t4282066566  QuadtreeGraph_ilo_LocalToWorldPosition1_m2695620593 (Il2CppObject * __this /* static, unused */, QuadtreeGraph_t1924141899 * ____this0, int32_t ___x1, int32_t ___y2, int32_t ___width3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeGraph::ilo_set_Height2(Pathfinding.QuadtreeGraph,System.Int32)
extern "C"  void QuadtreeGraph_ilo_set_Height2_m3501363273 (Il2CppObject * __this /* static, unused */, QuadtreeGraph_t1924141899 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.QuadtreeGraph::ilo_get_Width3(Pathfinding.QuadtreeGraph)
extern "C"  int32_t QuadtreeGraph_ilo_get_Width3_m1765343008 (Il2CppObject * __this /* static, unused */, QuadtreeGraph_t1924141899 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.QuadtreeGraph::ilo_CheckCollision4(Pathfinding.QuadtreeGraph,System.Int32,System.Int32)
extern "C"  bool QuadtreeGraph_ilo_CheckCollision4_m2281777950 (Il2CppObject * __this /* static, unused */, QuadtreeGraph_t1924141899 * ____this0, int32_t ___x1, int32_t ___y2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeGraph::ilo_RecalculateConnectionsRec5(Pathfinding.QuadtreeGraph,Pathfinding.QuadtreeNodeHolder,System.Int32,System.Int32,System.Int32)
extern "C"  void QuadtreeGraph_ilo_RecalculateConnectionsRec5_m4032527247 (Il2CppObject * __this /* static, unused */, QuadtreeGraph_t1924141899 * ____this0, QuadtreeNodeHolder_t2134093193 * ___holder1, int32_t ___depth2, int32_t ___x3, int32_t ___y4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeGraph::ilo_CreateNodeRec6(Pathfinding.QuadtreeGraph,Pathfinding.QuadtreeNodeHolder,System.Int32,System.Int32,System.Int32)
extern "C"  void QuadtreeGraph_ilo_CreateNodeRec6_m1399445970 (Il2CppObject * __this /* static, unused */, QuadtreeGraph_t1924141899 * ____this0, QuadtreeNodeHolder_t2134093193 * ___holder1, int32_t ___depth2, int32_t ___x3, int32_t ___y4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.IntRect Pathfinding.QuadtreeGraph::ilo_Expand7(Pathfinding.IntRect&,System.Int32)
extern "C"  IntRect_t3015058261  QuadtreeGraph_ilo_Expand7_m1163558774 (Il2CppObject * __this /* static, unused */, IntRect_t3015058261 * ____this0, int32_t ___range1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.QuadtreeGraph::ilo_op_Subtraction8(Pathfinding.Int3,Pathfinding.Int3)
extern "C"  Int3_t1974045594  QuadtreeGraph_ilo_op_Subtraction8_m981648670 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___lhs0, Int3_t1974045594  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeGraph::ilo_AddNeighboursRec9(Pathfinding.QuadtreeGraph,System.Collections.Generic.List`1<Pathfinding.QuadtreeNode>,Pathfinding.QuadtreeNodeHolder,System.Int32,System.Int32,System.Int32,Pathfinding.IntRect,Pathfinding.QuadtreeNode)
extern "C"  void QuadtreeGraph_ilo_AddNeighboursRec9_m3329624395 (Il2CppObject * __this /* static, unused */, QuadtreeGraph_t1924141899 * ____this0, List_1_t2791487309 * ___arr1, QuadtreeNodeHolder_t2134093193 * ___holder2, int32_t ___depth3, int32_t ___x4, int32_t ___y5, IntRect_t3015058261  ___bounds6, QuadtreeNode_t1423301757 * ___dontInclude7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeGraph::ilo_OnDrawGizmos10(Pathfinding.NavGraph,System.Boolean)
extern "C"  void QuadtreeGraph_ilo_OnDrawGizmos10_m1694553772 (Il2CppObject * __this /* static, unused */, NavGraph_t1254319713 * ____this0, bool ___drawNodes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeGraph::ilo_DrawRec11(Pathfinding.QuadtreeGraph,Pathfinding.QuadtreeNodeHolder,System.Int32,System.Int32,System.Int32,UnityEngine.Vector3)
extern "C"  void QuadtreeGraph_ilo_DrawRec11_m1786409745 (Il2CppObject * __this /* static, unused */, QuadtreeGraph_t1924141899 * ____this0, QuadtreeNodeHolder_t2134093193 * ___h1, int32_t ___depth2, int32_t ___x3, int32_t ___y4, Vector3_t4282066566  ___parentPos5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
