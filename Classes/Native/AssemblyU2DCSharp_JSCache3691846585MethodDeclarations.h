﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSCache
struct JSCache_t3691846585;
// System.String
struct String_t;
// JSCache/TypeInfo
struct TypeInfo_t3198813374;
// System.Type
struct Type_t;
// JSVCall
struct JSVCall_t3708497963;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void JSCache::.ctor()
extern "C"  void JSCache__ctor_m1555903442 (JSCache_t3691846585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCache::.cctor()
extern "C"  void JSCache__cctor_m506270235 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSCache::GetMonoBehaviourJSComponentName(System.String)
extern "C"  String_t* JSCache_GetMonoBehaviourJSComponentName_m3388316020 (Il2CppObject * __this /* static, unused */, String_t* ___monoBehaviourName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCache::InitMonoBehaviourJSComponentName()
extern "C"  void JSCache_InitMonoBehaviourJSComponentName_m2360287655 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSCache::IsInheritanceRel(System.String,System.String)
extern "C"  bool JSCache_IsInheritanceRel_m1667014119 (Il2CppObject * __this /* static, unused */, String_t* ___baseClassName0, String_t* ___subClassName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSCache/TypeInfo JSCache::GetTypeInfo(System.Type)
extern "C"  TypeInfo_t3198813374 * JSCache_GetTypeInfo_m3900101932 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSCache::ilo_CallJSFunctionName1(JSVCall,System.Int32,System.String,System.Object[])
extern "C"  bool JSCache_ilo_CallJSFunctionName1_m574383920 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ____this0, int32_t ___jsObjID1, String_t* ___funName2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSCache::ilo_getStringS2(System.Int32)
extern "C"  String_t* JSCache_ilo_getStringS2_m1970894263 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
