﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Caching
struct Caching_t189518581;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Hash128346790303.h"

// System.Void UnityEngine.Caching::.ctor()
extern "C"  void Caching__ctor_m1074247034 (Caching_t189518581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Caching::Authorize(System.String,System.String,System.Int64,System.String)
extern "C"  bool Caching_Authorize_m1832450173 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___domain1, int64_t ___size2, String_t* ___signature3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Caching::Authorize(System.String,System.String,System.Int64,System.Int32,System.String)
extern "C"  bool Caching_Authorize_m3691242034 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___domain1, int64_t ___size2, int32_t ___expiration3, String_t* ___signature4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Caching::CleanCache()
extern "C"  bool Caching_CleanCache_m3047430173 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Caching::IsVersionCached(System.String,System.Int32)
extern "C"  bool Caching_IsVersionCached_m1361005283 (Il2CppObject * __this /* static, unused */, String_t* ___url0, int32_t ___version1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Caching::IsVersionCached(System.String,UnityEngine.Hash128)
extern "C"  bool Caching_IsVersionCached_m2815263246 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Hash128_t346790303  ___hash1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Caching::INTERNAL_CALL_IsVersionCached(System.String,UnityEngine.Hash128&)
extern "C"  bool Caching_INTERNAL_CALL_IsVersionCached_m3654100127 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Hash128_t346790303 * ___hash1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Caching::MarkAsUsed(System.String,System.Int32)
extern "C"  bool Caching_MarkAsUsed_m2252289525 (Il2CppObject * __this /* static, unused */, String_t* ___url0, int32_t ___version1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Caching::MarkAsUsed(System.String,UnityEngine.Hash128)
extern "C"  bool Caching_MarkAsUsed_m1137991356 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Hash128_t346790303  ___hash1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Caching::INTERNAL_CALL_MarkAsUsed(System.String,UnityEngine.Hash128&)
extern "C"  bool Caching_INTERNAL_CALL_MarkAsUsed_m1987617487 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Hash128_t346790303 * ___hash1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 UnityEngine.Caching::get_spaceFree()
extern "C"  int64_t Caching_get_spaceFree_m2795221986 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 UnityEngine.Caching::get_maximumAvailableDiskSpace()
extern "C"  int64_t Caching_get_maximumAvailableDiskSpace_m807211184 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Caching::set_maximumAvailableDiskSpace(System.Int64)
extern "C"  void Caching_set_maximumAvailableDiskSpace_m3223996045 (Il2CppObject * __this /* static, unused */, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 UnityEngine.Caching::get_spaceOccupied()
extern "C"  int64_t Caching_get_spaceOccupied_m2881550612 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Caching::get_expirationDelay()
extern "C"  int32_t Caching_get_expirationDelay_m4059329891 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Caching::set_expirationDelay(System.Int32)
extern "C"  void Caching_set_expirationDelay_m4132285888 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Caching::get_enabled()
extern "C"  bool Caching_get_enabled_m1597173814 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Caching::set_enabled(System.Boolean)
extern "C"  void Caching_set_enabled_m3188262227 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Caching::get_compressionEnabled()
extern "C"  bool Caching_get_compressionEnabled_m2169612424 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Caching::set_compressionEnabled(System.Boolean)
extern "C"  void Caching_set_compressionEnabled_m3074467353 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Caching::get_ready()
extern "C"  bool Caching_get_ready_m3259280152 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
