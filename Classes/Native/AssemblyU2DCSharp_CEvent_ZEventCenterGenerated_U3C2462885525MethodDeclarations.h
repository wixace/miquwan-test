﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CEvent_ZEventCenterGenerated/<ZEventCenter_AddEventListener_GetDelegate_member0_arg1>c__AnonStorey52
struct U3CZEventCenter_AddEventListener_GetDelegate_member0_arg1U3Ec__AnonStorey52_t2462885525;
// CEvent.ZEvent
struct ZEvent_t3638018500;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"

// System.Void CEvent_ZEventCenterGenerated/<ZEventCenter_AddEventListener_GetDelegate_member0_arg1>c__AnonStorey52::.ctor()
extern "C"  void U3CZEventCenter_AddEventListener_GetDelegate_member0_arg1U3Ec__AnonStorey52__ctor_m3386989478 (U3CZEventCenter_AddEventListener_GetDelegate_member0_arg1U3Ec__AnonStorey52_t2462885525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent_ZEventCenterGenerated/<ZEventCenter_AddEventListener_GetDelegate_member0_arg1>c__AnonStorey52::<>m__19(CEvent.ZEvent)
extern "C"  void U3CZEventCenter_AddEventListener_GetDelegate_member0_arg1U3Ec__AnonStorey52_U3CU3Em__19_m2298364100 (U3CZEventCenter_AddEventListener_GetDelegate_member0_arg1U3Ec__AnonStorey52_t2462885525 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
