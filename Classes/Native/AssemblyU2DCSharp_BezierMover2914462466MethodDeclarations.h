﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BezierMover
struct BezierMover_t2914462466;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_BezierMover2914462466.h"

// System.Void BezierMover::.ctor()
extern "C"  void BezierMover__ctor_m1642566313 (BezierMover_t2914462466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BezierMover::Awake()
extern "C"  void BezierMover_Awake_m1880171532 (BezierMover_t2914462466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BezierMover::Update()
extern "C"  void BezierMover_Update_m1106810244 (BezierMover_t2914462466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 BezierMover::Plot(System.Single)
extern "C"  Vector3_t4282066566  BezierMover_Plot_m1568629385 (BezierMover_t2914462466 * __this, float ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BezierMover::Move(System.Boolean)
extern "C"  void BezierMover_Move_m3565755779 (BezierMover_t2914462466 * __this, bool ___progress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 BezierMover::ilo_Plot1(BezierMover,System.Single)
extern "C"  Vector3_t4282066566  BezierMover_ilo_Plot1_m1456532407 (Il2CppObject * __this /* static, unused */, BezierMover_t2914462466 * ____this0, float ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
