﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2761310900;
// UnityEngine.UI.Image
struct Image_t538875265;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_SimplePlayerModel3948357526.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplePlayerManager
struct  SimplePlayerManager_t1523766490  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Text SimplePlayerManager::moneyText
	Text_t9039225 * ___moneyText_3;
	// UnityEngine.UI.Text SimplePlayerManager::ticketText
	Text_t9039225 * ___ticketText_4;
	// UnityEngine.Sprite[] SimplePlayerManager::sprites
	SpriteU5BU5D_t2761310900* ___sprites_5;
	// UnityEngine.UI.Image SimplePlayerManager::icon
	Image_t538875265 * ___icon_6;

public:
	inline static int32_t get_offset_of_moneyText_3() { return static_cast<int32_t>(offsetof(SimplePlayerManager_t1523766490, ___moneyText_3)); }
	inline Text_t9039225 * get_moneyText_3() const { return ___moneyText_3; }
	inline Text_t9039225 ** get_address_of_moneyText_3() { return &___moneyText_3; }
	inline void set_moneyText_3(Text_t9039225 * value)
	{
		___moneyText_3 = value;
		Il2CppCodeGenWriteBarrier(&___moneyText_3, value);
	}

	inline static int32_t get_offset_of_ticketText_4() { return static_cast<int32_t>(offsetof(SimplePlayerManager_t1523766490, ___ticketText_4)); }
	inline Text_t9039225 * get_ticketText_4() const { return ___ticketText_4; }
	inline Text_t9039225 ** get_address_of_ticketText_4() { return &___ticketText_4; }
	inline void set_ticketText_4(Text_t9039225 * value)
	{
		___ticketText_4 = value;
		Il2CppCodeGenWriteBarrier(&___ticketText_4, value);
	}

	inline static int32_t get_offset_of_sprites_5() { return static_cast<int32_t>(offsetof(SimplePlayerManager_t1523766490, ___sprites_5)); }
	inline SpriteU5BU5D_t2761310900* get_sprites_5() const { return ___sprites_5; }
	inline SpriteU5BU5D_t2761310900** get_address_of_sprites_5() { return &___sprites_5; }
	inline void set_sprites_5(SpriteU5BU5D_t2761310900* value)
	{
		___sprites_5 = value;
		Il2CppCodeGenWriteBarrier(&___sprites_5, value);
	}

	inline static int32_t get_offset_of_icon_6() { return static_cast<int32_t>(offsetof(SimplePlayerManager_t1523766490, ___icon_6)); }
	inline Image_t538875265 * get_icon_6() const { return ___icon_6; }
	inline Image_t538875265 ** get_address_of_icon_6() { return &___icon_6; }
	inline void set_icon_6(Image_t538875265 * value)
	{
		___icon_6 = value;
		Il2CppCodeGenWriteBarrier(&___icon_6, value);
	}
};

struct SimplePlayerManager_t1523766490_StaticFields
{
public:
	// SimplePlayerModel SimplePlayerManager::_pm
	SimplePlayerModel_t3948357526  ____pm_2;

public:
	inline static int32_t get_offset_of__pm_2() { return static_cast<int32_t>(offsetof(SimplePlayerManager_t1523766490_StaticFields, ____pm_2)); }
	inline SimplePlayerModel_t3948357526  get__pm_2() const { return ____pm_2; }
	inline SimplePlayerModel_t3948357526 * get_address_of__pm_2() { return &____pm_2; }
	inline void set__pm_2(SimplePlayerModel_t3948357526  value)
	{
		____pm_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
