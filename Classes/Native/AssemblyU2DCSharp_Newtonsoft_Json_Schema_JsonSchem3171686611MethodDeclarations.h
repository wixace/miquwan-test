﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Schema.JsonSchemaNodeCollection
struct JsonSchemaNodeCollection_t3171686611;
// System.String
struct String_t;
// Newtonsoft.Json.Schema.JsonSchemaNode
struct JsonSchemaNode_t2115227093;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem2115227093.h"

// System.Void Newtonsoft.Json.Schema.JsonSchemaNodeCollection::.ctor()
extern "C"  void JsonSchemaNodeCollection__ctor_m2805292458 (JsonSchemaNodeCollection_t3171686611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchemaNodeCollection::GetKeyForItem(Newtonsoft.Json.Schema.JsonSchemaNode)
extern "C"  String_t* JsonSchemaNodeCollection_GetKeyForItem_m2109263025 (JsonSchemaNodeCollection_t3171686611 * __this, JsonSchemaNode_t2115227093 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchemaNodeCollection::ilo_get_Id1(Newtonsoft.Json.Schema.JsonSchemaNode)
extern "C"  String_t* JsonSchemaNodeCollection_ilo_get_Id1_m2267787242 (Il2CppObject * __this /* static, unused */, JsonSchemaNode_t2115227093 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
