﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "Pathfinding_Ionic_Zip_Reduced_Pathfinding_Ionic_Zi4282215738.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Ionic.Zip.BadStateException
struct  BadStateException_t806544713  : public ZipException_t4282215738
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
