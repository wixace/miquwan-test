﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.WheelFrictionCurve
struct WheelFrictionCurve_t2609054782;
struct WheelFrictionCurve_t2609054782_marshaled_pinvoke;
struct WheelFrictionCurve_t2609054782_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_WheelFrictionCurve2609054782.h"

// System.Single UnityEngine.WheelFrictionCurve::get_extremumSlip()
extern "C"  float WheelFrictionCurve_get_extremumSlip_m279160567 (WheelFrictionCurve_t2609054782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelFrictionCurve::set_extremumSlip(System.Single)
extern "C"  void WheelFrictionCurve_set_extremumSlip_m2926715820 (WheelFrictionCurve_t2609054782 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WheelFrictionCurve::get_extremumValue()
extern "C"  float WheelFrictionCurve_get_extremumValue_m2414611516 (WheelFrictionCurve_t2609054782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelFrictionCurve::set_extremumValue(System.Single)
extern "C"  void WheelFrictionCurve_set_extremumValue_m2386438743 (WheelFrictionCurve_t2609054782 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WheelFrictionCurve::get_asymptoteSlip()
extern "C"  float WheelFrictionCurve_get_asymptoteSlip_m3023574002 (WheelFrictionCurve_t2609054782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelFrictionCurve::set_asymptoteSlip(System.Single)
extern "C"  void WheelFrictionCurve_set_asymptoteSlip_m1772280417 (WheelFrictionCurve_t2609054782 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WheelFrictionCurve::get_asymptoteValue()
extern "C"  float WheelFrictionCurve_get_asymptoteValue_m1592082081 (WheelFrictionCurve_t2609054782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelFrictionCurve::set_asymptoteValue(System.Single)
extern "C"  void WheelFrictionCurve_set_asymptoteValue_m958679618 (WheelFrictionCurve_t2609054782 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WheelFrictionCurve::get_stiffness()
extern "C"  float WheelFrictionCurve_get_stiffness_m825641083 (WheelFrictionCurve_t2609054782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelFrictionCurve::set_stiffness(System.Single)
extern "C"  void WheelFrictionCurve_set_stiffness_m2581332088 (WheelFrictionCurve_t2609054782 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct WheelFrictionCurve_t2609054782;
struct WheelFrictionCurve_t2609054782_marshaled_pinvoke;

extern "C" void WheelFrictionCurve_t2609054782_marshal_pinvoke(const WheelFrictionCurve_t2609054782& unmarshaled, WheelFrictionCurve_t2609054782_marshaled_pinvoke& marshaled);
extern "C" void WheelFrictionCurve_t2609054782_marshal_pinvoke_back(const WheelFrictionCurve_t2609054782_marshaled_pinvoke& marshaled, WheelFrictionCurve_t2609054782& unmarshaled);
extern "C" void WheelFrictionCurve_t2609054782_marshal_pinvoke_cleanup(WheelFrictionCurve_t2609054782_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct WheelFrictionCurve_t2609054782;
struct WheelFrictionCurve_t2609054782_marshaled_com;

extern "C" void WheelFrictionCurve_t2609054782_marshal_com(const WheelFrictionCurve_t2609054782& unmarshaled, WheelFrictionCurve_t2609054782_marshaled_com& marshaled);
extern "C" void WheelFrictionCurve_t2609054782_marshal_com_back(const WheelFrictionCurve_t2609054782_marshaled_com& marshaled, WheelFrictionCurve_t2609054782& unmarshaled);
extern "C" void WheelFrictionCurve_t2609054782_marshal_com_cleanup(WheelFrictionCurve_t2609054782_marshaled_com& marshaled);
