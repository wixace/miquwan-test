﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1224156876.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_FightEnum_ESkillBuffTarget2441814200.h"

// System.Void System.Array/InternalEnumerator`1<FightEnum.ESkillBuffTarget>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3686721278_gshared (InternalEnumerator_1_t1224156876 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3686721278(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1224156876 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3686721278_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<FightEnum.ESkillBuffTarget>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3304866274_gshared (InternalEnumerator_1_t1224156876 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3304866274(__this, method) ((  void (*) (InternalEnumerator_1_t1224156876 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3304866274_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<FightEnum.ESkillBuffTarget>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2791404238_gshared (InternalEnumerator_1_t1224156876 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2791404238(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1224156876 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2791404238_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<FightEnum.ESkillBuffTarget>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3934484053_gshared (InternalEnumerator_1_t1224156876 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3934484053(__this, method) ((  void (*) (InternalEnumerator_1_t1224156876 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3934484053_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<FightEnum.ESkillBuffTarget>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m511631694_gshared (InternalEnumerator_1_t1224156876 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m511631694(__this, method) ((  bool (*) (InternalEnumerator_1_t1224156876 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m511631694_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<FightEnum.ESkillBuffTarget>::get_Current()
extern "C"  uint8_t InternalEnumerator_1_get_Current_m1382979397_gshared (InternalEnumerator_1_t1224156876 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1382979397(__this, method) ((  uint8_t (*) (InternalEnumerator_1_t1224156876 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1382979397_gshared)(__this, method)
