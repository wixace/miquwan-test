﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// Pathfinding.GridGraph/TextureData/ChannelUse[]
struct ChannelUseU5BU5D_t2916588678;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2960766953;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.GridGraph/TextureData
struct  TextureData_t1394081396  : public Il2CppObject
{
public:
	// System.Boolean Pathfinding.GridGraph/TextureData::enabled
	bool ___enabled_0;
	// UnityEngine.Texture2D Pathfinding.GridGraph/TextureData::source
	Texture2D_t3884108195 * ___source_1;
	// System.Single[] Pathfinding.GridGraph/TextureData::factors
	SingleU5BU5D_t2316563989* ___factors_2;
	// Pathfinding.GridGraph/TextureData/ChannelUse[] Pathfinding.GridGraph/TextureData::channels
	ChannelUseU5BU5D_t2916588678* ___channels_3;
	// UnityEngine.Color32[] Pathfinding.GridGraph/TextureData::data
	Color32U5BU5D_t2960766953* ___data_4;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(TextureData_t1394081396, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(TextureData_t1394081396, ___source_1)); }
	inline Texture2D_t3884108195 * get_source_1() const { return ___source_1; }
	inline Texture2D_t3884108195 ** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Texture2D_t3884108195 * value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_factors_2() { return static_cast<int32_t>(offsetof(TextureData_t1394081396, ___factors_2)); }
	inline SingleU5BU5D_t2316563989* get_factors_2() const { return ___factors_2; }
	inline SingleU5BU5D_t2316563989** get_address_of_factors_2() { return &___factors_2; }
	inline void set_factors_2(SingleU5BU5D_t2316563989* value)
	{
		___factors_2 = value;
		Il2CppCodeGenWriteBarrier(&___factors_2, value);
	}

	inline static int32_t get_offset_of_channels_3() { return static_cast<int32_t>(offsetof(TextureData_t1394081396, ___channels_3)); }
	inline ChannelUseU5BU5D_t2916588678* get_channels_3() const { return ___channels_3; }
	inline ChannelUseU5BU5D_t2916588678** get_address_of_channels_3() { return &___channels_3; }
	inline void set_channels_3(ChannelUseU5BU5D_t2916588678* value)
	{
		___channels_3 = value;
		Il2CppCodeGenWriteBarrier(&___channels_3, value);
	}

	inline static int32_t get_offset_of_data_4() { return static_cast<int32_t>(offsetof(TextureData_t1394081396, ___data_4)); }
	inline Color32U5BU5D_t2960766953* get_data_4() const { return ___data_4; }
	inline Color32U5BU5D_t2960766953** get_address_of_data_4() { return &___data_4; }
	inline void set_data_4(Color32U5BU5D_t2960766953* value)
	{
		___data_4 = value;
		Il2CppCodeGenWriteBarrier(&___data_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
