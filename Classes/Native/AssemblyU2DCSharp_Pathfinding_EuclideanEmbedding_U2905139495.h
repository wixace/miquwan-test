﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.EuclideanEmbedding/<RecalculatePivots>c__AnonStorey11D
struct  U3CRecalculatePivotsU3Ec__AnonStorey11D_t2905139495  : public Il2CppObject
{
public:
	// Pathfinding.GraphNode Pathfinding.EuclideanEmbedding/<RecalculatePivots>c__AnonStorey11D::first
	GraphNode_t23612370 * ___first_0;

public:
	inline static int32_t get_offset_of_first_0() { return static_cast<int32_t>(offsetof(U3CRecalculatePivotsU3Ec__AnonStorey11D_t2905139495, ___first_0)); }
	inline GraphNode_t23612370 * get_first_0() const { return ___first_0; }
	inline GraphNode_t23612370 ** get_address_of_first_0() { return &___first_0; }
	inline void set_first_0(GraphNode_t23612370 * value)
	{
		___first_0 = value;
		Il2CppCodeGenWriteBarrier(&___first_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
