﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Xml_XmlAttributeGenerated
struct System_Xml_XmlAttributeGenerated_t1586100850;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Xml_XmlAttributeGenerated::.ctor()
extern "C"  void System_Xml_XmlAttributeGenerated__ctor_m2819202153 (System_Xml_XmlAttributeGenerated_t1586100850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlAttributeGenerated::XmlAttribute_BaseURI(JSVCall)
extern "C"  void System_Xml_XmlAttributeGenerated_XmlAttribute_BaseURI_m3911653355 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlAttributeGenerated::XmlAttribute_InnerText(JSVCall)
extern "C"  void System_Xml_XmlAttributeGenerated_XmlAttribute_InnerText_m2783525155 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlAttributeGenerated::XmlAttribute_InnerXml(JSVCall)
extern "C"  void System_Xml_XmlAttributeGenerated_XmlAttribute_InnerXml_m1379486693 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlAttributeGenerated::XmlAttribute_LocalName(JSVCall)
extern "C"  void System_Xml_XmlAttributeGenerated_XmlAttribute_LocalName_m1953089168 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlAttributeGenerated::XmlAttribute_Name(JSVCall)
extern "C"  void System_Xml_XmlAttributeGenerated_XmlAttribute_Name_m3665748603 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlAttributeGenerated::XmlAttribute_NamespaceURI(JSVCall)
extern "C"  void System_Xml_XmlAttributeGenerated_XmlAttribute_NamespaceURI_m2666602933 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlAttributeGenerated::XmlAttribute_NodeType(JSVCall)
extern "C"  void System_Xml_XmlAttributeGenerated_XmlAttribute_NodeType_m1606543306 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlAttributeGenerated::XmlAttribute_OwnerDocument(JSVCall)
extern "C"  void System_Xml_XmlAttributeGenerated_XmlAttribute_OwnerDocument_m3052541240 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlAttributeGenerated::XmlAttribute_OwnerElement(JSVCall)
extern "C"  void System_Xml_XmlAttributeGenerated_XmlAttribute_OwnerElement_m229113341 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlAttributeGenerated::XmlAttribute_ParentNode(JSVCall)
extern "C"  void System_Xml_XmlAttributeGenerated_XmlAttribute_ParentNode_m783145050 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlAttributeGenerated::XmlAttribute_Prefix(JSVCall)
extern "C"  void System_Xml_XmlAttributeGenerated_XmlAttribute_Prefix_m2750052948 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlAttributeGenerated::XmlAttribute_SchemaInfo(JSVCall)
extern "C"  void System_Xml_XmlAttributeGenerated_XmlAttribute_SchemaInfo_m3779313559 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlAttributeGenerated::XmlAttribute_Specified(JSVCall)
extern "C"  void System_Xml_XmlAttributeGenerated_XmlAttribute_Specified_m4105750198 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlAttributeGenerated::XmlAttribute_Value(JSVCall)
extern "C"  void System_Xml_XmlAttributeGenerated_XmlAttribute_Value_m678530069 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlAttributeGenerated::XmlAttribute_AppendChild__XmlNode(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlAttributeGenerated_XmlAttribute_AppendChild__XmlNode_m2264084820 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlAttributeGenerated::XmlAttribute_CloneNode__Boolean(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlAttributeGenerated_XmlAttribute_CloneNode__Boolean_m3741314566 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlAttributeGenerated::XmlAttribute_InsertAfter__XmlNode__XmlNode(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlAttributeGenerated_XmlAttribute_InsertAfter__XmlNode__XmlNode_m2587659688 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlAttributeGenerated::XmlAttribute_InsertBefore__XmlNode__XmlNode(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlAttributeGenerated_XmlAttribute_InsertBefore__XmlNode__XmlNode_m2147210869 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlAttributeGenerated::XmlAttribute_PrependChild__XmlNode(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlAttributeGenerated_XmlAttribute_PrependChild__XmlNode_m1309245424 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlAttributeGenerated::XmlAttribute_RemoveChild__XmlNode(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlAttributeGenerated_XmlAttribute_RemoveChild__XmlNode_m3971378622 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlAttributeGenerated::XmlAttribute_ReplaceChild__XmlNode__XmlNode(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlAttributeGenerated_XmlAttribute_ReplaceChild__XmlNode__XmlNode_m2151254437 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlAttributeGenerated::XmlAttribute_WriteContentTo__XmlWriter(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlAttributeGenerated_XmlAttribute_WriteContentTo__XmlWriter_m2102958778 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlAttributeGenerated::XmlAttribute_WriteTo__XmlWriter(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlAttributeGenerated_XmlAttribute_WriteTo__XmlWriter_m2092382509 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlAttributeGenerated::__Register()
extern "C"  void System_Xml_XmlAttributeGenerated___Register_m1113995774 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlAttributeGenerated::ilo_setStringS1(System.Int32,System.String)
extern "C"  void System_Xml_XmlAttributeGenerated_ilo_setStringS1_m3392777906 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Xml_XmlAttributeGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_Xml_XmlAttributeGenerated_ilo_setObject2_m2265452546 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Xml_XmlAttributeGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_Xml_XmlAttributeGenerated_ilo_getObject3_m3986703208 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
