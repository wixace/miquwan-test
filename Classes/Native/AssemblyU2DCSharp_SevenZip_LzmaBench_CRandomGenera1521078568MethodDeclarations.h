﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.LzmaBench/CRandomGenerator
struct CRandomGenerator_t1521078568;

#include "codegen/il2cpp-codegen.h"

// System.Void SevenZip.LzmaBench/CRandomGenerator::.ctor()
extern "C"  void CRandomGenerator__ctor_m1007042371 (CRandomGenerator_t1521078568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaBench/CRandomGenerator::Init()
extern "C"  void CRandomGenerator_Init_m2477873841 (CRandomGenerator_t1521078568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.LzmaBench/CRandomGenerator::GetRnd()
extern "C"  uint32_t CRandomGenerator_GetRnd_m3792948056 (CRandomGenerator_t1521078568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
