﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtilGenerated/<BehaviourUtil_JSDelayCall_GetDelegate_member8_arg1>c__AnonStorey4D
struct U3CBehaviourUtil_JSDelayCall_GetDelegate_member8_arg1U3Ec__AnonStorey4D_t3278946429;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void BehaviourUtilGenerated/<BehaviourUtil_JSDelayCall_GetDelegate_member8_arg1>c__AnonStorey4D::.ctor()
extern "C"  void U3CBehaviourUtil_JSDelayCall_GetDelegate_member8_arg1U3Ec__AnonStorey4D__ctor_m1238312126 (U3CBehaviourUtil_JSDelayCall_GetDelegate_member8_arg1U3Ec__AnonStorey4D_t3278946429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtilGenerated/<BehaviourUtil_JSDelayCall_GetDelegate_member8_arg1>c__AnonStorey4D::<>m__E(System.Object,System.Object,System.Object)
extern "C"  void U3CBehaviourUtil_JSDelayCall_GetDelegate_member8_arg1U3Ec__AnonStorey4D_U3CU3Em__E_m4021484656 (U3CBehaviourUtil_JSDelayCall_GetDelegate_member8_arg1U3Ec__AnonStorey4D_t3278946429 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
