﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngineManual
struct UnityEngineManual_t3586848925;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Component[]
struct ComponentU5BU5D_t663911650;
// JSComponent[]
struct JSComponentU5BU5D_t1575483645;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// JSComponent
struct JSComponent_t1642894772;
// JSCache/TypeInfo
struct TypeInfo_t3198813374;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_JSComponent1642894772.h"
#include "AssemblyU2DCSharp_JSCache_TypeInfo3198813374.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngineManual::.ctor()
extern "C"  void UnityEngineManual__ctor_m2615809390 (UnityEngineManual_t3586848925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngineManual::.cctor()
extern "C"  void UnityEngineManual__cctor_m3298583551 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngineManual::help_retComArr(JSVCall,UnityEngine.Component[])
extern "C"  void UnityEngineManual_help_retComArr_m3449214354 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, ComponentU5BU5D_t663911650* ___arrRet1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngineManual::help_searchAndRetCom(JSVCall,JSComponent[],System.String)
extern "C"  void UnityEngineManual_help_searchAndRetCom_m3736407890 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, JSComponentU5BU5D_t1575483645* ___jsComs1, String_t* ___typeString2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngineManual::help_searchAndRetComs(JSVCall,JSComponent[],System.String)
extern "C"  void UnityEngineManual_help_searchAndRetComs_m3699546891 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, JSComponentU5BU5D_t1575483645* ___com1, String_t* ___typeString2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngineManual::help_getGoAndType(JSVCall)
extern "C"  void UnityEngineManual_help_getGoAndType_m2980879338 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngineManual::help_getComponentGo(JSVCall)
extern "C"  void UnityEngineManual_help_getComponentGo_m1359718670 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::GameObject_AddComponentT1(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_GameObject_AddComponentT1_m1371043607 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::Component_GetComponentT1(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_Component_GetComponentT1_m3323106040 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::GameObject_GetComponentT1(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_GameObject_GetComponentT1_m1141426946 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::Component_GetComponentsT1(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_Component_GetComponentsT1_m1981418775 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::GameObject_GetComponentsT1(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_GameObject_GetComponentsT1_m3068843597 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::Component_GetComponentInChildrenT1(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_Component_GetComponentInChildrenT1_m47204572 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::GameObject_GetComponentInChildrenT1(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_GameObject_GetComponentInChildrenT1_m3712973926 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::Component_GetComponentsInChildrenT1(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_Component_GetComponentsInChildrenT1_m2600003643 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::GameObject_GetComponentsInChildrenT1(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_GameObject_GetComponentsInChildrenT1_m274736625 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::Component_GetComponentsInChildrenT1__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_Component_GetComponentsInChildrenT1__Boolean_m1440188239 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::GameObject_GetComponentsInChildrenT1__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_GameObject_GetComponentsInChildrenT1__Boolean_m813389401 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::Component_GetComponentInParentT1(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_Component_GetComponentInParentT1_m2076222023 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::GameObject_GetComponentInParentT1(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_GameObject_GetComponentInParentT1_m1722495057 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::Component_GetComponentsInParentT1(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_Component_GetComponentsInParentT1_m3321335142 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::GameObject_GetComponentsInParentT1(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_GameObject_GetComponentsInParentT1_m945733788 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::Component_GetComponentsInParentT1__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_Component_GetComponentsInParentT1__Boolean_m114852484 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::GameObject_GetComponentsInParentT1__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_GameObject_GetComponentsInParentT1__Boolean_m2170063886 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngineManual::initManual()
extern "C"  void UnityEngineManual_initManual_m2849489324 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::isManual(System.String)
extern "C"  bool UnityEngineManual_isManual_m910413168 (Il2CppObject * __this /* static, unused */, String_t* ___functionName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngineManual::afterUse()
extern "C"  void UnityEngineManual_afterUse_m1184995009 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::Object_op_Inequality__Object__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_Object_op_Inequality__Object__Object_m737691661 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::Object_op_Equality__Object__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_Object_op_Equality__Object__Object_m3928092520 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::Vector2_GetHashCode(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_Vector2_GetHashCode_m1812899409 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::Vector2_MoveTowards__Vector2__Vector2__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_Vector2_MoveTowards__Vector2__Vector2__Single_m2660754099 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::Vector3_GetHashCode(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_Vector3_GetHashCode_m3538380306 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::Vector3_MoveTowards__Vector3__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_Vector3_MoveTowards__Vector3__Vector3__Single_m3306437716 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::Vector3_OrthoNormalize__Vector3__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_Vector3_OrthoNormalize__Vector3__Vector3__Vector3_m3425998588 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::Vector3_OrthoNormalize__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_Vector3_OrthoNormalize__Vector3__Vector3_m3117911062 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::Vector3_Project__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_Vector3_Project__Vector3__Vector3_m2975404890 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::Vector3_Reflect__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_Vector3_Reflect__Vector3__Vector3_m1198336766 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::Vector3_RotateTowards__Vector3__Vector3__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_Vector3_RotateTowards__Vector3__Vector3__Single__Single_m3455975634 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::Vector3_Slerp__Vector3__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_Vector3_Slerp__Vector3__Vector3__Single_m1817007091 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::Vector3_SmoothDamp__Vector3__Vector3__Vector3__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_Vector3_SmoothDamp__Vector3__Vector3__Vector3__Single__Single__Single_m2537146747 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::Vector3_SmoothDamp__Vector3__Vector3__Vector3__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_Vector3_SmoothDamp__Vector3__Vector3__Vector3__Single__Single_m2598231347 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::Vector3_SmoothDamp__Vector3__Vector3__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_Vector3_SmoothDamp__Vector3__Vector3__Vector3__Single_m1607280875 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngineManual::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngineManual_ilo_setObject1_m3932263552 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngineManual::ilo_setObject2(System.Int32,System.Int32)
extern "C"  void UnityEngineManual_ilo_setObject2_m1463023916 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::ilo_IsInheritanceRel3(System.String,System.String)
extern "C"  bool UnityEngineManual_ilo_IsInheritanceRel3_m681866391 (Il2CppObject * __this /* static, unused */, String_t* ___baseClassName0, String_t* ___subClassName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngineManual::ilo_moveSaveID2Arr4(System.Int32)
extern "C"  void UnityEngineManual_ilo_moveSaveID2Arr4_m3244602566 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngineManual::ilo_getStringS5(System.Int32)
extern "C"  String_t* UnityEngineManual_ilo_getStringS5_m2345728342 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type UnityEngineManual::ilo_GetTypeByName6(System.String,System.Type)
extern "C"  Type_t * UnityEngineManual_ilo_GetTypeByName6_m4062888770 (Il2CppObject * __this /* static, unused */, String_t* ___typeName0, Type_t * ___defaultType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngineManual::ilo_set_jsFail7(JSComponent,System.Boolean)
extern "C"  void UnityEngineManual_ilo_set_jsFail7_m2733936395 (Il2CppObject * __this /* static, unused */, JSComponent_t1642894772 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngineManual::ilo_init8(JSComponent,System.Boolean)
extern "C"  void UnityEngineManual_ilo_init8_m317355648 (Il2CppObject * __this /* static, unused */, JSComponent_t1642894772 * ____this0, bool ___callSerialize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngineManual::ilo_help_searchAndRetCom9(JSVCall,JSComponent[],System.String)
extern "C"  void UnityEngineManual_ilo_help_searchAndRetCom9_m2416149752 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, JSComponentU5BU5D_t1575483645* ___jsComs1, String_t* ___typeString2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngineManual::ilo_help_getGoAndType10(JSVCall)
extern "C"  void UnityEngineManual_ilo_help_getGoAndType10_m2480292062 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::ilo_get_IsCSMonoBehaviour11(JSCache/TypeInfo)
extern "C"  bool UnityEngineManual_ilo_get_IsCSMonoBehaviour11_m3034780486 (Il2CppObject * __this /* static, unused */, TypeInfo_t3198813374 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngineManual::ilo_help_retComArr12(JSVCall,UnityEngine.Component[])
extern "C"  void UnityEngineManual_ilo_help_retComArr12_m1284811326 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, ComponentU5BU5D_t663911650* ___arrRet1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngineManual::ilo_help_getComponentGo13(JSVCall)
extern "C"  void UnityEngineManual_ilo_help_getComponentGo13_m4118243231 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::ilo_GameObject_GetComponentInChildrenT114(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_ilo_GameObject_GetComponentInChildrenT114_m3792098166 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::ilo_GameObject_GetComponentsInChildrenT115(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_ilo_GameObject_GetComponentsInChildrenT115_m2963927112 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::ilo_GameObject_GetComponentsInParentT116(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_ilo_GameObject_GetComponentsInParentT116_m2940622388 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::ilo_GameObject_GetComponentsInParentT1__Boolean17(JSVCall,System.Int32)
extern "C"  bool UnityEngineManual_ilo_GameObject_GetComponentsInParentT1__Boolean17_m1434757089 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngineManual::ilo_getBooleanS18(System.Int32)
extern "C"  bool UnityEngineManual_ilo_getBooleanS18_m3257265062 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngineManual::ilo_setBooleanS19(System.Int32,System.Boolean)
extern "C"  void UnityEngineManual_ilo_setBooleanS19_m3535833182 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngineManual::ilo_getVector2S20(System.Int32)
extern "C"  Vector2_t4282066565  UnityEngineManual_ilo_getVector2S20_m1858559517 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngineManual::ilo_setVector2S21(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngineManual_ilo_setVector2S21_m3037016259 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngineManual::ilo_getSingle22(System.Int32)
extern "C"  float UnityEngineManual_ilo_getSingle22_m1633092532 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngineManual::ilo_getArgIndex23()
extern "C"  int32_t UnityEngineManual_ilo_getArgIndex23_m4176107962 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngineManual::ilo_setVector3S24(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngineManual_ilo_setVector3S24_m1256328388 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngineManual::ilo_getVector3S25(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngineManual_ilo_getVector3S25_m578481632 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngineManual::ilo_setArgIndex26(System.Int32)
extern "C"  void UnityEngineManual_ilo_setArgIndex26_m2977566028 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
