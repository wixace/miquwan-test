﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GUIContent
struct GUIContent_t2094828418;
// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t2526458961;
// UnityEngine.GUIContent[]
struct GUIContentU5BU5D_t3588725815;
// System.String[]
struct StringU5BU5D_t4054002952;
// UnityEngine.Texture[]
struct TextureU5BU5D_t606176076;
struct GUIContent_t2094828418_marshaled_pinvoke;
struct GUIContent_t2094828418_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_GUIContent2094828418.h"

// System.Void UnityEngine.GUIContent::.ctor()
extern "C"  void GUIContent__ctor_m923375087 (GUIContent_t2094828418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::.ctor(System.String)
extern "C"  void GUIContent__ctor_m174155123 (GUIContent_t2094828418 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::.ctor(UnityEngine.Texture)
extern "C"  void GUIContent__ctor_m2309015839 (GUIContent_t2094828418 * __this, Texture_t2526458961 * ___image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::.ctor(System.String,UnityEngine.Texture)
extern "C"  void GUIContent__ctor_m1972813595 (GUIContent_t2094828418 * __this, String_t* ___text0, Texture_t2526458961 * ___image1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::.ctor(System.String,System.String)
extern "C"  void GUIContent__ctor_m3415640175 (GUIContent_t2094828418 * __this, String_t* ___text0, String_t* ___tooltip1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::.ctor(UnityEngine.Texture,System.String)
extern "C"  void GUIContent__ctor_m1779961627 (GUIContent_t2094828418 * __this, Texture_t2526458961 * ___image0, String_t* ___tooltip1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::.ctor(System.String,UnityEngine.Texture,System.String)
extern "C"  void GUIContent__ctor_m905567255 (GUIContent_t2094828418 * __this, String_t* ___text0, Texture_t2526458961 * ___image1, String_t* ___tooltip2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::.ctor(UnityEngine.GUIContent)
extern "C"  void GUIContent__ctor_m3148225754 (GUIContent_t2094828418 * __this, GUIContent_t2094828418 * ___src0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::.cctor()
extern "C"  void GUIContent__cctor_m2372727710 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUIContent::get_text()
extern "C"  String_t* GUIContent_get_text_m3944801774 (GUIContent_t2094828418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::set_text(System.String)
extern "C"  void GUIContent_set_text_m1575840163 (GUIContent_t2094828418 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.GUIContent::get_image()
extern "C"  Texture_t2526458961 * GUIContent_get_image_m3045375856 (GUIContent_t2094828418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::set_image(UnityEngine.Texture)
extern "C"  void GUIContent_set_image_m3694827683 (GUIContent_t2094828418 * __this, Texture_t2526458961 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUIContent::get_tooltip()
extern "C"  String_t* GUIContent_get_tooltip_m1388301636 (GUIContent_t2094828418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::set_tooltip(System.String)
extern "C"  void GUIContent_set_tooltip_m3547225103 (GUIContent_t2094828418 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GUIContent::get_hash()
extern "C"  int32_t GUIContent_get_hash_m3969306220 (GUIContent_t2094828418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIContent UnityEngine.GUIContent::Temp(System.String)
extern "C"  GUIContent_t2094828418 * GUIContent_Temp_m2857440895 (Il2CppObject * __this /* static, unused */, String_t* ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIContent UnityEngine.GUIContent::Temp(UnityEngine.Texture)
extern "C"  GUIContent_t2094828418 * GUIContent_Temp_m3826417963 (Il2CppObject * __this /* static, unused */, Texture_t2526458961 * ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::ClearStaticCache()
extern "C"  void GUIContent_ClearStaticCache_m2388858588 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIContent[] UnityEngine.GUIContent::Temp(System.String[])
extern "C"  GUIContentU5BU5D_t3588725815* GUIContent_Temp_m3875603455 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t4054002952* ___texts0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIContent[] UnityEngine.GUIContent::Temp(UnityEngine.Texture[])
extern "C"  GUIContentU5BU5D_t3588725815* GUIContent_Temp_m199676459 (Il2CppObject * __this /* static, unused */, TextureU5BU5D_t606176076* ___images0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct GUIContent_t2094828418;
struct GUIContent_t2094828418_marshaled_pinvoke;

extern "C" void GUIContent_t2094828418_marshal_pinvoke(const GUIContent_t2094828418& unmarshaled, GUIContent_t2094828418_marshaled_pinvoke& marshaled);
extern "C" void GUIContent_t2094828418_marshal_pinvoke_back(const GUIContent_t2094828418_marshaled_pinvoke& marshaled, GUIContent_t2094828418& unmarshaled);
extern "C" void GUIContent_t2094828418_marshal_pinvoke_cleanup(GUIContent_t2094828418_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct GUIContent_t2094828418;
struct GUIContent_t2094828418_marshaled_com;

extern "C" void GUIContent_t2094828418_marshal_com(const GUIContent_t2094828418& unmarshaled, GUIContent_t2094828418_marshaled_com& marshaled);
extern "C" void GUIContent_t2094828418_marshal_com_back(const GUIContent_t2094828418_marshaled_com& marshaled, GUIContent_t2094828418& unmarshaled);
extern "C" void GUIContent_t2094828418_marshal_com_cleanup(GUIContent_t2094828418_marshaled_com& marshaled);
