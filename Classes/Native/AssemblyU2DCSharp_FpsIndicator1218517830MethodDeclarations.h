﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FpsIndicator
struct FpsIndicator_t1218517830;

#include "codegen/il2cpp-codegen.h"

// System.Void FpsIndicator::.ctor()
extern "C"  void FpsIndicator__ctor_m4014614037 (FpsIndicator_t1218517830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FpsIndicator::Start()
extern "C"  void FpsIndicator_Start_m2961751829 (FpsIndicator_t1218517830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FpsIndicator::Update()
extern "C"  void FpsIndicator_Update_m1625845656 (FpsIndicator_t1218517830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FpsIndicator::OnGUI()
extern "C"  void FpsIndicator_OnGUI_m3510012687 (FpsIndicator_t1218517830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
