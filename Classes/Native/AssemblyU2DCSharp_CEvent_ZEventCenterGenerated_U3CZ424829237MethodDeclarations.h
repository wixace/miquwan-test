﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CEvent_ZEventCenterGenerated/<ZEventCenter_AddEventListener_GetDelegate_member1_arg1>c__AnonStorey53
struct U3CZEventCenter_AddEventListener_GetDelegate_member1_arg1U3Ec__AnonStorey53_t424829237;
// CEvent.ZEvent
struct ZEvent_t3638018500;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"

// System.Void CEvent_ZEventCenterGenerated/<ZEventCenter_AddEventListener_GetDelegate_member1_arg1>c__AnonStorey53::.ctor()
extern "C"  void U3CZEventCenter_AddEventListener_GetDelegate_member1_arg1U3Ec__AnonStorey53__ctor_m3541275910 (U3CZEventCenter_AddEventListener_GetDelegate_member1_arg1U3Ec__AnonStorey53_t424829237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent_ZEventCenterGenerated/<ZEventCenter_AddEventListener_GetDelegate_member1_arg1>c__AnonStorey53::<>m__1B(CEvent.ZEvent)
extern "C"  void U3CZEventCenter_AddEventListener_GetDelegate_member1_arg1U3Ec__AnonStorey53_U3CU3Em__1B_m3032934459 (U3CZEventCenter_AddEventListener_GetDelegate_member1_arg1U3Ec__AnonStorey53_t424829237 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
