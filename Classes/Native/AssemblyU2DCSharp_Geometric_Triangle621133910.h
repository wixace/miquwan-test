﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Geometric/Triangle
struct  Triangle_t621133910 
{
public:
	// System.Single Geometric/Triangle::x1
	float ___x1_0;
	// System.Single Geometric/Triangle::y1
	float ___y1_1;
	// System.Single Geometric/Triangle::x2
	float ___x2_2;
	// System.Single Geometric/Triangle::y2
	float ___y2_3;
	// System.Single Geometric/Triangle::x3
	float ___x3_4;
	// System.Single Geometric/Triangle::y3
	float ___y3_5;

public:
	inline static int32_t get_offset_of_x1_0() { return static_cast<int32_t>(offsetof(Triangle_t621133910, ___x1_0)); }
	inline float get_x1_0() const { return ___x1_0; }
	inline float* get_address_of_x1_0() { return &___x1_0; }
	inline void set_x1_0(float value)
	{
		___x1_0 = value;
	}

	inline static int32_t get_offset_of_y1_1() { return static_cast<int32_t>(offsetof(Triangle_t621133910, ___y1_1)); }
	inline float get_y1_1() const { return ___y1_1; }
	inline float* get_address_of_y1_1() { return &___y1_1; }
	inline void set_y1_1(float value)
	{
		___y1_1 = value;
	}

	inline static int32_t get_offset_of_x2_2() { return static_cast<int32_t>(offsetof(Triangle_t621133910, ___x2_2)); }
	inline float get_x2_2() const { return ___x2_2; }
	inline float* get_address_of_x2_2() { return &___x2_2; }
	inline void set_x2_2(float value)
	{
		___x2_2 = value;
	}

	inline static int32_t get_offset_of_y2_3() { return static_cast<int32_t>(offsetof(Triangle_t621133910, ___y2_3)); }
	inline float get_y2_3() const { return ___y2_3; }
	inline float* get_address_of_y2_3() { return &___y2_3; }
	inline void set_y2_3(float value)
	{
		___y2_3 = value;
	}

	inline static int32_t get_offset_of_x3_4() { return static_cast<int32_t>(offsetof(Triangle_t621133910, ___x3_4)); }
	inline float get_x3_4() const { return ___x3_4; }
	inline float* get_address_of_x3_4() { return &___x3_4; }
	inline void set_x3_4(float value)
	{
		___x3_4 = value;
	}

	inline static int32_t get_offset_of_y3_5() { return static_cast<int32_t>(offsetof(Triangle_t621133910, ___y3_5)); }
	inline float get_y3_5() const { return ___y3_5; }
	inline float* get_address_of_y3_5() { return &___y3_5; }
	inline void set_y3_5(float value)
	{
		___y3_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Geometric/Triangle
struct Triangle_t621133910_marshaled_pinvoke
{
	float ___x1_0;
	float ___y1_1;
	float ___x2_2;
	float ___y2_3;
	float ___x3_4;
	float ___y3_5;
};
// Native definition for marshalling of: Geometric/Triangle
struct Triangle_t621133910_marshaled_com
{
	float ___x1_0;
	float ___y1_1;
	float ___x2_2;
	float ___y2_3;
	float ___x3_4;
	float ___y3_5;
};
