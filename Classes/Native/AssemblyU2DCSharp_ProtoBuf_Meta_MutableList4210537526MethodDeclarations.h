﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Meta.MutableList
struct MutableList_t4210537526;
// System.Object
struct Il2CppObject;
// ProtoBuf.Meta.BasicList/Node
struct Node_t1496877195;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_BasicList_Node1496877195.h"

// System.Void ProtoBuf.Meta.MutableList::.ctor()
extern "C"  void MutableList__ctor_m866856109 (MutableList_t4210537526 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.MutableList::get_Item(System.Int32)
extern "C"  Il2CppObject * MutableList_get_Item_m2008934415 (MutableList_t4210537526 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MutableList::set_Item(System.Int32,System.Object)
extern "C"  void MutableList_set_Item_m1966518374 (MutableList_t4210537526 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MutableList::RemoveLast()
extern "C"  void MutableList_RemoveLast_m2819661105 (MutableList_t4210537526 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MutableList::Clear()
extern "C"  void MutableList_Clear_m2567956696 (MutableList_t4210537526 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MutableList::ilo_RemoveLastWithMutate1(ProtoBuf.Meta.BasicList/Node)
extern "C"  void MutableList_ilo_RemoveLastWithMutate1_m1423108280 (Il2CppObject * __this /* static, unused */, Node_t1496877195 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MutableList::ilo_Clear2(ProtoBuf.Meta.BasicList/Node)
extern "C"  void MutableList_ilo_Clear2_m2510650308 (Il2CppObject * __this /* static, unused */, Node_t1496877195 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
