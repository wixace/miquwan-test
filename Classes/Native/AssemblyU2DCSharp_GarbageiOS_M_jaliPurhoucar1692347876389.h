﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_jaliPurhoucar169
struct  M_jaliPurhoucar169_t2347876389  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_jaliPurhoucar169::_jooyetror
	uint32_t ____jooyetror_0;
	// System.Int32 GarbageiOS.M_jaliPurhoucar169::_rormou
	int32_t ____rormou_1;
	// System.String GarbageiOS.M_jaliPurhoucar169::_taneNumou
	String_t* ____taneNumou_2;
	// System.Boolean GarbageiOS.M_jaliPurhoucar169::_whiswhiSeesairgas
	bool ____whiswhiSeesairgas_3;
	// System.Boolean GarbageiOS.M_jaliPurhoucar169::_naycoStouhene
	bool ____naycoStouhene_4;
	// System.Boolean GarbageiOS.M_jaliPurhoucar169::_zassoopemMouha
	bool ____zassoopemMouha_5;
	// System.UInt32 GarbageiOS.M_jaliPurhoucar169::_kirpeserTowmai
	uint32_t ____kirpeserTowmai_6;
	// System.String GarbageiOS.M_jaliPurhoucar169::_fuqooStowpemwha
	String_t* ____fuqooStowpemwha_7;
	// System.Single GarbageiOS.M_jaliPurhoucar169::_bute
	float ____bute_8;
	// System.Int32 GarbageiOS.M_jaliPurhoucar169::_trelrojawBaswhe
	int32_t ____trelrojawBaswhe_9;
	// System.Single GarbageiOS.M_jaliPurhoucar169::_falcoutoo
	float ____falcoutoo_10;
	// System.String GarbageiOS.M_jaliPurhoucar169::_kostaBemgere
	String_t* ____kostaBemgere_11;
	// System.String GarbageiOS.M_jaliPurhoucar169::_voocal
	String_t* ____voocal_12;
	// System.String GarbageiOS.M_jaliPurhoucar169::_dahow
	String_t* ____dahow_13;

public:
	inline static int32_t get_offset_of__jooyetror_0() { return static_cast<int32_t>(offsetof(M_jaliPurhoucar169_t2347876389, ____jooyetror_0)); }
	inline uint32_t get__jooyetror_0() const { return ____jooyetror_0; }
	inline uint32_t* get_address_of__jooyetror_0() { return &____jooyetror_0; }
	inline void set__jooyetror_0(uint32_t value)
	{
		____jooyetror_0 = value;
	}

	inline static int32_t get_offset_of__rormou_1() { return static_cast<int32_t>(offsetof(M_jaliPurhoucar169_t2347876389, ____rormou_1)); }
	inline int32_t get__rormou_1() const { return ____rormou_1; }
	inline int32_t* get_address_of__rormou_1() { return &____rormou_1; }
	inline void set__rormou_1(int32_t value)
	{
		____rormou_1 = value;
	}

	inline static int32_t get_offset_of__taneNumou_2() { return static_cast<int32_t>(offsetof(M_jaliPurhoucar169_t2347876389, ____taneNumou_2)); }
	inline String_t* get__taneNumou_2() const { return ____taneNumou_2; }
	inline String_t** get_address_of__taneNumou_2() { return &____taneNumou_2; }
	inline void set__taneNumou_2(String_t* value)
	{
		____taneNumou_2 = value;
		Il2CppCodeGenWriteBarrier(&____taneNumou_2, value);
	}

	inline static int32_t get_offset_of__whiswhiSeesairgas_3() { return static_cast<int32_t>(offsetof(M_jaliPurhoucar169_t2347876389, ____whiswhiSeesairgas_3)); }
	inline bool get__whiswhiSeesairgas_3() const { return ____whiswhiSeesairgas_3; }
	inline bool* get_address_of__whiswhiSeesairgas_3() { return &____whiswhiSeesairgas_3; }
	inline void set__whiswhiSeesairgas_3(bool value)
	{
		____whiswhiSeesairgas_3 = value;
	}

	inline static int32_t get_offset_of__naycoStouhene_4() { return static_cast<int32_t>(offsetof(M_jaliPurhoucar169_t2347876389, ____naycoStouhene_4)); }
	inline bool get__naycoStouhene_4() const { return ____naycoStouhene_4; }
	inline bool* get_address_of__naycoStouhene_4() { return &____naycoStouhene_4; }
	inline void set__naycoStouhene_4(bool value)
	{
		____naycoStouhene_4 = value;
	}

	inline static int32_t get_offset_of__zassoopemMouha_5() { return static_cast<int32_t>(offsetof(M_jaliPurhoucar169_t2347876389, ____zassoopemMouha_5)); }
	inline bool get__zassoopemMouha_5() const { return ____zassoopemMouha_5; }
	inline bool* get_address_of__zassoopemMouha_5() { return &____zassoopemMouha_5; }
	inline void set__zassoopemMouha_5(bool value)
	{
		____zassoopemMouha_5 = value;
	}

	inline static int32_t get_offset_of__kirpeserTowmai_6() { return static_cast<int32_t>(offsetof(M_jaliPurhoucar169_t2347876389, ____kirpeserTowmai_6)); }
	inline uint32_t get__kirpeserTowmai_6() const { return ____kirpeserTowmai_6; }
	inline uint32_t* get_address_of__kirpeserTowmai_6() { return &____kirpeserTowmai_6; }
	inline void set__kirpeserTowmai_6(uint32_t value)
	{
		____kirpeserTowmai_6 = value;
	}

	inline static int32_t get_offset_of__fuqooStowpemwha_7() { return static_cast<int32_t>(offsetof(M_jaliPurhoucar169_t2347876389, ____fuqooStowpemwha_7)); }
	inline String_t* get__fuqooStowpemwha_7() const { return ____fuqooStowpemwha_7; }
	inline String_t** get_address_of__fuqooStowpemwha_7() { return &____fuqooStowpemwha_7; }
	inline void set__fuqooStowpemwha_7(String_t* value)
	{
		____fuqooStowpemwha_7 = value;
		Il2CppCodeGenWriteBarrier(&____fuqooStowpemwha_7, value);
	}

	inline static int32_t get_offset_of__bute_8() { return static_cast<int32_t>(offsetof(M_jaliPurhoucar169_t2347876389, ____bute_8)); }
	inline float get__bute_8() const { return ____bute_8; }
	inline float* get_address_of__bute_8() { return &____bute_8; }
	inline void set__bute_8(float value)
	{
		____bute_8 = value;
	}

	inline static int32_t get_offset_of__trelrojawBaswhe_9() { return static_cast<int32_t>(offsetof(M_jaliPurhoucar169_t2347876389, ____trelrojawBaswhe_9)); }
	inline int32_t get__trelrojawBaswhe_9() const { return ____trelrojawBaswhe_9; }
	inline int32_t* get_address_of__trelrojawBaswhe_9() { return &____trelrojawBaswhe_9; }
	inline void set__trelrojawBaswhe_9(int32_t value)
	{
		____trelrojawBaswhe_9 = value;
	}

	inline static int32_t get_offset_of__falcoutoo_10() { return static_cast<int32_t>(offsetof(M_jaliPurhoucar169_t2347876389, ____falcoutoo_10)); }
	inline float get__falcoutoo_10() const { return ____falcoutoo_10; }
	inline float* get_address_of__falcoutoo_10() { return &____falcoutoo_10; }
	inline void set__falcoutoo_10(float value)
	{
		____falcoutoo_10 = value;
	}

	inline static int32_t get_offset_of__kostaBemgere_11() { return static_cast<int32_t>(offsetof(M_jaliPurhoucar169_t2347876389, ____kostaBemgere_11)); }
	inline String_t* get__kostaBemgere_11() const { return ____kostaBemgere_11; }
	inline String_t** get_address_of__kostaBemgere_11() { return &____kostaBemgere_11; }
	inline void set__kostaBemgere_11(String_t* value)
	{
		____kostaBemgere_11 = value;
		Il2CppCodeGenWriteBarrier(&____kostaBemgere_11, value);
	}

	inline static int32_t get_offset_of__voocal_12() { return static_cast<int32_t>(offsetof(M_jaliPurhoucar169_t2347876389, ____voocal_12)); }
	inline String_t* get__voocal_12() const { return ____voocal_12; }
	inline String_t** get_address_of__voocal_12() { return &____voocal_12; }
	inline void set__voocal_12(String_t* value)
	{
		____voocal_12 = value;
		Il2CppCodeGenWriteBarrier(&____voocal_12, value);
	}

	inline static int32_t get_offset_of__dahow_13() { return static_cast<int32_t>(offsetof(M_jaliPurhoucar169_t2347876389, ____dahow_13)); }
	inline String_t* get__dahow_13() const { return ____dahow_13; }
	inline String_t** get_address_of__dahow_13() { return &____dahow_13; }
	inline void set__dahow_13(String_t* value)
	{
		____dahow_13 = value;
		Il2CppCodeGenWriteBarrier(&____dahow_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
