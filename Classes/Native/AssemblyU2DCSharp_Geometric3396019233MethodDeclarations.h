﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Geometric_Triangle621133910.h"
#include "AssemblyU2DCSharp_Geometric_Point2499604994.h"
#include "AssemblyU2DCSharp_Geometric_Rectangle4010590433.h"
#include "AssemblyU2DCSharp_Geometric_Circle4095848094.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Boolean Geometric::IsWithinTriangle(Geometric/Triangle,Geometric/Point)
extern "C"  bool Geometric_IsWithinTriangle_m179496023 (Il2CppObject * __this /* static, unused */, Triangle_t621133910  ___g0, Point_t2499604994  ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Geometric::IsWithinRect(Geometric/Rectangle,Geometric/Point)
extern "C"  bool Geometric_IsWithinRect_m3090687068 (Il2CppObject * __this /* static, unused */, Rectangle_t4010590433  ___r0, Point_t2499604994  ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Geometric::IsWithinCircle(Geometric/Circle,Geometric/Point)
extern "C"  bool Geometric_IsWithinCircle_m1808291031 (Il2CppObject * __this /* static, unused */, Circle_t4095848094  ___c0, Point_t2499604994  ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Geometric::Multiply(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  float Geometric_Multiply_m2822501938 (Il2CppObject * __this /* static, unused */, float ___p1x0, float ___p1y1, float ___p2x2, float ___p2y3, float ___p0x4, float ___p0y5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Geometric::IsWithinRect(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Geometric_IsWithinRect_m1343336166 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___point0, Vector3_t4282066566  ___v01, Vector3_t4282066566  ___v12, Vector3_t4282066566  ___v23, Vector3_t4282066566  ___v34, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Geometric/Triangle Geometric::GetTriangle(Geometric/Point,System.Single,System.Single,System.Single)
extern "C"  Triangle_t621133910  Geometric_GetTriangle_m1915712146 (Il2CppObject * __this /* static, unused */, Point_t2499604994  ___p0, float ___radian11, float ___angle2, float ___middle_length3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Geometric::IsWithinSector(UnityEngine.Vector3,System.Single,System.Single,System.Single,UnityEngine.Vector3)
extern "C"  bool Geometric_IsWithinSector_m516839514 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___centerPos0, float ___radius1, float ___angle2, float ___middle_line_angle_from_x3, Vector3_t4282066566  ___pos4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Geometric::ilo_Multiply1(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  float Geometric_ilo_Multiply1_m598908950 (Il2CppObject * __this /* static, unused */, float ___p1x0, float ___p1y1, float ___p2x2, float ___p2y3, float ___p0x4, float ___p0y5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Geometric::ilo_IsWithinCircle2(Geometric/Circle,Geometric/Point)
extern "C"  bool Geometric_ilo_IsWithinCircle2_m1766807010 (Il2CppObject * __this /* static, unused */, Circle_t4095848094  ___c0, Point_t2499604994  ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Geometric::ilo_IsWithinTriangle3(Geometric/Triangle,Geometric/Point)
extern "C"  bool Geometric_ilo_IsWithinTriangle3_m3901611123 (Il2CppObject * __this /* static, unused */, Triangle_t621133910  ___g0, Point_t2499604994  ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
