﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t3991598821;
// Pathfinding.PathHandler
struct PathHandler_t918952263;
// AstarPath
struct AstarPath_t4090270936;
// Pathfinding.Path
struct Path_t1974241691;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_PathThreadInfo2420662483.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AstarPath/<CalculatePaths>c__IteratorD
struct  U3CCalculatePathsU3Ec__IteratorD_t1648267364  : public Il2CppObject
{
public:
	// System.Object AstarPath/<CalculatePaths>c__IteratorD::_threadInfo
	Il2CppObject * ____threadInfo_0;
	// Pathfinding.PathThreadInfo AstarPath/<CalculatePaths>c__IteratorD::<threadInfo>__0
	PathThreadInfo_t2420662483  ___U3CthreadInfoU3E__0_1;
	// System.Exception AstarPath/<CalculatePaths>c__IteratorD::<e>__1
	Exception_t3991598821 * ___U3CeU3E__1_2;
	// System.Int32 AstarPath/<CalculatePaths>c__IteratorD::<numPaths>__2
	int32_t ___U3CnumPathsU3E__2_3;
	// Pathfinding.PathHandler AstarPath/<CalculatePaths>c__IteratorD::<runData>__3
	PathHandler_t918952263 * ___U3CrunDataU3E__3_4;
	// AstarPath AstarPath/<CalculatePaths>c__IteratorD::<astar>__4
	AstarPath_t4090270936 * ___U3CastarU3E__4_5;
	// System.Int64 AstarPath/<CalculatePaths>c__IteratorD::<maxTicks>__5
	int64_t ___U3CmaxTicksU3E__5_6;
	// System.Int64 AstarPath/<CalculatePaths>c__IteratorD::<targetTick>__6
	int64_t ___U3CtargetTickU3E__6_7;
	// Pathfinding.Path AstarPath/<CalculatePaths>c__IteratorD::<p>__7
	Path_t1974241691 * ___U3CpU3E__7_8;
	// System.Boolean AstarPath/<CalculatePaths>c__IteratorD::<blockedBefore>__8
	bool ___U3CblockedBeforeU3E__8_9;
	// System.Int64 AstarPath/<CalculatePaths>c__IteratorD::<startTicks>__9
	int64_t ___U3CstartTicksU3E__9_10;
	// System.Int64 AstarPath/<CalculatePaths>c__IteratorD::<totalTicks>__10
	int64_t ___U3CtotalTicksU3E__10_11;
	// System.Int32 AstarPath/<CalculatePaths>c__IteratorD::$PC
	int32_t ___U24PC_12;
	// System.Object AstarPath/<CalculatePaths>c__IteratorD::$current
	Il2CppObject * ___U24current_13;
	// System.Object AstarPath/<CalculatePaths>c__IteratorD::<$>_threadInfo
	Il2CppObject * ___U3CU24U3E_threadInfo_14;

public:
	inline static int32_t get_offset_of__threadInfo_0() { return static_cast<int32_t>(offsetof(U3CCalculatePathsU3Ec__IteratorD_t1648267364, ____threadInfo_0)); }
	inline Il2CppObject * get__threadInfo_0() const { return ____threadInfo_0; }
	inline Il2CppObject ** get_address_of__threadInfo_0() { return &____threadInfo_0; }
	inline void set__threadInfo_0(Il2CppObject * value)
	{
		____threadInfo_0 = value;
		Il2CppCodeGenWriteBarrier(&____threadInfo_0, value);
	}

	inline static int32_t get_offset_of_U3CthreadInfoU3E__0_1() { return static_cast<int32_t>(offsetof(U3CCalculatePathsU3Ec__IteratorD_t1648267364, ___U3CthreadInfoU3E__0_1)); }
	inline PathThreadInfo_t2420662483  get_U3CthreadInfoU3E__0_1() const { return ___U3CthreadInfoU3E__0_1; }
	inline PathThreadInfo_t2420662483 * get_address_of_U3CthreadInfoU3E__0_1() { return &___U3CthreadInfoU3E__0_1; }
	inline void set_U3CthreadInfoU3E__0_1(PathThreadInfo_t2420662483  value)
	{
		___U3CthreadInfoU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CeU3E__1_2() { return static_cast<int32_t>(offsetof(U3CCalculatePathsU3Ec__IteratorD_t1648267364, ___U3CeU3E__1_2)); }
	inline Exception_t3991598821 * get_U3CeU3E__1_2() const { return ___U3CeU3E__1_2; }
	inline Exception_t3991598821 ** get_address_of_U3CeU3E__1_2() { return &___U3CeU3E__1_2; }
	inline void set_U3CeU3E__1_2(Exception_t3991598821 * value)
	{
		___U3CeU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CeU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CnumPathsU3E__2_3() { return static_cast<int32_t>(offsetof(U3CCalculatePathsU3Ec__IteratorD_t1648267364, ___U3CnumPathsU3E__2_3)); }
	inline int32_t get_U3CnumPathsU3E__2_3() const { return ___U3CnumPathsU3E__2_3; }
	inline int32_t* get_address_of_U3CnumPathsU3E__2_3() { return &___U3CnumPathsU3E__2_3; }
	inline void set_U3CnumPathsU3E__2_3(int32_t value)
	{
		___U3CnumPathsU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CrunDataU3E__3_4() { return static_cast<int32_t>(offsetof(U3CCalculatePathsU3Ec__IteratorD_t1648267364, ___U3CrunDataU3E__3_4)); }
	inline PathHandler_t918952263 * get_U3CrunDataU3E__3_4() const { return ___U3CrunDataU3E__3_4; }
	inline PathHandler_t918952263 ** get_address_of_U3CrunDataU3E__3_4() { return &___U3CrunDataU3E__3_4; }
	inline void set_U3CrunDataU3E__3_4(PathHandler_t918952263 * value)
	{
		___U3CrunDataU3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrunDataU3E__3_4, value);
	}

	inline static int32_t get_offset_of_U3CastarU3E__4_5() { return static_cast<int32_t>(offsetof(U3CCalculatePathsU3Ec__IteratorD_t1648267364, ___U3CastarU3E__4_5)); }
	inline AstarPath_t4090270936 * get_U3CastarU3E__4_5() const { return ___U3CastarU3E__4_5; }
	inline AstarPath_t4090270936 ** get_address_of_U3CastarU3E__4_5() { return &___U3CastarU3E__4_5; }
	inline void set_U3CastarU3E__4_5(AstarPath_t4090270936 * value)
	{
		___U3CastarU3E__4_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CastarU3E__4_5, value);
	}

	inline static int32_t get_offset_of_U3CmaxTicksU3E__5_6() { return static_cast<int32_t>(offsetof(U3CCalculatePathsU3Ec__IteratorD_t1648267364, ___U3CmaxTicksU3E__5_6)); }
	inline int64_t get_U3CmaxTicksU3E__5_6() const { return ___U3CmaxTicksU3E__5_6; }
	inline int64_t* get_address_of_U3CmaxTicksU3E__5_6() { return &___U3CmaxTicksU3E__5_6; }
	inline void set_U3CmaxTicksU3E__5_6(int64_t value)
	{
		___U3CmaxTicksU3E__5_6 = value;
	}

	inline static int32_t get_offset_of_U3CtargetTickU3E__6_7() { return static_cast<int32_t>(offsetof(U3CCalculatePathsU3Ec__IteratorD_t1648267364, ___U3CtargetTickU3E__6_7)); }
	inline int64_t get_U3CtargetTickU3E__6_7() const { return ___U3CtargetTickU3E__6_7; }
	inline int64_t* get_address_of_U3CtargetTickU3E__6_7() { return &___U3CtargetTickU3E__6_7; }
	inline void set_U3CtargetTickU3E__6_7(int64_t value)
	{
		___U3CtargetTickU3E__6_7 = value;
	}

	inline static int32_t get_offset_of_U3CpU3E__7_8() { return static_cast<int32_t>(offsetof(U3CCalculatePathsU3Ec__IteratorD_t1648267364, ___U3CpU3E__7_8)); }
	inline Path_t1974241691 * get_U3CpU3E__7_8() const { return ___U3CpU3E__7_8; }
	inline Path_t1974241691 ** get_address_of_U3CpU3E__7_8() { return &___U3CpU3E__7_8; }
	inline void set_U3CpU3E__7_8(Path_t1974241691 * value)
	{
		___U3CpU3E__7_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpU3E__7_8, value);
	}

	inline static int32_t get_offset_of_U3CblockedBeforeU3E__8_9() { return static_cast<int32_t>(offsetof(U3CCalculatePathsU3Ec__IteratorD_t1648267364, ___U3CblockedBeforeU3E__8_9)); }
	inline bool get_U3CblockedBeforeU3E__8_9() const { return ___U3CblockedBeforeU3E__8_9; }
	inline bool* get_address_of_U3CblockedBeforeU3E__8_9() { return &___U3CblockedBeforeU3E__8_9; }
	inline void set_U3CblockedBeforeU3E__8_9(bool value)
	{
		___U3CblockedBeforeU3E__8_9 = value;
	}

	inline static int32_t get_offset_of_U3CstartTicksU3E__9_10() { return static_cast<int32_t>(offsetof(U3CCalculatePathsU3Ec__IteratorD_t1648267364, ___U3CstartTicksU3E__9_10)); }
	inline int64_t get_U3CstartTicksU3E__9_10() const { return ___U3CstartTicksU3E__9_10; }
	inline int64_t* get_address_of_U3CstartTicksU3E__9_10() { return &___U3CstartTicksU3E__9_10; }
	inline void set_U3CstartTicksU3E__9_10(int64_t value)
	{
		___U3CstartTicksU3E__9_10 = value;
	}

	inline static int32_t get_offset_of_U3CtotalTicksU3E__10_11() { return static_cast<int32_t>(offsetof(U3CCalculatePathsU3Ec__IteratorD_t1648267364, ___U3CtotalTicksU3E__10_11)); }
	inline int64_t get_U3CtotalTicksU3E__10_11() const { return ___U3CtotalTicksU3E__10_11; }
	inline int64_t* get_address_of_U3CtotalTicksU3E__10_11() { return &___U3CtotalTicksU3E__10_11; }
	inline void set_U3CtotalTicksU3E__10_11(int64_t value)
	{
		___U3CtotalTicksU3E__10_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CCalculatePathsU3Ec__IteratorD_t1648267364, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}

	inline static int32_t get_offset_of_U24current_13() { return static_cast<int32_t>(offsetof(U3CCalculatePathsU3Ec__IteratorD_t1648267364, ___U24current_13)); }
	inline Il2CppObject * get_U24current_13() const { return ___U24current_13; }
	inline Il2CppObject ** get_address_of_U24current_13() { return &___U24current_13; }
	inline void set_U24current_13(Il2CppObject * value)
	{
		___U24current_13 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_13, value);
	}

	inline static int32_t get_offset_of_U3CU24U3E_threadInfo_14() { return static_cast<int32_t>(offsetof(U3CCalculatePathsU3Ec__IteratorD_t1648267364, ___U3CU24U3E_threadInfo_14)); }
	inline Il2CppObject * get_U3CU24U3E_threadInfo_14() const { return ___U3CU24U3E_threadInfo_14; }
	inline Il2CppObject ** get_address_of_U3CU24U3E_threadInfo_14() { return &___U3CU24U3E_threadInfo_14; }
	inline void set_U3CU24U3E_threadInfo_14(Il2CppObject * value)
	{
		___U3CU24U3E_threadInfo_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3E_threadInfo_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
