﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIButtonActivateGenerated
struct UIButtonActivateGenerated_t4167110550;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// UIButtonActivate
struct UIButtonActivate_t4170568185;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_UIButtonActivate4170568185.h"

// System.Void UIButtonActivateGenerated::.ctor()
extern "C"  void UIButtonActivateGenerated__ctor_m1910267541 (UIButtonActivateGenerated_t4167110550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonActivateGenerated::UIButtonActivate_UIButtonActivate1(JSVCall,System.Int32)
extern "C"  bool UIButtonActivateGenerated_UIButtonActivate_UIButtonActivate1_m14518365 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonActivateGenerated::UIButtonActivate_target(JSVCall)
extern "C"  void UIButtonActivateGenerated_UIButtonActivate_target_m3129593405 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonActivateGenerated::UIButtonActivate_state(JSVCall)
extern "C"  void UIButtonActivateGenerated_UIButtonActivate_state_m1664522221 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonActivateGenerated::UIButtonActivate_Set__Boolean(JSVCall,System.Int32)
extern "C"  bool UIButtonActivateGenerated_UIButtonActivate_Set__Boolean_m2254298691 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonActivateGenerated::__Register()
extern "C"  void UIButtonActivateGenerated___Register_m588378962 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonActivateGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UIButtonActivateGenerated_ilo_attachFinalizerObject1_m1747394306 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIButtonActivateGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIButtonActivateGenerated_ilo_setObject2_m1151535610 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIButtonActivateGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIButtonActivateGenerated_ilo_getObject3_m3952919538 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonActivateGenerated::ilo_Set4(UIButtonActivate,System.Boolean)
extern "C"  void UIButtonActivateGenerated_ilo_Set4_m3913682530 (Il2CppObject * __this /* static, unused */, UIButtonActivate_t4170568185 * ____this0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
