﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.MemoryStream
struct MemoryStream_t418716369;
// System.IO.BinaryWriter
struct BinaryWriter_t4146364100;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ByteWriteArray
struct  ByteWriteArray_t2165677314  : public Il2CppObject
{
public:
	// System.IO.MemoryStream ByteWriteArray::m_Stream
	MemoryStream_t418716369 * ___m_Stream_0;
	// System.IO.BinaryWriter ByteWriteArray::m_Writer
	BinaryWriter_t4146364100 * ___m_Writer_1;

public:
	inline static int32_t get_offset_of_m_Stream_0() { return static_cast<int32_t>(offsetof(ByteWriteArray_t2165677314, ___m_Stream_0)); }
	inline MemoryStream_t418716369 * get_m_Stream_0() const { return ___m_Stream_0; }
	inline MemoryStream_t418716369 ** get_address_of_m_Stream_0() { return &___m_Stream_0; }
	inline void set_m_Stream_0(MemoryStream_t418716369 * value)
	{
		___m_Stream_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Stream_0, value);
	}

	inline static int32_t get_offset_of_m_Writer_1() { return static_cast<int32_t>(offsetof(ByteWriteArray_t2165677314, ___m_Writer_1)); }
	inline BinaryWriter_t4146364100 * get_m_Writer_1() const { return ___m_Writer_1; }
	inline BinaryWriter_t4146364100 ** get_address_of_m_Writer_1() { return &___m_Writer_1; }
	inline void set_m_Writer_1(BinaryWriter_t4146364100 * value)
	{
		___m_Writer_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_Writer_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
