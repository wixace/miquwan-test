﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Ionic.Zlib.WorkItem
struct WorkItem_t4048658636;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_Ionic_Zip_Reduced_Pathfinding_Ionic_Zl3197845446.h"
#include "Pathfinding_Ionic_Zip_Reduced_Pathfinding_Ionic_Zl2182485191.h"

// System.Void Pathfinding.Ionic.Zlib.WorkItem::.ctor(System.Int32,Pathfinding.Ionic.Zlib.CompressionLevel,Pathfinding.Ionic.Zlib.CompressionStrategy,System.Int32)
extern "C"  void WorkItem__ctor_m3620382620 (WorkItem_t4048658636 * __this, int32_t ___size0, int32_t ___compressLevel1, int32_t ___strategy2, int32_t ___ix3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
