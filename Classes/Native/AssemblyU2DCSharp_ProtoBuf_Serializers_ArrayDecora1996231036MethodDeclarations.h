﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.ArrayDecorator
struct ArrayDecorator_t1996231036;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// ProtoBuf.Serializers.IProtoSerializer
struct IProtoSerializer_t3033312651;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// ProtoBuf.Meta.BasicList
struct BasicList_t528018366;
// System.Array
struct Il2CppArray;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "AssemblyU2DCSharp_ProtoBuf_WireType2355646059.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "AssemblyU2DCSharp_ProtoBuf_SubItemToken3365128146.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_BasicList528018366.h"
#include "mscorlib_System_Array1146569071.h"

// System.Void ProtoBuf.Serializers.ArrayDecorator::.ctor(ProtoBuf.Meta.TypeModel,ProtoBuf.Serializers.IProtoSerializer,System.Int32,System.Boolean,ProtoBuf.WireType,System.Type,System.Boolean,System.Boolean)
extern "C"  void ArrayDecorator__ctor_m2782852190 (ArrayDecorator_t1996231036 * __this, TypeModel_t2730011105 * ___model0, Il2CppObject * ___tail1, int32_t ___fieldNumber2, bool ___writePacked3, int32_t ___packedWireType4, Type_t * ___arrayType5, bool ___overwriteList6, bool ___supportNull7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.ArrayDecorator::get_ExpectedType()
extern "C"  Type_t * ArrayDecorator_get_ExpectedType_m1859600268 (ArrayDecorator_t1996231036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.ArrayDecorator::get_RequiresOldValue()
extern "C"  bool ArrayDecorator_get_RequiresOldValue_m354183416 (ArrayDecorator_t1996231036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.ArrayDecorator::get_ReturnsValue()
extern "C"  bool ArrayDecorator_get_ReturnsValue_m2007209870 (ArrayDecorator_t1996231036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.ArrayDecorator::get_AppendToCollection()
extern "C"  bool ArrayDecorator_get_AppendToCollection_m766846163 (ArrayDecorator_t1996231036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.ArrayDecorator::get_SupportNull()
extern "C"  bool ArrayDecorator_get_SupportNull_m355814776 (ArrayDecorator_t1996231036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.ArrayDecorator::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void ArrayDecorator_Write_m870826000 (ArrayDecorator_t1996231036 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.ArrayDecorator::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * ArrayDecorator_Read_m2800401334 (ArrayDecorator_t1996231036 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.ArrayDecorator::ilo_WriteFieldHeader1(System.Int32,ProtoBuf.WireType,ProtoBuf.ProtoWriter)
extern "C"  void ArrayDecorator_ilo_WriteFieldHeader1_m921289990 (Il2CppObject * __this /* static, unused */, int32_t ___fieldNumber0, int32_t ___wireType1, ProtoWriter_t4117914721 * ___writer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.SubItemToken ProtoBuf.Serializers.ArrayDecorator::ilo_StartSubItem2(System.Object,ProtoBuf.ProtoWriter)
extern "C"  SubItemToken_t3365128146  ArrayDecorator_ilo_StartSubItem2_m3689557129 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___instance0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.ArrayDecorator::ilo_SetPackedField3(System.Int32,ProtoBuf.ProtoWriter)
extern "C"  void ArrayDecorator_ilo_SetPackedField3_m3214236486 (Il2CppObject * __this /* static, unused */, int32_t ___fieldNumber0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.ArrayDecorator::ilo_Write4(ProtoBuf.Serializers.IProtoSerializer,System.Object,ProtoBuf.ProtoWriter)
extern "C"  void ArrayDecorator_ilo_Write4_m1113128418 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Il2CppObject * ___value1, ProtoWriter_t4117914721 * ___dest2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Serializers.ArrayDecorator::ilo_get_FieldNumber5(ProtoBuf.ProtoReader)
extern "C"  int32_t ArrayDecorator_ilo_get_FieldNumber5_m1276265059 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.ArrayDecorator::ilo_Read6(ProtoBuf.Serializers.IProtoSerializer,System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * ArrayDecorator_ilo_Read6_m244043544 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Il2CppObject * ___value1, ProtoReader_t3962509489 * ___source2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.ArrayDecorator::ilo_HasSubValue7(ProtoBuf.WireType,ProtoBuf.ProtoReader)
extern "C"  bool ArrayDecorator_ilo_HasSubValue7_m3415378852 (Il2CppObject * __this /* static, unused */, int32_t ___wireType0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Serializers.ArrayDecorator::ilo_Add8(ProtoBuf.Meta.BasicList,System.Object)
extern "C"  int32_t ArrayDecorator_ilo_Add8_m864348403 (Il2CppObject * __this /* static, unused */, BasicList_t528018366 * ____this0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.ArrayDecorator::ilo_CopyTo9(ProtoBuf.Meta.BasicList,System.Array,System.Int32)
extern "C"  void ArrayDecorator_ilo_CopyTo9_m1914651626 (Il2CppObject * __this /* static, unused */, BasicList_t528018366 * ____this0, Il2CppArray * ___array1, int32_t ___offset2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
