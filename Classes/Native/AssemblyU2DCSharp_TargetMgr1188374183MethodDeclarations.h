﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TargetMgr
struct TargetMgr_t1188374183;
// CombatEntity
struct CombatEntity_t684137495;
// TargetMgr/FightPoint
struct FightPoint_t4216057832;
// System.Collections.Generic.List`1<TargetMgr/AngleEntity>
struct List_1_t267346398;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// AstarPath
struct AstarPath_t4090270936;
// NpcMgr
struct NpcMgr_t2339534743;
// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;
// HeroMgr
struct HeroMgr_t2475965342;
// TargetMgr/AngleEntity
struct AngleEntity_t3194128142;
// System.Collections.Generic.List`1<JSCMapPathPointInfoConfig>
struct List_1_t4071866601;
// LevelConfigManager
struct LevelConfigManager_t657947911;
// JSCMapPathPointInfoConfig
struct JSCMapPathPointInfoConfig_t2703681049;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_Pathfinding_NNInfo1570625892.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "AssemblyU2DCSharp_NpcMgr2339534743.h"
#include "AssemblyU2DCSharp_TargetMgr1188374183.h"
#include "AssemblyU2DCSharp_TargetMgr_AngleEntity3194128142.h"
#include "AssemblyU2DCSharp_LevelConfigManager657947911.h"
#include "AssemblyU2DCSharp_JSCMapPathPointInfoConfig2703681049.h"

// System.Void TargetMgr::.ctor()
extern "C"  void TargetMgr__ctor_m1486453412 (TargetMgr_t1188374183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetMgr::.cctor()
extern "C"  void TargetMgr__cctor_m2648286601 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TargetMgr TargetMgr::get_instance()
extern "C"  TargetMgr_t1188374183 * TargetMgr_get_instance_m2480544722 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetMgr::Init()
extern "C"  void TargetMgr_Init_m1107865392 (TargetMgr_t1188374183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetMgr::ClearTargetFightPoint(CombatEntity)
extern "C"  void TargetMgr_ClearTargetFightPoint_m1247483593 (TargetMgr_t1188374183 * __this, CombatEntity_t684137495 * ____entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TargetMgr/FightPoint TargetMgr::GetTargetPoint(CombatEntity)
extern "C"  FightPoint_t4216057832 * TargetMgr_GetTargetPoint_m3081451837 (TargetMgr_t1188374183 * __this, CombatEntity_t684137495 * ____entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TargetMgr::isConnect(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool TargetMgr_isConnect_m588385974 (TargetMgr_t1188374183 * __this, Vector3_t4282066566  ___startPoint0, Vector3_t4282066566  ___endPoint1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TargetMgr::GetTargetPointInConnect(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  TargetMgr_GetTargetPointInConnect_m2935425076 (TargetMgr_t1188374183 * __this, Vector3_t4282066566  ___startPoint0, Vector3_t4282066566  ___endPoint1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TargetMgr::CheckTargetPosition(CombatEntity,System.Single,System.Single)
extern "C"  Vector3_t4282066566  TargetMgr_CheckTargetPosition_m693776337 (TargetMgr_t1188374183 * __this, CombatEntity_t684137495 * ___launchEntity0, float ___distance1, float ___EndToTargetMinDis2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<TargetMgr/AngleEntity> TargetMgr::AddAngleEntity(System.Int32)
extern "C"  List_1_t267346398 * TargetMgr_AddAngleEntity_m3093858935 (TargetMgr_t1188374183 * __this, int32_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetMgr::AddFightPoint(CombatEntity,System.Int32,System.Int32)
extern "C"  void TargetMgr_AddFightPoint_m534591372 (TargetMgr_t1188374183 * __this, CombatEntity_t684137495 * ___launchEntity0, int32_t ___nearcount1, int32_t ___farcount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TargetMgr::GetAngle(System.Int32)
extern "C"  float TargetMgr_GetAngle_m3998941442 (TargetMgr_t1188374183 * __this, int32_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TargetMgr::FromAndToCenterlinePoint(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t4282066566  TargetMgr_FromAndToCenterlinePoint_m2077654860 (TargetMgr_t1188374183 * __this, Vector3_t4282066566  ___form0, Vector3_t4282066566  ___to1, float ___distance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetMgr::CreateMapPathPoints()
extern "C"  void TargetMgr_CreateMapPathPoints_m2571979850 (TargetMgr_t1188374183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TargetMgr::ilo_get_Walkable1(Pathfinding.GraphNode)
extern "C"  bool TargetMgr_ilo_get_Walkable1_m2785039748 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo TargetMgr::ilo_GetNearest2(AstarPath,UnityEngine.Vector3)
extern "C"  NNInfo_t1570625892  TargetMgr_ilo_GetNearest2_m4111156239 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, Vector3_t4282066566  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NpcMgr TargetMgr::ilo_get_instance3()
extern "C"  NpcMgr_t2339534743 * TargetMgr_ilo_get_instance3_m3041091050 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> TargetMgr::ilo_GetAllFriendNpc4(NpcMgr)
extern "C"  List_1_t2052323047 * TargetMgr_ilo_GetAllFriendNpc4_m820967534 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroMgr TargetMgr::ilo_get_instance5()
extern "C"  HeroMgr_t2475965342 * TargetMgr_ilo_get_instance5_m303263003 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TargetMgr::ilo_get_canMove6(CombatEntity)
extern "C"  bool TargetMgr_ilo_get_canMove6_m4117810918 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity TargetMgr::ilo_get_atkTarget7(CombatEntity)
extern "C"  CombatEntity_t684137495 * TargetMgr_ilo_get_atkTarget7_m3185216077 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TargetMgr::ilo_get_atkType8(CombatEntity)
extern "C"  int32_t TargetMgr_ilo_get_atkType8_m2078935805 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TargetMgr::ilo_get_position9(CombatEntity)
extern "C"  Vector3_t4282066566  TargetMgr_ilo_get_position9_m1158846681 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TargetMgr::ilo_isConnect10(TargetMgr,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool TargetMgr_ilo_isConnect10_m2640535183 (Il2CppObject * __this /* static, unused */, TargetMgr_t1188374183 * ____this0, Vector3_t4282066566  ___startPoint1, Vector3_t4282066566  ___endPoint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetMgr::ilo_set_angleEntity11(CombatEntity,TargetMgr/AngleEntity)
extern "C"  void TargetMgr_ilo_set_angleEntity11_m3034577103 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, AngleEntity_t3194128142 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<TargetMgr/AngleEntity> TargetMgr::ilo_AddAngleEntity12(TargetMgr,System.Int32)
extern "C"  List_1_t267346398 * TargetMgr_ilo_AddAngleEntity12_m3096872198 (Il2CppObject * __this /* static, unused */, TargetMgr_t1188374183 * ____this0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<JSCMapPathPointInfoConfig> TargetMgr::ilo_GetMapathPointInfos13(LevelConfigManager,System.Int32)
extern "C"  List_1_t4071866601 * TargetMgr_ilo_GetMapathPointInfos13_m3318484271 (Il2CppObject * __this /* static, unused */, LevelConfigManager_t657947911 * ____this0, int32_t ___level1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TargetMgr::ilo_get_id14(JSCMapPathPointInfoConfig)
extern "C"  int32_t TargetMgr_ilo_get_id14_m3481873805 (Il2CppObject * __this /* static, unused */, JSCMapPathPointInfoConfig_t2703681049 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TargetMgr::ilo_get_z15(JSCMapPathPointInfoConfig)
extern "C"  float TargetMgr_ilo_get_z15_m1475892649 (Il2CppObject * __this /* static, unused */, JSCMapPathPointInfoConfig_t2703681049 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TargetMgr::ilo_get_angle16(JSCMapPathPointInfoConfig)
extern "C"  float TargetMgr_ilo_get_angle16_m560029647 (Il2CppObject * __this /* static, unused */, JSCMapPathPointInfoConfig_t2703681049 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
