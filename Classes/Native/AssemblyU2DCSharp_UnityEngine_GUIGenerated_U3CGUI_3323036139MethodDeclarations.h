﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUIGenerated/<GUI_ModalWindow_GetDelegate_member53_arg2>c__AnonStoreyE7
struct U3CGUI_ModalWindow_GetDelegate_member53_arg2U3Ec__AnonStoreyE7_t3323036139;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_GUIGenerated/<GUI_ModalWindow_GetDelegate_member53_arg2>c__AnonStoreyE7::.ctor()
extern "C"  void U3CGUI_ModalWindow_GetDelegate_member53_arg2U3Ec__AnonStoreyE7__ctor_m4107094496 (U3CGUI_ModalWindow_GetDelegate_member53_arg2U3Ec__AnonStoreyE7_t3323036139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated/<GUI_ModalWindow_GetDelegate_member53_arg2>c__AnonStoreyE7::<>m__1B8(System.Int32)
extern "C"  void U3CGUI_ModalWindow_GetDelegate_member53_arg2U3Ec__AnonStoreyE7_U3CU3Em__1B8_m187673105 (U3CGUI_ModalWindow_GetDelegate_member53_arg2U3Ec__AnonStoreyE7_t3323036139 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
