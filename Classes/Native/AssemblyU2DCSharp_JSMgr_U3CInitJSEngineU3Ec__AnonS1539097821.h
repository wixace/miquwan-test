﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// JSMgr/OnInitJSEngine
struct OnInitJSEngine_t2416054170;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSMgr/<InitJSEngine>c__AnonStoreyFC
struct  U3CInitJSEngineU3Ec__AnonStoreyFC_t1539097821  : public Il2CppObject
{
public:
	// System.Boolean JSMgr/<InitJSEngine>c__AnonStoreyFC::ret
	bool ___ret_0;
	// JSMgr/OnInitJSEngine JSMgr/<InitJSEngine>c__AnonStoreyFC::onInitJSEngine
	OnInitJSEngine_t2416054170 * ___onInitJSEngine_1;

public:
	inline static int32_t get_offset_of_ret_0() { return static_cast<int32_t>(offsetof(U3CInitJSEngineU3Ec__AnonStoreyFC_t1539097821, ___ret_0)); }
	inline bool get_ret_0() const { return ___ret_0; }
	inline bool* get_address_of_ret_0() { return &___ret_0; }
	inline void set_ret_0(bool value)
	{
		___ret_0 = value;
	}

	inline static int32_t get_offset_of_onInitJSEngine_1() { return static_cast<int32_t>(offsetof(U3CInitJSEngineU3Ec__AnonStoreyFC_t1539097821, ___onInitJSEngine_1)); }
	inline OnInitJSEngine_t2416054170 * get_onInitJSEngine_1() const { return ___onInitJSEngine_1; }
	inline OnInitJSEngine_t2416054170 ** get_address_of_onInitJSEngine_1() { return &___onInitJSEngine_1; }
	inline void set_onInitJSEngine_1(OnInitJSEngine_t2416054170 * value)
	{
		___onInitJSEngine_1 = value;
		Il2CppCodeGenWriteBarrier(&___onInitJSEngine_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
