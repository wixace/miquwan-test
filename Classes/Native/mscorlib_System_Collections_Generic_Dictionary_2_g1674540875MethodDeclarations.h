﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>
struct Dictionary_2_t1674540875;
// System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>
struct IEqualityComparer_1_t2682787083;
// System.Collections.Generic.IDictionary`2<UIModelDisplayType,System.Object>
struct IDictionary_2_t1252414220;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<UIModelDisplayType>
struct ICollection_1_t2786342666;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t770439062;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>[]
struct KeyValuePair_2U5BU5D_t2563117120;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>>
struct IEnumerator_1_t3485186630;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Object>
struct KeyCollection_t3301300326;
// System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Object>
struct ValueCollection_t375146588;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21573321581.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_UIModelDisplayType1891752679.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2991864267.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m2207730996_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m2207730996(__this, method) ((  void (*) (Dictionary_2_t1674540875 *, const MethodInfo*))Dictionary_2__ctor_m2207730996_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3801739691_gshared (Dictionary_2_t1674540875 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3801739691(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t1674540875 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3801739691_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m1905539300_gshared (Dictionary_2_t1674540875 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m1905539300(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t1674540875 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1905539300_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m3409368197_gshared (Dictionary_2_t1674540875 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m3409368197(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t1674540875 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3409368197_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1762102681_gshared (Dictionary_2_t1674540875 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1762102681(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t1674540875 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1762102681_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2864732277_gshared (Dictionary_2_t1674540875 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m2864732277(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1674540875 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m2864732277_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m927103204_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m927103204(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1674540875 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m927103204_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2066774464_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2066774464(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1674540875 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2066774464_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m4142730098_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m4142730098(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1674540875 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m4142730098_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m2096495584_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m2096495584(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1674540875 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m2096495584_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2438688745_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2438688745(__this, method) ((  bool (*) (Dictionary_2_t1674540875 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2438688745_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m394128880_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m394128880(__this, method) ((  bool (*) (Dictionary_2_t1674540875 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m394128880_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m3182515722_gshared (Dictionary_2_t1674540875 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m3182515722(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1674540875 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m3182515722_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m1222269049_gshared (Dictionary_2_t1674540875 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1222269049(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1674540875 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1222269049_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3054681656_gshared (Dictionary_2_t1674540875 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m3054681656(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1674540875 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3054681656_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m3946577146_gshared (Dictionary_2_t1674540875 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m3946577146(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1674540875 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m3946577146_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m391570615_gshared (Dictionary_2_t1674540875 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m391570615(__this, ___key0, method) ((  void (*) (Dictionary_2_t1674540875 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m391570615_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1116562586_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1116562586(__this, method) ((  bool (*) (Dictionary_2_t1674540875 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1116562586_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1418878540_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1418878540(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1674540875 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1418878540_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1619626078_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1619626078(__this, method) ((  bool (*) (Dictionary_2_t1674540875 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1619626078_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3736308173_gshared (Dictionary_2_t1674540875 * __this, KeyValuePair_2_t1573321581  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3736308173(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t1674540875 *, KeyValuePair_2_t1573321581 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3736308173_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2088441049_gshared (Dictionary_2_t1674540875 * __this, KeyValuePair_2_t1573321581  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2088441049(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1674540875 *, KeyValuePair_2_t1573321581 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2088441049_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1691385969_gshared (Dictionary_2_t1674540875 * __this, KeyValuePair_2U5BU5D_t2563117120* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1691385969(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1674540875 *, KeyValuePair_2U5BU5D_t2563117120*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1691385969_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3981090366_gshared (Dictionary_2_t1674540875 * __this, KeyValuePair_2_t1573321581  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3981090366(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1674540875 *, KeyValuePair_2_t1573321581 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3981090366_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m1001595536_gshared (Dictionary_2_t1674540875 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1001595536(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1674540875 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1001595536_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3893231391_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3893231391(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1674540875 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3893231391_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3784229078_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3784229078(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1674540875 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3784229078_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1153092451_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1153092451(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1674540875 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1153092451_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m2830948948_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m2830948948(__this, method) ((  int32_t (*) (Dictionary_2_t1674540875 *, const MethodInfo*))Dictionary_2_get_Count_m2830948948_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m1870284851_gshared (Dictionary_2_t1674540875 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m1870284851(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1674540875 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m1870284851_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m248666100_gshared (Dictionary_2_t1674540875 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m248666100(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1674540875 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m248666100_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m298353836_gshared (Dictionary_2_t1674540875 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m298353836(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t1674540875 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m298353836_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m2456312971_gshared (Dictionary_2_t1674540875 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m2456312971(__this, ___size0, method) ((  void (*) (Dictionary_2_t1674540875 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2456312971_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m2889673031_gshared (Dictionary_2_t1674540875 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m2889673031(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1674540875 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2889673031_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t1573321581  Dictionary_2_make_pair_m4019096795_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m4019096795(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t1573321581  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m4019096795_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m3431741315_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m3431741315(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m3431741315_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m3289262495_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m3289262495(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m3289262495_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m2879821992_gshared (Dictionary_2_t1674540875 * __this, KeyValuePair_2U5BU5D_t2563117120* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m2879821992(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1674540875 *, KeyValuePair_2U5BU5D_t2563117120*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m2879821992_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m4072948100_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m4072948100(__this, method) ((  void (*) (Dictionary_2_t1674540875 *, const MethodInfo*))Dictionary_2_Resize_m4072948100_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m3091300865_gshared (Dictionary_2_t1674540875 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m3091300865(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1674540875 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m3091300865_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m3908831583_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m3908831583(__this, method) ((  void (*) (Dictionary_2_t1674540875 *, const MethodInfo*))Dictionary_2_Clear_m3908831583_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m1251474633_gshared (Dictionary_2_t1674540875 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1251474633(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1674540875 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m1251474633_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m94749769_gshared (Dictionary_2_t1674540875 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m94749769(__this, ___value0, method) ((  bool (*) (Dictionary_2_t1674540875 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m94749769_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m3027262162_gshared (Dictionary_2_t1674540875 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m3027262162(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1674540875 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m3027262162_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m1047664338_gshared (Dictionary_2_t1674540875 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1047664338(__this, ___sender0, method) ((  void (*) (Dictionary_2_t1674540875 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1047664338_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m3194856071_gshared (Dictionary_2_t1674540875 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m3194856071(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1674540875 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m3194856071_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m2586016802_gshared (Dictionary_2_t1674540875 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m2586016802(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t1674540875 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m2586016802_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::get_Keys()
extern "C"  KeyCollection_t3301300326 * Dictionary_2_get_Keys_m3898484025_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m3898484025(__this, method) ((  KeyCollection_t3301300326 * (*) (Dictionary_2_t1674540875 *, const MethodInfo*))Dictionary_2_get_Keys_m3898484025_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::get_Values()
extern "C"  ValueCollection_t375146588 * Dictionary_2_get_Values_m874702741_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m874702741(__this, method) ((  ValueCollection_t375146588 * (*) (Dictionary_2_t1674540875 *, const MethodInfo*))Dictionary_2_get_Values_m874702741_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m2881600222_gshared (Dictionary_2_t1674540875 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m2881600222(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t1674540875 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m2881600222_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m1601576634_gshared (Dictionary_2_t1674540875 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1601576634(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t1674540875 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1601576634_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m1115101290_gshared (Dictionary_2_t1674540875 * __this, KeyValuePair_2_t1573321581  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1115101290(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t1674540875 *, KeyValuePair_2_t1573321581 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1115101290_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2991864267  Dictionary_2_GetEnumerator_m1602013183_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1602013183(__this, method) ((  Enumerator_t2991864267  (*) (Dictionary_2_t1674540875 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1602013183_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m2775660470_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m2775660470(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m2775660470_gshared)(__this /* static, unused */, ___key0, ___value1, method)
