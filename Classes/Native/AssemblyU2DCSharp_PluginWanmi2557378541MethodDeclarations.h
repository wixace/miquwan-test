﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginWanmi
struct PluginWanmi_t2557378541;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Char[]
struct CharU5BU5D_t3324145743;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// System.Action
struct Action_t3771233898;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_PluginWanmi2557378541.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "System_Core_System_Action3771233898.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void PluginWanmi::.ctor()
extern "C"  void PluginWanmi__ctor_m3986265630 (PluginWanmi_t2557378541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanmi::Init()
extern "C"  void PluginWanmi_Init_m3543809142 (PluginWanmi_t2557378541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginWanmi::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginWanmi_ReqSDKHttpLogin_m1559889423 (PluginWanmi_t2557378541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginWanmi::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginWanmi_IsLoginSuccess_m1313120141 (PluginWanmi_t2557378541 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanmi::OpenUserLogin()
extern "C"  void PluginWanmi_OpenUserLogin_m4150377072 (PluginWanmi_t2557378541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanmi::UserPay(CEvent.ZEvent)
extern "C"  void PluginWanmi_UserPay_m201341698 (PluginWanmi_t2557378541 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanmi::LoginUpinfo(CEvent.ZEvent)
extern "C"  void PluginWanmi_LoginUpinfo_m2854968877 (PluginWanmi_t2557378541 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanmi::RolelvUpinfo(CEvent.ZEvent)
extern "C"  void PluginWanmi_RolelvUpinfo_m2453029452 (PluginWanmi_t2557378541 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanmi::OnLoginSuccess(System.String)
extern "C"  void PluginWanmi_OnLoginSuccess_m856883875 (PluginWanmi_t2557378541 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanmi::OnLogoutSuccess(System.String)
extern "C"  void PluginWanmi_OnLogoutSuccess_m797139020 (PluginWanmi_t2557378541 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanmi::OnPaySuccess(System.String)
extern "C"  void PluginWanmi_OnPaySuccess_m4279218594 (PluginWanmi_t2557378541 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanmi::OnPayFail(System.String)
extern "C"  void PluginWanmi_OnPayFail_m670692063 (PluginWanmi_t2557378541 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanmi::init(System.String,System.String,System.String)
extern "C"  void PluginWanmi_init_m1946855396 (PluginWanmi_t2557378541 * __this, String_t* ___appId0, String_t* ___appKey1, String_t* ___appStoreId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanmi::login()
extern "C"  void PluginWanmi_login_m3508280453 (PluginWanmi_t2557378541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanmi::logout()
extern "C"  void PluginWanmi_logout_m1388334352 (PluginWanmi_t2557378541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanmi::pay(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginWanmi_pay_m4014567786 (PluginWanmi_t2557378541 * __this, String_t* ___goodsId0, String_t* ___price1, String_t* ___multiple2, String_t* ___extraData3, String_t* ___cpOrderId4, String_t* ___itemId5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanmi::loginUpinfo(System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginWanmi_loginUpinfo_m2593983364 (PluginWanmi_t2557378541 * __this, String_t* ___serverid0, String_t* ___servername1, String_t* ___rileid2, String_t* ___rolename3, String_t* ___rolegrade4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanmi::rolelvUpinfo(System.String,System.String)
extern "C"  void PluginWanmi_rolelvUpinfo_m2078369775 (PluginWanmi_t2557378541 * __this, String_t* ___roleid0, String_t* ___rolegrade1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanmi::<OnLogoutSuccess>m__463()
extern "C"  void PluginWanmi_U3COnLogoutSuccessU3Em__463_m3367490496 (PluginWanmi_t2557378541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanmi::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginWanmi_ilo_AddEventListener1_m641601302 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginWanmi::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginWanmi_ilo_get_Instance2_m2592490746 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] PluginWanmi::ilo_SplitStr3(System.String,System.Char[])
extern "C"  StringU5BU5D_t4054002952* PluginWanmi_ilo_SplitStr3_m2432273265 (Il2CppObject * __this /* static, unused */, String_t* ___sFile0, CharU5BU5D_t3324145743* ___separator1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanmi::ilo_init4(PluginWanmi,System.String,System.String,System.String)
extern "C"  void PluginWanmi_ilo_init4_m2045077196 (Il2CppObject * __this /* static, unused */, PluginWanmi_t2557378541 * ____this0, String_t* ___appId1, String_t* ___appKey2, String_t* ___appStoreId3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginWanmi::ilo_get_DeviceID5(PluginsSdkMgr)
extern "C"  String_t* PluginWanmi_ilo_get_DeviceID5_m910349165 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginWanmi::ilo_get_isSdkLogin6(VersionMgr)
extern "C"  bool PluginWanmi_ilo_get_isSdkLogin6_m416242705 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginWanmi::ilo_GetProductID7(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginWanmi_ilo_GetProductID7_m1373367155 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanmi::ilo_ReqSDKHttpLogin8(PluginsSdkMgr)
extern "C"  void PluginWanmi_ilo_ReqSDKHttpLogin8_m31768904 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanmi::ilo_Logout9(System.Action)
extern "C"  void PluginWanmi_ilo_Logout9_m803109413 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanmi::ilo_Log10(System.Object,System.Boolean)
extern "C"  void PluginWanmi_ilo_Log10_m2180370741 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanmi::ilo_DispatchEvent11(CEvent.ZEvent)
extern "C"  void PluginWanmi_ilo_DispatchEvent11_m4235136146 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginWanmi::ilo_OpenUserLogin12(PluginWanmi)
extern "C"  void PluginWanmi_ilo_OpenUserLogin12_m2807769511 (Il2CppObject * __this /* static, unused */, PluginWanmi_t2557378541 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
