﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t538875265;
// UnityEngine.UI.Text
struct Text_t9039225;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleShopItem
struct  SimpleShopItem_t4136890139  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Image SimpleShopItem::itemImage
	Image_t538875265 * ___itemImage_2;
	// UnityEngine.UI.Text SimpleShopItem::titleText
	Text_t9039225 * ___titleText_3;
	// UnityEngine.UI.Text SimpleShopItem::priceText
	Text_t9039225 * ___priceText_4;

public:
	inline static int32_t get_offset_of_itemImage_2() { return static_cast<int32_t>(offsetof(SimpleShopItem_t4136890139, ___itemImage_2)); }
	inline Image_t538875265 * get_itemImage_2() const { return ___itemImage_2; }
	inline Image_t538875265 ** get_address_of_itemImage_2() { return &___itemImage_2; }
	inline void set_itemImage_2(Image_t538875265 * value)
	{
		___itemImage_2 = value;
		Il2CppCodeGenWriteBarrier(&___itemImage_2, value);
	}

	inline static int32_t get_offset_of_titleText_3() { return static_cast<int32_t>(offsetof(SimpleShopItem_t4136890139, ___titleText_3)); }
	inline Text_t9039225 * get_titleText_3() const { return ___titleText_3; }
	inline Text_t9039225 ** get_address_of_titleText_3() { return &___titleText_3; }
	inline void set_titleText_3(Text_t9039225 * value)
	{
		___titleText_3 = value;
		Il2CppCodeGenWriteBarrier(&___titleText_3, value);
	}

	inline static int32_t get_offset_of_priceText_4() { return static_cast<int32_t>(offsetof(SimpleShopItem_t4136890139, ___priceText_4)); }
	inline Text_t9039225 * get_priceText_4() const { return ___priceText_4; }
	inline Text_t9039225 ** get_address_of_priceText_4() { return &___priceText_4; }
	inline void set_priceText_4(Text_t9039225 * value)
	{
		___priceText_4 = value;
		Il2CppCodeGenWriteBarrier(&___priceText_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
