﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_PointerEventDataGenerated
struct UnityEngine_EventSystems_PointerEventDataGenerated_t1494499611;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated__ctor_m526609888 (UnityEngine_EventSystems_PointerEventDataGenerated_t1494499611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_PointerEventDataGenerated::PointerEventData_PointerEventData1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_PointerEventDataGenerated_PointerEventData_PointerEventData1_m2631391898 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::PointerEventData_hovered(JSVCall)
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated_PointerEventData_hovered_m2192936544 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::PointerEventData_pointerEnter(JSVCall)
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated_PointerEventData_pointerEnter_m1247179030 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::PointerEventData_lastPress(JSVCall)
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated_PointerEventData_lastPress_m1252136174 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::PointerEventData_rawPointerPress(JSVCall)
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated_PointerEventData_rawPointerPress_m773352781 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::PointerEventData_pointerDrag(JSVCall)
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated_PointerEventData_pointerDrag_m2349266538 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::PointerEventData_pointerCurrentRaycast(JSVCall)
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated_PointerEventData_pointerCurrentRaycast_m1743254126 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::PointerEventData_pointerPressRaycast(JSVCall)
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated_PointerEventData_pointerPressRaycast_m2153483064 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::PointerEventData_eligibleForClick(JSVCall)
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated_PointerEventData_eligibleForClick_m146756443 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::PointerEventData_pointerId(JSVCall)
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated_PointerEventData_pointerId_m672665699 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::PointerEventData_position(JSVCall)
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated_PointerEventData_position_m686926792 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::PointerEventData_delta(JSVCall)
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated_PointerEventData_delta_m1668727651 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::PointerEventData_pressPosition(JSVCall)
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated_PointerEventData_pressPosition_m2502580751 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::PointerEventData_clickTime(JSVCall)
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated_PointerEventData_clickTime_m905488582 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::PointerEventData_clickCount(JSVCall)
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated_PointerEventData_clickCount_m1206227850 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::PointerEventData_scrollDelta(JSVCall)
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated_PointerEventData_scrollDelta_m2404124624 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::PointerEventData_useDragThreshold(JSVCall)
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated_PointerEventData_useDragThreshold_m1096251009 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::PointerEventData_dragging(JSVCall)
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated_PointerEventData_dragging_m2432283234 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::PointerEventData_button(JSVCall)
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated_PointerEventData_button_m37924511 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::PointerEventData_enterEventCamera(JSVCall)
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated_PointerEventData_enterEventCamera_m1508466506 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::PointerEventData_pressEventCamera(JSVCall)
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated_PointerEventData_pressEventCamera_m117709109 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::PointerEventData_pointerPress(JSVCall)
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated_PointerEventData_pointerPress_m1582087179 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_PointerEventDataGenerated::PointerEventData_IsPointerMoving(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_PointerEventDataGenerated_PointerEventData_IsPointerMoving_m4140782993 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_PointerEventDataGenerated::PointerEventData_IsScrolling(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_PointerEventDataGenerated_PointerEventData_IsScrolling_m1749579163 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_PointerEventDataGenerated::PointerEventData_ToString(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_PointerEventDataGenerated_PointerEventData_ToString_m2813070334 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated___Register_m3935812327 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_EventSystems_PointerEventDataGenerated::ilo_getObject1(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_EventSystems_PointerEventDataGenerated_ilo_getObject1_m778661583 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated_ilo_addJSCSRel2_m2292807965 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_EventSystems_PointerEventDataGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_EventSystems_PointerEventDataGenerated_ilo_setObject3_m1372838316 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::ilo_setInt324(System.Int32,System.Int32)
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated_ilo_setInt324_m2842458131 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::ilo_setVector2S5(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated_ilo_setVector2S5_m2855651275 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine_EventSystems_PointerEventDataGenerated::ilo_getVector2S6(System.Int32)
extern "C"  Vector2_t4282066565  UnityEngine_EventSystems_PointerEventDataGenerated_ilo_getVector2S6_m3529921855 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerEventDataGenerated::ilo_setBooleanS7(System.Int32,System.Boolean)
extern "C"  void UnityEngine_EventSystems_PointerEventDataGenerated_ilo_setBooleanS7_m463302155 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_PointerEventDataGenerated::ilo_getBooleanS8(System.Int32)
extern "C"  bool UnityEngine_EventSystems_PointerEventDataGenerated_ilo_getBooleanS8_m2312235443 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
