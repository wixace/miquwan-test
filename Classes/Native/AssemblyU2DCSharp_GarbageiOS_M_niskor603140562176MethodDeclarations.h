﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_niskor60
struct M_niskor60_t3140562176;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_niskor603140562176.h"

// System.Void GarbageiOS.M_niskor60::.ctor()
extern "C"  void M_niskor60__ctor_m230775091 (M_niskor60_t3140562176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_niskor60::M_roomerebar0(System.String[],System.Int32)
extern "C"  void M_niskor60_M_roomerebar0_m2829855184 (M_niskor60_t3140562176 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_niskor60::M_nowpemtiJarnu1(System.String[],System.Int32)
extern "C"  void M_niskor60_M_nowpemtiJarnu1_m2128957472 (M_niskor60_t3140562176 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_niskor60::M_zairjaFerekaqear2(System.String[],System.Int32)
extern "C"  void M_niskor60_M_zairjaFerekaqear2_m2486668276 (M_niskor60_t3140562176 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_niskor60::M_stellur3(System.String[],System.Int32)
extern "C"  void M_niskor60_M_stellur3_m749163020 (M_niskor60_t3140562176 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_niskor60::M_laikerehu4(System.String[],System.Int32)
extern "C"  void M_niskor60_M_laikerehu4_m1334052608 (M_niskor60_t3140562176 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_niskor60::M_kaigel5(System.String[],System.Int32)
extern "C"  void M_niskor60_M_kaigel5_m3035676976 (M_niskor60_t3140562176 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_niskor60::M_cearher6(System.String[],System.Int32)
extern "C"  void M_niskor60_M_cearher6_m4118759630 (M_niskor60_t3140562176 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_niskor60::M_lorricawWearhai7(System.String[],System.Int32)
extern "C"  void M_niskor60_M_lorricawWearhai7_m2166397651 (M_niskor60_t3140562176 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_niskor60::ilo_M_nowpemtiJarnu11(GarbageiOS.M_niskor60,System.String[],System.Int32)
extern "C"  void M_niskor60_ilo_M_nowpemtiJarnu11_m4070265402 (Il2CppObject * __this /* static, unused */, M_niskor60_t3140562176 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_niskor60::ilo_M_zairjaFerekaqear22(GarbageiOS.M_niskor60,System.String[],System.Int32)
extern "C"  void M_niskor60_ilo_M_zairjaFerekaqear22_m818211565 (Il2CppObject * __this /* static, unused */, M_niskor60_t3140562176 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_niskor60::ilo_M_stellur33(GarbageiOS.M_niskor60,System.String[],System.Int32)
extern "C"  void M_niskor60_ilo_M_stellur33_m1549094800 (Il2CppObject * __this /* static, unused */, M_niskor60_t3140562176 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_niskor60::ilo_M_kaigel54(GarbageiOS.M_niskor60,System.String[],System.Int32)
extern "C"  void M_niskor60_ilo_M_kaigel54_m4121872627 (Il2CppObject * __this /* static, unused */, M_niskor60_t3140562176 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
