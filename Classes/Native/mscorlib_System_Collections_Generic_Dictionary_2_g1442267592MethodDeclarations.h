﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>
struct Dictionary_2_t1442267592;
// System.Collections.Generic.IEqualityComparer`1<MScrollView/MoveWay>
struct IEqualityComparer_1_t4265975954;
// System.Collections.Generic.IDictionary`2<MScrollView/MoveWay,System.Object>
struct IDictionary_2_t1020140937;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<MScrollView/MoveWay>
struct ICollection_1_t74564241;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t770439062;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<MScrollView/MoveWay,System.Object>[]
struct KeyValuePair_2U5BU5D_t1493525295;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<MScrollView/MoveWay,System.Object>>
struct IEnumerator_1_t3252913347;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,System.Object>
struct KeyCollection_t3069027043;
// System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,System.Object>
struct ValueCollection_t142873305;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21341048298.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_MScrollView_MoveWay3474941550.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2759590984.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m2128016719_gshared (Dictionary_2_t1442267592 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m2128016719(__this, method) ((  void (*) (Dictionary_2_t1442267592 *, const MethodInfo*))Dictionary_2__ctor_m2128016719_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1980125766_gshared (Dictionary_2_t1442267592 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m1980125766(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t1442267592 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1980125766_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m1270082473_gshared (Dictionary_2_t1442267592 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m1270082473(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t1442267592 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1270082473_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m67906592_gshared (Dictionary_2_t1442267592 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m67906592(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t1442267592 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m67906592_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1501268980_gshared (Dictionary_2_t1442267592 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1501268980(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t1442267592 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1501268980_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2537996816_gshared (Dictionary_2_t1442267592 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m2537996816(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1442267592 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m2537996816_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1797564639_gshared (Dictionary_2_t1442267592 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1797564639(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1442267592 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1797564639_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m4119194335_gshared (Dictionary_2_t1442267592 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m4119194335(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1442267592 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m4119194335_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m191516245_gshared (Dictionary_2_t1442267592 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m191516245(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1442267592 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m191516245_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m1731072515_gshared (Dictionary_2_t1442267592 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m1731072515(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1442267592 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m1731072515_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m4158950002_gshared (Dictionary_2_t1442267592 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m4158950002(__this, method) ((  bool (*) (Dictionary_2_t1442267592 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m4158950002_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m865263175_gshared (Dictionary_2_t1442267592 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m865263175(__this, method) ((  bool (*) (Dictionary_2_t1442267592 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m865263175_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m1032045977_gshared (Dictionary_2_t1442267592 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1032045977(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1442267592 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1032045977_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m1104246334_gshared (Dictionary_2_t1442267592 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1104246334(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1442267592 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1104246334_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m81221971_gshared (Dictionary_2_t1442267592 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m81221971(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1442267592 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m81221971_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m943672515_gshared (Dictionary_2_t1442267592 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m943672515(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1442267592 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m943672515_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m3674483836_gshared (Dictionary_2_t1442267592 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m3674483836(__this, ___key0, method) ((  void (*) (Dictionary_2_t1442267592 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m3674483836_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1869894001_gshared (Dictionary_2_t1442267592 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1869894001(__this, method) ((  bool (*) (Dictionary_2_t1442267592 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1869894001_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3018613213_gshared (Dictionary_2_t1442267592 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3018613213(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1442267592 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3018613213_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m593521269_gshared (Dictionary_2_t1442267592 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m593521269(__this, method) ((  bool (*) (Dictionary_2_t1442267592 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m593521269_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3481430802_gshared (Dictionary_2_t1442267592 * __this, KeyValuePair_2_t1341048298  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3481430802(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t1442267592 *, KeyValuePair_2_t1341048298 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3481430802_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1789054192_gshared (Dictionary_2_t1442267592 * __this, KeyValuePair_2_t1341048298  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1789054192(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1442267592 *, KeyValuePair_2_t1341048298 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1789054192_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m754285174_gshared (Dictionary_2_t1442267592 * __this, KeyValuePair_2U5BU5D_t1493525295* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m754285174(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1442267592 *, KeyValuePair_2U5BU5D_t1493525295*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m754285174_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2371842069_gshared (Dictionary_2_t1442267592 * __this, KeyValuePair_2_t1341048298  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2371842069(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1442267592 *, KeyValuePair_2_t1341048298 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2371842069_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m3313625813_gshared (Dictionary_2_t1442267592 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3313625813(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1442267592 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3313625813_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3056044944_gshared (Dictionary_2_t1442267592 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3056044944(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1442267592 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3056044944_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3840076365_gshared (Dictionary_2_t1442267592 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3840076365(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1442267592 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3840076365_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2772512680_gshared (Dictionary_2_t1442267592 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2772512680(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1442267592 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2772512680_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m3456754231_gshared (Dictionary_2_t1442267592 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m3456754231(__this, method) ((  int32_t (*) (Dictionary_2_t1442267592 *, const MethodInfo*))Dictionary_2_get_Count_m3456754231_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m3188706644_gshared (Dictionary_2_t1442267592 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m3188706644(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1442267592 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m3188706644_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m1739601103_gshared (Dictionary_2_t1442267592 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m1739601103(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1442267592 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m1739601103_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m1861866695_gshared (Dictionary_2_t1442267592 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m1861866695(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t1442267592 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m1861866695_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m349212112_gshared (Dictionary_2_t1442267592 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m349212112(__this, ___size0, method) ((  void (*) (Dictionary_2_t1442267592 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m349212112_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m270466508_gshared (Dictionary_2_t1442267592 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m270466508(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1442267592 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m270466508_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t1341048298  Dictionary_2_make_pair_m4092280856_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m4092280856(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t1341048298  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m4092280856_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m3647536094_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m3647536094(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m3647536094_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m2991970718_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m2991970718(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m2991970718_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m652486787_gshared (Dictionary_2_t1442267592 * __this, KeyValuePair_2U5BU5D_t1493525295* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m652486787(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1442267592 *, KeyValuePair_2U5BU5D_t1493525295*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m652486787_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m1601805513_gshared (Dictionary_2_t1442267592 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1601805513(__this, method) ((  void (*) (Dictionary_2_t1442267592 *, const MethodInfo*))Dictionary_2_Resize_m1601805513_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m3923626054_gshared (Dictionary_2_t1442267592 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m3923626054(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1442267592 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m3923626054_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m3829117306_gshared (Dictionary_2_t1442267592 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m3829117306(__this, method) ((  void (*) (Dictionary_2_t1442267592 *, const MethodInfo*))Dictionary_2_Clear_m3829117306_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m1757040480_gshared (Dictionary_2_t1442267592 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1757040480(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1442267592 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m1757040480_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m3466523488_gshared (Dictionary_2_t1442267592 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m3466523488(__this, ___value0, method) ((  bool (*) (Dictionary_2_t1442267592 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m3466523488_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m2448226669_gshared (Dictionary_2_t1442267592 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m2448226669(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1442267592 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m2448226669_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m3901436695_gshared (Dictionary_2_t1442267592 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m3901436695(__this, ___sender0, method) ((  void (*) (Dictionary_2_t1442267592 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m3901436695_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m2710162832_gshared (Dictionary_2_t1442267592 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m2710162832(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1442267592 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m2710162832_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m3593553849_gshared (Dictionary_2_t1442267592 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m3593553849(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t1442267592 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m3593553849_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::get_Keys()
extern "C"  KeyCollection_t3069027043 * Dictionary_2_get_Keys_m2360677990_gshared (Dictionary_2_t1442267592 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m2360677990(__this, method) ((  KeyCollection_t3069027043 * (*) (Dictionary_2_t1442267592 *, const MethodInfo*))Dictionary_2_get_Keys_m2360677990_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::get_Values()
extern "C"  ValueCollection_t142873305 * Dictionary_2_get_Values_m380692454_gshared (Dictionary_2_t1442267592 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m380692454(__this, method) ((  ValueCollection_t142873305 * (*) (Dictionary_2_t1442267592 *, const MethodInfo*))Dictionary_2_get_Values_m380692454_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m3097395001_gshared (Dictionary_2_t1442267592 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m3097395001(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t1442267592 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m3097395001_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m1304284857_gshared (Dictionary_2_t1442267592 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1304284857(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t1442267592 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1304284857_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m712407923_gshared (Dictionary_2_t1442267592 * __this, KeyValuePair_2_t1341048298  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m712407923(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t1442267592 *, KeyValuePair_2_t1341048298 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m712407923_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2759590984  Dictionary_2_GetEnumerator_m310442132_gshared (Dictionary_2_t1442267592 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m310442132(__this, method) ((  Enumerator_t2759590984  (*) (Dictionary_2_t1442267592 *, const MethodInfo*))Dictionary_2_GetEnumerator_m310442132_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m2442688227_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m2442688227(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m2442688227_gshared)(__this /* static, unused */, ___key0, ___value1, method)
