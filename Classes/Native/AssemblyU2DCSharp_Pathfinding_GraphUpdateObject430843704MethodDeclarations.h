﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.GraphUpdateObject
struct GraphUpdateObject_t430843704;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// Pathfinding.GridNode
struct GridNode_t3795753694;
// Pathfinding.GraphUpdateShape
struct GraphUpdateShape_t2348620960;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_Pathfinding_GridNode3795753694.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphUpdateShape2348620960.h"

// System.Void Pathfinding.GraphUpdateObject::.ctor()
extern "C"  void GraphUpdateObject__ctor_m632979711 (GraphUpdateObject_t430843704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateObject::.ctor(UnityEngine.Bounds)
extern "C"  void GraphUpdateObject__ctor_m2634846803 (GraphUpdateObject_t430843704 * __this, Bounds_t2711641849  ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateObject::WillUpdateNode(Pathfinding.GraphNode)
extern "C"  void GraphUpdateObject_WillUpdateNode_m2326194218 (GraphUpdateObject_t430843704 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateObject::RevertFromBackup()
extern "C"  void GraphUpdateObject_RevertFromBackup_m1883201909 (GraphUpdateObject_t430843704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateObject::Apply(Pathfinding.GraphNode)
extern "C"  void GraphUpdateObject_Apply_m3028405697 (GraphUpdateObject_t430843704 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateObject::ilo_set_Flags1(Pathfinding.GraphNode,System.UInt32)
extern "C"  void GraphUpdateObject_ilo_set_Flags1_m114724705 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateObject::ilo_set_InternalGridFlags2(Pathfinding.GridNode,System.UInt16)
extern "C"  void GraphUpdateObject_ilo_set_InternalGridFlags2_m3009000447 (Il2CppObject * __this /* static, unused */, GridNode_t3795753694 * ____this0, uint16_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GraphUpdateObject::ilo_Contains3(Pathfinding.GraphUpdateShape,Pathfinding.GraphNode)
extern "C"  bool GraphUpdateObject_ilo_Contains3_m521161220 (Il2CppObject * __this /* static, unused */, GraphUpdateShape_t2348620960 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateObject::ilo_set_Walkable4(Pathfinding.GraphNode,System.Boolean)
extern "C"  void GraphUpdateObject_ilo_set_Walkable4_m3737608943 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateObject::ilo_set_Tag5(Pathfinding.GraphNode,System.UInt32)
extern "C"  void GraphUpdateObject_ilo_set_Tag5_m1971816368 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
