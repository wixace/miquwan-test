﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<PushType>
struct List_1_t3208828004;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3228500774.h"
#include "AssemblyU2DCSharp_PushType1840642452.h"

// System.Void System.Collections.Generic.List`1/Enumerator<PushType>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2728911143_gshared (Enumerator_t3228500774 * __this, List_1_t3208828004 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m2728911143(__this, ___l0, method) ((  void (*) (Enumerator_t3228500774 *, List_1_t3208828004 *, const MethodInfo*))Enumerator__ctor_m2728911143_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PushType>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m986371787_gshared (Enumerator_t3228500774 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m986371787(__this, method) ((  void (*) (Enumerator_t3228500774 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m986371787_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<PushType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3173458561_gshared (Enumerator_t3228500774 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3173458561(__this, method) ((  Il2CppObject * (*) (Enumerator_t3228500774 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3173458561_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PushType>::Dispose()
extern "C"  void Enumerator_Dispose_m3030771916_gshared (Enumerator_t3228500774 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3030771916(__this, method) ((  void (*) (Enumerator_t3228500774 *, const MethodInfo*))Enumerator_Dispose_m3030771916_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PushType>::VerifyState()
extern "C"  void Enumerator_VerifyState_m333472261_gshared (Enumerator_t3228500774 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m333472261(__this, method) ((  void (*) (Enumerator_t3228500774 *, const MethodInfo*))Enumerator_VerifyState_m333472261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<PushType>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2391662843_gshared (Enumerator_t3228500774 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2391662843(__this, method) ((  bool (*) (Enumerator_t3228500774 *, const MethodInfo*))Enumerator_MoveNext_m2391662843_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<PushType>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2857649822_gshared (Enumerator_t3228500774 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2857649822(__this, method) ((  int32_t (*) (Enumerator_t3228500774 *, const MethodInfo*))Enumerator_get_Current_m2857649822_gshared)(__this, method)
