﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2937880697MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m920659703(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3832890751 *, Dictionary_2_t2206131300 *, const MethodInfo*))KeyCollection__ctor_m4280222283_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3997725695(__this, ___item0, method) ((  void (*) (KeyCollection_t3832890751 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m94032939_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m969510198(__this, method) ((  void (*) (KeyCollection_t3832890751 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3763210338_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m4061925611(__this, ___item0, method) ((  bool (*) (KeyCollection_t3832890751 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2819840319_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4219322640(__this, ___item0, method) ((  bool (*) (KeyCollection_t3832890751 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3784511076_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3917628594(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3832890751 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3401802590_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m249778344(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3832890751 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m34367956_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2482113187(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3832890751 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1556449231_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2266298764(__this, method) ((  bool (*) (KeyCollection_t3832890751 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2623241440_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2171289918(__this, method) ((  bool (*) (KeyCollection_t3832890751 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3213499794_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2111124522(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3832890751 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m4228299518_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m4180617644(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3832890751 *, EBehaviorIDU5BU5D_t373889457*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m4262483200_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1687502057(__this, method) ((  Enumerator_t2821067354  (*) (KeyCollection_t3832890751 *, const MethodInfo*))KeyCollection_GetEnumerator_m2706716579_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::get_Count()
#define KeyCollection_get_Count_m732789124(__this, method) ((  int32_t (*) (KeyCollection_t3832890751 *, const MethodInfo*))KeyCollection_get_Count_m746767064_gshared)(__this, method)
