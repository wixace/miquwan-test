﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_worlem197
struct M_worlem197_t2624382165;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_worlem1972624382165.h"

// System.Void GarbageiOS.M_worlem197::.ctor()
extern "C"  void M_worlem197__ctor_m2071190574 (M_worlem197_t2624382165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_worlem197::M_selfelror0(System.String[],System.Int32)
extern "C"  void M_worlem197_M_selfelror0_m2263837709 (M_worlem197_t2624382165 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_worlem197::M_falldroofeaChataychis1(System.String[],System.Int32)
extern "C"  void M_worlem197_M_falldroofeaChataychis1_m4212563544 (M_worlem197_t2624382165 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_worlem197::ilo_M_selfelror01(GarbageiOS.M_worlem197,System.String[],System.Int32)
extern "C"  void M_worlem197_ilo_M_selfelror01_m2835655008 (Il2CppObject * __this /* static, unused */, M_worlem197_t2624382165 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
