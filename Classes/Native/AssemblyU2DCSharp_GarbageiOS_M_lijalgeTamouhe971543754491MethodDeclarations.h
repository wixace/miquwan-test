﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_lijalgeTamouhe97
struct M_lijalgeTamouhe97_t1543754491;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_lijalgeTamouhe971543754491.h"

// System.Void GarbageiOS.M_lijalgeTamouhe97::.ctor()
extern "C"  void M_lijalgeTamouhe97__ctor_m2779698584 (M_lijalgeTamouhe97_t1543754491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lijalgeTamouhe97::M_banowkallDuterejor0(System.String[],System.Int32)
extern "C"  void M_lijalgeTamouhe97_M_banowkallDuterejor0_m3021766848 (M_lijalgeTamouhe97_t1543754491 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lijalgeTamouhe97::M_ficedouFetersoo1(System.String[],System.Int32)
extern "C"  void M_lijalgeTamouhe97_M_ficedouFetersoo1_m664451376 (M_lijalgeTamouhe97_t1543754491 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lijalgeTamouhe97::M_werla2(System.String[],System.Int32)
extern "C"  void M_lijalgeTamouhe97_M_werla2_m2775142062 (M_lijalgeTamouhe97_t1543754491 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lijalgeTamouhe97::M_fayjisqem3(System.String[],System.Int32)
extern "C"  void M_lijalgeTamouhe97_M_fayjisqem3_m1088778533 (M_lijalgeTamouhe97_t1543754491 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lijalgeTamouhe97::M_pearsurmor4(System.String[],System.Int32)
extern "C"  void M_lijalgeTamouhe97_M_pearsurmor4_m2157948905 (M_lijalgeTamouhe97_t1543754491 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lijalgeTamouhe97::ilo_M_ficedouFetersoo11(GarbageiOS.M_lijalgeTamouhe97,System.String[],System.Int32)
extern "C"  void M_lijalgeTamouhe97_ilo_M_ficedouFetersoo11_m402957349 (Il2CppObject * __this /* static, unused */, M_lijalgeTamouhe97_t1543754491 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lijalgeTamouhe97::ilo_M_fayjisqem32(GarbageiOS.M_lijalgeTamouhe97,System.String[],System.Int32)
extern "C"  void M_lijalgeTamouhe97_ilo_M_fayjisqem32_m3316892625 (Il2CppObject * __this /* static, unused */, M_lijalgeTamouhe97_t1543754491 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
