﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.BBTree/BBTreeBox[]
struct BBTreeBoxU5BU5D_t3508094851;
// Pathfinding.INavmeshHolder
struct INavmeshHolder_t1019551337;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.BBTree
struct  BBTree_t1216325332  : public Il2CppObject
{
public:
	// Pathfinding.BBTree/BBTreeBox[] Pathfinding.BBTree::arr
	BBTreeBoxU5BU5D_t3508094851* ___arr_0;
	// System.Int32 Pathfinding.BBTree::count
	int32_t ___count_1;
	// Pathfinding.INavmeshHolder Pathfinding.BBTree::graph
	Il2CppObject * ___graph_2;

public:
	inline static int32_t get_offset_of_arr_0() { return static_cast<int32_t>(offsetof(BBTree_t1216325332, ___arr_0)); }
	inline BBTreeBoxU5BU5D_t3508094851* get_arr_0() const { return ___arr_0; }
	inline BBTreeBoxU5BU5D_t3508094851** get_address_of_arr_0() { return &___arr_0; }
	inline void set_arr_0(BBTreeBoxU5BU5D_t3508094851* value)
	{
		___arr_0 = value;
		Il2CppCodeGenWriteBarrier(&___arr_0, value);
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(BBTree_t1216325332, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}

	inline static int32_t get_offset_of_graph_2() { return static_cast<int32_t>(offsetof(BBTree_t1216325332, ___graph_2)); }
	inline Il2CppObject * get_graph_2() const { return ___graph_2; }
	inline Il2CppObject ** get_address_of_graph_2() { return &___graph_2; }
	inline void set_graph_2(Il2CppObject * value)
	{
		___graph_2 = value;
		Il2CppCodeGenWriteBarrier(&___graph_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
