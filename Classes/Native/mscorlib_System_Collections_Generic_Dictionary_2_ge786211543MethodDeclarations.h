﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2045888271MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::.ctor()
#define Dictionary_2__ctor_m3429029733(__this, method) ((  void (*) (Dictionary_2_t786211543 *, const MethodInfo*))Dictionary_2__ctor_m3794638399_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m3324803827(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t786211543 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m273898294_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m5429404(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t786211543 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m4211605689_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::.ctor(System.Int32)
#define Dictionary_2__ctor_m365053389(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t786211543 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2504582416_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m1819365601(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t786211543 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2042790116_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m1482815933(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t786211543 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m4162067200_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m458271058(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t786211543 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1029455407_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2223710226(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t786211543 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1376055727_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m3119508290(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t786211543 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1780508229_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::System.Collections.IDictionary.get_Values()
#define Dictionary_2_System_Collections_IDictionary_get_Values_m2327848880(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t786211543 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m4038979059_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::System.Collections.IDictionary.get_IsFixedSize()
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m254129509(__this, method) ((  bool (*) (Dictionary_2_t786211543 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m824729858_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::System.Collections.IDictionary.get_IsReadOnly()
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1154943220(__this, method) ((  bool (*) (Dictionary_2_t786211543 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1311897015_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m153972876(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t786211543 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1551250025_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1270464561(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t786211543 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1049066318_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m254763904(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t786211543 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m781609539_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m386086262(__this, ___key0, method) ((  bool (*) (Dictionary_2_t786211543 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1783363411_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m3232545903(__this, ___key0, method) ((  void (*) (Dictionary_2_t786211543 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2215006604_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2551799198(__this, method) ((  bool (*) (Dictionary_2_t786211543 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1826238689_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2651851146(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t786211543 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2075478797_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3109254242(__this, method) ((  bool (*) (Dictionary_2_t786211543 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1433083365_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2517640581(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t786211543 *, KeyValuePair_2_t684992249 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3257059362_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2539874525(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t786211543 *, KeyValuePair_2_t684992249 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2744049760_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2027068969(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t786211543 *, KeyValuePair_2U5BU5D_t2035345668*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2008986502_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3212845890(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t786211543 *, KeyValuePair_2_t684992249 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2350489477_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m103491144(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t786211543 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3550803941_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m508417603(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t786211543 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m4115711264_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3015823360(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t786211543 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m52259357_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2083404251(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t786211543 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1395730616_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::get_Count()
#define Dictionary_2_get_Count_m3760395236(__this, method) ((  int32_t (*) (Dictionary_2_t786211543 *, const MethodInfo*))Dictionary_2_get_Count_m1232250407_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::get_Item(TKey)
#define Dictionary_2_get_Item_m1509071833(__this, ___key0, method) ((  ByteU5BU5D_t4260760469* (*) (Dictionary_2_t786211543 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m2285357284_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m3829831484(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t786211543 *, String_t*, ByteU5BU5D_t4260760469*, const MethodInfo*))Dictionary_2_set_Item_m2627891647_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m1571774964(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t786211543 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m2966484407_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m1966635075(__this, ___size0, method) ((  void (*) (Dictionary_2_t786211543 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2119297760_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m485892863(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t786211543 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2536521436_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m688836683(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t684992249  (*) (Il2CppObject * /* static, unused */, String_t*, ByteU5BU5D_t4260760469*, const MethodInfo*))Dictionary_2_make_pair_m2083407400_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m816066123(__this /* static, unused */, ___key0, ___value1, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, String_t*, ByteU5BU5D_t4260760469*, const MethodInfo*))Dictionary_2_pick_key_m3909093582_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m3195574859(__this /* static, unused */, ___key0, ___value1, method) ((  ByteU5BU5D_t4260760469* (*) (Il2CppObject * /* static, unused */, String_t*, ByteU5BU5D_t4260760469*, const MethodInfo*))Dictionary_2_pick_value_m3477594126_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m2499043312(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t786211543 *, KeyValuePair_2U5BU5D_t2035345668*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3401241971_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::Resize()
#define Dictionary_2_Resize_m3153251644(__this, method) ((  void (*) (Dictionary_2_t786211543 *, const MethodInfo*))Dictionary_2_Resize_m1727470041_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::Add(TKey,TValue)
#define Dictionary_2_Add_m2756467361(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t786211543 *, String_t*, ByteU5BU5D_t4260760469*, const MethodInfo*))Dictionary_2_Add_m3537188182_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::Clear()
#define Dictionary_2_Clear_m3463521959(__this, method) ((  void (*) (Dictionary_2_t786211543 *, const MethodInfo*))Dictionary_2_Clear_m1200771690_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m2602530509(__this, ___key0, method) ((  bool (*) (Dictionary_2_t786211543 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m3006991056_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m1914215501(__this, ___value0, method) ((  bool (*) (Dictionary_2_t786211543 *, ByteU5BU5D_t4260760469*, const MethodInfo*))Dictionary_2_ContainsValue_m712275664_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m3995087898(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t786211543 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m1544184413_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m438762122(__this, ___sender0, method) ((  void (*) (Dictionary_2_t786211543 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1638301735_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::Remove(TKey)
#define Dictionary_2_Remove_m4246843176(__this, ___key0, method) ((  bool (*) (Dictionary_2_t786211543 *, String_t*, const MethodInfo*))Dictionary_2_Remove_m2155719712_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m1638052390(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t786211543 *, String_t*, ByteU5BU5D_t4260760469**, const MethodInfo*))Dictionary_2_TryGetValue_m2075628329_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::get_Keys()
#define Dictionary_2_get_Keys_m2706162969(__this, method) ((  KeyCollection_t2412970994 * (*) (Dictionary_2_t786211543 *, const MethodInfo*))Dictionary_2_get_Keys_m2624609910_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::get_Values()
#define Dictionary_2_get_Values_m3133680473(__this, method) ((  ValueCollection_t3781784552 * (*) (Dictionary_2_t786211543 *, const MethodInfo*))Dictionary_2_get_Values_m2070602102_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m265925030(__this, ___key0, method) ((  String_t* (*) (Dictionary_2_t786211543 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m3358952489_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m1507888998(__this, ___value0, method) ((  ByteU5BU5D_t4260760469* (*) (Dictionary_2_t786211543 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1789908265_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m411732966(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t786211543 *, KeyValuePair_2_t684992249 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m3073235459_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m3439552129(__this, method) ((  Enumerator_t2103534935  (*) (Dictionary_2_t786211543 *, const MethodInfo*))Dictionary_2_GetEnumerator_m65675076_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m3381154192(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, String_t*, ByteU5BU5D_t4260760469*, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m3818730131_gshared)(__this /* static, unused */, ___key0, ___value1, method)
