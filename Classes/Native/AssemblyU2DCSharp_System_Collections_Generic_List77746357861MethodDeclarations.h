﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_RemoveAll__PredicateT1_T>c__AnonStorey94
struct U3CListA1_RemoveAll__PredicateT1_TU3Ec__AnonStorey94_t746357861;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_RemoveAll__PredicateT1_T>c__AnonStorey94::.ctor()
extern "C"  void U3CListA1_RemoveAll__PredicateT1_TU3Ec__AnonStorey94__ctor_m1388251302 (U3CListA1_RemoveAll__PredicateT1_TU3Ec__AnonStorey94_t746357861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Collections_Generic_List77Generated/<ListA1_RemoveAll__PredicateT1_T>c__AnonStorey94::<>m__B9()
extern "C"  Il2CppObject * U3CListA1_RemoveAll__PredicateT1_TU3Ec__AnonStorey94_U3CU3Em__B9_m2840837467 (U3CListA1_RemoveAll__PredicateT1_TU3Ec__AnonStorey94_t746357861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
