﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GradientColorKeyGenerated
struct UnityEngine_GradientColorKeyGenerated_t104187707;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_GradientColorKeyGenerated::.ctor()
extern "C"  void UnityEngine_GradientColorKeyGenerated__ctor_m3216987280 (UnityEngine_GradientColorKeyGenerated_t104187707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GradientColorKeyGenerated::.cctor()
extern "C"  void UnityEngine_GradientColorKeyGenerated__cctor_m460261661 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GradientColorKeyGenerated::GradientColorKey_GradientColorKey1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GradientColorKeyGenerated_GradientColorKey_GradientColorKey1_m2744525154 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GradientColorKeyGenerated::GradientColorKey_GradientColorKey2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GradientColorKeyGenerated_GradientColorKey_GradientColorKey2_m3989289635 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GradientColorKeyGenerated::GradientColorKey_color(JSVCall)
extern "C"  void UnityEngine_GradientColorKeyGenerated_GradientColorKey_color_m249726083 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GradientColorKeyGenerated::GradientColorKey_time(JSVCall)
extern "C"  void UnityEngine_GradientColorKeyGenerated_GradientColorKey_time_m1373039193 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GradientColorKeyGenerated::__Register()
extern "C"  void UnityEngine_GradientColorKeyGenerated___Register_m4061168183 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GradientColorKeyGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_GradientColorKeyGenerated_ilo_attachFinalizerObject1_m3200205415 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_GradientColorKeyGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_GradientColorKeyGenerated_ilo_getObject2_m3939603734 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GradientColorKeyGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void UnityEngine_GradientColorKeyGenerated_ilo_setSingle3_m2771829478 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GradientColorKeyGenerated::ilo_changeJSObj4(System.Int32,System.Object)
extern "C"  void UnityEngine_GradientColorKeyGenerated_ilo_changeJSObj4_m1635260508 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
