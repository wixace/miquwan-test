﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConfigAssetMgr/<RunLoadConfig>c__Iterator33
struct U3CRunLoadConfigU3Ec__Iterator33_t1082580655;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ConfigAssetMgr/<RunLoadConfig>c__Iterator33::.ctor()
extern "C"  void U3CRunLoadConfigU3Ec__Iterator33__ctor_m4075622812 (U3CRunLoadConfigU3Ec__Iterator33_t1082580655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ConfigAssetMgr/<RunLoadConfig>c__Iterator33::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRunLoadConfigU3Ec__Iterator33_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m257417910 (U3CRunLoadConfigU3Ec__Iterator33_t1082580655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ConfigAssetMgr/<RunLoadConfig>c__Iterator33::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRunLoadConfigU3Ec__Iterator33_System_Collections_IEnumerator_get_Current_m2845971018 (U3CRunLoadConfigU3Ec__Iterator33_t1082580655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ConfigAssetMgr/<RunLoadConfig>c__Iterator33::MoveNext()
extern "C"  bool U3CRunLoadConfigU3Ec__Iterator33_MoveNext_m1532069080 (U3CRunLoadConfigU3Ec__Iterator33_t1082580655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConfigAssetMgr/<RunLoadConfig>c__Iterator33::Dispose()
extern "C"  void U3CRunLoadConfigU3Ec__Iterator33_Dispose_m3856598745 (U3CRunLoadConfigU3Ec__Iterator33_t1082580655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConfigAssetMgr/<RunLoadConfig>c__Iterator33::Reset()
extern "C"  void U3CRunLoadConfigU3Ec__Iterator33_Reset_m1722055753 (U3CRunLoadConfigU3Ec__Iterator33_t1082580655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
