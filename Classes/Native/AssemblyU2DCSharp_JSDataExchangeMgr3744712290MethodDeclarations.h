﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t4136801618;
// ConstructorID
struct ConstructorID_t3348888181;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// MethodID
struct MethodID_t3916401116;
// System.Type[]
struct TypeU5BU5D_t3339007067;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t4231331326;
// JSCache/TypeInfo
struct TypeInfo_t3198813374;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSApi_GetType1607644050.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr_eGetType1861086360.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_ConstructorID3348888181.h"
#include "AssemblyU2DCSharp_MethodID3916401116.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_JSCache_TypeInfo3198813374.h"

// System.Void JSDataExchangeMgr::.ctor()
extern "C"  void JSDataExchangeMgr__ctor_m1939600713 (JSDataExchangeMgr_t3744712290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSDataExchangeMgr::.cctor()
extern "C"  void JSDataExchangeMgr__cctor_m3810951044 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSDataExchangeMgr::setTemp(System.Object)
extern "C"  void JSDataExchangeMgr_setTemp_m2239373783 (JSDataExchangeMgr_t3744712290 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSDataExchangeMgr::getObject(System.Int32)
extern "C"  Il2CppObject * JSDataExchangeMgr_getObject_m3928859202 (JSDataExchangeMgr_t3744712290 * __this, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSDataExchangeMgr::getByType(JSApi/GetType)
extern "C"  Il2CppObject * JSDataExchangeMgr_getByType_m3388439133 (JSDataExchangeMgr_t3744712290 * __this, int32_t ___eType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSDataExchangeMgr::getWhatever(System.Int32)
extern "C"  Il2CppObject * JSDataExchangeMgr_getWhatever_m4049807205 (JSDataExchangeMgr_t3744712290 * __this, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSDataExchangeMgr::getDelegate(JSDataExchangeMgr/eGetType)
extern "C"  Il2CppObject * JSDataExchangeMgr_getDelegate_m1932769247 (JSDataExchangeMgr_t3744712290 * __this, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSDataExchangeMgr::setWhatever(System.Int32,System.Object)
extern "C"  void JSDataExchangeMgr_setWhatever_m3247707146 (JSDataExchangeMgr_t3744712290 * __this, int32_t ___e0, Il2CppObject * ___obj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSDataExchangeMgr::setObject(System.Int32,System.Object)
extern "C"  int32_t JSDataExchangeMgr_setObject_m2896437749 (JSDataExchangeMgr_t3744712290 * __this, int32_t ___e0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type JSDataExchangeMgr::GetTypeByName(System.String,System.Type)
extern "C"  Type_t * JSDataExchangeMgr_GetTypeByName_m2147716802 (Il2CppObject * __this /* static, unused */, String_t* ___typeName0, Type_t * ___defaultType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo JSDataExchangeMgr::makeGenericConstructor(System.Type,ConstructorID)
extern "C"  ConstructorInfo_t4136801618 * JSDataExchangeMgr_makeGenericConstructor_m682550131 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, ConstructorID_t3348888181 * ___constructorID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo JSDataExchangeMgr::makeGenericMethod(System.Type,MethodID,System.Int32)
extern "C"  MethodInfo_t * JSDataExchangeMgr_makeGenericMethod_m3409392139 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, MethodID_t3916401116 * ___methodID1, int32_t ___TCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type JSDataExchangeMgr::GetGenericTypeBaseType(System.Type)
extern "C"  Type_t * JSDataExchangeMgr_GetGenericTypeBaseType_m3581787730 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] JSDataExchangeMgr::RecursivelyGetGenericParameters(System.Type,System.Collections.Generic.List`1<System.Type>)
extern "C"  TypeU5BU5D_t3339007067* JSDataExchangeMgr_RecursivelyGetGenericParameters_m3129191795 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, List_1_t4231331326 * ___lst1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSDataExchangeMgr::GetMetatypeKeyword(System.Type)
extern "C"  String_t* JSDataExchangeMgr_GetMetatypeKeyword_m219722541 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSDataExchangeMgr::ilo_getCSObj1(System.Int32)
extern "C"  Il2CppObject * JSDataExchangeMgr_ilo_getCSObj1_m3482029370 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSDataExchangeMgr::ilo_getStringS2(System.Int32)
extern "C"  String_t* JSDataExchangeMgr_ilo_getStringS2_m3088938030 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSDataExchangeMgr::ilo_getBooleanS3(System.Int32)
extern "C"  bool JSDataExchangeMgr_ilo_getBooleanS3_m2004688637 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 JSDataExchangeMgr::ilo_getChar4(System.Int32)
extern "C"  int16_t JSDataExchangeMgr_ilo_getChar4_m940718431 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 JSDataExchangeMgr::ilo_getUInt325(System.Int32)
extern "C"  uint32_t JSDataExchangeMgr_ilo_getUInt325_m3374136394 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 JSDataExchangeMgr::ilo_getUInt646(System.Int32)
extern "C"  uint64_t JSDataExchangeMgr_ilo_getUInt646_m2888442539 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 JSDataExchangeMgr::ilo_getInt647(System.Int32)
extern "C"  int64_t JSDataExchangeMgr_ilo_getInt647_m863698164 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JSDataExchangeMgr::ilo_getSingle8(System.Int32)
extern "C"  float JSDataExchangeMgr_ilo_getSingle8_m1483726125 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 JSDataExchangeMgr::ilo_getVector3S9(System.Int32)
extern "C"  Vector3_t4282066566  JSDataExchangeMgr_ilo_getVector3S9_m2258918821 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSDataExchangeMgr::ilo_getObject10(System.Int32)
extern "C"  int32_t JSDataExchangeMgr_ilo_getObject10_m1967527207 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 JSDataExchangeMgr::ilo_getTag11(System.Int32)
extern "C"  uint32_t JSDataExchangeMgr_ilo_getTag11_m1885198792 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSDataExchangeMgr::ilo_isNullOrUndefined12(System.UInt32)
extern "C"  bool JSDataExchangeMgr_ilo_isNullOrUndefined12_m596627381 (Il2CppObject * __this /* static, unused */, uint32_t ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSDataExchangeMgr::ilo_isInt3213(System.UInt32)
extern "C"  bool JSDataExchangeMgr_ilo_isInt3213_m4220771948 (Il2CppObject * __this /* static, unused */, uint32_t ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSDataExchangeMgr::ilo_getInt3214(System.Int32)
extern "C"  int32_t JSDataExchangeMgr_ilo_getInt3214_m304865836 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSDataExchangeMgr::ilo_isVector2S15(System.Int32)
extern "C"  bool JSDataExchangeMgr_ilo_isVector2S15_m2438342173 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 JSDataExchangeMgr::ilo_getVector2S16(System.Int32)
extern "C"  Vector2_t4282066565  JSDataExchangeMgr_ilo_getVector2S16_m3849370783 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSDataExchangeMgr::ilo_setUndefined17(System.Int32)
extern "C"  void JSDataExchangeMgr_ilo_setUndefined17_m781290611 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSDataExchangeMgr::ilo_setEnum18(System.Int32,System.Int32)
extern "C"  void JSDataExchangeMgr_ilo_setEnum18_m1833252104 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSDataExchangeMgr::ilo_setBooleanS19(System.Int32,System.Boolean)
extern "C"  void JSDataExchangeMgr_ilo_setBooleanS19_m1096734755 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSDataExchangeMgr::ilo_setChar20(System.Int32,System.Char)
extern "C"  void JSDataExchangeMgr_ilo_setChar20_m3356428914 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Il2CppChar ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSDataExchangeMgr::ilo_setByte21(System.Int32,System.Byte)
extern "C"  void JSDataExchangeMgr_ilo_setByte21_m3496030835 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint8_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSDataExchangeMgr::ilo_setSByte22(System.Int32,System.SByte)
extern "C"  void JSDataExchangeMgr_ilo_setSByte22_m2759935634 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int8_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSDataExchangeMgr::ilo_setInt3223(System.Int32,System.Int32)
extern "C"  void JSDataExchangeMgr_ilo_setInt3223_m2853109835 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSDataExchangeMgr::ilo_setInt6424(System.Int32,System.Int64)
extern "C"  void JSDataExchangeMgr_ilo_setInt6424_m3673408300 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int64_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSDataExchangeMgr::ilo_setSingle25(System.Int32,System.Single)
extern "C"  void JSDataExchangeMgr_ilo_setSingle25_m2124879543 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSDataExchangeMgr::ilo_setDouble26(System.Int32,System.Double)
extern "C"  void JSDataExchangeMgr_ilo_setDouble26_m1763628696 (Il2CppObject * __this /* static, unused */, int32_t ___e0, double ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSDataExchangeMgr::ilo_setObject27(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t JSDataExchangeMgr_ilo_setObject27_m186878941 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSDataExchangeMgr::ilo_get_IsClass28(JSCache/TypeInfo)
extern "C"  bool JSDataExchangeMgr_ilo_get_IsClass28_m753485397 (Il2CppObject * __this /* static, unused */, TypeInfo_t3198813374 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSDataExchangeMgr::ilo_getJSObj29(System.Object,JSCache/TypeInfo)
extern "C"  int32_t JSDataExchangeMgr_ilo_getJSObj29_m290437939 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___csObj0, TypeInfo_t3198813374 * ___typeInfo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSDataExchangeMgr::ilo_get_IsDelegate30(JSCache/TypeInfo)
extern "C"  bool JSDataExchangeMgr_ilo_get_IsDelegate30_m3566211473 (Il2CppObject * __this /* static, unused */, TypeInfo_t3198813374 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSDataExchangeMgr::ilo_createJSClassObject31(System.String)
extern "C"  int32_t JSDataExchangeMgr_ilo_createJSClassObject31_m966586000 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type JSDataExchangeMgr::ilo_GetTypeByName32(System.String,System.Type)
extern "C"  Type_t * JSDataExchangeMgr_ilo_GetTypeByName32_m3777880054 (Il2CppObject * __this /* static, unused */, String_t* ___typeName0, Type_t * ___defaultType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo JSDataExchangeMgr::ilo_getConstructor33(System.Type,ConstructorID)
extern "C"  ConstructorInfo_t4136801618 * JSDataExchangeMgr_ilo_getConstructor33_m3105508557 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, ConstructorID_t3348888181 * ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo JSDataExchangeMgr::ilo_getMethod34(System.Type,MethodID)
extern "C"  MethodInfo_t * JSDataExchangeMgr_ilo_getMethod34_m717744679 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, MethodID_t3916401116 * ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type JSDataExchangeMgr::ilo_GetGenericTypeBaseType35(System.Type)
extern "C"  Type_t * JSDataExchangeMgr_ilo_GetGenericTypeBaseType35_m781874781 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] JSDataExchangeMgr::ilo_RecursivelyGetGenericParameters36(System.Type,System.Collections.Generic.List`1<System.Type>)
extern "C"  TypeU5BU5D_t3339007067* JSDataExchangeMgr_ilo_RecursivelyGetGenericParameters36_m3023886083 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, List_1_t4231331326 * ___lst1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
