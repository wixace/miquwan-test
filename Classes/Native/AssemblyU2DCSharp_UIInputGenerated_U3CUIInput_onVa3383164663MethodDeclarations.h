﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIInputGenerated/<UIInput_onValidate_GetDelegate_member16_arg0>c__AnonStoreyC6
struct U3CUIInput_onValidate_GetDelegate_member16_arg0U3Ec__AnonStoreyC6_t3383164663;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void UIInputGenerated/<UIInput_onValidate_GetDelegate_member16_arg0>c__AnonStoreyC6::.ctor()
extern "C"  void U3CUIInput_onValidate_GetDelegate_member16_arg0U3Ec__AnonStoreyC6__ctor_m4129026948 (U3CUIInput_onValidate_GetDelegate_member16_arg0U3Ec__AnonStoreyC6_t3383164663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char UIInputGenerated/<UIInput_onValidate_GetDelegate_member16_arg0>c__AnonStoreyC6::<>m__150(System.String,System.Int32,System.Char)
extern "C"  Il2CppChar U3CUIInput_onValidate_GetDelegate_member16_arg0U3Ec__AnonStoreyC6_U3CU3Em__150_m785553323 (U3CUIInput_onValidate_GetDelegate_member16_arg0U3Ec__AnonStoreyC6_t3383164663 * __this, String_t* ___text0, int32_t ___charIndex1, Il2CppChar ___addedChar2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
