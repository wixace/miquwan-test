﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22061784035.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24287931429.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1079219282_gshared (KeyValuePair_2_t2061784035 * __this, Il2CppObject * ___key0, KeyValuePair_2_t4287931429  ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m1079219282(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2061784035 *, Il2CppObject *, KeyValuePair_2_t4287931429 , const MethodInfo*))KeyValuePair_2__ctor_m1079219282_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1584147158_gshared (KeyValuePair_2_t2061784035 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1584147158(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t2061784035 *, const MethodInfo*))KeyValuePair_2_get_Key_m1584147158_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1377884823_gshared (KeyValuePair_2_t2061784035 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m1377884823(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2061784035 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m1377884823_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_Value()
extern "C"  KeyValuePair_2_t4287931429  KeyValuePair_2_get_Value_m2108684282_gshared (KeyValuePair_2_t2061784035 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m2108684282(__this, method) ((  KeyValuePair_2_t4287931429  (*) (KeyValuePair_2_t2061784035 *, const MethodInfo*))KeyValuePair_2_get_Value_m2108684282_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1053756183_gshared (KeyValuePair_2_t2061784035 * __this, KeyValuePair_2_t4287931429  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1053756183(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2061784035 *, KeyValuePair_2_t4287931429 , const MethodInfo*))KeyValuePair_2_set_Value_m1053756183_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3764046545_gshared (KeyValuePair_2_t2061784035 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m3764046545(__this, method) ((  String_t* (*) (KeyValuePair_2_t2061784035 *, const MethodInfo*))KeyValuePair_2_ToString_m3764046545_gshared)(__this, method)
