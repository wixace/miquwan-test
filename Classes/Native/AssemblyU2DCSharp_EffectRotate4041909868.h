﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectRotate
struct  EffectRotate_t4041909868  : public MonoBehaviour_t667441552
{
public:
	// System.Single EffectRotate::SpeedX
	float ___SpeedX_2;
	// System.Single EffectRotate::SpeedY
	float ___SpeedY_3;
	// System.Single EffectRotate::SpeedZ
	float ___SpeedZ_4;
	// UnityEngine.Transform EffectRotate::mTransform
	Transform_t1659122786 * ___mTransform_5;

public:
	inline static int32_t get_offset_of_SpeedX_2() { return static_cast<int32_t>(offsetof(EffectRotate_t4041909868, ___SpeedX_2)); }
	inline float get_SpeedX_2() const { return ___SpeedX_2; }
	inline float* get_address_of_SpeedX_2() { return &___SpeedX_2; }
	inline void set_SpeedX_2(float value)
	{
		___SpeedX_2 = value;
	}

	inline static int32_t get_offset_of_SpeedY_3() { return static_cast<int32_t>(offsetof(EffectRotate_t4041909868, ___SpeedY_3)); }
	inline float get_SpeedY_3() const { return ___SpeedY_3; }
	inline float* get_address_of_SpeedY_3() { return &___SpeedY_3; }
	inline void set_SpeedY_3(float value)
	{
		___SpeedY_3 = value;
	}

	inline static int32_t get_offset_of_SpeedZ_4() { return static_cast<int32_t>(offsetof(EffectRotate_t4041909868, ___SpeedZ_4)); }
	inline float get_SpeedZ_4() const { return ___SpeedZ_4; }
	inline float* get_address_of_SpeedZ_4() { return &___SpeedZ_4; }
	inline void set_SpeedZ_4(float value)
	{
		___SpeedZ_4 = value;
	}

	inline static int32_t get_offset_of_mTransform_5() { return static_cast<int32_t>(offsetof(EffectRotate_t4041909868, ___mTransform_5)); }
	inline Transform_t1659122786 * get_mTransform_5() const { return ___mTransform_5; }
	inline Transform_t1659122786 ** get_address_of_mTransform_5() { return &___mTransform_5; }
	inline void set_mTransform_5(Transform_t1659122786 * value)
	{
		___mTransform_5 = value;
		Il2CppCodeGenWriteBarrier(&___mTransform_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
