﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_kupere200
struct  M_kupere200_t2742283360  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_kupere200::_suwerca
	bool ____suwerca_0;
	// System.Boolean GarbageiOS.M_kupere200::_sayballraSawe
	bool ____sayballraSawe_1;
	// System.UInt32 GarbageiOS.M_kupere200::_hirsepair
	uint32_t ____hirsepair_2;
	// System.String GarbageiOS.M_kupere200::_laiperePembemdel
	String_t* ____laiperePembemdel_3;
	// System.UInt32 GarbageiOS.M_kupere200::_pishow
	uint32_t ____pishow_4;
	// System.String GarbageiOS.M_kupere200::_herezay
	String_t* ____herezay_5;
	// System.Single GarbageiOS.M_kupere200::_nepir
	float ____nepir_6;
	// System.Int32 GarbageiOS.M_kupere200::_bokeanaiStishu
	int32_t ____bokeanaiStishu_7;
	// System.UInt32 GarbageiOS.M_kupere200::_hurfurNorrer
	uint32_t ____hurfurNorrer_8;
	// System.Boolean GarbageiOS.M_kupere200::_lide
	bool ____lide_9;
	// System.UInt32 GarbageiOS.M_kupere200::_sadi
	uint32_t ____sadi_10;
	// System.Single GarbageiOS.M_kupere200::_sainooday
	float ____sainooday_11;
	// System.UInt32 GarbageiOS.M_kupere200::_sallnem
	uint32_t ____sallnem_12;
	// System.Boolean GarbageiOS.M_kupere200::_pipordayVasokas
	bool ____pipordayVasokas_13;
	// System.Boolean GarbageiOS.M_kupere200::_jecuWefearmear
	bool ____jecuWefearmear_14;
	// System.String GarbageiOS.M_kupere200::_sideawePoudea
	String_t* ____sideawePoudea_15;
	// System.Single GarbageiOS.M_kupere200::_mesemfa
	float ____mesemfa_16;
	// System.Int32 GarbageiOS.M_kupere200::_tafalbal
	int32_t ____tafalbal_17;
	// System.Int32 GarbageiOS.M_kupere200::_rearse
	int32_t ____rearse_18;
	// System.String GarbageiOS.M_kupere200::_zidowheTepu
	String_t* ____zidowheTepu_19;

public:
	inline static int32_t get_offset_of__suwerca_0() { return static_cast<int32_t>(offsetof(M_kupere200_t2742283360, ____suwerca_0)); }
	inline bool get__suwerca_0() const { return ____suwerca_0; }
	inline bool* get_address_of__suwerca_0() { return &____suwerca_0; }
	inline void set__suwerca_0(bool value)
	{
		____suwerca_0 = value;
	}

	inline static int32_t get_offset_of__sayballraSawe_1() { return static_cast<int32_t>(offsetof(M_kupere200_t2742283360, ____sayballraSawe_1)); }
	inline bool get__sayballraSawe_1() const { return ____sayballraSawe_1; }
	inline bool* get_address_of__sayballraSawe_1() { return &____sayballraSawe_1; }
	inline void set__sayballraSawe_1(bool value)
	{
		____sayballraSawe_1 = value;
	}

	inline static int32_t get_offset_of__hirsepair_2() { return static_cast<int32_t>(offsetof(M_kupere200_t2742283360, ____hirsepair_2)); }
	inline uint32_t get__hirsepair_2() const { return ____hirsepair_2; }
	inline uint32_t* get_address_of__hirsepair_2() { return &____hirsepair_2; }
	inline void set__hirsepair_2(uint32_t value)
	{
		____hirsepair_2 = value;
	}

	inline static int32_t get_offset_of__laiperePembemdel_3() { return static_cast<int32_t>(offsetof(M_kupere200_t2742283360, ____laiperePembemdel_3)); }
	inline String_t* get__laiperePembemdel_3() const { return ____laiperePembemdel_3; }
	inline String_t** get_address_of__laiperePembemdel_3() { return &____laiperePembemdel_3; }
	inline void set__laiperePembemdel_3(String_t* value)
	{
		____laiperePembemdel_3 = value;
		Il2CppCodeGenWriteBarrier(&____laiperePembemdel_3, value);
	}

	inline static int32_t get_offset_of__pishow_4() { return static_cast<int32_t>(offsetof(M_kupere200_t2742283360, ____pishow_4)); }
	inline uint32_t get__pishow_4() const { return ____pishow_4; }
	inline uint32_t* get_address_of__pishow_4() { return &____pishow_4; }
	inline void set__pishow_4(uint32_t value)
	{
		____pishow_4 = value;
	}

	inline static int32_t get_offset_of__herezay_5() { return static_cast<int32_t>(offsetof(M_kupere200_t2742283360, ____herezay_5)); }
	inline String_t* get__herezay_5() const { return ____herezay_5; }
	inline String_t** get_address_of__herezay_5() { return &____herezay_5; }
	inline void set__herezay_5(String_t* value)
	{
		____herezay_5 = value;
		Il2CppCodeGenWriteBarrier(&____herezay_5, value);
	}

	inline static int32_t get_offset_of__nepir_6() { return static_cast<int32_t>(offsetof(M_kupere200_t2742283360, ____nepir_6)); }
	inline float get__nepir_6() const { return ____nepir_6; }
	inline float* get_address_of__nepir_6() { return &____nepir_6; }
	inline void set__nepir_6(float value)
	{
		____nepir_6 = value;
	}

	inline static int32_t get_offset_of__bokeanaiStishu_7() { return static_cast<int32_t>(offsetof(M_kupere200_t2742283360, ____bokeanaiStishu_7)); }
	inline int32_t get__bokeanaiStishu_7() const { return ____bokeanaiStishu_7; }
	inline int32_t* get_address_of__bokeanaiStishu_7() { return &____bokeanaiStishu_7; }
	inline void set__bokeanaiStishu_7(int32_t value)
	{
		____bokeanaiStishu_7 = value;
	}

	inline static int32_t get_offset_of__hurfurNorrer_8() { return static_cast<int32_t>(offsetof(M_kupere200_t2742283360, ____hurfurNorrer_8)); }
	inline uint32_t get__hurfurNorrer_8() const { return ____hurfurNorrer_8; }
	inline uint32_t* get_address_of__hurfurNorrer_8() { return &____hurfurNorrer_8; }
	inline void set__hurfurNorrer_8(uint32_t value)
	{
		____hurfurNorrer_8 = value;
	}

	inline static int32_t get_offset_of__lide_9() { return static_cast<int32_t>(offsetof(M_kupere200_t2742283360, ____lide_9)); }
	inline bool get__lide_9() const { return ____lide_9; }
	inline bool* get_address_of__lide_9() { return &____lide_9; }
	inline void set__lide_9(bool value)
	{
		____lide_9 = value;
	}

	inline static int32_t get_offset_of__sadi_10() { return static_cast<int32_t>(offsetof(M_kupere200_t2742283360, ____sadi_10)); }
	inline uint32_t get__sadi_10() const { return ____sadi_10; }
	inline uint32_t* get_address_of__sadi_10() { return &____sadi_10; }
	inline void set__sadi_10(uint32_t value)
	{
		____sadi_10 = value;
	}

	inline static int32_t get_offset_of__sainooday_11() { return static_cast<int32_t>(offsetof(M_kupere200_t2742283360, ____sainooday_11)); }
	inline float get__sainooday_11() const { return ____sainooday_11; }
	inline float* get_address_of__sainooday_11() { return &____sainooday_11; }
	inline void set__sainooday_11(float value)
	{
		____sainooday_11 = value;
	}

	inline static int32_t get_offset_of__sallnem_12() { return static_cast<int32_t>(offsetof(M_kupere200_t2742283360, ____sallnem_12)); }
	inline uint32_t get__sallnem_12() const { return ____sallnem_12; }
	inline uint32_t* get_address_of__sallnem_12() { return &____sallnem_12; }
	inline void set__sallnem_12(uint32_t value)
	{
		____sallnem_12 = value;
	}

	inline static int32_t get_offset_of__pipordayVasokas_13() { return static_cast<int32_t>(offsetof(M_kupere200_t2742283360, ____pipordayVasokas_13)); }
	inline bool get__pipordayVasokas_13() const { return ____pipordayVasokas_13; }
	inline bool* get_address_of__pipordayVasokas_13() { return &____pipordayVasokas_13; }
	inline void set__pipordayVasokas_13(bool value)
	{
		____pipordayVasokas_13 = value;
	}

	inline static int32_t get_offset_of__jecuWefearmear_14() { return static_cast<int32_t>(offsetof(M_kupere200_t2742283360, ____jecuWefearmear_14)); }
	inline bool get__jecuWefearmear_14() const { return ____jecuWefearmear_14; }
	inline bool* get_address_of__jecuWefearmear_14() { return &____jecuWefearmear_14; }
	inline void set__jecuWefearmear_14(bool value)
	{
		____jecuWefearmear_14 = value;
	}

	inline static int32_t get_offset_of__sideawePoudea_15() { return static_cast<int32_t>(offsetof(M_kupere200_t2742283360, ____sideawePoudea_15)); }
	inline String_t* get__sideawePoudea_15() const { return ____sideawePoudea_15; }
	inline String_t** get_address_of__sideawePoudea_15() { return &____sideawePoudea_15; }
	inline void set__sideawePoudea_15(String_t* value)
	{
		____sideawePoudea_15 = value;
		Il2CppCodeGenWriteBarrier(&____sideawePoudea_15, value);
	}

	inline static int32_t get_offset_of__mesemfa_16() { return static_cast<int32_t>(offsetof(M_kupere200_t2742283360, ____mesemfa_16)); }
	inline float get__mesemfa_16() const { return ____mesemfa_16; }
	inline float* get_address_of__mesemfa_16() { return &____mesemfa_16; }
	inline void set__mesemfa_16(float value)
	{
		____mesemfa_16 = value;
	}

	inline static int32_t get_offset_of__tafalbal_17() { return static_cast<int32_t>(offsetof(M_kupere200_t2742283360, ____tafalbal_17)); }
	inline int32_t get__tafalbal_17() const { return ____tafalbal_17; }
	inline int32_t* get_address_of__tafalbal_17() { return &____tafalbal_17; }
	inline void set__tafalbal_17(int32_t value)
	{
		____tafalbal_17 = value;
	}

	inline static int32_t get_offset_of__rearse_18() { return static_cast<int32_t>(offsetof(M_kupere200_t2742283360, ____rearse_18)); }
	inline int32_t get__rearse_18() const { return ____rearse_18; }
	inline int32_t* get_address_of__rearse_18() { return &____rearse_18; }
	inline void set__rearse_18(int32_t value)
	{
		____rearse_18 = value;
	}

	inline static int32_t get_offset_of__zidowheTepu_19() { return static_cast<int32_t>(offsetof(M_kupere200_t2742283360, ____zidowheTepu_19)); }
	inline String_t* get__zidowheTepu_19() const { return ____zidowheTepu_19; }
	inline String_t** get_address_of__zidowheTepu_19() { return &____zidowheTepu_19; }
	inline void set__zidowheTepu_19(String_t* value)
	{
		____zidowheTepu_19 = value;
		Il2CppCodeGenWriteBarrier(&____zidowheTepu_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
