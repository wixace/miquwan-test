﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_RectTransformUtilityGenerated
struct UnityEngine_RectTransformUtilityGenerated_t4147042083;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UnityEngine_RectTransformUtilityGenerated::.ctor()
extern "C"  void UnityEngine_RectTransformUtilityGenerated__ctor_m336262568 (UnityEngine_RectTransformUtilityGenerated_t4147042083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectTransformUtilityGenerated::RectTransformUtility_CalculateRelativeRectTransformBounds__Transform__Transform(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectTransformUtilityGenerated_RectTransformUtility_CalculateRelativeRectTransformBounds__Transform__Transform_m3182249072 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectTransformUtilityGenerated::RectTransformUtility_CalculateRelativeRectTransformBounds__Transform(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectTransformUtilityGenerated_RectTransformUtility_CalculateRelativeRectTransformBounds__Transform_m2379237886 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectTransformUtilityGenerated::RectTransformUtility_FlipLayoutAxes__RectTransform__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectTransformUtilityGenerated_RectTransformUtility_FlipLayoutAxes__RectTransform__Boolean__Boolean_m3302633705 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectTransformUtilityGenerated::RectTransformUtility_FlipLayoutOnAxis__RectTransform__Int32__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectTransformUtilityGenerated_RectTransformUtility_FlipLayoutOnAxis__RectTransform__Int32__Boolean__Boolean_m1833650114 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectTransformUtilityGenerated::RectTransformUtility_PixelAdjustPoint__Vector2__Transform__Canvas(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectTransformUtilityGenerated_RectTransformUtility_PixelAdjustPoint__Vector2__Transform__Canvas_m3333900757 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectTransformUtilityGenerated::RectTransformUtility_PixelAdjustRect__RectTransform__Canvas(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectTransformUtilityGenerated_RectTransformUtility_PixelAdjustRect__RectTransform__Canvas_m1306430476 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectTransformUtilityGenerated::RectTransformUtility_RectangleContainsScreenPoint__RectTransform__Vector2__Camera(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectTransformUtilityGenerated_RectTransformUtility_RectangleContainsScreenPoint__RectTransform__Vector2__Camera_m1639604839 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectTransformUtilityGenerated::RectTransformUtility_RectangleContainsScreenPoint__RectTransform__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectTransformUtilityGenerated_RectTransformUtility_RectangleContainsScreenPoint__RectTransform__Vector2_m3257182722 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectTransformUtilityGenerated::RectTransformUtility_ScreenPointToLocalPointInRectangle__RectTransform__Vector2__Camera__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectTransformUtilityGenerated_RectTransformUtility_ScreenPointToLocalPointInRectangle__RectTransform__Vector2__Camera__Vector2_m2569785274 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectTransformUtilityGenerated::RectTransformUtility_ScreenPointToRay__Camera__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectTransformUtilityGenerated_RectTransformUtility_ScreenPointToRay__Camera__Vector2_m3573977756 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectTransformUtilityGenerated::RectTransformUtility_ScreenPointToWorldPointInRectangle__RectTransform__Vector2__Camera__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectTransformUtilityGenerated_RectTransformUtility_ScreenPointToWorldPointInRectangle__RectTransform__Vector2__Camera__Vector3_m1834430964 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectTransformUtilityGenerated::RectTransformUtility_WorldToScreenPoint__Camera__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectTransformUtilityGenerated_RectTransformUtility_WorldToScreenPoint__Camera__Vector3_m714721361 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectTransformUtilityGenerated::__Register()
extern "C"  void UnityEngine_RectTransformUtilityGenerated___Register_m2044055583 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RectTransformUtilityGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_RectTransformUtilityGenerated_ilo_setObject1_m52850310 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RectTransformUtilityGenerated::ilo_getInt322(System.Int32)
extern "C"  int32_t UnityEngine_RectTransformUtilityGenerated_ilo_getInt322_m1174426380 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine_RectTransformUtilityGenerated::ilo_getVector2S3(System.Int32)
extern "C"  Vector2_t4282066565  UnityEngine_RectTransformUtilityGenerated_ilo_getVector2S3_m2396613150 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_RectTransformUtilityGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_RectTransformUtilityGenerated_ilo_getObject4_m2507518848 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectTransformUtilityGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void UnityEngine_RectTransformUtilityGenerated_ilo_setBooleanS5_m3838806165 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RectTransformUtilityGenerated::ilo_incArgIndex6()
extern "C"  int32_t UnityEngine_RectTransformUtilityGenerated_ilo_incArgIndex6_m3000283645 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectTransformUtilityGenerated::ilo_setArgIndex7(System.Int32)
extern "C"  void UnityEngine_RectTransformUtilityGenerated_ilo_setArgIndex7_m4269672281 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectTransformUtilityGenerated::ilo_setVector2S8(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_RectTransformUtilityGenerated_ilo_setVector2S8_m469529158 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
