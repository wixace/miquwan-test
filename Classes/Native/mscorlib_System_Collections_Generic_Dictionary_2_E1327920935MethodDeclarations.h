﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<AnimationRunner/AniType,System.Object>
struct Dictionary_2_t10597543;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1327920935.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24204345545.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniType1006238651.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3775084990_gshared (Enumerator_t1327920935 * __this, Dictionary_2_t10597543 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m3775084990(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1327920935 *, Dictionary_2_t10597543 *, const MethodInfo*))Enumerator__ctor_m3775084990_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2126568163_gshared (Enumerator_t1327920935 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2126568163(__this, method) ((  Il2CppObject * (*) (Enumerator_t1327920935 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2126568163_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2700606519_gshared (Enumerator_t1327920935 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2700606519(__this, method) ((  void (*) (Enumerator_t1327920935 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2700606519_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m604236608_gshared (Enumerator_t1327920935 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m604236608(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1327920935 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m604236608_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m854270335_gshared (Enumerator_t1327920935 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m854270335(__this, method) ((  Il2CppObject * (*) (Enumerator_t1327920935 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m854270335_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1663474769_gshared (Enumerator_t1327920935 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1663474769(__this, method) ((  Il2CppObject * (*) (Enumerator_t1327920935 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1663474769_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2993083427_gshared (Enumerator_t1327920935 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2993083427(__this, method) ((  bool (*) (Enumerator_t1327920935 *, const MethodInfo*))Enumerator_MoveNext_m2993083427_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t4204345545  Enumerator_get_Current_m946966189_gshared (Enumerator_t1327920935 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m946966189(__this, method) ((  KeyValuePair_2_t4204345545  (*) (Enumerator_t1327920935 *, const MethodInfo*))Enumerator_get_Current_m946966189_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m2284203824_gshared (Enumerator_t1327920935 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m2284203824(__this, method) ((  int32_t (*) (Enumerator_t1327920935 *, const MethodInfo*))Enumerator_get_CurrentKey_m2284203824_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m1793743188_gshared (Enumerator_t1327920935 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1793743188(__this, method) ((  Il2CppObject * (*) (Enumerator_t1327920935 *, const MethodInfo*))Enumerator_get_CurrentValue_m1793743188_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m2978879248_gshared (Enumerator_t1327920935 * __this, const MethodInfo* method);
#define Enumerator_Reset_m2978879248(__this, method) ((  void (*) (Enumerator_t1327920935 *, const MethodInfo*))Enumerator_Reset_m2978879248_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m4275559193_gshared (Enumerator_t1327920935 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m4275559193(__this, method) ((  void (*) (Enumerator_t1327920935 *, const MethodInfo*))Enumerator_VerifyState_m4275559193_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m1273553985_gshared (Enumerator_t1327920935 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1273553985(__this, method) ((  void (*) (Enumerator_t1327920935 *, const MethodInfo*))Enumerator_VerifyCurrent_m1273553985_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m483199968_gshared (Enumerator_t1327920935 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m483199968(__this, method) ((  void (*) (Enumerator_t1327920935 *, const MethodInfo*))Enumerator_Dispose_m483199968_gshared)(__this, method)
