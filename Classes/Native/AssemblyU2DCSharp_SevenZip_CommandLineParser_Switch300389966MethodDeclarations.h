﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.CommandLineParser.SwitchForm
struct SwitchForm_t300389966;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_SevenZip_CommandLineParser_Switch300816580.h"

// System.Void SevenZip.CommandLineParser.SwitchForm::.ctor(System.String,SevenZip.CommandLineParser.SwitchType,System.Boolean,System.Int32,System.Int32,System.String)
extern "C"  void SwitchForm__ctor_m152262604 (SwitchForm_t300389966 * __this, String_t* ___idString0, int32_t ___type1, bool ___multi2, int32_t ___minLen3, int32_t ___maxLen4, String_t* ___postCharSet5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.CommandLineParser.SwitchForm::.ctor(System.String,SevenZip.CommandLineParser.SwitchType,System.Boolean,System.Int32)
extern "C"  void SwitchForm__ctor_m1726937255 (SwitchForm_t300389966 * __this, String_t* ___idString0, int32_t ___type1, bool ___multi2, int32_t ___minLen3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.CommandLineParser.SwitchForm::.ctor(System.String,SevenZip.CommandLineParser.SwitchType,System.Boolean)
extern "C"  void SwitchForm__ctor_m4245369392 (SwitchForm_t300389966 * __this, String_t* ___idString0, int32_t ___type1, bool ___multi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
