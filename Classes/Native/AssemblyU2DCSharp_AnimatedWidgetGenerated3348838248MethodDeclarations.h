﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimatedWidgetGenerated
struct AnimatedWidgetGenerated_t3348838248;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void AnimatedWidgetGenerated::.ctor()
extern "C"  void AnimatedWidgetGenerated__ctor_m1375141123 (AnimatedWidgetGenerated_t3348838248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimatedWidgetGenerated::AnimatedWidget_AnimatedWidget1(JSVCall,System.Int32)
extern "C"  bool AnimatedWidgetGenerated_AnimatedWidget_AnimatedWidget1_m96316655 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatedWidgetGenerated::AnimatedWidget_width(JSVCall)
extern "C"  void AnimatedWidgetGenerated_AnimatedWidget_width_m826097944 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatedWidgetGenerated::AnimatedWidget_height(JSVCall)
extern "C"  void AnimatedWidgetGenerated_AnimatedWidget_height_m494676135 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatedWidgetGenerated::__Register()
extern "C"  void AnimatedWidgetGenerated___Register_m2627676580 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AnimatedWidgetGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t AnimatedWidgetGenerated_ilo_getObject1_m2145829651 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatedWidgetGenerated::ilo_setSingle2(System.Int32,System.Single)
extern "C"  void AnimatedWidgetGenerated_ilo_setSingle2_m2075429138 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimatedWidgetGenerated::ilo_getSingle3(System.Int32)
extern "C"  float AnimatedWidgetGenerated_ilo_getSingle3_m2999231342 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
