﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ClientGenerated
struct ClientGenerated_t3739914724;
// JSVCall
struct JSVCall_t3708497963;
// Client/ResponseCallBack
struct ResponseCallBack_t59495818;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// Client/PushCallBack
struct PushCallBack_t4203546979;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// Client
struct Client_t2021122027;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Client2021122027.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_Client_ResponseCallBack59495818.h"
#include "AssemblyU2DCSharp_Client_PushCallBack4203546979.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void ClientGenerated::.ctor()
extern "C"  void ClientGenerated__ctor_m1157174791 (ClientGenerated_t3739914724 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ClientGenerated::Client_Client1(JSVCall,System.Int32)
extern "C"  bool ClientGenerated_Client_Client1_m2249116395 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClientGenerated::Client_LOGIN_SERVER(JSVCall)
extern "C"  void ClientGenerated_Client_LOGIN_SERVER_m3521487765 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClientGenerated::Client_CONNECT_SERVER(JSVCall)
extern "C"  void ClientGenerated_Client_CONNECT_SERVER_m960197910 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClientGenerated::Client_instance(JSVCall)
extern "C"  void ClientGenerated_Client_instance_m1231529849 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClientGenerated::Client_IsNetConnected(JSVCall)
extern "C"  void ClientGenerated_Client_IsNetConnected_m1771990744 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ClientGenerated::Client_CloseNetState(JSVCall,System.Int32)
extern "C"  bool ClientGenerated_Client_CloseNetState_m3210550449 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ClientGenerated::Client_Logout(JSVCall,System.Int32)
extern "C"  bool ClientGenerated_Client_Logout_m3605316615 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ClientGenerated::Client_Notify__String__JObject(JSVCall,System.Int32)
extern "C"  bool ClientGenerated_Client_Notify__String__JObject_m1738381204 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ClientGenerated::Client_Notify__String__String(JSVCall,System.Int32)
extern "C"  bool ClientGenerated_Client_Notify__String__String_m3240602664 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ClientGenerated::Client_OnDisconnect(JSVCall,System.Int32)
extern "C"  bool ClientGenerated_Client_OnDisconnect_m2028470936 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ClientGenerated::Client_Request__String__JObject(JSVCall,System.Int32)
extern "C"  bool ClientGenerated_Client_Request__String__JObject_m407032550 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ClientGenerated::Client_Request__String__String(JSVCall,System.Int32)
extern "C"  bool ClientGenerated_Client_Request__String__String_m4028939926 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Client/ResponseCallBack ClientGenerated::Client_Run_GetDelegate_member7_arg2(CSRepresentedObject)
extern "C"  ResponseCallBack_t59495818 * ClientGenerated_Client_Run_GetDelegate_member7_arg2_m3043078340 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Client/PushCallBack ClientGenerated::Client_Run_GetDelegate_member7_arg3(CSRepresentedObject)
extern "C"  PushCallBack_t4203546979 * ClientGenerated_Client_Run_GetDelegate_member7_arg3_m597960316 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ClientGenerated::Client_Run__String__Int32__ResponseCallBack__PushCallBack(JSVCall,System.Int32)
extern "C"  bool ClientGenerated_Client_Run__String__Int32__ResponseCallBack__PushCallBack_m807861012 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClientGenerated::__Register()
extern "C"  void ClientGenerated___Register_m3940029216 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Client/ResponseCallBack ClientGenerated::<Client_Run__String__Int32__ResponseCallBack__PushCallBack>m__2F()
extern "C"  ResponseCallBack_t59495818 * ClientGenerated_U3CClient_Run__String__Int32__ResponseCallBack__PushCallBackU3Em__2F_m2561410118 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Client/ResponseCallBack ClientGenerated::<Client_Run__String__Int32__ResponseCallBack__PushCallBack>m__30()
extern "C"  ResponseCallBack_t59495818 * ClientGenerated_U3CClient_Run__String__Int32__ResponseCallBack__PushCallBackU3Em__30_m2561418767 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Client/PushCallBack ClientGenerated::<Client_Run__String__Int32__ResponseCallBack__PushCallBack>m__31()
extern "C"  PushCallBack_t4203546979 * ClientGenerated_U3CClient_Run__String__Int32__ResponseCallBack__PushCallBackU3Em__31_m2656082345 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ClientGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t ClientGenerated_ilo_getObject1_m695993999 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ClientGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool ClientGenerated_ilo_attachFinalizerObject2_m2998621969 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClientGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void ClientGenerated_ilo_addJSCSRel3_m2456567365 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClientGenerated::ilo_setStringS4(System.Int32,System.String)
extern "C"  void ClientGenerated_ilo_setStringS4_m3424968403 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClientGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void ClientGenerated_ilo_setBooleanS5_m2843830708 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClientGenerated::ilo_CloseNetState6(Client)
extern "C"  void ClientGenerated_ilo_CloseNetState6_m3764563567 (Il2CppObject * __this /* static, unused */, Client_t2021122027 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ClientGenerated::ilo_getStringS7(System.Int32)
extern "C"  String_t* ClientGenerated_ilo_getStringS7_m1376064113 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClientGenerated::ilo_Notify8(Client,System.String,Newtonsoft.Json.Linq.JObject)
extern "C"  void ClientGenerated_ilo_Notify8_m3433507108 (Il2CppObject * __this /* static, unused */, Client_t2021122027 * ____this0, String_t* ___route1, JObject_t1798544199 * ___msg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ClientGenerated::ilo_Request9(Client,System.String,System.String)
extern "C"  uint32_t ClientGenerated_ilo_Request9_m2427505868 (Il2CppObject * __this /* static, unused */, Client_t2021122027 * ____this0, String_t* ___route1, String_t* ___msg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClientGenerated::ilo_setUInt3210(System.Int32,System.UInt32)
extern "C"  void ClientGenerated_ilo_setUInt3210_m2394580145 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClientGenerated::ilo_Run11(Client,System.String,System.Int32,Client/ResponseCallBack,Client/PushCallBack)
extern "C"  void ClientGenerated_ilo_Run11_m3063089588 (Il2CppObject * __this /* static, unused */, Client_t2021122027 * ____this0, String_t* ___host1, int32_t ___port2, ResponseCallBack_t59495818 * ___responseFun3, PushCallBack_t4203546979 * ___pushFun4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject ClientGenerated::ilo_getFunctionS12(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * ClientGenerated_ilo_getFunctionS12_m1336595850 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Client/ResponseCallBack ClientGenerated::ilo_Client_Run_GetDelegate_member7_arg213(CSRepresentedObject)
extern "C"  ResponseCallBack_t59495818 * ClientGenerated_ilo_Client_Run_GetDelegate_member7_arg213_m2962005141 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ClientGenerated::ilo_getObject14(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * ClientGenerated_ilo_getObject14_m1647568646 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ClientGenerated::ilo_isFunctionS15(System.Int32)
extern "C"  bool ClientGenerated_ilo_isFunctionS15_m4202947148 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
