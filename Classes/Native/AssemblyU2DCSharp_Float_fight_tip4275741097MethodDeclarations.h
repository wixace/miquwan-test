﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_fight_tip
struct Float_fight_tip_t4275741097;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// FloatTextUnit
struct FloatTextUnit_t2362298029;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"

// System.Void Float_fight_tip::.ctor()
extern "C"  void Float_fight_tip__ctor_m1563292642 (Float_fight_tip_t4275741097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_fight_tip::FloatID()
extern "C"  int32_t Float_fight_tip_FloatID_m220670560 (Float_fight_tip_t4275741097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_fight_tip::OnAwake(UnityEngine.GameObject)
extern "C"  void Float_fight_tip_OnAwake_m133878174 (Float_fight_tip_t4275741097 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_fight_tip::OnDestroy()
extern "C"  void Float_fight_tip_OnDestroy_m4215211995 (Float_fight_tip_t4275741097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_fight_tip::Init()
extern "C"  void Float_fight_tip_Init_m2357270066 (Float_fight_tip_t4275741097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_fight_tip::SetFile(System.Object[])
extern "C"  void Float_fight_tip_SetFile_m3248270644 (Float_fight_tip_t4275741097 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_fight_tip::ilo_Play1(FloatTextUnit)
extern "C"  void Float_fight_tip_ilo_Play1_m2363841723 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
