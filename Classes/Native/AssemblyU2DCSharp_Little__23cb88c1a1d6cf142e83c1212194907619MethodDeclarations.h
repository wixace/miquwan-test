﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._23cb88c1a1d6cf142e83c1212087e3c1
struct _23cb88c1a1d6cf142e83c1212087e3c1_t194907619;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._23cb88c1a1d6cf142e83c1212087e3c1::.ctor()
extern "C"  void _23cb88c1a1d6cf142e83c1212087e3c1__ctor_m969082794 (_23cb88c1a1d6cf142e83c1212087e3c1_t194907619 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._23cb88c1a1d6cf142e83c1212087e3c1::_23cb88c1a1d6cf142e83c1212087e3c1m2(System.Int32)
extern "C"  int32_t _23cb88c1a1d6cf142e83c1212087e3c1__23cb88c1a1d6cf142e83c1212087e3c1m2_m3784093369 (_23cb88c1a1d6cf142e83c1212087e3c1_t194907619 * __this, int32_t ____23cb88c1a1d6cf142e83c1212087e3c1a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._23cb88c1a1d6cf142e83c1212087e3c1::_23cb88c1a1d6cf142e83c1212087e3c1m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _23cb88c1a1d6cf142e83c1212087e3c1__23cb88c1a1d6cf142e83c1212087e3c1m_m75299869 (_23cb88c1a1d6cf142e83c1212087e3c1_t194907619 * __this, int32_t ____23cb88c1a1d6cf142e83c1212087e3c1a0, int32_t ____23cb88c1a1d6cf142e83c1212087e3c1411, int32_t ____23cb88c1a1d6cf142e83c1212087e3c1c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
