﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3884108195;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SplatPrototype
struct  SplatPrototype_t2969367140  : public Il2CppObject
{
public:
	// UnityEngine.Texture2D UnityEngine.SplatPrototype::m_Texture
	Texture2D_t3884108195 * ___m_Texture_0;
	// UnityEngine.Texture2D UnityEngine.SplatPrototype::m_NormalMap
	Texture2D_t3884108195 * ___m_NormalMap_1;
	// UnityEngine.Vector2 UnityEngine.SplatPrototype::m_TileSize
	Vector2_t4282066565  ___m_TileSize_2;
	// UnityEngine.Vector2 UnityEngine.SplatPrototype::m_TileOffset
	Vector2_t4282066565  ___m_TileOffset_3;
	// UnityEngine.Vector4 UnityEngine.SplatPrototype::m_SpecularMetallic
	Vector4_t4282066567  ___m_SpecularMetallic_4;
	// System.Single UnityEngine.SplatPrototype::m_Smoothness
	float ___m_Smoothness_5;

public:
	inline static int32_t get_offset_of_m_Texture_0() { return static_cast<int32_t>(offsetof(SplatPrototype_t2969367140, ___m_Texture_0)); }
	inline Texture2D_t3884108195 * get_m_Texture_0() const { return ___m_Texture_0; }
	inline Texture2D_t3884108195 ** get_address_of_m_Texture_0() { return &___m_Texture_0; }
	inline void set_m_Texture_0(Texture2D_t3884108195 * value)
	{
		___m_Texture_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Texture_0, value);
	}

	inline static int32_t get_offset_of_m_NormalMap_1() { return static_cast<int32_t>(offsetof(SplatPrototype_t2969367140, ___m_NormalMap_1)); }
	inline Texture2D_t3884108195 * get_m_NormalMap_1() const { return ___m_NormalMap_1; }
	inline Texture2D_t3884108195 ** get_address_of_m_NormalMap_1() { return &___m_NormalMap_1; }
	inline void set_m_NormalMap_1(Texture2D_t3884108195 * value)
	{
		___m_NormalMap_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_NormalMap_1, value);
	}

	inline static int32_t get_offset_of_m_TileSize_2() { return static_cast<int32_t>(offsetof(SplatPrototype_t2969367140, ___m_TileSize_2)); }
	inline Vector2_t4282066565  get_m_TileSize_2() const { return ___m_TileSize_2; }
	inline Vector2_t4282066565 * get_address_of_m_TileSize_2() { return &___m_TileSize_2; }
	inline void set_m_TileSize_2(Vector2_t4282066565  value)
	{
		___m_TileSize_2 = value;
	}

	inline static int32_t get_offset_of_m_TileOffset_3() { return static_cast<int32_t>(offsetof(SplatPrototype_t2969367140, ___m_TileOffset_3)); }
	inline Vector2_t4282066565  get_m_TileOffset_3() const { return ___m_TileOffset_3; }
	inline Vector2_t4282066565 * get_address_of_m_TileOffset_3() { return &___m_TileOffset_3; }
	inline void set_m_TileOffset_3(Vector2_t4282066565  value)
	{
		___m_TileOffset_3 = value;
	}

	inline static int32_t get_offset_of_m_SpecularMetallic_4() { return static_cast<int32_t>(offsetof(SplatPrototype_t2969367140, ___m_SpecularMetallic_4)); }
	inline Vector4_t4282066567  get_m_SpecularMetallic_4() const { return ___m_SpecularMetallic_4; }
	inline Vector4_t4282066567 * get_address_of_m_SpecularMetallic_4() { return &___m_SpecularMetallic_4; }
	inline void set_m_SpecularMetallic_4(Vector4_t4282066567  value)
	{
		___m_SpecularMetallic_4 = value;
	}

	inline static int32_t get_offset_of_m_Smoothness_5() { return static_cast<int32_t>(offsetof(SplatPrototype_t2969367140, ___m_Smoothness_5)); }
	inline float get_m_Smoothness_5() const { return ___m_Smoothness_5; }
	inline float* get_address_of_m_Smoothness_5() { return &___m_Smoothness_5; }
	inline void set_m_Smoothness_5(float value)
	{
		___m_Smoothness_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: UnityEngine.SplatPrototype
struct SplatPrototype_t2969367140_marshaled_pinvoke
{
	Texture2D_t3884108195 * ___m_Texture_0;
	Texture2D_t3884108195 * ___m_NormalMap_1;
	Vector2_t4282066565_marshaled_pinvoke ___m_TileSize_2;
	Vector2_t4282066565_marshaled_pinvoke ___m_TileOffset_3;
	Vector4_t4282066567_marshaled_pinvoke ___m_SpecularMetallic_4;
	float ___m_Smoothness_5;
};
// Native definition for marshalling of: UnityEngine.SplatPrototype
struct SplatPrototype_t2969367140_marshaled_com
{
	Texture2D_t3884108195 * ___m_Texture_0;
	Texture2D_t3884108195 * ___m_NormalMap_1;
	Vector2_t4282066565_marshaled_com ___m_TileSize_2;
	Vector2_t4282066565_marshaled_com ___m_TileOffset_3;
	Vector4_t4282066567_marshaled_com ___m_SpecularMetallic_4;
	float ___m_Smoothness_5;
};
