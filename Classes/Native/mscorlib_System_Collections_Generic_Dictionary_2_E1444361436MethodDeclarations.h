﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>
struct Dictionary_2_t127038044;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1444361436.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g25818750.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2407447341_gshared (Enumerator_t1444361436 * __this, Dictionary_2_t127038044 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m2407447341(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1444361436 *, Dictionary_2_t127038044 *, const MethodInfo*))Enumerator__ctor_m2407447341_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m804217620_gshared (Enumerator_t1444361436 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m804217620(__this, method) ((  Il2CppObject * (*) (Enumerator_t1444361436 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m804217620_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2949223272_gshared (Enumerator_t1444361436 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2949223272(__this, method) ((  void (*) (Enumerator_t1444361436 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2949223272_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m951612401_gshared (Enumerator_t1444361436 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m951612401(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1444361436 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m951612401_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m958726384_gshared (Enumerator_t1444361436 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m958726384(__this, method) ((  Il2CppObject * (*) (Enumerator_t1444361436 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m958726384_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3261490050_gshared (Enumerator_t1444361436 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3261490050(__this, method) ((  Il2CppObject * (*) (Enumerator_t1444361436 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3261490050_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m104105044_gshared (Enumerator_t1444361436 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m104105044(__this, method) ((  bool (*) (Enumerator_t1444361436 *, const MethodInfo*))Enumerator_MoveNext_m104105044_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t25818750  Enumerator_get_Current_m2788080988_gshared (Enumerator_t1444361436 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2788080988(__this, method) ((  KeyValuePair_2_t25818750  (*) (Enumerator_t1444361436 *, const MethodInfo*))Enumerator_get_Current_m2788080988_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m3468423457_gshared (Enumerator_t1444361436 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m3468423457(__this, method) ((  int32_t (*) (Enumerator_t1444361436 *, const MethodInfo*))Enumerator_get_CurrentKey_m3468423457_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m170021253_gshared (Enumerator_t1444361436 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m170021253(__this, method) ((  Il2CppObject * (*) (Enumerator_t1444361436 *, const MethodInfo*))Enumerator_get_CurrentValue_m170021253_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m1177477631_gshared (Enumerator_t1444361436 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1177477631(__this, method) ((  void (*) (Enumerator_t1444361436 *, const MethodInfo*))Enumerator_Reset_m1177477631_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2838132424_gshared (Enumerator_t1444361436 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2838132424(__this, method) ((  void (*) (Enumerator_t1444361436 *, const MethodInfo*))Enumerator_VerifyState_m2838132424_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m2885898288_gshared (Enumerator_t1444361436 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m2885898288(__this, method) ((  void (*) (Enumerator_t1444361436 *, const MethodInfo*))Enumerator_VerifyCurrent_m2885898288_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m208066319_gshared (Enumerator_t1444361436 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m208066319(__this, method) ((  void (*) (Enumerator_t1444361436 *, const MethodInfo*))Enumerator_Dispose_m208066319_gshared)(__this, method)
