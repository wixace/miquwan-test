﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>
struct KeyCollection_t4183098081;
// System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>
struct SortedDictionary_2_t186228230;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "System_System_Collections_Generic_SortedDictionary1266487790.h"

// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m508122988_gshared (KeyCollection_t4183098081 * __this, SortedDictionary_2_t186228230 * ___dic0, const MethodInfo* method);
#define KeyCollection__ctor_m508122988(__this, ___dic0, method) ((  void (*) (KeyCollection_t4183098081 *, SortedDictionary_2_t186228230 *, const MethodInfo*))KeyCollection__ctor_m508122988_gshared)(__this, ___dic0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m916086183_gshared (KeyCollection_t4183098081 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m916086183(__this, ___item0, method) ((  void (*) (KeyCollection_t4183098081 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m916086183_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m626639070_gshared (KeyCollection_t4183098081 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m626639070(__this, method) ((  void (*) (KeyCollection_t4183098081 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m626639070_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2950936111_gshared (KeyCollection_t4183098081 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2950936111(__this, ___item0, method) ((  bool (*) (KeyCollection_t4183098081 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2950936111_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2965491566_gshared (KeyCollection_t4183098081 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2965491566(__this, method) ((  Il2CppObject* (*) (KeyCollection_t4183098081 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2965491566_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4052245968_gshared (KeyCollection_t4183098081 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4052245968(__this, method) ((  bool (*) (KeyCollection_t4183098081 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4052245968_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2796939092_gshared (KeyCollection_t4183098081 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2796939092(__this, ___item0, method) ((  bool (*) (KeyCollection_t4183098081 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2796939092_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3267053648_gshared (KeyCollection_t4183098081 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m3267053648(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t4183098081 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3267053648_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1782475394_gshared (KeyCollection_t4183098081 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1782475394(__this, method) ((  bool (*) (KeyCollection_t4183098081 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1782475394_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2909609602_gshared (KeyCollection_t4183098081 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2909609602(__this, method) ((  Il2CppObject * (*) (KeyCollection_t4183098081 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2909609602_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m875996383_gshared (KeyCollection_t4183098081 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m875996383(__this, method) ((  Il2CppObject * (*) (KeyCollection_t4183098081 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m875996383_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3186125060_gshared (KeyCollection_t4183098081 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define KeyCollection_CopyTo_m3186125060(__this, ___array0, ___arrayIndex1, method) ((  void (*) (KeyCollection_t4183098081 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3186125060_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m942646088_gshared (KeyCollection_t4183098081 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m942646088(__this, method) ((  int32_t (*) (KeyCollection_t4183098081 *, const MethodInfo*))KeyCollection_get_Count_m942646088_gshared)(__this, method)
// System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1266487790  KeyCollection_GetEnumerator_m2123079620_gshared (KeyCollection_t4183098081 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m2123079620(__this, method) ((  Enumerator_t1266487790  (*) (KeyCollection_t4183098081 *, const MethodInfo*))KeyCollection_GetEnumerator_m2123079620_gshared)(__this, method)
