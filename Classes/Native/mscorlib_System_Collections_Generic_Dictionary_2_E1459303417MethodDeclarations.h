﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1327920935MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1588687632(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1459303417 *, Dictionary_2_t141980025 *, const MethodInfo*))Enumerator__ctor_m3775084990_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.String>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3682953169(__this, method) ((  Il2CppObject * (*) (Enumerator_t1459303417 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2126568163_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.String>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3685503909(__this, method) ((  void (*) (Enumerator_t1459303417 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2700606519_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.String>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3239815726(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1459303417 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m604236608_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.String>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m52544493(__this, method) ((  Il2CppObject * (*) (Enumerator_t1459303417 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m854270335_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.String>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m4086591(__this, method) ((  Il2CppObject * (*) (Enumerator_t1459303417 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1663474769_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.String>::MoveNext()
#define Enumerator_MoveNext_m722664593(__this, method) ((  bool (*) (Enumerator_t1459303417 *, const MethodInfo*))Enumerator_MoveNext_m2993083427_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.String>::get_Current()
#define Enumerator_get_Current_m44459903(__this, method) ((  KeyValuePair_2_t40760731  (*) (Enumerator_t1459303417 *, const MethodInfo*))Enumerator_get_Current_m946966189_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.String>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2214710558(__this, method) ((  int32_t (*) (Enumerator_t1459303417 *, const MethodInfo*))Enumerator_get_CurrentKey_m2284203824_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.String>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3730191298(__this, method) ((  String_t* (*) (Enumerator_t1459303417 *, const MethodInfo*))Enumerator_get_CurrentValue_m1793743188_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.String>::Reset()
#define Enumerator_Reset_m390231394(__this, method) ((  void (*) (Enumerator_t1459303417 *, const MethodInfo*))Enumerator_Reset_m2978879248_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.String>::VerifyState()
#define Enumerator_VerifyState_m3373052907(__this, method) ((  void (*) (Enumerator_t1459303417 *, const MethodInfo*))Enumerator_VerifyState_m4275559193_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.String>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1548406931(__this, method) ((  void (*) (Enumerator_t1459303417 *, const MethodInfo*))Enumerator_VerifyCurrent_m1273553985_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AnimationRunner/AniType,System.String>::Dispose()
#define Enumerator_Dispose_m3873643954(__this, method) ((  void (*) (Enumerator_t1459303417 *, const MethodInfo*))Enumerator_Dispose_m483199968_gshared)(__this, method)
