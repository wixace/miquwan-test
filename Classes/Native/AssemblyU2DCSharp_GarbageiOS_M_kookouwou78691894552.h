﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_kookouwou78
struct  M_kookouwou78_t691894552  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_kookouwou78::_sawlor
	bool ____sawlor_0;
	// System.Single GarbageiOS.M_kookouwou78::_qirleakisLemnaisear
	float ____qirleakisLemnaisear_1;
	// System.UInt32 GarbageiOS.M_kookouwou78::_panaw
	uint32_t ____panaw_2;
	// System.Int32 GarbageiOS.M_kookouwou78::_jijinalReardow
	int32_t ____jijinalReardow_3;
	// System.String GarbageiOS.M_kookouwou78::_coowemdiNeljou
	String_t* ____coowemdiNeljou_4;
	// System.Boolean GarbageiOS.M_kookouwou78::_wasmor
	bool ____wasmor_5;
	// System.UInt32 GarbageiOS.M_kookouwou78::_getreaHetalldou
	uint32_t ____getreaHetalldou_6;
	// System.UInt32 GarbageiOS.M_kookouwou78::_jejearWallseljar
	uint32_t ____jejearWallseljar_7;
	// System.String GarbageiOS.M_kookouwou78::_pasdras
	String_t* ____pasdras_8;
	// System.Int32 GarbageiOS.M_kookouwou78::_revoutrou
	int32_t ____revoutrou_9;
	// System.Boolean GarbageiOS.M_kookouwou78::_reartawchow
	bool ____reartawchow_10;
	// System.Int32 GarbageiOS.M_kookouwou78::_vikow
	int32_t ____vikow_11;
	// System.Boolean GarbageiOS.M_kookouwou78::_seredeariGomis
	bool ____seredeariGomis_12;
	// System.String GarbageiOS.M_kookouwou78::_hobaircerMeapisga
	String_t* ____hobaircerMeapisga_13;
	// System.String GarbageiOS.M_kookouwou78::_lorjepa
	String_t* ____lorjepa_14;
	// System.Single GarbageiOS.M_kookouwou78::_vounolarJahe
	float ____vounolarJahe_15;
	// System.UInt32 GarbageiOS.M_kookouwou78::_daicawMayhejis
	uint32_t ____daicawMayhejis_16;
	// System.Single GarbageiOS.M_kookouwou78::_namuRatursal
	float ____namuRatursal_17;
	// System.Boolean GarbageiOS.M_kookouwou78::_pirstafar
	bool ____pirstafar_18;
	// System.UInt32 GarbageiOS.M_kookouwou78::_menor
	uint32_t ____menor_19;
	// System.Int32 GarbageiOS.M_kookouwou78::_birgelbay
	int32_t ____birgelbay_20;
	// System.UInt32 GarbageiOS.M_kookouwou78::_lisamuDelhirris
	uint32_t ____lisamuDelhirris_21;
	// System.String GarbageiOS.M_kookouwou78::_lemqowtear
	String_t* ____lemqowtear_22;
	// System.Boolean GarbageiOS.M_kookouwou78::_galcel
	bool ____galcel_23;
	// System.UInt32 GarbageiOS.M_kookouwou78::_lurbirJerawcor
	uint32_t ____lurbirJerawcor_24;
	// System.Int32 GarbageiOS.M_kookouwou78::_rasji
	int32_t ____rasji_25;
	// System.Boolean GarbageiOS.M_kookouwou78::_mobursi
	bool ____mobursi_26;
	// System.String GarbageiOS.M_kookouwou78::_temmurkelWhemxay
	String_t* ____temmurkelWhemxay_27;
	// System.Boolean GarbageiOS.M_kookouwou78::_yehurhall
	bool ____yehurhall_28;
	// System.Int32 GarbageiOS.M_kookouwou78::_stekarna
	int32_t ____stekarna_29;

public:
	inline static int32_t get_offset_of__sawlor_0() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____sawlor_0)); }
	inline bool get__sawlor_0() const { return ____sawlor_0; }
	inline bool* get_address_of__sawlor_0() { return &____sawlor_0; }
	inline void set__sawlor_0(bool value)
	{
		____sawlor_0 = value;
	}

	inline static int32_t get_offset_of__qirleakisLemnaisear_1() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____qirleakisLemnaisear_1)); }
	inline float get__qirleakisLemnaisear_1() const { return ____qirleakisLemnaisear_1; }
	inline float* get_address_of__qirleakisLemnaisear_1() { return &____qirleakisLemnaisear_1; }
	inline void set__qirleakisLemnaisear_1(float value)
	{
		____qirleakisLemnaisear_1 = value;
	}

	inline static int32_t get_offset_of__panaw_2() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____panaw_2)); }
	inline uint32_t get__panaw_2() const { return ____panaw_2; }
	inline uint32_t* get_address_of__panaw_2() { return &____panaw_2; }
	inline void set__panaw_2(uint32_t value)
	{
		____panaw_2 = value;
	}

	inline static int32_t get_offset_of__jijinalReardow_3() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____jijinalReardow_3)); }
	inline int32_t get__jijinalReardow_3() const { return ____jijinalReardow_3; }
	inline int32_t* get_address_of__jijinalReardow_3() { return &____jijinalReardow_3; }
	inline void set__jijinalReardow_3(int32_t value)
	{
		____jijinalReardow_3 = value;
	}

	inline static int32_t get_offset_of__coowemdiNeljou_4() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____coowemdiNeljou_4)); }
	inline String_t* get__coowemdiNeljou_4() const { return ____coowemdiNeljou_4; }
	inline String_t** get_address_of__coowemdiNeljou_4() { return &____coowemdiNeljou_4; }
	inline void set__coowemdiNeljou_4(String_t* value)
	{
		____coowemdiNeljou_4 = value;
		Il2CppCodeGenWriteBarrier(&____coowemdiNeljou_4, value);
	}

	inline static int32_t get_offset_of__wasmor_5() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____wasmor_5)); }
	inline bool get__wasmor_5() const { return ____wasmor_5; }
	inline bool* get_address_of__wasmor_5() { return &____wasmor_5; }
	inline void set__wasmor_5(bool value)
	{
		____wasmor_5 = value;
	}

	inline static int32_t get_offset_of__getreaHetalldou_6() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____getreaHetalldou_6)); }
	inline uint32_t get__getreaHetalldou_6() const { return ____getreaHetalldou_6; }
	inline uint32_t* get_address_of__getreaHetalldou_6() { return &____getreaHetalldou_6; }
	inline void set__getreaHetalldou_6(uint32_t value)
	{
		____getreaHetalldou_6 = value;
	}

	inline static int32_t get_offset_of__jejearWallseljar_7() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____jejearWallseljar_7)); }
	inline uint32_t get__jejearWallseljar_7() const { return ____jejearWallseljar_7; }
	inline uint32_t* get_address_of__jejearWallseljar_7() { return &____jejearWallseljar_7; }
	inline void set__jejearWallseljar_7(uint32_t value)
	{
		____jejearWallseljar_7 = value;
	}

	inline static int32_t get_offset_of__pasdras_8() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____pasdras_8)); }
	inline String_t* get__pasdras_8() const { return ____pasdras_8; }
	inline String_t** get_address_of__pasdras_8() { return &____pasdras_8; }
	inline void set__pasdras_8(String_t* value)
	{
		____pasdras_8 = value;
		Il2CppCodeGenWriteBarrier(&____pasdras_8, value);
	}

	inline static int32_t get_offset_of__revoutrou_9() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____revoutrou_9)); }
	inline int32_t get__revoutrou_9() const { return ____revoutrou_9; }
	inline int32_t* get_address_of__revoutrou_9() { return &____revoutrou_9; }
	inline void set__revoutrou_9(int32_t value)
	{
		____revoutrou_9 = value;
	}

	inline static int32_t get_offset_of__reartawchow_10() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____reartawchow_10)); }
	inline bool get__reartawchow_10() const { return ____reartawchow_10; }
	inline bool* get_address_of__reartawchow_10() { return &____reartawchow_10; }
	inline void set__reartawchow_10(bool value)
	{
		____reartawchow_10 = value;
	}

	inline static int32_t get_offset_of__vikow_11() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____vikow_11)); }
	inline int32_t get__vikow_11() const { return ____vikow_11; }
	inline int32_t* get_address_of__vikow_11() { return &____vikow_11; }
	inline void set__vikow_11(int32_t value)
	{
		____vikow_11 = value;
	}

	inline static int32_t get_offset_of__seredeariGomis_12() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____seredeariGomis_12)); }
	inline bool get__seredeariGomis_12() const { return ____seredeariGomis_12; }
	inline bool* get_address_of__seredeariGomis_12() { return &____seredeariGomis_12; }
	inline void set__seredeariGomis_12(bool value)
	{
		____seredeariGomis_12 = value;
	}

	inline static int32_t get_offset_of__hobaircerMeapisga_13() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____hobaircerMeapisga_13)); }
	inline String_t* get__hobaircerMeapisga_13() const { return ____hobaircerMeapisga_13; }
	inline String_t** get_address_of__hobaircerMeapisga_13() { return &____hobaircerMeapisga_13; }
	inline void set__hobaircerMeapisga_13(String_t* value)
	{
		____hobaircerMeapisga_13 = value;
		Il2CppCodeGenWriteBarrier(&____hobaircerMeapisga_13, value);
	}

	inline static int32_t get_offset_of__lorjepa_14() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____lorjepa_14)); }
	inline String_t* get__lorjepa_14() const { return ____lorjepa_14; }
	inline String_t** get_address_of__lorjepa_14() { return &____lorjepa_14; }
	inline void set__lorjepa_14(String_t* value)
	{
		____lorjepa_14 = value;
		Il2CppCodeGenWriteBarrier(&____lorjepa_14, value);
	}

	inline static int32_t get_offset_of__vounolarJahe_15() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____vounolarJahe_15)); }
	inline float get__vounolarJahe_15() const { return ____vounolarJahe_15; }
	inline float* get_address_of__vounolarJahe_15() { return &____vounolarJahe_15; }
	inline void set__vounolarJahe_15(float value)
	{
		____vounolarJahe_15 = value;
	}

	inline static int32_t get_offset_of__daicawMayhejis_16() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____daicawMayhejis_16)); }
	inline uint32_t get__daicawMayhejis_16() const { return ____daicawMayhejis_16; }
	inline uint32_t* get_address_of__daicawMayhejis_16() { return &____daicawMayhejis_16; }
	inline void set__daicawMayhejis_16(uint32_t value)
	{
		____daicawMayhejis_16 = value;
	}

	inline static int32_t get_offset_of__namuRatursal_17() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____namuRatursal_17)); }
	inline float get__namuRatursal_17() const { return ____namuRatursal_17; }
	inline float* get_address_of__namuRatursal_17() { return &____namuRatursal_17; }
	inline void set__namuRatursal_17(float value)
	{
		____namuRatursal_17 = value;
	}

	inline static int32_t get_offset_of__pirstafar_18() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____pirstafar_18)); }
	inline bool get__pirstafar_18() const { return ____pirstafar_18; }
	inline bool* get_address_of__pirstafar_18() { return &____pirstafar_18; }
	inline void set__pirstafar_18(bool value)
	{
		____pirstafar_18 = value;
	}

	inline static int32_t get_offset_of__menor_19() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____menor_19)); }
	inline uint32_t get__menor_19() const { return ____menor_19; }
	inline uint32_t* get_address_of__menor_19() { return &____menor_19; }
	inline void set__menor_19(uint32_t value)
	{
		____menor_19 = value;
	}

	inline static int32_t get_offset_of__birgelbay_20() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____birgelbay_20)); }
	inline int32_t get__birgelbay_20() const { return ____birgelbay_20; }
	inline int32_t* get_address_of__birgelbay_20() { return &____birgelbay_20; }
	inline void set__birgelbay_20(int32_t value)
	{
		____birgelbay_20 = value;
	}

	inline static int32_t get_offset_of__lisamuDelhirris_21() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____lisamuDelhirris_21)); }
	inline uint32_t get__lisamuDelhirris_21() const { return ____lisamuDelhirris_21; }
	inline uint32_t* get_address_of__lisamuDelhirris_21() { return &____lisamuDelhirris_21; }
	inline void set__lisamuDelhirris_21(uint32_t value)
	{
		____lisamuDelhirris_21 = value;
	}

	inline static int32_t get_offset_of__lemqowtear_22() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____lemqowtear_22)); }
	inline String_t* get__lemqowtear_22() const { return ____lemqowtear_22; }
	inline String_t** get_address_of__lemqowtear_22() { return &____lemqowtear_22; }
	inline void set__lemqowtear_22(String_t* value)
	{
		____lemqowtear_22 = value;
		Il2CppCodeGenWriteBarrier(&____lemqowtear_22, value);
	}

	inline static int32_t get_offset_of__galcel_23() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____galcel_23)); }
	inline bool get__galcel_23() const { return ____galcel_23; }
	inline bool* get_address_of__galcel_23() { return &____galcel_23; }
	inline void set__galcel_23(bool value)
	{
		____galcel_23 = value;
	}

	inline static int32_t get_offset_of__lurbirJerawcor_24() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____lurbirJerawcor_24)); }
	inline uint32_t get__lurbirJerawcor_24() const { return ____lurbirJerawcor_24; }
	inline uint32_t* get_address_of__lurbirJerawcor_24() { return &____lurbirJerawcor_24; }
	inline void set__lurbirJerawcor_24(uint32_t value)
	{
		____lurbirJerawcor_24 = value;
	}

	inline static int32_t get_offset_of__rasji_25() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____rasji_25)); }
	inline int32_t get__rasji_25() const { return ____rasji_25; }
	inline int32_t* get_address_of__rasji_25() { return &____rasji_25; }
	inline void set__rasji_25(int32_t value)
	{
		____rasji_25 = value;
	}

	inline static int32_t get_offset_of__mobursi_26() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____mobursi_26)); }
	inline bool get__mobursi_26() const { return ____mobursi_26; }
	inline bool* get_address_of__mobursi_26() { return &____mobursi_26; }
	inline void set__mobursi_26(bool value)
	{
		____mobursi_26 = value;
	}

	inline static int32_t get_offset_of__temmurkelWhemxay_27() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____temmurkelWhemxay_27)); }
	inline String_t* get__temmurkelWhemxay_27() const { return ____temmurkelWhemxay_27; }
	inline String_t** get_address_of__temmurkelWhemxay_27() { return &____temmurkelWhemxay_27; }
	inline void set__temmurkelWhemxay_27(String_t* value)
	{
		____temmurkelWhemxay_27 = value;
		Il2CppCodeGenWriteBarrier(&____temmurkelWhemxay_27, value);
	}

	inline static int32_t get_offset_of__yehurhall_28() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____yehurhall_28)); }
	inline bool get__yehurhall_28() const { return ____yehurhall_28; }
	inline bool* get_address_of__yehurhall_28() { return &____yehurhall_28; }
	inline void set__yehurhall_28(bool value)
	{
		____yehurhall_28 = value;
	}

	inline static int32_t get_offset_of__stekarna_29() { return static_cast<int32_t>(offsetof(M_kookouwou78_t691894552, ____stekarna_29)); }
	inline int32_t get__stekarna_29() const { return ____stekarna_29; }
	inline int32_t* get_address_of__stekarna_29() { return &____stekarna_29; }
	inline void set__stekarna_29(int32_t value)
	{
		____stekarna_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
