﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CombatEntity
struct CombatEntity_t684137495;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entity.Behavior.IBehavior
struct  IBehavior_t770859129  : public Il2CppObject
{
public:
	// System.Boolean Entity.Behavior.IBehavior::IsPause
	bool ___IsPause_0;
	// CombatEntity Entity.Behavior.IBehavior::<entity>k__BackingField
	CombatEntity_t684137495 * ___U3CentityU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_IsPause_0() { return static_cast<int32_t>(offsetof(IBehavior_t770859129, ___IsPause_0)); }
	inline bool get_IsPause_0() const { return ___IsPause_0; }
	inline bool* get_address_of_IsPause_0() { return &___IsPause_0; }
	inline void set_IsPause_0(bool value)
	{
		___IsPause_0 = value;
	}

	inline static int32_t get_offset_of_U3CentityU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(IBehavior_t770859129, ___U3CentityU3Ek__BackingField_1)); }
	inline CombatEntity_t684137495 * get_U3CentityU3Ek__BackingField_1() const { return ___U3CentityU3Ek__BackingField_1; }
	inline CombatEntity_t684137495 ** get_address_of_U3CentityU3Ek__BackingField_1() { return &___U3CentityU3Ek__BackingField_1; }
	inline void set_U3CentityU3Ek__BackingField_1(CombatEntity_t684137495 * value)
	{
		___U3CentityU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CentityU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
