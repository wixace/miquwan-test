﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_jerecile223
struct M_jerecile223_t3340444166;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_jerecile223::.ctor()
extern "C"  void M_jerecile223__ctor_m2563653661 (M_jerecile223_t3340444166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jerecile223::M_fesowhalTrubocar0(System.String[],System.Int32)
extern "C"  void M_jerecile223_M_fesowhalTrubocar0_m2589381929 (M_jerecile223_t3340444166 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jerecile223::M_treejaHawpowfir1(System.String[],System.Int32)
extern "C"  void M_jerecile223_M_treejaHawpowfir1_m3694476097 (M_jerecile223_t3340444166 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
