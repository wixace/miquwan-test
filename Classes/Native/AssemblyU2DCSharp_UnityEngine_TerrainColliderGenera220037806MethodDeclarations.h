﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_TerrainColliderGenerated
struct UnityEngine_TerrainColliderGenerated_t220037806;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_TerrainColliderGenerated::.ctor()
extern "C"  void UnityEngine_TerrainColliderGenerated__ctor_m2651900845 (UnityEngine_TerrainColliderGenerated_t220037806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TerrainColliderGenerated::TerrainCollider_TerrainCollider1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TerrainColliderGenerated_TerrainCollider_TerrainCollider1_m3032161433 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TerrainColliderGenerated::TerrainCollider_terrainData(JSVCall)
extern "C"  void UnityEngine_TerrainColliderGenerated_TerrainCollider_terrainData_m2593497235 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TerrainColliderGenerated::__Register()
extern "C"  void UnityEngine_TerrainColliderGenerated___Register_m2693657914 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TerrainColliderGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_TerrainColliderGenerated_ilo_attachFinalizerObject1_m1213363154 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
