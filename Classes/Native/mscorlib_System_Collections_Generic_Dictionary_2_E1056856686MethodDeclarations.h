﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1938158026MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PushType,PluginPush/NotificationData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m212531086(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1056856686 *, Dictionary_2_t4034500590 *, const MethodInfo*))Enumerator__ctor_m396494165_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PushType,PluginPush/NotificationData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1518052829(__this, method) ((  Il2CppObject * (*) (Enumerator_t1056856686 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m817845174_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PushType,PluginPush/NotificationData>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3807067239(__this, method) ((  void (*) (Enumerator_t1056856686 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1157578816_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<PushType,PluginPush/NotificationData>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3636311582(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1056856686 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1960616567_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PushType,PluginPush/NotificationData>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m784091385(__this, method) ((  Il2CppObject * (*) (Enumerator_t1056856686 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1945268242_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PushType,PluginPush/NotificationData>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2940980555(__this, method) ((  Il2CppObject * (*) (Enumerator_t1056856686 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2140443172_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<PushType,PluginPush/NotificationData>::MoveNext()
#define Enumerator_MoveNext_m756770839(__this, method) ((  bool (*) (Enumerator_t1056856686 *, const MethodInfo*))Enumerator_MoveNext_m2697807600_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<PushType,PluginPush/NotificationData>::get_Current()
#define Enumerator_get_Current_m943156677(__this, method) ((  KeyValuePair_2_t3933281296  (*) (Enumerator_t1056856686 *, const MethodInfo*))Enumerator_get_Current_m912297676_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<PushType,PluginPush/NotificationData>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m509881312(__this, method) ((  int32_t (*) (Enumerator_t1056856686 *, const MethodInfo*))Enumerator_get_CurrentKey_m3381004409_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<PushType,PluginPush/NotificationData>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2122252448(__this, method) ((  NotificationData_t3289515031 * (*) (Enumerator_t1056856686 *, const MethodInfo*))Enumerator_get_CurrentValue_m3438306169_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PushType,PluginPush/NotificationData>::Reset()
#define Enumerator_Reset_m940448992(__this, method) ((  void (*) (Enumerator_t1056856686 *, const MethodInfo*))Enumerator_Reset_m2343661607_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PushType,PluginPush/NotificationData>::VerifyState()
#define Enumerator_VerifyState_m529548521(__this, method) ((  void (*) (Enumerator_t1056856686 *, const MethodInfo*))Enumerator_VerifyState_m1040434928_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PushType,PluginPush/NotificationData>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m539892241(__this, method) ((  void (*) (Enumerator_t1056856686 *, const MethodInfo*))Enumerator_VerifyCurrent_m1875457624_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PushType,PluginPush/NotificationData>::Dispose()
#define Enumerator_Dispose_m56810928(__this, method) ((  void (*) (Enumerator_t1056856686 *, const MethodInfo*))Enumerator_Dispose_m4219370295_gshared)(__this, method)
