﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AssetBundleRequestGenerated
struct UnityEngine_AssetBundleRequestGenerated_t871399690;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_AssetBundleRequestGenerated::.ctor()
extern "C"  void UnityEngine_AssetBundleRequestGenerated__ctor_m4035090849 (UnityEngine_AssetBundleRequestGenerated_t871399690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleRequestGenerated::AssetBundleRequest_AssetBundleRequest1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleRequestGenerated_AssetBundleRequest_AssetBundleRequest1_m2959590097 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AssetBundleRequestGenerated::AssetBundleRequest_asset(JSVCall)
extern "C"  void UnityEngine_AssetBundleRequestGenerated_AssetBundleRequest_asset_m2984561334 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AssetBundleRequestGenerated::AssetBundleRequest_allAssets(JSVCall)
extern "C"  void UnityEngine_AssetBundleRequestGenerated_AssetBundleRequest_allAssets_m2871578530 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AssetBundleRequestGenerated::__Register()
extern "C"  void UnityEngine_AssetBundleRequestGenerated___Register_m2660417478 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleRequestGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_AssetBundleRequestGenerated_ilo_attachFinalizerObject1_m2938314870 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AssetBundleRequestGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_AssetBundleRequestGenerated_ilo_addJSCSRel2_m2018875486 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AssetBundleRequestGenerated::ilo_moveSaveID2Arr3(System.Int32)
extern "C"  void UnityEngine_AssetBundleRequestGenerated_ilo_moveSaveID2Arr3_m3857392888 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AssetBundleRequestGenerated::ilo_setArrayS4(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_AssetBundleRequestGenerated_ilo_setArrayS4_m2002394565 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
