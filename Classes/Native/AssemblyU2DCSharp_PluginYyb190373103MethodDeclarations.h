﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginYyb
struct PluginYyb_t190373103;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// VersionMgr
struct VersionMgr_t1322950208;
// System.Object
struct Il2CppObject;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_FloatTextMgr630384591.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginYyb::.ctor()
extern "C"  void PluginYyb__ctor_m3800716636 (PluginYyb_t190373103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYyb::Init()
extern "C"  void PluginYyb_Init_m766877048 (PluginYyb_t190373103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginYyb::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginYyb_ReqSDKHttpLogin_m1984353101 (PluginYyb_t190373103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginYyb::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginYyb_IsLoginSuccess_m2406047119 (PluginYyb_t190373103 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYyb::OpenUserLogin()
extern "C"  void PluginYyb_OpenUserLogin_m4088814510 (PluginYyb_t190373103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYyb::UserPay(CEvent.ZEvent)
extern "C"  void PluginYyb_UserPay_m250435588 (PluginYyb_t190373103 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYyb::TryGetPayOpenKey()
extern "C"  void PluginYyb_TryGetPayOpenKey_m1051741136 (PluginYyb_t190373103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYyb::OnInitSuccess(System.String)
extern "C"  void PluginYyb_OnInitSuccess_m2869519316 (PluginYyb_t190373103 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYyb::OnInitFail(System.String)
extern "C"  void PluginYyb_OnInitFail_m3620650477 (PluginYyb_t190373103 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYyb::OnLoginSuccess(System.String)
extern "C"  void PluginYyb_OnLoginSuccess_m3445464929 (PluginYyb_t190373103 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYyb::OnLoginFail(System.String)
extern "C"  void PluginYyb_OnLoginFail_m3495818624 (PluginYyb_t190373103 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYyb::OnPaySuccess(System.String)
extern "C"  void PluginYyb_OnPaySuccess_m1211524576 (PluginYyb_t190373103 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYyb::OnPayFail(System.String)
extern "C"  void PluginYyb_OnPayFail_m605280097 (PluginYyb_t190373103 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYyb::OnLogoutSuccess(System.String)
extern "C"  void PluginYyb_OnLogoutSuccess_m3733740366 (PluginYyb_t190373103 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYyb::<OnLogoutSuccess>m__480()
extern "C"  void PluginYyb_U3COnLogoutSuccessU3Em__480_m1339776249 (PluginYyb_t190373103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginYyb::ilo_get_isSdkLogin1(VersionMgr)
extern "C"  bool PluginYyb_ilo_get_isSdkLogin1_m1404255690 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginYyb::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginYyb_ilo_get_Instance2_m1430930040 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYyb::ilo_Log3(System.Object,System.Boolean)
extern "C"  void PluginYyb_ilo_Log3_m2052716275 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYyb::ilo_FloatText4(FloatTextMgr,FLOAT_TEXT_ID,System.Int32,System.Object[])
extern "C"  void PluginYyb_ilo_FloatText4_m686492042 (Il2CppObject * __this /* static, unused */, FloatTextMgr_t630384591 * ____this0, int32_t ___textId1, int32_t ___offsetY2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYyb::ilo_Logout5(System.Action)
extern "C"  void PluginYyb_ilo_Logout5_m113888939 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
