﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// superskillCfg
struct superskillCfg_t3024131278;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void superskillCfg::.ctor()
extern "C"  void superskillCfg__ctor_m49894493 (superskillCfg_t3024131278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void superskillCfg_Init_m3509043720 (superskillCfg_t3024131278 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
