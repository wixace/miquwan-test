﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_HumanTraitGenerated
struct UnityEngine_HumanTraitGenerated_t3791372102;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_HumanTraitGenerated::.ctor()
extern "C"  void UnityEngine_HumanTraitGenerated__ctor_m3049064165 (UnityEngine_HumanTraitGenerated_t3791372102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_HumanTraitGenerated::HumanTrait_HumanTrait1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_HumanTraitGenerated_HumanTrait_HumanTrait1_m1834170509 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanTraitGenerated::HumanTrait_MuscleCount(JSVCall)
extern "C"  void UnityEngine_HumanTraitGenerated_HumanTrait_MuscleCount_m3868578536 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanTraitGenerated::HumanTrait_MuscleName(JSVCall)
extern "C"  void UnityEngine_HumanTraitGenerated_HumanTrait_MuscleName_m3658789802 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanTraitGenerated::HumanTrait_BoneCount(JSVCall)
extern "C"  void UnityEngine_HumanTraitGenerated_HumanTrait_BoneCount_m2557374203 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanTraitGenerated::HumanTrait_BoneName(JSVCall)
extern "C"  void UnityEngine_HumanTraitGenerated_HumanTrait_BoneName_m2508114231 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanTraitGenerated::HumanTrait_RequiredBoneCount(JSVCall)
extern "C"  void UnityEngine_HumanTraitGenerated_HumanTrait_RequiredBoneCount_m210996474 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_HumanTraitGenerated::HumanTrait_BoneFromMuscle__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_HumanTraitGenerated_HumanTrait_BoneFromMuscle__Int32_m2780135148 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_HumanTraitGenerated::HumanTrait_GetMuscleDefaultMax__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_HumanTraitGenerated_HumanTrait_GetMuscleDefaultMax__Int32_m2148400297 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_HumanTraitGenerated::HumanTrait_GetMuscleDefaultMin__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_HumanTraitGenerated_HumanTrait_GetMuscleDefaultMin__Int32_m546671227 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_HumanTraitGenerated::HumanTrait_GetParentBone__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_HumanTraitGenerated_HumanTrait_GetParentBone__Int32_m476634767 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_HumanTraitGenerated::HumanTrait_MuscleFromBone__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_HumanTraitGenerated_HumanTrait_MuscleFromBone__Int32__Int32_m2201107812 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_HumanTraitGenerated::HumanTrait_RequiredBone__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_HumanTraitGenerated_HumanTrait_RequiredBone__Int32_m992319752 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanTraitGenerated::__Register()
extern "C"  void UnityEngine_HumanTraitGenerated___Register_m2178809090 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_HumanTraitGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_HumanTraitGenerated_ilo_attachFinalizerObject1_m1958541234 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanTraitGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_HumanTraitGenerated_ilo_addJSCSRel2_m1446097826 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanTraitGenerated::ilo_setInt323(System.Int32,System.Int32)
extern "C"  void UnityEngine_HumanTraitGenerated_ilo_setInt323_m1060621391 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanTraitGenerated::ilo_moveSaveID2Arr4(System.Int32)
extern "C"  void UnityEngine_HumanTraitGenerated_ilo_moveSaveID2Arr4_m1311453053 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_HumanTraitGenerated::ilo_getInt325(System.Int32)
extern "C"  int32_t UnityEngine_HumanTraitGenerated_ilo_getInt325_m2958171148 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
