﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3771233898;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimeUpdateVo
struct  TimeUpdateVo_t1829512047  : public Il2CppObject
{
public:
	// System.Action TimeUpdateVo::action
	Action_t3771233898 * ___action_0;
	// System.Double TimeUpdateVo::delay
	double ___delay_1;
	// System.Double TimeUpdateVo::curTime
	double ___curTime_2;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(TimeUpdateVo_t1829512047, ___action_0)); }
	inline Action_t3771233898 * get_action_0() const { return ___action_0; }
	inline Action_t3771233898 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_t3771233898 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier(&___action_0, value);
	}

	inline static int32_t get_offset_of_delay_1() { return static_cast<int32_t>(offsetof(TimeUpdateVo_t1829512047, ___delay_1)); }
	inline double get_delay_1() const { return ___delay_1; }
	inline double* get_address_of_delay_1() { return &___delay_1; }
	inline void set_delay_1(double value)
	{
		___delay_1 = value;
	}

	inline static int32_t get_offset_of_curTime_2() { return static_cast<int32_t>(offsetof(TimeUpdateVo_t1829512047, ___curTime_2)); }
	inline double get_curTime_2() const { return ___curTime_2; }
	inline double* get_address_of_curTime_2() { return &___curTime_2; }
	inline void set_curTime_2(double value)
	{
		___curTime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
