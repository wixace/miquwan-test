﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._39ad81979896bc42e49b785b9f00b2d7
struct _39ad81979896bc42e49b785b9f00b2d7_t1602972682;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__39ad81979896bc42e49b785b1602972682.h"

// System.Void Little._39ad81979896bc42e49b785b9f00b2d7::.ctor()
extern "C"  void _39ad81979896bc42e49b785b9f00b2d7__ctor_m791651683 (_39ad81979896bc42e49b785b9f00b2d7_t1602972682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._39ad81979896bc42e49b785b9f00b2d7::_39ad81979896bc42e49b785b9f00b2d7m2(System.Int32)
extern "C"  int32_t _39ad81979896bc42e49b785b9f00b2d7__39ad81979896bc42e49b785b9f00b2d7m2_m2692760921 (_39ad81979896bc42e49b785b9f00b2d7_t1602972682 * __this, int32_t ____39ad81979896bc42e49b785b9f00b2d7a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._39ad81979896bc42e49b785b9f00b2d7::_39ad81979896bc42e49b785b9f00b2d7m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _39ad81979896bc42e49b785b9f00b2d7__39ad81979896bc42e49b785b9f00b2d7m_m1802639741 (_39ad81979896bc42e49b785b9f00b2d7_t1602972682 * __this, int32_t ____39ad81979896bc42e49b785b9f00b2d7a0, int32_t ____39ad81979896bc42e49b785b9f00b2d771, int32_t ____39ad81979896bc42e49b785b9f00b2d7c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._39ad81979896bc42e49b785b9f00b2d7::ilo__39ad81979896bc42e49b785b9f00b2d7m21(Little._39ad81979896bc42e49b785b9f00b2d7,System.Int32)
extern "C"  int32_t _39ad81979896bc42e49b785b9f00b2d7_ilo__39ad81979896bc42e49b785b9f00b2d7m21_m757772561 (Il2CppObject * __this /* static, unused */, _39ad81979896bc42e49b785b9f00b2d7_t1602972682 * ____this0, int32_t ____39ad81979896bc42e49b785b9f00b2d7a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
