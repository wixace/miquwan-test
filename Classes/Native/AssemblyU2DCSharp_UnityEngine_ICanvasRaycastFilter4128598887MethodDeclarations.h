﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ICanvasRaycastFilterGenerated
struct UnityEngine_ICanvasRaycastFilterGenerated_t4128598887;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_ICanvasRaycastFilterGenerated::.ctor()
extern "C"  void UnityEngine_ICanvasRaycastFilterGenerated__ctor_m3503091172 (UnityEngine_ICanvasRaycastFilterGenerated_t4128598887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ICanvasRaycastFilterGenerated::ICanvasRaycastFilter_IsRaycastLocationValid__Vector2__Camera(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ICanvasRaycastFilterGenerated_ICanvasRaycastFilter_IsRaycastLocationValid__Vector2__Camera_m1572808073 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ICanvasRaycastFilterGenerated::__Register()
extern "C"  void UnityEngine_ICanvasRaycastFilterGenerated___Register_m636591459 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ICanvasRaycastFilterGenerated::ilo_setBooleanS1(System.Int32,System.Boolean)
extern "C"  void UnityEngine_ICanvasRaycastFilterGenerated_ilo_setBooleanS1_m299812181 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
