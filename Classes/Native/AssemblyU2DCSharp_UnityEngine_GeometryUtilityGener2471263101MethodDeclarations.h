﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GeometryUtilityGenerated
struct UnityEngine_GeometryUtilityGenerated_t2471263101;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Plane[]
struct PlaneU5BU5D_t1447812263;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_GeometryUtilityGenerated::.ctor()
extern "C"  void UnityEngine_GeometryUtilityGenerated__ctor_m3634056126 (UnityEngine_GeometryUtilityGenerated_t2471263101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GeometryUtilityGenerated::GeometryUtility_GeometryUtility1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GeometryUtilityGenerated_GeometryUtility_GeometryUtility1_m1698964358 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GeometryUtilityGenerated::GeometryUtility_CalculateFrustumPlanes__Matrix4x4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GeometryUtilityGenerated_GeometryUtility_CalculateFrustumPlanes__Matrix4x4_m1539420623 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GeometryUtilityGenerated::GeometryUtility_CalculateFrustumPlanes__Camera(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GeometryUtilityGenerated_GeometryUtility_CalculateFrustumPlanes__Camera_m3453593959 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GeometryUtilityGenerated::GeometryUtility_TestPlanesAABB__Plane_Array__Bounds(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GeometryUtilityGenerated_GeometryUtility_TestPlanesAABB__Plane_Array__Bounds_m1954691637 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GeometryUtilityGenerated::__Register()
extern "C"  void UnityEngine_GeometryUtilityGenerated___Register_m4120238025 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Plane[] UnityEngine_GeometryUtilityGenerated::<GeometryUtility_TestPlanesAABB__Plane_Array__Bounds>m__251()
extern "C"  PlaneU5BU5D_t1447812263* UnityEngine_GeometryUtilityGenerated_U3CGeometryUtility_TestPlanesAABB__Plane_Array__BoundsU3Em__251_m2376962446 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GeometryUtilityGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_GeometryUtilityGenerated_ilo_attachFinalizerObject1_m4164155873 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GeometryUtilityGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_GeometryUtilityGenerated_ilo_addJSCSRel2_m528498683 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_GeometryUtilityGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_GeometryUtilityGenerated_ilo_getObject3_m4095364083 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GeometryUtilityGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_GeometryUtilityGenerated_ilo_setObject4_m946970959 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GeometryUtilityGenerated::ilo_setArrayS5(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_GeometryUtilityGenerated_ilo_setArrayS5_m1564273673 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GeometryUtilityGenerated::ilo_getElement6(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_GeometryUtilityGenerated_ilo_getElement6_m755248443 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
