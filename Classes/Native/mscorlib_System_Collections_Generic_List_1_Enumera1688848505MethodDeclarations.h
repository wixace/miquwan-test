﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.NavmeshAdd>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1338838638(__this, ___l0, method) ((  void (*) (Enumerator_t1688848505 *, List_1_t1669175735 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.NavmeshAdd>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2362587428(__this, method) ((  void (*) (Enumerator_t1688848505 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.NavmeshAdd>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3013741914(__this, method) ((  Il2CppObject * (*) (Enumerator_t1688848505 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.NavmeshAdd>::Dispose()
#define Enumerator_Dispose_m3750336467(__this, method) ((  void (*) (Enumerator_t1688848505 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.NavmeshAdd>::VerifyState()
#define Enumerator_VerifyState_m3082237324(__this, method) ((  void (*) (Enumerator_t1688848505 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.NavmeshAdd>::MoveNext()
#define Enumerator_MoveNext_m881690452(__this, method) ((  bool (*) (Enumerator_t1688848505 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.NavmeshAdd>::get_Current()
#define Enumerator_get_Current_m3503418469(__this, method) ((  NavmeshAdd_t300990183 * (*) (Enumerator_t1688848505 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
