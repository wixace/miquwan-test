﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginYouwo
struct PluginYouwo_t2559649700;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// VersionMgr
struct VersionMgr_t1322950208;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// Mihua.SDK.LvUpInfo
struct LvUpInfo_t411687177;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_PluginYouwo2559649700.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginYouwo::.ctor()
extern "C"  void PluginYouwo__ctor_m2097327175 (PluginYouwo_t2559649700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouwo::Init()
extern "C"  void PluginYouwo_Init_m850476333 (PluginYouwo_t2559649700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginYouwo::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginYouwo_ReqSDKHttpLogin_m4246748408 (PluginYouwo_t2559649700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginYouwo::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginYouwo_IsLoginSuccess_m4148341572 (PluginYouwo_t2559649700 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouwo::OpenUserLogin()
extern "C"  void PluginYouwo_OpenUserLogin_m2924124057 (PluginYouwo_t2559649700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouwo::UserPay(CEvent.ZEvent)
extern "C"  void PluginYouwo_UserPay_m1189408185 (PluginYouwo_t2559649700 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouwo::CreatRole(CEvent.ZEvent)
extern "C"  void PluginYouwo_CreatRole_m3605927607 (PluginYouwo_t2559649700 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouwo::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginYouwo_RoleUpgrade_m1388569776 (PluginYouwo_t2559649700 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouwo::OnLoginSuccess(System.String)
extern "C"  void PluginYouwo_OnLoginSuccess_m2195268620 (PluginYouwo_t2559649700 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouwo::OnLogoutSuccess(System.String)
extern "C"  void PluginYouwo_OnLogoutSuccess_m3632360451 (PluginYouwo_t2559649700 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouwo::OnLogout(System.String)
extern "C"  void PluginYouwo_OnLogout_m4250804380 (PluginYouwo_t2559649700 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouwo::initSdk(System.String,System.String,System.String)
extern "C"  void PluginYouwo_initSdk_m2970044747 (PluginYouwo_t2559649700 * __this, String_t* ___gameId0, String_t* ___gameName1, String_t* ___appKey2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouwo::login()
extern "C"  void PluginYouwo_login_m1619341998 (PluginYouwo_t2559649700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouwo::logout()
extern "C"  void PluginYouwo_logout_m2960784391 (PluginYouwo_t2559649700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouwo::creatRole(System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYouwo_creatRole_m702813710 (PluginYouwo_t2559649700 * __this, String_t* ___server_id0, String_t* ___server_name1, String_t* ___role_id2, String_t* ___role_name3, String_t* ___role_level4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouwo::roleUpgrade(System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYouwo_roleUpgrade_m1387015431 (PluginYouwo_t2559649700 * __this, String_t* ___server_id0, String_t* ___server_name1, String_t* ___role_id2, String_t* ___role_name3, String_t* ___role_level4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouwo::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYouwo_pay_m1052567129 (PluginYouwo_t2559649700 * __this, String_t* ___subject0, String_t* ___product_id1, String_t* ___game_sn2, String_t* ___server_id3, String_t* ___role_id4, String_t* ___role_name5, String_t* ___role_level6, String_t* ___game_param7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouwo::<OnLogout>m__47A()
extern "C"  void PluginYouwo_U3COnLogoutU3Em__47A_m1516536551 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouwo::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginYouwo_ilo_AddEventListener1_m3784562637 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginYouwo::ilo_get_DeviceID2(PluginsSdkMgr)
extern "C"  String_t* PluginYouwo_ilo_get_DeviceID2_m4080481223 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginYouwo::ilo_get_Instance3()
extern "C"  VersionMgr_t1322950208 * PluginYouwo_ilo_get_Instance3_m3388631396 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouwo::ilo_login4(PluginYouwo)
extern "C"  void PluginYouwo_ilo_login4_m3491728531 (Il2CppObject * __this /* static, unused */, PluginYouwo_t2559649700 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProductsCfgMgr PluginYouwo::ilo_get_ProductsCfgMgr5()
extern "C"  ProductsCfgMgr_t2493714872 * PluginYouwo_ilo_get_ProductsCfgMgr5_m2943957067 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouwo::ilo_pay6(PluginYouwo,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYouwo_ilo_pay6_m3259503856 (Il2CppObject * __this /* static, unused */, PluginYouwo_t2559649700 * ____this0, String_t* ___subject1, String_t* ___product_id2, String_t* ___game_sn3, String_t* ___server_id4, String_t* ___role_id5, String_t* ___role_name6, String_t* ___role_level7, String_t* ___game_param8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouwo::ilo_creatRole7(PluginYouwo,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYouwo_ilo_creatRole7_m561956434 (Il2CppObject * __this /* static, unused */, PluginYouwo_t2559649700 * ____this0, String_t* ___server_id1, String_t* ___server_name2, String_t* ___role_id3, String_t* ___role_name4, String_t* ___role_level5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.LvUpInfo PluginYouwo::ilo_Parse8(System.Object[])
extern "C"  LvUpInfo_t411687177 * PluginYouwo_ilo_Parse8_m4229154877 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouwo::ilo_Logout9(System.Action)
extern "C"  void PluginYouwo_ilo_Logout9_m791476572 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouwo::ilo_ReqSDKHttpLogin10(PluginsSdkMgr)
extern "C"  void PluginYouwo_ilo_ReqSDKHttpLogin10_m516054398 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
