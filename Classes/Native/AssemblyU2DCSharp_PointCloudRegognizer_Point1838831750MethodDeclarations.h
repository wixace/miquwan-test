﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PointCloudRegognizer/Point
struct Point_t1838831750;
struct Point_t1838831750_marshaled_pinvoke;
struct Point_t1838831750_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PointCloudRegognizer_Point1838831750.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void PointCloudRegognizer/Point::.ctor(System.Int32,UnityEngine.Vector2)
extern "C"  void Point__ctor_m2928540442 (Point_t1838831750 * __this, int32_t ___strokeId0, Vector2_t4282066565  ___pos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PointCloudRegognizer/Point::.ctor(System.Int32,System.Single,System.Single)
extern "C"  void Point__ctor_m2642227376 (Point_t1838831750 * __this, int32_t ___strokeId0, float ___x1, float ___y2, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Point_t1838831750;
struct Point_t1838831750_marshaled_pinvoke;

extern "C" void Point_t1838831750_marshal_pinvoke(const Point_t1838831750& unmarshaled, Point_t1838831750_marshaled_pinvoke& marshaled);
extern "C" void Point_t1838831750_marshal_pinvoke_back(const Point_t1838831750_marshaled_pinvoke& marshaled, Point_t1838831750& unmarshaled);
extern "C" void Point_t1838831750_marshal_pinvoke_cleanup(Point_t1838831750_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Point_t1838831750;
struct Point_t1838831750_marshaled_com;

extern "C" void Point_t1838831750_marshal_com(const Point_t1838831750& unmarshaled, Point_t1838831750_marshaled_com& marshaled);
extern "C" void Point_t1838831750_marshal_com_back(const Point_t1838831750_marshaled_com& marshaled, Point_t1838831750& unmarshaled);
extern "C" void Point_t1838831750_marshal_com_cleanup(Point_t1838831750_marshaled_com& marshaled);
