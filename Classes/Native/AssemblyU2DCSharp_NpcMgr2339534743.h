﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// NpcMgr
struct NpcMgr_t2339534743;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.Collections.Generic.Dictionary`2<System.Int32,JSCWaveNpcConfig>
struct Dictionary_2_t4218767895;
// JSCWaveNpcConfig
struct JSCWaveNpcConfig_t4221504656;
// GameMgr
struct GameMgr_t1469029542;
// Biaoche
struct Biaoche_t1544748459;
// HeroNpc
struct HeroNpc_t2475966567;
// System.Collections.Generic.List`1<CSHeroUnit>
struct List_1_t837576702;
// JSCLevelConfig
struct JSCLevelConfig_t1411099500;
// Boss
struct Boss_t2076557;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NpcMgr
struct  NpcMgr_t2339534743  : public Il2CppObject
{
public:
	// UnityEngine.GameObject NpcMgr::monsterContainer
	GameObject_t3674682005 * ___monsterContainer_5;
	// System.Collections.Generic.List`1<CombatEntity> NpcMgr::curNpcList
	List_1_t2052323047 * ___curNpcList_6;
	// System.Collections.Generic.List`1<CombatEntity> NpcMgr::curOtherlist
	List_1_t2052323047 * ___curOtherlist_7;
	// System.Collections.Generic.List`1<CombatEntity> NpcMgr::curUnFinishList
	List_1_t2052323047 * ___curUnFinishList_8;
	// System.Collections.Generic.List`1<CombatEntity> NpcMgr::curGuideList
	List_1_t2052323047 * ___curGuideList_9;
	// System.Collections.Generic.List`1<CombatEntity> NpcMgr::monsterList
	List_1_t2052323047 * ___monsterList_10;
	// System.Collections.Generic.List`1<CombatEntity> NpcMgr::friendsList
	List_1_t2052323047 * ___friendsList_11;
	// System.Collections.Generic.List`1<CombatEntity> NpcMgr::neutralList
	List_1_t2052323047 * ___neutralList_12;
	// System.Collections.Generic.List`1<System.Int32> NpcMgr::deathList
	List_1_t2522024052 * ___deathList_13;
	// System.Collections.Generic.List`1<CombatEntity> NpcMgr::aniShowNpcList
	List_1_t2052323047 * ___aniShowNpcList_14;
	// System.Collections.Generic.List`1<CombatEntity> NpcMgr::aniShowNpcInTruthList
	List_1_t2052323047 * ___aniShowNpcInTruthList_15;
	// System.Collections.Generic.Dictionary`2<System.Int32,JSCWaveNpcConfig> NpcMgr::cacheNpcDict
	Dictionary_2_t4218767895 * ___cacheNpcDict_16;
	// System.Collections.Generic.Dictionary`2<System.Int32,JSCWaveNpcConfig> NpcMgr::cacheOtherDict
	Dictionary_2_t4218767895 * ___cacheOtherDict_17;
	// System.Collections.Generic.Dictionary`2<System.Int32,JSCWaveNpcConfig> NpcMgr::cachePlotDict
	Dictionary_2_t4218767895 * ___cachePlotDict_18;
	// System.Collections.Generic.Dictionary`2<System.Int32,JSCWaveNpcConfig> NpcMgr::cacheGuideDict
	Dictionary_2_t4218767895 * ___cacheGuideDict_19;
	// JSCWaveNpcConfig NpcMgr::jscWaveNpcConfig_pvp
	JSCWaveNpcConfig_t4221504656 * ___jscWaveNpcConfig_pvp_20;
	// GameMgr NpcMgr::gameMgr
	GameMgr_t1469029542 * ___gameMgr_21;
	// System.Boolean NpcMgr::IsNotCreateMonster
	bool ___IsNotCreateMonster_22;
	// Biaoche NpcMgr::biaoche
	Biaoche_t1544748459 * ___biaoche_23;
	// System.Boolean NpcMgr::isPowerBuff
	bool ___isPowerBuff_24;
	// HeroNpc NpcMgr::devilMonster
	HeroNpc_t2475966567 * ___devilMonster_25;
	// System.Collections.Generic.List`1<System.Int32> NpcMgr::GlobalBuffIDList
	List_1_t2522024052 * ___GlobalBuffIDList_26;
	// System.Single NpcMgr::scrAtkInAllBuff
	float ___scrAtkInAllBuff_27;
	// System.Collections.Generic.List`1<CSHeroUnit> NpcMgr::monsterUnits
	List_1_t837576702 * ___monsterUnits_28;
	// System.Int32 NpcMgr::heroIndex
	int32_t ___heroIndex_29;
	// JSCLevelConfig NpcMgr::levelConfig
	JSCLevelConfig_t1411099500 * ___levelConfig_30;
	// System.Int32 NpcMgr::_BossConsumeHp
	int32_t ____BossConsumeHp_31;
	// System.Int32 NpcMgr::_ShiliBossConsumeHp
	int32_t ____ShiliBossConsumeHp_32;
	// Boss NpcMgr::<boss>k__BackingField
	Boss_t2076557 * ___U3CbossU3Ek__BackingField_33;
	// System.Int32 NpcMgr::<curRound>k__BackingField
	int32_t ___U3CcurRoundU3Ek__BackingField_34;
	// System.Int32 NpcMgr::<curOtherRound>k__BackingField
	int32_t ___U3CcurOtherRoundU3Ek__BackingField_35;
	// System.Boolean NpcMgr::<isKillBoss>k__BackingField
	bool ___U3CisKillBossU3Ek__BackingField_36;

public:
	inline static int32_t get_offset_of_monsterContainer_5() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___monsterContainer_5)); }
	inline GameObject_t3674682005 * get_monsterContainer_5() const { return ___monsterContainer_5; }
	inline GameObject_t3674682005 ** get_address_of_monsterContainer_5() { return &___monsterContainer_5; }
	inline void set_monsterContainer_5(GameObject_t3674682005 * value)
	{
		___monsterContainer_5 = value;
		Il2CppCodeGenWriteBarrier(&___monsterContainer_5, value);
	}

	inline static int32_t get_offset_of_curNpcList_6() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___curNpcList_6)); }
	inline List_1_t2052323047 * get_curNpcList_6() const { return ___curNpcList_6; }
	inline List_1_t2052323047 ** get_address_of_curNpcList_6() { return &___curNpcList_6; }
	inline void set_curNpcList_6(List_1_t2052323047 * value)
	{
		___curNpcList_6 = value;
		Il2CppCodeGenWriteBarrier(&___curNpcList_6, value);
	}

	inline static int32_t get_offset_of_curOtherlist_7() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___curOtherlist_7)); }
	inline List_1_t2052323047 * get_curOtherlist_7() const { return ___curOtherlist_7; }
	inline List_1_t2052323047 ** get_address_of_curOtherlist_7() { return &___curOtherlist_7; }
	inline void set_curOtherlist_7(List_1_t2052323047 * value)
	{
		___curOtherlist_7 = value;
		Il2CppCodeGenWriteBarrier(&___curOtherlist_7, value);
	}

	inline static int32_t get_offset_of_curUnFinishList_8() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___curUnFinishList_8)); }
	inline List_1_t2052323047 * get_curUnFinishList_8() const { return ___curUnFinishList_8; }
	inline List_1_t2052323047 ** get_address_of_curUnFinishList_8() { return &___curUnFinishList_8; }
	inline void set_curUnFinishList_8(List_1_t2052323047 * value)
	{
		___curUnFinishList_8 = value;
		Il2CppCodeGenWriteBarrier(&___curUnFinishList_8, value);
	}

	inline static int32_t get_offset_of_curGuideList_9() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___curGuideList_9)); }
	inline List_1_t2052323047 * get_curGuideList_9() const { return ___curGuideList_9; }
	inline List_1_t2052323047 ** get_address_of_curGuideList_9() { return &___curGuideList_9; }
	inline void set_curGuideList_9(List_1_t2052323047 * value)
	{
		___curGuideList_9 = value;
		Il2CppCodeGenWriteBarrier(&___curGuideList_9, value);
	}

	inline static int32_t get_offset_of_monsterList_10() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___monsterList_10)); }
	inline List_1_t2052323047 * get_monsterList_10() const { return ___monsterList_10; }
	inline List_1_t2052323047 ** get_address_of_monsterList_10() { return &___monsterList_10; }
	inline void set_monsterList_10(List_1_t2052323047 * value)
	{
		___monsterList_10 = value;
		Il2CppCodeGenWriteBarrier(&___monsterList_10, value);
	}

	inline static int32_t get_offset_of_friendsList_11() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___friendsList_11)); }
	inline List_1_t2052323047 * get_friendsList_11() const { return ___friendsList_11; }
	inline List_1_t2052323047 ** get_address_of_friendsList_11() { return &___friendsList_11; }
	inline void set_friendsList_11(List_1_t2052323047 * value)
	{
		___friendsList_11 = value;
		Il2CppCodeGenWriteBarrier(&___friendsList_11, value);
	}

	inline static int32_t get_offset_of_neutralList_12() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___neutralList_12)); }
	inline List_1_t2052323047 * get_neutralList_12() const { return ___neutralList_12; }
	inline List_1_t2052323047 ** get_address_of_neutralList_12() { return &___neutralList_12; }
	inline void set_neutralList_12(List_1_t2052323047 * value)
	{
		___neutralList_12 = value;
		Il2CppCodeGenWriteBarrier(&___neutralList_12, value);
	}

	inline static int32_t get_offset_of_deathList_13() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___deathList_13)); }
	inline List_1_t2522024052 * get_deathList_13() const { return ___deathList_13; }
	inline List_1_t2522024052 ** get_address_of_deathList_13() { return &___deathList_13; }
	inline void set_deathList_13(List_1_t2522024052 * value)
	{
		___deathList_13 = value;
		Il2CppCodeGenWriteBarrier(&___deathList_13, value);
	}

	inline static int32_t get_offset_of_aniShowNpcList_14() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___aniShowNpcList_14)); }
	inline List_1_t2052323047 * get_aniShowNpcList_14() const { return ___aniShowNpcList_14; }
	inline List_1_t2052323047 ** get_address_of_aniShowNpcList_14() { return &___aniShowNpcList_14; }
	inline void set_aniShowNpcList_14(List_1_t2052323047 * value)
	{
		___aniShowNpcList_14 = value;
		Il2CppCodeGenWriteBarrier(&___aniShowNpcList_14, value);
	}

	inline static int32_t get_offset_of_aniShowNpcInTruthList_15() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___aniShowNpcInTruthList_15)); }
	inline List_1_t2052323047 * get_aniShowNpcInTruthList_15() const { return ___aniShowNpcInTruthList_15; }
	inline List_1_t2052323047 ** get_address_of_aniShowNpcInTruthList_15() { return &___aniShowNpcInTruthList_15; }
	inline void set_aniShowNpcInTruthList_15(List_1_t2052323047 * value)
	{
		___aniShowNpcInTruthList_15 = value;
		Il2CppCodeGenWriteBarrier(&___aniShowNpcInTruthList_15, value);
	}

	inline static int32_t get_offset_of_cacheNpcDict_16() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___cacheNpcDict_16)); }
	inline Dictionary_2_t4218767895 * get_cacheNpcDict_16() const { return ___cacheNpcDict_16; }
	inline Dictionary_2_t4218767895 ** get_address_of_cacheNpcDict_16() { return &___cacheNpcDict_16; }
	inline void set_cacheNpcDict_16(Dictionary_2_t4218767895 * value)
	{
		___cacheNpcDict_16 = value;
		Il2CppCodeGenWriteBarrier(&___cacheNpcDict_16, value);
	}

	inline static int32_t get_offset_of_cacheOtherDict_17() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___cacheOtherDict_17)); }
	inline Dictionary_2_t4218767895 * get_cacheOtherDict_17() const { return ___cacheOtherDict_17; }
	inline Dictionary_2_t4218767895 ** get_address_of_cacheOtherDict_17() { return &___cacheOtherDict_17; }
	inline void set_cacheOtherDict_17(Dictionary_2_t4218767895 * value)
	{
		___cacheOtherDict_17 = value;
		Il2CppCodeGenWriteBarrier(&___cacheOtherDict_17, value);
	}

	inline static int32_t get_offset_of_cachePlotDict_18() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___cachePlotDict_18)); }
	inline Dictionary_2_t4218767895 * get_cachePlotDict_18() const { return ___cachePlotDict_18; }
	inline Dictionary_2_t4218767895 ** get_address_of_cachePlotDict_18() { return &___cachePlotDict_18; }
	inline void set_cachePlotDict_18(Dictionary_2_t4218767895 * value)
	{
		___cachePlotDict_18 = value;
		Il2CppCodeGenWriteBarrier(&___cachePlotDict_18, value);
	}

	inline static int32_t get_offset_of_cacheGuideDict_19() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___cacheGuideDict_19)); }
	inline Dictionary_2_t4218767895 * get_cacheGuideDict_19() const { return ___cacheGuideDict_19; }
	inline Dictionary_2_t4218767895 ** get_address_of_cacheGuideDict_19() { return &___cacheGuideDict_19; }
	inline void set_cacheGuideDict_19(Dictionary_2_t4218767895 * value)
	{
		___cacheGuideDict_19 = value;
		Il2CppCodeGenWriteBarrier(&___cacheGuideDict_19, value);
	}

	inline static int32_t get_offset_of_jscWaveNpcConfig_pvp_20() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___jscWaveNpcConfig_pvp_20)); }
	inline JSCWaveNpcConfig_t4221504656 * get_jscWaveNpcConfig_pvp_20() const { return ___jscWaveNpcConfig_pvp_20; }
	inline JSCWaveNpcConfig_t4221504656 ** get_address_of_jscWaveNpcConfig_pvp_20() { return &___jscWaveNpcConfig_pvp_20; }
	inline void set_jscWaveNpcConfig_pvp_20(JSCWaveNpcConfig_t4221504656 * value)
	{
		___jscWaveNpcConfig_pvp_20 = value;
		Il2CppCodeGenWriteBarrier(&___jscWaveNpcConfig_pvp_20, value);
	}

	inline static int32_t get_offset_of_gameMgr_21() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___gameMgr_21)); }
	inline GameMgr_t1469029542 * get_gameMgr_21() const { return ___gameMgr_21; }
	inline GameMgr_t1469029542 ** get_address_of_gameMgr_21() { return &___gameMgr_21; }
	inline void set_gameMgr_21(GameMgr_t1469029542 * value)
	{
		___gameMgr_21 = value;
		Il2CppCodeGenWriteBarrier(&___gameMgr_21, value);
	}

	inline static int32_t get_offset_of_IsNotCreateMonster_22() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___IsNotCreateMonster_22)); }
	inline bool get_IsNotCreateMonster_22() const { return ___IsNotCreateMonster_22; }
	inline bool* get_address_of_IsNotCreateMonster_22() { return &___IsNotCreateMonster_22; }
	inline void set_IsNotCreateMonster_22(bool value)
	{
		___IsNotCreateMonster_22 = value;
	}

	inline static int32_t get_offset_of_biaoche_23() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___biaoche_23)); }
	inline Biaoche_t1544748459 * get_biaoche_23() const { return ___biaoche_23; }
	inline Biaoche_t1544748459 ** get_address_of_biaoche_23() { return &___biaoche_23; }
	inline void set_biaoche_23(Biaoche_t1544748459 * value)
	{
		___biaoche_23 = value;
		Il2CppCodeGenWriteBarrier(&___biaoche_23, value);
	}

	inline static int32_t get_offset_of_isPowerBuff_24() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___isPowerBuff_24)); }
	inline bool get_isPowerBuff_24() const { return ___isPowerBuff_24; }
	inline bool* get_address_of_isPowerBuff_24() { return &___isPowerBuff_24; }
	inline void set_isPowerBuff_24(bool value)
	{
		___isPowerBuff_24 = value;
	}

	inline static int32_t get_offset_of_devilMonster_25() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___devilMonster_25)); }
	inline HeroNpc_t2475966567 * get_devilMonster_25() const { return ___devilMonster_25; }
	inline HeroNpc_t2475966567 ** get_address_of_devilMonster_25() { return &___devilMonster_25; }
	inline void set_devilMonster_25(HeroNpc_t2475966567 * value)
	{
		___devilMonster_25 = value;
		Il2CppCodeGenWriteBarrier(&___devilMonster_25, value);
	}

	inline static int32_t get_offset_of_GlobalBuffIDList_26() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___GlobalBuffIDList_26)); }
	inline List_1_t2522024052 * get_GlobalBuffIDList_26() const { return ___GlobalBuffIDList_26; }
	inline List_1_t2522024052 ** get_address_of_GlobalBuffIDList_26() { return &___GlobalBuffIDList_26; }
	inline void set_GlobalBuffIDList_26(List_1_t2522024052 * value)
	{
		___GlobalBuffIDList_26 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalBuffIDList_26, value);
	}

	inline static int32_t get_offset_of_scrAtkInAllBuff_27() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___scrAtkInAllBuff_27)); }
	inline float get_scrAtkInAllBuff_27() const { return ___scrAtkInAllBuff_27; }
	inline float* get_address_of_scrAtkInAllBuff_27() { return &___scrAtkInAllBuff_27; }
	inline void set_scrAtkInAllBuff_27(float value)
	{
		___scrAtkInAllBuff_27 = value;
	}

	inline static int32_t get_offset_of_monsterUnits_28() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___monsterUnits_28)); }
	inline List_1_t837576702 * get_monsterUnits_28() const { return ___monsterUnits_28; }
	inline List_1_t837576702 ** get_address_of_monsterUnits_28() { return &___monsterUnits_28; }
	inline void set_monsterUnits_28(List_1_t837576702 * value)
	{
		___monsterUnits_28 = value;
		Il2CppCodeGenWriteBarrier(&___monsterUnits_28, value);
	}

	inline static int32_t get_offset_of_heroIndex_29() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___heroIndex_29)); }
	inline int32_t get_heroIndex_29() const { return ___heroIndex_29; }
	inline int32_t* get_address_of_heroIndex_29() { return &___heroIndex_29; }
	inline void set_heroIndex_29(int32_t value)
	{
		___heroIndex_29 = value;
	}

	inline static int32_t get_offset_of_levelConfig_30() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___levelConfig_30)); }
	inline JSCLevelConfig_t1411099500 * get_levelConfig_30() const { return ___levelConfig_30; }
	inline JSCLevelConfig_t1411099500 ** get_address_of_levelConfig_30() { return &___levelConfig_30; }
	inline void set_levelConfig_30(JSCLevelConfig_t1411099500 * value)
	{
		___levelConfig_30 = value;
		Il2CppCodeGenWriteBarrier(&___levelConfig_30, value);
	}

	inline static int32_t get_offset_of__BossConsumeHp_31() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ____BossConsumeHp_31)); }
	inline int32_t get__BossConsumeHp_31() const { return ____BossConsumeHp_31; }
	inline int32_t* get_address_of__BossConsumeHp_31() { return &____BossConsumeHp_31; }
	inline void set__BossConsumeHp_31(int32_t value)
	{
		____BossConsumeHp_31 = value;
	}

	inline static int32_t get_offset_of__ShiliBossConsumeHp_32() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ____ShiliBossConsumeHp_32)); }
	inline int32_t get__ShiliBossConsumeHp_32() const { return ____ShiliBossConsumeHp_32; }
	inline int32_t* get_address_of__ShiliBossConsumeHp_32() { return &____ShiliBossConsumeHp_32; }
	inline void set__ShiliBossConsumeHp_32(int32_t value)
	{
		____ShiliBossConsumeHp_32 = value;
	}

	inline static int32_t get_offset_of_U3CbossU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___U3CbossU3Ek__BackingField_33)); }
	inline Boss_t2076557 * get_U3CbossU3Ek__BackingField_33() const { return ___U3CbossU3Ek__BackingField_33; }
	inline Boss_t2076557 ** get_address_of_U3CbossU3Ek__BackingField_33() { return &___U3CbossU3Ek__BackingField_33; }
	inline void set_U3CbossU3Ek__BackingField_33(Boss_t2076557 * value)
	{
		___U3CbossU3Ek__BackingField_33 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbossU3Ek__BackingField_33, value);
	}

	inline static int32_t get_offset_of_U3CcurRoundU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___U3CcurRoundU3Ek__BackingField_34)); }
	inline int32_t get_U3CcurRoundU3Ek__BackingField_34() const { return ___U3CcurRoundU3Ek__BackingField_34; }
	inline int32_t* get_address_of_U3CcurRoundU3Ek__BackingField_34() { return &___U3CcurRoundU3Ek__BackingField_34; }
	inline void set_U3CcurRoundU3Ek__BackingField_34(int32_t value)
	{
		___U3CcurRoundU3Ek__BackingField_34 = value;
	}

	inline static int32_t get_offset_of_U3CcurOtherRoundU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___U3CcurOtherRoundU3Ek__BackingField_35)); }
	inline int32_t get_U3CcurOtherRoundU3Ek__BackingField_35() const { return ___U3CcurOtherRoundU3Ek__BackingField_35; }
	inline int32_t* get_address_of_U3CcurOtherRoundU3Ek__BackingField_35() { return &___U3CcurOtherRoundU3Ek__BackingField_35; }
	inline void set_U3CcurOtherRoundU3Ek__BackingField_35(int32_t value)
	{
		___U3CcurOtherRoundU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of_U3CisKillBossU3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743, ___U3CisKillBossU3Ek__BackingField_36)); }
	inline bool get_U3CisKillBossU3Ek__BackingField_36() const { return ___U3CisKillBossU3Ek__BackingField_36; }
	inline bool* get_address_of_U3CisKillBossU3Ek__BackingField_36() { return &___U3CisKillBossU3Ek__BackingField_36; }
	inline void set_U3CisKillBossU3Ek__BackingField_36(bool value)
	{
		___U3CisKillBossU3Ek__BackingField_36 = value;
	}
};

struct NpcMgr_t2339534743_StaticFields
{
public:
	// NpcMgr NpcMgr::_inst
	NpcMgr_t2339534743 * ____inst_4;

public:
	inline static int32_t get_offset_of__inst_4() { return static_cast<int32_t>(offsetof(NpcMgr_t2339534743_StaticFields, ____inst_4)); }
	inline NpcMgr_t2339534743 * get__inst_4() const { return ____inst_4; }
	inline NpcMgr_t2339534743 ** get_address_of__inst_4() { return &____inst_4; }
	inline void set__inst_4(NpcMgr_t2339534743 * value)
	{
		____inst_4 = value;
		Il2CppCodeGenWriteBarrier(&____inst_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
