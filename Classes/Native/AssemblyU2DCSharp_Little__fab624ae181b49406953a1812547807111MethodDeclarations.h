﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._fab624ae181b49406953a181a69a3293
struct _fab624ae181b49406953a181a69a3293_t2547807111;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__fab624ae181b49406953a1812547807111.h"

// System.Void Little._fab624ae181b49406953a181a69a3293::.ctor()
extern "C"  void _fab624ae181b49406953a181a69a3293__ctor_m200456838 (_fab624ae181b49406953a181a69a3293_t2547807111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._fab624ae181b49406953a181a69a3293::_fab624ae181b49406953a181a69a3293m2(System.Int32)
extern "C"  int32_t _fab624ae181b49406953a181a69a3293__fab624ae181b49406953a181a69a3293m2_m232762425 (_fab624ae181b49406953a181a69a3293_t2547807111 * __this, int32_t ____fab624ae181b49406953a181a69a3293a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._fab624ae181b49406953a181a69a3293::_fab624ae181b49406953a181a69a3293m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _fab624ae181b49406953a181a69a3293__fab624ae181b49406953a181a69a3293m_m2889494173 (_fab624ae181b49406953a181a69a3293_t2547807111 * __this, int32_t ____fab624ae181b49406953a181a69a3293a0, int32_t ____fab624ae181b49406953a181a69a3293691, int32_t ____fab624ae181b49406953a181a69a3293c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._fab624ae181b49406953a181a69a3293::ilo__fab624ae181b49406953a181a69a3293m21(Little._fab624ae181b49406953a181a69a3293,System.Int32)
extern "C"  int32_t _fab624ae181b49406953a181a69a3293_ilo__fab624ae181b49406953a181a69a3293m21_m1899077422 (Il2CppObject * __this /* static, unused */, _fab624ae181b49406953a181a69a3293_t2547807111 * ____this0, int32_t ____fab624ae181b49406953a181a69a3293a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
