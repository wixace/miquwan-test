﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._7398858b399286fd156d20c25bce046f
struct _7398858b399286fd156d20c25bce046f_t79527896;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__7398858b399286fd156d20c25b79527896.h"

// System.Void Little._7398858b399286fd156d20c25bce046f::.ctor()
extern "C"  void _7398858b399286fd156d20c25bce046f__ctor_m1220278613 (_7398858b399286fd156d20c25bce046f_t79527896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._7398858b399286fd156d20c25bce046f::_7398858b399286fd156d20c25bce046fm2(System.Int32)
extern "C"  int32_t _7398858b399286fd156d20c25bce046f__7398858b399286fd156d20c25bce046fm2_m2133607065 (_7398858b399286fd156d20c25bce046f_t79527896 * __this, int32_t ____7398858b399286fd156d20c25bce046fa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._7398858b399286fd156d20c25bce046f::_7398858b399286fd156d20c25bce046fm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _7398858b399286fd156d20c25bce046f__7398858b399286fd156d20c25bce046fm_m279020605 (_7398858b399286fd156d20c25bce046f_t79527896 * __this, int32_t ____7398858b399286fd156d20c25bce046fa0, int32_t ____7398858b399286fd156d20c25bce046f121, int32_t ____7398858b399286fd156d20c25bce046fc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._7398858b399286fd156d20c25bce046f::ilo__7398858b399286fd156d20c25bce046fm21(Little._7398858b399286fd156d20c25bce046f,System.Int32)
extern "C"  int32_t _7398858b399286fd156d20c25bce046f_ilo__7398858b399286fd156d20c25bce046fm21_m1631692959 (Il2CppObject * __this /* static, unused */, _7398858b399286fd156d20c25bce046f_t79527896 * ____this0, int32_t ____7398858b399286fd156d20c25bce046fa1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
