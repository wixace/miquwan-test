﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Meta.CallbackSet
struct CallbackSet_t3590429999;
// ProtoBuf.Meta.MetaType
struct MetaType_t448283965;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Exception
struct Exception_t3991598821;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_MetaType448283965.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel_Callback2866957669.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_CallbackSet3590429999.h"

// System.Void ProtoBuf.Meta.CallbackSet::.ctor(ProtoBuf.Meta.MetaType)
extern "C"  void CallbackSet__ctor_m2604221103 (CallbackSet_t3590429999 * __this, MetaType_t448283965 * ___metaType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Meta.CallbackSet::get_Item(ProtoBuf.Meta.TypeModel/CallbackType)
extern "C"  MethodInfo_t * CallbackSet_get_Item_m806962589 (CallbackSet_t3590429999 * __this, int32_t ___callbackType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.CallbackSet::CheckCallbackParameters(ProtoBuf.Meta.TypeModel,System.Reflection.MethodInfo)
extern "C"  bool CallbackSet_CheckCallbackParameters_m1529962119 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, MethodInfo_t * ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Meta.CallbackSet::SanityCheckCallback(ProtoBuf.Meta.TypeModel,System.Reflection.MethodInfo)
extern "C"  MethodInfo_t * CallbackSet_SanityCheckCallback_m433939333 (CallbackSet_t3590429999 * __this, TypeModel_t2730011105 * ___model0, MethodInfo_t * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception ProtoBuf.Meta.CallbackSet::CreateInvalidCallbackSignature(System.Reflection.MethodInfo)
extern "C"  Exception_t3991598821 * CallbackSet_CreateInvalidCallbackSignature_m3323698792 (Il2CppObject * __this /* static, unused */, MethodInfo_t * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Meta.CallbackSet::get_BeforeSerialize()
extern "C"  MethodInfo_t * CallbackSet_get_BeforeSerialize_m1313577620 (CallbackSet_t3590429999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.CallbackSet::set_BeforeSerialize(System.Reflection.MethodInfo)
extern "C"  void CallbackSet_set_BeforeSerialize_m1357634275 (CallbackSet_t3590429999 * __this, MethodInfo_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Meta.CallbackSet::get_BeforeDeserialize()
extern "C"  MethodInfo_t * CallbackSet_get_BeforeDeserialize_m2517729971 (CallbackSet_t3590429999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.CallbackSet::set_BeforeDeserialize(System.Reflection.MethodInfo)
extern "C"  void CallbackSet_set_BeforeDeserialize_m2185215490 (CallbackSet_t3590429999 * __this, MethodInfo_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Meta.CallbackSet::get_AfterSerialize()
extern "C"  MethodInfo_t * CallbackSet_get_AfterSerialize_m844317299 (CallbackSet_t3590429999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.CallbackSet::set_AfterSerialize(System.Reflection.MethodInfo)
extern "C"  void CallbackSet_set_AfterSerialize_m2456467518 (CallbackSet_t3590429999 * __this, MethodInfo_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Meta.CallbackSet::get_AfterDeserialize()
extern "C"  MethodInfo_t * CallbackSet_get_AfterDeserialize_m2530127570 (CallbackSet_t3590429999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.CallbackSet::set_AfterDeserialize(System.Reflection.MethodInfo)
extern "C"  void CallbackSet_set_AfterDeserialize_m1602007197 (CallbackSet_t3590429999 * __this, MethodInfo_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.CallbackSet::get_NonTrivial()
extern "C"  bool CallbackSet_get_NonTrivial_m897102593 (CallbackSet_t3590429999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.CallbackSet::ilo_ThrowIfFrozen1(ProtoBuf.Meta.MetaType)
extern "C"  void CallbackSet_ilo_ThrowIfFrozen1_m2416449004 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.CallbackSet::ilo_CheckCallbackParameters2(ProtoBuf.Meta.TypeModel,System.Reflection.MethodInfo)
extern "C"  bool CallbackSet_ilo_CheckCallbackParameters2_m3149604980 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, MethodInfo_t * ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.TypeModel ProtoBuf.Meta.CallbackSet::ilo_get_Model3(ProtoBuf.Meta.MetaType)
extern "C"  TypeModel_t2730011105 * CallbackSet_ilo_get_Model3_m2008044263 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Meta.CallbackSet::ilo_SanityCheckCallback4(ProtoBuf.Meta.CallbackSet,ProtoBuf.Meta.TypeModel,System.Reflection.MethodInfo)
extern "C"  MethodInfo_t * CallbackSet_ilo_SanityCheckCallback4_m512203139 (Il2CppObject * __this /* static, unused */, CallbackSet_t3590429999 * ____this0, TypeModel_t2730011105 * ___model1, MethodInfo_t * ___callback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
