﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._29f9bc453a9dff6d7077c633a407a1d3
struct _29f9bc453a9dff6d7077c633a407a1d3_t1189118657;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._29f9bc453a9dff6d7077c633a407a1d3::.ctor()
extern "C"  void _29f9bc453a9dff6d7077c633a407a1d3__ctor_m2813537420 (_29f9bc453a9dff6d7077c633a407a1d3_t1189118657 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._29f9bc453a9dff6d7077c633a407a1d3::_29f9bc453a9dff6d7077c633a407a1d3m2(System.Int32)
extern "C"  int32_t _29f9bc453a9dff6d7077c633a407a1d3__29f9bc453a9dff6d7077c633a407a1d3m2_m3724317177 (_29f9bc453a9dff6d7077c633a407a1d3_t1189118657 * __this, int32_t ____29f9bc453a9dff6d7077c633a407a1d3a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._29f9bc453a9dff6d7077c633a407a1d3::_29f9bc453a9dff6d7077c633a407a1d3m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _29f9bc453a9dff6d7077c633a407a1d3__29f9bc453a9dff6d7077c633a407a1d3m_m3187097821 (_29f9bc453a9dff6d7077c633a407a1d3_t1189118657 * __this, int32_t ____29f9bc453a9dff6d7077c633a407a1d3a0, int32_t ____29f9bc453a9dff6d7077c633a407a1d3581, int32_t ____29f9bc453a9dff6d7077c633a407a1d3c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
