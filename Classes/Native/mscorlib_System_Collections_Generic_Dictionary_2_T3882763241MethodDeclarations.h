﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2232219549MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<PushType,PluginPush/NotificationData,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m1177798484(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t3882763241 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m2230835227_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PushType,PluginPush/NotificationData,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m494044904(__this, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Transform_1_t3882763241 *, int32_t, NotificationData_t3289515031 *, const MethodInfo*))Transform_1_Invoke_m3781741697_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<PushType,PluginPush/NotificationData,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m2931218887(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t3882763241 *, int32_t, NotificationData_t3289515031 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m683954144_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PushType,PluginPush/NotificationData,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m1736849570(__this, ___result0, method) ((  DictionaryEntry_t1751606614  (*) (Transform_1_t3882763241 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m680438121_gshared)(__this, ___result0, method)
