﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Core.RpsResult>
struct List_1_t1847780376;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1867453146.h"
#include "AssemblyU2DCSharp_Core_RpsResult479594824.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Core.RpsResult>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2771135414_gshared (Enumerator_t1867453146 * __this, List_1_t1847780376 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m2771135414(__this, ___l0, method) ((  void (*) (Enumerator_t1867453146 *, List_1_t1847780376 *, const MethodInfo*))Enumerator__ctor_m2771135414_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Core.RpsResult>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m364375772_gshared (Enumerator_t1867453146 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m364375772(__this, method) ((  void (*) (Enumerator_t1867453146 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m364375772_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Core.RpsResult>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3880754706_gshared (Enumerator_t1867453146 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3880754706(__this, method) ((  Il2CppObject * (*) (Enumerator_t1867453146 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3880754706_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Core.RpsResult>::Dispose()
extern "C"  void Enumerator_Dispose_m2247001883_gshared (Enumerator_t1867453146 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2247001883(__this, method) ((  void (*) (Enumerator_t1867453146 *, const MethodInfo*))Enumerator_Dispose_m2247001883_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Core.RpsResult>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3087220948_gshared (Enumerator_t1867453146 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3087220948(__this, method) ((  void (*) (Enumerator_t1867453146 *, const MethodInfo*))Enumerator_VerifyState_m3087220948_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Core.RpsResult>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4021986060_gshared (Enumerator_t1867453146 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m4021986060(__this, method) ((  bool (*) (Enumerator_t1867453146 *, const MethodInfo*))Enumerator_MoveNext_m4021986060_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Core.RpsResult>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1177512109_gshared (Enumerator_t1867453146 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1177512109(__this, method) ((  int32_t (*) (Enumerator_t1867453146 *, const MethodInfo*))Enumerator_get_Current_m1177512109_gshared)(__this, method)
