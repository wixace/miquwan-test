﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.AnimationLink/LinkClip
struct LinkClip_t2650609029;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.AnimationLink/LinkClip::.ctor()
extern "C"  void LinkClip__ctor_m4046790838 (LinkClip_t2650609029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.AnimationLink/LinkClip::get_name()
extern "C"  String_t* LinkClip_get_name_m3842813157 (LinkClip_t2650609029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
