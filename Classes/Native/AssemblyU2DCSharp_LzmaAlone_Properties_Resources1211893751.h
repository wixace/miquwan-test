﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Resources.ResourceManager
struct ResourceManager_t1323731545;
// System.Globalization.CultureInfo
struct CultureInfo_t1065375142;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LzmaAlone.Properties.Resources
struct  Resources_t1211893751  : public Il2CppObject
{
public:

public:
};

struct Resources_t1211893751_StaticFields
{
public:
	// System.Resources.ResourceManager LzmaAlone.Properties.Resources::_resMgr
	ResourceManager_t1323731545 * ____resMgr_0;
	// System.Globalization.CultureInfo LzmaAlone.Properties.Resources::_resCulture
	CultureInfo_t1065375142 * ____resCulture_1;

public:
	inline static int32_t get_offset_of__resMgr_0() { return static_cast<int32_t>(offsetof(Resources_t1211893751_StaticFields, ____resMgr_0)); }
	inline ResourceManager_t1323731545 * get__resMgr_0() const { return ____resMgr_0; }
	inline ResourceManager_t1323731545 ** get_address_of__resMgr_0() { return &____resMgr_0; }
	inline void set__resMgr_0(ResourceManager_t1323731545 * value)
	{
		____resMgr_0 = value;
		Il2CppCodeGenWriteBarrier(&____resMgr_0, value);
	}

	inline static int32_t get_offset_of__resCulture_1() { return static_cast<int32_t>(offsetof(Resources_t1211893751_StaticFields, ____resCulture_1)); }
	inline CultureInfo_t1065375142 * get__resCulture_1() const { return ____resCulture_1; }
	inline CultureInfo_t1065375142 ** get_address_of__resCulture_1() { return &____resCulture_1; }
	inline void set__resCulture_1(CultureInfo_t1065375142 * value)
	{
		____resCulture_1 = value;
		Il2CppCodeGenWriteBarrier(&____resCulture_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
