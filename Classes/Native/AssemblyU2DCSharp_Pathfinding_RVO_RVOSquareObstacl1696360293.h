﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Pathfinding_RVO_RVOObstacle1652911528.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RVO.RVOSquareObstacle
struct  RVOSquareObstacle_t1696360293  : public RVOObstacle_t1652911528
{
public:
	// System.Single Pathfinding.RVO.RVOSquareObstacle::height
	float ___height_11;
	// UnityEngine.Vector2 Pathfinding.RVO.RVOSquareObstacle::size
	Vector2_t4282066565  ___size_12;
	// UnityEngine.Vector2 Pathfinding.RVO.RVOSquareObstacle::center
	Vector2_t4282066565  ___center_13;

public:
	inline static int32_t get_offset_of_height_11() { return static_cast<int32_t>(offsetof(RVOSquareObstacle_t1696360293, ___height_11)); }
	inline float get_height_11() const { return ___height_11; }
	inline float* get_address_of_height_11() { return &___height_11; }
	inline void set_height_11(float value)
	{
		___height_11 = value;
	}

	inline static int32_t get_offset_of_size_12() { return static_cast<int32_t>(offsetof(RVOSquareObstacle_t1696360293, ___size_12)); }
	inline Vector2_t4282066565  get_size_12() const { return ___size_12; }
	inline Vector2_t4282066565 * get_address_of_size_12() { return &___size_12; }
	inline void set_size_12(Vector2_t4282066565  value)
	{
		___size_12 = value;
	}

	inline static int32_t get_offset_of_center_13() { return static_cast<int32_t>(offsetof(RVOSquareObstacle_t1696360293, ___center_13)); }
	inline Vector2_t4282066565  get_center_13() const { return ___center_13; }
	inline Vector2_t4282066565 * get_address_of_center_13() { return &___center_13; }
	inline void set_center_13(Vector2_t4282066565  value)
	{
		___center_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
