﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Single[]
struct SingleU5BU5D_t2316563989;
// System.Boolean[]
struct BooleanU5BU5D_t3456302923;

#include "AssemblyU2DCSharp_Pathfinding_MonoModifier200043088.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RadiusModifier
struct  RadiusModifier_t3948604841  : public MonoModifier_t200043088
{
public:
	// System.Single RadiusModifier::radius
	float ___radius_4;
	// System.Single RadiusModifier::detail
	float ___detail_5;
	// System.Single[] RadiusModifier::radi
	SingleU5BU5D_t2316563989* ___radi_6;
	// System.Single[] RadiusModifier::a1
	SingleU5BU5D_t2316563989* ___a1_7;
	// System.Single[] RadiusModifier::a2
	SingleU5BU5D_t2316563989* ___a2_8;
	// System.Boolean[] RadiusModifier::dir
	BooleanU5BU5D_t3456302923* ___dir_9;

public:
	inline static int32_t get_offset_of_radius_4() { return static_cast<int32_t>(offsetof(RadiusModifier_t3948604841, ___radius_4)); }
	inline float get_radius_4() const { return ___radius_4; }
	inline float* get_address_of_radius_4() { return &___radius_4; }
	inline void set_radius_4(float value)
	{
		___radius_4 = value;
	}

	inline static int32_t get_offset_of_detail_5() { return static_cast<int32_t>(offsetof(RadiusModifier_t3948604841, ___detail_5)); }
	inline float get_detail_5() const { return ___detail_5; }
	inline float* get_address_of_detail_5() { return &___detail_5; }
	inline void set_detail_5(float value)
	{
		___detail_5 = value;
	}

	inline static int32_t get_offset_of_radi_6() { return static_cast<int32_t>(offsetof(RadiusModifier_t3948604841, ___radi_6)); }
	inline SingleU5BU5D_t2316563989* get_radi_6() const { return ___radi_6; }
	inline SingleU5BU5D_t2316563989** get_address_of_radi_6() { return &___radi_6; }
	inline void set_radi_6(SingleU5BU5D_t2316563989* value)
	{
		___radi_6 = value;
		Il2CppCodeGenWriteBarrier(&___radi_6, value);
	}

	inline static int32_t get_offset_of_a1_7() { return static_cast<int32_t>(offsetof(RadiusModifier_t3948604841, ___a1_7)); }
	inline SingleU5BU5D_t2316563989* get_a1_7() const { return ___a1_7; }
	inline SingleU5BU5D_t2316563989** get_address_of_a1_7() { return &___a1_7; }
	inline void set_a1_7(SingleU5BU5D_t2316563989* value)
	{
		___a1_7 = value;
		Il2CppCodeGenWriteBarrier(&___a1_7, value);
	}

	inline static int32_t get_offset_of_a2_8() { return static_cast<int32_t>(offsetof(RadiusModifier_t3948604841, ___a2_8)); }
	inline SingleU5BU5D_t2316563989* get_a2_8() const { return ___a2_8; }
	inline SingleU5BU5D_t2316563989** get_address_of_a2_8() { return &___a2_8; }
	inline void set_a2_8(SingleU5BU5D_t2316563989* value)
	{
		___a2_8 = value;
		Il2CppCodeGenWriteBarrier(&___a2_8, value);
	}

	inline static int32_t get_offset_of_dir_9() { return static_cast<int32_t>(offsetof(RadiusModifier_t3948604841, ___dir_9)); }
	inline BooleanU5BU5D_t3456302923* get_dir_9() const { return ___dir_9; }
	inline BooleanU5BU5D_t3456302923** get_address_of_dir_9() { return &___dir_9; }
	inline void set_dir_9(BooleanU5BU5D_t3456302923* value)
	{
		___dir_9 = value;
		Il2CppCodeGenWriteBarrier(&___dir_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
