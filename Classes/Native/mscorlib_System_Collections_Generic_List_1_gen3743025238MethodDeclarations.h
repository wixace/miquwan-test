﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>
struct List_1_t3743025238;
// System.Collections.Generic.IEnumerable`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>
struct IEnumerable_1_t1380785347;
// System.Collections.Generic.IEnumerator`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>
struct IEnumerator_1_t4286704735;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>
struct ICollection_1_t3269429673;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>
struct ReadOnlyCollection_1_t3931917222;
// UnityEngine.MeshSubsetCombineUtility/SubMeshInstance[]
struct SubMeshInstanceU5BU5D_t2339421603;
// System.Predicate`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>
struct Predicate_1_t1985896569;
// System.Collections.Generic.IComparer`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>
struct IComparer_1_t654886432;
// System.Comparison`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>
struct Comparison_1_t1091200873;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_MeshSubsetCombineUtility_S2374839686.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3762698008.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::.ctor()
extern "C"  void List_1__ctor_m483933416_gshared (List_1_t3743025238 * __this, const MethodInfo* method);
#define List_1__ctor_m483933416(__this, method) ((  void (*) (List_1_t3743025238 *, const MethodInfo*))List_1__ctor_m483933416_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m2859689989_gshared (List_1_t3743025238 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m2859689989(__this, ___collection0, method) ((  void (*) (List_1_t3743025238 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m2859689989_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m3991081611_gshared (List_1_t3743025238 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m3991081611(__this, ___capacity0, method) ((  void (*) (List_1_t3743025238 *, int32_t, const MethodInfo*))List_1__ctor_m3991081611_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::.cctor()
extern "C"  void List_1__cctor_m2724490931_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m2724490931(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m2724490931_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1354400708_gshared (List_1_t3743025238 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1354400708(__this, method) ((  Il2CppObject* (*) (List_1_t3743025238 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1354400708_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3449878090_gshared (List_1_t3743025238 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m3449878090(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3743025238 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3449878090_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3546258393_gshared (List_1_t3743025238 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3546258393(__this, method) ((  Il2CppObject * (*) (List_1_t3743025238 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3546258393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m2042167812_gshared (List_1_t3743025238 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m2042167812(__this, ___item0, method) ((  int32_t (*) (List_1_t3743025238 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m2042167812_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1836469948_gshared (List_1_t3743025238 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1836469948(__this, ___item0, method) ((  bool (*) (List_1_t3743025238 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1836469948_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3235944028_gshared (List_1_t3743025238 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m3235944028(__this, ___item0, method) ((  int32_t (*) (List_1_t3743025238 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3235944028_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2486421455_gshared (List_1_t3743025238 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2486421455(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3743025238 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2486421455_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m3417070841_gshared (List_1_t3743025238 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m3417070841(__this, ___item0, method) ((  void (*) (List_1_t3743025238 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3417070841_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m940311485_gshared (List_1_t3743025238 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m940311485(__this, method) ((  bool (*) (List_1_t3743025238 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m940311485_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m977780512_gshared (List_1_t3743025238 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m977780512(__this, method) ((  bool (*) (List_1_t3743025238 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m977780512_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1759367186_gshared (List_1_t3743025238 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1759367186(__this, method) ((  Il2CppObject * (*) (List_1_t3743025238 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1759367186_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m2602482475_gshared (List_1_t3743025238 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m2602482475(__this, method) ((  bool (*) (List_1_t3743025238 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2602482475_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1784885870_gshared (List_1_t3743025238 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1784885870(__this, method) ((  bool (*) (List_1_t3743025238 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1784885870_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m3895330073_gshared (List_1_t3743025238 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m3895330073(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3743025238 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3895330073_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m2063154150_gshared (List_1_t3743025238 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m2063154150(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3743025238 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m2063154150_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::Add(T)
extern "C"  void List_1_Add_m178294232_gshared (List_1_t3743025238 * __this, SubMeshInstance_t2374839686  ___item0, const MethodInfo* method);
#define List_1_Add_m178294232(__this, ___item0, method) ((  void (*) (List_1_t3743025238 *, SubMeshInstance_t2374839686 , const MethodInfo*))List_1_Add_m178294232_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3028821696_gshared (List_1_t3743025238 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m3028821696(__this, ___newCount0, method) ((  void (*) (List_1_t3743025238 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m3028821696_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m2594850087_gshared (List_1_t3743025238 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m2594850087(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3743025238 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m2594850087_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m690847166_gshared (List_1_t3743025238 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m690847166(__this, ___collection0, method) ((  void (*) (List_1_t3743025238 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m690847166_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m4246786686_gshared (List_1_t3743025238 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m4246786686(__this, ___enumerable0, method) ((  void (*) (List_1_t3743025238 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m4246786686_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2104100185_gshared (List_1_t3743025238 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m2104100185(__this, ___collection0, method) ((  void (*) (List_1_t3743025238 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2104100185_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t3931917222 * List_1_AsReadOnly_m1113316492_gshared (List_1_t3743025238 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1113316492(__this, method) ((  ReadOnlyCollection_1_t3931917222 * (*) (List_1_t3743025238 *, const MethodInfo*))List_1_AsReadOnly_m1113316492_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::BinarySearch(T)
extern "C"  int32_t List_1_BinarySearch_m2766335047_gshared (List_1_t3743025238 * __this, SubMeshInstance_t2374839686  ___item0, const MethodInfo* method);
#define List_1_BinarySearch_m2766335047(__this, ___item0, method) ((  int32_t (*) (List_1_t3743025238 *, SubMeshInstance_t2374839686 , const MethodInfo*))List_1_BinarySearch_m2766335047_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::Clear()
extern "C"  void List_1_Clear_m2185034003_gshared (List_1_t3743025238 * __this, const MethodInfo* method);
#define List_1_Clear_m2185034003(__this, method) ((  void (*) (List_1_t3743025238 *, const MethodInfo*))List_1_Clear_m2185034003_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::Contains(T)
extern "C"  bool List_1_Contains_m3655088343_gshared (List_1_t3743025238 * __this, SubMeshInstance_t2374839686  ___item0, const MethodInfo* method);
#define List_1_Contains_m3655088343(__this, ___item0, method) ((  bool (*) (List_1_t3743025238 *, SubMeshInstance_t2374839686 , const MethodInfo*))List_1_Contains_m3655088343_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2641968565_gshared (List_1_t3743025238 * __this, SubMeshInstanceU5BU5D_t2339421603* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2641968565(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3743025238 *, SubMeshInstanceU5BU5D_t2339421603*, int32_t, const MethodInfo*))List_1_CopyTo_m2641968565_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::Find(System.Predicate`1<T>)
extern "C"  SubMeshInstance_t2374839686  List_1_Find_m1267636145_gshared (List_1_t3743025238 * __this, Predicate_1_t1985896569 * ___match0, const MethodInfo* method);
#define List_1_Find_m1267636145(__this, ___match0, method) ((  SubMeshInstance_t2374839686  (*) (List_1_t3743025238 *, Predicate_1_t1985896569 *, const MethodInfo*))List_1_Find_m1267636145_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m550141134_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t1985896569 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m550141134(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1985896569 *, const MethodInfo*))List_1_CheckMatch_m550141134_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m2317403307_gshared (List_1_t3743025238 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t1985896569 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m2317403307(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3743025238 *, int32_t, int32_t, Predicate_1_t1985896569 *, const MethodInfo*))List_1_GetIndex_m2317403307_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::GetEnumerator()
extern "C"  Enumerator_t3762698008  List_1_GetEnumerator_m553170580_gshared (List_1_t3743025238 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m553170580(__this, method) ((  Enumerator_t3762698008  (*) (List_1_t3743025238 *, const MethodInfo*))List_1_GetEnumerator_m553170580_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2500100161_gshared (List_1_t3743025238 * __this, SubMeshInstance_t2374839686  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2500100161(__this, ___item0, method) ((  int32_t (*) (List_1_t3743025238 *, SubMeshInstance_t2374839686 , const MethodInfo*))List_1_IndexOf_m2500100161_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2745601804_gshared (List_1_t3743025238 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m2745601804(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3743025238 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m2745601804_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m2388335749_gshared (List_1_t3743025238 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m2388335749(__this, ___index0, method) ((  void (*) (List_1_t3743025238 *, int32_t, const MethodInfo*))List_1_CheckIndex_m2388335749_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2152021356_gshared (List_1_t3743025238 * __this, int32_t ___index0, SubMeshInstance_t2374839686  ___item1, const MethodInfo* method);
#define List_1_Insert_m2152021356(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3743025238 *, int32_t, SubMeshInstance_t2374839686 , const MethodInfo*))List_1_Insert_m2152021356_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m3005831393_gshared (List_1_t3743025238 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m3005831393(__this, ___collection0, method) ((  void (*) (List_1_t3743025238 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3005831393_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::Remove(T)
extern "C"  bool List_1_Remove_m2242331282_gshared (List_1_t3743025238 * __this, SubMeshInstance_t2374839686  ___item0, const MethodInfo* method);
#define List_1_Remove_m2242331282(__this, ___item0, method) ((  bool (*) (List_1_t3743025238 *, SubMeshInstance_t2374839686 , const MethodInfo*))List_1_Remove_m2242331282_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m441921860_gshared (List_1_t3743025238 * __this, Predicate_1_t1985896569 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m441921860(__this, ___match0, method) ((  int32_t (*) (List_1_t3743025238 *, Predicate_1_t1985896569 *, const MethodInfo*))List_1_RemoveAll_m441921860_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m25874226_gshared (List_1_t3743025238 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m25874226(__this, ___index0, method) ((  void (*) (List_1_t3743025238 *, int32_t, const MethodInfo*))List_1_RemoveAt_m25874226_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m2866153301_gshared (List_1_t3743025238 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m2866153301(__this, ___index0, ___count1, method) ((  void (*) (List_1_t3743025238 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m2866153301_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::Reverse()
extern "C"  void List_1_Reverse_m657764858_gshared (List_1_t3743025238 * __this, const MethodInfo* method);
#define List_1_Reverse_m657764858(__this, method) ((  void (*) (List_1_t3743025238 *, const MethodInfo*))List_1_Reverse_m657764858_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::Sort()
extern "C"  void List_1_Sort_m3813302248_gshared (List_1_t3743025238 * __this, const MethodInfo* method);
#define List_1_Sort_m3813302248(__this, method) ((  void (*) (List_1_t3743025238 *, const MethodInfo*))List_1_Sort_m3813302248_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m2852776700_gshared (List_1_t3743025238 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m2852776700(__this, ___comparer0, method) ((  void (*) (List_1_t3743025238 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m2852776700_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1042419259_gshared (List_1_t3743025238 * __this, Comparison_1_t1091200873 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1042419259(__this, ___comparison0, method) ((  void (*) (List_1_t3743025238 *, Comparison_1_t1091200873 *, const MethodInfo*))List_1_Sort_m1042419259_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::ToArray()
extern "C"  SubMeshInstanceU5BU5D_t2339421603* List_1_ToArray_m3543078754_gshared (List_1_t3743025238 * __this, const MethodInfo* method);
#define List_1_ToArray_m3543078754(__this, method) ((  SubMeshInstanceU5BU5D_t2339421603* (*) (List_1_t3743025238 *, const MethodInfo*))List_1_ToArray_m3543078754_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::TrimExcess()
extern "C"  void List_1_TrimExcess_m211546497_gshared (List_1_t3743025238 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m211546497(__this, method) ((  void (*) (List_1_t3743025238 *, const MethodInfo*))List_1_TrimExcess_m211546497_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1652060145_gshared (List_1_t3743025238 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1652060145(__this, method) ((  int32_t (*) (List_1_t3743025238 *, const MethodInfo*))List_1_get_Capacity_m1652060145_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m239010514_gshared (List_1_t3743025238 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m239010514(__this, ___value0, method) ((  void (*) (List_1_t3743025238 *, int32_t, const MethodInfo*))List_1_set_Capacity_m239010514_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::get_Count()
extern "C"  int32_t List_1_get_Count_m11001178_gshared (List_1_t3743025238 * __this, const MethodInfo* method);
#define List_1_get_Count_m11001178(__this, method) ((  int32_t (*) (List_1_t3743025238 *, const MethodInfo*))List_1_get_Count_m11001178_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::get_Item(System.Int32)
extern "C"  SubMeshInstance_t2374839686  List_1_get_Item_m399448_gshared (List_1_t3743025238 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m399448(__this, ___index0, method) ((  SubMeshInstance_t2374839686  (*) (List_1_t3743025238 *, int32_t, const MethodInfo*))List_1_get_Item_m399448_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m4022850371_gshared (List_1_t3743025238 * __this, int32_t ___index0, SubMeshInstance_t2374839686  ___value1, const MethodInfo* method);
#define List_1_set_Item_m4022850371(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3743025238 *, int32_t, SubMeshInstance_t2374839686 , const MethodInfo*))List_1_set_Item_m4022850371_gshared)(__this, ___index0, ___value1, method)
