﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CombatEntity
struct CombatEntity_t684137495;
// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DamageSeparationSkill
struct  DamageSeparationSkill_t2001134364  : public Il2CppObject
{
public:
	// System.Int32 DamageSeparationSkill::skillIndex
	int32_t ___skillIndex_0;
	// System.Single DamageSeparationSkill::shootSkillRstTime
	float ___shootSkillRstTime_1;
	// System.Boolean DamageSeparationSkill::isFree
	bool ___isFree_2;
	// UnityEngine.Vector3 DamageSeparationSkill::center
	Vector3_t4282066566  ___center_3;
	// UnityEngine.Vector3 DamageSeparationSkill::lastCenter
	Vector3_t4282066566  ___lastCenter_4;
	// UnityEngine.Vector3 DamageSeparationSkill::BeforPlaySkillPos
	Vector3_t4282066566  ___BeforPlaySkillPos_5;
	// CombatEntity DamageSeparationSkill::atkTarget
	CombatEntity_t684137495 * ___atkTarget_6;
	// System.Collections.Generic.List`1<CombatEntity> DamageSeparationSkill::shootHitList
	List_1_t2052323047 * ___shootHitList_7;
	// System.Collections.Generic.List`1<CombatEntity> DamageSeparationSkill::shootSputterList
	List_1_t2052323047 * ___shootSputterList_8;

public:
	inline static int32_t get_offset_of_skillIndex_0() { return static_cast<int32_t>(offsetof(DamageSeparationSkill_t2001134364, ___skillIndex_0)); }
	inline int32_t get_skillIndex_0() const { return ___skillIndex_0; }
	inline int32_t* get_address_of_skillIndex_0() { return &___skillIndex_0; }
	inline void set_skillIndex_0(int32_t value)
	{
		___skillIndex_0 = value;
	}

	inline static int32_t get_offset_of_shootSkillRstTime_1() { return static_cast<int32_t>(offsetof(DamageSeparationSkill_t2001134364, ___shootSkillRstTime_1)); }
	inline float get_shootSkillRstTime_1() const { return ___shootSkillRstTime_1; }
	inline float* get_address_of_shootSkillRstTime_1() { return &___shootSkillRstTime_1; }
	inline void set_shootSkillRstTime_1(float value)
	{
		___shootSkillRstTime_1 = value;
	}

	inline static int32_t get_offset_of_isFree_2() { return static_cast<int32_t>(offsetof(DamageSeparationSkill_t2001134364, ___isFree_2)); }
	inline bool get_isFree_2() const { return ___isFree_2; }
	inline bool* get_address_of_isFree_2() { return &___isFree_2; }
	inline void set_isFree_2(bool value)
	{
		___isFree_2 = value;
	}

	inline static int32_t get_offset_of_center_3() { return static_cast<int32_t>(offsetof(DamageSeparationSkill_t2001134364, ___center_3)); }
	inline Vector3_t4282066566  get_center_3() const { return ___center_3; }
	inline Vector3_t4282066566 * get_address_of_center_3() { return &___center_3; }
	inline void set_center_3(Vector3_t4282066566  value)
	{
		___center_3 = value;
	}

	inline static int32_t get_offset_of_lastCenter_4() { return static_cast<int32_t>(offsetof(DamageSeparationSkill_t2001134364, ___lastCenter_4)); }
	inline Vector3_t4282066566  get_lastCenter_4() const { return ___lastCenter_4; }
	inline Vector3_t4282066566 * get_address_of_lastCenter_4() { return &___lastCenter_4; }
	inline void set_lastCenter_4(Vector3_t4282066566  value)
	{
		___lastCenter_4 = value;
	}

	inline static int32_t get_offset_of_BeforPlaySkillPos_5() { return static_cast<int32_t>(offsetof(DamageSeparationSkill_t2001134364, ___BeforPlaySkillPos_5)); }
	inline Vector3_t4282066566  get_BeforPlaySkillPos_5() const { return ___BeforPlaySkillPos_5; }
	inline Vector3_t4282066566 * get_address_of_BeforPlaySkillPos_5() { return &___BeforPlaySkillPos_5; }
	inline void set_BeforPlaySkillPos_5(Vector3_t4282066566  value)
	{
		___BeforPlaySkillPos_5 = value;
	}

	inline static int32_t get_offset_of_atkTarget_6() { return static_cast<int32_t>(offsetof(DamageSeparationSkill_t2001134364, ___atkTarget_6)); }
	inline CombatEntity_t684137495 * get_atkTarget_6() const { return ___atkTarget_6; }
	inline CombatEntity_t684137495 ** get_address_of_atkTarget_6() { return &___atkTarget_6; }
	inline void set_atkTarget_6(CombatEntity_t684137495 * value)
	{
		___atkTarget_6 = value;
		Il2CppCodeGenWriteBarrier(&___atkTarget_6, value);
	}

	inline static int32_t get_offset_of_shootHitList_7() { return static_cast<int32_t>(offsetof(DamageSeparationSkill_t2001134364, ___shootHitList_7)); }
	inline List_1_t2052323047 * get_shootHitList_7() const { return ___shootHitList_7; }
	inline List_1_t2052323047 ** get_address_of_shootHitList_7() { return &___shootHitList_7; }
	inline void set_shootHitList_7(List_1_t2052323047 * value)
	{
		___shootHitList_7 = value;
		Il2CppCodeGenWriteBarrier(&___shootHitList_7, value);
	}

	inline static int32_t get_offset_of_shootSputterList_8() { return static_cast<int32_t>(offsetof(DamageSeparationSkill_t2001134364, ___shootSputterList_8)); }
	inline List_1_t2052323047 * get_shootSputterList_8() const { return ___shootSputterList_8; }
	inline List_1_t2052323047 ** get_address_of_shootSputterList_8() { return &___shootSputterList_8; }
	inline void set_shootSputterList_8(List_1_t2052323047 * value)
	{
		___shootSputterList_8 = value;
		Il2CppCodeGenWriteBarrier(&___shootSputterList_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
