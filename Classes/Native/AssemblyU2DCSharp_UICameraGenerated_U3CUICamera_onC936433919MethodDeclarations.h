﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICameraGenerated/<UICamera_onClick_GetDelegate_member42_arg0>c__AnonStoreyA3
struct U3CUICamera_onClick_GetDelegate_member42_arg0U3Ec__AnonStoreyA3_t936433919;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UICameraGenerated/<UICamera_onClick_GetDelegate_member42_arg0>c__AnonStoreyA3::.ctor()
extern "C"  void U3CUICamera_onClick_GetDelegate_member42_arg0U3Ec__AnonStoreyA3__ctor_m3494442828 (U3CUICamera_onClick_GetDelegate_member42_arg0U3Ec__AnonStoreyA3_t936433919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated/<UICamera_onClick_GetDelegate_member42_arg0>c__AnonStoreyA3::<>m__10A(UnityEngine.GameObject)
extern "C"  void U3CUICamera_onClick_GetDelegate_member42_arg0U3Ec__AnonStoreyA3_U3CU3Em__10A_m1232622887 (U3CUICamera_onClick_GetDelegate_member42_arg0U3Ec__AnonStoreyA3_t936433919 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
