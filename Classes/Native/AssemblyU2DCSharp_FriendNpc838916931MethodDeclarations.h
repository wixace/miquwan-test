﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendNpc
struct FriendNpc_t838916931;
// CombatEntity
struct CombatEntity_t684137495;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// CSCheckPointUnit
struct CSCheckPointUnit_t3129216924;
// GameMgr
struct GameMgr_t1469029542;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// Monster
struct Monster_t2901270458;
// AICtrl
struct AICtrl_t1930422963;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"
#include "AssemblyU2DCSharp_Monster2901270458.h"
#include "AssemblyU2DCSharp_AICtrl1930422963.h"
#include "AssemblyU2DCSharp_AIEnum_EAIEventtype3896289311.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"

// System.Void FriendNpc::.ctor()
extern "C"  void FriendNpc__ctor_m1310274952 (FriendNpc_t838916931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::set_IsTrigger(System.Boolean)
extern "C"  void FriendNpc_set_IsTrigger_m2626449454 (FriendNpc_t838916931 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendNpc::get_IsTrigger()
extern "C"  bool FriendNpc_get_IsTrigger_m3030643103 (FriendNpc_t838916931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::OnTriggerBehaivir(CombatEntity)
extern "C"  void FriendNpc_OnTriggerBehaivir_m2570017466 (FriendNpc_t838916931 * __this, CombatEntity_t684137495 * ___main0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::Init()
extern "C"  void FriendNpc_Init_m1240729548 (FriendNpc_t838916931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::OnTriggerFun(System.Int32)
extern "C"  void FriendNpc_OnTriggerFun_m906268787 (FriendNpc_t838916931 * __this, int32_t ___effectId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::OnUseingSkill(System.Int32)
extern "C"  void FriendNpc_OnUseingSkill_m2543565934 (FriendNpc_t838916931 * __this, int32_t ___skillID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::OnLevelTime()
extern "C"  void FriendNpc_OnLevelTime_m539311032 (FriendNpc_t838916931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::OnAttckTime()
extern "C"  void FriendNpc_OnAttckTime_m3714956093 (FriendNpc_t838916931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::ATKDEBUFF()
extern "C"  void FriendNpc_ATKDEBUFF_m4182311442 (FriendNpc_t838916931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::AIMonsterDead(System.Int32)
extern "C"  void FriendNpc_AIMonsterDead_m2871393709 (FriendNpc_t838916931 * __this, int32_t ___murderID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::AIBeAttacked()
extern "C"  void FriendNpc_AIBeAttacked_m3316706222 (FriendNpc_t838916931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::AGAINATTACK()
extern "C"  void FriendNpc_AGAINATTACK_m215369358 (FriendNpc_t838916931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::InitPos(UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  void FriendNpc_InitPos_m3345312482 (FriendNpc_t838916931 * __this, Vector3_t4282066566  ___pos0, Quaternion_t1553702882  ___rot1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::OnTargetReached()
extern "C"  void FriendNpc_OnTargetReached_m3865447112 (FriendNpc_t838916931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::UpdateMove()
extern "C"  void FriendNpc_UpdateMove_m4230702134 (FriendNpc_t838916931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::RefreshState(CEvent.ZEvent)
extern "C"  void FriendNpc_RefreshState_m555023625 (FriendNpc_t838916931 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::Update()
extern "C"  void FriendNpc_Update_m3690679941 (FriendNpc_t838916931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::RangeMovetoBehacior()
extern "C"  void FriendNpc_RangeMovetoBehacior_m338977396 (FriendNpc_t838916931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::SearchAtkTarget()
extern "C"  void FriendNpc_SearchAtkTarget_m2532069799 (FriendNpc_t838916931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::Dead()
extern "C"  void FriendNpc_Dead_m1089018400 (FriendNpc_t838916931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::Clear()
extern "C"  void FriendNpc_Clear_m3011375539 (FriendNpc_t838916931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSCheckPointUnit FriendNpc::ilo_get_checkPoint1(GameMgr)
extern "C"  CSCheckPointUnit_t3129216924 * FriendNpc_ilo_get_checkPoint1_m2228202968 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FriendNpc::ilo_get_id2(CombatEntity)
extern "C"  int32_t FriendNpc_ilo_get_id2_m1720593048 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::ilo_DispatchEvent3(CEvent.ZEvent)
extern "C"  void FriendNpc_ilo_DispatchEvent3_m2652752601 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::ilo_set_InitPosition4(CombatEntity,UnityEngine.Vector3)
extern "C"  void FriendNpc_ilo_set_InitPosition4_m3739982669 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, Vector3_t4282066566  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSDatacfgManager FriendNpc::ilo_get_CfgDataMgr5()
extern "C"  CSDatacfgManager_t1565254243 * FriendNpc_ilo_get_CfgDataMgr5_m333069903 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::ilo_OnTriggerFun6(Monster,System.Int32)
extern "C"  void FriendNpc_ilo_OnTriggerFun6_m676126210 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, int32_t ___effectId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FriendNpc::ilo_get_time7(GameMgr)
extern "C"  int32_t FriendNpc_ilo_get_time7_m3641863018 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AICtrl FriendNpc::ilo_get_aiCtrl8(Monster)
extern "C"  AICtrl_t1930422963 * FriendNpc_ilo_get_aiCtrl8_m2023966975 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::ilo_OnObservableRequest9(AICtrl,AIEnum.EAIEventtype,System.Int32,System.Int32)
extern "C"  void FriendNpc_ilo_OnObservableRequest9_m621592424 (Il2CppObject * __this /* static, unused */, AICtrl_t1930422963 * ____this0, uint8_t ___eType1, int32_t ___tParam12, int32_t ___tParam23, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::ilo_set_startRotation10(CombatEntity,UnityEngine.Quaternion)
extern "C"  void FriendNpc_ilo_set_startRotation10_m3004689491 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, Quaternion_t1553702882  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::ilo_OnTargetReached11(Monster)
extern "C"  void FriendNpc_ilo_OnTargetReached11_m2327348931 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::ilo_TriggerBattle12(GameMgr,CombatEntity)
extern "C"  void FriendNpc_ilo_TriggerBattle12_m2381916999 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, CombatEntity_t684137495 * ___src1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::ilo_UpdateMove13(Monster)
extern "C"  void FriendNpc_ilo_UpdateMove13_m3041042829 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity FriendNpc::ilo_get_atkTarget14(CombatEntity)
extern "C"  CombatEntity_t684137495 * FriendNpc_ilo_get_atkTarget14_m147201413 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl FriendNpc::ilo_get_bvrCtrl15(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * FriendNpc_ilo_get_bvrCtrl15_m1779277630 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FriendNpc::ilo_get_atkRange16(CombatEntity)
extern "C"  float FriendNpc_ilo_get_atkRange16_m2423027391 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::ilo_TranBehavior17(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void FriendNpc_ilo_TranBehavior17_m2543540642 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, uint8_t ___id1, ObjectU5BU5D_t1108656482* ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::ilo_Update18(Monster)
extern "C"  void FriendNpc_ilo_Update18_m3600200761 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendNpc::ilo_get_isAttack19(CombatEntity)
extern "C"  bool FriendNpc_ilo_get_isAttack19_m3598007079 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 FriendNpc::ilo_get_InitPosition20(CombatEntity)
extern "C"  Vector3_t4282066566  FriendNpc_ilo_get_InitPosition20_m1433835550 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpc::ilo_Dead21(Monster)
extern "C"  void FriendNpc_ilo_Dead21_m909535526 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
