﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_xowtouyallWerewhelnis75
struct M_xowtouyallWerewhelnis75_t1395720251;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_xowtouyallWerewheln1395720251.h"

// System.Void GarbageiOS.M_xowtouyallWerewhelnis75::.ctor()
extern "C"  void M_xowtouyallWerewhelnis75__ctor_m2073438600 (M_xowtouyallWerewhelnis75_t1395720251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xowtouyallWerewhelnis75::M_jelji0(System.String[],System.Int32)
extern "C"  void M_xowtouyallWerewhelnis75_M_jelji0_m3936353701 (M_xowtouyallWerewhelnis75_t1395720251 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xowtouyallWerewhelnis75::M_heargur1(System.String[],System.Int32)
extern "C"  void M_xowtouyallWerewhelnis75_M_heargur1_m3414014176 (M_xowtouyallWerewhelnis75_t1395720251 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xowtouyallWerewhelnis75::M_bemhawKairstalldi2(System.String[],System.Int32)
extern "C"  void M_xowtouyallWerewhelnis75_M_bemhawKairstalldi2_m2887260709 (M_xowtouyallWerewhelnis75_t1395720251 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xowtouyallWerewhelnis75::M_rugisas3(System.String[],System.Int32)
extern "C"  void M_xowtouyallWerewhelnis75_M_rugisas3_m2847274328 (M_xowtouyallWerewhelnis75_t1395720251 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xowtouyallWerewhelnis75::ilo_M_heargur11(GarbageiOS.M_xowtouyallWerewhelnis75,System.String[],System.Int32)
extern "C"  void M_xowtouyallWerewhelnis75_ilo_M_heargur11_m2155438169 (Il2CppObject * __this /* static, unused */, M_xowtouyallWerewhelnis75_t1395720251 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
