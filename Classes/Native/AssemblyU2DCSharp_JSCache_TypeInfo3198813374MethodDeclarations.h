﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSCache/TypeInfo
struct TypeInfo_t3198813374;
// System.Type
struct Type_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"

// System.Void JSCache/TypeInfo::.ctor(System.Type)
extern "C"  void TypeInfo__ctor_m4011815868 (TypeInfo_t3198813374 * __this, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSCache/TypeInfo::get_IsValueType()
extern "C"  bool TypeInfo_get_IsValueType_m2620390879 (TypeInfo_t3198813374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSCache/TypeInfo::get_IsClass()
extern "C"  bool TypeInfo_get_IsClass_m3779673420 (TypeInfo_t3198813374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSCache/TypeInfo::get_IsDelegate()
extern "C"  bool TypeInfo_get_IsDelegate_m826340243 (TypeInfo_t3198813374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSCache/TypeInfo::get_IsCSMonoBehaviour()
extern "C"  bool TypeInfo_get_IsCSMonoBehaviour_m3513718334 (TypeInfo_t3198813374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSCache/TypeInfo::get_JSTypeFullName()
extern "C"  String_t* TypeInfo_get_JSTypeFullName_m3981287632 (TypeInfo_t3198813374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
