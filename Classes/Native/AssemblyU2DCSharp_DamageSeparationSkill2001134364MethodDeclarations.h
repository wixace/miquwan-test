﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DamageSeparationSkill
struct DamageSeparationSkill_t2001134364;

#include "codegen/il2cpp-codegen.h"

// System.Void DamageSeparationSkill::.ctor()
extern "C"  void DamageSeparationSkill__ctor_m1916261839 (DamageSeparationSkill_t2001134364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DamageSeparationSkill::Init(System.Int32,System.Single)
extern "C"  void DamageSeparationSkill_Init_m4289866971 (DamageSeparationSkill_t2001134364 * __this, int32_t ____skillIndex0, float ____shootSkillRstTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DamageSeparationSkill::Recycle()
extern "C"  void DamageSeparationSkill_Recycle_m4195256768 (DamageSeparationSkill_t2001134364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
