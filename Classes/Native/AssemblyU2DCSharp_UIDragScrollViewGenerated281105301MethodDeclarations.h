﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIDragScrollViewGenerated
struct UIDragScrollViewGenerated_t281105301;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UIDragScrollViewGenerated::.ctor()
extern "C"  void UIDragScrollViewGenerated__ctor_m1396648310 (UIDragScrollViewGenerated_t281105301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDragScrollViewGenerated::UIDragScrollView_UIDragScrollView1(JSVCall,System.Int32)
extern "C"  bool UIDragScrollViewGenerated_UIDragScrollView_UIDragScrollView1_m504354172 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragScrollViewGenerated::UIDragScrollView_scrollView(JSVCall)
extern "C"  void UIDragScrollViewGenerated_UIDragScrollView_scrollView_m2886568252 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragScrollViewGenerated::__Register()
extern "C"  void UIDragScrollViewGenerated___Register_m30395665 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
