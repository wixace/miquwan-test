﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.BiaocheBehaviorCtrl
struct BiaocheBehaviorCtrl_t3886392614;
// CombatEntity
struct CombatEntity_t684137495;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"

// System.Void Entity.Behavior.BiaocheBehaviorCtrl::.ctor(CombatEntity)
extern "C"  void BiaocheBehaviorCtrl__ctor_m2054493661 (BiaocheBehaviorCtrl_t3886392614 * __this, CombatEntity_t684137495 * ___entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.BiaocheBehaviorCtrl::Init()
extern "C"  void BiaocheBehaviorCtrl_Init_m2523354176 (BiaocheBehaviorCtrl_t3886392614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.BiaocheBehaviorCtrl::ilo_SetCurBehavior1(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void BiaocheBehaviorCtrl_ilo_SetCurBehavior1_m2408150452 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, uint8_t ___id1, ObjectU5BU5D_t1108656482* ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
