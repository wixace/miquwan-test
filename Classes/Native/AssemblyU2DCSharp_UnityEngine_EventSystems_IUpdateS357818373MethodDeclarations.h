﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_IUpdateSelectedHandlerGenerated
struct UnityEngine_EventSystems_IUpdateSelectedHandlerGenerated_t357818373;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_EventSystems_IUpdateSelectedHandlerGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_IUpdateSelectedHandlerGenerated__ctor_m2153415734 (UnityEngine_EventSystems_IUpdateSelectedHandlerGenerated_t357818373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_IUpdateSelectedHandlerGenerated::IUpdateSelectedHandler_OnUpdateSelected__BaseEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_IUpdateSelectedHandlerGenerated_IUpdateSelectedHandler_OnUpdateSelected__BaseEventData_m1451684256 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_IUpdateSelectedHandlerGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_IUpdateSelectedHandlerGenerated___Register_m2841586257 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
