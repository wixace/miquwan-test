﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// JSDataExchangeMgr/DGetV`1<UIEventListener/VoidDelegate>
struct DGetV_1_t863770523;
// JSDataExchangeMgr/DGetV`1<UIEventListener/BoolDelegate>
struct DGetV_1_t4118235889;
// JSDataExchangeMgr/DGetV`1<UIEventListener/FloatDelegate>
struct DGetV_1_t3127034201;
// JSDataExchangeMgr/DGetV`1<UIEventListener/VectorDelegate>
struct DGetV_1_t3389761290;
// JSDataExchangeMgr/DGetV`1<UIEventListener/ObjectDelegate>
struct DGetV_1_t50244870;
// JSDataExchangeMgr/DGetV`1<UIEventListener/KeyCodeDelegate>
struct DGetV_1_t1918386473;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIEventListenerGenerated
struct  UIEventListenerGenerated_t1580458869  : public Il2CppObject
{
public:

public:
};

struct UIEventListenerGenerated_t1580458869_StaticFields
{
public:
	// JSDataExchangeMgr/DGetV`1<UIEventListener/VoidDelegate> UIEventListenerGenerated::<>f__am$cache0
	DGetV_1_t863770523 * ___U3CU3Ef__amU24cache0_0;
	// JSDataExchangeMgr/DGetV`1<UIEventListener/VoidDelegate> UIEventListenerGenerated::<>f__am$cache1
	DGetV_1_t863770523 * ___U3CU3Ef__amU24cache1_1;
	// JSDataExchangeMgr/DGetV`1<UIEventListener/VoidDelegate> UIEventListenerGenerated::<>f__am$cache2
	DGetV_1_t863770523 * ___U3CU3Ef__amU24cache2_2;
	// JSDataExchangeMgr/DGetV`1<UIEventListener/BoolDelegate> UIEventListenerGenerated::<>f__am$cache3
	DGetV_1_t4118235889 * ___U3CU3Ef__amU24cache3_3;
	// JSDataExchangeMgr/DGetV`1<UIEventListener/BoolDelegate> UIEventListenerGenerated::<>f__am$cache4
	DGetV_1_t4118235889 * ___U3CU3Ef__amU24cache4_4;
	// JSDataExchangeMgr/DGetV`1<UIEventListener/BoolDelegate> UIEventListenerGenerated::<>f__am$cache5
	DGetV_1_t4118235889 * ___U3CU3Ef__amU24cache5_5;
	// JSDataExchangeMgr/DGetV`1<UIEventListener/FloatDelegate> UIEventListenerGenerated::<>f__am$cache6
	DGetV_1_t3127034201 * ___U3CU3Ef__amU24cache6_6;
	// JSDataExchangeMgr/DGetV`1<UIEventListener/VoidDelegate> UIEventListenerGenerated::<>f__am$cache7
	DGetV_1_t863770523 * ___U3CU3Ef__amU24cache7_7;
	// JSDataExchangeMgr/DGetV`1<UIEventListener/VectorDelegate> UIEventListenerGenerated::<>f__am$cache8
	DGetV_1_t3389761290 * ___U3CU3Ef__amU24cache8_8;
	// JSDataExchangeMgr/DGetV`1<UIEventListener/VoidDelegate> UIEventListenerGenerated::<>f__am$cache9
	DGetV_1_t863770523 * ___U3CU3Ef__amU24cache9_9;
	// JSDataExchangeMgr/DGetV`1<UIEventListener/VoidDelegate> UIEventListenerGenerated::<>f__am$cacheA
	DGetV_1_t863770523 * ___U3CU3Ef__amU24cacheA_10;
	// JSDataExchangeMgr/DGetV`1<UIEventListener/VoidDelegate> UIEventListenerGenerated::<>f__am$cacheB
	DGetV_1_t863770523 * ___U3CU3Ef__amU24cacheB_11;
	// JSDataExchangeMgr/DGetV`1<UIEventListener/ObjectDelegate> UIEventListenerGenerated::<>f__am$cacheC
	DGetV_1_t50244870 * ___U3CU3Ef__amU24cacheC_12;
	// JSDataExchangeMgr/DGetV`1<UIEventListener/KeyCodeDelegate> UIEventListenerGenerated::<>f__am$cacheD
	DGetV_1_t1918386473 * ___U3CU3Ef__amU24cacheD_13;
	// JSDataExchangeMgr/DGetV`1<UIEventListener/BoolDelegate> UIEventListenerGenerated::<>f__am$cacheE
	DGetV_1_t4118235889 * ___U3CU3Ef__amU24cacheE_14;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(UIEventListenerGenerated_t1580458869_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline DGetV_1_t863770523 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline DGetV_1_t863770523 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(DGetV_1_t863770523 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(UIEventListenerGenerated_t1580458869_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline DGetV_1_t863770523 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline DGetV_1_t863770523 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(DGetV_1_t863770523 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(UIEventListenerGenerated_t1580458869_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline DGetV_1_t863770523 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline DGetV_1_t863770523 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(DGetV_1_t863770523 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(UIEventListenerGenerated_t1580458869_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline DGetV_1_t4118235889 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline DGetV_1_t4118235889 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(DGetV_1_t4118235889 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(UIEventListenerGenerated_t1580458869_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline DGetV_1_t4118235889 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline DGetV_1_t4118235889 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(DGetV_1_t4118235889 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(UIEventListenerGenerated_t1580458869_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline DGetV_1_t4118235889 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline DGetV_1_t4118235889 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(DGetV_1_t4118235889 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(UIEventListenerGenerated_t1580458869_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline DGetV_1_t3127034201 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline DGetV_1_t3127034201 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(DGetV_1_t3127034201 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(UIEventListenerGenerated_t1580458869_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline DGetV_1_t863770523 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline DGetV_1_t863770523 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(DGetV_1_t863770523 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_8() { return static_cast<int32_t>(offsetof(UIEventListenerGenerated_t1580458869_StaticFields, ___U3CU3Ef__amU24cache8_8)); }
	inline DGetV_1_t3389761290 * get_U3CU3Ef__amU24cache8_8() const { return ___U3CU3Ef__amU24cache8_8; }
	inline DGetV_1_t3389761290 ** get_address_of_U3CU3Ef__amU24cache8_8() { return &___U3CU3Ef__amU24cache8_8; }
	inline void set_U3CU3Ef__amU24cache8_8(DGetV_1_t3389761290 * value)
	{
		___U3CU3Ef__amU24cache8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_9() { return static_cast<int32_t>(offsetof(UIEventListenerGenerated_t1580458869_StaticFields, ___U3CU3Ef__amU24cache9_9)); }
	inline DGetV_1_t863770523 * get_U3CU3Ef__amU24cache9_9() const { return ___U3CU3Ef__amU24cache9_9; }
	inline DGetV_1_t863770523 ** get_address_of_U3CU3Ef__amU24cache9_9() { return &___U3CU3Ef__amU24cache9_9; }
	inline void set_U3CU3Ef__amU24cache9_9(DGetV_1_t863770523 * value)
	{
		___U3CU3Ef__amU24cache9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_10() { return static_cast<int32_t>(offsetof(UIEventListenerGenerated_t1580458869_StaticFields, ___U3CU3Ef__amU24cacheA_10)); }
	inline DGetV_1_t863770523 * get_U3CU3Ef__amU24cacheA_10() const { return ___U3CU3Ef__amU24cacheA_10; }
	inline DGetV_1_t863770523 ** get_address_of_U3CU3Ef__amU24cacheA_10() { return &___U3CU3Ef__amU24cacheA_10; }
	inline void set_U3CU3Ef__amU24cacheA_10(DGetV_1_t863770523 * value)
	{
		___U3CU3Ef__amU24cacheA_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_11() { return static_cast<int32_t>(offsetof(UIEventListenerGenerated_t1580458869_StaticFields, ___U3CU3Ef__amU24cacheB_11)); }
	inline DGetV_1_t863770523 * get_U3CU3Ef__amU24cacheB_11() const { return ___U3CU3Ef__amU24cacheB_11; }
	inline DGetV_1_t863770523 ** get_address_of_U3CU3Ef__amU24cacheB_11() { return &___U3CU3Ef__amU24cacheB_11; }
	inline void set_U3CU3Ef__amU24cacheB_11(DGetV_1_t863770523 * value)
	{
		___U3CU3Ef__amU24cacheB_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_12() { return static_cast<int32_t>(offsetof(UIEventListenerGenerated_t1580458869_StaticFields, ___U3CU3Ef__amU24cacheC_12)); }
	inline DGetV_1_t50244870 * get_U3CU3Ef__amU24cacheC_12() const { return ___U3CU3Ef__amU24cacheC_12; }
	inline DGetV_1_t50244870 ** get_address_of_U3CU3Ef__amU24cacheC_12() { return &___U3CU3Ef__amU24cacheC_12; }
	inline void set_U3CU3Ef__amU24cacheC_12(DGetV_1_t50244870 * value)
	{
		___U3CU3Ef__amU24cacheC_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheC_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_13() { return static_cast<int32_t>(offsetof(UIEventListenerGenerated_t1580458869_StaticFields, ___U3CU3Ef__amU24cacheD_13)); }
	inline DGetV_1_t1918386473 * get_U3CU3Ef__amU24cacheD_13() const { return ___U3CU3Ef__amU24cacheD_13; }
	inline DGetV_1_t1918386473 ** get_address_of_U3CU3Ef__amU24cacheD_13() { return &___U3CU3Ef__amU24cacheD_13; }
	inline void set_U3CU3Ef__amU24cacheD_13(DGetV_1_t1918386473 * value)
	{
		___U3CU3Ef__amU24cacheD_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheD_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheE_14() { return static_cast<int32_t>(offsetof(UIEventListenerGenerated_t1580458869_StaticFields, ___U3CU3Ef__amU24cacheE_14)); }
	inline DGetV_1_t4118235889 * get_U3CU3Ef__amU24cacheE_14() const { return ___U3CU3Ef__amU24cacheE_14; }
	inline DGetV_1_t4118235889 ** get_address_of_U3CU3Ef__amU24cacheE_14() { return &___U3CU3Ef__amU24cacheE_14; }
	inline void set_U3CU3Ef__amU24cacheE_14(DGetV_1_t4118235889 * value)
	{
		___U3CU3Ef__amU24cacheE_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheE_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
