﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_FixedJointGenerated
struct UnityEngine_FixedJointGenerated_t312734289;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_FixedJointGenerated::.ctor()
extern "C"  void UnityEngine_FixedJointGenerated__ctor_m3804631098 (UnityEngine_FixedJointGenerated_t312734289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_FixedJointGenerated::FixedJoint_FixedJoint1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_FixedJointGenerated_FixedJoint_FixedJoint1_m1398281080 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_FixedJointGenerated::__Register()
extern "C"  void UnityEngine_FixedJointGenerated___Register_m4280189133 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
