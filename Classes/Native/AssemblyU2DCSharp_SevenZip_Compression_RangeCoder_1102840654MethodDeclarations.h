﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.Compression.RangeCoder.Decoder
struct Decoder_t1102840654;
// System.IO.Stream
struct Stream_t1561764144;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_1102840654.h"

// System.Void SevenZip.Compression.RangeCoder.Decoder::.ctor()
extern "C"  void Decoder__ctor_m1565968203 (Decoder_t1102840654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.Decoder::Init(System.IO.Stream)
extern "C"  void Decoder_Init_m3878520384 (Decoder_t1102840654 * __this, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.Decoder::ReleaseStream()
extern "C"  void Decoder_ReleaseStream_m3425847568 (Decoder_t1102840654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.Decoder::CloseStream()
extern "C"  void Decoder_CloseStream_m3994499073 (Decoder_t1102840654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.Decoder::Normalize()
extern "C"  void Decoder_Normalize_m3338078262 (Decoder_t1102840654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.Decoder::Normalize2()
extern "C"  void Decoder_Normalize2_m401220638 (Decoder_t1102840654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.RangeCoder.Decoder::GetThreshold(System.UInt32)
extern "C"  uint32_t Decoder_GetThreshold_m3101380935 (Decoder_t1102840654 * __this, uint32_t ___total0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.Decoder::Decode(System.UInt32,System.UInt32,System.UInt32)
extern "C"  void Decoder_Decode_m61905467 (Decoder_t1102840654 * __this, uint32_t ___start0, uint32_t ___size1, uint32_t ___total2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.RangeCoder.Decoder::DecodeDirectBits(System.Int32)
extern "C"  uint32_t Decoder_DecodeDirectBits_m2652244556 (Decoder_t1102840654 * __this, int32_t ___numTotalBits0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.RangeCoder.Decoder::DecodeBit(System.UInt32,System.Int32)
extern "C"  uint32_t Decoder_DecodeBit_m1705071072 (Decoder_t1102840654 * __this, uint32_t ___size00, int32_t ___numTotalBits1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.Decoder::ilo_Normalize1(SevenZip.Compression.RangeCoder.Decoder)
extern "C"  void Decoder_ilo_Normalize1_m4068967490 (Il2CppObject * __this /* static, unused */, Decoder_t1102840654 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
