﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// CSPlayerData/FightingPos[]
struct FightingPosU5BU5D_t3645161435;
// CSOtherPlayer
struct CSOtherPlayer_t3817567105;
// System.Collections.Generic.List`1<CSPlusAtt>
struct List_1_t341533415;
// System.Collections.Generic.Dictionary`2<System.String,CSPlusAtt>
struct Dictionary_2_t4088733529;
// CSDevilSkillData
struct CSDevilSkillData_t3736118547;
// System.Comparison`1<CSHeroUnit>
struct Comparison_1_t2480719633;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSPlayerData
struct  CSPlayerData_t1029357243  : public Il2CppObject
{
public:
	// System.UInt32 CSPlayerData::ID
	uint32_t ___ID_0;
	// System.String CSPlayerData::name
	String_t* ___name_1;
	// System.String CSPlayerData::icon
	String_t* ___icon_2;
	// System.UInt32 CSPlayerData::rank
	uint32_t ___rank_3;
	// System.UInt32 CSPlayerData::lv
	uint32_t ___lv_4;
	// System.UInt32 CSPlayerData::exp
	uint32_t ___exp_5;
	// System.UInt32 CSPlayerData::power
	uint32_t ___power_6;
	// System.UInt32 CSPlayerData::uPower
	uint32_t ___uPower_7;
	// CSPlayerData/FightingPos[] CSPlayerData::Fighting
	FightingPosU5BU5D_t3645161435* ___Fighting_8;
	// CSOtherPlayer CSPlayerData::currentOtherPlayer
	CSOtherPlayer_t3817567105 * ___currentOtherPlayer_9;
	// System.Collections.Generic.List`1<CSPlusAtt> CSPlayerData::leaderPlusAttList
	List_1_t341533415 * ___leaderPlusAttList_10;
	// System.Collections.Generic.Dictionary`2<System.String,CSPlusAtt> CSPlayerData::leaderPlusAttDic
	Dictionary_2_t4088733529 * ___leaderPlusAttDic_11;
	// System.Collections.Generic.List`1<CSPlusAtt> CSPlayerData::reinPlusAttList
	List_1_t341533415 * ___reinPlusAttList_12;
	// System.Collections.Generic.Dictionary`2<System.String,CSPlusAtt> CSPlayerData::reinPlusAttDic
	Dictionary_2_t4088733529 * ___reinPlusAttDic_13;
	// CSDevilSkillData CSPlayerData::devilSkillData
	CSDevilSkillData_t3736118547 * ___devilSkillData_14;

public:
	inline static int32_t get_offset_of_ID_0() { return static_cast<int32_t>(offsetof(CSPlayerData_t1029357243, ___ID_0)); }
	inline uint32_t get_ID_0() const { return ___ID_0; }
	inline uint32_t* get_address_of_ID_0() { return &___ID_0; }
	inline void set_ID_0(uint32_t value)
	{
		___ID_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(CSPlayerData_t1029357243, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_icon_2() { return static_cast<int32_t>(offsetof(CSPlayerData_t1029357243, ___icon_2)); }
	inline String_t* get_icon_2() const { return ___icon_2; }
	inline String_t** get_address_of_icon_2() { return &___icon_2; }
	inline void set_icon_2(String_t* value)
	{
		___icon_2 = value;
		Il2CppCodeGenWriteBarrier(&___icon_2, value);
	}

	inline static int32_t get_offset_of_rank_3() { return static_cast<int32_t>(offsetof(CSPlayerData_t1029357243, ___rank_3)); }
	inline uint32_t get_rank_3() const { return ___rank_3; }
	inline uint32_t* get_address_of_rank_3() { return &___rank_3; }
	inline void set_rank_3(uint32_t value)
	{
		___rank_3 = value;
	}

	inline static int32_t get_offset_of_lv_4() { return static_cast<int32_t>(offsetof(CSPlayerData_t1029357243, ___lv_4)); }
	inline uint32_t get_lv_4() const { return ___lv_4; }
	inline uint32_t* get_address_of_lv_4() { return &___lv_4; }
	inline void set_lv_4(uint32_t value)
	{
		___lv_4 = value;
	}

	inline static int32_t get_offset_of_exp_5() { return static_cast<int32_t>(offsetof(CSPlayerData_t1029357243, ___exp_5)); }
	inline uint32_t get_exp_5() const { return ___exp_5; }
	inline uint32_t* get_address_of_exp_5() { return &___exp_5; }
	inline void set_exp_5(uint32_t value)
	{
		___exp_5 = value;
	}

	inline static int32_t get_offset_of_power_6() { return static_cast<int32_t>(offsetof(CSPlayerData_t1029357243, ___power_6)); }
	inline uint32_t get_power_6() const { return ___power_6; }
	inline uint32_t* get_address_of_power_6() { return &___power_6; }
	inline void set_power_6(uint32_t value)
	{
		___power_6 = value;
	}

	inline static int32_t get_offset_of_uPower_7() { return static_cast<int32_t>(offsetof(CSPlayerData_t1029357243, ___uPower_7)); }
	inline uint32_t get_uPower_7() const { return ___uPower_7; }
	inline uint32_t* get_address_of_uPower_7() { return &___uPower_7; }
	inline void set_uPower_7(uint32_t value)
	{
		___uPower_7 = value;
	}

	inline static int32_t get_offset_of_Fighting_8() { return static_cast<int32_t>(offsetof(CSPlayerData_t1029357243, ___Fighting_8)); }
	inline FightingPosU5BU5D_t3645161435* get_Fighting_8() const { return ___Fighting_8; }
	inline FightingPosU5BU5D_t3645161435** get_address_of_Fighting_8() { return &___Fighting_8; }
	inline void set_Fighting_8(FightingPosU5BU5D_t3645161435* value)
	{
		___Fighting_8 = value;
		Il2CppCodeGenWriteBarrier(&___Fighting_8, value);
	}

	inline static int32_t get_offset_of_currentOtherPlayer_9() { return static_cast<int32_t>(offsetof(CSPlayerData_t1029357243, ___currentOtherPlayer_9)); }
	inline CSOtherPlayer_t3817567105 * get_currentOtherPlayer_9() const { return ___currentOtherPlayer_9; }
	inline CSOtherPlayer_t3817567105 ** get_address_of_currentOtherPlayer_9() { return &___currentOtherPlayer_9; }
	inline void set_currentOtherPlayer_9(CSOtherPlayer_t3817567105 * value)
	{
		___currentOtherPlayer_9 = value;
		Il2CppCodeGenWriteBarrier(&___currentOtherPlayer_9, value);
	}

	inline static int32_t get_offset_of_leaderPlusAttList_10() { return static_cast<int32_t>(offsetof(CSPlayerData_t1029357243, ___leaderPlusAttList_10)); }
	inline List_1_t341533415 * get_leaderPlusAttList_10() const { return ___leaderPlusAttList_10; }
	inline List_1_t341533415 ** get_address_of_leaderPlusAttList_10() { return &___leaderPlusAttList_10; }
	inline void set_leaderPlusAttList_10(List_1_t341533415 * value)
	{
		___leaderPlusAttList_10 = value;
		Il2CppCodeGenWriteBarrier(&___leaderPlusAttList_10, value);
	}

	inline static int32_t get_offset_of_leaderPlusAttDic_11() { return static_cast<int32_t>(offsetof(CSPlayerData_t1029357243, ___leaderPlusAttDic_11)); }
	inline Dictionary_2_t4088733529 * get_leaderPlusAttDic_11() const { return ___leaderPlusAttDic_11; }
	inline Dictionary_2_t4088733529 ** get_address_of_leaderPlusAttDic_11() { return &___leaderPlusAttDic_11; }
	inline void set_leaderPlusAttDic_11(Dictionary_2_t4088733529 * value)
	{
		___leaderPlusAttDic_11 = value;
		Il2CppCodeGenWriteBarrier(&___leaderPlusAttDic_11, value);
	}

	inline static int32_t get_offset_of_reinPlusAttList_12() { return static_cast<int32_t>(offsetof(CSPlayerData_t1029357243, ___reinPlusAttList_12)); }
	inline List_1_t341533415 * get_reinPlusAttList_12() const { return ___reinPlusAttList_12; }
	inline List_1_t341533415 ** get_address_of_reinPlusAttList_12() { return &___reinPlusAttList_12; }
	inline void set_reinPlusAttList_12(List_1_t341533415 * value)
	{
		___reinPlusAttList_12 = value;
		Il2CppCodeGenWriteBarrier(&___reinPlusAttList_12, value);
	}

	inline static int32_t get_offset_of_reinPlusAttDic_13() { return static_cast<int32_t>(offsetof(CSPlayerData_t1029357243, ___reinPlusAttDic_13)); }
	inline Dictionary_2_t4088733529 * get_reinPlusAttDic_13() const { return ___reinPlusAttDic_13; }
	inline Dictionary_2_t4088733529 ** get_address_of_reinPlusAttDic_13() { return &___reinPlusAttDic_13; }
	inline void set_reinPlusAttDic_13(Dictionary_2_t4088733529 * value)
	{
		___reinPlusAttDic_13 = value;
		Il2CppCodeGenWriteBarrier(&___reinPlusAttDic_13, value);
	}

	inline static int32_t get_offset_of_devilSkillData_14() { return static_cast<int32_t>(offsetof(CSPlayerData_t1029357243, ___devilSkillData_14)); }
	inline CSDevilSkillData_t3736118547 * get_devilSkillData_14() const { return ___devilSkillData_14; }
	inline CSDevilSkillData_t3736118547 ** get_address_of_devilSkillData_14() { return &___devilSkillData_14; }
	inline void set_devilSkillData_14(CSDevilSkillData_t3736118547 * value)
	{
		___devilSkillData_14 = value;
		Il2CppCodeGenWriteBarrier(&___devilSkillData_14, value);
	}
};

struct CSPlayerData_t1029357243_StaticFields
{
public:
	// System.Comparison`1<CSHeroUnit> CSPlayerData::<>f__am$cacheF
	Comparison_1_t2480719633 * ___U3CU3Ef__amU24cacheF_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheF_15() { return static_cast<int32_t>(offsetof(CSPlayerData_t1029357243_StaticFields, ___U3CU3Ef__amU24cacheF_15)); }
	inline Comparison_1_t2480719633 * get_U3CU3Ef__amU24cacheF_15() const { return ___U3CU3Ef__amU24cacheF_15; }
	inline Comparison_1_t2480719633 ** get_address_of_U3CU3Ef__amU24cacheF_15() { return &___U3CU3Ef__amU24cacheF_15; }
	inline void set_U3CU3Ef__amU24cacheF_15(Comparison_1_t2480719633 * value)
	{
		___U3CU3Ef__amU24cacheF_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheF_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
