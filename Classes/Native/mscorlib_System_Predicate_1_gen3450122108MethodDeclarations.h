﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen3781873254MethodDeclarations.h"

// System.Void System.Predicate`1<UnityEngine.MeshFilter>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m1449620031(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t3450122108 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m231416842_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<UnityEngine.MeshFilter>::Invoke(T)
#define Predicate_1_Invoke_m3107913571(__this, ___obj0, method) ((  bool (*) (Predicate_1_t3450122108 *, MeshFilter_t3839065225 *, const MethodInfo*))Predicate_1_Invoke_m1328901537_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.MeshFilter>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m3819163378(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t3450122108 *, MeshFilter_t3839065225 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m2038073176_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<UnityEngine.MeshFilter>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m2465321041(__this, ___result0, method) ((  bool (*) (Predicate_1_t3450122108 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3970497007_gshared)(__this, ___result0, method)
