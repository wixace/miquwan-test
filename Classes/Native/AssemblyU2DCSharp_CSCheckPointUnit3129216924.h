﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// checkpointCfg
struct checkpointCfg_t2816107964;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_CHECK_POINT_STATE2023483403.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSCheckPointUnit
struct  CSCheckPointUnit_t3129216924  : public Il2CppObject
{
public:
	// checkpointCfg CSCheckPointUnit::cfg
	checkpointCfg_t2816107964 * ___cfg_0;
	// System.UInt32 CSCheckPointUnit::id
	uint32_t ___id_1;
	// System.UInt32 CSCheckPointUnit::stars
	uint32_t ___stars_2;
	// System.UInt32 CSCheckPointUnit::surpEnterCount
	uint32_t ___surpEnterCount_3;
	// CHECK_POINT_STATE CSCheckPointUnit::state
	int32_t ___state_4;

public:
	inline static int32_t get_offset_of_cfg_0() { return static_cast<int32_t>(offsetof(CSCheckPointUnit_t3129216924, ___cfg_0)); }
	inline checkpointCfg_t2816107964 * get_cfg_0() const { return ___cfg_0; }
	inline checkpointCfg_t2816107964 ** get_address_of_cfg_0() { return &___cfg_0; }
	inline void set_cfg_0(checkpointCfg_t2816107964 * value)
	{
		___cfg_0 = value;
		Il2CppCodeGenWriteBarrier(&___cfg_0, value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(CSCheckPointUnit_t3129216924, ___id_1)); }
	inline uint32_t get_id_1() const { return ___id_1; }
	inline uint32_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(uint32_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_stars_2() { return static_cast<int32_t>(offsetof(CSCheckPointUnit_t3129216924, ___stars_2)); }
	inline uint32_t get_stars_2() const { return ___stars_2; }
	inline uint32_t* get_address_of_stars_2() { return &___stars_2; }
	inline void set_stars_2(uint32_t value)
	{
		___stars_2 = value;
	}

	inline static int32_t get_offset_of_surpEnterCount_3() { return static_cast<int32_t>(offsetof(CSCheckPointUnit_t3129216924, ___surpEnterCount_3)); }
	inline uint32_t get_surpEnterCount_3() const { return ___surpEnterCount_3; }
	inline uint32_t* get_address_of_surpEnterCount_3() { return &___surpEnterCount_3; }
	inline void set_surpEnterCount_3(uint32_t value)
	{
		___surpEnterCount_3 = value;
	}

	inline static int32_t get_offset_of_state_4() { return static_cast<int32_t>(offsetof(CSCheckPointUnit_t3129216924, ___state_4)); }
	inline int32_t get_state_4() const { return ___state_4; }
	inline int32_t* get_address_of_state_4() { return &___state_4; }
	inline void set_state_4(int32_t value)
	{
		___state_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
