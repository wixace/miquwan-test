﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.GraphNode[]
struct GraphNodeU5BU5D_t927449255;
// System.IO.BinaryReader
struct BinaryReader_t3990958868;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Serialization.AstarSerializer/<DeserializeExtraInfo>c__AnonStorey110
struct  U3CDeserializeExtraInfoU3Ec__AnonStorey110_t3805251454  : public Il2CppObject
{
public:
	// Pathfinding.GraphNode[] Pathfinding.Serialization.AstarSerializer/<DeserializeExtraInfo>c__AnonStorey110::int2Node
	GraphNodeU5BU5D_t927449255* ___int2Node_0;
	// System.IO.BinaryReader Pathfinding.Serialization.AstarSerializer/<DeserializeExtraInfo>c__AnonStorey110::reader
	BinaryReader_t3990958868 * ___reader_1;

public:
	inline static int32_t get_offset_of_int2Node_0() { return static_cast<int32_t>(offsetof(U3CDeserializeExtraInfoU3Ec__AnonStorey110_t3805251454, ___int2Node_0)); }
	inline GraphNodeU5BU5D_t927449255* get_int2Node_0() const { return ___int2Node_0; }
	inline GraphNodeU5BU5D_t927449255** get_address_of_int2Node_0() { return &___int2Node_0; }
	inline void set_int2Node_0(GraphNodeU5BU5D_t927449255* value)
	{
		___int2Node_0 = value;
		Il2CppCodeGenWriteBarrier(&___int2Node_0, value);
	}

	inline static int32_t get_offset_of_reader_1() { return static_cast<int32_t>(offsetof(U3CDeserializeExtraInfoU3Ec__AnonStorey110_t3805251454, ___reader_1)); }
	inline BinaryReader_t3990958868 * get_reader_1() const { return ___reader_1; }
	inline BinaryReader_t3990958868 ** get_address_of_reader_1() { return &___reader_1; }
	inline void set_reader_1(BinaryReader_t3990958868 * value)
	{
		___reader_1 = value;
		Il2CppCodeGenWriteBarrier(&___reader_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
