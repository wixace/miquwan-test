﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._b6fa4ce8834e6ca93691732bac8ab05e
struct _b6fa4ce8834e6ca93691732bac8ab05e_t97978827;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._b6fa4ce8834e6ca93691732bac8ab05e::.ctor()
extern "C"  void _b6fa4ce8834e6ca93691732bac8ab05e__ctor_m2734878914 (_b6fa4ce8834e6ca93691732bac8ab05e_t97978827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b6fa4ce8834e6ca93691732bac8ab05e::_b6fa4ce8834e6ca93691732bac8ab05em2(System.Int32)
extern "C"  int32_t _b6fa4ce8834e6ca93691732bac8ab05e__b6fa4ce8834e6ca93691732bac8ab05em2_m2295326137 (_b6fa4ce8834e6ca93691732bac8ab05e_t97978827 * __this, int32_t ____b6fa4ce8834e6ca93691732bac8ab05ea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b6fa4ce8834e6ca93691732bac8ab05e::_b6fa4ce8834e6ca93691732bac8ab05em(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _b6fa4ce8834e6ca93691732bac8ab05e__b6fa4ce8834e6ca93691732bac8ab05em_m2943722781 (_b6fa4ce8834e6ca93691732bac8ab05e_t97978827 * __this, int32_t ____b6fa4ce8834e6ca93691732bac8ab05ea0, int32_t ____b6fa4ce8834e6ca93691732bac8ab05e81, int32_t ____b6fa4ce8834e6ca93691732bac8ab05ec2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
