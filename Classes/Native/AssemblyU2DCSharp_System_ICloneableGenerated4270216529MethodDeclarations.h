﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_ICloneableGenerated
struct System_ICloneableGenerated_t4270216529;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void System_ICloneableGenerated::.ctor()
extern "C"  void System_ICloneableGenerated__ctor_m2375974506 (System_ICloneableGenerated_t4270216529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_ICloneableGenerated::ICloneable_Clone(JSVCall,System.Int32)
extern "C"  bool System_ICloneableGenerated_ICloneable_Clone_m3339643370 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_ICloneableGenerated::__Register()
extern "C"  void System_ICloneableGenerated___Register_m1648586909 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
