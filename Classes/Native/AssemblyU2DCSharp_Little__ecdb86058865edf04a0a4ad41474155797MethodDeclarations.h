﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._ecdb86058865edf04a0a4ad4161a0ce7
struct _ecdb86058865edf04a0a4ad4161a0ce7_t1474155797;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._ecdb86058865edf04a0a4ad4161a0ce7::.ctor()
extern "C"  void _ecdb86058865edf04a0a4ad4161a0ce7__ctor_m296433848 (_ecdb86058865edf04a0a4ad4161a0ce7_t1474155797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._ecdb86058865edf04a0a4ad4161a0ce7::_ecdb86058865edf04a0a4ad4161a0ce7m2(System.Int32)
extern "C"  int32_t _ecdb86058865edf04a0a4ad4161a0ce7__ecdb86058865edf04a0a4ad4161a0ce7m2_m574280057 (_ecdb86058865edf04a0a4ad4161a0ce7_t1474155797 * __this, int32_t ____ecdb86058865edf04a0a4ad4161a0ce7a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._ecdb86058865edf04a0a4ad4161a0ce7::_ecdb86058865edf04a0a4ad4161a0ce7m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _ecdb86058865edf04a0a4ad4161a0ce7__ecdb86058865edf04a0a4ad4161a0ce7m_m2207740253 (_ecdb86058865edf04a0a4ad4161a0ce7_t1474155797 * __this, int32_t ____ecdb86058865edf04a0a4ad4161a0ce7a0, int32_t ____ecdb86058865edf04a0a4ad4161a0ce7441, int32_t ____ecdb86058865edf04a0a4ad4161a0ce7c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
