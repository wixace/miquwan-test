﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_peregoo176
struct  M_peregoo176_t2304884001  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_peregoo176::_geatousti
	float ____geatousti_0;
	// System.Int32 GarbageiOS.M_peregoo176::_cifechow
	int32_t ____cifechow_1;
	// System.Boolean GarbageiOS.M_peregoo176::_jela
	bool ____jela_2;
	// System.Boolean GarbageiOS.M_peregoo176::_becou
	bool ____becou_3;
	// System.String GarbageiOS.M_peregoo176::_nairsee
	String_t* ____nairsee_4;
	// System.Int32 GarbageiOS.M_peregoo176::_radijallYirsair
	int32_t ____radijallYirsair_5;

public:
	inline static int32_t get_offset_of__geatousti_0() { return static_cast<int32_t>(offsetof(M_peregoo176_t2304884001, ____geatousti_0)); }
	inline float get__geatousti_0() const { return ____geatousti_0; }
	inline float* get_address_of__geatousti_0() { return &____geatousti_0; }
	inline void set__geatousti_0(float value)
	{
		____geatousti_0 = value;
	}

	inline static int32_t get_offset_of__cifechow_1() { return static_cast<int32_t>(offsetof(M_peregoo176_t2304884001, ____cifechow_1)); }
	inline int32_t get__cifechow_1() const { return ____cifechow_1; }
	inline int32_t* get_address_of__cifechow_1() { return &____cifechow_1; }
	inline void set__cifechow_1(int32_t value)
	{
		____cifechow_1 = value;
	}

	inline static int32_t get_offset_of__jela_2() { return static_cast<int32_t>(offsetof(M_peregoo176_t2304884001, ____jela_2)); }
	inline bool get__jela_2() const { return ____jela_2; }
	inline bool* get_address_of__jela_2() { return &____jela_2; }
	inline void set__jela_2(bool value)
	{
		____jela_2 = value;
	}

	inline static int32_t get_offset_of__becou_3() { return static_cast<int32_t>(offsetof(M_peregoo176_t2304884001, ____becou_3)); }
	inline bool get__becou_3() const { return ____becou_3; }
	inline bool* get_address_of__becou_3() { return &____becou_3; }
	inline void set__becou_3(bool value)
	{
		____becou_3 = value;
	}

	inline static int32_t get_offset_of__nairsee_4() { return static_cast<int32_t>(offsetof(M_peregoo176_t2304884001, ____nairsee_4)); }
	inline String_t* get__nairsee_4() const { return ____nairsee_4; }
	inline String_t** get_address_of__nairsee_4() { return &____nairsee_4; }
	inline void set__nairsee_4(String_t* value)
	{
		____nairsee_4 = value;
		Il2CppCodeGenWriteBarrier(&____nairsee_4, value);
	}

	inline static int32_t get_offset_of__radijallYirsair_5() { return static_cast<int32_t>(offsetof(M_peregoo176_t2304884001, ____radijallYirsair_5)); }
	inline int32_t get__radijallYirsair_5() const { return ____radijallYirsair_5; }
	inline int32_t* get_address_of__radijallYirsair_5() { return &____radijallYirsair_5; }
	inline void set__radijallYirsair_5(int32_t value)
	{
		____radijallYirsair_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
