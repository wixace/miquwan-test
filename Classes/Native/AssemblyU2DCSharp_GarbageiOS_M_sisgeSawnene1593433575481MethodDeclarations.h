﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_sisgeSawnene159
struct M_sisgeSawnene159_t3433575481;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_sisgeSawnene1593433575481.h"

// System.Void GarbageiOS.M_sisgeSawnene159::.ctor()
extern "C"  void M_sisgeSawnene159__ctor_m3906349898 (M_sisgeSawnene159_t3433575481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sisgeSawnene159::M_worrel0(System.String[],System.Int32)
extern "C"  void M_sisgeSawnene159_M_worrel0_m2741155934 (M_sisgeSawnene159_t3433575481 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sisgeSawnene159::M_teresere1(System.String[],System.Int32)
extern "C"  void M_sisgeSawnene159_M_teresere1_m3823215989 (M_sisgeSawnene159_t3433575481 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sisgeSawnene159::M_whakoCaiqi2(System.String[],System.Int32)
extern "C"  void M_sisgeSawnene159_M_whakoCaiqi2_m1358550960 (M_sisgeSawnene159_t3433575481 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sisgeSawnene159::M_beesati3(System.String[],System.Int32)
extern "C"  void M_sisgeSawnene159_M_beesati3_m3682822609 (M_sisgeSawnene159_t3433575481 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sisgeSawnene159::ilo_M_whakoCaiqi21(GarbageiOS.M_sisgeSawnene159,System.String[],System.Int32)
extern "C"  void M_sisgeSawnene159_ilo_M_whakoCaiqi21_m1851318753 (Il2CppObject * __this /* static, unused */, M_sisgeSawnene159_t3433575481 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
