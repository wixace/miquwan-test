﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIWrapContentGenerated/<UIWrapContent_onInitializeItem_GetDelegate_member4_arg0>c__AnonStoreyD9
struct U3CUIWrapContent_onInitializeItem_GetDelegate_member4_arg0U3Ec__AnonStoreyD9_t878174009;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UIWrapContentGenerated/<UIWrapContent_onInitializeItem_GetDelegate_member4_arg0>c__AnonStoreyD9::.ctor()
extern "C"  void U3CUIWrapContent_onInitializeItem_GetDelegate_member4_arg0U3Ec__AnonStoreyD9__ctor_m1012852818 (U3CUIWrapContent_onInitializeItem_GetDelegate_member4_arg0U3Ec__AnonStoreyD9_t878174009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWrapContentGenerated/<UIWrapContent_onInitializeItem_GetDelegate_member4_arg0>c__AnonStoreyD9::<>m__177(UnityEngine.GameObject,System.Int32,System.Int32)
extern "C"  void U3CUIWrapContent_onInitializeItem_GetDelegate_member4_arg0U3Ec__AnonStoreyD9_U3CU3Em__177_m3309474832 (U3CUIWrapContent_onInitializeItem_GetDelegate_member4_arg0U3Ec__AnonStoreyD9_t878174009 * __this, GameObject_t3674682005 * ___go0, int32_t ___wrapIndex1, int32_t ___realIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
