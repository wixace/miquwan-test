﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginQianYou
struct PluginQianYou_t106475207;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// VersionMgr
struct VersionMgr_t1322950208;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// System.Object
struct Il2CppObject;
// Mihua.SDK.CreatRoleInfo
struct CreatRoleInfo_t2858846543;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Mihua.SDK.EnterGameInfo
struct EnterGameInfo_t1897532954;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_PluginQianYou106475207.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginQianYou::.ctor()
extern "C"  void PluginQianYou__ctor_m1398563460 (PluginQianYou_t106475207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::Init()
extern "C"  void PluginQianYou_Init_m827935568 (PluginQianYou_t106475207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginQianYou::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginQianYou_ReqSDKHttpLogin_m587491189 (PluginQianYou_t106475207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginQianYou::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginQianYou_IsLoginSuccess_m3129228647 (PluginQianYou_t106475207 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::OpenUserLogin()
extern "C"  void PluginQianYou_OpenUserLogin_m383566038 (PluginQianYou_t106475207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::UserPay(CEvent.ZEvent)
extern "C"  void PluginQianYou_UserPay_m1189762524 (PluginQianYou_t106475207 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::SignCallBack(System.Boolean,System.String)
extern "C"  void PluginQianYou_SignCallBack_m243533429 (PluginQianYou_t106475207 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginQianYou::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginQianYou_BuildOrderParam2WWWForm_m1985394911 (PluginQianYou_t106475207 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::CreateRole(CEvent.ZEvent)
extern "C"  void PluginQianYou_CreateRole_m4280362921 (PluginQianYou_t106475207 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::EnterGame(CEvent.ZEvent)
extern "C"  void PluginQianYou_EnterGame_m3117031535 (PluginQianYou_t106475207 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginQianYou_RoleUpgrade_m2210562899 (PluginQianYou_t106475207 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::OnLoginSuccess(System.String)
extern "C"  void PluginQianYou_OnLoginSuccess_m288874121 (PluginQianYou_t106475207 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::OnLoginSuccessNew(System.String)
extern "C"  void PluginQianYou_OnLoginSuccessNew_m3888119353 (PluginQianYou_t106475207 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::OnLogoutSuccess(System.String)
extern "C"  void PluginQianYou_OnLogoutSuccess_m368705830 (PluginQianYou_t106475207 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::OnLogout(System.String)
extern "C"  void PluginQianYou_OnLogout_m4261788889 (PluginQianYou_t106475207 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::initSdk()
extern "C"  void PluginQianYou_initSdk_m1363897868 (PluginQianYou_t106475207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::login()
extern "C"  void PluginQianYou_login_m920578283 (PluginQianYou_t106475207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::logout()
extern "C"  void PluginQianYou_logout_m2773945706 (PluginQianYou_t106475207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::userInfo(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginQianYou_userInfo_m244118545 (PluginQianYou_t106475207 * __this, String_t* ___serverid0, String_t* ___servername1, String_t* ___roleid2, String_t* ___rolename3, String_t* ___gameState4, String_t* ___rolelv5, String_t* ___username6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginQianYou_pay_m2344032060 (PluginQianYou_t106475207 * __this, String_t* ___UserId0, String_t* ___RoleId1, String_t* ___RoleName2, String_t* ___Amount3, String_t* ___Product4, String_t* ___ServerID5, String_t* ___withInfo6, String_t* ___productName7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::<OnLogoutSuccess>m__44B()
extern "C"  void PluginQianYou_U3COnLogoutSuccessU3Em__44B_m3116491319 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::<OnLogout>m__44C()
extern "C"  void PluginQianYou_U3COnLogoutU3Em__44C_m1666195183 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::ilo_initSdk1(PluginQianYou)
extern "C"  void PluginQianYou_ilo_initSdk1_m2114880977 (Il2CppObject * __this /* static, unused */, PluginQianYou_t106475207 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginQianYou::ilo_get_PluginsSdkMgr2()
extern "C"  PluginsSdkMgr_t3884624670 * PluginQianYou_ilo_get_PluginsSdkMgr2_m3473130907 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginQianYou::ilo_get_Instance3()
extern "C"  VersionMgr_t1322950208 * PluginQianYou_ilo_get_Instance3_m3444265185 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginQianYou::ilo_get_isSdkLogin4(VersionMgr)
extern "C"  bool PluginQianYou_ilo_get_isSdkLogin4_m1467837557 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::ilo_login5(PluginQianYou)
extern "C"  void PluginQianYou_ilo_login5_m4263157036 (Il2CppObject * __this /* static, unused */, PluginQianYou_t106475207 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProductsCfgMgr PluginQianYou::ilo_get_ProductsCfgMgr6()
extern "C"  ProductsCfgMgr_t2493714872 * PluginQianYou_ilo_get_ProductsCfgMgr6_m1430597577 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginQianYou::ilo_GetProductID7(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginQianYou_ilo_GetProductID7_m3564600601 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::ilo_pay8(PluginQianYou,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginQianYou_ilo_pay8_m3807961960 (Il2CppObject * __this /* static, unused */, PluginQianYou_t106475207 * ____this0, String_t* ___UserId1, String_t* ___RoleId2, String_t* ___RoleName3, String_t* ___Amount4, String_t* ___Product5, String_t* ___ServerID6, String_t* ___withInfo7, String_t* ___productName8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::ilo_Log9(System.Object,System.Boolean)
extern "C"  void PluginQianYou_ilo_Log9_m2810508881 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::ilo_LogError10(System.Object,System.Boolean)
extern "C"  void PluginQianYou_ilo_LogError10_m1245117727 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.CreatRoleInfo PluginQianYou::ilo_Parse11(System.Object[])
extern "C"  CreatRoleInfo_t2858846543 * PluginQianYou_ilo_Parse11_m1866025184 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.EnterGameInfo PluginQianYou::ilo_Parse12(System.Object[])
extern "C"  EnterGameInfo_t1897532954 * PluginQianYou_ilo_Parse12_m2076371786 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::ilo_userInfo13(PluginQianYou,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginQianYou_ilo_userInfo13_m2059337377 (Il2CppObject * __this /* static, unused */, PluginQianYou_t106475207 * ____this0, String_t* ___serverid1, String_t* ___servername2, String_t* ___roleid3, String_t* ___rolename4, String_t* ___gameState5, String_t* ___rolelv6, String_t* ___username7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::ilo_ReqSDKHttpLogin14(PluginsSdkMgr)
extern "C"  void PluginQianYou_ilo_ReqSDKHttpLogin14_m611894237 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQianYou::ilo_Logout15(System.Action)
extern "C"  void PluginQianYou_ilo_Logout15_m1077442492 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
