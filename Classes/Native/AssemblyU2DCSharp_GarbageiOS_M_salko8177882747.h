﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_salko81
struct  M_salko81_t77882747  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_salko81::_xoowheBedre
	float ____xoowheBedre_0;
	// System.Int32 GarbageiOS.M_salko81::_lalkefaw
	int32_t ____lalkefaw_1;
	// System.String GarbageiOS.M_salko81::_tejear
	String_t* ____tejear_2;
	// System.Boolean GarbageiOS.M_salko81::_woucouwaMearje
	bool ____woucouwaMearje_3;
	// System.Boolean GarbageiOS.M_salko81::_celqairNearmi
	bool ____celqairNearmi_4;

public:
	inline static int32_t get_offset_of__xoowheBedre_0() { return static_cast<int32_t>(offsetof(M_salko81_t77882747, ____xoowheBedre_0)); }
	inline float get__xoowheBedre_0() const { return ____xoowheBedre_0; }
	inline float* get_address_of__xoowheBedre_0() { return &____xoowheBedre_0; }
	inline void set__xoowheBedre_0(float value)
	{
		____xoowheBedre_0 = value;
	}

	inline static int32_t get_offset_of__lalkefaw_1() { return static_cast<int32_t>(offsetof(M_salko81_t77882747, ____lalkefaw_1)); }
	inline int32_t get__lalkefaw_1() const { return ____lalkefaw_1; }
	inline int32_t* get_address_of__lalkefaw_1() { return &____lalkefaw_1; }
	inline void set__lalkefaw_1(int32_t value)
	{
		____lalkefaw_1 = value;
	}

	inline static int32_t get_offset_of__tejear_2() { return static_cast<int32_t>(offsetof(M_salko81_t77882747, ____tejear_2)); }
	inline String_t* get__tejear_2() const { return ____tejear_2; }
	inline String_t** get_address_of__tejear_2() { return &____tejear_2; }
	inline void set__tejear_2(String_t* value)
	{
		____tejear_2 = value;
		Il2CppCodeGenWriteBarrier(&____tejear_2, value);
	}

	inline static int32_t get_offset_of__woucouwaMearje_3() { return static_cast<int32_t>(offsetof(M_salko81_t77882747, ____woucouwaMearje_3)); }
	inline bool get__woucouwaMearje_3() const { return ____woucouwaMearje_3; }
	inline bool* get_address_of__woucouwaMearje_3() { return &____woucouwaMearje_3; }
	inline void set__woucouwaMearje_3(bool value)
	{
		____woucouwaMearje_3 = value;
	}

	inline static int32_t get_offset_of__celqairNearmi_4() { return static_cast<int32_t>(offsetof(M_salko81_t77882747, ____celqairNearmi_4)); }
	inline bool get__celqairNearmi_4() const { return ____celqairNearmi_4; }
	inline bool* get_address_of__celqairNearmi_4() { return &____celqairNearmi_4; }
	inline void set__celqairNearmi_4(bool value)
	{
		____celqairNearmi_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
