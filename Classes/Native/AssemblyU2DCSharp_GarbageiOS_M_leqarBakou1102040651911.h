﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_leqarBakou110
struct  M_leqarBakou110_t2040651911  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_leqarBakou110::_peyerdow
	int32_t ____peyerdow_0;
	// System.String GarbageiOS.M_leqarBakou110::_fairkeZereceehem
	String_t* ____fairkeZereceehem_1;
	// System.UInt32 GarbageiOS.M_leqarBakou110::_sallru
	uint32_t ____sallru_2;
	// System.Boolean GarbageiOS.M_leqarBakou110::_troloukouLaror
	bool ____troloukouLaror_3;
	// System.Int32 GarbageiOS.M_leqarBakou110::_ritisDrasi
	int32_t ____ritisDrasi_4;
	// System.String GarbageiOS.M_leqarBakou110::_dacemmirJusiza
	String_t* ____dacemmirJusiza_5;
	// System.Int32 GarbageiOS.M_leqarBakou110::_kalgirDele
	int32_t ____kalgirDele_6;
	// System.Int32 GarbageiOS.M_leqarBakou110::_silu
	int32_t ____silu_7;
	// System.Boolean GarbageiOS.M_leqarBakou110::_stearsougooSoulee
	bool ____stearsougooSoulee_8;
	// System.String GarbageiOS.M_leqarBakou110::_hejairchaGeryir
	String_t* ____hejairchaGeryir_9;
	// System.Int32 GarbageiOS.M_leqarBakou110::_rairsallyasRaiwe
	int32_t ____rairsallyasRaiwe_10;
	// System.Boolean GarbageiOS.M_leqarBakou110::_jallseakeChitoo
	bool ____jallseakeChitoo_11;
	// System.UInt32 GarbageiOS.M_leqarBakou110::_paybeNavi
	uint32_t ____paybeNavi_12;
	// System.Boolean GarbageiOS.M_leqarBakou110::_cairheaSoutal
	bool ____cairheaSoutal_13;
	// System.String GarbageiOS.M_leqarBakou110::_curme
	String_t* ____curme_14;
	// System.Int32 GarbageiOS.M_leqarBakou110::_tapuLerewou
	int32_t ____tapuLerewou_15;
	// System.Boolean GarbageiOS.M_leqarBakou110::_kuterse
	bool ____kuterse_16;
	// System.Int32 GarbageiOS.M_leqarBakou110::_zurfisxeFisnu
	int32_t ____zurfisxeFisnu_17;
	// System.UInt32 GarbageiOS.M_leqarBakou110::_bisaw
	uint32_t ____bisaw_18;
	// System.Single GarbageiOS.M_leqarBakou110::_nirogi
	float ____nirogi_19;
	// System.Boolean GarbageiOS.M_leqarBakou110::_soucearnowHelje
	bool ____soucearnowHelje_20;
	// System.String GarbageiOS.M_leqarBakou110::_moudiNairjaskar
	String_t* ____moudiNairjaskar_21;
	// System.UInt32 GarbageiOS.M_leqarBakou110::_xesee
	uint32_t ____xesee_22;
	// System.UInt32 GarbageiOS.M_leqarBakou110::_jejalKireetroo
	uint32_t ____jejalKireetroo_23;
	// System.UInt32 GarbageiOS.M_leqarBakou110::_chimaihalMuler
	uint32_t ____chimaihalMuler_24;
	// System.String GarbageiOS.M_leqarBakou110::_surteeDownu
	String_t* ____surteeDownu_25;
	// System.String GarbageiOS.M_leqarBakou110::_piseawherWhaylaca
	String_t* ____piseawherWhaylaca_26;
	// System.Boolean GarbageiOS.M_leqarBakou110::_yaru
	bool ____yaru_27;

public:
	inline static int32_t get_offset_of__peyerdow_0() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____peyerdow_0)); }
	inline int32_t get__peyerdow_0() const { return ____peyerdow_0; }
	inline int32_t* get_address_of__peyerdow_0() { return &____peyerdow_0; }
	inline void set__peyerdow_0(int32_t value)
	{
		____peyerdow_0 = value;
	}

	inline static int32_t get_offset_of__fairkeZereceehem_1() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____fairkeZereceehem_1)); }
	inline String_t* get__fairkeZereceehem_1() const { return ____fairkeZereceehem_1; }
	inline String_t** get_address_of__fairkeZereceehem_1() { return &____fairkeZereceehem_1; }
	inline void set__fairkeZereceehem_1(String_t* value)
	{
		____fairkeZereceehem_1 = value;
		Il2CppCodeGenWriteBarrier(&____fairkeZereceehem_1, value);
	}

	inline static int32_t get_offset_of__sallru_2() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____sallru_2)); }
	inline uint32_t get__sallru_2() const { return ____sallru_2; }
	inline uint32_t* get_address_of__sallru_2() { return &____sallru_2; }
	inline void set__sallru_2(uint32_t value)
	{
		____sallru_2 = value;
	}

	inline static int32_t get_offset_of__troloukouLaror_3() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____troloukouLaror_3)); }
	inline bool get__troloukouLaror_3() const { return ____troloukouLaror_3; }
	inline bool* get_address_of__troloukouLaror_3() { return &____troloukouLaror_3; }
	inline void set__troloukouLaror_3(bool value)
	{
		____troloukouLaror_3 = value;
	}

	inline static int32_t get_offset_of__ritisDrasi_4() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____ritisDrasi_4)); }
	inline int32_t get__ritisDrasi_4() const { return ____ritisDrasi_4; }
	inline int32_t* get_address_of__ritisDrasi_4() { return &____ritisDrasi_4; }
	inline void set__ritisDrasi_4(int32_t value)
	{
		____ritisDrasi_4 = value;
	}

	inline static int32_t get_offset_of__dacemmirJusiza_5() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____dacemmirJusiza_5)); }
	inline String_t* get__dacemmirJusiza_5() const { return ____dacemmirJusiza_5; }
	inline String_t** get_address_of__dacemmirJusiza_5() { return &____dacemmirJusiza_5; }
	inline void set__dacemmirJusiza_5(String_t* value)
	{
		____dacemmirJusiza_5 = value;
		Il2CppCodeGenWriteBarrier(&____dacemmirJusiza_5, value);
	}

	inline static int32_t get_offset_of__kalgirDele_6() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____kalgirDele_6)); }
	inline int32_t get__kalgirDele_6() const { return ____kalgirDele_6; }
	inline int32_t* get_address_of__kalgirDele_6() { return &____kalgirDele_6; }
	inline void set__kalgirDele_6(int32_t value)
	{
		____kalgirDele_6 = value;
	}

	inline static int32_t get_offset_of__silu_7() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____silu_7)); }
	inline int32_t get__silu_7() const { return ____silu_7; }
	inline int32_t* get_address_of__silu_7() { return &____silu_7; }
	inline void set__silu_7(int32_t value)
	{
		____silu_7 = value;
	}

	inline static int32_t get_offset_of__stearsougooSoulee_8() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____stearsougooSoulee_8)); }
	inline bool get__stearsougooSoulee_8() const { return ____stearsougooSoulee_8; }
	inline bool* get_address_of__stearsougooSoulee_8() { return &____stearsougooSoulee_8; }
	inline void set__stearsougooSoulee_8(bool value)
	{
		____stearsougooSoulee_8 = value;
	}

	inline static int32_t get_offset_of__hejairchaGeryir_9() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____hejairchaGeryir_9)); }
	inline String_t* get__hejairchaGeryir_9() const { return ____hejairchaGeryir_9; }
	inline String_t** get_address_of__hejairchaGeryir_9() { return &____hejairchaGeryir_9; }
	inline void set__hejairchaGeryir_9(String_t* value)
	{
		____hejairchaGeryir_9 = value;
		Il2CppCodeGenWriteBarrier(&____hejairchaGeryir_9, value);
	}

	inline static int32_t get_offset_of__rairsallyasRaiwe_10() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____rairsallyasRaiwe_10)); }
	inline int32_t get__rairsallyasRaiwe_10() const { return ____rairsallyasRaiwe_10; }
	inline int32_t* get_address_of__rairsallyasRaiwe_10() { return &____rairsallyasRaiwe_10; }
	inline void set__rairsallyasRaiwe_10(int32_t value)
	{
		____rairsallyasRaiwe_10 = value;
	}

	inline static int32_t get_offset_of__jallseakeChitoo_11() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____jallseakeChitoo_11)); }
	inline bool get__jallseakeChitoo_11() const { return ____jallseakeChitoo_11; }
	inline bool* get_address_of__jallseakeChitoo_11() { return &____jallseakeChitoo_11; }
	inline void set__jallseakeChitoo_11(bool value)
	{
		____jallseakeChitoo_11 = value;
	}

	inline static int32_t get_offset_of__paybeNavi_12() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____paybeNavi_12)); }
	inline uint32_t get__paybeNavi_12() const { return ____paybeNavi_12; }
	inline uint32_t* get_address_of__paybeNavi_12() { return &____paybeNavi_12; }
	inline void set__paybeNavi_12(uint32_t value)
	{
		____paybeNavi_12 = value;
	}

	inline static int32_t get_offset_of__cairheaSoutal_13() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____cairheaSoutal_13)); }
	inline bool get__cairheaSoutal_13() const { return ____cairheaSoutal_13; }
	inline bool* get_address_of__cairheaSoutal_13() { return &____cairheaSoutal_13; }
	inline void set__cairheaSoutal_13(bool value)
	{
		____cairheaSoutal_13 = value;
	}

	inline static int32_t get_offset_of__curme_14() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____curme_14)); }
	inline String_t* get__curme_14() const { return ____curme_14; }
	inline String_t** get_address_of__curme_14() { return &____curme_14; }
	inline void set__curme_14(String_t* value)
	{
		____curme_14 = value;
		Il2CppCodeGenWriteBarrier(&____curme_14, value);
	}

	inline static int32_t get_offset_of__tapuLerewou_15() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____tapuLerewou_15)); }
	inline int32_t get__tapuLerewou_15() const { return ____tapuLerewou_15; }
	inline int32_t* get_address_of__tapuLerewou_15() { return &____tapuLerewou_15; }
	inline void set__tapuLerewou_15(int32_t value)
	{
		____tapuLerewou_15 = value;
	}

	inline static int32_t get_offset_of__kuterse_16() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____kuterse_16)); }
	inline bool get__kuterse_16() const { return ____kuterse_16; }
	inline bool* get_address_of__kuterse_16() { return &____kuterse_16; }
	inline void set__kuterse_16(bool value)
	{
		____kuterse_16 = value;
	}

	inline static int32_t get_offset_of__zurfisxeFisnu_17() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____zurfisxeFisnu_17)); }
	inline int32_t get__zurfisxeFisnu_17() const { return ____zurfisxeFisnu_17; }
	inline int32_t* get_address_of__zurfisxeFisnu_17() { return &____zurfisxeFisnu_17; }
	inline void set__zurfisxeFisnu_17(int32_t value)
	{
		____zurfisxeFisnu_17 = value;
	}

	inline static int32_t get_offset_of__bisaw_18() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____bisaw_18)); }
	inline uint32_t get__bisaw_18() const { return ____bisaw_18; }
	inline uint32_t* get_address_of__bisaw_18() { return &____bisaw_18; }
	inline void set__bisaw_18(uint32_t value)
	{
		____bisaw_18 = value;
	}

	inline static int32_t get_offset_of__nirogi_19() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____nirogi_19)); }
	inline float get__nirogi_19() const { return ____nirogi_19; }
	inline float* get_address_of__nirogi_19() { return &____nirogi_19; }
	inline void set__nirogi_19(float value)
	{
		____nirogi_19 = value;
	}

	inline static int32_t get_offset_of__soucearnowHelje_20() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____soucearnowHelje_20)); }
	inline bool get__soucearnowHelje_20() const { return ____soucearnowHelje_20; }
	inline bool* get_address_of__soucearnowHelje_20() { return &____soucearnowHelje_20; }
	inline void set__soucearnowHelje_20(bool value)
	{
		____soucearnowHelje_20 = value;
	}

	inline static int32_t get_offset_of__moudiNairjaskar_21() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____moudiNairjaskar_21)); }
	inline String_t* get__moudiNairjaskar_21() const { return ____moudiNairjaskar_21; }
	inline String_t** get_address_of__moudiNairjaskar_21() { return &____moudiNairjaskar_21; }
	inline void set__moudiNairjaskar_21(String_t* value)
	{
		____moudiNairjaskar_21 = value;
		Il2CppCodeGenWriteBarrier(&____moudiNairjaskar_21, value);
	}

	inline static int32_t get_offset_of__xesee_22() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____xesee_22)); }
	inline uint32_t get__xesee_22() const { return ____xesee_22; }
	inline uint32_t* get_address_of__xesee_22() { return &____xesee_22; }
	inline void set__xesee_22(uint32_t value)
	{
		____xesee_22 = value;
	}

	inline static int32_t get_offset_of__jejalKireetroo_23() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____jejalKireetroo_23)); }
	inline uint32_t get__jejalKireetroo_23() const { return ____jejalKireetroo_23; }
	inline uint32_t* get_address_of__jejalKireetroo_23() { return &____jejalKireetroo_23; }
	inline void set__jejalKireetroo_23(uint32_t value)
	{
		____jejalKireetroo_23 = value;
	}

	inline static int32_t get_offset_of__chimaihalMuler_24() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____chimaihalMuler_24)); }
	inline uint32_t get__chimaihalMuler_24() const { return ____chimaihalMuler_24; }
	inline uint32_t* get_address_of__chimaihalMuler_24() { return &____chimaihalMuler_24; }
	inline void set__chimaihalMuler_24(uint32_t value)
	{
		____chimaihalMuler_24 = value;
	}

	inline static int32_t get_offset_of__surteeDownu_25() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____surteeDownu_25)); }
	inline String_t* get__surteeDownu_25() const { return ____surteeDownu_25; }
	inline String_t** get_address_of__surteeDownu_25() { return &____surteeDownu_25; }
	inline void set__surteeDownu_25(String_t* value)
	{
		____surteeDownu_25 = value;
		Il2CppCodeGenWriteBarrier(&____surteeDownu_25, value);
	}

	inline static int32_t get_offset_of__piseawherWhaylaca_26() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____piseawherWhaylaca_26)); }
	inline String_t* get__piseawherWhaylaca_26() const { return ____piseawherWhaylaca_26; }
	inline String_t** get_address_of__piseawherWhaylaca_26() { return &____piseawherWhaylaca_26; }
	inline void set__piseawherWhaylaca_26(String_t* value)
	{
		____piseawherWhaylaca_26 = value;
		Il2CppCodeGenWriteBarrier(&____piseawherWhaylaca_26, value);
	}

	inline static int32_t get_offset_of__yaru_27() { return static_cast<int32_t>(offsetof(M_leqarBakou110_t2040651911, ____yaru_27)); }
	inline bool get__yaru_27() const { return ____yaru_27; }
	inline bool* get_address_of__yaru_27() { return &____yaru_27; }
	inline void set__yaru_27(bool value)
	{
		____yaru_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
