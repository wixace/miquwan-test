﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// CameraAniMap
struct CameraAniMap_t3004656005;
// CombatEntity
struct CombatEntity_t684137495;
// cameraShotCfg
struct cameraShotCfg_t781157413;
// CameraShotMgr/CameraShotVO
struct CameraShotVO_t1900810094;
// System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>
struct List_1_t3268995646;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject>
struct Dictionary_2_t3671945244;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraShotMgr
struct  CameraShotMgr_t1580128697  : public Il2CppObject
{
public:
	// CameraAniMap CameraShotMgr::curMapCameraAni
	CameraAniMap_t3004656005 * ___curMapCameraAni_4;
	// UnityEngine.Vector3 CameraShotMgr::circleLastPos
	Vector3_t4282066566  ___circleLastPos_5;
	// UnityEngine.Quaternion CameraShotMgr::circleLastQua
	Quaternion_t1553702882  ___circleLastQua_6;
	// System.Boolean CameraShotMgr::isEditorRun
	bool ___isEditorRun_7;
	// CombatEntity CameraShotMgr::curSkillEntity
	CombatEntity_t684137495 * ___curSkillEntity_8;
	// System.Int32 CameraShotMgr::curEffectid
	int32_t ___curEffectid_9;
	// System.Boolean CameraShotMgr::isCameraShotRunning
	bool ___isCameraShotRunning_10;
	// System.Boolean CameraShotMgr::isWith
	bool ___isWith_11;
	// System.Int32 CameraShotMgr::temp_Id
	int32_t ___temp_Id_12;
	// System.Single CameraShotMgr::temp_float
	float ___temp_float_13;
	// System.Int32 CameraShotMgr::tempSkill_int
	int32_t ___tempSkill_int_14;
	// cameraShotCfg CameraShotMgr::cameraShot_temp
	cameraShotCfg_t781157413 * ___cameraShot_temp_15;
	// CameraShotMgr/CameraShotVO CameraShotMgr::curCameraShot
	CameraShotVO_t1900810094 * ___curCameraShot_16;
	// System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO> CameraShotMgr::cameraShotQueue
	List_1_t3268995646 * ___cameraShotQueue_17;
	// System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO> CameraShotMgr::withQueue
	List_1_t3268995646 * ___withQueue_18;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject> CameraShotMgr::effectDic
	Dictionary_2_t3671945244 * ___effectDic_19;

public:
	inline static int32_t get_offset_of_curMapCameraAni_4() { return static_cast<int32_t>(offsetof(CameraShotMgr_t1580128697, ___curMapCameraAni_4)); }
	inline CameraAniMap_t3004656005 * get_curMapCameraAni_4() const { return ___curMapCameraAni_4; }
	inline CameraAniMap_t3004656005 ** get_address_of_curMapCameraAni_4() { return &___curMapCameraAni_4; }
	inline void set_curMapCameraAni_4(CameraAniMap_t3004656005 * value)
	{
		___curMapCameraAni_4 = value;
		Il2CppCodeGenWriteBarrier(&___curMapCameraAni_4, value);
	}

	inline static int32_t get_offset_of_circleLastPos_5() { return static_cast<int32_t>(offsetof(CameraShotMgr_t1580128697, ___circleLastPos_5)); }
	inline Vector3_t4282066566  get_circleLastPos_5() const { return ___circleLastPos_5; }
	inline Vector3_t4282066566 * get_address_of_circleLastPos_5() { return &___circleLastPos_5; }
	inline void set_circleLastPos_5(Vector3_t4282066566  value)
	{
		___circleLastPos_5 = value;
	}

	inline static int32_t get_offset_of_circleLastQua_6() { return static_cast<int32_t>(offsetof(CameraShotMgr_t1580128697, ___circleLastQua_6)); }
	inline Quaternion_t1553702882  get_circleLastQua_6() const { return ___circleLastQua_6; }
	inline Quaternion_t1553702882 * get_address_of_circleLastQua_6() { return &___circleLastQua_6; }
	inline void set_circleLastQua_6(Quaternion_t1553702882  value)
	{
		___circleLastQua_6 = value;
	}

	inline static int32_t get_offset_of_isEditorRun_7() { return static_cast<int32_t>(offsetof(CameraShotMgr_t1580128697, ___isEditorRun_7)); }
	inline bool get_isEditorRun_7() const { return ___isEditorRun_7; }
	inline bool* get_address_of_isEditorRun_7() { return &___isEditorRun_7; }
	inline void set_isEditorRun_7(bool value)
	{
		___isEditorRun_7 = value;
	}

	inline static int32_t get_offset_of_curSkillEntity_8() { return static_cast<int32_t>(offsetof(CameraShotMgr_t1580128697, ___curSkillEntity_8)); }
	inline CombatEntity_t684137495 * get_curSkillEntity_8() const { return ___curSkillEntity_8; }
	inline CombatEntity_t684137495 ** get_address_of_curSkillEntity_8() { return &___curSkillEntity_8; }
	inline void set_curSkillEntity_8(CombatEntity_t684137495 * value)
	{
		___curSkillEntity_8 = value;
		Il2CppCodeGenWriteBarrier(&___curSkillEntity_8, value);
	}

	inline static int32_t get_offset_of_curEffectid_9() { return static_cast<int32_t>(offsetof(CameraShotMgr_t1580128697, ___curEffectid_9)); }
	inline int32_t get_curEffectid_9() const { return ___curEffectid_9; }
	inline int32_t* get_address_of_curEffectid_9() { return &___curEffectid_9; }
	inline void set_curEffectid_9(int32_t value)
	{
		___curEffectid_9 = value;
	}

	inline static int32_t get_offset_of_isCameraShotRunning_10() { return static_cast<int32_t>(offsetof(CameraShotMgr_t1580128697, ___isCameraShotRunning_10)); }
	inline bool get_isCameraShotRunning_10() const { return ___isCameraShotRunning_10; }
	inline bool* get_address_of_isCameraShotRunning_10() { return &___isCameraShotRunning_10; }
	inline void set_isCameraShotRunning_10(bool value)
	{
		___isCameraShotRunning_10 = value;
	}

	inline static int32_t get_offset_of_isWith_11() { return static_cast<int32_t>(offsetof(CameraShotMgr_t1580128697, ___isWith_11)); }
	inline bool get_isWith_11() const { return ___isWith_11; }
	inline bool* get_address_of_isWith_11() { return &___isWith_11; }
	inline void set_isWith_11(bool value)
	{
		___isWith_11 = value;
	}

	inline static int32_t get_offset_of_temp_Id_12() { return static_cast<int32_t>(offsetof(CameraShotMgr_t1580128697, ___temp_Id_12)); }
	inline int32_t get_temp_Id_12() const { return ___temp_Id_12; }
	inline int32_t* get_address_of_temp_Id_12() { return &___temp_Id_12; }
	inline void set_temp_Id_12(int32_t value)
	{
		___temp_Id_12 = value;
	}

	inline static int32_t get_offset_of_temp_float_13() { return static_cast<int32_t>(offsetof(CameraShotMgr_t1580128697, ___temp_float_13)); }
	inline float get_temp_float_13() const { return ___temp_float_13; }
	inline float* get_address_of_temp_float_13() { return &___temp_float_13; }
	inline void set_temp_float_13(float value)
	{
		___temp_float_13 = value;
	}

	inline static int32_t get_offset_of_tempSkill_int_14() { return static_cast<int32_t>(offsetof(CameraShotMgr_t1580128697, ___tempSkill_int_14)); }
	inline int32_t get_tempSkill_int_14() const { return ___tempSkill_int_14; }
	inline int32_t* get_address_of_tempSkill_int_14() { return &___tempSkill_int_14; }
	inline void set_tempSkill_int_14(int32_t value)
	{
		___tempSkill_int_14 = value;
	}

	inline static int32_t get_offset_of_cameraShot_temp_15() { return static_cast<int32_t>(offsetof(CameraShotMgr_t1580128697, ___cameraShot_temp_15)); }
	inline cameraShotCfg_t781157413 * get_cameraShot_temp_15() const { return ___cameraShot_temp_15; }
	inline cameraShotCfg_t781157413 ** get_address_of_cameraShot_temp_15() { return &___cameraShot_temp_15; }
	inline void set_cameraShot_temp_15(cameraShotCfg_t781157413 * value)
	{
		___cameraShot_temp_15 = value;
		Il2CppCodeGenWriteBarrier(&___cameraShot_temp_15, value);
	}

	inline static int32_t get_offset_of_curCameraShot_16() { return static_cast<int32_t>(offsetof(CameraShotMgr_t1580128697, ___curCameraShot_16)); }
	inline CameraShotVO_t1900810094 * get_curCameraShot_16() const { return ___curCameraShot_16; }
	inline CameraShotVO_t1900810094 ** get_address_of_curCameraShot_16() { return &___curCameraShot_16; }
	inline void set_curCameraShot_16(CameraShotVO_t1900810094 * value)
	{
		___curCameraShot_16 = value;
		Il2CppCodeGenWriteBarrier(&___curCameraShot_16, value);
	}

	inline static int32_t get_offset_of_cameraShotQueue_17() { return static_cast<int32_t>(offsetof(CameraShotMgr_t1580128697, ___cameraShotQueue_17)); }
	inline List_1_t3268995646 * get_cameraShotQueue_17() const { return ___cameraShotQueue_17; }
	inline List_1_t3268995646 ** get_address_of_cameraShotQueue_17() { return &___cameraShotQueue_17; }
	inline void set_cameraShotQueue_17(List_1_t3268995646 * value)
	{
		___cameraShotQueue_17 = value;
		Il2CppCodeGenWriteBarrier(&___cameraShotQueue_17, value);
	}

	inline static int32_t get_offset_of_withQueue_18() { return static_cast<int32_t>(offsetof(CameraShotMgr_t1580128697, ___withQueue_18)); }
	inline List_1_t3268995646 * get_withQueue_18() const { return ___withQueue_18; }
	inline List_1_t3268995646 ** get_address_of_withQueue_18() { return &___withQueue_18; }
	inline void set_withQueue_18(List_1_t3268995646 * value)
	{
		___withQueue_18 = value;
		Il2CppCodeGenWriteBarrier(&___withQueue_18, value);
	}

	inline static int32_t get_offset_of_effectDic_19() { return static_cast<int32_t>(offsetof(CameraShotMgr_t1580128697, ___effectDic_19)); }
	inline Dictionary_2_t3671945244 * get_effectDic_19() const { return ___effectDic_19; }
	inline Dictionary_2_t3671945244 ** get_address_of_effectDic_19() { return &___effectDic_19; }
	inline void set_effectDic_19(Dictionary_2_t3671945244 * value)
	{
		___effectDic_19 = value;
		Il2CppCodeGenWriteBarrier(&___effectDic_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
