﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIPopupList/<UpdateTweenPosition>c__Iterator27
struct U3CUpdateTweenPositionU3Ec__Iterator27_t600375136;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UIPopupList/<UpdateTweenPosition>c__Iterator27::.ctor()
extern "C"  void U3CUpdateTweenPositionU3Ec__Iterator27__ctor_m1957673275 (U3CUpdateTweenPositionU3Ec__Iterator27_t600375136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIPopupList/<UpdateTweenPosition>c__Iterator27::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CUpdateTweenPositionU3Ec__Iterator27_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m132849345 (U3CUpdateTweenPositionU3Ec__Iterator27_t600375136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIPopupList/<UpdateTweenPosition>c__Iterator27::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CUpdateTweenPositionU3Ec__Iterator27_System_Collections_IEnumerator_get_Current_m579650645 (U3CUpdateTweenPositionU3Ec__Iterator27_t600375136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPopupList/<UpdateTweenPosition>c__Iterator27::MoveNext()
extern "C"  bool U3CUpdateTweenPositionU3Ec__Iterator27_MoveNext_m448107457 (U3CUpdateTweenPositionU3Ec__Iterator27_t600375136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList/<UpdateTweenPosition>c__Iterator27::Dispose()
extern "C"  void U3CUpdateTweenPositionU3Ec__Iterator27_Dispose_m26624696 (U3CUpdateTweenPositionU3Ec__Iterator27_t600375136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList/<UpdateTweenPosition>c__Iterator27::Reset()
extern "C"  void U3CUpdateTweenPositionU3Ec__Iterator27_Reset_m3899073512 (U3CUpdateTweenPositionU3Ec__Iterator27_t600375136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
