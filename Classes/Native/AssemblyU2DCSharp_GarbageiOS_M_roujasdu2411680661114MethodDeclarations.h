﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_roujasdu241
struct M_roujasdu241_t1680661114;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_roujasdu241::.ctor()
extern "C"  void M_roujasdu241__ctor_m2574436905 (M_roujasdu241_t1680661114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_roujasdu241::M_jasste0(System.String[],System.Int32)
extern "C"  void M_roujasdu241_M_jasste0_m2554597716 (M_roujasdu241_t1680661114 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
