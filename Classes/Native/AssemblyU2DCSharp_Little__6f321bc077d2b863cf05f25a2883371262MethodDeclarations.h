﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._6f321bc077d2b863cf05f25a221a6c71
struct _6f321bc077d2b863cf05f25a221a6c71_t2883371262;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._6f321bc077d2b863cf05f25a221a6c71::.ctor()
extern "C"  void _6f321bc077d2b863cf05f25a221a6c71__ctor_m488415983 (_6f321bc077d2b863cf05f25a221a6c71_t2883371262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6f321bc077d2b863cf05f25a221a6c71::_6f321bc077d2b863cf05f25a221a6c71m2(System.Int32)
extern "C"  int32_t _6f321bc077d2b863cf05f25a221a6c71__6f321bc077d2b863cf05f25a221a6c71m2_m2595913945 (_6f321bc077d2b863cf05f25a221a6c71_t2883371262 * __this, int32_t ____6f321bc077d2b863cf05f25a221a6c71a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6f321bc077d2b863cf05f25a221a6c71::_6f321bc077d2b863cf05f25a221a6c71m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _6f321bc077d2b863cf05f25a221a6c71__6f321bc077d2b863cf05f25a221a6c71m_m2303315453 (_6f321bc077d2b863cf05f25a221a6c71_t2883371262 * __this, int32_t ____6f321bc077d2b863cf05f25a221a6c71a0, int32_t ____6f321bc077d2b863cf05f25a221a6c71461, int32_t ____6f321bc077d2b863cf05f25a221a6c71c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
