﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EffectRotate
struct EffectRotate_t4041909868;

#include "codegen/il2cpp-codegen.h"

// System.Void EffectRotate::.ctor()
extern "C"  void EffectRotate__ctor_m2893268399 (EffectRotate_t4041909868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectRotate::Awake()
extern "C"  void EffectRotate_Awake_m3130873618 (EffectRotate_t4041909868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectRotate::Update()
extern "C"  void EffectRotate_Update_m1223869246 (EffectRotate_t4041909868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
