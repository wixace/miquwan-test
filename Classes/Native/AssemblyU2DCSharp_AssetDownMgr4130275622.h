﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AssetDownMgr
struct AssetDownMgr_t4130275622;
// Filelist`1<FileInfoCrc>
struct Filelist_1_t2494800866;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Collections.Generic.Dictionary`2<System.String,FileInfoRes>
struct Dictionary_2_t2037478168;
// System.Collections.Generic.List`1<Mihua.Net.UrlLoader>
struct List_1_t3858915048;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.Dictionary`2<System.Int32,AssetDownMgr/SetBytes>
struct Dictionary_2_t2853399961;
// System.Action
struct Action_t3771233898;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<FileInfoRes>>
struct Dictionary_2_t2582508589;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;
// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetDownMgr
struct  AssetDownMgr_t4130275622  : public Il2CppObject
{
public:
	// Filelist`1<FileInfoCrc> AssetDownMgr::fileCrcList
	Filelist_1_t2494800866 * ___fileCrcList_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> AssetDownMgr::updateCrcDic
	Dictionary_2_t827649927 * ___updateCrcDic_2;
	// System.Collections.Generic.Dictionary`2<System.String,FileInfoRes> AssetDownMgr::updateDic
	Dictionary_2_t2037478168 * ___updateDic_3;
	// System.Collections.Generic.List`1<Mihua.Net.UrlLoader> AssetDownMgr::wwwGroups
	List_1_t3858915048 * ___wwwGroups_4;
	// System.Collections.Generic.List`1<System.String> AssetDownMgr::waitUrlGroups
	List_1_t1375417109 * ___waitUrlGroups_5;
	// System.Collections.Generic.List`1<System.String> AssetDownMgr::writeFileData
	List_1_t1375417109 * ___writeFileData_6;
	// System.Int32 AssetDownMgr::MAXDOWNFILE
	int32_t ___MAXDOWNFILE_7;
	// System.Int32 AssetDownMgr::MAXUNZIPFILE
	int32_t ___MAXUNZIPFILE_8;
	// System.Int32 AssetDownMgr::MAXDOWNERROR
	int32_t ___MAXDOWNERROR_9;
	// System.Boolean AssetDownMgr::isStart
	bool ___isStart_10;
	// System.UInt32 AssetDownMgr::totalBytes
	uint32_t ___totalBytes_11;
	// System.UInt32 AssetDownMgr::curBytes
	uint32_t ___curBytes_12;
	// System.Int32 AssetDownMgr::totalFileCount
	int32_t ___totalFileCount_13;
	// System.Int32 AssetDownMgr::curFileCount
	int32_t ___curFileCount_14;
	// System.Boolean AssetDownMgr::IsWriteFinish
	bool ___IsWriteFinish_15;
	// System.Collections.Generic.Dictionary`2<System.Int32,AssetDownMgr/SetBytes> AssetDownMgr::downFinishCallDic
	Dictionary_2_t2853399961 * ___downFinishCallDic_16;
	// System.Action AssetDownMgr::writeDownFinishCall
	Action_t3771233898 * ___writeDownFinishCall_17;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<FileInfoRes>> AssetDownMgr::addDataDic
	Dictionary_2_t2582508589 * ___addDataDic_18;
	// System.Int32 AssetDownMgr::addTotal
	int32_t ___addTotal_19;
	// System.Int32 AssetDownMgr::adddownIndex
	int32_t ___adddownIndex_20;
	// System.Boolean AssetDownMgr::IsDownSuccess
	bool ___IsDownSuccess_21;
	// System.UInt32 AssetDownMgr::loadingBytes
	uint32_t ___loadingBytes_22;
	// System.UInt32 AssetDownMgr::loadedBytes
	uint32_t ___loadedBytes_23;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> AssetDownMgr::errorDownFiles
	Dictionary_2_t1974256870 * ___errorDownFiles_24;
	// System.String AssetDownMgr::errorUrl
	String_t* ___errorUrl_25;
	// System.Boolean AssetDownMgr::CanDownFile
	bool ___CanDownFile_26;

public:
	inline static int32_t get_offset_of_fileCrcList_1() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___fileCrcList_1)); }
	inline Filelist_1_t2494800866 * get_fileCrcList_1() const { return ___fileCrcList_1; }
	inline Filelist_1_t2494800866 ** get_address_of_fileCrcList_1() { return &___fileCrcList_1; }
	inline void set_fileCrcList_1(Filelist_1_t2494800866 * value)
	{
		___fileCrcList_1 = value;
		Il2CppCodeGenWriteBarrier(&___fileCrcList_1, value);
	}

	inline static int32_t get_offset_of_updateCrcDic_2() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___updateCrcDic_2)); }
	inline Dictionary_2_t827649927 * get_updateCrcDic_2() const { return ___updateCrcDic_2; }
	inline Dictionary_2_t827649927 ** get_address_of_updateCrcDic_2() { return &___updateCrcDic_2; }
	inline void set_updateCrcDic_2(Dictionary_2_t827649927 * value)
	{
		___updateCrcDic_2 = value;
		Il2CppCodeGenWriteBarrier(&___updateCrcDic_2, value);
	}

	inline static int32_t get_offset_of_updateDic_3() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___updateDic_3)); }
	inline Dictionary_2_t2037478168 * get_updateDic_3() const { return ___updateDic_3; }
	inline Dictionary_2_t2037478168 ** get_address_of_updateDic_3() { return &___updateDic_3; }
	inline void set_updateDic_3(Dictionary_2_t2037478168 * value)
	{
		___updateDic_3 = value;
		Il2CppCodeGenWriteBarrier(&___updateDic_3, value);
	}

	inline static int32_t get_offset_of_wwwGroups_4() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___wwwGroups_4)); }
	inline List_1_t3858915048 * get_wwwGroups_4() const { return ___wwwGroups_4; }
	inline List_1_t3858915048 ** get_address_of_wwwGroups_4() { return &___wwwGroups_4; }
	inline void set_wwwGroups_4(List_1_t3858915048 * value)
	{
		___wwwGroups_4 = value;
		Il2CppCodeGenWriteBarrier(&___wwwGroups_4, value);
	}

	inline static int32_t get_offset_of_waitUrlGroups_5() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___waitUrlGroups_5)); }
	inline List_1_t1375417109 * get_waitUrlGroups_5() const { return ___waitUrlGroups_5; }
	inline List_1_t1375417109 ** get_address_of_waitUrlGroups_5() { return &___waitUrlGroups_5; }
	inline void set_waitUrlGroups_5(List_1_t1375417109 * value)
	{
		___waitUrlGroups_5 = value;
		Il2CppCodeGenWriteBarrier(&___waitUrlGroups_5, value);
	}

	inline static int32_t get_offset_of_writeFileData_6() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___writeFileData_6)); }
	inline List_1_t1375417109 * get_writeFileData_6() const { return ___writeFileData_6; }
	inline List_1_t1375417109 ** get_address_of_writeFileData_6() { return &___writeFileData_6; }
	inline void set_writeFileData_6(List_1_t1375417109 * value)
	{
		___writeFileData_6 = value;
		Il2CppCodeGenWriteBarrier(&___writeFileData_6, value);
	}

	inline static int32_t get_offset_of_MAXDOWNFILE_7() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___MAXDOWNFILE_7)); }
	inline int32_t get_MAXDOWNFILE_7() const { return ___MAXDOWNFILE_7; }
	inline int32_t* get_address_of_MAXDOWNFILE_7() { return &___MAXDOWNFILE_7; }
	inline void set_MAXDOWNFILE_7(int32_t value)
	{
		___MAXDOWNFILE_7 = value;
	}

	inline static int32_t get_offset_of_MAXUNZIPFILE_8() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___MAXUNZIPFILE_8)); }
	inline int32_t get_MAXUNZIPFILE_8() const { return ___MAXUNZIPFILE_8; }
	inline int32_t* get_address_of_MAXUNZIPFILE_8() { return &___MAXUNZIPFILE_8; }
	inline void set_MAXUNZIPFILE_8(int32_t value)
	{
		___MAXUNZIPFILE_8 = value;
	}

	inline static int32_t get_offset_of_MAXDOWNERROR_9() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___MAXDOWNERROR_9)); }
	inline int32_t get_MAXDOWNERROR_9() const { return ___MAXDOWNERROR_9; }
	inline int32_t* get_address_of_MAXDOWNERROR_9() { return &___MAXDOWNERROR_9; }
	inline void set_MAXDOWNERROR_9(int32_t value)
	{
		___MAXDOWNERROR_9 = value;
	}

	inline static int32_t get_offset_of_isStart_10() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___isStart_10)); }
	inline bool get_isStart_10() const { return ___isStart_10; }
	inline bool* get_address_of_isStart_10() { return &___isStart_10; }
	inline void set_isStart_10(bool value)
	{
		___isStart_10 = value;
	}

	inline static int32_t get_offset_of_totalBytes_11() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___totalBytes_11)); }
	inline uint32_t get_totalBytes_11() const { return ___totalBytes_11; }
	inline uint32_t* get_address_of_totalBytes_11() { return &___totalBytes_11; }
	inline void set_totalBytes_11(uint32_t value)
	{
		___totalBytes_11 = value;
	}

	inline static int32_t get_offset_of_curBytes_12() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___curBytes_12)); }
	inline uint32_t get_curBytes_12() const { return ___curBytes_12; }
	inline uint32_t* get_address_of_curBytes_12() { return &___curBytes_12; }
	inline void set_curBytes_12(uint32_t value)
	{
		___curBytes_12 = value;
	}

	inline static int32_t get_offset_of_totalFileCount_13() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___totalFileCount_13)); }
	inline int32_t get_totalFileCount_13() const { return ___totalFileCount_13; }
	inline int32_t* get_address_of_totalFileCount_13() { return &___totalFileCount_13; }
	inline void set_totalFileCount_13(int32_t value)
	{
		___totalFileCount_13 = value;
	}

	inline static int32_t get_offset_of_curFileCount_14() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___curFileCount_14)); }
	inline int32_t get_curFileCount_14() const { return ___curFileCount_14; }
	inline int32_t* get_address_of_curFileCount_14() { return &___curFileCount_14; }
	inline void set_curFileCount_14(int32_t value)
	{
		___curFileCount_14 = value;
	}

	inline static int32_t get_offset_of_IsWriteFinish_15() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___IsWriteFinish_15)); }
	inline bool get_IsWriteFinish_15() const { return ___IsWriteFinish_15; }
	inline bool* get_address_of_IsWriteFinish_15() { return &___IsWriteFinish_15; }
	inline void set_IsWriteFinish_15(bool value)
	{
		___IsWriteFinish_15 = value;
	}

	inline static int32_t get_offset_of_downFinishCallDic_16() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___downFinishCallDic_16)); }
	inline Dictionary_2_t2853399961 * get_downFinishCallDic_16() const { return ___downFinishCallDic_16; }
	inline Dictionary_2_t2853399961 ** get_address_of_downFinishCallDic_16() { return &___downFinishCallDic_16; }
	inline void set_downFinishCallDic_16(Dictionary_2_t2853399961 * value)
	{
		___downFinishCallDic_16 = value;
		Il2CppCodeGenWriteBarrier(&___downFinishCallDic_16, value);
	}

	inline static int32_t get_offset_of_writeDownFinishCall_17() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___writeDownFinishCall_17)); }
	inline Action_t3771233898 * get_writeDownFinishCall_17() const { return ___writeDownFinishCall_17; }
	inline Action_t3771233898 ** get_address_of_writeDownFinishCall_17() { return &___writeDownFinishCall_17; }
	inline void set_writeDownFinishCall_17(Action_t3771233898 * value)
	{
		___writeDownFinishCall_17 = value;
		Il2CppCodeGenWriteBarrier(&___writeDownFinishCall_17, value);
	}

	inline static int32_t get_offset_of_addDataDic_18() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___addDataDic_18)); }
	inline Dictionary_2_t2582508589 * get_addDataDic_18() const { return ___addDataDic_18; }
	inline Dictionary_2_t2582508589 ** get_address_of_addDataDic_18() { return &___addDataDic_18; }
	inline void set_addDataDic_18(Dictionary_2_t2582508589 * value)
	{
		___addDataDic_18 = value;
		Il2CppCodeGenWriteBarrier(&___addDataDic_18, value);
	}

	inline static int32_t get_offset_of_addTotal_19() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___addTotal_19)); }
	inline int32_t get_addTotal_19() const { return ___addTotal_19; }
	inline int32_t* get_address_of_addTotal_19() { return &___addTotal_19; }
	inline void set_addTotal_19(int32_t value)
	{
		___addTotal_19 = value;
	}

	inline static int32_t get_offset_of_adddownIndex_20() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___adddownIndex_20)); }
	inline int32_t get_adddownIndex_20() const { return ___adddownIndex_20; }
	inline int32_t* get_address_of_adddownIndex_20() { return &___adddownIndex_20; }
	inline void set_adddownIndex_20(int32_t value)
	{
		___adddownIndex_20 = value;
	}

	inline static int32_t get_offset_of_IsDownSuccess_21() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___IsDownSuccess_21)); }
	inline bool get_IsDownSuccess_21() const { return ___IsDownSuccess_21; }
	inline bool* get_address_of_IsDownSuccess_21() { return &___IsDownSuccess_21; }
	inline void set_IsDownSuccess_21(bool value)
	{
		___IsDownSuccess_21 = value;
	}

	inline static int32_t get_offset_of_loadingBytes_22() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___loadingBytes_22)); }
	inline uint32_t get_loadingBytes_22() const { return ___loadingBytes_22; }
	inline uint32_t* get_address_of_loadingBytes_22() { return &___loadingBytes_22; }
	inline void set_loadingBytes_22(uint32_t value)
	{
		___loadingBytes_22 = value;
	}

	inline static int32_t get_offset_of_loadedBytes_23() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___loadedBytes_23)); }
	inline uint32_t get_loadedBytes_23() const { return ___loadedBytes_23; }
	inline uint32_t* get_address_of_loadedBytes_23() { return &___loadedBytes_23; }
	inline void set_loadedBytes_23(uint32_t value)
	{
		___loadedBytes_23 = value;
	}

	inline static int32_t get_offset_of_errorDownFiles_24() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___errorDownFiles_24)); }
	inline Dictionary_2_t1974256870 * get_errorDownFiles_24() const { return ___errorDownFiles_24; }
	inline Dictionary_2_t1974256870 ** get_address_of_errorDownFiles_24() { return &___errorDownFiles_24; }
	inline void set_errorDownFiles_24(Dictionary_2_t1974256870 * value)
	{
		___errorDownFiles_24 = value;
		Il2CppCodeGenWriteBarrier(&___errorDownFiles_24, value);
	}

	inline static int32_t get_offset_of_errorUrl_25() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___errorUrl_25)); }
	inline String_t* get_errorUrl_25() const { return ___errorUrl_25; }
	inline String_t** get_address_of_errorUrl_25() { return &___errorUrl_25; }
	inline void set_errorUrl_25(String_t* value)
	{
		___errorUrl_25 = value;
		Il2CppCodeGenWriteBarrier(&___errorUrl_25, value);
	}

	inline static int32_t get_offset_of_CanDownFile_26() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622, ___CanDownFile_26)); }
	inline bool get_CanDownFile_26() const { return ___CanDownFile_26; }
	inline bool* get_address_of_CanDownFile_26() { return &___CanDownFile_26; }
	inline void set_CanDownFile_26(bool value)
	{
		___CanDownFile_26 = value;
	}
};

struct AssetDownMgr_t4130275622_StaticFields
{
public:
	// AssetDownMgr AssetDownMgr::_instance
	AssetDownMgr_t4130275622 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(AssetDownMgr_t4130275622_StaticFields, ____instance_0)); }
	inline AssetDownMgr_t4130275622 * get__instance_0() const { return ____instance_0; }
	inline AssetDownMgr_t4130275622 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(AssetDownMgr_t4130275622 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
