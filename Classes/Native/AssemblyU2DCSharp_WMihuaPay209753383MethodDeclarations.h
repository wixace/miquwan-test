﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WMihuaPay
struct WMihuaPay_t209753383;
// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// VersionMgr
struct VersionMgr_t1322950208;
// VersionInfo
struct VersionInfo_t2356638086;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_WMihuaPay209753383.h"

// System.Void WMihuaPay::.ctor(System.String)
extern "C"  void WMihuaPay__ctor_m782572894 (WMihuaPay_t209753383 * __this, String_t* ___referer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WMihuaPay::Tiaoqian(Mihua.SDK.PayInfo)
extern "C"  void WMihuaPay_Tiaoqian_m2255232030 (WMihuaPay_t209753383 * __this, PayInfo_t1775308120 * ___payInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WMihuaPay::SignCallBack(System.Boolean,System.String)
extern "C"  void WMihuaPay_SignCallBack_m1651943637 (WMihuaPay_t209753383 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WMihuaPay::__IWmp(System.String,System.String)
extern "C"  void WMihuaPay___IWmp_m1010143597 (WMihuaPay_t209753383 * __this, String_t* ___payOrder0, String_t* ___reScheme1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr WMihuaPay::ilo_get_Instance1()
extern "C"  VersionMgr_t1322950208 * WMihuaPay_ilo_get_Instance1_m3991296383 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo WMihuaPay::ilo_get_currentVS2(VersionMgr)
extern "C"  VersionInfo_t2356638086 * WMihuaPay_ilo_get_currentVS2_m922487179 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WMihuaPay::ilo___IWmp3(WMihuaPay,System.String,System.String)
extern "C"  void WMihuaPay_ilo___IWmp3_m1678098194 (Il2CppObject * __this /* static, unused */, WMihuaPay_t209753383 * ____this0, String_t* ___payOrder1, String_t* ___reScheme2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
