﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.LocalAvoidance/VO
struct VO_t2172220293;
// Pathfinding.LocalAvoidance/VOIntersection
struct VOIntersection_t3990934190;
struct VOIntersection_t3990934190_marshaled_pinvoke;
struct VOIntersection_t3990934190_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_VOInt3990934190.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_VO2172220293.h"

// System.Void Pathfinding.LocalAvoidance/VOIntersection::.ctor(Pathfinding.LocalAvoidance/VO,Pathfinding.LocalAvoidance/VO,System.Single,System.Single,System.Boolean)
extern "C"  void VOIntersection__ctor_m2887163740 (VOIntersection_t3990934190 * __this, VO_t2172220293 * ___vo10, VO_t2172220293 * ___vo21, float ___factor12, float ___factor23, bool ___inside4, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct VOIntersection_t3990934190;
struct VOIntersection_t3990934190_marshaled_pinvoke;

extern "C" void VOIntersection_t3990934190_marshal_pinvoke(const VOIntersection_t3990934190& unmarshaled, VOIntersection_t3990934190_marshaled_pinvoke& marshaled);
extern "C" void VOIntersection_t3990934190_marshal_pinvoke_back(const VOIntersection_t3990934190_marshaled_pinvoke& marshaled, VOIntersection_t3990934190& unmarshaled);
extern "C" void VOIntersection_t3990934190_marshal_pinvoke_cleanup(VOIntersection_t3990934190_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct VOIntersection_t3990934190;
struct VOIntersection_t3990934190_marshaled_com;

extern "C" void VOIntersection_t3990934190_marshal_com(const VOIntersection_t3990934190& unmarshaled, VOIntersection_t3990934190_marshaled_com& marshaled);
extern "C" void VOIntersection_t3990934190_marshal_com_back(const VOIntersection_t3990934190_marshaled_com& marshaled, VOIntersection_t3990934190& unmarshaled);
extern "C" void VOIntersection_t3990934190_marshal_com_cleanup(VOIntersection_t3990934190_marshaled_com& marshaled);
