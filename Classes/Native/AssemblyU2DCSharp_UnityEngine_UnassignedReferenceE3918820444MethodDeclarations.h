﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_UnassignedReferenceExceptionGenerated
struct UnityEngine_UnassignedReferenceExceptionGenerated_t3918820444;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_UnassignedReferenceExceptionGenerated::.ctor()
extern "C"  void UnityEngine_UnassignedReferenceExceptionGenerated__ctor_m3294150799 (UnityEngine_UnassignedReferenceExceptionGenerated_t3918820444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_UnassignedReferenceExceptionGenerated::UnassignedReferenceException_UnassignedReferenceException1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_UnassignedReferenceExceptionGenerated_UnassignedReferenceException_UnassignedReferenceException1_m93800803 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_UnassignedReferenceExceptionGenerated::UnassignedReferenceException_UnassignedReferenceException2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_UnassignedReferenceExceptionGenerated_UnassignedReferenceException_UnassignedReferenceException2_m1338565284 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_UnassignedReferenceExceptionGenerated::UnassignedReferenceException_UnassignedReferenceException3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_UnassignedReferenceExceptionGenerated_UnassignedReferenceException_UnassignedReferenceException3_m2583329765 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UnassignedReferenceExceptionGenerated::__Register()
extern "C"  void UnityEngine_UnassignedReferenceExceptionGenerated___Register_m784711064 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UnassignedReferenceExceptionGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_UnassignedReferenceExceptionGenerated_ilo_addJSCSRel1_m2672262091 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_UnassignedReferenceExceptionGenerated::ilo_getStringS2(System.Int32)
extern "C"  String_t* UnityEngine_UnassignedReferenceExceptionGenerated_ilo_getStringS2_m2370738932 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_UnassignedReferenceExceptionGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_UnassignedReferenceExceptionGenerated_ilo_getObject3_m4177996728 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
