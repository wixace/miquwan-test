﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginYiwanSuper
struct  PluginYiwanSuper_t2354859450  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginYiwanSuper::openid
	String_t* ___openid_6;
	// System.String PluginYiwanSuper::token
	String_t* ___token_7;
	// System.String PluginYiwanSuper::sign
	String_t* ___sign_8;
	// System.String PluginYiwanSuper::configId
	String_t* ___configId_9;
	// System.Boolean PluginYiwanSuper::isPluginInitOK
	bool ___isPluginInitOK_10;

public:
	inline static int32_t get_offset_of_openid_6() { return static_cast<int32_t>(offsetof(PluginYiwanSuper_t2354859450, ___openid_6)); }
	inline String_t* get_openid_6() const { return ___openid_6; }
	inline String_t** get_address_of_openid_6() { return &___openid_6; }
	inline void set_openid_6(String_t* value)
	{
		___openid_6 = value;
		Il2CppCodeGenWriteBarrier(&___openid_6, value);
	}

	inline static int32_t get_offset_of_token_7() { return static_cast<int32_t>(offsetof(PluginYiwanSuper_t2354859450, ___token_7)); }
	inline String_t* get_token_7() const { return ___token_7; }
	inline String_t** get_address_of_token_7() { return &___token_7; }
	inline void set_token_7(String_t* value)
	{
		___token_7 = value;
		Il2CppCodeGenWriteBarrier(&___token_7, value);
	}

	inline static int32_t get_offset_of_sign_8() { return static_cast<int32_t>(offsetof(PluginYiwanSuper_t2354859450, ___sign_8)); }
	inline String_t* get_sign_8() const { return ___sign_8; }
	inline String_t** get_address_of_sign_8() { return &___sign_8; }
	inline void set_sign_8(String_t* value)
	{
		___sign_8 = value;
		Il2CppCodeGenWriteBarrier(&___sign_8, value);
	}

	inline static int32_t get_offset_of_configId_9() { return static_cast<int32_t>(offsetof(PluginYiwanSuper_t2354859450, ___configId_9)); }
	inline String_t* get_configId_9() const { return ___configId_9; }
	inline String_t** get_address_of_configId_9() { return &___configId_9; }
	inline void set_configId_9(String_t* value)
	{
		___configId_9 = value;
		Il2CppCodeGenWriteBarrier(&___configId_9, value);
	}

	inline static int32_t get_offset_of_isPluginInitOK_10() { return static_cast<int32_t>(offsetof(PluginYiwanSuper_t2354859450, ___isPluginInitOK_10)); }
	inline bool get_isPluginInitOK_10() const { return ___isPluginInitOK_10; }
	inline bool* get_address_of_isPluginInitOK_10() { return &___isPluginInitOK_10; }
	inline void set_isPluginInitOK_10(bool value)
	{
		___isPluginInitOK_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
