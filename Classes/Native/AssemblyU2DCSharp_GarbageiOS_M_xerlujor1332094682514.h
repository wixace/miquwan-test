﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_xerlujor133
struct  M_xerlujor133_t2094682514  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_xerlujor133::_jemgi
	int32_t ____jemgi_0;
	// System.String GarbageiOS.M_xerlujor133::_tifu
	String_t* ____tifu_1;
	// System.String GarbageiOS.M_xerlujor133::_stujiTeechelpe
	String_t* ____stujiTeechelpe_2;
	// System.Int32 GarbageiOS.M_xerlujor133::_nasbayCaredir
	int32_t ____nasbayCaredir_3;
	// System.UInt32 GarbageiOS.M_xerlujor133::_falremtrall
	uint32_t ____falremtrall_4;
	// System.Int32 GarbageiOS.M_xerlujor133::_chaserRooco
	int32_t ____chaserRooco_5;
	// System.Single GarbageiOS.M_xerlujor133::_sowsostouRudis
	float ____sowsostouRudis_6;
	// System.String GarbageiOS.M_xerlujor133::_meazuqalKirsearbou
	String_t* ____meazuqalKirsearbou_7;
	// System.UInt32 GarbageiOS.M_xerlujor133::_rurter
	uint32_t ____rurter_8;
	// System.Boolean GarbageiOS.M_xerlujor133::_fachounaiSaitre
	bool ____fachounaiSaitre_9;
	// System.UInt32 GarbageiOS.M_xerlujor133::_steldowqallHurkule
	uint32_t ____steldowqallHurkule_10;
	// System.UInt32 GarbageiOS.M_xerlujor133::_belnelBenall
	uint32_t ____belnelBenall_11;
	// System.Boolean GarbageiOS.M_xerlujor133::_hemvis
	bool ____hemvis_12;
	// System.UInt32 GarbageiOS.M_xerlujor133::_mihem
	uint32_t ____mihem_13;

public:
	inline static int32_t get_offset_of__jemgi_0() { return static_cast<int32_t>(offsetof(M_xerlujor133_t2094682514, ____jemgi_0)); }
	inline int32_t get__jemgi_0() const { return ____jemgi_0; }
	inline int32_t* get_address_of__jemgi_0() { return &____jemgi_0; }
	inline void set__jemgi_0(int32_t value)
	{
		____jemgi_0 = value;
	}

	inline static int32_t get_offset_of__tifu_1() { return static_cast<int32_t>(offsetof(M_xerlujor133_t2094682514, ____tifu_1)); }
	inline String_t* get__tifu_1() const { return ____tifu_1; }
	inline String_t** get_address_of__tifu_1() { return &____tifu_1; }
	inline void set__tifu_1(String_t* value)
	{
		____tifu_1 = value;
		Il2CppCodeGenWriteBarrier(&____tifu_1, value);
	}

	inline static int32_t get_offset_of__stujiTeechelpe_2() { return static_cast<int32_t>(offsetof(M_xerlujor133_t2094682514, ____stujiTeechelpe_2)); }
	inline String_t* get__stujiTeechelpe_2() const { return ____stujiTeechelpe_2; }
	inline String_t** get_address_of__stujiTeechelpe_2() { return &____stujiTeechelpe_2; }
	inline void set__stujiTeechelpe_2(String_t* value)
	{
		____stujiTeechelpe_2 = value;
		Il2CppCodeGenWriteBarrier(&____stujiTeechelpe_2, value);
	}

	inline static int32_t get_offset_of__nasbayCaredir_3() { return static_cast<int32_t>(offsetof(M_xerlujor133_t2094682514, ____nasbayCaredir_3)); }
	inline int32_t get__nasbayCaredir_3() const { return ____nasbayCaredir_3; }
	inline int32_t* get_address_of__nasbayCaredir_3() { return &____nasbayCaredir_3; }
	inline void set__nasbayCaredir_3(int32_t value)
	{
		____nasbayCaredir_3 = value;
	}

	inline static int32_t get_offset_of__falremtrall_4() { return static_cast<int32_t>(offsetof(M_xerlujor133_t2094682514, ____falremtrall_4)); }
	inline uint32_t get__falremtrall_4() const { return ____falremtrall_4; }
	inline uint32_t* get_address_of__falremtrall_4() { return &____falremtrall_4; }
	inline void set__falremtrall_4(uint32_t value)
	{
		____falremtrall_4 = value;
	}

	inline static int32_t get_offset_of__chaserRooco_5() { return static_cast<int32_t>(offsetof(M_xerlujor133_t2094682514, ____chaserRooco_5)); }
	inline int32_t get__chaserRooco_5() const { return ____chaserRooco_5; }
	inline int32_t* get_address_of__chaserRooco_5() { return &____chaserRooco_5; }
	inline void set__chaserRooco_5(int32_t value)
	{
		____chaserRooco_5 = value;
	}

	inline static int32_t get_offset_of__sowsostouRudis_6() { return static_cast<int32_t>(offsetof(M_xerlujor133_t2094682514, ____sowsostouRudis_6)); }
	inline float get__sowsostouRudis_6() const { return ____sowsostouRudis_6; }
	inline float* get_address_of__sowsostouRudis_6() { return &____sowsostouRudis_6; }
	inline void set__sowsostouRudis_6(float value)
	{
		____sowsostouRudis_6 = value;
	}

	inline static int32_t get_offset_of__meazuqalKirsearbou_7() { return static_cast<int32_t>(offsetof(M_xerlujor133_t2094682514, ____meazuqalKirsearbou_7)); }
	inline String_t* get__meazuqalKirsearbou_7() const { return ____meazuqalKirsearbou_7; }
	inline String_t** get_address_of__meazuqalKirsearbou_7() { return &____meazuqalKirsearbou_7; }
	inline void set__meazuqalKirsearbou_7(String_t* value)
	{
		____meazuqalKirsearbou_7 = value;
		Il2CppCodeGenWriteBarrier(&____meazuqalKirsearbou_7, value);
	}

	inline static int32_t get_offset_of__rurter_8() { return static_cast<int32_t>(offsetof(M_xerlujor133_t2094682514, ____rurter_8)); }
	inline uint32_t get__rurter_8() const { return ____rurter_8; }
	inline uint32_t* get_address_of__rurter_8() { return &____rurter_8; }
	inline void set__rurter_8(uint32_t value)
	{
		____rurter_8 = value;
	}

	inline static int32_t get_offset_of__fachounaiSaitre_9() { return static_cast<int32_t>(offsetof(M_xerlujor133_t2094682514, ____fachounaiSaitre_9)); }
	inline bool get__fachounaiSaitre_9() const { return ____fachounaiSaitre_9; }
	inline bool* get_address_of__fachounaiSaitre_9() { return &____fachounaiSaitre_9; }
	inline void set__fachounaiSaitre_9(bool value)
	{
		____fachounaiSaitre_9 = value;
	}

	inline static int32_t get_offset_of__steldowqallHurkule_10() { return static_cast<int32_t>(offsetof(M_xerlujor133_t2094682514, ____steldowqallHurkule_10)); }
	inline uint32_t get__steldowqallHurkule_10() const { return ____steldowqallHurkule_10; }
	inline uint32_t* get_address_of__steldowqallHurkule_10() { return &____steldowqallHurkule_10; }
	inline void set__steldowqallHurkule_10(uint32_t value)
	{
		____steldowqallHurkule_10 = value;
	}

	inline static int32_t get_offset_of__belnelBenall_11() { return static_cast<int32_t>(offsetof(M_xerlujor133_t2094682514, ____belnelBenall_11)); }
	inline uint32_t get__belnelBenall_11() const { return ____belnelBenall_11; }
	inline uint32_t* get_address_of__belnelBenall_11() { return &____belnelBenall_11; }
	inline void set__belnelBenall_11(uint32_t value)
	{
		____belnelBenall_11 = value;
	}

	inline static int32_t get_offset_of__hemvis_12() { return static_cast<int32_t>(offsetof(M_xerlujor133_t2094682514, ____hemvis_12)); }
	inline bool get__hemvis_12() const { return ____hemvis_12; }
	inline bool* get_address_of__hemvis_12() { return &____hemvis_12; }
	inline void set__hemvis_12(bool value)
	{
		____hemvis_12 = value;
	}

	inline static int32_t get_offset_of__mihem_13() { return static_cast<int32_t>(offsetof(M_xerlujor133_t2094682514, ____mihem_13)); }
	inline uint32_t get__mihem_13() const { return ____mihem_13; }
	inline uint32_t* get_address_of__mihem_13() { return &____mihem_13; }
	inline void set__mihem_13(uint32_t value)
	{
		____mihem_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
