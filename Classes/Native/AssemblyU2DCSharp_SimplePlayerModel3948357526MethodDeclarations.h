﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimplePlayerModel
struct SimplePlayerModel_t3948357526;
struct SimplePlayerModel_t3948357526_marshaled_pinvoke;
struct SimplePlayerModel_t3948357526_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct SimplePlayerModel_t3948357526;
struct SimplePlayerModel_t3948357526_marshaled_pinvoke;

extern "C" void SimplePlayerModel_t3948357526_marshal_pinvoke(const SimplePlayerModel_t3948357526& unmarshaled, SimplePlayerModel_t3948357526_marshaled_pinvoke& marshaled);
extern "C" void SimplePlayerModel_t3948357526_marshal_pinvoke_back(const SimplePlayerModel_t3948357526_marshaled_pinvoke& marshaled, SimplePlayerModel_t3948357526& unmarshaled);
extern "C" void SimplePlayerModel_t3948357526_marshal_pinvoke_cleanup(SimplePlayerModel_t3948357526_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct SimplePlayerModel_t3948357526;
struct SimplePlayerModel_t3948357526_marshaled_com;

extern "C" void SimplePlayerModel_t3948357526_marshal_com(const SimplePlayerModel_t3948357526& unmarshaled, SimplePlayerModel_t3948357526_marshaled_com& marshaled);
extern "C" void SimplePlayerModel_t3948357526_marshal_com_back(const SimplePlayerModel_t3948357526_marshaled_com& marshaled, SimplePlayerModel_t3948357526& unmarshaled);
extern "C" void SimplePlayerModel_t3948357526_marshal_com_cleanup(SimplePlayerModel_t3948357526_marshaled_com& marshaled);
