﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._cc7bf951ac69f31c9adc19b4a3b34de0
struct _cc7bf951ac69f31c9adc19b4a3b34de0_t283737046;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._cc7bf951ac69f31c9adc19b4a3b34de0::.ctor()
extern "C"  void _cc7bf951ac69f31c9adc19b4a3b34de0__ctor_m532192023 (_cc7bf951ac69f31c9adc19b4a3b34de0_t283737046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._cc7bf951ac69f31c9adc19b4a3b34de0::_cc7bf951ac69f31c9adc19b4a3b34de0m2(System.Int32)
extern "C"  int32_t _cc7bf951ac69f31c9adc19b4a3b34de0__cc7bf951ac69f31c9adc19b4a3b34de0m2_m2831536601 (_cc7bf951ac69f31c9adc19b4a3b34de0_t283737046 * __this, int32_t ____cc7bf951ac69f31c9adc19b4a3b34de0a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._cc7bf951ac69f31c9adc19b4a3b34de0::_cc7bf951ac69f31c9adc19b4a3b34de0m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _cc7bf951ac69f31c9adc19b4a3b34de0__cc7bf951ac69f31c9adc19b4a3b34de0m_m2431360253 (_cc7bf951ac69f31c9adc19b4a3b34de0_t283737046 * __this, int32_t ____cc7bf951ac69f31c9adc19b4a3b34de0a0, int32_t ____cc7bf951ac69f31c9adc19b4a3b34de0921, int32_t ____cc7bf951ac69f31c9adc19b4a3b34de0c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
