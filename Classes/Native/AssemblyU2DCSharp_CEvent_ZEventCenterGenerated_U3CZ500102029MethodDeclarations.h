﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CEvent_ZEventCenterGenerated/<ZEventCenter_RemoveEventListener_GetDelegate_member3_arg1>c__AnonStorey54
struct U3CZEventCenter_RemoveEventListener_GetDelegate_member3_arg1U3Ec__AnonStorey54_t500102029;
// CEvent.ZEvent
struct ZEvent_t3638018500;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"

// System.Void CEvent_ZEventCenterGenerated/<ZEventCenter_RemoveEventListener_GetDelegate_member3_arg1>c__AnonStorey54::.ctor()
extern "C"  void U3CZEventCenter_RemoveEventListener_GetDelegate_member3_arg1U3Ec__AnonStorey54__ctor_m548276862 (U3CZEventCenter_RemoveEventListener_GetDelegate_member3_arg1U3Ec__AnonStorey54_t500102029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent_ZEventCenterGenerated/<ZEventCenter_RemoveEventListener_GetDelegate_member3_arg1>c__AnonStorey54::<>m__1D(CEvent.ZEvent)
extern "C"  void U3CZEventCenter_RemoveEventListener_GetDelegate_member3_arg1U3Ec__AnonStorey54_U3CU3Em__1D_m1775734657 (U3CZEventCenter_RemoveEventListener_GetDelegate_member3_arg1U3Ec__AnonStorey54_t500102029 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
