﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_PlaneGenerated
struct UnityEngine_PlaneGenerated_t1225406971;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_PlaneGenerated::.ctor()
extern "C"  void UnityEngine_PlaneGenerated__ctor_m134232320 (UnityEngine_PlaneGenerated_t1225406971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PlaneGenerated::.cctor()
extern "C"  void UnityEngine_PlaneGenerated__cctor_m3679105709 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlaneGenerated::Plane_Plane1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PlaneGenerated_Plane_Plane1_m646457408 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlaneGenerated::Plane_Plane2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PlaneGenerated_Plane_Plane2_m1891221889 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlaneGenerated::Plane_Plane3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PlaneGenerated_Plane_Plane3_m3135986370 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlaneGenerated::Plane_Plane4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PlaneGenerated_Plane_Plane4_m85783555 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PlaneGenerated::Plane_normal(JSVCall)
extern "C"  void UnityEngine_PlaneGenerated_Plane_normal_m371384057 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PlaneGenerated::Plane_distance(JSVCall)
extern "C"  void UnityEngine_PlaneGenerated_Plane_distance_m3337120043 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlaneGenerated::Plane_GetDistanceToPoint__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PlaneGenerated_Plane_GetDistanceToPoint__Vector3_m2535752669 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlaneGenerated::Plane_GetSide__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PlaneGenerated_Plane_GetSide__Vector3_m2790520846 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlaneGenerated::Plane_Raycast__Ray__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PlaneGenerated_Plane_Raycast__Ray__Single_m2229387540 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlaneGenerated::Plane_SameSide__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PlaneGenerated_Plane_SameSide__Vector3__Vector3_m686295944 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlaneGenerated::Plane_Set3Points__Vector3__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PlaneGenerated_Plane_Set3Points__Vector3__Vector3__Vector3_m2034501043 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlaneGenerated::Plane_SetNormalAndPosition__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PlaneGenerated_Plane_SetNormalAndPosition__Vector3__Vector3_m1600670754 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PlaneGenerated::__Register()
extern "C"  void UnityEngine_PlaneGenerated___Register_m2715608007 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlaneGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_PlaneGenerated_ilo_attachFinalizerObject1_m1862045151 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_PlaneGenerated::ilo_getObject2(System.Int32)
extern "C"  int32_t UnityEngine_PlaneGenerated_ilo_getObject2_m967038675 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PlaneGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void UnityEngine_PlaneGenerated_ilo_addJSCSRel3_m105322686 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_PlaneGenerated::ilo_getVector3S4(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_PlaneGenerated_ilo_getVector3S4_m4125506557 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PlaneGenerated::ilo_setVector3S5(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_PlaneGenerated_ilo_setVector3S5_m3772587083 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PlaneGenerated::ilo_changeJSObj6(System.Int32,System.Object)
extern "C"  void UnityEngine_PlaneGenerated_ilo_changeJSObj6_m3145335534 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PlaneGenerated::ilo_setBooleanS7(System.Int32,System.Boolean)
extern "C"  void UnityEngine_PlaneGenerated_ilo_setBooleanS7_m2473124139 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PlaneGenerated::ilo_setSingle8(System.Int32,System.Single)
extern "C"  void UnityEngine_PlaneGenerated_ilo_setSingle8_m1835237115 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
