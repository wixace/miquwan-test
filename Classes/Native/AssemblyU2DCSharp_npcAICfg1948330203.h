﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// npcAICfg
struct  npcAICfg_t1948330203  : public CsCfgBase_t69924517
{
public:
	// System.Int32 npcAICfg::id
	int32_t ___id_0;
	// System.Single npcAICfg::cdtime
	float ___cdtime_1;
	// System.Int32 npcAICfg::count
	int32_t ___count_2;
	// System.String npcAICfg::condition
	String_t* ___condition_3;
	// System.String npcAICfg::behavior
	String_t* ___behavior_4;
	// System.Int32 npcAICfg::cdstarttime
	int32_t ___cdstarttime_5;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(npcAICfg_t1948330203, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_cdtime_1() { return static_cast<int32_t>(offsetof(npcAICfg_t1948330203, ___cdtime_1)); }
	inline float get_cdtime_1() const { return ___cdtime_1; }
	inline float* get_address_of_cdtime_1() { return &___cdtime_1; }
	inline void set_cdtime_1(float value)
	{
		___cdtime_1 = value;
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(npcAICfg_t1948330203, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_condition_3() { return static_cast<int32_t>(offsetof(npcAICfg_t1948330203, ___condition_3)); }
	inline String_t* get_condition_3() const { return ___condition_3; }
	inline String_t** get_address_of_condition_3() { return &___condition_3; }
	inline void set_condition_3(String_t* value)
	{
		___condition_3 = value;
		Il2CppCodeGenWriteBarrier(&___condition_3, value);
	}

	inline static int32_t get_offset_of_behavior_4() { return static_cast<int32_t>(offsetof(npcAICfg_t1948330203, ___behavior_4)); }
	inline String_t* get_behavior_4() const { return ___behavior_4; }
	inline String_t** get_address_of_behavior_4() { return &___behavior_4; }
	inline void set_behavior_4(String_t* value)
	{
		___behavior_4 = value;
		Il2CppCodeGenWriteBarrier(&___behavior_4, value);
	}

	inline static int32_t get_offset_of_cdstarttime_5() { return static_cast<int32_t>(offsetof(npcAICfg_t1948330203, ___cdstarttime_5)); }
	inline int32_t get_cdstarttime_5() const { return ___cdstarttime_5; }
	inline int32_t* get_address_of_cdstarttime_5() { return &___cdstarttime_5; }
	inline void set_cdstarttime_5(int32_t value)
	{
		___cdstarttime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
