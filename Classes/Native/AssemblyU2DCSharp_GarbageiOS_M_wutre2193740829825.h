﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_wutre219
struct  M_wutre219_t3740829825  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_wutre219::_souceejereHelsta
	float ____souceejereHelsta_0;
	// System.String GarbageiOS.M_wutre219::_kotouKaras
	String_t* ____kotouKaras_1;
	// System.String GarbageiOS.M_wutre219::_totrahisRorstemhir
	String_t* ____totrahisRorstemhir_2;
	// System.Int32 GarbageiOS.M_wutre219::_reqearDarfaiko
	int32_t ____reqearDarfaiko_3;
	// System.Boolean GarbageiOS.M_wutre219::_mairnopa
	bool ____mairnopa_4;
	// System.Single GarbageiOS.M_wutre219::_guciNekooqar
	float ____guciNekooqar_5;
	// System.UInt32 GarbageiOS.M_wutre219::_zallnasYaystisxer
	uint32_t ____zallnasYaystisxer_6;
	// System.Single GarbageiOS.M_wutre219::_qerhairfaDuzibe
	float ____qerhairfaDuzibe_7;
	// System.UInt32 GarbageiOS.M_wutre219::_jealare
	uint32_t ____jealare_8;
	// System.Int32 GarbageiOS.M_wutre219::_seadearkaw
	int32_t ____seadearkaw_9;
	// System.UInt32 GarbageiOS.M_wutre219::_neci
	uint32_t ____neci_10;
	// System.Single GarbageiOS.M_wutre219::_rirarSadis
	float ____rirarSadis_11;
	// System.String GarbageiOS.M_wutre219::_bepea
	String_t* ____bepea_12;
	// System.UInt32 GarbageiOS.M_wutre219::_jemu
	uint32_t ____jemu_13;
	// System.Single GarbageiOS.M_wutre219::_firdemCearhow
	float ____firdemCearhow_14;
	// System.UInt32 GarbageiOS.M_wutre219::_zaraspaySirmem
	uint32_t ____zaraspaySirmem_15;
	// System.Single GarbageiOS.M_wutre219::_cemiTasyi
	float ____cemiTasyi_16;
	// System.String GarbageiOS.M_wutre219::_xeecelRissair
	String_t* ____xeecelRissair_17;
	// System.Int32 GarbageiOS.M_wutre219::_coustal
	int32_t ____coustal_18;
	// System.Int32 GarbageiOS.M_wutre219::_rairdreeDila
	int32_t ____rairdreeDila_19;
	// System.Boolean GarbageiOS.M_wutre219::_pouhihow
	bool ____pouhihow_20;
	// System.Single GarbageiOS.M_wutre219::_sowsear
	float ____sowsear_21;
	// System.Boolean GarbageiOS.M_wutre219::_howfeStaswhou
	bool ____howfeStaswhou_22;
	// System.Boolean GarbageiOS.M_wutre219::_qircheewa
	bool ____qircheewa_23;

public:
	inline static int32_t get_offset_of__souceejereHelsta_0() { return static_cast<int32_t>(offsetof(M_wutre219_t3740829825, ____souceejereHelsta_0)); }
	inline float get__souceejereHelsta_0() const { return ____souceejereHelsta_0; }
	inline float* get_address_of__souceejereHelsta_0() { return &____souceejereHelsta_0; }
	inline void set__souceejereHelsta_0(float value)
	{
		____souceejereHelsta_0 = value;
	}

	inline static int32_t get_offset_of__kotouKaras_1() { return static_cast<int32_t>(offsetof(M_wutre219_t3740829825, ____kotouKaras_1)); }
	inline String_t* get__kotouKaras_1() const { return ____kotouKaras_1; }
	inline String_t** get_address_of__kotouKaras_1() { return &____kotouKaras_1; }
	inline void set__kotouKaras_1(String_t* value)
	{
		____kotouKaras_1 = value;
		Il2CppCodeGenWriteBarrier(&____kotouKaras_1, value);
	}

	inline static int32_t get_offset_of__totrahisRorstemhir_2() { return static_cast<int32_t>(offsetof(M_wutre219_t3740829825, ____totrahisRorstemhir_2)); }
	inline String_t* get__totrahisRorstemhir_2() const { return ____totrahisRorstemhir_2; }
	inline String_t** get_address_of__totrahisRorstemhir_2() { return &____totrahisRorstemhir_2; }
	inline void set__totrahisRorstemhir_2(String_t* value)
	{
		____totrahisRorstemhir_2 = value;
		Il2CppCodeGenWriteBarrier(&____totrahisRorstemhir_2, value);
	}

	inline static int32_t get_offset_of__reqearDarfaiko_3() { return static_cast<int32_t>(offsetof(M_wutre219_t3740829825, ____reqearDarfaiko_3)); }
	inline int32_t get__reqearDarfaiko_3() const { return ____reqearDarfaiko_3; }
	inline int32_t* get_address_of__reqearDarfaiko_3() { return &____reqearDarfaiko_3; }
	inline void set__reqearDarfaiko_3(int32_t value)
	{
		____reqearDarfaiko_3 = value;
	}

	inline static int32_t get_offset_of__mairnopa_4() { return static_cast<int32_t>(offsetof(M_wutre219_t3740829825, ____mairnopa_4)); }
	inline bool get__mairnopa_4() const { return ____mairnopa_4; }
	inline bool* get_address_of__mairnopa_4() { return &____mairnopa_4; }
	inline void set__mairnopa_4(bool value)
	{
		____mairnopa_4 = value;
	}

	inline static int32_t get_offset_of__guciNekooqar_5() { return static_cast<int32_t>(offsetof(M_wutre219_t3740829825, ____guciNekooqar_5)); }
	inline float get__guciNekooqar_5() const { return ____guciNekooqar_5; }
	inline float* get_address_of__guciNekooqar_5() { return &____guciNekooqar_5; }
	inline void set__guciNekooqar_5(float value)
	{
		____guciNekooqar_5 = value;
	}

	inline static int32_t get_offset_of__zallnasYaystisxer_6() { return static_cast<int32_t>(offsetof(M_wutre219_t3740829825, ____zallnasYaystisxer_6)); }
	inline uint32_t get__zallnasYaystisxer_6() const { return ____zallnasYaystisxer_6; }
	inline uint32_t* get_address_of__zallnasYaystisxer_6() { return &____zallnasYaystisxer_6; }
	inline void set__zallnasYaystisxer_6(uint32_t value)
	{
		____zallnasYaystisxer_6 = value;
	}

	inline static int32_t get_offset_of__qerhairfaDuzibe_7() { return static_cast<int32_t>(offsetof(M_wutre219_t3740829825, ____qerhairfaDuzibe_7)); }
	inline float get__qerhairfaDuzibe_7() const { return ____qerhairfaDuzibe_7; }
	inline float* get_address_of__qerhairfaDuzibe_7() { return &____qerhairfaDuzibe_7; }
	inline void set__qerhairfaDuzibe_7(float value)
	{
		____qerhairfaDuzibe_7 = value;
	}

	inline static int32_t get_offset_of__jealare_8() { return static_cast<int32_t>(offsetof(M_wutre219_t3740829825, ____jealare_8)); }
	inline uint32_t get__jealare_8() const { return ____jealare_8; }
	inline uint32_t* get_address_of__jealare_8() { return &____jealare_8; }
	inline void set__jealare_8(uint32_t value)
	{
		____jealare_8 = value;
	}

	inline static int32_t get_offset_of__seadearkaw_9() { return static_cast<int32_t>(offsetof(M_wutre219_t3740829825, ____seadearkaw_9)); }
	inline int32_t get__seadearkaw_9() const { return ____seadearkaw_9; }
	inline int32_t* get_address_of__seadearkaw_9() { return &____seadearkaw_9; }
	inline void set__seadearkaw_9(int32_t value)
	{
		____seadearkaw_9 = value;
	}

	inline static int32_t get_offset_of__neci_10() { return static_cast<int32_t>(offsetof(M_wutre219_t3740829825, ____neci_10)); }
	inline uint32_t get__neci_10() const { return ____neci_10; }
	inline uint32_t* get_address_of__neci_10() { return &____neci_10; }
	inline void set__neci_10(uint32_t value)
	{
		____neci_10 = value;
	}

	inline static int32_t get_offset_of__rirarSadis_11() { return static_cast<int32_t>(offsetof(M_wutre219_t3740829825, ____rirarSadis_11)); }
	inline float get__rirarSadis_11() const { return ____rirarSadis_11; }
	inline float* get_address_of__rirarSadis_11() { return &____rirarSadis_11; }
	inline void set__rirarSadis_11(float value)
	{
		____rirarSadis_11 = value;
	}

	inline static int32_t get_offset_of__bepea_12() { return static_cast<int32_t>(offsetof(M_wutre219_t3740829825, ____bepea_12)); }
	inline String_t* get__bepea_12() const { return ____bepea_12; }
	inline String_t** get_address_of__bepea_12() { return &____bepea_12; }
	inline void set__bepea_12(String_t* value)
	{
		____bepea_12 = value;
		Il2CppCodeGenWriteBarrier(&____bepea_12, value);
	}

	inline static int32_t get_offset_of__jemu_13() { return static_cast<int32_t>(offsetof(M_wutre219_t3740829825, ____jemu_13)); }
	inline uint32_t get__jemu_13() const { return ____jemu_13; }
	inline uint32_t* get_address_of__jemu_13() { return &____jemu_13; }
	inline void set__jemu_13(uint32_t value)
	{
		____jemu_13 = value;
	}

	inline static int32_t get_offset_of__firdemCearhow_14() { return static_cast<int32_t>(offsetof(M_wutre219_t3740829825, ____firdemCearhow_14)); }
	inline float get__firdemCearhow_14() const { return ____firdemCearhow_14; }
	inline float* get_address_of__firdemCearhow_14() { return &____firdemCearhow_14; }
	inline void set__firdemCearhow_14(float value)
	{
		____firdemCearhow_14 = value;
	}

	inline static int32_t get_offset_of__zaraspaySirmem_15() { return static_cast<int32_t>(offsetof(M_wutre219_t3740829825, ____zaraspaySirmem_15)); }
	inline uint32_t get__zaraspaySirmem_15() const { return ____zaraspaySirmem_15; }
	inline uint32_t* get_address_of__zaraspaySirmem_15() { return &____zaraspaySirmem_15; }
	inline void set__zaraspaySirmem_15(uint32_t value)
	{
		____zaraspaySirmem_15 = value;
	}

	inline static int32_t get_offset_of__cemiTasyi_16() { return static_cast<int32_t>(offsetof(M_wutre219_t3740829825, ____cemiTasyi_16)); }
	inline float get__cemiTasyi_16() const { return ____cemiTasyi_16; }
	inline float* get_address_of__cemiTasyi_16() { return &____cemiTasyi_16; }
	inline void set__cemiTasyi_16(float value)
	{
		____cemiTasyi_16 = value;
	}

	inline static int32_t get_offset_of__xeecelRissair_17() { return static_cast<int32_t>(offsetof(M_wutre219_t3740829825, ____xeecelRissair_17)); }
	inline String_t* get__xeecelRissair_17() const { return ____xeecelRissair_17; }
	inline String_t** get_address_of__xeecelRissair_17() { return &____xeecelRissair_17; }
	inline void set__xeecelRissair_17(String_t* value)
	{
		____xeecelRissair_17 = value;
		Il2CppCodeGenWriteBarrier(&____xeecelRissair_17, value);
	}

	inline static int32_t get_offset_of__coustal_18() { return static_cast<int32_t>(offsetof(M_wutre219_t3740829825, ____coustal_18)); }
	inline int32_t get__coustal_18() const { return ____coustal_18; }
	inline int32_t* get_address_of__coustal_18() { return &____coustal_18; }
	inline void set__coustal_18(int32_t value)
	{
		____coustal_18 = value;
	}

	inline static int32_t get_offset_of__rairdreeDila_19() { return static_cast<int32_t>(offsetof(M_wutre219_t3740829825, ____rairdreeDila_19)); }
	inline int32_t get__rairdreeDila_19() const { return ____rairdreeDila_19; }
	inline int32_t* get_address_of__rairdreeDila_19() { return &____rairdreeDila_19; }
	inline void set__rairdreeDila_19(int32_t value)
	{
		____rairdreeDila_19 = value;
	}

	inline static int32_t get_offset_of__pouhihow_20() { return static_cast<int32_t>(offsetof(M_wutre219_t3740829825, ____pouhihow_20)); }
	inline bool get__pouhihow_20() const { return ____pouhihow_20; }
	inline bool* get_address_of__pouhihow_20() { return &____pouhihow_20; }
	inline void set__pouhihow_20(bool value)
	{
		____pouhihow_20 = value;
	}

	inline static int32_t get_offset_of__sowsear_21() { return static_cast<int32_t>(offsetof(M_wutre219_t3740829825, ____sowsear_21)); }
	inline float get__sowsear_21() const { return ____sowsear_21; }
	inline float* get_address_of__sowsear_21() { return &____sowsear_21; }
	inline void set__sowsear_21(float value)
	{
		____sowsear_21 = value;
	}

	inline static int32_t get_offset_of__howfeStaswhou_22() { return static_cast<int32_t>(offsetof(M_wutre219_t3740829825, ____howfeStaswhou_22)); }
	inline bool get__howfeStaswhou_22() const { return ____howfeStaswhou_22; }
	inline bool* get_address_of__howfeStaswhou_22() { return &____howfeStaswhou_22; }
	inline void set__howfeStaswhou_22(bool value)
	{
		____howfeStaswhou_22 = value;
	}

	inline static int32_t get_offset_of__qircheewa_23() { return static_cast<int32_t>(offsetof(M_wutre219_t3740829825, ____qircheewa_23)); }
	inline bool get__qircheewa_23() const { return ____qircheewa_23; }
	inline bool* get_address_of__qircheewa_23() { return &____qircheewa_23; }
	inline void set__qircheewa_23(bool value)
	{
		____qircheewa_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
