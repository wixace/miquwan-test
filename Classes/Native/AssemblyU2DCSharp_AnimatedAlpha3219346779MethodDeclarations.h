﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimatedAlpha
struct AnimatedAlpha_t3219346779;
// UIPanel
struct UIPanel_t295209936;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIPanel295209936.h"

// System.Void AnimatedAlpha::.ctor()
extern "C"  void AnimatedAlpha__ctor_m2397168240 (AnimatedAlpha_t3219346779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatedAlpha::OnEnable()
extern "C"  void AnimatedAlpha_OnEnable_m3689385750 (AnimatedAlpha_t3219346779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatedAlpha::LateUpdate()
extern "C"  void AnimatedAlpha_LateUpdate_m1961848291 (AnimatedAlpha_t3219346779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatedAlpha::ilo_set_alpha1(UIPanel,System.Single)
extern "C"  void AnimatedAlpha_ilo_set_alpha1_m899691184 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
