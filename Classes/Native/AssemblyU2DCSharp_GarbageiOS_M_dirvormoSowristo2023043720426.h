﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_dirvormoSowristo202
struct  M_dirvormoSowristo202_t3043720426  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_dirvormoSowristo202::_telo
	String_t* ____telo_0;
	// System.Boolean GarbageiOS.M_dirvormoSowristo202::_trucooMezalltair
	bool ____trucooMezalltair_1;
	// System.Boolean GarbageiOS.M_dirvormoSowristo202::_nizorsi
	bool ____nizorsi_2;
	// System.String GarbageiOS.M_dirvormoSowristo202::_tremfea
	String_t* ____tremfea_3;
	// System.UInt32 GarbageiOS.M_dirvormoSowristo202::_rerekale
	uint32_t ____rerekale_4;
	// System.Boolean GarbageiOS.M_dirvormoSowristo202::_drahearJesal
	bool ____drahearJesal_5;
	// System.UInt32 GarbageiOS.M_dirvormoSowristo202::_salwhallserKehea
	uint32_t ____salwhallserKehea_6;
	// System.Single GarbageiOS.M_dirvormoSowristo202::_wisasZearca
	float ____wisasZearca_7;
	// System.String GarbageiOS.M_dirvormoSowristo202::_ditearbayMadrirdi
	String_t* ____ditearbayMadrirdi_8;
	// System.UInt32 GarbageiOS.M_dirvormoSowristo202::_cearrouMalljearxel
	uint32_t ____cearrouMalljearxel_9;
	// System.Int32 GarbageiOS.M_dirvormoSowristo202::_cefi
	int32_t ____cefi_10;
	// System.Single GarbageiOS.M_dirvormoSowristo202::_ralpasemBonea
	float ____ralpasemBonea_11;
	// System.String GarbageiOS.M_dirvormoSowristo202::_staidemror
	String_t* ____staidemror_12;
	// System.String GarbageiOS.M_dirvormoSowristo202::_weaqeltrirDerese
	String_t* ____weaqeltrirDerese_13;
	// System.UInt32 GarbageiOS.M_dirvormoSowristo202::_hallbaifee
	uint32_t ____hallbaifee_14;
	// System.UInt32 GarbageiOS.M_dirvormoSowristo202::_kaile
	uint32_t ____kaile_15;
	// System.Boolean GarbageiOS.M_dirvormoSowristo202::_rece
	bool ____rece_16;
	// System.Single GarbageiOS.M_dirvormoSowristo202::_roowhousairParburdair
	float ____roowhousairParburdair_17;
	// System.Boolean GarbageiOS.M_dirvormoSowristo202::_kowse
	bool ____kowse_18;
	// System.Single GarbageiOS.M_dirvormoSowristo202::_qeekirQemzir
	float ____qeekirQemzir_19;
	// System.Boolean GarbageiOS.M_dirvormoSowristo202::_teasta
	bool ____teasta_20;
	// System.UInt32 GarbageiOS.M_dirvormoSowristo202::_jislorziJarjurdaw
	uint32_t ____jislorziJarjurdaw_21;

public:
	inline static int32_t get_offset_of__telo_0() { return static_cast<int32_t>(offsetof(M_dirvormoSowristo202_t3043720426, ____telo_0)); }
	inline String_t* get__telo_0() const { return ____telo_0; }
	inline String_t** get_address_of__telo_0() { return &____telo_0; }
	inline void set__telo_0(String_t* value)
	{
		____telo_0 = value;
		Il2CppCodeGenWriteBarrier(&____telo_0, value);
	}

	inline static int32_t get_offset_of__trucooMezalltair_1() { return static_cast<int32_t>(offsetof(M_dirvormoSowristo202_t3043720426, ____trucooMezalltair_1)); }
	inline bool get__trucooMezalltair_1() const { return ____trucooMezalltair_1; }
	inline bool* get_address_of__trucooMezalltair_1() { return &____trucooMezalltair_1; }
	inline void set__trucooMezalltair_1(bool value)
	{
		____trucooMezalltair_1 = value;
	}

	inline static int32_t get_offset_of__nizorsi_2() { return static_cast<int32_t>(offsetof(M_dirvormoSowristo202_t3043720426, ____nizorsi_2)); }
	inline bool get__nizorsi_2() const { return ____nizorsi_2; }
	inline bool* get_address_of__nizorsi_2() { return &____nizorsi_2; }
	inline void set__nizorsi_2(bool value)
	{
		____nizorsi_2 = value;
	}

	inline static int32_t get_offset_of__tremfea_3() { return static_cast<int32_t>(offsetof(M_dirvormoSowristo202_t3043720426, ____tremfea_3)); }
	inline String_t* get__tremfea_3() const { return ____tremfea_3; }
	inline String_t** get_address_of__tremfea_3() { return &____tremfea_3; }
	inline void set__tremfea_3(String_t* value)
	{
		____tremfea_3 = value;
		Il2CppCodeGenWriteBarrier(&____tremfea_3, value);
	}

	inline static int32_t get_offset_of__rerekale_4() { return static_cast<int32_t>(offsetof(M_dirvormoSowristo202_t3043720426, ____rerekale_4)); }
	inline uint32_t get__rerekale_4() const { return ____rerekale_4; }
	inline uint32_t* get_address_of__rerekale_4() { return &____rerekale_4; }
	inline void set__rerekale_4(uint32_t value)
	{
		____rerekale_4 = value;
	}

	inline static int32_t get_offset_of__drahearJesal_5() { return static_cast<int32_t>(offsetof(M_dirvormoSowristo202_t3043720426, ____drahearJesal_5)); }
	inline bool get__drahearJesal_5() const { return ____drahearJesal_5; }
	inline bool* get_address_of__drahearJesal_5() { return &____drahearJesal_5; }
	inline void set__drahearJesal_5(bool value)
	{
		____drahearJesal_5 = value;
	}

	inline static int32_t get_offset_of__salwhallserKehea_6() { return static_cast<int32_t>(offsetof(M_dirvormoSowristo202_t3043720426, ____salwhallserKehea_6)); }
	inline uint32_t get__salwhallserKehea_6() const { return ____salwhallserKehea_6; }
	inline uint32_t* get_address_of__salwhallserKehea_6() { return &____salwhallserKehea_6; }
	inline void set__salwhallserKehea_6(uint32_t value)
	{
		____salwhallserKehea_6 = value;
	}

	inline static int32_t get_offset_of__wisasZearca_7() { return static_cast<int32_t>(offsetof(M_dirvormoSowristo202_t3043720426, ____wisasZearca_7)); }
	inline float get__wisasZearca_7() const { return ____wisasZearca_7; }
	inline float* get_address_of__wisasZearca_7() { return &____wisasZearca_7; }
	inline void set__wisasZearca_7(float value)
	{
		____wisasZearca_7 = value;
	}

	inline static int32_t get_offset_of__ditearbayMadrirdi_8() { return static_cast<int32_t>(offsetof(M_dirvormoSowristo202_t3043720426, ____ditearbayMadrirdi_8)); }
	inline String_t* get__ditearbayMadrirdi_8() const { return ____ditearbayMadrirdi_8; }
	inline String_t** get_address_of__ditearbayMadrirdi_8() { return &____ditearbayMadrirdi_8; }
	inline void set__ditearbayMadrirdi_8(String_t* value)
	{
		____ditearbayMadrirdi_8 = value;
		Il2CppCodeGenWriteBarrier(&____ditearbayMadrirdi_8, value);
	}

	inline static int32_t get_offset_of__cearrouMalljearxel_9() { return static_cast<int32_t>(offsetof(M_dirvormoSowristo202_t3043720426, ____cearrouMalljearxel_9)); }
	inline uint32_t get__cearrouMalljearxel_9() const { return ____cearrouMalljearxel_9; }
	inline uint32_t* get_address_of__cearrouMalljearxel_9() { return &____cearrouMalljearxel_9; }
	inline void set__cearrouMalljearxel_9(uint32_t value)
	{
		____cearrouMalljearxel_9 = value;
	}

	inline static int32_t get_offset_of__cefi_10() { return static_cast<int32_t>(offsetof(M_dirvormoSowristo202_t3043720426, ____cefi_10)); }
	inline int32_t get__cefi_10() const { return ____cefi_10; }
	inline int32_t* get_address_of__cefi_10() { return &____cefi_10; }
	inline void set__cefi_10(int32_t value)
	{
		____cefi_10 = value;
	}

	inline static int32_t get_offset_of__ralpasemBonea_11() { return static_cast<int32_t>(offsetof(M_dirvormoSowristo202_t3043720426, ____ralpasemBonea_11)); }
	inline float get__ralpasemBonea_11() const { return ____ralpasemBonea_11; }
	inline float* get_address_of__ralpasemBonea_11() { return &____ralpasemBonea_11; }
	inline void set__ralpasemBonea_11(float value)
	{
		____ralpasemBonea_11 = value;
	}

	inline static int32_t get_offset_of__staidemror_12() { return static_cast<int32_t>(offsetof(M_dirvormoSowristo202_t3043720426, ____staidemror_12)); }
	inline String_t* get__staidemror_12() const { return ____staidemror_12; }
	inline String_t** get_address_of__staidemror_12() { return &____staidemror_12; }
	inline void set__staidemror_12(String_t* value)
	{
		____staidemror_12 = value;
		Il2CppCodeGenWriteBarrier(&____staidemror_12, value);
	}

	inline static int32_t get_offset_of__weaqeltrirDerese_13() { return static_cast<int32_t>(offsetof(M_dirvormoSowristo202_t3043720426, ____weaqeltrirDerese_13)); }
	inline String_t* get__weaqeltrirDerese_13() const { return ____weaqeltrirDerese_13; }
	inline String_t** get_address_of__weaqeltrirDerese_13() { return &____weaqeltrirDerese_13; }
	inline void set__weaqeltrirDerese_13(String_t* value)
	{
		____weaqeltrirDerese_13 = value;
		Il2CppCodeGenWriteBarrier(&____weaqeltrirDerese_13, value);
	}

	inline static int32_t get_offset_of__hallbaifee_14() { return static_cast<int32_t>(offsetof(M_dirvormoSowristo202_t3043720426, ____hallbaifee_14)); }
	inline uint32_t get__hallbaifee_14() const { return ____hallbaifee_14; }
	inline uint32_t* get_address_of__hallbaifee_14() { return &____hallbaifee_14; }
	inline void set__hallbaifee_14(uint32_t value)
	{
		____hallbaifee_14 = value;
	}

	inline static int32_t get_offset_of__kaile_15() { return static_cast<int32_t>(offsetof(M_dirvormoSowristo202_t3043720426, ____kaile_15)); }
	inline uint32_t get__kaile_15() const { return ____kaile_15; }
	inline uint32_t* get_address_of__kaile_15() { return &____kaile_15; }
	inline void set__kaile_15(uint32_t value)
	{
		____kaile_15 = value;
	}

	inline static int32_t get_offset_of__rece_16() { return static_cast<int32_t>(offsetof(M_dirvormoSowristo202_t3043720426, ____rece_16)); }
	inline bool get__rece_16() const { return ____rece_16; }
	inline bool* get_address_of__rece_16() { return &____rece_16; }
	inline void set__rece_16(bool value)
	{
		____rece_16 = value;
	}

	inline static int32_t get_offset_of__roowhousairParburdair_17() { return static_cast<int32_t>(offsetof(M_dirvormoSowristo202_t3043720426, ____roowhousairParburdair_17)); }
	inline float get__roowhousairParburdair_17() const { return ____roowhousairParburdair_17; }
	inline float* get_address_of__roowhousairParburdair_17() { return &____roowhousairParburdair_17; }
	inline void set__roowhousairParburdair_17(float value)
	{
		____roowhousairParburdair_17 = value;
	}

	inline static int32_t get_offset_of__kowse_18() { return static_cast<int32_t>(offsetof(M_dirvormoSowristo202_t3043720426, ____kowse_18)); }
	inline bool get__kowse_18() const { return ____kowse_18; }
	inline bool* get_address_of__kowse_18() { return &____kowse_18; }
	inline void set__kowse_18(bool value)
	{
		____kowse_18 = value;
	}

	inline static int32_t get_offset_of__qeekirQemzir_19() { return static_cast<int32_t>(offsetof(M_dirvormoSowristo202_t3043720426, ____qeekirQemzir_19)); }
	inline float get__qeekirQemzir_19() const { return ____qeekirQemzir_19; }
	inline float* get_address_of__qeekirQemzir_19() { return &____qeekirQemzir_19; }
	inline void set__qeekirQemzir_19(float value)
	{
		____qeekirQemzir_19 = value;
	}

	inline static int32_t get_offset_of__teasta_20() { return static_cast<int32_t>(offsetof(M_dirvormoSowristo202_t3043720426, ____teasta_20)); }
	inline bool get__teasta_20() const { return ____teasta_20; }
	inline bool* get_address_of__teasta_20() { return &____teasta_20; }
	inline void set__teasta_20(bool value)
	{
		____teasta_20 = value;
	}

	inline static int32_t get_offset_of__jislorziJarjurdaw_21() { return static_cast<int32_t>(offsetof(M_dirvormoSowristo202_t3043720426, ____jislorziJarjurdaw_21)); }
	inline uint32_t get__jislorziJarjurdaw_21() const { return ____jislorziJarjurdaw_21; }
	inline uint32_t* get_address_of__jislorziJarjurdaw_21() { return &____jislorziJarjurdaw_21; }
	inline void set__jislorziJarjurdaw_21(uint32_t value)
	{
		____jislorziJarjurdaw_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
