﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSCheckFinishDataGenerated
struct CSCheckFinishDataGenerated_t833488282;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void CSCheckFinishDataGenerated::.ctor()
extern "C"  void CSCheckFinishDataGenerated__ctor_m487460929 (CSCheckFinishDataGenerated_t833488282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSCheckFinishDataGenerated::CSCheckFinishData_CSCheckFinishData1(JSVCall,System.Int32)
extern "C"  bool CSCheckFinishDataGenerated_CSCheckFinishData_CSCheckFinishData1_m3752500077 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCheckFinishDataGenerated::CSCheckFinishData_stars(JSVCall)
extern "C"  void CSCheckFinishDataGenerated_CSCheckFinishData_stars_m1731429777 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCheckFinishDataGenerated::CSCheckFinishData_starStatus(JSVCall)
extern "C"  void CSCheckFinishDataGenerated_CSCheckFinishData_starStatus_m2000363862 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCheckFinishDataGenerated::CSCheckFinishData_bossHp(JSVCall)
extern "C"  void CSCheckFinishDataGenerated_CSCheckFinishData_bossHp_m3314051589 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCheckFinishDataGenerated::CSCheckFinishData_bossHpPer(JSVCall)
extern "C"  void CSCheckFinishDataGenerated_CSCheckFinishData_bossHpPer_m3238182986 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCheckFinishDataGenerated::__Register()
extern "C"  void CSCheckFinishDataGenerated___Register_m4257770790 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCheckFinishDataGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void CSCheckFinishDataGenerated_ilo_addJSCSRel1_m4134689405 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSCheckFinishDataGenerated::ilo_getUInt322(System.Int32)
extern "C"  uint32_t CSCheckFinishDataGenerated_ilo_getUInt322_m3610797605 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCheckFinishDataGenerated::ilo_setUInt323(System.Int32,System.UInt32)
extern "C"  void CSCheckFinishDataGenerated_ilo_setUInt323_m1614021555 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
