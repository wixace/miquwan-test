﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_tearjarorZehi155
struct M_tearjarorZehi155_t1280640825;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_tearjarorZehi1551280640825.h"

// System.Void GarbageiOS.M_tearjarorZehi155::.ctor()
extern "C"  void M_tearjarorZehi155__ctor_m2504099610 (M_tearjarorZehi155_t1280640825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_tearjarorZehi155::M_steabo0(System.String[],System.Int32)
extern "C"  void M_tearjarorZehi155_M_steabo0_m2691918019 (M_tearjarorZehi155_t1280640825 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_tearjarorZehi155::M_kageForboo1(System.String[],System.Int32)
extern "C"  void M_tearjarorZehi155_M_kageForboo1_m2537455553 (M_tearjarorZehi155_t1280640825 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_tearjarorZehi155::M_norkaKorse2(System.String[],System.Int32)
extern "C"  void M_tearjarorZehi155_M_norkaKorse2_m4251129334 (M_tearjarorZehi155_t1280640825 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_tearjarorZehi155::M_waircoofeLaytorso3(System.String[],System.Int32)
extern "C"  void M_tearjarorZehi155_M_waircoofeLaytorso3_m3581307912 (M_tearjarorZehi155_t1280640825 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_tearjarorZehi155::M_cesir4(System.String[],System.Int32)
extern "C"  void M_tearjarorZehi155_M_cesir4_m2884559853 (M_tearjarorZehi155_t1280640825 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_tearjarorZehi155::ilo_M_steabo01(GarbageiOS.M_tearjarorZehi155,System.String[],System.Int32)
extern "C"  void M_tearjarorZehi155_ilo_M_steabo01_m190005878 (Il2CppObject * __this /* static, unused */, M_tearjarorZehi155_t1280640825 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_tearjarorZehi155::ilo_M_kageForboo12(GarbageiOS.M_tearjarorZehi155,System.String[],System.Int32)
extern "C"  void M_tearjarorZehi155_ilo_M_kageForboo12_m3493034681 (Il2CppObject * __this /* static, unused */, M_tearjarorZehi155_t1280640825 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_tearjarorZehi155::ilo_M_norkaKorse23(GarbageiOS.M_tearjarorZehi155,System.String[],System.Int32)
extern "C"  void M_tearjarorZehi155_ilo_M_norkaKorse23_m3614375525 (Il2CppObject * __this /* static, unused */, M_tearjarorZehi155_t1280640825 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_tearjarorZehi155::ilo_M_waircoofeLaytorso34(GarbageiOS.M_tearjarorZehi155,System.String[],System.Int32)
extern "C"  void M_tearjarorZehi155_ilo_M_waircoofeLaytorso34_m3130381134 (Il2CppObject * __this /* static, unused */, M_tearjarorZehi155_t1280640825 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
