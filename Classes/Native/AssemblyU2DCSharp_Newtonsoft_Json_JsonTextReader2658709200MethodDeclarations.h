﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonTextReader
struct JsonTextReader_t2658709200;
// System.IO.TextReader
struct TextReader_t2148718976;
// System.Globalization.CultureInfo
struct CultureInfo_t1065375142;
// Newtonsoft.Json.JsonReaderException
struct JsonReaderException_t354140082;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// Newtonsoft.Json.Utilities.StringBuffer
struct StringBuffer_t1402275585;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_TextReader2148718976.h"
#include "mscorlib_System_Globalization_CultureInfo1065375142.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "mscorlib_System_Nullable_1_gen2038477154.h"
#include "mscorlib_System_Nullable_1_gen3968840829.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_String1402275585.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader816925123.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonTextReader2658709200.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonToken4173078175.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.JsonTextReader::.ctor(System.IO.TextReader)
extern "C"  void JsonTextReader__ctor_m1734909141 (JsonTextReader_t2658709200 * __this, TextReader_t2148718976 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo Newtonsoft.Json.JsonTextReader::get_Culture()
extern "C"  CultureInfo_t1065375142 * JsonTextReader_get_Culture_m648108836 (JsonTextReader_t2658709200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::set_Culture(System.Globalization.CultureInfo)
extern "C"  void JsonTextReader_set_Culture_m3713973037 (JsonTextReader_t2658709200 * __this, CultureInfo_t1065375142 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ParseString(System.Char)
extern "C"  void JsonTextReader_ParseString_m1781003597 (JsonTextReader_t2658709200 * __this, Il2CppChar ___quote0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ReadStringIntoBuffer(System.Char)
extern "C"  void JsonTextReader_ReadStringIntoBuffer_m2009816736 (JsonTextReader_t2658709200 * __this, Il2CppChar ___quote0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonReaderException Newtonsoft.Json.JsonTextReader::CreateJsonReaderException(System.String,System.Object[])
extern "C"  JsonReaderException_t354140082 * JsonTextReader_CreateJsonReaderException_m4127681662 (JsonTextReader_t2658709200 * __this, String_t* ___format0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan Newtonsoft.Json.JsonTextReader::ReadOffset(System.String)
extern "C"  TimeSpan_t413522987  JsonTextReader_ReadOffset_m1923131270 (JsonTextReader_t2658709200 * __this, String_t* ___offsetText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ParseDate(System.String)
extern "C"  void JsonTextReader_ParseDate_m2393404597 (JsonTextReader_t2658709200 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char Newtonsoft.Json.JsonTextReader::MoveNext()
extern "C"  Il2CppChar JsonTextReader_MoveNext_m729862232 (JsonTextReader_t2658709200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::HasNext()
extern "C"  bool JsonTextReader_HasNext_m3856665253 (JsonTextReader_t2658709200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.JsonTextReader::PeekNext()
extern "C"  int32_t JsonTextReader_PeekNext_m1914498546 (JsonTextReader_t2658709200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::Read()
extern "C"  bool JsonTextReader_Read_m2556382272 (JsonTextReader_t2658709200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Newtonsoft.Json.JsonTextReader::ReadAsBytes()
extern "C"  ByteU5BU5D_t4260760469* JsonTextReader_ReadAsBytes_m3558825317 (JsonTextReader_t2658709200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Decimal> Newtonsoft.Json.JsonTextReader::ReadAsDecimal()
extern "C"  Nullable_1_t2038477154  JsonTextReader_ReadAsDecimal_m1262009155 (JsonTextReader_t2658709200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTimeOffset> Newtonsoft.Json.JsonTextReader::ReadAsDateTimeOffset()
extern "C"  Nullable_1_t3968840829  JsonTextReader_ReadAsDateTimeOffset_m2554141901 (JsonTextReader_t2658709200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::ReadInternal()
extern "C"  bool JsonTextReader_ReadInternal_m1492813021 (JsonTextReader_t2658709200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::ParsePostValue(System.Char)
extern "C"  bool JsonTextReader_ParsePostValue_m3298799925 (JsonTextReader_t2658709200 * __this, Il2CppChar ___currentChar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::ParseObject(System.Char)
extern "C"  bool JsonTextReader_ParseObject_m3265400723 (JsonTextReader_t2658709200 * __this, Il2CppChar ___currentChar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::ParseProperty(System.Char)
extern "C"  bool JsonTextReader_ParseProperty_m824099645 (JsonTextReader_t2658709200 * __this, Il2CppChar ___firstChar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::ValidIdentifierChar(System.Char)
extern "C"  bool JsonTextReader_ValidIdentifierChar_m1586398794 (JsonTextReader_t2658709200 * __this, Il2CppChar ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char Newtonsoft.Json.JsonTextReader::ParseUnquotedProperty(System.Char)
extern "C"  Il2CppChar JsonTextReader_ParseUnquotedProperty_m904487878 (JsonTextReader_t2658709200 * __this, Il2CppChar ___firstChar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::ParseValue(System.Char)
extern "C"  bool JsonTextReader_ParseValue_m3650705781 (JsonTextReader_t2658709200 * __this, Il2CppChar ___currentChar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::EatWhitespace(System.Char,System.Boolean,System.Char&)
extern "C"  bool JsonTextReader_EatWhitespace_m269304264 (JsonTextReader_t2658709200 * __this, Il2CppChar ___initialChar0, bool ___oneOrMore1, Il2CppChar* ___finalChar2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ParseConstructor()
extern "C"  void JsonTextReader_ParseConstructor_m819741309 (JsonTextReader_t2658709200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ParseNumber(System.Char)
extern "C"  void JsonTextReader_ParseNumber_m4159206677 (JsonTextReader_t2658709200 * __this, Il2CppChar ___firstChar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ParseComment()
extern "C"  void JsonTextReader_ParseComment_m2538972322 (JsonTextReader_t2658709200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::MatchValue(System.Char,System.String)
extern "C"  bool JsonTextReader_MatchValue_m1036994563 (JsonTextReader_t2658709200 * __this, Il2CppChar ___firstChar0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::MatchValue(System.Char,System.String,System.Boolean)
extern "C"  bool JsonTextReader_MatchValue_m1085121818 (JsonTextReader_t2658709200 * __this, Il2CppChar ___firstChar0, String_t* ___value1, bool ___noTrailingNonSeperatorCharacters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::IsSeperator(System.Char)
extern "C"  bool JsonTextReader_IsSeperator_m3646701614 (JsonTextReader_t2658709200 * __this, Il2CppChar ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ParseTrue()
extern "C"  void JsonTextReader_ParseTrue_m2133750093 (JsonTextReader_t2658709200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ParseNull()
extern "C"  void JsonTextReader_ParseNull_m1964484358 (JsonTextReader_t2658709200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ParseUndefined()
extern "C"  void JsonTextReader_ParseUndefined_m1392436947 (JsonTextReader_t2658709200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ParseFalse()
extern "C"  void JsonTextReader_ParseFalse_m1687062246 (JsonTextReader_t2658709200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ParseNumberNegativeInfinity()
extern "C"  void JsonTextReader_ParseNumberNegativeInfinity_m111375653 (JsonTextReader_t2658709200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ParseNumberPositiveInfinity()
extern "C"  void JsonTextReader_ParseNumberPositiveInfinity_m404463081 (JsonTextReader_t2658709200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ParseNumberNaN()
extern "C"  void JsonTextReader_ParseNumberNaN_m3325348341 (JsonTextReader_t2658709200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::Close()
extern "C"  void JsonTextReader_Close_m2963275812 (JsonTextReader_t2658709200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::HasLineInfo()
extern "C"  bool JsonTextReader_HasLineInfo_m140645940 (JsonTextReader_t2658709200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.JsonTextReader::get_LineNumber()
extern "C"  int32_t JsonTextReader_get_LineNumber_m3459926954 (JsonTextReader_t2658709200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.JsonTextReader::get_LinePosition()
extern "C"  int32_t JsonTextReader_get_LinePosition_m2459707786 (JsonTextReader_t2658709200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.JsonTextReader::ilo_get_Position1(Newtonsoft.Json.Utilities.StringBuffer)
extern "C"  int32_t JsonTextReader_ilo_get_Position1_m1394577280 (Il2CppObject * __this /* static, unused */, StringBuffer_t1402275585 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonTextReader::ilo_ToString2(Newtonsoft.Json.Utilities.StringBuffer)
extern "C"  String_t* JsonTextReader_ilo_ToString2_m3767269714 (Il2CppObject * __this /* static, unused */, StringBuffer_t1402275585 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ilo_set_Position3(Newtonsoft.Json.Utilities.StringBuffer,System.Int32)
extern "C"  void JsonTextReader_ilo_set_Position3_m4013319291 (Il2CppObject * __this /* static, unused */, StringBuffer_t1402275585 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ilo_set_QuoteChar4(Newtonsoft.Json.JsonReader,System.Char)
extern "C"  void JsonTextReader_ilo_set_QuoteChar4_m136474975 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, Il2CppChar ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char Newtonsoft.Json.JsonTextReader::ilo_MoveNext5(Newtonsoft.Json.JsonTextReader)
extern "C"  Il2CppChar JsonTextReader_ilo_MoveNext5_m212704031 (Il2CppObject * __this /* static, unused */, JsonTextReader_t2658709200 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ilo_Append6(Newtonsoft.Json.Utilities.StringBuffer,System.Char)
extern "C"  void JsonTextReader_ilo_Append6_m359935682 (Il2CppObject * __this /* static, unused */, StringBuffer_t1402275585 * ____this0, Il2CppChar ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonReaderException Newtonsoft.Json.JsonTextReader::ilo_CreateJsonReaderException7(Newtonsoft.Json.JsonTextReader,System.String,System.Object[])
extern "C"  JsonReaderException_t354140082 * JsonTextReader_ilo_CreateJsonReaderException7_m3066147639 (Il2CppObject * __this /* static, unused */, JsonTextReader_t2658709200 * ____this0, String_t* ___format1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonTextReader::ilo_FormatWith8(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* JsonTextReader_ilo_FormatWith8_m3562366639 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Newtonsoft.Json.JsonTextReader::ilo_ConvertJavaScriptTicksToDateTime9(System.Int64)
extern "C"  DateTime_t4283661327  JsonTextReader_ilo_ConvertJavaScriptTicksToDateTime9_m1270139743 (Il2CppObject * __this /* static, unused */, int64_t ___javaScriptTicks0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ilo_SetToken10(Newtonsoft.Json.JsonReader,Newtonsoft.Json.JsonToken,System.Object)
extern "C"  void JsonTextReader_ilo_SetToken10_m1545069725 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, int32_t ___newToken1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonToken Newtonsoft.Json.JsonTextReader::ilo_get_TokenType11(Newtonsoft.Json.JsonReader)
extern "C"  int32_t JsonTextReader_ilo_get_TokenType11_m3473089292 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.JsonTextReader::ilo_get_Value12(Newtonsoft.Json.JsonReader)
extern "C"  Il2CppObject * JsonTextReader_ilo_get_Value12_m4182096909 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::ilo_ReadInternal13(Newtonsoft.Json.JsonTextReader)
extern "C"  bool JsonTextReader_ilo_ReadInternal13_m3402906341 (Il2CppObject * __this /* static, unused */, JsonTextReader_t2658709200 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo Newtonsoft.Json.JsonTextReader::ilo_get_Culture14(Newtonsoft.Json.JsonTextReader)
extern "C"  CultureInfo_t1065375142 * JsonTextReader_ilo_get_Culture14_m262351463 (Il2CppObject * __this /* static, unused */, JsonTextReader_t2658709200 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::ilo_ParseObject15(Newtonsoft.Json.JsonTextReader,System.Char)
extern "C"  bool JsonTextReader_ilo_ParseObject15_m1322700133 (Il2CppObject * __this /* static, unused */, JsonTextReader_t2658709200 * ____this0, Il2CppChar ___currentChar1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ilo_SetToken16(Newtonsoft.Json.JsonReader,Newtonsoft.Json.JsonToken)
extern "C"  void JsonTextReader_ilo_SetToken16_m3542638485 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, int32_t ___newToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::ilo_ParseProperty17(Newtonsoft.Json.JsonTextReader,System.Char)
extern "C"  bool JsonTextReader_ilo_ParseProperty17_m1568572605 (Il2CppObject * __this /* static, unused */, JsonTextReader_t2658709200 * ____this0, Il2CppChar ___firstChar1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char Newtonsoft.Json.JsonTextReader::ilo_ParseUnquotedProperty18(Newtonsoft.Json.JsonTextReader,System.Char)
extern "C"  Il2CppChar JsonTextReader_ilo_ParseUnquotedProperty18_m3742036949 (Il2CppObject * __this /* static, unused */, JsonTextReader_t2658709200 * ____this0, Il2CppChar ___firstChar1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::ilo_EatWhitespace19(Newtonsoft.Json.JsonTextReader,System.Char,System.Boolean,System.Char&)
extern "C"  bool JsonTextReader_ilo_EatWhitespace19_m5871364 (Il2CppObject * __this /* static, unused */, JsonTextReader_t2658709200 * ____this0, Il2CppChar ___initialChar1, bool ___oneOrMore2, Il2CppChar* ___finalChar3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ilo_ParseTrue20(Newtonsoft.Json.JsonTextReader)
extern "C"  void JsonTextReader_ilo_ParseTrue20_m2336991595 (Il2CppObject * __this /* static, unused */, JsonTextReader_t2658709200 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ilo_ParseNumberNegativeInfinity21(Newtonsoft.Json.JsonTextReader)
extern "C"  void JsonTextReader_ilo_ParseNumberNegativeInfinity21_m232925188 (Il2CppObject * __this /* static, unused */, JsonTextReader_t2658709200 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ilo_ParseComment22(Newtonsoft.Json.JsonTextReader)
extern "C"  void JsonTextReader_ilo_ParseComment22_m391121096 (Il2CppObject * __this /* static, unused */, JsonTextReader_t2658709200 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::ilo_MatchValue23(Newtonsoft.Json.JsonTextReader,System.Char,System.String,System.Boolean)
extern "C"  bool JsonTextReader_ilo_MatchValue23_m273925421 (Il2CppObject * __this /* static, unused */, JsonTextReader_t2658709200 * ____this0, Il2CppChar ___firstChar1, String_t* ___value2, bool ___noTrailingNonSeperatorCharacters3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::ilo_IsSeperator24(Newtonsoft.Json.JsonTextReader,System.Char)
extern "C"  bool JsonTextReader_ilo_IsSeperator24_m2369070920 (Il2CppObject * __this /* static, unused */, JsonTextReader_t2658709200 * ____this0, Il2CppChar ___c1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.JsonTextReader::ilo_PeekNext25(Newtonsoft.Json.JsonTextReader)
extern "C"  int32_t JsonTextReader_ilo_PeekNext25_m395280539 (Il2CppObject * __this /* static, unused */, JsonTextReader_t2658709200 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ilo_Close26(Newtonsoft.Json.JsonReader)
extern "C"  void JsonTextReader_ilo_Close26_m2781412405 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
