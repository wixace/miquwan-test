﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ComponentGenerated
struct UnityEngine_ComponentGenerated_t381315194;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Type
struct Type_t;
// MethodID
struct MethodID_t3916401116;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_MethodID3916401116.h"

// System.Void UnityEngine_ComponentGenerated::.ctor()
extern "C"  void UnityEngine_ComponentGenerated__ctor_m2054495585 (UnityEngine_ComponentGenerated_t381315194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ComponentGenerated::.cctor()
extern "C"  void UnityEngine_ComponentGenerated__cctor_m3077724780 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_Component1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_Component1_m2584486781 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ComponentGenerated::Component_transform(JSVCall)
extern "C"  void UnityEngine_ComponentGenerated_Component_transform_m3442073438 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ComponentGenerated::Component_gameObject(JSVCall)
extern "C"  void UnityEngine_ComponentGenerated_Component_gameObject_m3633643409 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ComponentGenerated::Component_tag(JSVCall)
extern "C"  void UnityEngine_ComponentGenerated_Component_tag_m4190536176 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_BroadcastMessage__String__Object__SendMessageOptions(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_BroadcastMessage__String__Object__SendMessageOptions_m2831046014 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_BroadcastMessage__String__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_BroadcastMessage__String__Object_m3029986175 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_BroadcastMessage__String__SendMessageOptions(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_BroadcastMessage__String__SendMessageOptions_m1880358911 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_BroadcastMessage__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_BroadcastMessage__String_m318972032 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_CompareTag__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_CompareTag__String_m4072433327 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_GetComponent__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_GetComponent__Type_m484746538 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_GetComponent__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_GetComponent__String_m2810154753 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_GetComponentT1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_GetComponentT1_m1825196941 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_GetComponentInChildren__Type__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_GetComponentInChildren__Type__Boolean_m1354740252 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_GetComponentInChildrenT1__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_GetComponentInChildrenT1__Boolean_m4155201817 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_GetComponentInChildren__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_GetComponentInChildren__Type_m898101966 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_GetComponentInChildrenT1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_GetComponentInChildrenT1_m4200467249 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_GetComponentInParent__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_GetComponentInParent__Type_m3525545977 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_GetComponentInParentT1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_GetComponentInParentT1_m4145346012 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_GetComponents__Type__ListT1_Component(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_GetComponents__Type__ListT1_Component_m826436376 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_GetComponentsT1__ListT1_T(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_GetComponentsT1__ListT1_T_m3145444978 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_GetComponents__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_GetComponents__Type_m2074209855 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_GetComponentsT1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_GetComponentsT1_m2790876962 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_GetComponentsInChildren__Type__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_GetComponentsInChildren__Type__Boolean_m2638896871 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_GetComponentsInChildrenT1__Boolean__ListT1_T(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_GetComponentsInChildrenT1__Boolean__ListT1_T_m803700788 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_GetComponentsInChildren__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_GetComponentsInChildren__Type_m2042668451 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_GetComponentsInChildrenT1__ListT1_T(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_GetComponentsInChildrenT1__ListT1_T_m1768180438 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_GetComponentsInChildrenT1__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_GetComponentsInChildrenT1__Boolean_m3230278500 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_GetComponentsInChildrenT1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_GetComponentsInChildrenT1_m2502127750 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_GetComponentsInParent__Type__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_GetComponentsInParent__Type__Boolean_m3515881692 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_GetComponentsInParentT1__Boolean__ListT1_T(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_GetComponentsInParentT1__Boolean__ListT1_T_m1570057833 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_GetComponentsInParentT1__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_GetComponentsInParentT1__Boolean_m943529945 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_GetComponentsInParent__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_GetComponentsInParent__Type_m3508859918 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_GetComponentsInParentT1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_GetComponentsInParentT1_m3039669361 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_SendMessage__String__Object__SendMessageOptions(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_SendMessage__String__Object__SendMessageOptions_m3733471079 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_SendMessage__String__SendMessageOptions(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_SendMessage__String__SendMessageOptions_m3952534248 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_SendMessage__String__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_SendMessage__String__Object_m2960335848 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_SendMessage__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_SendMessage__String_m1980486121 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_SendMessageUpwards__String__Object__SendMessageOptions(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_SendMessageUpwards__String__Object__SendMessageOptions_m1948981269 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_SendMessageUpwards__String__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_SendMessageUpwards__String__Object_m4188708758 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_SendMessageUpwards__String__SendMessageOptions(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_SendMessageUpwards__String__SendMessageOptions_m729565590 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::Component_SendMessageUpwards__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_Component_SendMessageUpwards__String_m2916670359 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ComponentGenerated::__Register()
extern "C"  void UnityEngine_ComponentGenerated___Register_m1314690054 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ComponentGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_ComponentGenerated_ilo_setObject1_m2482839369 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ComponentGenerated::ilo_setStringS2(System.Int32,System.String)
extern "C"  void UnityEngine_ComponentGenerated_ilo_setStringS2_m550107179 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ComponentGenerated::ilo_getEnum3(System.Int32)
extern "C"  int32_t UnityEngine_ComponentGenerated_ilo_getEnum3_m275871121 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_ComponentGenerated::ilo_getStringS4(System.Int32)
extern "C"  String_t* UnityEngine_ComponentGenerated_ilo_getStringS4_m1089064802 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ComponentGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void UnityEngine_ComponentGenerated_ilo_setBooleanS5_m2467765646 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::ilo_Component_GetComponentT16(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_ilo_Component_GetComponentT16_m3625150008 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_ComponentGenerated::ilo_getObject7(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_ComponentGenerated_ilo_getObject7_m2966041780 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::ilo_getBooleanS8(System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_ilo_getBooleanS8_m2179676946 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ComponentGenerated::ilo_setWhatever9(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  void UnityEngine_ComponentGenerated_ilo_setWhatever9_m3033023104 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::ilo_Component_GetComponentInParentT110(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_ilo_Component_GetComponentInParentT110_m2073350702 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngine_ComponentGenerated::ilo_makeGenericMethod11(System.Type,MethodID,System.Int32)
extern "C"  MethodInfo_t * UnityEngine_ComponentGenerated_ilo_makeGenericMethod11_m2142685402 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, MethodID_t3916401116 * ___methodID1, int32_t ___TCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ComponentGenerated::ilo_moveSaveID2Arr12(System.Int32)
extern "C"  void UnityEngine_ComponentGenerated_ilo_moveSaveID2Arr12_m2781088416 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ComponentGenerated::ilo_setArrayS13(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_ComponentGenerated_ilo_setArrayS13_m3342012193 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::ilo_Component_GetComponentsInChildrenT1__Boolean14(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_ilo_Component_GetComponentsInChildrenT1__Boolean14_m4186992186 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComponentGenerated::ilo_Component_GetComponentsInParentT115(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComponentGenerated_ilo_Component_GetComponentsInParentT115_m3575013314 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_ComponentGenerated::ilo_getWhatever16(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_ComponentGenerated_ilo_getWhatever16_m3531640123 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
