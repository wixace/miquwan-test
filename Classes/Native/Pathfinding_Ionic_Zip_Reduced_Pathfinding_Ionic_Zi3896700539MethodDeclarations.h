﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Ionic.Zip.BadPasswordException
struct BadPasswordException_t3896700539;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Pathfinding.Ionic.Zip.BadPasswordException::.ctor()
extern "C"  void BadPasswordException__ctor_m540074998 (BadPasswordException_t3896700539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Ionic.Zip.BadPasswordException::.ctor(System.String)
extern "C"  void BadPasswordException__ctor_m957962956 (BadPasswordException_t3896700539 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
