﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VersionInfoGenerated
struct VersionInfoGenerated_t1532337833;
// JSVCall
struct JSVCall_t3708497963;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.String
struct String_t;
// VersionInfo
struct VersionInfo_t2356638086;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionInfo2356638086.h"

// System.Void VersionInfoGenerated::.ctor()
extern "C"  void VersionInfoGenerated__ctor_m1136667154 (VersionInfoGenerated_t1532337833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VersionInfoGenerated::VersionInfo_VersionInfo1(JSVCall,System.Int32)
extern "C"  bool VersionInfoGenerated_VersionInfo_VersionInfo1_m126072602 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::VersionInfo_name(JSVCall)
extern "C"  void VersionInfoGenerated_VersionInfo_name_m413015281 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::VersionInfo_pkgvs(JSVCall)
extern "C"  void VersionInfoGenerated_VersionInfo_pkgvs_m2123747143 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::VersionInfo_pkgsize(JSVCall)
extern "C"  void VersionInfoGenerated_VersionInfo_pkgsize_m3255043 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::VersionInfo_pkgmd5(JSVCall)
extern "C"  void VersionInfoGenerated_VersionInfo_pkgmd5_m3846563242 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::VersionInfo_pkgurl(JSVCall)
extern "C"  void VersionInfoGenerated_VersionInfo_pkgurl_m3273401561 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::VersionInfo_assetsvs(JSVCall)
extern "C"  void VersionInfoGenerated_VersionInfo_assetsvs_m3679236764 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::VersionInfo_assetsurl(JSVCall)
extern "C"  void VersionInfoGenerated_VersionInfo_assetsurl_m4248939556 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::VersionInfo_servervs(JSVCall)
extern "C"  void VersionInfoGenerated_VersionInfo_servervs_m3586399964 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::VersionInfo_serverurl(JSVCall)
extern "C"  void VersionInfoGenerated_VersionInfo_serverurl_m1370998756 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::VersionInfo_bulletinurl(JSVCall)
extern "C"  void VersionInfoGenerated_VersionInfo_bulletinurl_m292223432 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::VersionInfo_appkey(JSVCall)
extern "C"  void VersionInfoGenerated_VersionInfo_appkey_m3596503742 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::VersionInfo_trackappkey(JSVCall)
extern "C"  void VersionInfoGenerated_VersionInfo_trackappkey_m3707074471 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::VersionInfo_usertype(JSVCall)
extern "C"  void VersionInfoGenerated_VersionInfo_usertype_m194967607 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::VersionInfo_hide2000(JSVCall)
extern "C"  void VersionInfoGenerated_VersionInfo_hide2000_m3579604764 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::VersionInfo_sdkinfo(JSVCall)
extern "C"  void VersionInfoGenerated_VersionInfo_sdkinfo_m1975054664 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::VersionInfo_paycburl(JSVCall)
extern "C"  void VersionInfoGenerated_VersionInfo_paycburl_m1493170196 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::VersionInfo_orderurl(JSVCall)
extern "C"  void VersionInfoGenerated_VersionInfo_orderurl_m1341266395 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::VersionInfo_pidurl(JSVCall)
extern "C"  void VersionInfoGenerated_VersionInfo_pidurl_m869760536 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::VersionInfo_hideGuide(JSVCall)
extern "C"  void VersionInfoGenerated_VersionInfo_hideGuide_m4181863446 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::VersionInfo_sdk(JSVCall)
extern "C"  void VersionInfoGenerated_VersionInfo_sdk_m1152174646 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::VersionInfo_isFirst(JSVCall)
extern "C"  void VersionInfoGenerated_VersionInfo_isFirst_m3097901898 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::__Register()
extern "C"  void VersionInfoGenerated___Register_m1143131637 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] VersionInfoGenerated::<VersionInfo_serverurl>m__2F8()
extern "C"  StringU5BU5D_t4054002952* VersionInfoGenerated_U3CVersionInfo_serverurlU3Em__2F8_m711201325 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VersionInfoGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t VersionInfoGenerated_ilo_getObject1_m3456425280 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VersionInfoGenerated::ilo_getStringS2(System.Int32)
extern "C"  String_t* VersionInfoGenerated_ilo_getStringS2_m1889228049 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::ilo_setStringS3(System.Int32,System.String)
extern "C"  void VersionInfoGenerated_ilo_setStringS3_m4275578077 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::ilo_moveSaveID2Arr4(System.Int32)
extern "C"  void VersionInfoGenerated_ilo_moveSaveID2Arr4_m3069740138 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::ilo_setArrayS5(System.Int32,System.Int32,System.Boolean)
extern "C"  void VersionInfoGenerated_ilo_setArrayS5_m408047669 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionInfoGenerated::ilo_setInt326(System.Int32,System.Int32)
extern "C"  void VersionInfoGenerated_ilo_setInt326_m2792200415 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VersionInfoGenerated::ilo_getInt327(System.Int32)
extern "C"  int32_t VersionInfoGenerated_ilo_getInt327_m829490655 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VersionInfoGenerated::ilo_get_isFirst8(VersionInfo)
extern "C"  bool VersionInfoGenerated_ilo_get_isFirst8_m3184892504 (Il2CppObject * __this /* static, unused */, VersionInfo_t2356638086 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
