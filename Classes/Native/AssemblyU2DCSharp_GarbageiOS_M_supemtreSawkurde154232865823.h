﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_supemtreSawkurde154
struct  M_supemtreSawkurde154_t232865823  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_supemtreSawkurde154::_jawni
	int32_t ____jawni_0;
	// System.Boolean GarbageiOS.M_supemtreSawkurde154::_pehoo
	bool ____pehoo_1;
	// System.Boolean GarbageiOS.M_supemtreSawkurde154::_falbekem
	bool ____falbekem_2;
	// System.Int32 GarbageiOS.M_supemtreSawkurde154::_woonicaiZotalear
	int32_t ____woonicaiZotalear_3;
	// System.UInt32 GarbageiOS.M_supemtreSawkurde154::_terehe
	uint32_t ____terehe_4;
	// System.Int32 GarbageiOS.M_supemtreSawkurde154::_sirboubaPerecearsi
	int32_t ____sirboubaPerecearsi_5;
	// System.Single GarbageiOS.M_supemtreSawkurde154::_naverRurwhe
	float ____naverRurwhe_6;
	// System.UInt32 GarbageiOS.M_supemtreSawkurde154::_demgis
	uint32_t ____demgis_7;
	// System.String GarbageiOS.M_supemtreSawkurde154::_lerdaiVesalnar
	String_t* ____lerdaiVesalnar_8;
	// System.Int32 GarbageiOS.M_supemtreSawkurde154::_qejear
	int32_t ____qejear_9;
	// System.Single GarbageiOS.M_supemtreSawkurde154::_memjeHeexasnall
	float ____memjeHeexasnall_10;
	// System.Boolean GarbageiOS.M_supemtreSawkurde154::_kaycaZeargize
	bool ____kaycaZeargize_11;
	// System.Boolean GarbageiOS.M_supemtreSawkurde154::_hearkaNouyai
	bool ____hearkaNouyai_12;
	// System.UInt32 GarbageiOS.M_supemtreSawkurde154::_daneaboo
	uint32_t ____daneaboo_13;
	// System.Int32 GarbageiOS.M_supemtreSawkurde154::_weabemwhow
	int32_t ____weabemwhow_14;
	// System.Int32 GarbageiOS.M_supemtreSawkurde154::_pairqenemFerenaibair
	int32_t ____pairqenemFerenaibair_15;
	// System.String GarbageiOS.M_supemtreSawkurde154::_mege
	String_t* ____mege_16;
	// System.UInt32 GarbageiOS.M_supemtreSawkurde154::_lehearSarlairve
	uint32_t ____lehearSarlairve_17;
	// System.Boolean GarbageiOS.M_supemtreSawkurde154::_pouci
	bool ____pouci_18;
	// System.UInt32 GarbageiOS.M_supemtreSawkurde154::_churkousa
	uint32_t ____churkousa_19;
	// System.UInt32 GarbageiOS.M_supemtreSawkurde154::_gawmojall
	uint32_t ____gawmojall_20;
	// System.Boolean GarbageiOS.M_supemtreSawkurde154::_mazusou
	bool ____mazusou_21;
	// System.Boolean GarbageiOS.M_supemtreSawkurde154::_sefo
	bool ____sefo_22;
	// System.Int32 GarbageiOS.M_supemtreSawkurde154::_semgoucaw
	int32_t ____semgoucaw_23;
	// System.Single GarbageiOS.M_supemtreSawkurde154::_qoukapasParsenis
	float ____qoukapasParsenis_24;
	// System.Boolean GarbageiOS.M_supemtreSawkurde154::_balldorro
	bool ____balldorro_25;
	// System.Single GarbageiOS.M_supemtreSawkurde154::_trorbuWorta
	float ____trorbuWorta_26;
	// System.Boolean GarbageiOS.M_supemtreSawkurde154::_jowmocear
	bool ____jowmocear_27;
	// System.UInt32 GarbageiOS.M_supemtreSawkurde154::_soxoupi
	uint32_t ____soxoupi_28;
	// System.Single GarbageiOS.M_supemtreSawkurde154::_fanaipee
	float ____fanaipee_29;
	// System.Single GarbageiOS.M_supemtreSawkurde154::_nemawmar
	float ____nemawmar_30;
	// System.Boolean GarbageiOS.M_supemtreSawkurde154::_jairste
	bool ____jairste_31;
	// System.String GarbageiOS.M_supemtreSawkurde154::_miyaGasga
	String_t* ____miyaGasga_32;
	// System.Boolean GarbageiOS.M_supemtreSawkurde154::_tayroo
	bool ____tayroo_33;
	// System.String GarbageiOS.M_supemtreSawkurde154::_wepehe
	String_t* ____wepehe_34;
	// System.Int32 GarbageiOS.M_supemtreSawkurde154::_qeljurnou
	int32_t ____qeljurnou_35;

public:
	inline static int32_t get_offset_of__jawni_0() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____jawni_0)); }
	inline int32_t get__jawni_0() const { return ____jawni_0; }
	inline int32_t* get_address_of__jawni_0() { return &____jawni_0; }
	inline void set__jawni_0(int32_t value)
	{
		____jawni_0 = value;
	}

	inline static int32_t get_offset_of__pehoo_1() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____pehoo_1)); }
	inline bool get__pehoo_1() const { return ____pehoo_1; }
	inline bool* get_address_of__pehoo_1() { return &____pehoo_1; }
	inline void set__pehoo_1(bool value)
	{
		____pehoo_1 = value;
	}

	inline static int32_t get_offset_of__falbekem_2() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____falbekem_2)); }
	inline bool get__falbekem_2() const { return ____falbekem_2; }
	inline bool* get_address_of__falbekem_2() { return &____falbekem_2; }
	inline void set__falbekem_2(bool value)
	{
		____falbekem_2 = value;
	}

	inline static int32_t get_offset_of__woonicaiZotalear_3() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____woonicaiZotalear_3)); }
	inline int32_t get__woonicaiZotalear_3() const { return ____woonicaiZotalear_3; }
	inline int32_t* get_address_of__woonicaiZotalear_3() { return &____woonicaiZotalear_3; }
	inline void set__woonicaiZotalear_3(int32_t value)
	{
		____woonicaiZotalear_3 = value;
	}

	inline static int32_t get_offset_of__terehe_4() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____terehe_4)); }
	inline uint32_t get__terehe_4() const { return ____terehe_4; }
	inline uint32_t* get_address_of__terehe_4() { return &____terehe_4; }
	inline void set__terehe_4(uint32_t value)
	{
		____terehe_4 = value;
	}

	inline static int32_t get_offset_of__sirboubaPerecearsi_5() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____sirboubaPerecearsi_5)); }
	inline int32_t get__sirboubaPerecearsi_5() const { return ____sirboubaPerecearsi_5; }
	inline int32_t* get_address_of__sirboubaPerecearsi_5() { return &____sirboubaPerecearsi_5; }
	inline void set__sirboubaPerecearsi_5(int32_t value)
	{
		____sirboubaPerecearsi_5 = value;
	}

	inline static int32_t get_offset_of__naverRurwhe_6() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____naverRurwhe_6)); }
	inline float get__naverRurwhe_6() const { return ____naverRurwhe_6; }
	inline float* get_address_of__naverRurwhe_6() { return &____naverRurwhe_6; }
	inline void set__naverRurwhe_6(float value)
	{
		____naverRurwhe_6 = value;
	}

	inline static int32_t get_offset_of__demgis_7() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____demgis_7)); }
	inline uint32_t get__demgis_7() const { return ____demgis_7; }
	inline uint32_t* get_address_of__demgis_7() { return &____demgis_7; }
	inline void set__demgis_7(uint32_t value)
	{
		____demgis_7 = value;
	}

	inline static int32_t get_offset_of__lerdaiVesalnar_8() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____lerdaiVesalnar_8)); }
	inline String_t* get__lerdaiVesalnar_8() const { return ____lerdaiVesalnar_8; }
	inline String_t** get_address_of__lerdaiVesalnar_8() { return &____lerdaiVesalnar_8; }
	inline void set__lerdaiVesalnar_8(String_t* value)
	{
		____lerdaiVesalnar_8 = value;
		Il2CppCodeGenWriteBarrier(&____lerdaiVesalnar_8, value);
	}

	inline static int32_t get_offset_of__qejear_9() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____qejear_9)); }
	inline int32_t get__qejear_9() const { return ____qejear_9; }
	inline int32_t* get_address_of__qejear_9() { return &____qejear_9; }
	inline void set__qejear_9(int32_t value)
	{
		____qejear_9 = value;
	}

	inline static int32_t get_offset_of__memjeHeexasnall_10() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____memjeHeexasnall_10)); }
	inline float get__memjeHeexasnall_10() const { return ____memjeHeexasnall_10; }
	inline float* get_address_of__memjeHeexasnall_10() { return &____memjeHeexasnall_10; }
	inline void set__memjeHeexasnall_10(float value)
	{
		____memjeHeexasnall_10 = value;
	}

	inline static int32_t get_offset_of__kaycaZeargize_11() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____kaycaZeargize_11)); }
	inline bool get__kaycaZeargize_11() const { return ____kaycaZeargize_11; }
	inline bool* get_address_of__kaycaZeargize_11() { return &____kaycaZeargize_11; }
	inline void set__kaycaZeargize_11(bool value)
	{
		____kaycaZeargize_11 = value;
	}

	inline static int32_t get_offset_of__hearkaNouyai_12() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____hearkaNouyai_12)); }
	inline bool get__hearkaNouyai_12() const { return ____hearkaNouyai_12; }
	inline bool* get_address_of__hearkaNouyai_12() { return &____hearkaNouyai_12; }
	inline void set__hearkaNouyai_12(bool value)
	{
		____hearkaNouyai_12 = value;
	}

	inline static int32_t get_offset_of__daneaboo_13() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____daneaboo_13)); }
	inline uint32_t get__daneaboo_13() const { return ____daneaboo_13; }
	inline uint32_t* get_address_of__daneaboo_13() { return &____daneaboo_13; }
	inline void set__daneaboo_13(uint32_t value)
	{
		____daneaboo_13 = value;
	}

	inline static int32_t get_offset_of__weabemwhow_14() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____weabemwhow_14)); }
	inline int32_t get__weabemwhow_14() const { return ____weabemwhow_14; }
	inline int32_t* get_address_of__weabemwhow_14() { return &____weabemwhow_14; }
	inline void set__weabemwhow_14(int32_t value)
	{
		____weabemwhow_14 = value;
	}

	inline static int32_t get_offset_of__pairqenemFerenaibair_15() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____pairqenemFerenaibair_15)); }
	inline int32_t get__pairqenemFerenaibair_15() const { return ____pairqenemFerenaibair_15; }
	inline int32_t* get_address_of__pairqenemFerenaibair_15() { return &____pairqenemFerenaibair_15; }
	inline void set__pairqenemFerenaibair_15(int32_t value)
	{
		____pairqenemFerenaibair_15 = value;
	}

	inline static int32_t get_offset_of__mege_16() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____mege_16)); }
	inline String_t* get__mege_16() const { return ____mege_16; }
	inline String_t** get_address_of__mege_16() { return &____mege_16; }
	inline void set__mege_16(String_t* value)
	{
		____mege_16 = value;
		Il2CppCodeGenWriteBarrier(&____mege_16, value);
	}

	inline static int32_t get_offset_of__lehearSarlairve_17() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____lehearSarlairve_17)); }
	inline uint32_t get__lehearSarlairve_17() const { return ____lehearSarlairve_17; }
	inline uint32_t* get_address_of__lehearSarlairve_17() { return &____lehearSarlairve_17; }
	inline void set__lehearSarlairve_17(uint32_t value)
	{
		____lehearSarlairve_17 = value;
	}

	inline static int32_t get_offset_of__pouci_18() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____pouci_18)); }
	inline bool get__pouci_18() const { return ____pouci_18; }
	inline bool* get_address_of__pouci_18() { return &____pouci_18; }
	inline void set__pouci_18(bool value)
	{
		____pouci_18 = value;
	}

	inline static int32_t get_offset_of__churkousa_19() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____churkousa_19)); }
	inline uint32_t get__churkousa_19() const { return ____churkousa_19; }
	inline uint32_t* get_address_of__churkousa_19() { return &____churkousa_19; }
	inline void set__churkousa_19(uint32_t value)
	{
		____churkousa_19 = value;
	}

	inline static int32_t get_offset_of__gawmojall_20() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____gawmojall_20)); }
	inline uint32_t get__gawmojall_20() const { return ____gawmojall_20; }
	inline uint32_t* get_address_of__gawmojall_20() { return &____gawmojall_20; }
	inline void set__gawmojall_20(uint32_t value)
	{
		____gawmojall_20 = value;
	}

	inline static int32_t get_offset_of__mazusou_21() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____mazusou_21)); }
	inline bool get__mazusou_21() const { return ____mazusou_21; }
	inline bool* get_address_of__mazusou_21() { return &____mazusou_21; }
	inline void set__mazusou_21(bool value)
	{
		____mazusou_21 = value;
	}

	inline static int32_t get_offset_of__sefo_22() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____sefo_22)); }
	inline bool get__sefo_22() const { return ____sefo_22; }
	inline bool* get_address_of__sefo_22() { return &____sefo_22; }
	inline void set__sefo_22(bool value)
	{
		____sefo_22 = value;
	}

	inline static int32_t get_offset_of__semgoucaw_23() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____semgoucaw_23)); }
	inline int32_t get__semgoucaw_23() const { return ____semgoucaw_23; }
	inline int32_t* get_address_of__semgoucaw_23() { return &____semgoucaw_23; }
	inline void set__semgoucaw_23(int32_t value)
	{
		____semgoucaw_23 = value;
	}

	inline static int32_t get_offset_of__qoukapasParsenis_24() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____qoukapasParsenis_24)); }
	inline float get__qoukapasParsenis_24() const { return ____qoukapasParsenis_24; }
	inline float* get_address_of__qoukapasParsenis_24() { return &____qoukapasParsenis_24; }
	inline void set__qoukapasParsenis_24(float value)
	{
		____qoukapasParsenis_24 = value;
	}

	inline static int32_t get_offset_of__balldorro_25() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____balldorro_25)); }
	inline bool get__balldorro_25() const { return ____balldorro_25; }
	inline bool* get_address_of__balldorro_25() { return &____balldorro_25; }
	inline void set__balldorro_25(bool value)
	{
		____balldorro_25 = value;
	}

	inline static int32_t get_offset_of__trorbuWorta_26() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____trorbuWorta_26)); }
	inline float get__trorbuWorta_26() const { return ____trorbuWorta_26; }
	inline float* get_address_of__trorbuWorta_26() { return &____trorbuWorta_26; }
	inline void set__trorbuWorta_26(float value)
	{
		____trorbuWorta_26 = value;
	}

	inline static int32_t get_offset_of__jowmocear_27() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____jowmocear_27)); }
	inline bool get__jowmocear_27() const { return ____jowmocear_27; }
	inline bool* get_address_of__jowmocear_27() { return &____jowmocear_27; }
	inline void set__jowmocear_27(bool value)
	{
		____jowmocear_27 = value;
	}

	inline static int32_t get_offset_of__soxoupi_28() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____soxoupi_28)); }
	inline uint32_t get__soxoupi_28() const { return ____soxoupi_28; }
	inline uint32_t* get_address_of__soxoupi_28() { return &____soxoupi_28; }
	inline void set__soxoupi_28(uint32_t value)
	{
		____soxoupi_28 = value;
	}

	inline static int32_t get_offset_of__fanaipee_29() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____fanaipee_29)); }
	inline float get__fanaipee_29() const { return ____fanaipee_29; }
	inline float* get_address_of__fanaipee_29() { return &____fanaipee_29; }
	inline void set__fanaipee_29(float value)
	{
		____fanaipee_29 = value;
	}

	inline static int32_t get_offset_of__nemawmar_30() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____nemawmar_30)); }
	inline float get__nemawmar_30() const { return ____nemawmar_30; }
	inline float* get_address_of__nemawmar_30() { return &____nemawmar_30; }
	inline void set__nemawmar_30(float value)
	{
		____nemawmar_30 = value;
	}

	inline static int32_t get_offset_of__jairste_31() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____jairste_31)); }
	inline bool get__jairste_31() const { return ____jairste_31; }
	inline bool* get_address_of__jairste_31() { return &____jairste_31; }
	inline void set__jairste_31(bool value)
	{
		____jairste_31 = value;
	}

	inline static int32_t get_offset_of__miyaGasga_32() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____miyaGasga_32)); }
	inline String_t* get__miyaGasga_32() const { return ____miyaGasga_32; }
	inline String_t** get_address_of__miyaGasga_32() { return &____miyaGasga_32; }
	inline void set__miyaGasga_32(String_t* value)
	{
		____miyaGasga_32 = value;
		Il2CppCodeGenWriteBarrier(&____miyaGasga_32, value);
	}

	inline static int32_t get_offset_of__tayroo_33() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____tayroo_33)); }
	inline bool get__tayroo_33() const { return ____tayroo_33; }
	inline bool* get_address_of__tayroo_33() { return &____tayroo_33; }
	inline void set__tayroo_33(bool value)
	{
		____tayroo_33 = value;
	}

	inline static int32_t get_offset_of__wepehe_34() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____wepehe_34)); }
	inline String_t* get__wepehe_34() const { return ____wepehe_34; }
	inline String_t** get_address_of__wepehe_34() { return &____wepehe_34; }
	inline void set__wepehe_34(String_t* value)
	{
		____wepehe_34 = value;
		Il2CppCodeGenWriteBarrier(&____wepehe_34, value);
	}

	inline static int32_t get_offset_of__qeljurnou_35() { return static_cast<int32_t>(offsetof(M_supemtreSawkurde154_t232865823, ____qeljurnou_35)); }
	inline int32_t get__qeljurnou_35() const { return ____qeljurnou_35; }
	inline int32_t* get_address_of__qeljurnou_35() { return &____qeljurnou_35; }
	inline void set__qeljurnou_35(int32_t value)
	{
		____qeljurnou_35 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
