﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CombatEntity
struct CombatEntity_t684137495;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CombatEntity/<EnterPartBloom>c__AnonStorey151
struct  U3CEnterPartBloomU3Ec__AnonStorey151_t1955055656  : public Il2CppObject
{
public:
	// System.Int32 CombatEntity/<EnterPartBloom>c__AnonStorey151::index
	int32_t ___index_0;
	// CombatEntity CombatEntity/<EnterPartBloom>c__AnonStorey151::<>f__this
	CombatEntity_t684137495 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(U3CEnterPartBloomU3Ec__AnonStorey151_t1955055656, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CEnterPartBloomU3Ec__AnonStorey151_t1955055656, ___U3CU3Ef__this_1)); }
	inline CombatEntity_t684137495 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline CombatEntity_t684137495 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(CombatEntity_t684137495 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
