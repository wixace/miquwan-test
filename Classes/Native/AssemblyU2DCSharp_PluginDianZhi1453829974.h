﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginDianZhi
struct  PluginDianZhi_t1453829974  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginDianZhi::openid
	String_t* ___openid_7;
	// System.String PluginDianZhi::token
	String_t* ___token_8;
	// System.String PluginDianZhi::sign
	String_t* ___sign_9;
	// System.String PluginDianZhi::configId
	String_t* ___configId_10;
	// System.String PluginDianZhi::serverid
	String_t* ___serverid_11;
	// System.String PluginDianZhi::servername
	String_t* ___servername_12;
	// System.String PluginDianZhi::roleid
	String_t* ___roleid_13;
	// System.String PluginDianZhi::rolename
	String_t* ___rolename_14;
	// System.String PluginDianZhi::rolelevel
	String_t* ___rolelevel_15;
	// System.String PluginDianZhi::CreatRoleTime
	String_t* ___CreatRoleTime_16;
	// System.String PluginDianZhi::gold
	String_t* ___gold_17;
	// System.String PluginDianZhi::VipLevel
	String_t* ___VipLevel_18;
	// System.Boolean PluginDianZhi::isFirstCollectData
	bool ___isFirstCollectData_19;

public:
	inline static int32_t get_offset_of_openid_7() { return static_cast<int32_t>(offsetof(PluginDianZhi_t1453829974, ___openid_7)); }
	inline String_t* get_openid_7() const { return ___openid_7; }
	inline String_t** get_address_of_openid_7() { return &___openid_7; }
	inline void set_openid_7(String_t* value)
	{
		___openid_7 = value;
		Il2CppCodeGenWriteBarrier(&___openid_7, value);
	}

	inline static int32_t get_offset_of_token_8() { return static_cast<int32_t>(offsetof(PluginDianZhi_t1453829974, ___token_8)); }
	inline String_t* get_token_8() const { return ___token_8; }
	inline String_t** get_address_of_token_8() { return &___token_8; }
	inline void set_token_8(String_t* value)
	{
		___token_8 = value;
		Il2CppCodeGenWriteBarrier(&___token_8, value);
	}

	inline static int32_t get_offset_of_sign_9() { return static_cast<int32_t>(offsetof(PluginDianZhi_t1453829974, ___sign_9)); }
	inline String_t* get_sign_9() const { return ___sign_9; }
	inline String_t** get_address_of_sign_9() { return &___sign_9; }
	inline void set_sign_9(String_t* value)
	{
		___sign_9 = value;
		Il2CppCodeGenWriteBarrier(&___sign_9, value);
	}

	inline static int32_t get_offset_of_configId_10() { return static_cast<int32_t>(offsetof(PluginDianZhi_t1453829974, ___configId_10)); }
	inline String_t* get_configId_10() const { return ___configId_10; }
	inline String_t** get_address_of_configId_10() { return &___configId_10; }
	inline void set_configId_10(String_t* value)
	{
		___configId_10 = value;
		Il2CppCodeGenWriteBarrier(&___configId_10, value);
	}

	inline static int32_t get_offset_of_serverid_11() { return static_cast<int32_t>(offsetof(PluginDianZhi_t1453829974, ___serverid_11)); }
	inline String_t* get_serverid_11() const { return ___serverid_11; }
	inline String_t** get_address_of_serverid_11() { return &___serverid_11; }
	inline void set_serverid_11(String_t* value)
	{
		___serverid_11 = value;
		Il2CppCodeGenWriteBarrier(&___serverid_11, value);
	}

	inline static int32_t get_offset_of_servername_12() { return static_cast<int32_t>(offsetof(PluginDianZhi_t1453829974, ___servername_12)); }
	inline String_t* get_servername_12() const { return ___servername_12; }
	inline String_t** get_address_of_servername_12() { return &___servername_12; }
	inline void set_servername_12(String_t* value)
	{
		___servername_12 = value;
		Il2CppCodeGenWriteBarrier(&___servername_12, value);
	}

	inline static int32_t get_offset_of_roleid_13() { return static_cast<int32_t>(offsetof(PluginDianZhi_t1453829974, ___roleid_13)); }
	inline String_t* get_roleid_13() const { return ___roleid_13; }
	inline String_t** get_address_of_roleid_13() { return &___roleid_13; }
	inline void set_roleid_13(String_t* value)
	{
		___roleid_13 = value;
		Il2CppCodeGenWriteBarrier(&___roleid_13, value);
	}

	inline static int32_t get_offset_of_rolename_14() { return static_cast<int32_t>(offsetof(PluginDianZhi_t1453829974, ___rolename_14)); }
	inline String_t* get_rolename_14() const { return ___rolename_14; }
	inline String_t** get_address_of_rolename_14() { return &___rolename_14; }
	inline void set_rolename_14(String_t* value)
	{
		___rolename_14 = value;
		Il2CppCodeGenWriteBarrier(&___rolename_14, value);
	}

	inline static int32_t get_offset_of_rolelevel_15() { return static_cast<int32_t>(offsetof(PluginDianZhi_t1453829974, ___rolelevel_15)); }
	inline String_t* get_rolelevel_15() const { return ___rolelevel_15; }
	inline String_t** get_address_of_rolelevel_15() { return &___rolelevel_15; }
	inline void set_rolelevel_15(String_t* value)
	{
		___rolelevel_15 = value;
		Il2CppCodeGenWriteBarrier(&___rolelevel_15, value);
	}

	inline static int32_t get_offset_of_CreatRoleTime_16() { return static_cast<int32_t>(offsetof(PluginDianZhi_t1453829974, ___CreatRoleTime_16)); }
	inline String_t* get_CreatRoleTime_16() const { return ___CreatRoleTime_16; }
	inline String_t** get_address_of_CreatRoleTime_16() { return &___CreatRoleTime_16; }
	inline void set_CreatRoleTime_16(String_t* value)
	{
		___CreatRoleTime_16 = value;
		Il2CppCodeGenWriteBarrier(&___CreatRoleTime_16, value);
	}

	inline static int32_t get_offset_of_gold_17() { return static_cast<int32_t>(offsetof(PluginDianZhi_t1453829974, ___gold_17)); }
	inline String_t* get_gold_17() const { return ___gold_17; }
	inline String_t** get_address_of_gold_17() { return &___gold_17; }
	inline void set_gold_17(String_t* value)
	{
		___gold_17 = value;
		Il2CppCodeGenWriteBarrier(&___gold_17, value);
	}

	inline static int32_t get_offset_of_VipLevel_18() { return static_cast<int32_t>(offsetof(PluginDianZhi_t1453829974, ___VipLevel_18)); }
	inline String_t* get_VipLevel_18() const { return ___VipLevel_18; }
	inline String_t** get_address_of_VipLevel_18() { return &___VipLevel_18; }
	inline void set_VipLevel_18(String_t* value)
	{
		___VipLevel_18 = value;
		Il2CppCodeGenWriteBarrier(&___VipLevel_18, value);
	}

	inline static int32_t get_offset_of_isFirstCollectData_19() { return static_cast<int32_t>(offsetof(PluginDianZhi_t1453829974, ___isFirstCollectData_19)); }
	inline bool get_isFirstCollectData_19() const { return ___isFirstCollectData_19; }
	inline bool* get_address_of_isFirstCollectData_19() { return &___isFirstCollectData_19; }
	inline void set_isFirstCollectData_19(bool value)
	{
		___isFirstCollectData_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
