﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader[]
struct ShaderU5BU5D_t3604329748;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShaderList
struct  ShaderList_t681705027  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Shader[] ShaderList::mainList
	ShaderU5BU5D_t3604329748* ___mainList_2;

public:
	inline static int32_t get_offset_of_mainList_2() { return static_cast<int32_t>(offsetof(ShaderList_t681705027, ___mainList_2)); }
	inline ShaderU5BU5D_t3604329748* get_mainList_2() const { return ___mainList_2; }
	inline ShaderU5BU5D_t3604329748** get_address_of_mainList_2() { return &___mainList_2; }
	inline void set_mainList_2(ShaderU5BU5D_t3604329748* value)
	{
		___mainList_2 = value;
		Il2CppCodeGenWriteBarrier(&___mainList_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
