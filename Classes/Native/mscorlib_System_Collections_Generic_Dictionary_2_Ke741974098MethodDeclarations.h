﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>
struct Dictionary_2_t127038044;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke741974098.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<FLOAT_TEXT_ID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1148223487_gshared (Enumerator_t741974098 * __this, Dictionary_2_t127038044 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1148223487(__this, ___host0, method) ((  void (*) (Enumerator_t741974098 *, Dictionary_2_t127038044 *, const MethodInfo*))Enumerator__ctor_m1148223487_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<FLOAT_TEXT_ID,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m182455618_gshared (Enumerator_t741974098 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m182455618(__this, method) ((  Il2CppObject * (*) (Enumerator_t741974098 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m182455618_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<FLOAT_TEXT_ID,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1596600150_gshared (Enumerator_t741974098 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1596600150(__this, method) ((  void (*) (Enumerator_t741974098 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1596600150_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<FLOAT_TEXT_ID,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1845646433_gshared (Enumerator_t741974098 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1845646433(__this, method) ((  void (*) (Enumerator_t741974098 *, const MethodInfo*))Enumerator_Dispose_m1845646433_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<FLOAT_TEXT_ID,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2928626882_gshared (Enumerator_t741974098 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2928626882(__this, method) ((  bool (*) (Enumerator_t741974098 *, const MethodInfo*))Enumerator_MoveNext_m2928626882_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<FLOAT_TEXT_ID,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1999768402_gshared (Enumerator_t741974098 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1999768402(__this, method) ((  int32_t (*) (Enumerator_t741974098 *, const MethodInfo*))Enumerator_get_Current_m1999768402_gshared)(__this, method)
