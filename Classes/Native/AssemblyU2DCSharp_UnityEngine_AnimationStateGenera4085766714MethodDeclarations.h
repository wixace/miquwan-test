﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AnimationStateGenerated
struct UnityEngine_AnimationStateGenerated_t4085766714;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_AnimationStateGenerated::.ctor()
extern "C"  void UnityEngine_AnimationStateGenerated__ctor_m2131511921 (UnityEngine_AnimationStateGenerated_t4085766714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationStateGenerated::AnimationState_AnimationState1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationStateGenerated_AnimationState_AnimationState1_m4263977217 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationStateGenerated::AnimationState_enabled(JSVCall)
extern "C"  void UnityEngine_AnimationStateGenerated_AnimationState_enabled_m3811302021 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationStateGenerated::AnimationState_weight(JSVCall)
extern "C"  void UnityEngine_AnimationStateGenerated_AnimationState_weight_m2701438702 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationStateGenerated::AnimationState_wrapMode(JSVCall)
extern "C"  void UnityEngine_AnimationStateGenerated_AnimationState_wrapMode_m3611854841 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationStateGenerated::AnimationState_time(JSVCall)
extern "C"  void UnityEngine_AnimationStateGenerated_AnimationState_time_m2297292345 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationStateGenerated::AnimationState_normalizedTime(JSVCall)
extern "C"  void UnityEngine_AnimationStateGenerated_AnimationState_normalizedTime_m365413506 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationStateGenerated::AnimationState_speed(JSVCall)
extern "C"  void UnityEngine_AnimationStateGenerated_AnimationState_speed_m1681650751 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationStateGenerated::AnimationState_normalizedSpeed(JSVCall)
extern "C"  void UnityEngine_AnimationStateGenerated_AnimationState_normalizedSpeed_m1922948886 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationStateGenerated::AnimationState_length(JSVCall)
extern "C"  void UnityEngine_AnimationStateGenerated_AnimationState_length_m4100805184 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationStateGenerated::AnimationState_layer(JSVCall)
extern "C"  void UnityEngine_AnimationStateGenerated_AnimationState_layer_m862136949 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationStateGenerated::AnimationState_clip(JSVCall)
extern "C"  void UnityEngine_AnimationStateGenerated_AnimationState_clip_m4108613910 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationStateGenerated::AnimationState_name(JSVCall)
extern "C"  void UnityEngine_AnimationStateGenerated_AnimationState_name_m3025048635 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationStateGenerated::AnimationState_blendMode(JSVCall)
extern "C"  void UnityEngine_AnimationStateGenerated_AnimationState_blendMode_m2604027282 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationStateGenerated::AnimationState_AddMixingTransform__Transform__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationStateGenerated_AnimationState_AddMixingTransform__Transform__Boolean_m4088394022 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationStateGenerated::AnimationState_AddMixingTransform__Transform(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationStateGenerated_AnimationState_AddMixingTransform__Transform_m3051667588 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationStateGenerated::AnimationState_RemoveMixingTransform__Transform(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationStateGenerated_AnimationState_RemoveMixingTransform__Transform_m4086189007 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationStateGenerated::__Register()
extern "C"  void UnityEngine_AnimationStateGenerated___Register_m1971785974 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AnimationStateGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_AnimationStateGenerated_ilo_getObject1_m4016393061 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationStateGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_AnimationStateGenerated_ilo_attachFinalizerObject2_m1226963431 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationStateGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void UnityEngine_AnimationStateGenerated_ilo_addJSCSRel3_m3111323055 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationStateGenerated::ilo_getBooleanS4(System.Int32)
extern "C"  bool UnityEngine_AnimationStateGenerated_ilo_getBooleanS4_m3373468694 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationStateGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void UnityEngine_AnimationStateGenerated_ilo_setSingle5_m1099825767 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_AnimationStateGenerated::ilo_getSingle6(System.Int32)
extern "C"  float UnityEngine_AnimationStateGenerated_ilo_getSingle6_m75200387 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AnimationStateGenerated::ilo_getInt327(System.Int32)
extern "C"  int32_t UnityEngine_AnimationStateGenerated_ilo_getInt327_m3202858778 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_AnimationStateGenerated::ilo_getObject8(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_AnimationStateGenerated_ilo_getObject8_m3117202139 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
