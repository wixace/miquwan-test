﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UISprite
struct UISprite_t661437049;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BossInofShow
struct  BossInofShow_t241030  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Camera BossInofShow::bossCamera
	Camera_t2727095145 * ___bossCamera_2;
	// UnityEngine.GameObject BossInofShow::_obj_left
	GameObject_t3674682005 * ____obj_left_3;
	// UnityEngine.GameObject BossInofShow::_obj_right
	GameObject_t3674682005 * ____obj_right_4;
	// UISprite BossInofShow::_boss_name_Left
	UISprite_t661437049 * ____boss_name_Left_5;
	// UISprite BossInofShow::_boss_name_right
	UISprite_t661437049 * ____boss_name_right_6;
	// System.Int32 BossInofShow::type
	int32_t ___type_7;

public:
	inline static int32_t get_offset_of_bossCamera_2() { return static_cast<int32_t>(offsetof(BossInofShow_t241030, ___bossCamera_2)); }
	inline Camera_t2727095145 * get_bossCamera_2() const { return ___bossCamera_2; }
	inline Camera_t2727095145 ** get_address_of_bossCamera_2() { return &___bossCamera_2; }
	inline void set_bossCamera_2(Camera_t2727095145 * value)
	{
		___bossCamera_2 = value;
		Il2CppCodeGenWriteBarrier(&___bossCamera_2, value);
	}

	inline static int32_t get_offset_of__obj_left_3() { return static_cast<int32_t>(offsetof(BossInofShow_t241030, ____obj_left_3)); }
	inline GameObject_t3674682005 * get__obj_left_3() const { return ____obj_left_3; }
	inline GameObject_t3674682005 ** get_address_of__obj_left_3() { return &____obj_left_3; }
	inline void set__obj_left_3(GameObject_t3674682005 * value)
	{
		____obj_left_3 = value;
		Il2CppCodeGenWriteBarrier(&____obj_left_3, value);
	}

	inline static int32_t get_offset_of__obj_right_4() { return static_cast<int32_t>(offsetof(BossInofShow_t241030, ____obj_right_4)); }
	inline GameObject_t3674682005 * get__obj_right_4() const { return ____obj_right_4; }
	inline GameObject_t3674682005 ** get_address_of__obj_right_4() { return &____obj_right_4; }
	inline void set__obj_right_4(GameObject_t3674682005 * value)
	{
		____obj_right_4 = value;
		Il2CppCodeGenWriteBarrier(&____obj_right_4, value);
	}

	inline static int32_t get_offset_of__boss_name_Left_5() { return static_cast<int32_t>(offsetof(BossInofShow_t241030, ____boss_name_Left_5)); }
	inline UISprite_t661437049 * get__boss_name_Left_5() const { return ____boss_name_Left_5; }
	inline UISprite_t661437049 ** get_address_of__boss_name_Left_5() { return &____boss_name_Left_5; }
	inline void set__boss_name_Left_5(UISprite_t661437049 * value)
	{
		____boss_name_Left_5 = value;
		Il2CppCodeGenWriteBarrier(&____boss_name_Left_5, value);
	}

	inline static int32_t get_offset_of__boss_name_right_6() { return static_cast<int32_t>(offsetof(BossInofShow_t241030, ____boss_name_right_6)); }
	inline UISprite_t661437049 * get__boss_name_right_6() const { return ____boss_name_right_6; }
	inline UISprite_t661437049 ** get_address_of__boss_name_right_6() { return &____boss_name_right_6; }
	inline void set__boss_name_right_6(UISprite_t661437049 * value)
	{
		____boss_name_right_6 = value;
		Il2CppCodeGenWriteBarrier(&____boss_name_right_6, value);
	}

	inline static int32_t get_offset_of_type_7() { return static_cast<int32_t>(offsetof(BossInofShow_t241030, ___type_7)); }
	inline int32_t get_type_7() const { return ___type_7; }
	inline int32_t* get_address_of_type_7() { return &___type_7; }
	inline void set_type_7(int32_t value)
	{
		___type_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
