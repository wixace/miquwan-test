﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AstarDebugger/GraphPoint
struct GraphPoint_t991085213;
struct GraphPoint_t991085213_marshaled_pinvoke;
struct GraphPoint_t991085213_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct GraphPoint_t991085213;
struct GraphPoint_t991085213_marshaled_pinvoke;

extern "C" void GraphPoint_t991085213_marshal_pinvoke(const GraphPoint_t991085213& unmarshaled, GraphPoint_t991085213_marshaled_pinvoke& marshaled);
extern "C" void GraphPoint_t991085213_marshal_pinvoke_back(const GraphPoint_t991085213_marshaled_pinvoke& marshaled, GraphPoint_t991085213& unmarshaled);
extern "C" void GraphPoint_t991085213_marshal_pinvoke_cleanup(GraphPoint_t991085213_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct GraphPoint_t991085213;
struct GraphPoint_t991085213_marshaled_com;

extern "C" void GraphPoint_t991085213_marshal_com(const GraphPoint_t991085213& unmarshaled, GraphPoint_t991085213_marshaled_com& marshaled);
extern "C" void GraphPoint_t991085213_marshal_com_back(const GraphPoint_t991085213_marshaled_com& marshaled, GraphPoint_t991085213& unmarshaled);
extern "C" void GraphPoint_t991085213_marshal_com_cleanup(GraphPoint_t991085213_marshaled_com& marshaled);
