﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUILayoutGenerated/<GUILayout_Window_GetDelegate_member104_arg2>c__AnonStoreyF6
struct U3CGUILayout_Window_GetDelegate_member104_arg2U3Ec__AnonStoreyF6_t413582995;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_GUILayoutGenerated/<GUILayout_Window_GetDelegate_member104_arg2>c__AnonStoreyF6::.ctor()
extern "C"  void U3CGUILayout_Window_GetDelegate_member104_arg2U3Ec__AnonStoreyF6__ctor_m1244748344 (U3CGUILayout_Window_GetDelegate_member104_arg2U3Ec__AnonStoreyF6_t413582995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUILayoutGenerated/<GUILayout_Window_GetDelegate_member104_arg2>c__AnonStoreyF6::<>m__23F(System.Int32)
extern "C"  void U3CGUILayout_Window_GetDelegate_member104_arg2U3Ec__AnonStoreyF6_U3CU3Em__23F_m1818987575 (U3CGUILayout_Window_GetDelegate_member104_arg2U3Ec__AnonStoreyF6_t413582995 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
