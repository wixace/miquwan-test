﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_kobiskereTairha204
struct  M_kobiskereTairha204_t3588696098  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_kobiskereTairha204::_meadrelTrechel
	int32_t ____meadrelTrechel_0;
	// System.Boolean GarbageiOS.M_kobiskereTairha204::_pasesowNunugi
	bool ____pasesowNunugi_1;
	// System.Boolean GarbageiOS.M_kobiskereTairha204::_curhel
	bool ____curhel_2;
	// System.UInt32 GarbageiOS.M_kobiskereTairha204::_wurnu
	uint32_t ____wurnu_3;
	// System.Int32 GarbageiOS.M_kobiskereTairha204::_zawaylearTone
	int32_t ____zawaylearTone_4;
	// System.Boolean GarbageiOS.M_kobiskereTairha204::_tiscedrouCorwerbe
	bool ____tiscedrouCorwerbe_5;
	// System.String GarbageiOS.M_kobiskereTairha204::_houleGawje
	String_t* ____houleGawje_6;
	// System.String GarbageiOS.M_kobiskereTairha204::_yisrallJexur
	String_t* ____yisrallJexur_7;
	// System.Boolean GarbageiOS.M_kobiskereTairha204::_chafeali
	bool ____chafeali_8;
	// System.Int32 GarbageiOS.M_kobiskereTairha204::_pustallBereme
	int32_t ____pustallBereme_9;
	// System.Int32 GarbageiOS.M_kobiskereTairha204::_rallreheeYirzoucall
	int32_t ____rallreheeYirzoucall_10;
	// System.Int32 GarbageiOS.M_kobiskereTairha204::_rouroojo
	int32_t ____rouroojo_11;

public:
	inline static int32_t get_offset_of__meadrelTrechel_0() { return static_cast<int32_t>(offsetof(M_kobiskereTairha204_t3588696098, ____meadrelTrechel_0)); }
	inline int32_t get__meadrelTrechel_0() const { return ____meadrelTrechel_0; }
	inline int32_t* get_address_of__meadrelTrechel_0() { return &____meadrelTrechel_0; }
	inline void set__meadrelTrechel_0(int32_t value)
	{
		____meadrelTrechel_0 = value;
	}

	inline static int32_t get_offset_of__pasesowNunugi_1() { return static_cast<int32_t>(offsetof(M_kobiskereTairha204_t3588696098, ____pasesowNunugi_1)); }
	inline bool get__pasesowNunugi_1() const { return ____pasesowNunugi_1; }
	inline bool* get_address_of__pasesowNunugi_1() { return &____pasesowNunugi_1; }
	inline void set__pasesowNunugi_1(bool value)
	{
		____pasesowNunugi_1 = value;
	}

	inline static int32_t get_offset_of__curhel_2() { return static_cast<int32_t>(offsetof(M_kobiskereTairha204_t3588696098, ____curhel_2)); }
	inline bool get__curhel_2() const { return ____curhel_2; }
	inline bool* get_address_of__curhel_2() { return &____curhel_2; }
	inline void set__curhel_2(bool value)
	{
		____curhel_2 = value;
	}

	inline static int32_t get_offset_of__wurnu_3() { return static_cast<int32_t>(offsetof(M_kobiskereTairha204_t3588696098, ____wurnu_3)); }
	inline uint32_t get__wurnu_3() const { return ____wurnu_3; }
	inline uint32_t* get_address_of__wurnu_3() { return &____wurnu_3; }
	inline void set__wurnu_3(uint32_t value)
	{
		____wurnu_3 = value;
	}

	inline static int32_t get_offset_of__zawaylearTone_4() { return static_cast<int32_t>(offsetof(M_kobiskereTairha204_t3588696098, ____zawaylearTone_4)); }
	inline int32_t get__zawaylearTone_4() const { return ____zawaylearTone_4; }
	inline int32_t* get_address_of__zawaylearTone_4() { return &____zawaylearTone_4; }
	inline void set__zawaylearTone_4(int32_t value)
	{
		____zawaylearTone_4 = value;
	}

	inline static int32_t get_offset_of__tiscedrouCorwerbe_5() { return static_cast<int32_t>(offsetof(M_kobiskereTairha204_t3588696098, ____tiscedrouCorwerbe_5)); }
	inline bool get__tiscedrouCorwerbe_5() const { return ____tiscedrouCorwerbe_5; }
	inline bool* get_address_of__tiscedrouCorwerbe_5() { return &____tiscedrouCorwerbe_5; }
	inline void set__tiscedrouCorwerbe_5(bool value)
	{
		____tiscedrouCorwerbe_5 = value;
	}

	inline static int32_t get_offset_of__houleGawje_6() { return static_cast<int32_t>(offsetof(M_kobiskereTairha204_t3588696098, ____houleGawje_6)); }
	inline String_t* get__houleGawje_6() const { return ____houleGawje_6; }
	inline String_t** get_address_of__houleGawje_6() { return &____houleGawje_6; }
	inline void set__houleGawje_6(String_t* value)
	{
		____houleGawje_6 = value;
		Il2CppCodeGenWriteBarrier(&____houleGawje_6, value);
	}

	inline static int32_t get_offset_of__yisrallJexur_7() { return static_cast<int32_t>(offsetof(M_kobiskereTairha204_t3588696098, ____yisrallJexur_7)); }
	inline String_t* get__yisrallJexur_7() const { return ____yisrallJexur_7; }
	inline String_t** get_address_of__yisrallJexur_7() { return &____yisrallJexur_7; }
	inline void set__yisrallJexur_7(String_t* value)
	{
		____yisrallJexur_7 = value;
		Il2CppCodeGenWriteBarrier(&____yisrallJexur_7, value);
	}

	inline static int32_t get_offset_of__chafeali_8() { return static_cast<int32_t>(offsetof(M_kobiskereTairha204_t3588696098, ____chafeali_8)); }
	inline bool get__chafeali_8() const { return ____chafeali_8; }
	inline bool* get_address_of__chafeali_8() { return &____chafeali_8; }
	inline void set__chafeali_8(bool value)
	{
		____chafeali_8 = value;
	}

	inline static int32_t get_offset_of__pustallBereme_9() { return static_cast<int32_t>(offsetof(M_kobiskereTairha204_t3588696098, ____pustallBereme_9)); }
	inline int32_t get__pustallBereme_9() const { return ____pustallBereme_9; }
	inline int32_t* get_address_of__pustallBereme_9() { return &____pustallBereme_9; }
	inline void set__pustallBereme_9(int32_t value)
	{
		____pustallBereme_9 = value;
	}

	inline static int32_t get_offset_of__rallreheeYirzoucall_10() { return static_cast<int32_t>(offsetof(M_kobiskereTairha204_t3588696098, ____rallreheeYirzoucall_10)); }
	inline int32_t get__rallreheeYirzoucall_10() const { return ____rallreheeYirzoucall_10; }
	inline int32_t* get_address_of__rallreheeYirzoucall_10() { return &____rallreheeYirzoucall_10; }
	inline void set__rallreheeYirzoucall_10(int32_t value)
	{
		____rallreheeYirzoucall_10 = value;
	}

	inline static int32_t get_offset_of__rouroojo_11() { return static_cast<int32_t>(offsetof(M_kobiskereTairha204_t3588696098, ____rouroojo_11)); }
	inline int32_t get__rouroojo_11() const { return ____rouroojo_11; }
	inline int32_t* get_address_of__rouroojo_11() { return &____rouroojo_11; }
	inline void set__rouroojo_11(int32_t value)
	{
		____rouroojo_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
