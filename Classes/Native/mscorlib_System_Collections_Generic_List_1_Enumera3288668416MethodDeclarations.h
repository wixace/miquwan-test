﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<CameraShotMgr/CameraShotVO>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1052690433(__this, ___l0, method) ((  void (*) (Enumerator_t3288668416 *, List_1_t3268995646 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<CameraShotMgr/CameraShotVO>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1486983089(__this, method) ((  void (*) (Enumerator_t3288668416 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<CameraShotMgr/CameraShotVO>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3249726375(__this, method) ((  Il2CppObject * (*) (Enumerator_t3288668416 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<CameraShotMgr/CameraShotVO>::Dispose()
#define Enumerator_Dispose_m1467524646(__this, method) ((  void (*) (Enumerator_t3288668416 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<CameraShotMgr/CameraShotVO>::VerifyState()
#define Enumerator_VerifyState_m1778442847(__this, method) ((  void (*) (Enumerator_t3288668416 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<CameraShotMgr/CameraShotVO>::MoveNext()
#define Enumerator_MoveNext_m1830744673(__this, method) ((  bool (*) (Enumerator_t3288668416 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<CameraShotMgr/CameraShotVO>::get_Current()
#define Enumerator_get_Current_m3491971256(__this, method) ((  CameraShotVO_t1900810094 * (*) (Enumerator_t3288668416 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
