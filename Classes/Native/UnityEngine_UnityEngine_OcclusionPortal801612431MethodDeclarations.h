﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.OcclusionPortal
struct OcclusionPortal_t801612431;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.OcclusionPortal::.ctor()
extern "C"  void OcclusionPortal__ctor_m982893600 (OcclusionPortal_t801612431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.OcclusionPortal::get_open()
extern "C"  bool OcclusionPortal_get_open_m2735656625 (OcclusionPortal_t801612431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.OcclusionPortal::set_open(System.Boolean)
extern "C"  void OcclusionPortal_set_open_m506470402 (OcclusionPortal_t801612431 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
