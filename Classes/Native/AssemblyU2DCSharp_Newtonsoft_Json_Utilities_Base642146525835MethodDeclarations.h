﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.Base64Encoder
struct Base64Encoder_t2146525835;
// System.IO.TextWriter
struct TextWriter_t2304124208;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Char[]
struct CharU5BU5D_t3324145743;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_TextWriter2304124208.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Base642146525835.h"

// System.Void Newtonsoft.Json.Utilities.Base64Encoder::.ctor(System.IO.TextWriter)
extern "C"  void Base64Encoder__ctor_m2259717102 (Base64Encoder_t2146525835 * __this, TextWriter_t2304124208 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.Base64Encoder::Encode(System.Byte[],System.Int32,System.Int32)
extern "C"  void Base64Encoder_Encode_m2944931046 (Base64Encoder_t2146525835 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___index1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.Base64Encoder::Flush()
extern "C"  void Base64Encoder_Flush_m795512185 (Base64Encoder_t2146525835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.Base64Encoder::WriteChars(System.Char[],System.Int32,System.Int32)
extern "C"  void Base64Encoder_WriteChars_m4143291184 (Base64Encoder_t2146525835 * __this, CharU5BU5D_t3324145743* ___chars0, int32_t ___index1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.Base64Encoder::ilo_WriteChars1(Newtonsoft.Json.Utilities.Base64Encoder,System.Char[],System.Int32,System.Int32)
extern "C"  void Base64Encoder_ilo_WriteChars1_m3066835646 (Il2CppObject * __this /* static, unused */, Base64Encoder_t2146525835 * ____this0, CharU5BU5D_t3324145743* ___chars1, int32_t ___index2, int32_t ___count3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
