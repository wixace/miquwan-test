﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginXiaoNiu
struct PluginXiaoNiu_t2024052712;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// VersionInfo
struct VersionInfo_t2356638086;
// Mihua.SDK.SdkwwwCallBack
struct SdkwwwCallBack_t3004559384;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// Mihua.SDK.CreatRoleInfo
struct CreatRoleInfo_t2858846543;
// System.Action
struct Action_t3771233898;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t2362096582;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_PluginXiaoNiu2024052712.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "UnityEngine_UnityEngine_WWWForm461342257.h"
#include "AssemblyU2DCSharp_Mihua_SDK_SdkwwwCallBack3004559384.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "AssemblyU2DCSharp_FloatTextMgr630384591.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginXiaoNiu::.ctor()
extern "C"  void PluginXiaoNiu__ctor_m2598252163 (PluginXiaoNiu_t2024052712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::Init()
extern "C"  void PluginXiaoNiu_Init_m2390655857 (PluginXiaoNiu_t2024052712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginXiaoNiu::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginXiaoNiu_ReqSDKHttpLogin_m1725071540 (PluginXiaoNiu_t2024052712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXiaoNiu::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginXiaoNiu_IsLoginSuccess_m3552273032 (PluginXiaoNiu_t2024052712 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::OpenUserLogin()
extern "C"  void PluginXiaoNiu_OpenUserLogin_m1099832789 (PluginXiaoNiu_t2024052712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::UserPay(CEvent.ZEvent)
extern "C"  void PluginXiaoNiu_UserPay_m1216687101 (PluginXiaoNiu_t2024052712 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::SignCallBack(System.Boolean,System.String)
extern "C"  void PluginXiaoNiu_SignCallBack_m3155132950 (PluginXiaoNiu_t2024052712 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginXiaoNiu::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginXiaoNiu_BuildOrderParam2WWWForm_m602027136 (PluginXiaoNiu_t2024052712 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::CreateRole(CEvent.ZEvent)
extern "C"  void PluginXiaoNiu_CreateRole_m3231551976 (PluginXiaoNiu_t2024052712 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::EnterGame(CEvent.ZEvent)
extern "C"  void PluginXiaoNiu_EnterGame_m3221746256 (PluginXiaoNiu_t2024052712 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginXiaoNiu_RoleUpgrade_m4057161972 (PluginXiaoNiu_t2024052712 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::OnLoginSuccess(System.String)
extern "C"  void PluginXiaoNiu_OnLoginSuccess_m2380730696 (PluginXiaoNiu_t2024052712 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::OnLogoutSuccess(System.String)
extern "C"  void PluginXiaoNiu_OnLogoutSuccess_m791750215 (PluginXiaoNiu_t2024052712 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::OnLogout(System.String)
extern "C"  void PluginXiaoNiu_OnLogout_m801483480 (PluginXiaoNiu_t2024052712 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::SwitchAccount(System.String)
extern "C"  void PluginXiaoNiu_SwitchAccount_m3932607368 (PluginXiaoNiu_t2024052712 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::initSdk(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginXiaoNiu_initSdk_m1146743163 (PluginXiaoNiu_t2024052712 * __this, String_t* ___gameVersion0, String_t* ___registerSource1, String_t* ___appid2, String_t* ___appname3, String_t* ___appkey4, String_t* ___channel5, String_t* ___trackingkey6, String_t* ___tTTrackerkey7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::login()
extern "C"  void PluginXiaoNiu_login_m2120266986 (PluginXiaoNiu_t2024052712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::logout()
extern "C"  void PluginXiaoNiu_logout_m1309589835 (PluginXiaoNiu_t2024052712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::creatRole(System.String,System.String,System.String,System.String)
extern "C"  void PluginXiaoNiu_creatRole_m3646328214 (PluginXiaoNiu_t2024052712 * __this, String_t* ___role_id0, String_t* ___role_name1, String_t* ___server_name2, String_t* ___role_level3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::roleUpgrade(System.String,System.String,System.String,System.String)
extern "C"  void PluginXiaoNiu_roleUpgrade_m1156995919 (PluginXiaoNiu_t2024052712 * __this, String_t* ___role_id0, String_t* ___role_name1, String_t* ___server_name2, String_t* ___role_level3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginXiaoNiu_pay_m2005522309 (PluginXiaoNiu_t2024052712 * __this, String_t* ___prodName0, String_t* ___prodCode1, String_t* ___prodNum2, String_t* ___prodPrice3, String_t* ___payFee4, String_t* ___payNotifyUrl5, String_t* ___cpOrderId6, String_t* ___roleLevel7, String_t* ___roleId8, String_t* ___roleName9, String_t* ___serverName10, String_t* ___extra11, String_t* ___timeStamp12, String_t* ___cpSign13, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::<OnLogoutSuccess>m__467()
extern "C"  void PluginXiaoNiu_U3COnLogoutSuccessU3Em__467_m2067729385 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::<OnLogout>m__468()
extern "C"  void PluginXiaoNiu_U3COnLogoutU3Em__468_m2571496707 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::<SwitchAccount>m__469()
extern "C"  void PluginXiaoNiu_U3CSwitchAccountU3Em__469_m1878344812 (PluginXiaoNiu_t2024052712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginXiaoNiu_ilo_AddEventListener1_m1460469265 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginXiaoNiu::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginXiaoNiu_ilo_get_Instance2_m1442321055 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginXiaoNiu::ilo_get_DeviceID3(PluginsSdkMgr)
extern "C"  String_t* PluginXiaoNiu_ilo_get_DeviceID3_m1069911978 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::ilo_OpenUserLogin4(PluginXiaoNiu)
extern "C"  void PluginXiaoNiu_ilo_OpenUserLogin4_m534272310 (Il2CppObject * __this /* static, unused */, PluginXiaoNiu_t2024052712 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::ilo_login5(PluginXiaoNiu)
extern "C"  void PluginXiaoNiu_ilo_login5_m2529706602 (Il2CppObject * __this /* static, unused */, PluginXiaoNiu_t2024052712 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.PayInfo PluginXiaoNiu::ilo_Parse6(System.Object[])
extern "C"  PayInfo_t1775308120 * PluginXiaoNiu_ilo_Parse6_m1217146338 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___payOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginXiaoNiu::ilo_get_currentVS7(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginXiaoNiu_ilo_get_currentVS7_m1572141905 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::ilo_Request8(System.String,UnityEngine.WWWForm,Mihua.SDK.SdkwwwCallBack)
extern "C"  void PluginXiaoNiu_ilo_Request8_m1922585037 (Il2CppObject * __this /* static, unused */, String_t* ___url0, WWWForm_t461342257 * ___wwwForm1, SdkwwwCallBack_t3004559384 * ___callBack2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::ilo_Log9(System.Object,System.Boolean)
extern "C"  void PluginXiaoNiu_ilo_Log9_m2593745522 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::ilo_LogError10(System.Object,System.Boolean)
extern "C"  void PluginXiaoNiu_ilo_LogError10_m3268547712 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginXiaoNiu::ilo_ToString11(Newtonsoft.Json.Linq.JToken)
extern "C"  String_t* PluginXiaoNiu_ilo_ToString11_m3804378093 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::ilo_pay12(PluginXiaoNiu,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginXiaoNiu_ilo_pay12_m2682668603 (Il2CppObject * __this /* static, unused */, PluginXiaoNiu_t2024052712 * ____this0, String_t* ___prodName1, String_t* ___prodCode2, String_t* ___prodNum3, String_t* ___prodPrice4, String_t* ___payFee5, String_t* ___payNotifyUrl6, String_t* ___cpOrderId7, String_t* ___roleLevel8, String_t* ___roleId9, String_t* ___roleName10, String_t* ___serverName11, String_t* ___extra12, String_t* ___timeStamp13, String_t* ___cpSign14, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::ilo_FloatText13(FloatTextMgr,FLOAT_TEXT_ID,System.Int32,System.Object[])
extern "C"  void PluginXiaoNiu_ilo_FloatText13_m2081260375 (Il2CppObject * __this /* static, unused */, FloatTextMgr_t630384591 * ____this0, int32_t ___textId1, int32_t ___offsetY2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginXiaoNiu::ilo_GetProductID14(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginXiaoNiu_ilo_GetProductID14_m3262497740 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.CreatRoleInfo PluginXiaoNiu::ilo_Parse15(System.Object[])
extern "C"  CreatRoleInfo_t2858846543 * PluginXiaoNiu_ilo_Parse15_m2932918973 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::ilo_roleUpgrade16(PluginXiaoNiu,System.String,System.String,System.String,System.String)
extern "C"  void PluginXiaoNiu_ilo_roleUpgrade16_m2486433665 (Il2CppObject * __this /* static, unused */, PluginXiaoNiu_t2024052712 * ____this0, String_t* ___role_id1, String_t* ___role_name2, String_t* ___server_name3, String_t* ___role_level4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::ilo_ReqSDKHttpLogin17(PluginsSdkMgr)
extern "C"  void PluginXiaoNiu_ilo_ReqSDKHttpLogin17_m4116932059 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoNiu::ilo_Logout18(System.Action)
extern "C"  void PluginXiaoNiu_ilo_Logout18_m955836376 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginXiaoNiu::ilo_get_PluginsSdkMgr19()
extern "C"  PluginsSdkMgr_t3884624670 * PluginXiaoNiu_ilo_get_PluginsSdkMgr19_m3539018624 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AndroidJavaObject PluginXiaoNiu::ilo_get_androidJavaObject20(PluginsSdkMgr)
extern "C"  AndroidJavaObject_t2362096582 * PluginXiaoNiu_ilo_get_androidJavaObject20_m3235447975 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
