﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1826376502MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1969362533(__this, ___dictionary0, method) ((  void (*) (Enumerator_t476755676 *, Dictionary_2_t3454399580 *, const MethodInfo*))Enumerator__ctor_m1912791287_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1030499430(__this, method) ((  Il2CppObject * (*) (Enumerator_t476755676 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1259720724_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2862916912(__this, method) ((  void (*) (Enumerator_t476755676 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m310780766_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m55122087(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t476755676 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2971872853_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4024829122(__this, method) ((  Il2CppObject * (*) (Enumerator_t476755676 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3875909104_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3438656212(__this, method) ((  Il2CppObject * (*) (Enumerator_t476755676 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2060439682_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::MoveNext()
#define Enumerator_MoveNext_m1123136864(__this, method) ((  bool (*) (Enumerator_t476755676 *, const MethodInfo*))Enumerator_MoveNext_m119841934_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Current()
#define Enumerator_get_Current_m1938106076(__this, method) ((  KeyValuePair_2_t3353180286  (*) (Enumerator_t476755676 *, const MethodInfo*))Enumerator_get_Current_m1456259310_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2479747433(__this, method) ((  String_t* (*) (Enumerator_t476755676 *, const MethodInfo*))Enumerator_get_CurrentKey_m1563444759_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m887337705(__this, method) ((  ErrorInfo_t2633981210  (*) (Enumerator_t476755676 *, const MethodInfo*))Enumerator_get_CurrentValue_m788763671_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::Reset()
#define Enumerator_Reset_m2170612023(__this, method) ((  void (*) (Enumerator_t476755676 *, const MethodInfo*))Enumerator_Reset_m4037146825_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::VerifyState()
#define Enumerator_VerifyState_m349713408(__this, method) ((  void (*) (Enumerator_t476755676 *, const MethodInfo*))Enumerator_VerifyState_m4162833938_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3812007784(__this, method) ((  void (*) (Enumerator_t476755676 *, const MethodInfo*))Enumerator_VerifyCurrent_m318766330_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::Dispose()
#define Enumerator_Dispose_m1127477319(__this, method) ((  void (*) (Enumerator_t476755676 *, const MethodInfo*))Enumerator_Dispose_m3866059609_gshared)(__this, method)
