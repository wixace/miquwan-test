﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_drerekereta39
struct  M_drerekereta39_t2722885482  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_drerekereta39::_bearloo
	uint32_t ____bearloo_0;
	// System.Boolean GarbageiOS.M_drerekereta39::_sousou
	bool ____sousou_1;
	// System.UInt32 GarbageiOS.M_drerekereta39::_danaldirDouchila
	uint32_t ____danaldirDouchila_2;
	// System.Boolean GarbageiOS.M_drerekereta39::_casou
	bool ____casou_3;
	// System.UInt32 GarbageiOS.M_drerekereta39::_sobeebeBawteestis
	uint32_t ____sobeebeBawteestis_4;
	// System.UInt32 GarbageiOS.M_drerekereta39::_qaihi
	uint32_t ____qaihi_5;
	// System.Boolean GarbageiOS.M_drerekereta39::_tenejuLacea
	bool ____tenejuLacea_6;
	// System.Boolean GarbageiOS.M_drerekereta39::_gatouhouKurdem
	bool ____gatouhouKurdem_7;
	// System.Single GarbageiOS.M_drerekereta39::_gakea
	float ____gakea_8;
	// System.Single GarbageiOS.M_drerekereta39::_whafipuHotoowee
	float ____whafipuHotoowee_9;
	// System.UInt32 GarbageiOS.M_drerekereta39::_tabasJekaystou
	uint32_t ____tabasJekaystou_10;
	// System.Single GarbageiOS.M_drerekereta39::_faykuWernisree
	float ____faykuWernisree_11;
	// System.Int32 GarbageiOS.M_drerekereta39::_neaxeldre
	int32_t ____neaxeldre_12;
	// System.String GarbageiOS.M_drerekereta39::_delfeaRekow
	String_t* ____delfeaRekow_13;
	// System.UInt32 GarbageiOS.M_drerekereta39::_morjayqir
	uint32_t ____morjayqir_14;
	// System.Int32 GarbageiOS.M_drerekereta39::_tropel
	int32_t ____tropel_15;
	// System.Boolean GarbageiOS.M_drerekereta39::_bupis
	bool ____bupis_16;
	// System.Int32 GarbageiOS.M_drerekereta39::_celgi
	int32_t ____celgi_17;
	// System.Int32 GarbageiOS.M_drerekereta39::_mempicalSiscis
	int32_t ____mempicalSiscis_18;
	// System.Boolean GarbageiOS.M_drerekereta39::_meheawoChallwa
	bool ____meheawoChallwa_19;
	// System.Int32 GarbageiOS.M_drerekereta39::_qasmisairTade
	int32_t ____qasmisairTade_20;
	// System.Single GarbageiOS.M_drerekereta39::_petuCekaha
	float ____petuCekaha_21;
	// System.UInt32 GarbageiOS.M_drerekereta39::_pallsoukeSabalmu
	uint32_t ____pallsoukeSabalmu_22;
	// System.UInt32 GarbageiOS.M_drerekereta39::_nanowdraSurlawyu
	uint32_t ____nanowdraSurlawyu_23;
	// System.Single GarbageiOS.M_drerekereta39::_literenir
	float ____literenir_24;
	// System.Int32 GarbageiOS.M_drerekereta39::_bedibe
	int32_t ____bedibe_25;
	// System.Boolean GarbageiOS.M_drerekereta39::_couyemGihor
	bool ____couyemGihor_26;
	// System.String GarbageiOS.M_drerekereta39::_drelstisvouGarmee
	String_t* ____drelstisvouGarmee_27;
	// System.Boolean GarbageiOS.M_drerekereta39::_jurtayfou
	bool ____jurtayfou_28;
	// System.Int32 GarbageiOS.M_drerekereta39::_cerbeCajea
	int32_t ____cerbeCajea_29;
	// System.Single GarbageiOS.M_drerekereta39::_meru
	float ____meru_30;
	// System.Int32 GarbageiOS.M_drerekereta39::_qoonu
	int32_t ____qoonu_31;
	// System.UInt32 GarbageiOS.M_drerekereta39::_wesorka
	uint32_t ____wesorka_32;
	// System.UInt32 GarbageiOS.M_drerekereta39::_jemneawuHerko
	uint32_t ____jemneawuHerko_33;
	// System.Single GarbageiOS.M_drerekereta39::_sissalStuheretall
	float ____sissalStuheretall_34;
	// System.Single GarbageiOS.M_drerekereta39::_mallsea
	float ____mallsea_35;
	// System.Single GarbageiOS.M_drerekereta39::_gasqa
	float ____gasqa_36;
	// System.UInt32 GarbageiOS.M_drerekereta39::_vedra
	uint32_t ____vedra_37;
	// System.String GarbageiOS.M_drerekereta39::_yasowRacas
	String_t* ____yasowRacas_38;
	// System.String GarbageiOS.M_drerekereta39::_locouqear
	String_t* ____locouqear_39;

public:
	inline static int32_t get_offset_of__bearloo_0() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____bearloo_0)); }
	inline uint32_t get__bearloo_0() const { return ____bearloo_0; }
	inline uint32_t* get_address_of__bearloo_0() { return &____bearloo_0; }
	inline void set__bearloo_0(uint32_t value)
	{
		____bearloo_0 = value;
	}

	inline static int32_t get_offset_of__sousou_1() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____sousou_1)); }
	inline bool get__sousou_1() const { return ____sousou_1; }
	inline bool* get_address_of__sousou_1() { return &____sousou_1; }
	inline void set__sousou_1(bool value)
	{
		____sousou_1 = value;
	}

	inline static int32_t get_offset_of__danaldirDouchila_2() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____danaldirDouchila_2)); }
	inline uint32_t get__danaldirDouchila_2() const { return ____danaldirDouchila_2; }
	inline uint32_t* get_address_of__danaldirDouchila_2() { return &____danaldirDouchila_2; }
	inline void set__danaldirDouchila_2(uint32_t value)
	{
		____danaldirDouchila_2 = value;
	}

	inline static int32_t get_offset_of__casou_3() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____casou_3)); }
	inline bool get__casou_3() const { return ____casou_3; }
	inline bool* get_address_of__casou_3() { return &____casou_3; }
	inline void set__casou_3(bool value)
	{
		____casou_3 = value;
	}

	inline static int32_t get_offset_of__sobeebeBawteestis_4() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____sobeebeBawteestis_4)); }
	inline uint32_t get__sobeebeBawteestis_4() const { return ____sobeebeBawteestis_4; }
	inline uint32_t* get_address_of__sobeebeBawteestis_4() { return &____sobeebeBawteestis_4; }
	inline void set__sobeebeBawteestis_4(uint32_t value)
	{
		____sobeebeBawteestis_4 = value;
	}

	inline static int32_t get_offset_of__qaihi_5() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____qaihi_5)); }
	inline uint32_t get__qaihi_5() const { return ____qaihi_5; }
	inline uint32_t* get_address_of__qaihi_5() { return &____qaihi_5; }
	inline void set__qaihi_5(uint32_t value)
	{
		____qaihi_5 = value;
	}

	inline static int32_t get_offset_of__tenejuLacea_6() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____tenejuLacea_6)); }
	inline bool get__tenejuLacea_6() const { return ____tenejuLacea_6; }
	inline bool* get_address_of__tenejuLacea_6() { return &____tenejuLacea_6; }
	inline void set__tenejuLacea_6(bool value)
	{
		____tenejuLacea_6 = value;
	}

	inline static int32_t get_offset_of__gatouhouKurdem_7() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____gatouhouKurdem_7)); }
	inline bool get__gatouhouKurdem_7() const { return ____gatouhouKurdem_7; }
	inline bool* get_address_of__gatouhouKurdem_7() { return &____gatouhouKurdem_7; }
	inline void set__gatouhouKurdem_7(bool value)
	{
		____gatouhouKurdem_7 = value;
	}

	inline static int32_t get_offset_of__gakea_8() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____gakea_8)); }
	inline float get__gakea_8() const { return ____gakea_8; }
	inline float* get_address_of__gakea_8() { return &____gakea_8; }
	inline void set__gakea_8(float value)
	{
		____gakea_8 = value;
	}

	inline static int32_t get_offset_of__whafipuHotoowee_9() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____whafipuHotoowee_9)); }
	inline float get__whafipuHotoowee_9() const { return ____whafipuHotoowee_9; }
	inline float* get_address_of__whafipuHotoowee_9() { return &____whafipuHotoowee_9; }
	inline void set__whafipuHotoowee_9(float value)
	{
		____whafipuHotoowee_9 = value;
	}

	inline static int32_t get_offset_of__tabasJekaystou_10() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____tabasJekaystou_10)); }
	inline uint32_t get__tabasJekaystou_10() const { return ____tabasJekaystou_10; }
	inline uint32_t* get_address_of__tabasJekaystou_10() { return &____tabasJekaystou_10; }
	inline void set__tabasJekaystou_10(uint32_t value)
	{
		____tabasJekaystou_10 = value;
	}

	inline static int32_t get_offset_of__faykuWernisree_11() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____faykuWernisree_11)); }
	inline float get__faykuWernisree_11() const { return ____faykuWernisree_11; }
	inline float* get_address_of__faykuWernisree_11() { return &____faykuWernisree_11; }
	inline void set__faykuWernisree_11(float value)
	{
		____faykuWernisree_11 = value;
	}

	inline static int32_t get_offset_of__neaxeldre_12() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____neaxeldre_12)); }
	inline int32_t get__neaxeldre_12() const { return ____neaxeldre_12; }
	inline int32_t* get_address_of__neaxeldre_12() { return &____neaxeldre_12; }
	inline void set__neaxeldre_12(int32_t value)
	{
		____neaxeldre_12 = value;
	}

	inline static int32_t get_offset_of__delfeaRekow_13() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____delfeaRekow_13)); }
	inline String_t* get__delfeaRekow_13() const { return ____delfeaRekow_13; }
	inline String_t** get_address_of__delfeaRekow_13() { return &____delfeaRekow_13; }
	inline void set__delfeaRekow_13(String_t* value)
	{
		____delfeaRekow_13 = value;
		Il2CppCodeGenWriteBarrier(&____delfeaRekow_13, value);
	}

	inline static int32_t get_offset_of__morjayqir_14() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____morjayqir_14)); }
	inline uint32_t get__morjayqir_14() const { return ____morjayqir_14; }
	inline uint32_t* get_address_of__morjayqir_14() { return &____morjayqir_14; }
	inline void set__morjayqir_14(uint32_t value)
	{
		____morjayqir_14 = value;
	}

	inline static int32_t get_offset_of__tropel_15() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____tropel_15)); }
	inline int32_t get__tropel_15() const { return ____tropel_15; }
	inline int32_t* get_address_of__tropel_15() { return &____tropel_15; }
	inline void set__tropel_15(int32_t value)
	{
		____tropel_15 = value;
	}

	inline static int32_t get_offset_of__bupis_16() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____bupis_16)); }
	inline bool get__bupis_16() const { return ____bupis_16; }
	inline bool* get_address_of__bupis_16() { return &____bupis_16; }
	inline void set__bupis_16(bool value)
	{
		____bupis_16 = value;
	}

	inline static int32_t get_offset_of__celgi_17() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____celgi_17)); }
	inline int32_t get__celgi_17() const { return ____celgi_17; }
	inline int32_t* get_address_of__celgi_17() { return &____celgi_17; }
	inline void set__celgi_17(int32_t value)
	{
		____celgi_17 = value;
	}

	inline static int32_t get_offset_of__mempicalSiscis_18() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____mempicalSiscis_18)); }
	inline int32_t get__mempicalSiscis_18() const { return ____mempicalSiscis_18; }
	inline int32_t* get_address_of__mempicalSiscis_18() { return &____mempicalSiscis_18; }
	inline void set__mempicalSiscis_18(int32_t value)
	{
		____mempicalSiscis_18 = value;
	}

	inline static int32_t get_offset_of__meheawoChallwa_19() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____meheawoChallwa_19)); }
	inline bool get__meheawoChallwa_19() const { return ____meheawoChallwa_19; }
	inline bool* get_address_of__meheawoChallwa_19() { return &____meheawoChallwa_19; }
	inline void set__meheawoChallwa_19(bool value)
	{
		____meheawoChallwa_19 = value;
	}

	inline static int32_t get_offset_of__qasmisairTade_20() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____qasmisairTade_20)); }
	inline int32_t get__qasmisairTade_20() const { return ____qasmisairTade_20; }
	inline int32_t* get_address_of__qasmisairTade_20() { return &____qasmisairTade_20; }
	inline void set__qasmisairTade_20(int32_t value)
	{
		____qasmisairTade_20 = value;
	}

	inline static int32_t get_offset_of__petuCekaha_21() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____petuCekaha_21)); }
	inline float get__petuCekaha_21() const { return ____petuCekaha_21; }
	inline float* get_address_of__petuCekaha_21() { return &____petuCekaha_21; }
	inline void set__petuCekaha_21(float value)
	{
		____petuCekaha_21 = value;
	}

	inline static int32_t get_offset_of__pallsoukeSabalmu_22() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____pallsoukeSabalmu_22)); }
	inline uint32_t get__pallsoukeSabalmu_22() const { return ____pallsoukeSabalmu_22; }
	inline uint32_t* get_address_of__pallsoukeSabalmu_22() { return &____pallsoukeSabalmu_22; }
	inline void set__pallsoukeSabalmu_22(uint32_t value)
	{
		____pallsoukeSabalmu_22 = value;
	}

	inline static int32_t get_offset_of__nanowdraSurlawyu_23() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____nanowdraSurlawyu_23)); }
	inline uint32_t get__nanowdraSurlawyu_23() const { return ____nanowdraSurlawyu_23; }
	inline uint32_t* get_address_of__nanowdraSurlawyu_23() { return &____nanowdraSurlawyu_23; }
	inline void set__nanowdraSurlawyu_23(uint32_t value)
	{
		____nanowdraSurlawyu_23 = value;
	}

	inline static int32_t get_offset_of__literenir_24() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____literenir_24)); }
	inline float get__literenir_24() const { return ____literenir_24; }
	inline float* get_address_of__literenir_24() { return &____literenir_24; }
	inline void set__literenir_24(float value)
	{
		____literenir_24 = value;
	}

	inline static int32_t get_offset_of__bedibe_25() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____bedibe_25)); }
	inline int32_t get__bedibe_25() const { return ____bedibe_25; }
	inline int32_t* get_address_of__bedibe_25() { return &____bedibe_25; }
	inline void set__bedibe_25(int32_t value)
	{
		____bedibe_25 = value;
	}

	inline static int32_t get_offset_of__couyemGihor_26() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____couyemGihor_26)); }
	inline bool get__couyemGihor_26() const { return ____couyemGihor_26; }
	inline bool* get_address_of__couyemGihor_26() { return &____couyemGihor_26; }
	inline void set__couyemGihor_26(bool value)
	{
		____couyemGihor_26 = value;
	}

	inline static int32_t get_offset_of__drelstisvouGarmee_27() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____drelstisvouGarmee_27)); }
	inline String_t* get__drelstisvouGarmee_27() const { return ____drelstisvouGarmee_27; }
	inline String_t** get_address_of__drelstisvouGarmee_27() { return &____drelstisvouGarmee_27; }
	inline void set__drelstisvouGarmee_27(String_t* value)
	{
		____drelstisvouGarmee_27 = value;
		Il2CppCodeGenWriteBarrier(&____drelstisvouGarmee_27, value);
	}

	inline static int32_t get_offset_of__jurtayfou_28() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____jurtayfou_28)); }
	inline bool get__jurtayfou_28() const { return ____jurtayfou_28; }
	inline bool* get_address_of__jurtayfou_28() { return &____jurtayfou_28; }
	inline void set__jurtayfou_28(bool value)
	{
		____jurtayfou_28 = value;
	}

	inline static int32_t get_offset_of__cerbeCajea_29() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____cerbeCajea_29)); }
	inline int32_t get__cerbeCajea_29() const { return ____cerbeCajea_29; }
	inline int32_t* get_address_of__cerbeCajea_29() { return &____cerbeCajea_29; }
	inline void set__cerbeCajea_29(int32_t value)
	{
		____cerbeCajea_29 = value;
	}

	inline static int32_t get_offset_of__meru_30() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____meru_30)); }
	inline float get__meru_30() const { return ____meru_30; }
	inline float* get_address_of__meru_30() { return &____meru_30; }
	inline void set__meru_30(float value)
	{
		____meru_30 = value;
	}

	inline static int32_t get_offset_of__qoonu_31() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____qoonu_31)); }
	inline int32_t get__qoonu_31() const { return ____qoonu_31; }
	inline int32_t* get_address_of__qoonu_31() { return &____qoonu_31; }
	inline void set__qoonu_31(int32_t value)
	{
		____qoonu_31 = value;
	}

	inline static int32_t get_offset_of__wesorka_32() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____wesorka_32)); }
	inline uint32_t get__wesorka_32() const { return ____wesorka_32; }
	inline uint32_t* get_address_of__wesorka_32() { return &____wesorka_32; }
	inline void set__wesorka_32(uint32_t value)
	{
		____wesorka_32 = value;
	}

	inline static int32_t get_offset_of__jemneawuHerko_33() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____jemneawuHerko_33)); }
	inline uint32_t get__jemneawuHerko_33() const { return ____jemneawuHerko_33; }
	inline uint32_t* get_address_of__jemneawuHerko_33() { return &____jemneawuHerko_33; }
	inline void set__jemneawuHerko_33(uint32_t value)
	{
		____jemneawuHerko_33 = value;
	}

	inline static int32_t get_offset_of__sissalStuheretall_34() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____sissalStuheretall_34)); }
	inline float get__sissalStuheretall_34() const { return ____sissalStuheretall_34; }
	inline float* get_address_of__sissalStuheretall_34() { return &____sissalStuheretall_34; }
	inline void set__sissalStuheretall_34(float value)
	{
		____sissalStuheretall_34 = value;
	}

	inline static int32_t get_offset_of__mallsea_35() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____mallsea_35)); }
	inline float get__mallsea_35() const { return ____mallsea_35; }
	inline float* get_address_of__mallsea_35() { return &____mallsea_35; }
	inline void set__mallsea_35(float value)
	{
		____mallsea_35 = value;
	}

	inline static int32_t get_offset_of__gasqa_36() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____gasqa_36)); }
	inline float get__gasqa_36() const { return ____gasqa_36; }
	inline float* get_address_of__gasqa_36() { return &____gasqa_36; }
	inline void set__gasqa_36(float value)
	{
		____gasqa_36 = value;
	}

	inline static int32_t get_offset_of__vedra_37() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____vedra_37)); }
	inline uint32_t get__vedra_37() const { return ____vedra_37; }
	inline uint32_t* get_address_of__vedra_37() { return &____vedra_37; }
	inline void set__vedra_37(uint32_t value)
	{
		____vedra_37 = value;
	}

	inline static int32_t get_offset_of__yasowRacas_38() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____yasowRacas_38)); }
	inline String_t* get__yasowRacas_38() const { return ____yasowRacas_38; }
	inline String_t** get_address_of__yasowRacas_38() { return &____yasowRacas_38; }
	inline void set__yasowRacas_38(String_t* value)
	{
		____yasowRacas_38 = value;
		Il2CppCodeGenWriteBarrier(&____yasowRacas_38, value);
	}

	inline static int32_t get_offset_of__locouqear_39() { return static_cast<int32_t>(offsetof(M_drerekereta39_t2722885482, ____locouqear_39)); }
	inline String_t* get__locouqear_39() const { return ____locouqear_39; }
	inline String_t** get_address_of__locouqear_39() { return &____locouqear_39; }
	inline void set__locouqear_39(String_t* value)
	{
		____locouqear_39 = value;
		Il2CppCodeGenWriteBarrier(&____locouqear_39, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
