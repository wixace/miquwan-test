﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,SoundStatus>
struct ShimEnumerator_t3305980211;
// System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>
struct Dictionary_2_t3590202184;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,SoundStatus>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m2328347194_gshared (ShimEnumerator_t3305980211 * __this, Dictionary_2_t3590202184 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m2328347194(__this, ___host0, method) ((  void (*) (ShimEnumerator_t3305980211 *, Dictionary_2_t3590202184 *, const MethodInfo*))ShimEnumerator__ctor_m2328347194_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,SoundStatus>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3964285419_gshared (ShimEnumerator_t3305980211 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m3964285419(__this, method) ((  bool (*) (ShimEnumerator_t3305980211 *, const MethodInfo*))ShimEnumerator_MoveNext_m3964285419_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,SoundStatus>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m805845855_gshared (ShimEnumerator_t3305980211 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m805845855(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t3305980211 *, const MethodInfo*))ShimEnumerator_get_Entry_m805845855_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,SoundStatus>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m430474718_gshared (ShimEnumerator_t3305980211 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m430474718(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3305980211 *, const MethodInfo*))ShimEnumerator_get_Key_m430474718_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,SoundStatus>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2417779952_gshared (ShimEnumerator_t3305980211 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m2417779952(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3305980211 *, const MethodInfo*))ShimEnumerator_get_Value_m2417779952_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,SoundStatus>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1333917560_gshared (ShimEnumerator_t3305980211 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1333917560(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3305980211 *, const MethodInfo*))ShimEnumerator_get_Current_m1333917560_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,SoundStatus>::Reset()
extern "C"  void ShimEnumerator_Reset_m2733614476_gshared (ShimEnumerator_t3305980211 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m2733614476(__this, method) ((  void (*) (ShimEnumerator_t3305980211 *, const MethodInfo*))ShimEnumerator_Reset_m2733614476_gshared)(__this, method)
