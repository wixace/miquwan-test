﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BMSymbol
struct BMSymbol_t1170982339;
// UIAtlas
struct UIAtlas_t281921111;
// UISpriteData
struct UISpriteData_t3578345923;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "AssemblyU2DCSharp_UIAtlas281921111.h"
#include "mscorlib_System_String7231557.h"

// System.Void BMSymbol::.ctor()
extern "C"  void BMSymbol__ctor_m2937308216 (BMSymbol_t1170982339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BMSymbol::get_length()
extern "C"  int32_t BMSymbol_get_length_m2924452201 (BMSymbol_t1170982339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BMSymbol::get_offsetX()
extern "C"  int32_t BMSymbol_get_offsetX_m2899125028 (BMSymbol_t1170982339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BMSymbol::get_offsetY()
extern "C"  int32_t BMSymbol_get_offsetY_m2899125989 (BMSymbol_t1170982339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BMSymbol::get_width()
extern "C"  int32_t BMSymbol_get_width_m3035166213 (BMSymbol_t1170982339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BMSymbol::get_height()
extern "C"  int32_t BMSymbol_get_height_m104686442 (BMSymbol_t1170982339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BMSymbol::get_advance()
extern "C"  int32_t BMSymbol_get_advance_m4050680385 (BMSymbol_t1170982339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect BMSymbol::get_uvRect()
extern "C"  Rect_t4241904616  BMSymbol_get_uvRect_m3961952604 (BMSymbol_t1170982339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMSymbol::MarkAsChanged()
extern "C"  void BMSymbol_MarkAsChanged_m50955499 (BMSymbol_t1170982339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BMSymbol::Validate(UIAtlas)
extern "C"  bool BMSymbol_Validate_m3490388837 (BMSymbol_t1170982339 * __this, UIAtlas_t281921111 * ___atlas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UISpriteData BMSymbol::ilo_GetSprite1(UIAtlas,System.String)
extern "C"  UISpriteData_t3578345923 * BMSymbol_ilo_GetSprite1_m3296017064 (Il2CppObject * __this /* static, unused */, UIAtlas_t281921111 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
