﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginXunFei
struct PluginXunFei_t2016385516;
// System.String
struct String_t;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Action
struct Action_t3771233898;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "System_Core_System_Action3771233898.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_PluginXunFei2016385516.h"

// System.Void PluginXunFei::.ctor()
extern "C"  void PluginXunFei__ctor_m3972579887 (PluginXunFei_t2016385516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginXunFei::get_Token_XunFei_Android()
extern "C"  String_t* PluginXunFei_get_Token_XunFei_Android_m4094187920 (PluginXunFei_t2016385516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::Init()
extern "C"  void PluginXunFei_Init_m2019347013 (PluginXunFei_t2016385516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::OnDestory()
extern "C"  void PluginXunFei_OnDestory_m2295791170 (PluginXunFei_t2016385516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::CreateGroup(CEvent.ZEvent)
extern "C"  void PluginXunFei_CreateGroup_m2121045515 (PluginXunFei_t2016385516 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::JoinChat(CEvent.ZEvent)
extern "C"  void PluginXunFei_JoinChat_m3192507268 (PluginXunFei_t2016385516 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PluginXunFei::DelayCall(System.Single,System.Action)
extern "C"  Il2CppObject * PluginXunFei_DelayCall_m497743644 (PluginXunFei_t2016385516 * __this, float ___delay0, Action_t3771233898 * ___call1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::QuitChat(CEvent.ZEvent)
extern "C"  void PluginXunFei_QuitChat_m1519542847 (PluginXunFei_t2016385516 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::RoleEnterGame(CEvent.ZEvent)
extern "C"  void PluginXunFei_RoleEnterGame_m3240498362 (PluginXunFei_t2016385516 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::OnLoginSuccessIMClient(System.String)
extern "C"  void PluginXunFei_OnLoginSuccessIMClient_m246356741 (PluginXunFei_t2016385516 * __this, String_t* ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::OnCreateGroupSuccessIMClient(System.String)
extern "C"  void PluginXunFei_OnCreateGroupSuccessIMClient_m3786692415 (PluginXunFei_t2016385516 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::OnAddUserByGroupSuccessIMClient(System.String)
extern "C"  void PluginXunFei_OnAddUserByGroupSuccessIMClient_m1445390302 (PluginXunFei_t2016385516 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::OnRecordStartedIMClient(System.String)
extern "C"  void PluginXunFei_OnRecordStartedIMClient_m3894782837 (PluginXunFei_t2016385516 * __this, String_t* ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::OnRecordFinishedIMClient(System.String)
extern "C"  void PluginXunFei_OnRecordFinishedIMClient_m1787456028 (PluginXunFei_t2016385516 * __this, String_t* ___filePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::OnRecordingPlayStartIMClient(System.String)
extern "C"  void PluginXunFei_OnRecordingPlayStartIMClient_m2061770658 (PluginXunFei_t2016385516 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::OnRecordingPlayStopIMClient(System.String)
extern "C"  void PluginXunFei_OnRecordingPlayStopIMClient_m879563934 (PluginXunFei_t2016385516 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::OnRecordingPlayPauseIMClient(System.String)
extern "C"  void PluginXunFei_OnRecordingPlayPauseIMClient_m1283471246 (PluginXunFei_t2016385516 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::OnBuildAudioMsgErrorIMClient(System.String)
extern "C"  void PluginXunFei_OnBuildAudioMsgErrorIMClient_m1049521360 (PluginXunFei_t2016385516 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::OnCheckHasPermissionIMClient(System.String)
extern "C"  void PluginXunFei_OnCheckHasPermissionIMClient_m1215818206 (PluginXunFei_t2016385516 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::OnSendMessageSuccessIMClient(System.String)
extern "C"  void PluginXunFei_OnSendMessageSuccessIMClient_m630289083 (PluginXunFei_t2016385516 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::OnReceiveMessageIMClient(System.String)
extern "C"  void PluginXunFei_OnReceiveMessageIMClient_m2402631995 (PluginXunFei_t2016385516 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::OnDownloadMsgIMClient(System.String)
extern "C"  void PluginXunFei_OnDownloadMsgIMClient_m2044357420 (PluginXunFei_t2016385516 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFei::PlayLocalMsg(System.String)
extern "C"  bool PluginXunFei_PlayLocalMsg_m1550492431 (PluginXunFei_t2016385516 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::PlayMsg(System.String,System.String,System.String,System.Boolean,System.String)
extern "C"  void PluginXunFei_PlayMsg_m2399820025 (PluginXunFei_t2016385516 * __this, String_t* ___path0, String_t* ___name1, String_t* ___msg2, bool ___isGroup3, String_t* ___extend4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFei::IsExistSound(System.String,System.String)
extern "C"  bool PluginXunFei_IsExistSound_m1846518355 (PluginXunFei_t2016385516 * __this, String_t* ___path0, String_t* ___seqId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::LoginIMClientMgr(System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginXunFei_LoginIMClientMgr_m1591823197 (PluginXunFei_t2016385516 * __this, String_t* ___uid0, String_t* ___name1, String_t* ___props2, String_t* ___icon3, String_t* ___token4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::StartRecordingIMClientMgr(System.String,System.String)
extern "C"  void PluginXunFei_StartRecordingIMClientMgr_m2946412055 (PluginXunFei_t2016385516 * __this, String_t* ___dir0, String_t* ___mCurrentFileName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::CheckHasPermissionIMClientMgr()
extern "C"  void PluginXunFei_CheckHasPermissionIMClientMgr_m1498523733 (PluginXunFei_t2016385516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::StopRecordingIMClientMgr()
extern "C"  void PluginXunFei_StopRecordingIMClientMgr_m854663887 (PluginXunFei_t2016385516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::InitPlayerIMClientMgr(System.String)
extern "C"  void PluginXunFei_InitPlayerIMClientMgr_m1316515069 (PluginXunFei_t2016385516 * __this, String_t* ___filePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::StartPlayIMClientMgr()
extern "C"  void PluginXunFei_StartPlayIMClientMgr_m71686536 (PluginXunFei_t2016385516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::StopPlayIMClientMgr()
extern "C"  void PluginXunFei_StopPlayIMClientMgr_m4069063424 (PluginXunFei_t2016385516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::PausePlayIMClientMgr()
extern "C"  void PluginXunFei_PausePlayIMClientMgr_m3810349684 (PluginXunFei_t2016385516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::SendUpAudioMsg(System.String,System.String,System.Boolean,System.String,System.String)
extern "C"  void PluginXunFei_SendUpAudioMsg_m3181671066 (PluginXunFei_t2016385516 * __this, String_t* ___name0, String_t* ___path1, bool ___isGroup2, String_t* ___extend3, String_t* ___state4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFei::DownloadMsg(System.String,System.String,System.Boolean,System.String,System.String)
extern "C"  bool PluginXunFei_DownloadMsg_m251306025 (PluginXunFei_t2016385516 * __this, String_t* ___name0, String_t* ___msg1, bool ___isGroup2, String_t* ___extend3, String_t* ___state4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::CreateGroupInfo(System.Int32,System.String,System.String,System.String)
extern "C"  void PluginXunFei_CreateGroupInfo_m525063555 (PluginXunFei_t2016385516 * __this, int32_t ___type0, String_t* ___groupName1, String_t* ___mGroupType2, String_t* ___msgContent3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::AddUserByGroup(System.String,System.Int32)
extern "C"  void PluginXunFei_AddUserByGroup_m1242405318 (PluginXunFei_t2016385516 * __this, String_t* ___groupId0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::RemoveUserByGroup(System.String)
extern "C"  void PluginXunFei_RemoveUserByGroup_m3116330684 (PluginXunFei_t2016385516 * __this, String_t* ___groupName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::CheckGroups(System.String)
extern "C"  void PluginXunFei_CheckGroups_m1280633177 (PluginXunFei_t2016385516 * __this, String_t* ___groupinfos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginXunFei_ilo_AddEventListener1_m239772901 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::ilo_RemoveEventListener2(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginXunFei_ilo_RemoveEventListener2_m463729171 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::ilo_DispatchEvent3(CEvent.ZEvent)
extern "C"  void PluginXunFei_ilo_DispatchEvent3_m3910681792 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFei::ilo_PlayLocalMsg4(PluginXunFei,System.String)
extern "C"  bool PluginXunFei_ilo_PlayLocalMsg4_m1772171360 (Il2CppObject * __this /* static, unused */, PluginXunFei_t2016385516 * ____this0, String_t* ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei::ilo_StartPlayIMClientMgr5(PluginXunFei)
extern "C"  void PluginXunFei_ilo_StartPlayIMClientMgr5_m2970888656 (Il2CppObject * __this /* static, unused */, PluginXunFei_t2016385516 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
