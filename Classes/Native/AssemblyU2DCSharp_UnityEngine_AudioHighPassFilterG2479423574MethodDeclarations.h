﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AudioHighPassFilterGenerated
struct UnityEngine_AudioHighPassFilterGenerated_t2479423574;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_AudioHighPassFilterGenerated::.ctor()
extern "C"  void UnityEngine_AudioHighPassFilterGenerated__ctor_m761618181 (UnityEngine_AudioHighPassFilterGenerated_t2479423574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioHighPassFilterGenerated::AudioHighPassFilter_AudioHighPassFilter1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioHighPassFilterGenerated_AudioHighPassFilter_AudioHighPassFilter1_m3712219793 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioHighPassFilterGenerated::AudioHighPassFilter_cutoffFrequency(JSVCall)
extern "C"  void UnityEngine_AudioHighPassFilterGenerated_AudioHighPassFilter_cutoffFrequency_m2989384563 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioHighPassFilterGenerated::AudioHighPassFilter_highpassResonanceQ(JSVCall)
extern "C"  void UnityEngine_AudioHighPassFilterGenerated_AudioHighPassFilter_highpassResonanceQ_m1587348724 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioHighPassFilterGenerated::__Register()
extern "C"  void UnityEngine_AudioHighPassFilterGenerated___Register_m4098341090 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioHighPassFilterGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_AudioHighPassFilterGenerated_ilo_attachFinalizerObject1_m2952180090 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioHighPassFilterGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_AudioHighPassFilterGenerated_ilo_addJSCSRel2_m1065909186 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_AudioHighPassFilterGenerated::ilo_getSingle3(System.Int32)
extern "C"  float UnityEngine_AudioHighPassFilterGenerated_ilo_getSingle3_m1431084996 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
