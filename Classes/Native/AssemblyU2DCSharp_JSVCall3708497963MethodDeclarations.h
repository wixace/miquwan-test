﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSVCall
struct JSVCall_t3708497963;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// JSMgr/CSCallbackField
struct CSCallbackField_t1235469093;
// JSMgr/CSCallbackMethod
struct CSCallbackMethod_t4136956950;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSMgr_CSCallbackField1235469093.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSMgr_CSCallbackMethod4136956950.h"

// System.Void JSVCall::.ctor()
extern "C"  void JSVCall__ctor_m520748960 (JSVCall_t3708497963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSVCall::CallJSFunctionValue(System.Int32,System.Int32,System.Object[])
extern "C"  bool JSVCall_CallJSFunctionValue_m3892116814 (JSVCall_t3708497963 * __this, int32_t ___jsObjID0, int32_t ___funID1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSVCall::CallJSFunctionName(System.Int32,System.String,System.Object[])
extern "C"  bool JSVCall_CallJSFunctionName_m3660341171 (JSVCall_t3708497963 * __this, int32_t ___jsObjID0, String_t* ___funName1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSVCall::CallCallback(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  bool JSVCall_CallCallback_m1236615940 (JSVCall_t3708497963 * __this, int32_t ___iOP0, int32_t ___slot1, int32_t ___index2, int32_t ___isStatic3, int32_t ___argc4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSVCall::ilo_get_IsShutDown1()
extern "C"  bool JSVCall_ilo_get_IsShutDown1_m4284297447 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSVCall::ilo_callFunctionValue2(System.Int32,System.Int32,System.Int32)
extern "C"  void JSVCall_ilo_callFunctionValue2_m3488219519 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, int32_t ___funID1, int32_t ___argCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSVCall::ilo_getObject3(System.Int32)
extern "C"  int32_t JSVCall_ilo_getObject3_m2095246616 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSVCall::ilo_getCSObj4(System.Int32)
extern "C"  Il2CppObject * JSVCall_ilo_getCSObj4_m3435971348 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSVCall::ilo_Invoke5(JSMgr/CSCallbackField,JSVCall)
extern "C"  void JSVCall_ilo_Invoke5_m3730560742 (Il2CppObject * __this /* static, unused */, CSCallbackField_t1235469093 * ____this0, JSVCall_t3708497963 * ___vc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSVCall::ilo_Invoke6(JSMgr/CSCallbackMethod,JSVCall,System.Int32)
extern "C"  bool JSVCall_ilo_Invoke6_m279548335 (Il2CppObject * __this /* static, unused */, CSCallbackMethod_t4136956950 * ____this0, JSVCall_t3708497963 * ___vc1, int32_t ___argc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
