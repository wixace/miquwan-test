﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TreeInstance
struct  TreeInstance_t2456306007 
{
public:
	// UnityEngine.Vector3 UnityEngine.TreeInstance::position
	Vector3_t4282066566  ___position_0;
	// System.Single UnityEngine.TreeInstance::widthScale
	float ___widthScale_1;
	// System.Single UnityEngine.TreeInstance::heightScale
	float ___heightScale_2;
	// System.Single UnityEngine.TreeInstance::rotation
	float ___rotation_3;
	// UnityEngine.Color32 UnityEngine.TreeInstance::color
	Color32_t598853688  ___color_4;
	// UnityEngine.Color32 UnityEngine.TreeInstance::lightmapColor
	Color32_t598853688  ___lightmapColor_5;
	// System.Int32 UnityEngine.TreeInstance::prototypeIndex
	int32_t ___prototypeIndex_6;
	// System.Single UnityEngine.TreeInstance::temporaryDistance
	float ___temporaryDistance_7;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(TreeInstance_t2456306007, ___position_0)); }
	inline Vector3_t4282066566  get_position_0() const { return ___position_0; }
	inline Vector3_t4282066566 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t4282066566  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_widthScale_1() { return static_cast<int32_t>(offsetof(TreeInstance_t2456306007, ___widthScale_1)); }
	inline float get_widthScale_1() const { return ___widthScale_1; }
	inline float* get_address_of_widthScale_1() { return &___widthScale_1; }
	inline void set_widthScale_1(float value)
	{
		___widthScale_1 = value;
	}

	inline static int32_t get_offset_of_heightScale_2() { return static_cast<int32_t>(offsetof(TreeInstance_t2456306007, ___heightScale_2)); }
	inline float get_heightScale_2() const { return ___heightScale_2; }
	inline float* get_address_of_heightScale_2() { return &___heightScale_2; }
	inline void set_heightScale_2(float value)
	{
		___heightScale_2 = value;
	}

	inline static int32_t get_offset_of_rotation_3() { return static_cast<int32_t>(offsetof(TreeInstance_t2456306007, ___rotation_3)); }
	inline float get_rotation_3() const { return ___rotation_3; }
	inline float* get_address_of_rotation_3() { return &___rotation_3; }
	inline void set_rotation_3(float value)
	{
		___rotation_3 = value;
	}

	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(TreeInstance_t2456306007, ___color_4)); }
	inline Color32_t598853688  get_color_4() const { return ___color_4; }
	inline Color32_t598853688 * get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(Color32_t598853688  value)
	{
		___color_4 = value;
	}

	inline static int32_t get_offset_of_lightmapColor_5() { return static_cast<int32_t>(offsetof(TreeInstance_t2456306007, ___lightmapColor_5)); }
	inline Color32_t598853688  get_lightmapColor_5() const { return ___lightmapColor_5; }
	inline Color32_t598853688 * get_address_of_lightmapColor_5() { return &___lightmapColor_5; }
	inline void set_lightmapColor_5(Color32_t598853688  value)
	{
		___lightmapColor_5 = value;
	}

	inline static int32_t get_offset_of_prototypeIndex_6() { return static_cast<int32_t>(offsetof(TreeInstance_t2456306007, ___prototypeIndex_6)); }
	inline int32_t get_prototypeIndex_6() const { return ___prototypeIndex_6; }
	inline int32_t* get_address_of_prototypeIndex_6() { return &___prototypeIndex_6; }
	inline void set_prototypeIndex_6(int32_t value)
	{
		___prototypeIndex_6 = value;
	}

	inline static int32_t get_offset_of_temporaryDistance_7() { return static_cast<int32_t>(offsetof(TreeInstance_t2456306007, ___temporaryDistance_7)); }
	inline float get_temporaryDistance_7() const { return ___temporaryDistance_7; }
	inline float* get_address_of_temporaryDistance_7() { return &___temporaryDistance_7; }
	inline void set_temporaryDistance_7(float value)
	{
		___temporaryDistance_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: UnityEngine.TreeInstance
struct TreeInstance_t2456306007_marshaled_pinvoke
{
	Vector3_t4282066566_marshaled_pinvoke ___position_0;
	float ___widthScale_1;
	float ___heightScale_2;
	float ___rotation_3;
	Color32_t598853688_marshaled_pinvoke ___color_4;
	Color32_t598853688_marshaled_pinvoke ___lightmapColor_5;
	int32_t ___prototypeIndex_6;
	float ___temporaryDistance_7;
};
// Native definition for marshalling of: UnityEngine.TreeInstance
struct TreeInstance_t2456306007_marshaled_com
{
	Vector3_t4282066566_marshaled_com ___position_0;
	float ___widthScale_1;
	float ___heightScale_2;
	float ___rotation_3;
	Color32_t598853688_marshaled_com ___color_4;
	Color32_t598853688_marshaled_com ___lightmapColor_5;
	int32_t ___prototypeIndex_6;
	float ___temporaryDistance_7;
};
