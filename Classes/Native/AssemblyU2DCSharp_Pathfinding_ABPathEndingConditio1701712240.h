﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.ABPath
struct ABPath_t1187561148;

#include "AssemblyU2DCSharp_Pathfinding_PathEndingCondition3410840753.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.ABPathEndingCondition
struct  ABPathEndingCondition_t1701712240  : public PathEndingCondition_t3410840753
{
public:
	// Pathfinding.ABPath Pathfinding.ABPathEndingCondition::abPath
	ABPath_t1187561148 * ___abPath_1;

public:
	inline static int32_t get_offset_of_abPath_1() { return static_cast<int32_t>(offsetof(ABPathEndingCondition_t1701712240, ___abPath_1)); }
	inline ABPath_t1187561148 * get_abPath_1() const { return ___abPath_1; }
	inline ABPath_t1187561148 ** get_address_of_abPath_1() { return &___abPath_1; }
	inline void set_abPath_1(ABPath_t1187561148 * value)
	{
		___abPath_1 = value;
		Il2CppCodeGenWriteBarrier(&___abPath_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
