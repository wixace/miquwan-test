﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.CrashReport
struct CrashReport_t2450738801;
// System.String
struct String_t;
// UnityEngine.CrashReport[]
struct CrashReportU5BU5D_t612474028;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "UnityEngine_UnityEngine_CrashReport2450738801.h"

// System.Void UnityEngine.CrashReport::.ctor(System.String,System.DateTime,System.String)
extern "C"  void CrashReport__ctor_m954585202 (CrashReport_t2450738801 * __this, String_t* ___id0, DateTime_t4283661327  ___time1, String_t* ___text2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CrashReport::.cctor()
extern "C"  void CrashReport__cctor_m3461647727 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CrashReport::Compare(UnityEngine.CrashReport,UnityEngine.CrashReport)
extern "C"  int32_t CrashReport_Compare_m3619803573 (Il2CppObject * __this /* static, unused */, CrashReport_t2450738801 * ___c10, CrashReport_t2450738801 * ___c21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CrashReport::PopulateReports()
extern "C"  void CrashReport_PopulateReports_m3018045841 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CrashReport[] UnityEngine.CrashReport::get_reports()
extern "C"  CrashReportU5BU5D_t612474028* CrashReport_get_reports_m2315890143 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CrashReport UnityEngine.CrashReport::get_lastReport()
extern "C"  CrashReport_t2450738801 * CrashReport_get_lastReport_m3034295882 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CrashReport::RemoveAll()
extern "C"  void CrashReport_RemoveAll_m3675667673 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CrashReport::Remove()
extern "C"  void CrashReport_Remove_m3835187658 (CrashReport_t2450738801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] UnityEngine.CrashReport::GetReports()
extern "C"  StringU5BU5D_t4054002952* CrashReport_GetReports_m239667950 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CrashReport::GetReportData(System.String,System.Double&,System.String&)
extern "C"  void CrashReport_GetReportData_m3909303730 (Il2CppObject * __this /* static, unused */, String_t* ___id0, double* ___secondsSinceUnixEpoch1, String_t** ___text2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.CrashReport::RemoveReport(System.String)
extern "C"  bool CrashReport_RemoveReport_m3990905930 (Il2CppObject * __this /* static, unused */, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
