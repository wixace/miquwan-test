﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HeroGenerated
struct HeroGenerated_t3761009301;
// JSVCall
struct JSVCall_t3708497963;
// System.Action
struct Action_t3771233898;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// CSSkillData[]
struct CSSkillDataU5BU5D_t1071816810;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// Hero
struct Hero_t2245658;
// AICtrl
struct AICtrl_t1930422963;
// herosCfg
struct herosCfg_t3676934635;
// JSCLevelHeroPointConfig
struct JSCLevelHeroPointConfig_t561690542;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_Hero2245658.h"
#include "AssemblyU2DCSharp_AICtrl1930422963.h"
#include "AssemblyU2DCSharp_herosCfg3676934635.h"
#include "AssemblyU2DCSharp_JSCLevelHeroPointConfig561690542.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

// System.Void HeroGenerated::.ctor()
extern "C"  void HeroGenerated__ctor_m1529677430 (HeroGenerated_t3761009301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_Hero1(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_Hero1_m2943489532 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::Hero_TARGET_REACHED(JSVCall)
extern "C"  void HeroGenerated_Hero_TARGET_REACHED_m2238234506 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::Hero_ATK_BOSS(JSVCall)
extern "C"  void HeroGenerated_Hero_ATK_BOSS_m899306298 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::Hero_eventDispatch(JSVCall)
extern "C"  void HeroGenerated_Hero_eventDispatch_m484146122 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::Hero_place(JSVCall)
extern "C"  void HeroGenerated_Hero_place_m3816823127 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::Hero_MycritP(JSVCall)
extern "C"  void HeroGenerated_Hero_MycritP_m4197145684 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::Hero_marshalIndex(JSVCall)
extern "C"  void HeroGenerated_Hero_marshalIndex_m2002385978 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::Hero_IsAIMoveEndReached(JSVCall)
extern "C"  void HeroGenerated_Hero_IsAIMoveEndReached_m146842260 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::Hero_tpl(JSVCall)
extern "C"  void HeroGenerated_Hero_tpl_m598635790 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::Hero_aiCtrl(JSVCall)
extern "C"  void HeroGenerated_Hero_aiCtrl_m3450546747 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::Hero_IsBattling(JSVCall)
extern "C"  void HeroGenerated_Hero_IsBattling_m1553605519 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::Hero_atkTarget(JSVCall)
extern "C"  void HeroGenerated_Hero_atkTarget_m814414421 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::Hero_isCaptain(JSVCall)
extern "C"  void HeroGenerated_Hero_isCaptain_m3665758500 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::Hero_hp(JSVCall)
extern "C"  void HeroGenerated_Hero_hp_m1764204422 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_Addhurt__Single(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_Addhurt__Single_m603365181 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_AGAINATTACK(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_AGAINATTACK_m4219234925 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_AIBeAttacked(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_AIBeAttacked_m3238566095 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_AIMonsterDead__Int32(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_AIMonsterDead__Int32_m2772994933 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_AIOtherDead__ZEvent(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_AIOtherDead__ZEvent_m3948887153 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_Answer__ZEvent(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_Answer__ZEvent_m1105891419 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_ATKDEBUFF(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_ATKDEBUFF_m1158284209 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_AutoFastRun(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_AutoFastRun_m3625021093 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_CalcuFinalProp(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_CalcuFinalProp_m1654284086 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_ClosePlot__ZEvent(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_ClosePlot__ZEvent_m888079166 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_Create__herosCfg__JSCLevelHeroPointConfig__CSSkillData_Array(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_Create__herosCfg__JSCLevelHeroPointConfig__CSSkillData_Array_m994795067 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_Create__CSHeroUnit(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_Create__CSHeroUnit_m582963559 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_GadInit(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_GadInit_m3391137855 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_InitPos__Vector3__Quaternion__Boolean__JSCLevelHeroPointConfig(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_InitPos__Vector3__Quaternion__Boolean__JSCLevelHeroPointConfig_m1414263501 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_LeaveAuto(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_LeaveAuto_m2763961419 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_MoceCameraEnd__ZEvent(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_MoceCameraEnd__ZEvent_m2787663575 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_OnDelayTime(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_OnDelayTime_m2974528470 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_OnLevelTime(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_OnLevelTime_m3486983831 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_OnTargetReached(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_OnTargetReached_m1977028135 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_OnTriggerFun__Int32(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_OnTriggerFun__Int32_m1483889837 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_OnUseingSkill__Int32(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_OnUseingSkill__Int32_m4057420436 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_SingleMove__Vector3__Boolean(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_SingleMove__Vector3__Boolean_m3975826926 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_Start(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_Start_m3648280743 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_Update(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_Update_m2779310822 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action HeroGenerated::Hero_UpdateTargetPos_GetDelegate_member24_arg1(CSRepresentedObject)
extern "C"  Action_t3771233898 * HeroGenerated_Hero_UpdateTargetPos_GetDelegate_member24_arg1_m1906009244 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::Hero_UpdateTargetPos__Vector3__Action(JSVCall,System.Int32)
extern "C"  bool HeroGenerated_Hero_UpdateTargetPos__Vector3__Action_m3433802601 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::__Register()
extern "C"  void HeroGenerated___Register_m84152337 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSSkillData[] HeroGenerated::<Hero_Create__herosCfg__JSCLevelHeroPointConfig__CSSkillData_Array>m__5F()
extern "C"  CSSkillDataU5BU5D_t1071816810* HeroGenerated_U3CHero_Create__herosCfg__JSCLevelHeroPointConfig__CSSkillData_ArrayU3Em__5F_m1138765231 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action HeroGenerated::<Hero_UpdateTargetPos__Vector3__Action>m__61()
extern "C"  Action_t3771233898 * HeroGenerated_U3CHero_UpdateTargetPos__Vector3__ActionU3Em__61_m3051594767 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool HeroGenerated_ilo_attachFinalizerObject1_m3329919297 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void HeroGenerated_ilo_addJSCSRel2_m2111635123 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t HeroGenerated_ilo_setObject3_m1599565306 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroGenerated::ilo_getInt324(System.Int32)
extern "C"  int32_t HeroGenerated_ilo_getInt324_m1088292444 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HeroGenerated::ilo_getSingle5(System.Int32)
extern "C"  float HeroGenerated_ilo_getSingle5_m2215638365 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::ilo_setInt326(System.Int32,System.Int32)
extern "C"  void HeroGenerated_ilo_setInt326_m1500829947 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::ilo_setBooleanS7(System.Int32,System.Boolean)
extern "C"  void HeroGenerated_ilo_setBooleanS7_m2449860129 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HeroGenerated::ilo_getObject8(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * HeroGenerated_ilo_getObject8_m1007707510 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::ilo_set_aiCtrl9(Hero,AICtrl)
extern "C"  void HeroGenerated_ilo_set_aiCtrl9_m996917859 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, AICtrl_t1930422963 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::ilo_get_IsBattling10(Hero)
extern "C"  bool HeroGenerated_ilo_get_IsBattling10_m3075760794 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::ilo_set_IsBattling11(Hero,System.Boolean)
extern "C"  void HeroGenerated_ilo_set_IsBattling11_m1800965018 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::ilo_AIMonsterDead12(Hero,System.Int32)
extern "C"  void HeroGenerated_ilo_AIMonsterDead12_m2497862745 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, int32_t ___murderID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::ilo_CalcuFinalProp13(Hero)
extern "C"  void HeroGenerated_ilo_CalcuFinalProp13_m952635746 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::ilo_Create14(Hero,herosCfg,JSCLevelHeroPointConfig,CSSkillData[])
extern "C"  void HeroGenerated_ilo_Create14_m856043026 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, herosCfg_t3676934635 * ___mcfg1, JSCLevelHeroPointConfig_t561690542 * ___lmcfg2, CSSkillDataU5BU5D_t1071816810* ___skillList3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroGenerated::ilo_getBooleanS15(System.Int32)
extern "C"  bool HeroGenerated_ilo_getBooleanS15_m2655977451 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::ilo_InitPos16(Hero,UnityEngine.Vector3,UnityEngine.Quaternion,System.Boolean,JSCLevelHeroPointConfig)
extern "C"  void HeroGenerated_ilo_InitPos16_m2406517837 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, Vector3_t4282066566  ___pos1, Quaternion_t1553702882  ___rot2, bool ___isTrue3, JSCLevelHeroPointConfig_t561690542 * ___lhcfg4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HeroGenerated::ilo_getVector3S17(System.Int32)
extern "C"  Vector3_t4282066566  HeroGenerated_ilo_getVector3S17_m1634650763 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::ilo_LeaveAuto18(Hero)
extern "C"  void HeroGenerated_ilo_LeaveAuto18_m1937109908 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::ilo_OnLevelTime19(Hero)
extern "C"  void HeroGenerated_ilo_OnLevelTime19_m2149520161 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::ilo_OnTriggerFun20(Hero,System.Int32)
extern "C"  void HeroGenerated_ilo_OnTriggerFun20_m3434680428 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, int32_t ___effectId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroGenerated::ilo_Update21(Hero)
extern "C"  void HeroGenerated_ilo_Update21_m1015774063 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroGenerated::ilo_getObject22(System.Int32)
extern "C"  int32_t HeroGenerated_ilo_getObject22_m3714727925 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action HeroGenerated::ilo_Hero_UpdateTargetPos_GetDelegate_member24_arg123(CSRepresentedObject)
extern "C"  Action_t3771233898 * HeroGenerated_ilo_Hero_UpdateTargetPos_GetDelegate_member24_arg123_m4080598504 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
