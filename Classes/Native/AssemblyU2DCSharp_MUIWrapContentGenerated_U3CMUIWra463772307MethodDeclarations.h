﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MUIWrapContentGenerated/<MUIWrapContent_onInitializeItem_GetDelegate_member6_arg0>c__AnonStorey6E
struct U3CMUIWrapContent_onInitializeItem_GetDelegate_member6_arg0U3Ec__AnonStorey6E_t463772307;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void MUIWrapContentGenerated/<MUIWrapContent_onInitializeItem_GetDelegate_member6_arg0>c__AnonStorey6E::.ctor()
extern "C"  void U3CMUIWrapContent_onInitializeItem_GetDelegate_member6_arg0U3Ec__AnonStorey6E__ctor_m2364946488 (U3CMUIWrapContent_onInitializeItem_GetDelegate_member6_arg0U3Ec__AnonStorey6E_t463772307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContentGenerated/<MUIWrapContent_onInitializeItem_GetDelegate_member6_arg0>c__AnonStorey6E::<>m__76(UnityEngine.GameObject,System.Int32,System.Int32)
extern "C"  void U3CMUIWrapContent_onInitializeItem_GetDelegate_member6_arg0U3Ec__AnonStorey6E_U3CU3Em__76_m999055096 (U3CMUIWrapContent_onInitializeItem_GetDelegate_member6_arg0U3Ec__AnonStorey6E_t463772307 * __this, GameObject_t3674682005 * ___go0, int32_t ___wrapIndex1, int32_t ___realIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
