﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CameraMotionBlur
struct CameraMotionBlur_t2114294370;
// TargetMotionBlur/OnFinish
struct OnFinish_t840705683;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TargetMotionBlur
struct  TargetMotionBlur_t1745103310  : public MonoBehaviour_t667441552
{
public:
	// CameraMotionBlur TargetMotionBlur::motion
	CameraMotionBlur_t2114294370 * ___motion_2;
	// UnityEngine.Vector3 TargetMotionBlur::targetPos
	Vector3_t4282066566  ___targetPos_3;
	// System.Single TargetMotionBlur::startDistance
	float ___startDistance_4;
	// System.Single TargetMotionBlur::sqrMinDistance
	float ___sqrMinDistance_5;
	// System.Single TargetMotionBlur::startVelocity
	float ___startVelocity_6;
	// System.Single TargetMotionBlur::endVelocity
	float ___endVelocity_7;
	// System.Single TargetMotionBlur::timeScale
	float ___timeScale_8;
	// System.Single TargetMotionBlur::motionDuration
	float ___motionDuration_9;
	// TargetMotionBlur/OnFinish TargetMotionBlur::fnFinish
	OnFinish_t840705683 * ___fnFinish_10;

public:
	inline static int32_t get_offset_of_motion_2() { return static_cast<int32_t>(offsetof(TargetMotionBlur_t1745103310, ___motion_2)); }
	inline CameraMotionBlur_t2114294370 * get_motion_2() const { return ___motion_2; }
	inline CameraMotionBlur_t2114294370 ** get_address_of_motion_2() { return &___motion_2; }
	inline void set_motion_2(CameraMotionBlur_t2114294370 * value)
	{
		___motion_2 = value;
		Il2CppCodeGenWriteBarrier(&___motion_2, value);
	}

	inline static int32_t get_offset_of_targetPos_3() { return static_cast<int32_t>(offsetof(TargetMotionBlur_t1745103310, ___targetPos_3)); }
	inline Vector3_t4282066566  get_targetPos_3() const { return ___targetPos_3; }
	inline Vector3_t4282066566 * get_address_of_targetPos_3() { return &___targetPos_3; }
	inline void set_targetPos_3(Vector3_t4282066566  value)
	{
		___targetPos_3 = value;
	}

	inline static int32_t get_offset_of_startDistance_4() { return static_cast<int32_t>(offsetof(TargetMotionBlur_t1745103310, ___startDistance_4)); }
	inline float get_startDistance_4() const { return ___startDistance_4; }
	inline float* get_address_of_startDistance_4() { return &___startDistance_4; }
	inline void set_startDistance_4(float value)
	{
		___startDistance_4 = value;
	}

	inline static int32_t get_offset_of_sqrMinDistance_5() { return static_cast<int32_t>(offsetof(TargetMotionBlur_t1745103310, ___sqrMinDistance_5)); }
	inline float get_sqrMinDistance_5() const { return ___sqrMinDistance_5; }
	inline float* get_address_of_sqrMinDistance_5() { return &___sqrMinDistance_5; }
	inline void set_sqrMinDistance_5(float value)
	{
		___sqrMinDistance_5 = value;
	}

	inline static int32_t get_offset_of_startVelocity_6() { return static_cast<int32_t>(offsetof(TargetMotionBlur_t1745103310, ___startVelocity_6)); }
	inline float get_startVelocity_6() const { return ___startVelocity_6; }
	inline float* get_address_of_startVelocity_6() { return &___startVelocity_6; }
	inline void set_startVelocity_6(float value)
	{
		___startVelocity_6 = value;
	}

	inline static int32_t get_offset_of_endVelocity_7() { return static_cast<int32_t>(offsetof(TargetMotionBlur_t1745103310, ___endVelocity_7)); }
	inline float get_endVelocity_7() const { return ___endVelocity_7; }
	inline float* get_address_of_endVelocity_7() { return &___endVelocity_7; }
	inline void set_endVelocity_7(float value)
	{
		___endVelocity_7 = value;
	}

	inline static int32_t get_offset_of_timeScale_8() { return static_cast<int32_t>(offsetof(TargetMotionBlur_t1745103310, ___timeScale_8)); }
	inline float get_timeScale_8() const { return ___timeScale_8; }
	inline float* get_address_of_timeScale_8() { return &___timeScale_8; }
	inline void set_timeScale_8(float value)
	{
		___timeScale_8 = value;
	}

	inline static int32_t get_offset_of_motionDuration_9() { return static_cast<int32_t>(offsetof(TargetMotionBlur_t1745103310, ___motionDuration_9)); }
	inline float get_motionDuration_9() const { return ___motionDuration_9; }
	inline float* get_address_of_motionDuration_9() { return &___motionDuration_9; }
	inline void set_motionDuration_9(float value)
	{
		___motionDuration_9 = value;
	}

	inline static int32_t get_offset_of_fnFinish_10() { return static_cast<int32_t>(offsetof(TargetMotionBlur_t1745103310, ___fnFinish_10)); }
	inline OnFinish_t840705683 * get_fnFinish_10() const { return ___fnFinish_10; }
	inline OnFinish_t840705683 ** get_address_of_fnFinish_10() { return &___fnFinish_10; }
	inline void set_fnFinish_10(OnFinish_t840705683 * value)
	{
		___fnFinish_10 = value;
		Il2CppCodeGenWriteBarrier(&___fnFinish_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
