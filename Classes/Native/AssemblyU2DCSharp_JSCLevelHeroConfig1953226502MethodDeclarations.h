﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSCLevelHeroConfig
struct JSCLevelHeroConfig_t1953226502;
// ProtoBuf.IExtension
struct IExtension_t1606339106;
// System.Collections.Generic.List`1<JSCLevelHeroPointConfig>
struct List_1_t1929876094;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;

#include "codegen/il2cpp-codegen.h"

// System.Void JSCLevelHeroConfig::.ctor()
extern "C"  void JSCLevelHeroConfig__ctor_m3747457109 (JSCLevelHeroConfig_t1953226502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension JSCLevelHeroConfig::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * JSCLevelHeroConfig_ProtoBuf_IExtensible_GetExtensionObject_m1739521701 (JSCLevelHeroConfig_t1953226502 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<JSCLevelHeroPointConfig> JSCLevelHeroConfig::get_pointList()
extern "C"  List_1_t1929876094 * JSCLevelHeroConfig_get_pointList_m2368961513 (JSCLevelHeroConfig_t1953226502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int32> JSCLevelHeroConfig::get_idList()
extern "C"  List_1_t2522024052 * JSCLevelHeroConfig_get_idList_m3986114099 (JSCLevelHeroConfig_t1953226502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JSCLevelHeroConfig::get_spacing()
extern "C"  float JSCLevelHeroConfig_get_spacing_m1184342689 (JSCLevelHeroConfig_t1953226502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCLevelHeroConfig::set_spacing(System.Single)
extern "C"  void JSCLevelHeroConfig_set_spacing_m3998333522 (JSCLevelHeroConfig_t1953226502 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension JSCLevelHeroConfig::ilo_GetExtensionObject1(ProtoBuf.IExtension&,System.Boolean)
extern "C"  Il2CppObject * JSCLevelHeroConfig_ilo_GetExtensionObject1_m3288025979 (Il2CppObject * __this /* static, unused */, Il2CppObject ** ___extensionObject0, bool ___createIfMissing1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
