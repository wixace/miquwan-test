﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3890650599.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Decoder813340627.h"

// System.Void System.Array/InternalEnumerator`1<SevenZip.Compression.LZMA.Decoder/LiteralDecoder/Decoder2>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1253350424_gshared (InternalEnumerator_1_t3890650599 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1253350424(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3890650599 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1253350424_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<SevenZip.Compression.LZMA.Decoder/LiteralDecoder/Decoder2>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3001561736_gshared (InternalEnumerator_1_t3890650599 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3001561736(__this, method) ((  void (*) (InternalEnumerator_1_t3890650599 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3001561736_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<SevenZip.Compression.LZMA.Decoder/LiteralDecoder/Decoder2>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4111162558_gshared (InternalEnumerator_1_t3890650599 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4111162558(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3890650599 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4111162558_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<SevenZip.Compression.LZMA.Decoder/LiteralDecoder/Decoder2>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m659153903_gshared (InternalEnumerator_1_t3890650599 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m659153903(__this, method) ((  void (*) (InternalEnumerator_1_t3890650599 *, const MethodInfo*))InternalEnumerator_1_Dispose_m659153903_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<SevenZip.Compression.LZMA.Decoder/LiteralDecoder/Decoder2>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1300650168_gshared (InternalEnumerator_1_t3890650599 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1300650168(__this, method) ((  bool (*) (InternalEnumerator_1_t3890650599 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1300650168_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<SevenZip.Compression.LZMA.Decoder/LiteralDecoder/Decoder2>::get_Current()
extern "C"  Decoder2_t813340627  InternalEnumerator_1_get_Current_m1307080577_gshared (InternalEnumerator_1_t3890650599 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1307080577(__this, method) ((  Decoder2_t813340627  (*) (InternalEnumerator_1_t3890650599 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1307080577_gshared)(__this, method)
