﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtilGenerated
struct BehaviourUtilGenerated_t715901456;
// JSVCall
struct JSVCall_t3708497963;
// System.Action
struct Action_t3771233898;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// BehaviourUtil/JSDelayFun4
struct JSDelayFun4_t1074711455;
// BehaviourUtil/JSDelayFun3
struct JSDelayFun3_t1074711454;
// BehaviourUtil/JSDelayFun2
struct JSDelayFun2_t1074711453;
// BehaviourUtil/JSDelayFun1
struct JSDelayFun1_t1074711452;
// BehaviourUtil/JSDelayFun0
struct JSDelayFun0_t1074711451;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Type
struct Type_t;
// MethodID
struct MethodID_t3916401116;
// System.Delegate
struct Delegate_t3310234105;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_MethodID3916401116.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "AssemblyU2DCSharp_BehaviourUtil_JSDelayFun31074711454.h"
#include "AssemblyU2DCSharp_BehaviourUtil_JSDelayFun01074711451.h"

// System.Void BehaviourUtilGenerated::.ctor()
extern "C"  void BehaviourUtilGenerated__ctor_m2089467019 (BehaviourUtilGenerated_t715901456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtilGenerated::.cctor()
extern "C"  void BehaviourUtilGenerated__cctor_m4161839234 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtilGenerated::BehaviourUtil_BehaviourUtil1(JSVCall,System.Int32)
extern "C"  bool BehaviourUtilGenerated_BehaviourUtil_BehaviourUtil1_m2490687311 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtilGenerated::BehaviourUtil_GlobalCoroutine(JSVCall)
extern "C"  void BehaviourUtilGenerated_BehaviourUtil_GlobalCoroutine_m637903625 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtilGenerated::BehaviourUtil_Update(JSVCall,System.Int32)
extern "C"  bool BehaviourUtilGenerated_BehaviourUtil_Update_m3665752870 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtilGenerated::BehaviourUtil_DelayCallT4__Single__ActionT4_T1_T2_T3_T4__T1__T2__T3__T4(JSVCall,System.Int32)
extern "C"  bool BehaviourUtilGenerated_BehaviourUtil_DelayCallT4__Single__ActionT4_T1_T2_T3_T4__T1__T2__T3__T4_m1803203968 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtilGenerated::BehaviourUtil_DelayCallT3__Single__ActionT3_T1_T2_T3__T1__T2__T3(JSVCall,System.Int32)
extern "C"  bool BehaviourUtilGenerated_BehaviourUtil_DelayCallT3__Single__ActionT3_T1_T2_T3__T1__T2__T3_m3079568343 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtilGenerated::BehaviourUtil_DelayCallT2__Single__ActionT2_T1_T2__T1__T2(JSVCall,System.Int32)
extern "C"  bool BehaviourUtilGenerated_BehaviourUtil_DelayCallT2__Single__ActionT2_T1_T2__T1__T2_m1023885244 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtilGenerated::BehaviourUtil_DelayCallT1__Single__ActionT1_T1__T1(JSVCall,System.Int32)
extern "C"  bool BehaviourUtilGenerated_BehaviourUtil_DelayCallT1__Single__ActionT1_T1__T1_m1269991613 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action BehaviourUtilGenerated::BehaviourUtil_DelayCall_GetDelegate_member5_arg1(CSRepresentedObject)
extern "C"  Action_t3771233898 * BehaviourUtilGenerated_BehaviourUtil_DelayCall_GetDelegate_member5_arg1_m2875189444 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtilGenerated::BehaviourUtil_DelayCall__Single__Action(JSVCall,System.Int32)
extern "C"  bool BehaviourUtilGenerated_BehaviourUtil_DelayCall__Single__Action_m197240132 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtilGenerated::BehaviourUtil_DelayPause__Single(JSVCall,System.Int32)
extern "C"  bool BehaviourUtilGenerated_BehaviourUtil_DelayPause__Single_m788020824 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BehaviourUtil/JSDelayFun4 BehaviourUtilGenerated::BehaviourUtil_JSDelayCall_GetDelegate_member7_arg1(CSRepresentedObject)
extern "C"  JSDelayFun4_t1074711455 * BehaviourUtilGenerated_BehaviourUtil_JSDelayCall_GetDelegate_member7_arg1_m1975097767 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtilGenerated::BehaviourUtil_JSDelayCall__Single__JSDelayFun4__Object__Object__Object__Object(JSVCall,System.Int32)
extern "C"  bool BehaviourUtilGenerated_BehaviourUtil_JSDelayCall__Single__JSDelayFun4__Object__Object__Object__Object_m153789640 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BehaviourUtil/JSDelayFun3 BehaviourUtilGenerated::BehaviourUtil_JSDelayCall_GetDelegate_member8_arg1(CSRepresentedObject)
extern "C"  JSDelayFun3_t1074711454 * BehaviourUtilGenerated_BehaviourUtil_JSDelayCall_GetDelegate_member8_arg1_m2136176231 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtilGenerated::BehaviourUtil_JSDelayCall__Single__JSDelayFun3__Object__Object__Object(JSVCall,System.Int32)
extern "C"  bool BehaviourUtilGenerated_BehaviourUtil_JSDelayCall__Single__JSDelayFun3__Object__Object__Object_m2311589000 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BehaviourUtil/JSDelayFun2 BehaviourUtilGenerated::BehaviourUtil_JSDelayCall_GetDelegate_member9_arg1(CSRepresentedObject)
extern "C"  JSDelayFun2_t1074711453 * BehaviourUtilGenerated_BehaviourUtil_JSDelayCall_GetDelegate_member9_arg1_m2297254695 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtilGenerated::BehaviourUtil_JSDelayCall__Single__JSDelayFun2__Object__Object(JSVCall,System.Int32)
extern "C"  bool BehaviourUtilGenerated_BehaviourUtil_JSDelayCall__Single__JSDelayFun2__Object__Object_m128082760 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BehaviourUtil/JSDelayFun1 BehaviourUtilGenerated::BehaviourUtil_JSDelayCall_GetDelegate_member10_arg1(CSRepresentedObject)
extern "C"  JSDelayFun1_t1074711452 * BehaviourUtilGenerated_BehaviourUtil_JSDelayCall_GetDelegate_member10_arg1_m1204107188 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtilGenerated::BehaviourUtil_JSDelayCall__Single__JSDelayFun1__Object(JSVCall,System.Int32)
extern "C"  bool BehaviourUtilGenerated_BehaviourUtil_JSDelayCall__Single__JSDelayFun1__Object_m1907861768 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BehaviourUtil/JSDelayFun0 BehaviourUtilGenerated::BehaviourUtil_JSDelayCall_GetDelegate_member11_arg1(CSRepresentedObject)
extern "C"  JSDelayFun0_t1074711451 * BehaviourUtilGenerated_BehaviourUtil_JSDelayCall_GetDelegate_member11_arg1_m3118922838 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtilGenerated::BehaviourUtil_JSDelayCall__Single__JSDelayFun0(JSVCall,System.Int32)
extern "C"  bool BehaviourUtilGenerated_BehaviourUtil_JSDelayCall__Single__JSDelayFun0_m2751847880 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtilGenerated::BehaviourUtil_SetPause__UInt32__Boolean(JSVCall,System.Int32)
extern "C"  bool BehaviourUtilGenerated_BehaviourUtil_SetPause__UInt32__Boolean_m2863659584 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtilGenerated::BehaviourUtil_StartCoroutine__IEnumerator__UInt32(JSVCall,System.Int32)
extern "C"  bool BehaviourUtilGenerated_BehaviourUtil_StartCoroutine__IEnumerator__UInt32_m1792114389 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtilGenerated::BehaviourUtil_StartCoroutine__IEnumerator(JSVCall,System.Int32)
extern "C"  bool BehaviourUtilGenerated_BehaviourUtil_StartCoroutine__IEnumerator_m186540924 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtilGenerated::BehaviourUtil_StartCoroutine__WaitForSeconds(JSVCall,System.Int32)
extern "C"  bool BehaviourUtilGenerated_BehaviourUtil_StartCoroutine__WaitForSeconds_m1468323262 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtilGenerated::BehaviourUtil_StopAllCoroutine(JSVCall,System.Int32)
extern "C"  bool BehaviourUtilGenerated_BehaviourUtil_StopAllCoroutine_m2034132662 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtilGenerated::BehaviourUtil_StopCoroutine__UInt32(JSVCall,System.Int32)
extern "C"  bool BehaviourUtilGenerated_BehaviourUtil_StopCoroutine__UInt32_m2831372884 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtilGenerated::__Register()
extern "C"  void BehaviourUtilGenerated___Register_m3952992028 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action BehaviourUtilGenerated::<BehaviourUtil_DelayCall__Single__Action>m__B()
extern "C"  Action_t3771233898 * BehaviourUtilGenerated_U3CBehaviourUtil_DelayCall__Single__ActionU3Em__B_m697672251 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BehaviourUtil/JSDelayFun4 BehaviourUtilGenerated::<BehaviourUtil_JSDelayCall__Single__JSDelayFun4__Object__Object__Object__Object>m__D()
extern "C"  JSDelayFun4_t1074711455 * BehaviourUtilGenerated_U3CBehaviourUtil_JSDelayCall__Single__JSDelayFun4__Object__Object__Object__ObjectU3Em__D_m2427758459 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BehaviourUtil/JSDelayFun3 BehaviourUtilGenerated::<BehaviourUtil_JSDelayCall__Single__JSDelayFun3__Object__Object__Object>m__F()
extern "C"  JSDelayFun3_t1074711454 * BehaviourUtilGenerated_U3CBehaviourUtil_JSDelayCall__Single__JSDelayFun3__Object__Object__ObjectU3Em__F_m2225308254 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BehaviourUtil/JSDelayFun2 BehaviourUtilGenerated::<BehaviourUtil_JSDelayCall__Single__JSDelayFun2__Object__Object>m__11()
extern "C"  JSDelayFun2_t1074711453 * BehaviourUtilGenerated_U3CBehaviourUtil_JSDelayCall__Single__JSDelayFun2__Object__ObjectU3Em__11_m3637445929 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BehaviourUtil/JSDelayFun1 BehaviourUtilGenerated::<BehaviourUtil_JSDelayCall__Single__JSDelayFun1__Object>m__13()
extern "C"  JSDelayFun1_t1074711452 * BehaviourUtilGenerated_U3CBehaviourUtil_JSDelayCall__Single__JSDelayFun1__ObjectU3Em__13_m3776298858 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BehaviourUtil/JSDelayFun0 BehaviourUtilGenerated::<BehaviourUtil_JSDelayCall__Single__JSDelayFun0>m__15()
extern "C"  JSDelayFun0_t1074711451 * BehaviourUtilGenerated_U3CBehaviourUtil_JSDelayCall__Single__JSDelayFun0U3Em__15_m296912555 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BehaviourUtilGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t BehaviourUtilGenerated_ilo_getObject1_m3315094759 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BehaviourUtilGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t BehaviourUtilGenerated_ilo_setObject2_m1568324256 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo BehaviourUtilGenerated::ilo_makeGenericMethod3(System.Type,MethodID,System.Int32)
extern "C"  MethodInfo_t * BehaviourUtilGenerated_ilo_makeGenericMethod3_m1459758983 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, MethodID_t3916401116 * ___methodID1, int32_t ___TCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BehaviourUtilGenerated::ilo_getWhatever4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * BehaviourUtilGenerated_ilo_getWhatever4_m1877803652 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtilGenerated::ilo_addJSFunCSDelegateRel5(System.Int32,System.Delegate)
extern "C"  void BehaviourUtilGenerated_ilo_addJSFunCSDelegateRel5_m1678443179 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, Delegate_t3310234105 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BehaviourUtilGenerated::ilo_getSingle6(System.Int32)
extern "C"  float BehaviourUtilGenerated_ilo_getSingle6_m2603019201 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtilGenerated::ilo_setUInt327(System.Int32,System.UInt32)
extern "C"  void BehaviourUtilGenerated_ilo_setUInt327_m2828907821 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 BehaviourUtilGenerated::ilo_JSDelayCall8(System.Single,BehaviourUtil/JSDelayFun3,System.Object,System.Object,System.Object)
extern "C"  uint32_t BehaviourUtilGenerated_ilo_JSDelayCall8_m2903041148 (Il2CppObject * __this /* static, unused */, float ___delaytime0, JSDelayFun3_t1074711454 * ___act1, Il2CppObject * ___arg12, Il2CppObject * ___arg23, Il2CppObject * ___arg34, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 BehaviourUtilGenerated::ilo_JSDelayCall9(System.Single,BehaviourUtil/JSDelayFun0)
extern "C"  uint32_t BehaviourUtilGenerated_ilo_JSDelayCall9_m1699188948 (Il2CppObject * __this /* static, unused */, float ___delaytime0, JSDelayFun0_t1074711451 * ___act1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 BehaviourUtilGenerated::ilo_getUInt3210(System.Int32)
extern "C"  uint32_t BehaviourUtilGenerated_ilo_getUInt3210_m1332328314 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtilGenerated::ilo_SetPause11(System.UInt32,System.Boolean)
extern "C"  void BehaviourUtilGenerated_ilo_SetPause11_m4255500579 (Il2CppObject * __this /* static, unused */, uint32_t ___id0, bool ___isPause1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtilGenerated::ilo_StopCoroutine12(System.UInt32)
extern "C"  void BehaviourUtilGenerated_ilo_StopCoroutine12_m3807855725 (Il2CppObject * __this /* static, unused */, uint32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject BehaviourUtilGenerated::ilo_getFunctionS13(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * BehaviourUtilGenerated_ilo_getFunctionS13_m1893335329 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action BehaviourUtilGenerated::ilo_BehaviourUtil_DelayCall_GetDelegate_member5_arg114(CSRepresentedObject)
extern "C"  Action_t3771233898 * BehaviourUtilGenerated_ilo_BehaviourUtil_DelayCall_GetDelegate_member5_arg114_m1745628590 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BehaviourUtilGenerated::ilo_getObject15(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * BehaviourUtilGenerated_ilo_getObject15_m2305967745 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtilGenerated::ilo_isFunctionS16(System.Int32)
extern "C"  bool BehaviourUtilGenerated_ilo_isFunctionS16_m3230992169 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BehaviourUtil/JSDelayFun2 BehaviourUtilGenerated::ilo_BehaviourUtil_JSDelayCall_GetDelegate_member9_arg117(CSRepresentedObject)
extern "C"  JSDelayFun2_t1074711453 * BehaviourUtilGenerated_ilo_BehaviourUtil_JSDelayCall_GetDelegate_member9_arg117_m1572679662 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BehaviourUtil/JSDelayFun1 BehaviourUtilGenerated::ilo_BehaviourUtil_JSDelayCall_GetDelegate_member10_arg118(CSRepresentedObject)
extern "C"  JSDelayFun1_t1074711452 * BehaviourUtilGenerated_ilo_BehaviourUtil_JSDelayCall_GetDelegate_member10_arg118_m2742302880 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BehaviourUtil/JSDelayFun0 BehaviourUtilGenerated::ilo_BehaviourUtil_JSDelayCall_GetDelegate_member11_arg119(CSRepresentedObject)
extern "C"  JSDelayFun0_t1074711451 * BehaviourUtilGenerated_ilo_BehaviourUtil_JSDelayCall_GetDelegate_member11_arg119_m702701281 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
