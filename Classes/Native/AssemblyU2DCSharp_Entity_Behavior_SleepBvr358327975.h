﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entity.Behavior.SleepBvr
struct  SleepBvr_t358327975  : public IBehavior_t770859129
{
public:
	// System.Boolean Entity.Behavior.SleepBvr::canTran2OtherBehavior
	bool ___canTran2OtherBehavior_2;

public:
	inline static int32_t get_offset_of_canTran2OtherBehavior_2() { return static_cast<int32_t>(offsetof(SleepBvr_t358327975, ___canTran2OtherBehavior_2)); }
	inline bool get_canTran2OtherBehavior_2() const { return ___canTran2OtherBehavior_2; }
	inline bool* get_address_of_canTran2OtherBehavior_2() { return &___canTran2OtherBehavior_2; }
	inline void set_canTran2OtherBehavior_2(bool value)
	{
		___canTran2OtherBehavior_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
