﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen740738874.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Pathfinding_BBTree_BBTreeBox1958396198.h"

// System.Void System.Array/InternalEnumerator`1<Pathfinding.BBTree/BBTreeBox>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2108631223_gshared (InternalEnumerator_1_t740738874 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2108631223(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t740738874 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2108631223_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.BBTree/BBTreeBox>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4167977865_gshared (InternalEnumerator_1_t740738874 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4167977865(__this, method) ((  void (*) (InternalEnumerator_1_t740738874 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4167977865_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Pathfinding.BBTree/BBTreeBox>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1506178165_gshared (InternalEnumerator_1_t740738874 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1506178165(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t740738874 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1506178165_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.BBTree/BBTreeBox>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1844370254_gshared (InternalEnumerator_1_t740738874 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1844370254(__this, method) ((  void (*) (InternalEnumerator_1_t740738874 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1844370254_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.BBTree/BBTreeBox>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3727705077_gshared (InternalEnumerator_1_t740738874 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3727705077(__this, method) ((  bool (*) (InternalEnumerator_1_t740738874 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3727705077_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Pathfinding.BBTree/BBTreeBox>::get_Current()
extern "C"  BBTreeBox_t1958396198  InternalEnumerator_1_get_Current_m132170750_gshared (InternalEnumerator_1_t740738874 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m132170750(__this, method) ((  BBTreeBox_t1958396198  (*) (InternalEnumerator_1_t740738874 *, const MethodInfo*))InternalEnumerator_1_get_Current_m132170750_gshared)(__this, method)
