﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<BuffState>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1206470469(__this, ___l0, method) ((  void (*) (Enumerator_t3436767600 *, List_1_t3417094830 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<BuffState>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m846042605(__this, method) ((  void (*) (Enumerator_t3436767600 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<BuffState>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2753380057(__this, method) ((  Il2CppObject * (*) (Enumerator_t3436767600 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<BuffState>::Dispose()
#define Enumerator_Dispose_m2180265578(__this, method) ((  void (*) (Enumerator_t3436767600 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<BuffState>::VerifyState()
#define Enumerator_VerifyState_m3488788643(__this, method) ((  void (*) (Enumerator_t3436767600 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<BuffState>::MoveNext()
#define Enumerator_MoveNext_m1381296729(__this, method) ((  bool (*) (Enumerator_t3436767600 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<BuffState>::get_Current()
#define Enumerator_get_Current_m1163337754(__this, method) ((  BuffState_t2048909278 * (*) (Enumerator_t3436767600 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
