﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ContrastEnhance
struct ContrastEnhance_t4202625324;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void ContrastEnhance::.ctor()
extern "C"  void ContrastEnhance__ctor_m2604694070 (ContrastEnhance_t4202625324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ContrastEnhance::CheckResources()
extern "C"  bool ContrastEnhance_CheckResources_m827509541 (ContrastEnhance_t4202625324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ContrastEnhance::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void ContrastEnhance_OnRenderImage_m3681364072 (ContrastEnhance_t4202625324 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ContrastEnhance::Main()
extern "C"  void ContrastEnhance_Main_m4155936711 (ContrastEnhance_t4202625324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
