﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_xerlujor133
struct M_xerlujor133_t2094682514;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_xerlujor133::.ctor()
extern "C"  void M_xerlujor133__ctor_m4259171857 (M_xerlujor133_t2094682514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xerlujor133::M_jitu0(System.String[],System.Int32)
extern "C"  void M_xerlujor133_M_jitu0_m3531105220 (M_xerlujor133_t2094682514 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
