﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RollNumber
struct RollNumber_t957354566;
// EventDelegate/Callback
struct Callback_t1094463061;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "AssemblyU2DCSharp_RollNumber957354566.h"

// System.Void RollNumber::.ctor()
extern "C"  void RollNumber__ctor_m2328147221 (RollNumber_t957354566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RollNumber::Update()
extern "C"  void RollNumber_Update_m884981912 (RollNumber_t957354566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RollNumber::Finish()
extern "C"  void RollNumber_Finish_m3183692258 (RollNumber_t957354566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RollNumber::MoveTo(System.Single,System.Single,System.Int32,System.Int32,EventDelegate/Callback)
extern "C"  void RollNumber_MoveTo_m2372150556 (RollNumber_t957354566 * __this, float ___time0, float ___delay1, int32_t ___start2, int32_t ___end3, Callback_t1094463061 * ___call4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RollNumber::MoveTo(System.Single,System.Int32,System.Int32)
extern "C"  void RollNumber_MoveTo_m1415043504 (RollNumber_t957354566 * __this, float ___time0, int32_t ___start1, int32_t ___end2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RollNumber::MoveTo(System.Single,System.Single,System.Int32,System.Int32)
extern "C"  void RollNumber_MoveTo_m1301812309 (RollNumber_t957354566 * __this, float ___time0, float ___delay1, int32_t ___start2, int32_t ___end3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RollNumber::MoveTo(System.Single,System.Int32,System.Int32,EventDelegate/Callback)
extern "C"  void RollNumber_MoveTo_m658803041 (RollNumber_t957354566 * __this, float ___time0, int32_t ___start1, int32_t ___end2, Callback_t1094463061 * ___call3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RollNumber::ilo_Finish1(RollNumber)
extern "C"  void RollNumber_ilo_Finish1_m193134232 (Il2CppObject * __this /* static, unused */, RollNumber_t957354566 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
