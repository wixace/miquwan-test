﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Poly2Tri.DTSweepContext
struct DTSweepContext_t543731303;
// Pathfinding.Poly2Tri.TriangulationPoint
struct TriangulationPoint_t3810082933;
// Pathfinding.Poly2Tri.DelaunayTriangle
struct DelaunayTriangle_t2835103587;
// Pathfinding.Poly2Tri.AdvancingFrontNode
struct AdvancingFrontNode_t688967424;
// Pathfinding.Poly2Tri.Triangulatable
struct Triangulatable_t923279207;
// Pathfinding.Poly2Tri.TriangulationConstraint
struct TriangulationConstraint_t137695394;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul3810082933.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Delaunay2835103587.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Advancing688967424.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul2946406228.h"

// System.Void Pathfinding.Poly2Tri.DTSweepContext::.ctor()
extern "C"  void DTSweepContext__ctor_m606266552 (DTSweepContext_t543731303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.TriangulationPoint Pathfinding.Poly2Tri.DTSweepContext::get_Head()
extern "C"  TriangulationPoint_t3810082933 * DTSweepContext_get_Head_m84775074 (DTSweepContext_t543731303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweepContext::set_Head(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  void DTSweepContext_set_Head_m3637117257 (DTSweepContext_t543731303 * __this, TriangulationPoint_t3810082933 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.TriangulationPoint Pathfinding.Poly2Tri.DTSweepContext::get_Tail()
extern "C"  TriangulationPoint_t3810082933 * DTSweepContext_get_Tail_m424876818 (DTSweepContext_t543731303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweepContext::set_Tail(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  void DTSweepContext_set_Tail_m1909889241 (DTSweepContext_t543731303 * __this, TriangulationPoint_t3810082933 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Poly2Tri.DTSweepContext::get_IsDebugEnabled()
extern "C"  bool DTSweepContext_get_IsDebugEnabled_m2721378423 (DTSweepContext_t543731303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweepContext::RemoveFromList(Pathfinding.Poly2Tri.DelaunayTriangle)
extern "C"  void DTSweepContext_RemoveFromList_m3222250764 (DTSweepContext_t543731303 * __this, DelaunayTriangle_t2835103587 * ___triangle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweepContext::MeshClean(Pathfinding.Poly2Tri.DelaunayTriangle)
extern "C"  void DTSweepContext_MeshClean_m3777320306 (DTSweepContext_t543731303 * __this, DelaunayTriangle_t2835103587 * ___triangle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweepContext::MeshCleanReq(Pathfinding.Poly2Tri.DelaunayTriangle)
extern "C"  void DTSweepContext_MeshCleanReq_m3751789302 (DTSweepContext_t543731303 * __this, DelaunayTriangle_t2835103587 * ___triangle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweepContext::Clear()
extern "C"  void DTSweepContext_Clear_m2307367139 (DTSweepContext_t543731303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweepContext::AddNode(Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void DTSweepContext_AddNode_m3594313838 (DTSweepContext_t543731303 * __this, AdvancingFrontNode_t688967424 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweepContext::RemoveNode(Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void DTSweepContext_RemoveNode_m2652831733 (DTSweepContext_t543731303 * __this, AdvancingFrontNode_t688967424 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.AdvancingFrontNode Pathfinding.Poly2Tri.DTSweepContext::LocateNode(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  AdvancingFrontNode_t688967424 * DTSweepContext_LocateNode_m451156442 (DTSweepContext_t543731303 * __this, TriangulationPoint_t3810082933 * ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweepContext::CreateAdvancingFront()
extern "C"  void DTSweepContext_CreateAdvancingFront_m616900306 (DTSweepContext_t543731303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweepContext::MapTriangleToNodes(Pathfinding.Poly2Tri.DelaunayTriangle)
extern "C"  void DTSweepContext_MapTriangleToNodes_m727745286 (DTSweepContext_t543731303 * __this, DelaunayTriangle_t2835103587 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweepContext::PrepareTriangulation(Pathfinding.Poly2Tri.Triangulatable)
extern "C"  void DTSweepContext_PrepareTriangulation_m2503426508 (DTSweepContext_t543731303 * __this, Il2CppObject * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweepContext::FinalizeTriangulation()
extern "C"  void DTSweepContext_FinalizeTriangulation_m615836583 (DTSweepContext_t543731303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.TriangulationConstraint Pathfinding.Poly2Tri.DTSweepContext::NewConstraint(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  TriangulationConstraint_t137695394 * DTSweepContext_NewConstraint_m994251481 (DTSweepContext_t543731303 * __this, TriangulationPoint_t3810082933 * ___a0, TriangulationPoint_t3810082933 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.TriangulationAlgorithm Pathfinding.Poly2Tri.DTSweepContext::get_Algorithm()
extern "C"  int32_t DTSweepContext_get_Algorithm_m2488598032 (DTSweepContext_t543731303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
