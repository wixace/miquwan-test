﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_WheelJoint2DGenerated
struct UnityEngine_WheelJoint2DGenerated_t303455654;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_WheelJoint2DGenerated::.ctor()
extern "C"  void UnityEngine_WheelJoint2DGenerated__ctor_m2673769093 (UnityEngine_WheelJoint2DGenerated_t303455654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WheelJoint2DGenerated::WheelJoint2D_WheelJoint2D1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WheelJoint2DGenerated_WheelJoint2D_WheelJoint2D1_m566797421 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelJoint2DGenerated::WheelJoint2D_suspension(JSVCall)
extern "C"  void UnityEngine_WheelJoint2DGenerated_WheelJoint2D_suspension_m1957571881 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelJoint2DGenerated::WheelJoint2D_useMotor(JSVCall)
extern "C"  void UnityEngine_WheelJoint2DGenerated_WheelJoint2D_useMotor_m605700344 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelJoint2DGenerated::WheelJoint2D_motor(JSVCall)
extern "C"  void UnityEngine_WheelJoint2DGenerated_WheelJoint2D_motor_m3091813841 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelJoint2DGenerated::WheelJoint2D_jointTranslation(JSVCall)
extern "C"  void UnityEngine_WheelJoint2DGenerated_WheelJoint2D_jointTranslation_m366863487 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelJoint2DGenerated::WheelJoint2D_jointSpeed(JSVCall)
extern "C"  void UnityEngine_WheelJoint2DGenerated_WheelJoint2D_jointSpeed_m3755536169 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WheelJoint2DGenerated::WheelJoint2D_GetMotorTorque__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WheelJoint2DGenerated_WheelJoint2D_GetMotorTorque__Single_m1128684854 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelJoint2DGenerated::__Register()
extern "C"  void UnityEngine_WheelJoint2DGenerated___Register_m3274952034 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_WheelJoint2DGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_WheelJoint2DGenerated_ilo_getObject1_m1277686865 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WheelJoint2DGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_WheelJoint2DGenerated_ilo_attachFinalizerObject2_m465313619 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_WheelJoint2DGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_WheelJoint2DGenerated_ilo_getObject3_m3909091074 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelJoint2DGenerated::ilo_setSingle4(System.Int32,System.Single)
extern "C"  void UnityEngine_WheelJoint2DGenerated_ilo_setSingle4_m4130231634 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_WheelJoint2DGenerated::ilo_getSingle5(System.Int32)
extern "C"  float UnityEngine_WheelJoint2DGenerated_ilo_getSingle5_m3593866926 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
