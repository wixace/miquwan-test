﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Serialization.UnityObjectConverter
struct UnityObjectConverter_t174134550;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t696267445;
// Pathfinding.UnityReferenceHelper
struct UnityReferenceHelper_t2955464730;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_UnityReferenceHelper2955464730.h"

// System.Void Pathfinding.Serialization.UnityObjectConverter::.ctor()
extern "C"  void UnityObjectConverter__ctor_m2050870311 (UnityObjectConverter_t174134550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Serialization.UnityObjectConverter::CanConvert(System.Type)
extern "C"  bool UnityObjectConverter_CanConvert_m27315205 (UnityObjectConverter_t174134550 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Pathfinding.Serialization.UnityObjectConverter::ReadJson(System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  Il2CppObject * UnityObjectConverter_ReadJson_m1475047260 (UnityObjectConverter_t174134550 * __this, Type_t * ___objectType0, Dictionary_2_t696267445 * ___values1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> Pathfinding.Serialization.UnityObjectConverter::WriteJson(System.Type,System.Object)
extern "C"  Dictionary_2_t696267445 * UnityObjectConverter_WriteJson_m3040245943 (UnityObjectConverter_t174134550 * __this, Type_t * ___type0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.UnityObjectConverter::ilo_Reset1(Pathfinding.UnityReferenceHelper)
extern "C"  void UnityObjectConverter_ilo_Reset1_m2084414884 (Il2CppObject * __this /* static, unused */, UnityReferenceHelper_t2955464730 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.Serialization.UnityObjectConverter::ilo_GetGUID2(Pathfinding.UnityReferenceHelper)
extern "C"  String_t* UnityObjectConverter_ilo_GetGUID2_m630454520 (Il2CppObject * __this /* static, unused */, UnityReferenceHelper_t2955464730 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
