﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_rallcaTanirzear68
struct  M_rallcaTanirzear68_t1718997467  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_rallcaTanirzear68::_zismawHideller
	int32_t ____zismawHideller_0;
	// System.UInt32 GarbageiOS.M_rallcaTanirzear68::_mirbouPepallsair
	uint32_t ____mirbouPepallsair_1;
	// System.Boolean GarbageiOS.M_rallcaTanirzear68::_neepereGelgou
	bool ____neepereGelgou_2;
	// System.UInt32 GarbageiOS.M_rallcaTanirzear68::_yudrarear
	uint32_t ____yudrarear_3;
	// System.Single GarbageiOS.M_rallcaTanirzear68::_mereleeXashor
	float ____mereleeXashor_4;
	// System.Single GarbageiOS.M_rallcaTanirzear68::_takeepe
	float ____takeepe_5;
	// System.UInt32 GarbageiOS.M_rallcaTanirzear68::_steatoojall
	uint32_t ____steatoojall_6;
	// System.Boolean GarbageiOS.M_rallcaTanirzear68::_ripair
	bool ____ripair_7;
	// System.Int32 GarbageiOS.M_rallcaTanirzear68::_neta
	int32_t ____neta_8;
	// System.Int32 GarbageiOS.M_rallcaTanirzear68::_mawfisayChayla
	int32_t ____mawfisayChayla_9;
	// System.String GarbageiOS.M_rallcaTanirzear68::_hostemYemmis
	String_t* ____hostemYemmis_10;
	// System.Single GarbageiOS.M_rallcaTanirzear68::_letemsear
	float ____letemsear_11;
	// System.String GarbageiOS.M_rallcaTanirzear68::_hutecar
	String_t* ____hutecar_12;
	// System.UInt32 GarbageiOS.M_rallcaTanirzear68::_tirnar
	uint32_t ____tirnar_13;
	// System.String GarbageiOS.M_rallcaTanirzear68::_cemsoupeBaysou
	String_t* ____cemsoupeBaysou_14;
	// System.Boolean GarbageiOS.M_rallcaTanirzear68::_jarle
	bool ____jarle_15;
	// System.Boolean GarbageiOS.M_rallcaTanirzear68::_kerkoupisGaschel
	bool ____kerkoupisGaschel_16;
	// System.Single GarbageiOS.M_rallcaTanirzear68::_ritu
	float ____ritu_17;
	// System.Int32 GarbageiOS.M_rallcaTanirzear68::_daimanay
	int32_t ____daimanay_18;
	// System.Single GarbageiOS.M_rallcaTanirzear68::_dafea
	float ____dafea_19;
	// System.Int32 GarbageiOS.M_rallcaTanirzear68::_tesofairMurriba
	int32_t ____tesofairMurriba_20;
	// System.Int32 GarbageiOS.M_rallcaTanirzear68::_nujall
	int32_t ____nujall_21;
	// System.Single GarbageiOS.M_rallcaTanirzear68::_fesailiMete
	float ____fesailiMete_22;
	// System.Single GarbageiOS.M_rallcaTanirzear68::_layxemeJokaqu
	float ____layxemeJokaqu_23;
	// System.Boolean GarbageiOS.M_rallcaTanirzear68::_mexurraPererersar
	bool ____mexurraPererersar_24;
	// System.UInt32 GarbageiOS.M_rallcaTanirzear68::_zoostoroo
	uint32_t ____zoostoroo_25;

public:
	inline static int32_t get_offset_of__zismawHideller_0() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____zismawHideller_0)); }
	inline int32_t get__zismawHideller_0() const { return ____zismawHideller_0; }
	inline int32_t* get_address_of__zismawHideller_0() { return &____zismawHideller_0; }
	inline void set__zismawHideller_0(int32_t value)
	{
		____zismawHideller_0 = value;
	}

	inline static int32_t get_offset_of__mirbouPepallsair_1() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____mirbouPepallsair_1)); }
	inline uint32_t get__mirbouPepallsair_1() const { return ____mirbouPepallsair_1; }
	inline uint32_t* get_address_of__mirbouPepallsair_1() { return &____mirbouPepallsair_1; }
	inline void set__mirbouPepallsair_1(uint32_t value)
	{
		____mirbouPepallsair_1 = value;
	}

	inline static int32_t get_offset_of__neepereGelgou_2() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____neepereGelgou_2)); }
	inline bool get__neepereGelgou_2() const { return ____neepereGelgou_2; }
	inline bool* get_address_of__neepereGelgou_2() { return &____neepereGelgou_2; }
	inline void set__neepereGelgou_2(bool value)
	{
		____neepereGelgou_2 = value;
	}

	inline static int32_t get_offset_of__yudrarear_3() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____yudrarear_3)); }
	inline uint32_t get__yudrarear_3() const { return ____yudrarear_3; }
	inline uint32_t* get_address_of__yudrarear_3() { return &____yudrarear_3; }
	inline void set__yudrarear_3(uint32_t value)
	{
		____yudrarear_3 = value;
	}

	inline static int32_t get_offset_of__mereleeXashor_4() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____mereleeXashor_4)); }
	inline float get__mereleeXashor_4() const { return ____mereleeXashor_4; }
	inline float* get_address_of__mereleeXashor_4() { return &____mereleeXashor_4; }
	inline void set__mereleeXashor_4(float value)
	{
		____mereleeXashor_4 = value;
	}

	inline static int32_t get_offset_of__takeepe_5() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____takeepe_5)); }
	inline float get__takeepe_5() const { return ____takeepe_5; }
	inline float* get_address_of__takeepe_5() { return &____takeepe_5; }
	inline void set__takeepe_5(float value)
	{
		____takeepe_5 = value;
	}

	inline static int32_t get_offset_of__steatoojall_6() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____steatoojall_6)); }
	inline uint32_t get__steatoojall_6() const { return ____steatoojall_6; }
	inline uint32_t* get_address_of__steatoojall_6() { return &____steatoojall_6; }
	inline void set__steatoojall_6(uint32_t value)
	{
		____steatoojall_6 = value;
	}

	inline static int32_t get_offset_of__ripair_7() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____ripair_7)); }
	inline bool get__ripair_7() const { return ____ripair_7; }
	inline bool* get_address_of__ripair_7() { return &____ripair_7; }
	inline void set__ripair_7(bool value)
	{
		____ripair_7 = value;
	}

	inline static int32_t get_offset_of__neta_8() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____neta_8)); }
	inline int32_t get__neta_8() const { return ____neta_8; }
	inline int32_t* get_address_of__neta_8() { return &____neta_8; }
	inline void set__neta_8(int32_t value)
	{
		____neta_8 = value;
	}

	inline static int32_t get_offset_of__mawfisayChayla_9() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____mawfisayChayla_9)); }
	inline int32_t get__mawfisayChayla_9() const { return ____mawfisayChayla_9; }
	inline int32_t* get_address_of__mawfisayChayla_9() { return &____mawfisayChayla_9; }
	inline void set__mawfisayChayla_9(int32_t value)
	{
		____mawfisayChayla_9 = value;
	}

	inline static int32_t get_offset_of__hostemYemmis_10() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____hostemYemmis_10)); }
	inline String_t* get__hostemYemmis_10() const { return ____hostemYemmis_10; }
	inline String_t** get_address_of__hostemYemmis_10() { return &____hostemYemmis_10; }
	inline void set__hostemYemmis_10(String_t* value)
	{
		____hostemYemmis_10 = value;
		Il2CppCodeGenWriteBarrier(&____hostemYemmis_10, value);
	}

	inline static int32_t get_offset_of__letemsear_11() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____letemsear_11)); }
	inline float get__letemsear_11() const { return ____letemsear_11; }
	inline float* get_address_of__letemsear_11() { return &____letemsear_11; }
	inline void set__letemsear_11(float value)
	{
		____letemsear_11 = value;
	}

	inline static int32_t get_offset_of__hutecar_12() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____hutecar_12)); }
	inline String_t* get__hutecar_12() const { return ____hutecar_12; }
	inline String_t** get_address_of__hutecar_12() { return &____hutecar_12; }
	inline void set__hutecar_12(String_t* value)
	{
		____hutecar_12 = value;
		Il2CppCodeGenWriteBarrier(&____hutecar_12, value);
	}

	inline static int32_t get_offset_of__tirnar_13() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____tirnar_13)); }
	inline uint32_t get__tirnar_13() const { return ____tirnar_13; }
	inline uint32_t* get_address_of__tirnar_13() { return &____tirnar_13; }
	inline void set__tirnar_13(uint32_t value)
	{
		____tirnar_13 = value;
	}

	inline static int32_t get_offset_of__cemsoupeBaysou_14() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____cemsoupeBaysou_14)); }
	inline String_t* get__cemsoupeBaysou_14() const { return ____cemsoupeBaysou_14; }
	inline String_t** get_address_of__cemsoupeBaysou_14() { return &____cemsoupeBaysou_14; }
	inline void set__cemsoupeBaysou_14(String_t* value)
	{
		____cemsoupeBaysou_14 = value;
		Il2CppCodeGenWriteBarrier(&____cemsoupeBaysou_14, value);
	}

	inline static int32_t get_offset_of__jarle_15() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____jarle_15)); }
	inline bool get__jarle_15() const { return ____jarle_15; }
	inline bool* get_address_of__jarle_15() { return &____jarle_15; }
	inline void set__jarle_15(bool value)
	{
		____jarle_15 = value;
	}

	inline static int32_t get_offset_of__kerkoupisGaschel_16() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____kerkoupisGaschel_16)); }
	inline bool get__kerkoupisGaschel_16() const { return ____kerkoupisGaschel_16; }
	inline bool* get_address_of__kerkoupisGaschel_16() { return &____kerkoupisGaschel_16; }
	inline void set__kerkoupisGaschel_16(bool value)
	{
		____kerkoupisGaschel_16 = value;
	}

	inline static int32_t get_offset_of__ritu_17() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____ritu_17)); }
	inline float get__ritu_17() const { return ____ritu_17; }
	inline float* get_address_of__ritu_17() { return &____ritu_17; }
	inline void set__ritu_17(float value)
	{
		____ritu_17 = value;
	}

	inline static int32_t get_offset_of__daimanay_18() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____daimanay_18)); }
	inline int32_t get__daimanay_18() const { return ____daimanay_18; }
	inline int32_t* get_address_of__daimanay_18() { return &____daimanay_18; }
	inline void set__daimanay_18(int32_t value)
	{
		____daimanay_18 = value;
	}

	inline static int32_t get_offset_of__dafea_19() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____dafea_19)); }
	inline float get__dafea_19() const { return ____dafea_19; }
	inline float* get_address_of__dafea_19() { return &____dafea_19; }
	inline void set__dafea_19(float value)
	{
		____dafea_19 = value;
	}

	inline static int32_t get_offset_of__tesofairMurriba_20() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____tesofairMurriba_20)); }
	inline int32_t get__tesofairMurriba_20() const { return ____tesofairMurriba_20; }
	inline int32_t* get_address_of__tesofairMurriba_20() { return &____tesofairMurriba_20; }
	inline void set__tesofairMurriba_20(int32_t value)
	{
		____tesofairMurriba_20 = value;
	}

	inline static int32_t get_offset_of__nujall_21() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____nujall_21)); }
	inline int32_t get__nujall_21() const { return ____nujall_21; }
	inline int32_t* get_address_of__nujall_21() { return &____nujall_21; }
	inline void set__nujall_21(int32_t value)
	{
		____nujall_21 = value;
	}

	inline static int32_t get_offset_of__fesailiMete_22() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____fesailiMete_22)); }
	inline float get__fesailiMete_22() const { return ____fesailiMete_22; }
	inline float* get_address_of__fesailiMete_22() { return &____fesailiMete_22; }
	inline void set__fesailiMete_22(float value)
	{
		____fesailiMete_22 = value;
	}

	inline static int32_t get_offset_of__layxemeJokaqu_23() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____layxemeJokaqu_23)); }
	inline float get__layxemeJokaqu_23() const { return ____layxemeJokaqu_23; }
	inline float* get_address_of__layxemeJokaqu_23() { return &____layxemeJokaqu_23; }
	inline void set__layxemeJokaqu_23(float value)
	{
		____layxemeJokaqu_23 = value;
	}

	inline static int32_t get_offset_of__mexurraPererersar_24() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____mexurraPererersar_24)); }
	inline bool get__mexurraPererersar_24() const { return ____mexurraPererersar_24; }
	inline bool* get_address_of__mexurraPererersar_24() { return &____mexurraPererersar_24; }
	inline void set__mexurraPererersar_24(bool value)
	{
		____mexurraPererersar_24 = value;
	}

	inline static int32_t get_offset_of__zoostoroo_25() { return static_cast<int32_t>(offsetof(M_rallcaTanirzear68_t1718997467, ____zoostoroo_25)); }
	inline uint32_t get__zoostoroo_25() const { return ____zoostoroo_25; }
	inline uint32_t* get_address_of__zoostoroo_25() { return &____zoostoroo_25; }
	inline void set__zoostoroo_25(uint32_t value)
	{
		____zoostoroo_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
