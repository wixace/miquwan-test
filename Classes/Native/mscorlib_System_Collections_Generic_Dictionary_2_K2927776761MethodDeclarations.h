﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,HatredCtrl/stValue>
struct KeyCollection_t2927776761;
// System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>
struct Dictionary_2_t1301017310;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1915953364.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,HatredCtrl/stValue>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m523256344_gshared (KeyCollection_t2927776761 * __this, Dictionary_2_t1301017310 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m523256344(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2927776761 *, Dictionary_2_t1301017310 *, const MethodInfo*))KeyCollection__ctor_m523256344_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,HatredCtrl/stValue>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2255104062_gshared (KeyCollection_t2927776761 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2255104062(__this, ___item0, method) ((  void (*) (KeyCollection_t2927776761 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2255104062_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,HatredCtrl/stValue>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1593394485_gshared (KeyCollection_t2927776761 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1593394485(__this, method) ((  void (*) (KeyCollection_t2927776761 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1593394485_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,HatredCtrl/stValue>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2639252176_gshared (KeyCollection_t2927776761 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2639252176(__this, ___item0, method) ((  bool (*) (KeyCollection_t2927776761 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2639252176_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,HatredCtrl/stValue>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3887116341_gshared (KeyCollection_t2927776761 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3887116341(__this, ___item0, method) ((  bool (*) (KeyCollection_t2927776761 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3887116341_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,HatredCtrl/stValue>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1898391879_gshared (KeyCollection_t2927776761 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1898391879(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2927776761 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1898391879_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,HatredCtrl/stValue>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m599523879_gshared (KeyCollection_t2927776761 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m599523879(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2927776761 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m599523879_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,HatredCtrl/stValue>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1005180150_gshared (KeyCollection_t2927776761 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1005180150(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2927776761 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1005180150_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,HatredCtrl/stValue>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m876727857_gshared (KeyCollection_t2927776761 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m876727857(__this, method) ((  bool (*) (KeyCollection_t2927776761 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m876727857_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,HatredCtrl/stValue>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m230010915_gshared (KeyCollection_t2927776761 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m230010915(__this, method) ((  bool (*) (KeyCollection_t2927776761 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m230010915_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,HatredCtrl/stValue>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2647228245_gshared (KeyCollection_t2927776761 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2647228245(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2927776761 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2647228245_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,HatredCtrl/stValue>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2884372685_gshared (KeyCollection_t2927776761 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m2884372685(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2927776761 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2884372685_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,HatredCtrl/stValue>::GetEnumerator()
extern "C"  Enumerator_t1915953364  KeyCollection_GetEnumerator_m3262716634_gshared (KeyCollection_t2927776761 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m3262716634(__this, method) ((  Enumerator_t1915953364  (*) (KeyCollection_t2927776761 *, const MethodInfo*))KeyCollection_GetEnumerator_m3262716634_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,HatredCtrl/stValue>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1550142813_gshared (KeyCollection_t2927776761 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m1550142813(__this, method) ((  int32_t (*) (KeyCollection_t2927776761 *, const MethodInfo*))KeyCollection_get_Count_m1550142813_gshared)(__this, method)
