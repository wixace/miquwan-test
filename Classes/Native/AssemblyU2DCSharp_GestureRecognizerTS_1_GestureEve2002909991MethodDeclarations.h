﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GestureRecognizerTS`1/GestureEventHandler<System.Object>
struct GestureEventHandler_t2002909991;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void GestureRecognizerTS`1/GestureEventHandler<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void GestureEventHandler__ctor_m3273015516_gshared (GestureEventHandler_t2002909991 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define GestureEventHandler__ctor_m3273015516(__this, ___object0, ___method1, method) ((  void (*) (GestureEventHandler_t2002909991 *, Il2CppObject *, IntPtr_t, const MethodInfo*))GestureEventHandler__ctor_m3273015516_gshared)(__this, ___object0, ___method1, method)
// System.Void GestureRecognizerTS`1/GestureEventHandler<System.Object>::Invoke(T)
extern "C"  void GestureEventHandler_Invoke_m61391848_gshared (GestureEventHandler_t2002909991 * __this, Il2CppObject * ___gesture0, const MethodInfo* method);
#define GestureEventHandler_Invoke_m61391848(__this, ___gesture0, method) ((  void (*) (GestureEventHandler_t2002909991 *, Il2CppObject *, const MethodInfo*))GestureEventHandler_Invoke_m61391848_gshared)(__this, ___gesture0, method)
// System.IAsyncResult GestureRecognizerTS`1/GestureEventHandler<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GestureEventHandler_BeginInvoke_m2633461941_gshared (GestureEventHandler_t2002909991 * __this, Il2CppObject * ___gesture0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define GestureEventHandler_BeginInvoke_m2633461941(__this, ___gesture0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (GestureEventHandler_t2002909991 *, Il2CppObject *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))GestureEventHandler_BeginInvoke_m2633461941_gshared)(__this, ___gesture0, ___callback1, ___object2, method)
// System.Void GestureRecognizerTS`1/GestureEventHandler<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void GestureEventHandler_EndInvoke_m1518633196_gshared (GestureEventHandler_t2002909991 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define GestureEventHandler_EndInvoke_m1518633196(__this, ___result0, method) ((  void (*) (GestureEventHandler_t2002909991 *, Il2CppObject *, const MethodInfo*))GestureEventHandler_EndInvoke_m1518633196_gshared)(__this, ___result0, method)
