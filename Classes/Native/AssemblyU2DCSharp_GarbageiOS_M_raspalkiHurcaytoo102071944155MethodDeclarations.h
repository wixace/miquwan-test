﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_raspalkiHurcaytoo103
struct M_raspalkiHurcaytoo103_t2071944155;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_raspalkiHurcaytoo103::.ctor()
extern "C"  void M_raspalkiHurcaytoo103__ctor_m1374721208 (M_raspalkiHurcaytoo103_t2071944155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_raspalkiHurcaytoo103::M_wherjayNibemja0(System.String[],System.Int32)
extern "C"  void M_raspalkiHurcaytoo103_M_wherjayNibemja0_m641523849 (M_raspalkiHurcaytoo103_t2071944155 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_raspalkiHurcaytoo103::M_tapoomaSasootir1(System.String[],System.Int32)
extern "C"  void M_raspalkiHurcaytoo103_M_tapoomaSasootir1_m1346522999 (M_raspalkiHurcaytoo103_t2071944155 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_raspalkiHurcaytoo103::M_temem2(System.String[],System.Int32)
extern "C"  void M_raspalkiHurcaytoo103_M_temem2_m431491139 (M_raspalkiHurcaytoo103_t2071944155 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
