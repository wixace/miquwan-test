﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CsObjectComparer
struct CsObjectComparer_t3718533436;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void CsObjectComparer::.ctor()
extern "C"  void CsObjectComparer__ctor_m2704439519 (CsObjectComparer_t3718533436 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CsObjectComparer::Equals(System.Object,System.Object)
extern "C"  bool CsObjectComparer_Equals_m1851861290 (CsObjectComparer_t3718533436 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CsObjectComparer::GetHashCode(System.Object)
extern "C"  int32_t CsObjectComparer_GetHashCode_m203504404 (CsObjectComparer_t3718533436 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
