﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TweenRotationGenerated
struct TweenRotationGenerated_t945496230;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void TweenRotationGenerated::.ctor()
extern "C"  void TweenRotationGenerated__ctor_m851734197 (TweenRotationGenerated_t945496230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenRotationGenerated::TweenRotation_TweenRotation1(JSVCall,System.Int32)
extern "C"  bool TweenRotationGenerated_TweenRotation_TweenRotation1_m152864913 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenRotationGenerated::TweenRotation_from(JSVCall)
extern "C"  void TweenRotationGenerated_TweenRotation_from_m1926083640 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenRotationGenerated::TweenRotation_to(JSVCall)
extern "C"  void TweenRotationGenerated_TweenRotation_to_m899477383 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenRotationGenerated::TweenRotation_cachedTransform(JSVCall)
extern "C"  void TweenRotationGenerated_TweenRotation_cachedTransform_m2141170624 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenRotationGenerated::TweenRotation_value(JSVCall)
extern "C"  void TweenRotationGenerated_TweenRotation_value_m464449433 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenRotationGenerated::TweenRotation_SetEndToCurrentValue(JSVCall,System.Int32)
extern "C"  bool TweenRotationGenerated_TweenRotation_SetEndToCurrentValue_m3856762965 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenRotationGenerated::TweenRotation_SetStartToCurrentValue(JSVCall,System.Int32)
extern "C"  bool TweenRotationGenerated_TweenRotation_SetStartToCurrentValue_m2818377436 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenRotationGenerated::TweenRotation_Begin__GameObject__Single__Quaternion(JSVCall,System.Int32)
extern "C"  bool TweenRotationGenerated_TweenRotation_Begin__GameObject__Single__Quaternion_m4101395001 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenRotationGenerated::__Register()
extern "C"  void TweenRotationGenerated___Register_m927921970 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TweenRotationGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t TweenRotationGenerated_ilo_getObject1_m1790083325 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenRotationGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void TweenRotationGenerated_ilo_addJSCSRel2_m2586669938 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TweenRotationGenerated::ilo_getVector3S3(System.Int32)
extern "C"  Vector3_t4282066566  TweenRotationGenerated_ilo_getVector3S3_m3005517159 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TweenRotationGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t TweenRotationGenerated_ilo_setObject4_m3932379960 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
