﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pomelo.DotNetClient.PackageProtocol
struct PackageProtocol_t2179241540;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// Pomelo.DotNetClient.Package
struct Package_t3219283596;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_PackageType3964380262.h"

// System.Void Pomelo.DotNetClient.PackageProtocol::.ctor()
extern "C"  void PackageProtocol__ctor_m884157320 (PackageProtocol_t2179241540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.DotNetClient.PackageProtocol::encode(Pomelo.DotNetClient.PackageType)
extern "C"  ByteU5BU5D_t4260760469* PackageProtocol_encode_m285025253 (Il2CppObject * __this /* static, unused */, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.DotNetClient.PackageProtocol::encode(Pomelo.DotNetClient.PackageType,System.Byte[])
extern "C"  ByteU5BU5D_t4260760469* PackageProtocol_encode_m148600872 (Il2CppObject * __this /* static, unused */, int32_t ___type0, ByteU5BU5D_t4260760469* ___body1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pomelo.DotNetClient.Package Pomelo.DotNetClient.PackageProtocol::decode(System.Byte[])
extern "C"  Package_t3219283596 * PackageProtocol_decode_m79812903 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___buf0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
