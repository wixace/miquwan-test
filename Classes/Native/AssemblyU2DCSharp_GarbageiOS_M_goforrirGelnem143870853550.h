﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_goforrirGelnem143
struct  M_goforrirGelnem143_t870853550  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_goforrirGelnem143::_tecopuRuqearxee
	float ____tecopuRuqearxee_0;
	// System.Single GarbageiOS.M_goforrirGelnem143::_zirabiWhasper
	float ____zirabiWhasper_1;
	// System.Single GarbageiOS.M_goforrirGelnem143::_serekitayCirfecis
	float ____serekitayCirfecis_2;
	// System.String GarbageiOS.M_goforrirGelnem143::_detayne
	String_t* ____detayne_3;
	// System.UInt32 GarbageiOS.M_goforrirGelnem143::_rowpairCerebempa
	uint32_t ____rowpairCerebempa_4;
	// System.String GarbageiOS.M_goforrirGelnem143::_rallwhetem
	String_t* ____rallwhetem_5;

public:
	inline static int32_t get_offset_of__tecopuRuqearxee_0() { return static_cast<int32_t>(offsetof(M_goforrirGelnem143_t870853550, ____tecopuRuqearxee_0)); }
	inline float get__tecopuRuqearxee_0() const { return ____tecopuRuqearxee_0; }
	inline float* get_address_of__tecopuRuqearxee_0() { return &____tecopuRuqearxee_0; }
	inline void set__tecopuRuqearxee_0(float value)
	{
		____tecopuRuqearxee_0 = value;
	}

	inline static int32_t get_offset_of__zirabiWhasper_1() { return static_cast<int32_t>(offsetof(M_goforrirGelnem143_t870853550, ____zirabiWhasper_1)); }
	inline float get__zirabiWhasper_1() const { return ____zirabiWhasper_1; }
	inline float* get_address_of__zirabiWhasper_1() { return &____zirabiWhasper_1; }
	inline void set__zirabiWhasper_1(float value)
	{
		____zirabiWhasper_1 = value;
	}

	inline static int32_t get_offset_of__serekitayCirfecis_2() { return static_cast<int32_t>(offsetof(M_goforrirGelnem143_t870853550, ____serekitayCirfecis_2)); }
	inline float get__serekitayCirfecis_2() const { return ____serekitayCirfecis_2; }
	inline float* get_address_of__serekitayCirfecis_2() { return &____serekitayCirfecis_2; }
	inline void set__serekitayCirfecis_2(float value)
	{
		____serekitayCirfecis_2 = value;
	}

	inline static int32_t get_offset_of__detayne_3() { return static_cast<int32_t>(offsetof(M_goforrirGelnem143_t870853550, ____detayne_3)); }
	inline String_t* get__detayne_3() const { return ____detayne_3; }
	inline String_t** get_address_of__detayne_3() { return &____detayne_3; }
	inline void set__detayne_3(String_t* value)
	{
		____detayne_3 = value;
		Il2CppCodeGenWriteBarrier(&____detayne_3, value);
	}

	inline static int32_t get_offset_of__rowpairCerebempa_4() { return static_cast<int32_t>(offsetof(M_goforrirGelnem143_t870853550, ____rowpairCerebempa_4)); }
	inline uint32_t get__rowpairCerebempa_4() const { return ____rowpairCerebempa_4; }
	inline uint32_t* get_address_of__rowpairCerebempa_4() { return &____rowpairCerebempa_4; }
	inline void set__rowpairCerebempa_4(uint32_t value)
	{
		____rowpairCerebempa_4 = value;
	}

	inline static int32_t get_offset_of__rallwhetem_5() { return static_cast<int32_t>(offsetof(M_goforrirGelnem143_t870853550, ____rallwhetem_5)); }
	inline String_t* get__rallwhetem_5() const { return ____rallwhetem_5; }
	inline String_t** get_address_of__rallwhetem_5() { return &____rallwhetem_5; }
	inline void set__rallwhetem_5(String_t* value)
	{
		____rallwhetem_5 = value;
		Il2CppCodeGenWriteBarrier(&____rallwhetem_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
