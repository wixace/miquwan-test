﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIColorPicker
struct UIColorPicker_t4217226909;
// UIWidget
struct UIWidget_t769069560;
// UnityEngine.Camera
struct Camera_t2727095145;
// UICamera
struct UICamera_t189364953;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_UIColorPicker4217226909.h"
#include "AssemblyU2DCSharp_UICamera189364953.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"

// System.Void UIColorPicker::.ctor()
extern "C"  void UIColorPicker__ctor_m42318702 (UIColorPicker_t4217226909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIColorPicker::Start()
extern "C"  void UIColorPicker_Start_m3284423790 (UIColorPicker_t4217226909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIColorPicker::OnDestroy()
extern "C"  void UIColorPicker_OnDestroy_m420489575 (UIColorPicker_t4217226909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIColorPicker::OnPress(System.Boolean)
extern "C"  void UIColorPicker_OnPress_m2075769831 (UIColorPicker_t4217226909 * __this, bool ___pressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIColorPicker::OnDrag(UnityEngine.Vector2)
extern "C"  void UIColorPicker_OnDrag_m1629159249 (UIColorPicker_t4217226909 * __this, Vector2_t4282066565  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIColorPicker::OnPan(UnityEngine.Vector2)
extern "C"  void UIColorPicker_OnPan_m4293219376 (UIColorPicker_t4217226909 * __this, Vector2_t4282066565  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIColorPicker::Sample()
extern "C"  void UIColorPicker_Sample_m3691712000 (UIColorPicker_t4217226909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIColorPicker::Select(UnityEngine.Vector2)
extern "C"  void UIColorPicker_Select_m2053163432 (UIColorPicker_t4217226909 * __this, Vector2_t4282066565  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UIColorPicker::Select(UnityEngine.Color)
extern "C"  Vector2_t4282066565  UIColorPicker_Select_m204342767 (UIColorPicker_t4217226909 * __this, Color_t4194546905  ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UIColorPicker::Sample(System.Single,System.Single)
extern "C"  Color_t4194546905  UIColorPicker_Sample_m155550679 (Il2CppObject * __this /* static, unused */, float ___x0, float ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIColorPicker::OnDestory()
extern "C"  void UIColorPicker_OnDestory_m417808385 (UIColorPicker_t4217226909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIColorPicker::ilo_get_width1(UIWidget)
extern "C"  int32_t UIColorPicker_ilo_get_width1_m617827799 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIColorPicker::ilo_get_height2(UIWidget)
extern "C"  int32_t UIColorPicker_ilo_get_height2_m4143476273 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIColorPicker::ilo_Sample3(UIColorPicker)
extern "C"  void UIColorPicker_ilo_Sample3_m2429263091 (Il2CppObject * __this /* static, unused */, UIColorPicker_t4217226909 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UIColorPicker::ilo_get_cachedCamera4(UICamera)
extern "C"  Camera_t2727095145 * UIColorPicker_ilo_get_cachedCamera4_m3589999365 (Il2CppObject * __this /* static, unused */, UICamera_t189364953 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIColorPicker::ilo_OverlayPosition5(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Camera)
extern "C"  void UIColorPicker_ilo_OverlayPosition5_m3151353887 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ___trans0, Vector3_t4282066566  ___worldPos1, Camera_t2727095145 * ___worldCam2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UIColorPicker::ilo_Sample6(System.Single,System.Single)
extern "C"  Color_t4194546905  UIColorPicker_ilo_Sample6_m1219890628 (Il2CppObject * __this /* static, unused */, float ___x0, float ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
