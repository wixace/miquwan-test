﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CEvent_ZEventCenterGenerated
struct CEvent_ZEventCenterGenerated_t206911506;
// JSVCall
struct JSVCall_t3708497963;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Delegate
struct Delegate_t3310234105;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "mscorlib_System_String7231557.h"

// System.Void CEvent_ZEventCenterGenerated::.ctor()
extern "C"  void CEvent_ZEventCenterGenerated__ctor_m1423212233 (CEvent_ZEventCenterGenerated_t206911506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CEvent_ZEventCenterGenerated::ZEventCenter_ZEventCenter1(JSVCall,System.Int32)
extern "C"  bool CEvent_ZEventCenterGenerated_ZEventCenter_ZEventCenter1_m3159674641 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CEvent.EventFunc`1<CEvent.ZEvent> CEvent_ZEventCenterGenerated::ZEventCenter_AddEventListener_GetDelegate_member0_arg1(CSRepresentedObject)
extern "C"  EventFunc_1_t1114914310 * CEvent_ZEventCenterGenerated_ZEventCenter_AddEventListener_GetDelegate_member0_arg1_m1306444297 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CEvent_ZEventCenterGenerated::ZEventCenter_AddEventListener__String__EventFuncT1_ZEvent__Boolean(JSVCall,System.Int32)
extern "C"  bool CEvent_ZEventCenterGenerated_ZEventCenter_AddEventListener__String__EventFuncT1_ZEvent__Boolean_m398145467 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CEvent.EventFunc`1<CEvent.ZEvent> CEvent_ZEventCenterGenerated::ZEventCenter_AddEventListener_GetDelegate_member1_arg1(CSRepresentedObject)
extern "C"  EventFunc_1_t1114914310 * CEvent_ZEventCenterGenerated_ZEventCenter_AddEventListener_GetDelegate_member1_arg1_m2268058314 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CEvent_ZEventCenterGenerated::ZEventCenter_AddEventListener__String__EventFuncT1_ZEvent(JSVCall,System.Int32)
extern "C"  bool CEvent_ZEventCenterGenerated_ZEventCenter_AddEventListener__String__EventFuncT1_ZEvent_m1356123727 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CEvent_ZEventCenterGenerated::ZEventCenter_DispatchEvent__ZEvent(JSVCall,System.Int32)
extern "C"  bool CEvent_ZEventCenterGenerated_ZEventCenter_DispatchEvent__ZEvent_m630358133 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CEvent.EventFunc`1<CEvent.ZEvent> CEvent_ZEventCenterGenerated::ZEventCenter_RemoveEventListener_GetDelegate_member3_arg1(CSRepresentedObject)
extern "C"  EventFunc_1_t1114914310 * CEvent_ZEventCenterGenerated_ZEventCenter_RemoveEventListener_GetDelegate_member3_arg1_m3628492665 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CEvent_ZEventCenterGenerated::ZEventCenter_RemoveEventListener__String__EventFuncT1_ZEvent(JSVCall,System.Int32)
extern "C"  bool CEvent_ZEventCenterGenerated_ZEventCenter_RemoveEventListener__String__EventFuncT1_ZEvent_m285230932 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent_ZEventCenterGenerated::__Register()
extern "C"  void CEvent_ZEventCenterGenerated___Register_m1323946910 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CEvent.EventFunc`1<CEvent.ZEvent> CEvent_ZEventCenterGenerated::<ZEventCenter_AddEventListener__String__EventFuncT1_ZEvent__Boolean>m__1A()
extern "C"  EventFunc_1_t1114914310 * CEvent_ZEventCenterGenerated_U3CZEventCenter_AddEventListener__String__EventFuncT1_ZEvent__BooleanU3Em__1A_m3282321270 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CEvent.EventFunc`1<CEvent.ZEvent> CEvent_ZEventCenterGenerated::<ZEventCenter_AddEventListener__String__EventFuncT1_ZEvent>m__1C()
extern "C"  EventFunc_1_t1114914310 * CEvent_ZEventCenterGenerated_U3CZEventCenter_AddEventListener__String__EventFuncT1_ZEventU3Em__1C_m3489238004 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CEvent.EventFunc`1<CEvent.ZEvent> CEvent_ZEventCenterGenerated::<ZEventCenter_RemoveEventListener__String__EventFuncT1_ZEvent>m__1E()
extern "C"  EventFunc_1_t1114914310 * CEvent_ZEventCenterGenerated_U3CZEventCenter_RemoveEventListener__String__EventFuncT1_ZEventU3Em__1E_m2323793619 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CEvent_ZEventCenterGenerated::ilo_getStringS1(System.Int32)
extern "C"  String_t* CEvent_ZEventCenterGenerated_ilo_getStringS1_m1695776583 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CEvent_ZEventCenterGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * CEvent_ZEventCenterGenerated_ilo_getObject2_m3517012871 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent_ZEventCenterGenerated::ilo_addJSFunCSDelegateRel3(System.Int32,System.Delegate)
extern "C"  void CEvent_ZEventCenterGenerated_ilo_addJSFunCSDelegateRel3_m3624545195 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, Delegate_t3310234105 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent_ZEventCenterGenerated::ilo_RemoveEventListener4(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void CEvent_ZEventCenterGenerated_ilo_RemoveEventListener4_m25717867 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject CEvent_ZEventCenterGenerated::ilo_getFunctionS5(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * CEvent_ZEventCenterGenerated_ilo_getFunctionS5_m2336213752 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
