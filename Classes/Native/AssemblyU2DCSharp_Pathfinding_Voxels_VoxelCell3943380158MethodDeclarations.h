﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Voxels.VoxelCell
struct VoxelCell_t3943380158;
struct VoxelCell_t3943380158_marshaled_pinvoke;
struct VoxelCell_t3943380158_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_VoxelCell3943380158.h"

// System.Void Pathfinding.Voxels.VoxelCell::AddSpan(System.UInt32,System.UInt32,System.Int32,System.Int32)
extern "C"  void VoxelCell_AddSpan_m444601755 (VoxelCell_t3943380158 * __this, uint32_t ___bottom0, uint32_t ___top1, int32_t ___area2, int32_t ___voxelWalkableClimb3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.VoxelCell::ilo_Abs1(System.Int32)
extern "C"  int32_t VoxelCell_ilo_Abs1_m1198561899 (Il2CppObject * __this /* static, unused */, int32_t ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct VoxelCell_t3943380158;
struct VoxelCell_t3943380158_marshaled_pinvoke;

extern "C" void VoxelCell_t3943380158_marshal_pinvoke(const VoxelCell_t3943380158& unmarshaled, VoxelCell_t3943380158_marshaled_pinvoke& marshaled);
extern "C" void VoxelCell_t3943380158_marshal_pinvoke_back(const VoxelCell_t3943380158_marshaled_pinvoke& marshaled, VoxelCell_t3943380158& unmarshaled);
extern "C" void VoxelCell_t3943380158_marshal_pinvoke_cleanup(VoxelCell_t3943380158_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct VoxelCell_t3943380158;
struct VoxelCell_t3943380158_marshaled_com;

extern "C" void VoxelCell_t3943380158_marshal_com(const VoxelCell_t3943380158& unmarshaled, VoxelCell_t3943380158_marshaled_com& marshaled);
extern "C" void VoxelCell_t3943380158_marshal_com_back(const VoxelCell_t3943380158_marshaled_com& marshaled, VoxelCell_t3943380158& unmarshaled);
extern "C" void VoxelCell_t3943380158_marshal_com_cleanup(VoxelCell_t3943380158_marshaled_com& marshaled);
