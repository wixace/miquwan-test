﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._8fd36b464ecc5562c6018682938b75c9
struct _8fd36b464ecc5562c6018682938b75c9_t1782997813;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._8fd36b464ecc5562c6018682938b75c9::.ctor()
extern "C"  void _8fd36b464ecc5562c6018682938b75c9__ctor_m2050002584 (_8fd36b464ecc5562c6018682938b75c9_t1782997813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8fd36b464ecc5562c6018682938b75c9::_8fd36b464ecc5562c6018682938b75c9m2(System.Int32)
extern "C"  int32_t _8fd36b464ecc5562c6018682938b75c9__8fd36b464ecc5562c6018682938b75c9m2_m4165934457 (_8fd36b464ecc5562c6018682938b75c9_t1782997813 * __this, int32_t ____8fd36b464ecc5562c6018682938b75c9a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8fd36b464ecc5562c6018682938b75c9::_8fd36b464ecc5562c6018682938b75c9m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _8fd36b464ecc5562c6018682938b75c9__8fd36b464ecc5562c6018682938b75c9m_m1890624861 (_8fd36b464ecc5562c6018682938b75c9_t1782997813 * __this, int32_t ____8fd36b464ecc5562c6018682938b75c9a0, int32_t ____8fd36b464ecc5562c6018682938b75c981, int32_t ____8fd36b464ecc5562c6018682938b75c9c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
