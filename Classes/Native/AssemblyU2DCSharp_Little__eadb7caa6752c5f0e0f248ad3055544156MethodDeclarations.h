﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._eadb7caa6752c5f0e0f248add71b2f4a
struct _eadb7caa6752c5f0e0f248add71b2f4a_t3055544156;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__eadb7caa6752c5f0e0f248ad3055544156.h"

// System.Void Little._eadb7caa6752c5f0e0f248add71b2f4a::.ctor()
extern "C"  void _eadb7caa6752c5f0e0f248add71b2f4a__ctor_m2281424465 (_eadb7caa6752c5f0e0f248add71b2f4a_t3055544156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._eadb7caa6752c5f0e0f248add71b2f4a::_eadb7caa6752c5f0e0f248add71b2f4am2(System.Int32)
extern "C"  int32_t _eadb7caa6752c5f0e0f248add71b2f4a__eadb7caa6752c5f0e0f248add71b2f4am2_m2913006617 (_eadb7caa6752c5f0e0f248add71b2f4a_t3055544156 * __this, int32_t ____eadb7caa6752c5f0e0f248add71b2f4aa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._eadb7caa6752c5f0e0f248add71b2f4a::_eadb7caa6752c5f0e0f248add71b2f4am(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _eadb7caa6752c5f0e0f248add71b2f4a__eadb7caa6752c5f0e0f248add71b2f4am_m2077196989 (_eadb7caa6752c5f0e0f248add71b2f4a_t3055544156 * __this, int32_t ____eadb7caa6752c5f0e0f248add71b2f4aa0, int32_t ____eadb7caa6752c5f0e0f248add71b2f4a371, int32_t ____eadb7caa6752c5f0e0f248add71b2f4ac2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._eadb7caa6752c5f0e0f248add71b2f4a::ilo__eadb7caa6752c5f0e0f248add71b2f4am21(Little._eadb7caa6752c5f0e0f248add71b2f4a,System.Int32)
extern "C"  int32_t _eadb7caa6752c5f0e0f248add71b2f4a_ilo__eadb7caa6752c5f0e0f248add71b2f4am21_m2162789539 (Il2CppObject * __this /* static, unused */, _eadb7caa6752c5f0e0f248add71b2f4a_t3055544156 * ____this0, int32_t ____eadb7caa6752c5f0e0f248add71b2f4aa1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
