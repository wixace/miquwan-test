﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RelevantGraphSurface
struct RelevantGraphSurface_t4201206834;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_RelevantGraphSurface4201206834.h"

// System.Void Pathfinding.RelevantGraphSurface::.ctor()
extern "C"  void RelevantGraphSurface__ctor_m2077471349 (RelevantGraphSurface_t4201206834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RelevantGraphSurface::get_Position()
extern "C"  Vector3_t4282066566  RelevantGraphSurface_get_Position_m2125372187 (RelevantGraphSurface_t4201206834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RelevantGraphSurface Pathfinding.RelevantGraphSurface::get_Next()
extern "C"  RelevantGraphSurface_t4201206834 * RelevantGraphSurface_get_Next_m2040822146 (RelevantGraphSurface_t4201206834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RelevantGraphSurface Pathfinding.RelevantGraphSurface::get_Prev()
extern "C"  RelevantGraphSurface_t4201206834 * RelevantGraphSurface_get_Prev_m2109522114 (RelevantGraphSurface_t4201206834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RelevantGraphSurface Pathfinding.RelevantGraphSurface::get_Root()
extern "C"  RelevantGraphSurface_t4201206834 * RelevantGraphSurface_get_Root_m2164305841 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RelevantGraphSurface::UpdatePosition()
extern "C"  void RelevantGraphSurface_UpdatePosition_m3478984513 (RelevantGraphSurface_t4201206834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RelevantGraphSurface::OnEnable()
extern "C"  void RelevantGraphSurface_OnEnable_m1541801201 (RelevantGraphSurface_t4201206834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RelevantGraphSurface::OnDisable()
extern "C"  void RelevantGraphSurface_OnDisable_m992134236 (RelevantGraphSurface_t4201206834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RelevantGraphSurface::UpdateAllPositions()
extern "C"  void RelevantGraphSurface_UpdateAllPositions_m2670283361 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RelevantGraphSurface::FindAllGraphSurfaces()
extern "C"  void RelevantGraphSurface_FindAllGraphSurfaces_m3041113531 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RelevantGraphSurface::OnDrawGizmos()
extern "C"  void RelevantGraphSurface_OnDrawGizmos_m334733579 (RelevantGraphSurface_t4201206834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RelevantGraphSurface::OnDrawGizmosSelected()
extern "C"  void RelevantGraphSurface_OnDrawGizmosSelected_m57248518 (RelevantGraphSurface_t4201206834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RelevantGraphSurface::ilo_OnEnable1(Pathfinding.RelevantGraphSurface)
extern "C"  void RelevantGraphSurface_ilo_OnEnable1_m552993737 (Il2CppObject * __this /* static, unused */, RelevantGraphSurface_t4201206834 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
