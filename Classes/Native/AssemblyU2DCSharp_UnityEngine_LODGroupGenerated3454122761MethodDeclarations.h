﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_LODGroupGenerated
struct UnityEngine_LODGroupGenerated_t3454122761;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.LOD[]
struct LODU5BU5D_t1411784526;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_LODGroupGenerated::.ctor()
extern "C"  void UnityEngine_LODGroupGenerated__ctor_m1366372482 (UnityEngine_LODGroupGenerated_t3454122761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LODGroupGenerated::LODGroup_LODGroup1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LODGroupGenerated_LODGroup_LODGroup1_m1884031600 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LODGroupGenerated::LODGroup_localReferencePoint(JSVCall)
extern "C"  void UnityEngine_LODGroupGenerated_LODGroup_localReferencePoint_m1140886390 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LODGroupGenerated::LODGroup_size(JSVCall)
extern "C"  void UnityEngine_LODGroupGenerated_LODGroup_size_m2756653413 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LODGroupGenerated::LODGroup_lodCount(JSVCall)
extern "C"  void UnityEngine_LODGroupGenerated_LODGroup_lodCount_m2019482040 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LODGroupGenerated::LODGroup_fadeMode(JSVCall)
extern "C"  void UnityEngine_LODGroupGenerated_LODGroup_fadeMode_m1091629287 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LODGroupGenerated::LODGroup_animateCrossFading(JSVCall)
extern "C"  void UnityEngine_LODGroupGenerated_LODGroup_animateCrossFading_m1858404430 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LODGroupGenerated::LODGroup_enabled(JSVCall)
extern "C"  void UnityEngine_LODGroupGenerated_LODGroup_enabled_m2342867109 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LODGroupGenerated::LODGroup_crossFadeAnimationDuration(JSVCall)
extern "C"  void UnityEngine_LODGroupGenerated_LODGroup_crossFadeAnimationDuration_m1305029130 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LODGroupGenerated::LODGroup_ForceLOD__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LODGroupGenerated_LODGroup_ForceLOD__Int32_m3888994549 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LODGroupGenerated::LODGroup_GetLODs(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LODGroupGenerated_LODGroup_GetLODs_m2180409669 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LODGroupGenerated::LODGroup_RecalculateBounds(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LODGroupGenerated_LODGroup_RecalculateBounds_m2057666469 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LODGroupGenerated::LODGroup_SetLODs__LOD_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LODGroupGenerated_LODGroup_SetLODs__LOD_Array_m913316940 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LODGroupGenerated::__Register()
extern "C"  void UnityEngine_LODGroupGenerated___Register_m423906693 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.LOD[] UnityEngine_LODGroupGenerated::<LODGroup_SetLODs__LOD_Array>m__25C()
extern "C"  LODU5BU5D_t1411784526* UnityEngine_LODGroupGenerated_U3CLODGroup_SetLODs__LOD_ArrayU3Em__25C_m1499974060 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LODGroupGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_LODGroupGenerated_ilo_addJSCSRel1_m3982634558 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_LODGroupGenerated::ilo_getVector3S2(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_LODGroupGenerated_ilo_getVector3S2_m1001730437 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LODGroupGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void UnityEngine_LODGroupGenerated_ilo_setSingle3_m1726108084 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_LODGroupGenerated::ilo_getSingle4(System.Int32)
extern "C"  float UnityEngine_LODGroupGenerated_ilo_getSingle4_m414021776 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LODGroupGenerated::ilo_setInt325(System.Int32,System.Int32)
extern "C"  void UnityEngine_LODGroupGenerated_ilo_setInt325_m1586108688 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LODGroupGenerated::ilo_setEnum6(System.Int32,System.Int32)
extern "C"  void UnityEngine_LODGroupGenerated_ilo_setEnum6_m649714430 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LODGroupGenerated::ilo_setBooleanS7(System.Int32,System.Boolean)
extern "C"  void UnityEngine_LODGroupGenerated_ilo_setBooleanS7_m1239820077 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_LODGroupGenerated::ilo_setObject8(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_LODGroupGenerated_ilo_setObject8_m1524731571 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_LODGroupGenerated::ilo_getObject9(System.Int32)
extern "C"  int32_t UnityEngine_LODGroupGenerated_ilo_getObject9_m2805139836 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_LODGroupGenerated::ilo_getElement10(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_LODGroupGenerated_ilo_getElement10_m1498259194 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_LODGroupGenerated::ilo_getObject11(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_LODGroupGenerated_ilo_getObject11_m3654194622 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
