//
//  MiquwanSDK.h
//  MiquwanSDK
//
//  Created by MiQuWang on 2019/10/25.
//  Copyright © 2019 LQX. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for MiquwanSDK.
FOUNDATION_EXPORT double MiquwanSDKVersionNumber;

//! Project version string for MiquwanSDK.
FOUNDATION_EXPORT const unsigned char MiquwanSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MiquwanSDK/PublicHeader.h>







#import "MQWPlatform.h"
#import "MQWNotification.h"
#import "MQWHud.h"




