﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>>
struct Transform_1_t287965411;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21199798016.h"
#include "AssemblyU2DCSharp_HatredCtrl_stValue3425945410.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m59148818_gshared (Transform_1_t287965411 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m59148818(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t287965411 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m59148818_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1199798016  Transform_1_Invoke_m4083138086_gshared (Transform_1_t287965411 * __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m4083138086(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t1199798016  (*) (Transform_1_t287965411 *, Il2CppObject *, stValue_t3425945410 , const MethodInfo*))Transform_1_Invoke_m4083138086_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m369224273_gshared (Transform_1_t287965411 * __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m369224273(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t287965411 *, Il2CppObject *, stValue_t3425945410 , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m369224273_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1199798016  Transform_1_EndInvoke_m2466688676_gshared (Transform_1_t287965411 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m2466688676(__this, ___result0, method) ((  KeyValuePair_2_t1199798016  (*) (Transform_1_t287965411 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m2466688676_gshared)(__this, ___result0, method)
