﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReplayData
struct ReplayData_t779762769;
// ReplayOtherPlayer
struct ReplayOtherPlayer_t1774842762;
// System.String
struct String_t;
// ReplayBase
struct ReplayBase_t779703160;
// System.Collections.Generic.List`1<ReplayBase>
struct List_1_t2147888712;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<ReplayBase>>
struct Dictionary_2_t2968307082;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ReplayOtherPlayer1774842762.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ReplayBase779703160.h"

// System.Void ReplayData::.ctor()
extern "C"  void ReplayData__ctor_m2555488106 (ReplayData_t779762769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayData::.ctor(ReplayOtherPlayer,ReplayOtherPlayer)
extern "C"  void ReplayData__ctor_m3958394976 (ReplayData_t779762769 * __this, ReplayOtherPlayer_t1774842762 * ___mine0, ReplayOtherPlayer_t1774842762 * ___enemy1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayData::Init(System.String)
extern "C"  void ReplayData_Init_m2180010392 (ReplayData_t779762769 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayData::ParserJson()
extern "C"  void ReplayData_ParserJson_m667173313 (ReplayData_t779762769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayData::SetJson()
extern "C"  void ReplayData_SetJson_m3375886514 (ReplayData_t779762769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReplayData::ToJson()
extern "C"  String_t* ReplayData_ToJson_m3779427904 (ReplayData_t779762769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReplayOtherPlayer ReplayData::GetMine()
extern "C"  ReplayOtherPlayer_t1774842762 * ReplayData_GetMine_m3546737640 (ReplayData_t779762769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReplayOtherPlayer ReplayData::GetEnemy()
extern "C"  ReplayOtherPlayer_t1774842762 * ReplayData_GetEnemy_m4199739829 (ReplayData_t779762769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayData::AddReplayBase(System.String,ReplayBase)
extern "C"  void ReplayData_AddReplayBase_m2046148269 (ReplayData_t779762769 * __this, String_t* ___key0, ReplayBase_t779703160 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<ReplayBase> ReplayData::GetReplayDatas(System.String)
extern "C"  List_1_t2147888712 * ReplayData_GetReplayDatas_m3808082577 (ReplayData_t779762769 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<ReplayBase>> ReplayData::GetReplayDataDics()
extern "C"  Dictionary_2_t2968307082 * ReplayData_GetReplayDataDics_m2071948478 (ReplayData_t779762769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayData::ilo_ParserJson1(ReplayOtherPlayer,System.String)
extern "C"  void ReplayData_ilo_ParserJson1_m3770659813 (Il2CppObject * __this /* static, unused */, ReplayOtherPlayer_t1774842762 * ____this0, String_t* ___json1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] ReplayData::ilo_ParserJsonStr2(ReplayBase,System.String)
extern "C"  StringU5BU5D_t4054002952* ReplayData_ilo_ParserJsonStr2_m2329891002 (Il2CppObject * __this /* static, unused */, ReplayBase_t779703160 * ____this0, String_t* ___json1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
