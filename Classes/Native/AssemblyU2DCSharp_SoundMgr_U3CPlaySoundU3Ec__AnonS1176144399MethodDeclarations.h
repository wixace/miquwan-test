﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SoundMgr/<PlaySound>c__AnonStorey15A
struct U3CPlaySoundU3Ec__AnonStorey15A_t1176144399;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset778728221.h"

// System.Void SoundMgr/<PlaySound>c__AnonStorey15A::.ctor()
extern "C"  void U3CPlaySoundU3Ec__AnonStorey15A__ctor_m3282010476 (U3CPlaySoundU3Ec__AnonStorey15A_t1176144399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr/<PlaySound>c__AnonStorey15A::<>m__3E4(Mihua.Asset.ABLoadOperation.AssetOperation)
extern "C"  void U3CPlaySoundU3Ec__AnonStorey15A_U3CU3Em__3E4_m4149436790 (U3CPlaySoundU3Ec__AnonStorey15A_t1176144399 * __this, AssetOperation_t778728221 * ___ao0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
