﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraAnis
struct CameraAnis_t4275747580;
// ProtoBuf.IExtension
struct IExtension_t1606339106;
// System.Collections.Generic.List`1<CameraAniMap>
struct List_1_t77874261;

#include "codegen/il2cpp-codegen.h"

// System.Void CameraAnis::.ctor()
extern "C"  void CameraAnis__ctor_m2259515679 (CameraAnis_t4275747580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension CameraAnis::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * CameraAnis_ProtoBuf_IExtensible_GetExtensionObject_m987363695 (CameraAnis_t4275747580 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CameraAniMap> CameraAnis::get_cameraMaps()
extern "C"  List_1_t77874261 * CameraAnis_get_cameraMaps_m120487266 (CameraAnis_t4275747580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
