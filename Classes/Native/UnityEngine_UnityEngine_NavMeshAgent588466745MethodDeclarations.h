﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.NavMeshAgent
struct NavMeshAgent_t588466745;
// UnityEngine.NavMeshPath
struct NavMeshPath_t384806059;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_NavMeshAgent588466745.h"
#include "UnityEngine_UnityEngine_OffMeshLinkData3329424662.h"
#include "UnityEngine_UnityEngine_NavMeshPathStatus2429954621.h"
#include "UnityEngine_UnityEngine_NavMeshPath384806059.h"
#include "UnityEngine_UnityEngine_NavMeshHit1624713351.h"
#include "UnityEngine_UnityEngine_ObstacleAvoidanceType2902649541.h"

// System.Void UnityEngine.NavMeshAgent::.ctor()
extern "C"  void NavMeshAgent__ctor_m1311914200 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshAgent::SetDestination(UnityEngine.Vector3)
extern "C"  bool NavMeshAgent_SetDestination_m1934812347 (NavMeshAgent_t588466745 * __this, Vector3_t4282066566  ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshAgent::INTERNAL_CALL_SetDestination(UnityEngine.NavMeshAgent,UnityEngine.Vector3&)
extern "C"  bool NavMeshAgent_INTERNAL_CALL_SetDestination_m1043531080 (Il2CppObject * __this /* static, unused */, NavMeshAgent_t588466745 * ___self0, Vector3_t4282066566 * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.NavMeshAgent::get_destination()
extern "C"  Vector3_t4282066566  NavMeshAgent_get_destination_m2210292865 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::set_destination(UnityEngine.Vector3)
extern "C"  void NavMeshAgent_set_destination_m1040949554 (NavMeshAgent_t588466745 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::INTERNAL_get_destination(UnityEngine.Vector3&)
extern "C"  void NavMeshAgent_INTERNAL_get_destination_m950103490 (NavMeshAgent_t588466745 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::INTERNAL_set_destination(UnityEngine.Vector3&)
extern "C"  void NavMeshAgent_INTERNAL_set_destination_m612325326 (NavMeshAgent_t588466745 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.NavMeshAgent::get_stoppingDistance()
extern "C"  float NavMeshAgent_get_stoppingDistance_m1449561130 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::set_stoppingDistance(System.Single)
extern "C"  void NavMeshAgent_set_stoppingDistance_m4184856153 (NavMeshAgent_t588466745 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.NavMeshAgent::get_velocity()
extern "C"  Vector3_t4282066566  NavMeshAgent_get_velocity_m2089438444 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::set_velocity(UnityEngine.Vector3)
extern "C"  void NavMeshAgent_set_velocity_m2183624243 (NavMeshAgent_t588466745 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::INTERNAL_get_velocity(UnityEngine.Vector3&)
extern "C"  void NavMeshAgent_INTERNAL_get_velocity_m3428405265 (NavMeshAgent_t588466745 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::INTERNAL_set_velocity(UnityEngine.Vector3&)
extern "C"  void NavMeshAgent_INTERNAL_set_velocity_m2849407365 (NavMeshAgent_t588466745 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.NavMeshAgent::get_nextPosition()
extern "C"  Vector3_t4282066566  NavMeshAgent_get_nextPosition_m3232465547 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::set_nextPosition(UnityEngine.Vector3)
extern "C"  void NavMeshAgent_set_nextPosition_m245986100 (NavMeshAgent_t588466745 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::INTERNAL_get_nextPosition(UnityEngine.Vector3&)
extern "C"  void NavMeshAgent_INTERNAL_get_nextPosition_m3093899056 (NavMeshAgent_t588466745 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::INTERNAL_set_nextPosition(UnityEngine.Vector3&)
extern "C"  void NavMeshAgent_INTERNAL_set_nextPosition_m1212710564 (NavMeshAgent_t588466745 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.NavMeshAgent::get_steeringTarget()
extern "C"  Vector3_t4282066566  NavMeshAgent_get_steeringTarget_m774585777 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::INTERNAL_get_steeringTarget(UnityEngine.Vector3&)
extern "C"  void NavMeshAgent_INTERNAL_get_steeringTarget_m1728239318 (NavMeshAgent_t588466745 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.NavMeshAgent::get_desiredVelocity()
extern "C"  Vector3_t4282066566  NavMeshAgent_get_desiredVelocity_m2559123146 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::INTERNAL_get_desiredVelocity(UnityEngine.Vector3&)
extern "C"  void NavMeshAgent_INTERNAL_get_desiredVelocity_m125379211 (NavMeshAgent_t588466745 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.NavMeshAgent::get_remainingDistance()
extern "C"  float NavMeshAgent_get_remainingDistance_m2123497836 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.NavMeshAgent::get_baseOffset()
extern "C"  float NavMeshAgent_get_baseOffset_m3332395941 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::set_baseOffset(System.Single)
extern "C"  void NavMeshAgent_set_baseOffset_m3850150078 (NavMeshAgent_t588466745 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshAgent::get_isOnOffMeshLink()
extern "C"  bool NavMeshAgent_get_isOnOffMeshLink_m2660539252 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::ActivateCurrentOffMeshLink(System.Boolean)
extern "C"  void NavMeshAgent_ActivateCurrentOffMeshLink_m2142198995 (NavMeshAgent_t588466745 * __this, bool ___activated0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.OffMeshLinkData UnityEngine.NavMeshAgent::get_currentOffMeshLinkData()
extern "C"  OffMeshLinkData_t3329424662  NavMeshAgent_get_currentOffMeshLinkData_m1370188454 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.OffMeshLinkData UnityEngine.NavMeshAgent::GetCurrentOffMeshLinkDataInternal()
extern "C"  OffMeshLinkData_t3329424662  NavMeshAgent_GetCurrentOffMeshLinkDataInternal_m3976973126 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.OffMeshLinkData UnityEngine.NavMeshAgent::get_nextOffMeshLinkData()
extern "C"  OffMeshLinkData_t3329424662  NavMeshAgent_get_nextOffMeshLinkData_m2309943984 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.OffMeshLinkData UnityEngine.NavMeshAgent::GetNextOffMeshLinkDataInternal()
extern "C"  OffMeshLinkData_t3329424662  NavMeshAgent_GetNextOffMeshLinkDataInternal_m1575830570 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::CompleteOffMeshLink()
extern "C"  void NavMeshAgent_CompleteOffMeshLink_m1517572371 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshAgent::get_autoTraverseOffMeshLink()
extern "C"  bool NavMeshAgent_get_autoTraverseOffMeshLink_m2026690780 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::set_autoTraverseOffMeshLink(System.Boolean)
extern "C"  void NavMeshAgent_set_autoTraverseOffMeshLink_m2573137285 (NavMeshAgent_t588466745 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshAgent::get_autoBraking()
extern "C"  bool NavMeshAgent_get_autoBraking_m4292644416 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::set_autoBraking(System.Boolean)
extern "C"  void NavMeshAgent_set_autoBraking_m1372628713 (NavMeshAgent_t588466745 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshAgent::get_autoRepath()
extern "C"  bool NavMeshAgent_get_autoRepath_m1666205410 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::set_autoRepath(System.Boolean)
extern "C"  void NavMeshAgent_set_autoRepath_m826591079 (NavMeshAgent_t588466745 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshAgent::get_hasPath()
extern "C"  bool NavMeshAgent_get_hasPath_m1075821862 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshAgent::get_pathPending()
extern "C"  bool NavMeshAgent_get_pathPending_m716492089 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshAgent::get_isPathStale()
extern "C"  bool NavMeshAgent_get_isPathStale_m692635121 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NavMeshPathStatus UnityEngine.NavMeshAgent::get_pathStatus()
extern "C"  int32_t NavMeshAgent_get_pathStatus_m1753966575 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.NavMeshAgent::get_pathEndPosition()
extern "C"  Vector3_t4282066566  NavMeshAgent_get_pathEndPosition_m1914885362 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::INTERNAL_get_pathEndPosition(UnityEngine.Vector3&)
extern "C"  void NavMeshAgent_INTERNAL_get_pathEndPosition_m3506582707 (NavMeshAgent_t588466745 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshAgent::Warp(UnityEngine.Vector3)
extern "C"  bool NavMeshAgent_Warp_m3181925567 (NavMeshAgent_t588466745 * __this, Vector3_t4282066566  ___newPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshAgent::INTERNAL_CALL_Warp(UnityEngine.NavMeshAgent,UnityEngine.Vector3&)
extern "C"  bool NavMeshAgent_INTERNAL_CALL_Warp_m1697680780 (Il2CppObject * __this /* static, unused */, NavMeshAgent_t588466745 * ___self0, Vector3_t4282066566 * ___newPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::Move(UnityEngine.Vector3)
extern "C"  void NavMeshAgent_Move_m4234479548 (NavMeshAgent_t588466745 * __this, Vector3_t4282066566  ___offset0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::INTERNAL_CALL_Move(UnityEngine.NavMeshAgent,UnityEngine.Vector3&)
extern "C"  void NavMeshAgent_INTERNAL_CALL_Move_m1459901385 (Il2CppObject * __this /* static, unused */, NavMeshAgent_t588466745 * ___self0, Vector3_t4282066566 * ___offset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::Stop()
extern "C"  void NavMeshAgent_Stop_m2641168622 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::StopInternal()
extern "C"  void NavMeshAgent_StopInternal_m152023947 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::Resume()
extern "C"  void NavMeshAgent_Resume_m2083126169 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::ResetPath()
extern "C"  void NavMeshAgent_ResetPath_m3980369034 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshAgent::SetPath(UnityEngine.NavMeshPath)
extern "C"  bool NavMeshAgent_SetPath_m1772794365 (NavMeshAgent_t588466745 * __this, NavMeshPath_t384806059 * ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NavMeshPath UnityEngine.NavMeshAgent::get_path()
extern "C"  NavMeshPath_t384806059 * NavMeshAgent_get_path_m1191632175 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::set_path(UnityEngine.NavMeshPath)
extern "C"  void NavMeshAgent_set_path_m1679630566 (NavMeshAgent_t588466745 * __this, NavMeshPath_t384806059 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::CopyPathTo(UnityEngine.NavMeshPath)
extern "C"  void NavMeshAgent_CopyPathTo_m1203331955 (NavMeshAgent_t588466745 * __this, NavMeshPath_t384806059 * ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshAgent::FindClosestEdge(UnityEngine.NavMeshHit&)
extern "C"  bool NavMeshAgent_FindClosestEdge_m342378521 (NavMeshAgent_t588466745 * __this, NavMeshHit_t1624713351 * ___hit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshAgent::Raycast(UnityEngine.Vector3,UnityEngine.NavMeshHit&)
extern "C"  bool NavMeshAgent_Raycast_m3784521856 (NavMeshAgent_t588466745 * __this, Vector3_t4282066566  ___targetPosition0, NavMeshHit_t1624713351 * ___hit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshAgent::INTERNAL_CALL_Raycast(UnityEngine.NavMeshAgent,UnityEngine.Vector3&,UnityEngine.NavMeshHit&)
extern "C"  bool NavMeshAgent_INTERNAL_CALL_Raycast_m2479412779 (Il2CppObject * __this /* static, unused */, NavMeshAgent_t588466745 * ___self0, Vector3_t4282066566 * ___targetPosition1, NavMeshHit_t1624713351 * ___hit2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshAgent::CalculatePath(UnityEngine.Vector3,UnityEngine.NavMeshPath)
extern "C"  bool NavMeshAgent_CalculatePath_m4132323788 (NavMeshAgent_t588466745 * __this, Vector3_t4282066566  ___targetPosition0, NavMeshPath_t384806059 * ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshAgent::CalculatePathInternal(UnityEngine.Vector3,UnityEngine.NavMeshPath)
extern "C"  bool NavMeshAgent_CalculatePathInternal_m4009899023 (NavMeshAgent_t588466745 * __this, Vector3_t4282066566  ___targetPosition0, NavMeshPath_t384806059 * ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshAgent::INTERNAL_CALL_CalculatePathInternal(UnityEngine.NavMeshAgent,UnityEngine.Vector3&,UnityEngine.NavMeshPath)
extern "C"  bool NavMeshAgent_INTERNAL_CALL_CalculatePathInternal_m1367361594 (Il2CppObject * __this /* static, unused */, NavMeshAgent_t588466745 * ___self0, Vector3_t4282066566 * ___targetPosition1, NavMeshPath_t384806059 * ___path2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshAgent::SamplePathPosition(System.Int32,System.Single,UnityEngine.NavMeshHit&)
extern "C"  bool NavMeshAgent_SamplePathPosition_m3008802624 (NavMeshAgent_t588466745 * __this, int32_t ___areaMask0, float ___maxDistance1, NavMeshHit_t1624713351 * ___hit2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::SetAreaCost(System.Int32,System.Single)
extern "C"  void NavMeshAgent_SetAreaCost_m2639637416 (NavMeshAgent_t588466745 * __this, int32_t ___areaIndex0, float ___areaCost1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.NavMeshAgent::GetAreaCost(System.Int32)
extern "C"  float NavMeshAgent_GetAreaCost_m351090539 (NavMeshAgent_t588466745 * __this, int32_t ___areaIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.NavMeshAgent::get_areaMask()
extern "C"  int32_t NavMeshAgent_get_areaMask_m1621758510 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::set_areaMask(System.Int32)
extern "C"  void NavMeshAgent_set_areaMask_m2706925427 (NavMeshAgent_t588466745 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.NavMeshAgent::get_speed()
extern "C"  float NavMeshAgent_get_speed_m3376630440 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::set_speed(System.Single)
extern "C"  void NavMeshAgent_set_speed_m3553192555 (NavMeshAgent_t588466745 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.NavMeshAgent::get_angularSpeed()
extern "C"  float NavMeshAgent_get_angularSpeed_m3882604582 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::set_angularSpeed(System.Single)
extern "C"  void NavMeshAgent_set_angularSpeed_m1505554397 (NavMeshAgent_t588466745 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.NavMeshAgent::get_acceleration()
extern "C"  float NavMeshAgent_get_acceleration_m2003974753 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::set_acceleration(System.Single)
extern "C"  void NavMeshAgent_set_acceleration_m2026063234 (NavMeshAgent_t588466745 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshAgent::get_updatePosition()
extern "C"  bool NavMeshAgent_get_updatePosition_m3252325581 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::set_updatePosition(System.Boolean)
extern "C"  void NavMeshAgent_set_updatePosition_m2814304018 (NavMeshAgent_t588466745 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshAgent::get_updateRotation()
extern "C"  bool NavMeshAgent_get_updateRotation_m1797046754 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::set_updateRotation(System.Boolean)
extern "C"  void NavMeshAgent_set_updateRotation_m3246394215 (NavMeshAgent_t588466745 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.NavMeshAgent::get_radius()
extern "C"  float NavMeshAgent_get_radius_m3696475635 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::set_radius(System.Single)
extern "C"  void NavMeshAgent_set_radius_m2835345712 (NavMeshAgent_t588466745 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.NavMeshAgent::get_height()
extern "C"  float NavMeshAgent_get_height_m2844201288 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::set_height(System.Single)
extern "C"  void NavMeshAgent_set_height_m229601275 (NavMeshAgent_t588466745 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ObstacleAvoidanceType UnityEngine.NavMeshAgent::get_obstacleAvoidanceType()
extern "C"  int32_t NavMeshAgent_get_obstacleAvoidanceType_m2146442113 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::set_obstacleAvoidanceType(UnityEngine.ObstacleAvoidanceType)
extern "C"  void NavMeshAgent_set_obstacleAvoidanceType_m3520554098 (NavMeshAgent_t588466745 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.NavMeshAgent::get_avoidancePriority()
extern "C"  int32_t NavMeshAgent_get_avoidancePriority_m1269686581 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::set_avoidancePriority(System.Int32)
extern "C"  void NavMeshAgent_set_avoidancePriority_m716459154 (NavMeshAgent_t588466745 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshAgent::get_isOnNavMesh()
extern "C"  bool NavMeshAgent_get_isOnNavMesh_m469159694 (NavMeshAgent_t588466745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
