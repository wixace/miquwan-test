﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.Mail.SmtpClient/CancellationException
struct CancellationException_t2018147685;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Net.Mail.SmtpClient/CancellationException::.ctor()
extern "C"  void CancellationException__ctor_m3190316247 (CancellationException_t2018147685 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
