﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Converters.XmlNodeConverter
struct XmlNodeConverter_t567010885;
// System.String
struct String_t;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;
// Newtonsoft.Json.Converters.IXmlNode
struct IXmlNode_t2349752174;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t1467853467;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// System.Type
struct Type_t;
// Newtonsoft.Json.Converters.IXmlDocument
struct IXmlDocument_t1497221255;
// Newtonsoft.Json.Converters.IXmlElement
struct IXmlElement_t2583566720;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Converters.IXmlNode>
struct IEnumerable_1_t1355697835;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Converters.IXmlNode>
struct IList_1_t749432081;
// Newtonsoft.Json.Converters.IXmlDeclaration
struct IXmlDeclaration_t2218682878;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer251850770.h"
#include "System_Xml_System_Xml_XmlNamespaceManager1467853467.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader816925123.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_XmlNod567010885.h"
#include "System_Xml_System_Xml_XmlNodeType992114213.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonToken4173078175.h"

// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::.ctor()
extern "C"  void XmlNodeConverter__ctor_m2472052734 (XmlNodeConverter_t567010885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlNodeConverter::get_DeserializeRootElementName()
extern "C"  String_t* XmlNodeConverter_get_DeserializeRootElementName_m3879429810 (XmlNodeConverter_t567010885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::set_DeserializeRootElementName(System.String)
extern "C"  void XmlNodeConverter_set_DeserializeRootElementName_m3092863801 (XmlNodeConverter_t567010885 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::get_WriteArrayAttribute()
extern "C"  bool XmlNodeConverter_get_WriteArrayAttribute_m219780457 (XmlNodeConverter_t567010885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::set_WriteArrayAttribute(System.Boolean)
extern "C"  void XmlNodeConverter_set_WriteArrayAttribute_m3395668344 (XmlNodeConverter_t567010885 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::get_OmitRootObject()
extern "C"  bool XmlNodeConverter_get_OmitRootObject_m2975710981 (XmlNodeConverter_t567010885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::set_OmitRootObject(System.Boolean)
extern "C"  void XmlNodeConverter_set_OmitRootObject_m67667556 (XmlNodeConverter_t567010885 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  void XmlNodeConverter_WriteJson_m1812059286 (XmlNodeConverter_t567010885 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, JsonSerializer_t251850770 * ___serializer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeConverter::WrapXml(System.Object)
extern "C"  Il2CppObject * XmlNodeConverter_WrapXml_m617411182 (XmlNodeConverter_t567010885 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::PushParentNamespaces(Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager)
extern "C"  void XmlNodeConverter_PushParentNamespaces_m4021842777 (XmlNodeConverter_t567010885 * __this, Il2CppObject * ___node0, XmlNamespaceManager_t1467853467 * ___manager1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlNodeConverter::ResolveFullName(Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager)
extern "C"  String_t* XmlNodeConverter_ResolveFullName_m1021095068 (XmlNodeConverter_t567010885 * __this, Il2CppObject * ___node0, XmlNamespaceManager_t1467853467 * ___manager1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlNodeConverter::GetPropertyName(Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager)
extern "C"  String_t* XmlNodeConverter_GetPropertyName_m3829159244 (XmlNodeConverter_t567010885 * __this, Il2CppObject * ___node0, XmlNamespaceManager_t1467853467 * ___manager1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::IsArray(Newtonsoft.Json.Converters.IXmlNode)
extern "C"  bool XmlNodeConverter_IsArray_m415743613 (XmlNodeConverter_t567010885 * __this, Il2CppObject * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::SerializeGroupedNodes(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager,System.Boolean)
extern "C"  void XmlNodeConverter_SerializeGroupedNodes_m1980391103 (XmlNodeConverter_t567010885 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___node1, XmlNamespaceManager_t1467853467 * ___manager2, bool ___writePropertyName3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::SerializeNode(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager,System.Boolean)
extern "C"  void XmlNodeConverter_SerializeNode_m257388430 (XmlNodeConverter_t567010885 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___node1, XmlNamespaceManager_t1467853467 * ___manager2, bool ___writePropertyName3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Converters.XmlNodeConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  Il2CppObject * XmlNodeConverter_ReadJson_m2881845767 (XmlNodeConverter_t567010885 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, Il2CppObject * ___existingValue2, JsonSerializer_t251850770 * ___serializer3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::DeserializeValue(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,System.Xml.XmlNamespaceManager,System.String,Newtonsoft.Json.Converters.IXmlNode)
extern "C"  void XmlNodeConverter_DeserializeValue_m1006350864 (XmlNodeConverter_t567010885 * __this, JsonReader_t816925123 * ___reader0, Il2CppObject * ___document1, XmlNamespaceManager_t1467853467 * ___manager2, String_t* ___propertyName3, Il2CppObject * ___currentNode4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::ReadElement(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,Newtonsoft.Json.Converters.IXmlNode,System.String,System.Xml.XmlNamespaceManager)
extern "C"  void XmlNodeConverter_ReadElement_m2786917570 (XmlNodeConverter_t567010885 * __this, JsonReader_t816925123 * ___reader0, Il2CppObject * ___document1, Il2CppObject * ___currentNode2, String_t* ___propertyName3, XmlNamespaceManager_t1467853467 * ___manager4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::ReadArrayElements(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,System.String,Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager)
extern "C"  void XmlNodeConverter_ReadArrayElements_m1210940686 (XmlNodeConverter_t567010885 * __this, JsonReader_t816925123 * ___reader0, Il2CppObject * ___document1, String_t* ___propertyName2, Il2CppObject * ___currentNode3, XmlNamespaceManager_t1467853467 * ___manager4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::AddJsonArrayAttribute(Newtonsoft.Json.Converters.IXmlElement,Newtonsoft.Json.Converters.IXmlDocument)
extern "C"  void XmlNodeConverter_AddJsonArrayAttribute_m923763997 (XmlNodeConverter_t567010885 * __this, Il2CppObject * ___element0, Il2CppObject * ___document1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> Newtonsoft.Json.Converters.XmlNodeConverter::ReadAttributeElements(Newtonsoft.Json.JsonReader,System.Xml.XmlNamespaceManager)
extern "C"  Dictionary_2_t827649927 * XmlNodeConverter_ReadAttributeElements_m1677748620 (XmlNodeConverter_t567010885 * __this, JsonReader_t816925123 * ___reader0, XmlNamespaceManager_t1467853467 * ___manager1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::CreateInstruction(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,Newtonsoft.Json.Converters.IXmlNode,System.String)
extern "C"  void XmlNodeConverter_CreateInstruction_m3462000285 (XmlNodeConverter_t567010885 * __this, JsonReader_t816925123 * ___reader0, Il2CppObject * ___document1, Il2CppObject * ___currentNode2, String_t* ___propertyName3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlElement Newtonsoft.Json.Converters.XmlNodeConverter::CreateElement(System.String,Newtonsoft.Json.Converters.IXmlDocument,System.String,System.Xml.XmlNamespaceManager)
extern "C"  Il2CppObject * XmlNodeConverter_CreateElement_m2527549609 (XmlNodeConverter_t567010885 * __this, String_t* ___elementName0, Il2CppObject * ___document1, String_t* ___elementPrefix2, XmlNamespaceManager_t1467853467 * ___manager3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::DeserializeNode(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,System.Xml.XmlNamespaceManager,Newtonsoft.Json.Converters.IXmlNode)
extern "C"  void XmlNodeConverter_DeserializeNode_m911498479 (XmlNodeConverter_t567010885 * __this, JsonReader_t816925123 * ___reader0, Il2CppObject * ___document1, XmlNamespaceManager_t1467853467 * ___manager2, Il2CppObject * ___currentNode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::IsNamespaceAttribute(System.String,System.String&)
extern "C"  bool XmlNodeConverter_IsNamespaceAttribute_m3991903787 (XmlNodeConverter_t567010885 * __this, String_t* ___attributeName0, String_t** ___prefix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XmlNodeConverter::ValueAttributes(System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Converters.IXmlNode>)
extern "C"  Il2CppObject* XmlNodeConverter_ValueAttributes_m2742225691 (XmlNodeConverter_t567010885 * __this, Il2CppObject* ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::CanConvert(System.Type)
extern "C"  bool XmlNodeConverter_CanConvert_m178606020 (XmlNodeConverter_t567010885 * __this, Type_t * ___valueType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::<IsArray>m__36A(Newtonsoft.Json.Converters.IXmlNode)
extern "C"  bool XmlNodeConverter_U3CIsArrayU3Em__36A_m700297168 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::<ValueAttributes>m__36E(Newtonsoft.Json.Converters.IXmlNode)
extern "C"  bool XmlNodeConverter_U3CValueAttributesU3Em__36E_m2407725605 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeConverter::ilo_WrapXml1(Newtonsoft.Json.Converters.XmlNodeConverter,System.Object)
extern "C"  Il2CppObject * XmlNodeConverter_ilo_WrapXml1_m1281573235 (Il2CppObject * __this /* static, unused */, XmlNodeConverter_t567010885 * ____this0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::ilo_PushParentNamespaces2(Newtonsoft.Json.Converters.XmlNodeConverter,Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager)
extern "C"  void XmlNodeConverter_ilo_PushParentNamespaces2_m3406526135 (Il2CppObject * __this /* static, unused */, XmlNodeConverter_t567010885 * ____this0, Il2CppObject * ___node1, XmlNamespaceManager_t1467853467 * ___manager2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::ilo_SerializeNode3(Newtonsoft.Json.Converters.XmlNodeConverter,Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager,System.Boolean)
extern "C"  void XmlNodeConverter_ilo_SerializeNode3_m821360699 (Il2CppObject * __this /* static, unused */, XmlNodeConverter_t567010885 * ____this0, JsonWriter_t972330355 * ___writer1, Il2CppObject * ___node2, XmlNamespaceManager_t1467853467 * ___manager3, bool ___writePropertyName4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::ilo_get_OmitRootObject4(Newtonsoft.Json.Converters.XmlNodeConverter)
extern "C"  bool XmlNodeConverter_ilo_get_OmitRootObject4_m2182922727 (Il2CppObject * __this /* static, unused */, XmlNodeConverter_t567010885 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeType Newtonsoft.Json.Converters.XmlNodeConverter::ilo_get_NodeType5(Newtonsoft.Json.Converters.IXmlNode)
extern "C"  int32_t XmlNodeConverter_ilo_get_NodeType5_m610918115 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlNodeConverter::ilo_get_LocalName6(Newtonsoft.Json.Converters.IXmlNode)
extern "C"  String_t* XmlNodeConverter_ilo_get_LocalName6_m1985070141 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlNodeConverter::ilo_get_NamespaceURI7(Newtonsoft.Json.Converters.IXmlNode)
extern "C"  String_t* XmlNodeConverter_ilo_get_NamespaceURI7_m1226787155 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlNodeConverter::ilo_ResolveFullName8(Newtonsoft.Json.Converters.XmlNodeConverter,Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager)
extern "C"  String_t* XmlNodeConverter_ilo_ResolveFullName8_m2738127712 (Il2CppObject * __this /* static, unused */, XmlNodeConverter_t567010885 * ____this0, Il2CppObject * ___node1, XmlNamespaceManager_t1467853467 * ___manager2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XmlNodeConverter::ilo_get_Attributes9(Newtonsoft.Json.Converters.IXmlNode)
extern "C"  Il2CppObject* XmlNodeConverter_ilo_get_Attributes9_m3088627586 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::ilo_IsArray10(Newtonsoft.Json.Converters.XmlNodeConverter,Newtonsoft.Json.Converters.IXmlNode)
extern "C"  bool XmlNodeConverter_ilo_IsArray10_m2175533808 (Il2CppObject * __this /* static, unused */, XmlNodeConverter_t567010885 * ____this0, Il2CppObject * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::ilo_WritePropertyName11(Newtonsoft.Json.JsonWriter,System.String)
extern "C"  void XmlNodeConverter_ilo_WritePropertyName11_m1521487572 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::ilo_SerializeGroupedNodes12(Newtonsoft.Json.Converters.XmlNodeConverter,Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager,System.Boolean)
extern "C"  void XmlNodeConverter_ilo_SerializeGroupedNodes12_m1363970158 (Il2CppObject * __this /* static, unused */, XmlNodeConverter_t567010885 * ____this0, JsonWriter_t972330355 * ___writer1, Il2CppObject * ___node2, XmlNamespaceManager_t1467853467 * ___manager3, bool ___writePropertyName4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlNodeConverter::ilo_GetPropertyName13(Newtonsoft.Json.Converters.XmlNodeConverter,Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager)
extern "C"  String_t* XmlNodeConverter_ilo_GetPropertyName13_m4229698940 (Il2CppObject * __this /* static, unused */, XmlNodeConverter_t567010885 * ____this0, Il2CppObject * ___node1, XmlNamespaceManager_t1467853467 * ___manager2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XmlNodeConverter::ilo_get_ChildNodes14(Newtonsoft.Json.Converters.IXmlNode)
extern "C"  Il2CppObject* XmlNodeConverter_ilo_get_ChildNodes14_m871323642 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlNodeConverter::ilo_get_Value15(Newtonsoft.Json.Converters.IXmlNode)
extern "C"  String_t* XmlNodeConverter_ilo_get_Value15_m570605092 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::ilo_WriteComment16(Newtonsoft.Json.JsonWriter,System.String)
extern "C"  void XmlNodeConverter_ilo_WriteComment16_m4170206730 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::ilo_WriteValue17(Newtonsoft.Json.JsonWriter,System.String)
extern "C"  void XmlNodeConverter_ilo_WriteValue17_m1865424669 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::ilo_WriteStartObject18(Newtonsoft.Json.JsonWriter)
extern "C"  void XmlNodeConverter_ilo_WriteStartObject18_m2446481202 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlNodeConverter::ilo_get_Version19(Newtonsoft.Json.Converters.IXmlDeclaration)
extern "C"  String_t* XmlNodeConverter_ilo_get_Version19_m1746051133 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlNodeConverter::ilo_get_Standalone20(Newtonsoft.Json.Converters.IXmlDeclaration)
extern "C"  String_t* XmlNodeConverter_ilo_get_Standalone20_m703042108 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonToken Newtonsoft.Json.Converters.XmlNodeConverter::ilo_get_TokenType21(Newtonsoft.Json.JsonReader)
extern "C"  int32_t XmlNodeConverter_ilo_get_TokenType21_m715170729 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::ilo_Read22(Newtonsoft.Json.JsonReader)
extern "C"  bool XmlNodeConverter_ilo_Read22_m494568443 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::ilo_DeserializeNode23(Newtonsoft.Json.Converters.XmlNodeConverter,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,System.Xml.XmlNamespaceManager,Newtonsoft.Json.Converters.IXmlNode)
extern "C"  void XmlNodeConverter_ilo_DeserializeNode23_m1804534976 (Il2CppObject * __this /* static, unused */, XmlNodeConverter_t567010885 * ____this0, JsonReader_t816925123 * ___reader1, Il2CppObject * ___document2, XmlNamespaceManager_t1467853467 * ___manager3, Il2CppObject * ___currentNode4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Converters.XmlNodeConverter::ilo_get_Value24(Newtonsoft.Json.JsonReader)
extern "C"  Il2CppObject * XmlNodeConverter_ilo_get_Value24_m1002640168 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeConverter::ilo_AppendChild25(Newtonsoft.Json.Converters.IXmlNode,Newtonsoft.Json.Converters.IXmlNode)
extern "C"  Il2CppObject * XmlNodeConverter_ilo_AppendChild25_m4145767303 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Il2CppObject * ___newChild1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::ilo_ReadArrayElements26(Newtonsoft.Json.Converters.XmlNodeConverter,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,System.String,Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager)
extern "C"  void XmlNodeConverter_ilo_ReadArrayElements26_m1292076668 (Il2CppObject * __this /* static, unused */, XmlNodeConverter_t567010885 * ____this0, JsonReader_t816925123 * ___reader1, Il2CppObject * ___document2, String_t* ___propertyName3, Il2CppObject * ___currentNode4, XmlNamespaceManager_t1467853467 * ___manager5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlNodeConverter::ilo_GetPrefix27(System.String)
extern "C"  String_t* XmlNodeConverter_ilo_GetPrefix27_m1232870953 (Il2CppObject * __this /* static, unused */, String_t* ___qualifiedName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlElement Newtonsoft.Json.Converters.XmlNodeConverter::ilo_CreateElement28(Newtonsoft.Json.Converters.XmlNodeConverter,System.String,Newtonsoft.Json.Converters.IXmlDocument,System.String,System.Xml.XmlNamespaceManager)
extern "C"  Il2CppObject * XmlNodeConverter_ilo_CreateElement28_m2086905405 (Il2CppObject * __this /* static, unused */, XmlNodeConverter_t567010885 * ____this0, String_t* ___elementName1, Il2CppObject * ___document2, String_t* ___elementPrefix3, XmlNamespaceManager_t1467853467 * ___manager4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeConverter::ilo_CreateTextNode29(Newtonsoft.Json.Converters.IXmlDocument,System.String)
extern "C"  Il2CppObject * XmlNodeConverter_ilo_CreateTextNode29_m2741106961 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::ilo_get_WriteArrayAttribute30(Newtonsoft.Json.Converters.XmlNodeConverter)
extern "C"  bool XmlNodeConverter_ilo_get_WriteArrayAttribute30_m2950696114 (Il2CppObject * __this /* static, unused */, XmlNodeConverter_t567010885 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::ilo_SetAttributeNode31(Newtonsoft.Json.Converters.IXmlElement,Newtonsoft.Json.Converters.IXmlNode)
extern "C"  void XmlNodeConverter_ilo_SetAttributeNode31_m2909485121 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Il2CppObject * ___attribute1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeConverter::ilo_CreateXmlDeclaration32(Newtonsoft.Json.Converters.IXmlDocument,System.String,System.String,System.String)
extern "C"  Il2CppObject * XmlNodeConverter_ilo_CreateXmlDeclaration32_m4152663965 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, String_t* ___version1, String_t* ___encoding2, String_t* ___standalone3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::ilo_DeserializeValue33(Newtonsoft.Json.Converters.XmlNodeConverter,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,System.Xml.XmlNamespaceManager,System.String,Newtonsoft.Json.Converters.IXmlNode)
extern "C"  void XmlNodeConverter_ilo_DeserializeValue33_m4128759708 (Il2CppObject * __this /* static, unused */, XmlNodeConverter_t567010885 * ____this0, JsonReader_t816925123 * ___reader1, Il2CppObject * ___document2, XmlNamespaceManager_t1467853467 * ___manager3, String_t* ___propertyName4, Il2CppObject * ___currentNode5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
