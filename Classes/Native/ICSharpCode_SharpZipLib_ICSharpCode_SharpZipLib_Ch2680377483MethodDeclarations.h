﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Checksums.Adler32
struct Adler32_t2680377483;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Int64 ICSharpCode.SharpZipLib.Checksums.Adler32::get_Value()
extern "C"  int64_t Adler32_get_Value_m2923380521 (Adler32_t2680377483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Checksums.Adler32::.ctor()
extern "C"  void Adler32__ctor_m2455030518 (Adler32_t2680377483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Checksums.Adler32::Reset()
extern "C"  void Adler32_Reset_m101463459 (Adler32_t2680377483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Checksums.Adler32::Update(System.Byte[],System.Int32,System.Int32)
extern "C"  void Adler32_Update_m2142069042 (Adler32_t2680377483 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
