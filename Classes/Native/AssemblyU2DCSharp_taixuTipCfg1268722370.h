﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// taixuTipCfg
struct  taixuTipCfg_t1268722370  : public CsCfgBase_t69924517
{
public:
	// System.Int32 taixuTipCfg::id
	int32_t ___id_0;
	// System.String taixuTipCfg::order
	String_t* ___order_1;
	// System.String taixuTipCfg::des
	String_t* ___des_2;
	// System.String taixuTipCfg::image
	String_t* ___image_3;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(taixuTipCfg_t1268722370, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_order_1() { return static_cast<int32_t>(offsetof(taixuTipCfg_t1268722370, ___order_1)); }
	inline String_t* get_order_1() const { return ___order_1; }
	inline String_t** get_address_of_order_1() { return &___order_1; }
	inline void set_order_1(String_t* value)
	{
		___order_1 = value;
		Il2CppCodeGenWriteBarrier(&___order_1, value);
	}

	inline static int32_t get_offset_of_des_2() { return static_cast<int32_t>(offsetof(taixuTipCfg_t1268722370, ___des_2)); }
	inline String_t* get_des_2() const { return ___des_2; }
	inline String_t** get_address_of_des_2() { return &___des_2; }
	inline void set_des_2(String_t* value)
	{
		___des_2 = value;
		Il2CppCodeGenWriteBarrier(&___des_2, value);
	}

	inline static int32_t get_offset_of_image_3() { return static_cast<int32_t>(offsetof(taixuTipCfg_t1268722370, ___image_3)); }
	inline String_t* get_image_3() const { return ___image_3; }
	inline String_t** get_address_of_image_3() { return &___image_3; }
	inline void set_image_3(String_t* value)
	{
		___image_3 = value;
		Il2CppCodeGenWriteBarrier(&___image_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
