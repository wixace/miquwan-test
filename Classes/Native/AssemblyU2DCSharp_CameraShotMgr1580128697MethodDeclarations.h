﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraShotMgr
struct CameraShotMgr_t1580128697;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// cameraShotCfg[]
struct cameraShotCfgU5BU5D_t3196548520;
// CameraAniMap
struct CameraAniMap_t3004656005;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// CameraAniCfgMgr
struct CameraAniCfgMgr_t11764299;
// System.Collections.Generic.List`1<CameraAniGroup>
struct List_1_t2619551544;
// CameraAniGroup
struct CameraAniGroup_t1251365992;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// NpcMgr
struct NpcMgr_t2339534743;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// GameMgr
struct GameMgr_t1469029542;
// EffectMgr
struct EffectMgr_t535289511;
// CameraSmoothFollow
struct CameraSmoothFollow_t2624612068;
// HeroMgr
struct HeroMgr_t2475965342;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// CameraShotHelper
struct CameraShotHelper_t627387501;
// AnimationRunner
struct AnimationRunner_t1015409588;
// FightCtrl
struct FightCtrl_t648967803;
// CombatEntity
struct CombatEntity_t684137495;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// CSCheckPointUnit
struct CSCheckPointUnit_t3129216924;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_triggerType3614031122.h"
#include "AssemblyU2DCSharp_CameraAniMap3004656005.h"
#include "AssemblyU2DCSharp_FunType1154568505.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CameraShotMgr1580128697.h"
#include "AssemblyU2DCSharp_CameraAniGroup1251365992.h"
#include "AssemblyU2DCSharp_NpcMgr2339534743.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"
#include "AssemblyU2DCSharp_EffectMgr535289511.h"
#include "AssemblyU2DCSharp_HeroMgr2475965342.h"
#include "AssemblyU2DCSharp_CameraShotHelper627387501.h"
#include "AssemblyU2DCSharp_AnimationRunner1015409588.h"
#include "AssemblyU2DCSharp_FightCtrl648967803.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitTag299076696.h"

// System.Void CameraShotMgr::.ctor()
extern "C"  void CameraShotMgr__ctor_m1715627986 (CameraShotMgr_t1580128697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::InitTrigger()
extern "C"  void CameraShotMgr_InitTrigger_m2781090360 (CameraShotMgr_t1580128697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::OnQuitGame(CEvent.ZEvent)
extern "C"  void CameraShotMgr_OnQuitGame_m493879657 (CameraShotMgr_t1580128697 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::OnOtherDead(CEvent.ZEvent)
extern "C"  void CameraShotMgr_OnOtherDead_m2047031222 (CameraShotMgr_t1580128697 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::OnBattleEnded(CEvent.ZEvent)
extern "C"  void CameraShotMgr_OnBattleEnded_m3710546088 (CameraShotMgr_t1580128697 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::OnCombatWin(CEvent.ZEvent)
extern "C"  void CameraShotMgr_OnCombatWin_m1963237506 (CameraShotMgr_t1580128697 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::OnEnterFuben(CEvent.ZEvent)
extern "C"  void CameraShotMgr_OnEnterFuben_m4162535558 (CameraShotMgr_t1580128697 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::Trigger(triggerType,System.Int32)
extern "C"  void CameraShotMgr_Trigger_m1796646047 (CameraShotMgr_t1580128697 * __this, int32_t ____eventType0, int32_t ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotMgr::GetCameraShotId(System.Int32,System.Int32)
extern "C"  int32_t CameraShotMgr_GetCameraShotId_m462892792 (CameraShotMgr_t1580128697 * __this, int32_t ___mapId0, int32_t ___cameraShotId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::PreviewCameraShot(System.Int32,System.Int32)
extern "C"  void CameraShotMgr_PreviewCameraShot_m96347311 (CameraShotMgr_t1580128697 * __this, int32_t ___mapId0, int32_t ___cameraShotId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraShotMgr::isCameraShotSkillFit(System.Int32,System.Int32)
extern "C"  bool CameraShotMgr_isCameraShotSkillFit_m2634532031 (CameraShotMgr_t1580128697 * __this, int32_t ___effid0, int32_t ___scrid1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::PreviewCameraShot(System.Int32)
extern "C"  void CameraShotMgr_PreviewCameraShot_m3420264744 (CameraShotMgr_t1580128697 * __this, int32_t ___cameraShotId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::PreviewCameraShot()
extern "C"  void CameraShotMgr_PreviewCameraShot_m3743231575 (CameraShotMgr_t1580128697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::previewInEditor(cameraShotCfg[],CameraAniMap)
extern "C"  void CameraShotMgr_previewInEditor_m283176864 (CameraShotMgr_t1580128697 * __this, cameraShotCfgU5BU5D_t3196548520* ___cameraShotCfgList0, CameraAniMap_t3004656005 * ___curAniMap1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotMgr::returnCameraShotId(FunType,System.Object)
extern "C"  int32_t CameraShotMgr_returnCameraShotId_m2536911257 (CameraShotMgr_t1580128697 * __this, int32_t ___funtype0, Il2CppObject * ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::OnStartAniComplete(CEvent.ZEvent)
extern "C"  void CameraShotMgr_OnStartAniComplete_m2041575575 (CameraShotMgr_t1580128697 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::OnLineComplete(CEvent.ZEvent)
extern "C"  void CameraShotMgr_OnLineComplete_m4048340349 (CameraShotMgr_t1580128697 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::OnShootEffectComplete(CEvent.ZEvent)
extern "C"  void CameraShotMgr_OnShootEffectComplete_m50057921 (CameraShotMgr_t1580128697 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::OnEffectDone(CEvent.ZEvent)
extern "C"  void CameraShotMgr_OnEffectDone_m1897550167 (CameraShotMgr_t1580128697 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::OnGameSpeedComplete(CEvent.ZEvent)
extern "C"  void CameraShotMgr_OnGameSpeedComplete_m1206483004 (CameraShotMgr_t1580128697 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::OnActionComplete(CEvent.ZEvent)
extern "C"  void CameraShotMgr_OnActionComplete_m2638477563 (CameraShotMgr_t1580128697 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::OnSkillComplete(CEvent.ZEvent)
extern "C"  void CameraShotMgr_OnSkillComplete_m3881105888 (CameraShotMgr_t1580128697 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::OnBrightComplete(CEvent.ZEvent)
extern "C"  void CameraShotMgr_OnBrightComplete_m3009162167 (CameraShotMgr_t1580128697 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::OnWaitComplete(CEvent.ZEvent)
extern "C"  void CameraShotMgr_OnWaitComplete_m2284045020 (CameraShotMgr_t1580128697 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::OnPlotComplete(CEvent.ZEvent)
extern "C"  void CameraShotMgr_OnPlotComplete_m610577904 (CameraShotMgr_t1580128697 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::OnMoveCameraComplete(CEvent.ZEvent)
extern "C"  void CameraShotMgr_OnMoveCameraComplete_m1772134619 (CameraShotMgr_t1580128697 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::OnMoveComplete(CEvent.ZEvent)
extern "C"  void CameraShotMgr_OnMoveComplete_m1463300352 (CameraShotMgr_t1580128697 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::Open()
extern "C"  void CameraShotMgr_Open_m3228417788 (CameraShotMgr_t1580128697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::JumpAllStory(CEvent.ZEvent)
extern "C"  void CameraShotMgr_JumpAllStory_m3675387175 (CameraShotMgr_t1580128697 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::Close()
extern "C"  void CameraShotMgr_Close_m3426487528 (CameraShotMgr_t1580128697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::ClosePeak()
extern "C"  void CameraShotMgr_ClosePeak_m1272472231 (CameraShotMgr_t1580128697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::OnComplete(System.Int32)
extern "C"  void CameraShotMgr_OnComplete_m1496319739 (CameraShotMgr_t1580128697 * __this, int32_t ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::Execute()
extern "C"  void CameraShotMgr_Execute_m2302442341 (CameraShotMgr_t1580128697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::OnStart()
extern "C"  void CameraShotMgr_OnStart_m2832515635 (CameraShotMgr_t1580128697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::OnEnd()
extern "C"  void CameraShotMgr_OnEnd_m1209950316 (CameraShotMgr_t1580128697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::<Execute>m__3E5()
extern "C"  void CameraShotMgr_U3CExecuteU3Em__3E5_m1927792491 (CameraShotMgr_t1580128697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void CameraShotMgr_ilo_AddEventListener1_m1224070882 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::ilo_Trigger2(CameraShotMgr,triggerType,System.Int32)
extern "C"  void CameraShotMgr_ilo_Trigger2_m1225476515 (Il2CppObject * __this /* static, unused */, CameraShotMgr_t1580128697 * ____this0, int32_t ____eventType1, int32_t ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CameraAniCfgMgr CameraShotMgr::ilo_get_instance3()
extern "C"  CameraAniCfgMgr_t11764299 * CameraShotMgr_ilo_get_instance3_m101745274 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CameraAniGroup> CameraShotMgr::ilo_get_cameraAniGroups4(CameraAniMap)
extern "C"  List_1_t2619551544 * CameraShotMgr_ilo_get_cameraAniGroups4_m754458039 (Il2CppObject * __this /* static, unused */, CameraAniMap_t3004656005 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotMgr::ilo_get_id5(CameraAniGroup)
extern "C"  int32_t CameraShotMgr_ilo_get_id5_m184153908 (Il2CppObject * __this /* static, unused */, CameraAniGroup_t1251365992 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotMgr::ilo_get_round6(CameraAniGroup)
extern "C"  int32_t CameraShotMgr_ilo_get_round6_m533742400 (Il2CppObject * __this /* static, unused */, CameraAniGroup_t1251365992 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSDatacfgManager CameraShotMgr::ilo_get_CfgDataMgr7()
extern "C"  CSDatacfgManager_t1565254243 * CameraShotMgr_ilo_get_CfgDataMgr7_m1507410331 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::ilo_Execute8(CameraShotMgr)
extern "C"  void CameraShotMgr_ilo_Execute8_m3072831697 (Il2CppObject * __this /* static, unused */, CameraShotMgr_t1580128697 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CameraShotMgr CameraShotMgr::ilo_get_CameraShotMgr9()
extern "C"  CameraShotMgr_t1580128697 * CameraShotMgr_ilo_get_CameraShotMgr9_m1042227060 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::ilo_OnEnd10(CameraShotMgr)
extern "C"  void CameraShotMgr_ilo_OnEnd10_m2070546017 (Il2CppObject * __this /* static, unused */, CameraShotMgr_t1580128697 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::ilo_Log11(System.Object,System.Boolean)
extern "C"  void CameraShotMgr_ilo_Log11_m196320042 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotMgr::ilo_returnCameraShotId12(CameraShotMgr,FunType,System.Object)
extern "C"  int32_t CameraShotMgr_ilo_returnCameraShotId12_m868799512 (Il2CppObject * __this /* static, unused */, CameraShotMgr_t1580128697 * ____this0, int32_t ___funtype1, Il2CppObject * ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::ilo_OnComplete13(CameraShotMgr,System.Int32)
extern "C"  void CameraShotMgr_ilo_OnComplete13_m762692125 (Il2CppObject * __this /* static, unused */, CameraShotMgr_t1580128697 * ____this0, int32_t ___a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::ilo_ClearAniShowList14(NpcMgr,System.Int32[])
extern "C"  void CameraShotMgr_ilo_ClearAniShowList14_m1245314458 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, Int32U5BU5D_t3230847821* ___special1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::ilo_UnLockNpcBehaviour15(GameMgr)
extern "C"  void CameraShotMgr_ilo_UnLockNpcBehaviour15_m429799491 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EffectMgr CameraShotMgr::ilo_get_EffectMgr16()
extern "C"  EffectMgr_t535289511 * CameraShotMgr_ilo_get_EffectMgr16_m652307052 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::ilo_Clear17(EffectMgr)
extern "C"  void CameraShotMgr_ilo_Clear17_m491404571 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::ilo_ClearSceneEffect18(EffectMgr)
extern "C"  void CameraShotMgr_ilo_ClearSceneEffect18_m1244628783 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NpcMgr CameraShotMgr::ilo_get_instance19()
extern "C"  NpcMgr_t2339534743 * CameraShotMgr_ilo_get_instance19_m1377081093 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CameraSmoothFollow CameraShotMgr::ilo_get_CameraSmoothFollow20(HeroMgr)
extern "C"  CameraSmoothFollow_t2624612068 * CameraShotMgr_ilo_get_CameraSmoothFollow20_m513871101 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroMgr CameraShotMgr::ilo_get_instance21()
extern "C"  HeroMgr_t2475965342 * CameraShotMgr_ilo_get_instance21_m2479971085 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::ilo_CallJsFun22(System.String,System.Object[])
extern "C"  void CameraShotMgr_ilo_CallJsFun22_m1082884281 (Il2CppObject * __this /* static, unused */, String_t* ___funName0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::ilo_gameSpeedCtrl23(CameraShotHelper,System.Single,System.Single,System.Single,System.Single,System.Int32)
extern "C"  void CameraShotMgr_ilo_gameSpeedCtrl23_m1105784626 (Il2CppObject * __this /* static, unused */, CameraShotHelper_t627387501 * ____this0, float ___speed1, float ___from2, float ___wait3, float ___to4, int32_t ___id5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::ilo_Play24(AnimationRunner,System.Boolean,System.String,System.Single)
extern "C"  void CameraShotMgr_ilo_Play24_m2571825147 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, bool ___isLoop1, String_t* ___aniName2, float ___playTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::ilo_ForceUsingSkill25(FightCtrl,System.Int32,System.Int32,System.Int32)
extern "C"  void CameraShotMgr_ilo_ForceUsingSkill25_m812254552 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, int32_t ___skillId1, int32_t ___hostId2, int32_t ___effectId3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity CameraShotMgr::ilo_GetEntityByID26(GameMgr,System.Boolean,System.Int32)
extern "C"  CombatEntity_t684137495 * CameraShotMgr_ilo_GetEntityByID26_m3964536052 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, bool ___isHero1, int32_t ___id2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::ilo_PlaySceneEffect27(EffectMgr,UnityEngine.GameObject,UnityEngine.Vector3)
extern "C"  void CameraShotMgr_ilo_PlaySceneEffect27_m1303580945 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, GameObject_t3674682005 * ___obj1, Vector3_t4282066566  ___pos2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::ilo_brightChange28(CameraShotHelper,System.Int32,System.Single,System.Int32)
extern "C"  void CameraShotMgr_ilo_brightChange28_m1272480931 (Il2CppObject * __this /* static, unused */, CameraShotHelper_t627387501 * ____this0, int32_t ___brightId1, float ___time2, int32_t ___curCameraShotId3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::ilo_PlayCameraShotShoot29(EffectMgr,System.Int32,UnityEngine.GameObject,CombatEntity,CombatEntity,System.Single,System.Boolean,UnityEngine.Vector3,EntityEnum.UnitTag,System.Int32)
extern "C"  void CameraShotMgr_ilo_PlayCameraShotShoot29_m4044187078 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, int32_t ___id1, GameObject_t3674682005 * ___obj2, CombatEntity_t684137495 * ___srcEntity3, CombatEntity_t684137495 * ___targetTf4, float ___speed5, bool ___isatk6, Vector3_t4282066566  ___targetPosition7, int32_t ___tag8, int32_t ___skillID9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSCheckPointUnit CameraShotMgr::ilo_get_checkPoint30(GameMgr)
extern "C"  CSCheckPointUnit_t3129216924 * CameraShotMgr_ilo_get_checkPoint30_m783090234 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::ilo_Close31(CameraShotMgr)
extern "C"  void CameraShotMgr_ilo_Close31_m1615588422 (Il2CppObject * __this /* static, unused */, CameraShotMgr_t1580128697 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgr::ilo_RemoveEventListener32(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void CameraShotMgr_ilo_RemoveEventListener32_m567889885 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
