﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.NavGraph/<RelocateNodes>c__AnonStorey114
struct  U3CRelocateNodesU3Ec__AnonStorey114_t2346854625  : public Il2CppObject
{
public:
	// UnityEngine.Matrix4x4 Pathfinding.NavGraph/<RelocateNodes>c__AnonStorey114::m
	Matrix4x4_t1651859333  ___m_0;

public:
	inline static int32_t get_offset_of_m_0() { return static_cast<int32_t>(offsetof(U3CRelocateNodesU3Ec__AnonStorey114_t2346854625, ___m_0)); }
	inline Matrix4x4_t1651859333  get_m_0() const { return ___m_0; }
	inline Matrix4x4_t1651859333 * get_address_of_m_0() { return &___m_0; }
	inline void set_m_0(Matrix4x4_t1651859333  value)
	{
		___m_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
