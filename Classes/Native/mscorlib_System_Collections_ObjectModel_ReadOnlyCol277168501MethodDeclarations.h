﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>
struct ReadOnlyCollection_1_t277168501;
// System.Collections.Generic.IList`1<Pathfinding.IntRect>
struct IList_1_t1414738168;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Pathfinding.IntRect[]
struct IntRectU5BU5D_t3425567672;
// System.Collections.Generic.IEnumerator`1<Pathfinding.IntRect>
struct IEnumerator_1_t631956014;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_IntRect3015058261.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m1037328199_gshared (ReadOnlyCollection_1_t277168501 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1037328199(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t277168501 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1037328199_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1750701489_gshared (ReadOnlyCollection_1_t277168501 * __this, IntRect_t3015058261  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1750701489(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t277168501 *, IntRect_t3015058261 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1750701489_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m443862329_gshared (ReadOnlyCollection_1_t277168501 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m443862329(__this, method) ((  void (*) (ReadOnlyCollection_1_t277168501 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m443862329_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1019178840_gshared (ReadOnlyCollection_1_t277168501 * __this, int32_t ___index0, IntRect_t3015058261  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1019178840(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t277168501 *, int32_t, IntRect_t3015058261 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1019178840_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m4066274338_gshared (ReadOnlyCollection_1_t277168501 * __this, IntRect_t3015058261  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m4066274338(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t277168501 *, IntRect_t3015058261 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m4066274338_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3187999006_gshared (ReadOnlyCollection_1_t277168501 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3187999006(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t277168501 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3187999006_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  IntRect_t3015058261  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m245467682_gshared (ReadOnlyCollection_1_t277168501 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m245467682(__this, ___index0, method) ((  IntRect_t3015058261  (*) (ReadOnlyCollection_1_t277168501 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m245467682_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1987918383_gshared (ReadOnlyCollection_1_t277168501 * __this, int32_t ___index0, IntRect_t3015058261  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1987918383(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t277168501 *, int32_t, IntRect_t3015058261 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1987918383_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2142226221_gshared (ReadOnlyCollection_1_t277168501 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2142226221(__this, method) ((  bool (*) (ReadOnlyCollection_1_t277168501 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2142226221_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1771882614_gshared (ReadOnlyCollection_1_t277168501 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1771882614(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t277168501 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1771882614_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3884748017_gshared (ReadOnlyCollection_1_t277168501 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3884748017(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t277168501 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3884748017_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m2955074464_gshared (ReadOnlyCollection_1_t277168501 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m2955074464(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t277168501 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m2955074464_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m1494822788_gshared (ReadOnlyCollection_1_t277168501 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1494822788(__this, method) ((  void (*) (ReadOnlyCollection_1_t277168501 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m1494822788_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1424137772_gshared (ReadOnlyCollection_1_t277168501 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1424137772(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t277168501 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1424137772_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2504802808_gshared (ReadOnlyCollection_1_t277168501 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2504802808(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t277168501 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2504802808_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3339733539_gshared (ReadOnlyCollection_1_t277168501 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3339733539(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t277168501 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m3339733539_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m3834028581_gshared (ReadOnlyCollection_1_t277168501 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m3834028581(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t277168501 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m3834028581_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m181157427_gshared (ReadOnlyCollection_1_t277168501 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m181157427(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t277168501 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m181157427_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m796391856_gshared (ReadOnlyCollection_1_t277168501 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m796391856(__this, method) ((  bool (*) (ReadOnlyCollection_1_t277168501 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m796391856_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m29818012_gshared (ReadOnlyCollection_1_t277168501 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m29818012(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t277168501 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m29818012_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1612144283_gshared (ReadOnlyCollection_1_t277168501 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1612144283(__this, method) ((  bool (*) (ReadOnlyCollection_1_t277168501 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1612144283_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3138412798_gshared (ReadOnlyCollection_1_t277168501 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3138412798(__this, method) ((  bool (*) (ReadOnlyCollection_1_t277168501 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3138412798_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m3950917411_gshared (ReadOnlyCollection_1_t277168501 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m3950917411(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t277168501 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3950917411_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1757313338_gshared (ReadOnlyCollection_1_t277168501 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1757313338(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t277168501 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1757313338_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m2772845415_gshared (ReadOnlyCollection_1_t277168501 * __this, IntRect_t3015058261  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m2772845415(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t277168501 *, IntRect_t3015058261 , const MethodInfo*))ReadOnlyCollection_1_Contains_m2772845415_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m1030990817_gshared (ReadOnlyCollection_1_t277168501 * __this, IntRectU5BU5D_t3425567672* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1030990817(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t277168501 *, IntRectU5BU5D_t3425567672*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1030990817_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m3667100746_gshared (ReadOnlyCollection_1_t277168501 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m3667100746(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t277168501 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m3667100746_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m3624899365_gshared (ReadOnlyCollection_1_t277168501 * __this, IntRect_t3015058261  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m3624899365(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t277168501 *, IntRect_t3015058261 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m3624899365_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m520038134_gshared (ReadOnlyCollection_1_t277168501 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m520038134(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t277168501 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m520038134_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>::get_Item(System.Int32)
extern "C"  IntRect_t3015058261  ReadOnlyCollection_1_get_Item_m1551865058_gshared (ReadOnlyCollection_1_t277168501 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1551865058(__this, ___index0, method) ((  IntRect_t3015058261  (*) (ReadOnlyCollection_1_t277168501 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1551865058_gshared)(__this, ___index0, method)
