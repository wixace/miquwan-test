﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,GenericTypeCache/TypeMembers>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2810563355(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2373599864 *, Type_t *, TypeMembers_t2369396673 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,GenericTypeCache/TypeMembers>::get_Key()
#define KeyValuePair_2_get_Key_m1703634541(__this, method) ((  Type_t * (*) (KeyValuePair_2_t2373599864 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,GenericTypeCache/TypeMembers>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3700935214(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2373599864 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,GenericTypeCache/TypeMembers>::get_Value()
#define KeyValuePair_2_get_Value_m482549997(__this, method) ((  TypeMembers_t2369396673 * (*) (KeyValuePair_2_t2373599864 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,GenericTypeCache/TypeMembers>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3464910638(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2373599864 *, TypeMembers_t2369396673 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,GenericTypeCache/TypeMembers>::ToString()
#define KeyValuePair_2_ToString_m409742900(__this, method) ((  String_t* (*) (KeyValuePair_2_t2373599864 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
