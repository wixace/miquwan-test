﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua.Asset.ABLoadOperation.AssetOperationPreload1
struct AssetOperationPreload1_t2389844357;
// System.String
struct String_t;
// UnityEngine.Object
struct Object_t3071478659;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;
// ConfigAssetMgr
struct ConfigAssetMgr_t4036193930;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset778728221.h"
#include "AssemblyU2DCSharp_ConfigAssetMgr4036193930.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"

// System.Void Mihua.Asset.ABLoadOperation.AssetOperationPreload1::.ctor(System.String)
extern "C"  void AssetOperationPreload1__ctor_m2889662184 (AssetOperationPreload1_t2389844357 * __this, String_t* ____assetName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.ABLoadOperation.AssetOperationPreload1::Init()
extern "C"  void AssetOperationPreload1_Init_m3238418618 (AssetOperationPreload1_t2389844357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object Mihua.Asset.ABLoadOperation.AssetOperationPreload1::GetAsset()
extern "C"  Object_t3071478659 * AssetOperationPreload1_GetAsset_m3889810859 (AssetOperationPreload1_t2389844357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Asset.ABLoadOperation.AssetOperationPreload1::IsDone()
extern "C"  bool AssetOperationPreload1_IsDone_m3693541098 (AssetOperationPreload1_t2389844357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mihua.Asset.ABLoadOperation.AssetOperationPreload1::get_progress()
extern "C"  float AssetOperationPreload1_get_progress_m3132427212 (AssetOperationPreload1_t2389844357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.ABLoadOperation.AssetOperationPreload1::CacheAsset()
extern "C"  void AssetOperationPreload1_CacheAsset_m242570744 (AssetOperationPreload1_t2389844357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.ABLoadOperation.AssetOperationPreload1::Clear()
extern "C"  void AssetOperationPreload1_Clear_m515227269 (AssetOperationPreload1_t2389844357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.ABLoadOperation.AssetOperationPreload1::ilo_Init1(Mihua.Asset.ABLoadOperation.AssetOperation)
extern "C"  void AssetOperationPreload1_ilo_Init1_m3167735085 (Il2CppObject * __this /* static, unused */, AssetOperation_t778728221 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mihua.Asset.ABLoadOperation.AssetOperationPreload1::ilo_LoadPathToAssetName2(ConfigAssetMgr,System.String)
extern "C"  String_t* AssetOperationPreload1_ilo_LoadPathToAssetName2_m3042524175 (Il2CppObject * __this /* static, unused */, ConfigAssetMgr_t4036193930 * ____this0, String_t* ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.ABLoadOperation.AssetOperationPreload1::ilo_AddCache3(System.String,UnityEngine.Object)
extern "C"  void AssetOperationPreload1_ilo_AddCache3_m741248357 (Il2CppObject * __this /* static, unused */, String_t* ____assetName0, Object_t3071478659 * ___asset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
