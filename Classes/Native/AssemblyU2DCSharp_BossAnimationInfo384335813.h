﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<AnimationPointJSC>
struct List_1_t1855780574;
// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BossAnimationInfo
struct  BossAnimationInfo_t384335813  : public Il2CppObject
{
public:
	// System.Int32 BossAnimationInfo::_id
	int32_t ____id_0;
	// System.Single BossAnimationInfo::_trigerPos_x
	float ____trigerPos_x_1;
	// System.Single BossAnimationInfo::_trigerPos_y
	float ____trigerPos_y_2;
	// System.Single BossAnimationInfo::_trigerPos_z
	float ____trigerPos_z_3;
	// System.Int32 BossAnimationInfo::_isBoss
	int32_t ____isBoss_4;
	// System.Single BossAnimationInfo::_bossPos_x
	float ____bossPos_x_5;
	// System.Single BossAnimationInfo::_bossPos_y
	float ____bossPos_y_6;
	// System.Single BossAnimationInfo::_bossPos_z
	float ____bossPos_z_7;
	// System.Single BossAnimationInfo::_bossRot_y
	float ____bossRot_y_8;
	// System.Collections.Generic.List`1<AnimationPointJSC> BossAnimationInfo::_camera
	List_1_t1855780574 * ____camera_9;
	// System.Collections.Generic.List`1<AnimationPointJSC> BossAnimationInfo::_boss
	List_1_t1855780574 * ____boss_10;
	// ProtoBuf.IExtension BossAnimationInfo::extensionObject
	Il2CppObject * ___extensionObject_11;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(BossAnimationInfo_t384335813, ____id_0)); }
	inline int32_t get__id_0() const { return ____id_0; }
	inline int32_t* get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(int32_t value)
	{
		____id_0 = value;
	}

	inline static int32_t get_offset_of__trigerPos_x_1() { return static_cast<int32_t>(offsetof(BossAnimationInfo_t384335813, ____trigerPos_x_1)); }
	inline float get__trigerPos_x_1() const { return ____trigerPos_x_1; }
	inline float* get_address_of__trigerPos_x_1() { return &____trigerPos_x_1; }
	inline void set__trigerPos_x_1(float value)
	{
		____trigerPos_x_1 = value;
	}

	inline static int32_t get_offset_of__trigerPos_y_2() { return static_cast<int32_t>(offsetof(BossAnimationInfo_t384335813, ____trigerPos_y_2)); }
	inline float get__trigerPos_y_2() const { return ____trigerPos_y_2; }
	inline float* get_address_of__trigerPos_y_2() { return &____trigerPos_y_2; }
	inline void set__trigerPos_y_2(float value)
	{
		____trigerPos_y_2 = value;
	}

	inline static int32_t get_offset_of__trigerPos_z_3() { return static_cast<int32_t>(offsetof(BossAnimationInfo_t384335813, ____trigerPos_z_3)); }
	inline float get__trigerPos_z_3() const { return ____trigerPos_z_3; }
	inline float* get_address_of__trigerPos_z_3() { return &____trigerPos_z_3; }
	inline void set__trigerPos_z_3(float value)
	{
		____trigerPos_z_3 = value;
	}

	inline static int32_t get_offset_of__isBoss_4() { return static_cast<int32_t>(offsetof(BossAnimationInfo_t384335813, ____isBoss_4)); }
	inline int32_t get__isBoss_4() const { return ____isBoss_4; }
	inline int32_t* get_address_of__isBoss_4() { return &____isBoss_4; }
	inline void set__isBoss_4(int32_t value)
	{
		____isBoss_4 = value;
	}

	inline static int32_t get_offset_of__bossPos_x_5() { return static_cast<int32_t>(offsetof(BossAnimationInfo_t384335813, ____bossPos_x_5)); }
	inline float get__bossPos_x_5() const { return ____bossPos_x_5; }
	inline float* get_address_of__bossPos_x_5() { return &____bossPos_x_5; }
	inline void set__bossPos_x_5(float value)
	{
		____bossPos_x_5 = value;
	}

	inline static int32_t get_offset_of__bossPos_y_6() { return static_cast<int32_t>(offsetof(BossAnimationInfo_t384335813, ____bossPos_y_6)); }
	inline float get__bossPos_y_6() const { return ____bossPos_y_6; }
	inline float* get_address_of__bossPos_y_6() { return &____bossPos_y_6; }
	inline void set__bossPos_y_6(float value)
	{
		____bossPos_y_6 = value;
	}

	inline static int32_t get_offset_of__bossPos_z_7() { return static_cast<int32_t>(offsetof(BossAnimationInfo_t384335813, ____bossPos_z_7)); }
	inline float get__bossPos_z_7() const { return ____bossPos_z_7; }
	inline float* get_address_of__bossPos_z_7() { return &____bossPos_z_7; }
	inline void set__bossPos_z_7(float value)
	{
		____bossPos_z_7 = value;
	}

	inline static int32_t get_offset_of__bossRot_y_8() { return static_cast<int32_t>(offsetof(BossAnimationInfo_t384335813, ____bossRot_y_8)); }
	inline float get__bossRot_y_8() const { return ____bossRot_y_8; }
	inline float* get_address_of__bossRot_y_8() { return &____bossRot_y_8; }
	inline void set__bossRot_y_8(float value)
	{
		____bossRot_y_8 = value;
	}

	inline static int32_t get_offset_of__camera_9() { return static_cast<int32_t>(offsetof(BossAnimationInfo_t384335813, ____camera_9)); }
	inline List_1_t1855780574 * get__camera_9() const { return ____camera_9; }
	inline List_1_t1855780574 ** get_address_of__camera_9() { return &____camera_9; }
	inline void set__camera_9(List_1_t1855780574 * value)
	{
		____camera_9 = value;
		Il2CppCodeGenWriteBarrier(&____camera_9, value);
	}

	inline static int32_t get_offset_of__boss_10() { return static_cast<int32_t>(offsetof(BossAnimationInfo_t384335813, ____boss_10)); }
	inline List_1_t1855780574 * get__boss_10() const { return ____boss_10; }
	inline List_1_t1855780574 ** get_address_of__boss_10() { return &____boss_10; }
	inline void set__boss_10(List_1_t1855780574 * value)
	{
		____boss_10 = value;
		Il2CppCodeGenWriteBarrier(&____boss_10, value);
	}

	inline static int32_t get_offset_of_extensionObject_11() { return static_cast<int32_t>(offsetof(BossAnimationInfo_t384335813, ___extensionObject_11)); }
	inline Il2CppObject * get_extensionObject_11() const { return ___extensionObject_11; }
	inline Il2CppObject ** get_address_of_extensionObject_11() { return &___extensionObject_11; }
	inline void set_extensionObject_11(Il2CppObject * value)
	{
		___extensionObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___extensionObject_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
