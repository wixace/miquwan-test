﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_terejaiCairlelair194
struct  M_terejaiCairlelair194_t4147113968  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_terejaiCairlelair194::_dasnai
	uint32_t ____dasnai_0;
	// System.String GarbageiOS.M_terejaiCairlelair194::_sejaKemdesir
	String_t* ____sejaKemdesir_1;
	// System.UInt32 GarbageiOS.M_terejaiCairlelair194::_fawrougur
	uint32_t ____fawrougur_2;
	// System.Int32 GarbageiOS.M_terejaiCairlelair194::_tedrejor
	int32_t ____tedrejor_3;
	// System.Int32 GarbageiOS.M_terejaiCairlelair194::_diteseeBoule
	int32_t ____diteseeBoule_4;
	// System.Int32 GarbageiOS.M_terejaiCairlelair194::_herecayQeaka
	int32_t ____herecayQeaka_5;
	// System.String GarbageiOS.M_terejaiCairlelair194::_nellobee
	String_t* ____nellobee_6;
	// System.Int32 GarbageiOS.M_terejaiCairlelair194::_werou
	int32_t ____werou_7;
	// System.UInt32 GarbageiOS.M_terejaiCairlelair194::_teduxowLerfowje
	uint32_t ____teduxowLerfowje_8;
	// System.String GarbageiOS.M_terejaiCairlelair194::_sowrall
	String_t* ____sowrall_9;

public:
	inline static int32_t get_offset_of__dasnai_0() { return static_cast<int32_t>(offsetof(M_terejaiCairlelair194_t4147113968, ____dasnai_0)); }
	inline uint32_t get__dasnai_0() const { return ____dasnai_0; }
	inline uint32_t* get_address_of__dasnai_0() { return &____dasnai_0; }
	inline void set__dasnai_0(uint32_t value)
	{
		____dasnai_0 = value;
	}

	inline static int32_t get_offset_of__sejaKemdesir_1() { return static_cast<int32_t>(offsetof(M_terejaiCairlelair194_t4147113968, ____sejaKemdesir_1)); }
	inline String_t* get__sejaKemdesir_1() const { return ____sejaKemdesir_1; }
	inline String_t** get_address_of__sejaKemdesir_1() { return &____sejaKemdesir_1; }
	inline void set__sejaKemdesir_1(String_t* value)
	{
		____sejaKemdesir_1 = value;
		Il2CppCodeGenWriteBarrier(&____sejaKemdesir_1, value);
	}

	inline static int32_t get_offset_of__fawrougur_2() { return static_cast<int32_t>(offsetof(M_terejaiCairlelair194_t4147113968, ____fawrougur_2)); }
	inline uint32_t get__fawrougur_2() const { return ____fawrougur_2; }
	inline uint32_t* get_address_of__fawrougur_2() { return &____fawrougur_2; }
	inline void set__fawrougur_2(uint32_t value)
	{
		____fawrougur_2 = value;
	}

	inline static int32_t get_offset_of__tedrejor_3() { return static_cast<int32_t>(offsetof(M_terejaiCairlelair194_t4147113968, ____tedrejor_3)); }
	inline int32_t get__tedrejor_3() const { return ____tedrejor_3; }
	inline int32_t* get_address_of__tedrejor_3() { return &____tedrejor_3; }
	inline void set__tedrejor_3(int32_t value)
	{
		____tedrejor_3 = value;
	}

	inline static int32_t get_offset_of__diteseeBoule_4() { return static_cast<int32_t>(offsetof(M_terejaiCairlelair194_t4147113968, ____diteseeBoule_4)); }
	inline int32_t get__diteseeBoule_4() const { return ____diteseeBoule_4; }
	inline int32_t* get_address_of__diteseeBoule_4() { return &____diteseeBoule_4; }
	inline void set__diteseeBoule_4(int32_t value)
	{
		____diteseeBoule_4 = value;
	}

	inline static int32_t get_offset_of__herecayQeaka_5() { return static_cast<int32_t>(offsetof(M_terejaiCairlelair194_t4147113968, ____herecayQeaka_5)); }
	inline int32_t get__herecayQeaka_5() const { return ____herecayQeaka_5; }
	inline int32_t* get_address_of__herecayQeaka_5() { return &____herecayQeaka_5; }
	inline void set__herecayQeaka_5(int32_t value)
	{
		____herecayQeaka_5 = value;
	}

	inline static int32_t get_offset_of__nellobee_6() { return static_cast<int32_t>(offsetof(M_terejaiCairlelair194_t4147113968, ____nellobee_6)); }
	inline String_t* get__nellobee_6() const { return ____nellobee_6; }
	inline String_t** get_address_of__nellobee_6() { return &____nellobee_6; }
	inline void set__nellobee_6(String_t* value)
	{
		____nellobee_6 = value;
		Il2CppCodeGenWriteBarrier(&____nellobee_6, value);
	}

	inline static int32_t get_offset_of__werou_7() { return static_cast<int32_t>(offsetof(M_terejaiCairlelair194_t4147113968, ____werou_7)); }
	inline int32_t get__werou_7() const { return ____werou_7; }
	inline int32_t* get_address_of__werou_7() { return &____werou_7; }
	inline void set__werou_7(int32_t value)
	{
		____werou_7 = value;
	}

	inline static int32_t get_offset_of__teduxowLerfowje_8() { return static_cast<int32_t>(offsetof(M_terejaiCairlelair194_t4147113968, ____teduxowLerfowje_8)); }
	inline uint32_t get__teduxowLerfowje_8() const { return ____teduxowLerfowje_8; }
	inline uint32_t* get_address_of__teduxowLerfowje_8() { return &____teduxowLerfowje_8; }
	inline void set__teduxowLerfowje_8(uint32_t value)
	{
		____teduxowLerfowje_8 = value;
	}

	inline static int32_t get_offset_of__sowrall_9() { return static_cast<int32_t>(offsetof(M_terejaiCairlelair194_t4147113968, ____sowrall_9)); }
	inline String_t* get__sowrall_9() const { return ____sowrall_9; }
	inline String_t** get_address_of__sowrall_9() { return &____sowrall_9; }
	inline void set__sowrall_9(String_t* value)
	{
		____sowrall_9 = value;
		Il2CppCodeGenWriteBarrier(&____sowrall_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
