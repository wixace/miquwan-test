﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.PolyNode>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1157634881(__this, ___l0, method) ((  void (*) (Enumerator_t429144834 *, List_1_t409472064 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.PolyNode>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2972267633(__this, method) ((  void (*) (Enumerator_t429144834 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.PolyNode>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3657943069(__this, method) ((  Il2CppObject * (*) (Enumerator_t429144834 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.PolyNode>::Dispose()
#define Enumerator_Dispose_m2614282598(__this, method) ((  void (*) (Enumerator_t429144834 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.PolyNode>::VerifyState()
#define Enumerator_VerifyState_m3793184159(__this, method) ((  void (*) (Enumerator_t429144834 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.PolyNode>::MoveNext()
#define Enumerator_MoveNext_m251114589(__this, method) ((  bool (*) (Enumerator_t429144834 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.PolyNode>::get_Current()
#define Enumerator_get_Current_m280075222(__this, method) ((  PolyNode_t3336253808 * (*) (Enumerator_t429144834 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
