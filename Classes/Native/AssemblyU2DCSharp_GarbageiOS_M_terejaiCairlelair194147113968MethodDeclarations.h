﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_terejaiCairlelair194
struct M_terejaiCairlelair194_t4147113968;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_terejaiCairlelair194::.ctor()
extern "C"  void M_terejaiCairlelair194__ctor_m2074921027 (M_terejaiCairlelair194_t4147113968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_terejaiCairlelair194::M_parsounalTrorvair0(System.String[],System.Int32)
extern "C"  void M_terejaiCairlelair194_M_parsounalTrorvair0_m1672380932 (M_terejaiCairlelair194_t4147113968 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
