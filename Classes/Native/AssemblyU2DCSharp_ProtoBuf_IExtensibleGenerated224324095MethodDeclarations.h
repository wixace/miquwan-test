﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf_IExtensibleGenerated
struct ProtoBuf_IExtensibleGenerated_t224324095;
// JSVCall
struct JSVCall_t3708497963;
// ProtoBuf.IExtension
struct IExtension_t1606339106;
// ProtoBuf.IExtensible
struct IExtensible_t1056931882;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void ProtoBuf_IExtensibleGenerated::.ctor()
extern "C"  void ProtoBuf_IExtensibleGenerated__ctor_m528119372 (ProtoBuf_IExtensibleGenerated_t224324095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf_IExtensibleGenerated::IExtensible_GetExtensionObject__Boolean(JSVCall,System.Int32)
extern "C"  bool ProtoBuf_IExtensibleGenerated_IExtensible_GetExtensionObject__Boolean_m1457654547 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf_IExtensibleGenerated::__Register()
extern "C"  void ProtoBuf_IExtensibleGenerated___Register_m3220248059 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension ProtoBuf_IExtensibleGenerated::ilo_GetExtensionObject1(ProtoBuf.IExtensible,System.Boolean)
extern "C"  Il2CppObject * ProtoBuf_IExtensibleGenerated_ilo_GetExtensionObject1_m3242711524 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, bool ___createIfMissing1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
