﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginGameFan
struct  PluginGameFan_t3898102510  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginGameFan::token
	String_t* ___token_2;
	// System.String PluginGameFan::pid
	String_t* ___pid_3;
	// System.String PluginGameFan::appId
	String_t* ___appId_4;
	// System.String PluginGameFan::configId
	String_t* ___configId_5;
	// System.String PluginGameFan::appkey
	String_t* ___appkey_6;
	// System.String PluginGameFan::sign
	String_t* ___sign_7;
	// System.String PluginGameFan::extra
	String_t* ___extra_8;
	// System.String PluginGameFan::gameId
	String_t* ___gameId_9;
	// Mihua.SDK.PayInfo PluginGameFan::iapInfo
	PayInfo_t1775308120 * ___iapInfo_10;

public:
	inline static int32_t get_offset_of_token_2() { return static_cast<int32_t>(offsetof(PluginGameFan_t3898102510, ___token_2)); }
	inline String_t* get_token_2() const { return ___token_2; }
	inline String_t** get_address_of_token_2() { return &___token_2; }
	inline void set_token_2(String_t* value)
	{
		___token_2 = value;
		Il2CppCodeGenWriteBarrier(&___token_2, value);
	}

	inline static int32_t get_offset_of_pid_3() { return static_cast<int32_t>(offsetof(PluginGameFan_t3898102510, ___pid_3)); }
	inline String_t* get_pid_3() const { return ___pid_3; }
	inline String_t** get_address_of_pid_3() { return &___pid_3; }
	inline void set_pid_3(String_t* value)
	{
		___pid_3 = value;
		Il2CppCodeGenWriteBarrier(&___pid_3, value);
	}

	inline static int32_t get_offset_of_appId_4() { return static_cast<int32_t>(offsetof(PluginGameFan_t3898102510, ___appId_4)); }
	inline String_t* get_appId_4() const { return ___appId_4; }
	inline String_t** get_address_of_appId_4() { return &___appId_4; }
	inline void set_appId_4(String_t* value)
	{
		___appId_4 = value;
		Il2CppCodeGenWriteBarrier(&___appId_4, value);
	}

	inline static int32_t get_offset_of_configId_5() { return static_cast<int32_t>(offsetof(PluginGameFan_t3898102510, ___configId_5)); }
	inline String_t* get_configId_5() const { return ___configId_5; }
	inline String_t** get_address_of_configId_5() { return &___configId_5; }
	inline void set_configId_5(String_t* value)
	{
		___configId_5 = value;
		Il2CppCodeGenWriteBarrier(&___configId_5, value);
	}

	inline static int32_t get_offset_of_appkey_6() { return static_cast<int32_t>(offsetof(PluginGameFan_t3898102510, ___appkey_6)); }
	inline String_t* get_appkey_6() const { return ___appkey_6; }
	inline String_t** get_address_of_appkey_6() { return &___appkey_6; }
	inline void set_appkey_6(String_t* value)
	{
		___appkey_6 = value;
		Il2CppCodeGenWriteBarrier(&___appkey_6, value);
	}

	inline static int32_t get_offset_of_sign_7() { return static_cast<int32_t>(offsetof(PluginGameFan_t3898102510, ___sign_7)); }
	inline String_t* get_sign_7() const { return ___sign_7; }
	inline String_t** get_address_of_sign_7() { return &___sign_7; }
	inline void set_sign_7(String_t* value)
	{
		___sign_7 = value;
		Il2CppCodeGenWriteBarrier(&___sign_7, value);
	}

	inline static int32_t get_offset_of_extra_8() { return static_cast<int32_t>(offsetof(PluginGameFan_t3898102510, ___extra_8)); }
	inline String_t* get_extra_8() const { return ___extra_8; }
	inline String_t** get_address_of_extra_8() { return &___extra_8; }
	inline void set_extra_8(String_t* value)
	{
		___extra_8 = value;
		Il2CppCodeGenWriteBarrier(&___extra_8, value);
	}

	inline static int32_t get_offset_of_gameId_9() { return static_cast<int32_t>(offsetof(PluginGameFan_t3898102510, ___gameId_9)); }
	inline String_t* get_gameId_9() const { return ___gameId_9; }
	inline String_t** get_address_of_gameId_9() { return &___gameId_9; }
	inline void set_gameId_9(String_t* value)
	{
		___gameId_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameId_9, value);
	}

	inline static int32_t get_offset_of_iapInfo_10() { return static_cast<int32_t>(offsetof(PluginGameFan_t3898102510, ___iapInfo_10)); }
	inline PayInfo_t1775308120 * get_iapInfo_10() const { return ___iapInfo_10; }
	inline PayInfo_t1775308120 ** get_address_of_iapInfo_10() { return &___iapInfo_10; }
	inline void set_iapInfo_10(PayInfo_t1775308120 * value)
	{
		___iapInfo_10 = value;
		Il2CppCodeGenWriteBarrier(&___iapInfo_10, value);
	}
};

struct PluginGameFan_t3898102510_StaticFields
{
public:
	// System.Action PluginGameFan::<>f__am$cache9
	Action_t3771233898 * ___U3CU3Ef__amU24cache9_11;
	// System.Action PluginGameFan::<>f__am$cacheA
	Action_t3771233898 * ___U3CU3Ef__amU24cacheA_12;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_11() { return static_cast<int32_t>(offsetof(PluginGameFan_t3898102510_StaticFields, ___U3CU3Ef__amU24cache9_11)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache9_11() const { return ___U3CU3Ef__amU24cache9_11; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache9_11() { return &___U3CU3Ef__amU24cache9_11; }
	inline void set_U3CU3Ef__amU24cache9_11(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache9_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_12() { return static_cast<int32_t>(offsetof(PluginGameFan_t3898102510_StaticFields, ___U3CU3Ef__amU24cacheA_12)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cacheA_12() const { return ___U3CU3Ef__amU24cacheA_12; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cacheA_12() { return &___U3CU3Ef__amU24cacheA_12; }
	inline void set_U3CU3Ef__amU24cacheA_12(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cacheA_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
