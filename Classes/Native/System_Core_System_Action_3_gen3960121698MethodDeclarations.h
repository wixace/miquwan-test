﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_3_gen2075012986MethodDeclarations.h"

// System.Void System.Action`3<Skill,UnityEngine.Vector3,System.Int32[]>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m2197193282(__this, ___object0, ___method1, method) ((  void (*) (Action_3_t3960121698 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m1521137548_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`3<Skill,UnityEngine.Vector3,System.Int32[]>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m1776722542(__this, ___arg10, ___arg21, ___arg32, method) ((  void (*) (Action_3_t3960121698 *, Skill_t79944241 *, Vector3_t4282066566 , Int32U5BU5D_t3230847821*, const MethodInfo*))Action_3_Invoke_m50122056_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.IAsyncResult System.Action`3<Skill,UnityEngine.Vector3,System.Int32[]>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Action_3_BeginInvoke_m455529757(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method) ((  Il2CppObject * (*) (Action_3_t3960121698 *, Skill_t79944241 *, Vector3_t4282066566 , Int32U5BU5D_t3230847821*, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_3_BeginInvoke_m2347005567_gshared)(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method)
// System.Void System.Action`3<Skill,UnityEngine.Vector3,System.Int32[]>::EndInvoke(System.IAsyncResult)
#define Action_3_EndInvoke_m3017976386(__this, ___result0, method) ((  void (*) (Action_3_t3960121698 *, Il2CppObject *, const MethodInfo*))Action_3_EndInvoke_m851431324_gshared)(__this, ___result0, method)
