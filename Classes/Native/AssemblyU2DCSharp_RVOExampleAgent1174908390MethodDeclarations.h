﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RVOExampleAgent
struct RVOExampleAgent_t1174908390;
// Pathfinding.Path
struct Path_t1974241691;
// Seeker
struct Seeker_t2472610117;
// OnPathDelegate
struct OnPathDelegate_t598607977;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_RVOExampleAgent1174908390.h"
#include "AssemblyU2DCSharp_Seeker2472610117.h"
#include "AssemblyU2DCSharp_OnPathDelegate598607977.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void RVOExampleAgent::.ctor()
extern "C"  void RVOExampleAgent__ctor_m2137939013 (RVOExampleAgent_t1174908390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RVOExampleAgent::Awake()
extern "C"  void RVOExampleAgent_Awake_m2375544232 (RVOExampleAgent_t1174908390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RVOExampleAgent::Start()
extern "C"  void RVOExampleAgent_Start_m1085076805 (RVOExampleAgent_t1174908390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RVOExampleAgent::SetTarget(UnityEngine.Vector3)
extern "C"  void RVOExampleAgent_SetTarget_m467187683 (RVOExampleAgent_t1174908390 * __this, Vector3_t4282066566  ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RVOExampleAgent::SetColor(UnityEngine.Color)
extern "C"  void RVOExampleAgent_SetColor_m3054006502 (RVOExampleAgent_t1174908390 * __this, Color_t4194546905  ___col0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RVOExampleAgent::RecalculatePath()
extern "C"  void RVOExampleAgent_RecalculatePath_m3351236155 (RVOExampleAgent_t1174908390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RVOExampleAgent::OnPathComplete(Pathfinding.Path)
extern "C"  void RVOExampleAgent_OnPathComplete_m2586444589 (RVOExampleAgent_t1174908390 * __this, Path_t1974241691 * ____p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RVOExampleAgent::Update()
extern "C"  void RVOExampleAgent_Update_m3578462056 (RVOExampleAgent_t1174908390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RVOExampleAgent::ilo_RecalculatePath1(RVOExampleAgent)
extern "C"  void RVOExampleAgent_ilo_RecalculatePath1_m848215937 (Il2CppObject * __this /* static, unused */, RVOExampleAgent_t1174908390 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Path RVOExampleAgent::ilo_StartPath2(Seeker,UnityEngine.Vector3,UnityEngine.Vector3,OnPathDelegate)
extern "C"  Path_t1974241691 * RVOExampleAgent_ilo_StartPath2_m1311968333 (Il2CppObject * __this /* static, unused */, Seeker_t2472610117 * ____this0, Vector3_t4282066566  ___start1, Vector3_t4282066566  ___end2, OnPathDelegate_t598607977 * ___callback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RVOExampleAgent::ilo_Release3(Pathfinding.Path,System.Object)
extern "C"  void RVOExampleAgent_ilo_Release3_m2742798077 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, Il2CppObject * ___o1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
