﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.NoneBvr
struct NoneBvr_t4201262756;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"

// System.Void Entity.Behavior.NoneBvr::.ctor()
extern "C"  void NoneBvr__ctor_m298702806 (NoneBvr_t4201262756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID Entity.Behavior.NoneBvr::get_id()
extern "C"  uint8_t NoneBvr_get_id_m4239884832 (NoneBvr_t4201262756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.NoneBvr::Reason()
extern "C"  void NoneBvr_Reason_m221240498 (NoneBvr_t4201262756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.NoneBvr::Action()
extern "C"  void NoneBvr_Action_m3712914468 (NoneBvr_t4201262756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.NoneBvr::DoEntering()
extern "C"  void NoneBvr_DoEntering_m2027252579 (NoneBvr_t4201262756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.NoneBvr::DoLeaving()
extern "C"  void NoneBvr_DoLeaving_m1341589469 (NoneBvr_t4201262756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
