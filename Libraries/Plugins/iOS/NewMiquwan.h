//
//  MiquwanAtMihua.h
//  Unity-iPhone
//
//  Created by admin on 2018/6/15.
//

#ifndef NewMiquwan_h
#define NewMiquwan_h
#import <MiquwanSDK/MiquwanSDK.h>
#import "ToUnityUtil.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NewMiquwan : NSObject
+(NewMiquwan *) instance;
- (void)MQWSDKInit:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
@end


#endif /* NewMiquwan_h */

