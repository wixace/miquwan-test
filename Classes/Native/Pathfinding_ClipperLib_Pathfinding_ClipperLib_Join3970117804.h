﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.ClipperLib.OutPt
struct OutPt_t3947633180;

#include "mscorlib_System_Object4170816371.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_IntP3326126179.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.ClipperLib.Join
struct  Join_t3970117804  : public Il2CppObject
{
public:
	// Pathfinding.ClipperLib.OutPt Pathfinding.ClipperLib.Join::OutPt1
	OutPt_t3947633180 * ___OutPt1_0;
	// Pathfinding.ClipperLib.OutPt Pathfinding.ClipperLib.Join::OutPt2
	OutPt_t3947633180 * ___OutPt2_1;
	// Pathfinding.ClipperLib.IntPoint Pathfinding.ClipperLib.Join::OffPt
	IntPoint_t3326126179  ___OffPt_2;

public:
	inline static int32_t get_offset_of_OutPt1_0() { return static_cast<int32_t>(offsetof(Join_t3970117804, ___OutPt1_0)); }
	inline OutPt_t3947633180 * get_OutPt1_0() const { return ___OutPt1_0; }
	inline OutPt_t3947633180 ** get_address_of_OutPt1_0() { return &___OutPt1_0; }
	inline void set_OutPt1_0(OutPt_t3947633180 * value)
	{
		___OutPt1_0 = value;
		Il2CppCodeGenWriteBarrier(&___OutPt1_0, value);
	}

	inline static int32_t get_offset_of_OutPt2_1() { return static_cast<int32_t>(offsetof(Join_t3970117804, ___OutPt2_1)); }
	inline OutPt_t3947633180 * get_OutPt2_1() const { return ___OutPt2_1; }
	inline OutPt_t3947633180 ** get_address_of_OutPt2_1() { return &___OutPt2_1; }
	inline void set_OutPt2_1(OutPt_t3947633180 * value)
	{
		___OutPt2_1 = value;
		Il2CppCodeGenWriteBarrier(&___OutPt2_1, value);
	}

	inline static int32_t get_offset_of_OffPt_2() { return static_cast<int32_t>(offsetof(Join_t3970117804, ___OffPt_2)); }
	inline IntPoint_t3326126179  get_OffPt_2() const { return ___OffPt_2; }
	inline IntPoint_t3326126179 * get_address_of_OffPt_2() { return &___OffPt_2; }
	inline void set_OffPt_2(IntPoint_t3326126179  value)
	{
		___OffPt_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
