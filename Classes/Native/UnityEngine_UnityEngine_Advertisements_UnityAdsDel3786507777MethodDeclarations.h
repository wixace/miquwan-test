﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Object>
struct UnityAdsDelegate_2_t3786507777;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAdsDelegate_2__ctor_m4033885901_gshared (UnityAdsDelegate_2_t3786507777 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define UnityAdsDelegate_2__ctor_m4033885901(__this, ___object0, ___method1, method) ((  void (*) (UnityAdsDelegate_2_t3786507777 *, Il2CppObject *, IntPtr_t, const MethodInfo*))UnityAdsDelegate_2__ctor_m4033885901_gshared)(__this, ___object0, ___method1, method)
// System.Void UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Object>::Invoke(T1,T2)
extern "C"  void UnityAdsDelegate_2_Invoke_m1818993470_gshared (UnityAdsDelegate_2_t3786507777 * __this, Il2CppObject * ___p10, Il2CppObject * ___p21, const MethodInfo* method);
#define UnityAdsDelegate_2_Invoke_m1818993470(__this, ___p10, ___p21, method) ((  void (*) (UnityAdsDelegate_2_t3786507777 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))UnityAdsDelegate_2_Invoke_m1818993470_gshared)(__this, ___p10, ___p21, method)
// System.IAsyncResult UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAdsDelegate_2_BeginInvoke_m3096834653_gshared (UnityAdsDelegate_2_t3786507777 * __this, Il2CppObject * ___p10, Il2CppObject * ___p21, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define UnityAdsDelegate_2_BeginInvoke_m3096834653(__this, ___p10, ___p21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (UnityAdsDelegate_2_t3786507777 *, Il2CppObject *, Il2CppObject *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))UnityAdsDelegate_2_BeginInvoke_m3096834653_gshared)(__this, ___p10, ___p21, ___callback2, ___object3, method)
// System.Void UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAdsDelegate_2_EndInvoke_m1306597213_gshared (UnityAdsDelegate_2_t3786507777 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define UnityAdsDelegate_2_EndInvoke_m1306597213(__this, ___result0, method) ((  void (*) (UnityAdsDelegate_2_t3786507777 *, Il2CppObject *, const MethodInfo*))UnityAdsDelegate_2_EndInvoke_m1306597213_gshared)(__this, ___result0, method)
