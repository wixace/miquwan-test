﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSCheckFinishData
struct  CSCheckFinishData_t2410628981  : public Il2CppObject
{
public:
	// System.UInt32 CSCheckFinishData::stars
	uint32_t ___stars_0;
	// System.UInt32 CSCheckFinishData::starStatus
	uint32_t ___starStatus_1;
	// System.UInt32 CSCheckFinishData::bossHp
	uint32_t ___bossHp_2;
	// System.UInt32 CSCheckFinishData::bossHpPer
	uint32_t ___bossHpPer_3;

public:
	inline static int32_t get_offset_of_stars_0() { return static_cast<int32_t>(offsetof(CSCheckFinishData_t2410628981, ___stars_0)); }
	inline uint32_t get_stars_0() const { return ___stars_0; }
	inline uint32_t* get_address_of_stars_0() { return &___stars_0; }
	inline void set_stars_0(uint32_t value)
	{
		___stars_0 = value;
	}

	inline static int32_t get_offset_of_starStatus_1() { return static_cast<int32_t>(offsetof(CSCheckFinishData_t2410628981, ___starStatus_1)); }
	inline uint32_t get_starStatus_1() const { return ___starStatus_1; }
	inline uint32_t* get_address_of_starStatus_1() { return &___starStatus_1; }
	inline void set_starStatus_1(uint32_t value)
	{
		___starStatus_1 = value;
	}

	inline static int32_t get_offset_of_bossHp_2() { return static_cast<int32_t>(offsetof(CSCheckFinishData_t2410628981, ___bossHp_2)); }
	inline uint32_t get_bossHp_2() const { return ___bossHp_2; }
	inline uint32_t* get_address_of_bossHp_2() { return &___bossHp_2; }
	inline void set_bossHp_2(uint32_t value)
	{
		___bossHp_2 = value;
	}

	inline static int32_t get_offset_of_bossHpPer_3() { return static_cast<int32_t>(offsetof(CSCheckFinishData_t2410628981, ___bossHpPer_3)); }
	inline uint32_t get_bossHpPer_3() const { return ___bossHpPer_3; }
	inline uint32_t* get_address_of_bossHpPer_3() { return &___bossHpPer_3; }
	inline void set_bossHpPer_3(uint32_t value)
	{
		___bossHpPer_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
