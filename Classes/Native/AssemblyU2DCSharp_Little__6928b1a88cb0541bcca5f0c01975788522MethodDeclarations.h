﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._6928b1a88cb0541bcca5f0c01bff83bd
struct _6928b1a88cb0541bcca5f0c01bff83bd_t975788522;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._6928b1a88cb0541bcca5f0c01bff83bd::.ctor()
extern "C"  void _6928b1a88cb0541bcca5f0c01bff83bd__ctor_m3006223235 (_6928b1a88cb0541bcca5f0c01bff83bd_t975788522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6928b1a88cb0541bcca5f0c01bff83bd::_6928b1a88cb0541bcca5f0c01bff83bdm2(System.Int32)
extern "C"  int32_t _6928b1a88cb0541bcca5f0c01bff83bd__6928b1a88cb0541bcca5f0c01bff83bdm2_m2935650649 (_6928b1a88cb0541bcca5f0c01bff83bd_t975788522 * __this, int32_t ____6928b1a88cb0541bcca5f0c01bff83bda0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6928b1a88cb0541bcca5f0c01bff83bd::_6928b1a88cb0541bcca5f0c01bff83bdm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _6928b1a88cb0541bcca5f0c01bff83bd__6928b1a88cb0541bcca5f0c01bff83bdm_m3743049085 (_6928b1a88cb0541bcca5f0c01bff83bd_t975788522 * __this, int32_t ____6928b1a88cb0541bcca5f0c01bff83bda0, int32_t ____6928b1a88cb0541bcca5f0c01bff83bd111, int32_t ____6928b1a88cb0541bcca5f0c01bff83bdc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
