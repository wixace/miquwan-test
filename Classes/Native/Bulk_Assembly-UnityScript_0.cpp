﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// AnimationScrollTexture1
struct AnimationScrollTexture1_t3327804647;
// UnityEngine.Renderer
struct Renderer_t3076687687;
// System.Object
struct Il2CppObject;
// AnimationSpriteSheet1
struct AnimationSpriteSheet1_t2485186395;
// FPSCounter
struct FPSCounter_t1808341236;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E86524790.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DUnityScript_AnimationScrollTexture13327804647.h"
#include "AssemblyU2DUnityScript_AnimationScrollTexture13327804647MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "mscorlib_System_Single4291918972.h"
#include "UnityEngine_UnityEngine_Time4241968337MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer3076687687MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material3870600107MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer3076687687.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "AssemblyU2DUnityScript_AnimationSpriteSheet12485186395.h"
#include "AssemblyU2DUnityScript_AnimationSpriteSheet12485186395MethodDeclarations.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DUnityScript_FPSCounter1808341235.h"
#include "AssemblyU2DUnityScript_FPSCounter1808341235MethodDeclarations.h"
#include "mscorlib_System_Boolean476798718.h"
#include "UnityEngine_UnityEngine_Screen3187157168MethodDeclarations.h"
#include "mscorlib_System_Single4291918972MethodDeclarations.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices3947355960MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI3134605553MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Rect4241904616MethodDeclarations.h"
#include "mscorlib_System_Double3868226565.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t3076687687_m1231607476(__this, method) ((  Renderer_t3076687687 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AnimationScrollTexture1::.ctor()
extern "C"  void AnimationScrollTexture1__ctor_m1776587599 (AnimationScrollTexture1_t3327804647 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		__this->set_Speed_2((0.25f));
		return;
	}
}
// System.Void AnimationScrollTexture1::FixedUpdate()
extern const MethodInfo* Component_GetComponent_TisRenderer_t3076687687_m1231607476_MethodInfo_var;
extern const uint32_t AnimationScrollTexture1_FixedUpdate_m3422583178_MetadataUsageId;
extern "C"  void AnimationScrollTexture1_FixedUpdate_m3422583178 (AnimationScrollTexture1_t3327804647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationScrollTexture1_FixedUpdate_m3422583178_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_Speed_2();
		V_0 = ((float)((float)L_0*(float)((-L_1))));
		Renderer_t3076687687 * L_2 = Component_GetComponent_TisRenderer_t3076687687_m1231607476(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m1231607476_MethodInfo_var);
		NullCheck(L_2);
		Material_t3870600107 * L_3 = Renderer_get_material_m2720864603(L_2, /*hidden argument*/NULL);
		float L_4 = V_0;
		Vector2_t4282066565  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector2__ctor_m1517109030(&L_5, (((float)((float)0))), L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		Material_set_mainTextureOffset_m3397882654(L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimationScrollTexture1::Main()
extern "C"  void AnimationScrollTexture1_Main_m3020844942 (AnimationScrollTexture1_t3327804647 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void AnimationSpriteSheet1::.ctor()
extern "C"  void AnimationSpriteSheet1__ctor_m4032250907 (AnimationSpriteSheet1_t2485186395 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		__this->set_uvX_2(4);
		__this->set_uvY_3(2);
		__this->set_fps_4((24.0f));
		return;
	}
}
// System.Void AnimationSpriteSheet1::Update()
extern const MethodInfo* Component_GetComponent_TisRenderer_t3076687687_m1231607476_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral558922319;
extern const uint32_t AnimationSpriteSheet1_Update_m2172588626_MetadataUsageId;
extern "C"  void AnimationSpriteSheet1_Update_m2172588626 (AnimationSpriteSheet1_t2485186395 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationSpriteSheet1_Update_m2172588626_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Vector2_t4282066565  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Vector2_t4282066565  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		float L_0 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_fps_4();
		V_0 = (((int32_t)((int32_t)((float)((float)L_0*(float)L_1)))));
		int32_t L_2 = V_0;
		int32_t L_3 = __this->get_uvX_2();
		int32_t L_4 = __this->get_uvY_3();
		V_0 = ((int32_t)((int32_t)L_2%(int32_t)((int32_t)((int32_t)L_3*(int32_t)L_4))));
		int32_t L_5 = __this->get_uvX_2();
		int32_t L_6 = __this->get_uvY_3();
		Vector2_t4282066565  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector2__ctor_m1517109030(&L_7, ((float)((float)(1.0f)/(float)(((float)((float)L_5))))), ((float)((float)(1.0f)/(float)(((float)((float)L_6))))), /*hidden argument*/NULL);
		V_1 = L_7;
		int32_t L_8 = V_0;
		int32_t L_9 = __this->get_uvX_2();
		V_2 = ((int32_t)((int32_t)L_8%(int32_t)L_9));
		int32_t L_10 = V_0;
		int32_t L_11 = __this->get_uvX_2();
		V_3 = ((int32_t)((int32_t)L_10/(int32_t)L_11));
		int32_t L_12 = V_2;
		float L_13 = (&V_1)->get_x_1();
		float L_14 = (&V_1)->get_y_2();
		int32_t L_15 = V_3;
		float L_16 = (&V_1)->get_y_2();
		Vector2_t4282066565  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector2__ctor_m1517109030(&L_17, ((float)((float)(((float)((float)L_12)))*(float)L_13)), ((float)((float)((float)((float)(1.0f)-(float)L_14))-(float)((float)((float)(((float)((float)L_15)))*(float)L_16)))), /*hidden argument*/NULL);
		V_4 = L_17;
		Renderer_t3076687687 * L_18 = Component_GetComponent_TisRenderer_t3076687687_m1231607476(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m1231607476_MethodInfo_var);
		NullCheck(L_18);
		Material_t3870600107 * L_19 = Renderer_get_material_m2720864603(L_18, /*hidden argument*/NULL);
		Vector2_t4282066565  L_20 = V_4;
		NullCheck(L_19);
		Material_SetTextureOffset_m1301408396(L_19, _stringLiteral558922319, L_20, /*hidden argument*/NULL);
		Renderer_t3076687687 * L_21 = Component_GetComponent_TisRenderer_t3076687687_m1231607476(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m1231607476_MethodInfo_var);
		NullCheck(L_21);
		Material_t3870600107 * L_22 = Renderer_get_material_m2720864603(L_21, /*hidden argument*/NULL);
		Vector2_t4282066565  L_23 = V_1;
		NullCheck(L_22);
		Material_SetTextureScale_m1752758881(L_22, _stringLiteral558922319, L_23, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimationSpriteSheet1::Main()
extern "C"  void AnimationSpriteSheet1_Main_m2539418946 (AnimationSpriteSheet1_t2485186395 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FPSCounter::.ctor()
extern "C"  void FPSCounter__ctor_m269244989 (FPSCounter_t1808341236 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		__this->set_updateInterval_2((0.5f));
		__this->set_x_location_3(5);
		__this->set_y_location_4(5);
		return;
	}
}
// System.Void FPSCounter::Awake()
extern "C"  void FPSCounter_Awake_m506850208 (FPSCounter_t1808341236 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour_set_useGUILayout_m589898551(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FPSCounter::OnGUI()
extern Il2CppClass* RuntimeServices_t3947355960_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral67111343;
extern Il2CppCodeGenString* _stringLiteral3212;
extern const uint32_t FPSCounter_OnGUI_m4059610935_MetadataUsageId;
extern "C"  void FPSCounter_OnGUI_m4059610935 (FPSCounter_t1808341236 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FPSCounter_OnGUI_m4059610935_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_x_location_3();
		int32_t L_2 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_y_location_4();
		Rect_t4241904616  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Rect__ctor_m3291325233(&L_4, (((float)((float)((int32_t)((int32_t)L_0-(int32_t)L_1))))), (((float)((float)((int32_t)((int32_t)L_2-(int32_t)L_3))))), (((float)((float)((int32_t)100)))), (((float)((float)((int32_t)30)))), /*hidden argument*/NULL);
		float* L_5 = __this->get_address_of_fps_7();
		String_t* L_6 = Single_ToString_m639595682(L_5, _stringLiteral3212, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t3947355960_il2cpp_TypeInfo_var);
		String_t* L_7 = RuntimeServices_op_Addition_m3391097550(NULL /*static, unused*/, _stringLiteral67111343, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_Label_m1483857617(NULL /*static, unused*/, L_4, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FPSCounter::Start()
extern "C"  void FPSCounter_Start_m3511350077 (FPSCounter_t1808341236 * __this, const MethodInfo* method)
{
	{
		float L_0 = Time_get_realtimeSinceStartup_m2972554983(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lastInterval_5((((double)((double)L_0))));
		__this->set_frames_6(0);
		return;
	}
}
// System.Void FPSCounter::Update()
extern "C"  void FPSCounter_Update_m1483522160 (FPSCounter_t1808341236 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		int32_t L_0 = __this->get_frames_6();
		__this->set_frames_6(((int32_t)((int32_t)L_0+(int32_t)1)));
		float L_1 = Time_get_realtimeSinceStartup_m2972554983(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = V_0;
		double L_3 = __this->get_lastInterval_5();
		float L_4 = __this->get_updateInterval_2();
		if ((((double)(((double)((double)L_2)))) <= ((double)((double)((double)L_3+(double)(((double)((double)L_4))))))))
		{
			goto IL_0050;
		}
	}
	{
		int32_t L_5 = __this->get_frames_6();
		float L_6 = V_0;
		double L_7 = __this->get_lastInterval_5();
		__this->set_fps_7((((float)((float)((double)((double)(((double)((double)L_5)))/(double)((double)((double)(((double)((double)L_6)))-(double)L_7))))))));
		__this->set_frames_6(0);
		float L_8 = V_0;
		__this->set_lastInterval_5((((double)((double)L_8))));
	}

IL_0050:
	{
		return;
	}
}
// System.Void FPSCounter::Main()
extern "C"  void FPSCounter_Main_m4219146976 (FPSCounter_t1808341236 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
