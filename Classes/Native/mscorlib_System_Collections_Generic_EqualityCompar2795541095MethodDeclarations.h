﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Core.RpsChoice>
struct DefaultComparer_t2795541095;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Core_RpsChoice52797132.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Core.RpsChoice>::.ctor()
extern "C"  void DefaultComparer__ctor_m2396458323_gshared (DefaultComparer_t2795541095 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2396458323(__this, method) ((  void (*) (DefaultComparer_t2795541095 *, const MethodInfo*))DefaultComparer__ctor_m2396458323_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Core.RpsChoice>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3768131392_gshared (DefaultComparer_t2795541095 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3768131392(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2795541095 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m3768131392_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Core.RpsChoice>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1988489320_gshared (DefaultComparer_t2795541095 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1988489320(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2795541095 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m1988489320_gshared)(__this, ___x0, ___y1, method)
