﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Type
struct Type_t;

#include "mscorlib_System_Attribute2523058482.h"
#include "mscorlib_System_Nullable_1_gen560925241.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SharpKit.JavaScript.JsPropertyAttribute
struct  JsPropertyAttribute_t1787918292  : public Attribute_t2523058482
{
public:
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsPropertyAttribute::_NativeField
	Nullable_1_t560925241  ____NativeField_0;
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsPropertyAttribute::_Export
	Nullable_1_t560925241  ____Export_1;
	// System.String SharpKit.JavaScript.JsPropertyAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_2;
	// System.String SharpKit.JavaScript.JsPropertyAttribute::<TargetProperty>k__BackingField
	String_t* ___U3CTargetPropertyU3Ek__BackingField_3;
	// System.Type SharpKit.JavaScript.JsPropertyAttribute::<TargetType>k__BackingField
	Type_t * ___U3CTargetTypeU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of__NativeField_0() { return static_cast<int32_t>(offsetof(JsPropertyAttribute_t1787918292, ____NativeField_0)); }
	inline Nullable_1_t560925241  get__NativeField_0() const { return ____NativeField_0; }
	inline Nullable_1_t560925241 * get_address_of__NativeField_0() { return &____NativeField_0; }
	inline void set__NativeField_0(Nullable_1_t560925241  value)
	{
		____NativeField_0 = value;
	}

	inline static int32_t get_offset_of__Export_1() { return static_cast<int32_t>(offsetof(JsPropertyAttribute_t1787918292, ____Export_1)); }
	inline Nullable_1_t560925241  get__Export_1() const { return ____Export_1; }
	inline Nullable_1_t560925241 * get_address_of__Export_1() { return &____Export_1; }
	inline void set__Export_1(Nullable_1_t560925241  value)
	{
		____Export_1 = value;
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(JsPropertyAttribute_t1787918292, ___U3CNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CNameU3Ek__BackingField_2() const { return ___U3CNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_2() { return &___U3CNameU3Ek__BackingField_2; }
	inline void set_U3CNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CNameU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CTargetPropertyU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(JsPropertyAttribute_t1787918292, ___U3CTargetPropertyU3Ek__BackingField_3)); }
	inline String_t* get_U3CTargetPropertyU3Ek__BackingField_3() const { return ___U3CTargetPropertyU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CTargetPropertyU3Ek__BackingField_3() { return &___U3CTargetPropertyU3Ek__BackingField_3; }
	inline void set_U3CTargetPropertyU3Ek__BackingField_3(String_t* value)
	{
		___U3CTargetPropertyU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTargetPropertyU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CTargetTypeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(JsPropertyAttribute_t1787918292, ___U3CTargetTypeU3Ek__BackingField_4)); }
	inline Type_t * get_U3CTargetTypeU3Ek__BackingField_4() const { return ___U3CTargetTypeU3Ek__BackingField_4; }
	inline Type_t ** get_address_of_U3CTargetTypeU3Ek__BackingField_4() { return &___U3CTargetTypeU3Ek__BackingField_4; }
	inline void set_U3CTargetTypeU3Ek__BackingField_4(Type_t * value)
	{
		___U3CTargetTypeU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTargetTypeU3Ek__BackingField_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
