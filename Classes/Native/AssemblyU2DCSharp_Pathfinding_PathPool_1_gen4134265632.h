﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t2974409999;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.PathPool`1<System.Object>
struct  PathPool_1_t4134265632  : public Il2CppObject
{
public:

public:
};

struct PathPool_1_t4134265632_StaticFields
{
public:
	// System.Collections.Generic.Stack`1<T> Pathfinding.PathPool`1::pool
	Stack_1_t2974409999 * ___pool_0;
	// System.Int32 Pathfinding.PathPool`1::totalCreated
	int32_t ___totalCreated_1;

public:
	inline static int32_t get_offset_of_pool_0() { return static_cast<int32_t>(offsetof(PathPool_1_t4134265632_StaticFields, ___pool_0)); }
	inline Stack_1_t2974409999 * get_pool_0() const { return ___pool_0; }
	inline Stack_1_t2974409999 ** get_address_of_pool_0() { return &___pool_0; }
	inline void set_pool_0(Stack_1_t2974409999 * value)
	{
		___pool_0 = value;
		Il2CppCodeGenWriteBarrier(&___pool_0, value);
	}

	inline static int32_t get_offset_of_totalCreated_1() { return static_cast<int32_t>(offsetof(PathPool_1_t4134265632_StaticFields, ___totalCreated_1)); }
	inline int32_t get_totalCreated_1() const { return ___totalCreated_1; }
	inline int32_t* get_address_of_totalCreated_1() { return &___totalCreated_1; }
	inline void set_totalCreated_1(int32_t value)
	{
		___totalCreated_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
