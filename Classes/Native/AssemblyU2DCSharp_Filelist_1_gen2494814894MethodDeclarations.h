﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Filelist_1_gen1153604171MethodDeclarations.h"

// System.Void Filelist`1<FileInfoRes>::.ctor()
#define Filelist_1__ctor_m375476690(__this, method) ((  void (*) (Filelist_1_t2494814894 *, const MethodInfo*))Filelist_1__ctor_m1745091098_gshared)(__this, method)
// Filelist`1<T> Filelist`1<FileInfoRes>::CreatFromBytes(System.Byte[])
#define Filelist_1_CreatFromBytes_m2988230071(__this /* static, unused */, ___bytes0, method) ((  Filelist_1_t2494814894 * (*) (Il2CppObject * /* static, unused */, ByteU5BU5D_t4260760469*, const MethodInfo*))Filelist_1_CreatFromBytes_m2160290303_gshared)(__this /* static, unused */, ___bytes0, method)
// System.Void Filelist`1<FileInfoRes>::AddResData(T)
#define Filelist_1_AddResData_m803702275(__this, ___data0, method) ((  void (*) (Filelist_1_t2494814894 *, FileInfoRes_t1217059798 *, const MethodInfo*))Filelist_1_AddResData_m934074443_gshared)(__this, ___data0, method)
// System.Void Filelist`1<FileInfoRes>::AddOrReplaceResData(T)
#define Filelist_1_AddOrReplaceResData_m848431572(__this, ___data0, method) ((  void (*) (Filelist_1_t2494814894 *, FileInfoRes_t1217059798 *, const MethodInfo*))Filelist_1_AddOrReplaceResData_m1397803148_gshared)(__this, ___data0, method)
// System.Void Filelist`1<FileInfoRes>::ReadBytes(System.Byte[])
#define Filelist_1_ReadBytes_m863195460(__this, ___bytes0, method) ((  void (*) (Filelist_1_t2494814894 *, ByteU5BU5D_t4260760469*, const MethodInfo*))Filelist_1_ReadBytes_m528302588_gshared)(__this, ___bytes0, method)
// System.Void Filelist`1<FileInfoRes>::RemoveResData(System.String)
#define Filelist_1_RemoveResData_m2874730764(__this, ___dataPath0, method) ((  void (*) (Filelist_1_t2494814894 *, String_t*, const MethodInfo*))Filelist_1_RemoveResData_m2869673412_gshared)(__this, ___dataPath0, method)
// System.Void Filelist`1<FileInfoRes>::RemoveResData(T)
#define Filelist_1_RemoveResData_m39751272(__this, ___data0, method) ((  void (*) (Filelist_1_t2494814894 *, FileInfoRes_t1217059798 *, const MethodInfo*))Filelist_1_RemoveResData_m1306572576_gshared)(__this, ___data0, method)
// T Filelist`1<FileInfoRes>::GetResData(System.String)
#define Filelist_1_GetResData_m3384635099(__this, ___fileName0, method) ((  FileInfoRes_t1217059798 * (*) (Filelist_1_t2494814894 *, String_t*, const MethodInfo*))Filelist_1_GetResData_m1855791459_gshared)(__this, ___fileName0, method)
// System.String Filelist`1<FileInfoRes>::ToString()
#define Filelist_1_ToString_m790526747(__this, method) ((  String_t* (*) (Filelist_1_t2494814894 *, const MethodInfo*))Filelist_1_ToString_m1484309139_gshared)(__this, method)
