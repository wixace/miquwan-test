﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIStretch
struct UIStretch_t3439076817;
// UIRoot
struct UIRoot_t2503447958;
// UIWidget
struct UIWidget_t769069560;
// UIAtlas
struct UIAtlas_t281921111;
// UISprite
struct UISprite_t661437049;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIRoot2503447958.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_UISprite661437049.h"

// System.Void UIStretch::.ctor()
extern "C"  void UIStretch__ctor_m2565728954 (UIStretch_t3439076817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIStretch::Awake()
extern "C"  void UIStretch_Awake_m2803334173 (UIStretch_t3439076817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIStretch::OnDestroy()
extern "C"  void UIStretch_OnDestroy_m3589788339 (UIStretch_t3439076817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIStretch::ScreenSizeChanged()
extern "C"  void UIStretch_ScreenSizeChanged_m2502851327 (UIStretch_t3439076817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIStretch::Start()
extern "C"  void UIStretch_Start_m1512866746 (UIStretch_t3439076817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIStretch::Update()
extern "C"  void UIStretch_Update_m3955048339 (UIStretch_t3439076817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIStretch::ilo_get_activeHeight1(UIRoot)
extern "C"  int32_t UIStretch_ilo_get_activeHeight1_m3621928696 (Il2CppObject * __this /* static, unused */, UIRoot_t2503447958 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIStretch::ilo_get_width2(UIWidget)
extern "C"  int32_t UIStretch_ilo_get_width2_m3186867148 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIStretch::ilo_get_height3(UIWidget)
extern "C"  int32_t UIStretch_ilo_get_height3_m253283390 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIAtlas UIStretch::ilo_get_atlas4(UISprite)
extern "C"  UIAtlas_t281921111 * UIStretch_ilo_get_atlas4_m3320518330 (Il2CppObject * __this /* static, unused */, UISprite_t661437049 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
