﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UpdateForJs
struct UpdateForJs_t1071193225;

#include "codegen/il2cpp-codegen.h"

// System.Void UpdateForJs::.ctor()
extern "C"  void UpdateForJs__ctor_m2602526978 (UpdateForJs_t1071193225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateForJs::ZUpdate()
extern "C"  void UpdateForJs_ZUpdate_m925988739 (UpdateForJs_t1071193225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateForJs::Finalize()
extern "C"  void UpdateForJs_Finalize_m3529048096 (UpdateForJs_t1071193225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
