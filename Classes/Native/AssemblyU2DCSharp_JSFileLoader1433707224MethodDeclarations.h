﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSFileLoader
struct JSFileLoader_t1433707224;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void JSFileLoader::.ctor()
extern "C"  void JSFileLoader__ctor_m2892038851 (JSFileLoader_t1433707224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] JSFileLoader::LoadJSSync(System.String)
extern "C"  ByteU5BU5D_t4260760469* JSFileLoader_LoadJSSync_m2645058349 (JSFileLoader_t1433707224 * __this, String_t* ___fullName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
