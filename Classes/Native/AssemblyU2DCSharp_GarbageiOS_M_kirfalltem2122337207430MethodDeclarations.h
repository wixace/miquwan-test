﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_kirfalltem212
struct M_kirfalltem212_t2337207430;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_kirfalltem212::.ctor()
extern "C"  void M_kirfalltem212__ctor_m2815825821 (M_kirfalltem212_t2337207430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kirfalltem212::M_troru0(System.String[],System.Int32)
extern "C"  void M_kirfalltem212_M_troru0_m3383863372 (M_kirfalltem212_t2337207430 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kirfalltem212::M_zemsawTeagis1(System.String[],System.Int32)
extern "C"  void M_kirfalltem212_M_zemsawTeagis1_m1031067881 (M_kirfalltem212_t2337207430 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
