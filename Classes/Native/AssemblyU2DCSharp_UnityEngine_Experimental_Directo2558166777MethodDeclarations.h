﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated
struct UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_t2558166777;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::.ctor()
extern "C"  void UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated__ctor_m2663769538 (UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_t2558166777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_layerCount(JSVCall)
extern "C"  void UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_layerCount_m1091525890 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_parameterCount(JSVCall)
extern "C"  void UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_parameterCount_m2928260186 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_CrossFade__Int32__Single__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_CrossFade__Int32__Single__Int32__Single_m4041277011 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_CrossFade__String__Single__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_CrossFade__String__Single__Int32__Single_m1181793772 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_CrossFadeInFixedTime__String__Single__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_CrossFadeInFixedTime__String__Single__Int32__Single_m1106748308 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_CrossFadeInFixedTime__Int32__Single__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_CrossFadeInFixedTime__Int32__Single__Int32__Single_m1267909547 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_GetAnimatorTransitionInfo__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_GetAnimatorTransitionInfo__Int32_m2321748825 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_GetBool__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_GetBool__Int32_m3479413721 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_GetBool__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_GetBool__String_m1983730408 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_GetCurrentAnimatorClipInfo__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_GetCurrentAnimatorClipInfo__Int32_m2048915005 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_GetCurrentAnimatorStateInfo__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_GetCurrentAnimatorStateInfo__Int32_m3179399076 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_GetFloat__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_GetFloat__Int32_m4170877791 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_GetFloat__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_GetFloat__String_m1944280098 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_GetInteger__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_GetInteger__String_m2864675460 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_GetInteger__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_GetInteger__Int32_m1013979325 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_GetLayerIndex__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_GetLayerIndex__String_m983899839 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_GetLayerName__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_GetLayerName__Int32_m3340050495 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_GetLayerWeight__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_GetLayerWeight__Int32_m3637662898 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_GetNextAnimatorClipInfo__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_GetNextAnimatorClipInfo__Int32_m2109312075 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_GetNextAnimatorStateInfo__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_GetNextAnimatorStateInfo__Int32_m756740950 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_GetParameter__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_GetParameter__Int32_m488131378 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_HasState__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_HasState__Int32__Int32_m3335997442 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_IsInTransition__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_IsInTransition__Int32_m993061281 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_IsParameterControlledByCurve__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_IsParameterControlledByCurve__String_m2823592775 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_IsParameterControlledByCurve__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_IsParameterControlledByCurve__Int32_m1982485402 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_Play__String__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_Play__String__Int32__Single_m3842986344 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_Play__Int32__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_Play__Int32__Int32__Single_m3500889063 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_PlayInFixedTime__String__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_PlayInFixedTime__String__Int32__Single_m845592616 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_PlayInFixedTime__Int32__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_PlayInFixedTime__Int32__Int32__Single_m3681293607 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_ResetTrigger__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_ResetTrigger__String_m1962854565 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_ResetTrigger__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_ResetTrigger__Int32_m569246332 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_SetBool__String__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_SetBool__String__Boolean_m388395702 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_SetBool__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_SetBool__Int32__Boolean_m590093181 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_SetFloat__String__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_SetFloat__String__Single_m1734315230 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_SetFloat__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_SetFloat__Int32__Single_m3082070451 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_SetInteger__String__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_SetInteger__String__Int32_m3398097752 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_SetInteger__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_SetInteger__Int32__Int32_m1032868711 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_SetLayerWeight__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_SetLayerWeight__Int32__Single_m692041478 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_SetTrigger__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_SetTrigger__String_m2018052082 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::IAnimatorControllerPlayable_SetTrigger__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_IAnimatorControllerPlayable_SetTrigger__Int32_m2649236879 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::__Register()
extern "C"  void UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated___Register_m1255071813 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::ilo_getSingle1(System.Int32)
extern "C"  float UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_ilo_getSingle1_m3711180709 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::ilo_getStringS2(System.Int32)
extern "C"  String_t* UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_ilo_getStringS2_m887133057 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::ilo_getInt323(System.Int32)
extern "C"  int32_t UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_ilo_getInt323_m766398475 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::ilo_moveSaveID2Arr4(System.Int32)
extern "C"  void UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_ilo_moveSaveID2Arr4_m1152779290 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::ilo_setObject5(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_ilo_setObject5_m1223312524 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::ilo_setSingle6(System.Int32,System.Single)
extern "C"  void UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_ilo_setSingle6_m3081191927 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::ilo_setArrayS7(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_ilo_setArrayS7_m929761287 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated::ilo_setBooleanS8(System.Int32,System.Boolean)
extern "C"  void UnityEngine_Experimental_Director_IAnimatorControllerPlayableGenerated_ilo_setBooleanS8_m3891632140 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
