﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated/<AssetOperationLoad__ctor_GetDelegate_member0_arg1>c__AnonStorey6F
struct U3CAssetOperationLoad__ctor_GetDelegate_member0_arg1U3Ec__AnonStorey6F_t3350490533;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset778728221.h"

// System.Void Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated/<AssetOperationLoad__ctor_GetDelegate_member0_arg1>c__AnonStorey6F::.ctor()
extern "C"  void U3CAssetOperationLoad__ctor_GetDelegate_member0_arg1U3Ec__AnonStorey6F__ctor_m764503190 (U3CAssetOperationLoad__ctor_GetDelegate_member0_arg1U3Ec__AnonStorey6F_t3350490533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated/<AssetOperationLoad__ctor_GetDelegate_member0_arg1>c__AnonStorey6F::<>m__78(Mihua.Asset.ABLoadOperation.AssetOperation)
extern "C"  void U3CAssetOperationLoad__ctor_GetDelegate_member0_arg1U3Ec__AnonStorey6F_U3CU3Em__78_m1737976295 (U3CAssetOperationLoad__ctor_GetDelegate_member0_arg1U3Ec__AnonStorey6F_t3350490533 * __this, AssetOperation_t778728221 * ___assetOperation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
