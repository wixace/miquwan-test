﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Font
struct Font_t4241557075;
// UIFont
struct UIFont_t2503090435;
// System.String
struct String_t;
// UnityEngine.Material
struct Material_t3870600107;
// BetterList`1<UILabel>
struct BetterList_1_t1788472332;
// System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Int32>
struct Dictionary_2_t1374193376;
// System.Collections.Generic.List`1<UIDrawCall>
struct List_1_t2281459526;
// BetterList`1<UnityEngine.Vector3>
struct BetterList_1_t1484067282;
// BetterList`1<System.Int32>
struct BetterList_1_t2650806512;

#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_UILabel_Crispness3952758239.h"
#include "UnityEngine_UnityEngine_FontStyle3350479768.h"
#include "AssemblyU2DCSharp_NGUIText_Alignment3426431694.h"
#include "AssemblyU2DCSharp_UILabel_Effect3176279520.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_NGUIText_SymbolStyle99318052.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_UILabel_Overflow229724305.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UILabel
struct  UILabel_t291504320  : public UIWidget_t769069560
{
public:
	// UILabel/Crispness UILabel::keepCrispWhenShrunk
	int32_t ___keepCrispWhenShrunk_53;
	// UnityEngine.Font UILabel::mTrueTypeFont
	Font_t4241557075 * ___mTrueTypeFont_54;
	// UIFont UILabel::mFont
	UIFont_t2503090435 * ___mFont_55;
	// System.String UILabel::mText
	String_t* ___mText_56;
	// System.Int32 UILabel::midKey
	int32_t ___midKey_57;
	// System.Int32 UILabel::mFontSize
	int32_t ___mFontSize_58;
	// UnityEngine.FontStyle UILabel::mFontStyle
	int32_t ___mFontStyle_59;
	// NGUIText/Alignment UILabel::mAlignment
	int32_t ___mAlignment_60;
	// System.Boolean UILabel::mEncoding
	bool ___mEncoding_61;
	// System.Int32 UILabel::mMaxLineCount
	int32_t ___mMaxLineCount_62;
	// UILabel/Effect UILabel::mEffectStyle
	int32_t ___mEffectStyle_63;
	// UnityEngine.Color UILabel::mEffectColor
	Color_t4194546905  ___mEffectColor_64;
	// NGUIText/SymbolStyle UILabel::mSymbols
	int32_t ___mSymbols_65;
	// UnityEngine.Vector2 UILabel::mEffectDistance
	Vector2_t4282066565  ___mEffectDistance_66;
	// UILabel/Overflow UILabel::mOverflow
	int32_t ___mOverflow_67;
	// UnityEngine.Material UILabel::mMaterial
	Material_t3870600107 * ___mMaterial_68;
	// System.Boolean UILabel::mApplyGradient
	bool ___mApplyGradient_69;
	// UnityEngine.Color UILabel::mGradientTop
	Color_t4194546905  ___mGradientTop_70;
	// UnityEngine.Color UILabel::mGradientBottom
	Color_t4194546905  ___mGradientBottom_71;
	// System.Int32 UILabel::mSpacingX
	int32_t ___mSpacingX_72;
	// System.Int32 UILabel::mSpacingY
	int32_t ___mSpacingY_73;
	// System.Boolean UILabel::mUseFloatSpacing
	bool ___mUseFloatSpacing_74;
	// System.Single UILabel::mFloatSpacingX
	float ___mFloatSpacingX_75;
	// System.Single UILabel::mFloatSpacingY
	float ___mFloatSpacingY_76;
	// System.Boolean UILabel::mOverflowEllipsis
	bool ___mOverflowEllipsis_77;
	// System.Boolean UILabel::mShrinkToFit
	bool ___mShrinkToFit_78;
	// System.Int32 UILabel::mMaxLineWidth
	int32_t ___mMaxLineWidth_79;
	// System.Int32 UILabel::mMaxLineHeight
	int32_t ___mMaxLineHeight_80;
	// System.Single UILabel::mLineWidth
	float ___mLineWidth_81;
	// System.Boolean UILabel::mMultiline
	bool ___mMultiline_82;
	// UnityEngine.Font UILabel::mActiveTTF
	Font_t4241557075 * ___mActiveTTF_83;
	// System.Single UILabel::mDensity
	float ___mDensity_84;
	// System.Boolean UILabel::mShouldBeProcessed
	bool ___mShouldBeProcessed_85;
	// System.String UILabel::mProcessedText
	String_t* ___mProcessedText_86;
	// System.Boolean UILabel::mPremultiply
	bool ___mPremultiply_87;
	// UnityEngine.Vector2 UILabel::mCalculatedSize
	Vector2_t4282066565  ___mCalculatedSize_88;
	// System.Single UILabel::mScale
	float ___mScale_89;
	// System.Int32 UILabel::mFinalFontSize
	int32_t ___mFinalFontSize_90;
	// System.Int32 UILabel::mLastWidth
	int32_t ___mLastWidth_91;
	// System.Int32 UILabel::mLastHeight
	int32_t ___mLastHeight_92;

public:
	inline static int32_t get_offset_of_keepCrispWhenShrunk_53() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___keepCrispWhenShrunk_53)); }
	inline int32_t get_keepCrispWhenShrunk_53() const { return ___keepCrispWhenShrunk_53; }
	inline int32_t* get_address_of_keepCrispWhenShrunk_53() { return &___keepCrispWhenShrunk_53; }
	inline void set_keepCrispWhenShrunk_53(int32_t value)
	{
		___keepCrispWhenShrunk_53 = value;
	}

	inline static int32_t get_offset_of_mTrueTypeFont_54() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mTrueTypeFont_54)); }
	inline Font_t4241557075 * get_mTrueTypeFont_54() const { return ___mTrueTypeFont_54; }
	inline Font_t4241557075 ** get_address_of_mTrueTypeFont_54() { return &___mTrueTypeFont_54; }
	inline void set_mTrueTypeFont_54(Font_t4241557075 * value)
	{
		___mTrueTypeFont_54 = value;
		Il2CppCodeGenWriteBarrier(&___mTrueTypeFont_54, value);
	}

	inline static int32_t get_offset_of_mFont_55() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mFont_55)); }
	inline UIFont_t2503090435 * get_mFont_55() const { return ___mFont_55; }
	inline UIFont_t2503090435 ** get_address_of_mFont_55() { return &___mFont_55; }
	inline void set_mFont_55(UIFont_t2503090435 * value)
	{
		___mFont_55 = value;
		Il2CppCodeGenWriteBarrier(&___mFont_55, value);
	}

	inline static int32_t get_offset_of_mText_56() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mText_56)); }
	inline String_t* get_mText_56() const { return ___mText_56; }
	inline String_t** get_address_of_mText_56() { return &___mText_56; }
	inline void set_mText_56(String_t* value)
	{
		___mText_56 = value;
		Il2CppCodeGenWriteBarrier(&___mText_56, value);
	}

	inline static int32_t get_offset_of_midKey_57() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___midKey_57)); }
	inline int32_t get_midKey_57() const { return ___midKey_57; }
	inline int32_t* get_address_of_midKey_57() { return &___midKey_57; }
	inline void set_midKey_57(int32_t value)
	{
		___midKey_57 = value;
	}

	inline static int32_t get_offset_of_mFontSize_58() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mFontSize_58)); }
	inline int32_t get_mFontSize_58() const { return ___mFontSize_58; }
	inline int32_t* get_address_of_mFontSize_58() { return &___mFontSize_58; }
	inline void set_mFontSize_58(int32_t value)
	{
		___mFontSize_58 = value;
	}

	inline static int32_t get_offset_of_mFontStyle_59() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mFontStyle_59)); }
	inline int32_t get_mFontStyle_59() const { return ___mFontStyle_59; }
	inline int32_t* get_address_of_mFontStyle_59() { return &___mFontStyle_59; }
	inline void set_mFontStyle_59(int32_t value)
	{
		___mFontStyle_59 = value;
	}

	inline static int32_t get_offset_of_mAlignment_60() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mAlignment_60)); }
	inline int32_t get_mAlignment_60() const { return ___mAlignment_60; }
	inline int32_t* get_address_of_mAlignment_60() { return &___mAlignment_60; }
	inline void set_mAlignment_60(int32_t value)
	{
		___mAlignment_60 = value;
	}

	inline static int32_t get_offset_of_mEncoding_61() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mEncoding_61)); }
	inline bool get_mEncoding_61() const { return ___mEncoding_61; }
	inline bool* get_address_of_mEncoding_61() { return &___mEncoding_61; }
	inline void set_mEncoding_61(bool value)
	{
		___mEncoding_61 = value;
	}

	inline static int32_t get_offset_of_mMaxLineCount_62() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mMaxLineCount_62)); }
	inline int32_t get_mMaxLineCount_62() const { return ___mMaxLineCount_62; }
	inline int32_t* get_address_of_mMaxLineCount_62() { return &___mMaxLineCount_62; }
	inline void set_mMaxLineCount_62(int32_t value)
	{
		___mMaxLineCount_62 = value;
	}

	inline static int32_t get_offset_of_mEffectStyle_63() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mEffectStyle_63)); }
	inline int32_t get_mEffectStyle_63() const { return ___mEffectStyle_63; }
	inline int32_t* get_address_of_mEffectStyle_63() { return &___mEffectStyle_63; }
	inline void set_mEffectStyle_63(int32_t value)
	{
		___mEffectStyle_63 = value;
	}

	inline static int32_t get_offset_of_mEffectColor_64() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mEffectColor_64)); }
	inline Color_t4194546905  get_mEffectColor_64() const { return ___mEffectColor_64; }
	inline Color_t4194546905 * get_address_of_mEffectColor_64() { return &___mEffectColor_64; }
	inline void set_mEffectColor_64(Color_t4194546905  value)
	{
		___mEffectColor_64 = value;
	}

	inline static int32_t get_offset_of_mSymbols_65() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mSymbols_65)); }
	inline int32_t get_mSymbols_65() const { return ___mSymbols_65; }
	inline int32_t* get_address_of_mSymbols_65() { return &___mSymbols_65; }
	inline void set_mSymbols_65(int32_t value)
	{
		___mSymbols_65 = value;
	}

	inline static int32_t get_offset_of_mEffectDistance_66() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mEffectDistance_66)); }
	inline Vector2_t4282066565  get_mEffectDistance_66() const { return ___mEffectDistance_66; }
	inline Vector2_t4282066565 * get_address_of_mEffectDistance_66() { return &___mEffectDistance_66; }
	inline void set_mEffectDistance_66(Vector2_t4282066565  value)
	{
		___mEffectDistance_66 = value;
	}

	inline static int32_t get_offset_of_mOverflow_67() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mOverflow_67)); }
	inline int32_t get_mOverflow_67() const { return ___mOverflow_67; }
	inline int32_t* get_address_of_mOverflow_67() { return &___mOverflow_67; }
	inline void set_mOverflow_67(int32_t value)
	{
		___mOverflow_67 = value;
	}

	inline static int32_t get_offset_of_mMaterial_68() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mMaterial_68)); }
	inline Material_t3870600107 * get_mMaterial_68() const { return ___mMaterial_68; }
	inline Material_t3870600107 ** get_address_of_mMaterial_68() { return &___mMaterial_68; }
	inline void set_mMaterial_68(Material_t3870600107 * value)
	{
		___mMaterial_68 = value;
		Il2CppCodeGenWriteBarrier(&___mMaterial_68, value);
	}

	inline static int32_t get_offset_of_mApplyGradient_69() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mApplyGradient_69)); }
	inline bool get_mApplyGradient_69() const { return ___mApplyGradient_69; }
	inline bool* get_address_of_mApplyGradient_69() { return &___mApplyGradient_69; }
	inline void set_mApplyGradient_69(bool value)
	{
		___mApplyGradient_69 = value;
	}

	inline static int32_t get_offset_of_mGradientTop_70() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mGradientTop_70)); }
	inline Color_t4194546905  get_mGradientTop_70() const { return ___mGradientTop_70; }
	inline Color_t4194546905 * get_address_of_mGradientTop_70() { return &___mGradientTop_70; }
	inline void set_mGradientTop_70(Color_t4194546905  value)
	{
		___mGradientTop_70 = value;
	}

	inline static int32_t get_offset_of_mGradientBottom_71() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mGradientBottom_71)); }
	inline Color_t4194546905  get_mGradientBottom_71() const { return ___mGradientBottom_71; }
	inline Color_t4194546905 * get_address_of_mGradientBottom_71() { return &___mGradientBottom_71; }
	inline void set_mGradientBottom_71(Color_t4194546905  value)
	{
		___mGradientBottom_71 = value;
	}

	inline static int32_t get_offset_of_mSpacingX_72() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mSpacingX_72)); }
	inline int32_t get_mSpacingX_72() const { return ___mSpacingX_72; }
	inline int32_t* get_address_of_mSpacingX_72() { return &___mSpacingX_72; }
	inline void set_mSpacingX_72(int32_t value)
	{
		___mSpacingX_72 = value;
	}

	inline static int32_t get_offset_of_mSpacingY_73() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mSpacingY_73)); }
	inline int32_t get_mSpacingY_73() const { return ___mSpacingY_73; }
	inline int32_t* get_address_of_mSpacingY_73() { return &___mSpacingY_73; }
	inline void set_mSpacingY_73(int32_t value)
	{
		___mSpacingY_73 = value;
	}

	inline static int32_t get_offset_of_mUseFloatSpacing_74() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mUseFloatSpacing_74)); }
	inline bool get_mUseFloatSpacing_74() const { return ___mUseFloatSpacing_74; }
	inline bool* get_address_of_mUseFloatSpacing_74() { return &___mUseFloatSpacing_74; }
	inline void set_mUseFloatSpacing_74(bool value)
	{
		___mUseFloatSpacing_74 = value;
	}

	inline static int32_t get_offset_of_mFloatSpacingX_75() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mFloatSpacingX_75)); }
	inline float get_mFloatSpacingX_75() const { return ___mFloatSpacingX_75; }
	inline float* get_address_of_mFloatSpacingX_75() { return &___mFloatSpacingX_75; }
	inline void set_mFloatSpacingX_75(float value)
	{
		___mFloatSpacingX_75 = value;
	}

	inline static int32_t get_offset_of_mFloatSpacingY_76() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mFloatSpacingY_76)); }
	inline float get_mFloatSpacingY_76() const { return ___mFloatSpacingY_76; }
	inline float* get_address_of_mFloatSpacingY_76() { return &___mFloatSpacingY_76; }
	inline void set_mFloatSpacingY_76(float value)
	{
		___mFloatSpacingY_76 = value;
	}

	inline static int32_t get_offset_of_mOverflowEllipsis_77() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mOverflowEllipsis_77)); }
	inline bool get_mOverflowEllipsis_77() const { return ___mOverflowEllipsis_77; }
	inline bool* get_address_of_mOverflowEllipsis_77() { return &___mOverflowEllipsis_77; }
	inline void set_mOverflowEllipsis_77(bool value)
	{
		___mOverflowEllipsis_77 = value;
	}

	inline static int32_t get_offset_of_mShrinkToFit_78() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mShrinkToFit_78)); }
	inline bool get_mShrinkToFit_78() const { return ___mShrinkToFit_78; }
	inline bool* get_address_of_mShrinkToFit_78() { return &___mShrinkToFit_78; }
	inline void set_mShrinkToFit_78(bool value)
	{
		___mShrinkToFit_78 = value;
	}

	inline static int32_t get_offset_of_mMaxLineWidth_79() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mMaxLineWidth_79)); }
	inline int32_t get_mMaxLineWidth_79() const { return ___mMaxLineWidth_79; }
	inline int32_t* get_address_of_mMaxLineWidth_79() { return &___mMaxLineWidth_79; }
	inline void set_mMaxLineWidth_79(int32_t value)
	{
		___mMaxLineWidth_79 = value;
	}

	inline static int32_t get_offset_of_mMaxLineHeight_80() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mMaxLineHeight_80)); }
	inline int32_t get_mMaxLineHeight_80() const { return ___mMaxLineHeight_80; }
	inline int32_t* get_address_of_mMaxLineHeight_80() { return &___mMaxLineHeight_80; }
	inline void set_mMaxLineHeight_80(int32_t value)
	{
		___mMaxLineHeight_80 = value;
	}

	inline static int32_t get_offset_of_mLineWidth_81() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mLineWidth_81)); }
	inline float get_mLineWidth_81() const { return ___mLineWidth_81; }
	inline float* get_address_of_mLineWidth_81() { return &___mLineWidth_81; }
	inline void set_mLineWidth_81(float value)
	{
		___mLineWidth_81 = value;
	}

	inline static int32_t get_offset_of_mMultiline_82() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mMultiline_82)); }
	inline bool get_mMultiline_82() const { return ___mMultiline_82; }
	inline bool* get_address_of_mMultiline_82() { return &___mMultiline_82; }
	inline void set_mMultiline_82(bool value)
	{
		___mMultiline_82 = value;
	}

	inline static int32_t get_offset_of_mActiveTTF_83() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mActiveTTF_83)); }
	inline Font_t4241557075 * get_mActiveTTF_83() const { return ___mActiveTTF_83; }
	inline Font_t4241557075 ** get_address_of_mActiveTTF_83() { return &___mActiveTTF_83; }
	inline void set_mActiveTTF_83(Font_t4241557075 * value)
	{
		___mActiveTTF_83 = value;
		Il2CppCodeGenWriteBarrier(&___mActiveTTF_83, value);
	}

	inline static int32_t get_offset_of_mDensity_84() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mDensity_84)); }
	inline float get_mDensity_84() const { return ___mDensity_84; }
	inline float* get_address_of_mDensity_84() { return &___mDensity_84; }
	inline void set_mDensity_84(float value)
	{
		___mDensity_84 = value;
	}

	inline static int32_t get_offset_of_mShouldBeProcessed_85() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mShouldBeProcessed_85)); }
	inline bool get_mShouldBeProcessed_85() const { return ___mShouldBeProcessed_85; }
	inline bool* get_address_of_mShouldBeProcessed_85() { return &___mShouldBeProcessed_85; }
	inline void set_mShouldBeProcessed_85(bool value)
	{
		___mShouldBeProcessed_85 = value;
	}

	inline static int32_t get_offset_of_mProcessedText_86() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mProcessedText_86)); }
	inline String_t* get_mProcessedText_86() const { return ___mProcessedText_86; }
	inline String_t** get_address_of_mProcessedText_86() { return &___mProcessedText_86; }
	inline void set_mProcessedText_86(String_t* value)
	{
		___mProcessedText_86 = value;
		Il2CppCodeGenWriteBarrier(&___mProcessedText_86, value);
	}

	inline static int32_t get_offset_of_mPremultiply_87() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mPremultiply_87)); }
	inline bool get_mPremultiply_87() const { return ___mPremultiply_87; }
	inline bool* get_address_of_mPremultiply_87() { return &___mPremultiply_87; }
	inline void set_mPremultiply_87(bool value)
	{
		___mPremultiply_87 = value;
	}

	inline static int32_t get_offset_of_mCalculatedSize_88() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mCalculatedSize_88)); }
	inline Vector2_t4282066565  get_mCalculatedSize_88() const { return ___mCalculatedSize_88; }
	inline Vector2_t4282066565 * get_address_of_mCalculatedSize_88() { return &___mCalculatedSize_88; }
	inline void set_mCalculatedSize_88(Vector2_t4282066565  value)
	{
		___mCalculatedSize_88 = value;
	}

	inline static int32_t get_offset_of_mScale_89() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mScale_89)); }
	inline float get_mScale_89() const { return ___mScale_89; }
	inline float* get_address_of_mScale_89() { return &___mScale_89; }
	inline void set_mScale_89(float value)
	{
		___mScale_89 = value;
	}

	inline static int32_t get_offset_of_mFinalFontSize_90() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mFinalFontSize_90)); }
	inline int32_t get_mFinalFontSize_90() const { return ___mFinalFontSize_90; }
	inline int32_t* get_address_of_mFinalFontSize_90() { return &___mFinalFontSize_90; }
	inline void set_mFinalFontSize_90(int32_t value)
	{
		___mFinalFontSize_90 = value;
	}

	inline static int32_t get_offset_of_mLastWidth_91() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mLastWidth_91)); }
	inline int32_t get_mLastWidth_91() const { return ___mLastWidth_91; }
	inline int32_t* get_address_of_mLastWidth_91() { return &___mLastWidth_91; }
	inline void set_mLastWidth_91(int32_t value)
	{
		___mLastWidth_91 = value;
	}

	inline static int32_t get_offset_of_mLastHeight_92() { return static_cast<int32_t>(offsetof(UILabel_t291504320, ___mLastHeight_92)); }
	inline int32_t get_mLastHeight_92() const { return ___mLastHeight_92; }
	inline int32_t* get_address_of_mLastHeight_92() { return &___mLastHeight_92; }
	inline void set_mLastHeight_92(int32_t value)
	{
		___mLastHeight_92 = value;
	}
};

struct UILabel_t291504320_StaticFields
{
public:
	// BetterList`1<UILabel> UILabel::mList
	BetterList_1_t1788472332 * ___mList_93;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Int32> UILabel::mFontUsage
	Dictionary_2_t1374193376 * ___mFontUsage_94;
	// System.Collections.Generic.List`1<UIDrawCall> UILabel::mTempDrawcalls
	List_1_t2281459526 * ___mTempDrawcalls_95;
	// System.Boolean UILabel::mTexRebuildAdded
	bool ___mTexRebuildAdded_96;
	// BetterList`1<UnityEngine.Vector3> UILabel::mTempVerts
	BetterList_1_t1484067282 * ___mTempVerts_97;
	// BetterList`1<System.Int32> UILabel::mTempIndices
	BetterList_1_t2650806512 * ___mTempIndices_98;

public:
	inline static int32_t get_offset_of_mList_93() { return static_cast<int32_t>(offsetof(UILabel_t291504320_StaticFields, ___mList_93)); }
	inline BetterList_1_t1788472332 * get_mList_93() const { return ___mList_93; }
	inline BetterList_1_t1788472332 ** get_address_of_mList_93() { return &___mList_93; }
	inline void set_mList_93(BetterList_1_t1788472332 * value)
	{
		___mList_93 = value;
		Il2CppCodeGenWriteBarrier(&___mList_93, value);
	}

	inline static int32_t get_offset_of_mFontUsage_94() { return static_cast<int32_t>(offsetof(UILabel_t291504320_StaticFields, ___mFontUsage_94)); }
	inline Dictionary_2_t1374193376 * get_mFontUsage_94() const { return ___mFontUsage_94; }
	inline Dictionary_2_t1374193376 ** get_address_of_mFontUsage_94() { return &___mFontUsage_94; }
	inline void set_mFontUsage_94(Dictionary_2_t1374193376 * value)
	{
		___mFontUsage_94 = value;
		Il2CppCodeGenWriteBarrier(&___mFontUsage_94, value);
	}

	inline static int32_t get_offset_of_mTempDrawcalls_95() { return static_cast<int32_t>(offsetof(UILabel_t291504320_StaticFields, ___mTempDrawcalls_95)); }
	inline List_1_t2281459526 * get_mTempDrawcalls_95() const { return ___mTempDrawcalls_95; }
	inline List_1_t2281459526 ** get_address_of_mTempDrawcalls_95() { return &___mTempDrawcalls_95; }
	inline void set_mTempDrawcalls_95(List_1_t2281459526 * value)
	{
		___mTempDrawcalls_95 = value;
		Il2CppCodeGenWriteBarrier(&___mTempDrawcalls_95, value);
	}

	inline static int32_t get_offset_of_mTexRebuildAdded_96() { return static_cast<int32_t>(offsetof(UILabel_t291504320_StaticFields, ___mTexRebuildAdded_96)); }
	inline bool get_mTexRebuildAdded_96() const { return ___mTexRebuildAdded_96; }
	inline bool* get_address_of_mTexRebuildAdded_96() { return &___mTexRebuildAdded_96; }
	inline void set_mTexRebuildAdded_96(bool value)
	{
		___mTexRebuildAdded_96 = value;
	}

	inline static int32_t get_offset_of_mTempVerts_97() { return static_cast<int32_t>(offsetof(UILabel_t291504320_StaticFields, ___mTempVerts_97)); }
	inline BetterList_1_t1484067282 * get_mTempVerts_97() const { return ___mTempVerts_97; }
	inline BetterList_1_t1484067282 ** get_address_of_mTempVerts_97() { return &___mTempVerts_97; }
	inline void set_mTempVerts_97(BetterList_1_t1484067282 * value)
	{
		___mTempVerts_97 = value;
		Il2CppCodeGenWriteBarrier(&___mTempVerts_97, value);
	}

	inline static int32_t get_offset_of_mTempIndices_98() { return static_cast<int32_t>(offsetof(UILabel_t291504320_StaticFields, ___mTempIndices_98)); }
	inline BetterList_1_t2650806512 * get_mTempIndices_98() const { return ___mTempIndices_98; }
	inline BetterList_1_t2650806512 ** get_address_of_mTempIndices_98() { return &___mTempIndices_98; }
	inline void set_mTempIndices_98(BetterList_1_t2650806512 * value)
	{
		___mTempIndices_98 = value;
		Il2CppCodeGenWriteBarrier(&___mTempIndices_98, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
