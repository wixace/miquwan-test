﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_FixedArra165589287.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_FixedArr3485577237.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_FixedBit3665369095.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Poly2Tri.DelaunayTriangle
struct  DelaunayTriangle_t2835103587  : public Il2CppObject
{
public:
	// Pathfinding.Poly2Tri.FixedArray3`1<Pathfinding.Poly2Tri.TriangulationPoint> Pathfinding.Poly2Tri.DelaunayTriangle::Points
	FixedArray3_1_t165589287  ___Points_0;
	// Pathfinding.Poly2Tri.FixedArray3`1<Pathfinding.Poly2Tri.DelaunayTriangle> Pathfinding.Poly2Tri.DelaunayTriangle::Neighbors
	FixedArray3_1_t3485577237  ___Neighbors_1;
	// Pathfinding.Poly2Tri.FixedBitArray3 Pathfinding.Poly2Tri.DelaunayTriangle::EdgeIsConstrained
	FixedBitArray3_t3665369095  ___EdgeIsConstrained_2;
	// Pathfinding.Poly2Tri.FixedBitArray3 Pathfinding.Poly2Tri.DelaunayTriangle::EdgeIsDelaunay
	FixedBitArray3_t3665369095  ___EdgeIsDelaunay_3;
	// System.Boolean Pathfinding.Poly2Tri.DelaunayTriangle::<IsInterior>k__BackingField
	bool ___U3CIsInteriorU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_Points_0() { return static_cast<int32_t>(offsetof(DelaunayTriangle_t2835103587, ___Points_0)); }
	inline FixedArray3_1_t165589287  get_Points_0() const { return ___Points_0; }
	inline FixedArray3_1_t165589287 * get_address_of_Points_0() { return &___Points_0; }
	inline void set_Points_0(FixedArray3_1_t165589287  value)
	{
		___Points_0 = value;
	}

	inline static int32_t get_offset_of_Neighbors_1() { return static_cast<int32_t>(offsetof(DelaunayTriangle_t2835103587, ___Neighbors_1)); }
	inline FixedArray3_1_t3485577237  get_Neighbors_1() const { return ___Neighbors_1; }
	inline FixedArray3_1_t3485577237 * get_address_of_Neighbors_1() { return &___Neighbors_1; }
	inline void set_Neighbors_1(FixedArray3_1_t3485577237  value)
	{
		___Neighbors_1 = value;
	}

	inline static int32_t get_offset_of_EdgeIsConstrained_2() { return static_cast<int32_t>(offsetof(DelaunayTriangle_t2835103587, ___EdgeIsConstrained_2)); }
	inline FixedBitArray3_t3665369095  get_EdgeIsConstrained_2() const { return ___EdgeIsConstrained_2; }
	inline FixedBitArray3_t3665369095 * get_address_of_EdgeIsConstrained_2() { return &___EdgeIsConstrained_2; }
	inline void set_EdgeIsConstrained_2(FixedBitArray3_t3665369095  value)
	{
		___EdgeIsConstrained_2 = value;
	}

	inline static int32_t get_offset_of_EdgeIsDelaunay_3() { return static_cast<int32_t>(offsetof(DelaunayTriangle_t2835103587, ___EdgeIsDelaunay_3)); }
	inline FixedBitArray3_t3665369095  get_EdgeIsDelaunay_3() const { return ___EdgeIsDelaunay_3; }
	inline FixedBitArray3_t3665369095 * get_address_of_EdgeIsDelaunay_3() { return &___EdgeIsDelaunay_3; }
	inline void set_EdgeIsDelaunay_3(FixedBitArray3_t3665369095  value)
	{
		___EdgeIsDelaunay_3 = value;
	}

	inline static int32_t get_offset_of_U3CIsInteriorU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DelaunayTriangle_t2835103587, ___U3CIsInteriorU3Ek__BackingField_4)); }
	inline bool get_U3CIsInteriorU3Ek__BackingField_4() const { return ___U3CIsInteriorU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsInteriorU3Ek__BackingField_4() { return &___U3CIsInteriorU3Ek__BackingField_4; }
	inline void set_U3CIsInteriorU3Ek__BackingField_4(bool value)
	{
		___U3CIsInteriorU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
