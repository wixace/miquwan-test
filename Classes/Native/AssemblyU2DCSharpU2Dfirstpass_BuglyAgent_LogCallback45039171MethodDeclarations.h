﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BuglyAgent/LogCallbackDelegate
struct LogCallbackDelegate_t45039171;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_LogType4286006228.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void BuglyAgent/LogCallbackDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void LogCallbackDelegate__ctor_m1543750454 (LogCallbackDelegate_t45039171 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent/LogCallbackDelegate::Invoke(System.String,System.String,UnityEngine.LogType)
extern "C"  void LogCallbackDelegate_Invoke_m2974759347 (LogCallbackDelegate_t45039171 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult BuglyAgent/LogCallbackDelegate::BeginInvoke(System.String,System.String,UnityEngine.LogType,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LogCallbackDelegate_BeginInvoke_m3880897110 (LogCallbackDelegate_t45039171 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent/LogCallbackDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void LogCallbackDelegate_EndInvoke_m586498118 (LogCallbackDelegate_t45039171 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
