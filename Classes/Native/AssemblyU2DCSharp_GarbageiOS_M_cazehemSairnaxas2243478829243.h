﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_cazehemSairnaxas224
struct  M_cazehemSairnaxas224_t3478829243  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_cazehemSairnaxas224::_virhouciWotenou
	int32_t ____virhouciWotenou_0;
	// System.Boolean GarbageiOS.M_cazehemSairnaxas224::_whotelRerqowlal
	bool ____whotelRerqowlal_1;
	// System.Boolean GarbageiOS.M_cazehemSairnaxas224::_corcesur
	bool ____corcesur_2;
	// System.UInt32 GarbageiOS.M_cazehemSairnaxas224::_bainaistowRixear
	uint32_t ____bainaistowRixear_3;
	// System.Boolean GarbageiOS.M_cazehemSairnaxas224::_meseSawear
	bool ____meseSawear_4;
	// System.UInt32 GarbageiOS.M_cazehemSairnaxas224::_ditrear
	uint32_t ____ditrear_5;
	// System.UInt32 GarbageiOS.M_cazehemSairnaxas224::_joosinar
	uint32_t ____joosinar_6;
	// System.Boolean GarbageiOS.M_cazehemSairnaxas224::_ceremallLereardre
	bool ____ceremallLereardre_7;
	// System.UInt32 GarbageiOS.M_cazehemSairnaxas224::_koonur
	uint32_t ____koonur_8;
	// System.Single GarbageiOS.M_cazehemSairnaxas224::_bearcay
	float ____bearcay_9;
	// System.Single GarbageiOS.M_cazehemSairnaxas224::_cordufe
	float ____cordufe_10;
	// System.Boolean GarbageiOS.M_cazehemSairnaxas224::_reekay
	bool ____reekay_11;
	// System.Single GarbageiOS.M_cazehemSairnaxas224::_yoheaLahu
	float ____yoheaLahu_12;
	// System.Boolean GarbageiOS.M_cazehemSairnaxas224::_daywhar
	bool ____daywhar_13;
	// System.String GarbageiOS.M_cazehemSairnaxas224::_kekai
	String_t* ____kekai_14;

public:
	inline static int32_t get_offset_of__virhouciWotenou_0() { return static_cast<int32_t>(offsetof(M_cazehemSairnaxas224_t3478829243, ____virhouciWotenou_0)); }
	inline int32_t get__virhouciWotenou_0() const { return ____virhouciWotenou_0; }
	inline int32_t* get_address_of__virhouciWotenou_0() { return &____virhouciWotenou_0; }
	inline void set__virhouciWotenou_0(int32_t value)
	{
		____virhouciWotenou_0 = value;
	}

	inline static int32_t get_offset_of__whotelRerqowlal_1() { return static_cast<int32_t>(offsetof(M_cazehemSairnaxas224_t3478829243, ____whotelRerqowlal_1)); }
	inline bool get__whotelRerqowlal_1() const { return ____whotelRerqowlal_1; }
	inline bool* get_address_of__whotelRerqowlal_1() { return &____whotelRerqowlal_1; }
	inline void set__whotelRerqowlal_1(bool value)
	{
		____whotelRerqowlal_1 = value;
	}

	inline static int32_t get_offset_of__corcesur_2() { return static_cast<int32_t>(offsetof(M_cazehemSairnaxas224_t3478829243, ____corcesur_2)); }
	inline bool get__corcesur_2() const { return ____corcesur_2; }
	inline bool* get_address_of__corcesur_2() { return &____corcesur_2; }
	inline void set__corcesur_2(bool value)
	{
		____corcesur_2 = value;
	}

	inline static int32_t get_offset_of__bainaistowRixear_3() { return static_cast<int32_t>(offsetof(M_cazehemSairnaxas224_t3478829243, ____bainaistowRixear_3)); }
	inline uint32_t get__bainaistowRixear_3() const { return ____bainaistowRixear_3; }
	inline uint32_t* get_address_of__bainaistowRixear_3() { return &____bainaistowRixear_3; }
	inline void set__bainaistowRixear_3(uint32_t value)
	{
		____bainaistowRixear_3 = value;
	}

	inline static int32_t get_offset_of__meseSawear_4() { return static_cast<int32_t>(offsetof(M_cazehemSairnaxas224_t3478829243, ____meseSawear_4)); }
	inline bool get__meseSawear_4() const { return ____meseSawear_4; }
	inline bool* get_address_of__meseSawear_4() { return &____meseSawear_4; }
	inline void set__meseSawear_4(bool value)
	{
		____meseSawear_4 = value;
	}

	inline static int32_t get_offset_of__ditrear_5() { return static_cast<int32_t>(offsetof(M_cazehemSairnaxas224_t3478829243, ____ditrear_5)); }
	inline uint32_t get__ditrear_5() const { return ____ditrear_5; }
	inline uint32_t* get_address_of__ditrear_5() { return &____ditrear_5; }
	inline void set__ditrear_5(uint32_t value)
	{
		____ditrear_5 = value;
	}

	inline static int32_t get_offset_of__joosinar_6() { return static_cast<int32_t>(offsetof(M_cazehemSairnaxas224_t3478829243, ____joosinar_6)); }
	inline uint32_t get__joosinar_6() const { return ____joosinar_6; }
	inline uint32_t* get_address_of__joosinar_6() { return &____joosinar_6; }
	inline void set__joosinar_6(uint32_t value)
	{
		____joosinar_6 = value;
	}

	inline static int32_t get_offset_of__ceremallLereardre_7() { return static_cast<int32_t>(offsetof(M_cazehemSairnaxas224_t3478829243, ____ceremallLereardre_7)); }
	inline bool get__ceremallLereardre_7() const { return ____ceremallLereardre_7; }
	inline bool* get_address_of__ceremallLereardre_7() { return &____ceremallLereardre_7; }
	inline void set__ceremallLereardre_7(bool value)
	{
		____ceremallLereardre_7 = value;
	}

	inline static int32_t get_offset_of__koonur_8() { return static_cast<int32_t>(offsetof(M_cazehemSairnaxas224_t3478829243, ____koonur_8)); }
	inline uint32_t get__koonur_8() const { return ____koonur_8; }
	inline uint32_t* get_address_of__koonur_8() { return &____koonur_8; }
	inline void set__koonur_8(uint32_t value)
	{
		____koonur_8 = value;
	}

	inline static int32_t get_offset_of__bearcay_9() { return static_cast<int32_t>(offsetof(M_cazehemSairnaxas224_t3478829243, ____bearcay_9)); }
	inline float get__bearcay_9() const { return ____bearcay_9; }
	inline float* get_address_of__bearcay_9() { return &____bearcay_9; }
	inline void set__bearcay_9(float value)
	{
		____bearcay_9 = value;
	}

	inline static int32_t get_offset_of__cordufe_10() { return static_cast<int32_t>(offsetof(M_cazehemSairnaxas224_t3478829243, ____cordufe_10)); }
	inline float get__cordufe_10() const { return ____cordufe_10; }
	inline float* get_address_of__cordufe_10() { return &____cordufe_10; }
	inline void set__cordufe_10(float value)
	{
		____cordufe_10 = value;
	}

	inline static int32_t get_offset_of__reekay_11() { return static_cast<int32_t>(offsetof(M_cazehemSairnaxas224_t3478829243, ____reekay_11)); }
	inline bool get__reekay_11() const { return ____reekay_11; }
	inline bool* get_address_of__reekay_11() { return &____reekay_11; }
	inline void set__reekay_11(bool value)
	{
		____reekay_11 = value;
	}

	inline static int32_t get_offset_of__yoheaLahu_12() { return static_cast<int32_t>(offsetof(M_cazehemSairnaxas224_t3478829243, ____yoheaLahu_12)); }
	inline float get__yoheaLahu_12() const { return ____yoheaLahu_12; }
	inline float* get_address_of__yoheaLahu_12() { return &____yoheaLahu_12; }
	inline void set__yoheaLahu_12(float value)
	{
		____yoheaLahu_12 = value;
	}

	inline static int32_t get_offset_of__daywhar_13() { return static_cast<int32_t>(offsetof(M_cazehemSairnaxas224_t3478829243, ____daywhar_13)); }
	inline bool get__daywhar_13() const { return ____daywhar_13; }
	inline bool* get_address_of__daywhar_13() { return &____daywhar_13; }
	inline void set__daywhar_13(bool value)
	{
		____daywhar_13 = value;
	}

	inline static int32_t get_offset_of__kekai_14() { return static_cast<int32_t>(offsetof(M_cazehemSairnaxas224_t3478829243, ____kekai_14)); }
	inline String_t* get__kekai_14() const { return ____kekai_14; }
	inline String_t** get_address_of__kekai_14() { return &____kekai_14; }
	inline void set__kekai_14(String_t* value)
	{
		____kekai_14 = value;
		Il2CppCodeGenWriteBarrier(&____kekai_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
