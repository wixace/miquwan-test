﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua.SDK.LvUpInfo
struct LvUpInfo_t411687177;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"

// System.Void Mihua.SDK.LvUpInfo::.ctor()
extern "C"  void LvUpInfo__ctor_m1405024574 (LvUpInfo_t411687177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.LvUpInfo Mihua.SDK.LvUpInfo::Parse(System.Object[])
extern "C"  LvUpInfo_t411687177 * LvUpInfo_Parse_m3696017827 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
