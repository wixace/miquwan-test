﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct U3CSortU3Ec__Iterator21_t4047429936;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t3856534026;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977.h"

// System.Void System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void U3CSortU3Ec__Iterator21__ctor_m2646069580_gshared (U3CSortU3Ec__Iterator21_t4047429936 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21__ctor_m2646069580(__this, method) ((  void (*) (U3CSortU3Ec__Iterator21_t4047429936 *, const MethodInfo*))U3CSortU3Ec__Iterator21__ctor_m2646069580_gshared)(__this, method)
// TElement System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerator<TElement>.get_Current()
extern "C"  KeyValuePair_2_t1944668977  U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumeratorU3CTElementU3E_get_Current_m527544381_gshared (U3CSortU3Ec__Iterator21_t4047429936 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumeratorU3CTElementU3E_get_Current_m527544381(__this, method) ((  KeyValuePair_2_t1944668977  (*) (U3CSortU3Ec__Iterator21_t4047429936 *, const MethodInfo*))U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumeratorU3CTElementU3E_get_Current_m527544381_gshared)(__this, method)
// System.Object System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSortU3Ec__Iterator21_System_Collections_IEnumerator_get_Current_m796520858_gshared (U3CSortU3Ec__Iterator21_t4047429936 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_System_Collections_IEnumerator_get_Current_m796520858(__this, method) ((  Il2CppObject * (*) (U3CSortU3Ec__Iterator21_t4047429936 *, const MethodInfo*))U3CSortU3Ec__Iterator21_System_Collections_IEnumerator_get_Current_m796520858_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CSortU3Ec__Iterator21_System_Collections_IEnumerable_GetEnumerator_m4102625461_gshared (U3CSortU3Ec__Iterator21_t4047429936 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_System_Collections_IEnumerable_GetEnumerator_m4102625461(__this, method) ((  Il2CppObject * (*) (U3CSortU3Ec__Iterator21_t4047429936 *, const MethodInfo*))U3CSortU3Ec__Iterator21_System_Collections_IEnumerable_GetEnumerator_m4102625461_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TElement> System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerable<TElement>.GetEnumerator()
extern "C"  Il2CppObject* U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumerableU3CTElementU3E_GetEnumerator_m1423616780_gshared (U3CSortU3Ec__Iterator21_t4047429936 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumerableU3CTElementU3E_GetEnumerator_m1423616780(__this, method) ((  Il2CppObject* (*) (U3CSortU3Ec__Iterator21_t4047429936 *, const MethodInfo*))U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumerableU3CTElementU3E_GetEnumerator_m1423616780_gshared)(__this, method)
// System.Boolean System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool U3CSortU3Ec__Iterator21_MoveNext_m3984028488_gshared (U3CSortU3Ec__Iterator21_t4047429936 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_MoveNext_m3984028488(__this, method) ((  bool (*) (U3CSortU3Ec__Iterator21_t4047429936 *, const MethodInfo*))U3CSortU3Ec__Iterator21_MoveNext_m3984028488_gshared)(__this, method)
// System.Void System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void U3CSortU3Ec__Iterator21_Dispose_m150510217_gshared (U3CSortU3Ec__Iterator21_t4047429936 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_Dispose_m150510217(__this, method) ((  void (*) (U3CSortU3Ec__Iterator21_t4047429936 *, const MethodInfo*))U3CSortU3Ec__Iterator21_Dispose_m150510217_gshared)(__this, method)
// System.Void System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Reset()
extern "C"  void U3CSortU3Ec__Iterator21_Reset_m292502521_gshared (U3CSortU3Ec__Iterator21_t4047429936 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_Reset_m292502521(__this, method) ((  void (*) (U3CSortU3Ec__Iterator21_t4047429936 *, const MethodInfo*))U3CSortU3Ec__Iterator21_Reset_m292502521_gshared)(__this, method)
