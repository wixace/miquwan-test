﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// JSFileLoader
struct JSFileLoader_t1433707224;
// JSMgr/OnInitJSEngine
struct OnInitJSEngine_t2416054170;
// JSMgr/JS_CS_Rel
struct JS_CS_Rel_t3554103776;
// JSCache/TypeInfo
struct TypeInfo_t3198813374;
// System.Collections.Generic.Dictionary`2<System.Int32,JSMgr/JS_CS_Rel>
struct Dictionary_2_t3551367015;
// System.Delegate
struct Delegate_t3310234105;
// JSApi/JSErrorReporter
struct JSErrorReporter_t2324290114;
// JSApi/CSEntry
struct CSEntry_t1794636260;
// JSApi/JSNative
struct JSNative_t3654534910;
// JSApi/OnObjCollected
struct OnObjCollected_t2008609135;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSFileLoader1433707224.h"
#include "AssemblyU2DCSharp_JSMgr_OnInitJSEngine2416054170.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSMgr_JS_CS_Rel3554103776.h"
#include "AssemblyU2DCSharp_JSCache_TypeInfo3198813374.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "AssemblyU2DCSharp_JSApi_JSErrorReporter2324290114.h"
#include "AssemblyU2DCSharp_JSApi_CSEntry1794636260.h"
#include "AssemblyU2DCSharp_JSApi_JSNative3654534910.h"
#include "AssemblyU2DCSharp_JSApi_OnObjCollected2008609135.h"
#include "mscorlib_System_Type2863145774.h"

// System.Void JSMgr::.cctor()
extern "C"  void JSMgr__cctor_m303455473 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSMgr::errorReporter(System.IntPtr,System.String,System.IntPtr)
extern "C"  int32_t JSMgr_errorReporter_m3068528493 (Il2CppObject * __this /* static, unused */, IntPtr_t ___cx0, String_t* ___message1, IntPtr_t ___report2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" int32_t DEFAULT_CALL ReversePInvokeWrapper_JSMgr_errorReporter_m3068528493(intptr_t ___cx0, char* ___message1, intptr_t ___report2);
// System.Boolean JSMgr::RefCallStaticMethod(System.String,System.String)
extern "C"  bool JSMgr_RefCallStaticMethod_m929653136 (Il2CppObject * __this /* static, unused */, String_t* ___className0, String_t* ___methodName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSMgr::RefGetStaticField(System.String,System.String)
extern "C"  Il2CppObject * JSMgr_RefGetStaticField_m678874950 (Il2CppObject * __this /* static, unused */, String_t* ___className0, String_t* ___fieldName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSMgr::IsJSIDOld(System.Int32)
extern "C"  bool JSMgr_IsJSIDOld_m2940205080 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSFileLoader JSMgr::get_jsLoader()
extern "C"  JSFileLoader_t1433707224 * JSMgr_get_jsLoader_m1433978728 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSMgr::set_jsLoader(JSFileLoader)
extern "C"  void JSMgr_set_jsLoader_m3726243913 (Il2CppObject * __this /* static, unused */, JSFileLoader_t1433707224 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSMgr::InitJSEngine(JSFileLoader,JSMgr/OnInitJSEngine)
extern "C"  bool JSMgr_InitJSEngine_m4117332821 (Il2CppObject * __this /* static, unused */, JSFileLoader_t1433707224 * ___jsLoader0, OnInitJSEngine_t2416054170 * ___onInitJSEngine1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSMgr::get_IsShutDown()
extern "C"  bool JSMgr_get_IsShutDown_m2281374109 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSMgr::ShutdownJSEngine()
extern "C"  void JSMgr_ShutdownJSEngine_m1140147721 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSMgr::getJSFullName(System.String)
extern "C"  String_t* JSMgr_getJSFullName_m481870508 (Il2CppObject * __this /* static, unused */, String_t* ___shortName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSMgr::CSEntry(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t JSMgr_CSEntry_m758593083 (Il2CppObject * __this /* static, unused */, int32_t ___iOP0, int32_t ___slot1, int32_t ___index2, int32_t ___isStatic3, int32_t ___argc4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" int32_t DEFAULT_CALL ReversePInvokeWrapper_JSMgr_CSEntry_m758593083(int32_t ___op0, int32_t ___slot1, int32_t ___index2, int32_t ___bStatic3, int32_t ___argc4);
// System.Void JSMgr::UpdateEvaluate()
extern "C"  void JSMgr_UpdateEvaluate_m978442954 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSMgr::evaluate(System.String)
extern "C"  bool JSMgr_evaluate_m2607946261 (Il2CppObject * __this /* static, unused */, String_t* ___jsScriptName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSMgr::evaluateExec(System.String)
extern "C"  void JSMgr_evaluateExec_m1647851920 (Il2CppObject * __this /* static, unused */, String_t* ___jsScriptName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSMgr::require(System.IntPtr,System.UInt32,System.IntPtr)
extern "C"  bool JSMgr_require_m3358638179 (Il2CppObject * __this /* static, unused */, IntPtr_t ___cx0, uint32_t ___argc1, IntPtr_t ___vp2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" int32_t DEFAULT_CALL ReversePInvokeWrapper_JSMgr_require_m3358638179(intptr_t ___cx0, uint32_t ___argc1, intptr_t ___vp2);
// System.Boolean JSMgr::print(System.IntPtr,System.UInt32,System.IntPtr)
extern "C"  bool JSMgr_print_m3573834459 (Il2CppObject * __this /* static, unused */, IntPtr_t ___cx0, uint32_t ___argc1, IntPtr_t ___vp2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" int32_t DEFAULT_CALL ReversePInvokeWrapper_JSMgr_print_m3573834459(intptr_t ___cx0, uint32_t ___argc1, intptr_t ___vp2);
// System.Void JSMgr::addJSCSRel(System.Int32,System.Object)
extern "C"  void JSMgr_addJSCSRel_m3830796454 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSMgr::addJSCSRel(System.Int32,System.Object,System.Boolean)
extern "C"  void JSMgr_addJSCSRel_m741794327 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, bool ___weakReference2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSMgr::AddDictionary1(System.Int32,JSMgr/JS_CS_Rel)
extern "C"  void JSMgr_AddDictionary1_m2350342111 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, JS_CS_Rel_t3554103776 * ___Rel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSMgr::AddDictionary2(JSMgr/JS_CS_Rel)
extern "C"  void JSMgr_AddDictionary2_m3720747055 (Il2CppObject * __this /* static, unused */, JS_CS_Rel_t3554103776 * ___Rel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSMgr::removeJSCSRel(System.Int32,System.Int32)
extern "C"  void JSMgr_removeJSCSRel_m3355644688 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___round1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSMgr::getCSObj(System.Int32)
extern "C"  Il2CppObject * JSMgr_getCSObj_m1056229301 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSMgr::getJSObj(System.Object,JSCache/TypeInfo)
extern "C"  int32_t JSMgr_getJSObj_m3188538854 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___csObj0, TypeInfo_t3198813374 * ___typeInfo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSMgr::changeJSObj(System.Int32,System.Object)
extern "C"  void JSMgr_changeJSObj_m2172050135 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSMgr::MoveJSCSRel2Old()
extern "C"  void JSMgr_MoveJSCSRel2Old_m2426459550 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSMgr::onObjCollected(System.Int32)
extern "C"  void JSMgr_onObjCollected_m1422890442 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_JSMgr_onObjCollected_m1422890442(int32_t ___id0);
// System.Void JSMgr::GetDictCount(System.Int32&,System.Int32&)
extern "C"  void JSMgr_GetDictCount_m2882055237 (Il2CppObject * __this /* static, unused */, int32_t* ___countDict10, int32_t* ___countDict21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,JSMgr/JS_CS_Rel> JSMgr::GetDict1()
extern "C"  Dictionary_2_t3551367015 * JSMgr_GetDict1_m1926198064 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSMgr::addJSFunCSDelegateRel(System.Int32,System.Delegate)
extern "C"  void JSMgr_addJSFunCSDelegateRel_m3394436802 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, Delegate_t3310234105 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSMgr::removeJSFunCSDelegateRel(System.Int32)
extern "C"  void JSMgr_removeJSFunCSDelegateRel_m1751380427 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSMgr::getFunIDByDelegate(System.Delegate)
extern "C"  int32_t JSMgr_getFunIDByDelegate_m441743028 (Il2CppObject * __this /* static, unused */, Delegate_t3310234105 * ___del0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSMgr::getJSFunCSDelegateCount()
extern "C"  String_t* JSMgr_getJSFunCSDelegateCount_m2990171383 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSMgr::ilo_getErroReportFileNameS1(System.IntPtr)
extern "C"  String_t* JSMgr_ilo_getErroReportFileNameS1_m795877297 (Il2CppObject * __this /* static, unused */, IntPtr_t ___report0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSMgr::ilo_getErroReportLineNo2(System.IntPtr)
extern "C"  int32_t JSMgr_ilo_getErroReportLineNo2_m602957022 (Il2CppObject * __this /* static, unused */, IntPtr_t ___report0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSMgr::ilo_InitJSEngine3(JSApi/JSErrorReporter,JSApi/CSEntry,JSApi/JSNative,JSApi/OnObjCollected,JSApi/JSNative)
extern "C"  int32_t JSMgr_ilo_InitJSEngine3_m2879487022 (Il2CppObject * __this /* static, unused */, JSErrorReporter_t2324290114 * ___er0, CSEntry_t1794636260 * ___csEntry1, JSNative_t3654534910 * ___req2, OnObjCollected_t2008609135 * ___onObjCollected3, JSNative_t3654534910 * ___print4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSMgr::ilo_getValueMapStartIndex4()
extern "C"  int32_t JSMgr_ilo_getValueMapStartIndex4_m1937367568 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSMgr::ilo_Invoke5(JSMgr/OnInitJSEngine,System.Boolean)
extern "C"  void JSMgr_ilo_Invoke5_m1028254419 (Il2CppObject * __this /* static, unused */, OnInitJSEngine_t2416054170 * ____this0, bool ___bSuccess1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSMgr::ilo_ShutdownJSEngine6(System.Int32)
extern "C"  void JSMgr_ilo_ShutdownJSEngine6_m3653471597 (Il2CppObject * __this /* static, unused */, int32_t ___bCleanup0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSMgr::ilo_getJSFullName7(System.String)
extern "C"  String_t* JSMgr_ilo_getJSFullName7_m1987433868 (Il2CppObject * __this /* static, unused */, String_t* ___shortName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSMgr::ilo_get_jscDir8()
extern "C"  String_t* JSMgr_ilo_get_jscDir8_m3339091910 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSMgr::ilo_getArgStringS9(System.IntPtr,System.Int32)
extern "C"  String_t* JSMgr_ilo_getArgStringS9_m3420690226 (Il2CppObject * __this /* static, unused */, IntPtr_t ___vp0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSMgr::ilo_evaluate10(System.String)
extern "C"  bool JSMgr_ilo_evaluate10_m510072771 (Il2CppObject * __this /* static, unused */, String_t* ___jsScriptName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSMgr::ilo_addJSCSRel11(System.Int32,System.Object,System.Boolean)
extern "C"  void JSMgr_ilo_addJSCSRel11_m4106879332 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, bool ___weakReference2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSMgr::ilo_AddDictionary112(System.Int32,JSMgr/JS_CS_Rel)
extern "C"  void JSMgr_ilo_AddDictionary112_m2220784499 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, JS_CS_Rel_t3554103776 * ___Rel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSCache/TypeInfo JSMgr::ilo_GetTypeInfo13(System.Type)
extern "C"  TypeInfo_t3198813374 * JSMgr_ilo_GetTypeInfo13_m3479406419 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSMgr::ilo_get_IsValueType14(JSCache/TypeInfo)
extern "C"  bool JSMgr_ilo_get_IsValueType14_m2515240248 (Il2CppObject * __this /* static, unused */, TypeInfo_t3198813374 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSMgr::ilo_get_IsClass15(JSCache/TypeInfo)
extern "C"  bool JSMgr_ilo_get_IsClass15_m2118213414 (Il2CppObject * __this /* static, unused */, TypeInfo_t3198813374 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSMgr::ilo_AddDictionary216(JSMgr/JS_CS_Rel)
extern "C"  void JSMgr_ilo_AddDictionary216_m2802661591 (Il2CppObject * __this /* static, unused */, JS_CS_Rel_t3554103776 * ___Rel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSMgr::ilo_IsJSIDOld17(System.Int32)
extern "C"  bool JSMgr_ilo_IsJSIDOld17_m4007804139 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSMgr::ilo_removeJSCSRel18(System.Int32,System.Int32)
extern "C"  void JSMgr_ilo_removeJSCSRel18_m2446543036 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___round1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
