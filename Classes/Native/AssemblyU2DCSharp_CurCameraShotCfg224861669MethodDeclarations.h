﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CurCameraShotCfg
struct CurCameraShotCfg_t224861669;
// ProtoBuf.IExtension
struct IExtension_t1606339106;
// System.String
struct String_t;
// System.Collections.Generic.List`1<CameraShotNodeCfg>
struct List_1_t650959091;
// System.Collections.Generic.List`1<CameraShotHeroOrNpcCfg>
struct List_1_t4076138479;
// System.Collections.Generic.List`1<CameraShotStoryCfg>
struct List_1_t1204171518;
// System.Collections.Generic.List`1<CameraShotGameSpeedCfg>
struct List_1_t1676810494;
// System.Collections.Generic.List`1<CameraShotActionCfg>
struct List_1_t3362023423;
// System.Collections.Generic.List`1<CameraShotSkillCfg>
struct List_1_t1628869666;
// System.Collections.Generic.List`1<CameraShotMoveCfg>
struct List_1_t4075046084;
// System.Collections.Generic.List`1<CameraShotCircleCfg>
struct List_1_t3959188261;
// System.Collections.Generic.List`1<CameraShotEffectCfg>
struct List_1_t1695066820;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void CurCameraShotCfg::.ctor()
extern "C"  void CurCameraShotCfg__ctor_m3595622998 (CurCameraShotCfg_t224861669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension CurCameraShotCfg::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * CurCameraShotCfg_ProtoBuf_IExtensible_GetExtensionObject_m1659184166 (CurCameraShotCfg_t224861669 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CurCameraShotCfg::get_id()
extern "C"  int32_t CurCameraShotCfg_get_id_m4139827968 (CurCameraShotCfg_t224861669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurCameraShotCfg::set_id(System.Int32)
extern "C"  void CurCameraShotCfg_set_id_m842063159 (CurCameraShotCfg_t224861669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CurCameraShotCfg::get_storyId()
extern "C"  int32_t CurCameraShotCfg_get_storyId_m2050505933 (CurCameraShotCfg_t224861669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurCameraShotCfg::set_storyId(System.Int32)
extern "C"  void CurCameraShotCfg_set_storyId_m4168245624 (CurCameraShotCfg_t224861669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CurCameraShotCfg::get_funType()
extern "C"  int32_t CurCameraShotCfg_get_funType_m4070172406 (CurCameraShotCfg_t224861669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurCameraShotCfg::set_funType(System.Int32)
extern "C"  void CurCameraShotCfg_set_funType_m480319009 (CurCameraShotCfg_t224861669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CurCameraShotCfg::get_withId()
extern "C"  int32_t CurCameraShotCfg_get_withId_m2395054822 (CurCameraShotCfg_t224861669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurCameraShotCfg::set_withId(System.Int32)
extern "C"  void CurCameraShotCfg_set_withId_m2838114589 (CurCameraShotCfg_t224861669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CurCameraShotCfg::get_remark()
extern "C"  String_t* CurCameraShotCfg_get_remark_m3956410234 (CurCameraShotCfg_t224861669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurCameraShotCfg::set_remark(System.String)
extern "C"  void CurCameraShotCfg_set_remark_m3495705559 (CurCameraShotCfg_t224861669 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CurCameraShotCfg::get_preId()
extern "C"  int32_t CurCameraShotCfg_get_preId_m2004842107 (CurCameraShotCfg_t224861669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurCameraShotCfg::set_preId(System.Int32)
extern "C"  void CurCameraShotCfg_set_preId_m2816439462 (CurCameraShotCfg_t224861669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CurCameraShotCfg::get_triggerCondition()
extern "C"  int32_t CurCameraShotCfg_get_triggerCondition_m1071711016 (CurCameraShotCfg_t224861669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurCameraShotCfg::set_triggerCondition(System.Int32)
extern "C"  void CurCameraShotCfg_set_triggerCondition_m1051498207 (CurCameraShotCfg_t224861669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CurCameraShotCfg::get_triggerConditionHeroOrNpcId()
extern "C"  int32_t CurCameraShotCfg_get_triggerConditionHeroOrNpcId_m3223518969 (CurCameraShotCfg_t224861669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurCameraShotCfg::set_triggerConditionHeroOrNpcId(System.Int32)
extern "C"  void CurCameraShotCfg_set_triggerConditionHeroOrNpcId_m2041532580 (CurCameraShotCfg_t224861669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CurCameraShotCfg::get_triggerConditionWave()
extern "C"  int32_t CurCameraShotCfg_get_triggerConditionWave_m3881516161 (CurCameraShotCfg_t224861669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurCameraShotCfg::set_triggerConditionWave(System.Int32)
extern "C"  void CurCameraShotCfg_set_triggerConditionWave_m277278648 (CurCameraShotCfg_t224861669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CurCameraShotCfg::get_isLockNpc()
extern "C"  int32_t CurCameraShotCfg_get_isLockNpc_m940334857 (CurCameraShotCfg_t224861669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurCameraShotCfg::set_isLockNpc(System.Int32)
extern "C"  void CurCameraShotCfg_set_isLockNpc_m3521332 (CurCameraShotCfg_t224861669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CurCameraShotCfg::get_isCloseFightCtrlUpdate()
extern "C"  int32_t CurCameraShotCfg_get_isCloseFightCtrlUpdate_m2189008795 (CurCameraShotCfg_t224861669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurCameraShotCfg::set_isCloseFightCtrlUpdate(System.Int32)
extern "C"  void CurCameraShotCfg_set_isCloseFightCtrlUpdate_m3051829842 (CurCameraShotCfg_t224861669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CurCameraShotCfg::get_isStopCameraShot()
extern "C"  int32_t CurCameraShotCfg_get_isStopCameraShot_m1399229456 (CurCameraShotCfg_t224861669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurCameraShotCfg::set_isStopCameraShot(System.Int32)
extern "C"  void CurCameraShotCfg_set_isStopCameraShot_m4108583879 (CurCameraShotCfg_t224861669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CurCameraShotCfg::get_clearAniShow()
extern "C"  int32_t CurCameraShotCfg_get_clearAniShow_m2717787281 (CurCameraShotCfg_t224861669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurCameraShotCfg::set_clearAniShow(System.Int32)
extern "C"  void CurCameraShotCfg_set_clearAniShow_m3402040776 (CurCameraShotCfg_t224861669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CameraShotNodeCfg> CurCameraShotCfg::get_cameraShotNode()
extern "C"  List_1_t650959091 * CurCameraShotCfg_get_cameraShotNode_m3971287388 (CurCameraShotCfg_t224861669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CameraShotHeroOrNpcCfg> CurCameraShotCfg::get_hero()
extern "C"  List_1_t4076138479 * CurCameraShotCfg_get_hero_m3152815747 (CurCameraShotCfg_t224861669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CameraShotHeroOrNpcCfg> CurCameraShotCfg::get_monster()
extern "C"  List_1_t4076138479 * CurCameraShotCfg_get_monster_m3921541459 (CurCameraShotCfg_t224861669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CameraShotStoryCfg> CurCameraShotCfg::get_story()
extern "C"  List_1_t1204171518 * CurCameraShotCfg_get_story_m627639359 (CurCameraShotCfg_t224861669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CameraShotGameSpeedCfg> CurCameraShotCfg::get_gameSpeed()
extern "C"  List_1_t1676810494 * CurCameraShotCfg_get_gameSpeed_m3427485887 (CurCameraShotCfg_t224861669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CameraShotActionCfg> CurCameraShotCfg::get_action()
extern "C"  List_1_t3362023423 * CurCameraShotCfg_get_action_m736519997 (CurCameraShotCfg_t224861669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CameraShotSkillCfg> CurCameraShotCfg::get_skill()
extern "C"  List_1_t1628869666 * CurCameraShotCfg_get_skill_m1096968439 (CurCameraShotCfg_t224861669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CameraShotMoveCfg> CurCameraShotCfg::get_move()
extern "C"  List_1_t4075046084 * CurCameraShotCfg_get_move_m135319389 (CurCameraShotCfg_t224861669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CameraShotCircleCfg> CurCameraShotCfg::get_circle()
extern "C"  List_1_t3959188261 * CurCameraShotCfg_get_circle_m773870205 (CurCameraShotCfg_t224861669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CameraShotEffectCfg> CurCameraShotCfg::get_effect()
extern "C"  List_1_t1695066820 * CurCameraShotCfg_get_effect_m3558614685 (CurCameraShotCfg_t224861669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
