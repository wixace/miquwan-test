﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<Pathfinding.AdvancedSmooth/Turn>
struct EqualityComparer_1_t3867999555;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.AdvancedSmooth/Turn>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m691292561_gshared (EqualityComparer_1_t3867999555 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m691292561(__this, method) ((  void (*) (EqualityComparer_1_t3867999555 *, const MethodInfo*))EqualityComparer_1__ctor_m691292561_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.AdvancedSmooth/Turn>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3768103996_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m3768103996(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m3768103996_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m957448810_gshared (EqualityComparer_1_t3867999555 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m957448810(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t3867999555 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m957448810_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1415742560_gshared (EqualityComparer_1_t3867999555 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1415742560(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t3867999555 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1415742560_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Pathfinding.AdvancedSmooth/Turn>::get_Default()
extern "C"  EqualityComparer_1_t3867999555 * EqualityComparer_1_get_Default_m1889070203_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m1889070203(__this /* static, unused */, method) ((  EqualityComparer_1_t3867999555 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m1889070203_gshared)(__this /* static, unused */, method)
