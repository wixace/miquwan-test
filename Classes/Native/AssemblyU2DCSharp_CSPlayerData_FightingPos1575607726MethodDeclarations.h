﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSPlayerData/FightingPos
struct FightingPos_t1575607726;

#include "codegen/il2cpp-codegen.h"

// System.Void CSPlayerData/FightingPos::.ctor()
extern "C"  void FightingPos__ctor_m705377453 (FightingPos_t1575607726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
