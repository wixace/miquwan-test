﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginMoHe
struct PluginMoHe_t1606231090;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// VersionInfo
struct VersionInfo_t2356638086;
// VersionMgr
struct VersionMgr_t1322950208;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// System.Action
struct Action_t3771233898;
// SceneMgr
struct SceneMgr_t3584105996;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginMoHe1606231090.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"

// System.Void PluginMoHe::.ctor()
extern "C"  void PluginMoHe__ctor_m2867793065 (PluginMoHe_t1606231090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::Init()
extern "C"  void PluginMoHe_Init_m459688075 (PluginMoHe_t1606231090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginMoHe::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginMoHe_ReqSDKHttpLogin_m3074404926 (PluginMoHe_t1606231090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginMoHe::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginMoHe_IsLoginSuccess_m193847690 (PluginMoHe_t1606231090 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::OpenUserLogin()
extern "C"  void PluginMoHe_OpenUserLogin_m2617865723 (PluginMoHe_t1606231090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::UserPay(CEvent.ZEvent)
extern "C"  void PluginMoHe_UserPay_m277052695 (PluginMoHe_t1606231090 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::SignCallBack(System.Boolean,System.String)
extern "C"  void PluginMoHe_SignCallBack_m1178878512 (PluginMoHe_t1606231090 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginMoHe::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginMoHe_BuildOrderParam2WWWForm_m3053572824 (PluginMoHe_t1606231090 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::EnterGame(CEvent.ZEvent)
extern "C"  void PluginMoHe_EnterGame_m2176214250 (PluginMoHe_t1606231090 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginMoHe_RoleUpgrade_m28284174 (PluginMoHe_t1606231090 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::OnLoginSuccess(System.String)
extern "C"  void PluginMoHe_OnLoginSuccess_m943337198 (PluginMoHe_t1606231090 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::SceneLoad(CEvent.ZEvent)
extern "C"  void PluginMoHe_SceneLoad_m12808002 (PluginMoHe_t1606231090 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::OnLogoutSuccess(System.String)
extern "C"  void PluginMoHe_OnLogoutSuccess_m3477192033 (PluginMoHe_t1606231090 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::OnLogout(System.String)
extern "C"  void PluginMoHe_OnLogout_m1737587966 (PluginMoHe_t1606231090 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::SwitchAccount(System.String)
extern "C"  void PluginMoHe_SwitchAccount_m1946577186 (PluginMoHe_t1606231090 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::initSdk()
extern "C"  void PluginMoHe_initSdk_m249307889 (PluginMoHe_t1606231090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::login()
extern "C"  void PluginMoHe_login_m2389807888 (PluginMoHe_t1606231090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::logout()
extern "C"  void PluginMoHe_logout_m1075423205 (PluginMoHe_t1606231090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::creatRole(System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginMoHe_creatRole_m3257078764 (PluginMoHe_t1606231090 * __this, String_t* ___server_id0, String_t* ___server_name1, String_t* ___role_id2, String_t* ___role_level3, String_t* ___role_name4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::roleUpgrade(System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginMoHe_roleUpgrade_m3609406309 (PluginMoHe_t1606231090 * __this, String_t* ___server_id0, String_t* ___server_name1, String_t* ___role_id2, String_t* ___role_level3, String_t* ___role_name4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::pay(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginMoHe_pay_m802380351 (PluginMoHe_t1606231090 * __this, String_t* ___serverId0, String_t* ___playerId1, String_t* ___playerName2, String_t* ___price3, String_t* ___productId4, String_t* ___extra5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::activeInitializeIosUI()
extern "C"  void PluginMoHe_activeInitializeIosUI_m4279067890 (PluginMoHe_t1606231090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::<OnLoginSuccess>m__43F()
extern "C"  void PluginMoHe_U3COnLoginSuccessU3Em__43F_m4148743806 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::<OnLogoutSuccess>m__440()
extern "C"  void PluginMoHe_U3COnLogoutSuccessU3Em__440_m4015909258 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::<OnLogout>m__441()
extern "C"  void PluginMoHe_U3COnLogoutU3Em__441_m284940312 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::<OnLoginSuccess>m__442()
extern "C"  void PluginMoHe_U3COnLoginSuccessU3Em__442_m4148754377 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginMoHe::ilo_get_currentVS1(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginMoHe_ilo_get_currentVS1_m2967431071 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginMoHe::ilo_get_isSdkLogin2(VersionMgr)
extern "C"  bool PluginMoHe_ilo_get_isSdkLogin2_m1252147120 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::ilo_OpenUserLogin3(PluginMoHe)
extern "C"  void PluginMoHe_ilo_OpenUserLogin3_m3010956027 (Il2CppObject * __this /* static, unused */, PluginMoHe_t1606231090 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::ilo_pay4(PluginMoHe,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginMoHe_ilo_pay4_m2322393872 (Il2CppObject * __this /* static, unused */, PluginMoHe_t1606231090 * ____this0, String_t* ___serverId1, String_t* ___playerId2, String_t* ___playerName3, String_t* ___price4, String_t* ___productId5, String_t* ___extra6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::ilo_LogError5(System.Object,System.Boolean)
extern "C"  void PluginMoHe_ilo_LogError5_m2120259822 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginMoHe::ilo_ContainsKey6(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  bool PluginMoHe_ilo_ContainsKey6_m3475938692 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken PluginMoHe::ilo_get_Item7(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  JToken_t3412245951 * PluginMoHe_ilo_get_Item7_m1875946512 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::ilo_Log8(System.Object,System.Boolean)
extern "C"  void PluginMoHe_ilo_Log8_m2184019787 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::ilo_roleUpgrade9(PluginMoHe,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginMoHe_ilo_roleUpgrade9_m991201467 (Il2CppObject * __this /* static, unused */, PluginMoHe_t1606231090 * ____this0, String_t* ___server_id1, String_t* ___server_name2, String_t* ___role_id3, String_t* ___role_level4, String_t* ___role_name5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 PluginMoHe::ilo_DelayCall10(System.Single,System.Action)
extern "C"  uint32_t PluginMoHe_ilo_DelayCall10_m1143759977 (Il2CppObject * __this /* static, unused */, float ___delaytime0, Action_t3771233898 * ___act1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SceneMgr PluginMoHe::ilo_get_SceneMgr11()
extern "C"  SceneMgr_t3584105996 * PluginMoHe_ilo_get_SceneMgr11_m4091471540 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::ilo_activeInitializeIosUI12(PluginMoHe)
extern "C"  void PluginMoHe_ilo_activeInitializeIosUI12_m3330264590 (Il2CppObject * __this /* static, unused */, PluginMoHe_t1606231090 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::ilo_Logout13(System.Action)
extern "C"  void PluginMoHe_ilo_Logout13_m3167347107 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMoHe::ilo_ReqSDKHttpLogin14(PluginsSdkMgr)
extern "C"  void PluginMoHe_ilo_ReqSDKHttpLogin14_m3507693528 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
