﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginXiaoQi
struct  PluginXiaoQi_t2004954762  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginXiaoQi::token
	String_t* ___token_2;
	// System.String PluginXiaoQi::guid
	String_t* ___guid_3;
	// System.String PluginXiaoQi::notifyId
	String_t* ___notifyId_4;
	// System.String PluginXiaoQi::appKey
	String_t* ___appKey_5;
	// System.Boolean PluginXiaoQi::isOut
	bool ___isOut_6;
	// System.String PluginXiaoQi::configId
	String_t* ___configId_7;
	// Mihua.SDK.PayInfo PluginXiaoQi::iapInfo
	PayInfo_t1775308120 * ___iapInfo_8;

public:
	inline static int32_t get_offset_of_token_2() { return static_cast<int32_t>(offsetof(PluginXiaoQi_t2004954762, ___token_2)); }
	inline String_t* get_token_2() const { return ___token_2; }
	inline String_t** get_address_of_token_2() { return &___token_2; }
	inline void set_token_2(String_t* value)
	{
		___token_2 = value;
		Il2CppCodeGenWriteBarrier(&___token_2, value);
	}

	inline static int32_t get_offset_of_guid_3() { return static_cast<int32_t>(offsetof(PluginXiaoQi_t2004954762, ___guid_3)); }
	inline String_t* get_guid_3() const { return ___guid_3; }
	inline String_t** get_address_of_guid_3() { return &___guid_3; }
	inline void set_guid_3(String_t* value)
	{
		___guid_3 = value;
		Il2CppCodeGenWriteBarrier(&___guid_3, value);
	}

	inline static int32_t get_offset_of_notifyId_4() { return static_cast<int32_t>(offsetof(PluginXiaoQi_t2004954762, ___notifyId_4)); }
	inline String_t* get_notifyId_4() const { return ___notifyId_4; }
	inline String_t** get_address_of_notifyId_4() { return &___notifyId_4; }
	inline void set_notifyId_4(String_t* value)
	{
		___notifyId_4 = value;
		Il2CppCodeGenWriteBarrier(&___notifyId_4, value);
	}

	inline static int32_t get_offset_of_appKey_5() { return static_cast<int32_t>(offsetof(PluginXiaoQi_t2004954762, ___appKey_5)); }
	inline String_t* get_appKey_5() const { return ___appKey_5; }
	inline String_t** get_address_of_appKey_5() { return &___appKey_5; }
	inline void set_appKey_5(String_t* value)
	{
		___appKey_5 = value;
		Il2CppCodeGenWriteBarrier(&___appKey_5, value);
	}

	inline static int32_t get_offset_of_isOut_6() { return static_cast<int32_t>(offsetof(PluginXiaoQi_t2004954762, ___isOut_6)); }
	inline bool get_isOut_6() const { return ___isOut_6; }
	inline bool* get_address_of_isOut_6() { return &___isOut_6; }
	inline void set_isOut_6(bool value)
	{
		___isOut_6 = value;
	}

	inline static int32_t get_offset_of_configId_7() { return static_cast<int32_t>(offsetof(PluginXiaoQi_t2004954762, ___configId_7)); }
	inline String_t* get_configId_7() const { return ___configId_7; }
	inline String_t** get_address_of_configId_7() { return &___configId_7; }
	inline void set_configId_7(String_t* value)
	{
		___configId_7 = value;
		Il2CppCodeGenWriteBarrier(&___configId_7, value);
	}

	inline static int32_t get_offset_of_iapInfo_8() { return static_cast<int32_t>(offsetof(PluginXiaoQi_t2004954762, ___iapInfo_8)); }
	inline PayInfo_t1775308120 * get_iapInfo_8() const { return ___iapInfo_8; }
	inline PayInfo_t1775308120 ** get_address_of_iapInfo_8() { return &___iapInfo_8; }
	inline void set_iapInfo_8(PayInfo_t1775308120 * value)
	{
		___iapInfo_8 = value;
		Il2CppCodeGenWriteBarrier(&___iapInfo_8, value);
	}
};

struct PluginXiaoQi_t2004954762_StaticFields
{
public:
	// System.Action PluginXiaoQi::<>f__am$cache7
	Action_t3771233898 * ___U3CU3Ef__amU24cache7_9;
	// System.Action PluginXiaoQi::<>f__am$cache8
	Action_t3771233898 * ___U3CU3Ef__amU24cache8_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_9() { return static_cast<int32_t>(offsetof(PluginXiaoQi_t2004954762_StaticFields, ___U3CU3Ef__amU24cache7_9)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache7_9() const { return ___U3CU3Ef__amU24cache7_9; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache7_9() { return &___U3CU3Ef__amU24cache7_9; }
	inline void set_U3CU3Ef__amU24cache7_9(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache7_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_10() { return static_cast<int32_t>(offsetof(PluginXiaoQi_t2004954762_StaticFields, ___U3CU3Ef__amU24cache8_10)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache8_10() const { return ___U3CU3Ef__amU24cache8_10; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache8_10() { return &___U3CU3Ef__amU24cache8_10; }
	inline void set_U3CU3Ef__amU24cache8_10(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache8_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
