﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSMgr/OnInitJSEngine
struct OnInitJSEngine_t2416054170;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void JSMgr/OnInitJSEngine::.ctor(System.Object,System.IntPtr)
extern "C"  void OnInitJSEngine__ctor_m1690026817 (OnInitJSEngine_t2416054170 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSMgr/OnInitJSEngine::Invoke(System.Boolean)
extern "C"  void OnInitJSEngine_Invoke_m4196636946 (OnInitJSEngine_t2416054170 * __this, bool ___bSuccess0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult JSMgr/OnInitJSEngine::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnInitJSEngine_BeginInvoke_m73427519 (OnInitJSEngine_t2416054170 * __this, bool ___bSuccess0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSMgr/OnInitJSEngine::EndInvoke(System.IAsyncResult)
extern "C"  void OnInitJSEngine_EndInvoke_m808254929 (OnInitJSEngine_t2416054170 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
