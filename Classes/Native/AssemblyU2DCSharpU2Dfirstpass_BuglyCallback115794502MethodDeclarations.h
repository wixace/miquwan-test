﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BuglyCallback
struct BuglyCallback_t115794502;

#include "codegen/il2cpp-codegen.h"

// System.Void BuglyCallback::.ctor()
extern "C"  void BuglyCallback__ctor_m3441550257 (BuglyCallback_t115794502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
