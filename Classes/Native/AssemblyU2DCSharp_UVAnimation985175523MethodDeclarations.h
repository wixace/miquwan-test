﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UVAnimation
struct UVAnimation_t985175523;

#include "codegen/il2cpp-codegen.h"

// System.Void UVAnimation::.ctor()
extern "C"  void UVAnimation__ctor_m57267432 (UVAnimation_t985175523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UVAnimation::Awake()
extern "C"  void UVAnimation_Awake_m294872651 (UVAnimation_t985175523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UVAnimation::Start()
extern "C"  void UVAnimation_Start_m3299372520 (UVAnimation_t985175523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UVAnimation::OnDestroy()
extern "C"  void UVAnimation_OnDestroy_m1861678561 (UVAnimation_t985175523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UVAnimation::Update()
extern "C"  void UVAnimation_Update_m3502152485 (UVAnimation_t985175523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
