﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.AstarData/<DeserializeGraphsPartAdditive>c__AnonStorey103
struct U3CDeserializeGraphsPartAdditiveU3Ec__AnonStorey103_t2853603656;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.AstarData/<DeserializeGraphsPartAdditive>c__AnonStorey103::.ctor()
extern "C"  void U3CDeserializeGraphsPartAdditiveU3Ec__AnonStorey103__ctor_m4099272995 (U3CDeserializeGraphsPartAdditiveU3Ec__AnonStorey103_t2853603656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.AstarData/<DeserializeGraphsPartAdditive>c__AnonStorey103::<>m__330(Pathfinding.GraphNode)
extern "C"  bool U3CDeserializeGraphsPartAdditiveU3Ec__AnonStorey103_U3CU3Em__330_m1371919450 (U3CDeserializeGraphsPartAdditiveU3Ec__AnonStorey103_t2853603656 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
