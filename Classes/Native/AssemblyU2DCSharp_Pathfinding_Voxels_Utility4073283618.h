﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Voxels.Utility
struct  Utility_t4073283618  : public Il2CppObject
{
public:

public:
};

struct Utility_t4073283618_StaticFields
{
public:
	// UnityEngine.Color[] Pathfinding.Voxels.Utility::colors
	ColorU5BU5D_t2441545636* ___colors_0;
	// System.Single Pathfinding.Voxels.Utility::lastStartTime
	float ___lastStartTime_1;
	// System.Single Pathfinding.Voxels.Utility::lastAdditiveTimerStart
	float ___lastAdditiveTimerStart_2;
	// System.Single Pathfinding.Voxels.Utility::additiveTimer
	float ___additiveTimer_3;
	// System.Single[] Pathfinding.Voxels.Utility::clipPolygonCache
	SingleU5BU5D_t2316563989* ___clipPolygonCache_4;
	// System.Int32[] Pathfinding.Voxels.Utility::clipPolygonIntCache
	Int32U5BU5D_t3230847821* ___clipPolygonIntCache_5;

public:
	inline static int32_t get_offset_of_colors_0() { return static_cast<int32_t>(offsetof(Utility_t4073283618_StaticFields, ___colors_0)); }
	inline ColorU5BU5D_t2441545636* get_colors_0() const { return ___colors_0; }
	inline ColorU5BU5D_t2441545636** get_address_of_colors_0() { return &___colors_0; }
	inline void set_colors_0(ColorU5BU5D_t2441545636* value)
	{
		___colors_0 = value;
		Il2CppCodeGenWriteBarrier(&___colors_0, value);
	}

	inline static int32_t get_offset_of_lastStartTime_1() { return static_cast<int32_t>(offsetof(Utility_t4073283618_StaticFields, ___lastStartTime_1)); }
	inline float get_lastStartTime_1() const { return ___lastStartTime_1; }
	inline float* get_address_of_lastStartTime_1() { return &___lastStartTime_1; }
	inline void set_lastStartTime_1(float value)
	{
		___lastStartTime_1 = value;
	}

	inline static int32_t get_offset_of_lastAdditiveTimerStart_2() { return static_cast<int32_t>(offsetof(Utility_t4073283618_StaticFields, ___lastAdditiveTimerStart_2)); }
	inline float get_lastAdditiveTimerStart_2() const { return ___lastAdditiveTimerStart_2; }
	inline float* get_address_of_lastAdditiveTimerStart_2() { return &___lastAdditiveTimerStart_2; }
	inline void set_lastAdditiveTimerStart_2(float value)
	{
		___lastAdditiveTimerStart_2 = value;
	}

	inline static int32_t get_offset_of_additiveTimer_3() { return static_cast<int32_t>(offsetof(Utility_t4073283618_StaticFields, ___additiveTimer_3)); }
	inline float get_additiveTimer_3() const { return ___additiveTimer_3; }
	inline float* get_address_of_additiveTimer_3() { return &___additiveTimer_3; }
	inline void set_additiveTimer_3(float value)
	{
		___additiveTimer_3 = value;
	}

	inline static int32_t get_offset_of_clipPolygonCache_4() { return static_cast<int32_t>(offsetof(Utility_t4073283618_StaticFields, ___clipPolygonCache_4)); }
	inline SingleU5BU5D_t2316563989* get_clipPolygonCache_4() const { return ___clipPolygonCache_4; }
	inline SingleU5BU5D_t2316563989** get_address_of_clipPolygonCache_4() { return &___clipPolygonCache_4; }
	inline void set_clipPolygonCache_4(SingleU5BU5D_t2316563989* value)
	{
		___clipPolygonCache_4 = value;
		Il2CppCodeGenWriteBarrier(&___clipPolygonCache_4, value);
	}

	inline static int32_t get_offset_of_clipPolygonIntCache_5() { return static_cast<int32_t>(offsetof(Utility_t4073283618_StaticFields, ___clipPolygonIntCache_5)); }
	inline Int32U5BU5D_t3230847821* get_clipPolygonIntCache_5() const { return ___clipPolygonIntCache_5; }
	inline Int32U5BU5D_t3230847821** get_address_of_clipPolygonIntCache_5() { return &___clipPolygonIntCache_5; }
	inline void set_clipPolygonIntCache_5(Int32U5BU5D_t3230847821* value)
	{
		___clipPolygonIntCache_5 = value;
		Il2CppCodeGenWriteBarrier(&___clipPolygonIntCache_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
