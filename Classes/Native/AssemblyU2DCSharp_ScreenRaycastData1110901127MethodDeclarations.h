﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// ScreenRaycastData
struct ScreenRaycastData_t1110901127;
struct ScreenRaycastData_t1110901127_marshaled_pinvoke;
struct ScreenRaycastData_t1110901127_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ScreenRaycastData1110901127.h"

// UnityEngine.GameObject ScreenRaycastData::get_GameObject()
extern "C"  GameObject_t3674682005 * ScreenRaycastData_get_GameObject_m499741509 (ScreenRaycastData_t1110901127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct ScreenRaycastData_t1110901127;
struct ScreenRaycastData_t1110901127_marshaled_pinvoke;

extern "C" void ScreenRaycastData_t1110901127_marshal_pinvoke(const ScreenRaycastData_t1110901127& unmarshaled, ScreenRaycastData_t1110901127_marshaled_pinvoke& marshaled);
extern "C" void ScreenRaycastData_t1110901127_marshal_pinvoke_back(const ScreenRaycastData_t1110901127_marshaled_pinvoke& marshaled, ScreenRaycastData_t1110901127& unmarshaled);
extern "C" void ScreenRaycastData_t1110901127_marshal_pinvoke_cleanup(ScreenRaycastData_t1110901127_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ScreenRaycastData_t1110901127;
struct ScreenRaycastData_t1110901127_marshaled_com;

extern "C" void ScreenRaycastData_t1110901127_marshal_com(const ScreenRaycastData_t1110901127& unmarshaled, ScreenRaycastData_t1110901127_marshaled_com& marshaled);
extern "C" void ScreenRaycastData_t1110901127_marshal_com_back(const ScreenRaycastData_t1110901127_marshaled_com& marshaled, ScreenRaycastData_t1110901127& unmarshaled);
extern "C" void ScreenRaycastData_t1110901127_marshal_com_cleanup(ScreenRaycastData_t1110901127_marshaled_com& marshaled);
