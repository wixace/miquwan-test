﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_sedre190
struct M_sedre190_t3586298931;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_sedre190::.ctor()
extern "C"  void M_sedre190__ctor_m2104843104 (M_sedre190_t3586298931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sedre190::M_zopaJari0(System.String[],System.Int32)
extern "C"  void M_sedre190_M_zopaJari0_m887824927 (M_sedre190_t3586298931 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
