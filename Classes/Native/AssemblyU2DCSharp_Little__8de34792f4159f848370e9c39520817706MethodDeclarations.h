﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._8de34792f4159f848370e9c3923fc2f3
struct _8de34792f4159f848370e9c3923fc2f3_t520817706;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._8de34792f4159f848370e9c3923fc2f3::.ctor()
extern "C"  void _8de34792f4159f848370e9c3923fc2f3__ctor_m3732023619 (_8de34792f4159f848370e9c3923fc2f3_t520817706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8de34792f4159f848370e9c3923fc2f3::_8de34792f4159f848370e9c3923fc2f3m2(System.Int32)
extern "C"  int32_t _8de34792f4159f848370e9c3923fc2f3__8de34792f4159f848370e9c3923fc2f3m2_m1403736409 (_8de34792f4159f848370e9c3923fc2f3_t520817706 * __this, int32_t ____8de34792f4159f848370e9c3923fc2f3a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8de34792f4159f848370e9c3923fc2f3::_8de34792f4159f848370e9c3923fc2f3m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _8de34792f4159f848370e9c3923fc2f3__8de34792f4159f848370e9c3923fc2f3m_m1049398653 (_8de34792f4159f848370e9c3923fc2f3_t520817706 * __this, int32_t ____8de34792f4159f848370e9c3923fc2f3a0, int32_t ____8de34792f4159f848370e9c3923fc2f3511, int32_t ____8de34792f4159f848370e9c3923fc2f3c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
