﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;

#include "AssemblyU2DCSharp_Pathfinding_MonoModifier200043088.h"
#include "UnityEngine_UnityEngine_LayerMask3236759763.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RaycastModifier
struct  RaycastModifier_t1011176258  : public MonoModifier_t200043088
{
public:
	// System.Boolean Pathfinding.RaycastModifier::useRaycasting
	bool ___useRaycasting_4;
	// UnityEngine.LayerMask Pathfinding.RaycastModifier::mask
	LayerMask_t3236759763  ___mask_5;
	// System.Boolean Pathfinding.RaycastModifier::thickRaycast
	bool ___thickRaycast_6;
	// System.Single Pathfinding.RaycastModifier::thickRaycastRadius
	float ___thickRaycastRadius_7;
	// UnityEngine.Vector3 Pathfinding.RaycastModifier::raycastOffset
	Vector3_t4282066566  ___raycastOffset_8;
	// System.Boolean Pathfinding.RaycastModifier::subdivideEveryIter
	bool ___subdivideEveryIter_9;
	// System.Int32 Pathfinding.RaycastModifier::iterations
	int32_t ___iterations_10;
	// System.Boolean Pathfinding.RaycastModifier::useGraphRaycasting
	bool ___useGraphRaycasting_11;

public:
	inline static int32_t get_offset_of_useRaycasting_4() { return static_cast<int32_t>(offsetof(RaycastModifier_t1011176258, ___useRaycasting_4)); }
	inline bool get_useRaycasting_4() const { return ___useRaycasting_4; }
	inline bool* get_address_of_useRaycasting_4() { return &___useRaycasting_4; }
	inline void set_useRaycasting_4(bool value)
	{
		___useRaycasting_4 = value;
	}

	inline static int32_t get_offset_of_mask_5() { return static_cast<int32_t>(offsetof(RaycastModifier_t1011176258, ___mask_5)); }
	inline LayerMask_t3236759763  get_mask_5() const { return ___mask_5; }
	inline LayerMask_t3236759763 * get_address_of_mask_5() { return &___mask_5; }
	inline void set_mask_5(LayerMask_t3236759763  value)
	{
		___mask_5 = value;
	}

	inline static int32_t get_offset_of_thickRaycast_6() { return static_cast<int32_t>(offsetof(RaycastModifier_t1011176258, ___thickRaycast_6)); }
	inline bool get_thickRaycast_6() const { return ___thickRaycast_6; }
	inline bool* get_address_of_thickRaycast_6() { return &___thickRaycast_6; }
	inline void set_thickRaycast_6(bool value)
	{
		___thickRaycast_6 = value;
	}

	inline static int32_t get_offset_of_thickRaycastRadius_7() { return static_cast<int32_t>(offsetof(RaycastModifier_t1011176258, ___thickRaycastRadius_7)); }
	inline float get_thickRaycastRadius_7() const { return ___thickRaycastRadius_7; }
	inline float* get_address_of_thickRaycastRadius_7() { return &___thickRaycastRadius_7; }
	inline void set_thickRaycastRadius_7(float value)
	{
		___thickRaycastRadius_7 = value;
	}

	inline static int32_t get_offset_of_raycastOffset_8() { return static_cast<int32_t>(offsetof(RaycastModifier_t1011176258, ___raycastOffset_8)); }
	inline Vector3_t4282066566  get_raycastOffset_8() const { return ___raycastOffset_8; }
	inline Vector3_t4282066566 * get_address_of_raycastOffset_8() { return &___raycastOffset_8; }
	inline void set_raycastOffset_8(Vector3_t4282066566  value)
	{
		___raycastOffset_8 = value;
	}

	inline static int32_t get_offset_of_subdivideEveryIter_9() { return static_cast<int32_t>(offsetof(RaycastModifier_t1011176258, ___subdivideEveryIter_9)); }
	inline bool get_subdivideEveryIter_9() const { return ___subdivideEveryIter_9; }
	inline bool* get_address_of_subdivideEveryIter_9() { return &___subdivideEveryIter_9; }
	inline void set_subdivideEveryIter_9(bool value)
	{
		___subdivideEveryIter_9 = value;
	}

	inline static int32_t get_offset_of_iterations_10() { return static_cast<int32_t>(offsetof(RaycastModifier_t1011176258, ___iterations_10)); }
	inline int32_t get_iterations_10() const { return ___iterations_10; }
	inline int32_t* get_address_of_iterations_10() { return &___iterations_10; }
	inline void set_iterations_10(int32_t value)
	{
		___iterations_10 = value;
	}

	inline static int32_t get_offset_of_useGraphRaycasting_11() { return static_cast<int32_t>(offsetof(RaycastModifier_t1011176258, ___useGraphRaycasting_11)); }
	inline bool get_useGraphRaycasting_11() const { return ___useGraphRaycasting_11; }
	inline bool* get_address_of_useGraphRaycasting_11() { return &___useGraphRaycasting_11; }
	inline void set_useGraphRaycasting_11(bool value)
	{
		___useGraphRaycasting_11 = value;
	}
};

struct RaycastModifier_t1011176258_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Pathfinding.RaycastModifier::nodes
	List_1_t1355284822 * ___nodes_12;

public:
	inline static int32_t get_offset_of_nodes_12() { return static_cast<int32_t>(offsetof(RaycastModifier_t1011176258_StaticFields, ___nodes_12)); }
	inline List_1_t1355284822 * get_nodes_12() const { return ___nodes_12; }
	inline List_1_t1355284822 ** get_address_of_nodes_12() { return &___nodes_12; }
	inline void set_nodes_12(List_1_t1355284822 * value)
	{
		___nodes_12 = value;
		Il2CppCodeGenWriteBarrier(&___nodes_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
