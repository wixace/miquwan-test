﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.ProtoIncludeAttribute
struct ProtoIncludeAttribute_t3860000898;
// System.Type
struct Type_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ProtoBuf_DataFormat274207885.h"

// System.Void ProtoBuf.ProtoIncludeAttribute::.ctor(System.Int32,System.Type)
extern "C"  void ProtoIncludeAttribute__ctor_m1784565542 (ProtoIncludeAttribute_t3860000898 * __this, int32_t ___tag0, Type_t * ___knownType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoIncludeAttribute::.ctor(System.Int32,System.String)
extern "C"  void ProtoIncludeAttribute__ctor_m246921071 (ProtoIncludeAttribute_t3860000898 * __this, int32_t ___tag0, String_t* ___knownTypeName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoIncludeAttribute::get_Tag()
extern "C"  int32_t ProtoIncludeAttribute_get_Tag_m1524687907 (ProtoIncludeAttribute_t3860000898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.ProtoIncludeAttribute::get_KnownTypeName()
extern "C"  String_t* ProtoIncludeAttribute_get_KnownTypeName_m977067548 (ProtoIncludeAttribute_t3860000898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.ProtoIncludeAttribute::get_KnownType()
extern "C"  Type_t * ProtoIncludeAttribute_get_KnownType_m4201445466 (ProtoIncludeAttribute_t3860000898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.DataFormat ProtoBuf.ProtoIncludeAttribute::get_DataFormat()
extern "C"  int32_t ProtoIncludeAttribute_get_DataFormat_m3065385051 (ProtoIncludeAttribute_t3860000898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoIncludeAttribute::set_DataFormat(ProtoBuf.DataFormat)
extern "C"  void ProtoIncludeAttribute_set_DataFormat_m3356164718 (ProtoIncludeAttribute_t3860000898 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
