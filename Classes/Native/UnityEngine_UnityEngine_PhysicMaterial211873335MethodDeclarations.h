﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.PhysicMaterial
struct PhysicMaterial_t211873335;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_PhysicMaterial211873335.h"
#include "UnityEngine_UnityEngine_PhysicMaterialCombine4248458306.h"

// System.Void UnityEngine.PhysicMaterial::.ctor()
extern "C"  void PhysicMaterial__ctor_m301074010 (PhysicMaterial_t211873335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PhysicMaterial::.ctor(System.String)
extern "C"  void PhysicMaterial__ctor_m2556574440 (PhysicMaterial_t211873335 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PhysicMaterial::Internal_CreateDynamicsMaterial(UnityEngine.PhysicMaterial,System.String)
extern "C"  void PhysicMaterial_Internal_CreateDynamicsMaterial_m1805194787 (Il2CppObject * __this /* static, unused */, PhysicMaterial_t211873335 * ___mat0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.PhysicMaterial::get_dynamicFriction()
extern "C"  float PhysicMaterial_get_dynamicFriction_m1651750716 (PhysicMaterial_t211873335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PhysicMaterial::set_dynamicFriction(System.Single)
extern "C"  void PhysicMaterial_set_dynamicFriction_m3170844503 (PhysicMaterial_t211873335 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.PhysicMaterial::get_staticFriction()
extern "C"  float PhysicMaterial_get_staticFriction_m1788959271 (PhysicMaterial_t211873335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PhysicMaterial::set_staticFriction(System.Single)
extern "C"  void PhysicMaterial_set_staticFriction_m4195237500 (PhysicMaterial_t211873335 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.PhysicMaterial::get_bounciness()
extern "C"  float PhysicMaterial_get_bounciness_m1135238594 (PhysicMaterial_t211873335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PhysicMaterial::set_bounciness(System.Single)
extern "C"  void PhysicMaterial_set_bounciness_m603634369 (PhysicMaterial_t211873335 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.PhysicMaterialCombine UnityEngine.PhysicMaterial::get_frictionCombine()
extern "C"  int32_t PhysicMaterial_get_frictionCombine_m2828035414 (PhysicMaterial_t211873335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PhysicMaterial::set_frictionCombine(UnityEngine.PhysicMaterialCombine)
extern "C"  void PhysicMaterial_set_frictionCombine_m2725979517 (PhysicMaterial_t211873335 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.PhysicMaterialCombine UnityEngine.PhysicMaterial::get_bounceCombine()
extern "C"  int32_t PhysicMaterial_get_bounceCombine_m1834090888 (PhysicMaterial_t211873335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PhysicMaterial::set_bounceCombine(UnityEngine.PhysicMaterialCombine)
extern "C"  void PhysicMaterial_set_bounceCombine_m2458938635 (PhysicMaterial_t211873335 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
