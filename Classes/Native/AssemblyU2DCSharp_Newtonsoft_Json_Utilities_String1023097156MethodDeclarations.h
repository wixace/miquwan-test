﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14C`1<System.Object>
struct U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_t1023097156;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14C`1<System.Object>::.ctor()
extern "C"  void U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1__ctor_m1201620411_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_t1023097156 * __this, const MethodInfo* method);
#define U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1__ctor_m1201620411(__this, method) ((  void (*) (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_t1023097156 *, const MethodInfo*))U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1__ctor_m1201620411_gshared)(__this, method)
// System.Void Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14C`1<System.Object>::<>m__3B8(TSource)
extern "C"  void U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_U3CU3Em__3B8_m4144775388_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_t1023097156 * __this, Il2CppObject * ___itm0, const MethodInfo* method);
#define U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_U3CU3Em__3B8_m4144775388(__this, ___itm0, method) ((  void (*) (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_t1023097156 *, Il2CppObject *, const MethodInfo*))U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_U3CU3Em__3B8_m4144775388_gshared)(__this, ___itm0, method)
