﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_jearmihoNece41
struct M_jearmihoNece41_t555957525;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_jearmihoNece41555957525.h"

// System.Void GarbageiOS.M_jearmihoNece41::.ctor()
extern "C"  void M_jearmihoNece41__ctor_m381239998 (M_jearmihoNece41_t555957525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jearmihoNece41::M_daltrerezirStojosu0(System.String[],System.Int32)
extern "C"  void M_jearmihoNece41_M_daltrerezirStojosu0_m3061689652 (M_jearmihoNece41_t555957525 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jearmihoNece41::M_hirpearbou1(System.String[],System.Int32)
extern "C"  void M_jearmihoNece41_M_hirpearbou1_m870162593 (M_jearmihoNece41_t555957525 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jearmihoNece41::ilo_M_daltrerezirStojosu01(GarbageiOS.M_jearmihoNece41,System.String[],System.Int32)
extern "C"  void M_jearmihoNece41_ilo_M_daltrerezirStojosu01_m693008961 (Il2CppObject * __this /* static, unused */, M_jearmihoNece41_t555957525 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
