﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_toqow109
struct  M_toqow109_t1689188556  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_toqow109::_veaweNeare
	String_t* ____veaweNeare_0;
	// System.String GarbageiOS.M_toqow109::_biritri
	String_t* ____biritri_1;
	// System.UInt32 GarbageiOS.M_toqow109::_moujookeaChirjeraw
	uint32_t ____moujookeaChirjeraw_2;
	// System.Int32 GarbageiOS.M_toqow109::_qowjar
	int32_t ____qowjar_3;
	// System.Boolean GarbageiOS.M_toqow109::_nerejalVearnairjor
	bool ____nerejalVearnairjor_4;
	// System.Single GarbageiOS.M_toqow109::_koohea
	float ____koohea_5;
	// System.Boolean GarbageiOS.M_toqow109::_nerwallRagoucho
	bool ____nerwallRagoucho_6;
	// System.Single GarbageiOS.M_toqow109::_woolaswear
	float ____woolaswear_7;
	// System.String GarbageiOS.M_toqow109::_nearifowPache
	String_t* ____nearifowPache_8;
	// System.String GarbageiOS.M_toqow109::_noutainaiRujor
	String_t* ____noutainaiRujor_9;
	// System.String GarbageiOS.M_toqow109::_leasow
	String_t* ____leasow_10;
	// System.UInt32 GarbageiOS.M_toqow109::_rerawNastrevas
	uint32_t ____rerawNastrevas_11;
	// System.Single GarbageiOS.M_toqow109::_trearmi
	float ____trearmi_12;
	// System.Int32 GarbageiOS.M_toqow109::_rereqechal
	int32_t ____rereqechal_13;
	// System.String GarbageiOS.M_toqow109::_jeateWhafar
	String_t* ____jeateWhafar_14;
	// System.Boolean GarbageiOS.M_toqow109::_wasvo
	bool ____wasvo_15;
	// System.String GarbageiOS.M_toqow109::_couheefo
	String_t* ____couheefo_16;

public:
	inline static int32_t get_offset_of__veaweNeare_0() { return static_cast<int32_t>(offsetof(M_toqow109_t1689188556, ____veaweNeare_0)); }
	inline String_t* get__veaweNeare_0() const { return ____veaweNeare_0; }
	inline String_t** get_address_of__veaweNeare_0() { return &____veaweNeare_0; }
	inline void set__veaweNeare_0(String_t* value)
	{
		____veaweNeare_0 = value;
		Il2CppCodeGenWriteBarrier(&____veaweNeare_0, value);
	}

	inline static int32_t get_offset_of__biritri_1() { return static_cast<int32_t>(offsetof(M_toqow109_t1689188556, ____biritri_1)); }
	inline String_t* get__biritri_1() const { return ____biritri_1; }
	inline String_t** get_address_of__biritri_1() { return &____biritri_1; }
	inline void set__biritri_1(String_t* value)
	{
		____biritri_1 = value;
		Il2CppCodeGenWriteBarrier(&____biritri_1, value);
	}

	inline static int32_t get_offset_of__moujookeaChirjeraw_2() { return static_cast<int32_t>(offsetof(M_toqow109_t1689188556, ____moujookeaChirjeraw_2)); }
	inline uint32_t get__moujookeaChirjeraw_2() const { return ____moujookeaChirjeraw_2; }
	inline uint32_t* get_address_of__moujookeaChirjeraw_2() { return &____moujookeaChirjeraw_2; }
	inline void set__moujookeaChirjeraw_2(uint32_t value)
	{
		____moujookeaChirjeraw_2 = value;
	}

	inline static int32_t get_offset_of__qowjar_3() { return static_cast<int32_t>(offsetof(M_toqow109_t1689188556, ____qowjar_3)); }
	inline int32_t get__qowjar_3() const { return ____qowjar_3; }
	inline int32_t* get_address_of__qowjar_3() { return &____qowjar_3; }
	inline void set__qowjar_3(int32_t value)
	{
		____qowjar_3 = value;
	}

	inline static int32_t get_offset_of__nerejalVearnairjor_4() { return static_cast<int32_t>(offsetof(M_toqow109_t1689188556, ____nerejalVearnairjor_4)); }
	inline bool get__nerejalVearnairjor_4() const { return ____nerejalVearnairjor_4; }
	inline bool* get_address_of__nerejalVearnairjor_4() { return &____nerejalVearnairjor_4; }
	inline void set__nerejalVearnairjor_4(bool value)
	{
		____nerejalVearnairjor_4 = value;
	}

	inline static int32_t get_offset_of__koohea_5() { return static_cast<int32_t>(offsetof(M_toqow109_t1689188556, ____koohea_5)); }
	inline float get__koohea_5() const { return ____koohea_5; }
	inline float* get_address_of__koohea_5() { return &____koohea_5; }
	inline void set__koohea_5(float value)
	{
		____koohea_5 = value;
	}

	inline static int32_t get_offset_of__nerwallRagoucho_6() { return static_cast<int32_t>(offsetof(M_toqow109_t1689188556, ____nerwallRagoucho_6)); }
	inline bool get__nerwallRagoucho_6() const { return ____nerwallRagoucho_6; }
	inline bool* get_address_of__nerwallRagoucho_6() { return &____nerwallRagoucho_6; }
	inline void set__nerwallRagoucho_6(bool value)
	{
		____nerwallRagoucho_6 = value;
	}

	inline static int32_t get_offset_of__woolaswear_7() { return static_cast<int32_t>(offsetof(M_toqow109_t1689188556, ____woolaswear_7)); }
	inline float get__woolaswear_7() const { return ____woolaswear_7; }
	inline float* get_address_of__woolaswear_7() { return &____woolaswear_7; }
	inline void set__woolaswear_7(float value)
	{
		____woolaswear_7 = value;
	}

	inline static int32_t get_offset_of__nearifowPache_8() { return static_cast<int32_t>(offsetof(M_toqow109_t1689188556, ____nearifowPache_8)); }
	inline String_t* get__nearifowPache_8() const { return ____nearifowPache_8; }
	inline String_t** get_address_of__nearifowPache_8() { return &____nearifowPache_8; }
	inline void set__nearifowPache_8(String_t* value)
	{
		____nearifowPache_8 = value;
		Il2CppCodeGenWriteBarrier(&____nearifowPache_8, value);
	}

	inline static int32_t get_offset_of__noutainaiRujor_9() { return static_cast<int32_t>(offsetof(M_toqow109_t1689188556, ____noutainaiRujor_9)); }
	inline String_t* get__noutainaiRujor_9() const { return ____noutainaiRujor_9; }
	inline String_t** get_address_of__noutainaiRujor_9() { return &____noutainaiRujor_9; }
	inline void set__noutainaiRujor_9(String_t* value)
	{
		____noutainaiRujor_9 = value;
		Il2CppCodeGenWriteBarrier(&____noutainaiRujor_9, value);
	}

	inline static int32_t get_offset_of__leasow_10() { return static_cast<int32_t>(offsetof(M_toqow109_t1689188556, ____leasow_10)); }
	inline String_t* get__leasow_10() const { return ____leasow_10; }
	inline String_t** get_address_of__leasow_10() { return &____leasow_10; }
	inline void set__leasow_10(String_t* value)
	{
		____leasow_10 = value;
		Il2CppCodeGenWriteBarrier(&____leasow_10, value);
	}

	inline static int32_t get_offset_of__rerawNastrevas_11() { return static_cast<int32_t>(offsetof(M_toqow109_t1689188556, ____rerawNastrevas_11)); }
	inline uint32_t get__rerawNastrevas_11() const { return ____rerawNastrevas_11; }
	inline uint32_t* get_address_of__rerawNastrevas_11() { return &____rerawNastrevas_11; }
	inline void set__rerawNastrevas_11(uint32_t value)
	{
		____rerawNastrevas_11 = value;
	}

	inline static int32_t get_offset_of__trearmi_12() { return static_cast<int32_t>(offsetof(M_toqow109_t1689188556, ____trearmi_12)); }
	inline float get__trearmi_12() const { return ____trearmi_12; }
	inline float* get_address_of__trearmi_12() { return &____trearmi_12; }
	inline void set__trearmi_12(float value)
	{
		____trearmi_12 = value;
	}

	inline static int32_t get_offset_of__rereqechal_13() { return static_cast<int32_t>(offsetof(M_toqow109_t1689188556, ____rereqechal_13)); }
	inline int32_t get__rereqechal_13() const { return ____rereqechal_13; }
	inline int32_t* get_address_of__rereqechal_13() { return &____rereqechal_13; }
	inline void set__rereqechal_13(int32_t value)
	{
		____rereqechal_13 = value;
	}

	inline static int32_t get_offset_of__jeateWhafar_14() { return static_cast<int32_t>(offsetof(M_toqow109_t1689188556, ____jeateWhafar_14)); }
	inline String_t* get__jeateWhafar_14() const { return ____jeateWhafar_14; }
	inline String_t** get_address_of__jeateWhafar_14() { return &____jeateWhafar_14; }
	inline void set__jeateWhafar_14(String_t* value)
	{
		____jeateWhafar_14 = value;
		Il2CppCodeGenWriteBarrier(&____jeateWhafar_14, value);
	}

	inline static int32_t get_offset_of__wasvo_15() { return static_cast<int32_t>(offsetof(M_toqow109_t1689188556, ____wasvo_15)); }
	inline bool get__wasvo_15() const { return ____wasvo_15; }
	inline bool* get_address_of__wasvo_15() { return &____wasvo_15; }
	inline void set__wasvo_15(bool value)
	{
		____wasvo_15 = value;
	}

	inline static int32_t get_offset_of__couheefo_16() { return static_cast<int32_t>(offsetof(M_toqow109_t1689188556, ____couheefo_16)); }
	inline String_t* get__couheefo_16() const { return ____couheefo_16; }
	inline String_t** get_address_of__couheefo_16() { return &____couheefo_16; }
	inline void set__couheefo_16(String_t* value)
	{
		____couheefo_16 = value;
		Il2CppCodeGenWriteBarrier(&____couheefo_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
