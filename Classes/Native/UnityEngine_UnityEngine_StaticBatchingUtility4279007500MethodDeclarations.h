﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.StaticBatchingUtility
struct StaticBatchingUtility_t4279007500;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// UnityEngine.Mesh
struct Mesh_t4241756145;
// UnityEngine.MeshSubsetCombineUtility/MeshInstance[]
struct MeshInstanceU5BU5D_t1318761771;
// System.String
struct String_t;
// UnityEngine.MeshSubsetCombineUtility/SubMeshInstance[]
struct SubMeshInstanceU5BU5D_t2339421603;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"

// System.Void UnityEngine.StaticBatchingUtility::.ctor()
extern "C"  void StaticBatchingUtility__ctor_m1750431491 (StaticBatchingUtility_t4279007500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.StaticBatchingUtility::Combine(UnityEngine.GameObject)
extern "C"  void StaticBatchingUtility_Combine_m3190863128 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___staticBatchRoot0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.StaticBatchingUtility::Combine(UnityEngine.GameObject[],UnityEngine.GameObject)
extern "C"  void StaticBatchingUtility_Combine_m2788127880 (Il2CppObject * __this /* static, unused */, GameObjectU5BU5D_t2662109048* ___gos0, GameObject_t3674682005 * ___staticBatchRoot1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh UnityEngine.StaticBatchingUtility::InternalCombineVertices(UnityEngine.MeshSubsetCombineUtility/MeshInstance[],System.String)
extern "C"  Mesh_t4241756145 * StaticBatchingUtility_InternalCombineVertices_m2999804615 (Il2CppObject * __this /* static, unused */, MeshInstanceU5BU5D_t1318761771* ___meshes0, String_t* ___meshName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.StaticBatchingUtility::InternalCombineIndices(UnityEngine.MeshSubsetCombineUtility/SubMeshInstance[],UnityEngine.Mesh)
extern "C"  void StaticBatchingUtility_InternalCombineIndices_m214094628 (Il2CppObject * __this /* static, unused */, SubMeshInstanceU5BU5D_t2339421603* ___submeshes0, Mesh_t4241756145 * ___combinedMesh1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
