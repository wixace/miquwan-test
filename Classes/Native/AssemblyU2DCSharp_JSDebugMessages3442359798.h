﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<JSDebugMessages/Message>
struct List_1_t2271975326;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSDebugMessages
struct  JSDebugMessages_t3442359798  : public Il2CppObject
{
public:

public:
};

struct JSDebugMessages_t3442359798_StaticFields
{
public:
	// System.Collections.Generic.List`1<JSDebugMessages/Message> JSDebugMessages::messages
	List_1_t2271975326 * ___messages_3;

public:
	inline static int32_t get_offset_of_messages_3() { return static_cast<int32_t>(offsetof(JSDebugMessages_t3442359798_StaticFields, ___messages_3)); }
	inline List_1_t2271975326 * get_messages_3() const { return ___messages_3; }
	inline List_1_t2271975326 ** get_address_of_messages_3() { return &___messages_3; }
	inline void set_messages_3(List_1_t2271975326 * value)
	{
		___messages_3 = value;
		Il2CppCodeGenWriteBarrier(&___messages_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
