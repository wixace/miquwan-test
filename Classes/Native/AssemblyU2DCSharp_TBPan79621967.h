﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.BoxCollider
struct BoxCollider_t2538127765;
// DragGesture
struct DragGesture_t2914643285;
// TBPan/PanEventHandler
struct PanEventHandler_t1012212141;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TBPan
struct  TBPan_t79621967  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Transform TBPan::cachedTransform
	Transform_t1659122786 * ___cachedTransform_2;
	// System.Single TBPan::sensitivity
	float ___sensitivity_3;
	// System.Single TBPan::smoothSpeed
	float ___smoothSpeed_4;
	// UnityEngine.BoxCollider TBPan::moveArea
	BoxCollider_t2538127765 * ___moveArea_5;
	// UnityEngine.Vector3 TBPan::idealPos
	Vector3_t4282066566  ___idealPos_6;
	// DragGesture TBPan::dragGesture
	DragGesture_t2914643285 * ___dragGesture_7;
	// TBPan/PanEventHandler TBPan::OnPan
	PanEventHandler_t1012212141 * ___OnPan_8;

public:
	inline static int32_t get_offset_of_cachedTransform_2() { return static_cast<int32_t>(offsetof(TBPan_t79621967, ___cachedTransform_2)); }
	inline Transform_t1659122786 * get_cachedTransform_2() const { return ___cachedTransform_2; }
	inline Transform_t1659122786 ** get_address_of_cachedTransform_2() { return &___cachedTransform_2; }
	inline void set_cachedTransform_2(Transform_t1659122786 * value)
	{
		___cachedTransform_2 = value;
		Il2CppCodeGenWriteBarrier(&___cachedTransform_2, value);
	}

	inline static int32_t get_offset_of_sensitivity_3() { return static_cast<int32_t>(offsetof(TBPan_t79621967, ___sensitivity_3)); }
	inline float get_sensitivity_3() const { return ___sensitivity_3; }
	inline float* get_address_of_sensitivity_3() { return &___sensitivity_3; }
	inline void set_sensitivity_3(float value)
	{
		___sensitivity_3 = value;
	}

	inline static int32_t get_offset_of_smoothSpeed_4() { return static_cast<int32_t>(offsetof(TBPan_t79621967, ___smoothSpeed_4)); }
	inline float get_smoothSpeed_4() const { return ___smoothSpeed_4; }
	inline float* get_address_of_smoothSpeed_4() { return &___smoothSpeed_4; }
	inline void set_smoothSpeed_4(float value)
	{
		___smoothSpeed_4 = value;
	}

	inline static int32_t get_offset_of_moveArea_5() { return static_cast<int32_t>(offsetof(TBPan_t79621967, ___moveArea_5)); }
	inline BoxCollider_t2538127765 * get_moveArea_5() const { return ___moveArea_5; }
	inline BoxCollider_t2538127765 ** get_address_of_moveArea_5() { return &___moveArea_5; }
	inline void set_moveArea_5(BoxCollider_t2538127765 * value)
	{
		___moveArea_5 = value;
		Il2CppCodeGenWriteBarrier(&___moveArea_5, value);
	}

	inline static int32_t get_offset_of_idealPos_6() { return static_cast<int32_t>(offsetof(TBPan_t79621967, ___idealPos_6)); }
	inline Vector3_t4282066566  get_idealPos_6() const { return ___idealPos_6; }
	inline Vector3_t4282066566 * get_address_of_idealPos_6() { return &___idealPos_6; }
	inline void set_idealPos_6(Vector3_t4282066566  value)
	{
		___idealPos_6 = value;
	}

	inline static int32_t get_offset_of_dragGesture_7() { return static_cast<int32_t>(offsetof(TBPan_t79621967, ___dragGesture_7)); }
	inline DragGesture_t2914643285 * get_dragGesture_7() const { return ___dragGesture_7; }
	inline DragGesture_t2914643285 ** get_address_of_dragGesture_7() { return &___dragGesture_7; }
	inline void set_dragGesture_7(DragGesture_t2914643285 * value)
	{
		___dragGesture_7 = value;
		Il2CppCodeGenWriteBarrier(&___dragGesture_7, value);
	}

	inline static int32_t get_offset_of_OnPan_8() { return static_cast<int32_t>(offsetof(TBPan_t79621967, ___OnPan_8)); }
	inline PanEventHandler_t1012212141 * get_OnPan_8() const { return ___OnPan_8; }
	inline PanEventHandler_t1012212141 ** get_address_of_OnPan_8() { return &___OnPan_8; }
	inline void set_OnPan_8(PanEventHandler_t1012212141 * value)
	{
		___OnPan_8 = value;
		Il2CppCodeGenWriteBarrier(&___OnPan_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
