﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.JointTranslationLimits2D
struct JointTranslationLimits2D_t2599234005;
struct JointTranslationLimits2D_t2599234005_marshaled_pinvoke;
struct JointTranslationLimits2D_t2599234005_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_JointTranslationLimits2D2599234005.h"

// System.Single UnityEngine.JointTranslationLimits2D::get_min()
extern "C"  float JointTranslationLimits2D_get_min_m3145474391 (JointTranslationLimits2D_t2599234005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.JointTranslationLimits2D::set_min(System.Single)
extern "C"  void JointTranslationLimits2D_set_min_m3907441692 (JointTranslationLimits2D_t2599234005 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.JointTranslationLimits2D::get_max()
extern "C"  float JointTranslationLimits2D_get_max_m3145245673 (JointTranslationLimits2D_t2599234005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.JointTranslationLimits2D::set_max(System.Single)
extern "C"  void JointTranslationLimits2D_set_max_m860524234 (JointTranslationLimits2D_t2599234005 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct JointTranslationLimits2D_t2599234005;
struct JointTranslationLimits2D_t2599234005_marshaled_pinvoke;

extern "C" void JointTranslationLimits2D_t2599234005_marshal_pinvoke(const JointTranslationLimits2D_t2599234005& unmarshaled, JointTranslationLimits2D_t2599234005_marshaled_pinvoke& marshaled);
extern "C" void JointTranslationLimits2D_t2599234005_marshal_pinvoke_back(const JointTranslationLimits2D_t2599234005_marshaled_pinvoke& marshaled, JointTranslationLimits2D_t2599234005& unmarshaled);
extern "C" void JointTranslationLimits2D_t2599234005_marshal_pinvoke_cleanup(JointTranslationLimits2D_t2599234005_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct JointTranslationLimits2D_t2599234005;
struct JointTranslationLimits2D_t2599234005_marshaled_com;

extern "C" void JointTranslationLimits2D_t2599234005_marshal_com(const JointTranslationLimits2D_t2599234005& unmarshaled, JointTranslationLimits2D_t2599234005_marshaled_com& marshaled);
extern "C" void JointTranslationLimits2D_t2599234005_marshal_com_back(const JointTranslationLimits2D_t2599234005_marshaled_com& marshaled, JointTranslationLimits2D_t2599234005& unmarshaled);
extern "C" void JointTranslationLimits2D_t2599234005_marshal_com_cleanup(JointTranslationLimits2D_t2599234005_marshaled_com& marshaled);
