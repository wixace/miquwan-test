﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22851311006.h"
#include "AssemblyU2DCSharp_UIModelDisplayType1891752679.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2384040859_gshared (KeyValuePair_2_t2851311006 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m2384040859(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2851311006 *, int32_t, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m2384040859_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m100238701_gshared (KeyValuePair_2_t2851311006 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m100238701(__this, method) ((  int32_t (*) (KeyValuePair_2_t2851311006 *, const MethodInfo*))KeyValuePair_2_get_Key_m100238701_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3521687982_gshared (KeyValuePair_2_t2851311006 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m3521687982(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2851311006 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m3521687982_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3733392849_gshared (KeyValuePair_2_t2851311006 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m3733392849(__this, method) ((  int32_t (*) (KeyValuePair_2_t2851311006 *, const MethodInfo*))KeyValuePair_2_get_Value_m3733392849_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1511489198_gshared (KeyValuePair_2_t2851311006 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1511489198(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2851311006 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m1511489198_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m4139490970_gshared (KeyValuePair_2_t2851311006 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m4139490970(__this, method) ((  String_t* (*) (KeyValuePair_2_t2851311006 *, const MethodInfo*))KeyValuePair_2_ToString_m4139490970_gshared)(__this, method)
