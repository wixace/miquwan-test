﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JObject/JPropertKeyedCollection
struct JPropertKeyedCollection_t2889692899;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t798265961;
// System.String
struct String_t;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Linq.JToken>
struct IDictionary_2_t3810537666;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"

// System.Void Newtonsoft.Json.Linq.JObject/JPropertKeyedCollection::.ctor(System.Collections.Generic.IEqualityComparer`1<System.String>)
extern "C"  void JPropertKeyedCollection__ctor_m739899714 (JPropertKeyedCollection_t2889692899 * __this, Il2CppObject* ___comparer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JObject/JPropertKeyedCollection::GetKeyForItem(Newtonsoft.Json.Linq.JToken)
extern "C"  String_t* JPropertKeyedCollection_GetKeyForItem_m3153036324 (JPropertKeyedCollection_t2889692899 * __this, JToken_t3412245951 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject/JPropertKeyedCollection::InsertItem(System.Int32,Newtonsoft.Json.Linq.JToken)
extern "C"  void JPropertKeyedCollection_InsertItem_m1554559245 (JPropertKeyedCollection_t2889692899 * __this, int32_t ___index0, JToken_t3412245951 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject/JPropertKeyedCollection::get_Dictionary()
extern "C"  Il2CppObject* JPropertKeyedCollection_get_Dictionary_m2532271602 (JPropertKeyedCollection_t2889692899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
