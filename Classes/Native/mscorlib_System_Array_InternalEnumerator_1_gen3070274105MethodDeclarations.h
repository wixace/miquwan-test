﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3070274105.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24287931429.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m725339696_gshared (InternalEnumerator_1_t3070274105 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m725339696(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3070274105 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m725339696_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m839637360_gshared (InternalEnumerator_1_t3070274105 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m839637360(__this, method) ((  void (*) (InternalEnumerator_1_t3070274105 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m839637360_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3609234982_gshared (InternalEnumerator_1_t3070274105 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3609234982(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3070274105 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3609234982_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1454134279_gshared (InternalEnumerator_1_t3070274105 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1454134279(__this, method) ((  void (*) (InternalEnumerator_1_t3070274105 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1454134279_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3534256288_gshared (InternalEnumerator_1_t3070274105 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3534256288(__this, method) ((  bool (*) (InternalEnumerator_1_t3070274105 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3534256288_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_Current()
extern "C"  KeyValuePair_2_t4287931429  InternalEnumerator_1_get_Current_m358699545_gshared (InternalEnumerator_1_t3070274105 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m358699545(__this, method) ((  KeyValuePair_2_t4287931429  (*) (InternalEnumerator_1_t3070274105 *, const MethodInfo*))InternalEnumerator_1_get_Current_m358699545_gshared)(__this, method)
