﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSFightforDataGenerated
struct CSFightforDataGenerated_t3816337468;
// JSVCall
struct JSVCall_t3708497963;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Collections.Generic.List`1<CSPlusAtt>
struct List_1_t341533415;
// CSFightforData
struct CSFightforData_t215334547;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CSFightforData215334547.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void CSFightforDataGenerated::.ctor()
extern "C"  void CSFightforDataGenerated__ctor_m4170330799 (CSFightforDataGenerated_t3816337468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSFightforDataGenerated::CSFightforData_CSFightforData1(JSVCall,System.Int32)
extern "C"  bool CSFightforDataGenerated_CSFightforData_CSFightforData1_m2163924035 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSFightforDataGenerated::CSFightforData_isRealMan(JSVCall)
extern "C"  void CSFightforDataGenerated_CSFightforData_isRealMan_m4068583564 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSFightforDataGenerated::CSFightforData_playerName(JSVCall)
extern "C"  void CSFightforDataGenerated_CSFightforData_playerName_m1619214306 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSFightforDataGenerated::CSFightforData_power(JSVCall)
extern "C"  void CSFightforDataGenerated_CSFightforData_power_m3386577721 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSFightforDataGenerated::CSFightforData_monsterIdArr(JSVCall)
extern "C"  void CSFightforDataGenerated_CSFightforData_monsterIdArr_m2700061538 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSFightforDataGenerated::CSFightforData_level(JSVCall)
extern "C"  void CSFightforDataGenerated_CSFightforData_level_m802906010 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSFightforDataGenerated::CSFightforData_country(JSVCall)
extern "C"  void CSFightforDataGenerated_CSFightforData_country_m1344352712 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSFightforDataGenerated::CSFightforData_ownCountry(JSVCall)
extern "C"  void CSFightforDataGenerated_CSFightforData_ownCountry_m300481598 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSFightforDataGenerated::CSFightforData_Clear(JSVCall,System.Int32)
extern "C"  bool CSFightforDataGenerated_CSFightforData_Clear_m4260233778 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSFightforDataGenerated::CSFightforData_GetCountryName(JSVCall,System.Int32)
extern "C"  bool CSFightforDataGenerated_CSFightforData_GetCountryName_m1995508936 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSFightforDataGenerated::CSFightforData_GetFightForOtherPlusAtt(JSVCall,System.Int32)
extern "C"  bool CSFightforDataGenerated_CSFightforData_GetFightForOtherPlusAtt_m602618187 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSFightforDataGenerated::CSFightforData_GetFightForPlusAtt(JSVCall,System.Int32)
extern "C"  bool CSFightforDataGenerated_CSFightforData_GetFightForPlusAtt_m1893641365 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSFightforDataGenerated::CSFightforData_LoadData__String(JSVCall,System.Int32)
extern "C"  bool CSFightforDataGenerated_CSFightforData_LoadData__String_m1380133118 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSFightforDataGenerated::CSFightforData_UnLoadData(JSVCall,System.Int32)
extern "C"  bool CSFightforDataGenerated_CSFightforData_UnLoadData_m3796018598 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSFightforDataGenerated::__Register()
extern "C"  void CSFightforDataGenerated___Register_m1967038328 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] CSFightforDataGenerated::<CSFightforData_monsterIdArr>m__22()
extern "C"  Int32U5BU5D_t3230847821* CSFightforDataGenerated_U3CCSFightforData_monsterIdArrU3Em__22_m3466701428 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSFightforDataGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t CSFightforDataGenerated_ilo_getObject1_m2846363879 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSFightforDataGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void CSFightforDataGenerated_ilo_addJSCSRel2_m1165640300 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSFightforDataGenerated::ilo_setStringS3(System.Int32,System.String)
extern "C"  void CSFightforDataGenerated_ilo_setStringS3_m2134041338 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CSFightforDataGenerated::ilo_getStringS4(System.Int32)
extern "C"  String_t* CSFightforDataGenerated_ilo_getStringS4_m2761289558 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSFightforDataGenerated::ilo_setInt325(System.Int32,System.Int32)
extern "C"  void CSFightforDataGenerated_ilo_setInt325_m3718697091 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSFightforDataGenerated::ilo_getInt326(System.Int32)
extern "C"  int32_t CSFightforDataGenerated_ilo_getInt326_m410638103 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSPlusAtt> CSFightforDataGenerated::ilo_GetFightForOtherPlusAtt7(CSFightforData)
extern "C"  List_1_t341533415 * CSFightforDataGenerated_ilo_GetFightForOtherPlusAtt7_m740627054 (Il2CppObject * __this /* static, unused */, CSFightforData_t215334547 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSFightforDataGenerated::ilo_setObject8(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t CSFightforDataGenerated_ilo_setObject8_m1953979430 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSPlusAtt> CSFightforDataGenerated::ilo_GetFightForPlusAtt9(CSFightforData)
extern "C"  List_1_t341533415 * CSFightforDataGenerated_ilo_GetFightForPlusAtt9_m1158190912 (Il2CppObject * __this /* static, unused */, CSFightforData_t215334547 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
