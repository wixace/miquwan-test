﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Appegg/ServerConfig
struct  ServerConfig_t2996416080  : public Il2CppObject
{
public:
	// System.Int32 Appegg/ServerConfig::appType
	int32_t ___appType_0;
	// System.Int32 Appegg/ServerConfig::version
	int32_t ___version_1;
	// System.Int32 Appegg/ServerConfig::nativeVersion
	int32_t ___nativeVersion_2;
	// System.Boolean Appegg/ServerConfig::updateEnable
	bool ___updateEnable_3;
	// System.Boolean Appegg/ServerConfig::forceUpdate
	bool ___forceUpdate_4;
	// System.String Appegg/ServerConfig::url
	String_t* ___url_5;
	// System.String Appegg/ServerConfig::privacyUrl
	String_t* ___privacyUrl_6;
	// System.String Appegg/ServerConfig::updateTitle
	String_t* ___updateTitle_7;
	// System.String Appegg/ServerConfig::updateText
	String_t* ___updateText_8;
	// System.Int32 Appegg/ServerConfig::hybridVersion
	int32_t ___hybridVersion_9;
	// System.String Appegg/ServerConfig::hybridIndex
	String_t* ___hybridIndex_10;
	// System.String Appegg/ServerConfig::hybridUrl
	String_t* ___hybridUrl_11;
	// System.String Appegg/ServerConfig::nativeUrl
	String_t* ___nativeUrl_12;
	// System.String Appegg/ServerConfig::picUrl
	String_t* ___picUrl_13;

public:
	inline static int32_t get_offset_of_appType_0() { return static_cast<int32_t>(offsetof(ServerConfig_t2996416080, ___appType_0)); }
	inline int32_t get_appType_0() const { return ___appType_0; }
	inline int32_t* get_address_of_appType_0() { return &___appType_0; }
	inline void set_appType_0(int32_t value)
	{
		___appType_0 = value;
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(ServerConfig_t2996416080, ___version_1)); }
	inline int32_t get_version_1() const { return ___version_1; }
	inline int32_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(int32_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_nativeVersion_2() { return static_cast<int32_t>(offsetof(ServerConfig_t2996416080, ___nativeVersion_2)); }
	inline int32_t get_nativeVersion_2() const { return ___nativeVersion_2; }
	inline int32_t* get_address_of_nativeVersion_2() { return &___nativeVersion_2; }
	inline void set_nativeVersion_2(int32_t value)
	{
		___nativeVersion_2 = value;
	}

	inline static int32_t get_offset_of_updateEnable_3() { return static_cast<int32_t>(offsetof(ServerConfig_t2996416080, ___updateEnable_3)); }
	inline bool get_updateEnable_3() const { return ___updateEnable_3; }
	inline bool* get_address_of_updateEnable_3() { return &___updateEnable_3; }
	inline void set_updateEnable_3(bool value)
	{
		___updateEnable_3 = value;
	}

	inline static int32_t get_offset_of_forceUpdate_4() { return static_cast<int32_t>(offsetof(ServerConfig_t2996416080, ___forceUpdate_4)); }
	inline bool get_forceUpdate_4() const { return ___forceUpdate_4; }
	inline bool* get_address_of_forceUpdate_4() { return &___forceUpdate_4; }
	inline void set_forceUpdate_4(bool value)
	{
		___forceUpdate_4 = value;
	}

	inline static int32_t get_offset_of_url_5() { return static_cast<int32_t>(offsetof(ServerConfig_t2996416080, ___url_5)); }
	inline String_t* get_url_5() const { return ___url_5; }
	inline String_t** get_address_of_url_5() { return &___url_5; }
	inline void set_url_5(String_t* value)
	{
		___url_5 = value;
		Il2CppCodeGenWriteBarrier(&___url_5, value);
	}

	inline static int32_t get_offset_of_privacyUrl_6() { return static_cast<int32_t>(offsetof(ServerConfig_t2996416080, ___privacyUrl_6)); }
	inline String_t* get_privacyUrl_6() const { return ___privacyUrl_6; }
	inline String_t** get_address_of_privacyUrl_6() { return &___privacyUrl_6; }
	inline void set_privacyUrl_6(String_t* value)
	{
		___privacyUrl_6 = value;
		Il2CppCodeGenWriteBarrier(&___privacyUrl_6, value);
	}

	inline static int32_t get_offset_of_updateTitle_7() { return static_cast<int32_t>(offsetof(ServerConfig_t2996416080, ___updateTitle_7)); }
	inline String_t* get_updateTitle_7() const { return ___updateTitle_7; }
	inline String_t** get_address_of_updateTitle_7() { return &___updateTitle_7; }
	inline void set_updateTitle_7(String_t* value)
	{
		___updateTitle_7 = value;
		Il2CppCodeGenWriteBarrier(&___updateTitle_7, value);
	}

	inline static int32_t get_offset_of_updateText_8() { return static_cast<int32_t>(offsetof(ServerConfig_t2996416080, ___updateText_8)); }
	inline String_t* get_updateText_8() const { return ___updateText_8; }
	inline String_t** get_address_of_updateText_8() { return &___updateText_8; }
	inline void set_updateText_8(String_t* value)
	{
		___updateText_8 = value;
		Il2CppCodeGenWriteBarrier(&___updateText_8, value);
	}

	inline static int32_t get_offset_of_hybridVersion_9() { return static_cast<int32_t>(offsetof(ServerConfig_t2996416080, ___hybridVersion_9)); }
	inline int32_t get_hybridVersion_9() const { return ___hybridVersion_9; }
	inline int32_t* get_address_of_hybridVersion_9() { return &___hybridVersion_9; }
	inline void set_hybridVersion_9(int32_t value)
	{
		___hybridVersion_9 = value;
	}

	inline static int32_t get_offset_of_hybridIndex_10() { return static_cast<int32_t>(offsetof(ServerConfig_t2996416080, ___hybridIndex_10)); }
	inline String_t* get_hybridIndex_10() const { return ___hybridIndex_10; }
	inline String_t** get_address_of_hybridIndex_10() { return &___hybridIndex_10; }
	inline void set_hybridIndex_10(String_t* value)
	{
		___hybridIndex_10 = value;
		Il2CppCodeGenWriteBarrier(&___hybridIndex_10, value);
	}

	inline static int32_t get_offset_of_hybridUrl_11() { return static_cast<int32_t>(offsetof(ServerConfig_t2996416080, ___hybridUrl_11)); }
	inline String_t* get_hybridUrl_11() const { return ___hybridUrl_11; }
	inline String_t** get_address_of_hybridUrl_11() { return &___hybridUrl_11; }
	inline void set_hybridUrl_11(String_t* value)
	{
		___hybridUrl_11 = value;
		Il2CppCodeGenWriteBarrier(&___hybridUrl_11, value);
	}

	inline static int32_t get_offset_of_nativeUrl_12() { return static_cast<int32_t>(offsetof(ServerConfig_t2996416080, ___nativeUrl_12)); }
	inline String_t* get_nativeUrl_12() const { return ___nativeUrl_12; }
	inline String_t** get_address_of_nativeUrl_12() { return &___nativeUrl_12; }
	inline void set_nativeUrl_12(String_t* value)
	{
		___nativeUrl_12 = value;
		Il2CppCodeGenWriteBarrier(&___nativeUrl_12, value);
	}

	inline static int32_t get_offset_of_picUrl_13() { return static_cast<int32_t>(offsetof(ServerConfig_t2996416080, ___picUrl_13)); }
	inline String_t* get_picUrl_13() const { return ___picUrl_13; }
	inline String_t** get_address_of_picUrl_13() { return &___picUrl_13; }
	inline void set_picUrl_13(String_t* value)
	{
		___picUrl_13 = value;
		Il2CppCodeGenWriteBarrier(&___picUrl_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
