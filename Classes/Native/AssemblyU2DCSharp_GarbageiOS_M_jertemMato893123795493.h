﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_jertemMato89
struct  M_jertemMato89_t3123795493  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_jertemMato89::_samaPace
	int32_t ____samaPace_0;
	// System.Single GarbageiOS.M_jertemMato89::_nairecuMerhir
	float ____nairecuMerhir_1;
	// System.Int32 GarbageiOS.M_jertemMato89::_palsor
	int32_t ____palsor_2;
	// System.String GarbageiOS.M_jertemMato89::_kurtembaKeepigoo
	String_t* ____kurtembaKeepigoo_3;
	// System.Boolean GarbageiOS.M_jertemMato89::_kirtal
	bool ____kirtal_4;
	// System.Single GarbageiOS.M_jertemMato89::_riwawCukepas
	float ____riwawCukepas_5;
	// System.String GarbageiOS.M_jertemMato89::_keakarcu
	String_t* ____keakarcu_6;
	// System.Single GarbageiOS.M_jertemMato89::_reaboZerecur
	float ____reaboZerecur_7;
	// System.UInt32 GarbageiOS.M_jertemMato89::_cayveChouriwou
	uint32_t ____cayveChouriwou_8;
	// System.Single GarbageiOS.M_jertemMato89::_neberLallmar
	float ____neberLallmar_9;
	// System.String GarbageiOS.M_jertemMato89::_cayne
	String_t* ____cayne_10;
	// System.String GarbageiOS.M_jertemMato89::_calcisarRusayel
	String_t* ____calcisarRusayel_11;
	// System.Boolean GarbageiOS.M_jertemMato89::_morherbeaSartu
	bool ____morherbeaSartu_12;
	// System.Single GarbageiOS.M_jertemMato89::_mearsooStaltur
	float ____mearsooStaltur_13;

public:
	inline static int32_t get_offset_of__samaPace_0() { return static_cast<int32_t>(offsetof(M_jertemMato89_t3123795493, ____samaPace_0)); }
	inline int32_t get__samaPace_0() const { return ____samaPace_0; }
	inline int32_t* get_address_of__samaPace_0() { return &____samaPace_0; }
	inline void set__samaPace_0(int32_t value)
	{
		____samaPace_0 = value;
	}

	inline static int32_t get_offset_of__nairecuMerhir_1() { return static_cast<int32_t>(offsetof(M_jertemMato89_t3123795493, ____nairecuMerhir_1)); }
	inline float get__nairecuMerhir_1() const { return ____nairecuMerhir_1; }
	inline float* get_address_of__nairecuMerhir_1() { return &____nairecuMerhir_1; }
	inline void set__nairecuMerhir_1(float value)
	{
		____nairecuMerhir_1 = value;
	}

	inline static int32_t get_offset_of__palsor_2() { return static_cast<int32_t>(offsetof(M_jertemMato89_t3123795493, ____palsor_2)); }
	inline int32_t get__palsor_2() const { return ____palsor_2; }
	inline int32_t* get_address_of__palsor_2() { return &____palsor_2; }
	inline void set__palsor_2(int32_t value)
	{
		____palsor_2 = value;
	}

	inline static int32_t get_offset_of__kurtembaKeepigoo_3() { return static_cast<int32_t>(offsetof(M_jertemMato89_t3123795493, ____kurtembaKeepigoo_3)); }
	inline String_t* get__kurtembaKeepigoo_3() const { return ____kurtembaKeepigoo_3; }
	inline String_t** get_address_of__kurtembaKeepigoo_3() { return &____kurtembaKeepigoo_3; }
	inline void set__kurtembaKeepigoo_3(String_t* value)
	{
		____kurtembaKeepigoo_3 = value;
		Il2CppCodeGenWriteBarrier(&____kurtembaKeepigoo_3, value);
	}

	inline static int32_t get_offset_of__kirtal_4() { return static_cast<int32_t>(offsetof(M_jertemMato89_t3123795493, ____kirtal_4)); }
	inline bool get__kirtal_4() const { return ____kirtal_4; }
	inline bool* get_address_of__kirtal_4() { return &____kirtal_4; }
	inline void set__kirtal_4(bool value)
	{
		____kirtal_4 = value;
	}

	inline static int32_t get_offset_of__riwawCukepas_5() { return static_cast<int32_t>(offsetof(M_jertemMato89_t3123795493, ____riwawCukepas_5)); }
	inline float get__riwawCukepas_5() const { return ____riwawCukepas_5; }
	inline float* get_address_of__riwawCukepas_5() { return &____riwawCukepas_5; }
	inline void set__riwawCukepas_5(float value)
	{
		____riwawCukepas_5 = value;
	}

	inline static int32_t get_offset_of__keakarcu_6() { return static_cast<int32_t>(offsetof(M_jertemMato89_t3123795493, ____keakarcu_6)); }
	inline String_t* get__keakarcu_6() const { return ____keakarcu_6; }
	inline String_t** get_address_of__keakarcu_6() { return &____keakarcu_6; }
	inline void set__keakarcu_6(String_t* value)
	{
		____keakarcu_6 = value;
		Il2CppCodeGenWriteBarrier(&____keakarcu_6, value);
	}

	inline static int32_t get_offset_of__reaboZerecur_7() { return static_cast<int32_t>(offsetof(M_jertemMato89_t3123795493, ____reaboZerecur_7)); }
	inline float get__reaboZerecur_7() const { return ____reaboZerecur_7; }
	inline float* get_address_of__reaboZerecur_7() { return &____reaboZerecur_7; }
	inline void set__reaboZerecur_7(float value)
	{
		____reaboZerecur_7 = value;
	}

	inline static int32_t get_offset_of__cayveChouriwou_8() { return static_cast<int32_t>(offsetof(M_jertemMato89_t3123795493, ____cayveChouriwou_8)); }
	inline uint32_t get__cayveChouriwou_8() const { return ____cayveChouriwou_8; }
	inline uint32_t* get_address_of__cayveChouriwou_8() { return &____cayveChouriwou_8; }
	inline void set__cayveChouriwou_8(uint32_t value)
	{
		____cayveChouriwou_8 = value;
	}

	inline static int32_t get_offset_of__neberLallmar_9() { return static_cast<int32_t>(offsetof(M_jertemMato89_t3123795493, ____neberLallmar_9)); }
	inline float get__neberLallmar_9() const { return ____neberLallmar_9; }
	inline float* get_address_of__neberLallmar_9() { return &____neberLallmar_9; }
	inline void set__neberLallmar_9(float value)
	{
		____neberLallmar_9 = value;
	}

	inline static int32_t get_offset_of__cayne_10() { return static_cast<int32_t>(offsetof(M_jertemMato89_t3123795493, ____cayne_10)); }
	inline String_t* get__cayne_10() const { return ____cayne_10; }
	inline String_t** get_address_of__cayne_10() { return &____cayne_10; }
	inline void set__cayne_10(String_t* value)
	{
		____cayne_10 = value;
		Il2CppCodeGenWriteBarrier(&____cayne_10, value);
	}

	inline static int32_t get_offset_of__calcisarRusayel_11() { return static_cast<int32_t>(offsetof(M_jertemMato89_t3123795493, ____calcisarRusayel_11)); }
	inline String_t* get__calcisarRusayel_11() const { return ____calcisarRusayel_11; }
	inline String_t** get_address_of__calcisarRusayel_11() { return &____calcisarRusayel_11; }
	inline void set__calcisarRusayel_11(String_t* value)
	{
		____calcisarRusayel_11 = value;
		Il2CppCodeGenWriteBarrier(&____calcisarRusayel_11, value);
	}

	inline static int32_t get_offset_of__morherbeaSartu_12() { return static_cast<int32_t>(offsetof(M_jertemMato89_t3123795493, ____morherbeaSartu_12)); }
	inline bool get__morherbeaSartu_12() const { return ____morherbeaSartu_12; }
	inline bool* get_address_of__morherbeaSartu_12() { return &____morherbeaSartu_12; }
	inline void set__morherbeaSartu_12(bool value)
	{
		____morherbeaSartu_12 = value;
	}

	inline static int32_t get_offset_of__mearsooStaltur_13() { return static_cast<int32_t>(offsetof(M_jertemMato89_t3123795493, ____mearsooStaltur_13)); }
	inline float get__mearsooStaltur_13() const { return ____mearsooStaltur_13; }
	inline float* get_address_of__mearsooStaltur_13() { return &____mearsooStaltur_13; }
	inline void set__mearsooStaltur_13(float value)
	{
		____mearsooStaltur_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
