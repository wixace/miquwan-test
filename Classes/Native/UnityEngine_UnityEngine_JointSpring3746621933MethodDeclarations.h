﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.JointSpring
struct JointSpring_t3746621933;
struct JointSpring_t3746621933_marshaled_pinvoke;
struct JointSpring_t3746621933_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct JointSpring_t3746621933;
struct JointSpring_t3746621933_marshaled_pinvoke;

extern "C" void JointSpring_t3746621933_marshal_pinvoke(const JointSpring_t3746621933& unmarshaled, JointSpring_t3746621933_marshaled_pinvoke& marshaled);
extern "C" void JointSpring_t3746621933_marshal_pinvoke_back(const JointSpring_t3746621933_marshaled_pinvoke& marshaled, JointSpring_t3746621933& unmarshaled);
extern "C" void JointSpring_t3746621933_marshal_pinvoke_cleanup(JointSpring_t3746621933_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct JointSpring_t3746621933;
struct JointSpring_t3746621933_marshaled_com;

extern "C" void JointSpring_t3746621933_marshal_com(const JointSpring_t3746621933& unmarshaled, JointSpring_t3746621933_marshaled_com& marshaled);
extern "C" void JointSpring_t3746621933_marshal_com_back(const JointSpring_t3746621933_marshaled_com& marshaled, JointSpring_t3746621933& unmarshaled);
extern "C" void JointSpring_t3746621933_marshal_com_cleanup(JointSpring_t3746621933_marshaled_com& marshaled);
