﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginGuoPan
struct PluginGuoPan_t1529729231;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// Mihua.SDK.CreatRoleInfo
struct CreatRoleInfo_t2858846543;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Object
struct Il2CppObject;
// System.Action
struct Action_t3771233898;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t2362096582;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_PluginGuoPan1529729231.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginGuoPan::.ctor()
extern "C"  void PluginGuoPan__ctor_m2726730412 (PluginGuoPan_t1529729231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::Init()
extern "C"  void PluginGuoPan_Init_m1702063656 (PluginGuoPan_t1529729231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginGuoPan::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginGuoPan_ReqSDKHttpLogin_m3826707457 (PluginGuoPan_t1529729231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginGuoPan::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginGuoPan_IsLoginSuccess_m2583601959 (PluginGuoPan_t1529729231 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::OpenUserLogin()
extern "C"  void PluginGuoPan_OpenUserLogin_m3204468990 (PluginGuoPan_t1529729231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::UserPay(CEvent.ZEvent)
extern "C"  void PluginGuoPan_UserPay_m2782120116 (PluginGuoPan_t1529729231 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginGuoPan::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginGuoPan_BuildOrderParam2WWWForm_m3289935413 (PluginGuoPan_t1529729231 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::CreateRole(CEvent.ZEvent)
extern "C"  void PluginGuoPan_CreateRole_m4291601873 (PluginGuoPan_t1529729231 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::EnterGame(CEvent.ZEvent)
extern "C"  void PluginGuoPan_EnterGame_m69352775 (PluginGuoPan_t1529729231 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginGuoPan_RoleUpgrade_m2558970411 (PluginGuoPan_t1529729231 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::OnLoginSuccess(System.String)
extern "C"  void PluginGuoPan_OnLoginSuccess_m3056076977 (PluginGuoPan_t1529729231 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::OnLogoutSuccess(System.String)
extern "C"  void PluginGuoPan_OnLogoutSuccess_m252648446 (PluginGuoPan_t1529729231 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::OnLogout(System.String)
extern "C"  void PluginGuoPan_OnLogout_m2085266689 (PluginGuoPan_t1529729231 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::SwitchAccount(System.String)
extern "C"  void PluginGuoPan_SwitchAccount_m2984561407 (PluginGuoPan_t1529729231 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::initSdk(System.String,System.String)
extern "C"  void PluginGuoPan_initSdk_m272047882 (PluginGuoPan_t1529729231 * __this, String_t* ___aAppID0, String_t* ___aSkey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::login()
extern "C"  void PluginGuoPan_login_m2248745235 (PluginGuoPan_t1529729231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::logout()
extern "C"  void PluginGuoPan_logout_m997448258 (PluginGuoPan_t1529729231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::updateRoleInfo(System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginGuoPan_updateRoleInfo_m4022181869 (PluginGuoPan_t1529729231 * __this, String_t* ___roleID0, String_t* ___roleName1, String_t* ___serverName2, String_t* ___serverID3, String_t* ___roleLv4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginGuoPan_pay_m2136093144 (PluginGuoPan_t1529729231 * __this, String_t* ___orderID0, String_t* ___productID1, String_t* ___productName2, String_t* ___money3, String_t* ___count4, String_t* ___extra5, String_t* ___productDes6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::<OnLogoutSuccess>m__425()
extern "C"  void PluginGuoPan_U3COnLogoutSuccessU3Em__425_m3127658196 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::<OnLogout>m__426()
extern "C"  void PluginGuoPan_U3COnLogoutU3Em__426_m3855852604 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::<SwitchAccount>m__427()
extern "C"  void PluginGuoPan_U3CSwitchAccountU3Em__427_m3162006935 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginGuoPan_ilo_AddEventListener1_m630239432 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::ilo_initSdk2(PluginGuoPan,System.String,System.String)
extern "C"  void PluginGuoPan_ilo_initSdk2_m1147284060 (Il2CppObject * __this /* static, unused */, PluginGuoPan_t1529729231 * ____this0, String_t* ___aAppID1, String_t* ___aSkey2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginGuoPan::ilo_get_Instance3()
extern "C"  VersionMgr_t1322950208 * PluginGuoPan_ilo_get_Instance3_m3962561903 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginGuoPan::ilo_get_PluginsSdkMgr4()
extern "C"  PluginsSdkMgr_t3884624670 * PluginGuoPan_ilo_get_PluginsSdkMgr4_m2975483583 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginGuoPan::ilo_get_DeviceID5(PluginsSdkMgr)
extern "C"  String_t* PluginGuoPan_ilo_get_DeviceID5_m4191747269 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::ilo_OpenUserLogin6(PluginGuoPan)
extern "C"  void PluginGuoPan_ilo_OpenUserLogin6_m305642270 (Il2CppObject * __this /* static, unused */, PluginGuoPan_t1529729231 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::ilo_login7(PluginGuoPan)
extern "C"  void PluginGuoPan_ilo_login7_m4054762090 (Il2CppObject * __this /* static, unused */, PluginGuoPan_t1529729231 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.CreatRoleInfo PluginGuoPan::ilo_Parse8(System.Object[])
extern "C"  CreatRoleInfo_t2858846543 * PluginGuoPan_ilo_Parse8_m1252066002 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::ilo_updateRoleInfo9(PluginGuoPan,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginGuoPan_ilo_updateRoleInfo9_m607028544 (Il2CppObject * __this /* static, unused */, PluginGuoPan_t1529729231 * ____this0, String_t* ___roleID1, String_t* ___roleName2, String_t* ___serverName3, String_t* ___serverID4, String_t* ___roleLv5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::ilo_Log10(System.Object,System.Boolean)
extern "C"  void PluginGuoPan_ilo_Log10_m1870470595 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::ilo_Logout11(System.Action)
extern "C"  void PluginGuoPan_ilo_Logout11_m1035310184 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuoPan::ilo_ReqSDKHttpLogin12(PluginsSdkMgr)
extern "C"  void PluginGuoPan_ilo_ReqSDKHttpLogin12_m2062590199 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AndroidJavaObject PluginGuoPan::ilo_get_androidJavaObject13(PluginsSdkMgr)
extern "C"  AndroidJavaObject_t2362096582 * PluginGuoPan_ilo_get_androidJavaObject13_m1480426798 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
