﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.GridNode
struct GridNode_t3795753694;
// AstarPath
struct AstarPath_t4090270936;
// Pathfinding.GridGraph
struct GridGraph_t2455707914;
// Pathfinding.GraphNodeDelegate
struct GraphNodeDelegate_t1466738551;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// System.Collections.Generic.Stack`1<Pathfinding.GraphNode>
struct Stack_1_t3122173294;
// Pathfinding.Path
struct Path_t1974241691;
// Pathfinding.PathNode
struct PathNode_t417131581;
// Pathfinding.PathHandler
struct PathHandler_t918952263;
// Pathfinding.Serialization.GraphSerializationContext
struct GraphSerializationContext_t3256954663;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "AssemblyU2DCSharp_Pathfinding_GridGraph2455707914.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNodeDelegate1466738551.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_Pathfinding_PathNode417131581.h"
#include "AssemblyU2DCSharp_Pathfinding_PathHandler918952263.h"
#include "AssemblyU2DCSharp_Pathfinding_Serialization_GraphS3256954663.h"
#include "AssemblyU2DCSharp_Pathfinding_GridNode3795753694.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

// System.Void Pathfinding.GridNode::.ctor(AstarPath)
extern "C"  void GridNode__ctor_m2451552561 (GridNode_t3795753694 * __this, AstarPath_t4090270936 * ___astar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridNode::.cctor()
extern "C"  void GridNode__cctor_m4134429572 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.GridGraph Pathfinding.GridNode::GetGridGraph(System.UInt32)
extern "C"  GridGraph_t2455707914 * GridNode_GetGridGraph_m4180775320 (Il2CppObject * __this /* static, unused */, uint32_t ___graphIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridNode::SetGridGraph(System.Int32,Pathfinding.GridGraph)
extern "C"  void GridNode_SetGridGraph_m2443344128 (Il2CppObject * __this /* static, unused */, int32_t ___graphIndex0, GridGraph_t2455707914 * ___graph1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.GridNode::GetIndex()
extern "C"  int32_t GridNode_GetIndex_m902476805 (GridNode_t3795753694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 Pathfinding.GridNode::get_InternalGridFlags()
extern "C"  uint16_t GridNode_get_InternalGridFlags_m3319282861 (GridNode_t3795753694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridNode::set_InternalGridFlags(System.UInt16)
extern "C"  void GridNode_set_InternalGridFlags_m1992524294 (GridNode_t3795753694 * __this, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridNode::GetConnectionInternal(System.Int32)
extern "C"  bool GridNode_GetConnectionInternal_m3099514101 (GridNode_t3795753694 * __this, int32_t ___dir0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridNode::SetConnectionInternal(System.Int32,System.Boolean)
extern "C"  void GridNode_SetConnectionInternal_m3074016424 (GridNode_t3795753694 * __this, int32_t ___dir0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridNode::ResetConnectionsInternal()
extern "C"  void GridNode_ResetConnectionsInternal_m2859083390 (GridNode_t3795753694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridNode::get_EdgeNode()
extern "C"  bool GridNode_get_EdgeNode_m550374327 (GridNode_t3795753694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridNode::set_EdgeNode(System.Boolean)
extern "C"  void GridNode_set_EdgeNode_m1694462894 (GridNode_t3795753694 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridNode::get_WalkableErosion()
extern "C"  bool GridNode_get_WalkableErosion_m434847230 (GridNode_t3795753694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridNode::set_WalkableErosion(System.Boolean)
extern "C"  void GridNode_set_WalkableErosion_m3550316597 (GridNode_t3795753694 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridNode::get_TmpWalkable()
extern "C"  bool GridNode_get_TmpWalkable_m3773942500 (GridNode_t3795753694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridNode::set_TmpWalkable(System.Boolean)
extern "C"  void GridNode_set_TmpWalkable_m1103571611 (GridNode_t3795753694 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.GridNode::get_NodeInGridIndex()
extern "C"  int32_t GridNode_get_NodeInGridIndex_m1309659477 (GridNode_t3795753694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridNode::set_NodeInGridIndex(System.Int32)
extern "C"  void GridNode_set_NodeInGridIndex_m4117944576 (GridNode_t3795753694 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridNode::ClearConnections(System.Boolean)
extern "C"  void GridNode_ClearConnections_m4234178266 (GridNode_t3795753694 * __this, bool ___alsoReverse0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridNode::GetConnections(Pathfinding.GraphNodeDelegate)
extern "C"  void GridNode_GetConnections_m2899632749 (GridNode_t3795753694 * __this, GraphNodeDelegate_t1466738551 * ___del0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridNode::GetPortal(Pathfinding.GraphNode,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Boolean)
extern "C"  bool GridNode_GetPortal_m17674822 (GridNode_t3795753694 * __this, GraphNode_t23612370 * ___other0, List_1_t1355284822 * ___left1, List_1_t1355284822 * ___right2, bool ___backwards3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridNode::FloodFill(System.Collections.Generic.Stack`1<Pathfinding.GraphNode>,System.UInt32)
extern "C"  void GridNode_FloodFill_m1633931732 (GridNode_t3795753694 * __this, Stack_1_t3122173294 * ___stack0, uint32_t ___region1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridNode::AddConnection(Pathfinding.GraphNode,System.UInt32)
extern "C"  void GridNode_AddConnection_m4229684058 (GridNode_t3795753694 * __this, GraphNode_t23612370 * ___node0, uint32_t ___cost1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridNode::RemoveConnection(Pathfinding.GraphNode)
extern "C"  void GridNode_RemoveConnection_m3015527727 (GridNode_t3795753694 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridNode::UpdateRecursiveG(Pathfinding.Path,Pathfinding.PathNode,Pathfinding.PathHandler)
extern "C"  void GridNode_UpdateRecursiveG_m4090962204 (GridNode_t3795753694 * __this, Path_t1974241691 * ___path0, PathNode_t417131581 * ___pathNode1, PathHandler_t918952263 * ___handler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridNode::Open(Pathfinding.Path,Pathfinding.PathNode,Pathfinding.PathHandler)
extern "C"  void GridNode_Open_m270752336 (GridNode_t3795753694 * __this, Path_t1974241691 * ___path0, PathNode_t417131581 * ___pathNode1, PathHandler_t918952263 * ___handler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridNode::SerializeNode(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void GridNode_SerializeNode_m3034942212 (GridNode_t3795753694 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridNode::DeserializeNode(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void GridNode_DeserializeNode_m296723653 (GridNode_t3795753694 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.GridNode::ilo_get_GraphIndex1(Pathfinding.GraphNode)
extern "C"  uint32_t GridNode_ilo_get_GraphIndex1_m1466362831 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridNode::ilo_ResetConnectionsInternal2(Pathfinding.GridNode)
extern "C"  void GridNode_ilo_ResetConnectionsInternal2_m1801283953 (Il2CppObject * __this /* static, unused */, GridNode_t3795753694 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.GridGraph Pathfinding.GridNode::ilo_GetGridGraph3(System.UInt32)
extern "C"  GridGraph_t2455707914 * GridNode_ilo_GetGridGraph3_m119255322 (Il2CppObject * __this /* static, unused */, uint32_t ___graphIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridNode::ilo_Invoke4(Pathfinding.GraphNodeDelegate,Pathfinding.GraphNode)
extern "C"  void GridNode_ilo_Invoke4_m826854141 (Il2CppObject * __this /* static, unused */, GraphNodeDelegate_t1466738551 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.GridNode::ilo_op_Explicit5(Pathfinding.Int3)
extern "C"  Vector3_t4282066566  GridNode_ilo_op_Explicit5_m2878066269 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridNode::ilo_set_Area6(Pathfinding.GraphNode,System.UInt32)
extern "C"  void GridNode_ilo_set_Area6_m4276884384 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.PathNode Pathfinding.GridNode::ilo_GetPathNode7(Pathfinding.PathHandler,Pathfinding.GraphNode)
extern "C"  PathNode_t417131581 * GridNode_ilo_GetPathNode7_m3533093865 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridNode::ilo_UpdateRecursiveG8(Pathfinding.GridNode,Pathfinding.Path,Pathfinding.PathNode,Pathfinding.PathHandler)
extern "C"  void GridNode_ilo_UpdateRecursiveG8_m3511851531 (Il2CppObject * __this /* static, unused */, GridNode_t3795753694 * ____this0, Path_t1974241691 * ___path1, PathNode_t417131581 * ___pathNode2, PathHandler_t918952263 * ___handler3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridNode::ilo_set_cost9(Pathfinding.PathNode,System.UInt32)
extern "C"  void GridNode_ilo_set_cost9_m3460654470 (Il2CppObject * __this /* static, unused */, PathNode_t417131581 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.GridNode::ilo_GetTraversalCost10(Pathfinding.Path,Pathfinding.GraphNode)
extern "C"  uint32_t GridNode_ilo_GetTraversalCost10_m295591668 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.GridNode::ilo_get_G11(Pathfinding.PathNode)
extern "C"  uint32_t GridNode_ilo_get_G11_m1094466598 (Il2CppObject * __this /* static, unused */, PathNode_t417131581 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridNode::ilo_CanTraverse12(Pathfinding.Path,Pathfinding.GraphNode)
extern "C"  bool GridNode_ilo_CanTraverse12_m517734810 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.GridNode::ilo_CalculateHScore13(Pathfinding.Path,Pathfinding.GraphNode)
extern "C"  uint32_t GridNode_ilo_CalculateHScore13_m4243367810 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridNode::ilo_DeserializeNode14(Pathfinding.GraphNode,Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void GridNode_ilo_DeserializeNode14_m3114226651 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, GraphSerializationContext_t3256954663 * ___ctx1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
