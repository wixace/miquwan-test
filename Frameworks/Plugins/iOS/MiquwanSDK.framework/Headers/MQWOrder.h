//
//  MQWOrder.h
//  MiquwanSDK
//
//  Created by MiQuWang on 2019/11/6.
//  Copyright © 2019 LQX. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MQWOrder : NSObject


/// 产品ID，用来识别购买的产品
@property (nonatomic, strong)NSString *goodsID;

/// 产品名称
@property (nonatomic, strong)NSString *goodsName;

/// 产品订单号（游戏方的订单号）
@property (nonatomic, strong)NSString *cpOrderID;

/// 游戏币数量
@property (nonatomic, strong)NSNumber *count;

/// 充值金额(元)
@property (nonatomic, strong)NSNumber *money;


/// 厂商自定义参数（长度限制250个字符）
@property (nonatomic, strong)NSString *callBackInfo;




/// 创建预下单信息
/// @param goodsId      预下单产品ID
/// @param goodName     产品名称
/// @param oredrId      订单id
/// @param count        充值对应游戏货币（金币，元宝。。）数量
/// @param money        充值金额
/// @param callBackInfo 厂商自定义参数（长度限制250个字符）
+ (MQWOrder *)createOrderWithGoodsID:(NSString *)goodsId
                     goodsName:(NSString *)goodName
                     cpOrderID:(NSString *)oredrId
                         count:(NSNumber *)count
                         money:(NSNumber *)money
                  callBackInfo:(NSString *)callBackInfo;



/// 预下单参数信息
- (NSDictionary *)orderInfo ;
@end

NS_ASSUME_NONNULL_END
