﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginIqiyi
struct PluginIqiyi_t2544921470;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// VersionInfo
struct VersionInfo_t2356638086;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_PluginIqiyi2544921470.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void PluginIqiyi::.ctor()
extern "C"  void PluginIqiyi__ctor_m1340677549 (PluginIqiyi_t2544921470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::Init()
extern "C"  void PluginIqiyi_Init_m3458467591 (PluginIqiyi_t2544921470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginIqiyi::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginIqiyi_ReqSDKHttpLogin_m780969694 (PluginIqiyi_t2544921470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginIqiyi::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginIqiyi_IsLoginSuccess_m1514931998 (PluginIqiyi_t2544921470 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::OpenUserLogin()
extern "C"  void PluginIqiyi_OpenUserLogin_m2205434623 (PluginIqiyi_t2544921470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::UserPay(CEvent.ZEvent)
extern "C"  void PluginIqiyi_UserPay_m3461137299 (PluginIqiyi_t2544921470 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::OnEnterGame(CEvent.ZEvent)
extern "C"  void PluginIqiyi_OnEnterGame_m1072446437 (PluginIqiyi_t2544921470 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::OnCreateRole(CEvent.ZEvent)
extern "C"  void PluginIqiyi_OnCreateRole_m1027767027 (PluginIqiyi_t2544921470 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::OnInitSuccess(System.String)
extern "C"  void PluginIqiyi_OnInitSuccess_m4001867811 (PluginIqiyi_t2544921470 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::OnInitFail(System.String)
extern "C"  void PluginIqiyi_OnInitFail_m418673662 (PluginIqiyi_t2544921470 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::OnLoginSuccess(System.String)
extern "C"  void PluginIqiyi_OnLoginSuccess_m4188529906 (PluginIqiyi_t2544921470 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::OnUserSwitchSuccess(System.String)
extern "C"  void PluginIqiyi_OnUserSwitchSuccess_m2283527410 (PluginIqiyi_t2544921470 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::OnLoginFail(System.String)
extern "C"  void PluginIqiyi_OnLoginFail_m3018785167 (PluginIqiyi_t2544921470 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::OnPaySuccess(System.String)
extern "C"  void PluginIqiyi_OnPaySuccess_m3603356593 (PluginIqiyi_t2544921470 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::OnPayFail(System.String)
extern "C"  void PluginIqiyi_OnPayFail_m2303105840 (PluginIqiyi_t2544921470 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::OnExitGameSuccess(System.String)
extern "C"  void PluginIqiyi_OnExitGameSuccess_m4074472131 (PluginIqiyi_t2544921470 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::OnExitGameFail(System.String)
extern "C"  void PluginIqiyi_OnExitGameFail_m495951198 (PluginIqiyi_t2544921470 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::LogoutSuccess(System.String)
extern "C"  void PluginIqiyi_LogoutSuccess_m3776995294 (PluginIqiyi_t2544921470 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::OnLogoutSuccess(System.String)
extern "C"  void PluginIqiyi_OnLogoutSuccess_m998950877 (PluginIqiyi_t2544921470 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::OnLogoutFail(System.String)
extern "C"  void PluginIqiyi_OnLogoutFail_m3200908932 (PluginIqiyi_t2544921470 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::InitIqiyi(System.String)
extern "C"  void PluginIqiyi_InitIqiyi_m1533297942 (PluginIqiyi_t2544921470 * __this, String_t* ___gameid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::LoginSdk()
extern "C"  void PluginIqiyi_LoginSdk_m1419193032 (PluginIqiyi_t2544921470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::EnterToGame(System.String)
extern "C"  void PluginIqiyi_EnterToGame_m2817104114 (PluginIqiyi_t2544921470 * __this, String_t* ___serverId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::CreateRole(System.String)
extern "C"  void PluginIqiyi_CreateRole_m1114101017 (PluginIqiyi_t2544921470 * __this, String_t* ___serverId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::Pay(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginIqiyi_Pay_m3850909147 (PluginIqiyi_t2544921470 * __this, String_t* ___serverId0, String_t* ___roleid1, String_t* ___money2, String_t* ___productid3, String_t* ___orderid4, String_t* ___extra5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::clearData()
extern "C"  void PluginIqiyi_clearData_m340166594 (PluginIqiyi_t2544921470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::<OnLoginSuccess>m__488()
extern "C"  void PluginIqiyi_U3COnLoginSuccessU3Em__488_m1742501703 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginIqiyi_ilo_AddEventListener1_m2001392039 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginIqiyi::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginIqiyi_ilo_get_Instance2_m1374940681 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginIqiyi::ilo_get_currentVS3(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginIqiyi_ilo_get_currentVS3_m2889874339 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginIqiyi::ilo_get_PluginsSdkMgr4()
extern "C"  PluginsSdkMgr_t3884624670 * PluginIqiyi_ilo_get_PluginsSdkMgr4_m4282247124 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginIqiyi::ilo_get_DeviceID5(PluginsSdkMgr)
extern "C"  String_t* PluginIqiyi_ilo_get_DeviceID5_m1578015422 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProductsCfgMgr PluginIqiyi::ilo_get_ProductsCfgMgr6()
extern "C"  ProductsCfgMgr_t2493714872 * PluginIqiyi_ilo_get_ProductsCfgMgr6_m4226310130 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::ilo_Pay7(PluginIqiyi,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginIqiyi_ilo_Pay7_m3574914499 (Il2CppObject * __this /* static, unused */, PluginIqiyi_t2544921470 * ____this0, String_t* ___serverId1, String_t* ___roleid2, String_t* ___money3, String_t* ___productid4, String_t* ___orderid5, String_t* ___extra6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::ilo_EnterToGame8(PluginIqiyi,System.String)
extern "C"  void PluginIqiyi_ilo_EnterToGame8_m1122533395 (Il2CppObject * __this /* static, unused */, PluginIqiyi_t2544921470 * ____this0, String_t* ___serverId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::ilo_Log9(System.Object,System.Boolean)
extern "C"  void PluginIqiyi_ilo_Log9_m668669448 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::ilo_DispatchEvent10(CEvent.ZEvent)
extern "C"  void PluginIqiyi_ilo_DispatchEvent10_m2127848900 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginIqiyi::ilo_ReqSDKHttpLogin11(PluginsSdkMgr)
extern "C"  void PluginIqiyi_ilo_ReqSDKHttpLogin11_m465292215 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
