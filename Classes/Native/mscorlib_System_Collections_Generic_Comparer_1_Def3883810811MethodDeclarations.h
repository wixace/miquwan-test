﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Rendering.ReflectionProbeBlendInfo>
struct DefaultComparer_t3883810811;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeB1080597738.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Rendering.ReflectionProbeBlendInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m3125108697_gshared (DefaultComparer_t3883810811 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3125108697(__this, method) ((  void (*) (DefaultComparer_t3883810811 *, const MethodInfo*))DefaultComparer__ctor_m3125108697_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Rendering.ReflectionProbeBlendInfo>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m2911831358_gshared (DefaultComparer_t3883810811 * __this, ReflectionProbeBlendInfo_t1080597738  ___x0, ReflectionProbeBlendInfo_t1080597738  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m2911831358(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t3883810811 *, ReflectionProbeBlendInfo_t1080597738 , ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))DefaultComparer_Compare_m2911831358_gshared)(__this, ___x0, ___y1, method)
