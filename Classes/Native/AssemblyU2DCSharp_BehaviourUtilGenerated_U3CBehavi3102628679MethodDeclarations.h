﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtilGenerated/<BehaviourUtil_DelayCallT2__Single__ActionT2_T1_T2__T1__T2>c__AnonStorey48
struct U3CBehaviourUtil_DelayCallT2__Single__ActionT2_T1_T2__T1__T2U3Ec__AnonStorey48_t3102628679;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtilGenerated/<BehaviourUtil_DelayCallT2__Single__ActionT2_T1_T2__T1__T2>c__AnonStorey48::.ctor()
extern "C"  void U3CBehaviourUtil_DelayCallT2__Single__ActionT2_T1_T2__T1__T2U3Ec__AnonStorey48__ctor_m2856845828 (U3CBehaviourUtil_DelayCallT2__Single__ActionT2_T1_T2__T1__T2U3Ec__AnonStorey48_t3102628679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BehaviourUtilGenerated/<BehaviourUtil_DelayCallT2__Single__ActionT2_T1_T2__T1__T2>c__AnonStorey48::<>m__7()
extern "C"  Il2CppObject * U3CBehaviourUtil_DelayCallT2__Single__ActionT2_T1_T2__T1__T2U3Ec__AnonStorey48_U3CU3Em__7_m1280417623 (U3CBehaviourUtil_DelayCallT2__Single__ActionT2_T1_T2__T1__T2U3Ec__AnonStorey48_t3102628679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
