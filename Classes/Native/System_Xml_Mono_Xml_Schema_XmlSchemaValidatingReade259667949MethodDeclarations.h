﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XmlSchemaValidatingReader/<XmlSchemaValidatingReader>c__AnonStorey4
struct U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_t259667949;
// System.Object
struct Il2CppObject;
// System.Xml.Schema.ValidationEventArgs
struct ValidationEventArgs_t3260433748;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Xml_System_Xml_Schema_ValidationEventArgs3260433748.h"

// System.Void Mono.Xml.Schema.XmlSchemaValidatingReader/<XmlSchemaValidatingReader>c__AnonStorey4::.ctor()
extern "C"  void U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4__ctor_m2744126868 (U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_t259667949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Xml.Schema.XmlSchemaValidatingReader/<XmlSchemaValidatingReader>c__AnonStorey4::<>m__0()
extern "C"  Il2CppObject * U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_U3CU3Em__0_m2225477696 (U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_t259667949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.Schema.XmlSchemaValidatingReader/<XmlSchemaValidatingReader>c__AnonStorey4::<>m__1(System.Object,System.Xml.Schema.ValidationEventArgs)
extern "C"  void U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_U3CU3Em__1_m2450371683 (U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_t259667949 * __this, Il2CppObject * ___o0, ValidationEventArgs_t3260433748 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
