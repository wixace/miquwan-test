﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_SphereColliderGenerated
struct UnityEngine_SphereColliderGenerated_t4189028678;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_SphereColliderGenerated::.ctor()
extern "C"  void UnityEngine_SphereColliderGenerated__ctor_m3506557157 (UnityEngine_SphereColliderGenerated_t4189028678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SphereColliderGenerated::SphereCollider_SphereCollider1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SphereColliderGenerated_SphereCollider_SphereCollider1_m241544589 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SphereColliderGenerated::SphereCollider_center(JSVCall)
extern "C"  void UnityEngine_SphereColliderGenerated_SphereCollider_center_m535361585 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SphereColliderGenerated::SphereCollider_radius(JSVCall)
extern "C"  void UnityEngine_SphereColliderGenerated_SphereCollider_radius_m2032225428 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SphereColliderGenerated::__Register()
extern "C"  void UnityEngine_SphereColliderGenerated___Register_m2215080706 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_SphereColliderGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_SphereColliderGenerated_ilo_getObject1_m3436713329 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SphereColliderGenerated::ilo_setVector3S2(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_SphereColliderGenerated_ilo_setVector3S2_m2085824515 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
