﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow
struct OutputWindow_t617095569;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator
struct StreamManipulator_t2348681196;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi2348681196.h"

// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::Write(System.Int32)
extern "C"  void OutputWindow_Write_m2128171238 (OutputWindow_t617095569 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::SlowRepeat(System.Int32,System.Int32,System.Int32)
extern "C"  void OutputWindow_SlowRepeat_m3539234201 (OutputWindow_t617095569 * __this, int32_t ___repStart0, int32_t ___length1, int32_t ___distance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::Repeat(System.Int32,System.Int32)
extern "C"  void OutputWindow_Repeat_m2942755967 (OutputWindow_t617095569 * __this, int32_t ___length0, int32_t ___distance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::CopyStored(ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator,System.Int32)
extern "C"  int32_t OutputWindow_CopyStored_m495876610 (OutputWindow_t617095569 * __this, StreamManipulator_t2348681196 * ___input0, int32_t ___length1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::GetFreeSpace()
extern "C"  int32_t OutputWindow_GetFreeSpace_m3222198588 (OutputWindow_t617095569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::GetAvailable()
extern "C"  int32_t OutputWindow_GetAvailable_m136666699 (OutputWindow_t617095569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::CopyOutput(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t OutputWindow_CopyOutput_m3388051483 (OutputWindow_t617095569 * __this, ByteU5BU5D_t4260760469* ___output0, int32_t ___offset1, int32_t ___len2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::Reset()
extern "C"  void OutputWindow_Reset_m883473541 (OutputWindow_t617095569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::.ctor()
extern "C"  void OutputWindow__ctor_m3237040600 (OutputWindow_t617095569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
