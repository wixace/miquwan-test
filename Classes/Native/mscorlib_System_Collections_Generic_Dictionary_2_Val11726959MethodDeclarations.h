﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,System.Object>
struct ValueCollection_t11726959;
// System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>
struct Dictionary_2_t1311121246;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3537921950.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m3411917597_gshared (ValueCollection_t11726959 * __this, Dictionary_2_t1311121246 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m3411917597(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t11726959 *, Dictionary_2_t1311121246 *, const MethodInfo*))ValueCollection__ctor_m3411917597_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1503537525_gshared (ValueCollection_t11726959 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1503537525(__this, ___item0, method) ((  void (*) (ValueCollection_t11726959 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1503537525_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4196735166_gshared (ValueCollection_t11726959 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4196735166(__this, method) ((  void (*) (ValueCollection_t11726959 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4196735166_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3169795921_gshared (ValueCollection_t11726959 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3169795921(__this, ___item0, method) ((  bool (*) (ValueCollection_t11726959 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3169795921_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2390882358_gshared (ValueCollection_t11726959 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2390882358(__this, ___item0, method) ((  bool (*) (ValueCollection_t11726959 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2390882358_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1763429260_gshared (ValueCollection_t11726959 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1763429260(__this, method) ((  Il2CppObject* (*) (ValueCollection_t11726959 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1763429260_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m2392482882_gshared (ValueCollection_t11726959 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m2392482882(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t11726959 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2392482882_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2470319165_gshared (ValueCollection_t11726959 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2470319165(__this, method) ((  Il2CppObject * (*) (ValueCollection_t11726959 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2470319165_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1444971780_gshared (ValueCollection_t11726959 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1444971780(__this, method) ((  bool (*) (ValueCollection_t11726959 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1444971780_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2812489956_gshared (ValueCollection_t11726959 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2812489956(__this, method) ((  bool (*) (ValueCollection_t11726959 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2812489956_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m2820594832_gshared (ValueCollection_t11726959 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2820594832(__this, method) ((  Il2CppObject * (*) (ValueCollection_t11726959 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2820594832_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m1636360228_gshared (ValueCollection_t11726959 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m1636360228(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t11726959 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1636360228_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3537921950  ValueCollection_GetEnumerator_m3349137735_gshared (ValueCollection_t11726959 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m3349137735(__this, method) ((  Enumerator_t3537921950  (*) (ValueCollection_t11726959 *, const MethodInfo*))ValueCollection_GetEnumerator_m3349137735_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m2699976362_gshared (ValueCollection_t11726959 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m2699976362(__this, method) ((  int32_t (*) (ValueCollection_t11726959 *, const MethodInfo*))ValueCollection_get_Count_m2699976362_gshared)(__this, method)
