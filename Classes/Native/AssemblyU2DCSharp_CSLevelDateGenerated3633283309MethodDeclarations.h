﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSLevelDateGenerated
struct CSLevelDateGenerated_t3633283309;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void CSLevelDateGenerated::.ctor()
extern "C"  void CSLevelDateGenerated__ctor_m1342608462 (CSLevelDateGenerated_t3633283309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSLevelDateGenerated::CSLevelDate_CSLevelDate1(JSVCall,System.Int32)
extern "C"  bool CSLevelDateGenerated_CSLevelDate_CSLevelDate1_m3810062822 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSLevelDateGenerated::CSLevelDate_checkpointUnit(JSVCall)
extern "C"  void CSLevelDateGenerated_CSLevelDate_checkpointUnit_m275373928 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSLevelDateGenerated::CSLevelDate_LoadData__String(JSVCall,System.Int32)
extern "C"  bool CSLevelDateGenerated_CSLevelDate_LoadData__String_m3911311768 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSLevelDateGenerated::CSLevelDate_UnLoadData(JSVCall,System.Int32)
extern "C"  bool CSLevelDateGenerated_CSLevelDate_UnLoadData_m3528874432 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSLevelDateGenerated::__Register()
extern "C"  void CSLevelDateGenerated___Register_m1001482553 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSLevelDateGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t CSLevelDateGenerated_ilo_getObject1_m807170180 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CSLevelDateGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * CSLevelDateGenerated_ilo_getObject2_m449236834 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
