﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._c890798975ebcbf134b28c1f872b733c
struct _c890798975ebcbf134b28c1f872b733c_t2896253305;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__c890798975ebcbf134b28c1f2896253305.h"

// System.Void Little._c890798975ebcbf134b28c1f872b733c::.ctor()
extern "C"  void _c890798975ebcbf134b28c1f872b733c__ctor_m1740860628 (_c890798975ebcbf134b28c1f872b733c_t2896253305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c890798975ebcbf134b28c1f872b733c::_c890798975ebcbf134b28c1f872b733cm2(System.Int32)
extern "C"  int32_t _c890798975ebcbf134b28c1f872b733c__c890798975ebcbf134b28c1f872b733cm2_m4037039865 (_c890798975ebcbf134b28c1f872b733c_t2896253305 * __this, int32_t ____c890798975ebcbf134b28c1f872b733ca0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c890798975ebcbf134b28c1f872b733c::_c890798975ebcbf134b28c1f872b733cm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _c890798975ebcbf134b28c1f872b733c__c890798975ebcbf134b28c1f872b733cm_m363535325 (_c890798975ebcbf134b28c1f872b733c_t2896253305 * __this, int32_t ____c890798975ebcbf134b28c1f872b733ca0, int32_t ____c890798975ebcbf134b28c1f872b733c391, int32_t ____c890798975ebcbf134b28c1f872b733cc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c890798975ebcbf134b28c1f872b733c::ilo__c890798975ebcbf134b28c1f872b733cm21(Little._c890798975ebcbf134b28c1f872b733c,System.Int32)
extern "C"  int32_t _c890798975ebcbf134b28c1f872b733c_ilo__c890798975ebcbf134b28c1f872b733cm21_m2745504096 (Il2CppObject * __this /* static, unused */, _c890798975ebcbf134b28c1f872b733c_t2896253305 * ____this0, int32_t ____c890798975ebcbf134b28c1f872b733ca1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
