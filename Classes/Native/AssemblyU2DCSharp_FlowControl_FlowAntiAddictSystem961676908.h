﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_FlowControl_FlowBase3680091731.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlowControl.FlowAntiAddictSystem
struct  FlowAntiAddictSystem_t961676908  : public FlowBase_t3680091731
{
public:
	// System.Single FlowControl.FlowAntiAddictSystem::showTime
	float ___showTime_7;
	// System.Single FlowControl.FlowAntiAddictSystem::time
	float ___time_8;
	// System.Single FlowControl.FlowAntiAddictSystem::alpha
	float ___alpha_9;
	// System.Boolean FlowControl.FlowAntiAddictSystem::isAlphaCom
	bool ___isAlphaCom_10;
	// System.Boolean FlowControl.FlowAntiAddictSystem::isShow
	bool ___isShow_11;

public:
	inline static int32_t get_offset_of_showTime_7() { return static_cast<int32_t>(offsetof(FlowAntiAddictSystem_t961676908, ___showTime_7)); }
	inline float get_showTime_7() const { return ___showTime_7; }
	inline float* get_address_of_showTime_7() { return &___showTime_7; }
	inline void set_showTime_7(float value)
	{
		___showTime_7 = value;
	}

	inline static int32_t get_offset_of_time_8() { return static_cast<int32_t>(offsetof(FlowAntiAddictSystem_t961676908, ___time_8)); }
	inline float get_time_8() const { return ___time_8; }
	inline float* get_address_of_time_8() { return &___time_8; }
	inline void set_time_8(float value)
	{
		___time_8 = value;
	}

	inline static int32_t get_offset_of_alpha_9() { return static_cast<int32_t>(offsetof(FlowAntiAddictSystem_t961676908, ___alpha_9)); }
	inline float get_alpha_9() const { return ___alpha_9; }
	inline float* get_address_of_alpha_9() { return &___alpha_9; }
	inline void set_alpha_9(float value)
	{
		___alpha_9 = value;
	}

	inline static int32_t get_offset_of_isAlphaCom_10() { return static_cast<int32_t>(offsetof(FlowAntiAddictSystem_t961676908, ___isAlphaCom_10)); }
	inline bool get_isAlphaCom_10() const { return ___isAlphaCom_10; }
	inline bool* get_address_of_isAlphaCom_10() { return &___isAlphaCom_10; }
	inline void set_isAlphaCom_10(bool value)
	{
		___isAlphaCom_10 = value;
	}

	inline static int32_t get_offset_of_isShow_11() { return static_cast<int32_t>(offsetof(FlowAntiAddictSystem_t961676908, ___isShow_11)); }
	inline bool get_isShow_11() const { return ___isShow_11; }
	inline bool* get_address_of_isShow_11() { return &___isShow_11; }
	inline void set_isShow_11(bool value)
	{
		___isShow_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
