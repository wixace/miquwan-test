﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ScriptableObjectGenerated
struct UnityEngine_ScriptableObjectGenerated_t2200492355;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_ScriptableObjectGenerated::.ctor()
extern "C"  void UnityEngine_ScriptableObjectGenerated__ctor_m4033612680 (UnityEngine_ScriptableObjectGenerated_t2200492355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ScriptableObjectGenerated::.cctor()
extern "C"  void UnityEngine_ScriptableObjectGenerated__cctor_m5845285 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ScriptableObjectGenerated::ScriptableObject_ScriptableObject1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ScriptableObjectGenerated_ScriptableObject_ScriptableObject1_m2811275882 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ScriptableObjectGenerated::ScriptableObject_CreateInstance__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ScriptableObjectGenerated_ScriptableObject_CreateInstance__Type_m1880894160 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ScriptableObjectGenerated::ScriptableObject_CreateInstance__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ScriptableObjectGenerated_ScriptableObject_CreateInstance__String_m183255847 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ScriptableObjectGenerated::ScriptableObject_CreateInstanceT1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ScriptableObjectGenerated_ScriptableObject_CreateInstanceT1_m1010996275 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ScriptableObjectGenerated::__Register()
extern "C"  void UnityEngine_ScriptableObjectGenerated___Register_m2249680447 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ScriptableObjectGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_ScriptableObjectGenerated_ilo_addJSCSRel1_m523927108 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_ScriptableObjectGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_ScriptableObjectGenerated_ilo_getObject2_m1055225630 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_ScriptableObjectGenerated::ilo_getStringS3(System.Int32)
extern "C"  String_t* UnityEngine_ScriptableObjectGenerated_ilo_getStringS3_m2992552942 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
