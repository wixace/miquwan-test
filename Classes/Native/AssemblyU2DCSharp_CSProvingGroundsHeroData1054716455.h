﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CSHeroUnit
struct CSHeroUnit_t3764358446;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSProvingGroundsHeroData
struct  CSProvingGroundsHeroData_t1054716455  : public Il2CppObject
{
public:
	// CSHeroUnit CSProvingGroundsHeroData::heroUnit
	CSHeroUnit_t3764358446 * ___heroUnit_0;
	// System.UInt32 CSProvingGroundsHeroData::hp
	uint32_t ___hp_1;
	// System.UInt32 CSProvingGroundsHeroData::anger
	uint32_t ___anger_2;

public:
	inline static int32_t get_offset_of_heroUnit_0() { return static_cast<int32_t>(offsetof(CSProvingGroundsHeroData_t1054716455, ___heroUnit_0)); }
	inline CSHeroUnit_t3764358446 * get_heroUnit_0() const { return ___heroUnit_0; }
	inline CSHeroUnit_t3764358446 ** get_address_of_heroUnit_0() { return &___heroUnit_0; }
	inline void set_heroUnit_0(CSHeroUnit_t3764358446 * value)
	{
		___heroUnit_0 = value;
		Il2CppCodeGenWriteBarrier(&___heroUnit_0, value);
	}

	inline static int32_t get_offset_of_hp_1() { return static_cast<int32_t>(offsetof(CSProvingGroundsHeroData_t1054716455, ___hp_1)); }
	inline uint32_t get_hp_1() const { return ___hp_1; }
	inline uint32_t* get_address_of_hp_1() { return &___hp_1; }
	inline void set_hp_1(uint32_t value)
	{
		___hp_1 = value;
	}

	inline static int32_t get_offset_of_anger_2() { return static_cast<int32_t>(offsetof(CSProvingGroundsHeroData_t1054716455, ___anger_2)); }
	inline uint32_t get_anger_2() const { return ___anger_2; }
	inline uint32_t* get_address_of_anger_2() { return &___anger_2; }
	inline void set_anger_2(uint32_t value)
	{
		___anger_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
