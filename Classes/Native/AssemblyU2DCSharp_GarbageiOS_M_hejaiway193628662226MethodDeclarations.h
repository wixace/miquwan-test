﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_hejaiway19
struct M_hejaiway19_t3628662226;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_hejaiway193628662226.h"

// System.Void GarbageiOS.M_hejaiway19::.ctor()
extern "C"  void M_hejaiway19__ctor_m1029290657 (M_hejaiway19_t3628662226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hejaiway19::M_roubismair0(System.String[],System.Int32)
extern "C"  void M_hejaiway19_M_roubismair0_m675323203 (M_hejaiway19_t3628662226 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hejaiway19::M_taswhaSorchall1(System.String[],System.Int32)
extern "C"  void M_hejaiway19_M_taswhaSorchall1_m1710748645 (M_hejaiway19_t3628662226 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hejaiway19::M_nagedalRipibo2(System.String[],System.Int32)
extern "C"  void M_hejaiway19_M_nagedalRipibo2_m2428193699 (M_hejaiway19_t3628662226 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hejaiway19::M_hawhis3(System.String[],System.Int32)
extern "C"  void M_hejaiway19_M_hawhis3_m2119815011 (M_hejaiway19_t3628662226 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hejaiway19::M_lerasSairdee4(System.String[],System.Int32)
extern "C"  void M_hejaiway19_M_lerasSairdee4_m2756658934 (M_hejaiway19_t3628662226 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hejaiway19::M_maijeKouruno5(System.String[],System.Int32)
extern "C"  void M_hejaiway19_M_maijeKouruno5_m3932588564 (M_hejaiway19_t3628662226 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hejaiway19::M_calljearTrargowbow6(System.String[],System.Int32)
extern "C"  void M_hejaiway19_M_calljearTrargowbow6_m538985958 (M_hejaiway19_t3628662226 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hejaiway19::M_zemstaihirDilu7(System.String[],System.Int32)
extern "C"  void M_hejaiway19_M_zemstaihirDilu7_m2703077095 (M_hejaiway19_t3628662226 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hejaiway19::M_hupoutallTairvi8(System.String[],System.Int32)
extern "C"  void M_hejaiway19_M_hupoutallTairvi8_m2929855653 (M_hejaiway19_t3628662226 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hejaiway19::ilo_M_roubismair01(GarbageiOS.M_hejaiway19,System.String[],System.Int32)
extern "C"  void M_hejaiway19_ilo_M_roubismair01_m1996578191 (Il2CppObject * __this /* static, unused */, M_hejaiway19_t3628662226 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hejaiway19::ilo_M_taswhaSorchall12(GarbageiOS.M_hejaiway19,System.String[],System.Int32)
extern "C"  void M_hejaiway19_ilo_M_taswhaSorchall12_m1903863022 (Il2CppObject * __this /* static, unused */, M_hejaiway19_t3628662226 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hejaiway19::ilo_M_lerasSairdee43(GarbageiOS.M_hejaiway19,System.String[],System.Int32)
extern "C"  void M_hejaiway19_ilo_M_lerasSairdee43_m1802665214 (Il2CppObject * __this /* static, unused */, M_hejaiway19_t3628662226 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hejaiway19::ilo_M_zemstaihirDilu74(GarbageiOS.M_hejaiway19,System.String[],System.Int32)
extern "C"  void M_hejaiway19_ilo_M_zemstaihirDilu74_m2953353902 (Il2CppObject * __this /* static, unused */, M_hejaiway19_t3628662226 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
