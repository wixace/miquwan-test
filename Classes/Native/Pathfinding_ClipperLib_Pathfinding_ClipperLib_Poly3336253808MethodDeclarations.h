﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.ClipperLib.PolyNode
struct PolyNode_t3336253808;
// System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>
struct List_1_t399344435;
// System.Collections.Generic.List`1<Pathfinding.ClipperLib.PolyNode>
struct List_1_t409472064;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Poly3336253808.h"

// System.Void Pathfinding.ClipperLib.PolyNode::.ctor()
extern "C"  void PolyNode__ctor_m2522874301 (PolyNode_t3336253808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.ClipperLib.PolyNode::get_ChildCount()
extern "C"  int32_t PolyNode_get_ChildCount_m368656717 (PolyNode_t3336253808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint> Pathfinding.ClipperLib.PolyNode::get_Contour()
extern "C"  List_1_t399344435 * PolyNode_get_Contour_m2041513154 (PolyNode_t3336253808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.PolyNode::AddChild(Pathfinding.ClipperLib.PolyNode)
extern "C"  void PolyNode_AddChild_m4066781430 (PolyNode_t3336253808 * __this, PolyNode_t3336253808 * ___Child0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Pathfinding.ClipperLib.PolyNode> Pathfinding.ClipperLib.PolyNode::get_Childs()
extern "C"  List_1_t409472064 * PolyNode_get_Childs_m2605215012 (PolyNode_t3336253808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.PolyNode::set_IsOpen(System.Boolean)
extern "C"  void PolyNode_set_IsOpen_m4104302511 (PolyNode_t3336253808 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
