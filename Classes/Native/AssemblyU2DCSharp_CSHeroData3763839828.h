﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CSHeroUnit[]
struct CSHeroUnitU5BU5D_t1342235227;
// System.Collections.Generic.Dictionary`2<System.UInt32,CSHeroUnit>
struct Dictionary_2_t3761508424;
// System.Collections.Generic.List`1<CSPlusAtt>
struct List_1_t341533415;
// System.Collections.Generic.Dictionary`2<System.String,CSPlusAtt>
struct Dictionary_2_t4088733529;
// System.Comparison`1<CSHeroUnit>
struct Comparison_1_t2480719633;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSHeroData
struct  CSHeroData_t3763839828  : public Il2CppObject
{
public:
	// CSHeroUnit[] CSHeroData::_heroList
	CSHeroUnitU5BU5D_t1342235227* ____heroList_0;
	// System.Collections.Generic.Dictionary`2<System.UInt32,CSHeroUnit> CSHeroData::_heroDic
	Dictionary_2_t3761508424 * ____heroDic_1;
	// System.Collections.Generic.List`1<CSPlusAtt> CSHeroData::leaderPlusAttList
	List_1_t341533415 * ___leaderPlusAttList_2;
	// System.Collections.Generic.Dictionary`2<System.String,CSPlusAtt> CSHeroData::leaderPlusAttDic
	Dictionary_2_t4088733529 * ___leaderPlusAttDic_3;
	// System.Collections.Generic.List`1<CSPlusAtt> CSHeroData::reinPlusAttList
	List_1_t341533415 * ___reinPlusAttList_4;
	// System.Collections.Generic.Dictionary`2<System.String,CSPlusAtt> CSHeroData::reinPlusAttDic
	Dictionary_2_t4088733529 * ___reinPlusAttDic_5;

public:
	inline static int32_t get_offset_of__heroList_0() { return static_cast<int32_t>(offsetof(CSHeroData_t3763839828, ____heroList_0)); }
	inline CSHeroUnitU5BU5D_t1342235227* get__heroList_0() const { return ____heroList_0; }
	inline CSHeroUnitU5BU5D_t1342235227** get_address_of__heroList_0() { return &____heroList_0; }
	inline void set__heroList_0(CSHeroUnitU5BU5D_t1342235227* value)
	{
		____heroList_0 = value;
		Il2CppCodeGenWriteBarrier(&____heroList_0, value);
	}

	inline static int32_t get_offset_of__heroDic_1() { return static_cast<int32_t>(offsetof(CSHeroData_t3763839828, ____heroDic_1)); }
	inline Dictionary_2_t3761508424 * get__heroDic_1() const { return ____heroDic_1; }
	inline Dictionary_2_t3761508424 ** get_address_of__heroDic_1() { return &____heroDic_1; }
	inline void set__heroDic_1(Dictionary_2_t3761508424 * value)
	{
		____heroDic_1 = value;
		Il2CppCodeGenWriteBarrier(&____heroDic_1, value);
	}

	inline static int32_t get_offset_of_leaderPlusAttList_2() { return static_cast<int32_t>(offsetof(CSHeroData_t3763839828, ___leaderPlusAttList_2)); }
	inline List_1_t341533415 * get_leaderPlusAttList_2() const { return ___leaderPlusAttList_2; }
	inline List_1_t341533415 ** get_address_of_leaderPlusAttList_2() { return &___leaderPlusAttList_2; }
	inline void set_leaderPlusAttList_2(List_1_t341533415 * value)
	{
		___leaderPlusAttList_2 = value;
		Il2CppCodeGenWriteBarrier(&___leaderPlusAttList_2, value);
	}

	inline static int32_t get_offset_of_leaderPlusAttDic_3() { return static_cast<int32_t>(offsetof(CSHeroData_t3763839828, ___leaderPlusAttDic_3)); }
	inline Dictionary_2_t4088733529 * get_leaderPlusAttDic_3() const { return ___leaderPlusAttDic_3; }
	inline Dictionary_2_t4088733529 ** get_address_of_leaderPlusAttDic_3() { return &___leaderPlusAttDic_3; }
	inline void set_leaderPlusAttDic_3(Dictionary_2_t4088733529 * value)
	{
		___leaderPlusAttDic_3 = value;
		Il2CppCodeGenWriteBarrier(&___leaderPlusAttDic_3, value);
	}

	inline static int32_t get_offset_of_reinPlusAttList_4() { return static_cast<int32_t>(offsetof(CSHeroData_t3763839828, ___reinPlusAttList_4)); }
	inline List_1_t341533415 * get_reinPlusAttList_4() const { return ___reinPlusAttList_4; }
	inline List_1_t341533415 ** get_address_of_reinPlusAttList_4() { return &___reinPlusAttList_4; }
	inline void set_reinPlusAttList_4(List_1_t341533415 * value)
	{
		___reinPlusAttList_4 = value;
		Il2CppCodeGenWriteBarrier(&___reinPlusAttList_4, value);
	}

	inline static int32_t get_offset_of_reinPlusAttDic_5() { return static_cast<int32_t>(offsetof(CSHeroData_t3763839828, ___reinPlusAttDic_5)); }
	inline Dictionary_2_t4088733529 * get_reinPlusAttDic_5() const { return ___reinPlusAttDic_5; }
	inline Dictionary_2_t4088733529 ** get_address_of_reinPlusAttDic_5() { return &___reinPlusAttDic_5; }
	inline void set_reinPlusAttDic_5(Dictionary_2_t4088733529 * value)
	{
		___reinPlusAttDic_5 = value;
		Il2CppCodeGenWriteBarrier(&___reinPlusAttDic_5, value);
	}
};

struct CSHeroData_t3763839828_StaticFields
{
public:
	// System.Comparison`1<CSHeroUnit> CSHeroData::<>f__am$cache6
	Comparison_1_t2480719633 * ___U3CU3Ef__amU24cache6_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(CSHeroData_t3763839828_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline Comparison_1_t2480719633 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline Comparison_1_t2480719633 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(Comparison_1_t2480719633 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
