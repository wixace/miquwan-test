﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_SystemInfoGenerated
struct UnityEngine_SystemInfoGenerated_t4282525738;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"

// System.Void UnityEngine_SystemInfoGenerated::.ctor()
extern "C"  void UnityEngine_SystemInfoGenerated__ctor_m2392088193 (UnityEngine_SystemInfoGenerated_t4282525738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SystemInfoGenerated::SystemInfo_SystemInfo1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SystemInfoGenerated_SystemInfo_SystemInfo1_m1117084657 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_operatingSystem(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_operatingSystem_m738725078 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_processorType(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_processorType_m3850419258 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_processorFrequency(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_processorFrequency_m2723745724 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_processorCount(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_processorCount_m2316998249 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_systemMemorySize(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_systemMemorySize_m2246859861 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_graphicsMemorySize(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_graphicsMemorySize_m857847417 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_graphicsDeviceName(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_graphicsDeviceName_m633964538 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_graphicsDeviceVendor(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_graphicsDeviceVendor_m14579229 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_graphicsDeviceID(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_graphicsDeviceID_m1824673610 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_graphicsDeviceVendorID(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_graphicsDeviceVendorID_m774353314 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_graphicsDeviceType(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_graphicsDeviceType_m875644971 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_graphicsDeviceVersion(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_graphicsDeviceVersion_m2740855471 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_graphicsShaderLevel(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_graphicsShaderLevel_m1979655442 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_graphicsMultiThreaded(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_graphicsMultiThreaded_m3731560399 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_supportsShadows(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_supportsShadows_m4146428503 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_supportsRawShadowDepthSampling(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_supportsRawShadowDepthSampling_m2601125696 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_supportsRenderTextures(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_supportsRenderTextures_m1929722772 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_supportsRenderToCubemap(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_supportsRenderToCubemap_m2096944116 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_supportsImageEffects(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_supportsImageEffects_m4203331067 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_supports3DTextures(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_supports3DTextures_m517430841 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_supportsComputeShaders(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_supportsComputeShaders_m14371627 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_supportsInstancing(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_supportsInstancing_m1740845168 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_supportsSparseTextures(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_supportsSparseTextures_m3488872522 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_supportedRenderTargetCount(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_supportedRenderTargetCount_m485647148 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_supportsStencil(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_supportsStencil_m3610000910 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_npotSupport(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_npotSupport_m3968635902 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_deviceUniqueIdentifier(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_deviceUniqueIdentifier_m3476186518 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_deviceName(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_deviceName_m1545675909 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_deviceModel(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_deviceModel_m396333203 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_supportsAccelerometer(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_supportsAccelerometer_m2515873243 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_supportsGyroscope(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_supportsGyroscope_m2635063269 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_supportsLocationService(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_supportsLocationService_m1264771850 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_supportsVibration(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_supportsVibration_m3558099192 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_deviceType(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_deviceType_m1787356342 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::SystemInfo_maxTextureSize(JSVCall)
extern "C"  void UnityEngine_SystemInfoGenerated_SystemInfo_maxTextureSize_m1135543470 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SystemInfoGenerated::SystemInfo_SupportsRenderTextureFormat__RenderTextureFormat(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SystemInfoGenerated_SystemInfo_SupportsRenderTextureFormat__RenderTextureFormat_m1782134825 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SystemInfoGenerated::SystemInfo_SupportsTextureFormat__TextureFormat(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SystemInfoGenerated_SystemInfo_SupportsTextureFormat__TextureFormat_m1774711913 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::__Register()
extern "C"  void UnityEngine_SystemInfoGenerated___Register_m389613286 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::ilo_setStringS1(System.Int32,System.String)
extern "C"  void UnityEngine_SystemInfoGenerated_ilo_setStringS1_m3116844234 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::ilo_setInt322(System.Int32,System.Int32)
extern "C"  void UnityEngine_SystemInfoGenerated_ilo_setInt322_m930205908 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void UnityEngine_SystemInfoGenerated_ilo_setBooleanS3_m4119015280 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SystemInfoGenerated::ilo_setEnum4(System.Int32,System.Int32)
extern "C"  void UnityEngine_SystemInfoGenerated_ilo_setEnum4_m2083208767 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
