﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_vakeka17
struct  M_vakeka17_t1158370833  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_vakeka17::_chekemZimerewha
	String_t* ____chekemZimerewha_0;
	// System.UInt32 GarbageiOS.M_vakeka17::_zirjay
	uint32_t ____zirjay_1;
	// System.Boolean GarbageiOS.M_vakeka17::_triwoujere
	bool ____triwoujere_2;
	// System.String GarbageiOS.M_vakeka17::_taytemow
	String_t* ____taytemow_3;
	// System.Boolean GarbageiOS.M_vakeka17::_tulersiSaslee
	bool ____tulersiSaslee_4;
	// System.Int32 GarbageiOS.M_vakeka17::_posereTelce
	int32_t ____posereTelce_5;
	// System.Boolean GarbageiOS.M_vakeka17::_searotra
	bool ____searotra_6;
	// System.UInt32 GarbageiOS.M_vakeka17::_sodou
	uint32_t ____sodou_7;
	// System.Single GarbageiOS.M_vakeka17::_kimur
	float ____kimur_8;
	// System.Boolean GarbageiOS.M_vakeka17::_learmeChallwicur
	bool ____learmeChallwicur_9;
	// System.UInt32 GarbageiOS.M_vakeka17::_delzakearNawte
	uint32_t ____delzakearNawte_10;
	// System.Boolean GarbageiOS.M_vakeka17::_cistea
	bool ____cistea_11;
	// System.String GarbageiOS.M_vakeka17::_cudallRelpurca
	String_t* ____cudallRelpurca_12;
	// System.Single GarbageiOS.M_vakeka17::_luju
	float ____luju_13;
	// System.Single GarbageiOS.M_vakeka17::_xeqaJaspouxir
	float ____xeqaJaspouxir_14;
	// System.Boolean GarbageiOS.M_vakeka17::_triwi
	bool ____triwi_15;
	// System.String GarbageiOS.M_vakeka17::_palmasGaysu
	String_t* ____palmasGaysu_16;
	// System.UInt32 GarbageiOS.M_vakeka17::_dameljearBifowju
	uint32_t ____dameljearBifowju_17;
	// System.String GarbageiOS.M_vakeka17::_yeartor
	String_t* ____yeartor_18;
	// System.Boolean GarbageiOS.M_vakeka17::_sorsaVouwairsu
	bool ____sorsaVouwairsu_19;
	// System.String GarbageiOS.M_vakeka17::_ladaimisCaireaxay
	String_t* ____ladaimisCaireaxay_20;
	// System.Single GarbageiOS.M_vakeka17::_meatema
	float ____meatema_21;
	// System.Boolean GarbageiOS.M_vakeka17::_lowhiqay
	bool ____lowhiqay_22;
	// System.Boolean GarbageiOS.M_vakeka17::_daspooMayne
	bool ____daspooMayne_23;
	// System.Boolean GarbageiOS.M_vakeka17::_sougereBabayair
	bool ____sougereBabayair_24;
	// System.Int32 GarbageiOS.M_vakeka17::_bisbaw
	int32_t ____bisbaw_25;
	// System.String GarbageiOS.M_vakeka17::_treqistiSesomir
	String_t* ____treqistiSesomir_26;
	// System.UInt32 GarbageiOS.M_vakeka17::_sakodrairJebemal
	uint32_t ____sakodrairJebemal_27;
	// System.String GarbageiOS.M_vakeka17::_mebisall
	String_t* ____mebisall_28;
	// System.String GarbageiOS.M_vakeka17::_vasstalltal
	String_t* ____vasstalltal_29;
	// System.Single GarbageiOS.M_vakeka17::_xasiSetel
	float ____xasiSetel_30;
	// System.Single GarbageiOS.M_vakeka17::_cawvisCeepi
	float ____cawvisCeepi_31;
	// System.Boolean GarbageiOS.M_vakeka17::_coocisiKisdur
	bool ____coocisiKisdur_32;
	// System.UInt32 GarbageiOS.M_vakeka17::_ferpelJotallju
	uint32_t ____ferpelJotallju_33;
	// System.Boolean GarbageiOS.M_vakeka17::_sairbal
	bool ____sairbal_34;
	// System.UInt32 GarbageiOS.M_vakeka17::_valzai
	uint32_t ____valzai_35;
	// System.Int32 GarbageiOS.M_vakeka17::_niloxaw
	int32_t ____niloxaw_36;
	// System.Boolean GarbageiOS.M_vakeka17::_semkorKimem
	bool ____semkorKimem_37;
	// System.String GarbageiOS.M_vakeka17::_mekoo
	String_t* ____mekoo_38;
	// System.String GarbageiOS.M_vakeka17::_jawurmuPowrur
	String_t* ____jawurmuPowrur_39;
	// System.Single GarbageiOS.M_vakeka17::_seenar
	float ____seenar_40;
	// System.Single GarbageiOS.M_vakeka17::_yallnal
	float ____yallnal_41;
	// System.UInt32 GarbageiOS.M_vakeka17::_jenouStowdairgi
	uint32_t ____jenouStowdairgi_42;
	// System.UInt32 GarbageiOS.M_vakeka17::_zelrar
	uint32_t ____zelrar_43;
	// System.String GarbageiOS.M_vakeka17::_sarpeTawpi
	String_t* ____sarpeTawpi_44;
	// System.Int32 GarbageiOS.M_vakeka17::_drelerecea
	int32_t ____drelerecea_45;
	// System.Boolean GarbageiOS.M_vakeka17::_seezoukelMairwhorla
	bool ____seezoukelMairwhorla_46;
	// System.Boolean GarbageiOS.M_vakeka17::_koujobisDapeti
	bool ____koujobisDapeti_47;
	// System.Boolean GarbageiOS.M_vakeka17::_halemTassoo
	bool ____halemTassoo_48;

public:
	inline static int32_t get_offset_of__chekemZimerewha_0() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____chekemZimerewha_0)); }
	inline String_t* get__chekemZimerewha_0() const { return ____chekemZimerewha_0; }
	inline String_t** get_address_of__chekemZimerewha_0() { return &____chekemZimerewha_0; }
	inline void set__chekemZimerewha_0(String_t* value)
	{
		____chekemZimerewha_0 = value;
		Il2CppCodeGenWriteBarrier(&____chekemZimerewha_0, value);
	}

	inline static int32_t get_offset_of__zirjay_1() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____zirjay_1)); }
	inline uint32_t get__zirjay_1() const { return ____zirjay_1; }
	inline uint32_t* get_address_of__zirjay_1() { return &____zirjay_1; }
	inline void set__zirjay_1(uint32_t value)
	{
		____zirjay_1 = value;
	}

	inline static int32_t get_offset_of__triwoujere_2() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____triwoujere_2)); }
	inline bool get__triwoujere_2() const { return ____triwoujere_2; }
	inline bool* get_address_of__triwoujere_2() { return &____triwoujere_2; }
	inline void set__triwoujere_2(bool value)
	{
		____triwoujere_2 = value;
	}

	inline static int32_t get_offset_of__taytemow_3() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____taytemow_3)); }
	inline String_t* get__taytemow_3() const { return ____taytemow_3; }
	inline String_t** get_address_of__taytemow_3() { return &____taytemow_3; }
	inline void set__taytemow_3(String_t* value)
	{
		____taytemow_3 = value;
		Il2CppCodeGenWriteBarrier(&____taytemow_3, value);
	}

	inline static int32_t get_offset_of__tulersiSaslee_4() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____tulersiSaslee_4)); }
	inline bool get__tulersiSaslee_4() const { return ____tulersiSaslee_4; }
	inline bool* get_address_of__tulersiSaslee_4() { return &____tulersiSaslee_4; }
	inline void set__tulersiSaslee_4(bool value)
	{
		____tulersiSaslee_4 = value;
	}

	inline static int32_t get_offset_of__posereTelce_5() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____posereTelce_5)); }
	inline int32_t get__posereTelce_5() const { return ____posereTelce_5; }
	inline int32_t* get_address_of__posereTelce_5() { return &____posereTelce_5; }
	inline void set__posereTelce_5(int32_t value)
	{
		____posereTelce_5 = value;
	}

	inline static int32_t get_offset_of__searotra_6() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____searotra_6)); }
	inline bool get__searotra_6() const { return ____searotra_6; }
	inline bool* get_address_of__searotra_6() { return &____searotra_6; }
	inline void set__searotra_6(bool value)
	{
		____searotra_6 = value;
	}

	inline static int32_t get_offset_of__sodou_7() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____sodou_7)); }
	inline uint32_t get__sodou_7() const { return ____sodou_7; }
	inline uint32_t* get_address_of__sodou_7() { return &____sodou_7; }
	inline void set__sodou_7(uint32_t value)
	{
		____sodou_7 = value;
	}

	inline static int32_t get_offset_of__kimur_8() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____kimur_8)); }
	inline float get__kimur_8() const { return ____kimur_8; }
	inline float* get_address_of__kimur_8() { return &____kimur_8; }
	inline void set__kimur_8(float value)
	{
		____kimur_8 = value;
	}

	inline static int32_t get_offset_of__learmeChallwicur_9() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____learmeChallwicur_9)); }
	inline bool get__learmeChallwicur_9() const { return ____learmeChallwicur_9; }
	inline bool* get_address_of__learmeChallwicur_9() { return &____learmeChallwicur_9; }
	inline void set__learmeChallwicur_9(bool value)
	{
		____learmeChallwicur_9 = value;
	}

	inline static int32_t get_offset_of__delzakearNawte_10() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____delzakearNawte_10)); }
	inline uint32_t get__delzakearNawte_10() const { return ____delzakearNawte_10; }
	inline uint32_t* get_address_of__delzakearNawte_10() { return &____delzakearNawte_10; }
	inline void set__delzakearNawte_10(uint32_t value)
	{
		____delzakearNawte_10 = value;
	}

	inline static int32_t get_offset_of__cistea_11() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____cistea_11)); }
	inline bool get__cistea_11() const { return ____cistea_11; }
	inline bool* get_address_of__cistea_11() { return &____cistea_11; }
	inline void set__cistea_11(bool value)
	{
		____cistea_11 = value;
	}

	inline static int32_t get_offset_of__cudallRelpurca_12() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____cudallRelpurca_12)); }
	inline String_t* get__cudallRelpurca_12() const { return ____cudallRelpurca_12; }
	inline String_t** get_address_of__cudallRelpurca_12() { return &____cudallRelpurca_12; }
	inline void set__cudallRelpurca_12(String_t* value)
	{
		____cudallRelpurca_12 = value;
		Il2CppCodeGenWriteBarrier(&____cudallRelpurca_12, value);
	}

	inline static int32_t get_offset_of__luju_13() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____luju_13)); }
	inline float get__luju_13() const { return ____luju_13; }
	inline float* get_address_of__luju_13() { return &____luju_13; }
	inline void set__luju_13(float value)
	{
		____luju_13 = value;
	}

	inline static int32_t get_offset_of__xeqaJaspouxir_14() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____xeqaJaspouxir_14)); }
	inline float get__xeqaJaspouxir_14() const { return ____xeqaJaspouxir_14; }
	inline float* get_address_of__xeqaJaspouxir_14() { return &____xeqaJaspouxir_14; }
	inline void set__xeqaJaspouxir_14(float value)
	{
		____xeqaJaspouxir_14 = value;
	}

	inline static int32_t get_offset_of__triwi_15() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____triwi_15)); }
	inline bool get__triwi_15() const { return ____triwi_15; }
	inline bool* get_address_of__triwi_15() { return &____triwi_15; }
	inline void set__triwi_15(bool value)
	{
		____triwi_15 = value;
	}

	inline static int32_t get_offset_of__palmasGaysu_16() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____palmasGaysu_16)); }
	inline String_t* get__palmasGaysu_16() const { return ____palmasGaysu_16; }
	inline String_t** get_address_of__palmasGaysu_16() { return &____palmasGaysu_16; }
	inline void set__palmasGaysu_16(String_t* value)
	{
		____palmasGaysu_16 = value;
		Il2CppCodeGenWriteBarrier(&____palmasGaysu_16, value);
	}

	inline static int32_t get_offset_of__dameljearBifowju_17() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____dameljearBifowju_17)); }
	inline uint32_t get__dameljearBifowju_17() const { return ____dameljearBifowju_17; }
	inline uint32_t* get_address_of__dameljearBifowju_17() { return &____dameljearBifowju_17; }
	inline void set__dameljearBifowju_17(uint32_t value)
	{
		____dameljearBifowju_17 = value;
	}

	inline static int32_t get_offset_of__yeartor_18() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____yeartor_18)); }
	inline String_t* get__yeartor_18() const { return ____yeartor_18; }
	inline String_t** get_address_of__yeartor_18() { return &____yeartor_18; }
	inline void set__yeartor_18(String_t* value)
	{
		____yeartor_18 = value;
		Il2CppCodeGenWriteBarrier(&____yeartor_18, value);
	}

	inline static int32_t get_offset_of__sorsaVouwairsu_19() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____sorsaVouwairsu_19)); }
	inline bool get__sorsaVouwairsu_19() const { return ____sorsaVouwairsu_19; }
	inline bool* get_address_of__sorsaVouwairsu_19() { return &____sorsaVouwairsu_19; }
	inline void set__sorsaVouwairsu_19(bool value)
	{
		____sorsaVouwairsu_19 = value;
	}

	inline static int32_t get_offset_of__ladaimisCaireaxay_20() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____ladaimisCaireaxay_20)); }
	inline String_t* get__ladaimisCaireaxay_20() const { return ____ladaimisCaireaxay_20; }
	inline String_t** get_address_of__ladaimisCaireaxay_20() { return &____ladaimisCaireaxay_20; }
	inline void set__ladaimisCaireaxay_20(String_t* value)
	{
		____ladaimisCaireaxay_20 = value;
		Il2CppCodeGenWriteBarrier(&____ladaimisCaireaxay_20, value);
	}

	inline static int32_t get_offset_of__meatema_21() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____meatema_21)); }
	inline float get__meatema_21() const { return ____meatema_21; }
	inline float* get_address_of__meatema_21() { return &____meatema_21; }
	inline void set__meatema_21(float value)
	{
		____meatema_21 = value;
	}

	inline static int32_t get_offset_of__lowhiqay_22() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____lowhiqay_22)); }
	inline bool get__lowhiqay_22() const { return ____lowhiqay_22; }
	inline bool* get_address_of__lowhiqay_22() { return &____lowhiqay_22; }
	inline void set__lowhiqay_22(bool value)
	{
		____lowhiqay_22 = value;
	}

	inline static int32_t get_offset_of__daspooMayne_23() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____daspooMayne_23)); }
	inline bool get__daspooMayne_23() const { return ____daspooMayne_23; }
	inline bool* get_address_of__daspooMayne_23() { return &____daspooMayne_23; }
	inline void set__daspooMayne_23(bool value)
	{
		____daspooMayne_23 = value;
	}

	inline static int32_t get_offset_of__sougereBabayair_24() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____sougereBabayair_24)); }
	inline bool get__sougereBabayair_24() const { return ____sougereBabayair_24; }
	inline bool* get_address_of__sougereBabayair_24() { return &____sougereBabayair_24; }
	inline void set__sougereBabayair_24(bool value)
	{
		____sougereBabayair_24 = value;
	}

	inline static int32_t get_offset_of__bisbaw_25() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____bisbaw_25)); }
	inline int32_t get__bisbaw_25() const { return ____bisbaw_25; }
	inline int32_t* get_address_of__bisbaw_25() { return &____bisbaw_25; }
	inline void set__bisbaw_25(int32_t value)
	{
		____bisbaw_25 = value;
	}

	inline static int32_t get_offset_of__treqistiSesomir_26() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____treqistiSesomir_26)); }
	inline String_t* get__treqistiSesomir_26() const { return ____treqistiSesomir_26; }
	inline String_t** get_address_of__treqistiSesomir_26() { return &____treqistiSesomir_26; }
	inline void set__treqistiSesomir_26(String_t* value)
	{
		____treqistiSesomir_26 = value;
		Il2CppCodeGenWriteBarrier(&____treqistiSesomir_26, value);
	}

	inline static int32_t get_offset_of__sakodrairJebemal_27() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____sakodrairJebemal_27)); }
	inline uint32_t get__sakodrairJebemal_27() const { return ____sakodrairJebemal_27; }
	inline uint32_t* get_address_of__sakodrairJebemal_27() { return &____sakodrairJebemal_27; }
	inline void set__sakodrairJebemal_27(uint32_t value)
	{
		____sakodrairJebemal_27 = value;
	}

	inline static int32_t get_offset_of__mebisall_28() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____mebisall_28)); }
	inline String_t* get__mebisall_28() const { return ____mebisall_28; }
	inline String_t** get_address_of__mebisall_28() { return &____mebisall_28; }
	inline void set__mebisall_28(String_t* value)
	{
		____mebisall_28 = value;
		Il2CppCodeGenWriteBarrier(&____mebisall_28, value);
	}

	inline static int32_t get_offset_of__vasstalltal_29() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____vasstalltal_29)); }
	inline String_t* get__vasstalltal_29() const { return ____vasstalltal_29; }
	inline String_t** get_address_of__vasstalltal_29() { return &____vasstalltal_29; }
	inline void set__vasstalltal_29(String_t* value)
	{
		____vasstalltal_29 = value;
		Il2CppCodeGenWriteBarrier(&____vasstalltal_29, value);
	}

	inline static int32_t get_offset_of__xasiSetel_30() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____xasiSetel_30)); }
	inline float get__xasiSetel_30() const { return ____xasiSetel_30; }
	inline float* get_address_of__xasiSetel_30() { return &____xasiSetel_30; }
	inline void set__xasiSetel_30(float value)
	{
		____xasiSetel_30 = value;
	}

	inline static int32_t get_offset_of__cawvisCeepi_31() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____cawvisCeepi_31)); }
	inline float get__cawvisCeepi_31() const { return ____cawvisCeepi_31; }
	inline float* get_address_of__cawvisCeepi_31() { return &____cawvisCeepi_31; }
	inline void set__cawvisCeepi_31(float value)
	{
		____cawvisCeepi_31 = value;
	}

	inline static int32_t get_offset_of__coocisiKisdur_32() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____coocisiKisdur_32)); }
	inline bool get__coocisiKisdur_32() const { return ____coocisiKisdur_32; }
	inline bool* get_address_of__coocisiKisdur_32() { return &____coocisiKisdur_32; }
	inline void set__coocisiKisdur_32(bool value)
	{
		____coocisiKisdur_32 = value;
	}

	inline static int32_t get_offset_of__ferpelJotallju_33() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____ferpelJotallju_33)); }
	inline uint32_t get__ferpelJotallju_33() const { return ____ferpelJotallju_33; }
	inline uint32_t* get_address_of__ferpelJotallju_33() { return &____ferpelJotallju_33; }
	inline void set__ferpelJotallju_33(uint32_t value)
	{
		____ferpelJotallju_33 = value;
	}

	inline static int32_t get_offset_of__sairbal_34() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____sairbal_34)); }
	inline bool get__sairbal_34() const { return ____sairbal_34; }
	inline bool* get_address_of__sairbal_34() { return &____sairbal_34; }
	inline void set__sairbal_34(bool value)
	{
		____sairbal_34 = value;
	}

	inline static int32_t get_offset_of__valzai_35() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____valzai_35)); }
	inline uint32_t get__valzai_35() const { return ____valzai_35; }
	inline uint32_t* get_address_of__valzai_35() { return &____valzai_35; }
	inline void set__valzai_35(uint32_t value)
	{
		____valzai_35 = value;
	}

	inline static int32_t get_offset_of__niloxaw_36() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____niloxaw_36)); }
	inline int32_t get__niloxaw_36() const { return ____niloxaw_36; }
	inline int32_t* get_address_of__niloxaw_36() { return &____niloxaw_36; }
	inline void set__niloxaw_36(int32_t value)
	{
		____niloxaw_36 = value;
	}

	inline static int32_t get_offset_of__semkorKimem_37() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____semkorKimem_37)); }
	inline bool get__semkorKimem_37() const { return ____semkorKimem_37; }
	inline bool* get_address_of__semkorKimem_37() { return &____semkorKimem_37; }
	inline void set__semkorKimem_37(bool value)
	{
		____semkorKimem_37 = value;
	}

	inline static int32_t get_offset_of__mekoo_38() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____mekoo_38)); }
	inline String_t* get__mekoo_38() const { return ____mekoo_38; }
	inline String_t** get_address_of__mekoo_38() { return &____mekoo_38; }
	inline void set__mekoo_38(String_t* value)
	{
		____mekoo_38 = value;
		Il2CppCodeGenWriteBarrier(&____mekoo_38, value);
	}

	inline static int32_t get_offset_of__jawurmuPowrur_39() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____jawurmuPowrur_39)); }
	inline String_t* get__jawurmuPowrur_39() const { return ____jawurmuPowrur_39; }
	inline String_t** get_address_of__jawurmuPowrur_39() { return &____jawurmuPowrur_39; }
	inline void set__jawurmuPowrur_39(String_t* value)
	{
		____jawurmuPowrur_39 = value;
		Il2CppCodeGenWriteBarrier(&____jawurmuPowrur_39, value);
	}

	inline static int32_t get_offset_of__seenar_40() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____seenar_40)); }
	inline float get__seenar_40() const { return ____seenar_40; }
	inline float* get_address_of__seenar_40() { return &____seenar_40; }
	inline void set__seenar_40(float value)
	{
		____seenar_40 = value;
	}

	inline static int32_t get_offset_of__yallnal_41() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____yallnal_41)); }
	inline float get__yallnal_41() const { return ____yallnal_41; }
	inline float* get_address_of__yallnal_41() { return &____yallnal_41; }
	inline void set__yallnal_41(float value)
	{
		____yallnal_41 = value;
	}

	inline static int32_t get_offset_of__jenouStowdairgi_42() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____jenouStowdairgi_42)); }
	inline uint32_t get__jenouStowdairgi_42() const { return ____jenouStowdairgi_42; }
	inline uint32_t* get_address_of__jenouStowdairgi_42() { return &____jenouStowdairgi_42; }
	inline void set__jenouStowdairgi_42(uint32_t value)
	{
		____jenouStowdairgi_42 = value;
	}

	inline static int32_t get_offset_of__zelrar_43() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____zelrar_43)); }
	inline uint32_t get__zelrar_43() const { return ____zelrar_43; }
	inline uint32_t* get_address_of__zelrar_43() { return &____zelrar_43; }
	inline void set__zelrar_43(uint32_t value)
	{
		____zelrar_43 = value;
	}

	inline static int32_t get_offset_of__sarpeTawpi_44() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____sarpeTawpi_44)); }
	inline String_t* get__sarpeTawpi_44() const { return ____sarpeTawpi_44; }
	inline String_t** get_address_of__sarpeTawpi_44() { return &____sarpeTawpi_44; }
	inline void set__sarpeTawpi_44(String_t* value)
	{
		____sarpeTawpi_44 = value;
		Il2CppCodeGenWriteBarrier(&____sarpeTawpi_44, value);
	}

	inline static int32_t get_offset_of__drelerecea_45() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____drelerecea_45)); }
	inline int32_t get__drelerecea_45() const { return ____drelerecea_45; }
	inline int32_t* get_address_of__drelerecea_45() { return &____drelerecea_45; }
	inline void set__drelerecea_45(int32_t value)
	{
		____drelerecea_45 = value;
	}

	inline static int32_t get_offset_of__seezoukelMairwhorla_46() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____seezoukelMairwhorla_46)); }
	inline bool get__seezoukelMairwhorla_46() const { return ____seezoukelMairwhorla_46; }
	inline bool* get_address_of__seezoukelMairwhorla_46() { return &____seezoukelMairwhorla_46; }
	inline void set__seezoukelMairwhorla_46(bool value)
	{
		____seezoukelMairwhorla_46 = value;
	}

	inline static int32_t get_offset_of__koujobisDapeti_47() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____koujobisDapeti_47)); }
	inline bool get__koujobisDapeti_47() const { return ____koujobisDapeti_47; }
	inline bool* get_address_of__koujobisDapeti_47() { return &____koujobisDapeti_47; }
	inline void set__koujobisDapeti_47(bool value)
	{
		____koujobisDapeti_47 = value;
	}

	inline static int32_t get_offset_of__halemTassoo_48() { return static_cast<int32_t>(offsetof(M_vakeka17_t1158370833, ____halemTassoo_48)); }
	inline bool get__halemTassoo_48() const { return ____halemTassoo_48; }
	inline bool* get_address_of__halemTassoo_48() { return &____halemTassoo_48; }
	inline void set__halemTassoo_48(bool value)
	{
		____halemTassoo_48 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
