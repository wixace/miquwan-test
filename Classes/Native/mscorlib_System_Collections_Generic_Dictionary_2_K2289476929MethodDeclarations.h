﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>
struct Dictionary_2_t1674540875;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2289476929.h"
#include "AssemblyU2DCSharp_UIModelDisplayType1891752679.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UIModelDisplayType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1185499606_gshared (Enumerator_t2289476929 * __this, Dictionary_2_t1674540875 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1185499606(__this, ___host0, method) ((  void (*) (Enumerator_t2289476929 *, Dictionary_2_t1674540875 *, const MethodInfo*))Enumerator__ctor_m1185499606_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UIModelDisplayType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m987491925_gshared (Enumerator_t2289476929 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m987491925(__this, method) ((  Il2CppObject * (*) (Enumerator_t2289476929 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m987491925_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UIModelDisplayType,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m927796511_gshared (Enumerator_t2289476929 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m927796511(__this, method) ((  void (*) (Enumerator_t2289476929 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m927796511_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UIModelDisplayType,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m711315960_gshared (Enumerator_t2289476929 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m711315960(__this, method) ((  void (*) (Enumerator_t2289476929 *, const MethodInfo*))Enumerator_Dispose_m711315960_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UIModelDisplayType,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3085230671_gshared (Enumerator_t2289476929 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3085230671(__this, method) ((  bool (*) (Enumerator_t2289476929 *, const MethodInfo*))Enumerator_MoveNext_m3085230671_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UIModelDisplayType,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1988930793_gshared (Enumerator_t2289476929 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1988930793(__this, method) ((  int32_t (*) (Enumerator_t2289476929 *, const MethodInfo*))Enumerator_get_Current_m1988930793_gshared)(__this, method)
