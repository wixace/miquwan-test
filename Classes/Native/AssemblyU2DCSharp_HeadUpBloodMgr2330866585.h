﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HeadUpBloodMgr
struct HeadUpBloodMgr_t2330866585;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.Dictionary`2<System.String,UIFont>
struct Dictionary_2_t3323508805;
// System.Collections.Generic.List`1<NumEffect>
struct List_1_t4192407399;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Single>
struct Dictionary_2_t4289182211;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeadUpBloodMgr
struct  HeadUpBloodMgr_t2330866585  : public Il2CppObject
{
public:
	// UnityEngine.GameObject HeadUpBloodMgr::parentGO
	GameObject_t3674682005 * ___parentGO_1;
	// System.Collections.Generic.Dictionary`2<System.String,UIFont> HeadUpBloodMgr::fontDic
	Dictionary_2_t3323508805 * ___fontDic_2;
	// UnityEngine.GameObject HeadUpBloodMgr::dtGO
	GameObject_t3674682005 * ___dtGO_3;
	// System.Collections.Generic.List`1<NumEffect> HeadUpBloodMgr::freeList
	List_1_t4192407399 * ___freeList_4;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Single> HeadUpBloodMgr::ObjLastTime
	Dictionary_2_t4289182211 * ___ObjLastTime_6;
	// System.Collections.Generic.List`1<NumEffect> HeadUpBloodMgr::timesList
	List_1_t4192407399 * ___timesList_7;
	// System.Collections.Generic.List`1<NumEffect> HeadUpBloodMgr::updateList
	List_1_t4192407399 * ___updateList_8;

public:
	inline static int32_t get_offset_of_parentGO_1() { return static_cast<int32_t>(offsetof(HeadUpBloodMgr_t2330866585, ___parentGO_1)); }
	inline GameObject_t3674682005 * get_parentGO_1() const { return ___parentGO_1; }
	inline GameObject_t3674682005 ** get_address_of_parentGO_1() { return &___parentGO_1; }
	inline void set_parentGO_1(GameObject_t3674682005 * value)
	{
		___parentGO_1 = value;
		Il2CppCodeGenWriteBarrier(&___parentGO_1, value);
	}

	inline static int32_t get_offset_of_fontDic_2() { return static_cast<int32_t>(offsetof(HeadUpBloodMgr_t2330866585, ___fontDic_2)); }
	inline Dictionary_2_t3323508805 * get_fontDic_2() const { return ___fontDic_2; }
	inline Dictionary_2_t3323508805 ** get_address_of_fontDic_2() { return &___fontDic_2; }
	inline void set_fontDic_2(Dictionary_2_t3323508805 * value)
	{
		___fontDic_2 = value;
		Il2CppCodeGenWriteBarrier(&___fontDic_2, value);
	}

	inline static int32_t get_offset_of_dtGO_3() { return static_cast<int32_t>(offsetof(HeadUpBloodMgr_t2330866585, ___dtGO_3)); }
	inline GameObject_t3674682005 * get_dtGO_3() const { return ___dtGO_3; }
	inline GameObject_t3674682005 ** get_address_of_dtGO_3() { return &___dtGO_3; }
	inline void set_dtGO_3(GameObject_t3674682005 * value)
	{
		___dtGO_3 = value;
		Il2CppCodeGenWriteBarrier(&___dtGO_3, value);
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(HeadUpBloodMgr_t2330866585, ___freeList_4)); }
	inline List_1_t4192407399 * get_freeList_4() const { return ___freeList_4; }
	inline List_1_t4192407399 ** get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(List_1_t4192407399 * value)
	{
		___freeList_4 = value;
		Il2CppCodeGenWriteBarrier(&___freeList_4, value);
	}

	inline static int32_t get_offset_of_ObjLastTime_6() { return static_cast<int32_t>(offsetof(HeadUpBloodMgr_t2330866585, ___ObjLastTime_6)); }
	inline Dictionary_2_t4289182211 * get_ObjLastTime_6() const { return ___ObjLastTime_6; }
	inline Dictionary_2_t4289182211 ** get_address_of_ObjLastTime_6() { return &___ObjLastTime_6; }
	inline void set_ObjLastTime_6(Dictionary_2_t4289182211 * value)
	{
		___ObjLastTime_6 = value;
		Il2CppCodeGenWriteBarrier(&___ObjLastTime_6, value);
	}

	inline static int32_t get_offset_of_timesList_7() { return static_cast<int32_t>(offsetof(HeadUpBloodMgr_t2330866585, ___timesList_7)); }
	inline List_1_t4192407399 * get_timesList_7() const { return ___timesList_7; }
	inline List_1_t4192407399 ** get_address_of_timesList_7() { return &___timesList_7; }
	inline void set_timesList_7(List_1_t4192407399 * value)
	{
		___timesList_7 = value;
		Il2CppCodeGenWriteBarrier(&___timesList_7, value);
	}

	inline static int32_t get_offset_of_updateList_8() { return static_cast<int32_t>(offsetof(HeadUpBloodMgr_t2330866585, ___updateList_8)); }
	inline List_1_t4192407399 * get_updateList_8() const { return ___updateList_8; }
	inline List_1_t4192407399 ** get_address_of_updateList_8() { return &___updateList_8; }
	inline void set_updateList_8(List_1_t4192407399 * value)
	{
		___updateList_8 = value;
		Il2CppCodeGenWriteBarrier(&___updateList_8, value);
	}
};

struct HeadUpBloodMgr_t2330866585_StaticFields
{
public:
	// HeadUpBloodMgr HeadUpBloodMgr::_instance
	HeadUpBloodMgr_t2330866585 * ____instance_0;
	// System.String HeadUpBloodMgr::CLEAR_ALL
	String_t* ___CLEAR_ALL_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> HeadUpBloodMgr::<>f__switch$map1C
	Dictionary_2_t1974256870 * ___U3CU3Ef__switchU24map1C_9;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(HeadUpBloodMgr_t2330866585_StaticFields, ____instance_0)); }
	inline HeadUpBloodMgr_t2330866585 * get__instance_0() const { return ____instance_0; }
	inline HeadUpBloodMgr_t2330866585 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(HeadUpBloodMgr_t2330866585 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}

	inline static int32_t get_offset_of_CLEAR_ALL_5() { return static_cast<int32_t>(offsetof(HeadUpBloodMgr_t2330866585_StaticFields, ___CLEAR_ALL_5)); }
	inline String_t* get_CLEAR_ALL_5() const { return ___CLEAR_ALL_5; }
	inline String_t** get_address_of_CLEAR_ALL_5() { return &___CLEAR_ALL_5; }
	inline void set_CLEAR_ALL_5(String_t* value)
	{
		___CLEAR_ALL_5 = value;
		Il2CppCodeGenWriteBarrier(&___CLEAR_ALL_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1C_9() { return static_cast<int32_t>(offsetof(HeadUpBloodMgr_t2330866585_StaticFields, ___U3CU3Ef__switchU24map1C_9)); }
	inline Dictionary_2_t1974256870 * get_U3CU3Ef__switchU24map1C_9() const { return ___U3CU3Ef__switchU24map1C_9; }
	inline Dictionary_2_t1974256870 ** get_address_of_U3CU3Ef__switchU24map1C_9() { return &___U3CU3Ef__switchU24map1C_9; }
	inline void set_U3CU3Ef__switchU24map1C_9(Dictionary_2_t1974256870 * value)
	{
		___U3CU3Ef__switchU24map1C_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1C_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
