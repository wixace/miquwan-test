﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AvatarBuilder
struct AvatarBuilder_t3880291448;
// UnityEngine.Avatar
struct Avatar_t2688887197;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_HumanDescription1495620627.h"
#include "mscorlib_System_String7231557.h"

// System.Void UnityEngine.AvatarBuilder::.ctor()
extern "C"  void AvatarBuilder__ctor_m3819981847 (AvatarBuilder_t3880291448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Avatar UnityEngine.AvatarBuilder::BuildHumanAvatar(UnityEngine.GameObject,UnityEngine.HumanDescription)
extern "C"  Avatar_t2688887197 * AvatarBuilder_BuildHumanAvatar_m3958469218 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, HumanDescription_t1495620627  ___monoHumanDescription1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Avatar UnityEngine.AvatarBuilder::BuildHumanAvatarMono(UnityEngine.GameObject,UnityEngine.HumanDescription)
extern "C"  Avatar_t2688887197 * AvatarBuilder_BuildHumanAvatarMono_m1763797439 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, HumanDescription_t1495620627  ___monoHumanDescription1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Avatar UnityEngine.AvatarBuilder::INTERNAL_CALL_BuildHumanAvatarMono(UnityEngine.GameObject,UnityEngine.HumanDescription&)
extern "C"  Avatar_t2688887197 * AvatarBuilder_INTERNAL_CALL_BuildHumanAvatarMono_m455422508 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, HumanDescription_t1495620627 * ___monoHumanDescription1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Avatar UnityEngine.AvatarBuilder::BuildGenericAvatar(UnityEngine.GameObject,System.String)
extern "C"  Avatar_t2688887197 * AvatarBuilder_BuildGenericAvatar_m2199908356 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, String_t* ___rootMotionTransformName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
