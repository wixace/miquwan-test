﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AstarPath/<UpdateGraphsInteral>c__IteratorC
struct U3CUpdateGraphsInteralU3Ec__IteratorC_t880590588;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AstarPath/<UpdateGraphsInteral>c__IteratorC::.ctor()
extern "C"  void U3CUpdateGraphsInteralU3Ec__IteratorC__ctor_m1604990959 (U3CUpdateGraphsInteralU3Ec__IteratorC_t880590588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AstarPath/<UpdateGraphsInteral>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CUpdateGraphsInteralU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m712018499 (U3CUpdateGraphsInteralU3Ec__IteratorC_t880590588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AstarPath/<UpdateGraphsInteral>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CUpdateGraphsInteralU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m806187991 (U3CUpdateGraphsInteralU3Ec__IteratorC_t880590588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AstarPath/<UpdateGraphsInteral>c__IteratorC::MoveNext()
extern "C"  bool U3CUpdateGraphsInteralU3Ec__IteratorC_MoveNext_m1793087909 (U3CUpdateGraphsInteralU3Ec__IteratorC_t880590588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath/<UpdateGraphsInteral>c__IteratorC::Dispose()
extern "C"  void U3CUpdateGraphsInteralU3Ec__IteratorC_Dispose_m401335404 (U3CUpdateGraphsInteralU3Ec__IteratorC_t880590588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath/<UpdateGraphsInteral>c__IteratorC::Reset()
extern "C"  void U3CUpdateGraphsInteralU3Ec__IteratorC_Reset_m3546391196 (U3CUpdateGraphsInteralU3Ec__IteratorC_t880590588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
