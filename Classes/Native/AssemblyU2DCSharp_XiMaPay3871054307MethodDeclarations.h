﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// XiMaPay
struct XiMaPay_t3871054307;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// VersionMgr
struct VersionMgr_t1322950208;
// Mihua.SDK.SdkwwwCallBack
struct SdkwwwCallBack_t3004559384;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_XiMaPay3871054307.h"
#include "UnityEngine_UnityEngine_WWWForm461342257.h"
#include "AssemblyU2DCSharp_Mihua_SDK_SdkwwwCallBack3004559384.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void XiMaPay::.ctor(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void XiMaPay__ctor_m1729894086 (XiMaPay_t3871054307 * __this, String_t* ___partner_id0, String_t* ___app_id1, String_t* ___return_url2, String_t* ___key3, String_t* ___qn4, String_t* ___packageName5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String XiMaPay::BuildOrderParam()
extern "C"  String_t* XiMaPay_BuildOrderParam_m1470896086 (XiMaPay_t3871054307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm XiMaPay::BuildOrderParam2WWWForm()
extern "C"  WWWForm_t461342257 * XiMaPay_BuildOrderParam2WWWForm_m670698763 (XiMaPay_t3871054307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void XiMaPay::Tiaoqian(Mihua.SDK.PayInfo,System.String)
extern "C"  void XiMaPay_Tiaoqian_m3966381982 (XiMaPay_t3871054307 * __this, PayInfo_t1775308120 * ___payInfo0, String_t* ___payType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void XiMaPay::SignCallBack(System.Boolean,System.String)
extern "C"  void XiMaPay_SignCallBack_m1295395217 (XiMaPay_t3871054307 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void XiMaPay::WXSignCallBack(System.Boolean,System.String)
extern "C"  void XiMaPay_WXSignCallBack_m2044774514 (XiMaPay_t3871054307 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void XiMaPay::__IXmp(System.String,System.String,System.String,System.String,System.String)
extern "C"  void XiMaPay___IXmp_m3172681156 (XiMaPay_t3871054307 * __this, String_t* ___sign0, String_t* ___sign11, String_t* ___orderNo2, String_t* ___arg3, String_t* ___url4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void XiMaPay::__IXmwp(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void XiMaPay___IXmwp_m2718001733 (XiMaPay_t3871054307 * __this, String_t* ___appid0, String_t* ___partnerid1, String_t* ___key2, String_t* ___money3, String_t* ___outtradeno4, String_t* ___qn5, String_t* ___subject6, String_t* ___packageName7, String_t* ___sign8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm XiMaPay::ilo_BuildOrderParam2WWWForm1(XiMaPay)
extern "C"  WWWForm_t461342257 * XiMaPay_ilo_BuildOrderParam2WWWForm1_m3284642292 (Il2CppObject * __this /* static, unused */, XiMaPay_t3871054307 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr XiMaPay::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * XiMaPay_ilo_get_Instance2_m2461137732 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void XiMaPay::ilo_Request3(System.String,UnityEngine.WWWForm,Mihua.SDK.SdkwwwCallBack)
extern "C"  void XiMaPay_ilo_Request3_m1064095107 (Il2CppObject * __this /* static, unused */, String_t* ___url0, WWWForm_t461342257 * ___wwwForm1, SdkwwwCallBack_t3004559384 * ___callBack2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken XiMaPay::ilo_get_Item4(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  JToken_t3412245951 * XiMaPay_ilo_get_Item4_m2715972866 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean XiMaPay::ilo_ContainsKey5(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  bool XiMaPay_ilo_ContainsKey5_m3753341756 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void XiMaPay::ilo_Log6(System.Object,System.Boolean)
extern "C"  void XiMaPay_ilo_Log6_m1113763498 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
