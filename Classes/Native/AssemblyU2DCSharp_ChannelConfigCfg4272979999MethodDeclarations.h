﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChannelConfigCfg
struct ChannelConfigCfg_t4272979999;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void ChannelConfigCfg::.ctor()
extern "C"  void ChannelConfigCfg__ctor_m3172555100 (ChannelConfigCfg_t4272979999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChannelConfigCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void ChannelConfigCfg_Init_m3968311465 (ChannelConfigCfg_t4272979999 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
