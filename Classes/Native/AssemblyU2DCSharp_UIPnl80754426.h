﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIPanel
struct UIPanel_t295209936;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPnl
struct  UIPnl_t80754426  : public MonoBehaviour_t667441552
{
public:
	// UIPanel UIPnl::panel
	UIPanel_t295209936 * ___panel_2;
	// System.Int32 UIPnl::depth
	int32_t ___depth_3;

public:
	inline static int32_t get_offset_of_panel_2() { return static_cast<int32_t>(offsetof(UIPnl_t80754426, ___panel_2)); }
	inline UIPanel_t295209936 * get_panel_2() const { return ___panel_2; }
	inline UIPanel_t295209936 ** get_address_of_panel_2() { return &___panel_2; }
	inline void set_panel_2(UIPanel_t295209936 * value)
	{
		___panel_2 = value;
		Il2CppCodeGenWriteBarrier(&___panel_2, value);
	}

	inline static int32_t get_offset_of_depth_3() { return static_cast<int32_t>(offsetof(UIPnl_t80754426, ___depth_3)); }
	inline int32_t get_depth_3() const { return ___depth_3; }
	inline int32_t* get_address_of_depth_3() { return &___depth_3; }
	inline void set_depth_3(int32_t value)
	{
		___depth_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
