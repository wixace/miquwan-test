﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._dbb7061852931c11263fb3ed08bf3f37
struct _dbb7061852931c11263fb3ed08bf3f37_t2297138892;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._dbb7061852931c11263fb3ed08bf3f37::.ctor()
extern "C"  void _dbb7061852931c11263fb3ed08bf3f37__ctor_m1920826593 (_dbb7061852931c11263fb3ed08bf3f37_t2297138892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._dbb7061852931c11263fb3ed08bf3f37::_dbb7061852931c11263fb3ed08bf3f37m2(System.Int32)
extern "C"  int32_t _dbb7061852931c11263fb3ed08bf3f37__dbb7061852931c11263fb3ed08bf3f37m2_m1465663001 (_dbb7061852931c11263fb3ed08bf3f37_t2297138892 * __this, int32_t ____dbb7061852931c11263fb3ed08bf3f37a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._dbb7061852931c11263fb3ed08bf3f37::_dbb7061852931c11263fb3ed08bf3f37m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _dbb7061852931c11263fb3ed08bf3f37__dbb7061852931c11263fb3ed08bf3f37m_m2088466621 (_dbb7061852931c11263fb3ed08bf3f37_t2297138892 * __this, int32_t ____dbb7061852931c11263fb3ed08bf3f37a0, int32_t ____dbb7061852931c11263fb3ed08bf3f37441, int32_t ____dbb7061852931c11263fb3ed08bf3f37c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
