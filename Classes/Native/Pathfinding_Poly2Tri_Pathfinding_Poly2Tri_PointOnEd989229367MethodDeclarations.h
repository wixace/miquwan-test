﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Poly2Tri.PointOnEdgeException
struct PointOnEdgeException_t989229367;
// System.String
struct String_t;
// Pathfinding.Poly2Tri.TriangulationPoint
struct TriangulationPoint_t3810082933;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul3810082933.h"

// System.Void Pathfinding.Poly2Tri.PointOnEdgeException::.ctor(System.String,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  void PointOnEdgeException__ctor_m41689854 (PointOnEdgeException_t989229367 * __this, String_t* ___message0, TriangulationPoint_t3810082933 * ___a1, TriangulationPoint_t3810082933 * ___b2, TriangulationPoint_t3810082933 * ___c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
