﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}/$ArrayType=256
struct  U24ArrayTypeU3D256_t2175733764 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU3D256_t2175733764__padding[256];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}/$ArrayType=256
struct U24ArrayTypeU3D256_t2175733764_marshaled_pinvoke
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU3D256_t2175733764__padding[256];
	};
};
// Native definition for marshalling of: <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}/$ArrayType=256
struct U24ArrayTypeU3D256_t2175733764_marshaled_com
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU3D256_t2175733764__padding[256];
	};
};
