﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Guide
struct Guide_t69159644;
// Pathfinding.Path
struct Path_t1974241691;
// AIPath
struct AIPath_t1930792045;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_AIPath1930792045.h"

// System.Void Guide::.ctor()
extern "C"  void Guide__ctor_m2156328463 (Guide_t69159644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Guide::Start()
extern "C"  void Guide_Start_m1103466255 (Guide_t69159644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Guide::OnPathComplete(Pathfinding.Path)
extern "C"  void Guide_OnPathComplete_m3694862499 (Guide_t69159644 * __this, Path_t1974241691 * ____p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Guide::UpdateTargetPos(UnityEngine.Vector3)
extern "C"  void Guide_UpdateTargetPos_m2160707218 (Guide_t69159644 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Guide::getVect(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Guide_getVect_m1736007836 (Guide_t69159644 * __this, Vector3_t4282066566  ___currentPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Guide::ilo_OnPathComplete1(AIPath,Pathfinding.Path)
extern "C"  void Guide_ilo_OnPathComplete1_m2016827798 (Il2CppObject * __this /* static, unused */, AIPath_t1930792045 * ____this0, Path_t1974241691 * ____p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
