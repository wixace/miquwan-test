﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// UnityEngine.Object
struct Object_t3071478659;
// UnityEngine.CrashReport
struct CrashReport_t2450738801;
// UnityEngine.SocialPlatforms.IAchievementDescription
struct IAchievementDescription_t655461400;
// UnityEngine.SocialPlatforms.IAchievement
struct IAchievement_t2957812780;
// UnityEngine.SocialPlatforms.IScore
struct IScore_t4279057999;
// UnityEngine.SocialPlatforms.IUserProfile
struct IUserProfile_t598900827;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
struct AchievementDescription_t2116066607;
// UnityEngine.SocialPlatforms.Impl.UserProfile
struct UserProfile_t2280656072;
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
struct GcLeaderboard_t1820874799;
// UnityEngine.SocialPlatforms.Impl.Achievement
struct Achievement_t344600729;
// UnityEngine.SocialPlatforms.Impl.Score
struct Score_t3396031228;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.Component
struct Component_t3501516275;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.LightmapData
struct LightmapData_t3709883476;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.Texture
struct Texture_t2526458961;
// UnityEngine.Renderer
struct Renderer_t3076687687;
// UnityEngine.ProceduralPropertyDescription
struct ProceduralPropertyDescription_t3210075808;
// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.Behaviour
struct Behaviour_t200106419;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t1654378665;
// UnityEngine.Display
struct Display_t1321072632;
// UnityEngine.Light
struct Light_t4202674828;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t2362096582;
// UnityEngine.Experimental.Director.Playable
struct Playable_t70832698;
// UnityEngine.Collider
struct Collider_t2939674232;
// UnityEngine.CapsuleCollider
struct CapsuleCollider_t318617463;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t1743771669;
// UnityEngine.Collider2D
struct Collider2D_t1552025098;
// UnityEngine.AnimationClipPair
struct AnimationClipPair_t2917473764;
// UnityEngine.AnimationClip
struct AnimationClip_t2007702890;
// UnityEngine.Motion
struct Motion_t3026528250;
// UnityEngine.AnimationEvent
struct AnimationEvent_t3669457594;
// UnityEngine.AnimatorControllerParameter
struct AnimatorControllerParameter_t3339705788;
// UnityEngine.TreePrototype
struct TreePrototype_t4061220762;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// UnityEngine.GUIContent
struct GUIContent_t2094828418;
// UnityEngine.GUILayoutOption
struct GUILayoutOption_t331591504;
// UnityEngine.GUILayoutUtility/LayoutCache
struct LayoutCache_t879908455;
// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_t1336615025;
// UnityEngine.GUIStyle
struct GUIStyle_t2990928826;
// UnityEngine.Event
struct Event_t4196595728;
// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_t62111112;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_t3132250205;
// UnityEngine.RequireComponent
struct RequireComponent_t1687166108;
// UnityEngine.Events.PersistentCall
struct PersistentCall_t2972625667;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t1559630662;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// UnityEngine.Canvas
struct Canvas_t2727140764;
// UnityEngine.Font
struct Font_t4241557075;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3702418109;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;
// UnityEngine.Mesh
struct Mesh_t4241756145;
// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_t3986041494;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t667441552;
// UnityEngine.TextAsset
struct TextAsset_t3836129977;
// UnityEngine.MeshRenderer
struct MeshRenderer_t2804666580;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UnityEngine.MeshFilter
struct MeshFilter_t3839065225;
// UnityEngine.Terrain
struct Terrain_t2520838763;
// UnityEngine.GUITexture
struct GUITexture_t4020448292;
// UnityEngine.GUIElement
struct GUIElement_t3775428101;
// UnityEngine.AudioListener
struct AudioListener_t3685735200;
// UnityEngine.ParticleSystem
struct ParticleSystem_t381473177;
// UnityEngine.Shader
struct Shader_t3191267369;
// UnityEngine.AssetBundle
struct AssetBundle_t2070959688;
// UnityEngine.Animator
struct Animator_t2776330603;
// UnityEngine.Experimental.Director.IAnimatorControllerPlayable
struct IAnimatorControllerPlayable_t1679869568;
// UnityEngine.Experimental.Director.DirectorPlayer
struct DirectorPlayer_t2782999289;
// UnityEngine.Animation
struct Animation_t1724966010;
// UnityEngine.TrailRenderer
struct TrailRenderer_t2401074527;

#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_CrashReport2450738801.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie2116066607.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserP2280656072.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1820874799.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3481375915.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achiev344600729.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2181296590.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score3396031228.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"
#include "UnityEngine_UnityEngine_BoneWeight2802309920.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "UnityEngine_UnityEngine_CombineInstance1399074090.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeB1080597738.h"
#include "UnityEngine_UnityEngine_RenderBuffer3529837690.h"
#include "UnityEngine_UnityEngine_Rendering_RenderBufferLoad4243858466.h"
#include "UnityEngine_UnityEngine_Rendering_RenderBufferStor2198970271.h"
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmoni3881980607.h"
#include "UnityEngine_UnityEngine_LightmapData3709883476.h"
#include "UnityEngine_UnityEngine_Plane4206452690.h"
#include "UnityEngine_UnityEngine_Resolution1578306928.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_MeshSubsetCombineUtility_M4121966238.h"
#include "UnityEngine_UnityEngine_MeshSubsetCombineUtility_S2374839686.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_Renderer3076687687.h"
#include "UnityEngine_UnityEngine_LOD3134610167.h"
#include "UnityEngine_UnityEngine_GradientColorKey1060627152.h"
#include "UnityEngine_UnityEngine_GradientAlphaKey3609975541.h"
#include "UnityEngine_UnityEngine_Keyframe4079056114.h"
#include "UnityEngine_UnityEngine_ProceduralPropertyDescript3210075808.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_Behaviour200106419.h"
#include "UnityEngine_UnityEngine_Rendering_CommandBuffer1654378665.h"
#include "UnityEngine_UnityEngine_Display1321072632.h"
#include "UnityEngine_UnityEngine_AccelerationEvent146935280.h"
#include "UnityEngine_UnityEngine_Touch4210255029.h"
#include "UnityEngine_UnityEngine_Light4202674828.h"
#include "UnityEngine_UnityEngine_AndroidJavaObject2362096582.h"
#include "UnityEngine_UnityEngine_jvalue3862675307.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Playab70832698.h"
#include "UnityEngine_UnityEngine_ParticleSystem_Particle405273609.h"
#include "UnityEngine_UnityEngine_Particle465417482.h"
#include "UnityEngine_UnityEngine_ContactPoint243083348.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "UnityEngine_UnityEngine_ClothSkinningCoefficient2944055502.h"
#include "UnityEngine_UnityEngine_CapsuleCollider318617463.h"
#include "UnityEngine_UnityEngine_ClothSphereColliderPair2674965067.h"
#include "UnityEngine_UnityEngine_Rigidbody2D1743771669.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"
#include "UnityEngine_UnityEngine_ContactPoint2D4288432358.h"
#include "UnityEngine_UnityEngine_WebCamDevice3274004757.h"
#include "UnityEngine_UnityEngine_AnimationClipPair2917473764.h"
#include "UnityEngine_UnityEngine_AnimationClip2007702890.h"
#include "UnityEngine_UnityEngine_Motion3026528250.h"
#include "UnityEngine_UnityEngine_AnimationEvent3669457594.h"
#include "UnityEngine_UnityEngine_AnimatorControllerParamete3339705788.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo2746035113.h"
#include "UnityEngine_UnityEngine_HumanBone194476679.h"
#include "UnityEngine_UnityEngine_SkeletonBone421858229.h"
#include "UnityEngine_UnityEngine_TreeInstance2456306007.h"
#include "UnityEngine_UnityEngine_TreePrototype4061220762.h"
#include "UnityEngine_UnityEngine_UIVertex4244065212.h"
#include "UnityEngine_UnityEngine_UICharInfo65807484.h"
#include "UnityEngine_UnityEngine_UILineInfo4113875482.h"
#include "UnityEngine_UnityEngine_CharacterInfo2481726445.h"
#include "UnityEngine_UnityEngine_RectTransform972643934.h"
#include "UnityEngine_UnityEngine_GUIContent2094828418.h"
#include "UnityEngine_UnityEngine_GUILayoutOption331591504.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility_LayoutCach879908455.h"
#include "UnityEngine_UnityEngine_GUILayoutEntry1336615025.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp4145961110.h"
#include "UnityEngine_UnityEngine_Event4196595728.h"
#include "UnityEngine_UnityEngine_DisallowMultipleComponent62111112.h"
#include "UnityEngine_UnityEngine_ExecuteInEditMode3132250205.h"
#include "UnityEngine_UnityEngine_RequireComponent1687166108.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo3209134097.h"
#include "UnityEngine_UnityEngine_Events_PersistentCall2972625667.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall1559630662.h"
#include "UnityEngine_UnityEngine_Sprite3199167241.h"
#include "UnityEngine_UnityEngine_Canvas2727140764.h"
#include "UnityEngine_UnityEngine_Font4241557075.h"
#include "UnityEngine_UnityEngine_CanvasGroup3702418109.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"
#include "UnityEngine_UnityEngine_SkinnedMeshRenderer3986041494.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_TextAsset3836129977.h"
#include "UnityEngine_UnityEngine_MeshRenderer2804666580.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_MeshFilter3839065225.h"
#include "UnityEngine_UnityEngine_Terrain2520838763.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_GUITexture4020448292.h"
#include "UnityEngine_UnityEngine_GUIElement3775428101.h"
#include "UnityEngine_UnityEngine_RuntimePlatform3050318497.h"
#include "UnityEngine_UnityEngine_AudioListener3685735200.h"
#include "UnityEngine_UnityEngine_ParticleSystem381473177.h"
#include "UnityEngine_UnityEngine_Shader3191267369.h"
#include "UnityEngine_UnityEngine_AssetBundle2070959688.h"
#include "UnityEngine_UnityEngine_Animator2776330603.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Dire2782999289.h"
#include "UnityEngine_UnityEngine_Animation1724966010.h"
#include "UnityEngine_UnityEngine_TrailRenderer2401074527.h"

#pragma once
// UnityEngine.Object[]
struct ObjectU5BU5D_t1015136018  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Object_t3071478659 * m_Items[1];

public:
	inline Object_t3071478659 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Object_t3071478659 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Object_t3071478659 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.CrashReport[]
struct CrashReportU5BU5D_t612474028  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CrashReport_t2450738801 * m_Items[1];

public:
	inline CrashReport_t2450738801 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CrashReport_t2450738801 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CrashReport_t2450738801 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.SocialPlatforms.IAchievementDescription[]
struct IAchievementDescriptionU5BU5D_t4128901257  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.SocialPlatforms.IAchievement[]
struct IAchievementU5BU5D_t1953253797  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.SocialPlatforms.IScore[]
struct IScoreU5BU5D_t250104726  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.SocialPlatforms.IUserProfile[]
struct IUserProfileU5BU5D_t3419104218  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
struct AchievementDescriptionU5BU5D_t759444790  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AchievementDescription_t2116066607 * m_Items[1];

public:
	inline AchievementDescription_t2116066607 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AchievementDescription_t2116066607 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AchievementDescription_t2116066607 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t2378268441  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UserProfile_t2280656072 * m_Items[1];

public:
	inline UserProfile_t2280656072 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UserProfile_t2280656072 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UserProfile_t2280656072 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
struct GcLeaderboardU5BU5D_t1649880630  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GcLeaderboard_t1820874799 * m_Items[1];

public:
	inline GcLeaderboard_t1820874799 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GcLeaderboard_t1820874799 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GcLeaderboard_t1820874799 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
struct GcAchievementDataU5BU5D_t1726768202  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GcAchievementData_t3481375915  m_Items[1];

public:
	inline GcAchievementData_t3481375915  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GcAchievementData_t3481375915 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GcAchievementData_t3481375915  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.SocialPlatforms.Impl.Achievement[]
struct AchievementU5BU5D_t912418020  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Achievement_t344600729 * m_Items[1];

public:
	inline Achievement_t344600729 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Achievement_t344600729 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Achievement_t344600729 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
struct GcScoreDataU5BU5D_t1670395707  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GcScoreData_t2181296590  m_Items[1];

public:
	inline GcScoreData_t2181296590  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GcScoreData_t2181296590 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GcScoreData_t2181296590  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.SocialPlatforms.Impl.Score[]
struct ScoreU5BU5D_t2926278037  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Score_t3396031228 * m_Items[1];

public:
	inline Score_t3396031228 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Score_t3396031228 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Score_t3396031228 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Vector3_t4282066566  m_Items[1];

public:
	inline Vector3_t4282066566  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Vector3_t4282066566 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Vector3_t4282066566  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t701588350  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Vector4_t4282066567  m_Items[1];

public:
	inline Vector4_t4282066567  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Vector4_t4282066567 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Vector4_t4282066567  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t4024180168  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Vector2_t4282066565  m_Items[1];

public:
	inline Vector2_t4282066565  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Vector2_t4282066565 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Vector2_t4282066565  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Color_t4194546905  m_Items[1];

public:
	inline Color_t4194546905  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Color_t4194546905 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Color_t4194546905  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Color32[]
struct Color32U5BU5D_t2960766953  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Color32_t598853688  m_Items[1];

public:
	inline Color32_t598853688  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Color32_t598853688 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Color32_t598853688  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.BoneWeight[]
struct BoneWeightU5BU5D_t1503835233  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BoneWeight_t2802309920  m_Items[1];

public:
	inline BoneWeight_t2802309920  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BoneWeight_t2802309920 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BoneWeight_t2802309920  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t1421664456  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Matrix4x4_t1651859333  m_Items[1];

public:
	inline Matrix4x4_t1651859333  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Matrix4x4_t1651859333 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Matrix4x4_t1651859333  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.CombineInstance[]
struct CombineInstanceU5BU5D_t1850423023  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CombineInstance_t1399074090  m_Items[1];

public:
	inline CombineInstance_t1399074090  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CombineInstance_t1399074090 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CombineInstance_t1399074090  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Transform[]
struct TransformU5BU5D_t3792884695  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Transform_t1659122786 * m_Items[1];

public:
	inline Transform_t1659122786 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Transform_t1659122786 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Transform_t1659122786 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Component[]
struct ComponentU5BU5D_t663911650  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Component_t3501516275 * m_Items[1];

public:
	inline Component_t3501516275 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Component_t3501516275 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Component_t3501516275 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Material[]
struct MaterialU5BU5D_t170856778  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Material_t3870600107 * m_Items[1];

public:
	inline Material_t3870600107 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Material_t3870600107 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Material_t3870600107 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Rendering.ReflectionProbeBlendInfo[]
struct ReflectionProbeBlendInfoU5BU5D_t558780463  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ReflectionProbeBlendInfo_t1080597738  m_Items[1];

public:
	inline ReflectionProbeBlendInfo_t1080597738  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ReflectionProbeBlendInfo_t1080597738 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ReflectionProbeBlendInfo_t1080597738  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.RenderBuffer[]
struct RenderBufferU5BU5D_t1970987103  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RenderBuffer_t3529837690  m_Items[1];

public:
	inline RenderBuffer_t3529837690  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline RenderBuffer_t3529837690 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, RenderBuffer_t3529837690  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Rendering.RenderBufferLoadAction[]
struct RenderBufferLoadActionU5BU5D_t3840699671  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Rendering.RenderBufferStoreAction[]
struct RenderBufferStoreActionU5BU5D_t2354850566  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Rendering.SphericalHarmonicsL2[]
struct SphericalHarmonicsL2U5BU5D_t3690964838  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SphericalHarmonicsL2_t3881980607  m_Items[1];

public:
	inline SphericalHarmonicsL2_t3881980607  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SphericalHarmonicsL2_t3881980607 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SphericalHarmonicsL2_t3881980607  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.LightmapData[]
struct LightmapDataU5BU5D_t705514461  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LightmapData_t3709883476 * m_Items[1];

public:
	inline LightmapData_t3709883476 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LightmapData_t3709883476 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LightmapData_t3709883476 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Plane[]
struct PlaneU5BU5D_t1447812263  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Plane_t4206452690  m_Items[1];

public:
	inline Plane_t4206452690  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Plane_t4206452690 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Plane_t4206452690  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Resolution[]
struct ResolutionU5BU5D_t3895287505  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Resolution_t1578306928  m_Items[1];

public:
	inline Resolution_t1578306928  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Resolution_t1578306928 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Resolution_t1578306928  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GameObject_t3674682005 * m_Items[1];

public:
	inline GameObject_t3674682005 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GameObject_t3674682005 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GameObject_t3674682005 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.MeshSubsetCombineUtility/MeshInstance[]
struct MeshInstanceU5BU5D_t1318761771  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MeshInstance_t4121966238  m_Items[1];

public:
	inline MeshInstance_t4121966238  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MeshInstance_t4121966238 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MeshInstance_t4121966238  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.MeshSubsetCombineUtility/SubMeshInstance[]
struct SubMeshInstanceU5BU5D_t2339421603  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SubMeshInstance_t2374839686  m_Items[1];

public:
	inline SubMeshInstance_t2374839686  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SubMeshInstance_t2374839686 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SubMeshInstance_t2374839686  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Rect[]
struct RectU5BU5D_t1023580025  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Rect_t4241904616  m_Items[1];

public:
	inline Rect_t4241904616  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Rect_t4241904616 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Rect_t4241904616  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t2376705138  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Texture2D_t3884108195 * m_Items[1];

public:
	inline Texture2D_t3884108195 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Texture2D_t3884108195 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Texture2D_t3884108195 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Texture[]
struct TextureU5BU5D_t606176076  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Texture_t2526458961 * m_Items[1];

public:
	inline Texture_t2526458961 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Texture_t2526458961 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Texture_t2526458961 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Renderer[]
struct RendererU5BU5D_t440051646  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Renderer_t3076687687 * m_Items[1];

public:
	inline Renderer_t3076687687 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Renderer_t3076687687 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Renderer_t3076687687 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.LOD[]
struct LODU5BU5D_t1411784526  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LOD_t3134610167  m_Items[1];

public:
	inline LOD_t3134610167  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LOD_t3134610167 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LOD_t3134610167  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.GradientColorKey[]
struct GradientColorKeyU5BU5D_t2977343473  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GradientColorKey_t1060627152  m_Items[1];

public:
	inline GradientColorKey_t1060627152  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GradientColorKey_t1060627152 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GradientColorKey_t1060627152  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.GradientAlphaKey[]
struct GradientAlphaKeyU5BU5D_t1918196120  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GradientAlphaKey_t3609975541  m_Items[1];

public:
	inline GradientAlphaKey_t3609975541  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GradientAlphaKey_t3609975541 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GradientAlphaKey_t3609975541  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t3589549831  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Keyframe_t4079056114  m_Items[1];

public:
	inline Keyframe_t4079056114  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Keyframe_t4079056114 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Keyframe_t4079056114  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.ProceduralPropertyDescription[]
struct ProceduralPropertyDescriptionU5BU5D_t3986076385  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ProceduralPropertyDescription_t3210075808 * m_Items[1];

public:
	inline ProceduralPropertyDescription_t3210075808 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ProceduralPropertyDescription_t3210075808 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ProceduralPropertyDescription_t3210075808 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Camera[]
struct CameraU5BU5D_t2716570836  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Camera_t2727095145 * m_Items[1];

public:
	inline Camera_t2727095145 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Camera_t2727095145 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Camera_t2727095145 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Behaviour[]
struct BehaviourU5BU5D_t1756617250  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Behaviour_t200106419 * m_Items[1];

public:
	inline Behaviour_t200106419 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Behaviour_t200106419 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Behaviour_t200106419 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Rendering.CommandBuffer[]
struct CommandBufferU5BU5D_t912290452  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CommandBuffer_t1654378665 * m_Items[1];

public:
	inline CommandBuffer_t1654378665 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CommandBuffer_t1654378665 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CommandBuffer_t1654378665 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Display[]
struct DisplayU5BU5D_t3684569385  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Display_t1321072632 * m_Items[1];

public:
	inline Display_t1321072632 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Display_t1321072632 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Display_t1321072632 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.AccelerationEvent[]
struct AccelerationEventU5BU5D_t657706065  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AccelerationEvent_t146935280  m_Items[1];

public:
	inline AccelerationEvent_t146935280  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AccelerationEvent_t146935280 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AccelerationEvent_t146935280  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Touch[]
struct TouchU5BU5D_t3635654872  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Touch_t4210255029  m_Items[1];

public:
	inline Touch_t4210255029  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Touch_t4210255029 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Touch_t4210255029  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Light[]
struct LightU5BU5D_t2617847237  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Light_t4202674828 * m_Items[1];

public:
	inline Light_t4202674828 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Light_t4202674828 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Light_t4202674828 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.AndroidJavaObject[]
struct AndroidJavaObjectU5BU5D_t3281907299  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AndroidJavaObject_t2362096582 * m_Items[1];

public:
	inline AndroidJavaObject_t2362096582 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AndroidJavaObject_t2362096582 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AndroidJavaObject_t2362096582 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.jvalue[]
struct jvalueU5BU5D_t1723627146  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) jvalue_t3862675307  m_Items[1];

public:
	inline jvalue_t3862675307  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline jvalue_t3862675307 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, jvalue_t3862675307  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Experimental.Director.Playable[]
struct PlayableU5BU5D_t910723999  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Playable_t70832698 * m_Items[1];

public:
	inline Playable_t70832698 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Playable_t70832698 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Playable_t70832698 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_t343690676  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Particle_t405273609  m_Items[1];

public:
	inline Particle_t405273609  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Particle_t405273609 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Particle_t405273609  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Particle[]
struct ParticleU5BU5D_t2879271823  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Particle_t465417482  m_Items[1];

public:
	inline Particle_t465417482  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Particle_t465417482 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Particle_t465417482  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.ContactPoint[]
struct ContactPointU5BU5D_t715040733  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ContactPoint_t243083348  m_Items[1];

public:
	inline ContactPoint_t243083348  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ContactPoint_t243083348 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ContactPoint_t243083348  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t528650843  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RaycastHit_t4003175726  m_Items[1];

public:
	inline RaycastHit_t4003175726  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline RaycastHit_t4003175726 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, RaycastHit_t4003175726  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Collider[]
struct ColliderU5BU5D_t2697150633  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Collider_t2939674232 * m_Items[1];

public:
	inline Collider_t2939674232 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Collider_t2939674232 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Collider_t2939674232 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.ClothSkinningCoefficient[]
struct ClothSkinningCoefficientU5BU5D_t3357176891  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ClothSkinningCoefficient_t2944055502  m_Items[1];

public:
	inline ClothSkinningCoefficient_t2944055502  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ClothSkinningCoefficient_t2944055502 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ClothSkinningCoefficient_t2944055502  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.CapsuleCollider[]
struct CapsuleColliderU5BU5D_t4217061582  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CapsuleCollider_t318617463 * m_Items[1];

public:
	inline CapsuleCollider_t318617463 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CapsuleCollider_t318617463 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CapsuleCollider_t318617463 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.ClothSphereColliderPair[]
struct ClothSphereColliderPairU5BU5D_t3700479018  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ClothSphereColliderPair_t2674965067  m_Items[1];

public:
	inline ClothSphereColliderPair_t2674965067  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ClothSphereColliderPair_t2674965067 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ClothSphereColliderPair_t2674965067  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Rigidbody2D[]
struct Rigidbody2DU5BU5D_t23929848  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Rigidbody2D_t1743771669 * m_Items[1];

public:
	inline Rigidbody2D_t1743771669 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Rigidbody2D_t1743771669 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Rigidbody2D_t1743771669 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t889400257  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RaycastHit2D_t1374744384  m_Items[1];

public:
	inline RaycastHit2D_t1374744384  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline RaycastHit2D_t1374744384 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, RaycastHit2D_t1374744384  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t1758559887  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Collider2D_t1552025098 * m_Items[1];

public:
	inline Collider2D_t1552025098 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Collider2D_t1552025098 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Collider2D_t1552025098 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.ContactPoint2D[]
struct ContactPoint2DU5BU5D_t3916425411  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ContactPoint2D_t4288432358  m_Items[1];

public:
	inline ContactPoint2D_t4288432358  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ContactPoint2D_t4288432358 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ContactPoint2D_t4288432358  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.WebCamDevice[]
struct WebCamDeviceU5BU5D_t3721690872  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) WebCamDevice_t3274004757  m_Items[1];

public:
	inline WebCamDevice_t3274004757  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline WebCamDevice_t3274004757 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, WebCamDevice_t3274004757  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.AnimationClipPair[]
struct AnimationClipPairU5BU5D_t3767720461  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AnimationClipPair_t2917473764 * m_Items[1];

public:
	inline AnimationClipPair_t2917473764 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AnimationClipPair_t2917473764 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AnimationClipPair_t2917473764 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.AnimationClip[]
struct AnimationClipU5BU5D_t4186127791  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AnimationClip_t2007702890 * m_Items[1];

public:
	inline AnimationClip_t2007702890 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AnimationClip_t2007702890 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AnimationClip_t2007702890 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Motion[]
struct MotionU5BU5D_t3086575327  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Motion_t3026528250 * m_Items[1];

public:
	inline Motion_t3026528250 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Motion_t3026528250 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Motion_t3026528250 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.AnimationEvent[]
struct AnimationEventU5BU5D_t1749266719  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AnimationEvent_t3669457594 * m_Items[1];

public:
	inline AnimationEvent_t3669457594 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AnimationEvent_t3669457594 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AnimationEvent_t3669457594 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.AnimatorControllerParameter[]
struct AnimatorControllerParameterU5BU5D_t1853597653  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AnimatorControllerParameter_t3339705788 * m_Items[1];

public:
	inline AnimatorControllerParameter_t3339705788 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AnimatorControllerParameter_t3339705788 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AnimatorControllerParameter_t3339705788 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.AnimatorClipInfo[]
struct AnimatorClipInfoU5BU5D_t3117370260  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AnimatorClipInfo_t2746035113  m_Items[1];

public:
	inline AnimatorClipInfo_t2746035113  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AnimatorClipInfo_t2746035113 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AnimatorClipInfo_t2746035113  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.HumanBone[]
struct HumanBoneU5BU5D_t838156158  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) HumanBone_t194476679  m_Items[1];

public:
	inline HumanBone_t194476679  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline HumanBone_t194476679 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, HumanBone_t194476679  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.SkeletonBone[]
struct SkeletonBoneU5BU5D_t786140632  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SkeletonBone_t421858229  m_Items[1];

public:
	inline SkeletonBone_t421858229  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SkeletonBone_t421858229 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SkeletonBone_t421858229  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.TreeInstance[]
struct TreeInstanceU5BU5D_t2340352878  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TreeInstance_t2456306007  m_Items[1];

public:
	inline TreeInstance_t2456306007  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TreeInstance_t2456306007 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TreeInstance_t2456306007  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.TreePrototype[]
struct TreePrototypeU5BU5D_t2643627199  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TreePrototype_t4061220762 * m_Items[1];

public:
	inline TreePrototype_t4061220762 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TreePrototype_t4061220762 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TreePrototype_t4061220762 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1796391381  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIVertex_t4244065212  m_Items[1];

public:
	inline UIVertex_t4244065212  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIVertex_t4244065212 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIVertex_t4244065212  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t4214337045  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UICharInfo_t65807484  m_Items[1];

public:
	inline UICharInfo_t65807484  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UICharInfo_t65807484 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UICharInfo_t65807484  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t2354741311  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UILineInfo_t4113875482  m_Items[1];

public:
	inline UILineInfo_t4113875482  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UILineInfo_t4113875482 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UILineInfo_t4113875482  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.CharacterInfo[]
struct CharacterInfoU5BU5D_t4214273472  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CharacterInfo_t2481726445  m_Items[1];

public:
	inline CharacterInfo_t2481726445  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CharacterInfo_t2481726445 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CharacterInfo_t2481726445  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_t3587651179  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RectTransform_t972643934 * m_Items[1];

public:
	inline RectTransform_t972643934 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline RectTransform_t972643934 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, RectTransform_t972643934 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.GUIContent[]
struct GUIContentU5BU5D_t3588725815  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GUIContent_t2094828418 * m_Items[1];

public:
	inline GUIContent_t2094828418 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GUIContent_t2094828418 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GUIContent_t2094828418 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t2977405297  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GUILayoutOption_t331591504 * m_Items[1];

public:
	inline GUILayoutOption_t331591504 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GUILayoutOption_t331591504 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GUILayoutOption_t331591504 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.GUILayoutUtility/LayoutCache[]
struct LayoutCacheU5BU5D_t3709016094  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LayoutCache_t879908455 * m_Items[1];

public:
	inline LayoutCache_t879908455 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LayoutCache_t879908455 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LayoutCache_t879908455 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.GUILayoutEntry[]
struct GUILayoutEntryU5BU5D_t2084979372  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GUILayoutEntry_t1336615025 * m_Items[1];

public:
	inline GUILayoutEntry_t1336615025 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GUILayoutEntry_t1336615025 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GUILayoutEntry_t1336615025 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t565654559  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GUIStyle_t2990928826 * m_Items[1];

public:
	inline GUIStyle_t2990928826 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GUIStyle_t2990928826 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GUIStyle_t2990928826 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.TextEditor/TextEditOp[]
struct TextEditOpU5BU5D_t2239804499  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Event[]
struct EventU5BU5D_t478434737  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Event_t4196595728 * m_Items[1];

public:
	inline Event_t4196595728 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Event_t4196595728 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Event_t4196595728 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.DisallowMultipleComponent[]
struct DisallowMultipleComponentU5BU5D_t3749917529  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DisallowMultipleComponent_t62111112 * m_Items[1];

public:
	inline DisallowMultipleComponent_t62111112 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DisallowMultipleComponent_t62111112 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DisallowMultipleComponent_t62111112 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.ExecuteInEditMode[]
struct ExecuteInEditModeU5BU5D_t156135824  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ExecuteInEditMode_t3132250205 * m_Items[1];

public:
	inline ExecuteInEditMode_t3132250205 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ExecuteInEditMode_t3132250205 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ExecuteInEditMode_t3132250205 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.RequireComponent[]
struct RequireComponentU5BU5D_t968569205  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RequireComponent_t1687166108 * m_Items[1];

public:
	inline RequireComponent_t1687166108 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline RequireComponent_t1687166108 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, RequireComponent_t1687166108 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.SendMouseEvents/HitInfo[]
struct HitInfoU5BU5D_t3452915852  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) HitInfo_t3209134097  m_Items[1];

public:
	inline HitInfo_t3209134097  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline HitInfo_t3209134097 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, HitInfo_t3209134097  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Events.PersistentCall[]
struct PersistentCallU5BU5D_t1880240530  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PersistentCall_t2972625667 * m_Items[1];

public:
	inline PersistentCall_t2972625667 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PersistentCall_t2972625667 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PersistentCall_t2972625667 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Events.BaseInvokableCall[]
struct BaseInvokableCallU5BU5D_t3104884963  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BaseInvokableCall_t1559630662 * m_Items[1];

public:
	inline BaseInvokableCall_t1559630662 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BaseInvokableCall_t1559630662 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BaseInvokableCall_t1559630662 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2761310900  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Sprite_t3199167241 * m_Items[1];

public:
	inline Sprite_t3199167241 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Sprite_t3199167241 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Sprite_t3199167241 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Canvas[]
struct CanvasU5BU5D_t2903919733  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Canvas_t2727140764 * m_Items[1];

public:
	inline Canvas_t2727140764 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Canvas_t2727140764 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Canvas_t2727140764 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Font[]
struct FontU5BU5D_t3453939458  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Font_t4241557075 * m_Items[1];

public:
	inline Font_t4241557075 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Font_t4241557075 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Font_t4241557075 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.CanvasGroup[]
struct CanvasGroupU5BU5D_t290646448  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CanvasGroup_t3702418109 * m_Items[1];

public:
	inline CanvasGroup_t3702418109 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CanvasGroup_t3702418109 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CanvasGroup_t3702418109 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.RenderTexture[]
struct RenderTextureU5BU5D_t1576771098  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RenderTexture_t1963041563 * m_Items[1];

public:
	inline RenderTexture_t1963041563 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline RenderTexture_t1963041563 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, RenderTexture_t1963041563 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Mesh[]
struct MeshU5BU5D_t1759126828  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Mesh_t4241756145 * m_Items[1];

public:
	inline Mesh_t4241756145 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Mesh_t4241756145 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Mesh_t4241756145 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.SkinnedMeshRenderer[]
struct SkinnedMeshRendererU5BU5D_t1155881555  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SkinnedMeshRenderer_t3986041494 * m_Items[1];

public:
	inline SkinnedMeshRenderer_t3986041494 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SkinnedMeshRenderer_t3986041494 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SkinnedMeshRenderer_t3986041494 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t129089073  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MonoBehaviour_t667441552 * m_Items[1];

public:
	inline MonoBehaviour_t667441552 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MonoBehaviour_t667441552 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MonoBehaviour_t667441552 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.TextAsset[]
struct TextAssetU5BU5D_t3697145412  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TextAsset_t3836129977 * m_Items[1];

public:
	inline TextAsset_t3836129977 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TextAsset_t3836129977 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TextAsset_t3836129977 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.MeshRenderer[]
struct MeshRendererU5BU5D_t3685338461  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MeshRenderer_t2804666580 * m_Items[1];

public:
	inline MeshRenderer_t2804666580 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MeshRenderer_t2804666580 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MeshRenderer_t2804666580 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Vector3[][]
struct Vector3U5BU5DU5BU5D_t3570135410  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Vector3U5BU5D_t215400611* m_Items[1];

public:
	inline Vector3U5BU5D_t215400611* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Vector3U5BU5D_t215400611** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Vector3U5BU5D_t215400611* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.MeshFilter[]
struct MeshFilterU5BU5D_t797195060  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MeshFilter_t3839065225 * m_Items[1];

public:
	inline MeshFilter_t3839065225 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MeshFilter_t3839065225 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MeshFilter_t3839065225 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Terrain[]
struct TerrainU5BU5D_t326440842  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Terrain_t2520838763 * m_Items[1];

public:
	inline Terrain_t2520838763 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Terrain_t2520838763 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Terrain_t2520838763 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Bounds[]
struct BoundsU5BU5D_t3144995076  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Bounds_t2711641849  m_Items[1];

public:
	inline Bounds_t2711641849  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Bounds_t2711641849 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Bounds_t2711641849  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.GUITexture[]
struct GUITextureU5BU5D_t1303218893  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GUITexture_t4020448292 * m_Items[1];

public:
	inline GUITexture_t4020448292 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GUITexture_t4020448292 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GUITexture_t4020448292 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.GUIElement[]
struct GUIElementU5BU5D_t2848858184  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GUIElement_t3775428101 * m_Items[1];

public:
	inline GUIElement_t3775428101 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GUIElement_t3775428101 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GUIElement_t3775428101 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.RuntimePlatform[]
struct RuntimePlatformU5BU5D_t2169002428  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Bounds[,]
struct BoundsU5BU2CU5D_t3144995077  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Bounds_t2711641849  m_Items[1];

public:
	inline Bounds_t2711641849  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Bounds_t2711641849 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Bounds_t2711641849  value)
	{
		m_Items[index] = value;
	}
	inline Bounds_t2711641849  GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		return m_Items[index];
	}
	inline Bounds_t2711641849 * GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, Bounds_t2711641849  value)
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		m_Items[index] = value;
	}
};
// UnityEngine.AudioListener[]
struct AudioListenerU5BU5D_t3042620513  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AudioListener_t3685735200 * m_Items[1];

public:
	inline AudioListener_t3685735200 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AudioListener_t3685735200 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AudioListener_t3685735200 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Color[,]
struct ColorU5BU2CU5D_t2441545637  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Color_t4194546905  m_Items[1];

public:
	inline Color_t4194546905  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Color_t4194546905 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Color_t4194546905  value)
	{
		m_Items[index] = value;
	}
	inline Color_t4194546905  GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		return m_Items[index];
	}
	inline Color_t4194546905 * GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, Color_t4194546905  value)
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		m_Items[index] = value;
	}
};
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t1536434148  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ParticleSystem_t381473177 * m_Items[1];

public:
	inline ParticleSystem_t381473177 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ParticleSystem_t381473177 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ParticleSystem_t381473177 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Shader[]
struct ShaderU5BU5D_t3604329748  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Shader_t3191267369 * m_Items[1];

public:
	inline Shader_t3191267369 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Shader_t3191267369 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Shader_t3191267369 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.AssetBundle[]
struct AssetBundleU5BU5D_t3523436441  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AssetBundle_t2070959688 * m_Items[1];

public:
	inline AssetBundle_t2070959688 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AssetBundle_t2070959688 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AssetBundle_t2070959688 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Animator[]
struct AnimatorU5BU5D_t618511498  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Animator_t2776330603 * m_Items[1];

public:
	inline Animator_t2776330603 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Animator_t2776330603 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Animator_t2776330603 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Experimental.Director.IAnimatorControllerPlayable[]
struct IAnimatorControllerPlayableU5BU5D_t1051119489  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Experimental.Director.DirectorPlayer[]
struct DirectorPlayerU5BU5D_t1829907716  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DirectorPlayer_t2782999289 * m_Items[1];

public:
	inline DirectorPlayer_t2782999289 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DirectorPlayer_t2782999289 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DirectorPlayer_t2782999289 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Animation[]
struct AnimationU5BU5D_t2624366687  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Animation_t1724966010 * m_Items[1];

public:
	inline Animation_t1724966010 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Animation_t1724966010 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Animation_t1724966010 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.TrailRenderer[]
struct TrailRendererU5BU5D_t3931528774  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TrailRenderer_t2401074527 * m_Items[1];

public:
	inline TrailRenderer_t2401074527 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TrailRenderer_t2401074527 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TrailRenderer_t2401074527 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
