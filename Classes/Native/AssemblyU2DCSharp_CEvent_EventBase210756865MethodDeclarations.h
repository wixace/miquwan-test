﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CEvent.EventBase
struct EventBase_t210756865;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void CEvent.EventBase::.ctor(System.String)
extern "C"  void EventBase__ctor_m4177075355 (EventBase_t210756865 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
