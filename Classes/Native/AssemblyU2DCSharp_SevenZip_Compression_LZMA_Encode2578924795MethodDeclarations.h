﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.Compression.RangeCoder.Encoder
struct Encoder_t2248006694;
// SevenZip.Compression.LZMA.Encoder/LiteralEncoder/Encoder2
struct Encoder2_t2578924795;
struct Encoder2_t2578924795_marshaled_pinvoke;
struct Encoder2_t2578924795_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Encode2578924795.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_2248006694.h"

// System.Void SevenZip.Compression.LZMA.Encoder/LiteralEncoder/Encoder2::Create()
extern "C"  void Encoder2_Create_m1880459696 (Encoder2_t2578924795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder/LiteralEncoder/Encoder2::Init()
extern "C"  void Encoder2_Init_m1975757188 (Encoder2_t2578924795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder/LiteralEncoder/Encoder2::Encode(SevenZip.Compression.RangeCoder.Encoder,System.Byte)
extern "C"  void Encoder2_Encode_m3717541397 (Encoder2_t2578924795 * __this, Encoder_t2248006694 * ___rangeEncoder0, uint8_t ___symbol1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder/LiteralEncoder/Encoder2::EncodeMatched(SevenZip.Compression.RangeCoder.Encoder,System.Byte,System.Byte)
extern "C"  void Encoder2_EncodeMatched_m1950185000 (Encoder2_t2578924795 * __this, Encoder_t2248006694 * ___rangeEncoder0, uint8_t ___matchByte1, uint8_t ___symbol2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Encoder/LiteralEncoder/Encoder2::GetPrice(System.Boolean,System.Byte,System.Byte)
extern "C"  uint32_t Encoder2_GetPrice_m425513165 (Encoder2_t2578924795 * __this, bool ___matchMode0, uint8_t ___matchByte1, uint8_t ___symbol2, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Encoder2_t2578924795;
struct Encoder2_t2578924795_marshaled_pinvoke;

extern "C" void Encoder2_t2578924795_marshal_pinvoke(const Encoder2_t2578924795& unmarshaled, Encoder2_t2578924795_marshaled_pinvoke& marshaled);
extern "C" void Encoder2_t2578924795_marshal_pinvoke_back(const Encoder2_t2578924795_marshaled_pinvoke& marshaled, Encoder2_t2578924795& unmarshaled);
extern "C" void Encoder2_t2578924795_marshal_pinvoke_cleanup(Encoder2_t2578924795_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Encoder2_t2578924795;
struct Encoder2_t2578924795_marshaled_com;

extern "C" void Encoder2_t2578924795_marshal_com(const Encoder2_t2578924795& unmarshaled, Encoder2_t2578924795_marshaled_com& marshaled);
extern "C" void Encoder2_t2578924795_marshal_com_back(const Encoder2_t2578924795_marshaled_com& marshaled, Encoder2_t2578924795& unmarshaled);
extern "C" void Encoder2_t2578924795_marshal_com_cleanup(Encoder2_t2578924795_marshaled_com& marshaled);
