﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_MissingReferenceExceptionGenerated
struct UnityEngine_MissingReferenceExceptionGenerated_t1928664781;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_MissingReferenceExceptionGenerated::.ctor()
extern "C"  void UnityEngine_MissingReferenceExceptionGenerated__ctor_m326282350 (UnityEngine_MissingReferenceExceptionGenerated_t1928664781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MissingReferenceExceptionGenerated::MissingReferenceException_MissingReferenceException1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MissingReferenceExceptionGenerated_MissingReferenceException_MissingReferenceException1_m353247670 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MissingReferenceExceptionGenerated::MissingReferenceException_MissingReferenceException2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MissingReferenceExceptionGenerated_MissingReferenceException_MissingReferenceException2_m1598012151 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MissingReferenceExceptionGenerated::MissingReferenceException_MissingReferenceException3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MissingReferenceExceptionGenerated_MissingReferenceException_MissingReferenceException3_m2842776632 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MissingReferenceExceptionGenerated::__Register()
extern "C"  void UnityEngine_MissingReferenceExceptionGenerated___Register_m3870254361 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MissingReferenceExceptionGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_MissingReferenceExceptionGenerated_ilo_addJSCSRel1_m1144595498 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_MissingReferenceExceptionGenerated::ilo_getObject2(System.Int32)
extern "C"  int32_t UnityEngine_MissingReferenceExceptionGenerated_ilo_getObject2_m722133285 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
