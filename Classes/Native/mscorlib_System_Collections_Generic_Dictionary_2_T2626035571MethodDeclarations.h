﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<Entity.Behavior.EBehaviorID,System.Object,Entity.Behavior.EBehaviorID>
struct Transform_1_t2626035571;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Entity.Behavior.EBehaviorID,System.Object,Entity.Behavior.EBehaviorID>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1792821181_gshared (Transform_1_t2626035571 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m1792821181(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t2626035571 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m1792821181_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Entity.Behavior.EBehaviorID,System.Object,Entity.Behavior.EBehaviorID>::Invoke(TKey,TValue)
extern "C"  uint8_t Transform_1_Invoke_m1248844319_gshared (Transform_1_t2626035571 * __this, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m1248844319(__this, ___key0, ___value1, method) ((  uint8_t (*) (Transform_1_t2626035571 *, uint8_t, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m1248844319_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Entity.Behavior.EBehaviorID,System.Object,Entity.Behavior.EBehaviorID>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2238627966_gshared (Transform_1_t2626035571 * __this, uint8_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m2238627966(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t2626035571 *, uint8_t, Il2CppObject *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m2238627966_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Entity.Behavior.EBehaviorID,System.Object,Entity.Behavior.EBehaviorID>::EndInvoke(System.IAsyncResult)
extern "C"  uint8_t Transform_1_EndInvoke_m1781632267_gshared (Transform_1_t2626035571 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m1781632267(__this, ___result0, method) ((  uint8_t (*) (Transform_1_t2626035571 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m1781632267_gshared)(__this, ___result0, method)
