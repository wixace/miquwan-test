﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// monstersCfg
struct  monstersCfg_t1542396363  : public CsCfgBase_t69924517
{
public:
	// System.Int32 monstersCfg::id
	int32_t ___id_0;
	// System.String monstersCfg::name
	String_t* ___name_1;
	// System.String monstersCfg::headicon
	String_t* ___headicon_2;
	// System.Int32 monstersCfg::quatily
	int32_t ___quatily_3;
	// System.Int32 monstersCfg::quatilyforshow
	int32_t ___quatilyforshow_4;
	// System.String monstersCfg::icon
	String_t* ___icon_5;
	// System.String monstersCfg::bossshow
	String_t* ___bossshow_6;
	// System.Int32 monstersCfg::uiNamePos
	int32_t ___uiNamePos_7;
	// System.String monstersCfg::largeIcon
	String_t* ___largeIcon_8;
	// System.Int32 monstersCfg::isAim
	int32_t ___isAim_9;
	// System.Int32 monstersCfg::elite
	int32_t ___elite_10;
	// System.Int32 monstersCfg::country
	int32_t ___country_11;
	// System.Int32 monstersCfg::sex
	int32_t ___sex_12;
	// System.Int32 monstersCfg::dutytype
	int32_t ___dutytype_13;
	// System.Int32 monstersCfg::heromajor
	int32_t ___heromajor_14;
	// System.Single monstersCfg::scale
	float ___scale_15;
	// System.Single monstersCfg::halfwidth
	float ___halfwidth_16;
	// System.Single monstersCfg::atkrange
	float ___atkrange_17;
	// System.Single monstersCfg::movespeed
	float ___movespeed_18;
	// System.Int32 monstersCfg::atktype
	int32_t ___atktype_19;
	// System.Int32 monstersCfg::identity
	int32_t ___identity_20;
	// System.Int32 monstersCfg::attack_nature
	int32_t ___attack_nature_21;
	// System.Int32 monstersCfg::super_armor
	int32_t ___super_armor_22;
	// System.Int32 monstersCfg::armor_recover_time
	int32_t ___armor_recover_time_23;
	// System.Int32 monstersCfg::attack
	int32_t ___attack_24;
	// System.Int32 monstersCfg::phy_def
	int32_t ___phy_def_25;
	// System.Int32 monstersCfg::mag_def
	int32_t ___mag_def_26;
	// System.Int32 monstersCfg::hp
	int32_t ___hp_27;
	// System.Int32 monstersCfg::hitrate
	int32_t ___hitrate_28;
	// System.Int32 monstersCfg::dodge
	int32_t ___dodge_29;
	// System.Int32 monstersCfg::destory
	int32_t ___destory_30;
	// System.Int32 monstersCfg::block
	int32_t ___block_31;
	// System.Int32 monstersCfg::crit
	int32_t ___crit_32;
	// System.Int32 monstersCfg::crit_hurt
	int32_t ___crit_hurt_33;
	// System.Int32 monstersCfg::anticrit
	int32_t ___anticrit_34;
	// System.Int32 monstersCfg::damage_reflect
	int32_t ___damage_reflect_35;
	// System.Int32 monstersCfg::heal_bonus
	int32_t ___heal_bonus_36;
	// System.Int32 monstersCfg::damage_bonus
	int32_t ___damage_bonus_37;
	// System.Int32 monstersCfg::damage_reduce
	int32_t ___damage_reduce_38;
	// System.Int32 monstersCfg::real_damage
	int32_t ___real_damage_39;
	// System.Int32 monstersCfg::firm
	int32_t ___firm_40;
	// System.Int32 monstersCfg::pure
	int32_t ___pure_41;
	// System.Int32 monstersCfg::stubborn
	int32_t ___stubborn_42;
	// System.Int32 monstersCfg::star_level
	int32_t ___star_level_43;
	// System.Int32 monstersCfg::anger_initial
	int32_t ___anger_initial_44;
	// System.Int32 monstersCfg::anger_max
	int32_t ___anger_max_45;
	// System.Int32 monstersCfg::anger_growth
	int32_t ___anger_growth_46;
	// System.Int32 monstersCfg::behurtanger
	int32_t ___behurtanger_47;
	// System.Int32 monstersCfg::element
	int32_t ___element_48;
	// System.Single monstersCfg::atkeffdelaytime
	float ___atkeffdelaytime_49;
	// System.Int32 monstersCfg::leader_skill
	int32_t ___leader_skill_50;
	// System.String monstersCfg::skills
	String_t* ___skills_51;
	// System.Int32 monstersCfg::dropId
	int32_t ___dropId_52;
	// System.Int32 monstersCfg::dropNum
	int32_t ___dropNum_53;
	// System.Int32 monstersCfg::dropNumratio
	int32_t ___dropNumratio_54;
	// System.String monstersCfg::AIID
	String_t* ___AIID_55;
	// System.Single monstersCfg::atkDis
	float ___atkDis_56;
	// System.Single monstersCfg::viewDis
	float ___viewDis_57;
	// System.Single monstersCfg::lucencyWait
	float ___lucencyWait_58;
	// System.Int32 monstersCfg::islucency
	int32_t ___islucency_59;
	// System.Single monstersCfg::lucencytime
	float ___lucencytime_60;
	// System.Int32 monstersCfg::atkedEff
	int32_t ___atkedEff_61;
	// System.Int32 monstersCfg::elementEffect
	int32_t ___elementEffect_62;
	// System.Int32 monstersCfg::appeareffect
	int32_t ___appeareffect_63;
	// System.Int32 monstersCfg::deatheffect
	int32_t ___deatheffect_64;
	// System.Int32 monstersCfg::taixudeatheffect
	int32_t ___taixudeatheffect_65;
	// System.Int32 monstersCfg::reviveEffectId
	int32_t ___reviveEffectId_66;
	// System.Int32 monstersCfg::skinID
	int32_t ___skinID_67;
	// System.String monstersCfg::storyID
	String_t* ___storyID_68;
	// System.String monstersCfg::resume
	String_t* ___resume_69;
	// System.Single monstersCfg::attack_growup
	float ___attack_growup_70;
	// System.Single monstersCfg::phydef_growup
	float ___phydef_growup_71;
	// System.Single monstersCfg::magdef_growup
	float ___magdef_growup_72;
	// System.Single monstersCfg::hp_growup
	float ___hp_growup_73;
	// System.Single monstersCfg::hitrate_growup
	float ___hitrate_growup_74;
	// System.Single monstersCfg::dodge_growup
	float ___dodge_growup_75;
	// System.Single monstersCfg::destory_growup
	float ___destory_growup_76;
	// System.Single monstersCfg::block_growup
	float ___block_growup_77;
	// System.Single monstersCfg::crit_growup
	float ___crit_growup_78;
	// System.Single monstersCfg::anticrit_growup
	float ___anticrit_growup_79;
	// System.Single monstersCfg::damage_bonus_growup
	float ___damage_bonus_growup_80;
	// System.Single monstersCfg::damage_reduce_growup
	float ___damage_reduce_growup_81;
	// System.Single monstersCfg::real_damage_growup
	float ___real_damage_growup_82;
	// System.String monstersCfg::offsetPos
	String_t* ___offsetPos_83;
	// System.String monstersCfg::offsetRob
	String_t* ___offsetRob_84;
	// System.Single monstersCfg::offsetScale
	float ___offsetScale_85;
	// System.Int32 monstersCfg::isTrue
	int32_t ___isTrue_86;
	// System.Int32 monstersCfg::ifattacked
	int32_t ___ifattacked_87;
	// System.Int32 monstersCfg::ifeffects
	int32_t ___ifeffects_88;
	// System.Single monstersCfg::damageshowpos
	float ___damageshowpos_89;
	// System.String monstersCfg::rgb
	String_t* ___rgb_90;
	// System.String monstersCfg::showrgb
	String_t* ___showrgb_91;
	// System.Single monstersCfg::rim
	float ___rim_92;
	// System.Single monstersCfg::findEnemyRange
	float ___findEnemyRange_93;
	// System.Boolean monstersCfg::isInFinish
	bool ___isInFinish_94;
	// System.Int32 monstersCfg::initBehavior
	int32_t ___initBehavior_95;
	// System.Boolean monstersCfg::isRuneffect
	bool ___isRuneffect_96;
	// System.Boolean monstersCfg::isInNum
	bool ___isInNum_97;
	// System.Boolean monstersCfg::isHideSummon
	bool ___isHideSummon_98;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_headicon_2() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___headicon_2)); }
	inline String_t* get_headicon_2() const { return ___headicon_2; }
	inline String_t** get_address_of_headicon_2() { return &___headicon_2; }
	inline void set_headicon_2(String_t* value)
	{
		___headicon_2 = value;
		Il2CppCodeGenWriteBarrier(&___headicon_2, value);
	}

	inline static int32_t get_offset_of_quatily_3() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___quatily_3)); }
	inline int32_t get_quatily_3() const { return ___quatily_3; }
	inline int32_t* get_address_of_quatily_3() { return &___quatily_3; }
	inline void set_quatily_3(int32_t value)
	{
		___quatily_3 = value;
	}

	inline static int32_t get_offset_of_quatilyforshow_4() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___quatilyforshow_4)); }
	inline int32_t get_quatilyforshow_4() const { return ___quatilyforshow_4; }
	inline int32_t* get_address_of_quatilyforshow_4() { return &___quatilyforshow_4; }
	inline void set_quatilyforshow_4(int32_t value)
	{
		___quatilyforshow_4 = value;
	}

	inline static int32_t get_offset_of_icon_5() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___icon_5)); }
	inline String_t* get_icon_5() const { return ___icon_5; }
	inline String_t** get_address_of_icon_5() { return &___icon_5; }
	inline void set_icon_5(String_t* value)
	{
		___icon_5 = value;
		Il2CppCodeGenWriteBarrier(&___icon_5, value);
	}

	inline static int32_t get_offset_of_bossshow_6() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___bossshow_6)); }
	inline String_t* get_bossshow_6() const { return ___bossshow_6; }
	inline String_t** get_address_of_bossshow_6() { return &___bossshow_6; }
	inline void set_bossshow_6(String_t* value)
	{
		___bossshow_6 = value;
		Il2CppCodeGenWriteBarrier(&___bossshow_6, value);
	}

	inline static int32_t get_offset_of_uiNamePos_7() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___uiNamePos_7)); }
	inline int32_t get_uiNamePos_7() const { return ___uiNamePos_7; }
	inline int32_t* get_address_of_uiNamePos_7() { return &___uiNamePos_7; }
	inline void set_uiNamePos_7(int32_t value)
	{
		___uiNamePos_7 = value;
	}

	inline static int32_t get_offset_of_largeIcon_8() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___largeIcon_8)); }
	inline String_t* get_largeIcon_8() const { return ___largeIcon_8; }
	inline String_t** get_address_of_largeIcon_8() { return &___largeIcon_8; }
	inline void set_largeIcon_8(String_t* value)
	{
		___largeIcon_8 = value;
		Il2CppCodeGenWriteBarrier(&___largeIcon_8, value);
	}

	inline static int32_t get_offset_of_isAim_9() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___isAim_9)); }
	inline int32_t get_isAim_9() const { return ___isAim_9; }
	inline int32_t* get_address_of_isAim_9() { return &___isAim_9; }
	inline void set_isAim_9(int32_t value)
	{
		___isAim_9 = value;
	}

	inline static int32_t get_offset_of_elite_10() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___elite_10)); }
	inline int32_t get_elite_10() const { return ___elite_10; }
	inline int32_t* get_address_of_elite_10() { return &___elite_10; }
	inline void set_elite_10(int32_t value)
	{
		___elite_10 = value;
	}

	inline static int32_t get_offset_of_country_11() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___country_11)); }
	inline int32_t get_country_11() const { return ___country_11; }
	inline int32_t* get_address_of_country_11() { return &___country_11; }
	inline void set_country_11(int32_t value)
	{
		___country_11 = value;
	}

	inline static int32_t get_offset_of_sex_12() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___sex_12)); }
	inline int32_t get_sex_12() const { return ___sex_12; }
	inline int32_t* get_address_of_sex_12() { return &___sex_12; }
	inline void set_sex_12(int32_t value)
	{
		___sex_12 = value;
	}

	inline static int32_t get_offset_of_dutytype_13() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___dutytype_13)); }
	inline int32_t get_dutytype_13() const { return ___dutytype_13; }
	inline int32_t* get_address_of_dutytype_13() { return &___dutytype_13; }
	inline void set_dutytype_13(int32_t value)
	{
		___dutytype_13 = value;
	}

	inline static int32_t get_offset_of_heromajor_14() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___heromajor_14)); }
	inline int32_t get_heromajor_14() const { return ___heromajor_14; }
	inline int32_t* get_address_of_heromajor_14() { return &___heromajor_14; }
	inline void set_heromajor_14(int32_t value)
	{
		___heromajor_14 = value;
	}

	inline static int32_t get_offset_of_scale_15() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___scale_15)); }
	inline float get_scale_15() const { return ___scale_15; }
	inline float* get_address_of_scale_15() { return &___scale_15; }
	inline void set_scale_15(float value)
	{
		___scale_15 = value;
	}

	inline static int32_t get_offset_of_halfwidth_16() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___halfwidth_16)); }
	inline float get_halfwidth_16() const { return ___halfwidth_16; }
	inline float* get_address_of_halfwidth_16() { return &___halfwidth_16; }
	inline void set_halfwidth_16(float value)
	{
		___halfwidth_16 = value;
	}

	inline static int32_t get_offset_of_atkrange_17() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___atkrange_17)); }
	inline float get_atkrange_17() const { return ___atkrange_17; }
	inline float* get_address_of_atkrange_17() { return &___atkrange_17; }
	inline void set_atkrange_17(float value)
	{
		___atkrange_17 = value;
	}

	inline static int32_t get_offset_of_movespeed_18() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___movespeed_18)); }
	inline float get_movespeed_18() const { return ___movespeed_18; }
	inline float* get_address_of_movespeed_18() { return &___movespeed_18; }
	inline void set_movespeed_18(float value)
	{
		___movespeed_18 = value;
	}

	inline static int32_t get_offset_of_atktype_19() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___atktype_19)); }
	inline int32_t get_atktype_19() const { return ___atktype_19; }
	inline int32_t* get_address_of_atktype_19() { return &___atktype_19; }
	inline void set_atktype_19(int32_t value)
	{
		___atktype_19 = value;
	}

	inline static int32_t get_offset_of_identity_20() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___identity_20)); }
	inline int32_t get_identity_20() const { return ___identity_20; }
	inline int32_t* get_address_of_identity_20() { return &___identity_20; }
	inline void set_identity_20(int32_t value)
	{
		___identity_20 = value;
	}

	inline static int32_t get_offset_of_attack_nature_21() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___attack_nature_21)); }
	inline int32_t get_attack_nature_21() const { return ___attack_nature_21; }
	inline int32_t* get_address_of_attack_nature_21() { return &___attack_nature_21; }
	inline void set_attack_nature_21(int32_t value)
	{
		___attack_nature_21 = value;
	}

	inline static int32_t get_offset_of_super_armor_22() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___super_armor_22)); }
	inline int32_t get_super_armor_22() const { return ___super_armor_22; }
	inline int32_t* get_address_of_super_armor_22() { return &___super_armor_22; }
	inline void set_super_armor_22(int32_t value)
	{
		___super_armor_22 = value;
	}

	inline static int32_t get_offset_of_armor_recover_time_23() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___armor_recover_time_23)); }
	inline int32_t get_armor_recover_time_23() const { return ___armor_recover_time_23; }
	inline int32_t* get_address_of_armor_recover_time_23() { return &___armor_recover_time_23; }
	inline void set_armor_recover_time_23(int32_t value)
	{
		___armor_recover_time_23 = value;
	}

	inline static int32_t get_offset_of_attack_24() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___attack_24)); }
	inline int32_t get_attack_24() const { return ___attack_24; }
	inline int32_t* get_address_of_attack_24() { return &___attack_24; }
	inline void set_attack_24(int32_t value)
	{
		___attack_24 = value;
	}

	inline static int32_t get_offset_of_phy_def_25() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___phy_def_25)); }
	inline int32_t get_phy_def_25() const { return ___phy_def_25; }
	inline int32_t* get_address_of_phy_def_25() { return &___phy_def_25; }
	inline void set_phy_def_25(int32_t value)
	{
		___phy_def_25 = value;
	}

	inline static int32_t get_offset_of_mag_def_26() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___mag_def_26)); }
	inline int32_t get_mag_def_26() const { return ___mag_def_26; }
	inline int32_t* get_address_of_mag_def_26() { return &___mag_def_26; }
	inline void set_mag_def_26(int32_t value)
	{
		___mag_def_26 = value;
	}

	inline static int32_t get_offset_of_hp_27() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___hp_27)); }
	inline int32_t get_hp_27() const { return ___hp_27; }
	inline int32_t* get_address_of_hp_27() { return &___hp_27; }
	inline void set_hp_27(int32_t value)
	{
		___hp_27 = value;
	}

	inline static int32_t get_offset_of_hitrate_28() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___hitrate_28)); }
	inline int32_t get_hitrate_28() const { return ___hitrate_28; }
	inline int32_t* get_address_of_hitrate_28() { return &___hitrate_28; }
	inline void set_hitrate_28(int32_t value)
	{
		___hitrate_28 = value;
	}

	inline static int32_t get_offset_of_dodge_29() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___dodge_29)); }
	inline int32_t get_dodge_29() const { return ___dodge_29; }
	inline int32_t* get_address_of_dodge_29() { return &___dodge_29; }
	inline void set_dodge_29(int32_t value)
	{
		___dodge_29 = value;
	}

	inline static int32_t get_offset_of_destory_30() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___destory_30)); }
	inline int32_t get_destory_30() const { return ___destory_30; }
	inline int32_t* get_address_of_destory_30() { return &___destory_30; }
	inline void set_destory_30(int32_t value)
	{
		___destory_30 = value;
	}

	inline static int32_t get_offset_of_block_31() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___block_31)); }
	inline int32_t get_block_31() const { return ___block_31; }
	inline int32_t* get_address_of_block_31() { return &___block_31; }
	inline void set_block_31(int32_t value)
	{
		___block_31 = value;
	}

	inline static int32_t get_offset_of_crit_32() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___crit_32)); }
	inline int32_t get_crit_32() const { return ___crit_32; }
	inline int32_t* get_address_of_crit_32() { return &___crit_32; }
	inline void set_crit_32(int32_t value)
	{
		___crit_32 = value;
	}

	inline static int32_t get_offset_of_crit_hurt_33() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___crit_hurt_33)); }
	inline int32_t get_crit_hurt_33() const { return ___crit_hurt_33; }
	inline int32_t* get_address_of_crit_hurt_33() { return &___crit_hurt_33; }
	inline void set_crit_hurt_33(int32_t value)
	{
		___crit_hurt_33 = value;
	}

	inline static int32_t get_offset_of_anticrit_34() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___anticrit_34)); }
	inline int32_t get_anticrit_34() const { return ___anticrit_34; }
	inline int32_t* get_address_of_anticrit_34() { return &___anticrit_34; }
	inline void set_anticrit_34(int32_t value)
	{
		___anticrit_34 = value;
	}

	inline static int32_t get_offset_of_damage_reflect_35() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___damage_reflect_35)); }
	inline int32_t get_damage_reflect_35() const { return ___damage_reflect_35; }
	inline int32_t* get_address_of_damage_reflect_35() { return &___damage_reflect_35; }
	inline void set_damage_reflect_35(int32_t value)
	{
		___damage_reflect_35 = value;
	}

	inline static int32_t get_offset_of_heal_bonus_36() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___heal_bonus_36)); }
	inline int32_t get_heal_bonus_36() const { return ___heal_bonus_36; }
	inline int32_t* get_address_of_heal_bonus_36() { return &___heal_bonus_36; }
	inline void set_heal_bonus_36(int32_t value)
	{
		___heal_bonus_36 = value;
	}

	inline static int32_t get_offset_of_damage_bonus_37() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___damage_bonus_37)); }
	inline int32_t get_damage_bonus_37() const { return ___damage_bonus_37; }
	inline int32_t* get_address_of_damage_bonus_37() { return &___damage_bonus_37; }
	inline void set_damage_bonus_37(int32_t value)
	{
		___damage_bonus_37 = value;
	}

	inline static int32_t get_offset_of_damage_reduce_38() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___damage_reduce_38)); }
	inline int32_t get_damage_reduce_38() const { return ___damage_reduce_38; }
	inline int32_t* get_address_of_damage_reduce_38() { return &___damage_reduce_38; }
	inline void set_damage_reduce_38(int32_t value)
	{
		___damage_reduce_38 = value;
	}

	inline static int32_t get_offset_of_real_damage_39() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___real_damage_39)); }
	inline int32_t get_real_damage_39() const { return ___real_damage_39; }
	inline int32_t* get_address_of_real_damage_39() { return &___real_damage_39; }
	inline void set_real_damage_39(int32_t value)
	{
		___real_damage_39 = value;
	}

	inline static int32_t get_offset_of_firm_40() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___firm_40)); }
	inline int32_t get_firm_40() const { return ___firm_40; }
	inline int32_t* get_address_of_firm_40() { return &___firm_40; }
	inline void set_firm_40(int32_t value)
	{
		___firm_40 = value;
	}

	inline static int32_t get_offset_of_pure_41() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___pure_41)); }
	inline int32_t get_pure_41() const { return ___pure_41; }
	inline int32_t* get_address_of_pure_41() { return &___pure_41; }
	inline void set_pure_41(int32_t value)
	{
		___pure_41 = value;
	}

	inline static int32_t get_offset_of_stubborn_42() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___stubborn_42)); }
	inline int32_t get_stubborn_42() const { return ___stubborn_42; }
	inline int32_t* get_address_of_stubborn_42() { return &___stubborn_42; }
	inline void set_stubborn_42(int32_t value)
	{
		___stubborn_42 = value;
	}

	inline static int32_t get_offset_of_star_level_43() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___star_level_43)); }
	inline int32_t get_star_level_43() const { return ___star_level_43; }
	inline int32_t* get_address_of_star_level_43() { return &___star_level_43; }
	inline void set_star_level_43(int32_t value)
	{
		___star_level_43 = value;
	}

	inline static int32_t get_offset_of_anger_initial_44() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___anger_initial_44)); }
	inline int32_t get_anger_initial_44() const { return ___anger_initial_44; }
	inline int32_t* get_address_of_anger_initial_44() { return &___anger_initial_44; }
	inline void set_anger_initial_44(int32_t value)
	{
		___anger_initial_44 = value;
	}

	inline static int32_t get_offset_of_anger_max_45() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___anger_max_45)); }
	inline int32_t get_anger_max_45() const { return ___anger_max_45; }
	inline int32_t* get_address_of_anger_max_45() { return &___anger_max_45; }
	inline void set_anger_max_45(int32_t value)
	{
		___anger_max_45 = value;
	}

	inline static int32_t get_offset_of_anger_growth_46() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___anger_growth_46)); }
	inline int32_t get_anger_growth_46() const { return ___anger_growth_46; }
	inline int32_t* get_address_of_anger_growth_46() { return &___anger_growth_46; }
	inline void set_anger_growth_46(int32_t value)
	{
		___anger_growth_46 = value;
	}

	inline static int32_t get_offset_of_behurtanger_47() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___behurtanger_47)); }
	inline int32_t get_behurtanger_47() const { return ___behurtanger_47; }
	inline int32_t* get_address_of_behurtanger_47() { return &___behurtanger_47; }
	inline void set_behurtanger_47(int32_t value)
	{
		___behurtanger_47 = value;
	}

	inline static int32_t get_offset_of_element_48() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___element_48)); }
	inline int32_t get_element_48() const { return ___element_48; }
	inline int32_t* get_address_of_element_48() { return &___element_48; }
	inline void set_element_48(int32_t value)
	{
		___element_48 = value;
	}

	inline static int32_t get_offset_of_atkeffdelaytime_49() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___atkeffdelaytime_49)); }
	inline float get_atkeffdelaytime_49() const { return ___atkeffdelaytime_49; }
	inline float* get_address_of_atkeffdelaytime_49() { return &___atkeffdelaytime_49; }
	inline void set_atkeffdelaytime_49(float value)
	{
		___atkeffdelaytime_49 = value;
	}

	inline static int32_t get_offset_of_leader_skill_50() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___leader_skill_50)); }
	inline int32_t get_leader_skill_50() const { return ___leader_skill_50; }
	inline int32_t* get_address_of_leader_skill_50() { return &___leader_skill_50; }
	inline void set_leader_skill_50(int32_t value)
	{
		___leader_skill_50 = value;
	}

	inline static int32_t get_offset_of_skills_51() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___skills_51)); }
	inline String_t* get_skills_51() const { return ___skills_51; }
	inline String_t** get_address_of_skills_51() { return &___skills_51; }
	inline void set_skills_51(String_t* value)
	{
		___skills_51 = value;
		Il2CppCodeGenWriteBarrier(&___skills_51, value);
	}

	inline static int32_t get_offset_of_dropId_52() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___dropId_52)); }
	inline int32_t get_dropId_52() const { return ___dropId_52; }
	inline int32_t* get_address_of_dropId_52() { return &___dropId_52; }
	inline void set_dropId_52(int32_t value)
	{
		___dropId_52 = value;
	}

	inline static int32_t get_offset_of_dropNum_53() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___dropNum_53)); }
	inline int32_t get_dropNum_53() const { return ___dropNum_53; }
	inline int32_t* get_address_of_dropNum_53() { return &___dropNum_53; }
	inline void set_dropNum_53(int32_t value)
	{
		___dropNum_53 = value;
	}

	inline static int32_t get_offset_of_dropNumratio_54() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___dropNumratio_54)); }
	inline int32_t get_dropNumratio_54() const { return ___dropNumratio_54; }
	inline int32_t* get_address_of_dropNumratio_54() { return &___dropNumratio_54; }
	inline void set_dropNumratio_54(int32_t value)
	{
		___dropNumratio_54 = value;
	}

	inline static int32_t get_offset_of_AIID_55() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___AIID_55)); }
	inline String_t* get_AIID_55() const { return ___AIID_55; }
	inline String_t** get_address_of_AIID_55() { return &___AIID_55; }
	inline void set_AIID_55(String_t* value)
	{
		___AIID_55 = value;
		Il2CppCodeGenWriteBarrier(&___AIID_55, value);
	}

	inline static int32_t get_offset_of_atkDis_56() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___atkDis_56)); }
	inline float get_atkDis_56() const { return ___atkDis_56; }
	inline float* get_address_of_atkDis_56() { return &___atkDis_56; }
	inline void set_atkDis_56(float value)
	{
		___atkDis_56 = value;
	}

	inline static int32_t get_offset_of_viewDis_57() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___viewDis_57)); }
	inline float get_viewDis_57() const { return ___viewDis_57; }
	inline float* get_address_of_viewDis_57() { return &___viewDis_57; }
	inline void set_viewDis_57(float value)
	{
		___viewDis_57 = value;
	}

	inline static int32_t get_offset_of_lucencyWait_58() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___lucencyWait_58)); }
	inline float get_lucencyWait_58() const { return ___lucencyWait_58; }
	inline float* get_address_of_lucencyWait_58() { return &___lucencyWait_58; }
	inline void set_lucencyWait_58(float value)
	{
		___lucencyWait_58 = value;
	}

	inline static int32_t get_offset_of_islucency_59() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___islucency_59)); }
	inline int32_t get_islucency_59() const { return ___islucency_59; }
	inline int32_t* get_address_of_islucency_59() { return &___islucency_59; }
	inline void set_islucency_59(int32_t value)
	{
		___islucency_59 = value;
	}

	inline static int32_t get_offset_of_lucencytime_60() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___lucencytime_60)); }
	inline float get_lucencytime_60() const { return ___lucencytime_60; }
	inline float* get_address_of_lucencytime_60() { return &___lucencytime_60; }
	inline void set_lucencytime_60(float value)
	{
		___lucencytime_60 = value;
	}

	inline static int32_t get_offset_of_atkedEff_61() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___atkedEff_61)); }
	inline int32_t get_atkedEff_61() const { return ___atkedEff_61; }
	inline int32_t* get_address_of_atkedEff_61() { return &___atkedEff_61; }
	inline void set_atkedEff_61(int32_t value)
	{
		___atkedEff_61 = value;
	}

	inline static int32_t get_offset_of_elementEffect_62() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___elementEffect_62)); }
	inline int32_t get_elementEffect_62() const { return ___elementEffect_62; }
	inline int32_t* get_address_of_elementEffect_62() { return &___elementEffect_62; }
	inline void set_elementEffect_62(int32_t value)
	{
		___elementEffect_62 = value;
	}

	inline static int32_t get_offset_of_appeareffect_63() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___appeareffect_63)); }
	inline int32_t get_appeareffect_63() const { return ___appeareffect_63; }
	inline int32_t* get_address_of_appeareffect_63() { return &___appeareffect_63; }
	inline void set_appeareffect_63(int32_t value)
	{
		___appeareffect_63 = value;
	}

	inline static int32_t get_offset_of_deatheffect_64() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___deatheffect_64)); }
	inline int32_t get_deatheffect_64() const { return ___deatheffect_64; }
	inline int32_t* get_address_of_deatheffect_64() { return &___deatheffect_64; }
	inline void set_deatheffect_64(int32_t value)
	{
		___deatheffect_64 = value;
	}

	inline static int32_t get_offset_of_taixudeatheffect_65() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___taixudeatheffect_65)); }
	inline int32_t get_taixudeatheffect_65() const { return ___taixudeatheffect_65; }
	inline int32_t* get_address_of_taixudeatheffect_65() { return &___taixudeatheffect_65; }
	inline void set_taixudeatheffect_65(int32_t value)
	{
		___taixudeatheffect_65 = value;
	}

	inline static int32_t get_offset_of_reviveEffectId_66() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___reviveEffectId_66)); }
	inline int32_t get_reviveEffectId_66() const { return ___reviveEffectId_66; }
	inline int32_t* get_address_of_reviveEffectId_66() { return &___reviveEffectId_66; }
	inline void set_reviveEffectId_66(int32_t value)
	{
		___reviveEffectId_66 = value;
	}

	inline static int32_t get_offset_of_skinID_67() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___skinID_67)); }
	inline int32_t get_skinID_67() const { return ___skinID_67; }
	inline int32_t* get_address_of_skinID_67() { return &___skinID_67; }
	inline void set_skinID_67(int32_t value)
	{
		___skinID_67 = value;
	}

	inline static int32_t get_offset_of_storyID_68() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___storyID_68)); }
	inline String_t* get_storyID_68() const { return ___storyID_68; }
	inline String_t** get_address_of_storyID_68() { return &___storyID_68; }
	inline void set_storyID_68(String_t* value)
	{
		___storyID_68 = value;
		Il2CppCodeGenWriteBarrier(&___storyID_68, value);
	}

	inline static int32_t get_offset_of_resume_69() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___resume_69)); }
	inline String_t* get_resume_69() const { return ___resume_69; }
	inline String_t** get_address_of_resume_69() { return &___resume_69; }
	inline void set_resume_69(String_t* value)
	{
		___resume_69 = value;
		Il2CppCodeGenWriteBarrier(&___resume_69, value);
	}

	inline static int32_t get_offset_of_attack_growup_70() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___attack_growup_70)); }
	inline float get_attack_growup_70() const { return ___attack_growup_70; }
	inline float* get_address_of_attack_growup_70() { return &___attack_growup_70; }
	inline void set_attack_growup_70(float value)
	{
		___attack_growup_70 = value;
	}

	inline static int32_t get_offset_of_phydef_growup_71() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___phydef_growup_71)); }
	inline float get_phydef_growup_71() const { return ___phydef_growup_71; }
	inline float* get_address_of_phydef_growup_71() { return &___phydef_growup_71; }
	inline void set_phydef_growup_71(float value)
	{
		___phydef_growup_71 = value;
	}

	inline static int32_t get_offset_of_magdef_growup_72() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___magdef_growup_72)); }
	inline float get_magdef_growup_72() const { return ___magdef_growup_72; }
	inline float* get_address_of_magdef_growup_72() { return &___magdef_growup_72; }
	inline void set_magdef_growup_72(float value)
	{
		___magdef_growup_72 = value;
	}

	inline static int32_t get_offset_of_hp_growup_73() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___hp_growup_73)); }
	inline float get_hp_growup_73() const { return ___hp_growup_73; }
	inline float* get_address_of_hp_growup_73() { return &___hp_growup_73; }
	inline void set_hp_growup_73(float value)
	{
		___hp_growup_73 = value;
	}

	inline static int32_t get_offset_of_hitrate_growup_74() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___hitrate_growup_74)); }
	inline float get_hitrate_growup_74() const { return ___hitrate_growup_74; }
	inline float* get_address_of_hitrate_growup_74() { return &___hitrate_growup_74; }
	inline void set_hitrate_growup_74(float value)
	{
		___hitrate_growup_74 = value;
	}

	inline static int32_t get_offset_of_dodge_growup_75() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___dodge_growup_75)); }
	inline float get_dodge_growup_75() const { return ___dodge_growup_75; }
	inline float* get_address_of_dodge_growup_75() { return &___dodge_growup_75; }
	inline void set_dodge_growup_75(float value)
	{
		___dodge_growup_75 = value;
	}

	inline static int32_t get_offset_of_destory_growup_76() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___destory_growup_76)); }
	inline float get_destory_growup_76() const { return ___destory_growup_76; }
	inline float* get_address_of_destory_growup_76() { return &___destory_growup_76; }
	inline void set_destory_growup_76(float value)
	{
		___destory_growup_76 = value;
	}

	inline static int32_t get_offset_of_block_growup_77() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___block_growup_77)); }
	inline float get_block_growup_77() const { return ___block_growup_77; }
	inline float* get_address_of_block_growup_77() { return &___block_growup_77; }
	inline void set_block_growup_77(float value)
	{
		___block_growup_77 = value;
	}

	inline static int32_t get_offset_of_crit_growup_78() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___crit_growup_78)); }
	inline float get_crit_growup_78() const { return ___crit_growup_78; }
	inline float* get_address_of_crit_growup_78() { return &___crit_growup_78; }
	inline void set_crit_growup_78(float value)
	{
		___crit_growup_78 = value;
	}

	inline static int32_t get_offset_of_anticrit_growup_79() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___anticrit_growup_79)); }
	inline float get_anticrit_growup_79() const { return ___anticrit_growup_79; }
	inline float* get_address_of_anticrit_growup_79() { return &___anticrit_growup_79; }
	inline void set_anticrit_growup_79(float value)
	{
		___anticrit_growup_79 = value;
	}

	inline static int32_t get_offset_of_damage_bonus_growup_80() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___damage_bonus_growup_80)); }
	inline float get_damage_bonus_growup_80() const { return ___damage_bonus_growup_80; }
	inline float* get_address_of_damage_bonus_growup_80() { return &___damage_bonus_growup_80; }
	inline void set_damage_bonus_growup_80(float value)
	{
		___damage_bonus_growup_80 = value;
	}

	inline static int32_t get_offset_of_damage_reduce_growup_81() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___damage_reduce_growup_81)); }
	inline float get_damage_reduce_growup_81() const { return ___damage_reduce_growup_81; }
	inline float* get_address_of_damage_reduce_growup_81() { return &___damage_reduce_growup_81; }
	inline void set_damage_reduce_growup_81(float value)
	{
		___damage_reduce_growup_81 = value;
	}

	inline static int32_t get_offset_of_real_damage_growup_82() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___real_damage_growup_82)); }
	inline float get_real_damage_growup_82() const { return ___real_damage_growup_82; }
	inline float* get_address_of_real_damage_growup_82() { return &___real_damage_growup_82; }
	inline void set_real_damage_growup_82(float value)
	{
		___real_damage_growup_82 = value;
	}

	inline static int32_t get_offset_of_offsetPos_83() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___offsetPos_83)); }
	inline String_t* get_offsetPos_83() const { return ___offsetPos_83; }
	inline String_t** get_address_of_offsetPos_83() { return &___offsetPos_83; }
	inline void set_offsetPos_83(String_t* value)
	{
		___offsetPos_83 = value;
		Il2CppCodeGenWriteBarrier(&___offsetPos_83, value);
	}

	inline static int32_t get_offset_of_offsetRob_84() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___offsetRob_84)); }
	inline String_t* get_offsetRob_84() const { return ___offsetRob_84; }
	inline String_t** get_address_of_offsetRob_84() { return &___offsetRob_84; }
	inline void set_offsetRob_84(String_t* value)
	{
		___offsetRob_84 = value;
		Il2CppCodeGenWriteBarrier(&___offsetRob_84, value);
	}

	inline static int32_t get_offset_of_offsetScale_85() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___offsetScale_85)); }
	inline float get_offsetScale_85() const { return ___offsetScale_85; }
	inline float* get_address_of_offsetScale_85() { return &___offsetScale_85; }
	inline void set_offsetScale_85(float value)
	{
		___offsetScale_85 = value;
	}

	inline static int32_t get_offset_of_isTrue_86() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___isTrue_86)); }
	inline int32_t get_isTrue_86() const { return ___isTrue_86; }
	inline int32_t* get_address_of_isTrue_86() { return &___isTrue_86; }
	inline void set_isTrue_86(int32_t value)
	{
		___isTrue_86 = value;
	}

	inline static int32_t get_offset_of_ifattacked_87() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___ifattacked_87)); }
	inline int32_t get_ifattacked_87() const { return ___ifattacked_87; }
	inline int32_t* get_address_of_ifattacked_87() { return &___ifattacked_87; }
	inline void set_ifattacked_87(int32_t value)
	{
		___ifattacked_87 = value;
	}

	inline static int32_t get_offset_of_ifeffects_88() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___ifeffects_88)); }
	inline int32_t get_ifeffects_88() const { return ___ifeffects_88; }
	inline int32_t* get_address_of_ifeffects_88() { return &___ifeffects_88; }
	inline void set_ifeffects_88(int32_t value)
	{
		___ifeffects_88 = value;
	}

	inline static int32_t get_offset_of_damageshowpos_89() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___damageshowpos_89)); }
	inline float get_damageshowpos_89() const { return ___damageshowpos_89; }
	inline float* get_address_of_damageshowpos_89() { return &___damageshowpos_89; }
	inline void set_damageshowpos_89(float value)
	{
		___damageshowpos_89 = value;
	}

	inline static int32_t get_offset_of_rgb_90() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___rgb_90)); }
	inline String_t* get_rgb_90() const { return ___rgb_90; }
	inline String_t** get_address_of_rgb_90() { return &___rgb_90; }
	inline void set_rgb_90(String_t* value)
	{
		___rgb_90 = value;
		Il2CppCodeGenWriteBarrier(&___rgb_90, value);
	}

	inline static int32_t get_offset_of_showrgb_91() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___showrgb_91)); }
	inline String_t* get_showrgb_91() const { return ___showrgb_91; }
	inline String_t** get_address_of_showrgb_91() { return &___showrgb_91; }
	inline void set_showrgb_91(String_t* value)
	{
		___showrgb_91 = value;
		Il2CppCodeGenWriteBarrier(&___showrgb_91, value);
	}

	inline static int32_t get_offset_of_rim_92() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___rim_92)); }
	inline float get_rim_92() const { return ___rim_92; }
	inline float* get_address_of_rim_92() { return &___rim_92; }
	inline void set_rim_92(float value)
	{
		___rim_92 = value;
	}

	inline static int32_t get_offset_of_findEnemyRange_93() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___findEnemyRange_93)); }
	inline float get_findEnemyRange_93() const { return ___findEnemyRange_93; }
	inline float* get_address_of_findEnemyRange_93() { return &___findEnemyRange_93; }
	inline void set_findEnemyRange_93(float value)
	{
		___findEnemyRange_93 = value;
	}

	inline static int32_t get_offset_of_isInFinish_94() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___isInFinish_94)); }
	inline bool get_isInFinish_94() const { return ___isInFinish_94; }
	inline bool* get_address_of_isInFinish_94() { return &___isInFinish_94; }
	inline void set_isInFinish_94(bool value)
	{
		___isInFinish_94 = value;
	}

	inline static int32_t get_offset_of_initBehavior_95() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___initBehavior_95)); }
	inline int32_t get_initBehavior_95() const { return ___initBehavior_95; }
	inline int32_t* get_address_of_initBehavior_95() { return &___initBehavior_95; }
	inline void set_initBehavior_95(int32_t value)
	{
		___initBehavior_95 = value;
	}

	inline static int32_t get_offset_of_isRuneffect_96() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___isRuneffect_96)); }
	inline bool get_isRuneffect_96() const { return ___isRuneffect_96; }
	inline bool* get_address_of_isRuneffect_96() { return &___isRuneffect_96; }
	inline void set_isRuneffect_96(bool value)
	{
		___isRuneffect_96 = value;
	}

	inline static int32_t get_offset_of_isInNum_97() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___isInNum_97)); }
	inline bool get_isInNum_97() const { return ___isInNum_97; }
	inline bool* get_address_of_isInNum_97() { return &___isInNum_97; }
	inline void set_isInNum_97(bool value)
	{
		___isInNum_97 = value;
	}

	inline static int32_t get_offset_of_isHideSummon_98() { return static_cast<int32_t>(offsetof(monstersCfg_t1542396363, ___isHideSummon_98)); }
	inline bool get_isHideSummon_98() const { return ___isHideSummon_98; }
	inline bool* get_address_of_isHideSummon_98() { return &___isHideSummon_98; }
	inline void set_isHideSummon_98(bool value)
	{
		___isHideSummon_98 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
