﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader[]
struct ShaderU5BU5D_t3604329748;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetShaderList
struct  AssetShaderList_t361542931  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Shader[] AssetShaderList::goList
	ShaderU5BU5D_t3604329748* ___goList_2;

public:
	inline static int32_t get_offset_of_goList_2() { return static_cast<int32_t>(offsetof(AssetShaderList_t361542931, ___goList_2)); }
	inline ShaderU5BU5D_t3604329748* get_goList_2() const { return ___goList_2; }
	inline ShaderU5BU5D_t3604329748** get_address_of_goList_2() { return &___goList_2; }
	inline void set_goList_2(ShaderU5BU5D_t3604329748* value)
	{
		___goList_2 = value;
		Il2CppCodeGenWriteBarrier(&___goList_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
