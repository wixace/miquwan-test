﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo1432926611MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m2303483340(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t4007816337 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1366664402_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m586467766(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t4007816337 *, CrashReport_t2450738801 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2541166012_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3007322580(__this, method) ((  void (*) (ReadOnlyCollection_1_t4007816337 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3473426062_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m241246493(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t4007816337 *, int32_t, CrashReport_t2450738801 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3496388003_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m897232637(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t4007816337 *, CrashReport_t2450738801 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m348744375_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2410066659(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t4007816337 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1370240873_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1045267559(__this, ___index0, method) ((  CrashReport_t2450738801 * (*) (ReadOnlyCollection_1_t4007816337 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3534609325_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1719242420(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4007816337 *, int32_t, CrashReport_t2450738801 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3174042042_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2957420530(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4007816337 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2459576056_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2728481403(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t4007816337 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1945557633_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2824192822(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4007816337 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3330065468_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m4253623931(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4007816337 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1628967861_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1541344649(__this, method) ((  void (*) (ReadOnlyCollection_1_t4007816337 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m514207119_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1464975665(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t4007816337 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m736178103_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3733694291(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4007816337 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3658311565_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m698667262(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4007816337 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2823806264_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m838407018(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t4007816337 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2498539760_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1806169486(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t4007816337 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1730676936_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1922317451(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4007816337 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1373829189_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m444719415(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4007816337 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m918746289_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2706008416(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4007816337 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m932754534_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2342414745(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4007816337 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2423760339_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m3239207166(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4007816337 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3512499704_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2018293077(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4007816337 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m4167408399_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::Contains(T)
#define ReadOnlyCollection_1_Contains_m4211785026(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t4007816337 *, CrashReport_t2450738801 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m687553276_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m1933991654(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t4007816337 *, CrashReportU5BU5D_t612474028*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m475587820_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m958961061(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t4007816337 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m809369055_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m1540838634(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4007816337 *, CrashReport_t2450738801 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m817393776_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::get_Count()
#define ReadOnlyCollection_1_get_Count_m338664913(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t4007816337 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3681678091_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CrashReport>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m784453031(__this, ___index0, method) ((  CrashReport_t2450738801 * (*) (ReadOnlyCollection_1_t4007816337 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2421641197_gshared)(__this, ___index0, method)
