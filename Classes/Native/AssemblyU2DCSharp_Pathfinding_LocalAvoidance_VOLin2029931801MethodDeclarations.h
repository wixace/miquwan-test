﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.LocalAvoidance/VO
struct VO_t2172220293;
// Pathfinding.LocalAvoidance/VOLine
struct VOLine_t2029931801;
struct VOLine_t2029931801_marshaled_pinvoke;
struct VOLine_t2029931801_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_VOLin2029931801.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_VO2172220293.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void Pathfinding.LocalAvoidance/VOLine::.ctor(Pathfinding.LocalAvoidance/VO,UnityEngine.Vector3,UnityEngine.Vector3,System.Boolean,System.Int32,System.Boolean)
extern "C"  void VOLine__ctor_m354744034 (VOLine_t2029931801 * __this, VO_t2172220293 * ___vo0, Vector3_t4282066566  ___start1, Vector3_t4282066566  ___end2, bool ___inf3, int32_t ___id4, bool ___wrongSide5, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct VOLine_t2029931801;
struct VOLine_t2029931801_marshaled_pinvoke;

extern "C" void VOLine_t2029931801_marshal_pinvoke(const VOLine_t2029931801& unmarshaled, VOLine_t2029931801_marshaled_pinvoke& marshaled);
extern "C" void VOLine_t2029931801_marshal_pinvoke_back(const VOLine_t2029931801_marshaled_pinvoke& marshaled, VOLine_t2029931801& unmarshaled);
extern "C" void VOLine_t2029931801_marshal_pinvoke_cleanup(VOLine_t2029931801_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct VOLine_t2029931801;
struct VOLine_t2029931801_marshaled_com;

extern "C" void VOLine_t2029931801_marshal_com(const VOLine_t2029931801& unmarshaled, VOLine_t2029931801_marshaled_com& marshaled);
extern "C" void VOLine_t2029931801_marshal_com_back(const VOLine_t2029931801_marshaled_com& marshaled, VOLine_t2029931801& unmarshaled);
extern "C" void VOLine_t2029931801_marshal_com_cleanup(VOLine_t2029931801_marshaled_com& marshaled);
