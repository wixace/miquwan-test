﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginQuick
struct  PluginQuick_t2552428122  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginQuick::uid
	String_t* ___uid_5;
	// System.String PluginQuick::session
	String_t* ___session_6;
	// System.String PluginQuick::channel
	String_t* ___channel_7;
	// System.String PluginQuick::adid
	String_t* ___adid_8;
	// System.String PluginQuick::uname
	String_t* ___uname_9;
	// System.Boolean PluginQuick::isOpenUserSwitch
	bool ___isOpenUserSwitch_10;
	// System.String[] PluginQuick::sdkInfos
	StringU5BU5D_t4054002952* ___sdkInfos_11;

public:
	inline static int32_t get_offset_of_uid_5() { return static_cast<int32_t>(offsetof(PluginQuick_t2552428122, ___uid_5)); }
	inline String_t* get_uid_5() const { return ___uid_5; }
	inline String_t** get_address_of_uid_5() { return &___uid_5; }
	inline void set_uid_5(String_t* value)
	{
		___uid_5 = value;
		Il2CppCodeGenWriteBarrier(&___uid_5, value);
	}

	inline static int32_t get_offset_of_session_6() { return static_cast<int32_t>(offsetof(PluginQuick_t2552428122, ___session_6)); }
	inline String_t* get_session_6() const { return ___session_6; }
	inline String_t** get_address_of_session_6() { return &___session_6; }
	inline void set_session_6(String_t* value)
	{
		___session_6 = value;
		Il2CppCodeGenWriteBarrier(&___session_6, value);
	}

	inline static int32_t get_offset_of_channel_7() { return static_cast<int32_t>(offsetof(PluginQuick_t2552428122, ___channel_7)); }
	inline String_t* get_channel_7() const { return ___channel_7; }
	inline String_t** get_address_of_channel_7() { return &___channel_7; }
	inline void set_channel_7(String_t* value)
	{
		___channel_7 = value;
		Il2CppCodeGenWriteBarrier(&___channel_7, value);
	}

	inline static int32_t get_offset_of_adid_8() { return static_cast<int32_t>(offsetof(PluginQuick_t2552428122, ___adid_8)); }
	inline String_t* get_adid_8() const { return ___adid_8; }
	inline String_t** get_address_of_adid_8() { return &___adid_8; }
	inline void set_adid_8(String_t* value)
	{
		___adid_8 = value;
		Il2CppCodeGenWriteBarrier(&___adid_8, value);
	}

	inline static int32_t get_offset_of_uname_9() { return static_cast<int32_t>(offsetof(PluginQuick_t2552428122, ___uname_9)); }
	inline String_t* get_uname_9() const { return ___uname_9; }
	inline String_t** get_address_of_uname_9() { return &___uname_9; }
	inline void set_uname_9(String_t* value)
	{
		___uname_9 = value;
		Il2CppCodeGenWriteBarrier(&___uname_9, value);
	}

	inline static int32_t get_offset_of_isOpenUserSwitch_10() { return static_cast<int32_t>(offsetof(PluginQuick_t2552428122, ___isOpenUserSwitch_10)); }
	inline bool get_isOpenUserSwitch_10() const { return ___isOpenUserSwitch_10; }
	inline bool* get_address_of_isOpenUserSwitch_10() { return &___isOpenUserSwitch_10; }
	inline void set_isOpenUserSwitch_10(bool value)
	{
		___isOpenUserSwitch_10 = value;
	}

	inline static int32_t get_offset_of_sdkInfos_11() { return static_cast<int32_t>(offsetof(PluginQuick_t2552428122, ___sdkInfos_11)); }
	inline StringU5BU5D_t4054002952* get_sdkInfos_11() const { return ___sdkInfos_11; }
	inline StringU5BU5D_t4054002952** get_address_of_sdkInfos_11() { return &___sdkInfos_11; }
	inline void set_sdkInfos_11(StringU5BU5D_t4054002952* value)
	{
		___sdkInfos_11 = value;
		Il2CppCodeGenWriteBarrier(&___sdkInfos_11, value);
	}
};

struct PluginQuick_t2552428122_StaticFields
{
public:
	// System.Action PluginQuick::<>f__am$cache7
	Action_t3771233898 * ___U3CU3Ef__amU24cache7_12;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_12() { return static_cast<int32_t>(offsetof(PluginQuick_t2552428122_StaticFields, ___U3CU3Ef__amU24cache7_12)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache7_12() const { return ___U3CU3Ef__amU24cache7_12; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache7_12() { return &___U3CU3Ef__amU24cache7_12; }
	inline void set_U3CU3Ef__amU24cache7_12(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache7_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
