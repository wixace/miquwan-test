﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Portal
struct Portal_t2396353676;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// CombatEntity
struct CombatEntity_t684137495;
// monstersCfg
struct monstersCfg_t1542396363;
// JSCLevelMonsterConfig
struct JSCLevelMonsterConfig_t1924079698;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// Monster
struct Monster_t2901270458;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// FightCtrl
struct FightCtrl_t648967803;
// System.String
struct String_t;
// GameMgr
struct GameMgr_t1469029542;
// NpcMgr
struct NpcMgr_t2339534743;
// Boss
struct Boss_t2076557;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_monstersCfg1542396363.h"
#include "AssemblyU2DCSharp_JSCLevelMonsterConfig1924079698.h"
#include "AssemblyU2DCSharp_FightEnum_EDamageType2535357340.h"
#include "AssemblyU2DCSharp_Monster2901270458.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_FightCtrl648967803.h"
#include "AssemblyU2DCSharp_Portal2396353676.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"
#include "AssemblyU2DCSharp_NpcMgr2339534743.h"
#include "AssemblyU2DCSharp_Boss2076557.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitType643359572.h"

// System.Void Portal::.ctor()
extern "C"  void Portal__ctor_m2088652175 (Portal_t2396353676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::set_children(UnityEngine.GameObject)
extern "C"  void Portal_set_children_m4048013481 (Portal_t2396353676 * __this, GameObject_t3674682005 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Portal::get_children()
extern "C"  GameObject_t3674682005 * Portal_get_children_m921326962 (Portal_t2396353676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::set_isPortalFinish(System.Boolean)
extern "C"  void Portal_set_isPortalFinish_m3716912978 (Portal_t2396353676 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Portal::get_isPortalFinish()
extern "C"  bool Portal_get_isPortalFinish_m3040541019 (Portal_t2396353676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::set_curRound(System.Int32)
extern "C"  void Portal_set_curRound_m98218673 (Portal_t2396353676 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Portal::get_curRound()
extern "C"  int32_t Portal_get_curRound_m574922106 (Portal_t2396353676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::OnTriggerBehaivir(CombatEntity)
extern "C"  void Portal_OnTriggerBehaivir_m1293848001 (Portal_t2396353676 * __this, CombatEntity_t684137495 * ___main0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::Init()
extern "C"  void Portal_Init_m3759690469 (Portal_t2396353676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::Create(monstersCfg,JSCLevelMonsterConfig)
extern "C"  void Portal_Create_m3333646704 (Portal_t2396353676 * __this, monstersCfg_t1542396363 * ___mcfg0, JSCLevelMonsterConfig_t1924079698 * ___lmcfg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::set_MonseterNum(System.Int32)
extern "C"  void Portal_set_MonseterNum_m2167147044 (Portal_t2396353676 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Portal::get_MonseterNum()
extern "C"  int32_t Portal_get_MonseterNum_m1455684345 (Portal_t2396353676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::SplitMonsterArr()
extern "C"  void Portal_SplitMonsterArr_m1763781198 (Portal_t2396353676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Portal::TopMonsterId()
extern "C"  int32_t Portal_TopMonsterId_m3687678499 (Portal_t2396353676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int32> Portal::TopMonsterIds()
extern "C"  List_1_t2522024052 * Portal_TopMonsterIds_m1119913406 (Portal_t2396353676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::OnUseingSkill(System.Int32)
extern "C"  void Portal_OnUseingSkill_m1353943797 (Portal_t2396353676 * __this, int32_t ___skillID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::SetRunSpeed(System.Single)
extern "C"  void Portal_SetRunSpeed_m4111020640 (Portal_t2396353676 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::OnTargetReached()
extern "C"  void Portal_OnTargetReached_m2643330575 (Portal_t2396353676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::UpdateHpBar()
extern "C"  void Portal_UpdateHpBar_m2518157263 (Portal_t2396353676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::BeAttacked(System.Int32,CombatEntity,FightEnum.EDamageType,System.Int32)
extern "C"  void Portal_BeAttacked_m3023767037 (Portal_t2396353676 * __this, int32_t ___hp0, CombatEntity_t684137495 * ___src1, int32_t ___damageType2, int32_t ___skillID3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::Update()
extern "C"  void Portal_Update_m2050570078 (Portal_t2396353676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::Dead()
extern "C"  void Portal_Dead_m3607979321 (Portal_t2396353676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::RemoveChile(Monster)
extern "C"  void Portal_RemoveChile_m688284370 (Portal_t2396353676 * __this, Monster_t2901270458 * ___chil0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::BrushMonsters(System.Int32,JSCLevelMonsterConfig)
extern "C"  void Portal_BrushMonsters_m2103835531 (Portal_t2396353676 * __this, int32_t ___monsterId0, JSCLevelMonsterConfig_t1924079698 * ___lmconfig1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Portal::GetYPos(UnityEngine.Transform)
extern "C"  float Portal_GetYPos_m1828512459 (Portal_t2396353676 * __this, Transform_t1659122786 * ___tr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] Portal::GetPosition(System.Int32)
extern "C"  Vector3U5BU5D_t215400611* Portal_GetPosition_m3730734341 (Portal_t2396353676 * __this, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::Clear()
extern "C"  void Portal_Clear_m3789752762 (Portal_t2396353676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::ilo_TranBehavior1(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void Portal_ilo_TranBehavior1_m3327994694 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, uint8_t ___id1, ObjectU5BU5D_t1108656482* ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::ilo_set_fightCtrl2(CombatEntity,FightCtrl)
extern "C"  void Portal_ilo_set_fightCtrl2_m711115830 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, FightCtrl_t648967803 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Portal::ilo_get_skills3(CombatEntity)
extern "C"  String_t* Portal_ilo_get_skills3_m1621895112 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightCtrl Portal::ilo_get_fightCtrl4(CombatEntity)
extern "C"  FightCtrl_t648967803 * Portal_ilo_get_fightCtrl4_m896061547 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Portal::ilo_get_moveTargetRoot5()
extern "C"  GameObject_t3674682005 * Portal_ilo_get_moveTargetRoot5_m788992621 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::ilo_ChangeParent6(UnityEngine.GameObject,UnityEngine.GameObject,System.Boolean)
extern "C"  void Portal_ilo_ChangeParent6_m2941760205 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, GameObject_t3674682005 * ___parent1, bool ___resetMat2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl Portal::ilo_get_bvrCtrl7(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * Portal_ilo_get_bvrCtrl7_m2584667910 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::ilo_set_isPortalFinish8(Portal,System.Boolean)
extern "C"  void Portal_ilo_set_isPortalFinish8_m668489917 (Il2CppObject * __this /* static, unused */, Portal_t2396353676 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::ilo_Create9(Monster,monstersCfg,JSCLevelMonsterConfig)
extern "C"  void Portal_ilo_Create9_m458435036 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, monstersCfg_t1542396363 * ___mcfg1, JSCLevelMonsterConfig_t1924079698 * ___lmcfg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::ilo_set_children10(Portal,UnityEngine.GameObject)
extern "C"  void Portal_ilo_set_children10_m3570761639 (Il2CppObject * __this /* static, unused */, Portal_t2396353676 * ____this0, GameObject_t3674682005 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Portal::ilo_get_id11(CombatEntity)
extern "C"  int32_t Portal_ilo_get_id11_m1209445315 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Portal::ilo_get_MonseterNum12(Portal)
extern "C"  int32_t Portal_ilo_get_MonseterNum12_m2589291835 (Il2CppObject * __this /* static, unused */, Portal_t2396353676 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Portal::ilo_get_isBattleEnd13(GameMgr)
extern "C"  bool Portal_ilo_get_isBattleEnd13_m730829780 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::ilo_set_x14(JSCLevelMonsterConfig,System.Single)
extern "C"  void Portal_ilo_set_x14_m2696521165 (Il2CppObject * __this /* static, unused */, JSCLevelMonsterConfig_t1924079698 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::ilo_set_rot15(JSCLevelMonsterConfig,System.Single)
extern "C"  void Portal_ilo_set_rot15_m457716525 (Il2CppObject * __this /* static, unused */, JSCLevelMonsterConfig_t1924079698 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] Portal::ilo_GetPosition16(Portal,System.Int32)
extern "C"  Vector3U5BU5D_t215400611* Portal_ilo_GetPosition16_m187350685 (Il2CppObject * __this /* static, unused */, Portal_t2396353676 * ____this0, int32_t ___size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Portal::ilo_get_level17(CombatEntity)
extern "C"  int32_t Portal_ilo_get_level17_m2583390070 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::ilo_BrushMonsters18(Portal,System.Int32,JSCLevelMonsterConfig)
extern "C"  void Portal_ilo_BrushMonsters18_m2759253449 (Il2CppObject * __this /* static, unused */, Portal_t2396353676 * ____this0, int32_t ___monsterId1, JSCLevelMonsterConfig_t1924079698 * ___lmconfig2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::ilo_OnTargetReached19(Monster)
extern "C"  void Portal_ilo_OnTargetReached19_m2792284116 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Portal::ilo_get_isDeath20(CombatEntity)
extern "C"  bool Portal_ilo_get_isDeath20_m719602062 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::ilo_Dead21(Portal)
extern "C"  void Portal_ilo_Dead21_m4157386335 (Il2CppObject * __this /* static, unused */, Portal_t2396353676 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Portal::ilo_get_curRound22(Portal)
extern "C"  int32_t Portal_ilo_get_curRound22_m1140222625 (Il2CppObject * __this /* static, unused */, Portal_t2396353676 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::ilo_RemoveAllStates23(CombatEntity)
extern "C"  void Portal_ilo_RemoveAllStates23_m1989229635 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::ilo_Clear24(FightCtrl)
extern "C"  void Portal_ilo_Clear24_m391315150 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Portal::ilo_Instantiate25(UnityEngine.GameObject)
extern "C"  GameObject_t3674682005 * Portal_ilo_Instantiate25_m1022865322 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Portal::ilo_get_x26(JSCLevelMonsterConfig)
extern "C"  float Portal_ilo_get_x26_m67538431 (Il2CppObject * __this /* static, unused */, JSCLevelMonsterConfig_t1924079698 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::ilo_set_y27(JSCLevelMonsterConfig,System.Single)
extern "C"  void Portal_ilo_set_y27_m3838862282 (Il2CppObject * __this /* static, unused */, JSCLevelMonsterConfig_t1924079698 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NpcMgr Portal::ilo_get_instance28()
extern "C"  NpcMgr_t2339534743 * Portal_ilo_get_instance28_m1530615890 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::ilo_set_boss29(NpcMgr,Boss)
extern "C"  void Portal_ilo_set_boss29_m624427767 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, Boss_t2076557 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::ilo_set_unitType30(CombatEntity,EntityEnum.UnitType)
extern "C"  void Portal_ilo_set_unitType30_m2960548877 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::ilo_Create31(Monster,Portal,monstersCfg,JSCLevelMonsterConfig)
extern "C"  void Portal_ilo_Create31_m2140454079 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, Portal_t2396353676 * ___por1, monstersCfg_t1542396363 * ___mcfg2, JSCLevelMonsterConfig_t1924079698 * ___lmcfg3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::ilo_OnTriggerFun32(Monster,CombatEntity,System.Int32)
extern "C"  void Portal_ilo_OnTriggerFun32_m3909232243 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, CombatEntity_t684137495 * ___main1, int32_t ___effectId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
