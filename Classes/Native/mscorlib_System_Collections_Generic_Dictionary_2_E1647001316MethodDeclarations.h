﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3484314264MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m4194122529(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1647001316 *, Dictionary_2_t329677924 *, const MethodInfo*))Enumerator__ctor_m3455248874_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,System.Single>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1438645482(__this, method) ((  Il2CppObject * (*) (Enumerator_t1647001316 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3267178999_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,System.Single>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2657649396(__this, method) ((  void (*) (Enumerator_t1647001316 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m692652171_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,System.Single>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1768013483(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1647001316 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m948350932_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,System.Single>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2496946758(__this, method) ((  Il2CppObject * (*) (Enumerator_t1647001316 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3926317459_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,System.Single>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m4022519640(__this, method) ((  Il2CppObject * (*) (Enumerator_t1647001316 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3258228581_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,System.Single>::MoveNext()
#define Enumerator_MoveNext_m2633846436(__this, method) ((  bool (*) (Enumerator_t1647001316 *, const MethodInfo*))Enumerator_MoveNext_m2518547447_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,System.Single>::get_Current()
#define Enumerator_get_Current_m3355195288(__this, method) ((  KeyValuePair_2_t228458630  (*) (Enumerator_t1647001316 *, const MethodInfo*))Enumerator_get_Current_m3624402649_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,System.Single>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m508832301(__this, method) ((  CombatEntity_t684137495 * (*) (Enumerator_t1647001316 *, const MethodInfo*))Enumerator_get_CurrentKey_m3221742212_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,System.Single>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m788914477(__this, method) ((  float (*) (Enumerator_t1647001316 *, const MethodInfo*))Enumerator_get_CurrentValue_m3627513384_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,System.Single>::Reset()
#define Enumerator_Reset_m1699918835(__this, method) ((  void (*) (Enumerator_t1647001316 *, const MethodInfo*))Enumerator_Reset_m2659337532_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,System.Single>::VerifyState()
#define Enumerator_VerifyState_m64881084(__this, method) ((  void (*) (Enumerator_t1647001316 *, const MethodInfo*))Enumerator_VerifyState_m3674454085_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,System.Single>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m671084068(__this, method) ((  void (*) (Enumerator_t1647001316 *, const MethodInfo*))Enumerator_VerifyCurrent_m3432130157_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,System.Single>::Dispose()
#define Enumerator_Dispose_m4057857027(__this, method) ((  void (*) (Enumerator_t1647001316 *, const MethodInfo*))Enumerator_Dispose_m2641256204_gshared)(__this, method)
