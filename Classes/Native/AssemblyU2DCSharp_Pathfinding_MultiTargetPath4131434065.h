﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OnPathDelegate[]
struct OnPathDelegateU5BU5D_t1505600468;
// Pathfinding.GraphNode[]
struct GraphNodeU5BU5D_t927449255;
// System.Boolean[]
struct BooleanU5BU5D_t3456302923;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Collections.Generic.List`1<UnityEngine.Vector3>[]
struct List_1U5BU5D_t1642958995;
// System.Collections.Generic.List`1<Pathfinding.GraphNode>[]
struct List_1U5BU5D_t2355007639;

#include "AssemblyU2DCSharp_Pathfinding_ABPath1187561148.h"
#include "AssemblyU2DCSharp_Pathfinding_MultiTargetPath_Heuri814064675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.MultiTargetPath
struct  MultiTargetPath_t4131434065  : public ABPath_t1187561148
{
public:
	// OnPathDelegate[] Pathfinding.MultiTargetPath::callbacks
	OnPathDelegateU5BU5D_t1505600468* ___callbacks_52;
	// Pathfinding.GraphNode[] Pathfinding.MultiTargetPath::targetNodes
	GraphNodeU5BU5D_t927449255* ___targetNodes_53;
	// System.Int32 Pathfinding.MultiTargetPath::targetNodeCount
	int32_t ___targetNodeCount_54;
	// System.Boolean[] Pathfinding.MultiTargetPath::targetsFound
	BooleanU5BU5D_t3456302923* ___targetsFound_55;
	// UnityEngine.Vector3[] Pathfinding.MultiTargetPath::targetPoints
	Vector3U5BU5D_t215400611* ___targetPoints_56;
	// UnityEngine.Vector3[] Pathfinding.MultiTargetPath::originalTargetPoints
	Vector3U5BU5D_t215400611* ___originalTargetPoints_57;
	// System.Collections.Generic.List`1<UnityEngine.Vector3>[] Pathfinding.MultiTargetPath::vectorPaths
	List_1U5BU5D_t1642958995* ___vectorPaths_58;
	// System.Collections.Generic.List`1<Pathfinding.GraphNode>[] Pathfinding.MultiTargetPath::nodePaths
	List_1U5BU5D_t2355007639* ___nodePaths_59;
	// System.Int32 Pathfinding.MultiTargetPath::endsFound
	int32_t ___endsFound_60;
	// System.Boolean Pathfinding.MultiTargetPath::pathsForAll
	bool ___pathsForAll_61;
	// System.Int32 Pathfinding.MultiTargetPath::chosenTarget
	int32_t ___chosenTarget_62;
	// System.Int32 Pathfinding.MultiTargetPath::sequentialTarget
	int32_t ___sequentialTarget_63;
	// Pathfinding.MultiTargetPath/HeuristicMode Pathfinding.MultiTargetPath::heuristicMode
	int32_t ___heuristicMode_64;
	// System.Boolean Pathfinding.MultiTargetPath::inverted
	bool ___inverted_65;

public:
	inline static int32_t get_offset_of_callbacks_52() { return static_cast<int32_t>(offsetof(MultiTargetPath_t4131434065, ___callbacks_52)); }
	inline OnPathDelegateU5BU5D_t1505600468* get_callbacks_52() const { return ___callbacks_52; }
	inline OnPathDelegateU5BU5D_t1505600468** get_address_of_callbacks_52() { return &___callbacks_52; }
	inline void set_callbacks_52(OnPathDelegateU5BU5D_t1505600468* value)
	{
		___callbacks_52 = value;
		Il2CppCodeGenWriteBarrier(&___callbacks_52, value);
	}

	inline static int32_t get_offset_of_targetNodes_53() { return static_cast<int32_t>(offsetof(MultiTargetPath_t4131434065, ___targetNodes_53)); }
	inline GraphNodeU5BU5D_t927449255* get_targetNodes_53() const { return ___targetNodes_53; }
	inline GraphNodeU5BU5D_t927449255** get_address_of_targetNodes_53() { return &___targetNodes_53; }
	inline void set_targetNodes_53(GraphNodeU5BU5D_t927449255* value)
	{
		___targetNodes_53 = value;
		Il2CppCodeGenWriteBarrier(&___targetNodes_53, value);
	}

	inline static int32_t get_offset_of_targetNodeCount_54() { return static_cast<int32_t>(offsetof(MultiTargetPath_t4131434065, ___targetNodeCount_54)); }
	inline int32_t get_targetNodeCount_54() const { return ___targetNodeCount_54; }
	inline int32_t* get_address_of_targetNodeCount_54() { return &___targetNodeCount_54; }
	inline void set_targetNodeCount_54(int32_t value)
	{
		___targetNodeCount_54 = value;
	}

	inline static int32_t get_offset_of_targetsFound_55() { return static_cast<int32_t>(offsetof(MultiTargetPath_t4131434065, ___targetsFound_55)); }
	inline BooleanU5BU5D_t3456302923* get_targetsFound_55() const { return ___targetsFound_55; }
	inline BooleanU5BU5D_t3456302923** get_address_of_targetsFound_55() { return &___targetsFound_55; }
	inline void set_targetsFound_55(BooleanU5BU5D_t3456302923* value)
	{
		___targetsFound_55 = value;
		Il2CppCodeGenWriteBarrier(&___targetsFound_55, value);
	}

	inline static int32_t get_offset_of_targetPoints_56() { return static_cast<int32_t>(offsetof(MultiTargetPath_t4131434065, ___targetPoints_56)); }
	inline Vector3U5BU5D_t215400611* get_targetPoints_56() const { return ___targetPoints_56; }
	inline Vector3U5BU5D_t215400611** get_address_of_targetPoints_56() { return &___targetPoints_56; }
	inline void set_targetPoints_56(Vector3U5BU5D_t215400611* value)
	{
		___targetPoints_56 = value;
		Il2CppCodeGenWriteBarrier(&___targetPoints_56, value);
	}

	inline static int32_t get_offset_of_originalTargetPoints_57() { return static_cast<int32_t>(offsetof(MultiTargetPath_t4131434065, ___originalTargetPoints_57)); }
	inline Vector3U5BU5D_t215400611* get_originalTargetPoints_57() const { return ___originalTargetPoints_57; }
	inline Vector3U5BU5D_t215400611** get_address_of_originalTargetPoints_57() { return &___originalTargetPoints_57; }
	inline void set_originalTargetPoints_57(Vector3U5BU5D_t215400611* value)
	{
		___originalTargetPoints_57 = value;
		Il2CppCodeGenWriteBarrier(&___originalTargetPoints_57, value);
	}

	inline static int32_t get_offset_of_vectorPaths_58() { return static_cast<int32_t>(offsetof(MultiTargetPath_t4131434065, ___vectorPaths_58)); }
	inline List_1U5BU5D_t1642958995* get_vectorPaths_58() const { return ___vectorPaths_58; }
	inline List_1U5BU5D_t1642958995** get_address_of_vectorPaths_58() { return &___vectorPaths_58; }
	inline void set_vectorPaths_58(List_1U5BU5D_t1642958995* value)
	{
		___vectorPaths_58 = value;
		Il2CppCodeGenWriteBarrier(&___vectorPaths_58, value);
	}

	inline static int32_t get_offset_of_nodePaths_59() { return static_cast<int32_t>(offsetof(MultiTargetPath_t4131434065, ___nodePaths_59)); }
	inline List_1U5BU5D_t2355007639* get_nodePaths_59() const { return ___nodePaths_59; }
	inline List_1U5BU5D_t2355007639** get_address_of_nodePaths_59() { return &___nodePaths_59; }
	inline void set_nodePaths_59(List_1U5BU5D_t2355007639* value)
	{
		___nodePaths_59 = value;
		Il2CppCodeGenWriteBarrier(&___nodePaths_59, value);
	}

	inline static int32_t get_offset_of_endsFound_60() { return static_cast<int32_t>(offsetof(MultiTargetPath_t4131434065, ___endsFound_60)); }
	inline int32_t get_endsFound_60() const { return ___endsFound_60; }
	inline int32_t* get_address_of_endsFound_60() { return &___endsFound_60; }
	inline void set_endsFound_60(int32_t value)
	{
		___endsFound_60 = value;
	}

	inline static int32_t get_offset_of_pathsForAll_61() { return static_cast<int32_t>(offsetof(MultiTargetPath_t4131434065, ___pathsForAll_61)); }
	inline bool get_pathsForAll_61() const { return ___pathsForAll_61; }
	inline bool* get_address_of_pathsForAll_61() { return &___pathsForAll_61; }
	inline void set_pathsForAll_61(bool value)
	{
		___pathsForAll_61 = value;
	}

	inline static int32_t get_offset_of_chosenTarget_62() { return static_cast<int32_t>(offsetof(MultiTargetPath_t4131434065, ___chosenTarget_62)); }
	inline int32_t get_chosenTarget_62() const { return ___chosenTarget_62; }
	inline int32_t* get_address_of_chosenTarget_62() { return &___chosenTarget_62; }
	inline void set_chosenTarget_62(int32_t value)
	{
		___chosenTarget_62 = value;
	}

	inline static int32_t get_offset_of_sequentialTarget_63() { return static_cast<int32_t>(offsetof(MultiTargetPath_t4131434065, ___sequentialTarget_63)); }
	inline int32_t get_sequentialTarget_63() const { return ___sequentialTarget_63; }
	inline int32_t* get_address_of_sequentialTarget_63() { return &___sequentialTarget_63; }
	inline void set_sequentialTarget_63(int32_t value)
	{
		___sequentialTarget_63 = value;
	}

	inline static int32_t get_offset_of_heuristicMode_64() { return static_cast<int32_t>(offsetof(MultiTargetPath_t4131434065, ___heuristicMode_64)); }
	inline int32_t get_heuristicMode_64() const { return ___heuristicMode_64; }
	inline int32_t* get_address_of_heuristicMode_64() { return &___heuristicMode_64; }
	inline void set_heuristicMode_64(int32_t value)
	{
		___heuristicMode_64 = value;
	}

	inline static int32_t get_offset_of_inverted_65() { return static_cast<int32_t>(offsetof(MultiTargetPath_t4131434065, ___inverted_65)); }
	inline bool get_inverted_65() const { return ___inverted_65; }
	inline bool* get_address_of_inverted_65() { return &___inverted_65; }
	inline void set_inverted_65(bool value)
	{
		___inverted_65 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
