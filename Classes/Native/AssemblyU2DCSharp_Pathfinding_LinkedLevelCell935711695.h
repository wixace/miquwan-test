﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.LinkedLevelNode
struct LinkedLevelNode_t936048751;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.LinkedLevelCell
struct  LinkedLevelCell_t935711695  : public Il2CppObject
{
public:
	// System.Int32 Pathfinding.LinkedLevelCell::count
	int32_t ___count_0;
	// System.Int32 Pathfinding.LinkedLevelCell::index
	int32_t ___index_1;
	// Pathfinding.LinkedLevelNode Pathfinding.LinkedLevelCell::first
	LinkedLevelNode_t936048751 * ___first_2;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(LinkedLevelCell_t935711695, ___count_0)); }
	inline int32_t get_count_0() const { return ___count_0; }
	inline int32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(LinkedLevelCell_t935711695, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_first_2() { return static_cast<int32_t>(offsetof(LinkedLevelCell_t935711695, ___first_2)); }
	inline LinkedLevelNode_t936048751 * get_first_2() const { return ___first_2; }
	inline LinkedLevelNode_t936048751 ** get_address_of_first_2() { return &___first_2; }
	inline void set_first_2(LinkedLevelNode_t936048751 * value)
	{
		___first_2 = value;
		Il2CppCodeGenWriteBarrier(&___first_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
