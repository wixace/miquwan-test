﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TypewriterEffectGenerated
struct TypewriterEffectGenerated_t3477007089;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void TypewriterEffectGenerated::.ctor()
extern "C"  void TypewriterEffectGenerated__ctor_m2910853530 (TypewriterEffectGenerated_t3477007089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TypewriterEffectGenerated::TypewriterEffect_TypewriterEffect1(JSVCall,System.Int32)
extern "C"  bool TypewriterEffectGenerated_TypewriterEffect_TypewriterEffect1_m2190624088 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TypewriterEffectGenerated::TypewriterEffect_current(JSVCall)
extern "C"  void TypewriterEffectGenerated_TypewriterEffect_current_m1981754661 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TypewriterEffectGenerated::TypewriterEffect_charsPerSecond(JSVCall)
extern "C"  void TypewriterEffectGenerated_TypewriterEffect_charsPerSecond_m4242095994 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TypewriterEffectGenerated::TypewriterEffect_fadeInTime(JSVCall)
extern "C"  void TypewriterEffectGenerated_TypewriterEffect_fadeInTime_m615139872 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TypewriterEffectGenerated::TypewriterEffect_delayOnPeriod(JSVCall)
extern "C"  void TypewriterEffectGenerated_TypewriterEffect_delayOnPeriod_m268916923 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TypewriterEffectGenerated::TypewriterEffect_delayOnNewLine(JSVCall)
extern "C"  void TypewriterEffectGenerated_TypewriterEffect_delayOnNewLine_m2458819900 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TypewriterEffectGenerated::TypewriterEffect_scrollView(JSVCall)
extern "C"  void TypewriterEffectGenerated_TypewriterEffect_scrollView_m1086337724 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TypewriterEffectGenerated::TypewriterEffect_keepFullDimensions(JSVCall)
extern "C"  void TypewriterEffectGenerated_TypewriterEffect_keepFullDimensions_m4082311181 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TypewriterEffectGenerated::TypewriterEffect_onFinished(JSVCall)
extern "C"  void TypewriterEffectGenerated_TypewriterEffect_onFinished_m1066283325 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TypewriterEffectGenerated::TypewriterEffect_isActive(JSVCall)
extern "C"  void TypewriterEffectGenerated_TypewriterEffect_isActive_m1512743102 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TypewriterEffectGenerated::TypewriterEffect_Finish(JSVCall,System.Int32)
extern "C"  bool TypewriterEffectGenerated_TypewriterEffect_Finish_m3869279536 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TypewriterEffectGenerated::TypewriterEffect_ResetToBeginning(JSVCall,System.Int32)
extern "C"  bool TypewriterEffectGenerated_TypewriterEffect_ResetToBeginning_m1460661616 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TypewriterEffectGenerated::__Register()
extern "C"  void TypewriterEffectGenerated___Register_m3625145197 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TypewriterEffectGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t TypewriterEffectGenerated_ilo_getObject1_m2377143900 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TypewriterEffectGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void TypewriterEffectGenerated_ilo_addJSCSRel2_m2600226775 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TypewriterEffectGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t TypewriterEffectGenerated_ilo_setObject3_m2522992214 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TypewriterEffectGenerated::ilo_setInt324(System.Int32,System.Int32)
extern "C"  void TypewriterEffectGenerated_ilo_setInt324_m4024806297 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TypewriterEffectGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void TypewriterEffectGenerated_ilo_setSingle5_m1659886238 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TypewriterEffectGenerated::ilo_getSingle6(System.Int32)
extern "C"  float TypewriterEffectGenerated_ilo_getSingle6_m4161830138 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TypewriterEffectGenerated::ilo_getBooleanS7(System.Int32)
extern "C"  bool TypewriterEffectGenerated_ilo_getBooleanS7_m3096824976 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
