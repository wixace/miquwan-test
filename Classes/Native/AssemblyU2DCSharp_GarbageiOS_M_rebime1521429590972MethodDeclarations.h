﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_rebime152
struct M_rebime152_t1429590972;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_rebime1521429590972.h"

// System.Void GarbageiOS.M_rebime152::.ctor()
extern "C"  void M_rebime152__ctor_m3642474791 (M_rebime152_t1429590972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rebime152::M_drijojaw0(System.String[],System.Int32)
extern "C"  void M_rebime152_M_drijojaw0_m578534746 (M_rebime152_t1429590972 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rebime152::M_cejaydere1(System.String[],System.Int32)
extern "C"  void M_rebime152_M_cejaydere1_m5875075 (M_rebime152_t1429590972 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rebime152::M_tarhatouSexeremai2(System.String[],System.Int32)
extern "C"  void M_rebime152_M_tarhatouSexeremai2_m1528636241 (M_rebime152_t1429590972 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rebime152::ilo_M_cejaydere11(GarbageiOS.M_rebime152,System.String[],System.Int32)
extern "C"  void M_rebime152_ilo_M_cejaydere11_m2606134781 (Il2CppObject * __this /* static, unused */, M_rebime152_t1429590972 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
