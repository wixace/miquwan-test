﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.UI.Button
struct Button_t3896396478;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_AppeggViewType181198339.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AppeggView
struct  AppeggView_t3951466281  : public MonoBehaviour_t667441552
{
public:
	// AppeggViewType AppeggView::_appeggViewType
	int32_t ____appeggViewType_2;
	// UnityEngine.UI.Text AppeggView::_primaryText
	Text_t9039225 * ____primaryText_3;
	// UnityEngine.UI.Text AppeggView::_secondaryText
	Text_t9039225 * ____secondaryText_4;
	// UnityEngine.UI.Button AppeggView::_primaryButton
	Button_t3896396478 * ____primaryButton_5;
	// UnityEngine.UI.Button AppeggView::_secondaryButton
	Button_t3896396478 * ____secondaryButton_6;

public:
	inline static int32_t get_offset_of__appeggViewType_2() { return static_cast<int32_t>(offsetof(AppeggView_t3951466281, ____appeggViewType_2)); }
	inline int32_t get__appeggViewType_2() const { return ____appeggViewType_2; }
	inline int32_t* get_address_of__appeggViewType_2() { return &____appeggViewType_2; }
	inline void set__appeggViewType_2(int32_t value)
	{
		____appeggViewType_2 = value;
	}

	inline static int32_t get_offset_of__primaryText_3() { return static_cast<int32_t>(offsetof(AppeggView_t3951466281, ____primaryText_3)); }
	inline Text_t9039225 * get__primaryText_3() const { return ____primaryText_3; }
	inline Text_t9039225 ** get_address_of__primaryText_3() { return &____primaryText_3; }
	inline void set__primaryText_3(Text_t9039225 * value)
	{
		____primaryText_3 = value;
		Il2CppCodeGenWriteBarrier(&____primaryText_3, value);
	}

	inline static int32_t get_offset_of__secondaryText_4() { return static_cast<int32_t>(offsetof(AppeggView_t3951466281, ____secondaryText_4)); }
	inline Text_t9039225 * get__secondaryText_4() const { return ____secondaryText_4; }
	inline Text_t9039225 ** get_address_of__secondaryText_4() { return &____secondaryText_4; }
	inline void set__secondaryText_4(Text_t9039225 * value)
	{
		____secondaryText_4 = value;
		Il2CppCodeGenWriteBarrier(&____secondaryText_4, value);
	}

	inline static int32_t get_offset_of__primaryButton_5() { return static_cast<int32_t>(offsetof(AppeggView_t3951466281, ____primaryButton_5)); }
	inline Button_t3896396478 * get__primaryButton_5() const { return ____primaryButton_5; }
	inline Button_t3896396478 ** get_address_of__primaryButton_5() { return &____primaryButton_5; }
	inline void set__primaryButton_5(Button_t3896396478 * value)
	{
		____primaryButton_5 = value;
		Il2CppCodeGenWriteBarrier(&____primaryButton_5, value);
	}

	inline static int32_t get_offset_of__secondaryButton_6() { return static_cast<int32_t>(offsetof(AppeggView_t3951466281, ____secondaryButton_6)); }
	inline Button_t3896396478 * get__secondaryButton_6() const { return ____secondaryButton_6; }
	inline Button_t3896396478 ** get_address_of__secondaryButton_6() { return &____secondaryButton_6; }
	inline void set__secondaryButton_6(Button_t3896396478 * value)
	{
		____secondaryButton_6 = value;
		Il2CppCodeGenWriteBarrier(&____secondaryButton_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
