﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pomelo.DotNetClient.HeartBeatService
struct HeartBeatService_t3983407957;
// Pomelo.DotNetClient.Protocol
struct Protocol_t400511732;
// System.Object
struct Il2CppObject;
// System.Timers.ElapsedEventArgs
struct ElapsedEventArgs_t2035959611;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_Protocol400511732.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_System_Timers_ElapsedEventArgs2035959611.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_PackageType3964380262.h"

// System.Void Pomelo.DotNetClient.HeartBeatService::.ctor(System.Int32,Pomelo.DotNetClient.Protocol)
extern "C"  void HeartBeatService__ctor_m2164286683 (HeartBeatService_t3983407957 * __this, int32_t ___interval0, Protocol_t400511732 * ___protocol1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.HeartBeatService::.cctor()
extern "C"  void HeartBeatService__cctor_m138154950 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.HeartBeatService::resetTimeout()
extern "C"  void HeartBeatService_resetTimeout_m247596047 (HeartBeatService_t3983407957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.HeartBeatService::sendHeartBeat(System.Object,System.Timers.ElapsedEventArgs)
extern "C"  void HeartBeatService_sendHeartBeat_m603660079 (HeartBeatService_t3983407957 * __this, Il2CppObject * ___source0, ElapsedEventArgs_t2035959611 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.HeartBeatService::start()
extern "C"  void HeartBeatService_start_m2705838567 (HeartBeatService_t3983407957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.HeartBeatService::stop()
extern "C"  void HeartBeatService_stop_m364792575 (HeartBeatService_t3983407957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.HeartBeatService::ilo_LogWarning1(System.Object,System.Boolean)
extern "C"  void HeartBeatService_ilo_LogWarning1_m2492701620 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.HeartBeatService::ilo_send2(Pomelo.DotNetClient.Protocol,Pomelo.DotNetClient.PackageType)
extern "C"  void HeartBeatService_ilo_send2_m1002233072 (Il2CppObject * __this /* static, unused */, Protocol_t400511732 * ____this0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
