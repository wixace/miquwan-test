﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginKuaiWanSJ
struct PluginKuaiWanSJ_t791724598;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// VersionMgr
struct VersionMgr_t1322950208;
// VersionInfo
struct VersionInfo_t2356638086;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// Mihua.SDK.CreatRoleInfo
struct CreatRoleInfo_t2858846543;
// Mihua.SDK.LvUpInfo
struct LvUpInfo_t411687177;
// System.Object
struct Il2CppObject;
// System.Action
struct Action_t3771233898;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginKuaiWanSJ791724598.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "AssemblyU2DCSharp_FloatTextMgr630384591.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginKuaiWanSJ::.ctor()
extern "C"  void PluginKuaiWanSJ__ctor_m3895402997 (PluginKuaiWanSJ_t791724598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWanSJ::Init()
extern "C"  void PluginKuaiWanSJ_Init_m1047026111 (PluginKuaiWanSJ_t791724598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginKuaiWanSJ::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginKuaiWanSJ_ReqSDKHttpLogin_m3698952742 (PluginKuaiWanSJ_t791724598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginKuaiWanSJ::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginKuaiWanSJ_IsLoginSuccess_m2000186326 (PluginKuaiWanSJ_t791724598 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWanSJ::OpenUserLogin()
extern "C"  void PluginKuaiWanSJ_OpenUserLogin_m984973127 (PluginKuaiWanSJ_t791724598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWanSJ::UserPay(CEvent.ZEvent)
extern "C"  void PluginKuaiWanSJ_UserPay_m2579801675 (PluginKuaiWanSJ_t791724598 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWanSJ::SignCallBack(System.Boolean,System.String)
extern "C"  void PluginKuaiWanSJ_SignCallBack_m3009988964 (PluginKuaiWanSJ_t791724598 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginKuaiWanSJ::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginKuaiWanSJ_BuildOrderParam2WWWForm_m562385806 (PluginKuaiWanSJ_t791724598 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWanSJ::CreateRole(CEvent.ZEvent)
extern "C"  void PluginKuaiWanSJ_CreateRole_m2862042330 (PluginKuaiWanSJ_t791724598 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWanSJ::EnterGame(CEvent.ZEvent)
extern "C"  void PluginKuaiWanSJ_EnterGame_m3209826590 (PluginKuaiWanSJ_t791724598 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWanSJ::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginKuaiWanSJ_RoleUpgrade_m1192297538 (PluginKuaiWanSJ_t791724598 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWanSJ::OnLoginSuccess(System.String)
extern "C"  void PluginKuaiWanSJ_OnLoginSuccess_m499516218 (PluginKuaiWanSJ_t791724598 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWanSJ::OnLogoutSuccess(System.String)
extern "C"  void PluginKuaiWanSJ_OnLogoutSuccess_m2603643541 (PluginKuaiWanSJ_t791724598 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWanSJ::OnLogout(System.String)
extern "C"  void PluginKuaiWanSJ_OnLogout_m108362314 (PluginKuaiWanSJ_t791724598 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWanSJ::initSdk(System.String,System.String,System.String)
extern "C"  void PluginKuaiWanSJ_initSdk_m187912925 (PluginKuaiWanSJ_t791724598 * __this, String_t* ___gameId0, String_t* ___gameName1, String_t* ___appKey2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWanSJ::login()
extern "C"  void PluginKuaiWanSJ_login_m3417417820 (PluginKuaiWanSJ_t791724598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWanSJ::logout()
extern "C"  void PluginKuaiWanSJ_logout_m2866560025 (PluginKuaiWanSJ_t791724598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWanSJ::creatRole(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginKuaiWanSJ_creatRole_m2133853276 (PluginKuaiWanSJ_t791724598 * __this, String_t* ___roleId0, String_t* ___serverId1, String_t* ___serverName2, String_t* ___roleName3, String_t* ___roleLv4, String_t* ___VipLevel5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWanSJ::roleUpgrade(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginKuaiWanSJ_roleUpgrade_m83924757 (PluginKuaiWanSJ_t791724598 * __this, String_t* ___roleId0, String_t* ___serverId1, String_t* ___serverName2, String_t* ___roleName3, String_t* ___roleLv4, String_t* ___VipLevel5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWanSJ::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginKuaiWanSJ_pay_m931560547 (PluginKuaiWanSJ_t791724598 * __this, String_t* ___orderId0, String_t* ___productName1, String_t* ___productId2, String_t* ___price3, String_t* ___roleId4, String_t* ___paycburl5, String_t* ___serverId6, String_t* ___serverName7, String_t* ___roleName8, String_t* ___extra9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWanSJ::<OnLogoutSuccess>m__439()
extern "C"  void PluginKuaiWanSJ_U3COnLogoutSuccessU3Em__439_m1698132288 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWanSJ::<OnLogout>m__43A()
extern "C"  void PluginKuaiWanSJ_U3COnLogoutU3Em__43A_m3876160253 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginKuaiWanSJ::ilo_get_Instance1()
extern "C"  VersionMgr_t1322950208 * PluginKuaiWanSJ_ilo_get_Instance1_m2249645072 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginKuaiWanSJ::ilo_get_currentVS2(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginKuaiWanSJ_ilo_get_currentVS2_m1046666842 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginKuaiWanSJ::ilo_get_isSdkLogin3(VersionMgr)
extern "C"  bool PluginKuaiWanSJ_ilo_get_isSdkLogin3_m1120666981 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginKuaiWanSJ::ilo_BuildOrderParam2WWWForm4(PluginKuaiWanSJ,Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginKuaiWanSJ_ilo_BuildOrderParam2WWWForm4_m3427967627 (Il2CppObject * __this /* static, unused */, PluginKuaiWanSJ_t791724598 * ____this0, PayInfo_t1775308120 * ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken PluginKuaiWanSJ::ilo_get_Item5(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  JToken_t3412245951 * PluginKuaiWanSJ_ilo_get_Item5_m23485520 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginKuaiWanSJ::ilo_ToString6(Newtonsoft.Json.Linq.JToken)
extern "C"  String_t* PluginKuaiWanSJ_ilo_ToString6_m3014171165 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTextMgr PluginKuaiWanSJ::ilo_get_FloatTextMgr7()
extern "C"  FloatTextMgr_t630384591 * PluginKuaiWanSJ_ilo_get_FloatTextMgr7_m131566029 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWanSJ::ilo_FloatText8(FloatTextMgr,FLOAT_TEXT_ID,System.Int32,System.Object[])
extern "C"  void PluginKuaiWanSJ_ilo_FloatText8_m1297279647 (Il2CppObject * __this /* static, unused */, FloatTextMgr_t630384591 * ____this0, int32_t ___textId1, int32_t ___offsetY2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginKuaiWanSJ::ilo_GetProductID9(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginKuaiWanSJ_ilo_GetProductID9_m642489164 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.CreatRoleInfo PluginKuaiWanSJ::ilo_Parse10(System.Object[])
extern "C"  CreatRoleInfo_t2858846543 * PluginKuaiWanSJ_ilo_Parse10_m306705392 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWanSJ::ilo_creatRole11(PluginKuaiWanSJ,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginKuaiWanSJ_ilo_creatRole11_m2772911973 (Il2CppObject * __this /* static, unused */, PluginKuaiWanSJ_t791724598 * ____this0, String_t* ___roleId1, String_t* ___serverId2, String_t* ___serverName3, String_t* ___roleName4, String_t* ___roleLv5, String_t* ___VipLevel6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.LvUpInfo PluginKuaiWanSJ::ilo_Parse12(System.Object[])
extern "C"  LvUpInfo_t411687177 * PluginKuaiWanSJ_ilo_Parse12_m4724542 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWanSJ::ilo_Log13(System.Object,System.Boolean)
extern "C"  void PluginKuaiWanSJ_ilo_Log13_m4203256271 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiWanSJ::ilo_Logout14(System.Action)
extern "C"  void PluginKuaiWanSJ_ilo_Logout14_m86521550 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginKuaiWanSJ::ilo_get_PluginsSdkMgr15()
extern "C"  PluginsSdkMgr_t3884624670 * PluginKuaiWanSJ_ilo_get_PluginsSdkMgr15_m2174780974 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
