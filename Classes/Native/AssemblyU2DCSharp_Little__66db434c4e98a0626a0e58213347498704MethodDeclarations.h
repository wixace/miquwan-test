﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._66db434c4e98a0626a0e5821dcf97c2b
struct _66db434c4e98a0626a0e5821dcf97c2b_t3347498704;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._66db434c4e98a0626a0e5821dcf97c2b::.ctor()
extern "C"  void _66db434c4e98a0626a0e5821dcf97c2b__ctor_m1411844445 (_66db434c4e98a0626a0e5821dcf97c2b_t3347498704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._66db434c4e98a0626a0e5821dcf97c2b::_66db434c4e98a0626a0e5821dcf97c2bm2(System.Int32)
extern "C"  int32_t _66db434c4e98a0626a0e5821dcf97c2b__66db434c4e98a0626a0e5821dcf97c2bm2_m4258996121 (_66db434c4e98a0626a0e5821dcf97c2b_t3347498704 * __this, int32_t ____66db434c4e98a0626a0e5821dcf97c2ba0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._66db434c4e98a0626a0e5821dcf97c2b::_66db434c4e98a0626a0e5821dcf97c2bm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _66db434c4e98a0626a0e5821dcf97c2b__66db434c4e98a0626a0e5821dcf97c2bm_m1853179709 (_66db434c4e98a0626a0e5821dcf97c2b_t3347498704 * __this, int32_t ____66db434c4e98a0626a0e5821dcf97c2ba0, int32_t ____66db434c4e98a0626a0e5821dcf97c2b361, int32_t ____66db434c4e98a0626a0e5821dcf97c2bc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
