﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_BehaviourGenerated
struct UnityEngine_BehaviourGenerated_t2062623930;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_BehaviourGenerated::.ctor()
extern "C"  void UnityEngine_BehaviourGenerated__ctor_m1953376545 (UnityEngine_BehaviourGenerated_t2062623930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BehaviourGenerated::Behaviour_Behaviour1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BehaviourGenerated_Behaviour_Behaviour1_m1236781117 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BehaviourGenerated::Behaviour_enabled(JSVCall)
extern "C"  void UnityEngine_BehaviourGenerated_Behaviour_enabled_m798046377 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BehaviourGenerated::Behaviour_isActiveAndEnabled(JSVCall)
extern "C"  void UnityEngine_BehaviourGenerated_Behaviour_isActiveAndEnabled_m1165541544 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BehaviourGenerated::__Register()
extern "C"  void UnityEngine_BehaviourGenerated___Register_m3035947078 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BehaviourGenerated::ilo_getBooleanS1(System.Int32)
extern "C"  bool UnityEngine_BehaviourGenerated_ilo_getBooleanS1_m4246802315 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
