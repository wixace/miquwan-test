﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSMgr/JS_CS_Rel
struct  JS_CS_Rel_t3554103776  : public Il2CppObject
{
public:
	// System.Int32 JSMgr/JS_CS_Rel::jsObjID
	int32_t ___jsObjID_0;
	// System.Object JSMgr/JS_CS_Rel::csObj
	Il2CppObject * ___csObj_1;
	// System.String JSMgr/JS_CS_Rel::name
	String_t* ___name_2;

public:
	inline static int32_t get_offset_of_jsObjID_0() { return static_cast<int32_t>(offsetof(JS_CS_Rel_t3554103776, ___jsObjID_0)); }
	inline int32_t get_jsObjID_0() const { return ___jsObjID_0; }
	inline int32_t* get_address_of_jsObjID_0() { return &___jsObjID_0; }
	inline void set_jsObjID_0(int32_t value)
	{
		___jsObjID_0 = value;
	}

	inline static int32_t get_offset_of_csObj_1() { return static_cast<int32_t>(offsetof(JS_CS_Rel_t3554103776, ___csObj_1)); }
	inline Il2CppObject * get_csObj_1() const { return ___csObj_1; }
	inline Il2CppObject ** get_address_of_csObj_1() { return &___csObj_1; }
	inline void set_csObj_1(Il2CppObject * value)
	{
		___csObj_1 = value;
		Il2CppCodeGenWriteBarrier(&___csObj_1, value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(JS_CS_Rel_t3554103776, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
