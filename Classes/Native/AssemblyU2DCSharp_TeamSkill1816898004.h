﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// superskillCfg
struct superskillCfg_t3024131278;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TeamSkill
struct  TeamSkill_t1816898004  : public Il2CppObject
{
public:
	// superskillCfg TeamSkill::mpJson
	superskillCfg_t3024131278 * ___mpJson_0;
	// System.Int32[] TeamSkill::skillIDArr
	Int32U5BU5D_t3230847821* ___skillIDArr_1;
	// System.Int32[] TeamSkill::upnumberArr
	Int32U5BU5D_t3230847821* ___upnumberArr_2;
	// System.Int32[] TeamSkill::endSkillIDArr
	Int32U5BU5D_t3230847821* ___endSkillIDArr_3;
	// System.Int32[] TeamSkill::captainEndSkillIDArr
	Int32U5BU5D_t3230847821* ___captainEndSkillIDArr_4;
	// System.Int32[] TeamSkill::UIEffectArr
	Int32U5BU5D_t3230847821* ___UIEffectArr_5;
	// System.Int32[] TeamSkill::SectionArr
	Int32U5BU5D_t3230847821* ___SectionArr_6;
	// System.Int32[] TeamSkill::ReducetimeArr
	Int32U5BU5D_t3230847821* ___ReducetimeArr_7;
	// System.Int32[] TeamSkill::GradeArr
	Int32U5BU5D_t3230847821* ___GradeArr_8;
	// System.Int32 TeamSkill::id
	int32_t ___id_9;

public:
	inline static int32_t get_offset_of_mpJson_0() { return static_cast<int32_t>(offsetof(TeamSkill_t1816898004, ___mpJson_0)); }
	inline superskillCfg_t3024131278 * get_mpJson_0() const { return ___mpJson_0; }
	inline superskillCfg_t3024131278 ** get_address_of_mpJson_0() { return &___mpJson_0; }
	inline void set_mpJson_0(superskillCfg_t3024131278 * value)
	{
		___mpJson_0 = value;
		Il2CppCodeGenWriteBarrier(&___mpJson_0, value);
	}

	inline static int32_t get_offset_of_skillIDArr_1() { return static_cast<int32_t>(offsetof(TeamSkill_t1816898004, ___skillIDArr_1)); }
	inline Int32U5BU5D_t3230847821* get_skillIDArr_1() const { return ___skillIDArr_1; }
	inline Int32U5BU5D_t3230847821** get_address_of_skillIDArr_1() { return &___skillIDArr_1; }
	inline void set_skillIDArr_1(Int32U5BU5D_t3230847821* value)
	{
		___skillIDArr_1 = value;
		Il2CppCodeGenWriteBarrier(&___skillIDArr_1, value);
	}

	inline static int32_t get_offset_of_upnumberArr_2() { return static_cast<int32_t>(offsetof(TeamSkill_t1816898004, ___upnumberArr_2)); }
	inline Int32U5BU5D_t3230847821* get_upnumberArr_2() const { return ___upnumberArr_2; }
	inline Int32U5BU5D_t3230847821** get_address_of_upnumberArr_2() { return &___upnumberArr_2; }
	inline void set_upnumberArr_2(Int32U5BU5D_t3230847821* value)
	{
		___upnumberArr_2 = value;
		Il2CppCodeGenWriteBarrier(&___upnumberArr_2, value);
	}

	inline static int32_t get_offset_of_endSkillIDArr_3() { return static_cast<int32_t>(offsetof(TeamSkill_t1816898004, ___endSkillIDArr_3)); }
	inline Int32U5BU5D_t3230847821* get_endSkillIDArr_3() const { return ___endSkillIDArr_3; }
	inline Int32U5BU5D_t3230847821** get_address_of_endSkillIDArr_3() { return &___endSkillIDArr_3; }
	inline void set_endSkillIDArr_3(Int32U5BU5D_t3230847821* value)
	{
		___endSkillIDArr_3 = value;
		Il2CppCodeGenWriteBarrier(&___endSkillIDArr_3, value);
	}

	inline static int32_t get_offset_of_captainEndSkillIDArr_4() { return static_cast<int32_t>(offsetof(TeamSkill_t1816898004, ___captainEndSkillIDArr_4)); }
	inline Int32U5BU5D_t3230847821* get_captainEndSkillIDArr_4() const { return ___captainEndSkillIDArr_4; }
	inline Int32U5BU5D_t3230847821** get_address_of_captainEndSkillIDArr_4() { return &___captainEndSkillIDArr_4; }
	inline void set_captainEndSkillIDArr_4(Int32U5BU5D_t3230847821* value)
	{
		___captainEndSkillIDArr_4 = value;
		Il2CppCodeGenWriteBarrier(&___captainEndSkillIDArr_4, value);
	}

	inline static int32_t get_offset_of_UIEffectArr_5() { return static_cast<int32_t>(offsetof(TeamSkill_t1816898004, ___UIEffectArr_5)); }
	inline Int32U5BU5D_t3230847821* get_UIEffectArr_5() const { return ___UIEffectArr_5; }
	inline Int32U5BU5D_t3230847821** get_address_of_UIEffectArr_5() { return &___UIEffectArr_5; }
	inline void set_UIEffectArr_5(Int32U5BU5D_t3230847821* value)
	{
		___UIEffectArr_5 = value;
		Il2CppCodeGenWriteBarrier(&___UIEffectArr_5, value);
	}

	inline static int32_t get_offset_of_SectionArr_6() { return static_cast<int32_t>(offsetof(TeamSkill_t1816898004, ___SectionArr_6)); }
	inline Int32U5BU5D_t3230847821* get_SectionArr_6() const { return ___SectionArr_6; }
	inline Int32U5BU5D_t3230847821** get_address_of_SectionArr_6() { return &___SectionArr_6; }
	inline void set_SectionArr_6(Int32U5BU5D_t3230847821* value)
	{
		___SectionArr_6 = value;
		Il2CppCodeGenWriteBarrier(&___SectionArr_6, value);
	}

	inline static int32_t get_offset_of_ReducetimeArr_7() { return static_cast<int32_t>(offsetof(TeamSkill_t1816898004, ___ReducetimeArr_7)); }
	inline Int32U5BU5D_t3230847821* get_ReducetimeArr_7() const { return ___ReducetimeArr_7; }
	inline Int32U5BU5D_t3230847821** get_address_of_ReducetimeArr_7() { return &___ReducetimeArr_7; }
	inline void set_ReducetimeArr_7(Int32U5BU5D_t3230847821* value)
	{
		___ReducetimeArr_7 = value;
		Il2CppCodeGenWriteBarrier(&___ReducetimeArr_7, value);
	}

	inline static int32_t get_offset_of_GradeArr_8() { return static_cast<int32_t>(offsetof(TeamSkill_t1816898004, ___GradeArr_8)); }
	inline Int32U5BU5D_t3230847821* get_GradeArr_8() const { return ___GradeArr_8; }
	inline Int32U5BU5D_t3230847821** get_address_of_GradeArr_8() { return &___GradeArr_8; }
	inline void set_GradeArr_8(Int32U5BU5D_t3230847821* value)
	{
		___GradeArr_8 = value;
		Il2CppCodeGenWriteBarrier(&___GradeArr_8, value);
	}

	inline static int32_t get_offset_of_id_9() { return static_cast<int32_t>(offsetof(TeamSkill_t1816898004, ___id_9)); }
	inline int32_t get_id_9() const { return ___id_9; }
	inline int32_t* get_address_of_id_9() { return &___id_9; }
	inline void set_id_9(int32_t value)
	{
		___id_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
