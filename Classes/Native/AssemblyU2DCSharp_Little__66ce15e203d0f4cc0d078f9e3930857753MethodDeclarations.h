﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._66ce15e203d0f4cc0d078f9eb3293853
struct _66ce15e203d0f4cc0d078f9eb3293853_t3930857753;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._66ce15e203d0f4cc0d078f9eb3293853::.ctor()
extern "C"  void _66ce15e203d0f4cc0d078f9eb3293853__ctor_m4189598004 (_66ce15e203d0f4cc0d078f9eb3293853_t3930857753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._66ce15e203d0f4cc0d078f9eb3293853::_66ce15e203d0f4cc0d078f9eb3293853m2(System.Int32)
extern "C"  int32_t _66ce15e203d0f4cc0d078f9eb3293853__66ce15e203d0f4cc0d078f9eb3293853m2_m1363227385 (_66ce15e203d0f4cc0d078f9eb3293853_t3930857753 * __this, int32_t ____66ce15e203d0f4cc0d078f9eb3293853a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._66ce15e203d0f4cc0d078f9eb3293853::_66ce15e203d0f4cc0d078f9eb3293853m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _66ce15e203d0f4cc0d078f9eb3293853__66ce15e203d0f4cc0d078f9eb3293853m_m1819918301 (_66ce15e203d0f4cc0d078f9eb3293853_t3930857753 * __this, int32_t ____66ce15e203d0f4cc0d078f9eb3293853a0, int32_t ____66ce15e203d0f4cc0d078f9eb3293853121, int32_t ____66ce15e203d0f4cc0d078f9eb3293853c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
