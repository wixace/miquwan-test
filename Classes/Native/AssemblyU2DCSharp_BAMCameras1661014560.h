﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t2727095145;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BAMCameras
struct  BAMCameras_t1661014560  : public Il2CppObject
{
public:
	// UnityEngine.Camera BAMCameras::camera
	Camera_t2727095145 * ___camera_0;
	// System.Boolean BAMCameras::initEnable
	bool ___initEnable_1;

public:
	inline static int32_t get_offset_of_camera_0() { return static_cast<int32_t>(offsetof(BAMCameras_t1661014560, ___camera_0)); }
	inline Camera_t2727095145 * get_camera_0() const { return ___camera_0; }
	inline Camera_t2727095145 ** get_address_of_camera_0() { return &___camera_0; }
	inline void set_camera_0(Camera_t2727095145 * value)
	{
		___camera_0 = value;
		Il2CppCodeGenWriteBarrier(&___camera_0, value);
	}

	inline static int32_t get_offset_of_initEnable_1() { return static_cast<int32_t>(offsetof(BAMCameras_t1661014560, ___initEnable_1)); }
	inline bool get_initEnable_1() const { return ___initEnable_1; }
	inline bool* get_address_of_initEnable_1() { return &___initEnable_1; }
	inline void set_initEnable_1(bool value)
	{
		___initEnable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
