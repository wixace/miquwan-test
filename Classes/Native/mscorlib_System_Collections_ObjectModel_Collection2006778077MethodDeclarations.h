﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>
struct Collection_1_t2006778077;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Pathfinding.LocalAvoidance/IntersectionPair[]
struct IntersectionPairU5BU5D_t3912429174;
// System.Collections.Generic.IEnumerator`1<Pathfinding.LocalAvoidance/IntersectionPair>
struct IEnumerator_1_t438217672;
// System.Collections.Generic.IList`1<Pathfinding.LocalAvoidance/IntersectionPair>
struct IList_1_t1220999826;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_Inter2821319919.h"

// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::.ctor()
extern "C"  void Collection_1__ctor_m1493918962_gshared (Collection_1_t2006778077 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1493918962(__this, method) ((  void (*) (Collection_1_t2006778077 *, const MethodInfo*))Collection_1__ctor_m1493918962_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3960935497_gshared (Collection_1_t2006778077 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3960935497(__this, method) ((  bool (*) (Collection_1_t2006778077 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3960935497_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m1707742354_gshared (Collection_1_t2006778077 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1707742354(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2006778077 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1707742354_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m2180250637_gshared (Collection_1_t2006778077 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m2180250637(__this, method) ((  Il2CppObject * (*) (Collection_1_t2006778077 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2180250637_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m360672772_gshared (Collection_1_t2006778077 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m360672772(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2006778077 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m360672772_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m3992346184_gshared (Collection_1_t2006778077 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m3992346184(__this, ___value0, method) ((  bool (*) (Collection_1_t2006778077 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m3992346184_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m4220584540_gshared (Collection_1_t2006778077 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m4220584540(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2006778077 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m4220584540_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m1381162119_gshared (Collection_1_t2006778077 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1381162119(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2006778077 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1381162119_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m4262591809_gshared (Collection_1_t2006778077 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m4262591809(__this, ___value0, method) ((  void (*) (Collection_1_t2006778077 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m4262591809_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m4040750100_gshared (Collection_1_t2006778077 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m4040750100(__this, method) ((  bool (*) (Collection_1_t2006778077 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m4040750100_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m1284675584_gshared (Collection_1_t2006778077 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1284675584(__this, method) ((  Il2CppObject * (*) (Collection_1_t2006778077 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1284675584_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1117021111_gshared (Collection_1_t2006778077 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1117021111(__this, method) ((  bool (*) (Collection_1_t2006778077 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1117021111_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m2291157090_gshared (Collection_1_t2006778077 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m2291157090(__this, method) ((  bool (*) (Collection_1_t2006778077 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m2291157090_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m2983233927_gshared (Collection_1_t2006778077 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m2983233927(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t2006778077 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2983233927_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m765854366_gshared (Collection_1_t2006778077 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m765854366(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2006778077 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m765854366_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::Add(T)
extern "C"  void Collection_1_Add_m2809279821_gshared (Collection_1_t2006778077 * __this, IntersectionPair_t2821319919  ___item0, const MethodInfo* method);
#define Collection_1_Add_m2809279821(__this, ___item0, method) ((  void (*) (Collection_1_t2006778077 *, IntersectionPair_t2821319919 , const MethodInfo*))Collection_1_Add_m2809279821_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::Clear()
extern "C"  void Collection_1_Clear_m3195019549_gshared (Collection_1_t2006778077 * __this, const MethodInfo* method);
#define Collection_1_Clear_m3195019549(__this, method) ((  void (*) (Collection_1_t2006778077 *, const MethodInfo*))Collection_1_Clear_m3195019549_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::ClearItems()
extern "C"  void Collection_1_ClearItems_m2690951685_gshared (Collection_1_t2006778077 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m2690951685(__this, method) ((  void (*) (Collection_1_t2006778077 *, const MethodInfo*))Collection_1_ClearItems_m2690951685_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::Contains(T)
extern "C"  bool Collection_1_Contains_m4267471307_gshared (Collection_1_t2006778077 * __this, IntersectionPair_t2821319919  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m4267471307(__this, ___item0, method) ((  bool (*) (Collection_1_t2006778077 *, IntersectionPair_t2821319919 , const MethodInfo*))Collection_1_Contains_m4267471307_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m1586775037_gshared (Collection_1_t2006778077 * __this, IntersectionPairU5BU5D_t3912429174* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m1586775037(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2006778077 *, IntersectionPairU5BU5D_t3912429174*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1586775037_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m391697582_gshared (Collection_1_t2006778077 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m391697582(__this, method) ((  Il2CppObject* (*) (Collection_1_t2006778077 *, const MethodInfo*))Collection_1_GetEnumerator_m391697582_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m3322975297_gshared (Collection_1_t2006778077 * __this, IntersectionPair_t2821319919  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m3322975297(__this, ___item0, method) ((  int32_t (*) (Collection_1_t2006778077 *, IntersectionPair_t2821319919 , const MethodInfo*))Collection_1_IndexOf_m3322975297_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m796734900_gshared (Collection_1_t2006778077 * __this, int32_t ___index0, IntersectionPair_t2821319919  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m796734900(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2006778077 *, int32_t, IntersectionPair_t2821319919 , const MethodInfo*))Collection_1_Insert_m796734900_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2138728935_gshared (Collection_1_t2006778077 * __this, int32_t ___index0, IntersectionPair_t2821319919  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2138728935(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2006778077 *, int32_t, IntersectionPair_t2821319919 , const MethodInfo*))Collection_1_InsertItem_m2138728935_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m1796940081_gshared (Collection_1_t2006778077 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m1796940081(__this, method) ((  Il2CppObject* (*) (Collection_1_t2006778077 *, const MethodInfo*))Collection_1_get_Items_m1796940081_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::Remove(T)
extern "C"  bool Collection_1_Remove_m2064197766_gshared (Collection_1_t2006778077 * __this, IntersectionPair_t2821319919  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m2064197766(__this, ___item0, method) ((  bool (*) (Collection_1_t2006778077 *, IntersectionPair_t2821319919 , const MethodInfo*))Collection_1_Remove_m2064197766_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m2965555066_gshared (Collection_1_t2006778077 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m2965555066(__this, ___index0, method) ((  void (*) (Collection_1_t2006778077 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m2965555066_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m939758874_gshared (Collection_1_t2006778077 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m939758874(__this, ___index0, method) ((  void (*) (Collection_1_t2006778077 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m939758874_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m4045293914_gshared (Collection_1_t2006778077 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m4045293914(__this, method) ((  int32_t (*) (Collection_1_t2006778077 *, const MethodInfo*))Collection_1_get_Count_m4045293914_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::get_Item(System.Int32)
extern "C"  IntersectionPair_t2821319919  Collection_1_get_Item_m4137690622_gshared (Collection_1_t2006778077 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m4137690622(__this, ___index0, method) ((  IntersectionPair_t2821319919  (*) (Collection_1_t2006778077 *, int32_t, const MethodInfo*))Collection_1_get_Item_m4137690622_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m2967656843_gshared (Collection_1_t2006778077 * __this, int32_t ___index0, IntersectionPair_t2821319919  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m2967656843(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2006778077 *, int32_t, IntersectionPair_t2821319919 , const MethodInfo*))Collection_1_set_Item_m2967656843_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m245567886_gshared (Collection_1_t2006778077 * __this, int32_t ___index0, IntersectionPair_t2821319919  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m245567886(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2006778077 *, int32_t, IntersectionPair_t2821319919 , const MethodInfo*))Collection_1_SetItem_m245567886_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m4076785921_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m4076785921(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m4076785921_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::ConvertItem(System.Object)
extern "C"  IntersectionPair_t2821319919  Collection_1_ConvertItem_m289056221_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m289056221(__this /* static, unused */, ___item0, method) ((  IntersectionPair_t2821319919  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m289056221_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m3505561661_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m3505561661(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m3505561661_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m1027496227_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1027496227(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m1027496227_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/IntersectionPair>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m1349793884_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1349793884(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m1349793884_gshared)(__this /* static, unused */, ___list0, method)
