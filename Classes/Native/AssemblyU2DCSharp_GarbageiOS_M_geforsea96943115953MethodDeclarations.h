﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_geforsea96
struct M_geforsea96_t943115953;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_geforsea96943115953.h"

// System.Void GarbageiOS.M_geforsea96::.ctor()
extern "C"  void M_geforsea96__ctor_m4025447586 (M_geforsea96_t943115953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_geforsea96::M_stokataGearwho0(System.String[],System.Int32)
extern "C"  void M_geforsea96_M_stokataGearwho0_m1275502967 (M_geforsea96_t943115953 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_geforsea96::M_sajicha1(System.String[],System.Int32)
extern "C"  void M_geforsea96_M_sajicha1_m2127792397 (M_geforsea96_t943115953 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_geforsea96::M_charrere2(System.String[],System.Int32)
extern "C"  void M_geforsea96_M_charrere2_m536010011 (M_geforsea96_t943115953 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_geforsea96::M_rehamai3(System.String[],System.Int32)
extern "C"  void M_geforsea96_M_rehamai3_m2860965973 (M_geforsea96_t943115953 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_geforsea96::ilo_M_charrere21(GarbageiOS.M_geforsea96,System.String[],System.Int32)
extern "C"  void M_geforsea96_ilo_M_charrere21_m1883499158 (Il2CppObject * __this /* static, unused */, M_geforsea96_t943115953 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
