﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Diagnostics.CodeAnalysis.SuppressMessageAttribute
struct SuppressMessageAttribute_t3921570300;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void System.Diagnostics.CodeAnalysis.SuppressMessageAttribute::.ctor(System.String,System.String)
extern "C"  void SuppressMessageAttribute__ctor_m2947654170 (SuppressMessageAttribute_t3921570300 * __this, String_t* ___category0, String_t* ___checkId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.CodeAnalysis.SuppressMessageAttribute::set_MessageId(System.String)
extern "C"  void SuppressMessageAttribute_set_MessageId_m1009771643 (SuppressMessageAttribute_t3921570300 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.CodeAnalysis.SuppressMessageAttribute::set_Scope(System.String)
extern "C"  void SuppressMessageAttribute_set_Scope_m3704961385 (SuppressMessageAttribute_t3921570300 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.CodeAnalysis.SuppressMessageAttribute::set_Target(System.String)
extern "C"  void SuppressMessageAttribute_set_Target_m629967572 (SuppressMessageAttribute_t3921570300 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
