﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BossAnimationMgr
struct BossAnimationMgr_t150948897;
// CombatEntity
struct CombatEntity_t684137495;
// System.String
struct String_t;
// BossAnimationInfo
struct BossAnimationInfo_t384335813;
// BossAnimationConfig
struct BossAnimationConfig_t4103877785;
// BossInofShow
struct BossInofShow_t241030;
// GameMgr
struct GameMgr_t1469029542;
// Hero
struct Hero_t2245658;
// HeroMgr
struct HeroMgr_t2475965342;
// GlobalGOMgr
struct GlobalGOMgr_t803081773;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Hashtable
struct Hashtable_t1407064410;
// AnimationPointJSC
struct AnimationPointJSC_t487595022;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// NpcMgr
struct NpcMgr_t2339534743;
// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_BossAnimationConfig4103877785.h"
#include "AssemblyU2DCSharp_BossAnimationMgr150948897.h"
#include "AssemblyU2DCSharp_BossInofShow241030.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_BossAnimationInfo384335813.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"
#include "AssemblyU2DCSharp_HeroMgr2475965342.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_Collections_Hashtable1407064410.h"
#include "AssemblyU2DCSharp_AnimationPointJSC487595022.h"
#include "AssemblyU2DCSharp_NpcMgr2339534743.h"

// System.Void BossAnimationMgr::.ctor()
extern "C"  void BossAnimationMgr__ctor_m3645042586 (BossAnimationMgr_t150948897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::set_IsPlaying(System.Boolean)
extern "C"  void BossAnimationMgr_set_IsPlaying_m1436892982 (BossAnimationMgr_t150948897 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BossAnimationMgr::get_IsPlaying()
extern "C"  bool BossAnimationMgr_get_IsPlaying_m3383079103 (BossAnimationMgr_t150948897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::Awake()
extern "C"  void BossAnimationMgr_Awake_m3882647805 (BossAnimationMgr_t150948897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::Init(System.Int32)
extern "C"  void BossAnimationMgr_Init_m1710494155 (BossAnimationMgr_t150948897 * __this, int32_t ___aniId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::OnDestroy()
extern "C"  void BossAnimationMgr_OnDestroy_m679438227 (BossAnimationMgr_t150948897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::Update()
extern "C"  void BossAnimationMgr_Update_m3054032563 (BossAnimationMgr_t150948897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::PlayEditor()
extern "C"  void BossAnimationMgr_PlayEditor_m4279100715 (BossAnimationMgr_t150948897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::Play()
extern "C"  void BossAnimationMgr_Play_m2622746782 (BossAnimationMgr_t150948897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::End()
extern "C"  void BossAnimationMgr_End_m1598526611 (BossAnimationMgr_t150948897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::UpdateCameraAnimation()
extern "C"  void BossAnimationMgr_UpdateCameraAnimation_m580046606 (BossAnimationMgr_t150948897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::UpdateBossAnimation()
extern "C"  void BossAnimationMgr_UpdateBossAnimation_m3488218438 (BossAnimationMgr_t150948897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::Boss3DShowEnd()
extern "C"  void BossAnimationMgr_Boss3DShowEnd_m2973044184 (BossAnimationMgr_t150948897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::GetBossEntity()
extern "C"  void BossAnimationMgr_GetBossEntity_m4046100958 (BossAnimationMgr_t150948897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::SetGameState(CombatEntity,System.Boolean)
extern "C"  void BossAnimationMgr_SetGameState_m2377206797 (BossAnimationMgr_t150948897 * __this, CombatEntity_t684137495 * ___entity0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::SetCameraState(System.Boolean)
extern "C"  void BossAnimationMgr_SetCameraState_m2468068811 (BossAnimationMgr_t150948897 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::<UpdateBossAnimation>m__3BC(System.String,System.Single)
extern "C"  void BossAnimationMgr_U3CUpdateBossAnimationU3Em__3BC_m2249617436 (BossAnimationMgr_t150948897 * __this, String_t* ___aniType0, float ___aniTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BossAnimationInfo BossAnimationMgr::ilo_GetBossCfg1(BossAnimationConfig,System.Int32)
extern "C"  BossAnimationInfo_t384335813 * BossAnimationMgr_ilo_GetBossCfg1_m2860130437 (Il2CppObject * __this /* static, unused */, BossAnimationConfig_t4103877785 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::ilo_set_IsPlaying2(BossAnimationMgr,System.Boolean)
extern "C"  void BossAnimationMgr_ilo_set_IsPlaying2_m247843190 (Il2CppObject * __this /* static, unused */, BossAnimationMgr_t150948897 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BossAnimationConfig BossAnimationMgr::ilo_get_Instance3()
extern "C"  BossAnimationConfig_t4103877785 * BossAnimationMgr_ilo_get_Instance3_m3010587680 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::ilo_Hide4(BossInofShow)
extern "C"  void BossAnimationMgr_ilo_Hide4_m2570082449 (Il2CppObject * __this /* static, unused */, BossInofShow_t241030 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 BossAnimationMgr::ilo_get_TrigerPos5(BossAnimationInfo)
extern "C"  Vector3_t4282066566  BossAnimationMgr_ilo_get_TrigerPos5_m229840099 (Il2CppObject * __this /* static, unused */, BossAnimationInfo_t384335813 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::ilo_OnDestroy6(CombatEntity)
extern "C"  void BossAnimationMgr_ilo_OnDestroy6_m1841181921 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BossAnimationMgr::ilo_get_level7(GameMgr)
extern "C"  int32_t BossAnimationMgr_ilo_get_level7_m3421168677 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero BossAnimationMgr::ilo_get_captain8(HeroMgr)
extern "C"  Hero_t2245658 * BossAnimationMgr_ilo_get_captain8_m3022186039 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::ilo_Play9(BossAnimationMgr)
extern "C"  void BossAnimationMgr_ilo_Play9_m2907310057 (Il2CppObject * __this /* static, unused */, BossAnimationMgr_t150948897 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GlobalGOMgr BossAnimationMgr::ilo_get_GlobalGOMgr10()
extern "C"  GlobalGOMgr_t803081773 * BossAnimationMgr_ilo_get_GlobalGOMgr10_m654922914 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BossAnimationMgr::ilo_get_isBoss11(BossAnimationInfo)
extern "C"  int32_t BossAnimationMgr_ilo_get_isBoss11_m2586692034 (Il2CppObject * __this /* static, unused */, BossAnimationInfo_t384335813 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::ilo_UpdateCameraAnimation12(BossAnimationMgr)
extern "C"  void BossAnimationMgr_ilo_UpdateCameraAnimation12_m3126788443 (Il2CppObject * __this /* static, unused */, BossAnimationMgr_t150948897 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::ilo_UpdateBossAnimation13(BossAnimationMgr)
extern "C"  void BossAnimationMgr_ilo_UpdateBossAnimation13_m840006292 (Il2CppObject * __this /* static, unused */, BossAnimationMgr_t150948897 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::ilo_SetLayer14(UnityEngine.GameObject,System.Int32)
extern "C"  void BossAnimationMgr_ilo_SetLayer14_m2414573872 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, int32_t ___layer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::ilo_SetGameState15(BossAnimationMgr,CombatEntity,System.Boolean)
extern "C"  void BossAnimationMgr_ilo_SetGameState15_m3234766667 (Il2CppObject * __this /* static, unused */, BossAnimationMgr_t150948897 * ____this0, CombatEntity_t684137495 * ___entity1, bool ___state2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::ilo_Boss3DShowEnd16(BossAnimationMgr)
extern "C"  void BossAnimationMgr_ilo_Boss3DShowEnd16_m739861929 (Il2CppObject * __this /* static, unused */, BossAnimationMgr_t150948897 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::ilo_ShakePosition17(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void BossAnimationMgr_ilo_ShakePosition17_m424661868 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Hashtable_t1407064410 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 BossAnimationMgr::ilo_get_Pos18(AnimationPointJSC)
extern "C"  Vector3_t4282066566  BossAnimationMgr_ilo_get_Pos18_m2970487879 (Il2CppObject * __this /* static, unused */, AnimationPointJSC_t487595022 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable BossAnimationMgr::ilo_Hash19(System.Object[])
extern "C"  Hashtable_t1407064410 * BossAnimationMgr_ilo_Hash19_m2348067562 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::ilo_MoveTo20(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void BossAnimationMgr_ilo_MoveTo20_m1799177631 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Hashtable_t1407064410 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BossAnimationMgr::ilo_get_delay21(AnimationPointJSC)
extern "C"  float BossAnimationMgr_ilo_get_delay21_m487013330 (Il2CppObject * __this /* static, unused */, AnimationPointJSC_t487595022 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::ilo_RotateTo22(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void BossAnimationMgr_ilo_RotateTo22_m831905683 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Hashtable_t1407064410 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::ilo_AddDelayId23(CombatEntity,System.UInt32)
extern "C"  void BossAnimationMgr_ilo_AddDelayId23_m4139810424 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, uint32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::ilo_set_isSkillFrozen24(CombatEntity,System.Boolean)
extern "C"  void BossAnimationMgr_ilo_set_isSkillFrozen24_m3392792643 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BossAnimationMgr::ilo_get_UINamePos25(CombatEntity)
extern "C"  int32_t BossAnimationMgr_ilo_get_UINamePos25_m3946075311 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationMgr::ilo_PlayForward26(BossInofShow)
extern "C"  void BossAnimationMgr_ilo_PlayForward26_m1630749460 (Il2CppObject * __this /* static, unused */, BossInofShow_t241030 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NpcMgr BossAnimationMgr::ilo_get_instance27()
extern "C"  NpcMgr_t2339534743 * BossAnimationMgr_ilo_get_instance27_m386825446 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> BossAnimationMgr::ilo_GetCurRoundMonster28(NpcMgr)
extern "C"  List_1_t2052323047 * BossAnimationMgr_ilo_GetCurRoundMonster28_m3354603692 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
