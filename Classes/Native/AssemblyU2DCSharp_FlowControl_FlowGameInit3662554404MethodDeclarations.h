﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FlowControl.FlowGameInit
struct FlowGameInit_t3662554404;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// SceneMgr
struct SceneMgr_t3584105996;
// ZiShiYingMgr
struct ZiShiYingMgr_t4004457962;
// UIModelDisplayMgr
struct UIModelDisplayMgr_t1446490315;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// LoadingBoardMgr
struct LoadingBoardMgr_t303632014;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_SceneMgr3584105996.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_LoadingBoardMgr303632014.h"
#include "mscorlib_System_String7231557.h"

// System.Void FlowControl.FlowGameInit::.ctor()
extern "C"  void FlowGameInit__ctor_m245942412 (FlowGameInit_t3662554404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowGameInit::Activate()
extern "C"  void FlowGameInit_Activate_m3651802091 (FlowGameInit_t3662554404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowGameInit::OnComplete()
extern "C"  void FlowGameInit_OnComplete_m94045232 (FlowGameInit_t3662554404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowGameInit::OnLoadUpdate(CEvent.ZEvent)
extern "C"  void FlowGameInit_OnLoadUpdate_m2013118709 (FlowGameInit_t3662554404 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowGameInit::OnSceneLoaded(CEvent.ZEvent)
extern "C"  void FlowGameInit_OnSceneLoaded_m3230003583 (FlowGameInit_t3662554404 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowGameInit::ilo_Init1(SceneMgr)
extern "C"  void FlowGameInit_ilo_Init1_m3353268460 (Il2CppObject * __this /* static, unused */, SceneMgr_t3584105996 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZiShiYingMgr FlowControl.FlowGameInit::ilo_get_ZiShiYingMgr2()
extern "C"  ZiShiYingMgr_t4004457962 * FlowGameInit_ilo_get_ZiShiYingMgr2_m2737525795 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIModelDisplayMgr FlowControl.FlowGameInit::ilo_get_Instance3()
extern "C"  UIModelDisplayMgr_t1446490315 * FlowGameInit_ilo_get_Instance3_m618771972 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr FlowControl.FlowGameInit::ilo_get_PluginsSdkMgr4()
extern "C"  PluginsSdkMgr_t3884624670 * FlowGameInit_ilo_get_PluginsSdkMgr4_m874229087 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowGameInit::ilo_Init5(PluginsSdkMgr)
extern "C"  void FlowGameInit_ilo_Init5_m1897462872 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SceneMgr FlowControl.FlowGameInit::ilo_get_SceneMgr6()
extern "C"  SceneMgr_t3584105996 * FlowGameInit_ilo_get_SceneMgr6_m2433650407 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LoadingBoardMgr FlowControl.FlowGameInit::ilo_get_Instance7()
extern "C"  LoadingBoardMgr_t303632014 * FlowGameInit_ilo_get_Instance7_m1925067467 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowGameInit::ilo_SetPercent8(LoadingBoardMgr,System.Single,System.String)
extern "C"  void FlowGameInit_ilo_SetPercent8_m471040665 (Il2CppObject * __this /* static, unused */, LoadingBoardMgr_t303632014 * ____this0, float ___value1, String_t* ___msg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowGameInit::ilo_RemoveEventListener9(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void FlowGameInit_ilo_RemoveEventListener9_m1062729737 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowGameInit::ilo_Close10(LoadingBoardMgr)
extern "C"  void FlowGameInit_ilo_Close10_m3063418486 (Il2CppObject * __this /* static, unused */, LoadingBoardMgr_t303632014 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowGameInit::ilo_DispatchEvent11(CEvent.ZEvent)
extern "C"  void FlowGameInit_ilo_DispatchEvent11_m4148282724 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
