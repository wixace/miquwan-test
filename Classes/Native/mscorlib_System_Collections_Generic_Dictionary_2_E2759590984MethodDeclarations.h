﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>
struct Dictionary_2_t1442267592;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2759590984.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21341048298.h"
#include "AssemblyU2DCSharp_MScrollView_MoveWay3474941550.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m753268849_gshared (Enumerator_t2759590984 * __this, Dictionary_2_t1442267592 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m753268849(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2759590984 *, Dictionary_2_t1442267592 *, const MethodInfo*))Enumerator__ctor_m753268849_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m67213840_gshared (Enumerator_t2759590984 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m67213840(__this, method) ((  Il2CppObject * (*) (Enumerator_t2759590984 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m67213840_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2131366820_gshared (Enumerator_t2759590984 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2131366820(__this, method) ((  void (*) (Enumerator_t2759590984 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2131366820_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2985981037_gshared (Enumerator_t2759590984 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2985981037(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2759590984 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2985981037_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3671657196_gshared (Enumerator_t2759590984 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3671657196(__this, method) ((  Il2CppObject * (*) (Enumerator_t2759590984 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3671657196_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3342851710_gshared (Enumerator_t2759590984 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3342851710(__this, method) ((  Il2CppObject * (*) (Enumerator_t2759590984 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3342851710_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m56045584_gshared (Enumerator_t2759590984 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m56045584(__this, method) ((  bool (*) (Enumerator_t2759590984 *, const MethodInfo*))Enumerator_MoveNext_m56045584_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t1341048298  Enumerator_get_Current_m2618191008_gshared (Enumerator_t2759590984 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2618191008(__this, method) ((  KeyValuePair_2_t1341048298  (*) (Enumerator_t2759590984 *, const MethodInfo*))Enumerator_get_Current_m2618191008_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m3308353117_gshared (Enumerator_t2759590984 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m3308353117(__this, method) ((  int32_t (*) (Enumerator_t2759590984 *, const MethodInfo*))Enumerator_get_CurrentKey_m3308353117_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m1303323201_gshared (Enumerator_t2759590984 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1303323201(__this, method) ((  Il2CppObject * (*) (Enumerator_t2759590984 *, const MethodInfo*))Enumerator_get_CurrentValue_m1303323201_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m1707899203_gshared (Enumerator_t2759590984 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1707899203(__this, method) ((  void (*) (Enumerator_t2759590984 *, const MethodInfo*))Enumerator_Reset_m1707899203_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3106048780_gshared (Enumerator_t2759590984 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3106048780(__this, method) ((  void (*) (Enumerator_t2759590984 *, const MethodInfo*))Enumerator_VerifyState_m3106048780_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m2655478644_gshared (Enumerator_t2759590984 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m2655478644(__this, method) ((  void (*) (Enumerator_t2759590984 *, const MethodInfo*))Enumerator_VerifyCurrent_m2655478644_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3137056083_gshared (Enumerator_t2759590984 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3137056083(__this, method) ((  void (*) (Enumerator_t2759590984 *, const MethodInfo*))Enumerator_Dispose_m3137056083_gshared)(__this, method)
