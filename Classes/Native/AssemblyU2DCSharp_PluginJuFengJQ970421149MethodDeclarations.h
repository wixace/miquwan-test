﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginJuFengJQ
struct PluginJuFengJQ_t970421149;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// VersionInfo
struct VersionInfo_t2356638086;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// Mihua.SDK.LvUpInfo
struct LvUpInfo_t411687177;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_PluginJuFengJQ970421149.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginJuFengJQ::.ctor()
extern "C"  void PluginJuFengJQ__ctor_m486152606 (PluginJuFengJQ_t970421149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuFengJQ::Init()
extern "C"  void PluginJuFengJQ_Init_m3153807606 (PluginJuFengJQ_t970421149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginJuFengJQ::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginJuFengJQ_ReqSDKHttpLogin_m145364979 (PluginJuFengJQ_t970421149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginJuFengJQ::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginJuFengJQ_IsLoginSuccess_m1062016757 (PluginJuFengJQ_t970421149 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuFengJQ::OpenUserLogin()
extern "C"  void PluginJuFengJQ_OpenUserLogin_m3989224944 (PluginJuFengJQ_t970421149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuFengJQ::UserPay(CEvent.ZEvent)
extern "C"  void PluginJuFengJQ_UserPay_m2652391298 (PluginJuFengJQ_t970421149 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuFengJQ::CreateRole(CEvent.ZEvent)
extern "C"  void PluginJuFengJQ_CreateRole_m715983939 (PluginJuFengJQ_t970421149 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuFengJQ::EnterGame(CEvent.ZEvent)
extern "C"  void PluginJuFengJQ_EnterGame_m4248977557 (PluginJuFengJQ_t970421149 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuFengJQ::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginJuFengJQ_RoleUpgrade_m3383964153 (PluginJuFengJQ_t970421149 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuFengJQ::OnLoginSuccess(System.String)
extern "C"  void PluginJuFengJQ_OnLoginSuccess_m346809891 (PluginJuFengJQ_t970421149 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuFengJQ::OnLogoutSuccess(System.String)
extern "C"  void PluginJuFengJQ_OnLogoutSuccess_m2164714700 (PluginJuFengJQ_t970421149 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuFengJQ::OnLogout(System.String)
extern "C"  void PluginJuFengJQ_OnLogout_m2358640627 (PluginJuFengJQ_t970421149 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuFengJQ::initSdk()
extern "C"  void PluginJuFengJQ_initSdk_m710395558 (PluginJuFengJQ_t970421149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuFengJQ::login()
extern "C"  void PluginJuFengJQ_login_m8167429 (PluginJuFengJQ_t970421149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuFengJQ::logout()
extern "C"  void PluginJuFengJQ_logout_m259013008 (PluginJuFengJQ_t970421149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuFengJQ::updateRoleInfo(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginJuFengJQ_updateRoleInfo_m3876265031 (PluginJuFengJQ_t970421149 * __this, String_t* ___type0, String_t* ___serverId1, String_t* ___serverName2, String_t* ___roleId3, String_t* ___roleName4, String_t* ___unionName5, String_t* ___roleLv6, String_t* ___VipLevel7, String_t* ___curGlodNum8, String_t* ___CreatRoleTime9, String_t* ___LvUpTime10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuFengJQ::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginJuFengJQ_pay_m3308525190 (PluginJuFengJQ_t970421149 * __this, String_t* ___orderId0, String_t* ___amount1, String_t* ___productId2, String_t* ___productName3, String_t* ___serverId4, String_t* ___serverName5, String_t* ___roleId6, String_t* ___roleName7, String_t* ___unionName8, String_t* ___roleLv9, String_t* ___vipLv10, String_t* ___curGlod11, String_t* ___createTime12, String_t* ___LvUpTime13, String_t* ___extra14, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuFengJQ::<OnLogoutSuccess>m__434()
extern "C"  void PluginJuFengJQ_U3COnLogoutSuccessU3Em__434_m3847036388 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuFengJQ::<OnLogout>m__435()
extern "C"  void PluginJuFengJQ_U3COnLogoutU3Em__435_m718547624 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuFengJQ::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginJuFengJQ_ilo_AddEventListener1_m1616583574 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginJuFengJQ::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginJuFengJQ_ilo_get_Instance2_m747075808 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginJuFengJQ::ilo_get_currentVS3(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginJuFengJQ_ilo_get_currentVS3_m4134049164 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginJuFengJQ::ilo_get_isSdkLogin4(VersionMgr)
extern "C"  bool PluginJuFengJQ_ilo_get_isSdkLogin4_m1808778407 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginJuFengJQ::ilo_get_DeviceID5(PluginsSdkMgr)
extern "C"  String_t* PluginJuFengJQ_ilo_get_DeviceID5_m4128646483 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuFengJQ::ilo_login6(PluginJuFengJQ)
extern "C"  void PluginJuFengJQ_ilo_login6_m2315417385 (Il2CppObject * __this /* static, unused */, PluginJuFengJQ_t970421149 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTextMgr PluginJuFengJQ::ilo_get_FloatTextMgr7()
extern "C"  FloatTextMgr_t630384591 * PluginJuFengJQ_ilo_get_FloatTextMgr7_m1628238106 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.PayInfo PluginJuFengJQ::ilo_Parse8(System.Object[])
extern "C"  PayInfo_t1775308120 * PluginJuFengJQ_ilo_Parse8_m4014969613 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___payOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginJuFengJQ::ilo_GetProductID9(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginJuFengJQ_ilo_GetProductID9_m2344313167 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.LvUpInfo PluginJuFengJQ::ilo_Parse10(System.Object[])
extern "C"  LvUpInfo_t411687177 * PluginJuFengJQ_ilo_Parse10_m2527721271 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuFengJQ::ilo_Logout11(System.Action)
extern "C"  void PluginJuFengJQ_ilo_Logout11_m840312410 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuFengJQ::ilo_logout12(PluginJuFengJQ)
extern "C"  void PluginJuFengJQ_ilo_logout12_m1449128039 (Il2CppObject * __this /* static, unused */, PluginJuFengJQ_t970421149 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginJuFengJQ::ilo_get_PluginsSdkMgr13()
extern "C"  PluginsSdkMgr_t3884624670 * PluginJuFengJQ_ilo_get_PluginsSdkMgr13_m2871407883 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
