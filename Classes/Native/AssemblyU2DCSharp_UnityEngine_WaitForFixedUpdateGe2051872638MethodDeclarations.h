﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_WaitForFixedUpdateGenerated
struct UnityEngine_WaitForFixedUpdateGenerated_t2051872638;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_WaitForFixedUpdateGenerated::.ctor()
extern "C"  void UnityEngine_WaitForFixedUpdateGenerated__ctor_m3409315245 (UnityEngine_WaitForFixedUpdateGenerated_t2051872638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WaitForFixedUpdateGenerated::WaitForFixedUpdate_WaitForFixedUpdate1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WaitForFixedUpdateGenerated_WaitForFixedUpdate_WaitForFixedUpdate1_m4089907653 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WaitForFixedUpdateGenerated::__Register()
extern "C"  void UnityEngine_WaitForFixedUpdateGenerated___Register_m3684498234 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WaitForFixedUpdateGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_WaitForFixedUpdateGenerated_ilo_addJSCSRel1_m3310422505 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
