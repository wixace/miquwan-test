﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Serialization.JsonFx.JsonMemberAttribute
struct JsonMemberAttribute_t1310195012;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.Serialization.JsonFx.JsonMemberAttribute::.ctor()
extern "C"  void JsonMemberAttribute__ctor_m1290858339 (JsonMemberAttribute_t1310195012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
