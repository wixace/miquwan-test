﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1637356994MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1857893303(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1768739476 *, Dictionary_2_t141980025 *, const MethodInfo*))KeyCollection__ctor_m4044290661_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.String>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3592843071(__this, ___item0, method) ((  void (*) (KeyCollection_t1768739476 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m537048017_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.String>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4218247286(__this, method) ((  void (*) (KeyCollection_t1768739476 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1582668168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.String>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2386778539(__this, ___item0, method) ((  bool (*) (KeyCollection_t1768739476 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m278208601_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.String>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3328195024(__this, ___item0, method) ((  bool (*) (KeyCollection_t1768739476 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4264547326_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.String>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m419740146(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1768739476 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3773548804_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m4210047976(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1768739476 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1270182394_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3021287907(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1768739476 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1983916149_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.String>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3062698572(__this, method) ((  bool (*) (KeyCollection_t1768739476 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3951551866_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.String>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3804711422(__this, method) ((  bool (*) (KeyCollection_t1768739476 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2888408748_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.String>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2906638058(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1768739476 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1903343128_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.String>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m2993632876(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1768739476 *, AniTypeU5BU5D_t3368333050*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m4220990746_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.String>::GetEnumerator()
#define KeyCollection_GetEnumerator_m440696975(__this, method) ((  Enumerator_t756916079  (*) (KeyCollection_t1768739476 *, const MethodInfo*))KeyCollection_GetEnumerator_m165844029_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.String>::get_Count()
#define KeyCollection_get_Count_m592072260(__this, method) ((  int32_t (*) (KeyCollection_t1768739476 *, const MethodInfo*))KeyCollection_get_Count_m2255579378_gshared)(__this, method)
