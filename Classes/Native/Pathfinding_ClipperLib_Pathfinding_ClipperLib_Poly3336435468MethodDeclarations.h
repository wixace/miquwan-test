﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.ClipperLib.PolyTree
struct PolyTree_t3336435468;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.ClipperLib.PolyTree::.ctor()
extern "C"  void PolyTree__ctor_m3647720353 (PolyTree_t3336435468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.PolyTree::Finalize()
extern "C"  void PolyTree_Finalize_m2371986721 (PolyTree_t3336435468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.PolyTree::Clear()
extern "C"  void PolyTree_Clear_m1053853644 (PolyTree_t3336435468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
