﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "System_Xml_System_Xml_XmlNode856910923.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNotation
struct  XmlNotation_t1447424459  : public XmlNode_t856910923
{
public:
	// System.String System.Xml.XmlNotation::localName
	String_t* ___localName_7;
	// System.String System.Xml.XmlNotation::publicId
	String_t* ___publicId_8;
	// System.String System.Xml.XmlNotation::systemId
	String_t* ___systemId_9;
	// System.String System.Xml.XmlNotation::prefix
	String_t* ___prefix_10;

public:
	inline static int32_t get_offset_of_localName_7() { return static_cast<int32_t>(offsetof(XmlNotation_t1447424459, ___localName_7)); }
	inline String_t* get_localName_7() const { return ___localName_7; }
	inline String_t** get_address_of_localName_7() { return &___localName_7; }
	inline void set_localName_7(String_t* value)
	{
		___localName_7 = value;
		Il2CppCodeGenWriteBarrier(&___localName_7, value);
	}

	inline static int32_t get_offset_of_publicId_8() { return static_cast<int32_t>(offsetof(XmlNotation_t1447424459, ___publicId_8)); }
	inline String_t* get_publicId_8() const { return ___publicId_8; }
	inline String_t** get_address_of_publicId_8() { return &___publicId_8; }
	inline void set_publicId_8(String_t* value)
	{
		___publicId_8 = value;
		Il2CppCodeGenWriteBarrier(&___publicId_8, value);
	}

	inline static int32_t get_offset_of_systemId_9() { return static_cast<int32_t>(offsetof(XmlNotation_t1447424459, ___systemId_9)); }
	inline String_t* get_systemId_9() const { return ___systemId_9; }
	inline String_t** get_address_of_systemId_9() { return &___systemId_9; }
	inline void set_systemId_9(String_t* value)
	{
		___systemId_9 = value;
		Il2CppCodeGenWriteBarrier(&___systemId_9, value);
	}

	inline static int32_t get_offset_of_prefix_10() { return static_cast<int32_t>(offsetof(XmlNotation_t1447424459, ___prefix_10)); }
	inline String_t* get_prefix_10() const { return ___prefix_10; }
	inline String_t** get_address_of_prefix_10() { return &___prefix_10; }
	inline void set_prefix_10(String_t* value)
	{
		___prefix_10 = value;
		Il2CppCodeGenWriteBarrier(&___prefix_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
