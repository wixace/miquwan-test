﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,HatredCtrl/stValue>
struct Transform_1_t2514112805;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_HatredCtrl_stValue3425945410.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,HatredCtrl/stValue>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m88096103_gshared (Transform_1_t2514112805 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m88096103(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t2514112805 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m88096103_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,HatredCtrl/stValue>::Invoke(TKey,TValue)
extern "C"  stValue_t3425945410  Transform_1_Invoke_m1178427957_gshared (Transform_1_t2514112805 * __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m1178427957(__this, ___key0, ___value1, method) ((  stValue_t3425945410  (*) (Transform_1_t2514112805 *, Il2CppObject *, stValue_t3425945410 , const MethodInfo*))Transform_1_Invoke_m1178427957_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,HatredCtrl/stValue>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3191743764_gshared (Transform_1_t2514112805 * __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m3191743764(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t2514112805 *, Il2CppObject *, stValue_t3425945410 , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m3191743764_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,HatredCtrl/stValue>::EndInvoke(System.IAsyncResult)
extern "C"  stValue_t3425945410  Transform_1_EndInvoke_m90389301_gshared (Transform_1_t2514112805 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m90389301(__this, ___result0, method) ((  stValue_t3425945410  (*) (Transform_1_t2514112805 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m90389301_gshared)(__this, ___result0, method)
