﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen2974409999MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<Pomelo.DotNetClient.MsgEvent>::.ctor()
#define Stack_1__ctor_m2336848548(__this, method) ((  void (*) (Stack_1_t3180884369 *, const MethodInfo*))Stack_1__ctor_m2725689112_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<Pomelo.DotNetClient.MsgEvent>::.ctor(System.Int32)
#define Stack_1__ctor_m962615797(__this, ___count0, method) ((  void (*) (Stack_1_t3180884369 *, int32_t, const MethodInfo*))Stack_1__ctor_m3186788457_gshared)(__this, ___count0, method)
// System.Boolean System.Collections.Generic.Stack`1<Pomelo.DotNetClient.MsgEvent>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m2209811378(__this, method) ((  bool (*) (Stack_1_t3180884369 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m1582336274_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<Pomelo.DotNetClient.MsgEvent>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m3376109234(__this, method) ((  Il2CppObject * (*) (Stack_1_t3180884369 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m2938343088_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<Pomelo.DotNetClient.MsgEvent>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m2097918752(__this, ___dest0, ___idx1, method) ((  void (*) (Stack_1_t3180884369 *, Il2CppArray *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m3277353260_gshared)(__this, ___dest0, ___idx1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<Pomelo.DotNetClient.MsgEvent>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1841443574(__this, method) ((  Il2CppObject* (*) (Stack_1_t3180884369 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m625377314_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<Pomelo.DotNetClient.MsgEvent>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m4266426799(__this, method) ((  Il2CppObject * (*) (Stack_1_t3180884369 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m4095051687_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<Pomelo.DotNetClient.MsgEvent>::Clear()
#define Stack_1_Clear_m4037949135(__this, method) ((  void (*) (Stack_1_t3180884369 *, const MethodInfo*))Stack_1_Clear_m131822403_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<Pomelo.DotNetClient.MsgEvent>::Contains(T)
#define Stack_1_Contains_m1924202345(__this, ___t0, method) ((  bool (*) (Stack_1_t3180884369 *, MsgEvent_t82323445 *, const MethodInfo*))Stack_1_Contains_m328948937_gshared)(__this, ___t0, method)
// T System.Collections.Generic.Stack`1<Pomelo.DotNetClient.MsgEvent>::Peek()
#define Stack_1_Peek_m3964933114(__this, method) ((  MsgEvent_t82323445 * (*) (Stack_1_t3180884369 *, const MethodInfo*))Stack_1_Peek_m3418768488_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<Pomelo.DotNetClient.MsgEvent>::Pop()
#define Stack_1_Pop_m266754804(__this, method) ((  MsgEvent_t82323445 * (*) (Stack_1_t3180884369 *, const MethodInfo*))Stack_1_Pop_m4267009222_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<Pomelo.DotNetClient.MsgEvent>::Push(T)
#define Stack_1_Push_m2961325540(__this, ___t0, method) ((  void (*) (Stack_1_t3180884369 *, MsgEvent_t82323445 *, const MethodInfo*))Stack_1_Push_m3350166104_gshared)(__this, ___t0, method)
// System.Int32 System.Collections.Generic.Stack`1<Pomelo.DotNetClient.MsgEvent>::get_Count()
#define Stack_1_get_Count_m2851161720(__this, method) ((  int32_t (*) (Stack_1_t3180884369 *, const MethodInfo*))Stack_1_get_Count_m3631765324_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<Pomelo.DotNetClient.MsgEvent>::GetEnumerator()
#define Stack_1_GetEnumerator_m2908875930(__this, method) ((  Enumerator_t2738670395  (*) (Stack_1_t3180884369 *, const MethodInfo*))Stack_1_GetEnumerator_m202302354_gshared)(__this, method)
