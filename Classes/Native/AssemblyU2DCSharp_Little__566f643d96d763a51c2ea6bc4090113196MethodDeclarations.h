﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._566f643d96d763a51c2ea6bc22726dd2
struct _566f643d96d763a51c2ea6bc22726dd2_t4090113196;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__566f643d96d763a51c2ea6bc4090113196.h"

// System.Void Little._566f643d96d763a51c2ea6bc22726dd2::.ctor()
extern "C"  void _566f643d96d763a51c2ea6bc22726dd2__ctor_m738360065 (_566f643d96d763a51c2ea6bc22726dd2_t4090113196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._566f643d96d763a51c2ea6bc22726dd2::_566f643d96d763a51c2ea6bc22726dd2m2(System.Int32)
extern "C"  int32_t _566f643d96d763a51c2ea6bc22726dd2__566f643d96d763a51c2ea6bc22726dd2m2_m815297049 (_566f643d96d763a51c2ea6bc22726dd2_t4090113196 * __this, int32_t ____566f643d96d763a51c2ea6bc22726dd2a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._566f643d96d763a51c2ea6bc22726dd2::_566f643d96d763a51c2ea6bc22726dd2m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _566f643d96d763a51c2ea6bc22726dd2__566f643d96d763a51c2ea6bc22726dd2m_m1671546045 (_566f643d96d763a51c2ea6bc22726dd2_t4090113196 * __this, int32_t ____566f643d96d763a51c2ea6bc22726dd2a0, int32_t ____566f643d96d763a51c2ea6bc22726dd2471, int32_t ____566f643d96d763a51c2ea6bc22726dd2c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._566f643d96d763a51c2ea6bc22726dd2::ilo__566f643d96d763a51c2ea6bc22726dd2m21(Little._566f643d96d763a51c2ea6bc22726dd2,System.Int32)
extern "C"  int32_t _566f643d96d763a51c2ea6bc22726dd2_ilo__566f643d96d763a51c2ea6bc22726dd2m21_m3780406259 (Il2CppObject * __this /* static, unused */, _566f643d96d763a51c2ea6bc22726dd2_t4090113196 * ____this0, int32_t ____566f643d96d763a51c2ea6bc22726dd2a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
