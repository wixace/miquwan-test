﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Ray2DGenerated
struct UnityEngine_Ray2DGenerated_t582650971;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_Ray2DGenerated::.ctor()
extern "C"  void UnityEngine_Ray2DGenerated__ctor_m907448992 (UnityEngine_Ray2DGenerated_t582650971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Ray2DGenerated::.cctor()
extern "C"  void UnityEngine_Ray2DGenerated__cctor_m1879018765 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Ray2DGenerated::Ray2D_Ray2D1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Ray2DGenerated_Ray2D_Ray2D1_m43439968 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Ray2DGenerated::Ray2D_Ray2D2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Ray2DGenerated_Ray2D_Ray2D2_m1288204449 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Ray2DGenerated::Ray2D_origin(JSVCall)
extern "C"  void UnityEngine_Ray2DGenerated_Ray2D_origin_m1247037146 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Ray2DGenerated::Ray2D_direction(JSVCall)
extern "C"  void UnityEngine_Ray2DGenerated_Ray2D_direction_m1726510317 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Ray2DGenerated::Ray2D_GetPoint__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Ray2DGenerated_Ray2D_GetPoint__Single_m3715621261 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Ray2DGenerated::Ray2D_ToString__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Ray2DGenerated_Ray2D_ToString__String_m3351370664 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Ray2DGenerated::Ray2D_ToString(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Ray2DGenerated_Ray2D_ToString_m1872061527 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Ray2DGenerated::__Register()
extern "C"  void UnityEngine_Ray2DGenerated___Register_m3252522535 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Ray2DGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_Ray2DGenerated_ilo_attachFinalizerObject1_m447735871 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Ray2DGenerated::ilo_setVector2S2(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_Ray2DGenerated_ilo_setVector2S2_m3174624200 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine_Ray2DGenerated::ilo_getVector2S3(System.Int32)
extern "C"  Vector2_t4282066565  UnityEngine_Ray2DGenerated_ilo_getVector2S3_m2855008956 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Ray2DGenerated::ilo_setStringS4(System.Int32,System.String)
extern "C"  void UnityEngine_Ray2DGenerated_ilo_setStringS4_m1015137004 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Ray2DGenerated::ilo_changeJSObj5(System.Int32,System.Object)
extern "C"  void UnityEngine_Ray2DGenerated_ilo_changeJSObj5_m636045517 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
