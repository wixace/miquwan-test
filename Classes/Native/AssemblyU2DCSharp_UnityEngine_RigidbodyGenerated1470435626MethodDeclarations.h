﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_RigidbodyGenerated
struct UnityEngine_RigidbodyGenerated_t1470435626;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_RigidbodyGenerated::.ctor()
extern "C"  void UnityEngine_RigidbodyGenerated__ctor_m3313842865 (UnityEngine_RigidbodyGenerated_t1470435626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_Rigidbody1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_Rigidbody1_m3435516301 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::Rigidbody_velocity(JSVCall)
extern "C"  void UnityEngine_RigidbodyGenerated_Rigidbody_velocity_m1899167653 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::Rigidbody_angularVelocity(JSVCall)
extern "C"  void UnityEngine_RigidbodyGenerated_Rigidbody_angularVelocity_m200820139 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::Rigidbody_drag(JSVCall)
extern "C"  void UnityEngine_RigidbodyGenerated_Rigidbody_drag_m3699838766 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::Rigidbody_angularDrag(JSVCall)
extern "C"  void UnityEngine_RigidbodyGenerated_Rigidbody_angularDrag_m909920308 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::Rigidbody_mass(JSVCall)
extern "C"  void UnityEngine_RigidbodyGenerated_Rigidbody_mass_m2816224622 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::Rigidbody_useGravity(JSVCall)
extern "C"  void UnityEngine_RigidbodyGenerated_Rigidbody_useGravity_m2262257147 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::Rigidbody_maxDepenetrationVelocity(JSVCall)
extern "C"  void UnityEngine_RigidbodyGenerated_Rigidbody_maxDepenetrationVelocity_m1102068511 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::Rigidbody_isKinematic(JSVCall)
extern "C"  void UnityEngine_RigidbodyGenerated_Rigidbody_isKinematic_m3677031951 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::Rigidbody_freezeRotation(JSVCall)
extern "C"  void UnityEngine_RigidbodyGenerated_Rigidbody_freezeRotation_m3196912461 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::Rigidbody_constraints(JSVCall)
extern "C"  void UnityEngine_RigidbodyGenerated_Rigidbody_constraints_m1892569140 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::Rigidbody_collisionDetectionMode(JSVCall)
extern "C"  void UnityEngine_RigidbodyGenerated_Rigidbody_collisionDetectionMode_m2262087660 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::Rigidbody_centerOfMass(JSVCall)
extern "C"  void UnityEngine_RigidbodyGenerated_Rigidbody_centerOfMass_m415088578 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::Rigidbody_worldCenterOfMass(JSVCall)
extern "C"  void UnityEngine_RigidbodyGenerated_Rigidbody_worldCenterOfMass_m405105688 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::Rigidbody_inertiaTensorRotation(JSVCall)
extern "C"  void UnityEngine_RigidbodyGenerated_Rigidbody_inertiaTensorRotation_m174741369 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::Rigidbody_inertiaTensor(JSVCall)
extern "C"  void UnityEngine_RigidbodyGenerated_Rigidbody_inertiaTensor_m3422943831 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::Rigidbody_detectCollisions(JSVCall)
extern "C"  void UnityEngine_RigidbodyGenerated_Rigidbody_detectCollisions_m334993886 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::Rigidbody_useConeFriction(JSVCall)
extern "C"  void UnityEngine_RigidbodyGenerated_Rigidbody_useConeFriction_m237947334 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::Rigidbody_position(JSVCall)
extern "C"  void UnityEngine_RigidbodyGenerated_Rigidbody_position_m4123769049 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::Rigidbody_rotation(JSVCall)
extern "C"  void UnityEngine_RigidbodyGenerated_Rigidbody_rotation_m3792474660 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::Rigidbody_interpolation(JSVCall)
extern "C"  void UnityEngine_RigidbodyGenerated_Rigidbody_interpolation_m1282195334 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::Rigidbody_solverIterationCount(JSVCall)
extern "C"  void UnityEngine_RigidbodyGenerated_Rigidbody_solverIterationCount_m3790463069 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::Rigidbody_sleepThreshold(JSVCall)
extern "C"  void UnityEngine_RigidbodyGenerated_Rigidbody_sleepThreshold_m756827662 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::Rigidbody_maxAngularVelocity(JSVCall)
extern "C"  void UnityEngine_RigidbodyGenerated_Rigidbody_maxAngularVelocity_m536366183 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_AddExplosionForce__Single__Vector3__Single__Single__ForceMode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_AddExplosionForce__Single__Vector3__Single__Single__ForceMode_m1079030598 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_AddExplosionForce__Single__Vector3__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_AddExplosionForce__Single__Vector3__Single__Single_m147993066 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_AddExplosionForce__Single__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_AddExplosionForce__Single__Vector3__Single_m2600543394 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_AddForce__Single__Single__Single__ForceMode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_AddForce__Single__Single__Single__ForceMode_m1576672101 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_AddForce__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_AddForce__Single__Single__Single_m3893929771 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_AddForce__Vector3__ForceMode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_AddForce__Vector3__ForceMode_m722689201 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_AddForce__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_AddForce__Vector3_m1757101407 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_AddForceAtPosition__Vector3__Vector3__ForceMode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_AddForceAtPosition__Vector3__Vector3__ForceMode_m702606369 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_AddForceAtPosition__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_AddForceAtPosition__Vector3__Vector3_m2228024303 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_AddRelativeForce__Single__Single__Single__ForceMode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_AddRelativeForce__Single__Single__Single__ForceMode_m4099140689 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_AddRelativeForce__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_AddRelativeForce__Single__Single__Single_m3459789247 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_AddRelativeForce__Vector3__ForceMode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_AddRelativeForce__Vector3__ForceMode_m1546080581 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_AddRelativeForce__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_AddRelativeForce__Vector3_m3859963211 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_AddRelativeTorque__Single__Single__Single__ForceMode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_AddRelativeTorque__Single__Single__Single__ForceMode_m3095711432 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_AddRelativeTorque__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_AddRelativeTorque__Single__Single__Single_m1306276776 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_AddRelativeTorque__Vector3__ForceMode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_AddRelativeTorque__Vector3__ForceMode_m2128785326 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_AddRelativeTorque__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_AddRelativeTorque__Vector3_m480372354 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_AddTorque__Single__Single__Single__ForceMode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_AddTorque__Single__Single__Single__ForceMode_m2208596532 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_AddTorque__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_AddTorque__Single__Single__Single_m1879731132 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_AddTorque__Vector3__ForceMode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_AddTorque__Vector3__ForceMode_m2373456322 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_AddTorque__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_AddTorque__Vector3_m4011133166 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_ClosestPointOnBounds__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_ClosestPointOnBounds__Vector3_m106729150 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_GetPointVelocity__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_GetPointVelocity__Vector3_m1410641842 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_GetRelativePointVelocity__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_GetRelativePointVelocity__Vector3_m1146298014 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_IsSleeping(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_IsSleeping_m1217521726 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_MovePosition__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_MovePosition__Vector3_m3892893199 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_MoveRotation__Quaternion(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_MoveRotation__Quaternion_m942350934 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_ResetCenterOfMass(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_ResetCenterOfMass_m736752104 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_ResetInertiaTensor(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_ResetInertiaTensor_m4060917933 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_SetDensity__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_SetDensity__Single_m2175549175 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_Sleep(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_Sleep_m435702192 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_SweepTest__Vector3__RaycastHit__Single__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_SweepTest__Vector3__RaycastHit__Single__QueryTriggerInteraction_m111639831 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_SweepTest__Vector3__RaycastHit__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_SweepTest__Vector3__RaycastHit__Single_m1345742637 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_SweepTest__Vector3__RaycastHit(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_SweepTest__Vector3__RaycastHit_m908348645 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_SweepTestAll__Vector3__Single__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_SweepTestAll__Vector3__Single__QueryTriggerInteraction_m88491062 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_SweepTestAll__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_SweepTestAll__Vector3__Single_m3344331566 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_SweepTestAll__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_SweepTestAll__Vector3_m1760356838 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::Rigidbody_WakeUp(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_Rigidbody_WakeUp_m784748328 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::__Register()
extern "C"  void UnityEngine_RigidbodyGenerated___Register_m1918519478 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::ilo_setVector3S1(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_RigidbodyGenerated_ilo_setVector3S1_m3635631862 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_RigidbodyGenerated::ilo_getVector3S2(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_RigidbodyGenerated_ilo_getVector3S2_m3951821226 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void UnityEngine_RigidbodyGenerated_ilo_setSingle3_m2854052133 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RigidbodyGenerated::ilo_getBooleanS4(System.Int32)
extern "C"  bool UnityEngine_RigidbodyGenerated_ilo_getBooleanS4_m1259961022 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void UnityEngine_RigidbodyGenerated_ilo_setBooleanS5_m4269194974 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RigidbodyGenerated::ilo_getEnum6(System.Int32)
extern "C"  int32_t UnityEngine_RigidbodyGenerated_ilo_getEnum6_m282031364 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RigidbodyGenerated::ilo_setObject7(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_RigidbodyGenerated_ilo_setObject7_m431593855 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_RigidbodyGenerated::ilo_getObject8(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_RigidbodyGenerated_ilo_getObject8_m792099173 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RigidbodyGenerated::ilo_getInt329(System.Int32)
extern "C"  int32_t UnityEngine_RigidbodyGenerated_ilo_getInt329_m3127242496 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_RigidbodyGenerated::ilo_getSingle10(System.Int32)
extern "C"  float UnityEngine_RigidbodyGenerated_ilo_getSingle10_m3367261246 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RigidbodyGenerated::ilo_incArgIndex11()
extern "C"  int32_t UnityEngine_RigidbodyGenerated_ilo_incArgIndex11_m215915208 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RigidbodyGenerated::ilo_setArgIndex12(System.Int32)
extern "C"  void UnityEngine_RigidbodyGenerated_ilo_setArgIndex12_m3302675116 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
