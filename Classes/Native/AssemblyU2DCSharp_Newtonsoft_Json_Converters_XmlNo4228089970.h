﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlNodeConverter/<DeserializeNode>c__AnonStorey12C
struct  U3CDeserializeNodeU3Ec__AnonStorey12C_t4228089970  : public Il2CppObject
{
public:
	// System.String Newtonsoft.Json.Converters.XmlNodeConverter/<DeserializeNode>c__AnonStorey12C::propertyName
	String_t* ___propertyName_0;

public:
	inline static int32_t get_offset_of_propertyName_0() { return static_cast<int32_t>(offsetof(U3CDeserializeNodeU3Ec__AnonStorey12C_t4228089970, ___propertyName_0)); }
	inline String_t* get_propertyName_0() const { return ___propertyName_0; }
	inline String_t** get_address_of_propertyName_0() { return &___propertyName_0; }
	inline void set_propertyName_0(String_t* value)
	{
		___propertyName_0 = value;
		Il2CppCodeGenWriteBarrier(&___propertyName_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
