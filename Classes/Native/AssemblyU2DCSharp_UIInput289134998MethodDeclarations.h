﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIInput
struct UIInput_t289134998;
// System.String
struct String_t;
// UITexture
struct UITexture_t3903132647;
// UIWidget
struct UIWidget_t769069560;
// UILabel
struct UILabel_t291504320;
// UnityEngine.Behaviour
struct Behaviour_t200106419;
// UIInput/OnValidate
struct OnValidate_t2690340430;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UnityEngine.Object
struct Object_t3071478659;
// System.Collections.Generic.List`1<EventDelegate>
struct List_1_t1077642479;
// UIFont
struct UIFont_t2503090435;
// BMFont
struct BMFont_t1962830650;
// BMGlyph
struct BMGlyph_t719052705;
// UnityEngine.Texture
struct Texture_t2526458961;
// UIGeometry
struct UIGeometry_t3586695974;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_UIInput289134998.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_UILabel291504320.h"
#include "AssemblyU2DCSharp_NGUIText_Alignment3426431694.h"
#include "UnityEngine_UnityEngine_Behaviour200106419.h"
#include "AssemblyU2DCSharp_UIInput_OnValidate2690340430.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "AssemblyU2DCSharp_UIFont2503090435.h"
#include "AssemblyU2DCSharp_BMFont1962830650.h"
#include "AssemblyU2DCSharp_UITexture3903132647.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "AssemblyU2DCSharp_UIWidget_Pivot240933195.h"
#include "AssemblyU2DCSharp_UIGeometry3586695974.h"

// System.Void UIInput::.ctor()
extern "C"  void UIInput__ctor_m926531221 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::.cctor()
extern "C"  void UIInput__cctor_m2470567864 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UIInput::get_defaultText()
extern "C"  String_t* UIInput_get_defaultText_m3672375323 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::set_defaultText(System.String)
extern "C"  void UIInput_set_defaultText_m3855940030 (UIInput_t289134998 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIInput::get_inputShouldBeHidden()
extern "C"  bool UIInput_get_inputShouldBeHidden_m4156970856 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UIInput::get_text()
extern "C"  String_t* UIInput_get_text_m3139426530 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::set_text(System.String)
extern "C"  void UIInput_set_text_m3606283849 (UIInput_t289134998 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UIInput::get_value()
extern "C"  String_t* UIInput_get_value_m187471550 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::set_value(System.String)
extern "C"  void UIInput_set_value_m753256763 (UIInput_t289134998 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIInput::get_selected()
extern "C"  bool UIInput_get_selected_m3709403423 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::set_selected(System.Boolean)
extern "C"  void UIInput_set_selected_m2884057854 (UIInput_t289134998 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIInput::get_isSelected()
extern "C"  bool UIInput_get_isSelected_m2185492777 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::set_isSelected(System.Boolean)
extern "C"  void UIInput_set_isSelected_m4090711944 (UIInput_t289134998 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIInput::get_cursorPosition()
extern "C"  int32_t UIInput_get_cursorPosition_m2810772681 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::set_cursorPosition(System.Int32)
extern "C"  void UIInput_set_cursorPosition_m1001112924 (UIInput_t289134998 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIInput::get_selectionStart()
extern "C"  int32_t UIInput_get_selectionStart_m3784741536 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::set_selectionStart(System.Int32)
extern "C"  void UIInput_set_selectionStart_m1561088179 (UIInput_t289134998 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIInput::get_selectionEnd()
extern "C"  int32_t UIInput_get_selectionEnd_m1711499289 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::set_selectionEnd(System.Int32)
extern "C"  void UIInput_set_selectionEnd_m3982164268 (UIInput_t289134998 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UITexture UIInput::get_caret()
extern "C"  UITexture_t3903132647 * UIInput_get_caret_m3992389369 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UIInput::Validate(System.String)
extern "C"  String_t* UIInput_Validate_m647251104 (UIInput_t289134998 * __this, String_t* ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::Start()
extern "C"  void UIInput_Start_m4168636309 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::Init()
extern "C"  void UIInput_Init_m2336729375 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::SaveToPlayerPrefs(System.String)
extern "C"  void UIInput_SaveToPlayerPrefs_m1967016952 (UIInput_t289134998 * __this, String_t* ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::OnSelect(System.Boolean)
extern "C"  void UIInput_OnSelect_m3605689857 (UIInput_t289134998 * __this, bool ___isSelected0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::OnSelectEvent()
extern "C"  void UIInput_OnSelectEvent_m3041294738 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::OnDeselectEvent()
extern "C"  void UIInput_OnDeselectEvent_m2876542449 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::Update()
extern "C"  void UIInput_Update_m384558872 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::DoBackspace()
extern "C"  void UIInput_DoBackspace_m3430448487 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::Insert(System.String)
extern "C"  void UIInput_Insert_m840718074 (UIInput_t289134998 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIInput::GetByteCount(System.String)
extern "C"  int32_t UIInput_GetByteCount_m1042934288 (UIInput_t289134998 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UIInput::GetLeftText()
extern "C"  String_t* UIInput_GetLeftText_m2658112736 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UIInput::GetRightText()
extern "C"  String_t* UIInput_GetRightText_m2994177663 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UIInput::GetSelection()
extern "C"  String_t* UIInput_GetSelection_m3614338530 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIInput::GetCharUnderMouse()
extern "C"  int32_t UIInput_GetCharUnderMouse_m796057882 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::OnPress(System.Boolean)
extern "C"  void UIInput_OnPress_m266899726 (UIInput_t289134998 * __this, bool ___isPressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::OnDrag(UnityEngine.Vector2)
extern "C"  void UIInput_OnDrag_m3925666040 (UIInput_t289134998 * __this, Vector2_t4282066565  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::OnDisable()
extern "C"  void UIInput_OnDisable_m2120597628 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::Cleanup()
extern "C"  void UIInput_Cleanup_m4007034071 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::Submit()
extern "C"  void UIInput_Submit_m1289967271 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::UpdateLabel()
extern "C"  void UIInput_UpdateLabel_m2984575774 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::SetPivotToLeft()
extern "C"  void UIInput_SetPivotToLeft_m2520116305 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::SetPivotToRight()
extern "C"  void UIInput_SetPivotToRight_m1959404596 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::RestoreLabelPivot()
extern "C"  void UIInput_RestoreLabelPivot_m3047902351 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char UIInput::Validate(System.String,System.Int32,System.Char)
extern "C"  Il2CppChar UIInput_Validate_m3634842707 (UIInput_t289134998 * __this, String_t* ___text0, int32_t ___pos1, Il2CppChar ___ch2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::ExecuteOnChange()
extern "C"  void UIInput_ExecuteOnChange_m4209425719 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::RemoveFocus()
extern "C"  void UIInput_RemoveFocus_m753211367 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::SaveValue()
extern "C"  void UIInput_SaveValue_m3264418727 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::LoadValue()
extern "C"  void UIInput_LoadValue_m2060982302 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::OnDestory()
extern "C"  void UIInput_OnDestory_m481192 (UIInput_t289134998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UIInput::ilo_get_value1(UIInput)
extern "C"  String_t* UIInput_ilo_get_value1_m2466492820 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIInput::ilo_get_isSelected2(UIInput)
extern "C"  bool UIInput_ilo_get_isSelected2_m1795333604 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::ilo_SaveToPlayerPrefs3(UIInput,System.String)
extern "C"  void UIInput_ilo_SaveToPlayerPrefs3_m3309814938 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, String_t* ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::ilo_UpdateLabel4(UIInput)
extern "C"  void UIInput_ilo_UpdateLabel4_m1251005329 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIInput::ilo_get_inputShouldBeHidden5(UIInput)
extern "C"  bool UIInput_ilo_get_inputShouldBeHidden5_m3737393530 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::ilo_set_value6(UIInput,System.String)
extern "C"  void UIInput_ilo_set_value6_m119385172 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UIInput::ilo_get_color7(UIWidget)
extern "C"  Color_t4194546905  UIInput_ilo_get_color7_m1829627584 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::ilo_set_supportEncoding8(UILabel,System.Boolean)
extern "C"  void UIInput_ilo_set_supportEncoding8_m1131044448 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NGUIText/Alignment UIInput::ilo_get_alignment9(UILabel)
extern "C"  int32_t UIInput_ilo_get_alignment9_m2132148064 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::ilo_set_alignment10(UILabel,NGUIText/Alignment)
extern "C"  void UIInput_ilo_set_alignment10_m544451883 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::ilo_Init11(UIInput)
extern "C"  void UIInput_ilo_Init11_m3696036202 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIInput::ilo_GetActive12(UnityEngine.Behaviour)
extern "C"  bool UIInput_ilo_GetActive12_m196568891 (Il2CppObject * __this /* static, unused */, Behaviour_t200106419 * ___mb0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::ilo_set_color13(UIWidget,UnityEngine.Color)
extern "C"  void UIInput_ilo_set_color13_m3469210192 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, Color_t4194546905  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIInput::ilo_get_multiLine14(UILabel)
extern "C"  bool UIInput_ilo_get_multiLine14_m494104215 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::ilo_DoBackspace15(UIInput)
extern "C"  void UIInput_ilo_DoBackspace15_m58862884 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::ilo_set_isSelected16(UIInput,System.Boolean)
extern "C"  void UIInput_ilo_set_isSelected16_m3354751690 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::ilo_Insert17(UIInput,System.String)
extern "C"  void UIInput_ilo_Insert17_m916107287 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::ilo_ExecuteOnChange18(UIInput)
extern "C"  void UIInput_ilo_ExecuteOnChange18_m683956529 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIInput::ilo_get_time19()
extern "C"  float UIInput_ilo_get_time19_m1302075412 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UIInput::ilo_GetLeftText20(UIInput)
extern "C"  String_t* UIInput_ilo_GetLeftText20_m3218728049 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIInput::ilo_GetByteCount21(UIInput,System.String)
extern "C"  int32_t UIInput_ilo_GetByteCount21_m1929189044 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, String_t* ___str1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char UIInput::ilo_Invoke22(UIInput/OnValidate,System.String,System.Int32,System.Char)
extern "C"  Il2CppChar UIInput_ilo_Invoke22_m3804399812 (Il2CppObject * __this /* static, unused */, OnValidate_t2690340430 * ____this0, String_t* ___text1, int32_t ___charIndex2, Il2CppChar ___addedChar3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UIInput::ilo_get_worldCorners23(UILabel)
extern "C"  Vector3U5BU5D_t215400611* UIInput_ilo_get_worldCorners23_m2078257362 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray UIInput::ilo_get_currentRay24()
extern "C"  Ray_t3134616544  UIInput_ilo_get_currentRay24_m480758718 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIInput::ilo_GetCharacterIndexAtPosition25(UILabel,UnityEngine.Vector3,System.Boolean)
extern "C"  int32_t UIInput_ilo_GetCharacterIndexAtPosition25_m705711812 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, Vector3_t4282066566  ___worldPos1, bool ___precise2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::ilo_set_selectionEnd26(UIInput,System.Int32)
extern "C"  void UIInput_ilo_set_selectionEnd26_m1899710093 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIInput::ilo_GetCharUnderMouse27(UIInput)
extern "C"  int32_t UIInput_ilo_GetCharUnderMouse27_m1967254672 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::ilo_Cleanup28(UIInput)
extern "C"  void UIInput_ilo_Cleanup28_m1624834898 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::ilo_Destroy29(UnityEngine.Object)
extern "C"  void UIInput_ilo_Destroy29_m327527083 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::ilo_Execute30(System.Collections.Generic.List`1<EventDelegate>)
extern "C"  void UIInput_ilo_Execute30_m1837838229 (Il2CppObject * __this /* static, unused */, List_1_t1077642479 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIFont UIInput::ilo_get_bitmapFont31(UILabel)
extern "C"  UIFont_t2503090435 * UIInput_ilo_get_bitmapFont31_m2155692613 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BMFont UIInput::ilo_get_bmFont32(UIFont)
extern "C"  BMFont_t1962830650 * UIInput_ilo_get_bmFont32_m2381207230 (Il2CppObject * __this /* static, unused */, UIFont_t2503090435 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BMGlyph UIInput::ilo_GetGlyph33(BMFont,System.Int32)
extern "C"  BMGlyph_t719052705 * UIInput_ilo_GetGlyph33_m3244300363 (Il2CppObject * __this /* static, unused */, BMFont_t1962830650 * ____this0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIInput::ilo_CalculateOffsetToFit34(UILabel,System.String)
extern "C"  int32_t UIInput_ilo_CalculateOffsetToFit34_m4015255324 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::ilo_SetPivotToLeft35(UIInput)
extern "C"  void UIInput_ilo_SetPivotToLeft35_m3873342518 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::ilo_SetPivotToRight36(UIInput)
extern "C"  void UIInput_ilo_SetPivotToRight36_m253690392 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::ilo_set_text37(UILabel,System.String)
extern "C"  void UIInput_ilo_set_text37_m3193920254 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::ilo_set_mainTexture38(UITexture,UnityEngine.Texture)
extern "C"  void UIInput_ilo_set_mainTexture38_m2396184297 (Il2CppObject * __this /* static, unused */, UITexture_t3903132647 * ____this0, Texture_t2526458961 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIWidget/Pivot UIInput::ilo_get_pivot39(UIWidget)
extern "C"  int32_t UIInput_ilo_get_pivot39_m2651891737 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::ilo_set_pivot40(UIWidget,UIWidget/Pivot)
extern "C"  void UIInput_ilo_set_pivot40_m3283807378 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::ilo_MarkAsChanged41(UIWidget)
extern "C"  void UIInput_ilo_MarkAsChanged41_m3705422074 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInput::ilo_PrintOverlay42(UILabel,System.Int32,System.Int32,UIGeometry,UIGeometry,UnityEngine.Color,UnityEngine.Color)
extern "C"  void UIInput_ilo_PrintOverlay42_m2391496911 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, int32_t ___start1, int32_t ___end2, UIGeometry_t3586695974 * ___caret3, UIGeometry_t3586695974 * ___highlight4, Color_t4194546905  ___caretColor5, Color_t4194546905  ___highlightColor6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
