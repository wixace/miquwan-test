﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entity.Behavior.PursuitBvr
struct  PursuitBvr_t3738440388  : public IBehavior_t770859129
{
public:
	// System.Single Entity.Behavior.PursuitBvr::_fightDis
	float ____fightDis_2;
	// UnityEngine.Vector3 Entity.Behavior.PursuitBvr::_initPos
	Vector3_t4282066566  ____initPos_3;

public:
	inline static int32_t get_offset_of__fightDis_2() { return static_cast<int32_t>(offsetof(PursuitBvr_t3738440388, ____fightDis_2)); }
	inline float get__fightDis_2() const { return ____fightDis_2; }
	inline float* get_address_of__fightDis_2() { return &____fightDis_2; }
	inline void set__fightDis_2(float value)
	{
		____fightDis_2 = value;
	}

	inline static int32_t get_offset_of__initPos_3() { return static_cast<int32_t>(offsetof(PursuitBvr_t3738440388, ____initPos_3)); }
	inline Vector3_t4282066566  get__initPos_3() const { return ____initPos_3; }
	inline Vector3_t4282066566 * get_address_of__initPos_3() { return &____initPos_3; }
	inline void set__initPos_3(Vector3_t4282066566  value)
	{
		____initPos_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
