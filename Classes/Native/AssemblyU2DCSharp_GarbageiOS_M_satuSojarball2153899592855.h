﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_satuSojarball215
struct  M_satuSojarball215_t3899592855  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_satuSojarball215::_sorperenirKairpall
	uint32_t ____sorperenirKairpall_0;
	// System.Boolean GarbageiOS.M_satuSojarball215::_disonee
	bool ____disonee_1;
	// System.UInt32 GarbageiOS.M_satuSojarball215::_perbarCheeremlay
	uint32_t ____perbarCheeremlay_2;
	// System.Int32 GarbageiOS.M_satuSojarball215::_cairtefi
	int32_t ____cairtefi_3;
	// System.UInt32 GarbageiOS.M_satuSojarball215::_tirloDarvel
	uint32_t ____tirloDarvel_4;
	// System.UInt32 GarbageiOS.M_satuSojarball215::_nelxesas
	uint32_t ____nelxesas_5;
	// System.UInt32 GarbageiOS.M_satuSojarball215::_trafou
	uint32_t ____trafou_6;
	// System.String GarbageiOS.M_satuSojarball215::_herbirmasCojeber
	String_t* ____herbirmasCojeber_7;
	// System.UInt32 GarbageiOS.M_satuSojarball215::_bemcedaw
	uint32_t ____bemcedaw_8;
	// System.Boolean GarbageiOS.M_satuSojarball215::_jelnairHawfu
	bool ____jelnairHawfu_9;
	// System.Single GarbageiOS.M_satuSojarball215::_dranonall
	float ____dranonall_10;
	// System.Boolean GarbageiOS.M_satuSojarball215::_dirmirpe
	bool ____dirmirpe_11;
	// System.Int32 GarbageiOS.M_satuSojarball215::_mebounorMastirju
	int32_t ____mebounorMastirju_12;
	// System.Single GarbageiOS.M_satuSojarball215::_loutrurCiralmas
	float ____loutrurCiralmas_13;
	// System.Boolean GarbageiOS.M_satuSojarball215::_wepa
	bool ____wepa_14;
	// System.Int32 GarbageiOS.M_satuSojarball215::_sawpairJawrowe
	int32_t ____sawpairJawrowe_15;
	// System.UInt32 GarbageiOS.M_satuSojarball215::_mouhouboMaigear
	uint32_t ____mouhouboMaigear_16;
	// System.Single GarbageiOS.M_satuSojarball215::_lairleGearlowyow
	float ____lairleGearlowyow_17;
	// System.Boolean GarbageiOS.M_satuSojarball215::_weargercerTasou
	bool ____weargercerTasou_18;
	// System.UInt32 GarbageiOS.M_satuSojarball215::_wujoolayGerpivai
	uint32_t ____wujoolayGerpivai_19;
	// System.Int32 GarbageiOS.M_satuSojarball215::_laipiwu
	int32_t ____laipiwu_20;
	// System.String GarbageiOS.M_satuSojarball215::_ceenereser
	String_t* ____ceenereser_21;
	// System.Int32 GarbageiOS.M_satuSojarball215::_beartoucor
	int32_t ____beartoucor_22;
	// System.String GarbageiOS.M_satuSojarball215::_vempuvuKalmow
	String_t* ____vempuvuKalmow_23;
	// System.Single GarbageiOS.M_satuSojarball215::_xewoukooHeejimay
	float ____xewoukooHeejimay_24;
	// System.String GarbageiOS.M_satuSojarball215::_hurkal
	String_t* ____hurkal_25;
	// System.String GarbageiOS.M_satuSojarball215::_mechel
	String_t* ____mechel_26;
	// System.Int32 GarbageiOS.M_satuSojarball215::_boutra
	int32_t ____boutra_27;
	// System.String GarbageiOS.M_satuSojarball215::_pijouvoLoyay
	String_t* ____pijouvoLoyay_28;
	// System.Single GarbageiOS.M_satuSojarball215::_drirjenaiCochai
	float ____drirjenaiCochai_29;
	// System.String GarbageiOS.M_satuSojarball215::_chuherege
	String_t* ____chuherege_30;
	// System.Int32 GarbageiOS.M_satuSojarball215::_ceereri
	int32_t ____ceereri_31;
	// System.String GarbageiOS.M_satuSojarball215::_whayaifaKayte
	String_t* ____whayaifaKayte_32;
	// System.Single GarbageiOS.M_satuSojarball215::_raperWherjosor
	float ____raperWherjosor_33;
	// System.String GarbageiOS.M_satuSojarball215::_reltawdou
	String_t* ____reltawdou_34;
	// System.String GarbageiOS.M_satuSojarball215::_selfearfallGearso
	String_t* ____selfearfallGearso_35;
	// System.String GarbageiOS.M_satuSojarball215::_zesaiCiror
	String_t* ____zesaiCiror_36;
	// System.UInt32 GarbageiOS.M_satuSojarball215::_sootiteaLorcerrair
	uint32_t ____sootiteaLorcerrair_37;
	// System.Boolean GarbageiOS.M_satuSojarball215::_nalwishas
	bool ____nalwishas_38;
	// System.UInt32 GarbageiOS.M_satuSojarball215::_whisxaParse
	uint32_t ____whisxaParse_39;
	// System.Single GarbageiOS.M_satuSojarball215::_trayboofaZalpai
	float ____trayboofaZalpai_40;
	// System.Single GarbageiOS.M_satuSojarball215::_sowkea
	float ____sowkea_41;
	// System.Single GarbageiOS.M_satuSojarball215::_jurcel
	float ____jurcel_42;
	// System.Int32 GarbageiOS.M_satuSojarball215::_musalColeltal
	int32_t ____musalColeltal_43;
	// System.String GarbageiOS.M_satuSojarball215::_toupereFixo
	String_t* ____toupereFixo_44;
	// System.Single GarbageiOS.M_satuSojarball215::_jastasLallsearti
	float ____jastasLallsearti_45;
	// System.UInt32 GarbageiOS.M_satuSojarball215::_jouriCorsou
	uint32_t ____jouriCorsou_46;
	// System.Boolean GarbageiOS.M_satuSojarball215::_labawBorleatou
	bool ____labawBorleatou_47;
	// System.UInt32 GarbageiOS.M_satuSojarball215::_drircaselBaltreya
	uint32_t ____drircaselBaltreya_48;
	// System.Single GarbageiOS.M_satuSojarball215::_jasramoSatar
	float ____jasramoSatar_49;
	// System.String GarbageiOS.M_satuSojarball215::_wircas
	String_t* ____wircas_50;
	// System.Boolean GarbageiOS.M_satuSojarball215::_woowouTaljasxor
	bool ____woowouTaljasxor_51;

public:
	inline static int32_t get_offset_of__sorperenirKairpall_0() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____sorperenirKairpall_0)); }
	inline uint32_t get__sorperenirKairpall_0() const { return ____sorperenirKairpall_0; }
	inline uint32_t* get_address_of__sorperenirKairpall_0() { return &____sorperenirKairpall_0; }
	inline void set__sorperenirKairpall_0(uint32_t value)
	{
		____sorperenirKairpall_0 = value;
	}

	inline static int32_t get_offset_of__disonee_1() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____disonee_1)); }
	inline bool get__disonee_1() const { return ____disonee_1; }
	inline bool* get_address_of__disonee_1() { return &____disonee_1; }
	inline void set__disonee_1(bool value)
	{
		____disonee_1 = value;
	}

	inline static int32_t get_offset_of__perbarCheeremlay_2() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____perbarCheeremlay_2)); }
	inline uint32_t get__perbarCheeremlay_2() const { return ____perbarCheeremlay_2; }
	inline uint32_t* get_address_of__perbarCheeremlay_2() { return &____perbarCheeremlay_2; }
	inline void set__perbarCheeremlay_2(uint32_t value)
	{
		____perbarCheeremlay_2 = value;
	}

	inline static int32_t get_offset_of__cairtefi_3() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____cairtefi_3)); }
	inline int32_t get__cairtefi_3() const { return ____cairtefi_3; }
	inline int32_t* get_address_of__cairtefi_3() { return &____cairtefi_3; }
	inline void set__cairtefi_3(int32_t value)
	{
		____cairtefi_3 = value;
	}

	inline static int32_t get_offset_of__tirloDarvel_4() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____tirloDarvel_4)); }
	inline uint32_t get__tirloDarvel_4() const { return ____tirloDarvel_4; }
	inline uint32_t* get_address_of__tirloDarvel_4() { return &____tirloDarvel_4; }
	inline void set__tirloDarvel_4(uint32_t value)
	{
		____tirloDarvel_4 = value;
	}

	inline static int32_t get_offset_of__nelxesas_5() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____nelxesas_5)); }
	inline uint32_t get__nelxesas_5() const { return ____nelxesas_5; }
	inline uint32_t* get_address_of__nelxesas_5() { return &____nelxesas_5; }
	inline void set__nelxesas_5(uint32_t value)
	{
		____nelxesas_5 = value;
	}

	inline static int32_t get_offset_of__trafou_6() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____trafou_6)); }
	inline uint32_t get__trafou_6() const { return ____trafou_6; }
	inline uint32_t* get_address_of__trafou_6() { return &____trafou_6; }
	inline void set__trafou_6(uint32_t value)
	{
		____trafou_6 = value;
	}

	inline static int32_t get_offset_of__herbirmasCojeber_7() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____herbirmasCojeber_7)); }
	inline String_t* get__herbirmasCojeber_7() const { return ____herbirmasCojeber_7; }
	inline String_t** get_address_of__herbirmasCojeber_7() { return &____herbirmasCojeber_7; }
	inline void set__herbirmasCojeber_7(String_t* value)
	{
		____herbirmasCojeber_7 = value;
		Il2CppCodeGenWriteBarrier(&____herbirmasCojeber_7, value);
	}

	inline static int32_t get_offset_of__bemcedaw_8() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____bemcedaw_8)); }
	inline uint32_t get__bemcedaw_8() const { return ____bemcedaw_8; }
	inline uint32_t* get_address_of__bemcedaw_8() { return &____bemcedaw_8; }
	inline void set__bemcedaw_8(uint32_t value)
	{
		____bemcedaw_8 = value;
	}

	inline static int32_t get_offset_of__jelnairHawfu_9() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____jelnairHawfu_9)); }
	inline bool get__jelnairHawfu_9() const { return ____jelnairHawfu_9; }
	inline bool* get_address_of__jelnairHawfu_9() { return &____jelnairHawfu_9; }
	inline void set__jelnairHawfu_9(bool value)
	{
		____jelnairHawfu_9 = value;
	}

	inline static int32_t get_offset_of__dranonall_10() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____dranonall_10)); }
	inline float get__dranonall_10() const { return ____dranonall_10; }
	inline float* get_address_of__dranonall_10() { return &____dranonall_10; }
	inline void set__dranonall_10(float value)
	{
		____dranonall_10 = value;
	}

	inline static int32_t get_offset_of__dirmirpe_11() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____dirmirpe_11)); }
	inline bool get__dirmirpe_11() const { return ____dirmirpe_11; }
	inline bool* get_address_of__dirmirpe_11() { return &____dirmirpe_11; }
	inline void set__dirmirpe_11(bool value)
	{
		____dirmirpe_11 = value;
	}

	inline static int32_t get_offset_of__mebounorMastirju_12() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____mebounorMastirju_12)); }
	inline int32_t get__mebounorMastirju_12() const { return ____mebounorMastirju_12; }
	inline int32_t* get_address_of__mebounorMastirju_12() { return &____mebounorMastirju_12; }
	inline void set__mebounorMastirju_12(int32_t value)
	{
		____mebounorMastirju_12 = value;
	}

	inline static int32_t get_offset_of__loutrurCiralmas_13() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____loutrurCiralmas_13)); }
	inline float get__loutrurCiralmas_13() const { return ____loutrurCiralmas_13; }
	inline float* get_address_of__loutrurCiralmas_13() { return &____loutrurCiralmas_13; }
	inline void set__loutrurCiralmas_13(float value)
	{
		____loutrurCiralmas_13 = value;
	}

	inline static int32_t get_offset_of__wepa_14() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____wepa_14)); }
	inline bool get__wepa_14() const { return ____wepa_14; }
	inline bool* get_address_of__wepa_14() { return &____wepa_14; }
	inline void set__wepa_14(bool value)
	{
		____wepa_14 = value;
	}

	inline static int32_t get_offset_of__sawpairJawrowe_15() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____sawpairJawrowe_15)); }
	inline int32_t get__sawpairJawrowe_15() const { return ____sawpairJawrowe_15; }
	inline int32_t* get_address_of__sawpairJawrowe_15() { return &____sawpairJawrowe_15; }
	inline void set__sawpairJawrowe_15(int32_t value)
	{
		____sawpairJawrowe_15 = value;
	}

	inline static int32_t get_offset_of__mouhouboMaigear_16() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____mouhouboMaigear_16)); }
	inline uint32_t get__mouhouboMaigear_16() const { return ____mouhouboMaigear_16; }
	inline uint32_t* get_address_of__mouhouboMaigear_16() { return &____mouhouboMaigear_16; }
	inline void set__mouhouboMaigear_16(uint32_t value)
	{
		____mouhouboMaigear_16 = value;
	}

	inline static int32_t get_offset_of__lairleGearlowyow_17() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____lairleGearlowyow_17)); }
	inline float get__lairleGearlowyow_17() const { return ____lairleGearlowyow_17; }
	inline float* get_address_of__lairleGearlowyow_17() { return &____lairleGearlowyow_17; }
	inline void set__lairleGearlowyow_17(float value)
	{
		____lairleGearlowyow_17 = value;
	}

	inline static int32_t get_offset_of__weargercerTasou_18() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____weargercerTasou_18)); }
	inline bool get__weargercerTasou_18() const { return ____weargercerTasou_18; }
	inline bool* get_address_of__weargercerTasou_18() { return &____weargercerTasou_18; }
	inline void set__weargercerTasou_18(bool value)
	{
		____weargercerTasou_18 = value;
	}

	inline static int32_t get_offset_of__wujoolayGerpivai_19() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____wujoolayGerpivai_19)); }
	inline uint32_t get__wujoolayGerpivai_19() const { return ____wujoolayGerpivai_19; }
	inline uint32_t* get_address_of__wujoolayGerpivai_19() { return &____wujoolayGerpivai_19; }
	inline void set__wujoolayGerpivai_19(uint32_t value)
	{
		____wujoolayGerpivai_19 = value;
	}

	inline static int32_t get_offset_of__laipiwu_20() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____laipiwu_20)); }
	inline int32_t get__laipiwu_20() const { return ____laipiwu_20; }
	inline int32_t* get_address_of__laipiwu_20() { return &____laipiwu_20; }
	inline void set__laipiwu_20(int32_t value)
	{
		____laipiwu_20 = value;
	}

	inline static int32_t get_offset_of__ceenereser_21() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____ceenereser_21)); }
	inline String_t* get__ceenereser_21() const { return ____ceenereser_21; }
	inline String_t** get_address_of__ceenereser_21() { return &____ceenereser_21; }
	inline void set__ceenereser_21(String_t* value)
	{
		____ceenereser_21 = value;
		Il2CppCodeGenWriteBarrier(&____ceenereser_21, value);
	}

	inline static int32_t get_offset_of__beartoucor_22() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____beartoucor_22)); }
	inline int32_t get__beartoucor_22() const { return ____beartoucor_22; }
	inline int32_t* get_address_of__beartoucor_22() { return &____beartoucor_22; }
	inline void set__beartoucor_22(int32_t value)
	{
		____beartoucor_22 = value;
	}

	inline static int32_t get_offset_of__vempuvuKalmow_23() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____vempuvuKalmow_23)); }
	inline String_t* get__vempuvuKalmow_23() const { return ____vempuvuKalmow_23; }
	inline String_t** get_address_of__vempuvuKalmow_23() { return &____vempuvuKalmow_23; }
	inline void set__vempuvuKalmow_23(String_t* value)
	{
		____vempuvuKalmow_23 = value;
		Il2CppCodeGenWriteBarrier(&____vempuvuKalmow_23, value);
	}

	inline static int32_t get_offset_of__xewoukooHeejimay_24() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____xewoukooHeejimay_24)); }
	inline float get__xewoukooHeejimay_24() const { return ____xewoukooHeejimay_24; }
	inline float* get_address_of__xewoukooHeejimay_24() { return &____xewoukooHeejimay_24; }
	inline void set__xewoukooHeejimay_24(float value)
	{
		____xewoukooHeejimay_24 = value;
	}

	inline static int32_t get_offset_of__hurkal_25() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____hurkal_25)); }
	inline String_t* get__hurkal_25() const { return ____hurkal_25; }
	inline String_t** get_address_of__hurkal_25() { return &____hurkal_25; }
	inline void set__hurkal_25(String_t* value)
	{
		____hurkal_25 = value;
		Il2CppCodeGenWriteBarrier(&____hurkal_25, value);
	}

	inline static int32_t get_offset_of__mechel_26() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____mechel_26)); }
	inline String_t* get__mechel_26() const { return ____mechel_26; }
	inline String_t** get_address_of__mechel_26() { return &____mechel_26; }
	inline void set__mechel_26(String_t* value)
	{
		____mechel_26 = value;
		Il2CppCodeGenWriteBarrier(&____mechel_26, value);
	}

	inline static int32_t get_offset_of__boutra_27() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____boutra_27)); }
	inline int32_t get__boutra_27() const { return ____boutra_27; }
	inline int32_t* get_address_of__boutra_27() { return &____boutra_27; }
	inline void set__boutra_27(int32_t value)
	{
		____boutra_27 = value;
	}

	inline static int32_t get_offset_of__pijouvoLoyay_28() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____pijouvoLoyay_28)); }
	inline String_t* get__pijouvoLoyay_28() const { return ____pijouvoLoyay_28; }
	inline String_t** get_address_of__pijouvoLoyay_28() { return &____pijouvoLoyay_28; }
	inline void set__pijouvoLoyay_28(String_t* value)
	{
		____pijouvoLoyay_28 = value;
		Il2CppCodeGenWriteBarrier(&____pijouvoLoyay_28, value);
	}

	inline static int32_t get_offset_of__drirjenaiCochai_29() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____drirjenaiCochai_29)); }
	inline float get__drirjenaiCochai_29() const { return ____drirjenaiCochai_29; }
	inline float* get_address_of__drirjenaiCochai_29() { return &____drirjenaiCochai_29; }
	inline void set__drirjenaiCochai_29(float value)
	{
		____drirjenaiCochai_29 = value;
	}

	inline static int32_t get_offset_of__chuherege_30() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____chuherege_30)); }
	inline String_t* get__chuherege_30() const { return ____chuherege_30; }
	inline String_t** get_address_of__chuherege_30() { return &____chuherege_30; }
	inline void set__chuherege_30(String_t* value)
	{
		____chuherege_30 = value;
		Il2CppCodeGenWriteBarrier(&____chuherege_30, value);
	}

	inline static int32_t get_offset_of__ceereri_31() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____ceereri_31)); }
	inline int32_t get__ceereri_31() const { return ____ceereri_31; }
	inline int32_t* get_address_of__ceereri_31() { return &____ceereri_31; }
	inline void set__ceereri_31(int32_t value)
	{
		____ceereri_31 = value;
	}

	inline static int32_t get_offset_of__whayaifaKayte_32() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____whayaifaKayte_32)); }
	inline String_t* get__whayaifaKayte_32() const { return ____whayaifaKayte_32; }
	inline String_t** get_address_of__whayaifaKayte_32() { return &____whayaifaKayte_32; }
	inline void set__whayaifaKayte_32(String_t* value)
	{
		____whayaifaKayte_32 = value;
		Il2CppCodeGenWriteBarrier(&____whayaifaKayte_32, value);
	}

	inline static int32_t get_offset_of__raperWherjosor_33() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____raperWherjosor_33)); }
	inline float get__raperWherjosor_33() const { return ____raperWherjosor_33; }
	inline float* get_address_of__raperWherjosor_33() { return &____raperWherjosor_33; }
	inline void set__raperWherjosor_33(float value)
	{
		____raperWherjosor_33 = value;
	}

	inline static int32_t get_offset_of__reltawdou_34() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____reltawdou_34)); }
	inline String_t* get__reltawdou_34() const { return ____reltawdou_34; }
	inline String_t** get_address_of__reltawdou_34() { return &____reltawdou_34; }
	inline void set__reltawdou_34(String_t* value)
	{
		____reltawdou_34 = value;
		Il2CppCodeGenWriteBarrier(&____reltawdou_34, value);
	}

	inline static int32_t get_offset_of__selfearfallGearso_35() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____selfearfallGearso_35)); }
	inline String_t* get__selfearfallGearso_35() const { return ____selfearfallGearso_35; }
	inline String_t** get_address_of__selfearfallGearso_35() { return &____selfearfallGearso_35; }
	inline void set__selfearfallGearso_35(String_t* value)
	{
		____selfearfallGearso_35 = value;
		Il2CppCodeGenWriteBarrier(&____selfearfallGearso_35, value);
	}

	inline static int32_t get_offset_of__zesaiCiror_36() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____zesaiCiror_36)); }
	inline String_t* get__zesaiCiror_36() const { return ____zesaiCiror_36; }
	inline String_t** get_address_of__zesaiCiror_36() { return &____zesaiCiror_36; }
	inline void set__zesaiCiror_36(String_t* value)
	{
		____zesaiCiror_36 = value;
		Il2CppCodeGenWriteBarrier(&____zesaiCiror_36, value);
	}

	inline static int32_t get_offset_of__sootiteaLorcerrair_37() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____sootiteaLorcerrair_37)); }
	inline uint32_t get__sootiteaLorcerrair_37() const { return ____sootiteaLorcerrair_37; }
	inline uint32_t* get_address_of__sootiteaLorcerrair_37() { return &____sootiteaLorcerrair_37; }
	inline void set__sootiteaLorcerrair_37(uint32_t value)
	{
		____sootiteaLorcerrair_37 = value;
	}

	inline static int32_t get_offset_of__nalwishas_38() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____nalwishas_38)); }
	inline bool get__nalwishas_38() const { return ____nalwishas_38; }
	inline bool* get_address_of__nalwishas_38() { return &____nalwishas_38; }
	inline void set__nalwishas_38(bool value)
	{
		____nalwishas_38 = value;
	}

	inline static int32_t get_offset_of__whisxaParse_39() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____whisxaParse_39)); }
	inline uint32_t get__whisxaParse_39() const { return ____whisxaParse_39; }
	inline uint32_t* get_address_of__whisxaParse_39() { return &____whisxaParse_39; }
	inline void set__whisxaParse_39(uint32_t value)
	{
		____whisxaParse_39 = value;
	}

	inline static int32_t get_offset_of__trayboofaZalpai_40() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____trayboofaZalpai_40)); }
	inline float get__trayboofaZalpai_40() const { return ____trayboofaZalpai_40; }
	inline float* get_address_of__trayboofaZalpai_40() { return &____trayboofaZalpai_40; }
	inline void set__trayboofaZalpai_40(float value)
	{
		____trayboofaZalpai_40 = value;
	}

	inline static int32_t get_offset_of__sowkea_41() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____sowkea_41)); }
	inline float get__sowkea_41() const { return ____sowkea_41; }
	inline float* get_address_of__sowkea_41() { return &____sowkea_41; }
	inline void set__sowkea_41(float value)
	{
		____sowkea_41 = value;
	}

	inline static int32_t get_offset_of__jurcel_42() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____jurcel_42)); }
	inline float get__jurcel_42() const { return ____jurcel_42; }
	inline float* get_address_of__jurcel_42() { return &____jurcel_42; }
	inline void set__jurcel_42(float value)
	{
		____jurcel_42 = value;
	}

	inline static int32_t get_offset_of__musalColeltal_43() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____musalColeltal_43)); }
	inline int32_t get__musalColeltal_43() const { return ____musalColeltal_43; }
	inline int32_t* get_address_of__musalColeltal_43() { return &____musalColeltal_43; }
	inline void set__musalColeltal_43(int32_t value)
	{
		____musalColeltal_43 = value;
	}

	inline static int32_t get_offset_of__toupereFixo_44() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____toupereFixo_44)); }
	inline String_t* get__toupereFixo_44() const { return ____toupereFixo_44; }
	inline String_t** get_address_of__toupereFixo_44() { return &____toupereFixo_44; }
	inline void set__toupereFixo_44(String_t* value)
	{
		____toupereFixo_44 = value;
		Il2CppCodeGenWriteBarrier(&____toupereFixo_44, value);
	}

	inline static int32_t get_offset_of__jastasLallsearti_45() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____jastasLallsearti_45)); }
	inline float get__jastasLallsearti_45() const { return ____jastasLallsearti_45; }
	inline float* get_address_of__jastasLallsearti_45() { return &____jastasLallsearti_45; }
	inline void set__jastasLallsearti_45(float value)
	{
		____jastasLallsearti_45 = value;
	}

	inline static int32_t get_offset_of__jouriCorsou_46() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____jouriCorsou_46)); }
	inline uint32_t get__jouriCorsou_46() const { return ____jouriCorsou_46; }
	inline uint32_t* get_address_of__jouriCorsou_46() { return &____jouriCorsou_46; }
	inline void set__jouriCorsou_46(uint32_t value)
	{
		____jouriCorsou_46 = value;
	}

	inline static int32_t get_offset_of__labawBorleatou_47() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____labawBorleatou_47)); }
	inline bool get__labawBorleatou_47() const { return ____labawBorleatou_47; }
	inline bool* get_address_of__labawBorleatou_47() { return &____labawBorleatou_47; }
	inline void set__labawBorleatou_47(bool value)
	{
		____labawBorleatou_47 = value;
	}

	inline static int32_t get_offset_of__drircaselBaltreya_48() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____drircaselBaltreya_48)); }
	inline uint32_t get__drircaselBaltreya_48() const { return ____drircaselBaltreya_48; }
	inline uint32_t* get_address_of__drircaselBaltreya_48() { return &____drircaselBaltreya_48; }
	inline void set__drircaselBaltreya_48(uint32_t value)
	{
		____drircaselBaltreya_48 = value;
	}

	inline static int32_t get_offset_of__jasramoSatar_49() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____jasramoSatar_49)); }
	inline float get__jasramoSatar_49() const { return ____jasramoSatar_49; }
	inline float* get_address_of__jasramoSatar_49() { return &____jasramoSatar_49; }
	inline void set__jasramoSatar_49(float value)
	{
		____jasramoSatar_49 = value;
	}

	inline static int32_t get_offset_of__wircas_50() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____wircas_50)); }
	inline String_t* get__wircas_50() const { return ____wircas_50; }
	inline String_t** get_address_of__wircas_50() { return &____wircas_50; }
	inline void set__wircas_50(String_t* value)
	{
		____wircas_50 = value;
		Il2CppCodeGenWriteBarrier(&____wircas_50, value);
	}

	inline static int32_t get_offset_of__woowouTaljasxor_51() { return static_cast<int32_t>(offsetof(M_satuSojarball215_t3899592855, ____woowouTaljasxor_51)); }
	inline bool get__woowouTaljasxor_51() const { return ____woowouTaljasxor_51; }
	inline bool* get_address_of__woowouTaljasxor_51() { return &____woowouTaljasxor_51; }
	inline void set__woowouTaljasxor_51(bool value)
	{
		____woowouTaljasxor_51 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
