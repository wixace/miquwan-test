﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Voxels.LinkedVoxelSpan
struct LinkedVoxelSpan_t3201576397;
struct LinkedVoxelSpan_t3201576397_marshaled_pinvoke;
struct LinkedVoxelSpan_t3201576397_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_LinkedVoxelSp3201576397.h"

// System.Void Pathfinding.Voxels.LinkedVoxelSpan::.ctor(System.UInt32,System.UInt32,System.Int32)
extern "C"  void LinkedVoxelSpan__ctor_m2229477988 (LinkedVoxelSpan_t3201576397 * __this, uint32_t ___bottom0, uint32_t ___top1, int32_t ___area2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.LinkedVoxelSpan::.ctor(System.UInt32,System.UInt32,System.Int32,System.Int32)
extern "C"  void LinkedVoxelSpan__ctor_m1653210355 (LinkedVoxelSpan_t3201576397 * __this, uint32_t ___bottom0, uint32_t ___top1, int32_t ___area2, int32_t ___next3, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct LinkedVoxelSpan_t3201576397;
struct LinkedVoxelSpan_t3201576397_marshaled_pinvoke;

extern "C" void LinkedVoxelSpan_t3201576397_marshal_pinvoke(const LinkedVoxelSpan_t3201576397& unmarshaled, LinkedVoxelSpan_t3201576397_marshaled_pinvoke& marshaled);
extern "C" void LinkedVoxelSpan_t3201576397_marshal_pinvoke_back(const LinkedVoxelSpan_t3201576397_marshaled_pinvoke& marshaled, LinkedVoxelSpan_t3201576397& unmarshaled);
extern "C" void LinkedVoxelSpan_t3201576397_marshal_pinvoke_cleanup(LinkedVoxelSpan_t3201576397_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct LinkedVoxelSpan_t3201576397;
struct LinkedVoxelSpan_t3201576397_marshaled_com;

extern "C" void LinkedVoxelSpan_t3201576397_marshal_com(const LinkedVoxelSpan_t3201576397& unmarshaled, LinkedVoxelSpan_t3201576397_marshaled_com& marshaled);
extern "C" void LinkedVoxelSpan_t3201576397_marshal_com_back(const LinkedVoxelSpan_t3201576397_marshaled_com& marshaled, LinkedVoxelSpan_t3201576397& unmarshaled);
extern "C" void LinkedVoxelSpan_t3201576397_marshal_com_cleanup(LinkedVoxelSpan_t3201576397_marshaled_com& marshaled);
