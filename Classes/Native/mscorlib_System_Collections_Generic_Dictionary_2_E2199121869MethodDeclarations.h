﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1190435706MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TextStringCfg>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m4254760888(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2199121869 *, Dictionary_2_t881798477 *, const MethodInfo*))Enumerator__ctor_m2377115088_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TextStringCfg>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1532689715(__this, method) ((  Il2CppObject * (*) (Enumerator_t2199121869 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1037642267_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TextStringCfg>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m859287037(__this, method) ((  void (*) (Enumerator_t2199121869 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2809374949_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TextStringCfg>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3951060468(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2199121869 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2434214620_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TextStringCfg>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3486926799(__this, method) ((  Il2CppObject * (*) (Enumerator_t2199121869 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3735627447_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TextStringCfg>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1910599329(__this, method) ((  Il2CppObject * (*) (Enumerator_t2199121869 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m393753481_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TextStringCfg>::MoveNext()
#define Enumerator_MoveNext_m340930861(__this, method) ((  bool (*) (Enumerator_t2199121869 *, const MethodInfo*))Enumerator_MoveNext_m1213995029_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TextStringCfg>::get_Current()
#define Enumerator_get_Current_m2267176047(__this, method) ((  KeyValuePair_2_t780579183  (*) (Enumerator_t2199121869 *, const MethodInfo*))Enumerator_get_Current_m1399860359_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TextStringCfg>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1445806582(__this, method) ((  int32_t (*) (Enumerator_t2199121869 *, const MethodInfo*))Enumerator_get_CurrentKey_m1767398110_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TextStringCfg>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3573033654(__this, method) ((  TextStringCfg_t884535238 * (*) (Enumerator_t2199121869 *, const MethodInfo*))Enumerator_get_CurrentValue_m3384846750_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TextStringCfg>::Reset()
#define Enumerator_Reset_m4056876554(__this, method) ((  void (*) (Enumerator_t2199121869 *, const MethodInfo*))Enumerator_Reset_m1080084514_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TextStringCfg>::VerifyState()
#define Enumerator_VerifyState_m3271829139(__this, method) ((  void (*) (Enumerator_t2199121869 *, const MethodInfo*))Enumerator_VerifyState_m2404513451_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TextStringCfg>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3056613691(__this, method) ((  void (*) (Enumerator_t2199121869 *, const MethodInfo*))Enumerator_VerifyCurrent_m2789892947_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TextStringCfg>::Dispose()
#define Enumerator_Dispose_m1351492698(__this, method) ((  void (*) (Enumerator_t2199121869 *, const MethodInfo*))Enumerator_Dispose_m1102561394_gshared)(__this, method)
