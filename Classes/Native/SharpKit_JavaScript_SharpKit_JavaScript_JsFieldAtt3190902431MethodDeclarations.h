﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SharpKit.JavaScript.JsFieldAttribute
struct JsFieldAttribute_t3190902431;

#include "codegen/il2cpp-codegen.h"

// System.Void SharpKit.JavaScript.JsFieldAttribute::set_Export(System.Boolean)
extern "C"  void JsFieldAttribute_set_Export_m3144860058 (JsFieldAttribute_t3190902431 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsFieldAttribute::.ctor()
extern "C"  void JsFieldAttribute__ctor_m1798584018 (JsFieldAttribute_t3190902431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
