﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BetterList`1/<GetEnumerator>c__Iterator29<System.Single>
struct U3CGetEnumeratorU3Ec__Iterator29_t1743915433;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BetterList`1/<GetEnumerator>c__Iterator29<System.Single>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29__ctor_m456393072_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1743915433 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29__ctor_m456393072(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t1743915433 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29__ctor_m456393072_gshared)(__this, method)
// T BetterList`1/<GetEnumerator>c__Iterator29<System.Single>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  float U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1547312729_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1743915433 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1547312729(__this, method) ((  float (*) (U3CGetEnumeratorU3Ec__Iterator29_t1743915433 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1547312729_gshared)(__this, method)
// System.Object BetterList`1/<GetEnumerator>c__Iterator29<System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m4220594944_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1743915433 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m4220594944(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator29_t1743915433 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m4220594944_gshared)(__this, method)
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator29<System.Single>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m2044187244_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1743915433 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m2044187244(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator29_t1743915433 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m2044187244_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator29<System.Single>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29_Dispose_m405361069_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1743915433 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_Dispose_m405361069(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t1743915433 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_Dispose_m405361069_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator29<System.Single>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29_Reset_m2397793309_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1743915433 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_Reset_m2397793309(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t1743915433 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_Reset_m2397793309_gshared)(__this, method)
