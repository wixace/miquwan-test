﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._6eea3fb2dfdc9ac8a56559e34cfe9565
struct _6eea3fb2dfdc9ac8a56559e34cfe9565_t3406393299;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__6eea3fb2dfdc9ac8a56559e33406393299.h"

// System.Void Little._6eea3fb2dfdc9ac8a56559e34cfe9565::.ctor()
extern "C"  void _6eea3fb2dfdc9ac8a56559e34cfe9565__ctor_m2070860730 (_6eea3fb2dfdc9ac8a56559e34cfe9565_t3406393299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6eea3fb2dfdc9ac8a56559e34cfe9565::_6eea3fb2dfdc9ac8a56559e34cfe9565m2(System.Int32)
extern "C"  int32_t _6eea3fb2dfdc9ac8a56559e34cfe9565__6eea3fb2dfdc9ac8a56559e34cfe9565m2_m3684296889 (_6eea3fb2dfdc9ac8a56559e34cfe9565_t3406393299 * __this, int32_t ____6eea3fb2dfdc9ac8a56559e34cfe9565a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6eea3fb2dfdc9ac8a56559e34cfe9565::_6eea3fb2dfdc9ac8a56559e34cfe9565m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _6eea3fb2dfdc9ac8a56559e34cfe9565__6eea3fb2dfdc9ac8a56559e34cfe9565m_m3219456541 (_6eea3fb2dfdc9ac8a56559e34cfe9565_t3406393299 * __this, int32_t ____6eea3fb2dfdc9ac8a56559e34cfe9565a0, int32_t ____6eea3fb2dfdc9ac8a56559e34cfe9565801, int32_t ____6eea3fb2dfdc9ac8a56559e34cfe9565c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6eea3fb2dfdc9ac8a56559e34cfe9565::ilo__6eea3fb2dfdc9ac8a56559e34cfe9565m21(Little._6eea3fb2dfdc9ac8a56559e34cfe9565,System.Int32)
extern "C"  int32_t _6eea3fb2dfdc9ac8a56559e34cfe9565_ilo__6eea3fb2dfdc9ac8a56559e34cfe9565m21_m3558410490 (Il2CppObject * __this /* static, unused */, _6eea3fb2dfdc9ac8a56559e34cfe9565_t3406393299 * ____this0, int32_t ____6eea3fb2dfdc9ac8a56559e34cfe9565a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
