﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._1b21e246f60b39dea87c8e576fb2beee
struct _1b21e246f60b39dea87c8e576fb2beee_t2492856054;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._1b21e246f60b39dea87c8e576fb2beee::.ctor()
extern "C"  void _1b21e246f60b39dea87c8e576fb2beee__ctor_m3491013623 (_1b21e246f60b39dea87c8e576fb2beee_t2492856054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1b21e246f60b39dea87c8e576fb2beee::_1b21e246f60b39dea87c8e576fb2beeem2(System.Int32)
extern "C"  int32_t _1b21e246f60b39dea87c8e576fb2beee__1b21e246f60b39dea87c8e576fb2beeem2_m694877657 (_1b21e246f60b39dea87c8e576fb2beee_t2492856054 * __this, int32_t ____1b21e246f60b39dea87c8e576fb2beeea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1b21e246f60b39dea87c8e576fb2beee::_1b21e246f60b39dea87c8e576fb2beeem(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _1b21e246f60b39dea87c8e576fb2beee__1b21e246f60b39dea87c8e576fb2beeem_m4264554749 (_1b21e246f60b39dea87c8e576fb2beee_t2492856054 * __this, int32_t ____1b21e246f60b39dea87c8e576fb2beeea0, int32_t ____1b21e246f60b39dea87c8e576fb2beee831, int32_t ____1b21e246f60b39dea87c8e576fb2beeec2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
