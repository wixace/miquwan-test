﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.LzmaBench/CProgressInfo
struct CProgressInfo_t3639622563;

#include "codegen/il2cpp-codegen.h"

// System.Void SevenZip.LzmaBench/CProgressInfo::.ctor()
extern "C"  void CProgressInfo__ctor_m3895140952 (CProgressInfo_t3639622563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaBench/CProgressInfo::Init()
extern "C"  void CProgressInfo_Init_m3263774972 (CProgressInfo_t3639622563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaBench/CProgressInfo::SetProgress(System.Int64,System.Int64)
extern "C"  void CProgressInfo_SetProgress_m2280969153 (CProgressInfo_t3639622563 * __this, int64_t ___inSize0, int64_t ___outSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaBench/CProgressInfo::SetProgressPercent(System.Int64,System.Int64)
extern "C"  void CProgressInfo_SetProgressPercent_m574998340 (CProgressInfo_t3639622563 * __this, int64_t ___fileSize0, int64_t ___processSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
