﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSCMapPathPointInfoConfig
struct  JSCMapPathPointInfoConfig_t2703681049  : public Il2CppObject
{
public:
	// System.Int32 JSCMapPathPointInfoConfig::_id
	int32_t ____id_0;
	// System.Single JSCMapPathPointInfoConfig::_x
	float ____x_1;
	// System.Single JSCMapPathPointInfoConfig::_y
	float ____y_2;
	// System.Single JSCMapPathPointInfoConfig::_z
	float ____z_3;
	// System.Single JSCMapPathPointInfoConfig::_angle
	float ____angle_4;
	// ProtoBuf.IExtension JSCMapPathPointInfoConfig::extensionObject
	Il2CppObject * ___extensionObject_5;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(JSCMapPathPointInfoConfig_t2703681049, ____id_0)); }
	inline int32_t get__id_0() const { return ____id_0; }
	inline int32_t* get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(int32_t value)
	{
		____id_0 = value;
	}

	inline static int32_t get_offset_of__x_1() { return static_cast<int32_t>(offsetof(JSCMapPathPointInfoConfig_t2703681049, ____x_1)); }
	inline float get__x_1() const { return ____x_1; }
	inline float* get_address_of__x_1() { return &____x_1; }
	inline void set__x_1(float value)
	{
		____x_1 = value;
	}

	inline static int32_t get_offset_of__y_2() { return static_cast<int32_t>(offsetof(JSCMapPathPointInfoConfig_t2703681049, ____y_2)); }
	inline float get__y_2() const { return ____y_2; }
	inline float* get_address_of__y_2() { return &____y_2; }
	inline void set__y_2(float value)
	{
		____y_2 = value;
	}

	inline static int32_t get_offset_of__z_3() { return static_cast<int32_t>(offsetof(JSCMapPathPointInfoConfig_t2703681049, ____z_3)); }
	inline float get__z_3() const { return ____z_3; }
	inline float* get_address_of__z_3() { return &____z_3; }
	inline void set__z_3(float value)
	{
		____z_3 = value;
	}

	inline static int32_t get_offset_of__angle_4() { return static_cast<int32_t>(offsetof(JSCMapPathPointInfoConfig_t2703681049, ____angle_4)); }
	inline float get__angle_4() const { return ____angle_4; }
	inline float* get_address_of__angle_4() { return &____angle_4; }
	inline void set__angle_4(float value)
	{
		____angle_4 = value;
	}

	inline static int32_t get_offset_of_extensionObject_5() { return static_cast<int32_t>(offsetof(JSCMapPathPointInfoConfig_t2703681049, ___extensionObject_5)); }
	inline Il2CppObject * get_extensionObject_5() const { return ___extensionObject_5; }
	inline Il2CppObject ** get_address_of_extensionObject_5() { return &___extensionObject_5; }
	inline void set_extensionObject_5(Il2CppObject * value)
	{
		___extensionObject_5 = value;
		Il2CppCodeGenWriteBarrier(&___extensionObject_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
