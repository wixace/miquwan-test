﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.TextWriter
struct TextWriter_t2304124208;
// Newtonsoft.Json.Utilities.Base64Encoder
struct Base64Encoder_t2146525835;

#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonTextWriter
struct  JsonTextWriter_t2814114432  : public JsonWriter_t972330355
{
public:
	// System.IO.TextWriter Newtonsoft.Json.JsonTextWriter::_writer
	TextWriter_t2304124208 * ____writer_6;
	// Newtonsoft.Json.Utilities.Base64Encoder Newtonsoft.Json.JsonTextWriter::_base64Encoder
	Base64Encoder_t2146525835 * ____base64Encoder_7;
	// System.Char Newtonsoft.Json.JsonTextWriter::_indentChar
	Il2CppChar ____indentChar_8;
	// System.Int32 Newtonsoft.Json.JsonTextWriter::_indentation
	int32_t ____indentation_9;
	// System.Char Newtonsoft.Json.JsonTextWriter::_quoteChar
	Il2CppChar ____quoteChar_10;
	// System.Boolean Newtonsoft.Json.JsonTextWriter::_quoteName
	bool ____quoteName_11;

public:
	inline static int32_t get_offset_of__writer_6() { return static_cast<int32_t>(offsetof(JsonTextWriter_t2814114432, ____writer_6)); }
	inline TextWriter_t2304124208 * get__writer_6() const { return ____writer_6; }
	inline TextWriter_t2304124208 ** get_address_of__writer_6() { return &____writer_6; }
	inline void set__writer_6(TextWriter_t2304124208 * value)
	{
		____writer_6 = value;
		Il2CppCodeGenWriteBarrier(&____writer_6, value);
	}

	inline static int32_t get_offset_of__base64Encoder_7() { return static_cast<int32_t>(offsetof(JsonTextWriter_t2814114432, ____base64Encoder_7)); }
	inline Base64Encoder_t2146525835 * get__base64Encoder_7() const { return ____base64Encoder_7; }
	inline Base64Encoder_t2146525835 ** get_address_of__base64Encoder_7() { return &____base64Encoder_7; }
	inline void set__base64Encoder_7(Base64Encoder_t2146525835 * value)
	{
		____base64Encoder_7 = value;
		Il2CppCodeGenWriteBarrier(&____base64Encoder_7, value);
	}

	inline static int32_t get_offset_of__indentChar_8() { return static_cast<int32_t>(offsetof(JsonTextWriter_t2814114432, ____indentChar_8)); }
	inline Il2CppChar get__indentChar_8() const { return ____indentChar_8; }
	inline Il2CppChar* get_address_of__indentChar_8() { return &____indentChar_8; }
	inline void set__indentChar_8(Il2CppChar value)
	{
		____indentChar_8 = value;
	}

	inline static int32_t get_offset_of__indentation_9() { return static_cast<int32_t>(offsetof(JsonTextWriter_t2814114432, ____indentation_9)); }
	inline int32_t get__indentation_9() const { return ____indentation_9; }
	inline int32_t* get_address_of__indentation_9() { return &____indentation_9; }
	inline void set__indentation_9(int32_t value)
	{
		____indentation_9 = value;
	}

	inline static int32_t get_offset_of__quoteChar_10() { return static_cast<int32_t>(offsetof(JsonTextWriter_t2814114432, ____quoteChar_10)); }
	inline Il2CppChar get__quoteChar_10() const { return ____quoteChar_10; }
	inline Il2CppChar* get_address_of__quoteChar_10() { return &____quoteChar_10; }
	inline void set__quoteChar_10(Il2CppChar value)
	{
		____quoteChar_10 = value;
	}

	inline static int32_t get_offset_of__quoteName_11() { return static_cast<int32_t>(offsetof(JsonTextWriter_t2814114432, ____quoteName_11)); }
	inline bool get__quoteName_11() const { return ____quoteName_11; }
	inline bool* get_address_of__quoteName_11() { return &____quoteName_11; }
	inline void set__quoteName_11(bool value)
	{
		____quoteName_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
