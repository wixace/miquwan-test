﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Single,System.Single>
struct U3CDelayCallExecU3Ec__Iterator40_2_t2421769420;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Single,System.Single>::.ctor()
extern "C"  void U3CDelayCallExecU3Ec__Iterator40_2__ctor_m2859982554_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t2421769420 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2__ctor_m2859982554(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator40_2_t2421769420 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2__ctor_m2859982554_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Single,System.Single>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4004655864_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t2421769420 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4004655864(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator40_2_t2421769420 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4004655864_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Single,System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_IEnumerator_get_Current_m3319342732_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t2421769420 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_IEnumerator_get_Current_m3319342732(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator40_2_t2421769420 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_IEnumerator_get_Current_m3319342732_gshared)(__this, method)
// System.Boolean BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Single,System.Single>::MoveNext()
extern "C"  bool U3CDelayCallExecU3Ec__Iterator40_2_MoveNext_m3869825498_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t2421769420 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2_MoveNext_m3869825498(__this, method) ((  bool (*) (U3CDelayCallExecU3Ec__Iterator40_2_t2421769420 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2_MoveNext_m3869825498_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Single,System.Single>::Dispose()
extern "C"  void U3CDelayCallExecU3Ec__Iterator40_2_Dispose_m3857415319_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t2421769420 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2_Dispose_m3857415319(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator40_2_t2421769420 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2_Dispose_m3857415319_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Single,System.Single>::Reset()
extern "C"  void U3CDelayCallExecU3Ec__Iterator40_2_Reset_m506415495_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t2421769420 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2_Reset_m506415495(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator40_2_t2421769420 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2_Reset_m506415495_gshared)(__this, method)
