﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AllUpdate
struct AllUpdate_t1625637290;
// IZUpdate
struct IZUpdate_t3482043738;
// System.Object
struct Il2CppObject;
// ICsObj
struct ICsObj_t2155308958;
// CEvent.ZEvent
struct ZEvent_t3638018500;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_AllUpdate1625637290.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"

// System.Void AllUpdate::.ctor()
extern "C"  void AllUpdate__ctor_m743707393 (AllUpdate_t1625637290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllUpdate::.cctor()
extern "C"  void AllUpdate__cctor_m1097996492 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllUpdate::AddUpdate(IZUpdate)
extern "C"  void AllUpdate_AddUpdate_m4230559055 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllUpdate::RemoveUpdate(IZUpdate)
extern "C"  void AllUpdate_RemoveUpdate_m117638710 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllUpdate::AddUpdate(System.Object)
extern "C"  void AllUpdate_AddUpdate_m2004305099 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllUpdate::RemoveUpdate(System.Object)
extern "C"  void AllUpdate_RemoveUpdate_m3329158212 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AllUpdate AllUpdate::get_allUpdate()
extern "C"  AllUpdate_t1625637290 * AllUpdate_get_allUpdate_m2962161353 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllUpdate::RemoveUpdate()
extern "C"  void AllUpdate_RemoveUpdate_m1792030544 (AllUpdate_t1625637290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllUpdate::Update()
extern "C"  void AllUpdate_Update_m3306954796 (AllUpdate_t1625637290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllUpdate::OnApplicationFocus(System.Boolean)
extern "C"  void AllUpdate_OnApplicationFocus_m3619104321 (AllUpdate_t1625637290 * __this, bool ___isFocus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AllUpdate AllUpdate::ilo_get_allUpdate1()
extern "C"  AllUpdate_t1625637290 * AllUpdate_ilo_get_allUpdate1_m201813981 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AllUpdate::ilo_get_jsobjID2(ICsObj)
extern "C"  int32_t AllUpdate_ilo_get_jsobjID2_m1250044668 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllUpdate::ilo_RemoveUpdate3(IZUpdate)
extern "C"  void AllUpdate_ilo_RemoveUpdate3_m912174936 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllUpdate::ilo_AddUpdate4(IZUpdate)
extern "C"  void AllUpdate_ilo_AddUpdate4_m1599029862 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllUpdate::ilo_RemoveUpdate5(AllUpdate)
extern "C"  void AllUpdate_ilo_RemoveUpdate5_m1156423252 (Il2CppObject * __this /* static, unused */, AllUpdate_t1625637290 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllUpdate::ilo_DispatchEvent6(CEvent.ZEvent)
extern "C"  void AllUpdate_ilo_DispatchEvent6_m2136272047 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
