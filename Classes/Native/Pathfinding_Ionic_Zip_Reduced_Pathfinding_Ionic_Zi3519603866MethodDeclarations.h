﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Ionic.Zip.ReadProgressEventArgs
struct ReadProgressEventArgs_t3519603866;
// System.String
struct String_t;
// Pathfinding.Ionic.Zip.ZipEntry
struct ZipEntry_t2786874973;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "Pathfinding_Ionic_Zip_Reduced_Pathfinding_Ionic_Zi3652703282.h"
#include "Pathfinding_Ionic_Zip_Reduced_Pathfinding_Ionic_Zi2786874973.h"

// System.Void Pathfinding.Ionic.Zip.ReadProgressEventArgs::.ctor(System.String,Pathfinding.Ionic.Zip.ZipProgressEventType)
extern "C"  void ReadProgressEventArgs__ctor_m4251616872 (ReadProgressEventArgs_t3519603866 * __this, String_t* ___archiveName0, int32_t ___flavor1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Ionic.Zip.ReadProgressEventArgs Pathfinding.Ionic.Zip.ReadProgressEventArgs::Before(System.String,System.Int32)
extern "C"  ReadProgressEventArgs_t3519603866 * ReadProgressEventArgs_Before_m1248670121 (Il2CppObject * __this /* static, unused */, String_t* ___archiveName0, int32_t ___entriesTotal1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Ionic.Zip.ReadProgressEventArgs Pathfinding.Ionic.Zip.ReadProgressEventArgs::After(System.String,Pathfinding.Ionic.Zip.ZipEntry,System.Int32)
extern "C"  ReadProgressEventArgs_t3519603866 * ReadProgressEventArgs_After_m616262558 (Il2CppObject * __this /* static, unused */, String_t* ___archiveName0, ZipEntry_t2786874973 * ___entry1, int32_t ___entriesTotal2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Ionic.Zip.ReadProgressEventArgs Pathfinding.Ionic.Zip.ReadProgressEventArgs::Started(System.String)
extern "C"  ReadProgressEventArgs_t3519603866 * ReadProgressEventArgs_Started_m2247098068 (Il2CppObject * __this /* static, unused */, String_t* ___archiveName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Ionic.Zip.ReadProgressEventArgs Pathfinding.Ionic.Zip.ReadProgressEventArgs::ByteUpdate(System.String,Pathfinding.Ionic.Zip.ZipEntry,System.Int64,System.Int64)
extern "C"  ReadProgressEventArgs_t3519603866 * ReadProgressEventArgs_ByteUpdate_m3735899350 (Il2CppObject * __this /* static, unused */, String_t* ___archiveName0, ZipEntry_t2786874973 * ___entry1, int64_t ___bytesXferred2, int64_t ___totalBytes3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Ionic.Zip.ReadProgressEventArgs Pathfinding.Ionic.Zip.ReadProgressEventArgs::Completed(System.String)
extern "C"  ReadProgressEventArgs_t3519603866 * ReadProgressEventArgs_Completed_m1439602378 (Il2CppObject * __this /* static, unused */, String_t* ___archiveName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
