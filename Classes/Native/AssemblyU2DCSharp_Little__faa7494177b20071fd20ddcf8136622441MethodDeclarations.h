﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._faa7494177b20071fd20ddcf89ba8b7a
struct _faa7494177b20071fd20ddcf89ba8b7a_t136622441;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._faa7494177b20071fd20ddcf89ba8b7a::.ctor()
extern "C"  void _faa7494177b20071fd20ddcf89ba8b7a__ctor_m2507471588 (_faa7494177b20071fd20ddcf89ba8b7a_t136622441 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._faa7494177b20071fd20ddcf89ba8b7a::_faa7494177b20071fd20ddcf89ba8b7am2(System.Int32)
extern "C"  int32_t _faa7494177b20071fd20ddcf89ba8b7a__faa7494177b20071fd20ddcf89ba8b7am2_m2931609849 (_faa7494177b20071fd20ddcf89ba8b7a_t136622441 * __this, int32_t ____faa7494177b20071fd20ddcf89ba8b7aa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._faa7494177b20071fd20ddcf89ba8b7a::_faa7494177b20071fd20ddcf89ba8b7am(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _faa7494177b20071fd20ddcf89ba8b7a__faa7494177b20071fd20ddcf89ba8b7am_m3232489949 (_faa7494177b20071fd20ddcf89ba8b7a_t136622441 * __this, int32_t ____faa7494177b20071fd20ddcf89ba8b7aa0, int32_t ____faa7494177b20071fd20ddcf89ba8b7a281, int32_t ____faa7494177b20071fd20ddcf89ba8b7ac2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
