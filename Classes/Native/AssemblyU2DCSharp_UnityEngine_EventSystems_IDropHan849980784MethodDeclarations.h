﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_IDropHandlerGenerated
struct UnityEngine_EventSystems_IDropHandlerGenerated_t849980784;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_EventSystems_IDropHandlerGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_IDropHandlerGenerated__ctor_m1363795243 (UnityEngine_EventSystems_IDropHandlerGenerated_t849980784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_IDropHandlerGenerated::IDropHandler_OnDrop__PointerEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_IDropHandlerGenerated_IDropHandler_OnDrop__PointerEventData_m1725817287 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_IDropHandlerGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_IDropHandlerGenerated___Register_m3797557884 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
