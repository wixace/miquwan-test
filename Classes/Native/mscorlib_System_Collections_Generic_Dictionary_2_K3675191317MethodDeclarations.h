﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,System.Object>
struct KeyCollection_t3675191317;
// System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>
struct Dictionary_2_t2048431866;
// System.Collections.Generic.IEnumerator`1<SoundTypeID>
struct IEnumerator_1_t1243514493;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// SoundTypeID[]
struct SoundTypeIDU5BU5D_t1111200077;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SoundTypeID3626616740.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2663367920.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m1559252174_gshared (KeyCollection_t3675191317 * __this, Dictionary_2_t2048431866 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m1559252174(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3675191317 *, Dictionary_2_t2048431866 *, const MethodInfo*))KeyCollection__ctor_m1559252174_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4000302024_gshared (KeyCollection_t3675191317 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4000302024(__this, ___item0, method) ((  void (*) (KeyCollection_t3675191317 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4000302024_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3588504383_gshared (KeyCollection_t3675191317 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3588504383(__this, method) ((  void (*) (KeyCollection_t3675191317 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3588504383_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3131629506_gshared (KeyCollection_t3675191317 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3131629506(__this, ___item0, method) ((  bool (*) (KeyCollection_t3675191317 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3131629506_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m365844903_gshared (KeyCollection_t3675191317 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m365844903(__this, ___item0, method) ((  bool (*) (KeyCollection_t3675191317 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m365844903_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m552096571_gshared (KeyCollection_t3675191317 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m552096571(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3675191317 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m552096571_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3305388337_gshared (KeyCollection_t3675191317 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m3305388337(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3675191317 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3305388337_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1119244396_gshared (KeyCollection_t3675191317 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1119244396(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3675191317 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1119244396_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1604939427_gshared (KeyCollection_t3675191317 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1604939427(__this, method) ((  bool (*) (KeyCollection_t3675191317 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1604939427_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3890468629_gshared (KeyCollection_t3675191317 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3890468629(__this, method) ((  bool (*) (KeyCollection_t3675191317 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3890468629_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3050212545_gshared (KeyCollection_t3675191317 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3050212545(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3675191317 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3050212545_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2085907331_gshared (KeyCollection_t3675191317 * __this, SoundTypeIDU5BU5D_t1111200077* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m2085907331(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3675191317 *, SoundTypeIDU5BU5D_t1111200077*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2085907331_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2663367920  KeyCollection_GetEnumerator_m2696925350_gshared (KeyCollection_t3675191317 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m2696925350(__this, method) ((  Enumerator_t2663367920  (*) (KeyCollection_t3675191317 *, const MethodInfo*))KeyCollection_GetEnumerator_m2696925350_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1814660955_gshared (KeyCollection_t3675191317 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m1814660955(__this, method) ((  int32_t (*) (KeyCollection_t3675191317 *, const MethodInfo*))KeyCollection_get_Count_m1814660955_gshared)(__this, method)
