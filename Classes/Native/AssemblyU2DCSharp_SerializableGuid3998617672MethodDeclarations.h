﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SerializableGuid
struct SerializableGuid_t3998617672;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Guid2862754429.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void SerializableGuid::.ctor()
extern "C"  void SerializableGuid__ctor_m108586835 (SerializableGuid_t3998617672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SerializableGuid::.ctor(System.Guid)
extern "C"  void SerializableGuid__ctor_m3977459927 (SerializableGuid_t3998617672 * __this, Guid_t2862754429  ___guid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SerializableGuid::.ctor(System.String)
extern "C"  void SerializableGuid__ctor_m1858107023 (SerializableGuid_t3998617672 * __this, String_t* ___guid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SerializableGuid SerializableGuid::NewGuid()
extern "C"  SerializableGuid_t3998617672 * SerializableGuid_NewGuid_m1850029285 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SerializableGuid::GetString()
extern "C"  String_t* SerializableGuid_GetString_m2397761141 (SerializableGuid_t3998617672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SerializableGuid::GetHashCode()
extern "C"  int32_t SerializableGuid_GetHashCode_m2751782388 (SerializableGuid_t3998617672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SerializableGuid::Equals(System.Object)
extern "C"  bool SerializableGuid_Equals_m3958964944 (SerializableGuid_t3998617672 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SerializableGuid::ToString()
extern "C"  String_t* SerializableGuid_ToString_m2420090528 (SerializableGuid_t3998617672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
