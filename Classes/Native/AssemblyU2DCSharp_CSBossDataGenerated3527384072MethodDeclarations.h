﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSBossDataGenerated
struct CSBossDataGenerated_t3527384072;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void CSBossDataGenerated::.ctor()
extern "C"  void CSBossDataGenerated__ctor_m1241088099 (CSBossDataGenerated_t3527384072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSBossDataGenerated::CSBossData_CSBossData1(JSVCall,System.Int32)
extern "C"  bool CSBossDataGenerated_CSBossData_CSBossData1_m4141729679 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSBossDataGenerated::CSBossData_CurBossUnit(JSVCall)
extern "C"  void CSBossDataGenerated_CSBossData_CurBossUnit_m1272084589 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSBossDataGenerated::CSBossData_LoadData__String(JSVCall,System.Int32)
extern "C"  bool CSBossDataGenerated_CSBossData_LoadData__String_m748822142 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSBossDataGenerated::CSBossData_UnLoadData(JSVCall,System.Int32)
extern "C"  bool CSBossDataGenerated_CSBossData_UnLoadData_m3597122342 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSBossDataGenerated::__Register()
extern "C"  void CSBossDataGenerated___Register_m2223489604 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSBossDataGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t CSBossDataGenerated_ilo_getObject1_m4283385011 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSBossDataGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void CSBossDataGenerated_ilo_addJSCSRel2_m2080359968 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
