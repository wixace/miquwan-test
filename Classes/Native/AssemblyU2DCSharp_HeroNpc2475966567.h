﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CSHeroUnit
struct CSHeroUnit_t3764358446;

#include "AssemblyU2DCSharp_Monster2901270458.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroNpc
struct  HeroNpc_t2475966567  : public Monster_t2901270458
{
public:
	// UnityEngine.Vector3 HeroNpc::_initPosition
	Vector3_t4282066566  ____initPosition_337;
	// CSHeroUnit HeroNpc::tpl
	CSHeroUnit_t3764358446 * ___tpl_338;
	// System.Boolean HeroNpc::<IsTrigger>k__BackingField
	bool ___U3CIsTriggerU3Ek__BackingField_339;

public:
	inline static int32_t get_offset_of__initPosition_337() { return static_cast<int32_t>(offsetof(HeroNpc_t2475966567, ____initPosition_337)); }
	inline Vector3_t4282066566  get__initPosition_337() const { return ____initPosition_337; }
	inline Vector3_t4282066566 * get_address_of__initPosition_337() { return &____initPosition_337; }
	inline void set__initPosition_337(Vector3_t4282066566  value)
	{
		____initPosition_337 = value;
	}

	inline static int32_t get_offset_of_tpl_338() { return static_cast<int32_t>(offsetof(HeroNpc_t2475966567, ___tpl_338)); }
	inline CSHeroUnit_t3764358446 * get_tpl_338() const { return ___tpl_338; }
	inline CSHeroUnit_t3764358446 ** get_address_of_tpl_338() { return &___tpl_338; }
	inline void set_tpl_338(CSHeroUnit_t3764358446 * value)
	{
		___tpl_338 = value;
		Il2CppCodeGenWriteBarrier(&___tpl_338, value);
	}

	inline static int32_t get_offset_of_U3CIsTriggerU3Ek__BackingField_339() { return static_cast<int32_t>(offsetof(HeroNpc_t2475966567, ___U3CIsTriggerU3Ek__BackingField_339)); }
	inline bool get_U3CIsTriggerU3Ek__BackingField_339() const { return ___U3CIsTriggerU3Ek__BackingField_339; }
	inline bool* get_address_of_U3CIsTriggerU3Ek__BackingField_339() { return &___U3CIsTriggerU3Ek__BackingField_339; }
	inline void set_U3CIsTriggerU3Ek__BackingField_339(bool value)
	{
		___U3CIsTriggerU3Ek__BackingField_339 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
