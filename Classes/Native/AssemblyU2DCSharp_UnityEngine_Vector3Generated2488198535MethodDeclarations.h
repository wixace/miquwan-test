﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Vector3Generated
struct UnityEngine_Vector3Generated_t2488198535;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_Vector3Generated::.ctor()
extern "C"  void UnityEngine_Vector3Generated__ctor_m4227736820 (UnityEngine_Vector3Generated_t2488198535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector3Generated::.cctor()
extern "C"  void UnityEngine_Vector3Generated__cctor_m1728726329 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_Vector31(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_Vector31_m2138310052 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_Vector32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_Vector32_m3383074533 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_Vector33(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_Vector33_m332871718 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector3Generated::Vector3_kEpsilon(JSVCall)
extern "C"  void UnityEngine_Vector3Generated_Vector3_kEpsilon_m598679433 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector3Generated::Vector3_x(JSVCall)
extern "C"  void UnityEngine_Vector3Generated_Vector3_x_m4095469228 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector3Generated::Vector3_y(JSVCall)
extern "C"  void UnityEngine_Vector3Generated_Vector3_y_m3898955723 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector3Generated::Vector3_z(JSVCall)
extern "C"  void UnityEngine_Vector3Generated_Vector3_z_m3702442218 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector3Generated::Vector3_Item_Int32(JSVCall)
extern "C"  void UnityEngine_Vector3Generated_Vector3_Item_Int32_m3352579206 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector3Generated::Vector3_normalized(JSVCall)
extern "C"  void UnityEngine_Vector3Generated_Vector3_normalized_m2833411985 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector3Generated::Vector3_magnitude(JSVCall)
extern "C"  void UnityEngine_Vector3Generated_Vector3_magnitude_m761422356 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector3Generated::Vector3_sqrMagnitude(JSVCall)
extern "C"  void UnityEngine_Vector3Generated_Vector3_sqrMagnitude_m1759279372 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector3Generated::Vector3_zero(JSVCall)
extern "C"  void UnityEngine_Vector3Generated_Vector3_zero_m261295232 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector3Generated::Vector3_one(JSVCall)
extern "C"  void UnityEngine_Vector3Generated_Vector3_one_m85054718 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector3Generated::Vector3_forward(JSVCall)
extern "C"  void UnityEngine_Vector3Generated_Vector3_forward_m440315007 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector3Generated::Vector3_back(JSVCall)
extern "C"  void UnityEngine_Vector3Generated_Vector3_back_m167222561 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector3Generated::Vector3_up(JSVCall)
extern "C"  void UnityEngine_Vector3Generated_Vector3_up_m2909208173 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector3Generated::Vector3_down(JSVCall)
extern "C"  void UnityEngine_Vector3Generated_Vector3_down_m3513712582 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector3Generated::Vector3_left(JSVCall)
extern "C"  void UnityEngine_Vector3Generated_Vector3_left_m3474949633 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector3Generated::Vector3_right(JSVCall)
extern "C"  void UnityEngine_Vector3Generated_Vector3_right_m3571662472 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_Equals__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_Equals__Object_m115271041 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_GetHashCode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_GetHashCode_m1886112304 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_Normalize(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_Normalize_m1265956396 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_Scale__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_Scale__Vector3_m2853846217 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_Set__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_Set__Single__Single__Single_m3335588633 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_ToString__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_ToString__String_m2432502336 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_ToString(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_ToString_m685026031 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_Angle__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_Angle__Vector3__Vector3_m867529266 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_ClampMagnitude__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_ClampMagnitude__Vector3__Single_m1808351426 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_Cross__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_Cross__Vector3__Vector3_m2201673887 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_Distance__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_Distance__Vector3__Vector3_m820127224 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_Dot__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_Dot__Vector3__Vector3_m1376603176 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_Lerp__Vector3__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_Lerp__Vector3__Vector3__Single_m2452328386 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_LerpUnclamped__Vector3__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_LerpUnclamped__Vector3__Vector3__Single_m833374417 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_Magnitude__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_Magnitude__Vector3_m2550174275 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_Max__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_Max__Vector3__Vector3_m743568707 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_Min__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_Min__Vector3__Vector3_m3912888369 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_MoveTowards__Vector3__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_MoveTowards__Vector3__Vector3__Single_m665032434 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_Normalize__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_Normalize__Vector3_m576595366 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_op_Addition__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_op_Addition__Vector3__Vector3_m6298553 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_op_Division__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_op_Division__Vector3__Single_m1934019120 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_op_Equality__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_op_Equality__Vector3__Vector3_m1931575223 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_op_Inequality__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_op_Inequality__Vector3__Vector3_m2534347740 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_op_Multiply__Single__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_op_Multiply__Single__Vector3_m3361616201 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_op_Multiply__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_op_Multiply__Vector3__Single_m4088179929 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_op_Subtraction__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_op_Subtraction__Vector3__Vector3_m1345542745 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_op_UnaryNegation__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_op_UnaryNegation__Vector3_m2302485465 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_OrthoNormalize__Vector3__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_OrthoNormalize__Vector3__Vector3__Vector3_m1278833306 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_OrthoNormalize__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_OrthoNormalize__Vector3__Vector3_m2003142328 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_Project__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_Project__Vector3__Vector3_m2501176056 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_ProjectOnPlane__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_ProjectOnPlane__Vector3__Vector3_m3158835175 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_Reflect__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_Reflect__Vector3__Vector3_m724107932 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_RotateTowards__Vector3__Vector3__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_RotateTowards__Vector3__Vector3__Single__Single_m3586862576 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_Scale__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_Scale__Vector3__Vector3_m1397882793 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_Slerp__Vector3__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_Slerp__Vector3__Vector3__Single_m1503952145 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_SlerpUnclamped__Vector3__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_SlerpUnclamped__Vector3__Vector3__Single_m2979521634 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_SmoothDamp__Vector3__Vector3__Vector3__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_SmoothDamp__Vector3__Vector3__Vector3__Single__Single__Single_m2376772121 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_SmoothDamp__Vector3__Vector3__Vector3__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_SmoothDamp__Vector3__Vector3__Vector3__Single__Single_m2152741841 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_SmoothDamp__Vector3__Vector3__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_SmoothDamp__Vector3__Vector3__Vector3__Single_m1419707785 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::Vector3_SqrMagnitude__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_Vector3_SqrMagnitude__Vector3_m798019347 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector3Generated::__Register()
extern "C"  void UnityEngine_Vector3Generated___Register_m1194872915 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector3Generated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_Vector3Generated_ilo_attachFinalizerObject1_m3964441451 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_Vector3Generated::ilo_getSingle2(System.Int32)
extern "C"  float UnityEngine_Vector3Generated_ilo_getSingle2_m178593012 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector3Generated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void UnityEngine_Vector3Generated_ilo_addJSCSRel3_m1256266418 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector3Generated::ilo_setSingle4(System.Int32,System.Single)
extern "C"  void UnityEngine_Vector3Generated_ilo_setSingle4_m2147998851 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector3Generated::ilo_changeJSObj5(System.Int32,System.Object)
extern "C"  void UnityEngine_Vector3Generated_ilo_changeJSObj5_m3771751161 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector3Generated::ilo_setVector3S6(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_Vector3Generated_ilo_setVector3S6_m1705475992 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_Vector3Generated::ilo_getVector3S7(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_Vector3Generated_ilo_getVector3S7_m3763340748 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_Vector3Generated::ilo_getStringS8(System.Int32)
extern "C"  String_t* UnityEngine_Vector3Generated_ilo_getStringS8_m1815169785 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector3Generated::ilo_setBooleanS9(System.Int32,System.Boolean)
extern "C"  void UnityEngine_Vector3Generated_ilo_setBooleanS9_m451120477 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
