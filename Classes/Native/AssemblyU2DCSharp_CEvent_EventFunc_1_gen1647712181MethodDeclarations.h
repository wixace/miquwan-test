﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CEvent.EventFunc`1<System.Object>
struct EventFunc_1_t1647712181;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void CEvent.EventFunc`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void EventFunc_1__ctor_m2817712347_gshared (EventFunc_1_t1647712181 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define EventFunc_1__ctor_m2817712347(__this, ___object0, ___method1, method) ((  void (*) (EventFunc_1_t1647712181 *, Il2CppObject *, IntPtr_t, const MethodInfo*))EventFunc_1__ctor_m2817712347_gshared)(__this, ___object0, ___method1, method)
// System.Void CEvent.EventFunc`1<System.Object>::Invoke(T)
extern "C"  void EventFunc_1_Invoke_m205765385_gshared (EventFunc_1_t1647712181 * __this, Il2CppObject * ___ev0, const MethodInfo* method);
#define EventFunc_1_Invoke_m205765385(__this, ___ev0, method) ((  void (*) (EventFunc_1_t1647712181 *, Il2CppObject *, const MethodInfo*))EventFunc_1_Invoke_m205765385_gshared)(__this, ___ev0, method)
// System.IAsyncResult CEvent.EventFunc`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * EventFunc_1_BeginInvoke_m1354789470_gshared (EventFunc_1_t1647712181 * __this, Il2CppObject * ___ev0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define EventFunc_1_BeginInvoke_m1354789470(__this, ___ev0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (EventFunc_1_t1647712181 *, Il2CppObject *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))EventFunc_1_BeginInvoke_m1354789470_gshared)(__this, ___ev0, ___callback1, ___object2, method)
// System.Void CEvent.EventFunc`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void EventFunc_1_EndInvoke_m4101437547_gshared (EventFunc_1_t1647712181 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define EventFunc_1_EndInvoke_m4101437547(__this, ___result0, method) ((  void (*) (EventFunc_1_t1647712181 *, Il2CppObject *, const MethodInfo*))EventFunc_1_EndInvoke_m4101437547_gshared)(__this, ___result0, method)
