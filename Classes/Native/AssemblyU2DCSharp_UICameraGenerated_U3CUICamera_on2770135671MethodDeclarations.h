﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICameraGenerated/<UICamera_onSelect_GetDelegate_member46_arg0>c__AnonStoreyA7
struct U3CUICamera_onSelect_GetDelegate_member46_arg0U3Ec__AnonStoreyA7_t2770135671;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UICameraGenerated/<UICamera_onSelect_GetDelegate_member46_arg0>c__AnonStoreyA7::.ctor()
extern "C"  void U3CUICamera_onSelect_GetDelegate_member46_arg0U3Ec__AnonStoreyA7__ctor_m3184982020 (U3CUICamera_onSelect_GetDelegate_member46_arg0U3Ec__AnonStoreyA7_t2770135671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated/<UICamera_onSelect_GetDelegate_member46_arg0>c__AnonStoreyA7::<>m__112(UnityEngine.GameObject,System.Boolean)
extern "C"  void U3CUICamera_onSelect_GetDelegate_member46_arg0U3Ec__AnonStoreyA7_U3CU3Em__112_m3141018142 (U3CUICamera_onSelect_GetDelegate_member46_arg0U3Ec__AnonStoreyA7_t2770135671 * __this, GameObject_t3674682005 * ___go0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
