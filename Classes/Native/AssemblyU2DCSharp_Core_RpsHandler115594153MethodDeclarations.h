﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Core.RpsHandler
struct RpsHandler_t115594153;
// Core.RpsStatistics
struct RpsStatistics_t1047797582;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Core_RpsChoice52797132.h"
#include "AssemblyU2DCSharp_Core_RpsResult479594824.h"
#include "AssemblyU2DCSharp_Core_RpsHandler115594153.h"
#include "AssemblyU2DCSharp_Core_RpsStatistics1047797582.h"

// System.Void Core.RpsHandler::.ctor()
extern "C"  void RpsHandler__ctor_m155620007 (RpsHandler_t115594153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Core.RpsHandler::Restart()
extern "C"  void RpsHandler_Restart_m1310960916 (RpsHandler_t115594153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Core.RpsHandler::UpdateResult(Core.RpsChoice)
extern "C"  void RpsHandler_UpdateResult_m2686345596 (RpsHandler_t115594153 * __this, int32_t ___choice0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Core.RpsResult Core.RpsHandler::GetPsychologyResult(Core.RpsChoice)
extern "C"  int32_t RpsHandler_GetPsychologyResult_m2082102388 (RpsHandler_t115594153 * __this, int32_t ___choice0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Core.RpsChoice Core.RpsHandler::GetRandomRps()
extern "C"  int32_t RpsHandler_GetRandomRps_m2546167299 (RpsHandler_t115594153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Core.RpsChoice Core.RpsHandler::GetWinningChoice(Core.RpsChoice)
extern "C"  int32_t RpsHandler_GetWinningChoice_m2800547099 (RpsHandler_t115594153 * __this, int32_t ___rpsChoice0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Core.RpsHandler::PredictPlayerChoice(Core.RpsChoice)
extern "C"  void RpsHandler_PredictPlayerChoice_m624811417 (RpsHandler_t115594153 * __this, int32_t ___choice0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Core.RpsResult Core.RpsHandler::DetermineResult(Core.RpsChoice,Core.RpsChoice)
extern "C"  int32_t RpsHandler_DetermineResult_m803140151 (RpsHandler_t115594153 * __this, int32_t ___playerChoice0, int32_t ___botChoice1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Core.RpsResult Core.RpsHandler::ilo_DetermineResult1(Core.RpsHandler,Core.RpsChoice,Core.RpsChoice)
extern "C"  int32_t RpsHandler_ilo_DetermineResult1_m2574185623 (Il2CppObject * __this /* static, unused */, RpsHandler_t115594153 * ____this0, int32_t ___playerChoice1, int32_t ___botChoice2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Core.RpsChoice Core.RpsHandler::ilo_PredictChoice2(Core.RpsStatistics)
extern "C"  int32_t RpsHandler_ilo_PredictChoice2_m2657831625 (Il2CppObject * __this /* static, unused */, RpsStatistics_t1047797582 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Core.RpsResult Core.RpsHandler::ilo_GetPsychologyResult3(Core.RpsHandler,Core.RpsChoice)
extern "C"  int32_t RpsHandler_ilo_GetPsychologyResult3_m3494643518 (Il2CppObject * __this /* static, unused */, RpsHandler_t115594153 * ____this0, int32_t ___choice1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Core.RpsChoice Core.RpsHandler::ilo_GetWinningChoice4(Core.RpsHandler,Core.RpsChoice)
extern "C"  int32_t RpsHandler_ilo_GetWinningChoice4_m3944902898 (Il2CppObject * __this /* static, unused */, RpsHandler_t115594153 * ____this0, int32_t ___rpsChoice1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
