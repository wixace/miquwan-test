﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>
struct Collection_1_t3403487873;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Pathfinding.Voxels.ExtraMesh[]
struct ExtraMeshU5BU5D_t2875893186;
// System.Collections.Generic.IEnumerator`1<Pathfinding.Voxels.ExtraMesh>
struct IEnumerator_1_t1834927468;
// System.Collections.Generic.IList`1<Pathfinding.Voxels.ExtraMesh>
struct IList_1_t2617709622;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_ExtraMesh4218029715.h"

// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::.ctor()
extern "C"  void Collection_1__ctor_m4242933613_gshared (Collection_1_t3403487873 * __this, const MethodInfo* method);
#define Collection_1__ctor_m4242933613(__this, method) ((  void (*) (Collection_1_t3403487873 *, const MethodInfo*))Collection_1__ctor_m4242933613_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1891075626_gshared (Collection_1_t3403487873 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1891075626(__this, method) ((  bool (*) (Collection_1_t3403487873 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1891075626_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m2445004407_gshared (Collection_1_t3403487873 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m2445004407(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3403487873 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2445004407_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m3651970758_gshared (Collection_1_t3403487873 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m3651970758(__this, method) ((  Il2CppObject * (*) (Collection_1_t3403487873 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m3651970758_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m3454232759_gshared (Collection_1_t3403487873 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m3454232759(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3403487873 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m3454232759_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1717601641_gshared (Collection_1_t3403487873 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1717601641(__this, ___value0, method) ((  bool (*) (Collection_1_t3403487873 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1717601641_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m2537712527_gshared (Collection_1_t3403487873 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m2537712527(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3403487873 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m2537712527_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m2629027714_gshared (Collection_1_t3403487873 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m2629027714(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3403487873 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2629027714_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m3871856230_gshared (Collection_1_t3403487873 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m3871856230(__this, ___value0, method) ((  void (*) (Collection_1_t3403487873 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m3871856230_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m3120065875_gshared (Collection_1_t3403487873 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3120065875(__this, method) ((  bool (*) (Collection_1_t3403487873 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m3120065875_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m3627853253_gshared (Collection_1_t3403487873 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m3627853253(__this, method) ((  Il2CppObject * (*) (Collection_1_t3403487873 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m3627853253_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m3270424472_gshared (Collection_1_t3403487873 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m3270424472(__this, method) ((  bool (*) (Collection_1_t3403487873 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m3270424472_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m698053729_gshared (Collection_1_t3403487873 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m698053729(__this, method) ((  bool (*) (Collection_1_t3403487873 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m698053729_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m4214110732_gshared (Collection_1_t3403487873 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m4214110732(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3403487873 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m4214110732_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m1668815577_gshared (Collection_1_t3403487873 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1668815577(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3403487873 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1668815577_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::Add(T)
extern "C"  void Collection_1_Add_m4006336370_gshared (Collection_1_t3403487873 * __this, ExtraMesh_t4218029715  ___item0, const MethodInfo* method);
#define Collection_1_Add_m4006336370(__this, ___item0, method) ((  void (*) (Collection_1_t3403487873 *, ExtraMesh_t4218029715 , const MethodInfo*))Collection_1_Add_m4006336370_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::Clear()
extern "C"  void Collection_1_Clear_m1649066904_gshared (Collection_1_t3403487873 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1649066904(__this, method) ((  void (*) (Collection_1_t3403487873 *, const MethodInfo*))Collection_1_Clear_m1649066904_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::ClearItems()
extern "C"  void Collection_1_ClearItems_m2546162794_gshared (Collection_1_t3403487873 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m2546162794(__this, method) ((  void (*) (Collection_1_t3403487873 *, const MethodInfo*))Collection_1_ClearItems_m2546162794_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::Contains(T)
extern "C"  bool Collection_1_Contains_m231036426_gshared (Collection_1_t3403487873 * __this, ExtraMesh_t4218029715  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m231036426(__this, ___item0, method) ((  bool (*) (Collection_1_t3403487873 *, ExtraMesh_t4218029715 , const MethodInfo*))Collection_1_Contains_m231036426_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m1831438050_gshared (Collection_1_t3403487873 * __this, ExtraMeshU5BU5D_t2875893186* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m1831438050(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3403487873 *, ExtraMeshU5BU5D_t2875893186*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1831438050_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m4089745377_gshared (Collection_1_t3403487873 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m4089745377(__this, method) ((  Il2CppObject* (*) (Collection_1_t3403487873 *, const MethodInfo*))Collection_1_GetEnumerator_m4089745377_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m2228742958_gshared (Collection_1_t3403487873 * __this, ExtraMesh_t4218029715  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m2228742958(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3403487873 *, ExtraMesh_t4218029715 , const MethodInfo*))Collection_1_IndexOf_m2228742958_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m3420450265_gshared (Collection_1_t3403487873 * __this, int32_t ___index0, ExtraMesh_t4218029715  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m3420450265(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3403487873 *, int32_t, ExtraMesh_t4218029715 , const MethodInfo*))Collection_1_Insert_m3420450265_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m1036683148_gshared (Collection_1_t3403487873 * __this, int32_t ___index0, ExtraMesh_t4218029715  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m1036683148(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3403487873 *, int32_t, ExtraMesh_t4218029715 , const MethodInfo*))Collection_1_InsertItem_m1036683148_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m714569176_gshared (Collection_1_t3403487873 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m714569176(__this, method) ((  Il2CppObject* (*) (Collection_1_t3403487873 *, const MethodInfo*))Collection_1_get_Items_m714569176_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::Remove(T)
extern "C"  bool Collection_1_Remove_m2676756613_gshared (Collection_1_t3403487873 * __this, ExtraMesh_t4218029715  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m2676756613(__this, ___item0, method) ((  bool (*) (Collection_1_t3403487873 *, ExtraMesh_t4218029715 , const MethodInfo*))Collection_1_Remove_m2676756613_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m1294303135_gshared (Collection_1_t3403487873 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m1294303135(__this, ___index0, method) ((  void (*) (Collection_1_t3403487873 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1294303135_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m1184421887_gshared (Collection_1_t3403487873 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m1184421887(__this, ___index0, method) ((  void (*) (Collection_1_t3403487873 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1184421887_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m188862477_gshared (Collection_1_t3403487873 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m188862477(__this, method) ((  int32_t (*) (Collection_1_t3403487873 *, const MethodInfo*))Collection_1_get_Count_m188862477_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::get_Item(System.Int32)
extern "C"  ExtraMesh_t4218029715  Collection_1_get_Item_m3230652037_gshared (Collection_1_t3403487873 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m3230652037(__this, ___index0, method) ((  ExtraMesh_t4218029715  (*) (Collection_1_t3403487873 *, int32_t, const MethodInfo*))Collection_1_get_Item_m3230652037_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3212319856_gshared (Collection_1_t3403487873 * __this, int32_t ___index0, ExtraMesh_t4218029715  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3212319856(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3403487873 *, int32_t, ExtraMesh_t4218029715 , const MethodInfo*))Collection_1_set_Item_m3212319856_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m4271332873_gshared (Collection_1_t3403487873 * __this, int32_t ___index0, ExtraMesh_t4218029715  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m4271332873(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3403487873 *, int32_t, ExtraMesh_t4218029715 , const MethodInfo*))Collection_1_SetItem_m4271332873_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m2467759842_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m2467759842(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m2467759842_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::ConvertItem(System.Object)
extern "C"  ExtraMesh_t4218029715  Collection_1_ConvertItem_m54573796_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m54573796(__this /* static, unused */, ___item0, method) ((  ExtraMesh_t4218029715  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m54573796_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m1801996706_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m1801996706(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m1801996706_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m1013218978_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1013218978(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m1013218978_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.ExtraMesh>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m1458641725_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1458641725(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m1458641725_gshared)(__this /* static, unused */, ___list0, method)
