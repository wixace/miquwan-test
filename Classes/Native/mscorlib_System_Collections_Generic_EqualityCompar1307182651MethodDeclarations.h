﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<FLOAT_TEXT_ID>
struct EqualityComparer_1_t1307182651;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.EqualityComparer`1<FLOAT_TEXT_ID>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2065780489_gshared (EqualityComparer_1_t1307182651 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m2065780489(__this, method) ((  void (*) (EqualityComparer_1_t1307182651 *, const MethodInfo*))EqualityComparer_1__ctor_m2065780489_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<FLOAT_TEXT_ID>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3427556804_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m3427556804(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m3427556804_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<FLOAT_TEXT_ID>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m4222472418_gshared (EqualityComparer_1_t1307182651 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m4222472418(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t1307182651 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m4222472418_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<FLOAT_TEXT_ID>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2878573160_gshared (EqualityComparer_1_t1307182651 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2878573160(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t1307182651 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2878573160_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<FLOAT_TEXT_ID>::get_Default()
extern "C"  EqualityComparer_1_t1307182651 * EqualityComparer_1_get_Default_m1809566963_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m1809566963(__this /* static, unused */, method) ((  EqualityComparer_1_t1307182651 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m1809566963_gshared)(__this /* static, unused */, method)
