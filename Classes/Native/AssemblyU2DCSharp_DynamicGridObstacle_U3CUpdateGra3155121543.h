﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// DynamicGridObstacle
struct DynamicGridObstacle_t1961628084;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DynamicGridObstacle/<UpdateGraphs>c__Iterator16
struct  U3CUpdateGraphsU3Ec__Iterator16_t3155121543  : public Il2CppObject
{
public:
	// UnityEngine.Bounds DynamicGridObstacle/<UpdateGraphs>c__Iterator16::<newBounds>__0
	Bounds_t2711641849  ___U3CnewBoundsU3E__0_0;
	// UnityEngine.Bounds DynamicGridObstacle/<UpdateGraphs>c__Iterator16::<merged>__1
	Bounds_t2711641849  ___U3CmergedU3E__1_1;
	// UnityEngine.Vector3 DynamicGridObstacle/<UpdateGraphs>c__Iterator16::<minDiff>__2
	Vector3_t4282066566  ___U3CminDiffU3E__2_2;
	// UnityEngine.Vector3 DynamicGridObstacle/<UpdateGraphs>c__Iterator16::<maxDiff>__3
	Vector3_t4282066566  ___U3CmaxDiffU3E__3_3;
	// System.Int32 DynamicGridObstacle/<UpdateGraphs>c__Iterator16::$PC
	int32_t ___U24PC_4;
	// System.Object DynamicGridObstacle/<UpdateGraphs>c__Iterator16::$current
	Il2CppObject * ___U24current_5;
	// DynamicGridObstacle DynamicGridObstacle/<UpdateGraphs>c__Iterator16::<>f__this
	DynamicGridObstacle_t1961628084 * ___U3CU3Ef__this_6;

public:
	inline static int32_t get_offset_of_U3CnewBoundsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUpdateGraphsU3Ec__Iterator16_t3155121543, ___U3CnewBoundsU3E__0_0)); }
	inline Bounds_t2711641849  get_U3CnewBoundsU3E__0_0() const { return ___U3CnewBoundsU3E__0_0; }
	inline Bounds_t2711641849 * get_address_of_U3CnewBoundsU3E__0_0() { return &___U3CnewBoundsU3E__0_0; }
	inline void set_U3CnewBoundsU3E__0_0(Bounds_t2711641849  value)
	{
		___U3CnewBoundsU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CmergedU3E__1_1() { return static_cast<int32_t>(offsetof(U3CUpdateGraphsU3Ec__Iterator16_t3155121543, ___U3CmergedU3E__1_1)); }
	inline Bounds_t2711641849  get_U3CmergedU3E__1_1() const { return ___U3CmergedU3E__1_1; }
	inline Bounds_t2711641849 * get_address_of_U3CmergedU3E__1_1() { return &___U3CmergedU3E__1_1; }
	inline void set_U3CmergedU3E__1_1(Bounds_t2711641849  value)
	{
		___U3CmergedU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CminDiffU3E__2_2() { return static_cast<int32_t>(offsetof(U3CUpdateGraphsU3Ec__Iterator16_t3155121543, ___U3CminDiffU3E__2_2)); }
	inline Vector3_t4282066566  get_U3CminDiffU3E__2_2() const { return ___U3CminDiffU3E__2_2; }
	inline Vector3_t4282066566 * get_address_of_U3CminDiffU3E__2_2() { return &___U3CminDiffU3E__2_2; }
	inline void set_U3CminDiffU3E__2_2(Vector3_t4282066566  value)
	{
		___U3CminDiffU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CmaxDiffU3E__3_3() { return static_cast<int32_t>(offsetof(U3CUpdateGraphsU3Ec__Iterator16_t3155121543, ___U3CmaxDiffU3E__3_3)); }
	inline Vector3_t4282066566  get_U3CmaxDiffU3E__3_3() const { return ___U3CmaxDiffU3E__3_3; }
	inline Vector3_t4282066566 * get_address_of_U3CmaxDiffU3E__3_3() { return &___U3CmaxDiffU3E__3_3; }
	inline void set_U3CmaxDiffU3E__3_3(Vector3_t4282066566  value)
	{
		___U3CmaxDiffU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CUpdateGraphsU3Ec__Iterator16_t3155121543, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CUpdateGraphsU3Ec__Iterator16_t3155121543, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_6() { return static_cast<int32_t>(offsetof(U3CUpdateGraphsU3Ec__Iterator16_t3155121543, ___U3CU3Ef__this_6)); }
	inline DynamicGridObstacle_t1961628084 * get_U3CU3Ef__this_6() const { return ___U3CU3Ef__this_6; }
	inline DynamicGridObstacle_t1961628084 ** get_address_of_U3CU3Ef__this_6() { return &___U3CU3Ef__this_6; }
	inline void set_U3CU3Ef__this_6(DynamicGridObstacle_t1961628084 * value)
	{
		___U3CU3Ef__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
