﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.FriendNpcBehaviorCtrl
struct FriendNpcBehaviorCtrl_t3694262014;
// CombatEntity
struct CombatEntity_t684137495;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"

// System.Void Entity.Behavior.FriendNpcBehaviorCtrl::.ctor(CombatEntity)
extern "C"  void FriendNpcBehaviorCtrl__ctor_m316588293 (FriendNpcBehaviorCtrl_t3694262014 * __this, CombatEntity_t684137495 * ___entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FriendNpcBehaviorCtrl::Init()
extern "C"  void FriendNpcBehaviorCtrl_Init_m3000936472 (FriendNpcBehaviorCtrl_t3694262014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FriendNpcBehaviorCtrl::ilo_AddBehavior1(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.IBehavior)
extern "C"  void FriendNpcBehaviorCtrl_ilo_AddBehavior1_m1302150534 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, IBehavior_t770859129 * ___behavior1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FriendNpcBehaviorCtrl::ilo_SetCurBehavior2(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void FriendNpcBehaviorCtrl_ilo_SetCurBehavior2_m2760191723 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, uint8_t ___id1, ObjectU5BU5D_t1108656482* ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
