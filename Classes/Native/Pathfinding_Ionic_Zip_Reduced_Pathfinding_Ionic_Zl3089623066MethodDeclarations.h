﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

// System.Int32 Pathfinding.Ionic.Zlib.SharedUtils::URShift(System.Int32,System.Int32)
extern "C"  int32_t SharedUtils_URShift_m4196208130 (Il2CppObject * __this /* static, unused */, int32_t ___number0, int32_t ___bits1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
