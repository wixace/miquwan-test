﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraShotMgr/CameraShotVO
struct CameraShotVO_t1900810094;
// cameraShotCfg
struct cameraShotCfg_t781157413;
// System.String
struct String_t;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_cameraShotCfg781157413.h"
#include "mscorlib_System_String7231557.h"

// System.Void CameraShotMgr/CameraShotVO::.ctor(cameraShotCfg)
extern "C"  void CameraShotVO__ctor_m2064386656 (CameraShotVO_t1900810094 * __this, cameraShotCfg_t781157413 * ___cfg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraShotMgr/CameraShotVO::isV3(System.String)
extern "C"  bool CameraShotVO_isV3_m259147888 (CameraShotVO_t1900810094 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] CameraShotMgr/CameraShotVO::SpliteToVector3(System.String)
extern "C"  Vector3U5BU5D_t215400611* CameraShotVO_SpliteToVector3_m432101861 (CameraShotVO_t1900810094 * __this, String_t* ___temp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] CameraShotMgr/CameraShotVO::SpliteToFloat(System.String)
extern "C"  SingleU5BU5D_t2316563989* CameraShotVO_SpliteToFloat_m4056110891 (CameraShotVO_t1900810094 * __this, String_t* ___temp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] CameraShotMgr/CameraShotVO::SpliteToInt(System.String)
extern "C"  Int32U5BU5D_t3230847821* CameraShotVO_SpliteToInt_m312091130 (CameraShotVO_t1900810094 * __this, String_t* ___temp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] CameraShotMgr/CameraShotVO::SpliteToInt(System.String,System.Boolean)
extern "C"  Int32U5BU5D_t3230847821* CameraShotVO_SpliteToInt_m1198702147 (CameraShotVO_t1900810094 * __this, String_t* ___temp0, bool ___isFocus1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] CameraShotMgr/CameraShotVO::SpliteToFloat(System.String,System.Boolean)
extern "C"  SingleU5BU5D_t2316563989* CameraShotVO_SpliteToFloat_m1896112370 (CameraShotVO_t1900810094 * __this, String_t* ___temp0, bool ___isFocus1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] CameraShotMgr/CameraShotVO::SpliteToFloatSP(System.String,System.Boolean)
extern "C"  SingleU5BU5D_t2316563989* CameraShotVO_SpliteToFloatSP_m4088050959 (CameraShotVO_t1900810094 * __this, String_t* ___temp0, bool ___isFirst1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] CameraShotMgr/CameraShotVO::SpliteToIntSP(System.String,System.Boolean)
extern "C"  Int32U5BU5D_t3230847821* CameraShotVO_SpliteToIntSP_m3891724832 (CameraShotVO_t1900810094 * __this, String_t* ___temp0, bool ___isFirst1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] CameraShotMgr/CameraShotVO::SpliteToString(System.String,System.Boolean)
extern "C"  StringU5BU5D_t4054002952* CameraShotVO_SpliteToString_m3525146634 (CameraShotVO_t1900810094 * __this, String_t* ___temp0, bool ___isFirst1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
