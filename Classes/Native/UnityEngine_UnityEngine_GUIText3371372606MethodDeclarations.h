﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GUIText
struct GUIText_t3371372606;
// System.String
struct String_t;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.Font
struct Font_t4241557075;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_GUIText3371372606.h"
#include "UnityEngine_UnityEngine_Font4241557075.h"
#include "UnityEngine_UnityEngine_TextAlignment2019773356.h"
#include "UnityEngine_UnityEngine_TextAnchor213922566.h"
#include "UnityEngine_UnityEngine_FontStyle3350479768.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

// System.Void UnityEngine.GUIText::.ctor()
extern "C"  void GUIText__ctor_m388389457 (GUIText_t3371372606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUIText::get_text()
extern "C"  String_t* GUIText_get_text_m3657607654 (GUIText_t3371372606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIText::set_text(System.String)
extern "C"  void GUIText_set_text_m1963534853 (GUIText_t3371372606 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.GUIText::get_material()
extern "C"  Material_t3870600107 * GUIText_get_material_m2804124146 (GUIText_t3371372606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIText::set_material(UnityEngine.Material)
extern "C"  void GUIText_set_material_m2602829689 (GUIText_t3371372606 * __this, Material_t3870600107 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIText::Internal_GetPixelOffset(UnityEngine.Vector2&)
extern "C"  void GUIText_Internal_GetPixelOffset_m3914030950 (GUIText_t3371372606 * __this, Vector2_t4282066565 * ___output0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIText::Internal_SetPixelOffset(UnityEngine.Vector2)
extern "C"  void GUIText_Internal_SetPixelOffset_m881214002 (GUIText_t3371372606 * __this, Vector2_t4282066565  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIText::INTERNAL_CALL_Internal_SetPixelOffset(UnityEngine.GUIText,UnityEngine.Vector2&)
extern "C"  void GUIText_INTERNAL_CALL_Internal_SetPixelOffset_m3761844352 (Il2CppObject * __this /* static, unused */, GUIText_t3371372606 * ___self0, Vector2_t4282066565 * ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.GUIText::get_pixelOffset()
extern "C"  Vector2_t4282066565  GUIText_get_pixelOffset_m3435835258 (GUIText_t3371372606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIText::set_pixelOffset(UnityEngine.Vector2)
extern "C"  void GUIText_set_pixelOffset_m3026830447 (GUIText_t3371372606 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Font UnityEngine.GUIText::get_font()
extern "C"  Font_t4241557075 * GUIText_get_font_m2345152450 (GUIText_t3371372606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIText::set_font(UnityEngine.Font)
extern "C"  void GUIText_set_font_m2511668601 (GUIText_t3371372606 * __this, Font_t4241557075 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TextAlignment UnityEngine.GUIText::get_alignment()
extern "C"  int32_t GUIText_get_alignment_m3414976989 (GUIText_t3371372606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIText::set_alignment(UnityEngine.TextAlignment)
extern "C"  void GUIText_set_alignment_m3608938494 (GUIText_t3371372606 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TextAnchor UnityEngine.GUIText::get_anchor()
extern "C"  int32_t GUIText_get_anchor_m1212926619 (GUIText_t3371372606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIText::set_anchor(UnityEngine.TextAnchor)
extern "C"  void GUIText_set_anchor_m663604044 (GUIText_t3371372606 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.GUIText::get_lineSpacing()
extern "C"  float GUIText_get_lineSpacing_m1774314721 (GUIText_t3371372606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIText::set_lineSpacing(System.Single)
extern "C"  void GUIText_set_lineSpacing_m2621795434 (GUIText_t3371372606 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.GUIText::get_tabSize()
extern "C"  float GUIText_get_tabSize_m123031336 (GUIText_t3371372606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIText::set_tabSize(System.Single)
extern "C"  void GUIText_set_tabSize_m2507025795 (GUIText_t3371372606 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GUIText::get_fontSize()
extern "C"  int32_t GUIText_get_fontSize_m3210485676 (GUIText_t3371372606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIText::set_fontSize(System.Int32)
extern "C"  void GUIText_set_fontSize_m2545899761 (GUIText_t3371372606 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.FontStyle UnityEngine.GUIText::get_fontStyle()
extern "C"  int32_t GUIText_get_fontStyle_m3755455344 (GUIText_t3371372606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIText::set_fontStyle(UnityEngine.FontStyle)
extern "C"  void GUIText_set_fontStyle_m700487667 (GUIText_t3371372606 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUIText::get_richText()
extern "C"  bool GUIText_get_richText_m4157266047 (GUIText_t3371372606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIText::set_richText(System.Boolean)
extern "C"  void GUIText_set_richText_m2474971728 (GUIText_t3371372606 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.GUIText::get_color()
extern "C"  Color_t4194546905  GUIText_get_color_m4000628304 (GUIText_t3371372606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIText::set_color(UnityEngine.Color)
extern "C"  void GUIText_set_color_m3031640689 (GUIText_t3371372606 * __this, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIText::INTERNAL_get_color(UnityEngine.Color&)
extern "C"  void GUIText_INTERNAL_get_color_m4188377873 (GUIText_t3371372606 * __this, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIText::INTERNAL_set_color(UnityEngine.Color&)
extern "C"  void GUIText_INTERNAL_set_color_m3915663645 (GUIText_t3371372606 * __this, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
