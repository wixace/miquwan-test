﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_moucir57
struct M_moucir57_t2477497995;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_moucir572477497995.h"

// System.Void GarbageiOS.M_moucir57::.ctor()
extern "C"  void M_moucir57__ctor_m760640520 (M_moucir57_t2477497995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_moucir57::M_chefisbai0(System.String[],System.Int32)
extern "C"  void M_moucir57_M_chefisbai0_m2771672155 (M_moucir57_t2477497995 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_moucir57::M_tererar1(System.String[],System.Int32)
extern "C"  void M_moucir57_M_tererar1_m3940799639 (M_moucir57_t2477497995 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_moucir57::M_sermarZale2(System.String[],System.Int32)
extern "C"  void M_moucir57_M_sermarZale2_m4176149759 (M_moucir57_t2477497995 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_moucir57::M_fapa3(System.String[],System.Int32)
extern "C"  void M_moucir57_M_fapa3_m4107324114 (M_moucir57_t2477497995 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_moucir57::M_reardror4(System.String[],System.Int32)
extern "C"  void M_moucir57_M_reardror4_m499989962 (M_moucir57_t2477497995 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_moucir57::M_cohow5(System.String[],System.Int32)
extern "C"  void M_moucir57_M_cohow5_m1041914134 (M_moucir57_t2477497995 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_moucir57::M_jura6(System.String[],System.Int32)
extern "C"  void M_moucir57_M_jura6_m2434329863 (M_moucir57_t2477497995 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_moucir57::M_zuya7(System.String[],System.Int32)
extern "C"  void M_moucir57_M_zuya7_m2222183775 (M_moucir57_t2477497995 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_moucir57::M_wemloudreCemseqoo8(System.String[],System.Int32)
extern "C"  void M_moucir57_M_wemloudreCemseqoo8_m3394425317 (M_moucir57_t2477497995 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_moucir57::M_couxoude9(System.String[],System.Int32)
extern "C"  void M_moucir57_M_couxoude9_m2668877838 (M_moucir57_t2477497995 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_moucir57::ilo_M_fapa31(GarbageiOS.M_moucir57,System.String[],System.Int32)
extern "C"  void M_moucir57_ilo_M_fapa31_m3752485913 (Il2CppObject * __this /* static, unused */, M_moucir57_t2477497995 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
