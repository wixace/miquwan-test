﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RVO.RVOObstacle
struct RVOObstacle_t1652911528;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// Pathfinding.RVO.ObstacleVertex
struct ObstacleVertex_t4170307099;
// Pathfinding.RVO.Simulator
struct Simulator_t2705969170;
// Pathfinding.RVO.RVOSimulator
struct RVOSimulator_t3058704865;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_RVOObstacle1652911528.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_Simulator2705969170.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_ObstacleVertex4170307099.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_RVOSimulator3058704865.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_RVOLayer1386980398.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void Pathfinding.RVO.RVOObstacle::.ctor()
extern "C"  void RVOObstacle__ctor_m2781905150 (RVOObstacle_t1652911528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOObstacle::OnDrawGizmos()
extern "C"  void RVOObstacle_OnDrawGizmos_m3222541922 (RVOObstacle_t1652911528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOObstacle::OnDrawGizmosSelected()
extern "C"  void RVOObstacle_OnDrawGizmosSelected_m965130589 (RVOObstacle_t1652911528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOObstacle::OnDrawGizmos(System.Boolean)
extern "C"  void RVOObstacle_OnDrawGizmos_m1352333401 (RVOObstacle_t1652911528 * __this, bool ___selected0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 Pathfinding.RVO.RVOObstacle::GetMatrix()
extern "C"  Matrix4x4_t1651859333  RVOObstacle_GetMatrix_m2694071342 (RVOObstacle_t1652911528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOObstacle::OnDisable()
extern "C"  void RVOObstacle_OnDisable_m1704142437 (RVOObstacle_t1652911528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOObstacle::OnEnable()
extern "C"  void RVOObstacle_OnEnable_m2118958536 (RVOObstacle_t1652911528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOObstacle::Start()
extern "C"  void RVOObstacle_Start_m1729042942 (RVOObstacle_t1652911528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOObstacle::Update()
extern "C"  void RVOObstacle_Update_m2066575823 (RVOObstacle_t1652911528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOObstacle::FindSimulator()
extern "C"  void RVOObstacle_FindSimulator_m3448767143 (RVOObstacle_t1652911528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOObstacle::AddObstacle(UnityEngine.Vector3[],System.Single)
extern "C"  void RVOObstacle_AddObstacle_m3534103472 (RVOObstacle_t1652911528 * __this, Vector3U5BU5D_t215400611* ___vertices0, float ___height1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOObstacle::AddObstacleInternal(UnityEngine.Vector3[],System.Single)
extern "C"  void RVOObstacle_AddObstacleInternal_m2862721523 (RVOObstacle_t1652911528 * __this, Vector3U5BU5D_t215400611* ___vertices0, float ___height1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOObstacle::WindCorrectly(UnityEngine.Vector3[])
extern "C"  void RVOObstacle_WindCorrectly_m1608871404 (RVOObstacle_t1652911528 * __this, Vector3U5BU5D_t215400611* ___vertices0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOObstacle::ilo_CreateObstacles1(Pathfinding.RVO.RVOObstacle)
extern "C"  void RVOObstacle_ilo_CreateObstacles1_m1537655427 (Il2CppObject * __this /* static, unused */, RVOObstacle_t1652911528 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RVO.ObstacleVertex Pathfinding.RVO.RVOObstacle::ilo_AddObstacle2(Pathfinding.RVO.Simulator,Pathfinding.RVO.ObstacleVertex)
extern "C"  ObstacleVertex_t4170307099 * RVOObstacle_ilo_AddObstacle2_m4184371081 (Il2CppObject * __this /* static, unused */, Simulator_t2705969170 * ____this0, ObstacleVertex_t4170307099 * ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOObstacle::ilo_UpdateObstacle3(Pathfinding.RVO.Simulator,Pathfinding.RVO.ObstacleVertex,UnityEngine.Vector3[],UnityEngine.Matrix4x4)
extern "C"  void RVOObstacle_ilo_UpdateObstacle3_m3521339572 (Il2CppObject * __this /* static, unused */, Simulator_t2705969170 * ____this0, ObstacleVertex_t4170307099 * ___obstacle1, Vector3U5BU5D_t215400611* ___vertices2, Matrix4x4_t1651859333  ___matrix3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RVO.Simulator Pathfinding.RVO.RVOObstacle::ilo_GetSimulator4(Pathfinding.RVO.RVOSimulator)
extern "C"  Simulator_t2705969170 * RVOObstacle_ilo_GetSimulator4_m1545367141 (Il2CppObject * __this /* static, unused */, RVOSimulator_t3058704865 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOObstacle::ilo_FindSimulator5(Pathfinding.RVO.RVOObstacle)
extern "C"  void RVOObstacle_ilo_FindSimulator5_m381192162 (Il2CppObject * __this /* static, unused */, RVOObstacle_t1652911528 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOObstacle::ilo_AddObstacleInternal6(Pathfinding.RVO.RVOObstacle,UnityEngine.Vector3[],System.Single)
extern "C"  void RVOObstacle_ilo_AddObstacleInternal6_m498479225 (Il2CppObject * __this /* static, unused */, RVOObstacle_t1652911528 * ____this0, Vector3U5BU5D_t215400611* ___vertices1, float ___height2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 Pathfinding.RVO.RVOObstacle::ilo_GetMatrix7(Pathfinding.RVO.RVOObstacle)
extern "C"  Matrix4x4_t1651859333  RVOObstacle_ilo_GetMatrix7_m2638666023 (Il2CppObject * __this /* static, unused */, RVOObstacle_t1652911528 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RVO.ObstacleVertex Pathfinding.RVO.RVOObstacle::ilo_AddObstacle8(Pathfinding.RVO.Simulator,UnityEngine.Vector3[],System.Single,UnityEngine.Matrix4x4,Pathfinding.RVO.RVOLayer)
extern "C"  ObstacleVertex_t4170307099 * RVOObstacle_ilo_AddObstacle8_m471680658 (Il2CppObject * __this /* static, unused */, Simulator_t2705969170 * ____this0, Vector3U5BU5D_t215400611* ___vertices1, float ___height2, Matrix4x4_t1651859333  ___matrix3, int32_t ___layer4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RVO.RVOObstacle::ilo_IsClockwise9(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool RVOObstacle_ilo_IsClockwise9_m3183260329 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, Vector3_t4282066566  ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
