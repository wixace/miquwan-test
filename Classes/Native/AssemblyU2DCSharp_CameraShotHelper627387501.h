﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// CameraShotHelper
struct CameraShotHelper_t627387501;
// CameraBezier
struct CameraBezier_t3025506948;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraShotHelper
struct  CameraShotHelper_t627387501  : public MonoBehaviour_t667441552
{
public:
	// CameraBezier CameraShotHelper::cameraBezier
	CameraBezier_t3025506948 * ___cameraBezier_6;
	// System.Boolean CameraShotHelper::IsOpeningMoveUpdate
	bool ___IsOpeningMoveUpdate_7;
	// System.Boolean CameraShotHelper::isBrightChange
	bool ___isBrightChange_8;
	// System.Boolean CameraShotHelper::isCircleCameraMoving
	bool ___isCircleCameraMoving_9;
	// UnityEngine.Transform CameraShotHelper::circle_TargetTr
	Transform_t1659122786 * ___circle_TargetTr_10;
	// UnityEngine.Vector3 CameraShotHelper::circle_TargetPos
	Vector3_t4282066566  ___circle_TargetPos_11;
	// System.Single CameraShotHelper::circle_Speed
	float ___circle_Speed_12;
	// System.Single CameraShotHelper::circle_Distance
	float ___circle_Distance_13;
	// System.Single CameraShotHelper::circle_High
	float ___circle_High_14;
	// System.Single CameraShotHelper::circle_StartAngle
	float ___circle_StartAngle_15;
	// System.Single CameraShotHelper::brightTime
	float ___brightTime_16;
	// System.Single CameraShotHelper::time_Temp
	float ___time_Temp_17;
	// System.Single CameraShotHelper::circle_R
	float ___circle_R_18;
	// System.Single CameraShotHelper::circle_angled
	float ___circle_angled_19;
	// System.Int32 CameraShotHelper::brightId
	int32_t ___brightId_20;
	// UnityEngine.GameObject CameraShotHelper::brightUI
	GameObject_t3674682005 * ___brightUI_21;
	// System.Int32 CameraShotHelper::curCameraShotId
	int32_t ___curCameraShotId_22;
	// System.Int32 CameraShotHelper::isRight
	int32_t ___isRight_23;
	// UnityEngine.Vector3 CameraShotHelper::curCirclePos
	Vector3_t4282066566  ___curCirclePos_24;
	// System.Boolean CameraShotHelper::isCtrlGameSpeed
	bool ___isCtrlGameSpeed_25;
	// System.Single CameraShotHelper::curSpeed
	float ___curSpeed_26;
	// System.Single CameraShotHelper::forNormalTime
	float ___forNormalTime_27;
	// System.Single CameraShotHelper::targetSpeed
	float ___targetSpeed_28;
	// System.Single CameraShotHelper::toNormalTime
	float ___toNormalTime_29;
	// System.Single CameraShotHelper::targetWaitTime
	float ___targetWaitTime_30;
	// System.Single CameraShotHelper::speedTime_Temp
	float ___speedTime_Temp_31;
	// System.Boolean CameraShotHelper::isTargetSpeeded
	bool ___isTargetSpeeded_32;
	// System.Boolean CameraShotHelper::isreturnSpeed
	bool ___isreturnSpeed_33;
	// System.Int32 CameraShotHelper::gameSpeedId
	int32_t ___gameSpeedId_34;
	// UnityEngine.Transform CameraShotHelper::line_Tr
	Transform_t1659122786 * ___line_Tr_35;
	// UnityEngine.Vector3 CameraShotHelper::lineTargetPos
	Vector3_t4282066566  ___lineTargetPos_36;
	// System.Single CameraShotHelper::lineTargetDis
	float ___lineTargetDis_37;
	// System.Single CameraShotHelper::lineTime
	float ___lineTime_38;
	// System.Boolean CameraShotHelper::isLineRunning
	bool ___isLineRunning_39;
	// UnityEngine.Vector3 CameraShotHelper::zero
	Vector3_t4282066566  ___zero_40;
	// UnityEngine.Vector3 CameraShotHelper::lineTarget_Temp
	Vector3_t4282066566  ___lineTarget_Temp_41;
	// System.Single CameraShotHelper::lineTime_Temp
	float ___lineTime_Temp_42;
	// System.Single CameraShotHelper::lineTruthDis
	float ___lineTruthDis_43;
	// System.Single CameraShotHelper::lineMidDis
	float ___lineMidDis_44;
	// System.Int32 CameraShotHelper::lineCameraShotId
	int32_t ___lineCameraShotId_45;
	// System.Int32 CameraShotHelper::isDamp
	int32_t ___isDamp_46;

public:
	inline static int32_t get_offset_of_cameraBezier_6() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___cameraBezier_6)); }
	inline CameraBezier_t3025506948 * get_cameraBezier_6() const { return ___cameraBezier_6; }
	inline CameraBezier_t3025506948 ** get_address_of_cameraBezier_6() { return &___cameraBezier_6; }
	inline void set_cameraBezier_6(CameraBezier_t3025506948 * value)
	{
		___cameraBezier_6 = value;
		Il2CppCodeGenWriteBarrier(&___cameraBezier_6, value);
	}

	inline static int32_t get_offset_of_IsOpeningMoveUpdate_7() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___IsOpeningMoveUpdate_7)); }
	inline bool get_IsOpeningMoveUpdate_7() const { return ___IsOpeningMoveUpdate_7; }
	inline bool* get_address_of_IsOpeningMoveUpdate_7() { return &___IsOpeningMoveUpdate_7; }
	inline void set_IsOpeningMoveUpdate_7(bool value)
	{
		___IsOpeningMoveUpdate_7 = value;
	}

	inline static int32_t get_offset_of_isBrightChange_8() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___isBrightChange_8)); }
	inline bool get_isBrightChange_8() const { return ___isBrightChange_8; }
	inline bool* get_address_of_isBrightChange_8() { return &___isBrightChange_8; }
	inline void set_isBrightChange_8(bool value)
	{
		___isBrightChange_8 = value;
	}

	inline static int32_t get_offset_of_isCircleCameraMoving_9() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___isCircleCameraMoving_9)); }
	inline bool get_isCircleCameraMoving_9() const { return ___isCircleCameraMoving_9; }
	inline bool* get_address_of_isCircleCameraMoving_9() { return &___isCircleCameraMoving_9; }
	inline void set_isCircleCameraMoving_9(bool value)
	{
		___isCircleCameraMoving_9 = value;
	}

	inline static int32_t get_offset_of_circle_TargetTr_10() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___circle_TargetTr_10)); }
	inline Transform_t1659122786 * get_circle_TargetTr_10() const { return ___circle_TargetTr_10; }
	inline Transform_t1659122786 ** get_address_of_circle_TargetTr_10() { return &___circle_TargetTr_10; }
	inline void set_circle_TargetTr_10(Transform_t1659122786 * value)
	{
		___circle_TargetTr_10 = value;
		Il2CppCodeGenWriteBarrier(&___circle_TargetTr_10, value);
	}

	inline static int32_t get_offset_of_circle_TargetPos_11() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___circle_TargetPos_11)); }
	inline Vector3_t4282066566  get_circle_TargetPos_11() const { return ___circle_TargetPos_11; }
	inline Vector3_t4282066566 * get_address_of_circle_TargetPos_11() { return &___circle_TargetPos_11; }
	inline void set_circle_TargetPos_11(Vector3_t4282066566  value)
	{
		___circle_TargetPos_11 = value;
	}

	inline static int32_t get_offset_of_circle_Speed_12() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___circle_Speed_12)); }
	inline float get_circle_Speed_12() const { return ___circle_Speed_12; }
	inline float* get_address_of_circle_Speed_12() { return &___circle_Speed_12; }
	inline void set_circle_Speed_12(float value)
	{
		___circle_Speed_12 = value;
	}

	inline static int32_t get_offset_of_circle_Distance_13() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___circle_Distance_13)); }
	inline float get_circle_Distance_13() const { return ___circle_Distance_13; }
	inline float* get_address_of_circle_Distance_13() { return &___circle_Distance_13; }
	inline void set_circle_Distance_13(float value)
	{
		___circle_Distance_13 = value;
	}

	inline static int32_t get_offset_of_circle_High_14() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___circle_High_14)); }
	inline float get_circle_High_14() const { return ___circle_High_14; }
	inline float* get_address_of_circle_High_14() { return &___circle_High_14; }
	inline void set_circle_High_14(float value)
	{
		___circle_High_14 = value;
	}

	inline static int32_t get_offset_of_circle_StartAngle_15() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___circle_StartAngle_15)); }
	inline float get_circle_StartAngle_15() const { return ___circle_StartAngle_15; }
	inline float* get_address_of_circle_StartAngle_15() { return &___circle_StartAngle_15; }
	inline void set_circle_StartAngle_15(float value)
	{
		___circle_StartAngle_15 = value;
	}

	inline static int32_t get_offset_of_brightTime_16() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___brightTime_16)); }
	inline float get_brightTime_16() const { return ___brightTime_16; }
	inline float* get_address_of_brightTime_16() { return &___brightTime_16; }
	inline void set_brightTime_16(float value)
	{
		___brightTime_16 = value;
	}

	inline static int32_t get_offset_of_time_Temp_17() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___time_Temp_17)); }
	inline float get_time_Temp_17() const { return ___time_Temp_17; }
	inline float* get_address_of_time_Temp_17() { return &___time_Temp_17; }
	inline void set_time_Temp_17(float value)
	{
		___time_Temp_17 = value;
	}

	inline static int32_t get_offset_of_circle_R_18() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___circle_R_18)); }
	inline float get_circle_R_18() const { return ___circle_R_18; }
	inline float* get_address_of_circle_R_18() { return &___circle_R_18; }
	inline void set_circle_R_18(float value)
	{
		___circle_R_18 = value;
	}

	inline static int32_t get_offset_of_circle_angled_19() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___circle_angled_19)); }
	inline float get_circle_angled_19() const { return ___circle_angled_19; }
	inline float* get_address_of_circle_angled_19() { return &___circle_angled_19; }
	inline void set_circle_angled_19(float value)
	{
		___circle_angled_19 = value;
	}

	inline static int32_t get_offset_of_brightId_20() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___brightId_20)); }
	inline int32_t get_brightId_20() const { return ___brightId_20; }
	inline int32_t* get_address_of_brightId_20() { return &___brightId_20; }
	inline void set_brightId_20(int32_t value)
	{
		___brightId_20 = value;
	}

	inline static int32_t get_offset_of_brightUI_21() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___brightUI_21)); }
	inline GameObject_t3674682005 * get_brightUI_21() const { return ___brightUI_21; }
	inline GameObject_t3674682005 ** get_address_of_brightUI_21() { return &___brightUI_21; }
	inline void set_brightUI_21(GameObject_t3674682005 * value)
	{
		___brightUI_21 = value;
		Il2CppCodeGenWriteBarrier(&___brightUI_21, value);
	}

	inline static int32_t get_offset_of_curCameraShotId_22() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___curCameraShotId_22)); }
	inline int32_t get_curCameraShotId_22() const { return ___curCameraShotId_22; }
	inline int32_t* get_address_of_curCameraShotId_22() { return &___curCameraShotId_22; }
	inline void set_curCameraShotId_22(int32_t value)
	{
		___curCameraShotId_22 = value;
	}

	inline static int32_t get_offset_of_isRight_23() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___isRight_23)); }
	inline int32_t get_isRight_23() const { return ___isRight_23; }
	inline int32_t* get_address_of_isRight_23() { return &___isRight_23; }
	inline void set_isRight_23(int32_t value)
	{
		___isRight_23 = value;
	}

	inline static int32_t get_offset_of_curCirclePos_24() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___curCirclePos_24)); }
	inline Vector3_t4282066566  get_curCirclePos_24() const { return ___curCirclePos_24; }
	inline Vector3_t4282066566 * get_address_of_curCirclePos_24() { return &___curCirclePos_24; }
	inline void set_curCirclePos_24(Vector3_t4282066566  value)
	{
		___curCirclePos_24 = value;
	}

	inline static int32_t get_offset_of_isCtrlGameSpeed_25() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___isCtrlGameSpeed_25)); }
	inline bool get_isCtrlGameSpeed_25() const { return ___isCtrlGameSpeed_25; }
	inline bool* get_address_of_isCtrlGameSpeed_25() { return &___isCtrlGameSpeed_25; }
	inline void set_isCtrlGameSpeed_25(bool value)
	{
		___isCtrlGameSpeed_25 = value;
	}

	inline static int32_t get_offset_of_curSpeed_26() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___curSpeed_26)); }
	inline float get_curSpeed_26() const { return ___curSpeed_26; }
	inline float* get_address_of_curSpeed_26() { return &___curSpeed_26; }
	inline void set_curSpeed_26(float value)
	{
		___curSpeed_26 = value;
	}

	inline static int32_t get_offset_of_forNormalTime_27() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___forNormalTime_27)); }
	inline float get_forNormalTime_27() const { return ___forNormalTime_27; }
	inline float* get_address_of_forNormalTime_27() { return &___forNormalTime_27; }
	inline void set_forNormalTime_27(float value)
	{
		___forNormalTime_27 = value;
	}

	inline static int32_t get_offset_of_targetSpeed_28() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___targetSpeed_28)); }
	inline float get_targetSpeed_28() const { return ___targetSpeed_28; }
	inline float* get_address_of_targetSpeed_28() { return &___targetSpeed_28; }
	inline void set_targetSpeed_28(float value)
	{
		___targetSpeed_28 = value;
	}

	inline static int32_t get_offset_of_toNormalTime_29() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___toNormalTime_29)); }
	inline float get_toNormalTime_29() const { return ___toNormalTime_29; }
	inline float* get_address_of_toNormalTime_29() { return &___toNormalTime_29; }
	inline void set_toNormalTime_29(float value)
	{
		___toNormalTime_29 = value;
	}

	inline static int32_t get_offset_of_targetWaitTime_30() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___targetWaitTime_30)); }
	inline float get_targetWaitTime_30() const { return ___targetWaitTime_30; }
	inline float* get_address_of_targetWaitTime_30() { return &___targetWaitTime_30; }
	inline void set_targetWaitTime_30(float value)
	{
		___targetWaitTime_30 = value;
	}

	inline static int32_t get_offset_of_speedTime_Temp_31() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___speedTime_Temp_31)); }
	inline float get_speedTime_Temp_31() const { return ___speedTime_Temp_31; }
	inline float* get_address_of_speedTime_Temp_31() { return &___speedTime_Temp_31; }
	inline void set_speedTime_Temp_31(float value)
	{
		___speedTime_Temp_31 = value;
	}

	inline static int32_t get_offset_of_isTargetSpeeded_32() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___isTargetSpeeded_32)); }
	inline bool get_isTargetSpeeded_32() const { return ___isTargetSpeeded_32; }
	inline bool* get_address_of_isTargetSpeeded_32() { return &___isTargetSpeeded_32; }
	inline void set_isTargetSpeeded_32(bool value)
	{
		___isTargetSpeeded_32 = value;
	}

	inline static int32_t get_offset_of_isreturnSpeed_33() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___isreturnSpeed_33)); }
	inline bool get_isreturnSpeed_33() const { return ___isreturnSpeed_33; }
	inline bool* get_address_of_isreturnSpeed_33() { return &___isreturnSpeed_33; }
	inline void set_isreturnSpeed_33(bool value)
	{
		___isreturnSpeed_33 = value;
	}

	inline static int32_t get_offset_of_gameSpeedId_34() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___gameSpeedId_34)); }
	inline int32_t get_gameSpeedId_34() const { return ___gameSpeedId_34; }
	inline int32_t* get_address_of_gameSpeedId_34() { return &___gameSpeedId_34; }
	inline void set_gameSpeedId_34(int32_t value)
	{
		___gameSpeedId_34 = value;
	}

	inline static int32_t get_offset_of_line_Tr_35() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___line_Tr_35)); }
	inline Transform_t1659122786 * get_line_Tr_35() const { return ___line_Tr_35; }
	inline Transform_t1659122786 ** get_address_of_line_Tr_35() { return &___line_Tr_35; }
	inline void set_line_Tr_35(Transform_t1659122786 * value)
	{
		___line_Tr_35 = value;
		Il2CppCodeGenWriteBarrier(&___line_Tr_35, value);
	}

	inline static int32_t get_offset_of_lineTargetPos_36() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___lineTargetPos_36)); }
	inline Vector3_t4282066566  get_lineTargetPos_36() const { return ___lineTargetPos_36; }
	inline Vector3_t4282066566 * get_address_of_lineTargetPos_36() { return &___lineTargetPos_36; }
	inline void set_lineTargetPos_36(Vector3_t4282066566  value)
	{
		___lineTargetPos_36 = value;
	}

	inline static int32_t get_offset_of_lineTargetDis_37() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___lineTargetDis_37)); }
	inline float get_lineTargetDis_37() const { return ___lineTargetDis_37; }
	inline float* get_address_of_lineTargetDis_37() { return &___lineTargetDis_37; }
	inline void set_lineTargetDis_37(float value)
	{
		___lineTargetDis_37 = value;
	}

	inline static int32_t get_offset_of_lineTime_38() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___lineTime_38)); }
	inline float get_lineTime_38() const { return ___lineTime_38; }
	inline float* get_address_of_lineTime_38() { return &___lineTime_38; }
	inline void set_lineTime_38(float value)
	{
		___lineTime_38 = value;
	}

	inline static int32_t get_offset_of_isLineRunning_39() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___isLineRunning_39)); }
	inline bool get_isLineRunning_39() const { return ___isLineRunning_39; }
	inline bool* get_address_of_isLineRunning_39() { return &___isLineRunning_39; }
	inline void set_isLineRunning_39(bool value)
	{
		___isLineRunning_39 = value;
	}

	inline static int32_t get_offset_of_zero_40() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___zero_40)); }
	inline Vector3_t4282066566  get_zero_40() const { return ___zero_40; }
	inline Vector3_t4282066566 * get_address_of_zero_40() { return &___zero_40; }
	inline void set_zero_40(Vector3_t4282066566  value)
	{
		___zero_40 = value;
	}

	inline static int32_t get_offset_of_lineTarget_Temp_41() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___lineTarget_Temp_41)); }
	inline Vector3_t4282066566  get_lineTarget_Temp_41() const { return ___lineTarget_Temp_41; }
	inline Vector3_t4282066566 * get_address_of_lineTarget_Temp_41() { return &___lineTarget_Temp_41; }
	inline void set_lineTarget_Temp_41(Vector3_t4282066566  value)
	{
		___lineTarget_Temp_41 = value;
	}

	inline static int32_t get_offset_of_lineTime_Temp_42() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___lineTime_Temp_42)); }
	inline float get_lineTime_Temp_42() const { return ___lineTime_Temp_42; }
	inline float* get_address_of_lineTime_Temp_42() { return &___lineTime_Temp_42; }
	inline void set_lineTime_Temp_42(float value)
	{
		___lineTime_Temp_42 = value;
	}

	inline static int32_t get_offset_of_lineTruthDis_43() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___lineTruthDis_43)); }
	inline float get_lineTruthDis_43() const { return ___lineTruthDis_43; }
	inline float* get_address_of_lineTruthDis_43() { return &___lineTruthDis_43; }
	inline void set_lineTruthDis_43(float value)
	{
		___lineTruthDis_43 = value;
	}

	inline static int32_t get_offset_of_lineMidDis_44() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___lineMidDis_44)); }
	inline float get_lineMidDis_44() const { return ___lineMidDis_44; }
	inline float* get_address_of_lineMidDis_44() { return &___lineMidDis_44; }
	inline void set_lineMidDis_44(float value)
	{
		___lineMidDis_44 = value;
	}

	inline static int32_t get_offset_of_lineCameraShotId_45() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___lineCameraShotId_45)); }
	inline int32_t get_lineCameraShotId_45() const { return ___lineCameraShotId_45; }
	inline int32_t* get_address_of_lineCameraShotId_45() { return &___lineCameraShotId_45; }
	inline void set_lineCameraShotId_45(int32_t value)
	{
		___lineCameraShotId_45 = value;
	}

	inline static int32_t get_offset_of_isDamp_46() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501, ___isDamp_46)); }
	inline int32_t get_isDamp_46() const { return ___isDamp_46; }
	inline int32_t* get_address_of_isDamp_46() { return &___isDamp_46; }
	inline void set_isDamp_46(int32_t value)
	{
		___isDamp_46 = value;
	}
};

struct CameraShotHelper_t627387501_StaticFields
{
public:
	// CameraShotHelper CameraShotHelper::instance
	CameraShotHelper_t627387501 * ___instance_5;

public:
	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(CameraShotHelper_t627387501_StaticFields, ___instance_5)); }
	inline CameraShotHelper_t627387501 * get_instance_5() const { return ___instance_5; }
	inline CameraShotHelper_t627387501 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(CameraShotHelper_t627387501 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier(&___instance_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
