﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_IPointerDownHandlerGenerated
struct UnityEngine_EventSystems_IPointerDownHandlerGenerated_t3013064072;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_EventSystems_IPointerDownHandlerGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_IPointerDownHandlerGenerated__ctor_m3681639651 (UnityEngine_EventSystems_IPointerDownHandlerGenerated_t3013064072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_IPointerDownHandlerGenerated::IPointerDownHandler_OnPointerDown__PointerEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_IPointerDownHandlerGenerated_IPointerDownHandler_OnPointerDown__PointerEventData_m1982759489 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_IPointerDownHandlerGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_IPointerDownHandlerGenerated___Register_m2315630020 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
