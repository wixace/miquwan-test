﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21573321581.h"
#include "AssemblyU2DCSharp_UIModelDisplayType1891752679.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2016324438_gshared (KeyValuePair_2_t1573321581 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m2016324438(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1573321581 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m2016324438_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2360182738_gshared (KeyValuePair_2_t1573321581 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2360182738(__this, method) ((  int32_t (*) (KeyValuePair_2_t1573321581 *, const MethodInfo*))KeyValuePair_2_get_Key_m2360182738_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m598503699_gshared (KeyValuePair_2_t1573321581 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m598503699(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1573321581 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m598503699_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1861716754_gshared (KeyValuePair_2_t1573321581 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1861716754(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t1573321581 *, const MethodInfo*))KeyValuePair_2_get_Value_m1861716754_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2608006035_gshared (KeyValuePair_2_t1573321581 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m2608006035(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1573321581 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m2608006035_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1592821359_gshared (KeyValuePair_2_t1573321581 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m1592821359(__this, method) ((  String_t* (*) (KeyValuePair_2_t1573321581 *, const MethodInfo*))KeyValuePair_2_ToString_m1592821359_gshared)(__this, method)
