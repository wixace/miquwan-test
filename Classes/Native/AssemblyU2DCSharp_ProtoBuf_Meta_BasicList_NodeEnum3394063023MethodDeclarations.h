﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Meta.BasicList/Node
struct Node_t1496877195;
// System.Object
struct Il2CppObject;
// ProtoBuf.Meta.BasicList/NodeEnumerator
struct NodeEnumerator_t3394063023;
struct NodeEnumerator_t3394063023_marshaled_pinvoke;
struct NodeEnumerator_t3394063023_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_BasicList_NodeEnum3394063023.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_BasicList_Node1496877195.h"

// System.Void ProtoBuf.Meta.BasicList/NodeEnumerator::.ctor(ProtoBuf.Meta.BasicList/Node)
extern "C"  void NodeEnumerator__ctor_m2820756769 (NodeEnumerator_t3394063023 * __this, Node_t1496877195 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.BasicList/NodeEnumerator::System.Collections.IEnumerator.Reset()
extern "C"  void NodeEnumerator_System_Collections_IEnumerator_Reset_m3510193326 (NodeEnumerator_t3394063023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.BasicList/NodeEnumerator::get_Current()
extern "C"  Il2CppObject * NodeEnumerator_get_Current_m353654053 (NodeEnumerator_t3394063023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.BasicList/NodeEnumerator::MoveNext()
extern "C"  bool NodeEnumerator_MoveNext_m3598892176 (NodeEnumerator_t3394063023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct NodeEnumerator_t3394063023;
struct NodeEnumerator_t3394063023_marshaled_pinvoke;

extern "C" void NodeEnumerator_t3394063023_marshal_pinvoke(const NodeEnumerator_t3394063023& unmarshaled, NodeEnumerator_t3394063023_marshaled_pinvoke& marshaled);
extern "C" void NodeEnumerator_t3394063023_marshal_pinvoke_back(const NodeEnumerator_t3394063023_marshaled_pinvoke& marshaled, NodeEnumerator_t3394063023& unmarshaled);
extern "C" void NodeEnumerator_t3394063023_marshal_pinvoke_cleanup(NodeEnumerator_t3394063023_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct NodeEnumerator_t3394063023;
struct NodeEnumerator_t3394063023_marshaled_com;

extern "C" void NodeEnumerator_t3394063023_marshal_com(const NodeEnumerator_t3394063023& unmarshaled, NodeEnumerator_t3394063023_marshaled_com& marshaled);
extern "C" void NodeEnumerator_t3394063023_marshal_com_back(const NodeEnumerator_t3394063023_marshaled_com& marshaled, NodeEnumerator_t3394063023& unmarshaled);
extern "C" void NodeEnumerator_t3394063023_marshal_com_cleanup(NodeEnumerator_t3394063023_marshaled_com& marshaled);
