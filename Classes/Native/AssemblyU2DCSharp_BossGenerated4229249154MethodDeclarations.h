﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BossGenerated
struct BossGenerated_t4229249154;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void BossGenerated::.ctor()
extern "C"  void BossGenerated__ctor_m3494877993 (BossGenerated_t4229249154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BossGenerated::Boss_Boss1(JSVCall,System.Int32)
extern "C"  bool BossGenerated_Boss_Boss1_m1212355785 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossGenerated::Boss_IsShowHp(JSVCall)
extern "C"  void BossGenerated_Boss_IsShowHp_m394677055 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossGenerated::__Register()
extern "C"  void BossGenerated___Register_m869364542 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BossGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t BossGenerated_ilo_getObject1_m611381037 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BossGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool BossGenerated_ilo_attachFinalizerObject2_m1913389359 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
