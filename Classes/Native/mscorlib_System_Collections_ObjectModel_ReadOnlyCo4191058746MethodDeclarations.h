﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>
struct ReadOnlyCollection_1_t4191058746;
// System.Collections.Generic.IList`1<Mihua.Assets.SubAssetMgr/ErrorInfo>
struct IList_1_t1033661117;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Mihua.Assets.SubAssetMgr/ErrorInfo[]
struct ErrorInfoU5BU5D_t2864912703;
// System.Collections.Generic.IEnumerator`1<Mihua.Assets.SubAssetMgr/ErrorInfo>
struct IEnumerator_1_t250878963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Mihua_Assets_SubAssetMgr_ErrorIn2633981210.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2997327766_gshared (ReadOnlyCollection_1_t4191058746 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2997327766(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t4191058746 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2997327766_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1428819584_gshared (ReadOnlyCollection_1_t4191058746 * __this, ErrorInfo_t2633981210  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1428819584(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t4191058746 *, ErrorInfo_t2633981210 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1428819584_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3350425162_gshared (ReadOnlyCollection_1_t4191058746 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3350425162(__this, method) ((  void (*) (ReadOnlyCollection_1_t4191058746 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3350425162_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4266029415_gshared (ReadOnlyCollection_1_t4191058746 * __this, int32_t ___index0, ErrorInfo_t2633981210  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4266029415(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t4191058746 *, int32_t, ErrorInfo_t2633981210 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4266029415_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3190220983_gshared (ReadOnlyCollection_1_t4191058746 * __this, ErrorInfo_t2633981210  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3190220983(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t4191058746 *, ErrorInfo_t2633981210 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3190220983_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2139882285_gshared (ReadOnlyCollection_1_t4191058746 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2139882285(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t4191058746 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2139882285_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  ErrorInfo_t2633981210  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2321870803_gshared (ReadOnlyCollection_1_t4191058746 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2321870803(__this, ___index0, method) ((  ErrorInfo_t2633981210  (*) (ReadOnlyCollection_1_t4191058746 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2321870803_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m4065064062_gshared (ReadOnlyCollection_1_t4191058746 * __this, int32_t ___index0, ErrorInfo_t2633981210  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m4065064062(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4191058746 *, int32_t, ErrorInfo_t2633981210 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m4065064062_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3937593592_gshared (ReadOnlyCollection_1_t4191058746 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3937593592(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4191058746 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3937593592_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2413282117_gshared (ReadOnlyCollection_1_t4191058746 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2413282117(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t4191058746 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2413282117_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2032157076_gshared (ReadOnlyCollection_1_t4191058746 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2032157076(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4191058746 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2032157076_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1379511849_gshared (ReadOnlyCollection_1_t4191058746 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1379511849(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4191058746 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1379511849_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m859514451_gshared (ReadOnlyCollection_1_t4191058746 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m859514451(__this, method) ((  void (*) (ReadOnlyCollection_1_t4191058746 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m859514451_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2779017655_gshared (ReadOnlyCollection_1_t4191058746 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2779017655(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t4191058746 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2779017655_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3543443457_gshared (ReadOnlyCollection_1_t4191058746 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3543443457(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4191058746 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3543443457_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m912886260_gshared (ReadOnlyCollection_1_t4191058746 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m912886260(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4191058746 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m912886260_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1902969524_gshared (ReadOnlyCollection_1_t4191058746 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1902969524(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t4191058746 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1902969524_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m447868804_gshared (ReadOnlyCollection_1_t4191058746 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m447868804(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t4191058746 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m447868804_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m4215305797_gshared (ReadOnlyCollection_1_t4191058746 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m4215305797(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4191058746 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m4215305797_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m602146551_gshared (ReadOnlyCollection_1_t4191058746 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m602146551(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4191058746 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m602146551_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m225801830_gshared (ReadOnlyCollection_1_t4191058746 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m225801830(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4191058746 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m225801830_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m184198099_gshared (ReadOnlyCollection_1_t4191058746 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m184198099(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4191058746 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m184198099_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m4202829502_gshared (ReadOnlyCollection_1_t4191058746 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m4202829502(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4191058746 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m4202829502_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1724319947_gshared (ReadOnlyCollection_1_t4191058746 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1724319947(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4191058746 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1724319947_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m1815001340_gshared (ReadOnlyCollection_1_t4191058746 * __this, ErrorInfo_t2633981210  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1815001340(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t4191058746 *, ErrorInfo_t2633981210 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1815001340_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m1782899120_gshared (ReadOnlyCollection_1_t4191058746 * __this, ErrorInfoU5BU5D_t2864912703* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1782899120(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t4191058746 *, ErrorInfoU5BU5D_t2864912703*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1782899120_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m1969214547_gshared (ReadOnlyCollection_1_t4191058746 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1969214547(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t4191058746 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1969214547_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2717837180_gshared (ReadOnlyCollection_1_t4191058746 * __this, ErrorInfo_t2633981210  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2717837180(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4191058746 *, ErrorInfo_t2633981210 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2717837180_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m2465881471_gshared (ReadOnlyCollection_1_t4191058746 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m2465881471(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t4191058746 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m2465881471_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Item(System.Int32)
extern "C"  ErrorInfo_t2633981210  ReadOnlyCollection_1_get_Item_m365985555_gshared (ReadOnlyCollection_1_t4191058746 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m365985555(__this, ___index0, method) ((  ErrorInfo_t2633981210  (*) (ReadOnlyCollection_1_t4191058746 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m365985555_gshared)(__this, ___index0, method)
