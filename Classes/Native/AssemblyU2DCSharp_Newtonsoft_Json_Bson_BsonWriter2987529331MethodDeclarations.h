﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Bson.BsonWriter
struct BsonWriter_t2987529331;
// System.IO.Stream
struct Stream_t1561764144;
// System.String
struct String_t;
// Newtonsoft.Json.Bson.BsonToken
struct BsonToken_t455725415;
// System.Object
struct Il2CppObject;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// SerializableGuid
struct SerializableGuid_t3998617672;
// System.Uri
struct Uri_t1116831938;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// Newtonsoft.Json.Bson.BsonBinaryWriter
struct BsonBinaryWriter_t2157484308;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "mscorlib_System_DateTimeKind1472618179.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonToken4173078175.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonToken455725415.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonType2455132538.h"
#include "mscorlib_System_Decimal1954350631.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_DateTimeOffset3884714306.h"
#include "mscorlib_System_Guid2862754429.h"
#include "AssemblyU2DCSharp_SerializableGuid3998617672.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "System_System_Uri1116831938.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonWriter2987529331.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonBinaryW2157484308.h"

// System.Void Newtonsoft.Json.Bson.BsonWriter::.ctor(System.IO.Stream)
extern "C"  void BsonWriter__ctor_m363953750 (BsonWriter_t2987529331 * __this, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeKind Newtonsoft.Json.Bson.BsonWriter::get_DateTimeKindHandling()
extern "C"  int32_t BsonWriter_get_DateTimeKindHandling_m1669842237 (BsonWriter_t2987529331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::set_DateTimeKindHandling(System.DateTimeKind)
extern "C"  void BsonWriter_set_DateTimeKindHandling_m2380469454 (BsonWriter_t2987529331 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::Flush()
extern "C"  void BsonWriter_Flush_m3504309473 (BsonWriter_t2987529331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteEnd(Newtonsoft.Json.JsonToken)
extern "C"  void BsonWriter_WriteEnd_m2547562421 (BsonWriter_t2987529331 * __this, int32_t ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteComment(System.String)
extern "C"  void BsonWriter_WriteComment_m2923290109 (BsonWriter_t2987529331 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteStartConstructor(System.String)
extern "C"  void BsonWriter_WriteStartConstructor_m4157951534 (BsonWriter_t2987529331 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteRaw(System.String)
extern "C"  void BsonWriter_WriteRaw_m2006822548 (BsonWriter_t2987529331 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteRawValue(System.String)
extern "C"  void BsonWriter_WriteRawValue_m3378293533 (BsonWriter_t2987529331 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteStartArray()
extern "C"  void BsonWriter_WriteStartArray_m2645094067 (BsonWriter_t2987529331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteStartObject()
extern "C"  void BsonWriter_WriteStartObject_m1776967175 (BsonWriter_t2987529331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WritePropertyName(System.String)
extern "C"  void BsonWriter_WritePropertyName_m314318822 (BsonWriter_t2987529331 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::Close()
extern "C"  void BsonWriter_Close_m836254421 (BsonWriter_t2987529331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::AddParent(Newtonsoft.Json.Bson.BsonToken)
extern "C"  void BsonWriter_AddParent_m2017448552 (BsonWriter_t2987529331 * __this, BsonToken_t455725415 * ___container0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::RemoveParent()
extern "C"  void BsonWriter_RemoveParent_m2233802355 (BsonWriter_t2987529331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::AddValue(System.Object,Newtonsoft.Json.Bson.BsonType)
extern "C"  void BsonWriter_AddValue_m1951859608 (BsonWriter_t2987529331 * __this, Il2CppObject * ___value0, int8_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::AddToken(Newtonsoft.Json.Bson.BsonToken)
extern "C"  void BsonWriter_AddToken_m4001526365 (BsonWriter_t2987529331 * __this, BsonToken_t455725415 * ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteNull()
extern "C"  void BsonWriter_WriteNull_m1375456611 (BsonWriter_t2987529331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteUndefined()
extern "C"  void BsonWriter_WriteUndefined_m1534407318 (BsonWriter_t2987529331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteValue(System.String)
extern "C"  void BsonWriter_WriteValue_m2637193067 (BsonWriter_t2987529331 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteValue(System.Int32)
extern "C"  void BsonWriter_WriteValue_m3256927176 (BsonWriter_t2987529331 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteValue(System.UInt32)
extern "C"  void BsonWriter_WriteValue_m3177722211 (BsonWriter_t2987529331 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteValue(System.Int64)
extern "C"  void BsonWriter_WriteValue_m3256930121 (BsonWriter_t2987529331 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteValue(System.UInt64)
extern "C"  void BsonWriter_WriteValue_m3177725156 (BsonWriter_t2987529331 * __this, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteValue(System.Single)
extern "C"  void BsonWriter_WriteValue_m2318516756 (BsonWriter_t2987529331 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteValue(System.Double)
extern "C"  void BsonWriter_WriteValue_m2068954027 (BsonWriter_t2987529331 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteValue(System.Boolean)
extern "C"  void BsonWriter_WriteValue_m359662126 (BsonWriter_t2987529331 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteValue(System.Int16)
extern "C"  void BsonWriter_WriteValue_m3256925378 (BsonWriter_t2987529331 * __this, int16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteValue(System.UInt16)
extern "C"  void BsonWriter_WriteValue_m3177720413 (BsonWriter_t2987529331 * __this, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteValue(System.Char)
extern "C"  void BsonWriter_WriteValue_m3701556614 (BsonWriter_t2987529331 * __this, Il2CppChar ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteValue(System.Byte)
extern "C"  void BsonWriter_WriteValue_m3701157396 (BsonWriter_t2987529331 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteValue(System.SByte)
extern "C"  void BsonWriter_WriteValue_m3502796763 (BsonWriter_t2987529331 * __this, int8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteValue(System.Decimal)
extern "C"  void BsonWriter_WriteValue_m3214098469 (BsonWriter_t2987529331 * __this, Decimal_t1954350631  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteValue(System.DateTime)
extern "C"  void BsonWriter_WriteValue_m241827937 (BsonWriter_t2987529331 * __this, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteValue(System.DateTimeOffset)
extern "C"  void BsonWriter_WriteValue_m2956246030 (BsonWriter_t2987529331 * __this, DateTimeOffset_t3884714306  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteValue(System.Byte[])
extern "C"  void BsonWriter_WriteValue_m579387442 (BsonWriter_t2987529331 * __this, ByteU5BU5D_t4260760469* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteValue(System.Guid)
extern "C"  void BsonWriter_WriteValue_m3705645235 (BsonWriter_t2987529331 * __this, Guid_t2862754429  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteValue(SerializableGuid)
extern "C"  void BsonWriter_WriteValue_m1730176431 (BsonWriter_t2987529331 * __this, SerializableGuid_t3998617672 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteValue(System.TimeSpan)
extern "C"  void BsonWriter_WriteValue_m526623685 (BsonWriter_t2987529331 * __this, TimeSpan_t413522987  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteValue(System.Uri)
extern "C"  void BsonWriter_WriteValue_m397045738 (BsonWriter_t2987529331 * __this, Uri_t1116831938 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteObjectId(System.Byte[])
extern "C"  void BsonWriter_WriteObjectId_m3866943795 (BsonWriter_t2987529331 * __this, ByteU5BU5D_t4260760469* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::WriteRegex(System.String,System.String)
extern "C"  void BsonWriter_WriteRegex_m3976169809 (BsonWriter_t2987529331 * __this, String_t* ___pattern0, String_t* ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Bson.BsonWriter::ilo_get_Top1(Newtonsoft.Json.JsonWriter)
extern "C"  int32_t BsonWriter_ilo_get_Top1_m3005159807 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::ilo_WriteStartArray2(Newtonsoft.Json.JsonWriter)
extern "C"  void BsonWriter_ilo_WriteStartArray2_m1516235300 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::ilo_AddParent3(Newtonsoft.Json.Bson.BsonWriter,Newtonsoft.Json.Bson.BsonToken)
extern "C"  void BsonWriter_ilo_AddParent3_m2767691360 (Il2CppObject * __this /* static, unused */, BsonWriter_t2987529331 * ____this0, BsonToken_t455725415 * ___container1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::ilo_Close4(Newtonsoft.Json.JsonWriter)
extern "C"  void BsonWriter_ilo_Close4_m2992002436 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::ilo_Close5(Newtonsoft.Json.Bson.BsonBinaryWriter)
extern "C"  void BsonWriter_ilo_Close5_m3069686736 (Il2CppObject * __this /* static, unused */, BsonBinaryWriter_t2157484308 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::ilo_AddToken6(Newtonsoft.Json.Bson.BsonWriter,Newtonsoft.Json.Bson.BsonToken)
extern "C"  void BsonWriter_ilo_AddToken6_m2557058504 (Il2CppObject * __this /* static, unused */, BsonWriter_t2987529331 * ____this0, BsonToken_t455725415 * ___token1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonWriter::ilo_get_Parent7(Newtonsoft.Json.Bson.BsonToken)
extern "C"  BsonToken_t455725415 * BsonWriter_ilo_get_Parent7_m2807698027 (Il2CppObject * __this /* static, unused */, BsonToken_t455725415 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonWriter::ilo_get_Type8(Newtonsoft.Json.Bson.BsonToken)
extern "C"  int8_t BsonWriter_ilo_get_Type8_m3676175871 (Il2CppObject * __this /* static, unused */, BsonToken_t455725415 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::ilo_WriteNull9(Newtonsoft.Json.JsonWriter)
extern "C"  void BsonWriter_ilo_WriteNull9_m346955579 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::ilo_AddValue10(Newtonsoft.Json.Bson.BsonWriter,System.Object,Newtonsoft.Json.Bson.BsonType)
extern "C"  void BsonWriter_ilo_AddValue10_m425730502 (Il2CppObject * __this /* static, unused */, BsonWriter_t2987529331 * ____this0, Il2CppObject * ___value1, int8_t ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::ilo_WriteValue11(Newtonsoft.Json.JsonWriter,System.String)
extern "C"  void BsonWriter_ilo_WriteValue11_m897891126 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::ilo_WriteValue12(Newtonsoft.Json.JsonWriter,System.Int32)
extern "C"  void BsonWriter_ilo_WriteValue12_m2493609916 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::ilo_WriteValue13(Newtonsoft.Json.JsonWriter,System.UInt32)
extern "C"  void BsonWriter_ilo_WriteValue13_m541026992 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::ilo_WriteValue14(Newtonsoft.Json.JsonWriter,System.Int64)
extern "C"  void BsonWriter_ilo_WriteValue14_m2741759355 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, int64_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::ilo_WriteValue15(Newtonsoft.Json.JsonWriter,System.UInt64)
extern "C"  void BsonWriter_ilo_WriteValue15_m3938603955 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, uint64_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::ilo_WriteValue16(Newtonsoft.Json.JsonWriter,System.Boolean)
extern "C"  void BsonWriter_ilo_WriteValue16_m1447374494 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::ilo_WriteValue17(Newtonsoft.Json.JsonWriter,System.Char)
extern "C"  void BsonWriter_ilo_WriteValue17_m4112587287 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, Il2CppChar ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::ilo_WriteValue18(Newtonsoft.Json.JsonWriter,System.Decimal)
extern "C"  void BsonWriter_ilo_WriteValue18_m2252422995 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, Decimal_t1954350631  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::ilo_WriteValue19(Newtonsoft.Json.JsonWriter,System.Guid)
extern "C"  void BsonWriter_ilo_WriteValue19_m245355334 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, Guid_t2862754429  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Bson.BsonWriter::ilo_ToString20(SerializableGuid)
extern "C"  String_t* BsonWriter_ilo_ToString20_m350262391 (Il2CppObject * __this /* static, unused */, SerializableGuid_t3998617672 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::ilo_ArgumentNotNull21(System.Object,System.String)
extern "C"  void BsonWriter_ilo_ArgumentNotNull21_m3528402890 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, String_t* ___parameterName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonWriter::ilo_AutoComplete22(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonToken)
extern "C"  void BsonWriter_ilo_AutoComplete22_m2516215168 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, int32_t ___tokenBeingWritten1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
