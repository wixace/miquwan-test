//
//  MQWNotification.h
//  MiquwanSDK
//
//  Created by MiQuWang on 2019/10/30.
//  Copyright © 2019 LQX. All rights reserved.
//




#ifndef MQWNotification_h
#define MQWNotification_h

///初始化完成通知
extern NSString * const MQWInitResultNotification;

///登录成功通知
extern NSString * const MQWLoginFinishedNotification;
///注销登录通知
extern NSString * const MQWLogoutFinishedNotification;

///注册成功通知
extern NSString * const MQWRegisterFinishedNotification;

///支付完成通知
extern NSString * const IAPFinishedNotification;
///支付失败通知
extern NSString * const IAPFailedNotification;




#endif



