﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EventNameMgrGenerated
struct EventNameMgrGenerated_t3877394204;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"

// System.Void EventNameMgrGenerated::.ctor()
extern "C"  void EventNameMgrGenerated__ctor_m1229251023 (EventNameMgrGenerated_t3877394204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventNameMgrGenerated::EventNameMgr_EventNameMgr1(JSVCall,System.Int32)
extern "C"  bool EventNameMgrGenerated_EventNameMgr_EventNameMgr1_m839865635 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_SET_GAME_QUATILY(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_SET_GAME_QUATILY_m3962735839 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_GUIDE_START(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_GUIDE_START_m1815688031 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_GUIDE_END(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_GUIDE_END_m4282230214 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_GUIDE_NEXT(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_GUIDE_NEXT_m3428662456 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_NPC_HP(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_NPC_HP_m2621964776 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_USING_SKILL(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_USING_SKILL_m550422472 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_USING_SKILL_END(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_USING_SKILL_END_m3292878412 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_EV_BE_KILLED(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_EV_BE_KILLED_m3248516835 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_USING_HETIJI(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_USING_HETIJI_m1974518978 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_ENEMY_ROUND(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_ENEMY_ROUND_m3618478503 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_ENTER_FUBEN(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_ENTER_FUBEN_m1765763145 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_ANGER_COLLECT(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_ANGER_COLLECT_m1059521324 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_OPEN_PANEL(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_OPEN_PANEL_m1300151199 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_UPGRADE_LV(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_UPGRADE_LV_m49526369 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_RECEIVE_TASK(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_RECEIVE_TASK_m1730418701 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_MESSAGE_RESPONSE(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_MESSAGE_RESPONSE_m1032588021 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_ON_CLICK_BOX(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_ON_CLICK_BOX_m3683536634 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_PLOT_START(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_PLOT_START_m2686813066 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_PLOT_END(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_PLOT_END_m868615345 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_REFRESH_FIGHTUI(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_REFRESH_FIGHTUI_m2312353950 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_REFRESH_FIGHTDATA(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_REFRESH_FIGHTDATA_m3280804168 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_SCREEN_SIZE_CHANGED(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_SCREEN_SIZE_CHANGED_m2937939573 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_PLAYER_UP(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_PLAYER_UP_m2515286789 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_LOADINGSCENE_OPENWHIRL(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_LOADINGSCENE_OPENWHIRL_m2469542773 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_NetWork_DISCONNECTED(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_NetWork_DISCONNECTED_m104604994 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_SKILL_CLICK(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_SKILL_CLICK_m1259362564 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_SKILL_USING_SUCCESS(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_SKILL_USING_SUCCESS_m241221988 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_EV_TANHUANG_UP(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_EV_TANHUANG_UP_m1852380142 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_EV_TANHUANG_DOWN(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_EV_TANHUANG_DOWN_m880622855 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_EV_DAILY_OPEN_ZHUANPAN(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_EV_DAILY_OPEN_ZHUANPAN_m1214059780 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_EV_DAILY_ZHUANPAN_PLAY_END(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_EV_DAILY_ZHUANPAN_PLAY_END_m441900796 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_EV_WILDERNESS_ADVENTURE(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_EV_WILDERNESS_ADVENTURE_m1284964529 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_EV_WILDERNESS_QUESTION(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_EV_WILDERNESS_QUESTION_m1743620325 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_ROTEPOINT_GUIDE(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_ROTEPOINT_GUIDE_m1885937503 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_EV_TEAMFACE_YIJIANQIANGHUA(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_EV_TEAMFACE_YIJIANQIANGHUA_m1285973833 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_EV_TEAMFACE_FIGHTINGFLOAT(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_EV_TEAMFACE_FIGHTINGFLOAT_m3200057163 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_EFFECT_DONE(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_EFFECT_DONE_m1768448558 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_SHOOTEFFECT_DONE(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_SHOOTEFFECT_DONE_m1560684413 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_CAMERASHOTJUMP_DONE(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_CAMERASHOTJUMP_DONE_m3560314858 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_MSG_REQUEST_END_UNIT(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_MSG_REQUEST_END_UNIT_m3067428280 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_MSG_REQUEST_START(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_MSG_REQUEST_START_m3709535242 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_MSG_REQUEST_END(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_MSG_REQUEST_END_m1495377201 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_LS_GUIDE(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_LS_GUIDE_m848416458 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_LS_GUIDE_LOCK_FUN(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_LS_GUIDE_LOCK_FUN_m2437633624 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_LS_YAOSHAIZI(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_LS_YAOSHAIZI_m1119407443 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_LS_HERO(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_LS_HERO_m4019469868 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_LS_BOSS_SHOW(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_LS_BOSS_SHOW_m2278655607 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_LS_NPC_TAUNT(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_LS_NPC_TAUNT_m2768460758 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::EventNameMgr_LS_STORY(JSVCall)
extern "C"  void EventNameMgrGenerated_EventNameMgr_LS_STORY_m1121761521 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::__Register()
extern "C"  void EventNameMgrGenerated___Register_m2591843416 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventNameMgrGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool EventNameMgrGenerated_ilo_attachFinalizerObject1_m3081220360 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventNameMgrGenerated::ilo_setStringS2(System.Int32,System.String)
extern "C"  void EventNameMgrGenerated_ilo_setStringS2_m3827604889 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
