﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_CanvasGroupGenerated
struct UnityEngine_CanvasGroupGenerated_t517140144;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_CanvasGroupGenerated::.ctor()
extern "C"  void UnityEngine_CanvasGroupGenerated__ctor_m3797716459 (UnityEngine_CanvasGroupGenerated_t517140144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CanvasGroupGenerated::CanvasGroup_CanvasGroup1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CanvasGroupGenerated_CanvasGroup_CanvasGroup1_m2346972575 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGroupGenerated::CanvasGroup_alpha(JSVCall)
extern "C"  void UnityEngine_CanvasGroupGenerated_CanvasGroup_alpha_m373457432 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGroupGenerated::CanvasGroup_interactable(JSVCall)
extern "C"  void UnityEngine_CanvasGroupGenerated_CanvasGroup_interactable_m205274310 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGroupGenerated::CanvasGroup_blocksRaycasts(JSVCall)
extern "C"  void UnityEngine_CanvasGroupGenerated_CanvasGroup_blocksRaycasts_m644916870 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGroupGenerated::CanvasGroup_ignoreParentGroups(JSVCall)
extern "C"  void UnityEngine_CanvasGroupGenerated_CanvasGroup_ignoreParentGroups_m3934954310 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CanvasGroupGenerated::CanvasGroup_IsRaycastLocationValid__Vector2__Camera(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CanvasGroupGenerated_CanvasGroup_IsRaycastLocationValid__Vector2__Camera_m2246170969 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGroupGenerated::__Register()
extern "C"  void UnityEngine_CanvasGroupGenerated___Register_m3373787580 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_CanvasGroupGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_CanvasGroupGenerated_ilo_getObject1_m3502908935 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGroupGenerated::ilo_setSingle2(System.Int32,System.Single)
extern "C"  void UnityEngine_CanvasGroupGenerated_ilo_setSingle2_m305139498 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_CanvasGroupGenerated::ilo_getSingle3(System.Int32)
extern "C"  float UnityEngine_CanvasGroupGenerated_ilo_getSingle3_m2513070238 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CanvasGroupGenerated::ilo_getBooleanS4(System.Int32)
extern "C"  bool UnityEngine_CanvasGroupGenerated_ilo_getBooleanS4_m203622724 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGroupGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void UnityEngine_CanvasGroupGenerated_ilo_setBooleanS5_m2624575128 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_CanvasGroupGenerated::ilo_getObject6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_CanvasGroupGenerated_ilo_getObject6_m2173417385 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
