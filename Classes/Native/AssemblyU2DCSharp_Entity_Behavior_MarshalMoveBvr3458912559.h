﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entity.Behavior.MarshalMoveBvr
struct  MarshalMoveBvr_t3458912559  : public IBehavior_t770859129
{
public:
	// UnityEngine.Vector3 Entity.Behavior.MarshalMoveBvr::marshalPos
	Vector3_t4282066566  ___marshalPos_2;

public:
	inline static int32_t get_offset_of_marshalPos_2() { return static_cast<int32_t>(offsetof(MarshalMoveBvr_t3458912559, ___marshalPos_2)); }
	inline Vector3_t4282066566  get_marshalPos_2() const { return ___marshalPos_2; }
	inline Vector3_t4282066566 * get_address_of_marshalPos_2() { return &___marshalPos_2; }
	inline void set_marshalPos_2(Vector3_t4282066566  value)
	{
		___marshalPos_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
