﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ActorBullet
struct ActorBullet_t2246481719;
// CombatEntity
struct CombatEntity_t684137495;
// UnityEngine.Transform
struct Transform_t1659122786;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Action
struct Action_t3771233898;
// EffectCtrl
struct EffectCtrl_t3708787644;
// IEffComponent
struct IEffComponent_t3802264961;
// skillCfg
struct skillCfg_t2142425171;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// UnityEngine.Collider
struct Collider_t2939674232;
// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;
// GameMgr
struct GameMgr_t1469029542;
// ReplayMgr
struct ReplayMgr_t1549183121;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitTag299076696.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_IEffComponent3802264961.h"
#include "AssemblyU2DCSharp_CSDatacfgManager1565254243.h"
#include "AssemblyU2DCSharp_ActorBullet2246481719.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitCamp642829979.h"

// System.Void ActorBullet::.ctor()
extern "C"  void ActorBullet__ctor_m3716551188 (ActorBullet_t2246481719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorBullet::Setup(CombatEntity,System.Single,UnityEngine.Vector3,EntityEnum.UnitTag,CombatEntity,System.Boolean,UnityEngine.Transform,System.Int32)
extern "C"  void ActorBullet_Setup_m230785640 (ActorBullet_t2246481719 * __this, CombatEntity_t684137495 * ___target0, float ___speed1, Vector3_t4282066566  ___targetPosition2, int32_t ___tag3, CombatEntity_t684137495 * ___host4, bool ___isatk5, Transform_t1659122786 * ___startTr6, int32_t ___skillID7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ActorBullet::VectorAngle(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float ActorBullet_VectorAngle_m1543373214 (ActorBullet_t2246481719 * __this, Vector3_t4282066566  ___from0, Vector3_t4282066566  ___to1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorBullet::SetParabolaPath(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Int32)
extern "C"  void ActorBullet_SetParabolaPath_m609755775 (ActorBullet_t2246481719 * __this, List_1_t1355284822 * ____paths0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorBullet::MoveEnd()
extern "C"  void ActorBullet_MoveEnd_m3179543484 (ActorBullet_t2246481719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorBullet::OnEnd()
extern "C"  void ActorBullet_OnEnd_m3210873518 (ActorBullet_t2246481719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorBullet::Init(UnityEngine.GameObject)
extern "C"  void ActorBullet_Init_m3309832504 (ActorBullet_t2246481719 * __this, GameObject_t3674682005 * ___effGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorBullet::Play()
extern "C"  void ActorBullet_Play_m3594884836 (ActorBullet_t2246481719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorBullet::Stop()
extern "C"  void ActorBullet_Stop_m3688568882 (ActorBullet_t2246481719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorBullet::Reset()
extern "C"  void ActorBullet_Reset_m1362984129 (ActorBullet_t2246481719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorBullet::Clear()
extern "C"  void ActorBullet_Clear_m1122684479 (ActorBullet_t2246481719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorBullet::Update()
extern "C"  void ActorBullet_Update_m975831929 (ActorBullet_t2246481719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorBullet::SetPause()
extern "C"  void ActorBullet_SetPause_m3952386788 (ActorBullet_t2246481719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorBullet::SetCancelPauer()
extern "C"  void ActorBullet_SetCancelPauer_m2594831589 (ActorBullet_t2246481719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ActorBullet::OnEnter(CombatEntity)
extern "C"  bool ActorBullet_OnEnter_m2818373224 (ActorBullet_t2246481719 * __this, CombatEntity_t684137495 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ActorBullet::OnStay(CombatEntity)
extern "C"  bool ActorBullet_OnStay_m1803693021 (ActorBullet_t2246481719 * __this, CombatEntity_t684137495 * ___entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorBullet::OnExit(CombatEntity)
extern "C"  void ActorBullet_OnExit_m1213950486 (ActorBullet_t2246481719 * __this, CombatEntity_t684137495 * ___entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorBullet::SetData(System.Action)
extern "C"  void ActorBullet_SetData_m4200600351 (ActorBullet_t2246481719 * __this, Action_t3771233898 * ___timeOutCall0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorBullet::<Setup>m__3D8()
extern "C"  void ActorBullet_U3CSetupU3Em__3D8_m1944632137 (ActorBullet_t2246481719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EffectCtrl ActorBullet::ilo_get_effCtrl1(IEffComponent)
extern "C"  EffectCtrl_t3708787644 * ActorBullet_ilo_get_effCtrl1_m2430652837 (Il2CppObject * __this /* static, unused */, IEffComponent_t3802264961 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// skillCfg ActorBullet::ilo_GetskillCfg2(CSDatacfgManager,System.Int32)
extern "C"  skillCfg_t2142425171 * ActorBullet_ilo_GetskillCfg2_m1211331256 (Il2CppObject * __this /* static, unused */, CSDatacfgManager_t1565254243 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ActorBullet::ilo_DelayCall3(System.Single,System.Action)
extern "C"  uint32_t ActorBullet_ilo_DelayCall3_m3054235176 (Il2CppObject * __this /* static, unused */, float ___delaytime0, Action_t3771233898 * ___act1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorBullet::ilo_StopCoroutine4(System.UInt32)
extern "C"  void ActorBullet_ilo_StopCoroutine4_m1971069305 (Il2CppObject * __this /* static, unused */, uint32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider ActorBullet::ilo_get_boxCollider5(CombatEntity)
extern "C"  Collider_t2939674232 * ActorBullet_ilo_get_boxCollider5_m2841078873 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorBullet::ilo_MoveEnd6(ActorBullet)
extern "C"  void ActorBullet_ilo_MoveEnd6_m100626668 (Il2CppObject * __this /* static, unused */, ActorBullet_t2246481719 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ActorBullet::ilo_get_isNotSelected7(CombatEntity)
extern "C"  bool ActorBullet_ilo_get_isNotSelected7_m516563412 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ActorBullet::ilo_OnEnter8(ActorBullet,CombatEntity)
extern "C"  bool ActorBullet_ilo_OnEnter8_m2761010786 (Il2CppObject * __this /* static, unused */, ActorBullet_t2246481719 * ____this0, CombatEntity_t684137495 * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorBullet::ilo_OnExit9(ActorBullet,CombatEntity)
extern "C"  void ActorBullet_ilo_OnExit9_m822152687 (Il2CppObject * __this /* static, unused */, ActorBullet_t2246481719 * ____this0, CombatEntity_t684137495 * ___entity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> ActorBullet::ilo_GetAllEnemy10(GameMgr,CombatEntity)
extern "C"  List_1_t2052323047 * ActorBullet_ilo_GetAllEnemy10_m1757416136 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, CombatEntity_t684137495 * ___entity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ActorBullet::ilo_get_isBattleStart11(GameMgr)
extern "C"  bool ActorBullet_ilo_get_isBattleStart11_m596199522 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReplayMgr ActorBullet::ilo_get_Instance12()
extern "C"  ReplayMgr_t1549183121 * ActorBullet_ilo_get_Instance12_m1832722752 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ActorBullet::ilo_get_vampire13(CombatEntity)
extern "C"  float ActorBullet_ilo_get_vampire13_m1145549979 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitCamp ActorBullet::ilo_get_unitCamp14(CombatEntity)
extern "C"  int32_t ActorBullet_ilo_get_unitCamp14_m331267449 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorBullet::ilo_AddHP15(CombatEntity,System.Single)
extern "C"  void ActorBullet_ilo_AddHP15_m320072730 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___delta1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
