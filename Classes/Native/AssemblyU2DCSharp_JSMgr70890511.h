﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// JSMgr/OnInitJSEngine
struct OnInitJSEngine_t2416054170;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t1297217088;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<JSMgr/CallbackInfo>
struct List_1_t841698179;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Action
struct Action_t3771233898;
// System.Collections.Generic.Dictionary`2<System.Int32,JSMgr/JS_CS_Rel>
struct Dictionary_2_t3551367015;
// CsObjectComparer
struct CsObjectComparer_t3718533436;
// System.Collections.Generic.Dictionary`2<System.Object,JSMgr/JS_CS_Rel>
struct Dictionary_2_t1429175676;
// System.Collections.Generic.Dictionary`2<System.Int32,System.WeakReference>
struct Dictionary_2_t2196742736;
// JSFileLoader
struct JSFileLoader_t1433707224;

#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Reflection_BindingFlags1523912596.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSMgr
struct  JSMgr_t70890511  : public Il2CppObject
{
public:

public:
};

struct JSMgr_t70890511_StaticFields
{
public:
	// JSMgr/OnInitJSEngine JSMgr::onInitJSEngine
	OnInitJSEngine_t2416054170 * ___onInitJSEngine_0;
	// System.Int32 JSMgr::jsEngineRound
	int32_t ___jsEngineRound_1;
	// System.Int32 JSMgr::startValueMapID
	int32_t ___startValueMapID_2;
	// System.Boolean JSMgr::InitJSEngine_ing
	bool ___InitJSEngine_ing_3;
	// System.Boolean JSMgr::shutDown
	bool ___shutDown_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> JSMgr::evaluatedScript
	Dictionary_2_t1297217088 * ___evaluatedScript_5;
	// System.Collections.Generic.List`1<System.String> JSMgr::evaluatList
	List_1_t1375417109 * ___evaluatList_6;
	// System.Collections.Generic.List`1<JSMgr/CallbackInfo> JSMgr::allCallbackInfo
	List_1_t841698179 * ___allCallbackInfo_7;
	// System.Reflection.BindingFlags JSMgr::BindingFlagsMethod
	int32_t ___BindingFlagsMethod_8;
	// System.Reflection.BindingFlags JSMgr::BindingFlagsMethod2
	int32_t ___BindingFlagsMethod2_9;
	// System.Reflection.BindingFlags JSMgr::BindingFlagsMethod3
	int32_t ___BindingFlagsMethod3_10;
	// System.Reflection.BindingFlags JSMgr::BindingFlagsProperty
	int32_t ___BindingFlagsProperty_11;
	// System.Reflection.BindingFlags JSMgr::BindingFlagsField
	int32_t ___BindingFlagsField_12;
	// JSVCall JSMgr::vCall
	JSVCall_t3708497963 * ___vCall_13;
	// JSDataExchangeMgr JSMgr::datax
	JSDataExchangeMgr_t3744712290 * ___datax_14;
	// System.Int32 JSMgr::jsScriptCount
	int32_t ___jsScriptCount_15;
	// System.Action JSMgr::LoadScriptsSuccess
	Action_t3771233898 * ___LoadScriptsSuccess_16;
	// System.Boolean JSMgr::IsJsFinish
	bool ___IsJsFinish_17;
	// System.Collections.Generic.Dictionary`2<System.Int32,JSMgr/JS_CS_Rel> JSMgr::mDictionary1
	Dictionary_2_t3551367015 * ___mDictionary1_18;
	// System.Collections.Generic.Dictionary`2<System.Int32,JSMgr/JS_CS_Rel> JSMgr::mDictionary1_Old
	Dictionary_2_t3551367015 * ___mDictionary1_Old_19;
	// CsObjectComparer JSMgr::csObjectComparer
	CsObjectComparer_t3718533436 * ___csObjectComparer_20;
	// System.Collections.Generic.Dictionary`2<System.Object,JSMgr/JS_CS_Rel> JSMgr::mDictionary2
	Dictionary_2_t1429175676 * ___mDictionary2_21;
	// System.Collections.Generic.Dictionary`2<System.Object,JSMgr/JS_CS_Rel> JSMgr::mDictionary2_Old
	Dictionary_2_t1429175676 * ___mDictionary2_Old_22;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.WeakReference> JSMgr::mDictJSFun1
	Dictionary_2_t2196742736 * ___mDictJSFun1_23;
	// JSFileLoader JSMgr::<jsLoader>k__BackingField
	JSFileLoader_t1433707224 * ___U3CjsLoaderU3Ek__BackingField_24;

public:
	inline static int32_t get_offset_of_onInitJSEngine_0() { return static_cast<int32_t>(offsetof(JSMgr_t70890511_StaticFields, ___onInitJSEngine_0)); }
	inline OnInitJSEngine_t2416054170 * get_onInitJSEngine_0() const { return ___onInitJSEngine_0; }
	inline OnInitJSEngine_t2416054170 ** get_address_of_onInitJSEngine_0() { return &___onInitJSEngine_0; }
	inline void set_onInitJSEngine_0(OnInitJSEngine_t2416054170 * value)
	{
		___onInitJSEngine_0 = value;
		Il2CppCodeGenWriteBarrier(&___onInitJSEngine_0, value);
	}

	inline static int32_t get_offset_of_jsEngineRound_1() { return static_cast<int32_t>(offsetof(JSMgr_t70890511_StaticFields, ___jsEngineRound_1)); }
	inline int32_t get_jsEngineRound_1() const { return ___jsEngineRound_1; }
	inline int32_t* get_address_of_jsEngineRound_1() { return &___jsEngineRound_1; }
	inline void set_jsEngineRound_1(int32_t value)
	{
		___jsEngineRound_1 = value;
	}

	inline static int32_t get_offset_of_startValueMapID_2() { return static_cast<int32_t>(offsetof(JSMgr_t70890511_StaticFields, ___startValueMapID_2)); }
	inline int32_t get_startValueMapID_2() const { return ___startValueMapID_2; }
	inline int32_t* get_address_of_startValueMapID_2() { return &___startValueMapID_2; }
	inline void set_startValueMapID_2(int32_t value)
	{
		___startValueMapID_2 = value;
	}

	inline static int32_t get_offset_of_InitJSEngine_ing_3() { return static_cast<int32_t>(offsetof(JSMgr_t70890511_StaticFields, ___InitJSEngine_ing_3)); }
	inline bool get_InitJSEngine_ing_3() const { return ___InitJSEngine_ing_3; }
	inline bool* get_address_of_InitJSEngine_ing_3() { return &___InitJSEngine_ing_3; }
	inline void set_InitJSEngine_ing_3(bool value)
	{
		___InitJSEngine_ing_3 = value;
	}

	inline static int32_t get_offset_of_shutDown_4() { return static_cast<int32_t>(offsetof(JSMgr_t70890511_StaticFields, ___shutDown_4)); }
	inline bool get_shutDown_4() const { return ___shutDown_4; }
	inline bool* get_address_of_shutDown_4() { return &___shutDown_4; }
	inline void set_shutDown_4(bool value)
	{
		___shutDown_4 = value;
	}

	inline static int32_t get_offset_of_evaluatedScript_5() { return static_cast<int32_t>(offsetof(JSMgr_t70890511_StaticFields, ___evaluatedScript_5)); }
	inline Dictionary_2_t1297217088 * get_evaluatedScript_5() const { return ___evaluatedScript_5; }
	inline Dictionary_2_t1297217088 ** get_address_of_evaluatedScript_5() { return &___evaluatedScript_5; }
	inline void set_evaluatedScript_5(Dictionary_2_t1297217088 * value)
	{
		___evaluatedScript_5 = value;
		Il2CppCodeGenWriteBarrier(&___evaluatedScript_5, value);
	}

	inline static int32_t get_offset_of_evaluatList_6() { return static_cast<int32_t>(offsetof(JSMgr_t70890511_StaticFields, ___evaluatList_6)); }
	inline List_1_t1375417109 * get_evaluatList_6() const { return ___evaluatList_6; }
	inline List_1_t1375417109 ** get_address_of_evaluatList_6() { return &___evaluatList_6; }
	inline void set_evaluatList_6(List_1_t1375417109 * value)
	{
		___evaluatList_6 = value;
		Il2CppCodeGenWriteBarrier(&___evaluatList_6, value);
	}

	inline static int32_t get_offset_of_allCallbackInfo_7() { return static_cast<int32_t>(offsetof(JSMgr_t70890511_StaticFields, ___allCallbackInfo_7)); }
	inline List_1_t841698179 * get_allCallbackInfo_7() const { return ___allCallbackInfo_7; }
	inline List_1_t841698179 ** get_address_of_allCallbackInfo_7() { return &___allCallbackInfo_7; }
	inline void set_allCallbackInfo_7(List_1_t841698179 * value)
	{
		___allCallbackInfo_7 = value;
		Il2CppCodeGenWriteBarrier(&___allCallbackInfo_7, value);
	}

	inline static int32_t get_offset_of_BindingFlagsMethod_8() { return static_cast<int32_t>(offsetof(JSMgr_t70890511_StaticFields, ___BindingFlagsMethod_8)); }
	inline int32_t get_BindingFlagsMethod_8() const { return ___BindingFlagsMethod_8; }
	inline int32_t* get_address_of_BindingFlagsMethod_8() { return &___BindingFlagsMethod_8; }
	inline void set_BindingFlagsMethod_8(int32_t value)
	{
		___BindingFlagsMethod_8 = value;
	}

	inline static int32_t get_offset_of_BindingFlagsMethod2_9() { return static_cast<int32_t>(offsetof(JSMgr_t70890511_StaticFields, ___BindingFlagsMethod2_9)); }
	inline int32_t get_BindingFlagsMethod2_9() const { return ___BindingFlagsMethod2_9; }
	inline int32_t* get_address_of_BindingFlagsMethod2_9() { return &___BindingFlagsMethod2_9; }
	inline void set_BindingFlagsMethod2_9(int32_t value)
	{
		___BindingFlagsMethod2_9 = value;
	}

	inline static int32_t get_offset_of_BindingFlagsMethod3_10() { return static_cast<int32_t>(offsetof(JSMgr_t70890511_StaticFields, ___BindingFlagsMethod3_10)); }
	inline int32_t get_BindingFlagsMethod3_10() const { return ___BindingFlagsMethod3_10; }
	inline int32_t* get_address_of_BindingFlagsMethod3_10() { return &___BindingFlagsMethod3_10; }
	inline void set_BindingFlagsMethod3_10(int32_t value)
	{
		___BindingFlagsMethod3_10 = value;
	}

	inline static int32_t get_offset_of_BindingFlagsProperty_11() { return static_cast<int32_t>(offsetof(JSMgr_t70890511_StaticFields, ___BindingFlagsProperty_11)); }
	inline int32_t get_BindingFlagsProperty_11() const { return ___BindingFlagsProperty_11; }
	inline int32_t* get_address_of_BindingFlagsProperty_11() { return &___BindingFlagsProperty_11; }
	inline void set_BindingFlagsProperty_11(int32_t value)
	{
		___BindingFlagsProperty_11 = value;
	}

	inline static int32_t get_offset_of_BindingFlagsField_12() { return static_cast<int32_t>(offsetof(JSMgr_t70890511_StaticFields, ___BindingFlagsField_12)); }
	inline int32_t get_BindingFlagsField_12() const { return ___BindingFlagsField_12; }
	inline int32_t* get_address_of_BindingFlagsField_12() { return &___BindingFlagsField_12; }
	inline void set_BindingFlagsField_12(int32_t value)
	{
		___BindingFlagsField_12 = value;
	}

	inline static int32_t get_offset_of_vCall_13() { return static_cast<int32_t>(offsetof(JSMgr_t70890511_StaticFields, ___vCall_13)); }
	inline JSVCall_t3708497963 * get_vCall_13() const { return ___vCall_13; }
	inline JSVCall_t3708497963 ** get_address_of_vCall_13() { return &___vCall_13; }
	inline void set_vCall_13(JSVCall_t3708497963 * value)
	{
		___vCall_13 = value;
		Il2CppCodeGenWriteBarrier(&___vCall_13, value);
	}

	inline static int32_t get_offset_of_datax_14() { return static_cast<int32_t>(offsetof(JSMgr_t70890511_StaticFields, ___datax_14)); }
	inline JSDataExchangeMgr_t3744712290 * get_datax_14() const { return ___datax_14; }
	inline JSDataExchangeMgr_t3744712290 ** get_address_of_datax_14() { return &___datax_14; }
	inline void set_datax_14(JSDataExchangeMgr_t3744712290 * value)
	{
		___datax_14 = value;
		Il2CppCodeGenWriteBarrier(&___datax_14, value);
	}

	inline static int32_t get_offset_of_jsScriptCount_15() { return static_cast<int32_t>(offsetof(JSMgr_t70890511_StaticFields, ___jsScriptCount_15)); }
	inline int32_t get_jsScriptCount_15() const { return ___jsScriptCount_15; }
	inline int32_t* get_address_of_jsScriptCount_15() { return &___jsScriptCount_15; }
	inline void set_jsScriptCount_15(int32_t value)
	{
		___jsScriptCount_15 = value;
	}

	inline static int32_t get_offset_of_LoadScriptsSuccess_16() { return static_cast<int32_t>(offsetof(JSMgr_t70890511_StaticFields, ___LoadScriptsSuccess_16)); }
	inline Action_t3771233898 * get_LoadScriptsSuccess_16() const { return ___LoadScriptsSuccess_16; }
	inline Action_t3771233898 ** get_address_of_LoadScriptsSuccess_16() { return &___LoadScriptsSuccess_16; }
	inline void set_LoadScriptsSuccess_16(Action_t3771233898 * value)
	{
		___LoadScriptsSuccess_16 = value;
		Il2CppCodeGenWriteBarrier(&___LoadScriptsSuccess_16, value);
	}

	inline static int32_t get_offset_of_IsJsFinish_17() { return static_cast<int32_t>(offsetof(JSMgr_t70890511_StaticFields, ___IsJsFinish_17)); }
	inline bool get_IsJsFinish_17() const { return ___IsJsFinish_17; }
	inline bool* get_address_of_IsJsFinish_17() { return &___IsJsFinish_17; }
	inline void set_IsJsFinish_17(bool value)
	{
		___IsJsFinish_17 = value;
	}

	inline static int32_t get_offset_of_mDictionary1_18() { return static_cast<int32_t>(offsetof(JSMgr_t70890511_StaticFields, ___mDictionary1_18)); }
	inline Dictionary_2_t3551367015 * get_mDictionary1_18() const { return ___mDictionary1_18; }
	inline Dictionary_2_t3551367015 ** get_address_of_mDictionary1_18() { return &___mDictionary1_18; }
	inline void set_mDictionary1_18(Dictionary_2_t3551367015 * value)
	{
		___mDictionary1_18 = value;
		Il2CppCodeGenWriteBarrier(&___mDictionary1_18, value);
	}

	inline static int32_t get_offset_of_mDictionary1_Old_19() { return static_cast<int32_t>(offsetof(JSMgr_t70890511_StaticFields, ___mDictionary1_Old_19)); }
	inline Dictionary_2_t3551367015 * get_mDictionary1_Old_19() const { return ___mDictionary1_Old_19; }
	inline Dictionary_2_t3551367015 ** get_address_of_mDictionary1_Old_19() { return &___mDictionary1_Old_19; }
	inline void set_mDictionary1_Old_19(Dictionary_2_t3551367015 * value)
	{
		___mDictionary1_Old_19 = value;
		Il2CppCodeGenWriteBarrier(&___mDictionary1_Old_19, value);
	}

	inline static int32_t get_offset_of_csObjectComparer_20() { return static_cast<int32_t>(offsetof(JSMgr_t70890511_StaticFields, ___csObjectComparer_20)); }
	inline CsObjectComparer_t3718533436 * get_csObjectComparer_20() const { return ___csObjectComparer_20; }
	inline CsObjectComparer_t3718533436 ** get_address_of_csObjectComparer_20() { return &___csObjectComparer_20; }
	inline void set_csObjectComparer_20(CsObjectComparer_t3718533436 * value)
	{
		___csObjectComparer_20 = value;
		Il2CppCodeGenWriteBarrier(&___csObjectComparer_20, value);
	}

	inline static int32_t get_offset_of_mDictionary2_21() { return static_cast<int32_t>(offsetof(JSMgr_t70890511_StaticFields, ___mDictionary2_21)); }
	inline Dictionary_2_t1429175676 * get_mDictionary2_21() const { return ___mDictionary2_21; }
	inline Dictionary_2_t1429175676 ** get_address_of_mDictionary2_21() { return &___mDictionary2_21; }
	inline void set_mDictionary2_21(Dictionary_2_t1429175676 * value)
	{
		___mDictionary2_21 = value;
		Il2CppCodeGenWriteBarrier(&___mDictionary2_21, value);
	}

	inline static int32_t get_offset_of_mDictionary2_Old_22() { return static_cast<int32_t>(offsetof(JSMgr_t70890511_StaticFields, ___mDictionary2_Old_22)); }
	inline Dictionary_2_t1429175676 * get_mDictionary2_Old_22() const { return ___mDictionary2_Old_22; }
	inline Dictionary_2_t1429175676 ** get_address_of_mDictionary2_Old_22() { return &___mDictionary2_Old_22; }
	inline void set_mDictionary2_Old_22(Dictionary_2_t1429175676 * value)
	{
		___mDictionary2_Old_22 = value;
		Il2CppCodeGenWriteBarrier(&___mDictionary2_Old_22, value);
	}

	inline static int32_t get_offset_of_mDictJSFun1_23() { return static_cast<int32_t>(offsetof(JSMgr_t70890511_StaticFields, ___mDictJSFun1_23)); }
	inline Dictionary_2_t2196742736 * get_mDictJSFun1_23() const { return ___mDictJSFun1_23; }
	inline Dictionary_2_t2196742736 ** get_address_of_mDictJSFun1_23() { return &___mDictJSFun1_23; }
	inline void set_mDictJSFun1_23(Dictionary_2_t2196742736 * value)
	{
		___mDictJSFun1_23 = value;
		Il2CppCodeGenWriteBarrier(&___mDictJSFun1_23, value);
	}

	inline static int32_t get_offset_of_U3CjsLoaderU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(JSMgr_t70890511_StaticFields, ___U3CjsLoaderU3Ek__BackingField_24)); }
	inline JSFileLoader_t1433707224 * get_U3CjsLoaderU3Ek__BackingField_24() const { return ___U3CjsLoaderU3Ek__BackingField_24; }
	inline JSFileLoader_t1433707224 ** get_address_of_U3CjsLoaderU3Ek__BackingField_24() { return &___U3CjsLoaderU3Ek__BackingField_24; }
	inline void set_U3CjsLoaderU3Ek__BackingField_24(JSFileLoader_t1433707224 * value)
	{
		___U3CjsLoaderU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsLoaderU3Ek__BackingField_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
