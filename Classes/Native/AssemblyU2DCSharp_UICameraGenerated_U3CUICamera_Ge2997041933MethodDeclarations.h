﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICameraGenerated/<UICamera_GetKey_GetDelegate_member3_arg0>c__AnonStorey9F
struct U3CUICamera_GetKey_GetDelegate_member3_arg0U3Ec__AnonStorey9F_t2997041933;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986.h"

// System.Void UICameraGenerated/<UICamera_GetKey_GetDelegate_member3_arg0>c__AnonStorey9F::.ctor()
extern "C"  void U3CUICamera_GetKey_GetDelegate_member3_arg0U3Ec__AnonStorey9F__ctor_m2980716286 (U3CUICamera_GetKey_GetDelegate_member3_arg0U3Ec__AnonStorey9F_t2997041933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICameraGenerated/<UICamera_GetKey_GetDelegate_member3_arg0>c__AnonStorey9F::<>m__102(UnityEngine.KeyCode)
extern "C"  bool U3CUICamera_GetKey_GetDelegate_member3_arg0U3Ec__AnonStorey9F_U3CU3Em__102_m672790275 (U3CUICamera_GetKey_GetDelegate_member3_arg0U3Ec__AnonStorey9F_t2997041933 * __this, int32_t ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
