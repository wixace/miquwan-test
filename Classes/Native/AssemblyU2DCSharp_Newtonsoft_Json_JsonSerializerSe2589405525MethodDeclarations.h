﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonSerializerSettings
struct JsonSerializerSettings_t2589405525;
// System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter>
struct IList_1_t559366761;
// Newtonsoft.Json.Serialization.IContractResolver
struct IContractResolver_t507353639;
// Newtonsoft.Json.Serialization.IReferenceResolver
struct IReferenceResolver_t425424564;
// System.Runtime.Serialization.SerializationBinder
struct SerializationBinder_t2137423328;
// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>
struct EventHandler_1_t937589677;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ReferenceLoopHan2761661122.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_MissingMemberHan2077487315.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ObjectCreationHand56081595.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_NullValueHandlin2754652381.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_DefaultValueHand1569448045.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_PreserveReferenc4230591217.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_TypeNameHandling2359325474.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_F3005881063.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ConstructorHandl2475221485.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2137423328.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"

// System.Void Newtonsoft.Json.JsonSerializerSettings::.ctor()
extern "C"  void JsonSerializerSettings__ctor_m2906146345 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::.cctor()
extern "C"  void JsonSerializerSettings__cctor_m3709094564 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.ReferenceLoopHandling Newtonsoft.Json.JsonSerializerSettings::get_ReferenceLoopHandling()
extern "C"  int32_t JsonSerializerSettings_get_ReferenceLoopHandling_m2545999838 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_ReferenceLoopHandling(Newtonsoft.Json.ReferenceLoopHandling)
extern "C"  void JsonSerializerSettings_set_ReferenceLoopHandling_m3123167285 (JsonSerializerSettings_t2589405525 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.MissingMemberHandling Newtonsoft.Json.JsonSerializerSettings::get_MissingMemberHandling()
extern "C"  int32_t JsonSerializerSettings_get_MissingMemberHandling_m2853425984 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_MissingMemberHandling(Newtonsoft.Json.MissingMemberHandling)
extern "C"  void JsonSerializerSettings_set_MissingMemberHandling_m2611614995 (JsonSerializerSettings_t2589405525 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.ObjectCreationHandling Newtonsoft.Json.JsonSerializerSettings::get_ObjectCreationHandling()
extern "C"  int32_t JsonSerializerSettings_get_ObjectCreationHandling_m2332923180 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_ObjectCreationHandling(Newtonsoft.Json.ObjectCreationHandling)
extern "C"  void JsonSerializerSettings_set_ObjectCreationHandling_m145276067 (JsonSerializerSettings_t2589405525 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.NullValueHandling Newtonsoft.Json.JsonSerializerSettings::get_NullValueHandling()
extern "C"  int32_t JsonSerializerSettings_get_NullValueHandling_m1407730388 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_NullValueHandling(Newtonsoft.Json.NullValueHandling)
extern "C"  void JsonSerializerSettings_set_NullValueHandling_m3053508607 (JsonSerializerSettings_t2589405525 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.DefaultValueHandling Newtonsoft.Json.JsonSerializerSettings::get_DefaultValueHandling()
extern "C"  int32_t JsonSerializerSettings_get_DefaultValueHandling_m1091834220 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_DefaultValueHandling(Newtonsoft.Json.DefaultValueHandling)
extern "C"  void JsonSerializerSettings_set_DefaultValueHandling_m3680369251 (JsonSerializerSettings_t2589405525 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter> Newtonsoft.Json.JsonSerializerSettings::get_Converters()
extern "C"  Il2CppObject* JsonSerializerSettings_get_Converters_m179934492 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_Converters(System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter>)
extern "C"  void JsonSerializerSettings_set_Converters_m2787434809 (JsonSerializerSettings_t2589405525 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.PreserveReferencesHandling Newtonsoft.Json.JsonSerializerSettings::get_PreserveReferencesHandling()
extern "C"  int32_t JsonSerializerSettings_get_PreserveReferencesHandling_m2702475372 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_PreserveReferencesHandling(Newtonsoft.Json.PreserveReferencesHandling)
extern "C"  void JsonSerializerSettings_set_PreserveReferencesHandling_m679465059 (JsonSerializerSettings_t2589405525 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.TypeNameHandling Newtonsoft.Json.JsonSerializerSettings::get_TypeNameHandling()
extern "C"  int32_t JsonSerializerSettings_get_TypeNameHandling_m1118558348 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_TypeNameHandling(Newtonsoft.Json.TypeNameHandling)
extern "C"  void JsonSerializerSettings_set_TypeNameHandling_m3038971587 (JsonSerializerSettings_t2589405525 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle Newtonsoft.Json.JsonSerializerSettings::get_TypeNameAssemblyFormat()
extern "C"  int32_t JsonSerializerSettings_get_TypeNameAssemblyFormat_m4279438064 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_TypeNameAssemblyFormat(System.Runtime.Serialization.Formatters.FormatterAssemblyStyle)
extern "C"  void JsonSerializerSettings_set_TypeNameAssemblyFormat_m1109965223 (JsonSerializerSettings_t2589405525 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.ConstructorHandling Newtonsoft.Json.JsonSerializerSettings::get_ConstructorHandling()
extern "C"  int32_t JsonSerializerSettings_get_ConstructorHandling_m4080815476 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_ConstructorHandling(Newtonsoft.Json.ConstructorHandling)
extern "C"  void JsonSerializerSettings_set_ConstructorHandling_m1250876767 (JsonSerializerSettings_t2589405525 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.JsonSerializerSettings::get_ContractResolver()
extern "C"  Il2CppObject * JsonSerializerSettings_get_ContractResolver_m3890060513 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_ContractResolver(Newtonsoft.Json.Serialization.IContractResolver)
extern "C"  void JsonSerializerSettings_set_ContractResolver_m3714876948 (JsonSerializerSettings_t2589405525 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IReferenceResolver Newtonsoft.Json.JsonSerializerSettings::get_ReferenceResolver()
extern "C"  Il2CppObject * JsonSerializerSettings_get_ReferenceResolver_m2170776877 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_ReferenceResolver(Newtonsoft.Json.Serialization.IReferenceResolver)
extern "C"  void JsonSerializerSettings_set_ReferenceResolver_m1323485184 (JsonSerializerSettings_t2589405525 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.SerializationBinder Newtonsoft.Json.JsonSerializerSettings::get_Binder()
extern "C"  SerializationBinder_t2137423328 * JsonSerializerSettings_get_Binder_m2039490852 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_Binder(System.Runtime.Serialization.SerializationBinder)
extern "C"  void JsonSerializerSettings_set_Binder_m2867822299 (JsonSerializerSettings_t2589405525 * __this, SerializationBinder_t2137423328 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs> Newtonsoft.Json.JsonSerializerSettings::get_Error()
extern "C"  EventHandler_1_t937589677 * JsonSerializerSettings_get_Error_m3254845971 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_Error(System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>)
extern "C"  void JsonSerializerSettings_set_Error_m1338687264 (JsonSerializerSettings_t2589405525 * __this, EventHandler_1_t937589677 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.StreamingContext Newtonsoft.Json.JsonSerializerSettings::get_Context()
extern "C"  StreamingContext_t2761351129  JsonSerializerSettings_get_Context_m1959544570 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_Context(System.Runtime.Serialization.StreamingContext)
extern "C"  void JsonSerializerSettings_set_Context_m3284524633 (JsonSerializerSettings_t2589405525 * __this, StreamingContext_t2761351129  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
