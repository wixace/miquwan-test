﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FlowControl.FlowAntiAddictSystem
struct FlowAntiAddictSystem_t961676908;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// FlowControl.FlowBase
struct FlowBase_t3680091731;
// LoadingBoardMgr
struct LoadingBoardMgr_t303632014;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_FlowControl_FlowBase3680091731.h"

// System.Void FlowControl.FlowAntiAddictSystem::.ctor()
extern "C"  void FlowAntiAddictSystem__ctor_m3726524484 (FlowAntiAddictSystem_t961676908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowAntiAddictSystem::Activate()
extern "C"  void FlowAntiAddictSystem_Activate_m276881715 (FlowAntiAddictSystem_t961676908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowAntiAddictSystem::OnHideedLogo(CEvent.ZEvent)
extern "C"  void FlowAntiAddictSystem_OnHideedLogo_m2891909552 (FlowAntiAddictSystem_t961676908 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowAntiAddictSystem::OnComplete()
extern "C"  void FlowAntiAddictSystem_OnComplete_m3790839672 (FlowAntiAddictSystem_t961676908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowAntiAddictSystem::FlowUpdate(System.Single)
extern "C"  void FlowAntiAddictSystem_FlowUpdate_m764216308 (FlowAntiAddictSystem_t961676908 * __this, float ___duringTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowAntiAddictSystem::ilo_RemoveEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void FlowAntiAddictSystem_ilo_RemoveEventListener1_m869757129 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowAntiAddictSystem::ilo_OnComplete2(FlowControl.FlowBase)
extern "C"  void FlowAntiAddictSystem_ilo_OnComplete2_m2007085291 (Il2CppObject * __this /* static, unused */, FlowBase_t3680091731 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LoadingBoardMgr FlowControl.FlowAntiAddictSystem::ilo_get_Instance3()
extern "C"  LoadingBoardMgr_t303632014 * FlowAntiAddictSystem_ilo_get_Instance3_m3818830975 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
