﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pomelo.Protobuf.MsgEncoder
struct MsgEncoder_t1387649199;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// Pomelo.Protobuf.Encoder
struct Encoder_t3832447410;
// Pomelo.Protobuf.Util
struct Util_t3483766166;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;
// Newtonsoft.Json.Linq.JArray
struct JArray_t3394795039;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Linq.JProperty
struct JProperty_t2616415645;
// Newtonsoft.Json.Linq.JContainer
struct JContainer_t3364442311;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_Pomelo_Protobuf_Encoder3832447410.h"
#include "AssemblyU2DCSharp_Pomelo_Protobuf_Util3483766166.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JArray3394795039.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JProperty2616415645.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JContainer3364442311.h"
#include "AssemblyU2DCSharp_Pomelo_Protobuf_MsgEncoder1387649199.h"

// System.Void Pomelo.Protobuf.MsgEncoder::.ctor(Newtonsoft.Json.Linq.JObject)
extern "C"  void MsgEncoder__ctor_m2814071585 (MsgEncoder_t1387649199 * __this, JObject_t1798544199 * ___protos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.Protobuf.MsgEncoder::set_protos(Newtonsoft.Json.Linq.JObject)
extern "C"  void MsgEncoder_set_protos_m1865468619 (MsgEncoder_t1387649199 * __this, JObject_t1798544199 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JObject Pomelo.Protobuf.MsgEncoder::get_protos()
extern "C"  JObject_t1798544199 * MsgEncoder_get_protos_m3509325972 (MsgEncoder_t1387649199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.Protobuf.MsgEncoder::set_encoder(Pomelo.Protobuf.Encoder)
extern "C"  void MsgEncoder_set_encoder_m185424439 (MsgEncoder_t1387649199 * __this, Encoder_t3832447410 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pomelo.Protobuf.Encoder Pomelo.Protobuf.MsgEncoder::get_encoder()
extern "C"  Encoder_t3832447410 * MsgEncoder_get_encoder_m3223596572 (MsgEncoder_t1387649199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.Protobuf.MsgEncoder::set_util(Pomelo.Protobuf.Util)
extern "C"  void MsgEncoder_set_util_m7717601 (MsgEncoder_t1387649199 * __this, Util_t3483766166 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pomelo.Protobuf.Util Pomelo.Protobuf.MsgEncoder::get_util()
extern "C"  Util_t3483766166 * MsgEncoder_get_util_m3519703082 (MsgEncoder_t1387649199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.Protobuf.MsgEncoder::encode(System.String,Newtonsoft.Json.Linq.JObject)
extern "C"  ByteU5BU5D_t4260760469* MsgEncoder_encode_m266572999 (MsgEncoder_t1387649199 * __this, String_t* ___route0, JObject_t1798544199 * ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pomelo.Protobuf.MsgEncoder::checkMsg(Newtonsoft.Json.Linq.JObject,Newtonsoft.Json.Linq.JObject)
extern "C"  bool MsgEncoder_checkMsg_m2129816614 (MsgEncoder_t1387649199 * __this, JObject_t1798544199 * ___msg0, JObject_t1798544199 * ___proto1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pomelo.Protobuf.MsgEncoder::encodeMsg(System.Byte[],System.Int32,Newtonsoft.Json.Linq.JObject,Newtonsoft.Json.Linq.JObject)
extern "C"  int32_t MsgEncoder_encodeMsg_m1445897882 (MsgEncoder_t1387649199 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, JObject_t1798544199 * ___proto2, JObject_t1798544199 * ___msg3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pomelo.Protobuf.MsgEncoder::encodeArray(Newtonsoft.Json.Linq.JArray,Newtonsoft.Json.Linq.JObject,System.Int32,System.Byte[],Newtonsoft.Json.Linq.JObject)
extern "C"  int32_t MsgEncoder_encodeArray_m71282060 (MsgEncoder_t1387649199 * __this, JArray_t3394795039 * ___msg0, JObject_t1798544199 * ___value1, int32_t ___offset2, ByteU5BU5D_t4260760469* ___buffer3, JObject_t1798544199 * ___proto4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pomelo.Protobuf.MsgEncoder::encodeProp(System.Object,System.String,System.Int32,System.Byte[],Newtonsoft.Json.Linq.JObject)
extern "C"  int32_t MsgEncoder_encodeProp_m3263447410 (MsgEncoder_t1387649199 * __this, Il2CppObject * ___value0, String_t* ___type1, int32_t ___offset2, ByteU5BU5D_t4260760469* ___buffer3, JObject_t1798544199 * ___proto4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.Protobuf.MsgEncoder::writeString(System.Byte[],System.Int32&,System.Object)
extern "C"  void MsgEncoder_writeString_m2775290893 (MsgEncoder_t1387649199 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t* ___offset1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.Protobuf.MsgEncoder::writeDouble(System.Byte[],System.Int32&,System.Object)
extern "C"  void MsgEncoder_writeDouble_m511025229 (MsgEncoder_t1387649199 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t* ___offset1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.Protobuf.MsgEncoder::writeFloat(System.Byte[],System.Int32&,System.Object)
extern "C"  void MsgEncoder_writeFloat_m3702652316 (MsgEncoder_t1387649199 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t* ___offset1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.Protobuf.MsgEncoder::writeUInt32(System.Byte[],System.Int32&,System.Object)
extern "C"  void MsgEncoder_writeUInt32_m330395653 (MsgEncoder_t1387649199 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t* ___offset1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.Protobuf.MsgEncoder::writeInt32(System.Byte[],System.Int32&,System.Object)
extern "C"  void MsgEncoder_writeInt32_m176543274 (MsgEncoder_t1387649199 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t* ___offset1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.Protobuf.MsgEncoder::writeBool(System.Byte[],System.Int32&,System.Object)
extern "C"  void MsgEncoder_writeBool_m345132116 (MsgEncoder_t1387649199 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t* ___offset1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pomelo.Protobuf.MsgEncoder::writeBytes(System.Byte[],System.Int32,System.Byte[])
extern "C"  int32_t MsgEncoder_writeBytes_m1311353374 (MsgEncoder_t1387649199 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, ByteU5BU5D_t4260760469* ___bytes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.Protobuf.MsgEncoder::encodeTag(System.String,System.Int32)
extern "C"  ByteU5BU5D_t4260760469* MsgEncoder_encodeTag_m3973396478 (MsgEncoder_t1387649199 * __this, String_t* ___type0, int32_t ___tag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.Protobuf.MsgEncoder::WriteRawLittleEndian64(System.Byte[],System.Int32,System.UInt64)
extern "C"  void MsgEncoder_WriteRawLittleEndian64_m291948526 (MsgEncoder_t1387649199 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, uint64_t ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pomelo.Protobuf.MsgEncoder::ilo_TryGetValue1(Newtonsoft.Json.Linq.JObject,System.String,Newtonsoft.Json.Linq.JToken&)
extern "C"  bool MsgEncoder_ilo_TryGetValue1_m2000762343 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, JToken_t3412245951 ** ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pomelo.Protobuf.MsgEncoder::ilo_ToString2(Newtonsoft.Json.Linq.JToken)
extern "C"  String_t* MsgEncoder_ilo_ToString2_m93885579 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Pomelo.Protobuf.MsgEncoder::ilo_get_Item3(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  JToken_t3412245951 * MsgEncoder_ilo_get_Item3_m1644435668 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pomelo.Protobuf.MsgEncoder::ilo_get_Name4(Newtonsoft.Json.Linq.JProperty)
extern "C"  String_t* MsgEncoder_ilo_get_Name4_m2929114483 (Il2CppObject * __this /* static, unused */, JProperty_t2616415645 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pomelo.Protobuf.MsgEncoder::ilo_ContainsKey5(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  bool MsgEncoder_ilo_ContainsKey5_m1729862971 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pomelo.Protobuf.MsgEncoder::ilo_get_Count6(Newtonsoft.Json.Linq.JContainer)
extern "C"  int32_t MsgEncoder_ilo_get_Count6_m620236062 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pomelo.Protobuf.MsgEncoder::ilo_isSimpleType7(Pomelo.Protobuf.Util,System.String)
extern "C"  bool MsgEncoder_ilo_isSimpleType7_m166658260 (Il2CppObject * __this /* static, unused */, Util_t3483766166 * ____this0, String_t* ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pomelo.Protobuf.MsgEncoder::ilo_writeBytes8(Pomelo.Protobuf.MsgEncoder,System.Byte[],System.Int32,System.Byte[])
extern "C"  int32_t MsgEncoder_ilo_writeBytes8_m2962534311 (Il2CppObject * __this /* static, unused */, MsgEncoder_t1387649199 * ____this0, ByteU5BU5D_t4260760469* ___buffer1, int32_t ___offset2, ByteU5BU5D_t4260760469* ___bytes3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.Protobuf.MsgEncoder::ilo_writeUInt329(Pomelo.Protobuf.MsgEncoder,System.Byte[],System.Int32&,System.Object)
extern "C"  void MsgEncoder_ilo_writeUInt329_m1393871395 (Il2CppObject * __this /* static, unused */, MsgEncoder_t1387649199 * ____this0, ByteU5BU5D_t4260760469* ___buffer1, int32_t* ___offset2, Il2CppObject * ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.Protobuf.MsgEncoder::ilo_writeDouble10(Pomelo.Protobuf.MsgEncoder,System.Byte[],System.Int32&,System.Object)
extern "C"  void MsgEncoder_ilo_writeDouble10_m3814371207 (Il2CppObject * __this /* static, unused */, MsgEncoder_t1387649199 * ____this0, ByteU5BU5D_t4260760469* ___buffer1, int32_t* ___offset2, Il2CppObject * ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.Protobuf.MsgEncoder::ilo_writeString11(Pomelo.Protobuf.MsgEncoder,System.Byte[],System.Int32&,System.Object)
extern "C"  void MsgEncoder_ilo_writeString11_m2820278024 (Il2CppObject * __this /* static, unused */, MsgEncoder_t1387649199 * ____this0, ByteU5BU5D_t4260760469* ___buffer1, int32_t* ___offset2, Il2CppObject * ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.Protobuf.MsgEncoder::ilo_writeBool12(Pomelo.Protobuf.MsgEncoder,System.Byte[],System.Int32&,System.Object)
extern "C"  void MsgEncoder_ilo_writeBool12_m856732322 (Il2CppObject * __this /* static, unused */, MsgEncoder_t1387649199 * ____this0, ByteU5BU5D_t4260760469* ___buffer1, int32_t* ___offset2, Il2CppObject * ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JObject Pomelo.Protobuf.MsgEncoder::ilo_get_protos13(Pomelo.Protobuf.MsgEncoder)
extern "C"  JObject_t1798544199 * MsgEncoder_ilo_get_protos13_m3595090879 (Il2CppObject * __this /* static, unused */, MsgEncoder_t1387649199 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pomelo.Protobuf.MsgEncoder::ilo_byteLength14(System.String)
extern "C"  int32_t MsgEncoder_ilo_byteLength14_m3200045981 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pomelo.Protobuf.MsgEncoder::ilo_encodeMsg15(Pomelo.Protobuf.MsgEncoder,System.Byte[],System.Int32,Newtonsoft.Json.Linq.JObject,Newtonsoft.Json.Linq.JObject)
extern "C"  int32_t MsgEncoder_ilo_encodeMsg15_m787733441 (Il2CppObject * __this /* static, unused */, MsgEncoder_t1387649199 * ____this0, ByteU5BU5D_t4260760469* ___buffer1, int32_t ___offset2, JObject_t1798544199 * ___proto3, JObject_t1798544199 * ___msg4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.Protobuf.MsgEncoder::ilo_encodeUInt3216(System.UInt32)
extern "C"  ByteU5BU5D_t4260760469* MsgEncoder_ilo_encodeUInt3216_m1548550678 (Il2CppObject * __this /* static, unused */, uint32_t ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.Protobuf.MsgEncoder::ilo_WriteRawLittleEndian6417(Pomelo.Protobuf.MsgEncoder,System.Byte[],System.Int32,System.UInt64)
extern "C"  void MsgEncoder_ilo_WriteRawLittleEndian6417_m4077027311 (Il2CppObject * __this /* static, unused */, MsgEncoder_t1387649199 * ____this0, ByteU5BU5D_t4260760469* ___buffer1, int32_t ___offset2, uint64_t ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.Protobuf.MsgEncoder::ilo_encodeFloat18(System.Single)
extern "C"  ByteU5BU5D_t4260760469* MsgEncoder_ilo_encodeFloat18_m3388645324 (Il2CppObject * __this /* static, unused */, float ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.Protobuf.MsgEncoder::ilo_encodeUInt3219(System.String)
extern "C"  ByteU5BU5D_t4260760469* MsgEncoder_ilo_encodeUInt3219_m3771386299 (Il2CppObject * __this /* static, unused */, String_t* ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pomelo.Protobuf.Util Pomelo.Protobuf.MsgEncoder::ilo_get_util20(Pomelo.Protobuf.MsgEncoder)
extern "C"  Util_t3483766166 * MsgEncoder_ilo_get_util20_m3830676913 (Il2CppObject * __this /* static, unused */, MsgEncoder_t1387649199 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pomelo.Protobuf.MsgEncoder::ilo_containType21(Pomelo.Protobuf.Util,System.String)
extern "C"  int32_t MsgEncoder_ilo_containType21_m56018310 (Il2CppObject * __this /* static, unused */, Util_t3483766166 * ____this0, String_t* ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
