﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginFastPlay
struct PluginFastPlay_t3320186403;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Object
struct Il2CppObject;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// Mihua.SDK.EnterGameInfo
struct EnterGameInfo_t1897532954;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Mihua.SDK.LvUpInfo
struct LvUpInfo_t411687177;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "AssemblyU2DCSharp_PluginFastPlay3320186403.h"

// System.Void PluginFastPlay::.ctor()
extern "C"  void PluginFastPlay__ctor_m27327960 (PluginFastPlay_t3320186403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFastPlay::Init()
extern "C"  void PluginFastPlay_Init_m4247385468 (PluginFastPlay_t3320186403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginFastPlay::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginFastPlay_ReqSDKHttpLogin_m1087667117 (PluginFastPlay_t3320186403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFastPlay::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginFastPlay_IsLoginSuccess_m4219474299 (PluginFastPlay_t3320186403 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFastPlay::OpenUserLogin()
extern "C"  void PluginFastPlay_OpenUserLogin_m1456130090 (PluginFastPlay_t3320186403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFastPlay::UserPay(CEvent.ZEvent)
extern "C"  void PluginFastPlay_UserPay_m492382216 (PluginFastPlay_t3320186403 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFastPlay::SignCallBack(System.Boolean,System.String)
extern "C"  void PluginFastPlay_SignCallBack_m94374561 (PluginFastPlay_t3320186403 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginFastPlay::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginFastPlay_BuildOrderParam2WWWForm_m1302662089 (PluginFastPlay_t3320186403 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFastPlay::CreateRole(CEvent.ZEvent)
extern "C"  void PluginFastPlay_CreateRole_m3380418045 (PluginFastPlay_t3320186403 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFastPlay::EnterGame(CEvent.ZEvent)
extern "C"  void PluginFastPlay_EnterGame_m2949453723 (PluginFastPlay_t3320186403 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFastPlay::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginFastPlay_RoleUpgrade_m82075519 (PluginFastPlay_t3320186403 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFastPlay::OnLoginSuccess(System.String)
extern "C"  void PluginFastPlay_OnLoginSuccess_m1418494685 (PluginFastPlay_t3320186403 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFastPlay::OnLogoutSuccess(System.String)
extern "C"  void PluginFastPlay_OnLogoutSuccess_m1027204946 (PluginFastPlay_t3320186403 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFastPlay::OnLogout(System.String)
extern "C"  void PluginFastPlay_OnLogout_m4117835821 (PluginFastPlay_t3320186403 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFastPlay::initSdk(System.String,System.String,System.String)
extern "C"  void PluginFastPlay_initSdk_m3941074714 (PluginFastPlay_t3320186403 * __this, String_t* ___appid0, String_t* ___key1, String_t* ___reyunkey2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFastPlay::login()
extern "C"  void PluginFastPlay_login_m3844310079 (PluginFastPlay_t3320186403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFastPlay::logout()
extern "C"  void PluginFastPlay_logout_m3215318166 (PluginFastPlay_t3320186403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFastPlay::creatRole(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginFastPlay_creatRole_m3660175765 (PluginFastPlay_t3320186403 * __this, String_t* ___role_id0, String_t* ___role_name1, String_t* ___role_level2, String_t* ___gold3, String_t* ___vipLv4, String_t* ___server_id5, String_t* ___server_name6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFastPlay::roleUpgrade(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginFastPlay_roleUpgrade_m2092640590 (PluginFastPlay_t3320186403 * __this, String_t* ___role_id0, String_t* ___role_name1, String_t* ___role_level2, String_t* ___gold3, String_t* ___vipLv4, String_t* ___server_id5, String_t* ___server_name6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFastPlay::pay(System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginFastPlay_pay_m1664903092 (PluginFastPlay_t3320186403 * __this, String_t* ___orderId0, String_t* ___productName1, String_t* ___productId2, String_t* ___price3, String_t* ___extra4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFastPlay::<OnLogoutSuccess>m__41A()
extern "C"  void PluginFastPlay_U3COnLogoutSuccessU3Em__41A_m2216456109 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFastPlay::<OnLogout>m__41B()
extern "C"  void PluginFastPlay_U3COnLogoutU3Em__41B_m4160063037 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFastPlay::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginFastPlay_ilo_AddEventListener1_m2288162844 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginFastPlay::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginFastPlay_ilo_get_Instance2_m61330970 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginFastPlay::ilo_get_DeviceID3(PluginsSdkMgr)
extern "C"  String_t* PluginFastPlay_ilo_get_DeviceID3_m2924534427 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFastPlay::ilo_get_isSdkLogin4(VersionMgr)
extern "C"  bool PluginFastPlay_ilo_get_isSdkLogin4_m905714401 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFastPlay::ilo_LogError5(System.Object,System.Boolean)
extern "C"  void PluginFastPlay_ilo_LogError5_m2860375709 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProductsCfgMgr PluginFastPlay::ilo_get_ProductsCfgMgr6()
extern "C"  ProductsCfgMgr_t2493714872 * PluginFastPlay_ilo_get_ProductsCfgMgr6_m3409929267 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginFastPlay::ilo_GetProductID7(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginFastPlay_ilo_GetProductID7_m2871423751 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFastPlay::ilo_pay8(PluginFastPlay,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginFastPlay_ilo_pay8_m2594894136 (Il2CppObject * __this /* static, unused */, PluginFastPlay_t3320186403 * ____this0, String_t* ___orderId1, String_t* ___productName2, String_t* ___productId3, String_t* ___price4, String_t* ___extra5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFastPlay::ilo_Log9(System.Object,System.Boolean)
extern "C"  void PluginFastPlay_ilo_Log9_m390197373 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTextMgr PluginFastPlay::ilo_get_FloatTextMgr10()
extern "C"  FloatTextMgr_t630384591 * PluginFastPlay_ilo_get_FloatTextMgr10_m1931046148 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.EnterGameInfo PluginFastPlay::ilo_Parse11(System.Object[])
extern "C"  EnterGameInfo_t1897532954 * PluginFastPlay_ilo_Parse11_m4165240705 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFastPlay::ilo_creatRole12(PluginFastPlay,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginFastPlay_ilo_creatRole12_m3419482596 (Il2CppObject * __this /* static, unused */, PluginFastPlay_t3320186403 * ____this0, String_t* ___role_id1, String_t* ___role_name2, String_t* ___role_level3, String_t* ___gold4, String_t* ___vipLv5, String_t* ___server_id6, String_t* ___server_name7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.LvUpInfo PluginFastPlay::ilo_Parse13(System.Object[])
extern "C"  LvUpInfo_t411687177 * PluginFastPlay_ilo_Parse13_m402358682 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFastPlay::ilo_roleUpgrade14(PluginFastPlay,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginFastPlay_ilo_roleUpgrade14_m1019017101 (Il2CppObject * __this /* static, unused */, PluginFastPlay_t3320186403 * ____this0, String_t* ___role_id1, String_t* ___role_name2, String_t* ___role_level3, String_t* ___gold4, String_t* ___vipLv5, String_t* ___server_id6, String_t* ___server_name7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginFastPlay::ilo_get_PluginsSdkMgr15()
extern "C"  PluginsSdkMgr_t3884624670 * PluginFastPlay_ilo_get_PluginsSdkMgr15_m1240876615 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFastPlay::ilo_logout16(PluginFastPlay)
extern "C"  void PluginFastPlay_ilo_logout16_m3261532843 (Il2CppObject * __this /* static, unused */, PluginFastPlay_t3320186403 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
