﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_StoryMgr_FunType304731181.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StoryMgr/StoryVO
struct  StoryVO_t3225531714  : public Il2CppObject
{
public:
	// System.Int32 StoryMgr/StoryVO::id
	int32_t ___id_0;
	// StoryMgr/FunType StoryMgr/StoryVO::funType
	int32_t ___funType_1;
	// System.Int32 StoryMgr/StoryVO::guideID
	int32_t ___guideID_2;
	// System.Boolean StoryMgr/StoryVO::isHero
	bool ___isHero_3;
	// System.Int32 StoryMgr/StoryVO::tempID
	int32_t ___tempID_4;
	// System.Int32 StoryMgr/StoryVO::skillID
	int32_t ___skillID_5;
	// System.String StoryMgr/StoryVO::actionName
	String_t* ___actionName_6;
	// System.Single StoryMgr/StoryVO::actionTime
	float ___actionTime_7;
	// System.Single StoryMgr/StoryVO::rotY
	float ___rotY_8;
	// System.Single StoryMgr/StoryVO::rotX
	float ___rotX_9;
	// System.Single StoryMgr/StoryVO::dis
	float ___dis_10;
	// System.Single StoryMgr/StoryVO::fov
	float ___fov_11;
	// System.String StoryMgr/StoryVO::objName
	String_t* ___objName_12;
	// System.Boolean StoryMgr/StoryVO::isMove
	bool ___isMove_13;
	// System.Single StoryMgr/StoryVO::moveTime
	float ___moveTime_14;
	// System.Single StoryMgr/StoryVO::scaleTime
	float ___scaleTime_15;
	// System.Single StoryMgr/StoryVO::formScale
	float ___formScale_16;
	// System.Single StoryMgr/StoryVO::toScale
	float ___toScale_17;
	// System.Single StoryMgr/StoryVO::ssFps
	float ___ssFps_18;
	// System.Single StoryMgr/StoryVO::ssTime
	float ___ssTime_19;
	// System.Single StoryMgr/StoryVO::hpValue
	float ___hpValue_20;
	// System.Single StoryMgr/StoryVO::timeScale
	float ___timeScale_21;
	// System.Int32 StoryMgr/StoryVO::npcID
	int32_t ___npcID_22;
	// System.Int32 StoryMgr/StoryVO::initBuffID
	int32_t ___initBuffID_23;
	// System.Int32 StoryMgr/StoryVO::buffID
	int32_t ___buffID_24;
	// System.Int32 StoryMgr/StoryVO::effID
	int32_t ___effID_25;
	// System.Int32 StoryMgr/StoryVO::rage
	int32_t ___rage_26;
	// System.Single StoryMgr/StoryVO::waitTime
	float ___waitTime_27;
	// System.Int32 StoryMgr/StoryVO::plotID
	int32_t ___plotID_28;
	// System.Boolean StoryMgr/StoryVO::isPauseFB
	bool ___isPauseFB_29;
	// UnityEngine.Vector3 StoryMgr/StoryVO::pos
	Vector3_t4282066566  ___pos_30;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_funType_1() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___funType_1)); }
	inline int32_t get_funType_1() const { return ___funType_1; }
	inline int32_t* get_address_of_funType_1() { return &___funType_1; }
	inline void set_funType_1(int32_t value)
	{
		___funType_1 = value;
	}

	inline static int32_t get_offset_of_guideID_2() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___guideID_2)); }
	inline int32_t get_guideID_2() const { return ___guideID_2; }
	inline int32_t* get_address_of_guideID_2() { return &___guideID_2; }
	inline void set_guideID_2(int32_t value)
	{
		___guideID_2 = value;
	}

	inline static int32_t get_offset_of_isHero_3() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___isHero_3)); }
	inline bool get_isHero_3() const { return ___isHero_3; }
	inline bool* get_address_of_isHero_3() { return &___isHero_3; }
	inline void set_isHero_3(bool value)
	{
		___isHero_3 = value;
	}

	inline static int32_t get_offset_of_tempID_4() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___tempID_4)); }
	inline int32_t get_tempID_4() const { return ___tempID_4; }
	inline int32_t* get_address_of_tempID_4() { return &___tempID_4; }
	inline void set_tempID_4(int32_t value)
	{
		___tempID_4 = value;
	}

	inline static int32_t get_offset_of_skillID_5() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___skillID_5)); }
	inline int32_t get_skillID_5() const { return ___skillID_5; }
	inline int32_t* get_address_of_skillID_5() { return &___skillID_5; }
	inline void set_skillID_5(int32_t value)
	{
		___skillID_5 = value;
	}

	inline static int32_t get_offset_of_actionName_6() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___actionName_6)); }
	inline String_t* get_actionName_6() const { return ___actionName_6; }
	inline String_t** get_address_of_actionName_6() { return &___actionName_6; }
	inline void set_actionName_6(String_t* value)
	{
		___actionName_6 = value;
		Il2CppCodeGenWriteBarrier(&___actionName_6, value);
	}

	inline static int32_t get_offset_of_actionTime_7() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___actionTime_7)); }
	inline float get_actionTime_7() const { return ___actionTime_7; }
	inline float* get_address_of_actionTime_7() { return &___actionTime_7; }
	inline void set_actionTime_7(float value)
	{
		___actionTime_7 = value;
	}

	inline static int32_t get_offset_of_rotY_8() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___rotY_8)); }
	inline float get_rotY_8() const { return ___rotY_8; }
	inline float* get_address_of_rotY_8() { return &___rotY_8; }
	inline void set_rotY_8(float value)
	{
		___rotY_8 = value;
	}

	inline static int32_t get_offset_of_rotX_9() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___rotX_9)); }
	inline float get_rotX_9() const { return ___rotX_9; }
	inline float* get_address_of_rotX_9() { return &___rotX_9; }
	inline void set_rotX_9(float value)
	{
		___rotX_9 = value;
	}

	inline static int32_t get_offset_of_dis_10() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___dis_10)); }
	inline float get_dis_10() const { return ___dis_10; }
	inline float* get_address_of_dis_10() { return &___dis_10; }
	inline void set_dis_10(float value)
	{
		___dis_10 = value;
	}

	inline static int32_t get_offset_of_fov_11() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___fov_11)); }
	inline float get_fov_11() const { return ___fov_11; }
	inline float* get_address_of_fov_11() { return &___fov_11; }
	inline void set_fov_11(float value)
	{
		___fov_11 = value;
	}

	inline static int32_t get_offset_of_objName_12() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___objName_12)); }
	inline String_t* get_objName_12() const { return ___objName_12; }
	inline String_t** get_address_of_objName_12() { return &___objName_12; }
	inline void set_objName_12(String_t* value)
	{
		___objName_12 = value;
		Il2CppCodeGenWriteBarrier(&___objName_12, value);
	}

	inline static int32_t get_offset_of_isMove_13() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___isMove_13)); }
	inline bool get_isMove_13() const { return ___isMove_13; }
	inline bool* get_address_of_isMove_13() { return &___isMove_13; }
	inline void set_isMove_13(bool value)
	{
		___isMove_13 = value;
	}

	inline static int32_t get_offset_of_moveTime_14() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___moveTime_14)); }
	inline float get_moveTime_14() const { return ___moveTime_14; }
	inline float* get_address_of_moveTime_14() { return &___moveTime_14; }
	inline void set_moveTime_14(float value)
	{
		___moveTime_14 = value;
	}

	inline static int32_t get_offset_of_scaleTime_15() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___scaleTime_15)); }
	inline float get_scaleTime_15() const { return ___scaleTime_15; }
	inline float* get_address_of_scaleTime_15() { return &___scaleTime_15; }
	inline void set_scaleTime_15(float value)
	{
		___scaleTime_15 = value;
	}

	inline static int32_t get_offset_of_formScale_16() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___formScale_16)); }
	inline float get_formScale_16() const { return ___formScale_16; }
	inline float* get_address_of_formScale_16() { return &___formScale_16; }
	inline void set_formScale_16(float value)
	{
		___formScale_16 = value;
	}

	inline static int32_t get_offset_of_toScale_17() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___toScale_17)); }
	inline float get_toScale_17() const { return ___toScale_17; }
	inline float* get_address_of_toScale_17() { return &___toScale_17; }
	inline void set_toScale_17(float value)
	{
		___toScale_17 = value;
	}

	inline static int32_t get_offset_of_ssFps_18() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___ssFps_18)); }
	inline float get_ssFps_18() const { return ___ssFps_18; }
	inline float* get_address_of_ssFps_18() { return &___ssFps_18; }
	inline void set_ssFps_18(float value)
	{
		___ssFps_18 = value;
	}

	inline static int32_t get_offset_of_ssTime_19() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___ssTime_19)); }
	inline float get_ssTime_19() const { return ___ssTime_19; }
	inline float* get_address_of_ssTime_19() { return &___ssTime_19; }
	inline void set_ssTime_19(float value)
	{
		___ssTime_19 = value;
	}

	inline static int32_t get_offset_of_hpValue_20() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___hpValue_20)); }
	inline float get_hpValue_20() const { return ___hpValue_20; }
	inline float* get_address_of_hpValue_20() { return &___hpValue_20; }
	inline void set_hpValue_20(float value)
	{
		___hpValue_20 = value;
	}

	inline static int32_t get_offset_of_timeScale_21() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___timeScale_21)); }
	inline float get_timeScale_21() const { return ___timeScale_21; }
	inline float* get_address_of_timeScale_21() { return &___timeScale_21; }
	inline void set_timeScale_21(float value)
	{
		___timeScale_21 = value;
	}

	inline static int32_t get_offset_of_npcID_22() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___npcID_22)); }
	inline int32_t get_npcID_22() const { return ___npcID_22; }
	inline int32_t* get_address_of_npcID_22() { return &___npcID_22; }
	inline void set_npcID_22(int32_t value)
	{
		___npcID_22 = value;
	}

	inline static int32_t get_offset_of_initBuffID_23() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___initBuffID_23)); }
	inline int32_t get_initBuffID_23() const { return ___initBuffID_23; }
	inline int32_t* get_address_of_initBuffID_23() { return &___initBuffID_23; }
	inline void set_initBuffID_23(int32_t value)
	{
		___initBuffID_23 = value;
	}

	inline static int32_t get_offset_of_buffID_24() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___buffID_24)); }
	inline int32_t get_buffID_24() const { return ___buffID_24; }
	inline int32_t* get_address_of_buffID_24() { return &___buffID_24; }
	inline void set_buffID_24(int32_t value)
	{
		___buffID_24 = value;
	}

	inline static int32_t get_offset_of_effID_25() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___effID_25)); }
	inline int32_t get_effID_25() const { return ___effID_25; }
	inline int32_t* get_address_of_effID_25() { return &___effID_25; }
	inline void set_effID_25(int32_t value)
	{
		___effID_25 = value;
	}

	inline static int32_t get_offset_of_rage_26() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___rage_26)); }
	inline int32_t get_rage_26() const { return ___rage_26; }
	inline int32_t* get_address_of_rage_26() { return &___rage_26; }
	inline void set_rage_26(int32_t value)
	{
		___rage_26 = value;
	}

	inline static int32_t get_offset_of_waitTime_27() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___waitTime_27)); }
	inline float get_waitTime_27() const { return ___waitTime_27; }
	inline float* get_address_of_waitTime_27() { return &___waitTime_27; }
	inline void set_waitTime_27(float value)
	{
		___waitTime_27 = value;
	}

	inline static int32_t get_offset_of_plotID_28() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___plotID_28)); }
	inline int32_t get_plotID_28() const { return ___plotID_28; }
	inline int32_t* get_address_of_plotID_28() { return &___plotID_28; }
	inline void set_plotID_28(int32_t value)
	{
		___plotID_28 = value;
	}

	inline static int32_t get_offset_of_isPauseFB_29() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___isPauseFB_29)); }
	inline bool get_isPauseFB_29() const { return ___isPauseFB_29; }
	inline bool* get_address_of_isPauseFB_29() { return &___isPauseFB_29; }
	inline void set_isPauseFB_29(bool value)
	{
		___isPauseFB_29 = value;
	}

	inline static int32_t get_offset_of_pos_30() { return static_cast<int32_t>(offsetof(StoryVO_t3225531714, ___pos_30)); }
	inline Vector3_t4282066566  get_pos_30() const { return ___pos_30; }
	inline Vector3_t4282066566 * get_address_of_pos_30() { return &___pos_30; }
	inline void set_pos_30(Vector3_t4282066566  value)
	{
		___pos_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
