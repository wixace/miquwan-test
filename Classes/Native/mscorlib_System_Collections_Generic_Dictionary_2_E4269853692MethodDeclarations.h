﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>
struct Dictionary_2_t2952530300;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4269853692.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22851311006.h"
#include "AssemblyU2DCSharp_UIModelDisplayType1891752679.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1938982213_gshared (Enumerator_t4269853692 * __this, Dictionary_2_t2952530300 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m1938982213(__this, ___dictionary0, method) ((  void (*) (Enumerator_t4269853692 *, Dictionary_2_t2952530300 *, const MethodInfo*))Enumerator__ctor_m1938982213_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m472833660_gshared (Enumerator_t4269853692 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m472833660(__this, method) ((  Il2CppObject * (*) (Enumerator_t4269853692 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m472833660_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3176180304_gshared (Enumerator_t4269853692 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3176180304(__this, method) ((  void (*) (Enumerator_t4269853692 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3176180304_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2214297689_gshared (Enumerator_t4269853692 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2214297689(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t4269853692 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2214297689_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3592703576_gshared (Enumerator_t4269853692 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3592703576(__this, method) ((  Il2CppObject * (*) (Enumerator_t4269853692 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3592703576_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m482866922_gshared (Enumerator_t4269853692 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m482866922(__this, method) ((  Il2CppObject * (*) (Enumerator_t4269853692 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m482866922_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m716798012_gshared (Enumerator_t4269853692 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m716798012(__this, method) ((  bool (*) (Enumerator_t4269853692 *, const MethodInfo*))Enumerator_MoveNext_m716798012_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Int32>::get_Current()
extern "C"  KeyValuePair_2_t2851311006  Enumerator_get_Current_m3359458164_gshared (Enumerator_t4269853692 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3359458164(__this, method) ((  KeyValuePair_2_t2851311006  (*) (Enumerator_t4269853692 *, const MethodInfo*))Enumerator_get_Current_m3359458164_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Int32>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m804079113_gshared (Enumerator_t4269853692 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m804079113(__this, method) ((  int32_t (*) (Enumerator_t4269853692 *, const MethodInfo*))Enumerator_get_CurrentKey_m804079113_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Int32>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m2948379501_gshared (Enumerator_t4269853692 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m2948379501(__this, method) ((  int32_t (*) (Enumerator_t4269853692 *, const MethodInfo*))Enumerator_get_CurrentValue_m2948379501_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Int32>::Reset()
extern "C"  void Enumerator_Reset_m2137589783_gshared (Enumerator_t4269853692 * __this, const MethodInfo* method);
#define Enumerator_Reset_m2137589783(__this, method) ((  void (*) (Enumerator_t4269853692 *, const MethodInfo*))Enumerator_Reset_m2137589783_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Int32>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2974132960_gshared (Enumerator_t4269853692 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2974132960(__this, method) ((  void (*) (Enumerator_t4269853692 *, const MethodInfo*))Enumerator_VerifyState_m2974132960_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Int32>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m438427208_gshared (Enumerator_t4269853692 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m438427208(__this, method) ((  void (*) (Enumerator_t4269853692 *, const MethodInfo*))Enumerator_VerifyCurrent_m438427208_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m3752843047_gshared (Enumerator_t4269853692 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3752843047(__this, method) ((  void (*) (Enumerator_t4269853692 *, const MethodInfo*))Enumerator_Dispose_m3752843047_gshared)(__this, method)
