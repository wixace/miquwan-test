﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.PathUtilities/<BFS>c__AnonStorey126
struct U3CBFSU3Ec__AnonStorey126_t1732117893;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.PathUtilities/<BFS>c__AnonStorey126::.ctor()
extern "C"  void U3CBFSU3Ec__AnonStorey126__ctor_m554796934 (U3CBFSU3Ec__AnonStorey126_t1732117893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PathUtilities/<BFS>c__AnonStorey126::<>m__35D(Pathfinding.GraphNode)
extern "C"  void U3CBFSU3Ec__AnonStorey126_U3CU3Em__35D_m191319415 (U3CBFSU3Ec__AnonStorey126_t1732117893 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PathUtilities/<BFS>c__AnonStorey126::<>m__35E(Pathfinding.GraphNode)
extern "C"  void U3CBFSU3Ec__AnonStorey126_U3CU3Em__35E_m124312662 (U3CBFSU3Ec__AnonStorey126_t1732117893 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
