﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._270c56bceeed90b22e94469631975949
struct _270c56bceeed90b22e94469631975949_t3655011435;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._270c56bceeed90b22e94469631975949::.ctor()
extern "C"  void _270c56bceeed90b22e94469631975949__ctor_m1413997090 (_270c56bceeed90b22e94469631975949_t3655011435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._270c56bceeed90b22e94469631975949::_270c56bceeed90b22e94469631975949m2(System.Int32)
extern "C"  int32_t _270c56bceeed90b22e94469631975949__270c56bceeed90b22e94469631975949m2_m13361593 (_270c56bceeed90b22e94469631975949_t3655011435 * __this, int32_t ____270c56bceeed90b22e94469631975949a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._270c56bceeed90b22e94469631975949::_270c56bceeed90b22e94469631975949m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _270c56bceeed90b22e94469631975949__270c56bceeed90b22e94469631975949m_m2242717981 (_270c56bceeed90b22e94469631975949_t3655011435 * __this, int32_t ____270c56bceeed90b22e94469631975949a0, int32_t ____270c56bceeed90b22e94469631975949461, int32_t ____270c56bceeed90b22e94469631975949c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
