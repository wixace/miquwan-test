﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.Compression.RangeCoder.Decoder
struct Decoder_t1102840654;
// SevenZip.Compression.RangeCoder.BitDecoder
struct BitDecoder_t4202293321;
struct BitDecoder_t4202293321_marshaled_pinvoke;
struct BitDecoder_t4202293321_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_4202293321.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_1102840654.h"

// System.Void SevenZip.Compression.RangeCoder.BitDecoder::UpdateModel(System.Int32,System.UInt32)
extern "C"  void BitDecoder_UpdateModel_m2419711331 (BitDecoder_t4202293321 * __this, int32_t ___numMoveBits0, uint32_t ___symbol1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.BitDecoder::Init()
extern "C"  void BitDecoder_Init_m3837451380 (BitDecoder_t4202293321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.RangeCoder.BitDecoder::Decode(SevenZip.Compression.RangeCoder.Decoder)
extern "C"  uint32_t BitDecoder_Decode_m3109661189 (BitDecoder_t4202293321 * __this, Decoder_t1102840654 * ___rangeDecoder0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct BitDecoder_t4202293321;
struct BitDecoder_t4202293321_marshaled_pinvoke;

extern "C" void BitDecoder_t4202293321_marshal_pinvoke(const BitDecoder_t4202293321& unmarshaled, BitDecoder_t4202293321_marshaled_pinvoke& marshaled);
extern "C" void BitDecoder_t4202293321_marshal_pinvoke_back(const BitDecoder_t4202293321_marshaled_pinvoke& marshaled, BitDecoder_t4202293321& unmarshaled);
extern "C" void BitDecoder_t4202293321_marshal_pinvoke_cleanup(BitDecoder_t4202293321_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct BitDecoder_t4202293321;
struct BitDecoder_t4202293321_marshaled_com;

extern "C" void BitDecoder_t4202293321_marshal_com(const BitDecoder_t4202293321& unmarshaled, BitDecoder_t4202293321_marshaled_com& marshaled);
extern "C" void BitDecoder_t4202293321_marshal_com_back(const BitDecoder_t4202293321_marshaled_com& marshaled, BitDecoder_t4202293321& unmarshaled);
extern "C" void BitDecoder_t4202293321_marshal_com_cleanup(BitDecoder_t4202293321_marshaled_com& marshaled);
