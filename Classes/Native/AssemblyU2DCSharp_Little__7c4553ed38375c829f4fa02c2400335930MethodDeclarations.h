﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._7c4553ed38375c829f4fa02ccfc8c0ac
struct _7c4553ed38375c829f4fa02ccfc8c0ac_t2400335930;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._7c4553ed38375c829f4fa02ccfc8c0ac::.ctor()
extern "C"  void _7c4553ed38375c829f4fa02ccfc8c0ac__ctor_m1783861043 (_7c4553ed38375c829f4fa02ccfc8c0ac_t2400335930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._7c4553ed38375c829f4fa02ccfc8c0ac::_7c4553ed38375c829f4fa02ccfc8c0acm2(System.Int32)
extern "C"  int32_t _7c4553ed38375c829f4fa02ccfc8c0ac__7c4553ed38375c829f4fa02ccfc8c0acm2_m4079179609 (_7c4553ed38375c829f4fa02ccfc8c0ac_t2400335930 * __this, int32_t ____7c4553ed38375c829f4fa02ccfc8c0aca0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._7c4553ed38375c829f4fa02ccfc8c0ac::_7c4553ed38375c829f4fa02ccfc8c0acm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _7c4553ed38375c829f4fa02ccfc8c0ac__7c4553ed38375c829f4fa02ccfc8c0acm_m2382841725 (_7c4553ed38375c829f4fa02ccfc8c0ac_t2400335930 * __this, int32_t ____7c4553ed38375c829f4fa02ccfc8c0aca0, int32_t ____7c4553ed38375c829f4fa02ccfc8c0ac551, int32_t ____7c4553ed38375c829f4fa02ccfc8c0acc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
