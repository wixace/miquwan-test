﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Voxels.VoxelArea
struct VoxelArea_t3943332841;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_VoxelArea3943332841.h"

// System.Void Pathfinding.Voxels.VoxelArea::.ctor(System.Int32,System.Int32)
extern "C"  void VoxelArea__ctor_m1880086823 (VoxelArea_t3943332841 * __this, int32_t ___width0, int32_t ___depth1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.VoxelArea::Reset()
extern "C"  void VoxelArea_Reset_m3601960332 (VoxelArea_t3943332841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.VoxelArea::ResetLinkedVoxelSpans()
extern "C"  void VoxelArea_ResetLinkedVoxelSpans_m2004582376 (VoxelArea_t3943332841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.VoxelArea::GetSpanCountAll()
extern "C"  int32_t VoxelArea_GetSpanCountAll_m1069131425 (VoxelArea_t3943332841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.VoxelArea::GetSpanCount()
extern "C"  int32_t VoxelArea_GetSpanCount_m1683651842 (VoxelArea_t3943332841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.VoxelArea::AddLinkedSpan(System.Int32,System.UInt32,System.UInt32,System.Int32,System.Int32)
extern "C"  void VoxelArea_AddLinkedSpan_m3817787034 (VoxelArea_t3943332841 * __this, int32_t ___index0, uint32_t ___bottom1, uint32_t ___top2, int32_t ___area3, int32_t ___voxelWalkableClimb4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.VoxelArea::ilo_ResetLinkedVoxelSpans1(Pathfinding.Voxels.VoxelArea)
extern "C"  void VoxelArea_ilo_ResetLinkedVoxelSpans1_m3462304994 (Il2CppObject * __this /* static, unused */, VoxelArea_t3943332841 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
