﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// checkpointCfgGenerated
struct checkpointCfgGenerated_t1971521331;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"

// System.Void checkpointCfgGenerated::.ctor()
extern "C"  void checkpointCfgGenerated__ctor_m1502779080 (checkpointCfgGenerated_t1971521331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean checkpointCfgGenerated::checkpointCfg_checkpointCfg1(JSVCall,System.Int32)
extern "C"  bool checkpointCfgGenerated_checkpointCfg_checkpointCfg1_m3065765752 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_id(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_id_m1226915981 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_mapID(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_mapID_m1541200621 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_name(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_name_m1878821021 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_sceneName(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_sceneName_m29821485 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_iconType(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_iconType_m3136133077 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_LevelIcon(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_LevelIcon_m3609026599 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_localPos(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_localPos_m2032794879 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_Hatred(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_Hatred_m3007685330 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_Super_Skill_Limit(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_Super_Skill_Limit_m1683374203 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_attack_effect(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_attack_effect_m4240210972 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_workpoint(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_workpoint_m3641048901 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_preloadUIID(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_preloadUIID_m953955532 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_type(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_type_m2120501454 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_panelType(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_panelType_m329251814 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_finishType(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_finishType_m2895165819 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_effect_nf(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_effect_nf_m170370334 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_ifAppear(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_ifAppear_m683261174 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_time(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_time_m1151064731 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_action(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_action_m1409101010 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_exp(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_exp_m986864263 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_heroExp(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_heroExp_m1211164833 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_copper(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_copper_m438282575 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_brushEnemy(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_brushEnemy_m2619096218 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_passType(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_passType_m4247881533 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_passValue(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_passValue_m2642622884 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_loseValue(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_loseValue_m176555432 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_passAnimation(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_passAnimation_m4099442033 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_delayFinishTime(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_delayFinishTime_m3175323265 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_passCameraAnimation(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_passCameraAnimation_m3386258998 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_npcArr(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_npcArr_m3787679368 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_rewardArr(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_rewardArr_m2083091730 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_dropId(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_dropId_m2248781086 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_firsttimedrop(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_firsttimedrop_m174637592 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_firsttimedropnum(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_firsttimedropnum_m1742299886 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_sweep_lv(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_sweep_lv_m1093598891 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_swipedrop(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_swipedrop_m873101947 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_swipedropcount(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_swipedropcount_m1585586562 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_hidestartext(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_hidestartext_m2287046695 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_gradeType1(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_gradeType1_m2189515432 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_gradeValue1(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_gradeValue1_m314164301 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_gradetext1(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_gradetext1_m317829307 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_gradetext1_lose(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_gradetext1_lose_m1642239485 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_gradeType2(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_gradeType2_m1993001927 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_gradeValue2(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_gradeValue2_m117650796 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_gradetext2(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_gradetext2_m121315802 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_gradetext2_lose(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_gradetext2_lose_m3149791294 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_gradeType3(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_gradeType3_m1796488422 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_gradeValue3(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_gradeValue3_m4216104587 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_gradetext3(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_gradetext3_m4219769593 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_gradetext3_lose(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_gradetext3_lose_m362375807 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_Plot_off(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_Plot_off_m699774903 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_enterPlotId(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_enterPlotId_m663425456 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_passPlotId(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_passPlotId_m2422071963 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_rewards(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_rewards_m189175392 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_des(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_des_m50578482 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_AI(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_AI_m1101423648 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_ifAuto(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_ifAuto_m2104246108 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_ifMotion(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_ifMotion_m2840829941 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_ifStartAuto(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_ifStartAuto_m1828733936 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_Formation(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_Formation_m881649683 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_MusicID(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_MusicID_m4203079460 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_bossAnimationId(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_bossAnimationId_m4213829938 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_isCameraSlow(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_isCameraSlow_m1635128 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_Camera_distance(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_Camera_distance_m1627344949 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_camera_if_move(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_camera_if_move_m2313388495 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_isCameraAnimation(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_isCameraAnimation_m1510140879 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_Camera_distance_outattack(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_Camera_distance_outattack_m2818438590 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_Camera_alternate_time(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_Camera_alternate_time_m1343692376 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_Camera_time(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_Camera_time_m1243012701 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_Camera_end(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_Camera_end_m2475068807 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_Camera_rx(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_Camera_rx_m2406837252 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_Camera_rx__outattack(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_Camera_rx__outattack_m2587432402 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_Camera_ry(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_Camera_ry_m2210323747 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_Camera_posDamping(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_Camera_posDamping_m1110514492 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_Camera_MoveMax(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_Camera_MoveMax_m2086698927 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_Camera_waittime(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_Camera_waittime_m749334792 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_Camera_disMove(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_Camera_disMove_m3193713507 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_IsExtryHero(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_IsExtryHero_m1962107628 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_hero_Pos(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_hero_Pos_m3894724153 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_hero_Px(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_hero_Px_m1858817815 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_moster_Pos(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_moster_Pos_m3761367555 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_moster_Px(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_moster_Px_m1577421325 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_friendNpc(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_friendNpc_m3520979329 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_power(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_power_m1890711839 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_maxSweepCount(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_maxSweepCount_m3126258781 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_maxCopper(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_maxCopper_m521090695 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_need_checkpoint_id(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_need_checkpoint_id_m905392671 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_need_normal_checkpoint_id(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_need_normal_checkpoint_id_m4104783617 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_need_role_level(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_need_role_level_m403128768 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_bigType(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_bigType_m4087491050 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_isAutoCreatNpc(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_isAutoCreatNpc_m247661751 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_bossId(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_bossId_m4258956768 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_isSpeedup(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_isSpeedup_m3780069356 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_largeTip(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_largeTip_m4046893672 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_isTop(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_isTop_m3636464921 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_monsterwavecount(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_monsterwavecount_m1892492332 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_islvrepress(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_islvrepress_m4253627976 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_isShowNew(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_isShowNew_m1623654603 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_loc1_anger(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_loc1_anger_m3376973359 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_loc2_anger(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_loc2_anger_m2866439182 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_loc3_anger(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_loc3_anger_m2355905005 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_loc4_anger(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_loc4_anger_m1845370828 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_boxID(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_boxID_m4027365150 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_treasure_pos(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_treasure_pos_m4093639812 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_is_superskill_prompt(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_is_superskill_prompt_m180274064 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_ifUseHint(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_ifUseHint_m2045402067 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_hintText(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_hintText_m43550708 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_guide_or_not(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_guide_or_not_m256124142 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_chapter_id(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_chapter_id_m1784950171 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_energy_rate(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_energy_rate_m642292653 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_wait_time(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_wait_time_m1844720653 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_item_id(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_item_id_m3012016925 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_is_time_not(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_is_time_not_m3208369390 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_camera_limit(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_camera_limit_m2120353735 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_layer(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_layer_m3983786579 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_Is_Lead_skill(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_Is_Lead_skill_m4243001345 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_Is_relief(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_Is_relief_m1753485342 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::checkpointCfg_Is_close(JSVCall)
extern "C"  void checkpointCfgGenerated_checkpointCfg_Is_close_m983017061 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean checkpointCfgGenerated::checkpointCfg_Init__DictionaryT2_String_String(JSVCall,System.Int32)
extern "C"  bool checkpointCfgGenerated_checkpointCfg_Init__DictionaryT2_String_String_m2276221287 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::__Register()
extern "C"  void checkpointCfgGenerated___Register_m2206589695 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 checkpointCfgGenerated::ilo_getInt321(System.Int32)
extern "C"  int32_t checkpointCfgGenerated_ilo_getInt321_m4012988815 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::ilo_setInt322(System.Int32,System.Int32)
extern "C"  void checkpointCfgGenerated_ilo_setInt322_m719977837 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::ilo_setStringS3(System.Int32,System.String)
extern "C"  void checkpointCfgGenerated_ilo_setStringS3_m3660902035 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String checkpointCfgGenerated::ilo_getStringS4(System.Int32)
extern "C"  String_t* checkpointCfgGenerated_ilo_getStringS4_m3972677257 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void checkpointCfgGenerated_ilo_setSingle5_m4018167472 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single checkpointCfgGenerated::ilo_getSingle6(System.Int32)
extern "C"  float checkpointCfgGenerated_ilo_getSingle6_m3880307108 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfgGenerated::ilo_setBooleanS7(System.Int32,System.Boolean)
extern "C"  void checkpointCfgGenerated_ilo_setBooleanS7_m2316216563 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean checkpointCfgGenerated::ilo_getBooleanS8(System.Int32)
extern "C"  bool checkpointCfgGenerated_ilo_getBooleanS8_m1560744395 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
