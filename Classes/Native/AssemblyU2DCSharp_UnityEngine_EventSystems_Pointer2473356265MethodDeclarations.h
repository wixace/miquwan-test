﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_PointerInputModuleGenerated
struct UnityEngine_EventSystems_PointerInputModuleGenerated_t2473356265;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"

// System.Void UnityEngine_EventSystems_PointerInputModuleGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_PointerInputModuleGenerated__ctor_m919463122 (UnityEngine_EventSystems_PointerInputModuleGenerated_t2473356265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerInputModuleGenerated::PointerInputModule_kMouseLeftId(JSVCall)
extern "C"  void UnityEngine_EventSystems_PointerInputModuleGenerated_PointerInputModule_kMouseLeftId_m1760387957 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerInputModuleGenerated::PointerInputModule_kMouseRightId(JSVCall)
extern "C"  void UnityEngine_EventSystems_PointerInputModuleGenerated_PointerInputModule_kMouseRightId_m424421886 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerInputModuleGenerated::PointerInputModule_kMouseMiddleId(JSVCall)
extern "C"  void UnityEngine_EventSystems_PointerInputModuleGenerated_PointerInputModule_kMouseMiddleId_m5782759 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerInputModuleGenerated::PointerInputModule_kFakeTouchesId(JSVCall)
extern "C"  void UnityEngine_EventSystems_PointerInputModuleGenerated_PointerInputModule_kFakeTouchesId_m2787276201 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_PointerInputModuleGenerated::PointerInputModule_IsPointerOverGameObject__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_PointerInputModuleGenerated_PointerInputModule_IsPointerOverGameObject__Int32_m2289329352 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_PointerInputModuleGenerated::PointerInputModule_ToString(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_PointerInputModuleGenerated_PointerInputModule_ToString_m2663981118 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerInputModuleGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_PointerInputModuleGenerated___Register_m3728460597 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerInputModuleGenerated::ilo_setInt321(System.Int32,System.Int32)
extern "C"  void UnityEngine_EventSystems_PointerInputModuleGenerated_ilo_setInt321_m1094521156 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_EventSystems_PointerInputModuleGenerated::ilo_getInt322(System.Int32)
extern "C"  int32_t UnityEngine_EventSystems_PointerInputModuleGenerated_ilo_getInt322_m2587985242 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PointerInputModuleGenerated::ilo_setStringS3(System.Int32,System.String)
extern "C"  void UnityEngine_EventSystems_PointerInputModuleGenerated_ilo_setStringS3_m3706683293 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
