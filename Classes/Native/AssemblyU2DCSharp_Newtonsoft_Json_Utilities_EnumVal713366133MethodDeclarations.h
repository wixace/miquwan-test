﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.EnumValues`1<System.UInt64>
struct EnumValues_1_t713366133;
// System.String
struct String_t;
// Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>
struct EnumValue_1_t3851137816;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Utilities.EnumValues`1<System.UInt64>::.ctor()
extern "C"  void EnumValues_1__ctor_m1471327349_gshared (EnumValues_1_t713366133 * __this, const MethodInfo* method);
#define EnumValues_1__ctor_m1471327349(__this, method) ((  void (*) (EnumValues_1_t713366133 *, const MethodInfo*))EnumValues_1__ctor_m1471327349_gshared)(__this, method)
// System.String Newtonsoft.Json.Utilities.EnumValues`1<System.UInt64>::GetKeyForItem(Newtonsoft.Json.Utilities.EnumValue`1<T>)
extern "C"  String_t* EnumValues_1_GetKeyForItem_m2282326315_gshared (EnumValues_1_t713366133 * __this, EnumValue_1_t3851137816 * ___item0, const MethodInfo* method);
#define EnumValues_1_GetKeyForItem_m2282326315(__this, ___item0, method) ((  String_t* (*) (EnumValues_1_t713366133 *, EnumValue_1_t3851137816 *, const MethodInfo*))EnumValues_1_GetKeyForItem_m2282326315_gshared)(__this, ___item0, method)
