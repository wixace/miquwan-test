﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AstarPath/<FloodFill>c__AnonStorey104
struct U3CFloodFillU3Ec__AnonStorey104_t3497852784;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void AstarPath/<FloodFill>c__AnonStorey104::.ctor()
extern "C"  void U3CFloodFillU3Ec__AnonStorey104__ctor_m961251323 (U3CFloodFillU3Ec__AnonStorey104_t3497852784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AstarPath/<FloodFill>c__AnonStorey104::<>m__333(Pathfinding.GraphNode)
extern "C"  bool U3CFloodFillU3Ec__AnonStorey104_U3CU3Em__333_m3515539407 (U3CFloodFillU3Ec__AnonStorey104_t3497852784 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
