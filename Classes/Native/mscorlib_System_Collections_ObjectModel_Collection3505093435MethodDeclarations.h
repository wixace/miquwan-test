﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.UInt32>
struct Collection_1_t3505093435;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.UInt32[]
struct UInt32U5BU5D_t3230734560;
// System.Collections.Generic.IEnumerator`1<System.UInt32>
struct IEnumerator_1_t1936533030;
// System.Collections.Generic.IList`1<System.UInt32>
struct IList_1_t2719315184;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::.ctor()
extern "C"  void Collection_1__ctor_m2493402363_gshared (Collection_1_t3505093435 * __this, const MethodInfo* method);
#define Collection_1__ctor_m2493402363(__this, method) ((  void (*) (Collection_1_t3505093435 *, const MethodInfo*))Collection_1__ctor_m2493402363_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3831277856_gshared (Collection_1_t3505093435 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3831277856(__this, method) ((  bool (*) (Collection_1_t3505093435 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3831277856_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m2344746153_gshared (Collection_1_t3505093435 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m2344746153(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3505093435 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2344746153_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m11212388_gshared (Collection_1_t3505093435 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m11212388(__this, method) ((  Il2CppObject * (*) (Collection_1_t3505093435 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m11212388_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m2156891277_gshared (Collection_1_t3505093435 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m2156891277(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3505093435 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m2156891277_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m3243295711_gshared (Collection_1_t3505093435 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m3243295711(__this, ___value0, method) ((  bool (*) (Collection_1_t3505093435 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m3243295711_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m216839269_gshared (Collection_1_t3505093435 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m216839269(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3505093435 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m216839269_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m1892811280_gshared (Collection_1_t3505093435 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1892811280(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3505093435 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1892811280_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m4103572760_gshared (Collection_1_t3505093435 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m4103572760(__this, ___value0, method) ((  void (*) (Collection_1_t3505093435 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m4103572760_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1433211677_gshared (Collection_1_t3505093435 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1433211677(__this, method) ((  bool (*) (Collection_1_t3505093435 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1433211677_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m3168423561_gshared (Collection_1_t3505093435 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m3168423561(__this, method) ((  Il2CppObject * (*) (Collection_1_t3505093435 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m3168423561_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m750028430_gshared (Collection_1_t3505093435 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m750028430(__this, method) ((  bool (*) (Collection_1_t3505093435 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m750028430_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m755297963_gshared (Collection_1_t3505093435 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m755297963(__this, method) ((  bool (*) (Collection_1_t3505093435 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m755297963_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m788840912_gshared (Collection_1_t3505093435 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m788840912(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3505093435 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m788840912_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2834426343_gshared (Collection_1_t3505093435 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m2834426343(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3505093435 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2834426343_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::Add(T)
extern "C"  void Collection_1_Add_m1733142564_gshared (Collection_1_t3505093435 * __this, uint32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m1733142564(__this, ___item0, method) ((  void (*) (Collection_1_t3505093435 *, uint32_t, const MethodInfo*))Collection_1_Add_m1733142564_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::Clear()
extern "C"  void Collection_1_Clear_m4194502950_gshared (Collection_1_t3505093435 * __this, const MethodInfo* method);
#define Collection_1_Clear_m4194502950(__this, method) ((  void (*) (Collection_1_t3505093435 *, const MethodInfo*))Collection_1_Clear_m4194502950_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3284033436_gshared (Collection_1_t3505093435 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m3284033436(__this, method) ((  void (*) (Collection_1_t3505093435 *, const MethodInfo*))Collection_1_ClearItems_m3284033436_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt32>::Contains(T)
extern "C"  bool Collection_1_Contains_m3977973204_gshared (Collection_1_t3505093435 * __this, uint32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m3977973204(__this, ___item0, method) ((  bool (*) (Collection_1_t3505093435 *, uint32_t, const MethodInfo*))Collection_1_Contains_m3977973204_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m2538557204_gshared (Collection_1_t3505093435 * __this, UInt32U5BU5D_t3230734560* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m2538557204(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3505093435 *, UInt32U5BU5D_t3230734560*, int32_t, const MethodInfo*))Collection_1_CopyTo_m2538557204_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.UInt32>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m826518199_gshared (Collection_1_t3505093435 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m826518199(__this, method) ((  Il2CppObject* (*) (Collection_1_t3505093435 *, const MethodInfo*))Collection_1_GetEnumerator_m826518199_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.UInt32>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m3195870360_gshared (Collection_1_t3505093435 * __this, uint32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m3195870360(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3505093435 *, uint32_t, const MethodInfo*))Collection_1_IndexOf_m3195870360_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m1021188747_gshared (Collection_1_t3505093435 * __this, int32_t ___index0, uint32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m1021188747(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3505093435 *, int32_t, uint32_t, const MethodInfo*))Collection_1_Insert_m1021188747_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m1973357374_gshared (Collection_1_t3505093435 * __this, int32_t ___index0, uint32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m1973357374(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3505093435 *, int32_t, uint32_t, const MethodInfo*))Collection_1_InsertItem_m1973357374_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<System.UInt32>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m109312570_gshared (Collection_1_t3505093435 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m109312570(__this, method) ((  Il2CppObject* (*) (Collection_1_t3505093435 *, const MethodInfo*))Collection_1_get_Items_m109312570_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt32>::Remove(T)
extern "C"  bool Collection_1_Remove_m799093455_gshared (Collection_1_t3505093435 * __this, uint32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m799093455(__this, ___item0, method) ((  bool (*) (Collection_1_t3505093435 *, uint32_t, const MethodInfo*))Collection_1_Remove_m799093455_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3190008913_gshared (Collection_1_t3505093435 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m3190008913(__this, ___index0, method) ((  void (*) (Collection_1_t3505093435 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3190008913_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m1891541041_gshared (Collection_1_t3505093435 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m1891541041(__this, ___index0, method) ((  void (*) (Collection_1_t3505093435 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1891541041_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.UInt32>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m105040867_gshared (Collection_1_t3505093435 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m105040867(__this, method) ((  int32_t (*) (Collection_1_t3505093435 *, const MethodInfo*))Collection_1_get_Count_m105040867_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.UInt32>::get_Item(System.Int32)
extern "C"  uint32_t Collection_1_get_Item_m2169475861_gshared (Collection_1_t3505093435 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m2169475861(__this, ___index0, method) ((  uint32_t (*) (Collection_1_t3505093435 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2169475861_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3919439010_gshared (Collection_1_t3505093435 * __this, int32_t ___index0, uint32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3919439010(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3505093435 *, int32_t, uint32_t, const MethodInfo*))Collection_1_set_Item_m3919439010_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2908669847_gshared (Collection_1_t3505093435 * __this, int32_t ___index0, uint32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m2908669847(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3505093435 *, int32_t, uint32_t, const MethodInfo*))Collection_1_SetItem_m2908669847_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt32>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m3251995096_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m3251995096(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3251995096_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<System.UInt32>::ConvertItem(System.Object)
extern "C"  uint32_t Collection_1_ConvertItem_m2913972084_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m2913972084(__this /* static, unused */, ___item0, method) ((  uint32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m2913972084_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m405582548_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m405582548(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m405582548_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt32>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m3443972588_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m3443972588(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m3443972588_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt32>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m3066795059_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m3066795059(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3066795059_gshared)(__this /* static, unused */, ___list0, method)
