﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RemoteHero
struct RemoteHero_t3034416512;
// CSHeroUnit
struct CSHeroUnit_t3764358446;
// CombatEntity
struct CombatEntity_t684137495;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CSHeroUnit3764358446.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_FightEnum_EDamageType2535357340.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"

// System.Void RemoteHero::.ctor()
extern "C"  void RemoteHero__ctor_m1544307483 (RemoteHero_t3034416512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteHero::.cctor()
extern "C"  void RemoteHero__cctor_m146795506 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteHero::Create(CSHeroUnit)
extern "C"  void RemoteHero_Create_m2813108311 (RemoteHero_t3034416512 * __this, CSHeroUnit_t3764358446 * ___hcfg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteHero::BeAttacked(System.Int32,CombatEntity,FightEnum.EDamageType,System.Int32)
extern "C"  void RemoteHero_BeAttacked_m1032987121 (RemoteHero_t3034416512 * __this, int32_t ___hp0, CombatEntity_t684137495 * ___src1, int32_t ___damageType2, int32_t ___skillID3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteHero::ilo_BeAttacked1(CombatEntity,System.Int32,CombatEntity,FightEnum.EDamageType,System.Int32)
extern "C"  void RemoteHero_ilo_BeAttacked1_m2137722382 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___hp1, CombatEntity_t684137495 * ___src2, int32_t ___damageType3, int32_t ___skillID4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl RemoteHero::ilo_get_bvrCtrl2(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * RemoteHero_ilo_get_bvrCtrl2_m3824353717 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteHero::ilo_TranBehavior3(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void RemoteHero_ilo_TranBehavior3_m3859881976 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, uint8_t ___id1, ObjectU5BU5D_t1108656482* ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
