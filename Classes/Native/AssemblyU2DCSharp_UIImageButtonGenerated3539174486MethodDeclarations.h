﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIImageButtonGenerated
struct UIImageButtonGenerated_t3539174486;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"

// System.Void UIImageButtonGenerated::.ctor()
extern "C"  void UIImageButtonGenerated__ctor_m1550397701 (UIImageButtonGenerated_t3539174486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIImageButtonGenerated::UIImageButton_UIImageButton1(JSVCall,System.Int32)
extern "C"  bool UIImageButtonGenerated_UIImageButton_UIImageButton1_m924753825 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIImageButtonGenerated::UIImageButton_target(JSVCall)
extern "C"  void UIImageButtonGenerated_UIImageButton_target_m629502929 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIImageButtonGenerated::UIImageButton_normalSprite(JSVCall)
extern "C"  void UIImageButtonGenerated_UIImageButton_normalSprite_m4024512566 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIImageButtonGenerated::UIImageButton_hoverSprite(JSVCall)
extern "C"  void UIImageButtonGenerated_UIImageButton_hoverSprite_m2333169737 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIImageButtonGenerated::UIImageButton_pressedSprite(JSVCall)
extern "C"  void UIImageButtonGenerated_UIImageButton_pressedSprite_m428043171 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIImageButtonGenerated::UIImageButton_disabledSprite(JSVCall)
extern "C"  void UIImageButtonGenerated_UIImageButton_disabledSprite_m2134587137 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIImageButtonGenerated::UIImageButton_pixelSnap(JSVCall)
extern "C"  void UIImageButtonGenerated_UIImageButton_pixelSnap_m703461850 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIImageButtonGenerated::UIImageButton_isEnabled(JSVCall)
extern "C"  void UIImageButtonGenerated_UIImageButton_isEnabled_m1058990707 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIImageButtonGenerated::__Register()
extern "C"  void UIImageButtonGenerated___Register_m148317922 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIImageButtonGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIImageButtonGenerated_ilo_setObject1_m492632101 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIImageButtonGenerated::ilo_setStringS2(System.Int32,System.String)
extern "C"  void UIImageButtonGenerated_ilo_setStringS2_m619973071 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIImageButtonGenerated::ilo_getBooleanS3(System.Int32)
extern "C"  bool UIImageButtonGenerated_ilo_getBooleanS3_m3296276393 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
