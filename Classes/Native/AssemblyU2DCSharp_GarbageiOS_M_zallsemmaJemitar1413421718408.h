﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_zallsemmaJemitar141
struct  M_zallsemmaJemitar141_t3421718408  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_zallsemmaJemitar141::_mejeapeBetouce
	float ____mejeapeBetouce_0;
	// System.UInt32 GarbageiOS.M_zallsemmaJemitar141::_cereJawhir
	uint32_t ____cereJawhir_1;
	// System.UInt32 GarbageiOS.M_zallsemmaJemitar141::_tearwhir
	uint32_t ____tearwhir_2;
	// System.Int32 GarbageiOS.M_zallsemmaJemitar141::_letelair
	int32_t ____letelair_3;
	// System.Int32 GarbageiOS.M_zallsemmaJemitar141::_walsairher
	int32_t ____walsairher_4;
	// System.Single GarbageiOS.M_zallsemmaJemitar141::_miswhaHetasrear
	float ____miswhaHetasrear_5;
	// System.UInt32 GarbageiOS.M_zallsemmaJemitar141::_lipareFeresefor
	uint32_t ____lipareFeresefor_6;
	// System.Single GarbageiOS.M_zallsemmaJemitar141::_filuwelDriri
	float ____filuwelDriri_7;
	// System.String GarbageiOS.M_zallsemmaJemitar141::_laxoore
	String_t* ____laxoore_8;
	// System.Boolean GarbageiOS.M_zallsemmaJemitar141::_stawneChormaste
	bool ____stawneChormaste_9;
	// System.String GarbageiOS.M_zallsemmaJemitar141::_cawgo
	String_t* ____cawgo_10;
	// System.Int32 GarbageiOS.M_zallsemmaJemitar141::_tokiswirKecusa
	int32_t ____tokiswirKecusa_11;
	// System.Int32 GarbageiOS.M_zallsemmaJemitar141::_mirwouMurgedi
	int32_t ____mirwouMurgedi_12;
	// System.String GarbageiOS.M_zallsemmaJemitar141::_lallceceeHoube
	String_t* ____lallceceeHoube_13;
	// System.Boolean GarbageiOS.M_zallsemmaJemitar141::_geefa
	bool ____geefa_14;
	// System.UInt32 GarbageiOS.M_zallsemmaJemitar141::_hitai
	uint32_t ____hitai_15;
	// System.Int32 GarbageiOS.M_zallsemmaJemitar141::_stebearce
	int32_t ____stebearce_16;
	// System.String GarbageiOS.M_zallsemmaJemitar141::_peardinaw
	String_t* ____peardinaw_17;
	// System.Single GarbageiOS.M_zallsemmaJemitar141::_mija
	float ____mija_18;
	// System.Boolean GarbageiOS.M_zallsemmaJemitar141::_josi
	bool ____josi_19;
	// System.String GarbageiOS.M_zallsemmaJemitar141::_nokirNacemhe
	String_t* ____nokirNacemhe_20;
	// System.Single GarbageiOS.M_zallsemmaJemitar141::_cuzir
	float ____cuzir_21;
	// System.String GarbageiOS.M_zallsemmaJemitar141::_chowjeawiSiszaw
	String_t* ____chowjeawiSiszaw_22;
	// System.UInt32 GarbageiOS.M_zallsemmaJemitar141::_dosaTerta
	uint32_t ____dosaTerta_23;
	// System.UInt32 GarbageiOS.M_zallsemmaJemitar141::_sirmeeTevesur
	uint32_t ____sirmeeTevesur_24;

public:
	inline static int32_t get_offset_of__mejeapeBetouce_0() { return static_cast<int32_t>(offsetof(M_zallsemmaJemitar141_t3421718408, ____mejeapeBetouce_0)); }
	inline float get__mejeapeBetouce_0() const { return ____mejeapeBetouce_0; }
	inline float* get_address_of__mejeapeBetouce_0() { return &____mejeapeBetouce_0; }
	inline void set__mejeapeBetouce_0(float value)
	{
		____mejeapeBetouce_0 = value;
	}

	inline static int32_t get_offset_of__cereJawhir_1() { return static_cast<int32_t>(offsetof(M_zallsemmaJemitar141_t3421718408, ____cereJawhir_1)); }
	inline uint32_t get__cereJawhir_1() const { return ____cereJawhir_1; }
	inline uint32_t* get_address_of__cereJawhir_1() { return &____cereJawhir_1; }
	inline void set__cereJawhir_1(uint32_t value)
	{
		____cereJawhir_1 = value;
	}

	inline static int32_t get_offset_of__tearwhir_2() { return static_cast<int32_t>(offsetof(M_zallsemmaJemitar141_t3421718408, ____tearwhir_2)); }
	inline uint32_t get__tearwhir_2() const { return ____tearwhir_2; }
	inline uint32_t* get_address_of__tearwhir_2() { return &____tearwhir_2; }
	inline void set__tearwhir_2(uint32_t value)
	{
		____tearwhir_2 = value;
	}

	inline static int32_t get_offset_of__letelair_3() { return static_cast<int32_t>(offsetof(M_zallsemmaJemitar141_t3421718408, ____letelair_3)); }
	inline int32_t get__letelair_3() const { return ____letelair_3; }
	inline int32_t* get_address_of__letelair_3() { return &____letelair_3; }
	inline void set__letelair_3(int32_t value)
	{
		____letelair_3 = value;
	}

	inline static int32_t get_offset_of__walsairher_4() { return static_cast<int32_t>(offsetof(M_zallsemmaJemitar141_t3421718408, ____walsairher_4)); }
	inline int32_t get__walsairher_4() const { return ____walsairher_4; }
	inline int32_t* get_address_of__walsairher_4() { return &____walsairher_4; }
	inline void set__walsairher_4(int32_t value)
	{
		____walsairher_4 = value;
	}

	inline static int32_t get_offset_of__miswhaHetasrear_5() { return static_cast<int32_t>(offsetof(M_zallsemmaJemitar141_t3421718408, ____miswhaHetasrear_5)); }
	inline float get__miswhaHetasrear_5() const { return ____miswhaHetasrear_5; }
	inline float* get_address_of__miswhaHetasrear_5() { return &____miswhaHetasrear_5; }
	inline void set__miswhaHetasrear_5(float value)
	{
		____miswhaHetasrear_5 = value;
	}

	inline static int32_t get_offset_of__lipareFeresefor_6() { return static_cast<int32_t>(offsetof(M_zallsemmaJemitar141_t3421718408, ____lipareFeresefor_6)); }
	inline uint32_t get__lipareFeresefor_6() const { return ____lipareFeresefor_6; }
	inline uint32_t* get_address_of__lipareFeresefor_6() { return &____lipareFeresefor_6; }
	inline void set__lipareFeresefor_6(uint32_t value)
	{
		____lipareFeresefor_6 = value;
	}

	inline static int32_t get_offset_of__filuwelDriri_7() { return static_cast<int32_t>(offsetof(M_zallsemmaJemitar141_t3421718408, ____filuwelDriri_7)); }
	inline float get__filuwelDriri_7() const { return ____filuwelDriri_7; }
	inline float* get_address_of__filuwelDriri_7() { return &____filuwelDriri_7; }
	inline void set__filuwelDriri_7(float value)
	{
		____filuwelDriri_7 = value;
	}

	inline static int32_t get_offset_of__laxoore_8() { return static_cast<int32_t>(offsetof(M_zallsemmaJemitar141_t3421718408, ____laxoore_8)); }
	inline String_t* get__laxoore_8() const { return ____laxoore_8; }
	inline String_t** get_address_of__laxoore_8() { return &____laxoore_8; }
	inline void set__laxoore_8(String_t* value)
	{
		____laxoore_8 = value;
		Il2CppCodeGenWriteBarrier(&____laxoore_8, value);
	}

	inline static int32_t get_offset_of__stawneChormaste_9() { return static_cast<int32_t>(offsetof(M_zallsemmaJemitar141_t3421718408, ____stawneChormaste_9)); }
	inline bool get__stawneChormaste_9() const { return ____stawneChormaste_9; }
	inline bool* get_address_of__stawneChormaste_9() { return &____stawneChormaste_9; }
	inline void set__stawneChormaste_9(bool value)
	{
		____stawneChormaste_9 = value;
	}

	inline static int32_t get_offset_of__cawgo_10() { return static_cast<int32_t>(offsetof(M_zallsemmaJemitar141_t3421718408, ____cawgo_10)); }
	inline String_t* get__cawgo_10() const { return ____cawgo_10; }
	inline String_t** get_address_of__cawgo_10() { return &____cawgo_10; }
	inline void set__cawgo_10(String_t* value)
	{
		____cawgo_10 = value;
		Il2CppCodeGenWriteBarrier(&____cawgo_10, value);
	}

	inline static int32_t get_offset_of__tokiswirKecusa_11() { return static_cast<int32_t>(offsetof(M_zallsemmaJemitar141_t3421718408, ____tokiswirKecusa_11)); }
	inline int32_t get__tokiswirKecusa_11() const { return ____tokiswirKecusa_11; }
	inline int32_t* get_address_of__tokiswirKecusa_11() { return &____tokiswirKecusa_11; }
	inline void set__tokiswirKecusa_11(int32_t value)
	{
		____tokiswirKecusa_11 = value;
	}

	inline static int32_t get_offset_of__mirwouMurgedi_12() { return static_cast<int32_t>(offsetof(M_zallsemmaJemitar141_t3421718408, ____mirwouMurgedi_12)); }
	inline int32_t get__mirwouMurgedi_12() const { return ____mirwouMurgedi_12; }
	inline int32_t* get_address_of__mirwouMurgedi_12() { return &____mirwouMurgedi_12; }
	inline void set__mirwouMurgedi_12(int32_t value)
	{
		____mirwouMurgedi_12 = value;
	}

	inline static int32_t get_offset_of__lallceceeHoube_13() { return static_cast<int32_t>(offsetof(M_zallsemmaJemitar141_t3421718408, ____lallceceeHoube_13)); }
	inline String_t* get__lallceceeHoube_13() const { return ____lallceceeHoube_13; }
	inline String_t** get_address_of__lallceceeHoube_13() { return &____lallceceeHoube_13; }
	inline void set__lallceceeHoube_13(String_t* value)
	{
		____lallceceeHoube_13 = value;
		Il2CppCodeGenWriteBarrier(&____lallceceeHoube_13, value);
	}

	inline static int32_t get_offset_of__geefa_14() { return static_cast<int32_t>(offsetof(M_zallsemmaJemitar141_t3421718408, ____geefa_14)); }
	inline bool get__geefa_14() const { return ____geefa_14; }
	inline bool* get_address_of__geefa_14() { return &____geefa_14; }
	inline void set__geefa_14(bool value)
	{
		____geefa_14 = value;
	}

	inline static int32_t get_offset_of__hitai_15() { return static_cast<int32_t>(offsetof(M_zallsemmaJemitar141_t3421718408, ____hitai_15)); }
	inline uint32_t get__hitai_15() const { return ____hitai_15; }
	inline uint32_t* get_address_of__hitai_15() { return &____hitai_15; }
	inline void set__hitai_15(uint32_t value)
	{
		____hitai_15 = value;
	}

	inline static int32_t get_offset_of__stebearce_16() { return static_cast<int32_t>(offsetof(M_zallsemmaJemitar141_t3421718408, ____stebearce_16)); }
	inline int32_t get__stebearce_16() const { return ____stebearce_16; }
	inline int32_t* get_address_of__stebearce_16() { return &____stebearce_16; }
	inline void set__stebearce_16(int32_t value)
	{
		____stebearce_16 = value;
	}

	inline static int32_t get_offset_of__peardinaw_17() { return static_cast<int32_t>(offsetof(M_zallsemmaJemitar141_t3421718408, ____peardinaw_17)); }
	inline String_t* get__peardinaw_17() const { return ____peardinaw_17; }
	inline String_t** get_address_of__peardinaw_17() { return &____peardinaw_17; }
	inline void set__peardinaw_17(String_t* value)
	{
		____peardinaw_17 = value;
		Il2CppCodeGenWriteBarrier(&____peardinaw_17, value);
	}

	inline static int32_t get_offset_of__mija_18() { return static_cast<int32_t>(offsetof(M_zallsemmaJemitar141_t3421718408, ____mija_18)); }
	inline float get__mija_18() const { return ____mija_18; }
	inline float* get_address_of__mija_18() { return &____mija_18; }
	inline void set__mija_18(float value)
	{
		____mija_18 = value;
	}

	inline static int32_t get_offset_of__josi_19() { return static_cast<int32_t>(offsetof(M_zallsemmaJemitar141_t3421718408, ____josi_19)); }
	inline bool get__josi_19() const { return ____josi_19; }
	inline bool* get_address_of__josi_19() { return &____josi_19; }
	inline void set__josi_19(bool value)
	{
		____josi_19 = value;
	}

	inline static int32_t get_offset_of__nokirNacemhe_20() { return static_cast<int32_t>(offsetof(M_zallsemmaJemitar141_t3421718408, ____nokirNacemhe_20)); }
	inline String_t* get__nokirNacemhe_20() const { return ____nokirNacemhe_20; }
	inline String_t** get_address_of__nokirNacemhe_20() { return &____nokirNacemhe_20; }
	inline void set__nokirNacemhe_20(String_t* value)
	{
		____nokirNacemhe_20 = value;
		Il2CppCodeGenWriteBarrier(&____nokirNacemhe_20, value);
	}

	inline static int32_t get_offset_of__cuzir_21() { return static_cast<int32_t>(offsetof(M_zallsemmaJemitar141_t3421718408, ____cuzir_21)); }
	inline float get__cuzir_21() const { return ____cuzir_21; }
	inline float* get_address_of__cuzir_21() { return &____cuzir_21; }
	inline void set__cuzir_21(float value)
	{
		____cuzir_21 = value;
	}

	inline static int32_t get_offset_of__chowjeawiSiszaw_22() { return static_cast<int32_t>(offsetof(M_zallsemmaJemitar141_t3421718408, ____chowjeawiSiszaw_22)); }
	inline String_t* get__chowjeawiSiszaw_22() const { return ____chowjeawiSiszaw_22; }
	inline String_t** get_address_of__chowjeawiSiszaw_22() { return &____chowjeawiSiszaw_22; }
	inline void set__chowjeawiSiszaw_22(String_t* value)
	{
		____chowjeawiSiszaw_22 = value;
		Il2CppCodeGenWriteBarrier(&____chowjeawiSiszaw_22, value);
	}

	inline static int32_t get_offset_of__dosaTerta_23() { return static_cast<int32_t>(offsetof(M_zallsemmaJemitar141_t3421718408, ____dosaTerta_23)); }
	inline uint32_t get__dosaTerta_23() const { return ____dosaTerta_23; }
	inline uint32_t* get_address_of__dosaTerta_23() { return &____dosaTerta_23; }
	inline void set__dosaTerta_23(uint32_t value)
	{
		____dosaTerta_23 = value;
	}

	inline static int32_t get_offset_of__sirmeeTevesur_24() { return static_cast<int32_t>(offsetof(M_zallsemmaJemitar141_t3421718408, ____sirmeeTevesur_24)); }
	inline uint32_t get__sirmeeTevesur_24() const { return ____sirmeeTevesur_24; }
	inline uint32_t* get_address_of__sirmeeTevesur_24() { return &____sirmeeTevesur_24; }
	inline void set__sirmeeTevesur_24(uint32_t value)
	{
		____sirmeeTevesur_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
