﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_stiroomoo94
struct  M_stiroomoo94_t3902132158  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_stiroomoo94::_tralmesair
	String_t* ____tralmesair_0;
	// System.UInt32 GarbageiOS.M_stiroomoo94::_nurtawalVeyeremow
	uint32_t ____nurtawalVeyeremow_1;
	// System.Boolean GarbageiOS.M_stiroomoo94::_desis
	bool ____desis_2;
	// System.UInt32 GarbageiOS.M_stiroomoo94::_haydrisMepair
	uint32_t ____haydrisMepair_3;
	// System.Single GarbageiOS.M_stiroomoo94::_tuliJelpeldi
	float ____tuliJelpeldi_4;
	// System.String GarbageiOS.M_stiroomoo94::_heqaiQasou
	String_t* ____heqaiQasou_5;
	// System.Single GarbageiOS.M_stiroomoo94::_tokor
	float ____tokor_6;
	// System.Int32 GarbageiOS.M_stiroomoo94::_rarel
	int32_t ____rarel_7;
	// System.Boolean GarbageiOS.M_stiroomoo94::_taqeacoo
	bool ____taqeacoo_8;
	// System.Single GarbageiOS.M_stiroomoo94::_qusis
	float ____qusis_9;
	// System.Boolean GarbageiOS.M_stiroomoo94::_taire
	bool ____taire_10;
	// System.String GarbageiOS.M_stiroomoo94::_xehas
	String_t* ____xehas_11;
	// System.UInt32 GarbageiOS.M_stiroomoo94::_waydarnor
	uint32_t ____waydarnor_12;
	// System.Int32 GarbageiOS.M_stiroomoo94::_yamirai
	int32_t ____yamirai_13;
	// System.Single GarbageiOS.M_stiroomoo94::_burfaypeeJootoo
	float ____burfaypeeJootoo_14;
	// System.UInt32 GarbageiOS.M_stiroomoo94::_mebewaMaypel
	uint32_t ____mebewaMaypel_15;
	// System.Int32 GarbageiOS.M_stiroomoo94::_gisyascou
	int32_t ____gisyascou_16;
	// System.UInt32 GarbageiOS.M_stiroomoo94::_pelsearpalSebe
	uint32_t ____pelsearpalSebe_17;
	// System.Int32 GarbageiOS.M_stiroomoo94::_rairqorber
	int32_t ____rairqorber_18;
	// System.UInt32 GarbageiOS.M_stiroomoo94::_talpochay
	uint32_t ____talpochay_19;
	// System.Single GarbageiOS.M_stiroomoo94::_telmumairSaykaiwa
	float ____telmumairSaykaiwa_20;
	// System.Boolean GarbageiOS.M_stiroomoo94::_cejoosouQeyo
	bool ____cejoosouQeyo_21;
	// System.Int32 GarbageiOS.M_stiroomoo94::_bawwowjear
	int32_t ____bawwowjear_22;
	// System.String GarbageiOS.M_stiroomoo94::_qedralreCalla
	String_t* ____qedralreCalla_23;
	// System.Single GarbageiOS.M_stiroomoo94::_nimertal
	float ____nimertal_24;
	// System.Boolean GarbageiOS.M_stiroomoo94::_cayjalrea
	bool ____cayjalrea_25;
	// System.UInt32 GarbageiOS.M_stiroomoo94::_seaho
	uint32_t ____seaho_26;
	// System.String GarbageiOS.M_stiroomoo94::_wheerair
	String_t* ____wheerair_27;
	// System.String GarbageiOS.M_stiroomoo94::_trerkaforPaicece
	String_t* ____trerkaforPaicece_28;
	// System.UInt32 GarbageiOS.M_stiroomoo94::_sawpeawereKewo
	uint32_t ____sawpeawereKewo_29;
	// System.String GarbageiOS.M_stiroomoo94::_hepurhe
	String_t* ____hepurhe_30;
	// System.String GarbageiOS.M_stiroomoo94::_kepall
	String_t* ____kepall_31;
	// System.String GarbageiOS.M_stiroomoo94::_komawdeMairne
	String_t* ____komawdeMairne_32;
	// System.String GarbageiOS.M_stiroomoo94::_hasveregurNairbawnir
	String_t* ____hasveregurNairbawnir_33;
	// System.String GarbageiOS.M_stiroomoo94::_saleLererilel
	String_t* ____saleLererilel_34;
	// System.Boolean GarbageiOS.M_stiroomoo94::_talmeLalpow
	bool ____talmeLalpow_35;
	// System.String GarbageiOS.M_stiroomoo94::_laichexal
	String_t* ____laichexal_36;
	// System.String GarbageiOS.M_stiroomoo94::_nearluMujeagall
	String_t* ____nearluMujeagall_37;
	// System.Single GarbageiOS.M_stiroomoo94::_jatal
	float ____jatal_38;
	// System.Int32 GarbageiOS.M_stiroomoo94::_joujoo
	int32_t ____joujoo_39;
	// System.Int32 GarbageiOS.M_stiroomoo94::_facouQahir
	int32_t ____facouQahir_40;
	// System.Boolean GarbageiOS.M_stiroomoo94::_jayje
	bool ____jayje_41;
	// System.Single GarbageiOS.M_stiroomoo94::_barearle
	float ____barearle_42;
	// System.Boolean GarbageiOS.M_stiroomoo94::_jozubasKati
	bool ____jozubasKati_43;
	// System.Boolean GarbageiOS.M_stiroomoo94::_miberepou
	bool ____miberepou_44;

public:
	inline static int32_t get_offset_of__tralmesair_0() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____tralmesair_0)); }
	inline String_t* get__tralmesair_0() const { return ____tralmesair_0; }
	inline String_t** get_address_of__tralmesair_0() { return &____tralmesair_0; }
	inline void set__tralmesair_0(String_t* value)
	{
		____tralmesair_0 = value;
		Il2CppCodeGenWriteBarrier(&____tralmesair_0, value);
	}

	inline static int32_t get_offset_of__nurtawalVeyeremow_1() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____nurtawalVeyeremow_1)); }
	inline uint32_t get__nurtawalVeyeremow_1() const { return ____nurtawalVeyeremow_1; }
	inline uint32_t* get_address_of__nurtawalVeyeremow_1() { return &____nurtawalVeyeremow_1; }
	inline void set__nurtawalVeyeremow_1(uint32_t value)
	{
		____nurtawalVeyeremow_1 = value;
	}

	inline static int32_t get_offset_of__desis_2() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____desis_2)); }
	inline bool get__desis_2() const { return ____desis_2; }
	inline bool* get_address_of__desis_2() { return &____desis_2; }
	inline void set__desis_2(bool value)
	{
		____desis_2 = value;
	}

	inline static int32_t get_offset_of__haydrisMepair_3() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____haydrisMepair_3)); }
	inline uint32_t get__haydrisMepair_3() const { return ____haydrisMepair_3; }
	inline uint32_t* get_address_of__haydrisMepair_3() { return &____haydrisMepair_3; }
	inline void set__haydrisMepair_3(uint32_t value)
	{
		____haydrisMepair_3 = value;
	}

	inline static int32_t get_offset_of__tuliJelpeldi_4() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____tuliJelpeldi_4)); }
	inline float get__tuliJelpeldi_4() const { return ____tuliJelpeldi_4; }
	inline float* get_address_of__tuliJelpeldi_4() { return &____tuliJelpeldi_4; }
	inline void set__tuliJelpeldi_4(float value)
	{
		____tuliJelpeldi_4 = value;
	}

	inline static int32_t get_offset_of__heqaiQasou_5() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____heqaiQasou_5)); }
	inline String_t* get__heqaiQasou_5() const { return ____heqaiQasou_5; }
	inline String_t** get_address_of__heqaiQasou_5() { return &____heqaiQasou_5; }
	inline void set__heqaiQasou_5(String_t* value)
	{
		____heqaiQasou_5 = value;
		Il2CppCodeGenWriteBarrier(&____heqaiQasou_5, value);
	}

	inline static int32_t get_offset_of__tokor_6() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____tokor_6)); }
	inline float get__tokor_6() const { return ____tokor_6; }
	inline float* get_address_of__tokor_6() { return &____tokor_6; }
	inline void set__tokor_6(float value)
	{
		____tokor_6 = value;
	}

	inline static int32_t get_offset_of__rarel_7() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____rarel_7)); }
	inline int32_t get__rarel_7() const { return ____rarel_7; }
	inline int32_t* get_address_of__rarel_7() { return &____rarel_7; }
	inline void set__rarel_7(int32_t value)
	{
		____rarel_7 = value;
	}

	inline static int32_t get_offset_of__taqeacoo_8() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____taqeacoo_8)); }
	inline bool get__taqeacoo_8() const { return ____taqeacoo_8; }
	inline bool* get_address_of__taqeacoo_8() { return &____taqeacoo_8; }
	inline void set__taqeacoo_8(bool value)
	{
		____taqeacoo_8 = value;
	}

	inline static int32_t get_offset_of__qusis_9() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____qusis_9)); }
	inline float get__qusis_9() const { return ____qusis_9; }
	inline float* get_address_of__qusis_9() { return &____qusis_9; }
	inline void set__qusis_9(float value)
	{
		____qusis_9 = value;
	}

	inline static int32_t get_offset_of__taire_10() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____taire_10)); }
	inline bool get__taire_10() const { return ____taire_10; }
	inline bool* get_address_of__taire_10() { return &____taire_10; }
	inline void set__taire_10(bool value)
	{
		____taire_10 = value;
	}

	inline static int32_t get_offset_of__xehas_11() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____xehas_11)); }
	inline String_t* get__xehas_11() const { return ____xehas_11; }
	inline String_t** get_address_of__xehas_11() { return &____xehas_11; }
	inline void set__xehas_11(String_t* value)
	{
		____xehas_11 = value;
		Il2CppCodeGenWriteBarrier(&____xehas_11, value);
	}

	inline static int32_t get_offset_of__waydarnor_12() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____waydarnor_12)); }
	inline uint32_t get__waydarnor_12() const { return ____waydarnor_12; }
	inline uint32_t* get_address_of__waydarnor_12() { return &____waydarnor_12; }
	inline void set__waydarnor_12(uint32_t value)
	{
		____waydarnor_12 = value;
	}

	inline static int32_t get_offset_of__yamirai_13() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____yamirai_13)); }
	inline int32_t get__yamirai_13() const { return ____yamirai_13; }
	inline int32_t* get_address_of__yamirai_13() { return &____yamirai_13; }
	inline void set__yamirai_13(int32_t value)
	{
		____yamirai_13 = value;
	}

	inline static int32_t get_offset_of__burfaypeeJootoo_14() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____burfaypeeJootoo_14)); }
	inline float get__burfaypeeJootoo_14() const { return ____burfaypeeJootoo_14; }
	inline float* get_address_of__burfaypeeJootoo_14() { return &____burfaypeeJootoo_14; }
	inline void set__burfaypeeJootoo_14(float value)
	{
		____burfaypeeJootoo_14 = value;
	}

	inline static int32_t get_offset_of__mebewaMaypel_15() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____mebewaMaypel_15)); }
	inline uint32_t get__mebewaMaypel_15() const { return ____mebewaMaypel_15; }
	inline uint32_t* get_address_of__mebewaMaypel_15() { return &____mebewaMaypel_15; }
	inline void set__mebewaMaypel_15(uint32_t value)
	{
		____mebewaMaypel_15 = value;
	}

	inline static int32_t get_offset_of__gisyascou_16() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____gisyascou_16)); }
	inline int32_t get__gisyascou_16() const { return ____gisyascou_16; }
	inline int32_t* get_address_of__gisyascou_16() { return &____gisyascou_16; }
	inline void set__gisyascou_16(int32_t value)
	{
		____gisyascou_16 = value;
	}

	inline static int32_t get_offset_of__pelsearpalSebe_17() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____pelsearpalSebe_17)); }
	inline uint32_t get__pelsearpalSebe_17() const { return ____pelsearpalSebe_17; }
	inline uint32_t* get_address_of__pelsearpalSebe_17() { return &____pelsearpalSebe_17; }
	inline void set__pelsearpalSebe_17(uint32_t value)
	{
		____pelsearpalSebe_17 = value;
	}

	inline static int32_t get_offset_of__rairqorber_18() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____rairqorber_18)); }
	inline int32_t get__rairqorber_18() const { return ____rairqorber_18; }
	inline int32_t* get_address_of__rairqorber_18() { return &____rairqorber_18; }
	inline void set__rairqorber_18(int32_t value)
	{
		____rairqorber_18 = value;
	}

	inline static int32_t get_offset_of__talpochay_19() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____talpochay_19)); }
	inline uint32_t get__talpochay_19() const { return ____talpochay_19; }
	inline uint32_t* get_address_of__talpochay_19() { return &____talpochay_19; }
	inline void set__talpochay_19(uint32_t value)
	{
		____talpochay_19 = value;
	}

	inline static int32_t get_offset_of__telmumairSaykaiwa_20() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____telmumairSaykaiwa_20)); }
	inline float get__telmumairSaykaiwa_20() const { return ____telmumairSaykaiwa_20; }
	inline float* get_address_of__telmumairSaykaiwa_20() { return &____telmumairSaykaiwa_20; }
	inline void set__telmumairSaykaiwa_20(float value)
	{
		____telmumairSaykaiwa_20 = value;
	}

	inline static int32_t get_offset_of__cejoosouQeyo_21() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____cejoosouQeyo_21)); }
	inline bool get__cejoosouQeyo_21() const { return ____cejoosouQeyo_21; }
	inline bool* get_address_of__cejoosouQeyo_21() { return &____cejoosouQeyo_21; }
	inline void set__cejoosouQeyo_21(bool value)
	{
		____cejoosouQeyo_21 = value;
	}

	inline static int32_t get_offset_of__bawwowjear_22() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____bawwowjear_22)); }
	inline int32_t get__bawwowjear_22() const { return ____bawwowjear_22; }
	inline int32_t* get_address_of__bawwowjear_22() { return &____bawwowjear_22; }
	inline void set__bawwowjear_22(int32_t value)
	{
		____bawwowjear_22 = value;
	}

	inline static int32_t get_offset_of__qedralreCalla_23() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____qedralreCalla_23)); }
	inline String_t* get__qedralreCalla_23() const { return ____qedralreCalla_23; }
	inline String_t** get_address_of__qedralreCalla_23() { return &____qedralreCalla_23; }
	inline void set__qedralreCalla_23(String_t* value)
	{
		____qedralreCalla_23 = value;
		Il2CppCodeGenWriteBarrier(&____qedralreCalla_23, value);
	}

	inline static int32_t get_offset_of__nimertal_24() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____nimertal_24)); }
	inline float get__nimertal_24() const { return ____nimertal_24; }
	inline float* get_address_of__nimertal_24() { return &____nimertal_24; }
	inline void set__nimertal_24(float value)
	{
		____nimertal_24 = value;
	}

	inline static int32_t get_offset_of__cayjalrea_25() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____cayjalrea_25)); }
	inline bool get__cayjalrea_25() const { return ____cayjalrea_25; }
	inline bool* get_address_of__cayjalrea_25() { return &____cayjalrea_25; }
	inline void set__cayjalrea_25(bool value)
	{
		____cayjalrea_25 = value;
	}

	inline static int32_t get_offset_of__seaho_26() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____seaho_26)); }
	inline uint32_t get__seaho_26() const { return ____seaho_26; }
	inline uint32_t* get_address_of__seaho_26() { return &____seaho_26; }
	inline void set__seaho_26(uint32_t value)
	{
		____seaho_26 = value;
	}

	inline static int32_t get_offset_of__wheerair_27() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____wheerair_27)); }
	inline String_t* get__wheerair_27() const { return ____wheerair_27; }
	inline String_t** get_address_of__wheerair_27() { return &____wheerair_27; }
	inline void set__wheerair_27(String_t* value)
	{
		____wheerair_27 = value;
		Il2CppCodeGenWriteBarrier(&____wheerair_27, value);
	}

	inline static int32_t get_offset_of__trerkaforPaicece_28() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____trerkaforPaicece_28)); }
	inline String_t* get__trerkaforPaicece_28() const { return ____trerkaforPaicece_28; }
	inline String_t** get_address_of__trerkaforPaicece_28() { return &____trerkaforPaicece_28; }
	inline void set__trerkaforPaicece_28(String_t* value)
	{
		____trerkaforPaicece_28 = value;
		Il2CppCodeGenWriteBarrier(&____trerkaforPaicece_28, value);
	}

	inline static int32_t get_offset_of__sawpeawereKewo_29() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____sawpeawereKewo_29)); }
	inline uint32_t get__sawpeawereKewo_29() const { return ____sawpeawereKewo_29; }
	inline uint32_t* get_address_of__sawpeawereKewo_29() { return &____sawpeawereKewo_29; }
	inline void set__sawpeawereKewo_29(uint32_t value)
	{
		____sawpeawereKewo_29 = value;
	}

	inline static int32_t get_offset_of__hepurhe_30() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____hepurhe_30)); }
	inline String_t* get__hepurhe_30() const { return ____hepurhe_30; }
	inline String_t** get_address_of__hepurhe_30() { return &____hepurhe_30; }
	inline void set__hepurhe_30(String_t* value)
	{
		____hepurhe_30 = value;
		Il2CppCodeGenWriteBarrier(&____hepurhe_30, value);
	}

	inline static int32_t get_offset_of__kepall_31() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____kepall_31)); }
	inline String_t* get__kepall_31() const { return ____kepall_31; }
	inline String_t** get_address_of__kepall_31() { return &____kepall_31; }
	inline void set__kepall_31(String_t* value)
	{
		____kepall_31 = value;
		Il2CppCodeGenWriteBarrier(&____kepall_31, value);
	}

	inline static int32_t get_offset_of__komawdeMairne_32() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____komawdeMairne_32)); }
	inline String_t* get__komawdeMairne_32() const { return ____komawdeMairne_32; }
	inline String_t** get_address_of__komawdeMairne_32() { return &____komawdeMairne_32; }
	inline void set__komawdeMairne_32(String_t* value)
	{
		____komawdeMairne_32 = value;
		Il2CppCodeGenWriteBarrier(&____komawdeMairne_32, value);
	}

	inline static int32_t get_offset_of__hasveregurNairbawnir_33() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____hasveregurNairbawnir_33)); }
	inline String_t* get__hasveregurNairbawnir_33() const { return ____hasveregurNairbawnir_33; }
	inline String_t** get_address_of__hasveregurNairbawnir_33() { return &____hasveregurNairbawnir_33; }
	inline void set__hasveregurNairbawnir_33(String_t* value)
	{
		____hasveregurNairbawnir_33 = value;
		Il2CppCodeGenWriteBarrier(&____hasveregurNairbawnir_33, value);
	}

	inline static int32_t get_offset_of__saleLererilel_34() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____saleLererilel_34)); }
	inline String_t* get__saleLererilel_34() const { return ____saleLererilel_34; }
	inline String_t** get_address_of__saleLererilel_34() { return &____saleLererilel_34; }
	inline void set__saleLererilel_34(String_t* value)
	{
		____saleLererilel_34 = value;
		Il2CppCodeGenWriteBarrier(&____saleLererilel_34, value);
	}

	inline static int32_t get_offset_of__talmeLalpow_35() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____talmeLalpow_35)); }
	inline bool get__talmeLalpow_35() const { return ____talmeLalpow_35; }
	inline bool* get_address_of__talmeLalpow_35() { return &____talmeLalpow_35; }
	inline void set__talmeLalpow_35(bool value)
	{
		____talmeLalpow_35 = value;
	}

	inline static int32_t get_offset_of__laichexal_36() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____laichexal_36)); }
	inline String_t* get__laichexal_36() const { return ____laichexal_36; }
	inline String_t** get_address_of__laichexal_36() { return &____laichexal_36; }
	inline void set__laichexal_36(String_t* value)
	{
		____laichexal_36 = value;
		Il2CppCodeGenWriteBarrier(&____laichexal_36, value);
	}

	inline static int32_t get_offset_of__nearluMujeagall_37() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____nearluMujeagall_37)); }
	inline String_t* get__nearluMujeagall_37() const { return ____nearluMujeagall_37; }
	inline String_t** get_address_of__nearluMujeagall_37() { return &____nearluMujeagall_37; }
	inline void set__nearluMujeagall_37(String_t* value)
	{
		____nearluMujeagall_37 = value;
		Il2CppCodeGenWriteBarrier(&____nearluMujeagall_37, value);
	}

	inline static int32_t get_offset_of__jatal_38() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____jatal_38)); }
	inline float get__jatal_38() const { return ____jatal_38; }
	inline float* get_address_of__jatal_38() { return &____jatal_38; }
	inline void set__jatal_38(float value)
	{
		____jatal_38 = value;
	}

	inline static int32_t get_offset_of__joujoo_39() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____joujoo_39)); }
	inline int32_t get__joujoo_39() const { return ____joujoo_39; }
	inline int32_t* get_address_of__joujoo_39() { return &____joujoo_39; }
	inline void set__joujoo_39(int32_t value)
	{
		____joujoo_39 = value;
	}

	inline static int32_t get_offset_of__facouQahir_40() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____facouQahir_40)); }
	inline int32_t get__facouQahir_40() const { return ____facouQahir_40; }
	inline int32_t* get_address_of__facouQahir_40() { return &____facouQahir_40; }
	inline void set__facouQahir_40(int32_t value)
	{
		____facouQahir_40 = value;
	}

	inline static int32_t get_offset_of__jayje_41() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____jayje_41)); }
	inline bool get__jayje_41() const { return ____jayje_41; }
	inline bool* get_address_of__jayje_41() { return &____jayje_41; }
	inline void set__jayje_41(bool value)
	{
		____jayje_41 = value;
	}

	inline static int32_t get_offset_of__barearle_42() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____barearle_42)); }
	inline float get__barearle_42() const { return ____barearle_42; }
	inline float* get_address_of__barearle_42() { return &____barearle_42; }
	inline void set__barearle_42(float value)
	{
		____barearle_42 = value;
	}

	inline static int32_t get_offset_of__jozubasKati_43() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____jozubasKati_43)); }
	inline bool get__jozubasKati_43() const { return ____jozubasKati_43; }
	inline bool* get_address_of__jozubasKati_43() { return &____jozubasKati_43; }
	inline void set__jozubasKati_43(bool value)
	{
		____jozubasKati_43 = value;
	}

	inline static int32_t get_offset_of__miberepou_44() { return static_cast<int32_t>(offsetof(M_stiroomoo94_t3902132158, ____miberepou_44)); }
	inline bool get__miberepou_44() const { return ____miberepou_44; }
	inline bool* get_address_of__miberepou_44() { return &____miberepou_44; }
	inline void set__miberepou_44(bool value)
	{
		____miberepou_44 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
