﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_dawchougaNezis166
struct  M_dawchougaNezis166_t3164355875  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_dawchougaNezis166::_gelbalwea
	uint32_t ____gelbalwea_0;
	// System.UInt32 GarbageiOS.M_dawchougaNezis166::_narsawheBekeene
	uint32_t ____narsawheBekeene_1;
	// System.Boolean GarbageiOS.M_dawchougaNezis166::_nepirtiHasta
	bool ____nepirtiHasta_2;
	// System.Boolean GarbageiOS.M_dawchougaNezis166::_keahouhayMehaw
	bool ____keahouhayMehaw_3;
	// System.Int32 GarbageiOS.M_dawchougaNezis166::_faqo
	int32_t ____faqo_4;
	// System.Single GarbageiOS.M_dawchougaNezis166::_sercouza
	float ____sercouza_5;
	// System.String GarbageiOS.M_dawchougaNezis166::_cuxuNimi
	String_t* ____cuxuNimi_6;
	// System.String GarbageiOS.M_dawchougaNezis166::_horjisJisxere
	String_t* ____horjisJisxere_7;
	// System.Single GarbageiOS.M_dawchougaNezis166::_rassePicela
	float ____rassePicela_8;
	// System.Int32 GarbageiOS.M_dawchougaNezis166::_peetrirRemji
	int32_t ____peetrirRemji_9;
	// System.String GarbageiOS.M_dawchougaNezis166::_jore
	String_t* ____jore_10;
	// System.UInt32 GarbageiOS.M_dawchougaNezis166::_berewhoustow
	uint32_t ____berewhoustow_11;
	// System.String GarbageiOS.M_dawchougaNezis166::_houheajouSenanow
	String_t* ____houheajouSenanow_12;
	// System.Boolean GarbageiOS.M_dawchougaNezis166::_cisjomirTurbi
	bool ____cisjomirTurbi_13;
	// System.Single GarbageiOS.M_dawchougaNezis166::_forjaha
	float ____forjaha_14;
	// System.String GarbageiOS.M_dawchougaNezis166::_sursemweLarsti
	String_t* ____sursemweLarsti_15;
	// System.Boolean GarbageiOS.M_dawchougaNezis166::_louvear
	bool ____louvear_16;
	// System.Int32 GarbageiOS.M_dawchougaNezis166::_tornar
	int32_t ____tornar_17;
	// System.String GarbageiOS.M_dawchougaNezis166::_whecasir
	String_t* ____whecasir_18;
	// System.String GarbageiOS.M_dawchougaNezis166::_filemte
	String_t* ____filemte_19;

public:
	inline static int32_t get_offset_of__gelbalwea_0() { return static_cast<int32_t>(offsetof(M_dawchougaNezis166_t3164355875, ____gelbalwea_0)); }
	inline uint32_t get__gelbalwea_0() const { return ____gelbalwea_0; }
	inline uint32_t* get_address_of__gelbalwea_0() { return &____gelbalwea_0; }
	inline void set__gelbalwea_0(uint32_t value)
	{
		____gelbalwea_0 = value;
	}

	inline static int32_t get_offset_of__narsawheBekeene_1() { return static_cast<int32_t>(offsetof(M_dawchougaNezis166_t3164355875, ____narsawheBekeene_1)); }
	inline uint32_t get__narsawheBekeene_1() const { return ____narsawheBekeene_1; }
	inline uint32_t* get_address_of__narsawheBekeene_1() { return &____narsawheBekeene_1; }
	inline void set__narsawheBekeene_1(uint32_t value)
	{
		____narsawheBekeene_1 = value;
	}

	inline static int32_t get_offset_of__nepirtiHasta_2() { return static_cast<int32_t>(offsetof(M_dawchougaNezis166_t3164355875, ____nepirtiHasta_2)); }
	inline bool get__nepirtiHasta_2() const { return ____nepirtiHasta_2; }
	inline bool* get_address_of__nepirtiHasta_2() { return &____nepirtiHasta_2; }
	inline void set__nepirtiHasta_2(bool value)
	{
		____nepirtiHasta_2 = value;
	}

	inline static int32_t get_offset_of__keahouhayMehaw_3() { return static_cast<int32_t>(offsetof(M_dawchougaNezis166_t3164355875, ____keahouhayMehaw_3)); }
	inline bool get__keahouhayMehaw_3() const { return ____keahouhayMehaw_3; }
	inline bool* get_address_of__keahouhayMehaw_3() { return &____keahouhayMehaw_3; }
	inline void set__keahouhayMehaw_3(bool value)
	{
		____keahouhayMehaw_3 = value;
	}

	inline static int32_t get_offset_of__faqo_4() { return static_cast<int32_t>(offsetof(M_dawchougaNezis166_t3164355875, ____faqo_4)); }
	inline int32_t get__faqo_4() const { return ____faqo_4; }
	inline int32_t* get_address_of__faqo_4() { return &____faqo_4; }
	inline void set__faqo_4(int32_t value)
	{
		____faqo_4 = value;
	}

	inline static int32_t get_offset_of__sercouza_5() { return static_cast<int32_t>(offsetof(M_dawchougaNezis166_t3164355875, ____sercouza_5)); }
	inline float get__sercouza_5() const { return ____sercouza_5; }
	inline float* get_address_of__sercouza_5() { return &____sercouza_5; }
	inline void set__sercouza_5(float value)
	{
		____sercouza_5 = value;
	}

	inline static int32_t get_offset_of__cuxuNimi_6() { return static_cast<int32_t>(offsetof(M_dawchougaNezis166_t3164355875, ____cuxuNimi_6)); }
	inline String_t* get__cuxuNimi_6() const { return ____cuxuNimi_6; }
	inline String_t** get_address_of__cuxuNimi_6() { return &____cuxuNimi_6; }
	inline void set__cuxuNimi_6(String_t* value)
	{
		____cuxuNimi_6 = value;
		Il2CppCodeGenWriteBarrier(&____cuxuNimi_6, value);
	}

	inline static int32_t get_offset_of__horjisJisxere_7() { return static_cast<int32_t>(offsetof(M_dawchougaNezis166_t3164355875, ____horjisJisxere_7)); }
	inline String_t* get__horjisJisxere_7() const { return ____horjisJisxere_7; }
	inline String_t** get_address_of__horjisJisxere_7() { return &____horjisJisxere_7; }
	inline void set__horjisJisxere_7(String_t* value)
	{
		____horjisJisxere_7 = value;
		Il2CppCodeGenWriteBarrier(&____horjisJisxere_7, value);
	}

	inline static int32_t get_offset_of__rassePicela_8() { return static_cast<int32_t>(offsetof(M_dawchougaNezis166_t3164355875, ____rassePicela_8)); }
	inline float get__rassePicela_8() const { return ____rassePicela_8; }
	inline float* get_address_of__rassePicela_8() { return &____rassePicela_8; }
	inline void set__rassePicela_8(float value)
	{
		____rassePicela_8 = value;
	}

	inline static int32_t get_offset_of__peetrirRemji_9() { return static_cast<int32_t>(offsetof(M_dawchougaNezis166_t3164355875, ____peetrirRemji_9)); }
	inline int32_t get__peetrirRemji_9() const { return ____peetrirRemji_9; }
	inline int32_t* get_address_of__peetrirRemji_9() { return &____peetrirRemji_9; }
	inline void set__peetrirRemji_9(int32_t value)
	{
		____peetrirRemji_9 = value;
	}

	inline static int32_t get_offset_of__jore_10() { return static_cast<int32_t>(offsetof(M_dawchougaNezis166_t3164355875, ____jore_10)); }
	inline String_t* get__jore_10() const { return ____jore_10; }
	inline String_t** get_address_of__jore_10() { return &____jore_10; }
	inline void set__jore_10(String_t* value)
	{
		____jore_10 = value;
		Il2CppCodeGenWriteBarrier(&____jore_10, value);
	}

	inline static int32_t get_offset_of__berewhoustow_11() { return static_cast<int32_t>(offsetof(M_dawchougaNezis166_t3164355875, ____berewhoustow_11)); }
	inline uint32_t get__berewhoustow_11() const { return ____berewhoustow_11; }
	inline uint32_t* get_address_of__berewhoustow_11() { return &____berewhoustow_11; }
	inline void set__berewhoustow_11(uint32_t value)
	{
		____berewhoustow_11 = value;
	}

	inline static int32_t get_offset_of__houheajouSenanow_12() { return static_cast<int32_t>(offsetof(M_dawchougaNezis166_t3164355875, ____houheajouSenanow_12)); }
	inline String_t* get__houheajouSenanow_12() const { return ____houheajouSenanow_12; }
	inline String_t** get_address_of__houheajouSenanow_12() { return &____houheajouSenanow_12; }
	inline void set__houheajouSenanow_12(String_t* value)
	{
		____houheajouSenanow_12 = value;
		Il2CppCodeGenWriteBarrier(&____houheajouSenanow_12, value);
	}

	inline static int32_t get_offset_of__cisjomirTurbi_13() { return static_cast<int32_t>(offsetof(M_dawchougaNezis166_t3164355875, ____cisjomirTurbi_13)); }
	inline bool get__cisjomirTurbi_13() const { return ____cisjomirTurbi_13; }
	inline bool* get_address_of__cisjomirTurbi_13() { return &____cisjomirTurbi_13; }
	inline void set__cisjomirTurbi_13(bool value)
	{
		____cisjomirTurbi_13 = value;
	}

	inline static int32_t get_offset_of__forjaha_14() { return static_cast<int32_t>(offsetof(M_dawchougaNezis166_t3164355875, ____forjaha_14)); }
	inline float get__forjaha_14() const { return ____forjaha_14; }
	inline float* get_address_of__forjaha_14() { return &____forjaha_14; }
	inline void set__forjaha_14(float value)
	{
		____forjaha_14 = value;
	}

	inline static int32_t get_offset_of__sursemweLarsti_15() { return static_cast<int32_t>(offsetof(M_dawchougaNezis166_t3164355875, ____sursemweLarsti_15)); }
	inline String_t* get__sursemweLarsti_15() const { return ____sursemweLarsti_15; }
	inline String_t** get_address_of__sursemweLarsti_15() { return &____sursemweLarsti_15; }
	inline void set__sursemweLarsti_15(String_t* value)
	{
		____sursemweLarsti_15 = value;
		Il2CppCodeGenWriteBarrier(&____sursemweLarsti_15, value);
	}

	inline static int32_t get_offset_of__louvear_16() { return static_cast<int32_t>(offsetof(M_dawchougaNezis166_t3164355875, ____louvear_16)); }
	inline bool get__louvear_16() const { return ____louvear_16; }
	inline bool* get_address_of__louvear_16() { return &____louvear_16; }
	inline void set__louvear_16(bool value)
	{
		____louvear_16 = value;
	}

	inline static int32_t get_offset_of__tornar_17() { return static_cast<int32_t>(offsetof(M_dawchougaNezis166_t3164355875, ____tornar_17)); }
	inline int32_t get__tornar_17() const { return ____tornar_17; }
	inline int32_t* get_address_of__tornar_17() { return &____tornar_17; }
	inline void set__tornar_17(int32_t value)
	{
		____tornar_17 = value;
	}

	inline static int32_t get_offset_of__whecasir_18() { return static_cast<int32_t>(offsetof(M_dawchougaNezis166_t3164355875, ____whecasir_18)); }
	inline String_t* get__whecasir_18() const { return ____whecasir_18; }
	inline String_t** get_address_of__whecasir_18() { return &____whecasir_18; }
	inline void set__whecasir_18(String_t* value)
	{
		____whecasir_18 = value;
		Il2CppCodeGenWriteBarrier(&____whecasir_18, value);
	}

	inline static int32_t get_offset_of__filemte_19() { return static_cast<int32_t>(offsetof(M_dawchougaNezis166_t3164355875, ____filemte_19)); }
	inline String_t* get__filemte_19() const { return ____filemte_19; }
	inline String_t** get_address_of__filemte_19() { return &____filemte_19; }
	inline void set__filemte_19(String_t* value)
	{
		____filemte_19 = value;
		Il2CppCodeGenWriteBarrier(&____filemte_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
