﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.NormalBvr
struct NormalBvr_t236031701;
// Hero
struct Hero_t2245658;
// HeroMgr
struct HeroMgr_t2475965342;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// CombatEntity
struct CombatEntity_t684137495;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;
// AnimationRunner
struct AnimationRunner_t1015409588;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_HeroMgr2475965342.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_AnimationRunner1015409588.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniType1006238651.h"

// System.Void Entity.Behavior.NormalBvr::.ctor()
extern "C"  void NormalBvr__ctor_m3706127557 (NormalBvr_t236031701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID Entity.Behavior.NormalBvr::get_id()
extern "C"  uint8_t NormalBvr_get_id_m3812777873 (NormalBvr_t236031701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.NormalBvr::Reason()
extern "C"  void NormalBvr_Reason_m2772192675 (NormalBvr_t236031701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.NormalBvr::Action()
extern "C"  void NormalBvr_Action_m1968899349 (NormalBvr_t236031701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.NormalBvr::DoEntering()
extern "C"  void NormalBvr_DoEntering_m1651375060 (NormalBvr_t236031701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.NormalBvr::DoLeaving()
extern "C"  void NormalBvr_DoLeaving_m1606559052 (NormalBvr_t236031701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero Entity.Behavior.NormalBvr::ilo_get_captain1(HeroMgr)
extern "C"  Hero_t2245658 * NormalBvr_ilo_get_captain1_m2860228591 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Entity.Behavior.NormalBvr::ilo_GetMinMarshalPoint2(HeroMgr,System.Int32&)
extern "C"  GameObject_t3674682005 * NormalBvr_ilo_GetMinMarshalPoint2_m95348896 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, int32_t* ___marshalIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.NormalBvr::ilo_DispatchEvent3(CEvent.ZEvent)
extern "C"  void NormalBvr_ilo_DispatchEvent3_m1267193814 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.NormalBvr::ilo_get_entity4(Entity.Behavior.IBehavior)
extern "C"  CombatEntity_t684137495 * NormalBvr_ilo_get_entity4_m2830705450 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.NormalBvr::ilo_set_canMove5(CombatEntity,System.Boolean)
extern "C"  void NormalBvr_ilo_set_canMove5_m2819365617 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AnimationRunner Entity.Behavior.NormalBvr::ilo_get_characterAnim6(CombatEntity)
extern "C"  AnimationRunner_t1015409588 * NormalBvr_ilo_get_characterAnim6_m627035489 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.NormalBvr::ilo_Play7(AnimationRunner,AnimationRunner/AniType,System.Single,System.Boolean)
extern "C"  void NormalBvr_ilo_Play7_m1993440732 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, int32_t ___aniType1, float ___playTime2, bool ___isskill3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
