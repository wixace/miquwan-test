﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._e755cffed1eea0e0877909b557c635fa
struct _e755cffed1eea0e0877909b557c635fa_t204486147;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._e755cffed1eea0e0877909b557c635fa::.ctor()
extern "C"  void _e755cffed1eea0e0877909b557c635fa__ctor_m3226207114 (_e755cffed1eea0e0877909b557c635fa_t204486147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e755cffed1eea0e0877909b557c635fa::_e755cffed1eea0e0877909b557c635fam2(System.Int32)
extern "C"  int32_t _e755cffed1eea0e0877909b557c635fa__e755cffed1eea0e0877909b557c635fam2_m3144334009 (_e755cffed1eea0e0877909b557c635fa_t204486147 * __this, int32_t ____e755cffed1eea0e0877909b557c635faa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e755cffed1eea0e0877909b557c635fa::_e755cffed1eea0e0877909b557c635fam(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _e755cffed1eea0e0877909b557c635fa__e755cffed1eea0e0877909b557c635fam_m2420404253 (_e755cffed1eea0e0877909b557c635fa_t204486147 * __this, int32_t ____e755cffed1eea0e0877909b557c635faa0, int32_t ____e755cffed1eea0e0877909b557c635fa761, int32_t ____e755cffed1eea0e0877909b557c635fac2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
