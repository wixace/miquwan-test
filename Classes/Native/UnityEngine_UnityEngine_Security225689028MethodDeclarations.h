﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Security
struct Security_t225689028;
// System.String
struct String_t;
// System.Reflection.Assembly
struct Assembly_t1418687608;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void UnityEngine.Security::.ctor()
extern "C"  void Security__ctor_m2182224429 (Security_t225689028 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Security::PrefetchSocketPolicy(System.String,System.Int32)
extern "C"  bool Security_PrefetchSocketPolicy_m682431054 (Il2CppObject * __this /* static, unused */, String_t* ___ip0, int32_t ___atPort1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Security::PrefetchSocketPolicy(System.String,System.Int32,System.Int32)
extern "C"  bool Security_PrefetchSocketPolicy_m1901508937 (Il2CppObject * __this /* static, unused */, String_t* ___ip0, int32_t ___atPort1, int32_t ___timeout2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly UnityEngine.Security::LoadAndVerifyAssembly(System.Byte[],System.String)
extern "C"  Assembly_t1418687608 * Security_LoadAndVerifyAssembly_m2336320367 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___assemblyData0, String_t* ___authorizationKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly UnityEngine.Security::LoadAndVerifyAssembly(System.Byte[])
extern "C"  Assembly_t1418687608 * Security_LoadAndVerifyAssembly_m428181619 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___assemblyData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
