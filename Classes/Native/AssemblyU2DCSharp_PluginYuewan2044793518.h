﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginYuewan
struct  PluginYuewan_t2044793518  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginYuewan::uid
	String_t* ___uid_2;
	// System.String PluginYuewan::token
	String_t* ___token_3;
	// System.String PluginYuewan::userName
	String_t* ___userName_4;
	// System.String PluginYuewan::expires
	String_t* ___expires_5;
	// System.String PluginYuewan::authroizeCode
	String_t* ___authroizeCode_6;
	// System.String PluginYuewan::CheckID
	String_t* ___CheckID_7;
	// System.String PluginYuewan::configId
	String_t* ___configId_8;
	// Mihua.SDK.PayInfo PluginYuewan::payInfo
	PayInfo_t1775308120 * ___payInfo_9;
	// System.Boolean PluginYuewan::isOut
	bool ___isOut_10;

public:
	inline static int32_t get_offset_of_uid_2() { return static_cast<int32_t>(offsetof(PluginYuewan_t2044793518, ___uid_2)); }
	inline String_t* get_uid_2() const { return ___uid_2; }
	inline String_t** get_address_of_uid_2() { return &___uid_2; }
	inline void set_uid_2(String_t* value)
	{
		___uid_2 = value;
		Il2CppCodeGenWriteBarrier(&___uid_2, value);
	}

	inline static int32_t get_offset_of_token_3() { return static_cast<int32_t>(offsetof(PluginYuewan_t2044793518, ___token_3)); }
	inline String_t* get_token_3() const { return ___token_3; }
	inline String_t** get_address_of_token_3() { return &___token_3; }
	inline void set_token_3(String_t* value)
	{
		___token_3 = value;
		Il2CppCodeGenWriteBarrier(&___token_3, value);
	}

	inline static int32_t get_offset_of_userName_4() { return static_cast<int32_t>(offsetof(PluginYuewan_t2044793518, ___userName_4)); }
	inline String_t* get_userName_4() const { return ___userName_4; }
	inline String_t** get_address_of_userName_4() { return &___userName_4; }
	inline void set_userName_4(String_t* value)
	{
		___userName_4 = value;
		Il2CppCodeGenWriteBarrier(&___userName_4, value);
	}

	inline static int32_t get_offset_of_expires_5() { return static_cast<int32_t>(offsetof(PluginYuewan_t2044793518, ___expires_5)); }
	inline String_t* get_expires_5() const { return ___expires_5; }
	inline String_t** get_address_of_expires_5() { return &___expires_5; }
	inline void set_expires_5(String_t* value)
	{
		___expires_5 = value;
		Il2CppCodeGenWriteBarrier(&___expires_5, value);
	}

	inline static int32_t get_offset_of_authroizeCode_6() { return static_cast<int32_t>(offsetof(PluginYuewan_t2044793518, ___authroizeCode_6)); }
	inline String_t* get_authroizeCode_6() const { return ___authroizeCode_6; }
	inline String_t** get_address_of_authroizeCode_6() { return &___authroizeCode_6; }
	inline void set_authroizeCode_6(String_t* value)
	{
		___authroizeCode_6 = value;
		Il2CppCodeGenWriteBarrier(&___authroizeCode_6, value);
	}

	inline static int32_t get_offset_of_CheckID_7() { return static_cast<int32_t>(offsetof(PluginYuewan_t2044793518, ___CheckID_7)); }
	inline String_t* get_CheckID_7() const { return ___CheckID_7; }
	inline String_t** get_address_of_CheckID_7() { return &___CheckID_7; }
	inline void set_CheckID_7(String_t* value)
	{
		___CheckID_7 = value;
		Il2CppCodeGenWriteBarrier(&___CheckID_7, value);
	}

	inline static int32_t get_offset_of_configId_8() { return static_cast<int32_t>(offsetof(PluginYuewan_t2044793518, ___configId_8)); }
	inline String_t* get_configId_8() const { return ___configId_8; }
	inline String_t** get_address_of_configId_8() { return &___configId_8; }
	inline void set_configId_8(String_t* value)
	{
		___configId_8 = value;
		Il2CppCodeGenWriteBarrier(&___configId_8, value);
	}

	inline static int32_t get_offset_of_payInfo_9() { return static_cast<int32_t>(offsetof(PluginYuewan_t2044793518, ___payInfo_9)); }
	inline PayInfo_t1775308120 * get_payInfo_9() const { return ___payInfo_9; }
	inline PayInfo_t1775308120 ** get_address_of_payInfo_9() { return &___payInfo_9; }
	inline void set_payInfo_9(PayInfo_t1775308120 * value)
	{
		___payInfo_9 = value;
		Il2CppCodeGenWriteBarrier(&___payInfo_9, value);
	}

	inline static int32_t get_offset_of_isOut_10() { return static_cast<int32_t>(offsetof(PluginYuewan_t2044793518, ___isOut_10)); }
	inline bool get_isOut_10() const { return ___isOut_10; }
	inline bool* get_address_of_isOut_10() { return &___isOut_10; }
	inline void set_isOut_10(bool value)
	{
		___isOut_10 = value;
	}
};

struct PluginYuewan_t2044793518_StaticFields
{
public:
	// System.Action PluginYuewan::<>f__am$cache9
	Action_t3771233898 * ___U3CU3Ef__amU24cache9_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_11() { return static_cast<int32_t>(offsetof(PluginYuewan_t2044793518_StaticFields, ___U3CU3Ef__amU24cache9_11)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache9_11() const { return ___U3CU3Ef__amU24cache9_11; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache9_11() { return &___U3CU3Ef__amU24cache9_11; }
	inline void set_U3CU3Ef__amU24cache9_11(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache9_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
