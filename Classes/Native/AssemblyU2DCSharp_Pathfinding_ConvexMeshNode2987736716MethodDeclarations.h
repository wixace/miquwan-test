﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.ConvexMeshNode
struct ConvexMeshNode_t2987736716;
// AstarPath
struct AstarPath_t4090270936;
// Pathfinding.INavmeshHolder
struct INavmeshHolder_t1019551337;
// Pathfinding.GraphNodeDelegate
struct GraphNodeDelegate_t1466738551;
// Pathfinding.Path
struct Path_t1974241691;
// Pathfinding.PathNode
struct PathNode_t417131581;
// Pathfinding.PathHandler
struct PathHandler_t918952263;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNodeDelegate1466738551.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_Pathfinding_PathNode417131581.h"
#include "AssemblyU2DCSharp_Pathfinding_PathHandler918952263.h"
#include "AssemblyU2DCSharp_Pathfinding_ConvexMeshNode2987736716.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.ConvexMeshNode::.ctor(AstarPath)
extern "C"  void ConvexMeshNode__ctor_m1346486111 (ConvexMeshNode_t2987736716 * __this, AstarPath_t4090270936 * ___astar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ConvexMeshNode::.cctor()
extern "C"  void ConvexMeshNode__cctor_m2174252978 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.INavmeshHolder Pathfinding.ConvexMeshNode::GetNavmeshHolder(System.UInt32)
extern "C"  Il2CppObject * ConvexMeshNode_GetNavmeshHolder_m3557369003 (Il2CppObject * __this /* static, unused */, uint32_t ___graphIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ConvexMeshNode::SetPosition(Pathfinding.Int3)
extern "C"  void ConvexMeshNode_SetPosition_m3186112726 (ConvexMeshNode_t2987736716 * __this, Int3_t1974045594  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.ConvexMeshNode::GetVertexIndex(System.Int32)
extern "C"  int32_t ConvexMeshNode_GetVertexIndex_m1556207296 (ConvexMeshNode_t2987736716 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.ConvexMeshNode::GetVertex(System.Int32)
extern "C"  Int3_t1974045594  ConvexMeshNode_GetVertex_m4223263701 (ConvexMeshNode_t2987736716 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.ConvexMeshNode::GetVertexCount()
extern "C"  int32_t ConvexMeshNode_GetVertexCount_m3573718252 (ConvexMeshNode_t2987736716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.ConvexMeshNode::ClosestPointOnNode(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  ConvexMeshNode_ClosestPointOnNode_m486275550 (ConvexMeshNode_t2987736716 * __this, Vector3_t4282066566  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.ConvexMeshNode::ClosestPointOnNodeXZ(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  ConvexMeshNode_ClosestPointOnNodeXZ_m2158539420 (ConvexMeshNode_t2987736716 * __this, Vector3_t4282066566  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ConvexMeshNode::GetConnections(Pathfinding.GraphNodeDelegate)
extern "C"  void ConvexMeshNode_GetConnections_m2768204543 (ConvexMeshNode_t2987736716 * __this, GraphNodeDelegate_t1466738551 * ___del0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ConvexMeshNode::Open(Pathfinding.Path,Pathfinding.PathNode,Pathfinding.PathHandler)
extern "C"  void ConvexMeshNode_Open_m1665880418 (ConvexMeshNode_t2987736716 * __this, Path_t1974241691 * ___path0, PathNode_t417131581 * ___pathNode1, PathHandler_t918952263 * ___handler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.ConvexMeshNode::ilo_GetVertexIndex1(Pathfinding.ConvexMeshNode,System.Int32)
extern "C"  int32_t ConvexMeshNode_ilo_GetVertexIndex1_m1551904454 (Il2CppObject * __this /* static, unused */, ConvexMeshNode_t2987736716 * ____this0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.PathNode Pathfinding.ConvexMeshNode::ilo_GetPathNode2(Pathfinding.PathHandler,Pathfinding.GraphNode)
extern "C"  PathNode_t417131581 * ConvexMeshNode_ilo_GetPathNode2_m1080116128 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ConvexMeshNode::ilo_set_cost3(Pathfinding.PathNode,System.UInt32)
extern "C"  void ConvexMeshNode_ilo_set_cost3_m549611538 (Il2CppObject * __this /* static, unused */, PathNode_t417131581 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.ConvexMeshNode::ilo_CalculateHScore4(Pathfinding.Path,Pathfinding.GraphNode)
extern "C"  uint32_t ConvexMeshNode_ilo_CalculateHScore4_m189481778 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ConvexMeshNode::ilo_UpdateG5(Pathfinding.GraphNode,Pathfinding.Path,Pathfinding.PathNode)
extern "C"  void ConvexMeshNode_ilo_UpdateG5_m2724934391 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, Path_t1974241691 * ___path1, PathNode_t417131581 * ___pathNode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ConvexMeshNode::ilo_PushNode6(Pathfinding.PathHandler,Pathfinding.PathNode)
extern "C"  void ConvexMeshNode_ilo_PushNode6_m2810951454 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, PathNode_t417131581 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.ConvexMeshNode::ilo_get_G7(Pathfinding.PathNode)
extern "C"  uint32_t ConvexMeshNode_ilo_get_G7_m971159167 (Il2CppObject * __this /* static, unused */, PathNode_t417131581 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.ConvexMeshNode::ilo_GetTraversalCost8(Pathfinding.Path,Pathfinding.GraphNode)
extern "C"  uint32_t ConvexMeshNode_ilo_GetTraversalCost8_m1067835397 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
