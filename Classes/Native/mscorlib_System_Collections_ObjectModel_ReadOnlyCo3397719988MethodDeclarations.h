﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>
struct ReadOnlyCollection_1_t3397719988;
// System.Collections.Generic.IList`1<PushType>
struct IList_1_t240322359;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// PushType[]
struct PushTypeU5BU5D_t3978570141;
// System.Collections.Generic.IEnumerator`1<PushType>
struct IEnumerator_1_t3752507501;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PushType1840642452.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m1532088284_gshared (ReadOnlyCollection_1_t3397719988 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1532088284(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t3397719988 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1532088284_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2983375814_gshared (ReadOnlyCollection_1_t3397719988 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2983375814(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t3397719988 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2983375814_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2060740_gshared (ReadOnlyCollection_1_t3397719988 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2060740(__this, method) ((  void (*) (ReadOnlyCollection_1_t3397719988 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2060740_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3333273389_gshared (ReadOnlyCollection_1_t3397719988 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3333273389(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t3397719988 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3333273389_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3321754673_gshared (ReadOnlyCollection_1_t3397719988 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3321754673(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t3397719988 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3321754673_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1207126259_gshared (ReadOnlyCollection_1_t3397719988 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1207126259(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3397719988 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1207126259_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2772208857_gshared (ReadOnlyCollection_1_t3397719988 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2772208857(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3397719988 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2772208857_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1039720644_gshared (ReadOnlyCollection_1_t3397719988 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1039720644(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3397719988 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1039720644_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3778601022_gshared (ReadOnlyCollection_1_t3397719988 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3778601022(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3397719988 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3778601022_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m654908555_gshared (ReadOnlyCollection_1_t3397719988 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m654908555(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3397719988 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m654908555_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2464710874_gshared (ReadOnlyCollection_1_t3397719988 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2464710874(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3397719988 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2464710874_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m458473507_gshared (ReadOnlyCollection_1_t3397719988 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m458473507(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3397719988 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m458473507_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m3849525145_gshared (ReadOnlyCollection_1_t3397719988 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m3849525145(__this, method) ((  void (*) (ReadOnlyCollection_1_t3397719988 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m3849525145_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2220075133_gshared (ReadOnlyCollection_1_t3397719988 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2220075133(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3397719988 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2220075133_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2090937595_gshared (ReadOnlyCollection_1_t3397719988 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2090937595(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3397719988 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2090937595_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m2062220526_gshared (ReadOnlyCollection_1_t3397719988 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2062220526(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3397719988 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2062220526_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m2557100410_gshared (ReadOnlyCollection_1_t3397719988 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m2557100410(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t3397719988 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2557100410_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3546057086_gshared (ReadOnlyCollection_1_t3397719988 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3546057086(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3397719988 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3546057086_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m51872191_gshared (ReadOnlyCollection_1_t3397719988 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m51872191(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3397719988 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m51872191_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4150808497_gshared (ReadOnlyCollection_1_t3397719988 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4150808497(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3397719988 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4150808497_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2456730028_gshared (ReadOnlyCollection_1_t3397719988 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2456730028(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3397719988 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2456730028_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m533258189_gshared (ReadOnlyCollection_1_t3397719988 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m533258189(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3397719988 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m533258189_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m4262926584_gshared (ReadOnlyCollection_1_t3397719988 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m4262926584(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3397719988 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m4262926584_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m2427954501_gshared (ReadOnlyCollection_1_t3397719988 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2427954501(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3397719988 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m2427954501_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3338815094_gshared (ReadOnlyCollection_1_t3397719988 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3338815094(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3397719988 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m3338815094_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m3159193846_gshared (ReadOnlyCollection_1_t3397719988 * __this, PushTypeU5BU5D_t3978570141* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m3159193846(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3397719988 *, PushTypeU5BU5D_t3978570141*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m3159193846_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m328761677_gshared (ReadOnlyCollection_1_t3397719988 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m328761677(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t3397719988 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m328761677_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m1356482114_gshared (ReadOnlyCollection_1_t3397719988 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1356482114(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3397719988 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1356482114_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m3213547385_gshared (ReadOnlyCollection_1_t3397719988 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m3213547385(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t3397719988 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3213547385_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m1978476313_gshared (ReadOnlyCollection_1_t3397719988 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1978476313(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3397719988 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1978476313_gshared)(__this, ___index0, method)
