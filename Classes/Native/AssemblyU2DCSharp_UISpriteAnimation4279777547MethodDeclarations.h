﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UISpriteAnimation
struct UISpriteAnimation_t4279777547;
// System.String
struct String_t;
// UISprite
struct UISprite_t661437049;
// UIAtlas
struct UIAtlas_t281921111;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_UISpriteAnimation4279777547.h"
#include "AssemblyU2DCSharp_UISprite661437049.h"

// System.Void UISpriteAnimation::.ctor()
extern "C"  void UISpriteAnimation__ctor_m3977474240 (UISpriteAnimation_t4279777547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UISpriteAnimation::get_frames()
extern "C"  int32_t UISpriteAnimation_get_frames_m1438610501 (UISpriteAnimation_t4279777547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UISpriteAnimation::get_framesPerSecond()
extern "C"  int32_t UISpriteAnimation_get_framesPerSecond_m2544945614 (UISpriteAnimation_t4279777547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteAnimation::set_framesPerSecond(System.Int32)
extern "C"  void UISpriteAnimation_set_framesPerSecond_m1620652829 (UISpriteAnimation_t4279777547 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UISpriteAnimation::get_namePrefix()
extern "C"  String_t* UISpriteAnimation_get_namePrefix_m2167980967 (UISpriteAnimation_t4279777547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteAnimation::set_namePrefix(System.String)
extern "C"  void UISpriteAnimation_set_namePrefix_m178335908 (UISpriteAnimation_t4279777547 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISpriteAnimation::get_loop()
extern "C"  bool UISpriteAnimation_get_loop_m8362845 (UISpriteAnimation_t4279777547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteAnimation::set_loop(System.Boolean)
extern "C"  void UISpriteAnimation_set_loop_m3202610108 (UISpriteAnimation_t4279777547 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISpriteAnimation::get_isPlaying()
extern "C"  bool UISpriteAnimation_get_isPlaying_m1764199917 (UISpriteAnimation_t4279777547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteAnimation::Start()
extern "C"  void UISpriteAnimation_Start_m2924612032 (UISpriteAnimation_t4279777547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteAnimation::Update()
extern "C"  void UISpriteAnimation_Update_m474511949 (UISpriteAnimation_t4279777547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteAnimation::RebuildSpriteList()
extern "C"  void UISpriteAnimation_RebuildSpriteList_m2996903516 (UISpriteAnimation_t4279777547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteAnimation::Play()
extern "C"  void UISpriteAnimation_Play_m2910565048 (UISpriteAnimation_t4279777547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteAnimation::Pause()
extern "C"  void UISpriteAnimation_Pause_m4031600212 (UISpriteAnimation_t4279777547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteAnimation::ResetToBeginning()
extern "C"  void UISpriteAnimation_ResetToBeginning_m4170941207 (UISpriteAnimation_t4279777547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteAnimation::ilo_RebuildSpriteList1(UISpriteAnimation)
extern "C"  void UISpriteAnimation_ilo_RebuildSpriteList1_m1823997597 (Il2CppObject * __this /* static, unused */, UISpriteAnimation_t4279777547 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteAnimation::ilo_MakePixelPerfect2(UISprite)
extern "C"  void UISpriteAnimation_ilo_MakePixelPerfect2_m1334879311 (Il2CppObject * __this /* static, unused */, UISprite_t661437049 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIAtlas UISpriteAnimation::ilo_get_atlas3(UISprite)
extern "C"  UIAtlas_t281921111 * UISpriteAnimation_ilo_get_atlas3_m3408381619 (Il2CppObject * __this /* static, unused */, UISprite_t661437049 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
