﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<Newtonsoft.Json.Linq.JTokenType>
struct DefaultComparer_t2425143338;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType3916897561.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Newtonsoft.Json.Linq.JTokenType>::.ctor()
extern "C"  void DefaultComparer__ctor_m2373559494_gshared (DefaultComparer_t2425143338 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2373559494(__this, method) ((  void (*) (DefaultComparer_t2425143338 *, const MethodInfo*))DefaultComparer__ctor_m2373559494_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Newtonsoft.Json.Linq.JTokenType>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m1672152105_gshared (DefaultComparer_t2425143338 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m1672152105(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t2425143338 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m1672152105_gshared)(__this, ___x0, ___y1, method)
