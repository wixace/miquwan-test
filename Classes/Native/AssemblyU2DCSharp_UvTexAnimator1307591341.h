﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UITexture
struct UITexture_t3903132647;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UvTexAnimator
struct  UvTexAnimator_t1307591341  : public MonoBehaviour_t667441552
{
public:
	// System.Single UvTexAnimator::tileX
	float ___tileX_2;
	// System.Single UvTexAnimator::tileY
	float ___tileY_3;
	// System.Single UvTexAnimator::fps
	float ___fps_4;
	// UnityEngine.Rect UvTexAnimator::rect
	Rect_t4241904616  ___rect_5;
	// UITexture UvTexAnimator::tex
	UITexture_t3903132647 * ___tex_6;

public:
	inline static int32_t get_offset_of_tileX_2() { return static_cast<int32_t>(offsetof(UvTexAnimator_t1307591341, ___tileX_2)); }
	inline float get_tileX_2() const { return ___tileX_2; }
	inline float* get_address_of_tileX_2() { return &___tileX_2; }
	inline void set_tileX_2(float value)
	{
		___tileX_2 = value;
	}

	inline static int32_t get_offset_of_tileY_3() { return static_cast<int32_t>(offsetof(UvTexAnimator_t1307591341, ___tileY_3)); }
	inline float get_tileY_3() const { return ___tileY_3; }
	inline float* get_address_of_tileY_3() { return &___tileY_3; }
	inline void set_tileY_3(float value)
	{
		___tileY_3 = value;
	}

	inline static int32_t get_offset_of_fps_4() { return static_cast<int32_t>(offsetof(UvTexAnimator_t1307591341, ___fps_4)); }
	inline float get_fps_4() const { return ___fps_4; }
	inline float* get_address_of_fps_4() { return &___fps_4; }
	inline void set_fps_4(float value)
	{
		___fps_4 = value;
	}

	inline static int32_t get_offset_of_rect_5() { return static_cast<int32_t>(offsetof(UvTexAnimator_t1307591341, ___rect_5)); }
	inline Rect_t4241904616  get_rect_5() const { return ___rect_5; }
	inline Rect_t4241904616 * get_address_of_rect_5() { return &___rect_5; }
	inline void set_rect_5(Rect_t4241904616  value)
	{
		___rect_5 = value;
	}

	inline static int32_t get_offset_of_tex_6() { return static_cast<int32_t>(offsetof(UvTexAnimator_t1307591341, ___tex_6)); }
	inline UITexture_t3903132647 * get_tex_6() const { return ___tex_6; }
	inline UITexture_t3903132647 ** get_address_of_tex_6() { return &___tex_6; }
	inline void set_tex_6(UITexture_t3903132647 * value)
	{
		___tex_6 = value;
		Il2CppCodeGenWriteBarrier(&___tex_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
