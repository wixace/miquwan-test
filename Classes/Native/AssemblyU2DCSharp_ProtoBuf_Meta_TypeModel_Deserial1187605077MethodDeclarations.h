﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Meta.TypeModel/DeserializeItemsIterator
struct DeserializeItemsIterator_t1187605077;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.IO.Stream
struct Stream_t1561764144;
// System.Type
struct Type_t;
// ProtoBuf.Serializer/TypeResolver
struct TypeResolver_t356109658;
// ProtoBuf.SerializationContext
struct SerializationContext_t3997850667;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_ProtoBuf_PrefixStyle1974492709.h"
#include "AssemblyU2DCSharp_ProtoBuf_Serializer_TypeResolver356109658.h"
#include "AssemblyU2DCSharp_ProtoBuf_SerializationContext3997850667.h"

// System.Void ProtoBuf.Meta.TypeModel/DeserializeItemsIterator::.ctor(ProtoBuf.Meta.TypeModel,System.IO.Stream,System.Type,ProtoBuf.PrefixStyle,System.Int32,ProtoBuf.Serializer/TypeResolver,ProtoBuf.SerializationContext)
extern "C"  void DeserializeItemsIterator__ctor_m2411945372 (DeserializeItemsIterator_t1187605077 * __this, TypeModel_t2730011105 * ___model0, Stream_t1561764144 * ___source1, Type_t * ___type2, int32_t ___style3, int32_t ___expectedField4, TypeResolver_t356109658 * ___resolver5, SerializationContext_t3997850667 * ___context6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ProtoBuf.Meta.TypeModel/DeserializeItemsIterator::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * DeserializeItemsIterator_System_Collections_IEnumerable_GetEnumerator_m3951959627 (DeserializeItemsIterator_t1187605077 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel/DeserializeItemsIterator::System.Collections.IEnumerator.Reset()
extern "C"  void DeserializeItemsIterator_System_Collections_IEnumerator_Reset_m1376275540 (DeserializeItemsIterator_t1187605077 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.TypeModel/DeserializeItemsIterator::MoveNext()
extern "C"  bool DeserializeItemsIterator_MoveNext_m4068212790 (DeserializeItemsIterator_t1187605077 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.TypeModel/DeserializeItemsIterator::get_Current()
extern "C"  Il2CppObject * DeserializeItemsIterator_get_Current_m660778239 (DeserializeItemsIterator_t1187605077 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
