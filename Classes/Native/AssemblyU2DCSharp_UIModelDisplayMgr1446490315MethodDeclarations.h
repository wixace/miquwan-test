﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIModelDisplayMgr
struct UIModelDisplayMgr_t1446490315;
// UIFBXDisplaySprite
struct UIFBXDisplaySprite_t2233392895;
// System.String
struct String_t;
// AnimationRunner
struct AnimationRunner_t1015409588;
// SpinWithMouse
struct SpinWithMouse_t1751689533;
// System.Action
struct Action_t3771233898;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// UIModelDisplay
struct UIModelDisplay_t1730520589;
// System.Object
struct Il2CppObject;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// herosCfg
struct herosCfg_t3676934635;
// monstersCfg
struct monstersCfg_t1542396363;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// PetsCfg
struct PetsCfg_t988016752;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIModelDisplayType1891752679.h"
#include "AssemblyU2DCSharp_UIFBXDisplaySprite2233392895.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniType1006238651.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_SpinWithMouse1751689533.h"
#include "System_Core_System_Action3771233898.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_UIModelDisplayMgr1446490315.h"
#include "AssemblyU2DCSharp_UIModelDisplay1730520589.h"
#include "AssemblyU2DCSharp_AnimationRunner1015409588.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniState1127540784.h"
#include "UnityEngine_UnityEngine_WrapMode1491636113.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_CSDatacfgManager1565254243.h"

// System.Void UIModelDisplayMgr::.ctor()
extern "C"  void UIModelDisplayMgr__ctor_m2206118144 (UIModelDisplayMgr_t1446490315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::.cctor()
extern "C"  void UIModelDisplayMgr__cctor_m3483056813 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIModelDisplayMgr UIModelDisplayMgr::get_Instance()
extern "C"  UIModelDisplayMgr_t1446490315 * UIModelDisplayMgr_get_Instance_m4234074682 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::Init()
extern "C"  void UIModelDisplayMgr_Init_m1823817044 (UIModelDisplayMgr_t1446490315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplayMgr::BindUIFBXSprite(UIModelDisplayType,UIFBXDisplaySprite)
extern "C"  bool UIModelDisplayMgr_BindUIFBXSprite_m944759660 (UIModelDisplayMgr_t1446490315 * __this, int32_t ___type0, UIFBXDisplaySprite_t2233392895 * ___sp1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::UnbindUIFBXSprite(UIModelDisplayType)
extern "C"  void UIModelDisplayMgr_UnbindUIFBXSprite_m3336475822 (UIModelDisplayMgr_t1446490315 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::SetUIModelDisplay(UIModelDisplayType,System.Boolean)
extern "C"  void UIModelDisplayMgr_SetUIModelDisplay_m176057335 (UIModelDisplayMgr_t1446490315 * __this, int32_t ___type0, bool ___active1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::SetUIModelCameraFOV(UIModelDisplayType,System.Single)
extern "C"  void UIModelDisplayMgr_SetUIModelCameraFOV_m3264642513 (UIModelDisplayMgr_t1446490315 * __this, int32_t ___type0, float ___fCameraFOV1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::SetTransform(UnityEngine.Vector3,UnityEngine.Vector3,UIModelDisplayType,System.Int32,System.Single)
extern "C"  void UIModelDisplayMgr_SetTransform_m467365275 (UIModelDisplayMgr_t1446490315 * __this, Vector3_t4282066566  ___pos0, Vector3_t4282066566  ___rot1, int32_t ___type2, int32_t ___id3, float ___sca4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::PlayAniamtion(UIModelDisplayType,System.Int32,AnimationRunner/AniType)
extern "C"  void UIModelDisplayMgr_PlayAniamtion_m3746005225 (UIModelDisplayMgr_t1446490315 * __this, int32_t ___type0, int32_t ___id1, int32_t ___ani2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::PlayAniamtion(UIModelDisplayType,System.Int32,System.String,System.Boolean)
extern "C"  void UIModelDisplayMgr_PlayAniamtion_m3423083689 (UIModelDisplayMgr_t1446490315 * __this, int32_t ___type0, int32_t ___id1, String_t* ___ani2, bool ___isHero3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::PlayMonsterAniamtion(UIModelDisplayType,System.Int32,AnimationRunner/AniType)
extern "C"  void UIModelDisplayMgr_PlayMonsterAniamtion_m3609488213 (UIModelDisplayMgr_t1446490315 * __this, int32_t ___type0, int32_t ___id1, int32_t ___ani2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AnimationRunner UIModelDisplayMgr::GetCurModelAnimation(UIModelDisplayType,System.Int32)
extern "C"  AnimationRunner_t1015409588 * UIModelDisplayMgr_GetCurModelAnimation_m893088500 (UIModelDisplayMgr_t1446490315 * __this, int32_t ___type0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::SetActive(UIModelDisplayType,System.Int32,System.Boolean)
extern "C"  void UIModelDisplayMgr_SetActive_m4217244069 (UIModelDisplayMgr_t1446490315 * __this, int32_t ___type0, int32_t ___id1, bool ___action2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::RelationSpin(SpinWithMouse,UIModelDisplayType,System.Int32)
extern "C"  void UIModelDisplayMgr_RelationSpin_m311440171 (UIModelDisplayMgr_t1446490315 * __this, SpinWithMouse_t1751689533 * ___spin0, int32_t ___type1, int32_t ___id2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::ChangeGaoliangShader(UIModelDisplayType,System.Int32,System.Boolean)
extern "C"  void UIModelDisplayMgr_ChangeGaoliangShader_m3556749600 (UIModelDisplayMgr_t1446490315 * __this, int32_t ___type0, int32_t ___id1, bool ___isHero2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplayMgr::IsThere(UIModelDisplayType,System.Int32)
extern "C"  bool UIModelDisplayMgr_IsThere_m3209677138 (UIModelDisplayMgr_t1446490315 * __this, int32_t ___type0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::AttachObject(UIModelDisplayType,System.Int32,System.Action,System.Boolean)
extern "C"  void UIModelDisplayMgr_AttachObject_m1263399824 (UIModelDisplayMgr_t1446490315 * __this, int32_t ___type0, int32_t ___id1, Action_t3771233898 * ___action2, bool ___isHero3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::AttachObject(UIModelDisplayType,System.Int32,UnityEngine.GameObject)
extern "C"  void UIModelDisplayMgr_AttachObject_m303547048 (UIModelDisplayMgr_t1446490315 * __this, int32_t ___type0, int32_t ___id1, GameObject_t3674682005 * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::CreateUIModelDisplay(UIModelDisplayType,System.Int32,System.Int32,System.Int32,System.Int32,System.Single)
extern "C"  void UIModelDisplayMgr_CreateUIModelDisplay_m266689323 (UIModelDisplayMgr_t1446490315 * __this, int32_t ___type0, int32_t ___width1, int32_t ___height2, int32_t ___layer3, int32_t ___depth4, float ___y5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::SetActive(UIModelDisplayType,System.Boolean)
extern "C"  void UIModelDisplayMgr_SetActive_m2457762110 (UIModelDisplayMgr_t1446490315 * __this, int32_t ___type0, bool ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UIModelDisplayMgr::GetModel(UIModelDisplayType,System.Int32)
extern "C"  GameObject_t3674682005 * UIModelDisplayMgr_GetModel_m763574812 (UIModelDisplayMgr_t1446490315 * __this, int32_t ___type0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UIModelDisplayMgr::ModelPath(UIModelDisplayType)
extern "C"  String_t* UIModelDisplayMgr_ModelPath_m1239272040 (UIModelDisplayMgr_t1446490315 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::CreatHeroModel(UIModelDisplayType,System.Int32,System.Action,System.String)
extern "C"  void UIModelDisplayMgr_CreatHeroModel_m2302940903 (UIModelDisplayMgr_t1446490315 * __this, int32_t ___type0, int32_t ___id1, Action_t3771233898 * ___action2, String_t* ___name3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::EventHeroLightState(CEvent.ZEvent)
extern "C"  void UIModelDisplayMgr_EventHeroLightState_m1003823118 (UIModelDisplayMgr_t1446490315 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::ClearModel()
extern "C"  void UIModelDisplayMgr_ClearModel_m2567148512 (UIModelDisplayMgr_t1446490315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::Clear()
extern "C"  void UIModelDisplayMgr_Clear_m3907218731 (UIModelDisplayMgr_t1446490315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::ilo_CreateUIModelDisplay1(UIModelDisplayMgr,UIModelDisplayType,System.Int32,System.Int32,System.Int32,System.Int32,System.Single)
extern "C"  void UIModelDisplayMgr_ilo_CreateUIModelDisplay1_m3480981712 (Il2CppObject * __this /* static, unused */, UIModelDisplayMgr_t1446490315 * ____this0, int32_t ___type1, int32_t ___width2, int32_t ___height3, int32_t ___layer4, int32_t ___depth5, float ___y6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::ilo_Attach2(UIModelDisplay)
extern "C"  void UIModelDisplayMgr_ilo_Attach2_m2659881003 (Il2CppObject * __this /* static, unused */, UIModelDisplay_t1730520589 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::ilo_Detach3(UIModelDisplay)
extern "C"  void UIModelDisplayMgr_ilo_Detach3_m3958590110 (Il2CppObject * __this /* static, unused */, UIModelDisplay_t1730520589 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UIModelDisplayMgr::ilo_GetModel4(UIModelDisplayMgr,UIModelDisplayType,System.Int32)
extern "C"  GameObject_t3674682005 * UIModelDisplayMgr_ilo_GetModel4_m2210437758 (Il2CppObject * __this /* static, unused */, UIModelDisplayMgr_t1446490315 * ____this0, int32_t ___type1, int32_t ___id2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::ilo_set_aniState5(AnimationRunner,AnimationRunner/AniState)
extern "C"  void UIModelDisplayMgr_ilo_set_aniState5_m3907401574 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::ilo_Clear6(AnimationRunner)
extern "C"  void UIModelDisplayMgr_ilo_Clear6_m1535259582 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::ilo_Play7(AnimationRunner,AnimationRunner/AniType,System.Single,System.Boolean)
extern "C"  void UIModelDisplayMgr_ilo_Play7_m1029400343 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, int32_t ___aniType1, float ___playTime2, bool ___isskill3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::ilo_SetWrapModel8(AnimationRunner,UnityEngine.WrapMode)
extern "C"  void UIModelDisplayMgr_ilo_SetWrapModel8_m838790442 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, int32_t ___wrap1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::ilo_Log9(System.Object,System.Boolean)
extern "C"  void UIModelDisplayMgr_ilo_Log9_m2650265685 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSDatacfgManager UIModelDisplayMgr::ilo_get_CfgDataMgr10()
extern "C"  CSDatacfgManager_t1565254243 * UIModelDisplayMgr_ilo_get_CfgDataMgr10_m3909490607 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// herosCfg UIModelDisplayMgr::ilo_GetherosCfg11(CSDatacfgManager,System.Int32)
extern "C"  herosCfg_t3676934635 * UIModelDisplayMgr_ilo_GetherosCfg11_m1497060636 (Il2CppObject * __this /* static, unused */, CSDatacfgManager_t1565254243 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// monstersCfg UIModelDisplayMgr::ilo_GetmonstersCfg12(CSDatacfgManager,System.Int32)
extern "C"  monstersCfg_t1542396363 * UIModelDisplayMgr_ilo_GetmonstersCfg12_m732109429 (Il2CppObject * __this /* static, unused */, CSDatacfgManager_t1565254243 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] UIModelDisplayMgr::ilo_GetArticle_Int13(System.String)
extern "C"  Int32U5BU5D_t3230847821* UIModelDisplayMgr_ilo_GetArticle_Int13_m3817386725 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::ilo_SetActive14(UIModelDisplayMgr,UIModelDisplayType,System.Boolean)
extern "C"  void UIModelDisplayMgr_ilo_SetActive14_m1563579055 (Il2CppObject * __this /* static, unused */, UIModelDisplayMgr_t1446490315 * ____this0, int32_t ___type1, bool ___action2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplayMgr::ilo_IsThere15(UIModelDisplayMgr,UIModelDisplayType,System.Int32)
extern "C"  bool UIModelDisplayMgr_ilo_IsThere15_m1597398818 (Il2CppObject * __this /* static, unused */, UIModelDisplayMgr_t1446490315 * ____this0, int32_t ___type1, int32_t ___id2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PetsCfg UIModelDisplayMgr::ilo_GetPetsCfg16(CSDatacfgManager,System.Int32)
extern "C"  PetsCfg_t988016752 * UIModelDisplayMgr_ilo_GetPetsCfg16_m4292151463 (Il2CppObject * __this /* static, unused */, CSDatacfgManager_t1565254243 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::ilo_CloseLight17(UIModelDisplay)
extern "C"  void UIModelDisplayMgr_ilo_CloseLight17_m593841262 (Il2CppObject * __this /* static, unused */, UIModelDisplay_t1730520589 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UIModelDisplayMgr::ilo_ModelPath18(UIModelDisplayMgr,UIModelDisplayType)
extern "C"  String_t* UIModelDisplayMgr_ilo_ModelPath18_m3951743515 (Il2CppObject * __this /* static, unused */, UIModelDisplayMgr_t1446490315 * ____this0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr::ilo_RemoveEventListener19(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void UIModelDisplayMgr_ilo_RemoveEventListener19_m2556759878 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
