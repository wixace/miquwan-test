﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int3,System.Int32>
struct ShimEnumerator_t2447283848;
// System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>
struct Dictionary_2_t2731505821;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int3,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m3503827671_gshared (ShimEnumerator_t2447283848 * __this, Dictionary_2_t2731505821 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m3503827671(__this, ___host0, method) ((  void (*) (ShimEnumerator_t2447283848 *, Dictionary_2_t2731505821 *, const MethodInfo*))ShimEnumerator__ctor_m3503827671_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int3,System.Int32>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1974269162_gshared (ShimEnumerator_t2447283848 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1974269162(__this, method) ((  bool (*) (ShimEnumerator_t2447283848 *, const MethodInfo*))ShimEnumerator_MoveNext_m1974269162_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int3,System.Int32>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m2939352810_gshared (ShimEnumerator_t2447283848 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2939352810(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t2447283848 *, const MethodInfo*))ShimEnumerator_get_Entry_m2939352810_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int3,System.Int32>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3818568133_gshared (ShimEnumerator_t2447283848 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m3818568133(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2447283848 *, const MethodInfo*))ShimEnumerator_get_Key_m3818568133_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int3,System.Int32>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2790341399_gshared (ShimEnumerator_t2447283848 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m2790341399(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2447283848 *, const MethodInfo*))ShimEnumerator_get_Value_m2790341399_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int3,System.Int32>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2883182559_gshared (ShimEnumerator_t2447283848 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m2883182559(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2447283848 *, const MethodInfo*))ShimEnumerator_get_Current_m2883182559_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int3,System.Int32>::Reset()
extern "C"  void ShimEnumerator_Reset_m3761664681_gshared (ShimEnumerator_t2447283848 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m3761664681(__this, method) ((  void (*) (ShimEnumerator_t2447283848 *, const MethodInfo*))ShimEnumerator_Reset_m3761664681_gshared)(__this, method)
