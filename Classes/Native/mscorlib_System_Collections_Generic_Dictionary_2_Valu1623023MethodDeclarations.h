﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,HatredCtrl/stValue>
struct ValueCollection_t1623023;
// System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>
struct Dictionary_2_t1301017310;
// System.Collections.Generic.IEnumerator`1<HatredCtrl/stValue>
struct IEnumerator_1_t1042843163;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// HatredCtrl/stValue[]
struct stValueU5BU5D_t1121401207;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_HatredCtrl_stValue3425945410.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3527818014.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,HatredCtrl/stValue>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m98628870_gshared (ValueCollection_t1623023 * __this, Dictionary_2_t1301017310 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m98628870(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1623023 *, Dictionary_2_t1301017310 *, const MethodInfo*))ValueCollection__ctor_m98628870_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,HatredCtrl/stValue>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1950846572_gshared (ValueCollection_t1623023 * __this, stValue_t3425945410  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1950846572(__this, ___item0, method) ((  void (*) (ValueCollection_t1623023 *, stValue_t3425945410 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1950846572_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,HatredCtrl/stValue>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3208483125_gshared (ValueCollection_t1623023 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3208483125(__this, method) ((  void (*) (ValueCollection_t1623023 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3208483125_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,HatredCtrl/stValue>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3364432766_gshared (ValueCollection_t1623023 * __this, stValue_t3425945410  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3364432766(__this, ___item0, method) ((  bool (*) (ValueCollection_t1623023 *, stValue_t3425945410 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3364432766_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,HatredCtrl/stValue>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m974326691_gshared (ValueCollection_t1623023 * __this, stValue_t3425945410  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m974326691(__this, ___item0, method) ((  bool (*) (ValueCollection_t1623023 *, stValue_t3425945410 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m974326691_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,HatredCtrl/stValue>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2159196213_gshared (ValueCollection_t1623023 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2159196213(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1623023 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2159196213_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,HatredCtrl/stValue>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m1070330873_gshared (ValueCollection_t1623023 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m1070330873(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1623023 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1070330873_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,HatredCtrl/stValue>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2860709064_gshared (ValueCollection_t1623023 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2860709064(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1623023 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2860709064_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,HatredCtrl/stValue>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1639608625_gshared (ValueCollection_t1623023 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1639608625(__this, method) ((  bool (*) (ValueCollection_t1623023 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1639608625_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,HatredCtrl/stValue>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4177803409_gshared (ValueCollection_t1623023 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4177803409(__this, method) ((  bool (*) (ValueCollection_t1623023 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4177803409_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,HatredCtrl/stValue>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m2089021827_gshared (ValueCollection_t1623023 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2089021827(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1623023 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2089021827_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,HatredCtrl/stValue>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m2476091341_gshared (ValueCollection_t1623023 * __this, stValueU5BU5D_t1121401207* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m2476091341(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1623023 *, stValueU5BU5D_t1121401207*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2476091341_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,HatredCtrl/stValue>::GetEnumerator()
extern "C"  Enumerator_t3527818014  ValueCollection_GetEnumerator_m1892245686_gshared (ValueCollection_t1623023 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1892245686(__this, method) ((  Enumerator_t3527818014  (*) (ValueCollection_t1623023 *, const MethodInfo*))ValueCollection_GetEnumerator_m1892245686_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,HatredCtrl/stValue>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3779968331_gshared (ValueCollection_t1623023 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m3779968331(__this, method) ((  int32_t (*) (ValueCollection_t1623023 *, const MethodInfo*))ValueCollection_get_Count_m3779968331_gshared)(__this, method)
