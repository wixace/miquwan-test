﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSFightforData
struct  CSFightforData_t215334547  : public Il2CppObject
{
public:
	// System.Boolean CSFightforData::isRealMan
	bool ___isRealMan_0;
	// System.String CSFightforData::playerName
	String_t* ___playerName_1;
	// System.Int32 CSFightforData::power
	int32_t ___power_2;
	// System.Int32[] CSFightforData::monsterIdArr
	Int32U5BU5D_t3230847821* ___monsterIdArr_3;
	// System.Int32 CSFightforData::level
	int32_t ___level_4;
	// System.Int32 CSFightforData::country
	int32_t ___country_5;
	// System.String CSFightforData::addAttributeKey
	String_t* ___addAttributeKey_6;
	// System.Int32 CSFightforData::addAttributeValue
	int32_t ___addAttributeValue_7;
	// System.Int32 CSFightforData::ownCountry
	int32_t ___ownCountry_8;
	// System.String CSFightforData::ownAddAttributeKey
	String_t* ___ownAddAttributeKey_9;
	// System.Int32 CSFightforData::ownAddAttributeValue
	int32_t ___ownAddAttributeValue_10;

public:
	inline static int32_t get_offset_of_isRealMan_0() { return static_cast<int32_t>(offsetof(CSFightforData_t215334547, ___isRealMan_0)); }
	inline bool get_isRealMan_0() const { return ___isRealMan_0; }
	inline bool* get_address_of_isRealMan_0() { return &___isRealMan_0; }
	inline void set_isRealMan_0(bool value)
	{
		___isRealMan_0 = value;
	}

	inline static int32_t get_offset_of_playerName_1() { return static_cast<int32_t>(offsetof(CSFightforData_t215334547, ___playerName_1)); }
	inline String_t* get_playerName_1() const { return ___playerName_1; }
	inline String_t** get_address_of_playerName_1() { return &___playerName_1; }
	inline void set_playerName_1(String_t* value)
	{
		___playerName_1 = value;
		Il2CppCodeGenWriteBarrier(&___playerName_1, value);
	}

	inline static int32_t get_offset_of_power_2() { return static_cast<int32_t>(offsetof(CSFightforData_t215334547, ___power_2)); }
	inline int32_t get_power_2() const { return ___power_2; }
	inline int32_t* get_address_of_power_2() { return &___power_2; }
	inline void set_power_2(int32_t value)
	{
		___power_2 = value;
	}

	inline static int32_t get_offset_of_monsterIdArr_3() { return static_cast<int32_t>(offsetof(CSFightforData_t215334547, ___monsterIdArr_3)); }
	inline Int32U5BU5D_t3230847821* get_monsterIdArr_3() const { return ___monsterIdArr_3; }
	inline Int32U5BU5D_t3230847821** get_address_of_monsterIdArr_3() { return &___monsterIdArr_3; }
	inline void set_monsterIdArr_3(Int32U5BU5D_t3230847821* value)
	{
		___monsterIdArr_3 = value;
		Il2CppCodeGenWriteBarrier(&___monsterIdArr_3, value);
	}

	inline static int32_t get_offset_of_level_4() { return static_cast<int32_t>(offsetof(CSFightforData_t215334547, ___level_4)); }
	inline int32_t get_level_4() const { return ___level_4; }
	inline int32_t* get_address_of_level_4() { return &___level_4; }
	inline void set_level_4(int32_t value)
	{
		___level_4 = value;
	}

	inline static int32_t get_offset_of_country_5() { return static_cast<int32_t>(offsetof(CSFightforData_t215334547, ___country_5)); }
	inline int32_t get_country_5() const { return ___country_5; }
	inline int32_t* get_address_of_country_5() { return &___country_5; }
	inline void set_country_5(int32_t value)
	{
		___country_5 = value;
	}

	inline static int32_t get_offset_of_addAttributeKey_6() { return static_cast<int32_t>(offsetof(CSFightforData_t215334547, ___addAttributeKey_6)); }
	inline String_t* get_addAttributeKey_6() const { return ___addAttributeKey_6; }
	inline String_t** get_address_of_addAttributeKey_6() { return &___addAttributeKey_6; }
	inline void set_addAttributeKey_6(String_t* value)
	{
		___addAttributeKey_6 = value;
		Il2CppCodeGenWriteBarrier(&___addAttributeKey_6, value);
	}

	inline static int32_t get_offset_of_addAttributeValue_7() { return static_cast<int32_t>(offsetof(CSFightforData_t215334547, ___addAttributeValue_7)); }
	inline int32_t get_addAttributeValue_7() const { return ___addAttributeValue_7; }
	inline int32_t* get_address_of_addAttributeValue_7() { return &___addAttributeValue_7; }
	inline void set_addAttributeValue_7(int32_t value)
	{
		___addAttributeValue_7 = value;
	}

	inline static int32_t get_offset_of_ownCountry_8() { return static_cast<int32_t>(offsetof(CSFightforData_t215334547, ___ownCountry_8)); }
	inline int32_t get_ownCountry_8() const { return ___ownCountry_8; }
	inline int32_t* get_address_of_ownCountry_8() { return &___ownCountry_8; }
	inline void set_ownCountry_8(int32_t value)
	{
		___ownCountry_8 = value;
	}

	inline static int32_t get_offset_of_ownAddAttributeKey_9() { return static_cast<int32_t>(offsetof(CSFightforData_t215334547, ___ownAddAttributeKey_9)); }
	inline String_t* get_ownAddAttributeKey_9() const { return ___ownAddAttributeKey_9; }
	inline String_t** get_address_of_ownAddAttributeKey_9() { return &___ownAddAttributeKey_9; }
	inline void set_ownAddAttributeKey_9(String_t* value)
	{
		___ownAddAttributeKey_9 = value;
		Il2CppCodeGenWriteBarrier(&___ownAddAttributeKey_9, value);
	}

	inline static int32_t get_offset_of_ownAddAttributeValue_10() { return static_cast<int32_t>(offsetof(CSFightforData_t215334547, ___ownAddAttributeValue_10)); }
	inline int32_t get_ownAddAttributeValue_10() const { return ___ownAddAttributeValue_10; }
	inline int32_t* get_address_of_ownAddAttributeValue_10() { return &___ownAddAttributeValue_10; }
	inline void set_ownAddAttributeValue_10(int32_t value)
	{
		___ownAddAttributeValue_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
