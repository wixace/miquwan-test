﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Meta.MetaType
struct MetaType_t448283965;
// ProtoBuf.Meta.RuntimeTypeModel
struct RuntimeTypeModel_t242172789;
// System.Type
struct Type_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// ProtoBuf.Serializers.IProtoSerializer
struct IProtoSerializer_t3033312651;
// System.String
struct String_t;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// ProtoBuf.Meta.CallbackSet
struct CallbackSet_t3590429999;
// System.Exception
struct Exception_t3991598821;
// ProtoBuf.Serializers.IProtoTypeSerializer
struct IProtoTypeSerializer_t321624293;
// ProtoBuf.Meta.BasicList
struct BasicList_t528018366;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t2824366364;
// ProtoBuf.Meta.AttributeMap[]
struct AttributeMapU5BU5D_t2350548939;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t4136801618;
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t674955999;
// ProtoBuf.ProtoMemberAttribute
struct ProtoMemberAttribute_t2007578278;
// ProtoBuf.Meta.ValueMember
struct ValueMember_t110398141;
// ProtoBuf.Meta.AttributeMap
struct AttributeMap_t924393598;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Object
struct Il2CppObject;
// ProtoBuf.Meta.ValueMember[]
struct ValueMemberU5BU5D_t3379579312;
// ProtoBuf.Meta.SubType[]
struct SubTypeU5BU5D_t4186375397;
// ProtoBuf.Serializers.EnumSerializer/EnumPair[]
struct EnumPairU5BU5D_t754245438;
// System.Collections.IEnumerable
struct IEnumerable_t3464557803;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// ProtoBuf.Meta.SubType
struct SubType_t3836516844;
// System.Reflection.Assembly
struct Assembly_t1418687608;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Array
struct Il2CppArray;
// System.Type[]
struct TypeU5BU5D_t3339007067;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_RuntimeTypeModel242172789.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "AssemblyU2DCSharp_ProtoBuf_DataFormat274207885.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_MetaType448283965.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_MetaType_Attribute1564241526.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_BasicList528018366.h"
#include "AssemblyU2DCSharp_ProtoBuf_ImplicitFields1595963338.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoMemberAttribute2007578278.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_AttributeMap924393598.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_ValueMember110398141.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_CallbackSet3590429999.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_SubType3836516844.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_BasicList_NodeEnum3394063023.h"
#include "mscorlib_System_Reflection_Assembly1418687608.h"
#include "mscorlib_System_Reflection_PropertyInfo924268725.h"
#include "mscorlib_System_Array1146569071.h"

// System.Void ProtoBuf.Meta.MetaType::.ctor(ProtoBuf.Meta.RuntimeTypeModel,System.Type,System.Reflection.MethodInfo)
extern "C"  void MetaType__ctor_m168829671 (MetaType_t448283965 * __this, RuntimeTypeModel_t242172789 * ___model0, Type_t * ___type1, MethodInfo_t * ___factory2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::.cctor()
extern "C"  void MetaType__cctor_m4273233847 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Serializers.IProtoSerializer ProtoBuf.Meta.MetaType::ProtoBuf.Serializers.ISerializerProxy.get_Serializer()
extern "C"  Il2CppObject * MetaType_ProtoBuf_Serializers_ISerializerProxy_get_Serializer_m942438406 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.Meta.MetaType::ToString()
extern "C"  String_t* MetaType_ToString_m3882771997 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.MetaType::get_BaseType()
extern "C"  MetaType_t448283965 * MetaType_get_BaseType_m4127982938 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.TypeModel ProtoBuf.Meta.MetaType::get_Model()
extern "C"  TypeModel_t2730011105 * MetaType_get_Model_m2121018122 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::get_IncludeSerializerMethod()
extern "C"  bool MetaType_get_IncludeSerializerMethod_m3934356690 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::set_IncludeSerializerMethod(System.Boolean)
extern "C"  void MetaType_set_IncludeSerializerMethod_m1046138633 (MetaType_t448283965 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::get_AsReferenceDefault()
extern "C"  bool MetaType_get_AsReferenceDefault_m3126228307 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::set_AsReferenceDefault(System.Boolean)
extern "C"  void MetaType_set_AsReferenceDefault_m2113980746 (MetaType_t448283965 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::IsValidSubType(System.Type)
extern "C"  bool MetaType_IsValidSubType_m1266459823 (MetaType_t448283965 * __this, Type_t * ___subType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.MetaType::AddSubType(System.Int32,System.Type)
extern "C"  MetaType_t448283965 * MetaType_AddSubType_m4184969379 (MetaType_t448283965 * __this, int32_t ___fieldNumber0, Type_t * ___derivedType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.MetaType::AddSubType(System.Int32,System.Type,ProtoBuf.DataFormat)
extern "C"  MetaType_t448283965 * MetaType_AddSubType_m1290943403 (MetaType_t448283965 * __this, int32_t ___fieldNumber0, Type_t * ___derivedType1, int32_t ___dataFormat2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::SetBaseType(ProtoBuf.Meta.MetaType)
extern "C"  void MetaType_SetBaseType_m594871708 (MetaType_t448283965 * __this, MetaType_t448283965 * ___baseType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::get_HasCallbacks()
extern "C"  bool MetaType_get_HasCallbacks_m3728151647 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::get_HasSubtypes()
extern "C"  bool MetaType_get_HasSubtypes_m1602500842 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.CallbackSet ProtoBuf.Meta.MetaType::get_Callbacks()
extern "C"  CallbackSet_t3590429999 * MetaType_get_Callbacks_m67134333 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::get_IsValueType()
extern "C"  bool MetaType_get_IsValueType_m2110919864 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.MetaType::SetCallbacks(System.Reflection.MethodInfo,System.Reflection.MethodInfo,System.Reflection.MethodInfo,System.Reflection.MethodInfo)
extern "C"  MetaType_t448283965 * MetaType_SetCallbacks_m1644887380 (MetaType_t448283965 * __this, MethodInfo_t * ___beforeSerialize0, MethodInfo_t * ___afterSerialize1, MethodInfo_t * ___beforeDeserialize2, MethodInfo_t * ___afterDeserialize3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.MetaType::SetCallbacks(System.String,System.String,System.String,System.String)
extern "C"  MetaType_t448283965 * MetaType_SetCallbacks_m2708063364 (MetaType_t448283965 * __this, String_t* ___beforeSerialize0, String_t* ___afterSerialize1, String_t* ___beforeDeserialize2, String_t* ___afterDeserialize3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.Meta.MetaType::GetSchemaTypeName()
extern "C"  String_t* MetaType_GetSchemaTypeName_m2078359789 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.Meta.MetaType::get_Name()
extern "C"  String_t* MetaType_get_Name_m226079621 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::set_Name(System.String)
extern "C"  void MetaType_set_Name_m2654245676 (MetaType_t448283965 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.MetaType::SetFactory(System.Reflection.MethodInfo)
extern "C"  MetaType_t448283965 * MetaType_SetFactory_m1885814747 (MetaType_t448283965 * __this, MethodInfo_t * ___factory0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.MetaType::SetFactory(System.String)
extern "C"  MetaType_t448283965 * MetaType_SetFactory_m639655508 (MetaType_t448283965 * __this, String_t* ___factory0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Meta.MetaType::ResolveMethod(System.String,System.Boolean)
extern "C"  MethodInfo_t * MetaType_ResolveMethod_m4197562834 (MetaType_t448283965 * __this, String_t* ___name0, bool ___instance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception ProtoBuf.Meta.MetaType::InbuiltType(System.Type)
extern "C"  Exception_t3991598821 * MetaType_InbuiltType_m2482863455 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::ThrowIfFrozen()
extern "C"  void MetaType_ThrowIfFrozen_m3134467895 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.MetaType::get_Type()
extern "C"  Type_t * MetaType_get_Type_m1250891691 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Serializers.IProtoTypeSerializer ProtoBuf.Meta.MetaType::get_Serializer()
extern "C"  Il2CppObject * MetaType_get_Serializer_m3753406049 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::get_IsList()
extern "C"  bool MetaType_get_IsList_m3817986163 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Serializers.IProtoTypeSerializer ProtoBuf.Meta.MetaType::BuildSerializer()
extern "C"  Il2CppObject * MetaType_BuildSerializer_m624130620 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.MetaType::GetBaseType(ProtoBuf.Meta.MetaType)
extern "C"  Type_t * MetaType_GetBaseType_m2689587510 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::GetAsReferenceDefault(ProtoBuf.Meta.RuntimeTypeModel,System.Type)
extern "C"  bool MetaType_GetAsReferenceDefault_m906697908 (Il2CppObject * __this /* static, unused */, RuntimeTypeModel_t242172789 * ___model0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::ApplyDefaultBehaviour()
extern "C"  void MetaType_ApplyDefaultBehaviour_m3912699486 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::ApplyDefaultBehaviour_AddMembers(ProtoBuf.Meta.TypeModel,ProtoBuf.Meta.MetaType/AttributeFamily,System.Boolean,ProtoBuf.Meta.BasicList,System.Int32,System.Boolean,ProtoBuf.ImplicitFields,ProtoBuf.Meta.BasicList,System.Reflection.MemberInfo,System.Boolean&,System.Boolean,System.Boolean,System.Type&)
extern "C"  void MetaType_ApplyDefaultBehaviour_AddMembers_m244066792 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, int32_t ___family1, bool ___isEnum2, BasicList_t528018366 * ___partialMembers3, int32_t ___dataMemberOffset4, bool ___inferTagByName5, int32_t ___implicitMode6, BasicList_t528018366 * ___members7, MemberInfo_t * ___member8, bool* ___forced9, bool ___isPublic10, bool ___isField11, Type_t ** ___effectiveType12, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Meta.MetaType::Coalesce(System.Reflection.MethodInfo[],System.Int32,System.Int32)
extern "C"  MethodInfo_t * MetaType_Coalesce_m1401570506 (Il2CppObject * __this /* static, unused */, MethodInfoU5BU5D_t2824366364* ___arr0, int32_t ___x1, int32_t ___y2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType/AttributeFamily ProtoBuf.Meta.MetaType::GetContractFamily(ProtoBuf.Meta.RuntimeTypeModel,System.Type,ProtoBuf.Meta.AttributeMap[])
extern "C"  int32_t MetaType_GetContractFamily_m2539703023 (Il2CppObject * __this /* static, unused */, RuntimeTypeModel_t242172789 * ___model0, Type_t * ___type1, AttributeMapU5BU5D_t2350548939* ___attributes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo ProtoBuf.Meta.MetaType::ResolveTupleConstructor(System.Type,System.Reflection.MemberInfo[]&)
extern "C"  ConstructorInfo_t4136801618 * MetaType_ResolveTupleConstructor_m4039459626 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, MemberInfoU5BU5D_t674955999** ___mappedMembers1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::CheckForCallback(System.Reflection.MethodInfo,ProtoBuf.Meta.AttributeMap[],System.String,System.Reflection.MethodInfo[]&,System.Int32)
extern "C"  void MetaType_CheckForCallback_m1327858307 (Il2CppObject * __this /* static, unused */, MethodInfo_t * ___method0, AttributeMapU5BU5D_t2350548939* ___attributes1, String_t* ___callbackTypeName2, MethodInfoU5BU5D_t2824366364** ___callbacks3, int32_t ___index4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::HasFamily(ProtoBuf.Meta.MetaType/AttributeFamily,ProtoBuf.Meta.MetaType/AttributeFamily)
extern "C"  bool MetaType_HasFamily_m3834556232 (Il2CppObject * __this /* static, unused */, int32_t ___value0, int32_t ___required1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.ProtoMemberAttribute ProtoBuf.Meta.MetaType::NormalizeProtoMember(ProtoBuf.Meta.TypeModel,System.Reflection.MemberInfo,ProtoBuf.Meta.MetaType/AttributeFamily,System.Boolean,System.Boolean,ProtoBuf.Meta.BasicList,System.Int32,System.Boolean)
extern "C"  ProtoMemberAttribute_t2007578278 * MetaType_NormalizeProtoMember_m338439346 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, MemberInfo_t * ___member1, int32_t ___family2, bool ___forced3, bool ___isEnum4, BasicList_t528018366 * ___partialMembers5, int32_t ___dataMemberOffset6, bool ___inferByTagName7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.ValueMember ProtoBuf.Meta.MetaType::ApplyDefaultBehaviour(System.Boolean,ProtoBuf.ProtoMemberAttribute)
extern "C"  ValueMember_t110398141 * MetaType_ApplyDefaultBehaviour_m89546678 (MetaType_t448283965 * __this, bool ___isEnum0, ProtoMemberAttribute_t2007578278 * ___normalizedAttribute1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::GetDataFormat(ProtoBuf.DataFormat&,ProtoBuf.Meta.AttributeMap,System.String)
extern "C"  void MetaType_GetDataFormat_m3629099539 (Il2CppObject * __this /* static, unused */, int32_t* ___value0, AttributeMap_t924393598 * ___attrib1, String_t* ___memberName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::GetIgnore(System.Boolean&,ProtoBuf.Meta.AttributeMap,ProtoBuf.Meta.AttributeMap[],System.String)
extern "C"  void MetaType_GetIgnore_m2681644211 (Il2CppObject * __this /* static, unused */, bool* ___ignore0, AttributeMap_t924393598 * ___attrib1, AttributeMapU5BU5D_t2350548939* ___attribs2, String_t* ___fullName3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::GetFieldBoolean(System.Boolean&,ProtoBuf.Meta.AttributeMap,System.String)
extern "C"  void MetaType_GetFieldBoolean_m3035903551 (Il2CppObject * __this /* static, unused */, bool* ___value0, AttributeMap_t924393598 * ___attrib1, String_t* ___memberName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::GetFieldBoolean(System.Boolean&,ProtoBuf.Meta.AttributeMap,System.String,System.Boolean)
extern "C"  bool MetaType_GetFieldBoolean_m1426951698 (Il2CppObject * __this /* static, unused */, bool* ___value0, AttributeMap_t924393598 * ___attrib1, String_t* ___memberName2, bool ___publicOnly3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::GetFieldNumber(System.Int32&,ProtoBuf.Meta.AttributeMap,System.String)
extern "C"  void MetaType_GetFieldNumber_m4032885756 (Il2CppObject * __this /* static, unused */, int32_t* ___value0, AttributeMap_t924393598 * ___attrib1, String_t* ___memberName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::GetFieldName(System.String&,ProtoBuf.Meta.AttributeMap,System.String)
extern "C"  void MetaType_GetFieldName_m2226551573 (Il2CppObject * __this /* static, unused */, String_t** ___name0, AttributeMap_t924393598 * ___attrib1, String_t* ___memberName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.AttributeMap ProtoBuf.Meta.MetaType::GetAttribute(ProtoBuf.Meta.AttributeMap[],System.String)
extern "C"  AttributeMap_t924393598 * MetaType_GetAttribute_m3593524159 (Il2CppObject * __this /* static, unused */, AttributeMapU5BU5D_t2350548939* ___attribs0, String_t* ___fullName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.MetaType::Add(System.Int32,System.String)
extern "C"  MetaType_t448283965 * MetaType_Add_m470385770 (MetaType_t448283965 * __this, int32_t ___fieldNumber0, String_t* ___memberName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.ValueMember ProtoBuf.Meta.MetaType::AddField(System.Int32,System.String)
extern "C"  ValueMember_t110398141 * MetaType_AddField_m868516834 (MetaType_t448283965 * __this, int32_t ___fieldNumber0, String_t* ___memberName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::get_UseConstructor()
extern "C"  bool MetaType_get_UseConstructor_m3648477886 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::set_UseConstructor(System.Boolean)
extern "C"  void MetaType_set_UseConstructor_m279852917 (MetaType_t448283965 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.MetaType::get_ConstructType()
extern "C"  Type_t * MetaType_get_ConstructType_m196560002 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::set_ConstructType(System.Type)
extern "C"  void MetaType_set_ConstructType_m1753047441 (MetaType_t448283965 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.MetaType::Add(System.String)
extern "C"  MetaType_t448283965 * MetaType_Add_m3600816965 (MetaType_t448283965 * __this, String_t* ___memberName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::SetSurrogate(System.Type)
extern "C"  void MetaType_SetSurrogate_m867036949 (MetaType_t448283965 * __this, Type_t * ___surrogateType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.MetaType::GetSurrogateOrSelf()
extern "C"  MetaType_t448283965 * MetaType_GetSurrogateOrSelf_m3658662007 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.MetaType::GetSurrogateOrBaseOrSelf(System.Boolean)
extern "C"  MetaType_t448283965 * MetaType_GetSurrogateOrBaseOrSelf_m607401186 (MetaType_t448283965 * __this, bool ___deep0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.MetaType::GetNextFieldNumber()
extern "C"  int32_t MetaType_GetNextFieldNumber_m1172274518 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.MetaType::Add(System.String[])
extern "C"  MetaType_t448283965 * MetaType_Add_m2936481059 (MetaType_t448283965 * __this, StringU5BU5D_t4054002952* ___memberNames0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.MetaType::Add(System.Int32,System.String,System.Object)
extern "C"  MetaType_t448283965 * MetaType_Add_m1302515128 (MetaType_t448283965 * __this, int32_t ___fieldNumber0, String_t* ___memberName1, Il2CppObject * ___defaultValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.MetaType::Add(System.Int32,System.String,System.Type,System.Type)
extern "C"  MetaType_t448283965 * MetaType_Add_m1373130832 (MetaType_t448283965 * __this, int32_t ___fieldNumber0, String_t* ___memberName1, Type_t * ___itemType2, Type_t * ___defaultType3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.ValueMember ProtoBuf.Meta.MetaType::AddField(System.Int32,System.String,System.Type,System.Type)
extern "C"  ValueMember_t110398141 * MetaType_AddField_m1860855752 (MetaType_t448283965 * __this, int32_t ___fieldNumber0, String_t* ___memberName1, Type_t * ___itemType2, Type_t * ___defaultType3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.ValueMember ProtoBuf.Meta.MetaType::AddField(System.Int32,System.String,System.Type,System.Type,System.Object)
extern "C"  ValueMember_t110398141 * MetaType_AddField_m2789962902 (MetaType_t448283965 * __this, int32_t ___fieldNumber0, String_t* ___memberName1, Type_t * ___itemType2, Type_t * ___defaultType3, Il2CppObject * ___defaultValue4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::ResolveListTypes(ProtoBuf.Meta.TypeModel,System.Type,System.Type&,System.Type&)
extern "C"  void MetaType_ResolveListTypes_m2954654047 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, Type_t * ___type1, Type_t ** ___itemType2, Type_t ** ___defaultType3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::Add(ProtoBuf.Meta.ValueMember)
extern "C"  void MetaType_Add_m3286527096 (MetaType_t448283965 * __this, ValueMember_t110398141 * ___member0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.ValueMember ProtoBuf.Meta.MetaType::get_Item(System.Int32)
extern "C"  ValueMember_t110398141 * MetaType_get_Item_m3577551465 (MetaType_t448283965 * __this, int32_t ___fieldNumber0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.ValueMember ProtoBuf.Meta.MetaType::get_Item(System.Reflection.MemberInfo)
extern "C"  ValueMember_t110398141 * MetaType_get_Item_m2106881132 (MetaType_t448283965 * __this, MemberInfo_t * ___member0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.ValueMember[] ProtoBuf.Meta.MetaType::GetFields()
extern "C"  ValueMemberU5BU5D_t3379579312* MetaType_GetFields_m4240229751 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.SubType[] ProtoBuf.Meta.MetaType::GetSubtypes()
extern "C"  SubTypeU5BU5D_t4186375397* MetaType_GetSubtypes_m2961955942 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::IsDefined(System.Int32)
extern "C"  bool MetaType_IsDefined_m1099832240 (MetaType_t448283965 * __this, int32_t ___fieldNumber0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.MetaType::GetKey(System.Boolean,System.Boolean)
extern "C"  int32_t MetaType_GetKey_m736156609 (MetaType_t448283965 * __this, bool ___demand0, bool ___getBaseKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Serializers.EnumSerializer/EnumPair[] ProtoBuf.Meta.MetaType::GetEnumMap()
extern "C"  EnumPairU5BU5D_t754245438* MetaType_GetEnumMap_m1265998311 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::get_EnumPassthru()
extern "C"  bool MetaType_get_EnumPassthru_m3045346100 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::set_EnumPassthru(System.Boolean)
extern "C"  void MetaType_set_EnumPassthru_m3669627243 (MetaType_t448283965 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::get_IgnoreListHandling()
extern "C"  bool MetaType_get_IgnoreListHandling_m944272352 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::set_IgnoreListHandling(System.Boolean)
extern "C"  void MetaType_set_IgnoreListHandling_m1937231127 (MetaType_t448283965 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::get_Pending()
extern "C"  bool MetaType_get_Pending_m3954488718 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::set_Pending(System.Boolean)
extern "C"  void MetaType_set_Pending_m540837573 (MetaType_t448283965 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::HasFlag(System.Byte)
extern "C"  bool MetaType_HasFlag_m3275442469 (MetaType_t448283965 * __this, uint8_t ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::SetFlag(System.Byte,System.Boolean,System.Boolean)
extern "C"  void MetaType_SetFlag_m2507677769 (MetaType_t448283965 * __this, uint8_t ___flag0, bool ___value1, bool ___throwIfFrozen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.MetaType::GetRootType(ProtoBuf.Meta.MetaType)
extern "C"  MetaType_t448283965 * MetaType_GetRootType_m890218249 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::IsPrepared()
extern "C"  bool MetaType_IsPrepared_m2856241289 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerable ProtoBuf.Meta.MetaType::get_Fields()
extern "C"  Il2CppObject * MetaType_get_Fields_m2465342996 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder ProtoBuf.Meta.MetaType::NewLine(System.Text.StringBuilder,System.Int32)
extern "C"  StringBuilder_t243639308 * MetaType_NewLine_m3778052234 (Il2CppObject * __this /* static, unused */, StringBuilder_t243639308 * ___builder0, int32_t ___indent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::get_IsAutoTuple()
extern "C"  bool MetaType_get_IsAutoTuple_m1143866438 (MetaType_t448283965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::WriteSchema(System.Text.StringBuilder,System.Int32,System.Boolean&)
extern "C"  void MetaType_WriteSchema_m4184237072 (MetaType_t448283965 * __this, StringBuilder_t243639308 * ___builder0, int32_t ___indent1, bool* ___requiresBclImport2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::ilo_HasFlag1(ProtoBuf.Meta.MetaType,System.Byte)
extern "C"  bool MetaType_ilo_HasFlag1_m1141799552 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, uint8_t ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::ilo_SetFlag2(ProtoBuf.Meta.MetaType,System.Byte,System.Boolean,System.Boolean)
extern "C"  void MetaType_ilo_SetFlag2_m3912952357 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, uint8_t ___flag1, bool ___value2, bool ___throwIfFrozen3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.MetaType::ilo_AddSubType3(ProtoBuf.Meta.MetaType,System.Int32,System.Type,ProtoBuf.DataFormat)
extern "C"  MetaType_t448283965 * MetaType_ilo_AddSubType3_m979472500 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, int32_t ___fieldNumber1, Type_t * ___derivedType2, int32_t ___dataFormat3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::ilo_ThrowIfFrozen4(ProtoBuf.Meta.MetaType)
extern "C"  void MetaType_ilo_ThrowIfFrozen4_m1716685517 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::ilo_ThrowIfFrozen5(ProtoBuf.Meta.MetaType)
extern "C"  void MetaType_ilo_ThrowIfFrozen5_m3934443470 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.MetaType::ilo_Add6(ProtoBuf.Meta.BasicList,System.Object)
extern "C"  int32_t MetaType_ilo_Add6_m1755569974 (Il2CppObject * __this /* static, unused */, BasicList_t528018366 * ____this0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::ilo_set_AfterSerialize7(ProtoBuf.Meta.CallbackSet,System.Reflection.MethodInfo)
extern "C"  void MetaType_ilo_set_AfterSerialize7_m2984335791 (Il2CppObject * __this /* static, unused */, CallbackSet_t3590429999 * ____this0, MethodInfo_t * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.CallbackSet ProtoBuf.Meta.MetaType::ilo_get_Callbacks8(ProtoBuf.Meta.MetaType)
extern "C"  CallbackSet_t3590429999 * MetaType_ilo_get_Callbacks8_m313810379 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Meta.MetaType::ilo_ResolveMethod9(ProtoBuf.Meta.MetaType,System.String,System.Boolean)
extern "C"  MethodInfo_t * MetaType_ilo_ResolveMethod9_m2653278479 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, String_t* ___name1, bool ___instance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::ilo_set_BeforeSerialize10(ProtoBuf.Meta.CallbackSet,System.Reflection.MethodInfo)
extern "C"  void MetaType_ilo_set_BeforeSerialize10_m1446860188 (Il2CppObject * __this /* static, unused */, CallbackSet_t3590429999 * ____this0, MethodInfo_t * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::ilo_set_BeforeDeserialize11(ProtoBuf.Meta.CallbackSet,System.Reflection.MethodInfo)
extern "C"  void MetaType_ilo_set_BeforeDeserialize11_m1262454140 (Il2CppObject * __this /* static, unused */, CallbackSet_t3590429999 * ____this0, MethodInfo_t * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.Meta.MetaType::ilo_GetSchemaTypeName12(ProtoBuf.Meta.MetaType)
extern "C"  String_t* MetaType_ilo_GetSchemaTypeName12_m2940436246 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.MetaType::ilo_get_Item13(ProtoBuf.Meta.RuntimeTypeModel,System.Type)
extern "C"  MetaType_t448283965 * MetaType_ilo_get_Item13_m4224864749 (Il2CppObject * __this /* static, unused */, RuntimeTypeModel_t242172789 * ____this0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.MetaType::ilo_GetListItemType14(ProtoBuf.Meta.TypeModel,System.Type)
extern "C"  Type_t * MetaType_ilo_GetListItemType14_m334370769 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, Type_t * ___listType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Serializers.EnumSerializer/EnumPair[] ProtoBuf.Meta.MetaType::ilo_GetEnumMap15(ProtoBuf.Meta.MetaType)
extern "C"  EnumPairU5BU5D_t754245438* MetaType_ilo_GetEnumMap15_m226759001 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::ilo_get_IgnoreListHandling16(ProtoBuf.Meta.MetaType)
extern "C"  bool MetaType_ilo_get_IgnoreListHandling16_m1277651155 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.MetaType::ilo_get_Count17(ProtoBuf.Meta.BasicList)
extern "C"  int32_t MetaType_ilo_get_Count17_m1626449741 (Il2CppObject * __this /* static, unused */, BasicList_t528018366 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::ilo_ResolveListTypes18(ProtoBuf.Meta.TypeModel,System.Type,System.Type&,System.Type&)
extern "C"  void MetaType_ilo_ResolveListTypes18_m583796229 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, Type_t * ___type1, Type_t ** ___itemType2, Type_t ** ___defaultType3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo ProtoBuf.Meta.MetaType::ilo_ResolveTupleConstructor19(System.Type,System.Reflection.MemberInfo[]&)
extern "C"  ConstructorInfo_t4136801618 * MetaType_ilo_ResolveTupleConstructor19_m2481302645 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, MemberInfoU5BU5D_t674955999** ___mappedMembers1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.MetaType::ilo_MapType20(ProtoBuf.Meta.TypeModel,System.Type)
extern "C"  Type_t * MetaType_ilo_MapType20_m3621196897 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.MetaType::ilo_get_DerivedType21(ProtoBuf.Meta.SubType)
extern "C"  MetaType_t448283965 * MetaType_ilo_get_DerivedType21_m1864908040 (Il2CppObject * __this /* static, unused */, SubType_t3836516844 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.BasicList/NodeEnumerator ProtoBuf.Meta.MetaType::ilo_GetEnumerator22(ProtoBuf.Meta.BasicList)
extern "C"  NodeEnumerator_t3394063023  MetaType_ilo_GetEnumerator22_m3291674431 (Il2CppObject * __this /* static, unused */, BasicList_t528018366 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.MetaType::ilo_get_FieldNumber23(ProtoBuf.Meta.ValueMember)
extern "C"  int32_t MetaType_ilo_get_FieldNumber23_m371551263 (Il2CppObject * __this /* static, unused */, ValueMember_t110398141 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.MetaType::ilo_get_BaseType24(ProtoBuf.Meta.MetaType)
extern "C"  MetaType_t448283965 * MetaType_ilo_get_BaseType24_m3268773674 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::ilo_TryGet25(ProtoBuf.Meta.AttributeMap,System.String,System.Object&)
extern "C"  bool MetaType_ilo_TryGet25_m81601557 (Il2CppObject * __this /* static, unused */, AttributeMap_t924393598 * ____this0, String_t* ___key1, Il2CppObject ** ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.MetaType::ilo_GetBaseType26(ProtoBuf.Meta.MetaType)
extern "C"  Type_t * MetaType_ilo_GetBaseType26_m1911879207 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.MetaType::ilo_FindOrAddAuto27(ProtoBuf.Meta.RuntimeTypeModel,System.Type,System.Boolean,System.Boolean,System.Boolean)
extern "C"  int32_t MetaType_ilo_FindOrAddAuto27_m1239861179 (Il2CppObject * __this /* static, unused */, RuntimeTypeModel_t242172789 * ____this0, Type_t * ___type1, bool ___demand2, bool ___addWithContractOnly3, bool ___addEvenIfAutoDisabled4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::ilo_get_EnumPassthru28(ProtoBuf.Meta.MetaType)
extern "C"  bool MetaType_ilo_get_EnumPassthru28_m3455775624 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::ilo_get_InferTagFromNameDefault29(ProtoBuf.Meta.RuntimeTypeModel)
extern "C"  bool MetaType_ilo_get_InferTagFromNameDefault29_m2762918958 (Il2CppObject * __this /* static, unused */, RuntimeTypeModel_t242172789 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.MetaType::ilo_GetType30(ProtoBuf.Meta.TypeModel,System.String,System.Reflection.Assembly)
extern "C"  Type_t * MetaType_ilo_GetType30_m2798699915 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, String_t* ___fullName1, Assembly_t1418687608 * ___context2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::ilo_IsEnum31(System.Type)
extern "C"  bool MetaType_ilo_IsEnum31_m3213165851 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::ilo_set_EnumPassthru32(ProtoBuf.Meta.MetaType,System.Boolean)
extern "C"  void MetaType_ilo_set_EnumPassthru32_m2478450428 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::ilo_set_UseConstructor33(ProtoBuf.Meta.MetaType,System.Boolean)
extern "C"  void MetaType_ilo_set_UseConstructor33_m1245428177 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::ilo_set_AsReferenceDefault34(ProtoBuf.Meta.MetaType,System.Boolean)
extern "C"  void MetaType_ilo_set_AsReferenceDefault34_m3285476347 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Meta.MetaType::ilo_GetGetMethod35(System.Reflection.PropertyInfo,System.Boolean,System.Boolean)
extern "C"  MethodInfo_t * MetaType_ilo_GetGetMethod35_m3296102087 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___property0, bool ___nonPublic1, bool ___allowInternal2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::ilo_ApplyDefaultBehaviour_AddMembers36(ProtoBuf.Meta.TypeModel,ProtoBuf.Meta.MetaType/AttributeFamily,System.Boolean,ProtoBuf.Meta.BasicList,System.Int32,System.Boolean,ProtoBuf.ImplicitFields,ProtoBuf.Meta.BasicList,System.Reflection.MemberInfo,System.Boolean&,System.Boolean,System.Boolean,System.Type&)
extern "C"  void MetaType_ilo_ApplyDefaultBehaviour_AddMembers36_m1734595922 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, int32_t ___family1, bool ___isEnum2, BasicList_t528018366 * ___partialMembers3, int32_t ___dataMemberOffset4, bool ___inferTagByName5, int32_t ___implicitMode6, BasicList_t528018366 * ___members7, MemberInfo_t * ___member8, bool* ___forced9, bool ___isPublic10, bool ___isField11, Type_t ** ___effectiveType12, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::ilo_CheckForCallback37(System.Reflection.MethodInfo,ProtoBuf.Meta.AttributeMap[],System.String,System.Reflection.MethodInfo[]&,System.Int32)
extern "C"  void MetaType_ilo_CheckForCallback37_m3844159962 (Il2CppObject * __this /* static, unused */, MethodInfo_t * ___method0, AttributeMapU5BU5D_t2350548939* ___attributes1, String_t* ___callbackTypeName2, MethodInfoU5BU5D_t2824366364** ___callbacks3, int32_t ___index4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::ilo_CopyTo38(ProtoBuf.Meta.BasicList,System.Array,System.Int32)
extern "C"  void MetaType_ilo_CopyTo38_m1494229921 (Il2CppObject * __this /* static, unused */, BasicList_t528018366 * ____this0, Il2CppArray * ___array1, int32_t ___offset2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.ProtoMemberAttribute ProtoBuf.Meta.MetaType::ilo_NormalizeProtoMember39(ProtoBuf.Meta.TypeModel,System.Reflection.MemberInfo,ProtoBuf.Meta.MetaType/AttributeFamily,System.Boolean,System.Boolean,ProtoBuf.Meta.BasicList,System.Int32,System.Boolean)
extern "C"  ProtoMemberAttribute_t2007578278 * MetaType_ilo_NormalizeProtoMember39_m1724569273 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, MemberInfo_t * ___member1, int32_t ___family2, bool ___forced3, bool ___isEnum4, BasicList_t528018366 * ___partialMembers5, int32_t ___dataMemberOffset6, bool ___inferByTagName7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.AttributeMap[] ProtoBuf.Meta.MetaType::ilo_Create40(ProtoBuf.Meta.TypeModel,System.Type,System.Boolean)
extern "C"  AttributeMapU5BU5D_t2350548939* MetaType_ilo_Create40_m3326094319 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, Type_t * ___type1, bool ___inherit2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::ilo_get_AutoAddProtoContractTypesOnly41(ProtoBuf.Meta.RuntimeTypeModel)
extern "C"  bool MetaType_ilo_get_AutoAddProtoContractTypesOnly41_m3688652449 (Il2CppObject * __this /* static, unused */, RuntimeTypeModel_t242172789 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Meta.MetaType::ilo_GetSetMethod42(System.Reflection.PropertyInfo,System.Boolean,System.Boolean)
extern "C"  MethodInfo_t * MetaType_ilo_GetSetMethod42_m3166163439 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___property0, bool ___nonPublic1, bool ___allowInternal2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.MetaType::ilo_get_AttributeType43(ProtoBuf.Meta.AttributeMap)
extern "C"  Type_t * MetaType_ilo_get_AttributeType43_m4265071981 (Il2CppObject * __this /* static, unused */, AttributeMap_t924393598 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.AttributeMap[] ProtoBuf.Meta.MetaType::ilo_Create44(ProtoBuf.Meta.TypeModel,System.Reflection.MemberInfo,System.Boolean)
extern "C"  AttributeMapU5BU5D_t2350548939* MetaType_ilo_Create44_m1183416954 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, MemberInfo_t * ___member1, bool ___inherit2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.AttributeMap ProtoBuf.Meta.MetaType::ilo_GetAttribute45(ProtoBuf.Meta.AttributeMap[],System.String)
extern "C"  AttributeMap_t924393598 * MetaType_ilo_GetAttribute45_m2376016051 (Il2CppObject * __this /* static, unused */, AttributeMapU5BU5D_t2350548939* ___attribs0, String_t* ___fullName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::ilo_GetFieldName46(System.String&,ProtoBuf.Meta.AttributeMap,System.String)
extern "C"  void MetaType_ilo_GetFieldName46_m2888407744 (Il2CppObject * __this /* static, unused */, String_t** ___name0, AttributeMap_t924393598 * ___attrib1, String_t* ___memberName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::ilo_GetFieldBoolean47(System.Boolean&,ProtoBuf.Meta.AttributeMap,System.String)
extern "C"  void MetaType_ilo_GetFieldBoolean47_m1863091119 (Il2CppObject * __this /* static, unused */, bool* ___value0, AttributeMap_t924393598 * ___attrib1, String_t* ___memberName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::ilo_MoveNext48(ProtoBuf.Meta.BasicList/NodeEnumerator&)
extern "C"  bool MetaType_ilo_MoveNext48_m2995637118 (Il2CppObject * __this /* static, unused */, NodeEnumerator_t3394063023 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::ilo_set_AsReference49(ProtoBuf.ProtoMemberAttribute,System.Boolean)
extern "C"  void MetaType_ilo_set_AsReference49_m2729579562 (Il2CppObject * __this /* static, unused */, ProtoMemberAttribute_t2007578278 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::ilo_set_DataFormat50(ProtoBuf.ProtoMemberAttribute,ProtoBuf.DataFormat)
extern "C"  void MetaType_ilo_set_DataFormat50_m2149726595 (Il2CppObject * __this /* static, unused */, ProtoMemberAttribute_t2007578278 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::ilo_get_UseImplicitZeroDefaults51(ProtoBuf.Meta.RuntimeTypeModel)
extern "C"  bool MetaType_ilo_get_UseImplicitZeroDefaults51_m198575529 (Il2CppObject * __this /* static, unused */, RuntimeTypeModel_t242172789 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.MetaType::ilo_get_Tag52(ProtoBuf.ProtoMemberAttribute)
extern "C"  int32_t MetaType_ilo_get_Tag52_m1781502260 (Il2CppObject * __this /* static, unused */, ProtoMemberAttribute_t2007578278 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.DataFormat ProtoBuf.Meta.MetaType::ilo_get_DataFormat53(ProtoBuf.ProtoMemberAttribute)
extern "C"  int32_t MetaType_ilo_get_DataFormat53_m756638013 (Il2CppObject * __this /* static, unused */, ProtoMemberAttribute_t2007578278 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Meta.MetaType::ilo_GetInstanceMethod54(System.Type,System.String,System.Type[])
extern "C"  MethodInfo_t * MetaType_ilo_GetInstanceMethod54_m3031436036 (Il2CppObject * __this /* static, unused */, Type_t * ___declaringType0, String_t* ___name1, TypeU5BU5D_t3339007067* ___types2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::ilo_get_AsReferenceHasValue55(ProtoBuf.ProtoMemberAttribute)
extern "C"  bool MetaType_ilo_get_AsReferenceHasValue55_m1142503809 (Il2CppObject * __this /* static, unused */, ProtoMemberAttribute_t2007578278 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::ilo_set_DynamicType56(ProtoBuf.Meta.ValueMember,System.Boolean)
extern "C"  void MetaType_ilo_set_DynamicType56_m3175216878 (Il2CppObject * __this /* static, unused */, ValueMember_t110398141 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::ilo_TryGet57(ProtoBuf.Meta.AttributeMap,System.String,System.Boolean,System.Object&)
extern "C"  bool MetaType_ilo_TryGet57_m3642891465 (Il2CppObject * __this /* static, unused */, AttributeMap_t924393598 * ____this0, String_t* ___key1, bool ___publicOnly2, Il2CppObject ** ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.MetaType::ilo_get_FieldNumber58(ProtoBuf.Meta.SubType)
extern "C"  int32_t MetaType_ilo_get_FieldNumber58_m581458350 (Il2CppObject * __this /* static, unused */, SubType_t3836516844 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.ValueMember ProtoBuf.Meta.MetaType::ilo_AddField59(ProtoBuf.Meta.MetaType,System.Int32,System.String,System.Type,System.Type,System.Object)
extern "C"  ValueMember_t110398141 * MetaType_ilo_AddField59_m3704841120 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, int32_t ___fieldNumber1, String_t* ___memberName2, Type_t * ___itemType3, Type_t * ___defaultType4, Il2CppObject * ___defaultValue5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::ilo_Add60(ProtoBuf.Meta.MetaType,ProtoBuf.Meta.ValueMember)
extern "C"  void MetaType_ilo_Add60_m3743971646 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, ValueMember_t110398141 * ___member1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception ProtoBuf.Meta.MetaType::ilo_CreateNestedListsNotSupported61()
extern "C"  Exception_t3991598821 * MetaType_ilo_CreateNestedListsNotSupported61_m3636417900 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::ilo_IsAssignableFrom62(System.Type,System.Type)
extern "C"  bool MetaType_ilo_IsAssignableFrom62_m521560670 (Il2CppObject * __this /* static, unused */, Type_t * ___target0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::ilo_TakeLock63(ProtoBuf.Meta.RuntimeTypeModel,System.Int32&)
extern "C"  void MetaType_ilo_TakeLock63_m2488228424 (Il2CppObject * __this /* static, unused */, RuntimeTypeModel_t242172789 * ____this0, int32_t* ___opaqueToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType::ilo_ReleaseLock64(ProtoBuf.Meta.RuntimeTypeModel,System.Int32)
extern "C"  void MetaType_ilo_ReleaseLock64_m2135109795 (Il2CppObject * __this /* static, unused */, RuntimeTypeModel_t242172789 * ____this0, int32_t ___opaqueToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.MetaType::ilo_get_Current65(ProtoBuf.Meta.BasicList/NodeEnumerator&)
extern "C"  Il2CppObject * MetaType_ilo_get_Current65_m2683634208 (Il2CppObject * __this /* static, unused */, NodeEnumerator_t3394063023 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.MetaType::ilo_get_MemberType66(ProtoBuf.Meta.ValueMember)
extern "C"  Type_t * MetaType_ilo_get_MemberType66_m1607353909 (Il2CppObject * __this /* static, unused */, ValueMember_t110398141 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder ProtoBuf.Meta.MetaType::ilo_NewLine67(System.Text.StringBuilder,System.Int32)
extern "C"  StringBuilder_t243639308 * MetaType_ilo_NewLine67_m3893956984 (Il2CppObject * __this /* static, unused */, StringBuilder_t243639308 * ___builder0, int32_t ___indent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.Meta.MetaType::ilo_GetSchemaTypeName68(ProtoBuf.Meta.MetaType)
extern "C"  String_t* MetaType_ilo_GetSchemaTypeName68_m3517181111 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.MetaType::ilo_get_ItemType69(ProtoBuf.Meta.ValueMember)
extern "C"  Type_t * MetaType_ilo_get_ItemType69_m1076353593 (Il2CppObject * __this /* static, unused */, ValueMember_t110398141 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.DataFormat ProtoBuf.Meta.MetaType::ilo_get_DataFormat70(ProtoBuf.Meta.ValueMember)
extern "C"  int32_t MetaType_ilo_get_DataFormat70_m1159167322 (Il2CppObject * __this /* static, unused */, ValueMember_t110398141 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.MetaType::ilo_get_DefaultValue71(ProtoBuf.Meta.ValueMember)
extern "C"  Il2CppObject * MetaType_ilo_get_DefaultValue71_m2502597156 (Il2CppObject * __this /* static, unused */, ValueMember_t110398141 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.MetaType::ilo_get_IsPacked72(ProtoBuf.Meta.ValueMember)
extern "C"  bool MetaType_ilo_get_IsPacked72_m3461000850 (Il2CppObject * __this /* static, unused */, ValueMember_t110398141 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
