﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.iOS.LocalNotification
struct LocalNotification_t1344855248;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_iOS_LocalNotification1344855248.h"
#include "UnityEngine_UnityEngine_iOS_NotificationType3233079183.h"

// System.Void UnityEngine.iOS.NotificationServices::ScheduleLocalNotification(UnityEngine.iOS.LocalNotification)
extern "C"  void NotificationServices_ScheduleLocalNotification_m3628575988 (Il2CppObject * __this /* static, unused */, LocalNotification_t1344855248 * ___notification0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.NotificationServices::PresentLocalNotificationNow(UnityEngine.iOS.LocalNotification)
extern "C"  void NotificationServices_PresentLocalNotificationNow_m2681731384 (Il2CppObject * __this /* static, unused */, LocalNotification_t1344855248 * ___notification0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.NotificationServices::CancelAllLocalNotifications()
extern "C"  void NotificationServices_CancelAllLocalNotifications_m169357189 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.NotificationServices::ClearLocalNotifications()
extern "C"  void NotificationServices_ClearLocalNotifications_m694087179 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.NotificationServices::RegisterForNotifications(UnityEngine.iOS.NotificationType)
extern "C"  void NotificationServices_RegisterForNotifications_m460427814 (Il2CppObject * __this /* static, unused */, int32_t ___notificationTypes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.NotificationServices::RegisterForNotifications(UnityEngine.iOS.NotificationType,System.Boolean)
extern "C"  void NotificationServices_RegisterForNotifications_m736063639 (Il2CppObject * __this /* static, unused */, int32_t ___notificationTypes0, bool ___registerForRemote1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
