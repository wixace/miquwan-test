﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>
struct Dictionary_2_t1311121246;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3537921950.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Entity.Behavior.EBehaviorID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2145503844_gshared (Enumerator_t3537921950 * __this, Dictionary_2_t1311121246 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2145503844(__this, ___host0, method) ((  void (*) (Enumerator_t3537921950 *, Dictionary_2_t1311121246 *, const MethodInfo*))Enumerator__ctor_m2145503844_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3948261245_gshared (Enumerator_t3537921950 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3948261245(__this, method) ((  Il2CppObject * (*) (Enumerator_t3537921950 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3948261245_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3033894865_gshared (Enumerator_t3537921950 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3033894865(__this, method) ((  void (*) (Enumerator_t3537921950 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3033894865_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Entity.Behavior.EBehaviorID,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1157280262_gshared (Enumerator_t3537921950 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1157280262(__this, method) ((  void (*) (Enumerator_t3537921950 *, const MethodInfo*))Enumerator_Dispose_m1157280262_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Entity.Behavior.EBehaviorID,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2792384445_gshared (Enumerator_t3537921950 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2792384445(__this, method) ((  bool (*) (Enumerator_t3537921950 *, const MethodInfo*))Enumerator_MoveNext_m2792384445_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Entity.Behavior.EBehaviorID,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2378358949_gshared (Enumerator_t3537921950 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2378358949(__this, method) ((  Il2CppObject * (*) (Enumerator_t3537921950 *, const MethodInfo*))Enumerator_get_Current_m2378358949_gshared)(__this, method)
