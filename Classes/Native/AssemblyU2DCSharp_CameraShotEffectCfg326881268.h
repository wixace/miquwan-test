﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraShotEffectCfg
struct  CameraShotEffectCfg_t326881268  : public Il2CppObject
{
public:
	// System.Int32 CameraShotEffectCfg::_id
	int32_t ____id_0;
	// System.Int32 CameraShotEffectCfg::_heroOrNpcId
	int32_t ____heroOrNpcId_1;
	// System.Int32 CameraShotEffectCfg::_isHero
	int32_t ____isHero_2;
	// System.Single CameraShotEffectCfg::_lifeTime
	float ____lifeTime_3;
	// System.Single CameraShotEffectCfg::_posX
	float ____posX_4;
	// System.Single CameraShotEffectCfg::_posY
	float ____posY_5;
	// System.Single CameraShotEffectCfg::_posZ
	float ____posZ_6;
	// System.Int32 CameraShotEffectCfg::_hostId
	int32_t ____hostId_7;
	// System.Int32 CameraShotEffectCfg::_isShowInBlack
	int32_t ____isShowInBlack_8;
	// System.Single CameraShotEffectCfg::_playSpeed
	float ____playSpeed_9;
	// ProtoBuf.IExtension CameraShotEffectCfg::extensionObject
	Il2CppObject * ___extensionObject_10;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(CameraShotEffectCfg_t326881268, ____id_0)); }
	inline int32_t get__id_0() const { return ____id_0; }
	inline int32_t* get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(int32_t value)
	{
		____id_0 = value;
	}

	inline static int32_t get_offset_of__heroOrNpcId_1() { return static_cast<int32_t>(offsetof(CameraShotEffectCfg_t326881268, ____heroOrNpcId_1)); }
	inline int32_t get__heroOrNpcId_1() const { return ____heroOrNpcId_1; }
	inline int32_t* get_address_of__heroOrNpcId_1() { return &____heroOrNpcId_1; }
	inline void set__heroOrNpcId_1(int32_t value)
	{
		____heroOrNpcId_1 = value;
	}

	inline static int32_t get_offset_of__isHero_2() { return static_cast<int32_t>(offsetof(CameraShotEffectCfg_t326881268, ____isHero_2)); }
	inline int32_t get__isHero_2() const { return ____isHero_2; }
	inline int32_t* get_address_of__isHero_2() { return &____isHero_2; }
	inline void set__isHero_2(int32_t value)
	{
		____isHero_2 = value;
	}

	inline static int32_t get_offset_of__lifeTime_3() { return static_cast<int32_t>(offsetof(CameraShotEffectCfg_t326881268, ____lifeTime_3)); }
	inline float get__lifeTime_3() const { return ____lifeTime_3; }
	inline float* get_address_of__lifeTime_3() { return &____lifeTime_3; }
	inline void set__lifeTime_3(float value)
	{
		____lifeTime_3 = value;
	}

	inline static int32_t get_offset_of__posX_4() { return static_cast<int32_t>(offsetof(CameraShotEffectCfg_t326881268, ____posX_4)); }
	inline float get__posX_4() const { return ____posX_4; }
	inline float* get_address_of__posX_4() { return &____posX_4; }
	inline void set__posX_4(float value)
	{
		____posX_4 = value;
	}

	inline static int32_t get_offset_of__posY_5() { return static_cast<int32_t>(offsetof(CameraShotEffectCfg_t326881268, ____posY_5)); }
	inline float get__posY_5() const { return ____posY_5; }
	inline float* get_address_of__posY_5() { return &____posY_5; }
	inline void set__posY_5(float value)
	{
		____posY_5 = value;
	}

	inline static int32_t get_offset_of__posZ_6() { return static_cast<int32_t>(offsetof(CameraShotEffectCfg_t326881268, ____posZ_6)); }
	inline float get__posZ_6() const { return ____posZ_6; }
	inline float* get_address_of__posZ_6() { return &____posZ_6; }
	inline void set__posZ_6(float value)
	{
		____posZ_6 = value;
	}

	inline static int32_t get_offset_of__hostId_7() { return static_cast<int32_t>(offsetof(CameraShotEffectCfg_t326881268, ____hostId_7)); }
	inline int32_t get__hostId_7() const { return ____hostId_7; }
	inline int32_t* get_address_of__hostId_7() { return &____hostId_7; }
	inline void set__hostId_7(int32_t value)
	{
		____hostId_7 = value;
	}

	inline static int32_t get_offset_of__isShowInBlack_8() { return static_cast<int32_t>(offsetof(CameraShotEffectCfg_t326881268, ____isShowInBlack_8)); }
	inline int32_t get__isShowInBlack_8() const { return ____isShowInBlack_8; }
	inline int32_t* get_address_of__isShowInBlack_8() { return &____isShowInBlack_8; }
	inline void set__isShowInBlack_8(int32_t value)
	{
		____isShowInBlack_8 = value;
	}

	inline static int32_t get_offset_of__playSpeed_9() { return static_cast<int32_t>(offsetof(CameraShotEffectCfg_t326881268, ____playSpeed_9)); }
	inline float get__playSpeed_9() const { return ____playSpeed_9; }
	inline float* get_address_of__playSpeed_9() { return &____playSpeed_9; }
	inline void set__playSpeed_9(float value)
	{
		____playSpeed_9 = value;
	}

	inline static int32_t get_offset_of_extensionObject_10() { return static_cast<int32_t>(offsetof(CameraShotEffectCfg_t326881268, ___extensionObject_10)); }
	inline Il2CppObject * get_extensionObject_10() const { return ___extensionObject_10; }
	inline Il2CppObject ** get_address_of_extensionObject_10() { return &___extensionObject_10; }
	inline void set_extensionObject_10(Il2CppObject * value)
	{
		___extensionObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___extensionObject_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
