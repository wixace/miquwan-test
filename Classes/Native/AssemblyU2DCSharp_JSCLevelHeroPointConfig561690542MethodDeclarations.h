﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSCLevelHeroPointConfig
struct JSCLevelHeroPointConfig_t561690542;
// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "codegen/il2cpp-codegen.h"

// System.Void JSCLevelHeroPointConfig::.ctor()
extern "C"  void JSCLevelHeroPointConfig__ctor_m664562557 (JSCLevelHeroPointConfig_t561690542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension JSCLevelHeroPointConfig::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * JSCLevelHeroPointConfig_ProtoBuf_IExtensible_GetExtensionObject_m3162856705 (JSCLevelHeroPointConfig_t561690542 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JSCLevelHeroPointConfig::get_x()
extern "C"  float JSCLevelHeroPointConfig_get_x_m3583183766 (JSCLevelHeroPointConfig_t561690542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCLevelHeroPointConfig::set_x(System.Single)
extern "C"  void JSCLevelHeroPointConfig_set_x_m513329045 (JSCLevelHeroPointConfig_t561690542 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JSCLevelHeroPointConfig::get_y()
extern "C"  float JSCLevelHeroPointConfig_get_y_m3583184727 (JSCLevelHeroPointConfig_t561690542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCLevelHeroPointConfig::set_y(System.Single)
extern "C"  void JSCLevelHeroPointConfig_set_y_m2794868 (JSCLevelHeroPointConfig_t561690542 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JSCLevelHeroPointConfig::get_z()
extern "C"  float JSCLevelHeroPointConfig_get_z_m3583185688 (JSCLevelHeroPointConfig_t561690542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCLevelHeroPointConfig::set_z(System.Single)
extern "C"  void JSCLevelHeroPointConfig_set_z_m3787227987 (JSCLevelHeroPointConfig_t561690542 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JSCLevelHeroPointConfig::get_rot()
extern "C"  float JSCLevelHeroPointConfig_get_rot_m3167442421 (JSCLevelHeroPointConfig_t561690542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCLevelHeroPointConfig::set_rot(System.Single)
extern "C"  void JSCLevelHeroPointConfig_set_rot_m947527574 (JSCLevelHeroPointConfig_t561690542 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
