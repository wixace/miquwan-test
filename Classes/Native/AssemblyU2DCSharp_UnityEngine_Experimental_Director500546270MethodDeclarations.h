﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Experimental_Director_DirectorPlayerGenerated
struct UnityEngine_Experimental_Director_DirectorPlayerGenerated_t500546270;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_Experimental_Director_DirectorPlayerGenerated::.ctor()
extern "C"  void UnityEngine_Experimental_Director_DirectorPlayerGenerated__ctor_m129123405 (UnityEngine_Experimental_Director_DirectorPlayerGenerated_t500546270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_DirectorPlayerGenerated::DirectorPlayer_DirectorPlayer1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_DirectorPlayerGenerated_DirectorPlayer_DirectorPlayer1_m1958010789 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_DirectorPlayerGenerated::DirectorPlayer_GetTime(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_DirectorPlayerGenerated_DirectorPlayer_GetTime_m1959321956 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_DirectorPlayerGenerated::DirectorPlayer_GetTimeUpdateMode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_DirectorPlayerGenerated_DirectorPlayer_GetTimeUpdateMode_m666493648 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_DirectorPlayerGenerated::DirectorPlayer_Play__Playable__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_DirectorPlayerGenerated_DirectorPlayer_Play__Playable__Object_m1462540546 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_DirectorPlayerGenerated::DirectorPlayer_Play__Playable(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_DirectorPlayerGenerated_DirectorPlayer_Play__Playable_m2993750275 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_DirectorPlayerGenerated::DirectorPlayer_SetTime__Double(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_DirectorPlayerGenerated_DirectorPlayer_SetTime__Double_m1753133441 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_DirectorPlayerGenerated::DirectorPlayer_SetTimeUpdateMode__DirectorUpdateMode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_DirectorPlayerGenerated_DirectorPlayer_SetTimeUpdateMode__DirectorUpdateMode_m1912530708 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Experimental_Director_DirectorPlayerGenerated::DirectorPlayer_Stop(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Experimental_Director_DirectorPlayerGenerated_DirectorPlayer_Stop_m89793603 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Experimental_Director_DirectorPlayerGenerated::__Register()
extern "C"  void UnityEngine_Experimental_Director_DirectorPlayerGenerated___Register_m3927892122 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Experimental_Director_DirectorPlayerGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_Experimental_Director_DirectorPlayerGenerated_ilo_getObject1_m1692967305 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Experimental_Director_DirectorPlayerGenerated::ilo_setDouble2(System.Int32,System.Double)
extern "C"  void UnityEngine_Experimental_Director_DirectorPlayerGenerated_ilo_setDouble2_m404908086 (Il2CppObject * __this /* static, unused */, int32_t ___e0, double ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Experimental_Director_DirectorPlayerGenerated::ilo_getEnum3(System.Int32)
extern "C"  int32_t UnityEngine_Experimental_Director_DirectorPlayerGenerated_ilo_getEnum3_m3543801161 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
