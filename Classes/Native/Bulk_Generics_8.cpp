﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t2021033010;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t666883479;
// System.Collections.Generic.IDictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct IDictionary_2_t1598906355;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t770439062;
// System.Collections.Generic.ICollection`1<UnityEngine.TextEditor/TextEditOp>
struct ICollection_1_t745583801;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t3614328797;
// System.Array
struct Il2CppArray;
// System.Collections.DictionaryEntry[]
struct DictionaryEntryU5BU5D_t479206547;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>
struct Transform_1_t1958177301;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>
struct Transform_1_t2126384403;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>
struct IEnumerator_1_t3831678765;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>
struct KeyCollection_t3647792461;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.TextEditor/TextEditOp>
struct ValueCollection_t721638723;
// System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>
struct Dictionary_2_t1738881263;
// System.Collections.Generic.IEqualityComparer`1<System.UInt16>
struct IEqualityComparer_1_t815702327;
// System.Collections.Generic.IDictionary`2<System.UInt16,System.Object>
struct IDictionary_2_t1316754608;
// System.Collections.Generic.ICollection`1<System.UInt16>
struct ICollection_1_t919257910;
// System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>[]
struct KeyValuePair_2U5BU5D_t794066828;
// System.Collections.Generic.Dictionary`2/Transform`1<System.UInt16,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t3432882628;
// System.Collections.Generic.Dictionary`2/Transform`1<System.UInt16,System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>>
struct Transform_1_t3318937983;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>>
struct IEnumerator_1_t3549527018;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.Object>
struct KeyCollection_t3365640714;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt16,System.Object>
struct ValueCollection_t439486976;
// System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>
struct Dictionary_2_t4167966349;
// System.Collections.Generic.IEqualityComparer`1<System.UInt32>
struct IEqualityComparer_1_t815702385;
// System.Collections.Generic.IDictionary`2<System.UInt32,System.Object>
struct IDictionary_2_t3745839694;
// System.Collections.Generic.ICollection`1<System.UInt32>
struct ICollection_1_t919257968;
// System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>[]
struct KeyValuePair_2U5BU5D_t2966088118;
// System.Collections.Generic.Dictionary`2/Transform`1<System.UInt32,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t1309936622;
// System.Collections.Generic.Dictionary`2/Transform`1<System.UInt32,System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>
struct Transform_1_t3625077063;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>
struct IEnumerator_1_t1683644808;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Object>
struct KeyCollection_t1499758504;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,System.Object>
struct ValueCollection_t2868572062;
// System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>
struct Dictionary_2_t2952530300;
// System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>
struct IEqualityComparer_1_t2682787083;
// System.Collections.Generic.IDictionary`2<UIModelDisplayType,System.Int32>
struct IDictionary_2_t2530403645;
// System.Collections.Generic.ICollection`1<UIModelDisplayType>
struct ICollection_1_t2786342666;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t2048428487;
// System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>[]
struct KeyValuePair_2U5BU5D_t390341163;
// System.Collections.Generic.Dictionary`2/Transform`1<UIModelDisplayType,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t3029156963;
// System.Collections.Generic.Dictionary`2/Transform`1<UIModelDisplayType,System.Int32,System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>>
struct Transform_1_t4128861355;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>>
struct IEnumerator_1_t468208759;
// System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Int32>
struct KeyCollection_t284322455;
// System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Int32>
struct ValueCollection_t1653136013;
// System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>
struct Dictionary_2_t1674540875;
// System.Collections.Generic.IDictionary`2<UIModelDisplayType,System.Object>
struct IDictionary_2_t1252414220;
// System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>[]
struct KeyValuePair_2U5BU5D_t2563117120;
// System.Collections.Generic.Dictionary`2/Transform`1<UIModelDisplayType,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t906965624;
// System.Collections.Generic.Dictionary`2/Transform`1<UIModelDisplayType,System.Object,System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>>
struct Transform_1_t728680591;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>>
struct IEnumerator_1_t3485186630;
// System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Object>
struct KeyCollection_t3301300326;
// System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Object>
struct ValueCollection_t375146588;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<AIEnum.EAIEventtype>
struct DefaultComparer_t2344065978;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<AnimationRunner/AniType>
struct DefaultComparer_t3748982614;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Core.RpsChoice>
struct DefaultComparer_t2795541095;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Core.RpsResult>
struct DefaultComparer_t3222338787;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Entity.Behavior.EBehaviorID>
struct DefaultComparer_t3981277099;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<FLOAT_TEXT_ID>
struct DefaultComparer_t647122437;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<HatredCtrl/stValue>
struct DefaultComparer_t1873722077;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Mihua.Assets.SubAssetMgr/ErrorInfo>
struct DefaultComparer_t1081757877;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<MScrollView/MoveWay>
struct DefaultComparer_t1922718217;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Linq.JTokenType>
struct DefaultComparer_t2364674228;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Schema.JsonSchemaType>
struct DefaultComparer_t563192488;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>
struct DefaultComparer_t1419621458;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>
struct DefaultComparer_t1340106157;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>
struct DefaultComparer_t3608878137;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.AdvancedSmooth/Turn>
struct DefaultComparer_t3207939341;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.ClipperLib.IntPoint>
struct DefaultComparer_t1773902846;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Int2>
struct DefaultComparer_t421822260;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Int3>
struct DefaultComparer_t421822261;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.IntRect>
struct DefaultComparer_t1462834928;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.LocalAvoidance/IntersectionPair>
struct DefaultComparer_t1269096586;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.LocalAvoidance/VOLine>
struct DefaultComparer_t477708468;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Voxels.ExtraMesh>
struct DefaultComparer_t2665806382;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Voxels.VoxelContour>
struct DefaultComparer_t2044977683;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<PointCloudRegognizer/Point>
struct DefaultComparer_t286608417;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<ProductsCfgMgr/ProductInfo>
struct DefaultComparer_t4048735201;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<PushType>
struct DefaultComparer_t288419119;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<SoundStatus>
struct DefaultComparer_t2040715612;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<SoundTypeID>
struct DefaultComparer_t2074393407;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Boolean>
struct DefaultComparer_t3219542681;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Byte>
struct DefaultComparer_t1310386327;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct DefaultComparer_t392445644;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>
struct DefaultComparer_t2735708096;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>
struct DefaultComparer_t2731437994;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>
struct DefaultComparer_t2332490973;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>
struct DefaultComparer_t1310531096;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Int32>
struct DefaultComparer_t3896582463;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Object>
struct DefaultComparer_t2618593038;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeNamedArgument>
struct DefaultComparer_t1507389656;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>
struct DefaultComparer_t1749070089;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Single>
struct DefaultComparer_t2739695639;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>
struct DefaultComparer_t3156266950;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.UInt16>
struct DefaultComparer_t2767411886;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.UInt32>
struct DefaultComparer_t2767411944;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<TypewriterEffect/FadeEntry>
struct DefaultComparer_t1306248768;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UICamera/DepthEntry>
struct DefaultComparer_t3888358432;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UIModelDisplayType>
struct DefaultComparer_t339529346;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Bounds>
struct DefaultComparer_t1159418516;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color>
struct DefaultComparer_t2642323572;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color32>
struct DefaultComparer_t3341597651;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>
struct DefaultComparer_t2210438031;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.MeshSubsetCombineUtility/MeshInstance>
struct DefaultComparer_t2569742905;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>
struct DefaultComparer_t822616353;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.RaycastHit>
struct DefaultComparer_t2450952393;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Rendering.ReflectionProbeBlendInfo>
struct DefaultComparer_t3823341701;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.TextEditor/TextEditOp>
struct DefaultComparer_t2593737777;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>
struct DefaultComparer_t2808551447;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>
struct DefaultComparer_t2561652149;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>
struct DefaultComparer_t2691841879;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector2>
struct DefaultComparer_t2729843232;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector3>
struct DefaultComparer_t2729843233;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector4>
struct DefaultComparer_t2729843234;
// System.Collections.Generic.EqualityComparer`1<AIEnum.EAIEventtype>
struct EqualityComparer_1_t3004126192;
// System.Collections.Generic.EqualityComparer`1<AnimationRunner/AniType>
struct EqualityComparer_1_t114075532;
// System.Collections.Generic.EqualityComparer`1<Core.RpsChoice>
struct EqualityComparer_1_t3455601309;
// System.Collections.Generic.EqualityComparer`1<Core.RpsResult>
struct EqualityComparer_1_t3882399001;
// System.Collections.Generic.EqualityComparer`1<Entity.Behavior.EBehaviorID>
struct EqualityComparer_1_t346370017;
// System.Collections.Generic.EqualityComparer`1<FLOAT_TEXT_ID>
struct EqualityComparer_1_t1307182651;
// System.Collections.Generic.EqualityComparer`1<HatredCtrl/stValue>
struct EqualityComparer_1_t2533782291;
// System.Collections.Generic.EqualityComparer`1<Mihua.Assets.SubAssetMgr/ErrorInfo>
struct EqualityComparer_1_t1741818091;
// System.Collections.Generic.EqualityComparer`1<MScrollView/MoveWay>
struct EqualityComparer_1_t2582778431;
// System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Linq.JTokenType>
struct EqualityComparer_1_t3024734442;
// System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Schema.JsonSchemaType>
struct EqualityComparer_1_t1223252702;
// System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>
struct EqualityComparer_1_t2079681672;
// System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>
struct EqualityComparer_1_t2000166371;
// System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>
struct EqualityComparer_1_t4268938351;
// System.Collections.Generic.EqualityComparer`1<Pathfinding.AdvancedSmooth/Turn>
struct EqualityComparer_1_t3867999555;
// System.Collections.Generic.EqualityComparer`1<Pathfinding.ClipperLib.IntPoint>
struct EqualityComparer_1_t2433963060;
// System.Collections.Generic.EqualityComparer`1<Pathfinding.Int2>
struct EqualityComparer_1_t1081882474;
// System.Collections.Generic.EqualityComparer`1<Pathfinding.Int3>
struct EqualityComparer_1_t1081882475;
// System.Collections.Generic.EqualityComparer`1<Pathfinding.IntRect>
struct EqualityComparer_1_t2122895142;
// System.Collections.Generic.EqualityComparer`1<Pathfinding.LocalAvoidance/IntersectionPair>
struct EqualityComparer_1_t1929156800;
// System.Collections.Generic.EqualityComparer`1<Pathfinding.LocalAvoidance/VOLine>
struct EqualityComparer_1_t1137768682;
// System.Collections.Generic.EqualityComparer`1<Pathfinding.Voxels.ExtraMesh>
struct EqualityComparer_1_t3325866596;
// System.Collections.Generic.EqualityComparer`1<Pathfinding.Voxels.VoxelContour>
struct EqualityComparer_1_t2705037897;
// System.Collections.Generic.EqualityComparer`1<PointCloudRegognizer/Point>
struct EqualityComparer_1_t946668631;
// System.Collections.Generic.EqualityComparer`1<ProductsCfgMgr/ProductInfo>
struct EqualityComparer_1_t413828119;
// System.Collections.Generic.EqualityComparer`1<PushType>
struct EqualityComparer_1_t948479333;
// System.Collections.Generic.EqualityComparer`1<SoundStatus>
struct EqualityComparer_1_t2700775826;
// System.Collections.Generic.EqualityComparer`1<SoundTypeID>
struct EqualityComparer_1_t2734453621;
// System.Collections.Generic.EqualityComparer`1<System.Boolean>
struct EqualityComparer_1_t3879602895;
// System.Collections.Generic.EqualityComparer`1<System.Byte>
struct EqualityComparer_1_t1970446541;
// System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct EqualityComparer_1_t1052505858;
// System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>
struct EqualityComparer_1_t3395768310;
// System.Collections.Generic.EqualityComparer`1<System.DateTime>
struct EqualityComparer_1_t3391498208;
// System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>
struct EqualityComparer_1_t2992551187;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2021033010.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2021033010MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_System_ArgumentNullException3573189601MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21919813716.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_ArgumentNullException3573189601.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21919813716MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp4145961110.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3647792461.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va721638723.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1958177301.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1958177301MethodDeclarations.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2126384403.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2126384403MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3338356402.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3338356402MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1736811037.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1736811037MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyNotFoundExce876339989MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Link2063667470.h"
#include "UnityEngine_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_KeyNotFoundExce876339989.h"
#include "mscorlib_System_ArgumentOutOfRangeException3816648464MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3816648464.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3278653252MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3278653252.h"
#include "mscorlib_System_Array1146569071MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144.h"
#include "mscorlib_System_Collections_Hashtable1407064410MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3253797991MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3253797991.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892MethodDeclarations.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3647792461MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va721638723MethodDeclarations.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1738881263.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1738881263MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21637661969.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21637661969MethodDeclarations.h"
#include "mscorlib_System_UInt1624667923.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3365640714.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va439486976.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3432882628.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3432882628MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3318937983.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3318937983MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3056204655.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3056204655MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1454659290.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1454659290MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3427472100MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3427472100.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3365640714MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va439486976MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4167966349.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4167966349MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066747055.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066747055MethodDeclarations.h"
#include "mscorlib_System_UInt3224667981.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1499758504.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2868572062.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1309936622.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1309936622MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3625077063.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3625077063MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1190322445.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1190322445MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3883744376.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3883744376MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3427472158MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3427472158.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1499758504MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2868572062MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2952530300.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2952530300MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22851311006.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22851311006MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIModelDisplayType1891752679.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke284322455.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1653136013.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3029156963.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3029156963MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4128861355.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4128861355MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4269853692.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4269853692MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2668308327.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2668308327MethodDeclarations.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare999589560MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare999589560.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare261675381MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare261675381.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke284322455MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1653136013MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1674540875.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1674540875MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21573321581.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21573321581MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3301300326.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va375146588.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr906965624.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr906965624MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr728680591.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr728680591MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2991864267.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2991864267MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1390318902.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1390318902MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3301300326MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va375146588MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2344065978.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2344065978MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3004126192MethodDeclarations.h"
#include "AssemblyU2DCSharp_AIEnum_EAIEventtype3896289311.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3748982614.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3748982614MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare114075532MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniType1006238651.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2795541095.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2795541095MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3455601309MethodDeclarations.h"
#include "AssemblyU2DCSharp_Core_RpsChoice52797132.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3222338787.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3222338787MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3882399001MethodDeclarations.h"
#include "AssemblyU2DCSharp_Core_RpsResult479594824.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3981277099.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3981277099MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare346370017MethodDeclarations.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare647122437.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare647122437MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1307182651MethodDeclarations.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1873722077.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1873722077MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2533782291MethodDeclarations.h"
#include "AssemblyU2DCSharp_HatredCtrl_stValue3425945410.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1081757877.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1081757877MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1741818091MethodDeclarations.h"
#include "AssemblyU2DCSharp_Mihua_Assets_SubAssetMgr_ErrorIn2633981210.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1922718217.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1922718217MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2582778431MethodDeclarations.h"
#include "AssemblyU2DCSharp_MScrollView_MoveWay3474941550.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2364674228.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2364674228MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3024734442MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType3916897561.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare563192488.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare563192488MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1223252702MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem2115415821.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1419621458.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1419621458MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2079681672MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_De2971844791.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_De2971844791MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1340106157.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1340106157MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2000166371MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js2892329490.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3608878137.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3608878137MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar4268938351MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Convert866134174.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Convert866134174MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3207939341.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3207939341MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3867999555MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_AdvancedSmooth_Turn465195378.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1773902846.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1773902846MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2433963060MethodDeclarations.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_IntP3326126179.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_IntP3326126179MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare421822260.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare421822260MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1081882474MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare421822261.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare421822261MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1081882475MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1462834928.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1462834928MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2122895142MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_IntRect3015058261.h"
#include "AssemblyU2DCSharp_Pathfinding_IntRect3015058261MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1269096586.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1269096586MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1929156800MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_Inter2821319919.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare477708468.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare477708468MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1137768682MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_VOLin2029931801.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2665806382.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2665806382MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3325866596MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_ExtraMesh4218029715.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2044977683.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2044977683MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2705037897MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_VoxelContour3597201016.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare286608417.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare286608417MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare946668631MethodDeclarations.h"
#include "AssemblyU2DCSharp_PointCloudRegognizer_Point1838831750.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar4048735201.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar4048735201MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare413828119MethodDeclarations.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr_ProductInfo1305991238.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare288419119.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare288419119MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare948479333MethodDeclarations.h"
#include "AssemblyU2DCSharp_PushType1840642452.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2040715612.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2040715612MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2700775826MethodDeclarations.h"
#include "AssemblyU2DCSharp_SoundStatus3592938945.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2074393407.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2074393407MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2734453621MethodDeclarations.h"
#include "AssemblyU2DCSharp_SoundTypeID3626616740.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3219542681.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3219542681MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3879602895MethodDeclarations.h"
#include "mscorlib_System_Boolean476798718MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1310386327.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1310386327MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1970446541MethodDeclarations.h"
#include "mscorlib_System_Byte2862609660.h"
#include "mscorlib_System_Byte2862609660MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare392445644.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare392445644MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1052505858MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2735708096.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2735708096MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3395768310MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24287931429.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2731437994.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2731437994MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3391498208MethodDeclarations.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_DateTime4283661327MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2332490973.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2332490973MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2992551187MethodDeclarations.h"
#include "mscorlib_System_DateTimeOffset3884714306.h"
#include "mscorlib_System_DateTimeOffset3884714306MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1310531096.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1310531096MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1970591310MethodDeclarations.h"
#include "mscorlib_System_Guid2862754429.h"
#include "mscorlib_System_Guid2862754429MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3896582463.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3896582463MethodDeclarations.h"
#include "mscorlib_System_Int321153838500MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2618593038.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2618593038MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1507389656.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1507389656MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2167449870MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArg3059612989.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArg3059612989MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1749070089.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1749070089MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2409130303MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg3301293422.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg3301293422MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2739695639.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2739695639MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3399755853MethodDeclarations.h"
#include "mscorlib_System_Single4291918972.h"
#include "mscorlib_System_Single4291918972MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3156266950.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3156266950MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3816327164MethodDeclarations.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "mscorlib_System_TimeSpan413522987MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2767411886.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2767411886MethodDeclarations.h"
#include "mscorlib_System_UInt1624667923MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2767411944.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2767411944MethodDeclarations.h"
#include "mscorlib_System_UInt3224667981MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1306248768.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1306248768MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1966308982MethodDeclarations.h"
#include "AssemblyU2DCSharp_TypewriterEffect_FadeEntry2858472101.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3888358432.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3888358432MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare253451350MethodDeclarations.h"
#include "AssemblyU2DCSharp_UICamera_DepthEntry1145614469.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare339529346.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare339529346MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1159418516.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1159418516MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1819478730MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Bounds2711641849MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2642323572.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2642323572MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3302383786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Color4194546905MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3341597651.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3341597651MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar4001657865MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2210438031.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2210438031MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2870498245MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastRes3762661364.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2569742905.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2569742905MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3229803119MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshSubsetCombineUtility_M4121966238.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare822616353.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare822616353MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1482676567MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshSubsetCombineUtility_S2374839686.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2450952393.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2450952393MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3111012607MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3823341701.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3823341701MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare188434619MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeB1080597738.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2593737777.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2593737777MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2808551447.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2808551447MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3468611661MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UICharInfo65807484.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2561652149.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2561652149MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3221712363MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UILineInfo4113875482.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2691841879.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2691841879MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3351902093MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UIVertex4244065212.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2729843232.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2729843232MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3389903446MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2729843233.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2729843233MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3389903447MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2729843234.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2729843234MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3389903448MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Vector44282066567MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3004126192.h"
#include "mscorlib_System_Activator2714366379MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare114075532.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3455601309.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3882399001.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare346370017.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1307182651.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2533782291.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1741818091.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2582778431.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3024734442.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1223252702.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2079681672.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2000166371.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar4268938351.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3867999555.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2433963060.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1081882474.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1081882475.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2122895142.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1929156800.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1137768682.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3325866596.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2705037897.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare946668631.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare413828119.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare948479333.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2700775826.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2734453621.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3879602895.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1970446541.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1052505858.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3395768310.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3391498208.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2992551187.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Do_CopyTo<System.Collections.DictionaryEntry,System.Collections.DictionaryEntry>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1751606614_TisDictionaryEntry_t1751606614_m3610181089_gshared (Dictionary_2_t2021033010 * __this, DictionaryEntryU5BU5D_t479206547* p0, int32_t p1, Transform_1_t1958177301 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1751606614_TisDictionaryEntry_t1751606614_m3610181089(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2021033010 *, DictionaryEntryU5BU5D_t479206547*, int32_t, Transform_1_t1958177301 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1751606614_TisDictionaryEntry_t1751606614_m3610181089_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Do_ICollectionCopyTo<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t1919813716_m1683628900_gshared (Dictionary_2_t2021033010 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t2126384403 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t1919813716_m1683628900(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2021033010 *, Il2CppArray *, int32_t, Transform_1_t2126384403 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t1919813716_m1683628900_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t1919813716_TisKeyValuePair_2_t1919813716_m2336579201_gshared (Dictionary_2_t2021033010 * __this, KeyValuePair_2U5BU5D_t3614328797* p0, int32_t p1, Transform_1_t2126384403 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t1919813716_TisKeyValuePair_2_t1919813716_m2336579201(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2021033010 *, KeyValuePair_2U5BU5D_t3614328797*, int32_t, Transform_1_t2126384403 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t1919813716_TisKeyValuePair_2_t1919813716_m2336579201_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::Do_CopyTo<System.Collections.DictionaryEntry,System.Collections.DictionaryEntry>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1751606614_TisDictionaryEntry_t1751606614_m2503974905_gshared (Dictionary_2_t1738881263 * __this, DictionaryEntryU5BU5D_t479206547* p0, int32_t p1, Transform_1_t3432882628 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1751606614_TisDictionaryEntry_t1751606614_m2503974905(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1738881263 *, DictionaryEntryU5BU5D_t479206547*, int32_t, Transform_1_t3432882628 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1751606614_TisDictionaryEntry_t1751606614_m2503974905_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::Do_ICollectionCopyTo<System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t1637661969_m1839004004_gshared (Dictionary_2_t1738881263 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t3318937983 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t1637661969_m1839004004(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1738881263 *, Il2CppArray *, int32_t, Transform_1_t3318937983 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t1637661969_m1839004004_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>,System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t1637661969_TisKeyValuePair_2_t1637661969_m2290748953_gshared (Dictionary_2_t1738881263 * __this, KeyValuePair_2U5BU5D_t794066828* p0, int32_t p1, Transform_1_t3318937983 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t1637661969_TisKeyValuePair_2_t1637661969_m2290748953(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1738881263 *, KeyValuePair_2U5BU5D_t794066828*, int32_t, Transform_1_t3318937983 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t1637661969_TisKeyValuePair_2_t1637661969_m2290748953_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::Do_CopyTo<System.Collections.DictionaryEntry,System.Collections.DictionaryEntry>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1751606614_TisDictionaryEntry_t1751606614_m1665042943_gshared (Dictionary_2_t4167966349 * __this, DictionaryEntryU5BU5D_t479206547* p0, int32_t p1, Transform_1_t1309936622 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1751606614_TisDictionaryEntry_t1751606614_m1665042943(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t4167966349 *, DictionaryEntryU5BU5D_t479206547*, int32_t, Transform_1_t1309936622 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1751606614_TisDictionaryEntry_t1751606614_m1665042943_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::Do_ICollectionCopyTo<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t4066747055_m332336932_gshared (Dictionary_2_t4167966349 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t3625077063 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t4066747055_m332336932(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t4167966349 *, Il2CppArray *, int32_t, Transform_1_t3625077063 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t4066747055_m332336932_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>,System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t4066747055_TisKeyValuePair_2_t4066747055_m1093680351_gshared (Dictionary_2_t4167966349 * __this, KeyValuePair_2U5BU5D_t2966088118* p0, int32_t p1, Transform_1_t3625077063 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t4066747055_TisKeyValuePair_2_t4066747055_m1093680351(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t4167966349 *, KeyValuePair_2U5BU5D_t2966088118*, int32_t, Transform_1_t3625077063 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t4066747055_TisKeyValuePair_2_t4066747055_m1093680351_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::Do_CopyTo<System.Collections.DictionaryEntry,System.Collections.DictionaryEntry>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1751606614_TisDictionaryEntry_t1751606614_m3935421941_gshared (Dictionary_2_t2952530300 * __this, DictionaryEntryU5BU5D_t479206547* p0, int32_t p1, Transform_1_t3029156963 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1751606614_TisDictionaryEntry_t1751606614_m3935421941(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2952530300 *, DictionaryEntryU5BU5D_t479206547*, int32_t, Transform_1_t3029156963 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1751606614_TisDictionaryEntry_t1751606614_m3935421941_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::Do_ICollectionCopyTo<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2851311006_m2525471204_gshared (Dictionary_2_t2952530300 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t4128861355 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2851311006_m2525471204(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2952530300 *, Il2CppArray *, int32_t, Transform_1_t4128861355 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2851311006_m2525471204_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>,System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2851311006_TisKeyValuePair_2_t2851311006_m3323577621_gshared (Dictionary_2_t2952530300 * __this, KeyValuePair_2U5BU5D_t390341163* p0, int32_t p1, Transform_1_t4128861355 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2851311006_TisKeyValuePair_2_t2851311006_m3323577621(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2952530300 *, KeyValuePair_2U5BU5D_t390341163*, int32_t, Transform_1_t4128861355 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2851311006_TisKeyValuePair_2_t2851311006_m3323577621_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::Do_CopyTo<System.Collections.DictionaryEntry,System.Collections.DictionaryEntry>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1751606614_TisDictionaryEntry_t1751606614_m3045918148_gshared (Dictionary_2_t1674540875 * __this, DictionaryEntryU5BU5D_t479206547* p0, int32_t p1, Transform_1_t906965624 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1751606614_TisDictionaryEntry_t1751606614_m3045918148(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1674540875 *, DictionaryEntryU5BU5D_t479206547*, int32_t, Transform_1_t906965624 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1751606614_TisDictionaryEntry_t1751606614_m3045918148_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::Do_ICollectionCopyTo<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t1573321581_m760153630_gshared (Dictionary_2_t1674540875 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t728680591 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t1573321581_m760153630(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1674540875 *, Il2CppArray *, int32_t, Transform_1_t728680591 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t1573321581_m760153630_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>,System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t1573321581_TisKeyValuePair_2_t1573321581_m1269478640_gshared (Dictionary_2_t1674540875 * __this, KeyValuePair_2U5BU5D_t2563117120* p0, int32_t p1, Transform_1_t728680591 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t1573321581_TisKeyValuePair_2_t1573321581_m1269478640(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1674540875 *, KeyValuePair_2U5BU5D_t2563117120*, int32_t, Transform_1_t728680591 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t1573321581_TisKeyValuePair_2_t1573321581_m1269478640_gshared)(__this, p0, p1, p2, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor()
extern "C"  void Dictionary_2__ctor_m3596022519_gshared (Dictionary_2_t2021033010 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t2021033010 *)__this);
		((  void (*) (Dictionary_2_t2021033010 *, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2021033010 *)__this, (int32_t)((int32_t)10), (Il2CppObject*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m200690670_gshared (Dictionary_2_t2021033010 * __this, Il2CppObject* ___comparer0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___comparer0;
		NullCheck((Dictionary_2_t2021033010 *)__this);
		((  void (*) (Dictionary_2_t2021033010 *, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2021033010 *)__this, (int32_t)((int32_t)10), (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m1942169345_gshared (Dictionary_2_t2021033010 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___dictionary0;
		NullCheck((Dictionary_2_t2021033010 *)__this);
		((  void (*) (Dictionary_2_t2021033010 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Dictionary_2_t2021033010 *)__this, (Il2CppObject*)L_0, (Il2CppObject*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m410316232_gshared (Dictionary_2_t2021033010 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		NullCheck((Dictionary_2_t2021033010 *)__this);
		((  void (*) (Dictionary_2_t2021033010 *, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2021033010 *)__this, (int32_t)L_0, (Il2CppObject*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral447049878;
extern const uint32_t Dictionary_2__ctor_m3966678940_MetadataUsageId;
extern "C"  void Dictionary_2__ctor_m3966678940_gshared (Dictionary_2_t2021033010 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2__ctor_m3966678940_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t1919813716  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Il2CppObject* V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral447049878, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Il2CppObject* L_2 = ___dictionary0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_3 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_2);
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		Il2CppObject* L_5 = ___comparer1;
		NullCheck((Dictionary_2_t2021033010 *)__this);
		((  void (*) (Dictionary_2_t2021033010 *, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2021033010 *)__this, (int32_t)L_4, (Il2CppObject*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Il2CppObject* L_6 = ___dictionary0;
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject* L_7 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_6);
		V_2 = (Il2CppObject*)L_7;
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004d;
		}

IL_0032:
		{
			Il2CppObject* L_8 = V_2;
			NullCheck((Il2CppObject*)L_8);
			KeyValuePair_2_t1919813716  L_9 = InterfaceFuncInvoker0< KeyValuePair_2_t1919813716  >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_8);
			V_1 = (KeyValuePair_2_t1919813716 )L_9;
			Il2CppObject * L_10 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1919813716 *)(&V_1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			int32_t L_11 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1919813716 *)(&V_1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			NullCheck((Dictionary_2_t2021033010 *)__this);
			((  void (*) (Dictionary_2_t2021033010 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t2021033010 *)__this, (Il2CppObject *)L_10, (int32_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		}

IL_004d:
		{
			Il2CppObject* L_12 = V_2;
			NullCheck((Il2CppObject *)L_12);
			bool L_13 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
			if (L_13)
			{
				goto IL_0032;
			}
		}

IL_0058:
		{
			IL2CPP_LEAVE(0x68, FINALLY_005d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_005d;
	}

FINALLY_005d:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_14 = V_2;
			if (L_14)
			{
				goto IL_0061;
			}
		}

IL_0060:
		{
			IL2CPP_END_FINALLY(93)
		}

IL_0061:
		{
			Il2CppObject* L_15 = V_2;
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			IL2CPP_END_FINALLY(93)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(93)
	{
		IL2CPP_JUMP_TBL(0x68, IL_0068)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0068:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m1696986040_gshared (Dictionary_2_t2021033010 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		SerializationInfo_t2185721892 * L_0 = ___info0;
		__this->set_serialization_info_13(L_0);
		return;
	}
}
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1679073015_gshared (Dictionary_2_t2021033010 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t2021033010 *)__this);
		KeyCollection_t3647792461 * L_0 = ((  KeyCollection_t3647792461 * (*) (Dictionary_2_t2021033010 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t2021033010 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_0;
	}
}
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3816547191_gshared (Dictionary_2_t2021033010 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t2021033010 *)__this);
		ValueCollection_t721638723 * L_0 = ((  ValueCollection_t721638723 * (*) (Dictionary_2_t2021033010 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t2021033010 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m3937643261_gshared (Dictionary_2_t2021033010 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t2021033010 *)__this);
		KeyCollection_t3647792461 * L_0 = ((  KeyCollection_t3647792461 * (*) (Dictionary_2_t2021033010 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t2021033010 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_0;
	}
}
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m2576540843_gshared (Dictionary_2_t2021033010 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t2021033010 *)__this);
		ValueCollection_t721638723 * L_0 = ((  ValueCollection_t721638723 * (*) (Dictionary_2_t2021033010 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t2021033010 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m420142666_gshared (Dictionary_2_t2021033010 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1021751151_gshared (Dictionary_2_t2021033010 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m3113787953_gshared (Dictionary_2_t2021033010 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		if (!((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))
		{
			goto IL_002f;
		}
	}
	{
		Il2CppObject * L_1 = ___key0;
		NullCheck((Dictionary_2_t2021033010 *)__this);
		bool L_2 = ((  bool (*) (Dictionary_2_t2021033010 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Dictionary_2_t2021033010 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Il2CppObject * L_3 = ___key0;
		NullCheck((Dictionary_2_t2021033010 *)__this);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Dictionary_2_t2021033010 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t2021033010 *)__this, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((Dictionary_2_t2021033010 *)__this);
		int32_t L_5 = ((  int32_t (*) (Dictionary_2_t2021033010 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((Dictionary_2_t2021033010 *)__this, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), &L_6);
		return L_7;
	}

IL_002f:
	{
		return NULL;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m2818300310_gshared (Dictionary_2_t2021033010 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		NullCheck((Dictionary_2_t2021033010 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Dictionary_2_t2021033010 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t2021033010 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		Il2CppObject * L_2 = ___value1;
		NullCheck((Dictionary_2_t2021033010 *)__this);
		int32_t L_3 = ((  int32_t (*) (Dictionary_2_t2021033010 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((Dictionary_2_t2021033010 *)__this, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		NullCheck((Dictionary_2_t2021033010 *)__this);
		((  void (*) (Dictionary_2_t2021033010 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((Dictionary_2_t2021033010 *)__this, (Il2CppObject *)L_1, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3378659067_gshared (Dictionary_2_t2021033010 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		NullCheck((Dictionary_2_t2021033010 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Dictionary_2_t2021033010 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t2021033010 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		Il2CppObject * L_2 = ___value1;
		NullCheck((Dictionary_2_t2021033010 *)__this);
		int32_t L_3 = ((  int32_t (*) (Dictionary_2_t2021033010 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((Dictionary_2_t2021033010 *)__this, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		NullCheck((Dictionary_2_t2021033010 *)__this);
		((  void (*) (Dictionary_2_t2021033010 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t2021033010 *)__this, (Il2CppObject *)L_1, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Contains(System.Object)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_System_Collections_IDictionary_Contains_m2891800219_MetadataUsageId;
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m2891800219_gshared (Dictionary_2_t2021033010 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_System_Collections_IDictionary_Contains_m2891800219_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___key0;
		if (!((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject * L_3 = ___key0;
		NullCheck((Dictionary_2_t2021033010 *)__this);
		bool L_4 = ((  bool (*) (Dictionary_2_t2021033010 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Dictionary_2_t2021033010 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_4;
	}

IL_0029:
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Remove(System.Object)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_System_Collections_IDictionary_Remove_m55590868_MetadataUsageId;
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m55590868_gshared (Dictionary_2_t2021033010 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_System_Collections_IDictionary_Remove_m55590868_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___key0;
		if (!((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject * L_3 = ___key0;
		NullCheck((Dictionary_2_t2021033010 *)__this);
		((  bool (*) (Dictionary_2_t2021033010 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((Dictionary_2_t2021033010 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m447434393_gshared (Dictionary_2_t2021033010 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4022893125_gshared (Dictionary_2_t2021033010 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2262377885_gshared (Dictionary_2_t2021033010 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3561681514_gshared (Dictionary_2_t2021033010 * __this, KeyValuePair_2_t1919813716  ___keyValuePair0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1919813716 *)(&___keyValuePair0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		int32_t L_1 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1919813716 *)(&___keyValuePair0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((Dictionary_2_t2021033010 *)__this);
		((  void (*) (Dictionary_2_t2021033010 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t2021033010 *)__this, (Il2CppObject *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1863948312_gshared (Dictionary_2_t2021033010 * __this, KeyValuePair_2_t1919813716  ___keyValuePair0, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1919813716  L_0 = ___keyValuePair0;
		NullCheck((Dictionary_2_t2021033010 *)__this);
		bool L_1 = ((  bool (*) (Dictionary_2_t2021033010 *, KeyValuePair_2_t1919813716 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((Dictionary_2_t2021033010 *)__this, (KeyValuePair_2_t1919813716 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m467393486_gshared (Dictionary_2_t2021033010 * __this, KeyValuePair_2U5BU5D_t3614328797* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t3614328797* L_0 = ___array0;
		int32_t L_1 = ___index1;
		NullCheck((Dictionary_2_t2021033010 *)__this);
		((  void (*) (Dictionary_2_t2021033010 *, KeyValuePair_2U5BU5D_t3614328797*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((Dictionary_2_t2021033010 *)__this, (KeyValuePair_2U5BU5D_t3614328797*)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m870245693_gshared (Dictionary_2_t2021033010 * __this, KeyValuePair_2_t1919813716  ___keyValuePair0, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1919813716  L_0 = ___keyValuePair0;
		NullCheck((Dictionary_2_t2021033010 *)__this);
		bool L_1 = ((  bool (*) (Dictionary_2_t2021033010 *, KeyValuePair_2_t1919813716 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((Dictionary_2_t2021033010 *)__this, (KeyValuePair_2_t1919813716 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (bool)0;
	}

IL_000e:
	{
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1919813716 *)(&___keyValuePair0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((Dictionary_2_t2021033010 *)__this);
		bool L_3 = ((  bool (*) (Dictionary_2_t2021033010 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((Dictionary_2_t2021033010 *)__this, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		return L_3;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* DictionaryEntryU5BU5D_t479206547_il2cpp_TypeInfo_var;
extern const uint32_t Dictionary_2_System_Collections_ICollection_CopyTo_m2454960685_MetadataUsageId;
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m2454960685_gshared (Dictionary_2_t2021033010 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_System_Collections_ICollection_CopyTo_m2454960685_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2U5BU5D_t3614328797* V_0 = NULL;
	DictionaryEntryU5BU5D_t479206547* V_1 = NULL;
	int32_t G_B5_0 = 0;
	DictionaryEntryU5BU5D_t479206547* G_B5_1 = NULL;
	Dictionary_2_t2021033010 * G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	DictionaryEntryU5BU5D_t479206547* G_B4_1 = NULL;
	Dictionary_2_t2021033010 * G_B4_2 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (KeyValuePair_2U5BU5D_t3614328797*)((KeyValuePair_2U5BU5D_t3614328797*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20)));
		KeyValuePair_2U5BU5D_t3614328797* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		KeyValuePair_2U5BU5D_t3614328797* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((Dictionary_2_t2021033010 *)__this);
		((  void (*) (Dictionary_2_t2021033010 *, KeyValuePair_2U5BU5D_t3614328797*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((Dictionary_2_t2021033010 *)__this, (KeyValuePair_2U5BU5D_t3614328797*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}

IL_0016:
	{
		Il2CppArray * L_4 = ___array0;
		int32_t L_5 = ___index1;
		NullCheck((Dictionary_2_t2021033010 *)__this);
		((  void (*) (Dictionary_2_t2021033010 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((Dictionary_2_t2021033010 *)__this, (Il2CppArray *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		Il2CppArray * L_6 = ___array0;
		V_1 = (DictionaryEntryU5BU5D_t479206547*)((DictionaryEntryU5BU5D_t479206547*)IsInst(L_6, DictionaryEntryU5BU5D_t479206547_il2cpp_TypeInfo_var));
		DictionaryEntryU5BU5D_t479206547* L_7 = V_1;
		if (!L_7)
		{
			goto IL_0051;
		}
	}
	{
		DictionaryEntryU5BU5D_t479206547* L_8 = V_1;
		int32_t L_9 = ___index1;
		Transform_1_t1958177301 * L_10 = ((Dictionary_2_t2021033010_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22)->static_fields)->get_U3CU3Ef__amU24cacheB_15();
		G_B4_0 = L_9;
		G_B4_1 = L_8;
		G_B4_2 = ((Dictionary_2_t2021033010 *)(__this));
		if (L_10)
		{
			G_B5_0 = L_9;
			G_B5_1 = L_8;
			G_B5_2 = ((Dictionary_2_t2021033010 *)(__this));
			goto IL_0046;
		}
	}
	{
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		Transform_1_t1958177301 * L_12 = (Transform_1_t1958177301 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 24));
		((  void (*) (Transform_1_t1958177301 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)(L_12, (Il2CppObject *)NULL, (IntPtr_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		((Dictionary_2_t2021033010_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22)->static_fields)->set_U3CU3Ef__amU24cacheB_15(L_12);
		G_B5_0 = G_B4_0;
		G_B5_1 = G_B4_1;
		G_B5_2 = ((Dictionary_2_t2021033010 *)(G_B4_2));
	}

IL_0046:
	{
		Transform_1_t1958177301 * L_13 = ((Dictionary_2_t2021033010_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22)->static_fields)->get_U3CU3Ef__amU24cacheB_15();
		NullCheck((Dictionary_2_t2021033010 *)G_B5_2);
		((  void (*) (Dictionary_2_t2021033010 *, DictionaryEntryU5BU5D_t479206547*, int32_t, Transform_1_t1958177301 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)((Dictionary_2_t2021033010 *)G_B5_2, (DictionaryEntryU5BU5D_t479206547*)G_B5_1, (int32_t)G_B5_0, (Transform_1_t1958177301 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		return;
	}

IL_0051:
	{
		Il2CppArray * L_14 = ___array0;
		int32_t L_15 = ___index1;
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		Transform_1_t2126384403 * L_17 = (Transform_1_t2126384403 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 28));
		((  void (*) (Transform_1_t2126384403 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)(L_17, (Il2CppObject *)NULL, (IntPtr_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		NullCheck((Dictionary_2_t2021033010 *)__this);
		((  void (*) (Dictionary_2_t2021033010 *, Il2CppArray *, int32_t, Transform_1_t2126384403 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((Dictionary_2_t2021033010 *)__this, (Il2CppArray *)L_14, (int32_t)L_15, (Transform_1_t2126384403 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1165648488_gshared (Dictionary_2_t2021033010 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3338356402  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m1870154201(&L_0, (Dictionary_2_t2021033010 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 32));
		Enumerator_t3338356402  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 31), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m634143205_gshared (Dictionary_2_t2021033010 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3338356402  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m1870154201(&L_0, (Dictionary_2_t2021033010 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 32));
		Enumerator_t3338356402  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 31), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1648852224_gshared (Dictionary_2_t2021033010 * __this, const MethodInfo* method)
{
	{
		ShimEnumerator_t1736811037 * L_0 = (ShimEnumerator_t1736811037 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 33));
		((  void (*) (ShimEnumerator_t1736811037 *, Dictionary_2_t2021033010 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34)->methodPointer)(L_0, (Dictionary_2_t2021033010 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m2721664223_gshared (Dictionary_2_t2021033010 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_count_10();
		return L_0;
	}
}
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Item(TKey)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* KeyNotFoundException_t876339989_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_get_Item_m3909921324_MetadataUsageId;
extern "C"  int32_t Dictionary_2_get_Item_m3909921324_gshared (Dictionary_2_t2021033010 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_get_Item_m3909921324_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		Il2CppObject * L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		Int32U5BU5D_t3230847821* L_5 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_6 = V_0;
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))));
		int32_t L_9 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (int32_t)((int32_t)((int32_t)L_9-(int32_t)1));
		goto IL_009b;
	}

IL_0048:
	{
		LinkU5BU5D_t375419643* L_10 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = (int32_t)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->get_HashCode_0();
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_0089;
		}
	}
	{
		Il2CppObject* L_14 = (Il2CppObject*)__this->get_hcp_12();
		ObjectU5BU5D_t1108656482* L_15 = (ObjectU5BU5D_t1108656482*)__this->get_keySlots_6();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		Il2CppObject * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		Il2CppObject * L_19 = ___key0;
		NullCheck((Il2CppObject*)L_14);
		bool L_20 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_14, (Il2CppObject *)L_18, (Il2CppObject *)L_19);
		if (!L_20)
		{
			goto IL_0089;
		}
	}
	{
		TextEditOpU5BU5D_t2239804499* L_21 = (TextEditOpU5BU5D_t2239804499*)__this->get_valueSlots_7();
		int32_t L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = L_22;
		int32_t L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		return L_24;
	}

IL_0089:
	{
		LinkU5BU5D_t375419643* L_25 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_26 = V_1;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		int32_t L_27 = (int32_t)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))->get_Next_1();
		V_1 = (int32_t)L_27;
	}

IL_009b:
	{
		int32_t L_28 = V_1;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		KeyNotFoundException_t876339989 * L_29 = (KeyNotFoundException_t876339989 *)il2cpp_codegen_object_new(KeyNotFoundException_t876339989_il2cpp_TypeInfo_var);
		KeyNotFoundException__ctor_m3542895460(L_29, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_29);
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::set_Item(TKey,TValue)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_set_Item_m82783351_MetadataUsageId;
extern "C"  void Dictionary_2_set_Item_m82783351_gshared (Dictionary_2_t2021033010 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_set_Item_m82783351_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		Il2CppObject * L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t3230847821* L_6 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))));
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_2 = (int32_t)((int32_t)((int32_t)L_10-(int32_t)1));
		V_3 = (int32_t)(-1);
		int32_t L_11 = V_2;
		if ((((int32_t)L_11) == ((int32_t)(-1))))
		{
			goto IL_00a2;
		}
	}

IL_004e:
	{
		LinkU5BU5D_t375419643* L_12 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = (int32_t)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_HashCode_0();
		int32_t L_15 = V_0;
		if ((!(((uint32_t)L_14) == ((uint32_t)L_15))))
		{
			goto IL_0087;
		}
	}
	{
		Il2CppObject* L_16 = (Il2CppObject*)__this->get_hcp_12();
		ObjectU5BU5D_t1108656482* L_17 = (ObjectU5BU5D_t1108656482*)__this->get_keySlots_6();
		int32_t L_18 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		Il2CppObject * L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		Il2CppObject * L_21 = ___key0;
		NullCheck((Il2CppObject*)L_16);
		bool L_22 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_16, (Il2CppObject *)L_20, (Il2CppObject *)L_21);
		if (!L_22)
		{
			goto IL_0087;
		}
	}
	{
		goto IL_00a2;
	}

IL_0087:
	{
		int32_t L_23 = V_2;
		V_3 = (int32_t)L_23;
		LinkU5BU5D_t375419643* L_24 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_25 = V_2;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		int32_t L_26 = (int32_t)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_25)))->get_Next_1();
		V_2 = (int32_t)L_26;
		int32_t L_27 = V_2;
		if ((!(((uint32_t)L_27) == ((uint32_t)(-1)))))
		{
			goto IL_004e;
		}
	}

IL_00a2:
	{
		int32_t L_28 = V_2;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_0166;
		}
	}
	{
		int32_t L_29 = (int32_t)__this->get_count_10();
		int32_t L_30 = (int32_t)((int32_t)((int32_t)L_29+(int32_t)1));
		V_4 = (int32_t)L_30;
		__this->set_count_10(L_30);
		int32_t L_31 = V_4;
		int32_t L_32 = (int32_t)__this->get_threshold_11();
		if ((((int32_t)L_31) <= ((int32_t)L_32)))
		{
			goto IL_00de;
		}
	}
	{
		NullCheck((Dictionary_2_t2021033010 *)__this);
		((  void (*) (Dictionary_2_t2021033010 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36)->methodPointer)((Dictionary_2_t2021033010 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36));
		int32_t L_33 = V_0;
		Int32U5BU5D_t3230847821* L_34 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_34);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_33&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_34)->max_length))))));
	}

IL_00de:
	{
		int32_t L_35 = (int32_t)__this->get_emptySlot_9();
		V_2 = (int32_t)L_35;
		int32_t L_36 = V_2;
		if ((!(((uint32_t)L_36) == ((uint32_t)(-1)))))
		{
			goto IL_0105;
		}
	}
	{
		int32_t L_37 = (int32_t)__this->get_touchedSlots_8();
		int32_t L_38 = (int32_t)L_37;
		V_4 = (int32_t)L_38;
		__this->set_touchedSlots_8(((int32_t)((int32_t)L_38+(int32_t)1)));
		int32_t L_39 = V_4;
		V_2 = (int32_t)L_39;
		goto IL_011c;
	}

IL_0105:
	{
		LinkU5BU5D_t375419643* L_40 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_41 = V_2;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, L_41);
		int32_t L_42 = (int32_t)((L_40)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_41)))->get_Next_1();
		__this->set_emptySlot_9(L_42);
	}

IL_011c:
	{
		LinkU5BU5D_t375419643* L_43 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_44 = V_2;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		Int32U5BU5D_t3230847821* L_45 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_46 = V_1;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, L_46);
		int32_t L_47 = L_46;
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_44)))->set_Next_1(((int32_t)((int32_t)L_48-(int32_t)1)));
		Int32U5BU5D_t3230847821* L_49 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_50 = V_1;
		int32_t L_51 = V_2;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, L_50);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(L_50), (int32_t)((int32_t)((int32_t)L_51+(int32_t)1)));
		LinkU5BU5D_t375419643* L_52 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_53 = V_2;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, L_53);
		int32_t L_54 = V_0;
		((L_52)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_53)))->set_HashCode_0(L_54);
		ObjectU5BU5D_t1108656482* L_55 = (ObjectU5BU5D_t1108656482*)__this->get_keySlots_6();
		int32_t L_56 = V_2;
		Il2CppObject * L_57 = ___key0;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, L_56);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(L_56), (Il2CppObject *)L_57);
		goto IL_01b5;
	}

IL_0166:
	{
		int32_t L_58 = V_3;
		if ((((int32_t)L_58) == ((int32_t)(-1))))
		{
			goto IL_01b5;
		}
	}
	{
		LinkU5BU5D_t375419643* L_59 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_60 = V_3;
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, L_60);
		LinkU5BU5D_t375419643* L_61 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_62 = V_2;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, L_62);
		int32_t L_63 = (int32_t)((L_61)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_62)))->get_Next_1();
		((L_59)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_60)))->set_Next_1(L_63);
		LinkU5BU5D_t375419643* L_64 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_65 = V_2;
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, L_65);
		Int32U5BU5D_t3230847821* L_66 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_67 = V_1;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, L_67);
		int32_t L_68 = L_67;
		int32_t L_69 = (L_66)->GetAt(static_cast<il2cpp_array_size_t>(L_68));
		((L_64)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_65)))->set_Next_1(((int32_t)((int32_t)L_69-(int32_t)1)));
		Int32U5BU5D_t3230847821* L_70 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_71 = V_1;
		int32_t L_72 = V_2;
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, L_71);
		(L_70)->SetAt(static_cast<il2cpp_array_size_t>(L_71), (int32_t)((int32_t)((int32_t)L_72+(int32_t)1)));
	}

IL_01b5:
	{
		TextEditOpU5BU5D_t2239804499* L_73 = (TextEditOpU5BU5D_t2239804499*)__this->get_valueSlots_7();
		int32_t L_74 = V_2;
		int32_t L_75 = ___value1;
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, L_74);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(L_74), (int32_t)L_75);
		int32_t L_76 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_76+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4227142842;
extern const uint32_t Dictionary_2_Init_m2377683567_MetadataUsageId;
extern "C"  void Dictionary_2_Init_m2377683567_gshared (Dictionary_2_t2021033010 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Init_m2377683567_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Dictionary_2_t2021033010 * G_B4_0 = NULL;
	Dictionary_2_t2021033010 * G_B3_0 = NULL;
	Il2CppObject* G_B5_0 = NULL;
	Dictionary_2_t2021033010 * G_B5_1 = NULL;
	{
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_1 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_1, (String_t*)_stringLiteral4227142842, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Il2CppObject* L_2 = ___hcp1;
		G_B3_0 = ((Dictionary_2_t2021033010 *)(__this));
		if (!L_2)
		{
			G_B4_0 = ((Dictionary_2_t2021033010 *)(__this));
			goto IL_0021;
		}
	}
	{
		Il2CppObject* L_3 = ___hcp1;
		V_0 = (Il2CppObject*)L_3;
		Il2CppObject* L_4 = V_0;
		G_B5_0 = L_4;
		G_B5_1 = ((Dictionary_2_t2021033010 *)(G_B3_0));
		goto IL_0026;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 38));
		EqualityComparer_1_t3278653252 * L_5 = ((  EqualityComparer_1_t3278653252 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 37)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 37));
		G_B5_0 = ((Il2CppObject*)(L_5));
		G_B5_1 = ((Dictionary_2_t2021033010 *)(G_B4_0));
	}

IL_0026:
	{
		NullCheck(G_B5_1);
		G_B5_1->set_hcp_12(G_B5_0);
		int32_t L_6 = ___capacity0;
		if (L_6)
		{
			goto IL_0035;
		}
	}
	{
		___capacity0 = (int32_t)((int32_t)10);
	}

IL_0035:
	{
		int32_t L_7 = ___capacity0;
		___capacity0 = (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)((float)((float)(((float)((float)L_7)))/(float)(0.9f))))))+(int32_t)1));
		int32_t L_8 = ___capacity0;
		NullCheck((Dictionary_2_t2021033010 *)__this);
		((  void (*) (Dictionary_2_t2021033010 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 39)->methodPointer)((Dictionary_2_t2021033010 *)__this, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 39));
		__this->set_generation_14(0);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::InitArrays(System.Int32)
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern Il2CppClass* LinkU5BU5D_t375419643_il2cpp_TypeInfo_var;
extern const uint32_t Dictionary_2_InitArrays_m4150695208_MetadataUsageId;
extern "C"  void Dictionary_2_InitArrays_m4150695208_gshared (Dictionary_2_t2021033010 * __this, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_InitArrays_m4150695208_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___size0;
		__this->set_table_4(((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)L_0)));
		int32_t L_1 = ___size0;
		__this->set_linkSlots_5(((LinkU5BU5D_t375419643*)SZArrayNew(LinkU5BU5D_t375419643_il2cpp_TypeInfo_var, (uint32_t)L_1)));
		__this->set_emptySlot_9((-1));
		int32_t L_2 = ___size0;
		__this->set_keySlots_6(((ObjectU5BU5D_t1108656482*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 40), (uint32_t)L_2)));
		int32_t L_3 = ___size0;
		__this->set_valueSlots_7(((TextEditOpU5BU5D_t2239804499*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 41), (uint32_t)L_3)));
		__this->set_touchedSlots_8(0);
		Int32U5BU5D_t3230847821* L_4 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_4);
		__this->set_threshold_11((((int32_t)((int32_t)((float)((float)(((float)((float)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length)))))))*(float)(0.9f)))))));
		int32_t L_5 = (int32_t)__this->get_threshold_11();
		if (L_5)
		{
			goto IL_0074;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_6 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_6);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0074;
		}
	}
	{
		__this->set_threshold_11(1);
	}

IL_0074:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::CopyToCheck(System.Array,System.Int32)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern Il2CppCodeGenString* _stringLiteral1142730282;
extern Il2CppCodeGenString* _stringLiteral2802514764;
extern const uint32_t Dictionary_2_CopyToCheck_m2542357284_MetadataUsageId;
extern "C"  void Dictionary_2_CopyToCheck_m2542357284_gshared (Dictionary_2_t2021033010 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_CopyToCheck_m2542357284_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppArray * L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		int32_t L_4 = ___index1;
		Il2CppArray * L_5 = ___array0;
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)L_6)))
		{
			goto IL_003a;
		}
	}
	{
		ArgumentException_t928607144 * L_7 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_7, (String_t*)_stringLiteral1142730282, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003a:
	{
		Il2CppArray * L_8 = ___array0;
		NullCheck((Il2CppArray *)L_8);
		int32_t L_9 = Array_get_Length_m1203127607((Il2CppArray *)L_8, /*hidden argument*/NULL);
		int32_t L_10 = ___index1;
		NullCheck((Dictionary_2_t2021033010 *)__this);
		int32_t L_11 = ((  int32_t (*) (Dictionary_2_t2021033010 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 42)->methodPointer)((Dictionary_2_t2021033010 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 42));
		if ((((int32_t)((int32_t)((int32_t)L_9-(int32_t)L_10))) >= ((int32_t)L_11)))
		{
			goto IL_0058;
		}
	}
	{
		ArgumentException_t928607144 * L_12 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_12, (String_t*)_stringLiteral2802514764, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0058:
	{
		return;
	}
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t1919813716  Dictionary_2_make_pair_m1700658288_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		int32_t L_1 = ___value1;
		KeyValuePair_2_t1919813716  L_2;
		memset(&L_2, 0, sizeof(L_2));
		KeyValuePair_2__ctor_m2418427527(&L_2, (Il2CppObject *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 44));
		return L_2;
	}
}
// TKey System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::pick_key(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_key_m2946196358_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		return L_0;
	}
}
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::pick_value(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_value_m1577578438_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value1;
		return L_0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m1878525995_gshared (Dictionary_2_t2021033010 * __this, KeyValuePair_2U5BU5D_t3614328797* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t3614328797* L_0 = ___array0;
		int32_t L_1 = ___index1;
		NullCheck((Dictionary_2_t2021033010 *)__this);
		((  void (*) (Dictionary_2_t2021033010 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((Dictionary_2_t2021033010 *)__this, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		KeyValuePair_2U5BU5D_t3614328797* L_2 = ___array0;
		int32_t L_3 = ___index1;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		Transform_1_t2126384403 * L_5 = (Transform_1_t2126384403 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 28));
		((  void (*) (Transform_1_t2126384403 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)(L_5, (Il2CppObject *)NULL, (IntPtr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		NullCheck((Dictionary_2_t2021033010 *)__this);
		((  void (*) (Dictionary_2_t2021033010 *, KeyValuePair_2U5BU5D_t3614328797*, int32_t, Transform_1_t2126384403 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 45)->methodPointer)((Dictionary_2_t2021033010 *)__this, (KeyValuePair_2U5BU5D_t3614328797*)L_2, (int32_t)L_3, (Transform_1_t2126384403 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 45));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Resize()
extern Il2CppClass* Hashtable_t1407064410_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern Il2CppClass* LinkU5BU5D_t375419643_il2cpp_TypeInfo_var;
extern const uint32_t Dictionary_2_Resize_m4160312353_MetadataUsageId;
extern "C"  void Dictionary_2_Resize_m4160312353_gshared (Dictionary_2_t2021033010 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Resize_m4160312353_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Int32U5BU5D_t3230847821* V_1 = NULL;
	LinkU5BU5D_t375419643* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	ObjectU5BU5D_t1108656482* V_7 = NULL;
	TextEditOpU5BU5D_t2239804499* V_8 = NULL;
	int32_t V_9 = 0;
	{
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Hashtable_t1407064410_il2cpp_TypeInfo_var);
		int32_t L_1 = Hashtable_ToPrime_m840372929(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))<<(int32_t)1))|(int32_t)1)), /*hidden argument*/NULL);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		V_1 = (Int32U5BU5D_t3230847821*)((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)L_2));
		int32_t L_3 = V_0;
		V_2 = (LinkU5BU5D_t375419643*)((LinkU5BU5D_t375419643*)SZArrayNew(LinkU5BU5D_t375419643_il2cpp_TypeInfo_var, (uint32_t)L_3));
		V_3 = (int32_t)0;
		goto IL_00b1;
	}

IL_0027:
	{
		Int32U5BU5D_t3230847821* L_4 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_5 = V_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		int32_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_4 = (int32_t)((int32_t)((int32_t)L_7-(int32_t)1));
		goto IL_00a5;
	}

IL_0038:
	{
		LinkU5BU5D_t375419643* L_8 = V_2;
		int32_t L_9 = V_4;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		Il2CppObject* L_10 = (Il2CppObject*)__this->get_hcp_12();
		ObjectU5BU5D_t1108656482* L_11 = (ObjectU5BU5D_t1108656482*)__this->get_keySlots_6();
		int32_t L_12 = V_4;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Il2CppObject*)L_10);
		int32_t L_15 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_10, (Il2CppObject *)L_14);
		int32_t L_16 = (int32_t)((int32_t)((int32_t)L_15|(int32_t)((int32_t)-2147483648LL)));
		V_9 = (int32_t)L_16;
		((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9)))->set_HashCode_0(L_16);
		int32_t L_17 = V_9;
		V_5 = (int32_t)L_17;
		int32_t L_18 = V_5;
		int32_t L_19 = V_0;
		V_6 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_18&(int32_t)((int32_t)2147483647LL)))%(int32_t)L_19));
		LinkU5BU5D_t375419643* L_20 = V_2;
		int32_t L_21 = V_4;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		Int32U5BU5D_t3230847821* L_22 = V_1;
		int32_t L_23 = V_6;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		int32_t L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_21)))->set_Next_1(((int32_t)((int32_t)L_25-(int32_t)1)));
		Int32U5BU5D_t3230847821* L_26 = V_1;
		int32_t L_27 = V_6;
		int32_t L_28 = V_4;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(L_27), (int32_t)((int32_t)((int32_t)L_28+(int32_t)1)));
		LinkU5BU5D_t375419643* L_29 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_30 = V_4;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
		int32_t L_31 = (int32_t)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_30)))->get_Next_1();
		V_4 = (int32_t)L_31;
	}

IL_00a5:
	{
		int32_t L_32 = V_4;
		if ((!(((uint32_t)L_32) == ((uint32_t)(-1)))))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_33 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00b1:
	{
		int32_t L_34 = V_3;
		Int32U5BU5D_t3230847821* L_35 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_35);
		if ((((int32_t)L_34) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_35)->max_length)))))))
		{
			goto IL_0027;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_36 = V_1;
		__this->set_table_4(L_36);
		LinkU5BU5D_t375419643* L_37 = V_2;
		__this->set_linkSlots_5(L_37);
		int32_t L_38 = V_0;
		V_7 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 40), (uint32_t)L_38));
		int32_t L_39 = V_0;
		V_8 = (TextEditOpU5BU5D_t2239804499*)((TextEditOpU5BU5D_t2239804499*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 41), (uint32_t)L_39));
		ObjectU5BU5D_t1108656482* L_40 = (ObjectU5BU5D_t1108656482*)__this->get_keySlots_6();
		ObjectU5BU5D_t1108656482* L_41 = V_7;
		int32_t L_42 = (int32_t)__this->get_touchedSlots_8();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_40, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_41, (int32_t)0, (int32_t)L_42, /*hidden argument*/NULL);
		TextEditOpU5BU5D_t2239804499* L_43 = (TextEditOpU5BU5D_t2239804499*)__this->get_valueSlots_7();
		TextEditOpU5BU5D_t2239804499* L_44 = V_8;
		int32_t L_45 = (int32_t)__this->get_touchedSlots_8();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_43, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_44, (int32_t)0, (int32_t)L_45, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_46 = V_7;
		__this->set_keySlots_6(L_46);
		TextEditOpU5BU5D_t2239804499* L_47 = V_8;
		__this->set_valueSlots_7(L_47);
		int32_t L_48 = V_0;
		__this->set_threshold_11((((int32_t)((int32_t)((float)((float)(((float)((float)L_48)))*(float)(0.9f)))))));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Add(TKey,TValue)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern Il2CppCodeGenString* _stringLiteral628480033;
extern const uint32_t Dictionary_2_Add_m1983686558_MetadataUsageId;
extern "C"  void Dictionary_2_Add_m1983686558_gshared (Dictionary_2_t2021033010 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Add_m1983686558_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		Il2CppObject * L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t3230847821* L_6 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))));
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_2 = (int32_t)((int32_t)((int32_t)L_10-(int32_t)1));
		goto IL_009b;
	}

IL_004a:
	{
		LinkU5BU5D_t375419643* L_11 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = (int32_t)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)))->get_HashCode_0();
		int32_t L_14 = V_0;
		if ((!(((uint32_t)L_13) == ((uint32_t)L_14))))
		{
			goto IL_0089;
		}
	}
	{
		Il2CppObject* L_15 = (Il2CppObject*)__this->get_hcp_12();
		ObjectU5BU5D_t1108656482* L_16 = (ObjectU5BU5D_t1108656482*)__this->get_keySlots_6();
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		Il2CppObject * L_20 = ___key0;
		NullCheck((Il2CppObject*)L_15);
		bool L_21 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_15, (Il2CppObject *)L_19, (Il2CppObject *)L_20);
		if (!L_21)
		{
			goto IL_0089;
		}
	}
	{
		ArgumentException_t928607144 * L_22 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_22, (String_t*)_stringLiteral628480033, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	}

IL_0089:
	{
		LinkU5BU5D_t375419643* L_23 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_24 = V_2;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = (int32_t)((L_23)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_24)))->get_Next_1();
		V_2 = (int32_t)L_25;
	}

IL_009b:
	{
		int32_t L_26 = V_2;
		if ((!(((uint32_t)L_26) == ((uint32_t)(-1)))))
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_27 = (int32_t)__this->get_count_10();
		int32_t L_28 = (int32_t)((int32_t)((int32_t)L_27+(int32_t)1));
		V_3 = (int32_t)L_28;
		__this->set_count_10(L_28);
		int32_t L_29 = V_3;
		int32_t L_30 = (int32_t)__this->get_threshold_11();
		if ((((int32_t)L_29) <= ((int32_t)L_30)))
		{
			goto IL_00d5;
		}
	}
	{
		NullCheck((Dictionary_2_t2021033010 *)__this);
		((  void (*) (Dictionary_2_t2021033010 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36)->methodPointer)((Dictionary_2_t2021033010 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36));
		int32_t L_31 = V_0;
		Int32U5BU5D_t3230847821* L_32 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_32);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_31&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_32)->max_length))))));
	}

IL_00d5:
	{
		int32_t L_33 = (int32_t)__this->get_emptySlot_9();
		V_2 = (int32_t)L_33;
		int32_t L_34 = V_2;
		if ((!(((uint32_t)L_34) == ((uint32_t)(-1)))))
		{
			goto IL_00fa;
		}
	}
	{
		int32_t L_35 = (int32_t)__this->get_touchedSlots_8();
		int32_t L_36 = (int32_t)L_35;
		V_3 = (int32_t)L_36;
		__this->set_touchedSlots_8(((int32_t)((int32_t)L_36+(int32_t)1)));
		int32_t L_37 = V_3;
		V_2 = (int32_t)L_37;
		goto IL_0111;
	}

IL_00fa:
	{
		LinkU5BU5D_t375419643* L_38 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_39 = V_2;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_39);
		int32_t L_40 = (int32_t)((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_39)))->get_Next_1();
		__this->set_emptySlot_9(L_40);
	}

IL_0111:
	{
		LinkU5BU5D_t375419643* L_41 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_42 = V_2;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
		int32_t L_43 = V_0;
		((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_42)))->set_HashCode_0(L_43);
		LinkU5BU5D_t375419643* L_44 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_45 = V_2;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, L_45);
		Int32U5BU5D_t3230847821* L_46 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_47 = V_1;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, L_47);
		int32_t L_48 = L_47;
		int32_t L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_45)))->set_Next_1(((int32_t)((int32_t)L_49-(int32_t)1)));
		Int32U5BU5D_t3230847821* L_50 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_51 = V_1;
		int32_t L_52 = V_2;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, L_51);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(L_51), (int32_t)((int32_t)((int32_t)L_52+(int32_t)1)));
		ObjectU5BU5D_t1108656482* L_53 = (ObjectU5BU5D_t1108656482*)__this->get_keySlots_6();
		int32_t L_54 = V_2;
		Il2CppObject * L_55 = ___key0;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, L_54);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(L_54), (Il2CppObject *)L_55);
		TextEditOpU5BU5D_t2239804499* L_56 = (TextEditOpU5BU5D_t2239804499*)__this->get_valueSlots_7();
		int32_t L_57 = V_2;
		int32_t L_58 = ___value1;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, L_57);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(L_57), (int32_t)L_58);
		int32_t L_59 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_59+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Clear()
extern "C"  void Dictionary_2_Clear_m1002155810_gshared (Dictionary_2_t2021033010 * __this, const MethodInfo* method)
{
	{
		__this->set_count_10(0);
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		Int32U5BU5D_t3230847821* L_1 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_1);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_2 = (ObjectU5BU5D_t1108656482*)__this->get_keySlots_6();
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)__this->get_keySlots_6();
		NullCheck(L_3);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))), /*hidden argument*/NULL);
		TextEditOpU5BU5D_t2239804499* L_4 = (TextEditOpU5BU5D_t2239804499*)__this->get_valueSlots_7();
		TextEditOpU5BU5D_t2239804499* L_5 = (TextEditOpU5BU5D_t2239804499*)__this->get_valueSlots_7();
		NullCheck(L_5);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_4, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))), /*hidden argument*/NULL);
		LinkU5BU5D_t375419643* L_6 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		LinkU5BU5D_t375419643* L_7 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		NullCheck(L_7);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))), /*hidden argument*/NULL);
		__this->set_emptySlot_9((-1));
		__this->set_touchedSlots_8(0);
		int32_t L_8 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_8+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ContainsKey(TKey)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_ContainsKey_m3078952584_MetadataUsageId;
extern "C"  bool Dictionary_2_ContainsKey_m3078952584_gshared (Dictionary_2_t2021033010 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_ContainsKey_m3078952584_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		Il2CppObject * L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		Int32U5BU5D_t3230847821* L_5 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_6 = V_0;
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))));
		int32_t L_9 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (int32_t)((int32_t)((int32_t)L_9-(int32_t)1));
		goto IL_0090;
	}

IL_0048:
	{
		LinkU5BU5D_t375419643* L_10 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = (int32_t)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->get_HashCode_0();
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_007e;
		}
	}
	{
		Il2CppObject* L_14 = (Il2CppObject*)__this->get_hcp_12();
		ObjectU5BU5D_t1108656482* L_15 = (ObjectU5BU5D_t1108656482*)__this->get_keySlots_6();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		Il2CppObject * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		Il2CppObject * L_19 = ___key0;
		NullCheck((Il2CppObject*)L_14);
		bool L_20 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_14, (Il2CppObject *)L_18, (Il2CppObject *)L_19);
		if (!L_20)
		{
			goto IL_007e;
		}
	}
	{
		return (bool)1;
	}

IL_007e:
	{
		LinkU5BU5D_t375419643* L_21 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = (int32_t)((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_22)))->get_Next_1();
		V_1 = (int32_t)L_23;
	}

IL_0090:
	{
		int32_t L_24 = V_1;
		if ((!(((uint32_t)L_24) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m2665604744_gshared (Dictionary_2_t2021033010 * __this, int32_t ___value0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 47));
		EqualityComparer_1_t3253797991 * L_0 = ((  EqualityComparer_1_t3253797991 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46));
		V_0 = (Il2CppObject*)L_0;
		V_1 = (int32_t)0;
		goto IL_0054;
	}

IL_000d:
	{
		Int32U5BU5D_t3230847821* L_1 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		int32_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_2 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		goto IL_0049;
	}

IL_001d:
	{
		Il2CppObject* L_5 = V_0;
		TextEditOpU5BU5D_t2239804499* L_6 = (TextEditOpU5BU5D_t2239804499*)__this->get_valueSlots_7();
		int32_t L_7 = V_2;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		int32_t L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		int32_t L_10 = ___value0;
		NullCheck((Il2CppObject*)L_5);
		bool L_11 = InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<UnityEngine.TextEditor/TextEditOp>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 48), (Il2CppObject*)L_5, (int32_t)L_9, (int32_t)L_10);
		if (!L_11)
		{
			goto IL_0037;
		}
	}
	{
		return (bool)1;
	}

IL_0037:
	{
		LinkU5BU5D_t375419643* L_12 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = (int32_t)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_Next_1();
		V_2 = (int32_t)L_14;
	}

IL_0049:
	{
		int32_t L_15 = V_2;
		if ((!(((uint32_t)L_15) == ((uint32_t)(-1)))))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_17 = V_1;
		Int32U5BU5D_t3230847821* L_18 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3237038;
extern Il2CppCodeGenString* _stringLiteral2016261304;
extern Il2CppCodeGenString* _stringLiteral3759850573;
extern Il2CppCodeGenString* _stringLiteral212812367;
extern Il2CppCodeGenString* _stringLiteral961688967;
extern const uint32_t Dictionary_2_GetObjectData_m1995703061_MetadataUsageId;
extern "C"  void Dictionary_2_GetObjectData_m1995703061_gshared (Dictionary_2_t2021033010 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_GetObjectData_m1995703061_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2U5BU5D_t3614328797* V_0 = NULL;
	{
		SerializationInfo_t2185721892 * L_0 = ___info0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral3237038, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		SerializationInfo_t2185721892 * L_2 = ___info0;
		int32_t L_3 = (int32_t)__this->get_generation_14();
		NullCheck((SerializationInfo_t2185721892 *)L_2);
		SerializationInfo_AddValue_m2348540514((SerializationInfo_t2185721892 *)L_2, (String_t*)_stringLiteral2016261304, (int32_t)L_3, /*hidden argument*/NULL);
		SerializationInfo_t2185721892 * L_4 = ___info0;
		Il2CppObject* L_5 = (Il2CppObject*)__this->get_hcp_12();
		NullCheck((SerializationInfo_t2185721892 *)L_4);
		SerializationInfo_AddValue_m469120675((SerializationInfo_t2185721892 *)L_4, (String_t*)_stringLiteral3759850573, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		V_0 = (KeyValuePair_2U5BU5D_t3614328797*)NULL;
		int32_t L_6 = (int32_t)__this->get_count_10();
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_7 = (int32_t)__this->get_count_10();
		V_0 = (KeyValuePair_2U5BU5D_t3614328797*)((KeyValuePair_2U5BU5D_t3614328797*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 49), (uint32_t)L_7));
		KeyValuePair_2U5BU5D_t3614328797* L_8 = V_0;
		NullCheck((Dictionary_2_t2021033010 *)__this);
		((  void (*) (Dictionary_2_t2021033010 *, KeyValuePair_2U5BU5D_t3614328797*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((Dictionary_2_t2021033010 *)__this, (KeyValuePair_2U5BU5D_t3614328797*)L_8, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
	}

IL_0055:
	{
		SerializationInfo_t2185721892 * L_9 = ___info0;
		Int32U5BU5D_t3230847821* L_10 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_10);
		NullCheck((SerializationInfo_t2185721892 *)L_9);
		SerializationInfo_AddValue_m2348540514((SerializationInfo_t2185721892 *)L_9, (String_t*)_stringLiteral212812367, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))), /*hidden argument*/NULL);
		SerializationInfo_t2185721892 * L_11 = ___info0;
		KeyValuePair_2U5BU5D_t3614328797* L_12 = V_0;
		NullCheck((SerializationInfo_t2185721892 *)L_11);
		SerializationInfo_AddValue_m469120675((SerializationInfo_t2185721892 *)L_11, (String_t*)_stringLiteral961688967, (Il2CppObject *)(Il2CppObject *)L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::OnDeserialization(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2016261304;
extern Il2CppCodeGenString* _stringLiteral3759850573;
extern Il2CppCodeGenString* _stringLiteral212812367;
extern Il2CppCodeGenString* _stringLiteral961688967;
extern const uint32_t Dictionary_2_OnDeserialization_m2880463471_MetadataUsageId;
extern "C"  void Dictionary_2_OnDeserialization_m2880463471_gshared (Dictionary_2_t2021033010 * __this, Il2CppObject * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_OnDeserialization_m2880463471_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2U5BU5D_t3614328797* V_1 = NULL;
	int32_t V_2 = 0;
	{
		SerializationInfo_t2185721892 * L_0 = (SerializationInfo_t2185721892 *)__this->get_serialization_info_13();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		SerializationInfo_t2185721892 * L_1 = (SerializationInfo_t2185721892 *)__this->get_serialization_info_13();
		NullCheck((SerializationInfo_t2185721892 *)L_1);
		int32_t L_2 = SerializationInfo_GetInt32_m4048035953((SerializationInfo_t2185721892 *)L_1, (String_t*)_stringLiteral2016261304, /*hidden argument*/NULL);
		__this->set_generation_14(L_2);
		SerializationInfo_t2185721892 * L_3 = (SerializationInfo_t2185721892 *)__this->get_serialization_info_13();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 50)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2185721892 *)L_3);
		Il2CppObject * L_5 = SerializationInfo_GetValue_m4125471336((SerializationInfo_t2185721892 *)L_3, (String_t*)_stringLiteral3759850573, (Type_t *)L_4, /*hidden argument*/NULL);
		__this->set_hcp_12(((Il2CppObject*)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35))));
		SerializationInfo_t2185721892 * L_6 = (SerializationInfo_t2185721892 *)__this->get_serialization_info_13();
		NullCheck((SerializationInfo_t2185721892 *)L_6);
		int32_t L_7 = SerializationInfo_GetInt32_m4048035953((SerializationInfo_t2185721892 *)L_6, (String_t*)_stringLiteral212812367, /*hidden argument*/NULL);
		V_0 = (int32_t)L_7;
		SerializationInfo_t2185721892 * L_8 = (SerializationInfo_t2185721892 *)__this->get_serialization_info_13();
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 51)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2185721892 *)L_8);
		Il2CppObject * L_10 = SerializationInfo_GetValue_m4125471336((SerializationInfo_t2185721892 *)L_8, (String_t*)_stringLiteral961688967, (Type_t *)L_9, /*hidden argument*/NULL);
		V_1 = (KeyValuePair_2U5BU5D_t3614328797*)((KeyValuePair_2U5BU5D_t3614328797*)Castclass(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20)));
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) >= ((int32_t)((int32_t)10))))
		{
			goto IL_0083;
		}
	}
	{
		V_0 = (int32_t)((int32_t)10);
	}

IL_0083:
	{
		int32_t L_12 = V_0;
		NullCheck((Dictionary_2_t2021033010 *)__this);
		((  void (*) (Dictionary_2_t2021033010 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 39)->methodPointer)((Dictionary_2_t2021033010 *)__this, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 39));
		__this->set_count_10(0);
		KeyValuePair_2U5BU5D_t3614328797* L_13 = V_1;
		if (!L_13)
		{
			goto IL_00c9;
		}
	}
	{
		V_2 = (int32_t)0;
		goto IL_00c0;
	}

IL_009e:
	{
		KeyValuePair_2U5BU5D_t3614328797* L_14 = V_1;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		Il2CppObject * L_16 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1919813716 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_15)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		KeyValuePair_2U5BU5D_t3614328797* L_17 = V_1;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1919813716 *)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((Dictionary_2_t2021033010 *)__this);
		((  void (*) (Dictionary_2_t2021033010 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t2021033010 *)__this, (Il2CppObject *)L_16, (int32_t)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		int32_t L_20 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_00c0:
	{
		int32_t L_21 = V_2;
		KeyValuePair_2U5BU5D_t3614328797* L_22 = V_1;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_22)->max_length)))))))
		{
			goto IL_009e;
		}
	}

IL_00c9:
	{
		int32_t L_23 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_23+(int32_t)1)));
		__this->set_serialization_info_13((SerializationInfo_t2185721892 *)NULL);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Remove(TKey)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* TextEditOp_t4145961110_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_Remove_m1092169064_MetadataUsageId;
extern "C"  bool Dictionary_2_Remove_m1092169064_gshared (Dictionary_2_t2021033010 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Remove_m1092169064_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Il2CppObject * V_4 = NULL;
	int32_t V_5 = 0;
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		Il2CppObject * L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t3230847821* L_6 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))));
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_2 = (int32_t)((int32_t)((int32_t)L_10-(int32_t)1));
		int32_t L_11 = V_2;
		if ((!(((uint32_t)L_11) == ((uint32_t)(-1)))))
		{
			goto IL_004e;
		}
	}
	{
		return (bool)0;
	}

IL_004e:
	{
		V_3 = (int32_t)(-1);
	}

IL_0050:
	{
		LinkU5BU5D_t375419643* L_12 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = (int32_t)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_HashCode_0();
		int32_t L_15 = V_0;
		if ((!(((uint32_t)L_14) == ((uint32_t)L_15))))
		{
			goto IL_0089;
		}
	}
	{
		Il2CppObject* L_16 = (Il2CppObject*)__this->get_hcp_12();
		ObjectU5BU5D_t1108656482* L_17 = (ObjectU5BU5D_t1108656482*)__this->get_keySlots_6();
		int32_t L_18 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		Il2CppObject * L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		Il2CppObject * L_21 = ___key0;
		NullCheck((Il2CppObject*)L_16);
		bool L_22 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_16, (Il2CppObject *)L_20, (Il2CppObject *)L_21);
		if (!L_22)
		{
			goto IL_0089;
		}
	}
	{
		goto IL_00a4;
	}

IL_0089:
	{
		int32_t L_23 = V_2;
		V_3 = (int32_t)L_23;
		LinkU5BU5D_t375419643* L_24 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_25 = V_2;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		int32_t L_26 = (int32_t)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_25)))->get_Next_1();
		V_2 = (int32_t)L_26;
		int32_t L_27 = V_2;
		if ((!(((uint32_t)L_27) == ((uint32_t)(-1)))))
		{
			goto IL_0050;
		}
	}

IL_00a4:
	{
		int32_t L_28 = V_2;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_00ad;
		}
	}
	{
		return (bool)0;
	}

IL_00ad:
	{
		int32_t L_29 = (int32_t)__this->get_count_10();
		__this->set_count_10(((int32_t)((int32_t)L_29-(int32_t)1)));
		int32_t L_30 = V_3;
		if ((!(((uint32_t)L_30) == ((uint32_t)(-1)))))
		{
			goto IL_00e2;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_31 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_32 = V_1;
		LinkU5BU5D_t375419643* L_33 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_34 = V_2;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, L_34);
		int32_t L_35 = (int32_t)((L_33)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_34)))->get_Next_1();
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(L_32), (int32_t)((int32_t)((int32_t)L_35+(int32_t)1)));
		goto IL_0104;
	}

IL_00e2:
	{
		LinkU5BU5D_t375419643* L_36 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_37 = V_3;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		LinkU5BU5D_t375419643* L_38 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_39 = V_2;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_39);
		int32_t L_40 = (int32_t)((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_39)))->get_Next_1();
		((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_37)))->set_Next_1(L_40);
	}

IL_0104:
	{
		LinkU5BU5D_t375419643* L_41 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_42 = V_2;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
		int32_t L_43 = (int32_t)__this->get_emptySlot_9();
		((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_42)))->set_Next_1(L_43);
		int32_t L_44 = V_2;
		__this->set_emptySlot_9(L_44);
		LinkU5BU5D_t375419643* L_45 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_46 = V_2;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, L_46);
		((L_45)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_46)))->set_HashCode_0(0);
		ObjectU5BU5D_t1108656482* L_47 = (ObjectU5BU5D_t1108656482*)__this->get_keySlots_6();
		int32_t L_48 = V_2;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_4));
		Il2CppObject * L_49 = V_4;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, L_48);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(L_48), (Il2CppObject *)L_49);
		TextEditOpU5BU5D_t2239804499* L_50 = (TextEditOpU5BU5D_t2239804499*)__this->get_valueSlots_7();
		int32_t L_51 = V_2;
		Initobj (TextEditOp_t4145961110_il2cpp_TypeInfo_var, (&V_5));
		int32_t L_52 = V_5;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, L_51);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(L_51), (int32_t)L_52);
		int32_t L_53 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_53+(int32_t)1)));
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::TryGetValue(TKey,TValue&)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* TextEditOp_t4145961110_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_TryGetValue_m402024161_MetadataUsageId;
extern "C"  bool Dictionary_2_TryGetValue_m402024161_gshared (Dictionary_2_t2021033010 * __this, Il2CppObject * ___key0, int32_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_TryGetValue_m402024161_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		Il2CppObject * L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		Int32U5BU5D_t3230847821* L_5 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_6 = V_0;
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))));
		int32_t L_9 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (int32_t)((int32_t)((int32_t)L_9-(int32_t)1));
		goto IL_00a2;
	}

IL_0048:
	{
		LinkU5BU5D_t375419643* L_10 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = (int32_t)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->get_HashCode_0();
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_0090;
		}
	}
	{
		Il2CppObject* L_14 = (Il2CppObject*)__this->get_hcp_12();
		ObjectU5BU5D_t1108656482* L_15 = (ObjectU5BU5D_t1108656482*)__this->get_keySlots_6();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		Il2CppObject * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		Il2CppObject * L_19 = ___key0;
		NullCheck((Il2CppObject*)L_14);
		bool L_20 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_14, (Il2CppObject *)L_18, (Il2CppObject *)L_19);
		if (!L_20)
		{
			goto IL_0090;
		}
	}
	{
		int32_t* L_21 = ___value1;
		TextEditOpU5BU5D_t2239804499* L_22 = (TextEditOpU5BU5D_t2239804499*)__this->get_valueSlots_7();
		int32_t L_23 = V_1;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		int32_t L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		(*(int32_t*)L_21) = L_25;
		return (bool)1;
	}

IL_0090:
	{
		LinkU5BU5D_t375419643* L_26 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_27 = V_1;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		int32_t L_28 = (int32_t)((L_26)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_27)))->get_Next_1();
		V_1 = (int32_t)L_28;
	}

IL_00a2:
	{
		int32_t L_29 = V_1;
		if ((!(((uint32_t)L_29) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		int32_t* L_30 = ___value1;
		Initobj (TextEditOp_t4145961110_il2cpp_TypeInfo_var, (&V_2));
		int32_t L_31 = V_2;
		(*(int32_t*)L_30) = L_31;
		return (bool)0;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Keys()
extern "C"  KeyCollection_t3647792461 * Dictionary_2_get_Keys_m3389666494_gshared (Dictionary_2_t2021033010 * __this, const MethodInfo* method)
{
	{
		KeyCollection_t3647792461 * L_0 = (KeyCollection_t3647792461 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 52));
		((  void (*) (KeyCollection_t3647792461 *, Dictionary_2_t2021033010 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 53)->methodPointer)(L_0, (Dictionary_2_t2021033010 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 53));
		return L_0;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Values()
extern "C"  ValueCollection_t721638723 * Dictionary_2_get_Values_m3764701374_gshared (Dictionary_2_t2021033010 * __this, const MethodInfo* method)
{
	{
		ValueCollection_t721638723 * L_0 = (ValueCollection_t721638723 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 54));
		((  void (*) (ValueCollection_t721638723 *, Dictionary_2_t2021033010 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 55)->methodPointer)(L_0, (Dictionary_2_t2021033010 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 55));
		return L_0;
	}
}
// TKey System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ToTKey(System.Object)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern Il2CppCodeGenString* _stringLiteral3927817532;
extern const uint32_t Dictionary_2_ToTKey_m2396055265_MetadataUsageId;
extern "C"  Il2CppObject * Dictionary_2_ToTKey_m2396055265_gshared (Dictionary_2_t2021033010 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_ToTKey_m2396055265_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___key0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 56)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m138640077(NULL /*static, unused*/, (String_t*)_stringLiteral3927817532, (String_t*)L_4, /*hidden argument*/NULL);
		ArgumentException_t928607144 * L_6 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_6, (String_t*)L_5, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0040:
	{
		Il2CppObject * L_7 = ___key0;
		return ((Il2CppObject *)Castclass(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10)));
	}
}
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ToTValue(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TextEditOp_t4145961110_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3927817532;
extern Il2CppCodeGenString* _stringLiteral111972721;
extern const uint32_t Dictionary_2_ToTValue_m4184859873_MetadataUsageId;
extern "C"  int32_t Dictionary_2_ToTValue_m4184859873_gshared (Dictionary_2_t2021033010 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_ToTValue_m4184859873_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Il2CppObject * L_0 = ___value0;
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 57)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_1);
		bool L_2 = Type_get_IsValueType_m1914757235((Type_t *)L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		Initobj (TextEditOp_t4145961110_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		Il2CppObject * L_4 = ___value0;
		if (((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14))))
		{
			goto IL_0053;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 57)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m138640077(NULL /*static, unused*/, (String_t*)_stringLiteral3927817532, (String_t*)L_6, /*hidden argument*/NULL);
		ArgumentException_t928607144 * L_8 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_8, (String_t*)L_7, (String_t*)_stringLiteral111972721, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0053:
	{
		Il2CppObject * L_9 = ___value0;
		return ((*(int32_t*)((int32_t*)UnBox (L_9, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14)))));
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m2031819595_gshared (Dictionary_2_t2021033010 * __this, KeyValuePair_2_t1919813716  ___pair0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1919813716 *)(&___pair0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((Dictionary_2_t2021033010 *)__this);
		bool L_1 = ((  bool (*) (Dictionary_2_t2021033010 *, Il2CppObject *, int32_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 58)->methodPointer)((Dictionary_2_t2021033010 *)__this, (Il2CppObject *)L_0, (int32_t*)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 58));
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		return (bool)0;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 47));
		EqualityComparer_1_t3253797991 * L_2 = ((  EqualityComparer_1_t3253797991 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46));
		int32_t L_3 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1919813716 *)(&___pair0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		int32_t L_4 = V_0;
		NullCheck((EqualityComparer_1_t3253797991 *)L_2);
		bool L_5 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.TextEditor/TextEditOp>::Equals(T,T) */, (EqualityComparer_1_t3253797991 *)L_2, (int32_t)L_3, (int32_t)L_4);
		return L_5;
	}
}
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::GetEnumerator()
extern "C"  Enumerator_t3338356402  Dictionary_2_GetEnumerator_m646851452_gshared (Dictionary_2_t2021033010 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3338356402  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m1870154201(&L_0, (Dictionary_2_t2021033010 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 32));
		return L_0;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m2021542603_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		int32_t L_1 = ___value1;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), &L_2);
		DictionaryEntry_t1751606614  L_4;
		memset(&L_4, 0, sizeof(L_4));
		DictionaryEntry__ctor_m2600671860(&L_4, (Il2CppObject *)L_0, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m2622194143_gshared (Dictionary_2_t1738881263 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t1738881263 *)__this);
		((  void (*) (Dictionary_2_t1738881263 *, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1738881263 *)__this, (int32_t)((int32_t)10), (Il2CppObject*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m194033366_gshared (Dictionary_2_t1738881263 * __this, Il2CppObject* ___comparer0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___comparer0;
		NullCheck((Dictionary_2_t1738881263 *)__this);
		((  void (*) (Dictionary_2_t1738881263 *, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1738881263 *)__this, (int32_t)((int32_t)10), (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m1735792921_gshared (Dictionary_2_t1738881263 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___dictionary0;
		NullCheck((Dictionary_2_t1738881263 *)__this);
		((  void (*) (Dictionary_2_t1738881263 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Dictionary_2_t1738881263 *)__this, (Il2CppObject*)L_0, (Il2CppObject*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m1690460336_gshared (Dictionary_2_t1738881263 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		NullCheck((Dictionary_2_t1738881263 *)__this);
		((  void (*) (Dictionary_2_t1738881263 *, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1738881263 *)__this, (int32_t)L_0, (Il2CppObject*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral447049878;
extern const uint32_t Dictionary_2__ctor_m1413670020_MetadataUsageId;
extern "C"  void Dictionary_2__ctor_m1413670020_gshared (Dictionary_2_t1738881263 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2__ctor_m1413670020_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t1637661969  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Il2CppObject* V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral447049878, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Il2CppObject* L_2 = ___dictionary0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_3 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_2);
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		Il2CppObject* L_5 = ___comparer1;
		NullCheck((Dictionary_2_t1738881263 *)__this);
		((  void (*) (Dictionary_2_t1738881263 *, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1738881263 *)__this, (int32_t)L_4, (Il2CppObject*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Il2CppObject* L_6 = ___dictionary0;
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject* L_7 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_6);
		V_2 = (Il2CppObject*)L_7;
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004d;
		}

IL_0032:
		{
			Il2CppObject* L_8 = V_2;
			NullCheck((Il2CppObject*)L_8);
			KeyValuePair_2_t1637661969  L_9 = InterfaceFuncInvoker0< KeyValuePair_2_t1637661969  >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_8);
			V_1 = (KeyValuePair_2_t1637661969 )L_9;
			uint16_t L_10 = ((  uint16_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1637661969 *)(&V_1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			Il2CppObject * L_11 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1637661969 *)(&V_1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			NullCheck((Dictionary_2_t1738881263 *)__this);
			((  void (*) (Dictionary_2_t1738881263 *, uint16_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t1738881263 *)__this, (uint16_t)L_10, (Il2CppObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		}

IL_004d:
		{
			Il2CppObject* L_12 = V_2;
			NullCheck((Il2CppObject *)L_12);
			bool L_13 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
			if (L_13)
			{
				goto IL_0032;
			}
		}

IL_0058:
		{
			IL2CPP_LEAVE(0x68, FINALLY_005d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_005d;
	}

FINALLY_005d:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_14 = V_2;
			if (L_14)
			{
				goto IL_0061;
			}
		}

IL_0060:
		{
			IL2CPP_END_FINALLY(93)
		}

IL_0061:
		{
			Il2CppObject* L_15 = V_2;
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			IL2CPP_END_FINALLY(93)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(93)
	{
		IL2CPP_JUMP_TBL(0x68, IL_0068)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0068:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m95770272_gshared (Dictionary_2_t1738881263 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		SerializationInfo_t2185721892 * L_0 = ___info0;
		__this->set_serialization_info_13(L_0);
		return;
	}
}
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1185267343_gshared (Dictionary_2_t1738881263 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t1738881263 *)__this);
		KeyCollection_t3365640714 * L_0 = ((  KeyCollection_t3365640714 * (*) (Dictionary_2_t1738881263 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t1738881263 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_0;
	}
}
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m787470863_gshared (Dictionary_2_t1738881263 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t1738881263 *)__this);
		ValueCollection_t439486976 * L_0 = ((  ValueCollection_t439486976 * (*) (Dictionary_2_t1738881263 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t1738881263 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m2625333733_gshared (Dictionary_2_t1738881263 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t1738881263 *)__this);
		KeyCollection_t3365640714 * L_0 = ((  KeyCollection_t3365640714 * (*) (Dictionary_2_t1738881263 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t1738881263 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_0;
	}
}
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m4167469459_gshared (Dictionary_2_t1738881263 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t1738881263 *)__this);
		ValueCollection_t439486976 * L_0 = ((  ValueCollection_t439486976 * (*) (Dictionary_2_t1738881263 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t1738881263 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1118832994_gshared (Dictionary_2_t1738881263 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3538141527_gshared (Dictionary_2_t1738881263 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m3656782537_gshared (Dictionary_2_t1738881263 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		if (!((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))
		{
			goto IL_002f;
		}
	}
	{
		Il2CppObject * L_1 = ___key0;
		NullCheck((Dictionary_2_t1738881263 *)__this);
		bool L_2 = ((  bool (*) (Dictionary_2_t1738881263 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Dictionary_2_t1738881263 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Il2CppObject * L_3 = ___key0;
		NullCheck((Dictionary_2_t1738881263 *)__this);
		uint16_t L_4 = ((  uint16_t (*) (Dictionary_2_t1738881263 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1738881263 *)__this, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((Dictionary_2_t1738881263 *)__this);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (Dictionary_2_t1738881263 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((Dictionary_2_t1738881263 *)__this, (uint16_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_5;
	}

IL_002f:
	{
		return NULL;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m1627861934_gshared (Dictionary_2_t1738881263 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		NullCheck((Dictionary_2_t1738881263 *)__this);
		uint16_t L_1 = ((  uint16_t (*) (Dictionary_2_t1738881263 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1738881263 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		Il2CppObject * L_2 = ___value1;
		NullCheck((Dictionary_2_t1738881263 *)__this);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Dictionary_2_t1738881263 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((Dictionary_2_t1738881263 *)__this, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		NullCheck((Dictionary_2_t1738881263 *)__this);
		((  void (*) (Dictionary_2_t1738881263 *, uint16_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((Dictionary_2_t1738881263 *)__this, (uint16_t)L_1, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m1316812259_gshared (Dictionary_2_t1738881263 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		NullCheck((Dictionary_2_t1738881263 *)__this);
		uint16_t L_1 = ((  uint16_t (*) (Dictionary_2_t1738881263 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1738881263 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		Il2CppObject * L_2 = ___value1;
		NullCheck((Dictionary_2_t1738881263 *)__this);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Dictionary_2_t1738881263 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((Dictionary_2_t1738881263 *)__this, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		NullCheck((Dictionary_2_t1738881263 *)__this);
		((  void (*) (Dictionary_2_t1738881263 *, uint16_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t1738881263 *)__this, (uint16_t)L_1, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_System_Collections_IDictionary_Contains_m3888895923_MetadataUsageId;
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m3888895923_gshared (Dictionary_2_t1738881263 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_System_Collections_IDictionary_Contains_m3888895923_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___key0;
		if (!((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject * L_3 = ___key0;
		NullCheck((Dictionary_2_t1738881263 *)__this);
		bool L_4 = ((  bool (*) (Dictionary_2_t1738881263 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Dictionary_2_t1738881263 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_4;
	}

IL_0029:
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_System_Collections_IDictionary_Remove_m3200436716_MetadataUsageId;
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m3200436716_gshared (Dictionary_2_t1738881263 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_System_Collections_IDictionary_Remove_m3200436716_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___key0;
		if (!((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject * L_3 = ___key0;
		NullCheck((Dictionary_2_t1738881263 *)__this);
		((  bool (*) (Dictionary_2_t1738881263 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((Dictionary_2_t1738881263 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1719479425_gshared (Dictionary_2_t1738881263 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1000701613_gshared (Dictionary_2_t1738881263 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1374572933_gshared (Dictionary_2_t1738881263 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m511676546_gshared (Dictionary_2_t1738881263 * __this, KeyValuePair_2_t1637661969  ___keyValuePair0, const MethodInfo* method)
{
	{
		uint16_t L_0 = ((  uint16_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1637661969 *)(&___keyValuePair0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1637661969 *)(&___keyValuePair0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((Dictionary_2_t1738881263 *)__this);
		((  void (*) (Dictionary_2_t1738881263 *, uint16_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t1738881263 *)__this, (uint16_t)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2378191872_gshared (Dictionary_2_t1738881263 * __this, KeyValuePair_2_t1637661969  ___keyValuePair0, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1637661969  L_0 = ___keyValuePair0;
		NullCheck((Dictionary_2_t1738881263 *)__this);
		bool L_1 = ((  bool (*) (Dictionary_2_t1738881263 *, KeyValuePair_2_t1637661969 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((Dictionary_2_t1738881263 *)__this, (KeyValuePair_2_t1637661969 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m828253158_gshared (Dictionary_2_t1738881263 * __this, KeyValuePair_2U5BU5D_t794066828* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t794066828* L_0 = ___array0;
		int32_t L_1 = ___index1;
		NullCheck((Dictionary_2_t1738881263 *)__this);
		((  void (*) (Dictionary_2_t1738881263 *, KeyValuePair_2U5BU5D_t794066828*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((Dictionary_2_t1738881263 *)__this, (KeyValuePair_2U5BU5D_t794066828*)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3713235749_gshared (Dictionary_2_t1738881263 * __this, KeyValuePair_2_t1637661969  ___keyValuePair0, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1637661969  L_0 = ___keyValuePair0;
		NullCheck((Dictionary_2_t1738881263 *)__this);
		bool L_1 = ((  bool (*) (Dictionary_2_t1738881263 *, KeyValuePair_2_t1637661969 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((Dictionary_2_t1738881263 *)__this, (KeyValuePair_2_t1637661969 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (bool)0;
	}

IL_000e:
	{
		uint16_t L_2 = ((  uint16_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1637661969 *)(&___keyValuePair0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((Dictionary_2_t1738881263 *)__this);
		bool L_3 = ((  bool (*) (Dictionary_2_t1738881263 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((Dictionary_2_t1738881263 *)__this, (uint16_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		return L_3;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* DictionaryEntryU5BU5D_t479206547_il2cpp_TypeInfo_var;
extern const uint32_t Dictionary_2_System_Collections_ICollection_CopyTo_m2962219077_MetadataUsageId;
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m2962219077_gshared (Dictionary_2_t1738881263 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_System_Collections_ICollection_CopyTo_m2962219077_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2U5BU5D_t794066828* V_0 = NULL;
	DictionaryEntryU5BU5D_t479206547* V_1 = NULL;
	int32_t G_B5_0 = 0;
	DictionaryEntryU5BU5D_t479206547* G_B5_1 = NULL;
	Dictionary_2_t1738881263 * G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	DictionaryEntryU5BU5D_t479206547* G_B4_1 = NULL;
	Dictionary_2_t1738881263 * G_B4_2 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (KeyValuePair_2U5BU5D_t794066828*)((KeyValuePair_2U5BU5D_t794066828*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20)));
		KeyValuePair_2U5BU5D_t794066828* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		KeyValuePair_2U5BU5D_t794066828* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((Dictionary_2_t1738881263 *)__this);
		((  void (*) (Dictionary_2_t1738881263 *, KeyValuePair_2U5BU5D_t794066828*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((Dictionary_2_t1738881263 *)__this, (KeyValuePair_2U5BU5D_t794066828*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}

IL_0016:
	{
		Il2CppArray * L_4 = ___array0;
		int32_t L_5 = ___index1;
		NullCheck((Dictionary_2_t1738881263 *)__this);
		((  void (*) (Dictionary_2_t1738881263 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((Dictionary_2_t1738881263 *)__this, (Il2CppArray *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		Il2CppArray * L_6 = ___array0;
		V_1 = (DictionaryEntryU5BU5D_t479206547*)((DictionaryEntryU5BU5D_t479206547*)IsInst(L_6, DictionaryEntryU5BU5D_t479206547_il2cpp_TypeInfo_var));
		DictionaryEntryU5BU5D_t479206547* L_7 = V_1;
		if (!L_7)
		{
			goto IL_0051;
		}
	}
	{
		DictionaryEntryU5BU5D_t479206547* L_8 = V_1;
		int32_t L_9 = ___index1;
		Transform_1_t3432882628 * L_10 = ((Dictionary_2_t1738881263_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22)->static_fields)->get_U3CU3Ef__amU24cacheB_15();
		G_B4_0 = L_9;
		G_B4_1 = L_8;
		G_B4_2 = ((Dictionary_2_t1738881263 *)(__this));
		if (L_10)
		{
			G_B5_0 = L_9;
			G_B5_1 = L_8;
			G_B5_2 = ((Dictionary_2_t1738881263 *)(__this));
			goto IL_0046;
		}
	}
	{
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		Transform_1_t3432882628 * L_12 = (Transform_1_t3432882628 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 24));
		((  void (*) (Transform_1_t3432882628 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)(L_12, (Il2CppObject *)NULL, (IntPtr_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		((Dictionary_2_t1738881263_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22)->static_fields)->set_U3CU3Ef__amU24cacheB_15(L_12);
		G_B5_0 = G_B4_0;
		G_B5_1 = G_B4_1;
		G_B5_2 = ((Dictionary_2_t1738881263 *)(G_B4_2));
	}

IL_0046:
	{
		Transform_1_t3432882628 * L_13 = ((Dictionary_2_t1738881263_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22)->static_fields)->get_U3CU3Ef__amU24cacheB_15();
		NullCheck((Dictionary_2_t1738881263 *)G_B5_2);
		((  void (*) (Dictionary_2_t1738881263 *, DictionaryEntryU5BU5D_t479206547*, int32_t, Transform_1_t3432882628 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)((Dictionary_2_t1738881263 *)G_B5_2, (DictionaryEntryU5BU5D_t479206547*)G_B5_1, (int32_t)G_B5_0, (Transform_1_t3432882628 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		return;
	}

IL_0051:
	{
		Il2CppArray * L_14 = ___array0;
		int32_t L_15 = ___index1;
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		Transform_1_t3318937983 * L_17 = (Transform_1_t3318937983 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 28));
		((  void (*) (Transform_1_t3318937983 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)(L_17, (Il2CppObject *)NULL, (IntPtr_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		NullCheck((Dictionary_2_t1738881263 *)__this);
		((  void (*) (Dictionary_2_t1738881263 *, Il2CppArray *, int32_t, Transform_1_t3318937983 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((Dictionary_2_t1738881263 *)__this, (Il2CppArray *)L_14, (int32_t)L_15, (Transform_1_t3318937983 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m862389632_gshared (Dictionary_2_t1738881263 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3056204655  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m3840966209(&L_0, (Dictionary_2_t1738881263 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 32));
		Enumerator_t3056204655  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 31), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1435845245_gshared (Dictionary_2_t1738881263 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3056204655  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m3840966209(&L_0, (Dictionary_2_t1738881263 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 32));
		Enumerator_t3056204655  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 31), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2437376280_gshared (Dictionary_2_t1738881263 * __this, const MethodInfo* method)
{
	{
		ShimEnumerator_t1454659290 * L_0 = (ShimEnumerator_t1454659290 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 33));
		((  void (*) (ShimEnumerator_t1454659290 *, Dictionary_2_t1738881263 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34)->methodPointer)(L_0, (Dictionary_2_t1738881263 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m2775695815_gshared (Dictionary_2_t1738881263 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_count_10();
		return L_0;
	}
}
// TValue System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::get_Item(TKey)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* KeyNotFoundException_t876339989_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_get_Item_m1147636036_MetadataUsageId;
extern "C"  Il2CppObject * Dictionary_2_get_Item_m1147636036_gshared (Dictionary_2_t1738881263 * __this, uint16_t ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_get_Item_m1147636036_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		goto IL_0016;
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		uint16_t L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, uint16_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.UInt16>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (uint16_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		Int32U5BU5D_t3230847821* L_5 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_6 = V_0;
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))));
		int32_t L_9 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (int32_t)((int32_t)((int32_t)L_9-(int32_t)1));
		goto IL_009b;
	}

IL_0048:
	{
		LinkU5BU5D_t375419643* L_10 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = (int32_t)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->get_HashCode_0();
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_0089;
		}
	}
	{
		Il2CppObject* L_14 = (Il2CppObject*)__this->get_hcp_12();
		UInt16U5BU5D_t801649474* L_15 = (UInt16U5BU5D_t801649474*)__this->get_keySlots_6();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		uint16_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		uint16_t L_19 = ___key0;
		NullCheck((Il2CppObject*)L_14);
		bool L_20 = InterfaceFuncInvoker2< bool, uint16_t, uint16_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.UInt16>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_14, (uint16_t)L_18, (uint16_t)L_19);
		if (!L_20)
		{
			goto IL_0089;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_21 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		int32_t L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = L_22;
		Il2CppObject * L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		return L_24;
	}

IL_0089:
	{
		LinkU5BU5D_t375419643* L_25 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_26 = V_1;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		int32_t L_27 = (int32_t)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))->get_Next_1();
		V_1 = (int32_t)L_27;
	}

IL_009b:
	{
		int32_t L_28 = V_1;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		KeyNotFoundException_t876339989 * L_29 = (KeyNotFoundException_t876339989 *)il2cpp_codegen_object_new(KeyNotFoundException_t876339989_il2cpp_TypeInfo_var);
		KeyNotFoundException__ctor_m3542895460(L_29, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_29);
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::set_Item(TKey,TValue)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_set_Item_m1940620639_MetadataUsageId;
extern "C"  void Dictionary_2_set_Item_m1940620639_gshared (Dictionary_2_t1738881263 * __this, uint16_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_set_Item_m1940620639_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		goto IL_0016;
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		uint16_t L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, uint16_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.UInt16>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (uint16_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t3230847821* L_6 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))));
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_2 = (int32_t)((int32_t)((int32_t)L_10-(int32_t)1));
		V_3 = (int32_t)(-1);
		int32_t L_11 = V_2;
		if ((((int32_t)L_11) == ((int32_t)(-1))))
		{
			goto IL_00a2;
		}
	}

IL_004e:
	{
		LinkU5BU5D_t375419643* L_12 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = (int32_t)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_HashCode_0();
		int32_t L_15 = V_0;
		if ((!(((uint32_t)L_14) == ((uint32_t)L_15))))
		{
			goto IL_0087;
		}
	}
	{
		Il2CppObject* L_16 = (Il2CppObject*)__this->get_hcp_12();
		UInt16U5BU5D_t801649474* L_17 = (UInt16U5BU5D_t801649474*)__this->get_keySlots_6();
		int32_t L_18 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		uint16_t L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		uint16_t L_21 = ___key0;
		NullCheck((Il2CppObject*)L_16);
		bool L_22 = InterfaceFuncInvoker2< bool, uint16_t, uint16_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.UInt16>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_16, (uint16_t)L_20, (uint16_t)L_21);
		if (!L_22)
		{
			goto IL_0087;
		}
	}
	{
		goto IL_00a2;
	}

IL_0087:
	{
		int32_t L_23 = V_2;
		V_3 = (int32_t)L_23;
		LinkU5BU5D_t375419643* L_24 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_25 = V_2;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		int32_t L_26 = (int32_t)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_25)))->get_Next_1();
		V_2 = (int32_t)L_26;
		int32_t L_27 = V_2;
		if ((!(((uint32_t)L_27) == ((uint32_t)(-1)))))
		{
			goto IL_004e;
		}
	}

IL_00a2:
	{
		int32_t L_28 = V_2;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_0166;
		}
	}
	{
		int32_t L_29 = (int32_t)__this->get_count_10();
		int32_t L_30 = (int32_t)((int32_t)((int32_t)L_29+(int32_t)1));
		V_4 = (int32_t)L_30;
		__this->set_count_10(L_30);
		int32_t L_31 = V_4;
		int32_t L_32 = (int32_t)__this->get_threshold_11();
		if ((((int32_t)L_31) <= ((int32_t)L_32)))
		{
			goto IL_00de;
		}
	}
	{
		NullCheck((Dictionary_2_t1738881263 *)__this);
		((  void (*) (Dictionary_2_t1738881263 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36)->methodPointer)((Dictionary_2_t1738881263 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36));
		int32_t L_33 = V_0;
		Int32U5BU5D_t3230847821* L_34 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_34);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_33&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_34)->max_length))))));
	}

IL_00de:
	{
		int32_t L_35 = (int32_t)__this->get_emptySlot_9();
		V_2 = (int32_t)L_35;
		int32_t L_36 = V_2;
		if ((!(((uint32_t)L_36) == ((uint32_t)(-1)))))
		{
			goto IL_0105;
		}
	}
	{
		int32_t L_37 = (int32_t)__this->get_touchedSlots_8();
		int32_t L_38 = (int32_t)L_37;
		V_4 = (int32_t)L_38;
		__this->set_touchedSlots_8(((int32_t)((int32_t)L_38+(int32_t)1)));
		int32_t L_39 = V_4;
		V_2 = (int32_t)L_39;
		goto IL_011c;
	}

IL_0105:
	{
		LinkU5BU5D_t375419643* L_40 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_41 = V_2;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, L_41);
		int32_t L_42 = (int32_t)((L_40)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_41)))->get_Next_1();
		__this->set_emptySlot_9(L_42);
	}

IL_011c:
	{
		LinkU5BU5D_t375419643* L_43 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_44 = V_2;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		Int32U5BU5D_t3230847821* L_45 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_46 = V_1;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, L_46);
		int32_t L_47 = L_46;
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_44)))->set_Next_1(((int32_t)((int32_t)L_48-(int32_t)1)));
		Int32U5BU5D_t3230847821* L_49 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_50 = V_1;
		int32_t L_51 = V_2;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, L_50);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(L_50), (int32_t)((int32_t)((int32_t)L_51+(int32_t)1)));
		LinkU5BU5D_t375419643* L_52 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_53 = V_2;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, L_53);
		int32_t L_54 = V_0;
		((L_52)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_53)))->set_HashCode_0(L_54);
		UInt16U5BU5D_t801649474* L_55 = (UInt16U5BU5D_t801649474*)__this->get_keySlots_6();
		int32_t L_56 = V_2;
		uint16_t L_57 = ___key0;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, L_56);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(L_56), (uint16_t)L_57);
		goto IL_01b5;
	}

IL_0166:
	{
		int32_t L_58 = V_3;
		if ((((int32_t)L_58) == ((int32_t)(-1))))
		{
			goto IL_01b5;
		}
	}
	{
		LinkU5BU5D_t375419643* L_59 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_60 = V_3;
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, L_60);
		LinkU5BU5D_t375419643* L_61 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_62 = V_2;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, L_62);
		int32_t L_63 = (int32_t)((L_61)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_62)))->get_Next_1();
		((L_59)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_60)))->set_Next_1(L_63);
		LinkU5BU5D_t375419643* L_64 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_65 = V_2;
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, L_65);
		Int32U5BU5D_t3230847821* L_66 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_67 = V_1;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, L_67);
		int32_t L_68 = L_67;
		int32_t L_69 = (L_66)->GetAt(static_cast<il2cpp_array_size_t>(L_68));
		((L_64)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_65)))->set_Next_1(((int32_t)((int32_t)L_69-(int32_t)1)));
		Int32U5BU5D_t3230847821* L_70 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_71 = V_1;
		int32_t L_72 = V_2;
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, L_71);
		(L_70)->SetAt(static_cast<il2cpp_array_size_t>(L_71), (int32_t)((int32_t)((int32_t)L_72+(int32_t)1)));
	}

IL_01b5:
	{
		ObjectU5BU5D_t1108656482* L_73 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		int32_t L_74 = V_2;
		Il2CppObject * L_75 = ___value1;
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, L_74);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(L_74), (Il2CppObject *)L_75);
		int32_t L_76 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_76+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4227142842;
extern const uint32_t Dictionary_2_Init_m1572987223_MetadataUsageId;
extern "C"  void Dictionary_2_Init_m1572987223_gshared (Dictionary_2_t1738881263 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Init_m1572987223_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Dictionary_2_t1738881263 * G_B4_0 = NULL;
	Dictionary_2_t1738881263 * G_B3_0 = NULL;
	Il2CppObject* G_B5_0 = NULL;
	Dictionary_2_t1738881263 * G_B5_1 = NULL;
	{
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_1 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_1, (String_t*)_stringLiteral4227142842, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Il2CppObject* L_2 = ___hcp1;
		G_B3_0 = ((Dictionary_2_t1738881263 *)(__this));
		if (!L_2)
		{
			G_B4_0 = ((Dictionary_2_t1738881263 *)(__this));
			goto IL_0021;
		}
	}
	{
		Il2CppObject* L_3 = ___hcp1;
		V_0 = (Il2CppObject*)L_3;
		Il2CppObject* L_4 = V_0;
		G_B5_0 = L_4;
		G_B5_1 = ((Dictionary_2_t1738881263 *)(G_B3_0));
		goto IL_0026;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 38));
		EqualityComparer_1_t3427472100 * L_5 = ((  EqualityComparer_1_t3427472100 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 37)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 37));
		G_B5_0 = ((Il2CppObject*)(L_5));
		G_B5_1 = ((Dictionary_2_t1738881263 *)(G_B4_0));
	}

IL_0026:
	{
		NullCheck(G_B5_1);
		G_B5_1->set_hcp_12(G_B5_0);
		int32_t L_6 = ___capacity0;
		if (L_6)
		{
			goto IL_0035;
		}
	}
	{
		___capacity0 = (int32_t)((int32_t)10);
	}

IL_0035:
	{
		int32_t L_7 = ___capacity0;
		___capacity0 = (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)((float)((float)(((float)((float)L_7)))/(float)(0.9f))))))+(int32_t)1));
		int32_t L_8 = ___capacity0;
		NullCheck((Dictionary_2_t1738881263 *)__this);
		((  void (*) (Dictionary_2_t1738881263 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 39)->methodPointer)((Dictionary_2_t1738881263 *)__this, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 39));
		__this->set_generation_14(0);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::InitArrays(System.Int32)
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern Il2CppClass* LinkU5BU5D_t375419643_il2cpp_TypeInfo_var;
extern const uint32_t Dictionary_2_InitArrays_m1737798464_MetadataUsageId;
extern "C"  void Dictionary_2_InitArrays_m1737798464_gshared (Dictionary_2_t1738881263 * __this, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_InitArrays_m1737798464_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___size0;
		__this->set_table_4(((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)L_0)));
		int32_t L_1 = ___size0;
		__this->set_linkSlots_5(((LinkU5BU5D_t375419643*)SZArrayNew(LinkU5BU5D_t375419643_il2cpp_TypeInfo_var, (uint32_t)L_1)));
		__this->set_emptySlot_9((-1));
		int32_t L_2 = ___size0;
		__this->set_keySlots_6(((UInt16U5BU5D_t801649474*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 40), (uint32_t)L_2)));
		int32_t L_3 = ___size0;
		__this->set_valueSlots_7(((ObjectU5BU5D_t1108656482*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 41), (uint32_t)L_3)));
		__this->set_touchedSlots_8(0);
		Int32U5BU5D_t3230847821* L_4 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_4);
		__this->set_threshold_11((((int32_t)((int32_t)((float)((float)(((float)((float)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length)))))))*(float)(0.9f)))))));
		int32_t L_5 = (int32_t)__this->get_threshold_11();
		if (L_5)
		{
			goto IL_0074;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_6 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_6);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0074;
		}
	}
	{
		__this->set_threshold_11(1);
	}

IL_0074:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::CopyToCheck(System.Array,System.Int32)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern Il2CppCodeGenString* _stringLiteral1142730282;
extern Il2CppCodeGenString* _stringLiteral2802514764;
extern const uint32_t Dictionary_2_CopyToCheck_m1111285564_MetadataUsageId;
extern "C"  void Dictionary_2_CopyToCheck_m1111285564_gshared (Dictionary_2_t1738881263 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_CopyToCheck_m1111285564_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppArray * L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		int32_t L_4 = ___index1;
		Il2CppArray * L_5 = ___array0;
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)L_6)))
		{
			goto IL_003a;
		}
	}
	{
		ArgumentException_t928607144 * L_7 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_7, (String_t*)_stringLiteral1142730282, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003a:
	{
		Il2CppArray * L_8 = ___array0;
		NullCheck((Il2CppArray *)L_8);
		int32_t L_9 = Array_get_Length_m1203127607((Il2CppArray *)L_8, /*hidden argument*/NULL);
		int32_t L_10 = ___index1;
		NullCheck((Dictionary_2_t1738881263 *)__this);
		int32_t L_11 = ((  int32_t (*) (Dictionary_2_t1738881263 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 42)->methodPointer)((Dictionary_2_t1738881263 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 42));
		if ((((int32_t)((int32_t)((int32_t)L_9-(int32_t)L_10))) >= ((int32_t)L_11)))
		{
			goto IL_0058;
		}
	}
	{
		ArgumentException_t928607144 * L_12 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_12, (String_t*)_stringLiteral2802514764, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0058:
	{
		return;
	}
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t1637661969  Dictionary_2_make_pair_m2252842632_gshared (Il2CppObject * __this /* static, unused */, uint16_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___key0;
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_t1637661969  L_2;
		memset(&L_2, 0, sizeof(L_2));
		KeyValuePair_2__ctor_m2617982879(&L_2, (uint16_t)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 44));
		return L_2;
	}
}
// TKey System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::pick_key(TKey,TValue)
extern "C"  uint16_t Dictionary_2_pick_key_m3221822574_gshared (Il2CppObject * __this /* static, unused */, uint16_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___key0;
		return L_0;
	}
}
// TValue System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m140151726_gshared (Il2CppObject * __this /* static, unused */, uint16_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value1;
		return L_0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m1811187987_gshared (Dictionary_2_t1738881263 * __this, KeyValuePair_2U5BU5D_t794066828* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t794066828* L_0 = ___array0;
		int32_t L_1 = ___index1;
		NullCheck((Dictionary_2_t1738881263 *)__this);
		((  void (*) (Dictionary_2_t1738881263 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((Dictionary_2_t1738881263 *)__this, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		KeyValuePair_2U5BU5D_t794066828* L_2 = ___array0;
		int32_t L_3 = ___index1;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		Transform_1_t3318937983 * L_5 = (Transform_1_t3318937983 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 28));
		((  void (*) (Transform_1_t3318937983 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)(L_5, (Il2CppObject *)NULL, (IntPtr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		NullCheck((Dictionary_2_t1738881263 *)__this);
		((  void (*) (Dictionary_2_t1738881263 *, KeyValuePair_2U5BU5D_t794066828*, int32_t, Transform_1_t3318937983 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 45)->methodPointer)((Dictionary_2_t1738881263 *)__this, (KeyValuePair_2U5BU5D_t794066828*)L_2, (int32_t)L_3, (Transform_1_t3318937983 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 45));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::Resize()
extern Il2CppClass* Hashtable_t1407064410_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern Il2CppClass* LinkU5BU5D_t375419643_il2cpp_TypeInfo_var;
extern const uint32_t Dictionary_2_Resize_m4036403769_MetadataUsageId;
extern "C"  void Dictionary_2_Resize_m4036403769_gshared (Dictionary_2_t1738881263 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Resize_m4036403769_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Int32U5BU5D_t3230847821* V_1 = NULL;
	LinkU5BU5D_t375419643* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	UInt16U5BU5D_t801649474* V_7 = NULL;
	ObjectU5BU5D_t1108656482* V_8 = NULL;
	int32_t V_9 = 0;
	{
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Hashtable_t1407064410_il2cpp_TypeInfo_var);
		int32_t L_1 = Hashtable_ToPrime_m840372929(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))<<(int32_t)1))|(int32_t)1)), /*hidden argument*/NULL);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		V_1 = (Int32U5BU5D_t3230847821*)((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)L_2));
		int32_t L_3 = V_0;
		V_2 = (LinkU5BU5D_t375419643*)((LinkU5BU5D_t375419643*)SZArrayNew(LinkU5BU5D_t375419643_il2cpp_TypeInfo_var, (uint32_t)L_3));
		V_3 = (int32_t)0;
		goto IL_00b1;
	}

IL_0027:
	{
		Int32U5BU5D_t3230847821* L_4 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_5 = V_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		int32_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_4 = (int32_t)((int32_t)((int32_t)L_7-(int32_t)1));
		goto IL_00a5;
	}

IL_0038:
	{
		LinkU5BU5D_t375419643* L_8 = V_2;
		int32_t L_9 = V_4;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		Il2CppObject* L_10 = (Il2CppObject*)__this->get_hcp_12();
		UInt16U5BU5D_t801649474* L_11 = (UInt16U5BU5D_t801649474*)__this->get_keySlots_6();
		int32_t L_12 = V_4;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		uint16_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Il2CppObject*)L_10);
		int32_t L_15 = InterfaceFuncInvoker1< int32_t, uint16_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.UInt16>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_10, (uint16_t)L_14);
		int32_t L_16 = (int32_t)((int32_t)((int32_t)L_15|(int32_t)((int32_t)-2147483648LL)));
		V_9 = (int32_t)L_16;
		((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9)))->set_HashCode_0(L_16);
		int32_t L_17 = V_9;
		V_5 = (int32_t)L_17;
		int32_t L_18 = V_5;
		int32_t L_19 = V_0;
		V_6 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_18&(int32_t)((int32_t)2147483647LL)))%(int32_t)L_19));
		LinkU5BU5D_t375419643* L_20 = V_2;
		int32_t L_21 = V_4;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		Int32U5BU5D_t3230847821* L_22 = V_1;
		int32_t L_23 = V_6;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		int32_t L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_21)))->set_Next_1(((int32_t)((int32_t)L_25-(int32_t)1)));
		Int32U5BU5D_t3230847821* L_26 = V_1;
		int32_t L_27 = V_6;
		int32_t L_28 = V_4;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(L_27), (int32_t)((int32_t)((int32_t)L_28+(int32_t)1)));
		LinkU5BU5D_t375419643* L_29 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_30 = V_4;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
		int32_t L_31 = (int32_t)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_30)))->get_Next_1();
		V_4 = (int32_t)L_31;
	}

IL_00a5:
	{
		int32_t L_32 = V_4;
		if ((!(((uint32_t)L_32) == ((uint32_t)(-1)))))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_33 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00b1:
	{
		int32_t L_34 = V_3;
		Int32U5BU5D_t3230847821* L_35 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_35);
		if ((((int32_t)L_34) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_35)->max_length)))))))
		{
			goto IL_0027;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_36 = V_1;
		__this->set_table_4(L_36);
		LinkU5BU5D_t375419643* L_37 = V_2;
		__this->set_linkSlots_5(L_37);
		int32_t L_38 = V_0;
		V_7 = (UInt16U5BU5D_t801649474*)((UInt16U5BU5D_t801649474*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 40), (uint32_t)L_38));
		int32_t L_39 = V_0;
		V_8 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 41), (uint32_t)L_39));
		UInt16U5BU5D_t801649474* L_40 = (UInt16U5BU5D_t801649474*)__this->get_keySlots_6();
		UInt16U5BU5D_t801649474* L_41 = V_7;
		int32_t L_42 = (int32_t)__this->get_touchedSlots_8();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_40, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_41, (int32_t)0, (int32_t)L_42, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_43 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		ObjectU5BU5D_t1108656482* L_44 = V_8;
		int32_t L_45 = (int32_t)__this->get_touchedSlots_8();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_43, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_44, (int32_t)0, (int32_t)L_45, /*hidden argument*/NULL);
		UInt16U5BU5D_t801649474* L_46 = V_7;
		__this->set_keySlots_6(L_46);
		ObjectU5BU5D_t1108656482* L_47 = V_8;
		__this->set_valueSlots_7(L_47);
		int32_t L_48 = V_0;
		__this->set_threshold_11((((int32_t)((int32_t)((float)((float)(((float)((float)L_48)))*(float)(0.9f)))))));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::Add(TKey,TValue)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern Il2CppCodeGenString* _stringLiteral628480033;
extern const uint32_t Dictionary_2_Add_m1108762038_MetadataUsageId;
extern "C"  void Dictionary_2_Add_m1108762038_gshared (Dictionary_2_t1738881263 * __this, uint16_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Add_m1108762038_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		goto IL_0016;
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		uint16_t L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, uint16_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.UInt16>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (uint16_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t3230847821* L_6 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))));
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_2 = (int32_t)((int32_t)((int32_t)L_10-(int32_t)1));
		goto IL_009b;
	}

IL_004a:
	{
		LinkU5BU5D_t375419643* L_11 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = (int32_t)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)))->get_HashCode_0();
		int32_t L_14 = V_0;
		if ((!(((uint32_t)L_13) == ((uint32_t)L_14))))
		{
			goto IL_0089;
		}
	}
	{
		Il2CppObject* L_15 = (Il2CppObject*)__this->get_hcp_12();
		UInt16U5BU5D_t801649474* L_16 = (UInt16U5BU5D_t801649474*)__this->get_keySlots_6();
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		uint16_t L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		uint16_t L_20 = ___key0;
		NullCheck((Il2CppObject*)L_15);
		bool L_21 = InterfaceFuncInvoker2< bool, uint16_t, uint16_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.UInt16>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_15, (uint16_t)L_19, (uint16_t)L_20);
		if (!L_21)
		{
			goto IL_0089;
		}
	}
	{
		ArgumentException_t928607144 * L_22 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_22, (String_t*)_stringLiteral628480033, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	}

IL_0089:
	{
		LinkU5BU5D_t375419643* L_23 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_24 = V_2;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = (int32_t)((L_23)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_24)))->get_Next_1();
		V_2 = (int32_t)L_25;
	}

IL_009b:
	{
		int32_t L_26 = V_2;
		if ((!(((uint32_t)L_26) == ((uint32_t)(-1)))))
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_27 = (int32_t)__this->get_count_10();
		int32_t L_28 = (int32_t)((int32_t)((int32_t)L_27+(int32_t)1));
		V_3 = (int32_t)L_28;
		__this->set_count_10(L_28);
		int32_t L_29 = V_3;
		int32_t L_30 = (int32_t)__this->get_threshold_11();
		if ((((int32_t)L_29) <= ((int32_t)L_30)))
		{
			goto IL_00d5;
		}
	}
	{
		NullCheck((Dictionary_2_t1738881263 *)__this);
		((  void (*) (Dictionary_2_t1738881263 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36)->methodPointer)((Dictionary_2_t1738881263 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36));
		int32_t L_31 = V_0;
		Int32U5BU5D_t3230847821* L_32 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_32);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_31&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_32)->max_length))))));
	}

IL_00d5:
	{
		int32_t L_33 = (int32_t)__this->get_emptySlot_9();
		V_2 = (int32_t)L_33;
		int32_t L_34 = V_2;
		if ((!(((uint32_t)L_34) == ((uint32_t)(-1)))))
		{
			goto IL_00fa;
		}
	}
	{
		int32_t L_35 = (int32_t)__this->get_touchedSlots_8();
		int32_t L_36 = (int32_t)L_35;
		V_3 = (int32_t)L_36;
		__this->set_touchedSlots_8(((int32_t)((int32_t)L_36+(int32_t)1)));
		int32_t L_37 = V_3;
		V_2 = (int32_t)L_37;
		goto IL_0111;
	}

IL_00fa:
	{
		LinkU5BU5D_t375419643* L_38 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_39 = V_2;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_39);
		int32_t L_40 = (int32_t)((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_39)))->get_Next_1();
		__this->set_emptySlot_9(L_40);
	}

IL_0111:
	{
		LinkU5BU5D_t375419643* L_41 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_42 = V_2;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
		int32_t L_43 = V_0;
		((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_42)))->set_HashCode_0(L_43);
		LinkU5BU5D_t375419643* L_44 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_45 = V_2;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, L_45);
		Int32U5BU5D_t3230847821* L_46 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_47 = V_1;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, L_47);
		int32_t L_48 = L_47;
		int32_t L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_45)))->set_Next_1(((int32_t)((int32_t)L_49-(int32_t)1)));
		Int32U5BU5D_t3230847821* L_50 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_51 = V_1;
		int32_t L_52 = V_2;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, L_51);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(L_51), (int32_t)((int32_t)((int32_t)L_52+(int32_t)1)));
		UInt16U5BU5D_t801649474* L_53 = (UInt16U5BU5D_t801649474*)__this->get_keySlots_6();
		int32_t L_54 = V_2;
		uint16_t L_55 = ___key0;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, L_54);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(L_54), (uint16_t)L_55);
		ObjectU5BU5D_t1108656482* L_56 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		int32_t L_57 = V_2;
		Il2CppObject * L_58 = ___value1;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, L_57);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(L_57), (Il2CppObject *)L_58);
		int32_t L_59 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_59+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m28327434_gshared (Dictionary_2_t1738881263 * __this, const MethodInfo* method)
{
	{
		__this->set_count_10(0);
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		Int32U5BU5D_t3230847821* L_1 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_1);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		UInt16U5BU5D_t801649474* L_2 = (UInt16U5BU5D_t801649474*)__this->get_keySlots_6();
		UInt16U5BU5D_t801649474* L_3 = (UInt16U5BU5D_t801649474*)__this->get_keySlots_6();
		NullCheck(L_3);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))), /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_4 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		ObjectU5BU5D_t1108656482* L_5 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		NullCheck(L_5);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_4, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))), /*hidden argument*/NULL);
		LinkU5BU5D_t375419643* L_6 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		LinkU5BU5D_t375419643* L_7 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		NullCheck(L_7);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))), /*hidden argument*/NULL);
		__this->set_emptySlot_9((-1));
		__this->set_touchedSlots_8(0);
		int32_t L_8 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_8+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::ContainsKey(TKey)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_ContainsKey_m740224624_MetadataUsageId;
extern "C"  bool Dictionary_2_ContainsKey_m740224624_gshared (Dictionary_2_t1738881263 * __this, uint16_t ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_ContainsKey_m740224624_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		goto IL_0016;
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		uint16_t L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, uint16_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.UInt16>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (uint16_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		Int32U5BU5D_t3230847821* L_5 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_6 = V_0;
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))));
		int32_t L_9 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (int32_t)((int32_t)((int32_t)L_9-(int32_t)1));
		goto IL_0090;
	}

IL_0048:
	{
		LinkU5BU5D_t375419643* L_10 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = (int32_t)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->get_HashCode_0();
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_007e;
		}
	}
	{
		Il2CppObject* L_14 = (Il2CppObject*)__this->get_hcp_12();
		UInt16U5BU5D_t801649474* L_15 = (UInt16U5BU5D_t801649474*)__this->get_keySlots_6();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		uint16_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		uint16_t L_19 = ___key0;
		NullCheck((Il2CppObject*)L_14);
		bool L_20 = InterfaceFuncInvoker2< bool, uint16_t, uint16_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.UInt16>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_14, (uint16_t)L_18, (uint16_t)L_19);
		if (!L_20)
		{
			goto IL_007e;
		}
	}
	{
		return (bool)1;
	}

IL_007e:
	{
		LinkU5BU5D_t375419643* L_21 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = (int32_t)((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_22)))->get_Next_1();
		V_1 = (int32_t)L_23;
	}

IL_0090:
	{
		int32_t L_24 = V_1;
		if ((!(((uint32_t)L_24) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m25004656_gshared (Dictionary_2_t1738881263 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 47));
		EqualityComparer_1_t3278653252 * L_0 = ((  EqualityComparer_1_t3278653252 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46));
		V_0 = (Il2CppObject*)L_0;
		V_1 = (int32_t)0;
		goto IL_0054;
	}

IL_000d:
	{
		Int32U5BU5D_t3230847821* L_1 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		int32_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_2 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		goto IL_0049;
	}

IL_001d:
	{
		Il2CppObject* L_5 = V_0;
		ObjectU5BU5D_t1108656482* L_6 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		int32_t L_7 = V_2;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		Il2CppObject * L_10 = ___value0;
		NullCheck((Il2CppObject*)L_5);
		bool L_11 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 48), (Il2CppObject*)L_5, (Il2CppObject *)L_9, (Il2CppObject *)L_10);
		if (!L_11)
		{
			goto IL_0037;
		}
	}
	{
		return (bool)1;
	}

IL_0037:
	{
		LinkU5BU5D_t375419643* L_12 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = (int32_t)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_Next_1();
		V_2 = (int32_t)L_14;
	}

IL_0049:
	{
		int32_t L_15 = V_2;
		if ((!(((uint32_t)L_15) == ((uint32_t)(-1)))))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_17 = V_1;
		Int32U5BU5D_t3230847821* L_18 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3237038;
extern Il2CppCodeGenString* _stringLiteral2016261304;
extern Il2CppCodeGenString* _stringLiteral3759850573;
extern Il2CppCodeGenString* _stringLiteral212812367;
extern Il2CppCodeGenString* _stringLiteral961688967;
extern const uint32_t Dictionary_2_GetObjectData_m2835742205_MetadataUsageId;
extern "C"  void Dictionary_2_GetObjectData_m2835742205_gshared (Dictionary_2_t1738881263 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_GetObjectData_m2835742205_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2U5BU5D_t794066828* V_0 = NULL;
	{
		SerializationInfo_t2185721892 * L_0 = ___info0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral3237038, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		SerializationInfo_t2185721892 * L_2 = ___info0;
		int32_t L_3 = (int32_t)__this->get_generation_14();
		NullCheck((SerializationInfo_t2185721892 *)L_2);
		SerializationInfo_AddValue_m2348540514((SerializationInfo_t2185721892 *)L_2, (String_t*)_stringLiteral2016261304, (int32_t)L_3, /*hidden argument*/NULL);
		SerializationInfo_t2185721892 * L_4 = ___info0;
		Il2CppObject* L_5 = (Il2CppObject*)__this->get_hcp_12();
		NullCheck((SerializationInfo_t2185721892 *)L_4);
		SerializationInfo_AddValue_m469120675((SerializationInfo_t2185721892 *)L_4, (String_t*)_stringLiteral3759850573, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		V_0 = (KeyValuePair_2U5BU5D_t794066828*)NULL;
		int32_t L_6 = (int32_t)__this->get_count_10();
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_7 = (int32_t)__this->get_count_10();
		V_0 = (KeyValuePair_2U5BU5D_t794066828*)((KeyValuePair_2U5BU5D_t794066828*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 49), (uint32_t)L_7));
		KeyValuePair_2U5BU5D_t794066828* L_8 = V_0;
		NullCheck((Dictionary_2_t1738881263 *)__this);
		((  void (*) (Dictionary_2_t1738881263 *, KeyValuePair_2U5BU5D_t794066828*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((Dictionary_2_t1738881263 *)__this, (KeyValuePair_2U5BU5D_t794066828*)L_8, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
	}

IL_0055:
	{
		SerializationInfo_t2185721892 * L_9 = ___info0;
		Int32U5BU5D_t3230847821* L_10 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_10);
		NullCheck((SerializationInfo_t2185721892 *)L_9);
		SerializationInfo_AddValue_m2348540514((SerializationInfo_t2185721892 *)L_9, (String_t*)_stringLiteral212812367, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))), /*hidden argument*/NULL);
		SerializationInfo_t2185721892 * L_11 = ___info0;
		KeyValuePair_2U5BU5D_t794066828* L_12 = V_0;
		NullCheck((SerializationInfo_t2185721892 *)L_11);
		SerializationInfo_AddValue_m469120675((SerializationInfo_t2185721892 *)L_11, (String_t*)_stringLiteral961688967, (Il2CppObject *)(Il2CppObject *)L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::OnDeserialization(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2016261304;
extern Il2CppCodeGenString* _stringLiteral3759850573;
extern Il2CppCodeGenString* _stringLiteral212812367;
extern Il2CppCodeGenString* _stringLiteral961688967;
extern const uint32_t Dictionary_2_OnDeserialization_m4182435975_MetadataUsageId;
extern "C"  void Dictionary_2_OnDeserialization_m4182435975_gshared (Dictionary_2_t1738881263 * __this, Il2CppObject * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_OnDeserialization_m4182435975_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2U5BU5D_t794066828* V_1 = NULL;
	int32_t V_2 = 0;
	{
		SerializationInfo_t2185721892 * L_0 = (SerializationInfo_t2185721892 *)__this->get_serialization_info_13();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		SerializationInfo_t2185721892 * L_1 = (SerializationInfo_t2185721892 *)__this->get_serialization_info_13();
		NullCheck((SerializationInfo_t2185721892 *)L_1);
		int32_t L_2 = SerializationInfo_GetInt32_m4048035953((SerializationInfo_t2185721892 *)L_1, (String_t*)_stringLiteral2016261304, /*hidden argument*/NULL);
		__this->set_generation_14(L_2);
		SerializationInfo_t2185721892 * L_3 = (SerializationInfo_t2185721892 *)__this->get_serialization_info_13();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 50)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2185721892 *)L_3);
		Il2CppObject * L_5 = SerializationInfo_GetValue_m4125471336((SerializationInfo_t2185721892 *)L_3, (String_t*)_stringLiteral3759850573, (Type_t *)L_4, /*hidden argument*/NULL);
		__this->set_hcp_12(((Il2CppObject*)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35))));
		SerializationInfo_t2185721892 * L_6 = (SerializationInfo_t2185721892 *)__this->get_serialization_info_13();
		NullCheck((SerializationInfo_t2185721892 *)L_6);
		int32_t L_7 = SerializationInfo_GetInt32_m4048035953((SerializationInfo_t2185721892 *)L_6, (String_t*)_stringLiteral212812367, /*hidden argument*/NULL);
		V_0 = (int32_t)L_7;
		SerializationInfo_t2185721892 * L_8 = (SerializationInfo_t2185721892 *)__this->get_serialization_info_13();
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 51)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2185721892 *)L_8);
		Il2CppObject * L_10 = SerializationInfo_GetValue_m4125471336((SerializationInfo_t2185721892 *)L_8, (String_t*)_stringLiteral961688967, (Type_t *)L_9, /*hidden argument*/NULL);
		V_1 = (KeyValuePair_2U5BU5D_t794066828*)((KeyValuePair_2U5BU5D_t794066828*)Castclass(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20)));
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) >= ((int32_t)((int32_t)10))))
		{
			goto IL_0083;
		}
	}
	{
		V_0 = (int32_t)((int32_t)10);
	}

IL_0083:
	{
		int32_t L_12 = V_0;
		NullCheck((Dictionary_2_t1738881263 *)__this);
		((  void (*) (Dictionary_2_t1738881263 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 39)->methodPointer)((Dictionary_2_t1738881263 *)__this, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 39));
		__this->set_count_10(0);
		KeyValuePair_2U5BU5D_t794066828* L_13 = V_1;
		if (!L_13)
		{
			goto IL_00c9;
		}
	}
	{
		V_2 = (int32_t)0;
		goto IL_00c0;
	}

IL_009e:
	{
		KeyValuePair_2U5BU5D_t794066828* L_14 = V_1;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		uint16_t L_16 = ((  uint16_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1637661969 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_15)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		KeyValuePair_2U5BU5D_t794066828* L_17 = V_1;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		Il2CppObject * L_19 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1637661969 *)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((Dictionary_2_t1738881263 *)__this);
		((  void (*) (Dictionary_2_t1738881263 *, uint16_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t1738881263 *)__this, (uint16_t)L_16, (Il2CppObject *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		int32_t L_20 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_00c0:
	{
		int32_t L_21 = V_2;
		KeyValuePair_2U5BU5D_t794066828* L_22 = V_1;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_22)->max_length)))))))
		{
			goto IL_009e;
		}
	}

IL_00c9:
	{
		int32_t L_23 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_23+(int32_t)1)));
		__this->set_serialization_info_13((SerializationInfo_t2185721892 *)NULL);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::Remove(TKey)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt16_t24667923_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_Remove_m2757887104_MetadataUsageId;
extern "C"  bool Dictionary_2_Remove_m2757887104_gshared (Dictionary_2_t1738881263 * __this, uint16_t ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Remove_m2757887104_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	uint16_t V_4 = 0;
	Il2CppObject * V_5 = NULL;
	{
		goto IL_0016;
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		uint16_t L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, uint16_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.UInt16>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (uint16_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t3230847821* L_6 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))));
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_2 = (int32_t)((int32_t)((int32_t)L_10-(int32_t)1));
		int32_t L_11 = V_2;
		if ((!(((uint32_t)L_11) == ((uint32_t)(-1)))))
		{
			goto IL_004e;
		}
	}
	{
		return (bool)0;
	}

IL_004e:
	{
		V_3 = (int32_t)(-1);
	}

IL_0050:
	{
		LinkU5BU5D_t375419643* L_12 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = (int32_t)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_HashCode_0();
		int32_t L_15 = V_0;
		if ((!(((uint32_t)L_14) == ((uint32_t)L_15))))
		{
			goto IL_0089;
		}
	}
	{
		Il2CppObject* L_16 = (Il2CppObject*)__this->get_hcp_12();
		UInt16U5BU5D_t801649474* L_17 = (UInt16U5BU5D_t801649474*)__this->get_keySlots_6();
		int32_t L_18 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		uint16_t L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		uint16_t L_21 = ___key0;
		NullCheck((Il2CppObject*)L_16);
		bool L_22 = InterfaceFuncInvoker2< bool, uint16_t, uint16_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.UInt16>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_16, (uint16_t)L_20, (uint16_t)L_21);
		if (!L_22)
		{
			goto IL_0089;
		}
	}
	{
		goto IL_00a4;
	}

IL_0089:
	{
		int32_t L_23 = V_2;
		V_3 = (int32_t)L_23;
		LinkU5BU5D_t375419643* L_24 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_25 = V_2;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		int32_t L_26 = (int32_t)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_25)))->get_Next_1();
		V_2 = (int32_t)L_26;
		int32_t L_27 = V_2;
		if ((!(((uint32_t)L_27) == ((uint32_t)(-1)))))
		{
			goto IL_0050;
		}
	}

IL_00a4:
	{
		int32_t L_28 = V_2;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_00ad;
		}
	}
	{
		return (bool)0;
	}

IL_00ad:
	{
		int32_t L_29 = (int32_t)__this->get_count_10();
		__this->set_count_10(((int32_t)((int32_t)L_29-(int32_t)1)));
		int32_t L_30 = V_3;
		if ((!(((uint32_t)L_30) == ((uint32_t)(-1)))))
		{
			goto IL_00e2;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_31 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_32 = V_1;
		LinkU5BU5D_t375419643* L_33 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_34 = V_2;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, L_34);
		int32_t L_35 = (int32_t)((L_33)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_34)))->get_Next_1();
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(L_32), (int32_t)((int32_t)((int32_t)L_35+(int32_t)1)));
		goto IL_0104;
	}

IL_00e2:
	{
		LinkU5BU5D_t375419643* L_36 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_37 = V_3;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		LinkU5BU5D_t375419643* L_38 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_39 = V_2;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_39);
		int32_t L_40 = (int32_t)((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_39)))->get_Next_1();
		((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_37)))->set_Next_1(L_40);
	}

IL_0104:
	{
		LinkU5BU5D_t375419643* L_41 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_42 = V_2;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
		int32_t L_43 = (int32_t)__this->get_emptySlot_9();
		((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_42)))->set_Next_1(L_43);
		int32_t L_44 = V_2;
		__this->set_emptySlot_9(L_44);
		LinkU5BU5D_t375419643* L_45 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_46 = V_2;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, L_46);
		((L_45)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_46)))->set_HashCode_0(0);
		UInt16U5BU5D_t801649474* L_47 = (UInt16U5BU5D_t801649474*)__this->get_keySlots_6();
		int32_t L_48 = V_2;
		Initobj (UInt16_t24667923_il2cpp_TypeInfo_var, (&V_4));
		uint16_t L_49 = V_4;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, L_48);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(L_48), (uint16_t)L_49);
		ObjectU5BU5D_t1108656482* L_50 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		int32_t L_51 = V_2;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_5));
		Il2CppObject * L_52 = V_5;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, L_51);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(L_51), (Il2CppObject *)L_52);
		int32_t L_53 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_53+(int32_t)1)));
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::TryGetValue(TKey,TValue&)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_TryGetValue_m3134052041_MetadataUsageId;
extern "C"  bool Dictionary_2_TryGetValue_m3134052041_gshared (Dictionary_2_t1738881263 * __this, uint16_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_TryGetValue_m3134052041_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	{
		goto IL_0016;
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		uint16_t L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, uint16_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.UInt16>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (uint16_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		Int32U5BU5D_t3230847821* L_5 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_6 = V_0;
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))));
		int32_t L_9 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (int32_t)((int32_t)((int32_t)L_9-(int32_t)1));
		goto IL_00a2;
	}

IL_0048:
	{
		LinkU5BU5D_t375419643* L_10 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = (int32_t)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->get_HashCode_0();
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_0090;
		}
	}
	{
		Il2CppObject* L_14 = (Il2CppObject*)__this->get_hcp_12();
		UInt16U5BU5D_t801649474* L_15 = (UInt16U5BU5D_t801649474*)__this->get_keySlots_6();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		uint16_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		uint16_t L_19 = ___key0;
		NullCheck((Il2CppObject*)L_14);
		bool L_20 = InterfaceFuncInvoker2< bool, uint16_t, uint16_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.UInt16>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_14, (uint16_t)L_18, (uint16_t)L_19);
		if (!L_20)
		{
			goto IL_0090;
		}
	}
	{
		Il2CppObject ** L_21 = ___value1;
		ObjectU5BU5D_t1108656482* L_22 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		int32_t L_23 = V_1;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		Il2CppObject * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		(*(Il2CppObject **)L_21) = L_25;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)L_21, L_25);
		return (bool)1;
	}

IL_0090:
	{
		LinkU5BU5D_t375419643* L_26 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_27 = V_1;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		int32_t L_28 = (int32_t)((L_26)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_27)))->get_Next_1();
		V_1 = (int32_t)L_28;
	}

IL_00a2:
	{
		int32_t L_29 = V_1;
		if ((!(((uint32_t)L_29) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		Il2CppObject ** L_30 = ___value1;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_31 = V_2;
		(*(Il2CppObject **)L_30) = L_31;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)L_30, L_31);
		return (bool)0;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::get_Keys()
extern "C"  KeyCollection_t3365640714 * Dictionary_2_get_Keys_m1011830486_gshared (Dictionary_2_t1738881263 * __this, const MethodInfo* method)
{
	{
		KeyCollection_t3365640714 * L_0 = (KeyCollection_t3365640714 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 52));
		((  void (*) (KeyCollection_t3365640714 *, Dictionary_2_t1738881263 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 53)->methodPointer)(L_0, (Dictionary_2_t1738881263 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 53));
		return L_0;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::get_Values()
extern "C"  ValueCollection_t439486976 * Dictionary_2_get_Values_m2672769494_gshared (Dictionary_2_t1738881263 * __this, const MethodInfo* method)
{
	{
		ValueCollection_t439486976 * L_0 = (ValueCollection_t439486976 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 54));
		((  void (*) (ValueCollection_t439486976 *, Dictionary_2_t1738881263 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 55)->methodPointer)(L_0, (Dictionary_2_t1738881263 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 55));
		return L_0;
	}
}
// TKey System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::ToTKey(System.Object)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern Il2CppCodeGenString* _stringLiteral3927817532;
extern const uint32_t Dictionary_2_ToTKey_m2671681481_MetadataUsageId;
extern "C"  uint16_t Dictionary_2_ToTKey_m2671681481_gshared (Dictionary_2_t1738881263 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_ToTKey_m2671681481_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___key0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 56)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m138640077(NULL /*static, unused*/, (String_t*)_stringLiteral3927817532, (String_t*)L_4, /*hidden argument*/NULL);
		ArgumentException_t928607144 * L_6 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_6, (String_t*)L_5, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0040:
	{
		Il2CppObject * L_7 = ___key0;
		return ((*(uint16_t*)((uint16_t*)UnBox (L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10)))));
	}
}
// TValue System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::ToTValue(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3927817532;
extern Il2CppCodeGenString* _stringLiteral111972721;
extern const uint32_t Dictionary_2_ToTValue_m2747433161_MetadataUsageId;
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m2747433161_gshared (Dictionary_2_t1738881263 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_ToTValue_m2747433161_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 57)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_1);
		bool L_2 = Type_get_IsValueType_m1914757235((Type_t *)L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		Il2CppObject * L_4 = ___value0;
		if (((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14))))
		{
			goto IL_0053;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 57)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m138640077(NULL /*static, unused*/, (String_t*)_stringLiteral3927817532, (String_t*)L_6, /*hidden argument*/NULL);
		ArgumentException_t928607144 * L_8 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_8, (String_t*)L_7, (String_t*)_stringLiteral111972721, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0053:
	{
		Il2CppObject * L_9 = ___value0;
		return ((Il2CppObject *)Castclass(L_9, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14)));
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m3021943395_gshared (Dictionary_2_t1738881263 * __this, KeyValuePair_2_t1637661969  ___pair0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		uint16_t L_0 = ((  uint16_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1637661969 *)(&___pair0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((Dictionary_2_t1738881263 *)__this);
		bool L_1 = ((  bool (*) (Dictionary_2_t1738881263 *, uint16_t, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 58)->methodPointer)((Dictionary_2_t1738881263 *)__this, (uint16_t)L_0, (Il2CppObject **)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 58));
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		return (bool)0;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 47));
		EqualityComparer_1_t3278653252 * L_2 = ((  EqualityComparer_1_t3278653252 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46));
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1637661969 *)(&___pair0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Il2CppObject * L_4 = V_0;
		NullCheck((EqualityComparer_1_t3278653252 *)L_2);
		bool L_5 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(T,T) */, (EqualityComparer_1_t3278653252 *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_4);
		return L_5;
	}
}
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3056204655  Dictionary_2_GetEnumerator_m3451022052_gshared (Dictionary_2_t1738881263 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3056204655  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m3840966209(&L_0, (Dictionary_2_t1738881263 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 32));
		return L_0;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m582186547_gshared (Il2CppObject * __this /* static, unused */, uint16_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___key0;
		uint16_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), &L_1);
		Il2CppObject * L_3 = ___value1;
		DictionaryEntry_t1751606614  L_4;
		memset(&L_4, 0, sizeof(L_4));
		DictionaryEntry__ctor_m2600671860(&L_4, (Il2CppObject *)L_2, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m2403136537_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t4167966349 *)__this);
		((  void (*) (Dictionary_2_t4167966349 *, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t4167966349 *)__this, (int32_t)((int32_t)10), (Il2CppObject*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m978935824_gshared (Dictionary_2_t4167966349 * __this, Il2CppObject* ___comparer0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___comparer0;
		NullCheck((Dictionary_2_t4167966349 *)__this);
		((  void (*) (Dictionary_2_t4167966349 *, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t4167966349 *)__this, (int32_t)((int32_t)10), (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m297965343_gshared (Dictionary_2_t4167966349 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___dictionary0;
		NullCheck((Dictionary_2_t4167966349 *)__this);
		((  void (*) (Dictionary_2_t4167966349 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Dictionary_2_t4167966349 *)__this, (Il2CppObject*)L_0, (Il2CppObject*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m3637005290_gshared (Dictionary_2_t4167966349 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		NullCheck((Dictionary_2_t4167966349 *)__this);
		((  void (*) (Dictionary_2_t4167966349 *, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t4167966349 *)__this, (int32_t)L_0, (Il2CppObject*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral447049878;
extern const uint32_t Dictionary_2__ctor_m2792234558_MetadataUsageId;
extern "C"  void Dictionary_2__ctor_m2792234558_gshared (Dictionary_2_t4167966349 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2__ctor_m2792234558_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t4066747055  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Il2CppObject* V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral447049878, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Il2CppObject* L_2 = ___dictionary0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_3 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_2);
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		Il2CppObject* L_5 = ___comparer1;
		NullCheck((Dictionary_2_t4167966349 *)__this);
		((  void (*) (Dictionary_2_t4167966349 *, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t4167966349 *)__this, (int32_t)L_4, (Il2CppObject*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Il2CppObject* L_6 = ___dictionary0;
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject* L_7 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_6);
		V_2 = (Il2CppObject*)L_7;
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004d;
		}

IL_0032:
		{
			Il2CppObject* L_8 = V_2;
			NullCheck((Il2CppObject*)L_8);
			KeyValuePair_2_t4066747055  L_9 = InterfaceFuncInvoker0< KeyValuePair_2_t4066747055  >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_8);
			V_1 = (KeyValuePair_2_t4066747055 )L_9;
			uint32_t L_10 = ((  uint32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066747055 *)(&V_1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			Il2CppObject * L_11 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066747055 *)(&V_1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			NullCheck((Dictionary_2_t4167966349 *)__this);
			((  void (*) (Dictionary_2_t4167966349 *, uint32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t4167966349 *)__this, (uint32_t)L_10, (Il2CppObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		}

IL_004d:
		{
			Il2CppObject* L_12 = V_2;
			NullCheck((Il2CppObject *)L_12);
			bool L_13 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
			if (L_13)
			{
				goto IL_0032;
			}
		}

IL_0058:
		{
			IL2CPP_LEAVE(0x68, FINALLY_005d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_005d;
	}

FINALLY_005d:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_14 = V_2;
			if (L_14)
			{
				goto IL_0061;
			}
		}

IL_0060:
		{
			IL2CPP_END_FINALLY(93)
		}

IL_0061:
		{
			Il2CppObject* L_15 = V_2;
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			IL2CPP_END_FINALLY(93)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(93)
	{
		IL2CPP_JUMP_TBL(0x68, IL_0068)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0068:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m1697299930_gshared (Dictionary_2_t4167966349 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		SerializationInfo_t2185721892 * L_0 = ___info0;
		__this->set_serialization_info_13(L_0);
		return;
	}
}
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2412434197_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t4167966349 *)__this);
		KeyCollection_t1499758504 * L_0 = ((  KeyCollection_t1499758504 * (*) (Dictionary_2_t4167966349 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t4167966349 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_0;
	}
}
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3273778453_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t4167966349 *)__this);
		ValueCollection_t2868572062 * L_0 = ((  ValueCollection_t2868572062 * (*) (Dictionary_2_t4167966349 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t4167966349 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m1705758111_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t4167966349 *)__this);
		KeyCollection_t1499758504 * L_0 = ((  KeyCollection_t1499758504 * (*) (Dictionary_2_t4167966349 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t4167966349 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_0;
	}
}
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m923592397_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t4167966349 *)__this);
		ValueCollection_t2868572062 * L_0 = ((  ValueCollection_t2868572062 * (*) (Dictionary_2_t4167966349 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t4167966349 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1366614120_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1883566481_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m1591938383_gshared (Dictionary_2_t4167966349 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		if (!((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))
		{
			goto IL_002f;
		}
	}
	{
		Il2CppObject * L_1 = ___key0;
		NullCheck((Dictionary_2_t4167966349 *)__this);
		bool L_2 = ((  bool (*) (Dictionary_2_t4167966349 *, uint32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Dictionary_2_t4167966349 *)__this, (uint32_t)((*(uint32_t*)((uint32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Il2CppObject * L_3 = ___key0;
		NullCheck((Dictionary_2_t4167966349 *)__this);
		uint32_t L_4 = ((  uint32_t (*) (Dictionary_2_t4167966349 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t4167966349 *)__this, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((Dictionary_2_t4167966349 *)__this);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (Dictionary_2_t4167966349 *, uint32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((Dictionary_2_t4167966349 *)__this, (uint32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_5;
	}

IL_002f:
	{
		return NULL;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m663767988_gshared (Dictionary_2_t4167966349 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		NullCheck((Dictionary_2_t4167966349 *)__this);
		uint32_t L_1 = ((  uint32_t (*) (Dictionary_2_t4167966349 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t4167966349 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		Il2CppObject * L_2 = ___value1;
		NullCheck((Dictionary_2_t4167966349 *)__this);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Dictionary_2_t4167966349 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((Dictionary_2_t4167966349 *)__this, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		NullCheck((Dictionary_2_t4167966349 *)__this);
		((  void (*) (Dictionary_2_t4167966349 *, uint32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((Dictionary_2_t4167966349 *)__this, (uint32_t)L_1, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m704279069_gshared (Dictionary_2_t4167966349 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		NullCheck((Dictionary_2_t4167966349 *)__this);
		uint32_t L_1 = ((  uint32_t (*) (Dictionary_2_t4167966349 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t4167966349 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		Il2CppObject * L_2 = ___value1;
		NullCheck((Dictionary_2_t4167966349 *)__this);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Dictionary_2_t4167966349 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((Dictionary_2_t4167966349 *)__this, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		NullCheck((Dictionary_2_t4167966349 *)__this);
		((  void (*) (Dictionary_2_t4167966349 *, uint32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t4167966349 *)__this, (uint32_t)L_1, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_System_Collections_IDictionary_Contains_m1824051769_MetadataUsageId;
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m1824051769_gshared (Dictionary_2_t4167966349 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_System_Collections_IDictionary_Contains_m1824051769_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___key0;
		if (!((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject * L_3 = ___key0;
		NullCheck((Dictionary_2_t4167966349 *)__this);
		bool L_4 = ((  bool (*) (Dictionary_2_t4167966349 *, uint32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Dictionary_2_t4167966349 *)__this, (uint32_t)((*(uint32_t*)((uint32_t*)UnBox (L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_4;
	}

IL_0029:
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_System_Collections_IDictionary_Remove_m2711137778_MetadataUsageId;
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m2711137778_gshared (Dictionary_2_t4167966349 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_System_Collections_IDictionary_Remove_m2711137778_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___key0;
		if (!((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject * L_3 = ___key0;
		NullCheck((Dictionary_2_t4167966349 *)__this);
		((  bool (*) (Dictionary_2_t4167966349 *, uint32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((Dictionary_2_t4167966349 *)__this, (uint32_t)((*(uint32_t*)((uint32_t*)UnBox (L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m318222267_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1781101927_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1658812479_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m4166465928_gshared (Dictionary_2_t4167966349 * __this, KeyValuePair_2_t4066747055  ___keyValuePair0, const MethodInfo* method)
{
	{
		uint32_t L_0 = ((  uint32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066747055 *)(&___keyValuePair0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066747055 *)(&___keyValuePair0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((Dictionary_2_t4167966349 *)__this);
		((  void (*) (Dictionary_2_t4167966349 *, uint32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t4167966349 *)__this, (uint32_t)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2944884410_gshared (Dictionary_2_t4167966349 * __this, KeyValuePair_2_t4066747055  ___keyValuePair0, const MethodInfo* method)
{
	{
		KeyValuePair_2_t4066747055  L_0 = ___keyValuePair0;
		NullCheck((Dictionary_2_t4167966349 *)__this);
		bool L_1 = ((  bool (*) (Dictionary_2_t4167966349 *, KeyValuePair_2_t4066747055 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((Dictionary_2_t4167966349 *)__this, (KeyValuePair_2_t4066747055 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m805033836_gshared (Dictionary_2_t4167966349 * __this, KeyValuePair_2U5BU5D_t2966088118* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t2966088118* L_0 = ___array0;
		int32_t L_1 = ___index1;
		NullCheck((Dictionary_2_t4167966349 *)__this);
		((  void (*) (Dictionary_2_t4167966349 *, KeyValuePair_2U5BU5D_t2966088118*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((Dictionary_2_t4167966349 *)__this, (KeyValuePair_2U5BU5D_t2966088118*)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1827794015_gshared (Dictionary_2_t4167966349 * __this, KeyValuePair_2_t4066747055  ___keyValuePair0, const MethodInfo* method)
{
	{
		KeyValuePair_2_t4066747055  L_0 = ___keyValuePair0;
		NullCheck((Dictionary_2_t4167966349 *)__this);
		bool L_1 = ((  bool (*) (Dictionary_2_t4167966349 *, KeyValuePair_2_t4066747055 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((Dictionary_2_t4167966349 *)__this, (KeyValuePair_2_t4066747055 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (bool)0;
	}

IL_000e:
	{
		uint32_t L_2 = ((  uint32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066747055 *)(&___keyValuePair0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((Dictionary_2_t4167966349 *)__this);
		bool L_3 = ((  bool (*) (Dictionary_2_t4167966349 *, uint32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((Dictionary_2_t4167966349 *)__this, (uint32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		return L_3;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* DictionaryEntryU5BU5D_t479206547_il2cpp_TypeInfo_var;
extern const uint32_t Dictionary_2_System_Collections_ICollection_CopyTo_m1153559371_MetadataUsageId;
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m1153559371_gshared (Dictionary_2_t4167966349 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_System_Collections_ICollection_CopyTo_m1153559371_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2U5BU5D_t2966088118* V_0 = NULL;
	DictionaryEntryU5BU5D_t479206547* V_1 = NULL;
	int32_t G_B5_0 = 0;
	DictionaryEntryU5BU5D_t479206547* G_B5_1 = NULL;
	Dictionary_2_t4167966349 * G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	DictionaryEntryU5BU5D_t479206547* G_B4_1 = NULL;
	Dictionary_2_t4167966349 * G_B4_2 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (KeyValuePair_2U5BU5D_t2966088118*)((KeyValuePair_2U5BU5D_t2966088118*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20)));
		KeyValuePair_2U5BU5D_t2966088118* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		KeyValuePair_2U5BU5D_t2966088118* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((Dictionary_2_t4167966349 *)__this);
		((  void (*) (Dictionary_2_t4167966349 *, KeyValuePair_2U5BU5D_t2966088118*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((Dictionary_2_t4167966349 *)__this, (KeyValuePair_2U5BU5D_t2966088118*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}

IL_0016:
	{
		Il2CppArray * L_4 = ___array0;
		int32_t L_5 = ___index1;
		NullCheck((Dictionary_2_t4167966349 *)__this);
		((  void (*) (Dictionary_2_t4167966349 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((Dictionary_2_t4167966349 *)__this, (Il2CppArray *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		Il2CppArray * L_6 = ___array0;
		V_1 = (DictionaryEntryU5BU5D_t479206547*)((DictionaryEntryU5BU5D_t479206547*)IsInst(L_6, DictionaryEntryU5BU5D_t479206547_il2cpp_TypeInfo_var));
		DictionaryEntryU5BU5D_t479206547* L_7 = V_1;
		if (!L_7)
		{
			goto IL_0051;
		}
	}
	{
		DictionaryEntryU5BU5D_t479206547* L_8 = V_1;
		int32_t L_9 = ___index1;
		Transform_1_t1309936622 * L_10 = ((Dictionary_2_t4167966349_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22)->static_fields)->get_U3CU3Ef__amU24cacheB_15();
		G_B4_0 = L_9;
		G_B4_1 = L_8;
		G_B4_2 = ((Dictionary_2_t4167966349 *)(__this));
		if (L_10)
		{
			G_B5_0 = L_9;
			G_B5_1 = L_8;
			G_B5_2 = ((Dictionary_2_t4167966349 *)(__this));
			goto IL_0046;
		}
	}
	{
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		Transform_1_t1309936622 * L_12 = (Transform_1_t1309936622 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 24));
		((  void (*) (Transform_1_t1309936622 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)(L_12, (Il2CppObject *)NULL, (IntPtr_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		((Dictionary_2_t4167966349_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22)->static_fields)->set_U3CU3Ef__amU24cacheB_15(L_12);
		G_B5_0 = G_B4_0;
		G_B5_1 = G_B4_1;
		G_B5_2 = ((Dictionary_2_t4167966349 *)(G_B4_2));
	}

IL_0046:
	{
		Transform_1_t1309936622 * L_13 = ((Dictionary_2_t4167966349_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22)->static_fields)->get_U3CU3Ef__amU24cacheB_15();
		NullCheck((Dictionary_2_t4167966349 *)G_B5_2);
		((  void (*) (Dictionary_2_t4167966349 *, DictionaryEntryU5BU5D_t479206547*, int32_t, Transform_1_t1309936622 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)((Dictionary_2_t4167966349 *)G_B5_2, (DictionaryEntryU5BU5D_t479206547*)G_B5_1, (int32_t)G_B5_0, (Transform_1_t1309936622 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		return;
	}

IL_0051:
	{
		Il2CppArray * L_14 = ___array0;
		int32_t L_15 = ___index1;
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		Transform_1_t3625077063 * L_17 = (Transform_1_t3625077063 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 28));
		((  void (*) (Transform_1_t3625077063 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)(L_17, (Il2CppObject *)NULL, (IntPtr_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		NullCheck((Dictionary_2_t4167966349 *)__this);
		((  void (*) (Dictionary_2_t4167966349 *, Il2CppArray *, int32_t, Transform_1_t3625077063 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((Dictionary_2_t4167966349 *)__this, (Il2CppArray *)L_14, (int32_t)L_15, (Transform_1_t3625077063 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3579962886_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1190322445  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m330901371(&L_0, (Dictionary_2_t4167966349 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 32));
		Enumerator_t1190322445  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 31), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1860656259_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1190322445  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m330901371(&L_0, (Dictionary_2_t4167966349 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 32));
		Enumerator_t1190322445  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 31), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m859982238_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method)
{
	{
		ShimEnumerator_t3883744376 * L_0 = (ShimEnumerator_t3883744376 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 33));
		((  void (*) (ShimEnumerator_t3883744376 *, Dictionary_2_t4167966349 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34)->methodPointer)(L_0, (Dictionary_2_t4167966349 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m25921281_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_count_10();
		return L_0;
	}
}
// TValue System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::get_Item(TKey)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* KeyNotFoundException_t876339989_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_get_Item_m525730250_MetadataUsageId;
extern "C"  Il2CppObject * Dictionary_2_get_Item_m525730250_gshared (Dictionary_2_t4167966349 * __this, uint32_t ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_get_Item_m525730250_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		goto IL_0016;
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		uint32_t L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, uint32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.UInt32>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (uint32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		Int32U5BU5D_t3230847821* L_5 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_6 = V_0;
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))));
		int32_t L_9 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (int32_t)((int32_t)((int32_t)L_9-(int32_t)1));
		goto IL_009b;
	}

IL_0048:
	{
		LinkU5BU5D_t375419643* L_10 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = (int32_t)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->get_HashCode_0();
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_0089;
		}
	}
	{
		Il2CppObject* L_14 = (Il2CppObject*)__this->get_hcp_12();
		UInt32U5BU5D_t3230734560* L_15 = (UInt32U5BU5D_t3230734560*)__this->get_keySlots_6();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		uint32_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		uint32_t L_19 = ___key0;
		NullCheck((Il2CppObject*)L_14);
		bool L_20 = InterfaceFuncInvoker2< bool, uint32_t, uint32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.UInt32>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_14, (uint32_t)L_18, (uint32_t)L_19);
		if (!L_20)
		{
			goto IL_0089;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_21 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		int32_t L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = L_22;
		Il2CppObject * L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		return L_24;
	}

IL_0089:
	{
		LinkU5BU5D_t375419643* L_25 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_26 = V_1;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		int32_t L_27 = (int32_t)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))->get_Next_1();
		V_1 = (int32_t)L_27;
	}

IL_009b:
	{
		int32_t L_28 = V_1;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		KeyNotFoundException_t876339989 * L_29 = (KeyNotFoundException_t876339989 *)il2cpp_codegen_object_new(KeyNotFoundException_t876339989_il2cpp_TypeInfo_var);
		KeyNotFoundException__ctor_m3542895460(L_29, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_29);
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::set_Item(TKey,TValue)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_set_Item_m4259547673_MetadataUsageId;
extern "C"  void Dictionary_2_set_Item_m4259547673_gshared (Dictionary_2_t4167966349 * __this, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_set_Item_m4259547673_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		goto IL_0016;
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		uint32_t L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, uint32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.UInt32>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (uint32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t3230847821* L_6 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))));
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_2 = (int32_t)((int32_t)((int32_t)L_10-(int32_t)1));
		V_3 = (int32_t)(-1);
		int32_t L_11 = V_2;
		if ((((int32_t)L_11) == ((int32_t)(-1))))
		{
			goto IL_00a2;
		}
	}

IL_004e:
	{
		LinkU5BU5D_t375419643* L_12 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = (int32_t)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_HashCode_0();
		int32_t L_15 = V_0;
		if ((!(((uint32_t)L_14) == ((uint32_t)L_15))))
		{
			goto IL_0087;
		}
	}
	{
		Il2CppObject* L_16 = (Il2CppObject*)__this->get_hcp_12();
		UInt32U5BU5D_t3230734560* L_17 = (UInt32U5BU5D_t3230734560*)__this->get_keySlots_6();
		int32_t L_18 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		uint32_t L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		uint32_t L_21 = ___key0;
		NullCheck((Il2CppObject*)L_16);
		bool L_22 = InterfaceFuncInvoker2< bool, uint32_t, uint32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.UInt32>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_16, (uint32_t)L_20, (uint32_t)L_21);
		if (!L_22)
		{
			goto IL_0087;
		}
	}
	{
		goto IL_00a2;
	}

IL_0087:
	{
		int32_t L_23 = V_2;
		V_3 = (int32_t)L_23;
		LinkU5BU5D_t375419643* L_24 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_25 = V_2;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		int32_t L_26 = (int32_t)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_25)))->get_Next_1();
		V_2 = (int32_t)L_26;
		int32_t L_27 = V_2;
		if ((!(((uint32_t)L_27) == ((uint32_t)(-1)))))
		{
			goto IL_004e;
		}
	}

IL_00a2:
	{
		int32_t L_28 = V_2;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_0166;
		}
	}
	{
		int32_t L_29 = (int32_t)__this->get_count_10();
		int32_t L_30 = (int32_t)((int32_t)((int32_t)L_29+(int32_t)1));
		V_4 = (int32_t)L_30;
		__this->set_count_10(L_30);
		int32_t L_31 = V_4;
		int32_t L_32 = (int32_t)__this->get_threshold_11();
		if ((((int32_t)L_31) <= ((int32_t)L_32)))
		{
			goto IL_00de;
		}
	}
	{
		NullCheck((Dictionary_2_t4167966349 *)__this);
		((  void (*) (Dictionary_2_t4167966349 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36)->methodPointer)((Dictionary_2_t4167966349 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36));
		int32_t L_33 = V_0;
		Int32U5BU5D_t3230847821* L_34 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_34);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_33&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_34)->max_length))))));
	}

IL_00de:
	{
		int32_t L_35 = (int32_t)__this->get_emptySlot_9();
		V_2 = (int32_t)L_35;
		int32_t L_36 = V_2;
		if ((!(((uint32_t)L_36) == ((uint32_t)(-1)))))
		{
			goto IL_0105;
		}
	}
	{
		int32_t L_37 = (int32_t)__this->get_touchedSlots_8();
		int32_t L_38 = (int32_t)L_37;
		V_4 = (int32_t)L_38;
		__this->set_touchedSlots_8(((int32_t)((int32_t)L_38+(int32_t)1)));
		int32_t L_39 = V_4;
		V_2 = (int32_t)L_39;
		goto IL_011c;
	}

IL_0105:
	{
		LinkU5BU5D_t375419643* L_40 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_41 = V_2;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, L_41);
		int32_t L_42 = (int32_t)((L_40)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_41)))->get_Next_1();
		__this->set_emptySlot_9(L_42);
	}

IL_011c:
	{
		LinkU5BU5D_t375419643* L_43 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_44 = V_2;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		Int32U5BU5D_t3230847821* L_45 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_46 = V_1;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, L_46);
		int32_t L_47 = L_46;
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_44)))->set_Next_1(((int32_t)((int32_t)L_48-(int32_t)1)));
		Int32U5BU5D_t3230847821* L_49 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_50 = V_1;
		int32_t L_51 = V_2;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, L_50);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(L_50), (int32_t)((int32_t)((int32_t)L_51+(int32_t)1)));
		LinkU5BU5D_t375419643* L_52 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_53 = V_2;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, L_53);
		int32_t L_54 = V_0;
		((L_52)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_53)))->set_HashCode_0(L_54);
		UInt32U5BU5D_t3230734560* L_55 = (UInt32U5BU5D_t3230734560*)__this->get_keySlots_6();
		int32_t L_56 = V_2;
		uint32_t L_57 = ___key0;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, L_56);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(L_56), (uint32_t)L_57);
		goto IL_01b5;
	}

IL_0166:
	{
		int32_t L_58 = V_3;
		if ((((int32_t)L_58) == ((int32_t)(-1))))
		{
			goto IL_01b5;
		}
	}
	{
		LinkU5BU5D_t375419643* L_59 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_60 = V_3;
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, L_60);
		LinkU5BU5D_t375419643* L_61 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_62 = V_2;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, L_62);
		int32_t L_63 = (int32_t)((L_61)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_62)))->get_Next_1();
		((L_59)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_60)))->set_Next_1(L_63);
		LinkU5BU5D_t375419643* L_64 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_65 = V_2;
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, L_65);
		Int32U5BU5D_t3230847821* L_66 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_67 = V_1;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, L_67);
		int32_t L_68 = L_67;
		int32_t L_69 = (L_66)->GetAt(static_cast<il2cpp_array_size_t>(L_68));
		((L_64)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_65)))->set_Next_1(((int32_t)((int32_t)L_69-(int32_t)1)));
		Int32U5BU5D_t3230847821* L_70 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_71 = V_1;
		int32_t L_72 = V_2;
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, L_71);
		(L_70)->SetAt(static_cast<il2cpp_array_size_t>(L_71), (int32_t)((int32_t)((int32_t)L_72+(int32_t)1)));
	}

IL_01b5:
	{
		ObjectU5BU5D_t1108656482* L_73 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		int32_t L_74 = V_2;
		Il2CppObject * L_75 = ___value1;
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, L_74);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(L_74), (Il2CppObject *)L_75);
		int32_t L_76 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_76+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4227142842;
extern const uint32_t Dictionary_2_Init_m696550289_MetadataUsageId;
extern "C"  void Dictionary_2_Init_m696550289_gshared (Dictionary_2_t4167966349 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Init_m696550289_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Dictionary_2_t4167966349 * G_B4_0 = NULL;
	Dictionary_2_t4167966349 * G_B3_0 = NULL;
	Il2CppObject* G_B5_0 = NULL;
	Dictionary_2_t4167966349 * G_B5_1 = NULL;
	{
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_1 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_1, (String_t*)_stringLiteral4227142842, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Il2CppObject* L_2 = ___hcp1;
		G_B3_0 = ((Dictionary_2_t4167966349 *)(__this));
		if (!L_2)
		{
			G_B4_0 = ((Dictionary_2_t4167966349 *)(__this));
			goto IL_0021;
		}
	}
	{
		Il2CppObject* L_3 = ___hcp1;
		V_0 = (Il2CppObject*)L_3;
		Il2CppObject* L_4 = V_0;
		G_B5_0 = L_4;
		G_B5_1 = ((Dictionary_2_t4167966349 *)(G_B3_0));
		goto IL_0026;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 38));
		EqualityComparer_1_t3427472158 * L_5 = ((  EqualityComparer_1_t3427472158 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 37)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 37));
		G_B5_0 = ((Il2CppObject*)(L_5));
		G_B5_1 = ((Dictionary_2_t4167966349 *)(G_B4_0));
	}

IL_0026:
	{
		NullCheck(G_B5_1);
		G_B5_1->set_hcp_12(G_B5_0);
		int32_t L_6 = ___capacity0;
		if (L_6)
		{
			goto IL_0035;
		}
	}
	{
		___capacity0 = (int32_t)((int32_t)10);
	}

IL_0035:
	{
		int32_t L_7 = ___capacity0;
		___capacity0 = (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)((float)((float)(((float)((float)L_7)))/(float)(0.9f))))))+(int32_t)1));
		int32_t L_8 = ___capacity0;
		NullCheck((Dictionary_2_t4167966349 *)__this);
		((  void (*) (Dictionary_2_t4167966349 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 39)->methodPointer)((Dictionary_2_t4167966349 *)__this, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 39));
		__this->set_generation_14(0);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::InitArrays(System.Int32)
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern Il2CppClass* LinkU5BU5D_t375419643_il2cpp_TypeInfo_var;
extern const uint32_t Dictionary_2_InitArrays_m344112198_MetadataUsageId;
extern "C"  void Dictionary_2_InitArrays_m344112198_gshared (Dictionary_2_t4167966349 * __this, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_InitArrays_m344112198_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___size0;
		__this->set_table_4(((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)L_0)));
		int32_t L_1 = ___size0;
		__this->set_linkSlots_5(((LinkU5BU5D_t375419643*)SZArrayNew(LinkU5BU5D_t375419643_il2cpp_TypeInfo_var, (uint32_t)L_1)));
		__this->set_emptySlot_9((-1));
		int32_t L_2 = ___size0;
		__this->set_keySlots_6(((UInt32U5BU5D_t3230734560*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 40), (uint32_t)L_2)));
		int32_t L_3 = ___size0;
		__this->set_valueSlots_7(((ObjectU5BU5D_t1108656482*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 41), (uint32_t)L_3)));
		__this->set_touchedSlots_8(0);
		Int32U5BU5D_t3230847821* L_4 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_4);
		__this->set_threshold_11((((int32_t)((int32_t)((float)((float)(((float)((float)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length)))))))*(float)(0.9f)))))));
		int32_t L_5 = (int32_t)__this->get_threshold_11();
		if (L_5)
		{
			goto IL_0074;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_6 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_6);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0074;
		}
	}
	{
		__this->set_threshold_11(1);
	}

IL_0074:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::CopyToCheck(System.Array,System.Int32)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern Il2CppCodeGenString* _stringLiteral1142730282;
extern Il2CppCodeGenString* _stringLiteral2802514764;
extern const uint32_t Dictionary_2_CopyToCheck_m2290853314_MetadataUsageId;
extern "C"  void Dictionary_2_CopyToCheck_m2290853314_gshared (Dictionary_2_t4167966349 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_CopyToCheck_m2290853314_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppArray * L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		int32_t L_4 = ___index1;
		Il2CppArray * L_5 = ___array0;
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)L_6)))
		{
			goto IL_003a;
		}
	}
	{
		ArgumentException_t928607144 * L_7 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_7, (String_t*)_stringLiteral1142730282, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003a:
	{
		Il2CppArray * L_8 = ___array0;
		NullCheck((Il2CppArray *)L_8);
		int32_t L_9 = Array_get_Length_m1203127607((Il2CppArray *)L_8, /*hidden argument*/NULL);
		int32_t L_10 = ___index1;
		NullCheck((Dictionary_2_t4167966349 *)__this);
		int32_t L_11 = ((  int32_t (*) (Dictionary_2_t4167966349 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 42)->methodPointer)((Dictionary_2_t4167966349 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 42));
		if ((((int32_t)((int32_t)((int32_t)L_9-(int32_t)L_10))) >= ((int32_t)L_11)))
		{
			goto IL_0058;
		}
	}
	{
		ArgumentException_t928607144 * L_12 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_12, (String_t*)_stringLiteral2802514764, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0058:
	{
		return;
	}
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t4066747055  Dictionary_2_make_pair_m1125136654_gshared (Il2CppObject * __this /* static, unused */, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___key0;
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_t4066747055  L_2;
		memset(&L_2, 0, sizeof(L_2));
		KeyValuePair_2__ctor_m4066247973(&L_2, (uint32_t)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 44));
		return L_2;
	}
}
// TKey System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::pick_key(TKey,TValue)
extern "C"  uint32_t Dictionary_2_pick_key_m1245782312_gshared (Il2CppObject * __this /* static, unused */, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___key0;
		return L_0;
	}
}
// TValue System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m3835972072_gshared (Il2CppObject * __this /* static, unused */, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value1;
		return L_0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m2346653645_gshared (Dictionary_2_t4167966349 * __this, KeyValuePair_2U5BU5D_t2966088118* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t2966088118* L_0 = ___array0;
		int32_t L_1 = ___index1;
		NullCheck((Dictionary_2_t4167966349 *)__this);
		((  void (*) (Dictionary_2_t4167966349 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((Dictionary_2_t4167966349 *)__this, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		KeyValuePair_2U5BU5D_t2966088118* L_2 = ___array0;
		int32_t L_3 = ___index1;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		Transform_1_t3625077063 * L_5 = (Transform_1_t3625077063 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 28));
		((  void (*) (Transform_1_t3625077063 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)(L_5, (Il2CppObject *)NULL, (IntPtr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		NullCheck((Dictionary_2_t4167966349 *)__this);
		((  void (*) (Dictionary_2_t4167966349 *, KeyValuePair_2U5BU5D_t2966088118*, int32_t, Transform_1_t3625077063 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 45)->methodPointer)((Dictionary_2_t4167966349 *)__this, (KeyValuePair_2U5BU5D_t2966088118*)L_2, (int32_t)L_3, (Transform_1_t3625077063 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 45));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::Resize()
extern Il2CppClass* Hashtable_t1407064410_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern Il2CppClass* LinkU5BU5D_t375419643_il2cpp_TypeInfo_var;
extern const uint32_t Dictionary_2_Resize_m1540585279_MetadataUsageId;
extern "C"  void Dictionary_2_Resize_m1540585279_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Resize_m1540585279_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Int32U5BU5D_t3230847821* V_1 = NULL;
	LinkU5BU5D_t375419643* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	UInt32U5BU5D_t3230734560* V_7 = NULL;
	ObjectU5BU5D_t1108656482* V_8 = NULL;
	int32_t V_9 = 0;
	{
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Hashtable_t1407064410_il2cpp_TypeInfo_var);
		int32_t L_1 = Hashtable_ToPrime_m840372929(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))<<(int32_t)1))|(int32_t)1)), /*hidden argument*/NULL);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		V_1 = (Int32U5BU5D_t3230847821*)((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)L_2));
		int32_t L_3 = V_0;
		V_2 = (LinkU5BU5D_t375419643*)((LinkU5BU5D_t375419643*)SZArrayNew(LinkU5BU5D_t375419643_il2cpp_TypeInfo_var, (uint32_t)L_3));
		V_3 = (int32_t)0;
		goto IL_00b1;
	}

IL_0027:
	{
		Int32U5BU5D_t3230847821* L_4 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_5 = V_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		int32_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_4 = (int32_t)((int32_t)((int32_t)L_7-(int32_t)1));
		goto IL_00a5;
	}

IL_0038:
	{
		LinkU5BU5D_t375419643* L_8 = V_2;
		int32_t L_9 = V_4;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		Il2CppObject* L_10 = (Il2CppObject*)__this->get_hcp_12();
		UInt32U5BU5D_t3230734560* L_11 = (UInt32U5BU5D_t3230734560*)__this->get_keySlots_6();
		int32_t L_12 = V_4;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		uint32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Il2CppObject*)L_10);
		int32_t L_15 = InterfaceFuncInvoker1< int32_t, uint32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.UInt32>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_10, (uint32_t)L_14);
		int32_t L_16 = (int32_t)((int32_t)((int32_t)L_15|(int32_t)((int32_t)-2147483648LL)));
		V_9 = (int32_t)L_16;
		((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9)))->set_HashCode_0(L_16);
		int32_t L_17 = V_9;
		V_5 = (int32_t)L_17;
		int32_t L_18 = V_5;
		int32_t L_19 = V_0;
		V_6 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_18&(int32_t)((int32_t)2147483647LL)))%(int32_t)L_19));
		LinkU5BU5D_t375419643* L_20 = V_2;
		int32_t L_21 = V_4;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		Int32U5BU5D_t3230847821* L_22 = V_1;
		int32_t L_23 = V_6;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		int32_t L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_21)))->set_Next_1(((int32_t)((int32_t)L_25-(int32_t)1)));
		Int32U5BU5D_t3230847821* L_26 = V_1;
		int32_t L_27 = V_6;
		int32_t L_28 = V_4;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(L_27), (int32_t)((int32_t)((int32_t)L_28+(int32_t)1)));
		LinkU5BU5D_t375419643* L_29 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_30 = V_4;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
		int32_t L_31 = (int32_t)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_30)))->get_Next_1();
		V_4 = (int32_t)L_31;
	}

IL_00a5:
	{
		int32_t L_32 = V_4;
		if ((!(((uint32_t)L_32) == ((uint32_t)(-1)))))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_33 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00b1:
	{
		int32_t L_34 = V_3;
		Int32U5BU5D_t3230847821* L_35 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_35);
		if ((((int32_t)L_34) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_35)->max_length)))))))
		{
			goto IL_0027;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_36 = V_1;
		__this->set_table_4(L_36);
		LinkU5BU5D_t375419643* L_37 = V_2;
		__this->set_linkSlots_5(L_37);
		int32_t L_38 = V_0;
		V_7 = (UInt32U5BU5D_t3230734560*)((UInt32U5BU5D_t3230734560*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 40), (uint32_t)L_38));
		int32_t L_39 = V_0;
		V_8 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 41), (uint32_t)L_39));
		UInt32U5BU5D_t3230734560* L_40 = (UInt32U5BU5D_t3230734560*)__this->get_keySlots_6();
		UInt32U5BU5D_t3230734560* L_41 = V_7;
		int32_t L_42 = (int32_t)__this->get_touchedSlots_8();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_40, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_41, (int32_t)0, (int32_t)L_42, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_43 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		ObjectU5BU5D_t1108656482* L_44 = V_8;
		int32_t L_45 = (int32_t)__this->get_touchedSlots_8();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_43, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_44, (int32_t)0, (int32_t)L_45, /*hidden argument*/NULL);
		UInt32U5BU5D_t3230734560* L_46 = V_7;
		__this->set_keySlots_6(L_46);
		ObjectU5BU5D_t1108656482* L_47 = V_8;
		__this->set_valueSlots_7(L_47);
		int32_t L_48 = V_0;
		__this->set_threshold_11((((int32_t)((int32_t)((float)((float)(((float)((float)L_48)))*(float)(0.9f)))))));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::Add(TKey,TValue)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern Il2CppCodeGenString* _stringLiteral628480033;
extern const uint32_t Dictionary_2_Add_m457755836_MetadataUsageId;
extern "C"  void Dictionary_2_Add_m457755836_gshared (Dictionary_2_t4167966349 * __this, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Add_m457755836_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		goto IL_0016;
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		uint32_t L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, uint32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.UInt32>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (uint32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t3230847821* L_6 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))));
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_2 = (int32_t)((int32_t)((int32_t)L_10-(int32_t)1));
		goto IL_009b;
	}

IL_004a:
	{
		LinkU5BU5D_t375419643* L_11 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = (int32_t)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)))->get_HashCode_0();
		int32_t L_14 = V_0;
		if ((!(((uint32_t)L_13) == ((uint32_t)L_14))))
		{
			goto IL_0089;
		}
	}
	{
		Il2CppObject* L_15 = (Il2CppObject*)__this->get_hcp_12();
		UInt32U5BU5D_t3230734560* L_16 = (UInt32U5BU5D_t3230734560*)__this->get_keySlots_6();
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		uint32_t L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		uint32_t L_20 = ___key0;
		NullCheck((Il2CppObject*)L_15);
		bool L_21 = InterfaceFuncInvoker2< bool, uint32_t, uint32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.UInt32>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_15, (uint32_t)L_19, (uint32_t)L_20);
		if (!L_21)
		{
			goto IL_0089;
		}
	}
	{
		ArgumentException_t928607144 * L_22 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_22, (String_t*)_stringLiteral628480033, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	}

IL_0089:
	{
		LinkU5BU5D_t375419643* L_23 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_24 = V_2;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = (int32_t)((L_23)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_24)))->get_Next_1();
		V_2 = (int32_t)L_25;
	}

IL_009b:
	{
		int32_t L_26 = V_2;
		if ((!(((uint32_t)L_26) == ((uint32_t)(-1)))))
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_27 = (int32_t)__this->get_count_10();
		int32_t L_28 = (int32_t)((int32_t)((int32_t)L_27+(int32_t)1));
		V_3 = (int32_t)L_28;
		__this->set_count_10(L_28);
		int32_t L_29 = V_3;
		int32_t L_30 = (int32_t)__this->get_threshold_11();
		if ((((int32_t)L_29) <= ((int32_t)L_30)))
		{
			goto IL_00d5;
		}
	}
	{
		NullCheck((Dictionary_2_t4167966349 *)__this);
		((  void (*) (Dictionary_2_t4167966349 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36)->methodPointer)((Dictionary_2_t4167966349 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36));
		int32_t L_31 = V_0;
		Int32U5BU5D_t3230847821* L_32 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_32);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_31&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_32)->max_length))))));
	}

IL_00d5:
	{
		int32_t L_33 = (int32_t)__this->get_emptySlot_9();
		V_2 = (int32_t)L_33;
		int32_t L_34 = V_2;
		if ((!(((uint32_t)L_34) == ((uint32_t)(-1)))))
		{
			goto IL_00fa;
		}
	}
	{
		int32_t L_35 = (int32_t)__this->get_touchedSlots_8();
		int32_t L_36 = (int32_t)L_35;
		V_3 = (int32_t)L_36;
		__this->set_touchedSlots_8(((int32_t)((int32_t)L_36+(int32_t)1)));
		int32_t L_37 = V_3;
		V_2 = (int32_t)L_37;
		goto IL_0111;
	}

IL_00fa:
	{
		LinkU5BU5D_t375419643* L_38 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_39 = V_2;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_39);
		int32_t L_40 = (int32_t)((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_39)))->get_Next_1();
		__this->set_emptySlot_9(L_40);
	}

IL_0111:
	{
		LinkU5BU5D_t375419643* L_41 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_42 = V_2;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
		int32_t L_43 = V_0;
		((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_42)))->set_HashCode_0(L_43);
		LinkU5BU5D_t375419643* L_44 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_45 = V_2;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, L_45);
		Int32U5BU5D_t3230847821* L_46 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_47 = V_1;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, L_47);
		int32_t L_48 = L_47;
		int32_t L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_45)))->set_Next_1(((int32_t)((int32_t)L_49-(int32_t)1)));
		Int32U5BU5D_t3230847821* L_50 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_51 = V_1;
		int32_t L_52 = V_2;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, L_51);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(L_51), (int32_t)((int32_t)((int32_t)L_52+(int32_t)1)));
		UInt32U5BU5D_t3230734560* L_53 = (UInt32U5BU5D_t3230734560*)__this->get_keySlots_6();
		int32_t L_54 = V_2;
		uint32_t L_55 = ___key0;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, L_54);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(L_54), (uint32_t)L_55);
		ObjectU5BU5D_t1108656482* L_56 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		int32_t L_57 = V_2;
		Il2CppObject * L_58 = ___value1;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, L_57);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(L_57), (Il2CppObject *)L_58);
		int32_t L_59 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_59+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m4104237124_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method)
{
	{
		__this->set_count_10(0);
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		Int32U5BU5D_t3230847821* L_1 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_1);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		UInt32U5BU5D_t3230734560* L_2 = (UInt32U5BU5D_t3230734560*)__this->get_keySlots_6();
		UInt32U5BU5D_t3230734560* L_3 = (UInt32U5BU5D_t3230734560*)__this->get_keySlots_6();
		NullCheck(L_3);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))), /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_4 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		ObjectU5BU5D_t1108656482* L_5 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		NullCheck(L_5);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_4, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))), /*hidden argument*/NULL);
		LinkU5BU5D_t375419643* L_6 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		LinkU5BU5D_t375419643* L_7 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		NullCheck(L_7);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))), /*hidden argument*/NULL);
		__this->set_emptySlot_9((-1));
		__this->set_touchedSlots_8(0);
		int32_t L_8 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_8+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::ContainsKey(TKey)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_ContainsKey_m2033868842_MetadataUsageId;
extern "C"  bool Dictionary_2_ContainsKey_m2033868842_gshared (Dictionary_2_t4167966349 * __this, uint32_t ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_ContainsKey_m2033868842_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		goto IL_0016;
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		uint32_t L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, uint32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.UInt32>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (uint32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		Int32U5BU5D_t3230847821* L_5 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_6 = V_0;
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))));
		int32_t L_9 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (int32_t)((int32_t)((int32_t)L_9-(int32_t)1));
		goto IL_0090;
	}

IL_0048:
	{
		LinkU5BU5D_t375419643* L_10 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = (int32_t)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->get_HashCode_0();
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_007e;
		}
	}
	{
		Il2CppObject* L_14 = (Il2CppObject*)__this->get_hcp_12();
		UInt32U5BU5D_t3230734560* L_15 = (UInt32U5BU5D_t3230734560*)__this->get_keySlots_6();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		uint32_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		uint32_t L_19 = ___key0;
		NullCheck((Il2CppObject*)L_14);
		bool L_20 = InterfaceFuncInvoker2< bool, uint32_t, uint32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.UInt32>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_14, (uint32_t)L_18, (uint32_t)L_19);
		if (!L_20)
		{
			goto IL_007e;
		}
	}
	{
		return (bool)1;
	}

IL_007e:
	{
		LinkU5BU5D_t375419643* L_21 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = (int32_t)((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_22)))->get_Next_1();
		V_1 = (int32_t)L_23;
	}

IL_0090:
	{
		int32_t L_24 = V_1;
		if ((!(((uint32_t)L_24) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m2343931690_gshared (Dictionary_2_t4167966349 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 47));
		EqualityComparer_1_t3278653252 * L_0 = ((  EqualityComparer_1_t3278653252 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46));
		V_0 = (Il2CppObject*)L_0;
		V_1 = (int32_t)0;
		goto IL_0054;
	}

IL_000d:
	{
		Int32U5BU5D_t3230847821* L_1 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		int32_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_2 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		goto IL_0049;
	}

IL_001d:
	{
		Il2CppObject* L_5 = V_0;
		ObjectU5BU5D_t1108656482* L_6 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		int32_t L_7 = V_2;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		Il2CppObject * L_10 = ___value0;
		NullCheck((Il2CppObject*)L_5);
		bool L_11 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 48), (Il2CppObject*)L_5, (Il2CppObject *)L_9, (Il2CppObject *)L_10);
		if (!L_11)
		{
			goto IL_0037;
		}
	}
	{
		return (bool)1;
	}

IL_0037:
	{
		LinkU5BU5D_t375419643* L_12 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = (int32_t)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_Next_1();
		V_2 = (int32_t)L_14;
	}

IL_0049:
	{
		int32_t L_15 = V_2;
		if ((!(((uint32_t)L_15) == ((uint32_t)(-1)))))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_17 = V_1;
		Int32U5BU5D_t3230847821* L_18 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3237038;
extern Il2CppCodeGenString* _stringLiteral2016261304;
extern Il2CppCodeGenString* _stringLiteral3759850573;
extern Il2CppCodeGenString* _stringLiteral212812367;
extern Il2CppCodeGenString* _stringLiteral961688967;
extern const uint32_t Dictionary_2_GetObjectData_m3819277111_MetadataUsageId;
extern "C"  void Dictionary_2_GetObjectData_m3819277111_gshared (Dictionary_2_t4167966349 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_GetObjectData_m3819277111_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2U5BU5D_t2966088118* V_0 = NULL;
	{
		SerializationInfo_t2185721892 * L_0 = ___info0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral3237038, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		SerializationInfo_t2185721892 * L_2 = ___info0;
		int32_t L_3 = (int32_t)__this->get_generation_14();
		NullCheck((SerializationInfo_t2185721892 *)L_2);
		SerializationInfo_AddValue_m2348540514((SerializationInfo_t2185721892 *)L_2, (String_t*)_stringLiteral2016261304, (int32_t)L_3, /*hidden argument*/NULL);
		SerializationInfo_t2185721892 * L_4 = ___info0;
		Il2CppObject* L_5 = (Il2CppObject*)__this->get_hcp_12();
		NullCheck((SerializationInfo_t2185721892 *)L_4);
		SerializationInfo_AddValue_m469120675((SerializationInfo_t2185721892 *)L_4, (String_t*)_stringLiteral3759850573, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		V_0 = (KeyValuePair_2U5BU5D_t2966088118*)NULL;
		int32_t L_6 = (int32_t)__this->get_count_10();
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_7 = (int32_t)__this->get_count_10();
		V_0 = (KeyValuePair_2U5BU5D_t2966088118*)((KeyValuePair_2U5BU5D_t2966088118*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 49), (uint32_t)L_7));
		KeyValuePair_2U5BU5D_t2966088118* L_8 = V_0;
		NullCheck((Dictionary_2_t4167966349 *)__this);
		((  void (*) (Dictionary_2_t4167966349 *, KeyValuePair_2U5BU5D_t2966088118*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((Dictionary_2_t4167966349 *)__this, (KeyValuePair_2U5BU5D_t2966088118*)L_8, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
	}

IL_0055:
	{
		SerializationInfo_t2185721892 * L_9 = ___info0;
		Int32U5BU5D_t3230847821* L_10 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_10);
		NullCheck((SerializationInfo_t2185721892 *)L_9);
		SerializationInfo_AddValue_m2348540514((SerializationInfo_t2185721892 *)L_9, (String_t*)_stringLiteral212812367, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))), /*hidden argument*/NULL);
		SerializationInfo_t2185721892 * L_11 = ___info0;
		KeyValuePair_2U5BU5D_t2966088118* L_12 = V_0;
		NullCheck((SerializationInfo_t2185721892 *)L_11);
		SerializationInfo_AddValue_m469120675((SerializationInfo_t2185721892 *)L_11, (String_t*)_stringLiteral961688967, (Il2CppObject *)(Il2CppObject *)L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::OnDeserialization(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2016261304;
extern Il2CppCodeGenString* _stringLiteral3759850573;
extern Il2CppCodeGenString* _stringLiteral212812367;
extern Il2CppCodeGenString* _stringLiteral961688967;
extern const uint32_t Dictionary_2_OnDeserialization_m3116010381_MetadataUsageId;
extern "C"  void Dictionary_2_OnDeserialization_m3116010381_gshared (Dictionary_2_t4167966349 * __this, Il2CppObject * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_OnDeserialization_m3116010381_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2U5BU5D_t2966088118* V_1 = NULL;
	int32_t V_2 = 0;
	{
		SerializationInfo_t2185721892 * L_0 = (SerializationInfo_t2185721892 *)__this->get_serialization_info_13();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		SerializationInfo_t2185721892 * L_1 = (SerializationInfo_t2185721892 *)__this->get_serialization_info_13();
		NullCheck((SerializationInfo_t2185721892 *)L_1);
		int32_t L_2 = SerializationInfo_GetInt32_m4048035953((SerializationInfo_t2185721892 *)L_1, (String_t*)_stringLiteral2016261304, /*hidden argument*/NULL);
		__this->set_generation_14(L_2);
		SerializationInfo_t2185721892 * L_3 = (SerializationInfo_t2185721892 *)__this->get_serialization_info_13();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 50)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2185721892 *)L_3);
		Il2CppObject * L_5 = SerializationInfo_GetValue_m4125471336((SerializationInfo_t2185721892 *)L_3, (String_t*)_stringLiteral3759850573, (Type_t *)L_4, /*hidden argument*/NULL);
		__this->set_hcp_12(((Il2CppObject*)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35))));
		SerializationInfo_t2185721892 * L_6 = (SerializationInfo_t2185721892 *)__this->get_serialization_info_13();
		NullCheck((SerializationInfo_t2185721892 *)L_6);
		int32_t L_7 = SerializationInfo_GetInt32_m4048035953((SerializationInfo_t2185721892 *)L_6, (String_t*)_stringLiteral212812367, /*hidden argument*/NULL);
		V_0 = (int32_t)L_7;
		SerializationInfo_t2185721892 * L_8 = (SerializationInfo_t2185721892 *)__this->get_serialization_info_13();
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 51)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2185721892 *)L_8);
		Il2CppObject * L_10 = SerializationInfo_GetValue_m4125471336((SerializationInfo_t2185721892 *)L_8, (String_t*)_stringLiteral961688967, (Type_t *)L_9, /*hidden argument*/NULL);
		V_1 = (KeyValuePair_2U5BU5D_t2966088118*)((KeyValuePair_2U5BU5D_t2966088118*)Castclass(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20)));
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) >= ((int32_t)((int32_t)10))))
		{
			goto IL_0083;
		}
	}
	{
		V_0 = (int32_t)((int32_t)10);
	}

IL_0083:
	{
		int32_t L_12 = V_0;
		NullCheck((Dictionary_2_t4167966349 *)__this);
		((  void (*) (Dictionary_2_t4167966349 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 39)->methodPointer)((Dictionary_2_t4167966349 *)__this, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 39));
		__this->set_count_10(0);
		KeyValuePair_2U5BU5D_t2966088118* L_13 = V_1;
		if (!L_13)
		{
			goto IL_00c9;
		}
	}
	{
		V_2 = (int32_t)0;
		goto IL_00c0;
	}

IL_009e:
	{
		KeyValuePair_2U5BU5D_t2966088118* L_14 = V_1;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		uint32_t L_16 = ((  uint32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066747055 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_15)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		KeyValuePair_2U5BU5D_t2966088118* L_17 = V_1;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		Il2CppObject * L_19 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066747055 *)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((Dictionary_2_t4167966349 *)__this);
		((  void (*) (Dictionary_2_t4167966349 *, uint32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t4167966349 *)__this, (uint32_t)L_16, (Il2CppObject *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		int32_t L_20 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_00c0:
	{
		int32_t L_21 = V_2;
		KeyValuePair_2U5BU5D_t2966088118* L_22 = V_1;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_22)->max_length)))))))
		{
			goto IL_009e;
		}
	}

IL_00c9:
	{
		int32_t L_23 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_23+(int32_t)1)));
		__this->set_serialization_info_13((SerializationInfo_t2185721892 *)NULL);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::Remove(TKey)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t24667981_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_Remove_m3414222470_MetadataUsageId;
extern "C"  bool Dictionary_2_Remove_m3414222470_gshared (Dictionary_2_t4167966349 * __this, uint32_t ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Remove_m3414222470_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	uint32_t V_4 = 0;
	Il2CppObject * V_5 = NULL;
	{
		goto IL_0016;
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		uint32_t L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, uint32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.UInt32>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (uint32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t3230847821* L_6 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))));
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_2 = (int32_t)((int32_t)((int32_t)L_10-(int32_t)1));
		int32_t L_11 = V_2;
		if ((!(((uint32_t)L_11) == ((uint32_t)(-1)))))
		{
			goto IL_004e;
		}
	}
	{
		return (bool)0;
	}

IL_004e:
	{
		V_3 = (int32_t)(-1);
	}

IL_0050:
	{
		LinkU5BU5D_t375419643* L_12 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = (int32_t)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_HashCode_0();
		int32_t L_15 = V_0;
		if ((!(((uint32_t)L_14) == ((uint32_t)L_15))))
		{
			goto IL_0089;
		}
	}
	{
		Il2CppObject* L_16 = (Il2CppObject*)__this->get_hcp_12();
		UInt32U5BU5D_t3230734560* L_17 = (UInt32U5BU5D_t3230734560*)__this->get_keySlots_6();
		int32_t L_18 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		uint32_t L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		uint32_t L_21 = ___key0;
		NullCheck((Il2CppObject*)L_16);
		bool L_22 = InterfaceFuncInvoker2< bool, uint32_t, uint32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.UInt32>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_16, (uint32_t)L_20, (uint32_t)L_21);
		if (!L_22)
		{
			goto IL_0089;
		}
	}
	{
		goto IL_00a4;
	}

IL_0089:
	{
		int32_t L_23 = V_2;
		V_3 = (int32_t)L_23;
		LinkU5BU5D_t375419643* L_24 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_25 = V_2;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		int32_t L_26 = (int32_t)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_25)))->get_Next_1();
		V_2 = (int32_t)L_26;
		int32_t L_27 = V_2;
		if ((!(((uint32_t)L_27) == ((uint32_t)(-1)))))
		{
			goto IL_0050;
		}
	}

IL_00a4:
	{
		int32_t L_28 = V_2;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_00ad;
		}
	}
	{
		return (bool)0;
	}

IL_00ad:
	{
		int32_t L_29 = (int32_t)__this->get_count_10();
		__this->set_count_10(((int32_t)((int32_t)L_29-(int32_t)1)));
		int32_t L_30 = V_3;
		if ((!(((uint32_t)L_30) == ((uint32_t)(-1)))))
		{
			goto IL_00e2;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_31 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_32 = V_1;
		LinkU5BU5D_t375419643* L_33 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_34 = V_2;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, L_34);
		int32_t L_35 = (int32_t)((L_33)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_34)))->get_Next_1();
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(L_32), (int32_t)((int32_t)((int32_t)L_35+(int32_t)1)));
		goto IL_0104;
	}

IL_00e2:
	{
		LinkU5BU5D_t375419643* L_36 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_37 = V_3;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		LinkU5BU5D_t375419643* L_38 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_39 = V_2;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_39);
		int32_t L_40 = (int32_t)((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_39)))->get_Next_1();
		((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_37)))->set_Next_1(L_40);
	}

IL_0104:
	{
		LinkU5BU5D_t375419643* L_41 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_42 = V_2;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
		int32_t L_43 = (int32_t)__this->get_emptySlot_9();
		((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_42)))->set_Next_1(L_43);
		int32_t L_44 = V_2;
		__this->set_emptySlot_9(L_44);
		LinkU5BU5D_t375419643* L_45 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_46 = V_2;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, L_46);
		((L_45)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_46)))->set_HashCode_0(0);
		UInt32U5BU5D_t3230734560* L_47 = (UInt32U5BU5D_t3230734560*)__this->get_keySlots_6();
		int32_t L_48 = V_2;
		Initobj (UInt32_t24667981_il2cpp_TypeInfo_var, (&V_4));
		uint32_t L_49 = V_4;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, L_48);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(L_48), (uint32_t)L_49);
		ObjectU5BU5D_t1108656482* L_50 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		int32_t L_51 = V_2;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_5));
		Il2CppObject * L_52 = V_5;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, L_51);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(L_51), (Il2CppObject *)L_52);
		int32_t L_53 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_53+(int32_t)1)));
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::TryGetValue(TKey,TValue&)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_TryGetValue_m2879450755_MetadataUsageId;
extern "C"  bool Dictionary_2_TryGetValue_m2879450755_gshared (Dictionary_2_t4167966349 * __this, uint32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_TryGetValue_m2879450755_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	{
		goto IL_0016;
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		uint32_t L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, uint32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.UInt32>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (uint32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		Int32U5BU5D_t3230847821* L_5 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_6 = V_0;
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))));
		int32_t L_9 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (int32_t)((int32_t)((int32_t)L_9-(int32_t)1));
		goto IL_00a2;
	}

IL_0048:
	{
		LinkU5BU5D_t375419643* L_10 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = (int32_t)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->get_HashCode_0();
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_0090;
		}
	}
	{
		Il2CppObject* L_14 = (Il2CppObject*)__this->get_hcp_12();
		UInt32U5BU5D_t3230734560* L_15 = (UInt32U5BU5D_t3230734560*)__this->get_keySlots_6();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		uint32_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		uint32_t L_19 = ___key0;
		NullCheck((Il2CppObject*)L_14);
		bool L_20 = InterfaceFuncInvoker2< bool, uint32_t, uint32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.UInt32>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_14, (uint32_t)L_18, (uint32_t)L_19);
		if (!L_20)
		{
			goto IL_0090;
		}
	}
	{
		Il2CppObject ** L_21 = ___value1;
		ObjectU5BU5D_t1108656482* L_22 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		int32_t L_23 = V_1;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		Il2CppObject * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		(*(Il2CppObject **)L_21) = L_25;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)L_21, L_25);
		return (bool)1;
	}

IL_0090:
	{
		LinkU5BU5D_t375419643* L_26 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_27 = V_1;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		int32_t L_28 = (int32_t)((L_26)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_27)))->get_Next_1();
		V_1 = (int32_t)L_28;
	}

IL_00a2:
	{
		int32_t L_29 = V_1;
		if ((!(((uint32_t)L_29) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		Il2CppObject ** L_30 = ___value1;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_31 = V_2;
		(*(Il2CppObject **)L_30) = L_31;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)L_30, L_31);
		return (bool)0;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::get_Keys()
extern "C"  KeyCollection_t1499758504 * Dictionary_2_get_Keys_m3416980060_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method)
{
	{
		KeyCollection_t1499758504 * L_0 = (KeyCollection_t1499758504 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 52));
		((  void (*) (KeyCollection_t1499758504 *, Dictionary_2_t4167966349 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 53)->methodPointer)(L_0, (Dictionary_2_t4167966349 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 53));
		return L_0;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::get_Values()
extern "C"  ValueCollection_t2868572062 * Dictionary_2_get_Values_m3329104860_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method)
{
	{
		ValueCollection_t2868572062 * L_0 = (ValueCollection_t2868572062 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 54));
		((  void (*) (ValueCollection_t2868572062 *, Dictionary_2_t4167966349 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 55)->methodPointer)(L_0, (Dictionary_2_t4167966349 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 55));
		return L_0;
	}
}
// TKey System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::ToTKey(System.Object)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern Il2CppCodeGenString* _stringLiteral3927817532;
extern const uint32_t Dictionary_2_ToTKey_m695641219_MetadataUsageId;
extern "C"  uint32_t Dictionary_2_ToTKey_m695641219_gshared (Dictionary_2_t4167966349 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_ToTKey_m695641219_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___key0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 56)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m138640077(NULL /*static, unused*/, (String_t*)_stringLiteral3927817532, (String_t*)L_4, /*hidden argument*/NULL);
		ArgumentException_t928607144 * L_6 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_6, (String_t*)L_5, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0040:
	{
		Il2CppObject * L_7 = ___key0;
		return ((*(uint32_t*)((uint32_t*)UnBox (L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10)))));
	}
}
// TValue System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::ToTValue(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3927817532;
extern Il2CppCodeGenString* _stringLiteral111972721;
extern const uint32_t Dictionary_2_ToTValue_m2148286211_MetadataUsageId;
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m2148286211_gshared (Dictionary_2_t4167966349 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_ToTValue_m2148286211_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 57)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_1);
		bool L_2 = Type_get_IsValueType_m1914757235((Type_t *)L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		Il2CppObject * L_4 = ___value0;
		if (((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14))))
		{
			goto IL_0053;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 57)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m138640077(NULL /*static, unused*/, (String_t*)_stringLiteral3927817532, (String_t*)L_6, /*hidden argument*/NULL);
		ArgumentException_t928607144 * L_8 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_8, (String_t*)L_7, (String_t*)_stringLiteral111972721, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0053:
	{
		Il2CppObject * L_9 = ___value0;
		return ((Il2CppObject *)Castclass(L_9, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14)));
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m3316311145_gshared (Dictionary_2_t4167966349 * __this, KeyValuePair_2_t4066747055  ___pair0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		uint32_t L_0 = ((  uint32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066747055 *)(&___pair0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((Dictionary_2_t4167966349 *)__this);
		bool L_1 = ((  bool (*) (Dictionary_2_t4167966349 *, uint32_t, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 58)->methodPointer)((Dictionary_2_t4167966349 *)__this, (uint32_t)L_0, (Il2CppObject **)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 58));
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		return (bool)0;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 47));
		EqualityComparer_1_t3278653252 * L_2 = ((  EqualityComparer_1_t3278653252 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46));
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066747055 *)(&___pair0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Il2CppObject * L_4 = V_0;
		NullCheck((EqualityComparer_1_t3278653252 *)L_2);
		bool L_5 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(T,T) */, (EqualityComparer_1_t3278653252 *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_4);
		return L_5;
	}
}
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1190322445  Dictionary_2_GetEnumerator_m1351811870_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1190322445  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m330901371(&L_0, (Dictionary_2_t4167966349 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 32));
		return L_0;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m327585261_gshared (Il2CppObject * __this /* static, unused */, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___key0;
		uint32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), &L_1);
		Il2CppObject * L_3 = ___value1;
		DictionaryEntry_t1751606614  L_4;
		memset(&L_4, 0, sizeof(L_4));
		DictionaryEntry__ctor_m2600671860(&L_4, (Il2CppObject *)L_2, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::.ctor()
extern "C"  void Dictionary_2__ctor_m4195593932_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t2952530300 *)__this);
		((  void (*) (Dictionary_2_t2952530300 *, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2952530300 *)__this, (int32_t)((int32_t)10), (Il2CppObject*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1515362650_gshared (Dictionary_2_t2952530300 * __this, Il2CppObject* ___comparer0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___comparer0;
		NullCheck((Dictionary_2_t2952530300 *)__this);
		((  void (*) (Dictionary_2_t2952530300 *, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2952530300 *)__this, (int32_t)((int32_t)10), (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m4042295061_gshared (Dictionary_2_t2952530300 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___dictionary0;
		NullCheck((Dictionary_2_t2952530300 *)__this);
		((  void (*) (Dictionary_2_t2952530300 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Dictionary_2_t2952530300 *)__this, (Il2CppObject*)L_0, (Il2CppObject*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m2673612596_gshared (Dictionary_2_t2952530300 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		NullCheck((Dictionary_2_t2952530300 *)__this);
		((  void (*) (Dictionary_2_t2952530300 *, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2952530300 *)__this, (int32_t)L_0, (Il2CppObject*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral447049878;
extern const uint32_t Dictionary_2__ctor_m721632264_MetadataUsageId;
extern "C"  void Dictionary_2__ctor_m721632264_gshared (Dictionary_2_t2952530300 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2__ctor_m721632264_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t2851311006  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Il2CppObject* V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral447049878, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Il2CppObject* L_2 = ___dictionary0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_3 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_2);
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		Il2CppObject* L_5 = ___comparer1;
		NullCheck((Dictionary_2_t2952530300 *)__this);
		((  void (*) (Dictionary_2_t2952530300 *, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2952530300 *)__this, (int32_t)L_4, (Il2CppObject*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Il2CppObject* L_6 = ___dictionary0;
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject* L_7 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_6);
		V_2 = (Il2CppObject*)L_7;
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004d;
		}

IL_0032:
		{
			Il2CppObject* L_8 = V_2;
			NullCheck((Il2CppObject*)L_8);
			KeyValuePair_2_t2851311006  L_9 = InterfaceFuncInvoker0< KeyValuePair_2_t2851311006  >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_8);
			V_1 = (KeyValuePair_2_t2851311006 )L_9;
			int32_t L_10 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2851311006 *)(&V_1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			int32_t L_11 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2851311006 *)(&V_1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			NullCheck((Dictionary_2_t2952530300 *)__this);
			((  void (*) (Dictionary_2_t2952530300 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t2952530300 *)__this, (int32_t)L_10, (int32_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		}

IL_004d:
		{
			Il2CppObject* L_12 = V_2;
			NullCheck((Il2CppObject *)L_12);
			bool L_13 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
			if (L_13)
			{
				goto IL_0032;
			}
		}

IL_0058:
		{
			IL2CPP_LEAVE(0x68, FINALLY_005d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_005d;
	}

FINALLY_005d:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_14 = V_2;
			if (L_14)
			{
				goto IL_0061;
			}
		}

IL_0060:
		{
			IL2CPP_END_FINALLY(93)
		}

IL_0061:
		{
			Il2CppObject* L_15 = V_2;
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			IL2CPP_END_FINALLY(93)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(93)
	{
		IL2CPP_JUMP_TBL(0x68, IL_0068)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0068:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2818794788_gshared (Dictionary_2_t2952530300 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		SerializationInfo_t2185721892 * L_0 = ___info0;
		__this->set_serialization_info_13(L_0);
		return;
	}
}
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3241164811_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t2952530300 *)__this);
		KeyCollection_t284322455 * L_0 = ((  KeyCollection_t284322455 * (*) (Dictionary_2_t2952530300 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t2952530300 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_0;
	}
}
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1819571083_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t2952530300 *)__this);
		ValueCollection_t1653136013 * L_0 = ((  ValueCollection_t1653136013 * (*) (Dictionary_2_t2952530300 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t2952530300 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m1430143849_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t2952530300 *)__this);
		KeyCollection_t284322455 * L_0 = ((  KeyCollection_t284322455 * (*) (Dictionary_2_t2952530300 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t2952530300 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_0;
	}
}
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m2346258967_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t2952530300 *)__this);
		ValueCollection_t1653136013 * L_0 = ((  ValueCollection_t1653136013 * (*) (Dictionary_2_t2952530300 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t2952530300 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1793458270_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1758788315_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m3619564357_gshared (Dictionary_2_t2952530300 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		if (!((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))
		{
			goto IL_002f;
		}
	}
	{
		Il2CppObject * L_1 = ___key0;
		NullCheck((Dictionary_2_t2952530300 *)__this);
		bool L_2 = ((  bool (*) (Dictionary_2_t2952530300 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Dictionary_2_t2952530300 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Il2CppObject * L_3 = ___key0;
		NullCheck((Dictionary_2_t2952530300 *)__this);
		int32_t L_4 = ((  int32_t (*) (Dictionary_2_t2952530300 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t2952530300 *)__this, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((Dictionary_2_t2952530300 *)__this);
		int32_t L_5 = ((  int32_t (*) (Dictionary_2_t2952530300 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((Dictionary_2_t2952530300 *)__this, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), &L_6);
		return L_7;
	}

IL_002f:
	{
		return NULL;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m3877758378_gshared (Dictionary_2_t2952530300 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		NullCheck((Dictionary_2_t2952530300 *)__this);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t2952530300 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t2952530300 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		Il2CppObject * L_2 = ___value1;
		NullCheck((Dictionary_2_t2952530300 *)__this);
		int32_t L_3 = ((  int32_t (*) (Dictionary_2_t2952530300 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((Dictionary_2_t2952530300 *)__this, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		NullCheck((Dictionary_2_t2952530300 *)__this);
		((  void (*) (Dictionary_2_t2952530300 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((Dictionary_2_t2952530300 *)__this, (int32_t)L_1, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3435263591_gshared (Dictionary_2_t2952530300 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		NullCheck((Dictionary_2_t2952530300 *)__this);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t2952530300 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t2952530300 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		Il2CppObject * L_2 = ___value1;
		NullCheck((Dictionary_2_t2952530300 *)__this);
		int32_t L_3 = ((  int32_t (*) (Dictionary_2_t2952530300 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((Dictionary_2_t2952530300 *)__this, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		NullCheck((Dictionary_2_t2952530300 *)__this);
		((  void (*) (Dictionary_2_t2952530300 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t2952530300 *)__this, (int32_t)L_1, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_System_Collections_IDictionary_Contains_m2748555695_MetadataUsageId;
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m2748555695_gshared (Dictionary_2_t2952530300 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_System_Collections_IDictionary_Contains_m2748555695_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___key0;
		if (!((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject * L_3 = ___key0;
		NullCheck((Dictionary_2_t2952530300 *)__this);
		bool L_4 = ((  bool (*) (Dictionary_2_t2952530300 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Dictionary_2_t2952530300 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_4;
	}

IL_0029:
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_System_Collections_IDictionary_Remove_m353956840_MetadataUsageId;
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m353956840_gshared (Dictionary_2_t2952530300 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_System_Collections_IDictionary_Remove_m353956840_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___key0;
		if (!((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject * L_3 = ___key0;
		NullCheck((Dictionary_2_t2952530300 *)__this);
		((  bool (*) (Dictionary_2_t2952530300 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((Dictionary_2_t2952530300 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3329098757_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3813976753_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m848428041_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1208716414_gshared (Dictionary_2_t2952530300 * __this, KeyValuePair_2_t2851311006  ___keyValuePair0, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2851311006 *)(&___keyValuePair0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		int32_t L_1 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2851311006 *)(&___keyValuePair0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((Dictionary_2_t2952530300 *)__this);
		((  void (*) (Dictionary_2_t2952530300 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t2952530300 *)__this, (int32_t)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2677761668_gshared (Dictionary_2_t2952530300 * __this, KeyValuePair_2_t2851311006  ___keyValuePair0, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2851311006  L_0 = ___keyValuePair0;
		NullCheck((Dictionary_2_t2952530300 *)__this);
		bool L_1 = ((  bool (*) (Dictionary_2_t2952530300 *, KeyValuePair_2_t2851311006 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((Dictionary_2_t2952530300 *)__this, (KeyValuePair_2_t2851311006 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2948176098_gshared (Dictionary_2_t2952530300 * __this, KeyValuePair_2U5BU5D_t390341163* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t390341163* L_0 = ___array0;
		int32_t L_1 = ___index1;
		NullCheck((Dictionary_2_t2952530300 *)__this);
		((  void (*) (Dictionary_2_t2952530300 *, KeyValuePair_2U5BU5D_t390341163*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((Dictionary_2_t2952530300 *)__this, (KeyValuePair_2U5BU5D_t390341163*)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3373883049_gshared (Dictionary_2_t2952530300 * __this, KeyValuePair_2_t2851311006  ___keyValuePair0, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2851311006  L_0 = ___keyValuePair0;
		NullCheck((Dictionary_2_t2952530300 *)__this);
		bool L_1 = ((  bool (*) (Dictionary_2_t2952530300 *, KeyValuePair_2_t2851311006 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((Dictionary_2_t2952530300 *)__this, (KeyValuePair_2_t2851311006 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (bool)0;
	}

IL_000e:
	{
		int32_t L_2 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2851311006 *)(&___keyValuePair0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((Dictionary_2_t2952530300 *)__this);
		bool L_3 = ((  bool (*) (Dictionary_2_t2952530300 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((Dictionary_2_t2952530300 *)__this, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		return L_3;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* DictionaryEntryU5BU5D_t479206547_il2cpp_TypeInfo_var;
extern const uint32_t Dictionary_2_System_Collections_ICollection_CopyTo_m4209700929_MetadataUsageId;
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m4209700929_gshared (Dictionary_2_t2952530300 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_System_Collections_ICollection_CopyTo_m4209700929_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2U5BU5D_t390341163* V_0 = NULL;
	DictionaryEntryU5BU5D_t479206547* V_1 = NULL;
	int32_t G_B5_0 = 0;
	DictionaryEntryU5BU5D_t479206547* G_B5_1 = NULL;
	Dictionary_2_t2952530300 * G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	DictionaryEntryU5BU5D_t479206547* G_B4_1 = NULL;
	Dictionary_2_t2952530300 * G_B4_2 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (KeyValuePair_2U5BU5D_t390341163*)((KeyValuePair_2U5BU5D_t390341163*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20)));
		KeyValuePair_2U5BU5D_t390341163* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		KeyValuePair_2U5BU5D_t390341163* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((Dictionary_2_t2952530300 *)__this);
		((  void (*) (Dictionary_2_t2952530300 *, KeyValuePair_2U5BU5D_t390341163*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((Dictionary_2_t2952530300 *)__this, (KeyValuePair_2U5BU5D_t390341163*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}

IL_0016:
	{
		Il2CppArray * L_4 = ___array0;
		int32_t L_5 = ___index1;
		NullCheck((Dictionary_2_t2952530300 *)__this);
		((  void (*) (Dictionary_2_t2952530300 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((Dictionary_2_t2952530300 *)__this, (Il2CppArray *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		Il2CppArray * L_6 = ___array0;
		V_1 = (DictionaryEntryU5BU5D_t479206547*)((DictionaryEntryU5BU5D_t479206547*)IsInst(L_6, DictionaryEntryU5BU5D_t479206547_il2cpp_TypeInfo_var));
		DictionaryEntryU5BU5D_t479206547* L_7 = V_1;
		if (!L_7)
		{
			goto IL_0051;
		}
	}
	{
		DictionaryEntryU5BU5D_t479206547* L_8 = V_1;
		int32_t L_9 = ___index1;
		Transform_1_t3029156963 * L_10 = ((Dictionary_2_t2952530300_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22)->static_fields)->get_U3CU3Ef__amU24cacheB_15();
		G_B4_0 = L_9;
		G_B4_1 = L_8;
		G_B4_2 = ((Dictionary_2_t2952530300 *)(__this));
		if (L_10)
		{
			G_B5_0 = L_9;
			G_B5_1 = L_8;
			G_B5_2 = ((Dictionary_2_t2952530300 *)(__this));
			goto IL_0046;
		}
	}
	{
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		Transform_1_t3029156963 * L_12 = (Transform_1_t3029156963 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 24));
		((  void (*) (Transform_1_t3029156963 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)(L_12, (Il2CppObject *)NULL, (IntPtr_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		((Dictionary_2_t2952530300_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22)->static_fields)->set_U3CU3Ef__amU24cacheB_15(L_12);
		G_B5_0 = G_B4_0;
		G_B5_1 = G_B4_1;
		G_B5_2 = ((Dictionary_2_t2952530300 *)(G_B4_2));
	}

IL_0046:
	{
		Transform_1_t3029156963 * L_13 = ((Dictionary_2_t2952530300_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22)->static_fields)->get_U3CU3Ef__amU24cacheB_15();
		NullCheck((Dictionary_2_t2952530300 *)G_B5_2);
		((  void (*) (Dictionary_2_t2952530300 *, DictionaryEntryU5BU5D_t479206547*, int32_t, Transform_1_t3029156963 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)((Dictionary_2_t2952530300 *)G_B5_2, (DictionaryEntryU5BU5D_t479206547*)G_B5_1, (int32_t)G_B5_0, (Transform_1_t3029156963 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		return;
	}

IL_0051:
	{
		Il2CppArray * L_14 = ___array0;
		int32_t L_15 = ___index1;
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		Transform_1_t4128861355 * L_17 = (Transform_1_t4128861355 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 28));
		((  void (*) (Transform_1_t4128861355 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)(L_17, (Il2CppObject *)NULL, (IntPtr_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		NullCheck((Dictionary_2_t2952530300 *)__this);
		((  void (*) (Dictionary_2_t2952530300 *, Il2CppArray *, int32_t, Transform_1_t4128861355 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((Dictionary_2_t2952530300 *)__this, (Il2CppArray *)L_14, (int32_t)L_15, (Transform_1_t4128861355 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m934584700_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4269853692  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m1938982213(&L_0, (Dictionary_2_t2952530300 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 32));
		Enumerator_t4269853692  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 31), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2236931065_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4269853692  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m1938982213(&L_0, (Dictionary_2_t2952530300 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 32));
		Enumerator_t4269853692  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 31), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3102904340_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method)
{
	{
		ShimEnumerator_t2668308327 * L_0 = (ShimEnumerator_t2668308327 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 33));
		((  void (*) (ShimEnumerator_t2668308327 *, Dictionary_2_t2952530300 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34)->methodPointer)(L_0, (Dictionary_2_t2952530300 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m33942091_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_count_10();
		return L_0;
	}
}
// TValue System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::get_Item(TKey)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* KeyNotFoundException_t876339989_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_get_Item_m2389179986_MetadataUsageId;
extern "C"  int32_t Dictionary_2_get_Item_m2389179986_gshared (Dictionary_2_t2952530300 * __this, int32_t ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_get_Item_m2389179986_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		goto IL_0016;
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		int32_t L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		Int32U5BU5D_t3230847821* L_5 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_6 = V_0;
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))));
		int32_t L_9 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (int32_t)((int32_t)((int32_t)L_9-(int32_t)1));
		goto IL_009b;
	}

IL_0048:
	{
		LinkU5BU5D_t375419643* L_10 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = (int32_t)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->get_HashCode_0();
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_0089;
		}
	}
	{
		Il2CppObject* L_14 = (Il2CppObject*)__this->get_hcp_12();
		UIModelDisplayTypeU5BU5D_t737309086* L_15 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		int32_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		int32_t L_19 = ___key0;
		NullCheck((Il2CppObject*)L_14);
		bool L_20 = InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_14, (int32_t)L_18, (int32_t)L_19);
		if (!L_20)
		{
			goto IL_0089;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_21 = (Int32U5BU5D_t3230847821*)__this->get_valueSlots_7();
		int32_t L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = L_22;
		int32_t L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		return L_24;
	}

IL_0089:
	{
		LinkU5BU5D_t375419643* L_25 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_26 = V_1;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		int32_t L_27 = (int32_t)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))->get_Next_1();
		V_1 = (int32_t)L_27;
	}

IL_009b:
	{
		int32_t L_28 = V_1;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		KeyNotFoundException_t876339989 * L_29 = (KeyNotFoundException_t876339989 *)il2cpp_codegen_object_new(KeyNotFoundException_t876339989_il2cpp_TypeInfo_var);
		KeyNotFoundException__ctor_m3542895460(L_29, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_29);
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::set_Item(TKey,TValue)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_set_Item_m2297180221_MetadataUsageId;
extern "C"  void Dictionary_2_set_Item_m2297180221_gshared (Dictionary_2_t2952530300 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_set_Item_m2297180221_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		goto IL_0016;
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		int32_t L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t3230847821* L_6 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))));
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_2 = (int32_t)((int32_t)((int32_t)L_10-(int32_t)1));
		V_3 = (int32_t)(-1);
		int32_t L_11 = V_2;
		if ((((int32_t)L_11) == ((int32_t)(-1))))
		{
			goto IL_00a2;
		}
	}

IL_004e:
	{
		LinkU5BU5D_t375419643* L_12 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = (int32_t)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_HashCode_0();
		int32_t L_15 = V_0;
		if ((!(((uint32_t)L_14) == ((uint32_t)L_15))))
		{
			goto IL_0087;
		}
	}
	{
		Il2CppObject* L_16 = (Il2CppObject*)__this->get_hcp_12();
		UIModelDisplayTypeU5BU5D_t737309086* L_17 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		int32_t L_18 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		int32_t L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		int32_t L_21 = ___key0;
		NullCheck((Il2CppObject*)L_16);
		bool L_22 = InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_16, (int32_t)L_20, (int32_t)L_21);
		if (!L_22)
		{
			goto IL_0087;
		}
	}
	{
		goto IL_00a2;
	}

IL_0087:
	{
		int32_t L_23 = V_2;
		V_3 = (int32_t)L_23;
		LinkU5BU5D_t375419643* L_24 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_25 = V_2;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		int32_t L_26 = (int32_t)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_25)))->get_Next_1();
		V_2 = (int32_t)L_26;
		int32_t L_27 = V_2;
		if ((!(((uint32_t)L_27) == ((uint32_t)(-1)))))
		{
			goto IL_004e;
		}
	}

IL_00a2:
	{
		int32_t L_28 = V_2;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_0166;
		}
	}
	{
		int32_t L_29 = (int32_t)__this->get_count_10();
		int32_t L_30 = (int32_t)((int32_t)((int32_t)L_29+(int32_t)1));
		V_4 = (int32_t)L_30;
		__this->set_count_10(L_30);
		int32_t L_31 = V_4;
		int32_t L_32 = (int32_t)__this->get_threshold_11();
		if ((((int32_t)L_31) <= ((int32_t)L_32)))
		{
			goto IL_00de;
		}
	}
	{
		NullCheck((Dictionary_2_t2952530300 *)__this);
		((  void (*) (Dictionary_2_t2952530300 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36)->methodPointer)((Dictionary_2_t2952530300 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36));
		int32_t L_33 = V_0;
		Int32U5BU5D_t3230847821* L_34 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_34);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_33&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_34)->max_length))))));
	}

IL_00de:
	{
		int32_t L_35 = (int32_t)__this->get_emptySlot_9();
		V_2 = (int32_t)L_35;
		int32_t L_36 = V_2;
		if ((!(((uint32_t)L_36) == ((uint32_t)(-1)))))
		{
			goto IL_0105;
		}
	}
	{
		int32_t L_37 = (int32_t)__this->get_touchedSlots_8();
		int32_t L_38 = (int32_t)L_37;
		V_4 = (int32_t)L_38;
		__this->set_touchedSlots_8(((int32_t)((int32_t)L_38+(int32_t)1)));
		int32_t L_39 = V_4;
		V_2 = (int32_t)L_39;
		goto IL_011c;
	}

IL_0105:
	{
		LinkU5BU5D_t375419643* L_40 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_41 = V_2;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, L_41);
		int32_t L_42 = (int32_t)((L_40)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_41)))->get_Next_1();
		__this->set_emptySlot_9(L_42);
	}

IL_011c:
	{
		LinkU5BU5D_t375419643* L_43 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_44 = V_2;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		Int32U5BU5D_t3230847821* L_45 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_46 = V_1;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, L_46);
		int32_t L_47 = L_46;
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_44)))->set_Next_1(((int32_t)((int32_t)L_48-(int32_t)1)));
		Int32U5BU5D_t3230847821* L_49 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_50 = V_1;
		int32_t L_51 = V_2;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, L_50);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(L_50), (int32_t)((int32_t)((int32_t)L_51+(int32_t)1)));
		LinkU5BU5D_t375419643* L_52 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_53 = V_2;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, L_53);
		int32_t L_54 = V_0;
		((L_52)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_53)))->set_HashCode_0(L_54);
		UIModelDisplayTypeU5BU5D_t737309086* L_55 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		int32_t L_56 = V_2;
		int32_t L_57 = ___key0;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, L_56);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(L_56), (int32_t)L_57);
		goto IL_01b5;
	}

IL_0166:
	{
		int32_t L_58 = V_3;
		if ((((int32_t)L_58) == ((int32_t)(-1))))
		{
			goto IL_01b5;
		}
	}
	{
		LinkU5BU5D_t375419643* L_59 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_60 = V_3;
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, L_60);
		LinkU5BU5D_t375419643* L_61 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_62 = V_2;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, L_62);
		int32_t L_63 = (int32_t)((L_61)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_62)))->get_Next_1();
		((L_59)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_60)))->set_Next_1(L_63);
		LinkU5BU5D_t375419643* L_64 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_65 = V_2;
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, L_65);
		Int32U5BU5D_t3230847821* L_66 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_67 = V_1;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, L_67);
		int32_t L_68 = L_67;
		int32_t L_69 = (L_66)->GetAt(static_cast<il2cpp_array_size_t>(L_68));
		((L_64)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_65)))->set_Next_1(((int32_t)((int32_t)L_69-(int32_t)1)));
		Int32U5BU5D_t3230847821* L_70 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_71 = V_1;
		int32_t L_72 = V_2;
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, L_71);
		(L_70)->SetAt(static_cast<il2cpp_array_size_t>(L_71), (int32_t)((int32_t)((int32_t)L_72+(int32_t)1)));
	}

IL_01b5:
	{
		Int32U5BU5D_t3230847821* L_73 = (Int32U5BU5D_t3230847821*)__this->get_valueSlots_7();
		int32_t L_74 = V_2;
		int32_t L_75 = ___value1;
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, L_74);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(L_74), (int32_t)L_75);
		int32_t L_76 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_76+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4227142842;
extern const uint32_t Dictionary_2_Init_m978329051_MetadataUsageId;
extern "C"  void Dictionary_2_Init_m978329051_gshared (Dictionary_2_t2952530300 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Init_m978329051_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Dictionary_2_t2952530300 * G_B4_0 = NULL;
	Dictionary_2_t2952530300 * G_B3_0 = NULL;
	Il2CppObject* G_B5_0 = NULL;
	Dictionary_2_t2952530300 * G_B5_1 = NULL;
	{
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_1 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_1, (String_t*)_stringLiteral4227142842, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Il2CppObject* L_2 = ___hcp1;
		G_B3_0 = ((Dictionary_2_t2952530300 *)(__this));
		if (!L_2)
		{
			G_B4_0 = ((Dictionary_2_t2952530300 *)(__this));
			goto IL_0021;
		}
	}
	{
		Il2CppObject* L_3 = ___hcp1;
		V_0 = (Il2CppObject*)L_3;
		Il2CppObject* L_4 = V_0;
		G_B5_0 = L_4;
		G_B5_1 = ((Dictionary_2_t2952530300 *)(G_B3_0));
		goto IL_0026;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 38));
		EqualityComparer_1_t999589560 * L_5 = ((  EqualityComparer_1_t999589560 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 37)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 37));
		G_B5_0 = ((Il2CppObject*)(L_5));
		G_B5_1 = ((Dictionary_2_t2952530300 *)(G_B4_0));
	}

IL_0026:
	{
		NullCheck(G_B5_1);
		G_B5_1->set_hcp_12(G_B5_0);
		int32_t L_6 = ___capacity0;
		if (L_6)
		{
			goto IL_0035;
		}
	}
	{
		___capacity0 = (int32_t)((int32_t)10);
	}

IL_0035:
	{
		int32_t L_7 = ___capacity0;
		___capacity0 = (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)((float)((float)(((float)((float)L_7)))/(float)(0.9f))))))+(int32_t)1));
		int32_t L_8 = ___capacity0;
		NullCheck((Dictionary_2_t2952530300 *)__this);
		((  void (*) (Dictionary_2_t2952530300 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 39)->methodPointer)((Dictionary_2_t2952530300 *)__this, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 39));
		__this->set_generation_14(0);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::InitArrays(System.Int32)
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern Il2CppClass* LinkU5BU5D_t375419643_il2cpp_TypeInfo_var;
extern const uint32_t Dictionary_2_InitArrays_m1474064188_MetadataUsageId;
extern "C"  void Dictionary_2_InitArrays_m1474064188_gshared (Dictionary_2_t2952530300 * __this, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_InitArrays_m1474064188_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___size0;
		__this->set_table_4(((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)L_0)));
		int32_t L_1 = ___size0;
		__this->set_linkSlots_5(((LinkU5BU5D_t375419643*)SZArrayNew(LinkU5BU5D_t375419643_il2cpp_TypeInfo_var, (uint32_t)L_1)));
		__this->set_emptySlot_9((-1));
		int32_t L_2 = ___size0;
		__this->set_keySlots_6(((UIModelDisplayTypeU5BU5D_t737309086*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 40), (uint32_t)L_2)));
		int32_t L_3 = ___size0;
		__this->set_valueSlots_7(((Int32U5BU5D_t3230847821*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 41), (uint32_t)L_3)));
		__this->set_touchedSlots_8(0);
		Int32U5BU5D_t3230847821* L_4 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_4);
		__this->set_threshold_11((((int32_t)((int32_t)((float)((float)(((float)((float)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length)))))))*(float)(0.9f)))))));
		int32_t L_5 = (int32_t)__this->get_threshold_11();
		if (L_5)
		{
			goto IL_0074;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_6 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_6);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0074;
		}
	}
	{
		__this->set_threshold_11(1);
	}

IL_0074:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern Il2CppCodeGenString* _stringLiteral1142730282;
extern Il2CppCodeGenString* _stringLiteral2802514764;
extern const uint32_t Dictionary_2_CopyToCheck_m1692267064_MetadataUsageId;
extern "C"  void Dictionary_2_CopyToCheck_m1692267064_gshared (Dictionary_2_t2952530300 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_CopyToCheck_m1692267064_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppArray * L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		int32_t L_4 = ___index1;
		Il2CppArray * L_5 = ___array0;
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)L_6)))
		{
			goto IL_003a;
		}
	}
	{
		ArgumentException_t928607144 * L_7 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_7, (String_t*)_stringLiteral1142730282, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003a:
	{
		Il2CppArray * L_8 = ___array0;
		NullCheck((Il2CppArray *)L_8);
		int32_t L_9 = Array_get_Length_m1203127607((Il2CppArray *)L_8, /*hidden argument*/NULL);
		int32_t L_10 = ___index1;
		NullCheck((Dictionary_2_t2952530300 *)__this);
		int32_t L_11 = ((  int32_t (*) (Dictionary_2_t2952530300 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 42)->methodPointer)((Dictionary_2_t2952530300 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 42));
		if ((((int32_t)((int32_t)((int32_t)L_9-(int32_t)L_10))) >= ((int32_t)L_11)))
		{
			goto IL_0058;
		}
	}
	{
		ArgumentException_t928607144 * L_12 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_12, (String_t*)_stringLiteral2802514764, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0058:
	{
		return;
	}
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t2851311006  Dictionary_2_make_pair_m782631812_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		int32_t L_1 = ___value1;
		KeyValuePair_2_t2851311006  L_2;
		memset(&L_2, 0, sizeof(L_2));
		KeyValuePair_2__ctor_m2384040859(&L_2, (int32_t)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 44));
		return L_2;
	}
}
// TKey System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m1202020850_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		return L_0;
	}
}
// TValue System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::pick_value(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_value_m3626985266_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value1;
		return L_0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m1654411927_gshared (Dictionary_2_t2952530300 * __this, KeyValuePair_2U5BU5D_t390341163* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t390341163* L_0 = ___array0;
		int32_t L_1 = ___index1;
		NullCheck((Dictionary_2_t2952530300 *)__this);
		((  void (*) (Dictionary_2_t2952530300 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((Dictionary_2_t2952530300 *)__this, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		KeyValuePair_2U5BU5D_t390341163* L_2 = ___array0;
		int32_t L_3 = ___index1;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		Transform_1_t4128861355 * L_5 = (Transform_1_t4128861355 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 28));
		((  void (*) (Transform_1_t4128861355 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)(L_5, (Il2CppObject *)NULL, (IntPtr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		NullCheck((Dictionary_2_t2952530300 *)__this);
		((  void (*) (Dictionary_2_t2952530300 *, KeyValuePair_2U5BU5D_t390341163*, int32_t, Transform_1_t4128861355 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 45)->methodPointer)((Dictionary_2_t2952530300 *)__this, (KeyValuePair_2U5BU5D_t390341163*)L_2, (int32_t)L_3, (Transform_1_t4128861355 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 45));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::Resize()
extern Il2CppClass* Hashtable_t1407064410_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern Il2CppClass* LinkU5BU5D_t375419643_il2cpp_TypeInfo_var;
extern const uint32_t Dictionary_2_Resize_m812504629_MetadataUsageId;
extern "C"  void Dictionary_2_Resize_m812504629_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Resize_m812504629_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Int32U5BU5D_t3230847821* V_1 = NULL;
	LinkU5BU5D_t375419643* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	UIModelDisplayTypeU5BU5D_t737309086* V_7 = NULL;
	Int32U5BU5D_t3230847821* V_8 = NULL;
	int32_t V_9 = 0;
	{
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Hashtable_t1407064410_il2cpp_TypeInfo_var);
		int32_t L_1 = Hashtable_ToPrime_m840372929(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))<<(int32_t)1))|(int32_t)1)), /*hidden argument*/NULL);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		V_1 = (Int32U5BU5D_t3230847821*)((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)L_2));
		int32_t L_3 = V_0;
		V_2 = (LinkU5BU5D_t375419643*)((LinkU5BU5D_t375419643*)SZArrayNew(LinkU5BU5D_t375419643_il2cpp_TypeInfo_var, (uint32_t)L_3));
		V_3 = (int32_t)0;
		goto IL_00b1;
	}

IL_0027:
	{
		Int32U5BU5D_t3230847821* L_4 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_5 = V_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		int32_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_4 = (int32_t)((int32_t)((int32_t)L_7-(int32_t)1));
		goto IL_00a5;
	}

IL_0038:
	{
		LinkU5BU5D_t375419643* L_8 = V_2;
		int32_t L_9 = V_4;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		Il2CppObject* L_10 = (Il2CppObject*)__this->get_hcp_12();
		UIModelDisplayTypeU5BU5D_t737309086* L_11 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		int32_t L_12 = V_4;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Il2CppObject*)L_10);
		int32_t L_15 = InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_10, (int32_t)L_14);
		int32_t L_16 = (int32_t)((int32_t)((int32_t)L_15|(int32_t)((int32_t)-2147483648LL)));
		V_9 = (int32_t)L_16;
		((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9)))->set_HashCode_0(L_16);
		int32_t L_17 = V_9;
		V_5 = (int32_t)L_17;
		int32_t L_18 = V_5;
		int32_t L_19 = V_0;
		V_6 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_18&(int32_t)((int32_t)2147483647LL)))%(int32_t)L_19));
		LinkU5BU5D_t375419643* L_20 = V_2;
		int32_t L_21 = V_4;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		Int32U5BU5D_t3230847821* L_22 = V_1;
		int32_t L_23 = V_6;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		int32_t L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_21)))->set_Next_1(((int32_t)((int32_t)L_25-(int32_t)1)));
		Int32U5BU5D_t3230847821* L_26 = V_1;
		int32_t L_27 = V_6;
		int32_t L_28 = V_4;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(L_27), (int32_t)((int32_t)((int32_t)L_28+(int32_t)1)));
		LinkU5BU5D_t375419643* L_29 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_30 = V_4;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
		int32_t L_31 = (int32_t)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_30)))->get_Next_1();
		V_4 = (int32_t)L_31;
	}

IL_00a5:
	{
		int32_t L_32 = V_4;
		if ((!(((uint32_t)L_32) == ((uint32_t)(-1)))))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_33 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00b1:
	{
		int32_t L_34 = V_3;
		Int32U5BU5D_t3230847821* L_35 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_35);
		if ((((int32_t)L_34) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_35)->max_length)))))))
		{
			goto IL_0027;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_36 = V_1;
		__this->set_table_4(L_36);
		LinkU5BU5D_t375419643* L_37 = V_2;
		__this->set_linkSlots_5(L_37);
		int32_t L_38 = V_0;
		V_7 = (UIModelDisplayTypeU5BU5D_t737309086*)((UIModelDisplayTypeU5BU5D_t737309086*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 40), (uint32_t)L_38));
		int32_t L_39 = V_0;
		V_8 = (Int32U5BU5D_t3230847821*)((Int32U5BU5D_t3230847821*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 41), (uint32_t)L_39));
		UIModelDisplayTypeU5BU5D_t737309086* L_40 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		UIModelDisplayTypeU5BU5D_t737309086* L_41 = V_7;
		int32_t L_42 = (int32_t)__this->get_touchedSlots_8();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_40, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_41, (int32_t)0, (int32_t)L_42, /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_43 = (Int32U5BU5D_t3230847821*)__this->get_valueSlots_7();
		Int32U5BU5D_t3230847821* L_44 = V_8;
		int32_t L_45 = (int32_t)__this->get_touchedSlots_8();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_43, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_44, (int32_t)0, (int32_t)L_45, /*hidden argument*/NULL);
		UIModelDisplayTypeU5BU5D_t737309086* L_46 = V_7;
		__this->set_keySlots_6(L_46);
		Int32U5BU5D_t3230847821* L_47 = V_8;
		__this->set_valueSlots_7(L_47);
		int32_t L_48 = V_0;
		__this->set_threshold_11((((int32_t)((int32_t)((float)((float)(((float)((float)L_48)))*(float)(0.9f)))))));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::Add(TKey,TValue)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern Il2CppCodeGenString* _stringLiteral628480033;
extern const uint32_t Dictionary_2_Add_m3129406938_MetadataUsageId;
extern "C"  void Dictionary_2_Add_m3129406938_gshared (Dictionary_2_t2952530300 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Add_m3129406938_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		goto IL_0016;
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		int32_t L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t3230847821* L_6 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))));
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_2 = (int32_t)((int32_t)((int32_t)L_10-(int32_t)1));
		goto IL_009b;
	}

IL_004a:
	{
		LinkU5BU5D_t375419643* L_11 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = (int32_t)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)))->get_HashCode_0();
		int32_t L_14 = V_0;
		if ((!(((uint32_t)L_13) == ((uint32_t)L_14))))
		{
			goto IL_0089;
		}
	}
	{
		Il2CppObject* L_15 = (Il2CppObject*)__this->get_hcp_12();
		UIModelDisplayTypeU5BU5D_t737309086* L_16 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		int32_t L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		int32_t L_20 = ___key0;
		NullCheck((Il2CppObject*)L_15);
		bool L_21 = InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_15, (int32_t)L_19, (int32_t)L_20);
		if (!L_21)
		{
			goto IL_0089;
		}
	}
	{
		ArgumentException_t928607144 * L_22 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_22, (String_t*)_stringLiteral628480033, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	}

IL_0089:
	{
		LinkU5BU5D_t375419643* L_23 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_24 = V_2;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = (int32_t)((L_23)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_24)))->get_Next_1();
		V_2 = (int32_t)L_25;
	}

IL_009b:
	{
		int32_t L_26 = V_2;
		if ((!(((uint32_t)L_26) == ((uint32_t)(-1)))))
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_27 = (int32_t)__this->get_count_10();
		int32_t L_28 = (int32_t)((int32_t)((int32_t)L_27+(int32_t)1));
		V_3 = (int32_t)L_28;
		__this->set_count_10(L_28);
		int32_t L_29 = V_3;
		int32_t L_30 = (int32_t)__this->get_threshold_11();
		if ((((int32_t)L_29) <= ((int32_t)L_30)))
		{
			goto IL_00d5;
		}
	}
	{
		NullCheck((Dictionary_2_t2952530300 *)__this);
		((  void (*) (Dictionary_2_t2952530300 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36)->methodPointer)((Dictionary_2_t2952530300 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36));
		int32_t L_31 = V_0;
		Int32U5BU5D_t3230847821* L_32 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_32);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_31&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_32)->max_length))))));
	}

IL_00d5:
	{
		int32_t L_33 = (int32_t)__this->get_emptySlot_9();
		V_2 = (int32_t)L_33;
		int32_t L_34 = V_2;
		if ((!(((uint32_t)L_34) == ((uint32_t)(-1)))))
		{
			goto IL_00fa;
		}
	}
	{
		int32_t L_35 = (int32_t)__this->get_touchedSlots_8();
		int32_t L_36 = (int32_t)L_35;
		V_3 = (int32_t)L_36;
		__this->set_touchedSlots_8(((int32_t)((int32_t)L_36+(int32_t)1)));
		int32_t L_37 = V_3;
		V_2 = (int32_t)L_37;
		goto IL_0111;
	}

IL_00fa:
	{
		LinkU5BU5D_t375419643* L_38 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_39 = V_2;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_39);
		int32_t L_40 = (int32_t)((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_39)))->get_Next_1();
		__this->set_emptySlot_9(L_40);
	}

IL_0111:
	{
		LinkU5BU5D_t375419643* L_41 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_42 = V_2;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
		int32_t L_43 = V_0;
		((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_42)))->set_HashCode_0(L_43);
		LinkU5BU5D_t375419643* L_44 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_45 = V_2;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, L_45);
		Int32U5BU5D_t3230847821* L_46 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_47 = V_1;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, L_47);
		int32_t L_48 = L_47;
		int32_t L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_45)))->set_Next_1(((int32_t)((int32_t)L_49-(int32_t)1)));
		Int32U5BU5D_t3230847821* L_50 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_51 = V_1;
		int32_t L_52 = V_2;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, L_51);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(L_51), (int32_t)((int32_t)((int32_t)L_52+(int32_t)1)));
		UIModelDisplayTypeU5BU5D_t737309086* L_53 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		int32_t L_54 = V_2;
		int32_t L_55 = ___key0;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, L_54);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(L_54), (int32_t)L_55);
		Int32U5BU5D_t3230847821* L_56 = (Int32U5BU5D_t3230847821*)__this->get_valueSlots_7();
		int32_t L_57 = V_2;
		int32_t L_58 = ___value1;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, L_57);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(L_57), (int32_t)L_58);
		int32_t L_59 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_59+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::Clear()
extern "C"  void Dictionary_2_Clear_m1601727223_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method)
{
	{
		__this->set_count_10(0);
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		Int32U5BU5D_t3230847821* L_1 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_1);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		UIModelDisplayTypeU5BU5D_t737309086* L_2 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		UIModelDisplayTypeU5BU5D_t737309086* L_3 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		NullCheck(L_3);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))), /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_4 = (Int32U5BU5D_t3230847821*)__this->get_valueSlots_7();
		Int32U5BU5D_t3230847821* L_5 = (Int32U5BU5D_t3230847821*)__this->get_valueSlots_7();
		NullCheck(L_5);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_4, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))), /*hidden argument*/NULL);
		LinkU5BU5D_t375419643* L_6 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		LinkU5BU5D_t375419643* L_7 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		NullCheck(L_7);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))), /*hidden argument*/NULL);
		__this->set_emptySlot_9((-1));
		__this->set_touchedSlots_8(0);
		int32_t L_8 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_8+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::ContainsKey(TKey)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_ContainsKey_m2504713972_MetadataUsageId;
extern "C"  bool Dictionary_2_ContainsKey_m2504713972_gshared (Dictionary_2_t2952530300 * __this, int32_t ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_ContainsKey_m2504713972_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		goto IL_0016;
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		int32_t L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		Int32U5BU5D_t3230847821* L_5 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_6 = V_0;
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))));
		int32_t L_9 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (int32_t)((int32_t)((int32_t)L_9-(int32_t)1));
		goto IL_0090;
	}

IL_0048:
	{
		LinkU5BU5D_t375419643* L_10 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = (int32_t)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->get_HashCode_0();
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_007e;
		}
	}
	{
		Il2CppObject* L_14 = (Il2CppObject*)__this->get_hcp_12();
		UIModelDisplayTypeU5BU5D_t737309086* L_15 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		int32_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		int32_t L_19 = ___key0;
		NullCheck((Il2CppObject*)L_14);
		bool L_20 = InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_14, (int32_t)L_18, (int32_t)L_19);
		if (!L_20)
		{
			goto IL_007e;
		}
	}
	{
		return (bool)1;
	}

IL_007e:
	{
		LinkU5BU5D_t375419643* L_21 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = (int32_t)((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_22)))->get_Next_1();
		V_1 = (int32_t)L_23;
	}

IL_0090:
	{
		int32_t L_24 = V_1;
		if ((!(((uint32_t)L_24) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m2335285492_gshared (Dictionary_2_t2952530300 * __this, int32_t ___value0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 47));
		EqualityComparer_1_t261675381 * L_0 = ((  EqualityComparer_1_t261675381 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46));
		V_0 = (Il2CppObject*)L_0;
		V_1 = (int32_t)0;
		goto IL_0054;
	}

IL_000d:
	{
		Int32U5BU5D_t3230847821* L_1 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		int32_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_2 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		goto IL_0049;
	}

IL_001d:
	{
		Il2CppObject* L_5 = V_0;
		Int32U5BU5D_t3230847821* L_6 = (Int32U5BU5D_t3230847821*)__this->get_valueSlots_7();
		int32_t L_7 = V_2;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		int32_t L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		int32_t L_10 = ___value0;
		NullCheck((Il2CppObject*)L_5);
		bool L_11 = InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Int32>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 48), (Il2CppObject*)L_5, (int32_t)L_9, (int32_t)L_10);
		if (!L_11)
		{
			goto IL_0037;
		}
	}
	{
		return (bool)1;
	}

IL_0037:
	{
		LinkU5BU5D_t375419643* L_12 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = (int32_t)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_Next_1();
		V_2 = (int32_t)L_14;
	}

IL_0049:
	{
		int32_t L_15 = V_2;
		if ((!(((uint32_t)L_15) == ((uint32_t)(-1)))))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_17 = V_1;
		Int32U5BU5D_t3230847821* L_18 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3237038;
extern Il2CppCodeGenString* _stringLiteral2016261304;
extern Il2CppCodeGenString* _stringLiteral3759850573;
extern Il2CppCodeGenString* _stringLiteral212812367;
extern Il2CppCodeGenString* _stringLiteral961688967;
extern const uint32_t Dictionary_2_GetObjectData_m1320110209_MetadataUsageId;
extern "C"  void Dictionary_2_GetObjectData_m1320110209_gshared (Dictionary_2_t2952530300 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_GetObjectData_m1320110209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2U5BU5D_t390341163* V_0 = NULL;
	{
		SerializationInfo_t2185721892 * L_0 = ___info0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral3237038, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		SerializationInfo_t2185721892 * L_2 = ___info0;
		int32_t L_3 = (int32_t)__this->get_generation_14();
		NullCheck((SerializationInfo_t2185721892 *)L_2);
		SerializationInfo_AddValue_m2348540514((SerializationInfo_t2185721892 *)L_2, (String_t*)_stringLiteral2016261304, (int32_t)L_3, /*hidden argument*/NULL);
		SerializationInfo_t2185721892 * L_4 = ___info0;
		Il2CppObject* L_5 = (Il2CppObject*)__this->get_hcp_12();
		NullCheck((SerializationInfo_t2185721892 *)L_4);
		SerializationInfo_AddValue_m469120675((SerializationInfo_t2185721892 *)L_4, (String_t*)_stringLiteral3759850573, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		V_0 = (KeyValuePair_2U5BU5D_t390341163*)NULL;
		int32_t L_6 = (int32_t)__this->get_count_10();
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_7 = (int32_t)__this->get_count_10();
		V_0 = (KeyValuePair_2U5BU5D_t390341163*)((KeyValuePair_2U5BU5D_t390341163*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 49), (uint32_t)L_7));
		KeyValuePair_2U5BU5D_t390341163* L_8 = V_0;
		NullCheck((Dictionary_2_t2952530300 *)__this);
		((  void (*) (Dictionary_2_t2952530300 *, KeyValuePair_2U5BU5D_t390341163*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((Dictionary_2_t2952530300 *)__this, (KeyValuePair_2U5BU5D_t390341163*)L_8, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
	}

IL_0055:
	{
		SerializationInfo_t2185721892 * L_9 = ___info0;
		Int32U5BU5D_t3230847821* L_10 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_10);
		NullCheck((SerializationInfo_t2185721892 *)L_9);
		SerializationInfo_AddValue_m2348540514((SerializationInfo_t2185721892 *)L_9, (String_t*)_stringLiteral212812367, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))), /*hidden argument*/NULL);
		SerializationInfo_t2185721892 * L_11 = ___info0;
		KeyValuePair_2U5BU5D_t390341163* L_12 = V_0;
		NullCheck((SerializationInfo_t2185721892 *)L_11);
		SerializationInfo_AddValue_m469120675((SerializationInfo_t2185721892 *)L_11, (String_t*)_stringLiteral961688967, (Il2CppObject *)(Il2CppObject *)L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::OnDeserialization(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2016261304;
extern Il2CppCodeGenString* _stringLiteral3759850573;
extern Il2CppCodeGenString* _stringLiteral212812367;
extern Il2CppCodeGenString* _stringLiteral961688967;
extern const uint32_t Dictionary_2_OnDeserialization_m2735007875_MetadataUsageId;
extern "C"  void Dictionary_2_OnDeserialization_m2735007875_gshared (Dictionary_2_t2952530300 * __this, Il2CppObject * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_OnDeserialization_m2735007875_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2U5BU5D_t390341163* V_1 = NULL;
	int32_t V_2 = 0;
	{
		SerializationInfo_t2185721892 * L_0 = (SerializationInfo_t2185721892 *)__this->get_serialization_info_13();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		SerializationInfo_t2185721892 * L_1 = (SerializationInfo_t2185721892 *)__this->get_serialization_info_13();
		NullCheck((SerializationInfo_t2185721892 *)L_1);
		int32_t L_2 = SerializationInfo_GetInt32_m4048035953((SerializationInfo_t2185721892 *)L_1, (String_t*)_stringLiteral2016261304, /*hidden argument*/NULL);
		__this->set_generation_14(L_2);
		SerializationInfo_t2185721892 * L_3 = (SerializationInfo_t2185721892 *)__this->get_serialization_info_13();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 50)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2185721892 *)L_3);
		Il2CppObject * L_5 = SerializationInfo_GetValue_m4125471336((SerializationInfo_t2185721892 *)L_3, (String_t*)_stringLiteral3759850573, (Type_t *)L_4, /*hidden argument*/NULL);
		__this->set_hcp_12(((Il2CppObject*)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35))));
		SerializationInfo_t2185721892 * L_6 = (SerializationInfo_t2185721892 *)__this->get_serialization_info_13();
		NullCheck((SerializationInfo_t2185721892 *)L_6);
		int32_t L_7 = SerializationInfo_GetInt32_m4048035953((SerializationInfo_t2185721892 *)L_6, (String_t*)_stringLiteral212812367, /*hidden argument*/NULL);
		V_0 = (int32_t)L_7;
		SerializationInfo_t2185721892 * L_8 = (SerializationInfo_t2185721892 *)__this->get_serialization_info_13();
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 51)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2185721892 *)L_8);
		Il2CppObject * L_10 = SerializationInfo_GetValue_m4125471336((SerializationInfo_t2185721892 *)L_8, (String_t*)_stringLiteral961688967, (Type_t *)L_9, /*hidden argument*/NULL);
		V_1 = (KeyValuePair_2U5BU5D_t390341163*)((KeyValuePair_2U5BU5D_t390341163*)Castclass(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20)));
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) >= ((int32_t)((int32_t)10))))
		{
			goto IL_0083;
		}
	}
	{
		V_0 = (int32_t)((int32_t)10);
	}

IL_0083:
	{
		int32_t L_12 = V_0;
		NullCheck((Dictionary_2_t2952530300 *)__this);
		((  void (*) (Dictionary_2_t2952530300 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 39)->methodPointer)((Dictionary_2_t2952530300 *)__this, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 39));
		__this->set_count_10(0);
		KeyValuePair_2U5BU5D_t390341163* L_13 = V_1;
		if (!L_13)
		{
			goto IL_00c9;
		}
	}
	{
		V_2 = (int32_t)0;
		goto IL_00c0;
	}

IL_009e:
	{
		KeyValuePair_2U5BU5D_t390341163* L_14 = V_1;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2851311006 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_15)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		KeyValuePair_2U5BU5D_t390341163* L_17 = V_1;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2851311006 *)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((Dictionary_2_t2952530300 *)__this);
		((  void (*) (Dictionary_2_t2952530300 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t2952530300 *)__this, (int32_t)L_16, (int32_t)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		int32_t L_20 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_00c0:
	{
		int32_t L_21 = V_2;
		KeyValuePair_2U5BU5D_t390341163* L_22 = V_1;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_22)->max_length)))))))
		{
			goto IL_009e;
		}
	}

IL_00c9:
	{
		int32_t L_23 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_23+(int32_t)1)));
		__this->set_serialization_info_13((SerializationInfo_t2185721892 *)NULL);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::Remove(TKey)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* UIModelDisplayType_t1891752679_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_Remove_m2197145980_MetadataUsageId;
extern "C"  bool Dictionary_2_Remove_m2197145980_gshared (Dictionary_2_t2952530300 * __this, int32_t ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Remove_m2197145980_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		goto IL_0016;
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		int32_t L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t3230847821* L_6 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))));
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_2 = (int32_t)((int32_t)((int32_t)L_10-(int32_t)1));
		int32_t L_11 = V_2;
		if ((!(((uint32_t)L_11) == ((uint32_t)(-1)))))
		{
			goto IL_004e;
		}
	}
	{
		return (bool)0;
	}

IL_004e:
	{
		V_3 = (int32_t)(-1);
	}

IL_0050:
	{
		LinkU5BU5D_t375419643* L_12 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = (int32_t)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_HashCode_0();
		int32_t L_15 = V_0;
		if ((!(((uint32_t)L_14) == ((uint32_t)L_15))))
		{
			goto IL_0089;
		}
	}
	{
		Il2CppObject* L_16 = (Il2CppObject*)__this->get_hcp_12();
		UIModelDisplayTypeU5BU5D_t737309086* L_17 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		int32_t L_18 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		int32_t L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		int32_t L_21 = ___key0;
		NullCheck((Il2CppObject*)L_16);
		bool L_22 = InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_16, (int32_t)L_20, (int32_t)L_21);
		if (!L_22)
		{
			goto IL_0089;
		}
	}
	{
		goto IL_00a4;
	}

IL_0089:
	{
		int32_t L_23 = V_2;
		V_3 = (int32_t)L_23;
		LinkU5BU5D_t375419643* L_24 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_25 = V_2;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		int32_t L_26 = (int32_t)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_25)))->get_Next_1();
		V_2 = (int32_t)L_26;
		int32_t L_27 = V_2;
		if ((!(((uint32_t)L_27) == ((uint32_t)(-1)))))
		{
			goto IL_0050;
		}
	}

IL_00a4:
	{
		int32_t L_28 = V_2;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_00ad;
		}
	}
	{
		return (bool)0;
	}

IL_00ad:
	{
		int32_t L_29 = (int32_t)__this->get_count_10();
		__this->set_count_10(((int32_t)((int32_t)L_29-(int32_t)1)));
		int32_t L_30 = V_3;
		if ((!(((uint32_t)L_30) == ((uint32_t)(-1)))))
		{
			goto IL_00e2;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_31 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_32 = V_1;
		LinkU5BU5D_t375419643* L_33 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_34 = V_2;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, L_34);
		int32_t L_35 = (int32_t)((L_33)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_34)))->get_Next_1();
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(L_32), (int32_t)((int32_t)((int32_t)L_35+(int32_t)1)));
		goto IL_0104;
	}

IL_00e2:
	{
		LinkU5BU5D_t375419643* L_36 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_37 = V_3;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		LinkU5BU5D_t375419643* L_38 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_39 = V_2;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_39);
		int32_t L_40 = (int32_t)((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_39)))->get_Next_1();
		((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_37)))->set_Next_1(L_40);
	}

IL_0104:
	{
		LinkU5BU5D_t375419643* L_41 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_42 = V_2;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
		int32_t L_43 = (int32_t)__this->get_emptySlot_9();
		((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_42)))->set_Next_1(L_43);
		int32_t L_44 = V_2;
		__this->set_emptySlot_9(L_44);
		LinkU5BU5D_t375419643* L_45 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_46 = V_2;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, L_46);
		((L_45)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_46)))->set_HashCode_0(0);
		UIModelDisplayTypeU5BU5D_t737309086* L_47 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		int32_t L_48 = V_2;
		Initobj (UIModelDisplayType_t1891752679_il2cpp_TypeInfo_var, (&V_4));
		int32_t L_49 = V_4;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, L_48);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(L_48), (int32_t)L_49);
		Int32U5BU5D_t3230847821* L_50 = (Int32U5BU5D_t3230847821*)__this->get_valueSlots_7();
		int32_t L_51 = V_2;
		Initobj (Int32_t1153838500_il2cpp_TypeInfo_var, (&V_5));
		int32_t L_52 = V_5;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, L_51);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(L_51), (int32_t)L_52);
		int32_t L_53 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_53+(int32_t)1)));
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::TryGetValue(TKey,TValue&)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_TryGetValue_m2278230861_MetadataUsageId;
extern "C"  bool Dictionary_2_TryGetValue_m2278230861_gshared (Dictionary_2_t2952530300 * __this, int32_t ___key0, int32_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_TryGetValue_m2278230861_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		goto IL_0016;
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		int32_t L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		Int32U5BU5D_t3230847821* L_5 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_6 = V_0;
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))));
		int32_t L_9 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (int32_t)((int32_t)((int32_t)L_9-(int32_t)1));
		goto IL_00a2;
	}

IL_0048:
	{
		LinkU5BU5D_t375419643* L_10 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = (int32_t)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->get_HashCode_0();
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_0090;
		}
	}
	{
		Il2CppObject* L_14 = (Il2CppObject*)__this->get_hcp_12();
		UIModelDisplayTypeU5BU5D_t737309086* L_15 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		int32_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		int32_t L_19 = ___key0;
		NullCheck((Il2CppObject*)L_14);
		bool L_20 = InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_14, (int32_t)L_18, (int32_t)L_19);
		if (!L_20)
		{
			goto IL_0090;
		}
	}
	{
		int32_t* L_21 = ___value1;
		Int32U5BU5D_t3230847821* L_22 = (Int32U5BU5D_t3230847821*)__this->get_valueSlots_7();
		int32_t L_23 = V_1;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		int32_t L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		(*(int32_t*)L_21) = L_25;
		return (bool)1;
	}

IL_0090:
	{
		LinkU5BU5D_t375419643* L_26 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_27 = V_1;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		int32_t L_28 = (int32_t)((L_26)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_27)))->get_Next_1();
		V_1 = (int32_t)L_28;
	}

IL_00a2:
	{
		int32_t L_29 = V_1;
		if ((!(((uint32_t)L_29) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		int32_t* L_30 = ___value1;
		Initobj (Int32_t1153838500_il2cpp_TypeInfo_var, (&V_2));
		int32_t L_31 = V_2;
		(*(int32_t*)L_30) = L_31;
		return (bool)0;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::get_Keys()
extern "C"  KeyCollection_t284322455 * Dictionary_2_get_Keys_m1011591634_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method)
{
	{
		KeyCollection_t284322455 * L_0 = (KeyCollection_t284322455 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 52));
		((  void (*) (KeyCollection_t284322455 *, Dictionary_2_t2952530300 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 53)->methodPointer)(L_0, (Dictionary_2_t2952530300 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 53));
		return L_0;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::get_Values()
extern "C"  ValueCollection_t1653136013 * Dictionary_2_get_Values_m2094050514_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method)
{
	{
		ValueCollection_t1653136013 * L_0 = (ValueCollection_t1653136013 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 54));
		((  void (*) (ValueCollection_t1653136013 *, Dictionary_2_t2952530300 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 55)->methodPointer)(L_0, (Dictionary_2_t2952530300 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 55));
		return L_0;
	}
}
// TKey System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::ToTKey(System.Object)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern Il2CppCodeGenString* _stringLiteral3927817532;
extern const uint32_t Dictionary_2_ToTKey_m651879757_MetadataUsageId;
extern "C"  int32_t Dictionary_2_ToTKey_m651879757_gshared (Dictionary_2_t2952530300 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_ToTKey_m651879757_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___key0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 56)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m138640077(NULL /*static, unused*/, (String_t*)_stringLiteral3927817532, (String_t*)L_4, /*hidden argument*/NULL);
		ArgumentException_t928607144 * L_6 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_6, (String_t*)L_5, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0040:
	{
		Il2CppObject * L_7 = ___key0;
		return ((*(int32_t*)((int32_t*)UnBox (L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10)))));
	}
}
// TValue System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::ToTValue(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3927817532;
extern Il2CppCodeGenString* _stringLiteral111972721;
extern const uint32_t Dictionary_2_ToTValue_m1939299405_MetadataUsageId;
extern "C"  int32_t Dictionary_2_ToTValue_m1939299405_gshared (Dictionary_2_t2952530300 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_ToTValue_m1939299405_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Il2CppObject * L_0 = ___value0;
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 57)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_1);
		bool L_2 = Type_get_IsValueType_m1914757235((Type_t *)L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		Initobj (Int32_t1153838500_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		Il2CppObject * L_4 = ___value0;
		if (((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14))))
		{
			goto IL_0053;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 57)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m138640077(NULL /*static, unused*/, (String_t*)_stringLiteral3927817532, (String_t*)L_6, /*hidden argument*/NULL);
		ArgumentException_t928607144 * L_8 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_8, (String_t*)L_7, (String_t*)_stringLiteral111972721, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0053:
	{
		Il2CppObject * L_9 = ___value0;
		return ((*(int32_t*)((int32_t*)UnBox (L_9, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14)))));
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m2601326431_gshared (Dictionary_2_t2952530300 * __this, KeyValuePair_2_t2851311006  ___pair0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2851311006 *)(&___pair0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((Dictionary_2_t2952530300 *)__this);
		bool L_1 = ((  bool (*) (Dictionary_2_t2952530300 *, int32_t, int32_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 58)->methodPointer)((Dictionary_2_t2952530300 *)__this, (int32_t)L_0, (int32_t*)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 58));
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		return (bool)0;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 47));
		EqualityComparer_1_t261675381 * L_2 = ((  EqualityComparer_1_t261675381 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46));
		int32_t L_3 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2851311006 *)(&___pair0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		int32_t L_4 = V_0;
		NullCheck((EqualityComparer_1_t261675381 *)L_2);
		bool L_5 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int32>::Equals(T,T) */, (EqualityComparer_1_t261675381 *)L_2, (int32_t)L_3, (int32_t)L_4);
		return L_5;
	}
}
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t4269853692  Dictionary_2_GetEnumerator_m1011803368_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4269853692  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m1938982213(&L_0, (Dictionary_2_t2952530300 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 32));
		return L_0;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m1264190775_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), &L_1);
		int32_t L_3 = ___value1;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), &L_4);
		DictionaryEntry_t1751606614  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2600671860(&L_6, (Il2CppObject *)L_2, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m2207730996_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t1674540875 *)__this);
		((  void (*) (Dictionary_2_t1674540875 *, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1674540875 *)__this, (int32_t)((int32_t)10), (Il2CppObject*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3801739691_gshared (Dictionary_2_t1674540875 * __this, Il2CppObject* ___comparer0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___comparer0;
		NullCheck((Dictionary_2_t1674540875 *)__this);
		((  void (*) (Dictionary_2_t1674540875 *, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1674540875 *)__this, (int32_t)((int32_t)10), (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m1905539300_gshared (Dictionary_2_t1674540875 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___dictionary0;
		NullCheck((Dictionary_2_t1674540875 *)__this);
		((  void (*) (Dictionary_2_t1674540875 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Dictionary_2_t1674540875 *)__this, (Il2CppObject*)L_0, (Il2CppObject*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m3409368197_gshared (Dictionary_2_t1674540875 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		NullCheck((Dictionary_2_t1674540875 *)__this);
		((  void (*) (Dictionary_2_t1674540875 *, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1674540875 *)__this, (int32_t)L_0, (Il2CppObject*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral447049878;
extern const uint32_t Dictionary_2__ctor_m1762102681_MetadataUsageId;
extern "C"  void Dictionary_2__ctor_m1762102681_gshared (Dictionary_2_t1674540875 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2__ctor_m1762102681_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t1573321581  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Il2CppObject* V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral447049878, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Il2CppObject* L_2 = ___dictionary0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_3 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_2);
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		Il2CppObject* L_5 = ___comparer1;
		NullCheck((Dictionary_2_t1674540875 *)__this);
		((  void (*) (Dictionary_2_t1674540875 *, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1674540875 *)__this, (int32_t)L_4, (Il2CppObject*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Il2CppObject* L_6 = ___dictionary0;
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject* L_7 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_6);
		V_2 = (Il2CppObject*)L_7;
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004d;
		}

IL_0032:
		{
			Il2CppObject* L_8 = V_2;
			NullCheck((Il2CppObject*)L_8);
			KeyValuePair_2_t1573321581  L_9 = InterfaceFuncInvoker0< KeyValuePair_2_t1573321581  >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_8);
			V_1 = (KeyValuePair_2_t1573321581 )L_9;
			int32_t L_10 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1573321581 *)(&V_1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			Il2CppObject * L_11 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1573321581 *)(&V_1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			NullCheck((Dictionary_2_t1674540875 *)__this);
			((  void (*) (Dictionary_2_t1674540875 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t1674540875 *)__this, (int32_t)L_10, (Il2CppObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		}

IL_004d:
		{
			Il2CppObject* L_12 = V_2;
			NullCheck((Il2CppObject *)L_12);
			bool L_13 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
			if (L_13)
			{
				goto IL_0032;
			}
		}

IL_0058:
		{
			IL2CPP_LEAVE(0x68, FINALLY_005d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_005d;
	}

FINALLY_005d:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_14 = V_2;
			if (L_14)
			{
				goto IL_0061;
			}
		}

IL_0060:
		{
			IL2CPP_END_FINALLY(93)
		}

IL_0061:
		{
			Il2CppObject* L_15 = V_2;
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			IL2CPP_END_FINALLY(93)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(93)
	{
		IL2CPP_JUMP_TBL(0x68, IL_0068)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0068:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2864732277_gshared (Dictionary_2_t1674540875 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		SerializationInfo_t2185721892 * L_0 = ___info0;
		__this->set_serialization_info_13(L_0);
		return;
	}
}
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m927103204_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t1674540875 *)__this);
		KeyCollection_t3301300326 * L_0 = ((  KeyCollection_t3301300326 * (*) (Dictionary_2_t1674540875 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t1674540875 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_0;
	}
}
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2066774464_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t1674540875 *)__this);
		ValueCollection_t375146588 * L_0 = ((  ValueCollection_t375146588 * (*) (Dictionary_2_t1674540875 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t1674540875 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m4142730098_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t1674540875 *)__this);
		KeyCollection_t3301300326 * L_0 = ((  KeyCollection_t3301300326 * (*) (Dictionary_2_t1674540875 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t1674540875 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_0;
	}
}
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m2096495584_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t1674540875 *)__this);
		ValueCollection_t375146588 * L_0 = ((  ValueCollection_t375146588 * (*) (Dictionary_2_t1674540875 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t1674540875 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2438688745_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m394128880_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m3182515722_gshared (Dictionary_2_t1674540875 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		if (!((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))
		{
			goto IL_002f;
		}
	}
	{
		Il2CppObject * L_1 = ___key0;
		NullCheck((Dictionary_2_t1674540875 *)__this);
		bool L_2 = ((  bool (*) (Dictionary_2_t1674540875 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Dictionary_2_t1674540875 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Il2CppObject * L_3 = ___key0;
		NullCheck((Dictionary_2_t1674540875 *)__this);
		int32_t L_4 = ((  int32_t (*) (Dictionary_2_t1674540875 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1674540875 *)__this, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((Dictionary_2_t1674540875 *)__this);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (Dictionary_2_t1674540875 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((Dictionary_2_t1674540875 *)__this, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_5;
	}

IL_002f:
	{
		return NULL;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m1222269049_gshared (Dictionary_2_t1674540875 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		NullCheck((Dictionary_2_t1674540875 *)__this);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t1674540875 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1674540875 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		Il2CppObject * L_2 = ___value1;
		NullCheck((Dictionary_2_t1674540875 *)__this);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Dictionary_2_t1674540875 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((Dictionary_2_t1674540875 *)__this, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		NullCheck((Dictionary_2_t1674540875 *)__this);
		((  void (*) (Dictionary_2_t1674540875 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((Dictionary_2_t1674540875 *)__this, (int32_t)L_1, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3054681656_gshared (Dictionary_2_t1674540875 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		NullCheck((Dictionary_2_t1674540875 *)__this);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t1674540875 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1674540875 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		Il2CppObject * L_2 = ___value1;
		NullCheck((Dictionary_2_t1674540875 *)__this);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Dictionary_2_t1674540875 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((Dictionary_2_t1674540875 *)__this, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		NullCheck((Dictionary_2_t1674540875 *)__this);
		((  void (*) (Dictionary_2_t1674540875 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t1674540875 *)__this, (int32_t)L_1, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_System_Collections_IDictionary_Contains_m3946577146_MetadataUsageId;
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m3946577146_gshared (Dictionary_2_t1674540875 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_System_Collections_IDictionary_Contains_m3946577146_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___key0;
		if (!((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject * L_3 = ___key0;
		NullCheck((Dictionary_2_t1674540875 *)__this);
		bool L_4 = ((  bool (*) (Dictionary_2_t1674540875 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Dictionary_2_t1674540875 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_4;
	}

IL_0029:
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_System_Collections_IDictionary_Remove_m391570615_MetadataUsageId;
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m391570615_gshared (Dictionary_2_t1674540875 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_System_Collections_IDictionary_Remove_m391570615_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___key0;
		if (!((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject * L_3 = ___key0;
		NullCheck((Dictionary_2_t1674540875 *)__this);
		((  bool (*) (Dictionary_2_t1674540875 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((Dictionary_2_t1674540875 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1116562586_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1418878540_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1619626078_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3736308173_gshared (Dictionary_2_t1674540875 * __this, KeyValuePair_2_t1573321581  ___keyValuePair0, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1573321581 *)(&___keyValuePair0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1573321581 *)(&___keyValuePair0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((Dictionary_2_t1674540875 *)__this);
		((  void (*) (Dictionary_2_t1674540875 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t1674540875 *)__this, (int32_t)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2088441049_gshared (Dictionary_2_t1674540875 * __this, KeyValuePair_2_t1573321581  ___keyValuePair0, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1573321581  L_0 = ___keyValuePair0;
		NullCheck((Dictionary_2_t1674540875 *)__this);
		bool L_1 = ((  bool (*) (Dictionary_2_t1674540875 *, KeyValuePair_2_t1573321581 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((Dictionary_2_t1674540875 *)__this, (KeyValuePair_2_t1573321581 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1691385969_gshared (Dictionary_2_t1674540875 * __this, KeyValuePair_2U5BU5D_t2563117120* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t2563117120* L_0 = ___array0;
		int32_t L_1 = ___index1;
		NullCheck((Dictionary_2_t1674540875 *)__this);
		((  void (*) (Dictionary_2_t1674540875 *, KeyValuePair_2U5BU5D_t2563117120*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((Dictionary_2_t1674540875 *)__this, (KeyValuePair_2U5BU5D_t2563117120*)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3981090366_gshared (Dictionary_2_t1674540875 * __this, KeyValuePair_2_t1573321581  ___keyValuePair0, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1573321581  L_0 = ___keyValuePair0;
		NullCheck((Dictionary_2_t1674540875 *)__this);
		bool L_1 = ((  bool (*) (Dictionary_2_t1674540875 *, KeyValuePair_2_t1573321581 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((Dictionary_2_t1674540875 *)__this, (KeyValuePair_2_t1573321581 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (bool)0;
	}

IL_000e:
	{
		int32_t L_2 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1573321581 *)(&___keyValuePair0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((Dictionary_2_t1674540875 *)__this);
		bool L_3 = ((  bool (*) (Dictionary_2_t1674540875 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((Dictionary_2_t1674540875 *)__this, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		return L_3;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* DictionaryEntryU5BU5D_t479206547_il2cpp_TypeInfo_var;
extern const uint32_t Dictionary_2_System_Collections_ICollection_CopyTo_m1001595536_MetadataUsageId;
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m1001595536_gshared (Dictionary_2_t1674540875 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_System_Collections_ICollection_CopyTo_m1001595536_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2U5BU5D_t2563117120* V_0 = NULL;
	DictionaryEntryU5BU5D_t479206547* V_1 = NULL;
	int32_t G_B5_0 = 0;
	DictionaryEntryU5BU5D_t479206547* G_B5_1 = NULL;
	Dictionary_2_t1674540875 * G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	DictionaryEntryU5BU5D_t479206547* G_B4_1 = NULL;
	Dictionary_2_t1674540875 * G_B4_2 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (KeyValuePair_2U5BU5D_t2563117120*)((KeyValuePair_2U5BU5D_t2563117120*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20)));
		KeyValuePair_2U5BU5D_t2563117120* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		KeyValuePair_2U5BU5D_t2563117120* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((Dictionary_2_t1674540875 *)__this);
		((  void (*) (Dictionary_2_t1674540875 *, KeyValuePair_2U5BU5D_t2563117120*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((Dictionary_2_t1674540875 *)__this, (KeyValuePair_2U5BU5D_t2563117120*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}

IL_0016:
	{
		Il2CppArray * L_4 = ___array0;
		int32_t L_5 = ___index1;
		NullCheck((Dictionary_2_t1674540875 *)__this);
		((  void (*) (Dictionary_2_t1674540875 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((Dictionary_2_t1674540875 *)__this, (Il2CppArray *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		Il2CppArray * L_6 = ___array0;
		V_1 = (DictionaryEntryU5BU5D_t479206547*)((DictionaryEntryU5BU5D_t479206547*)IsInst(L_6, DictionaryEntryU5BU5D_t479206547_il2cpp_TypeInfo_var));
		DictionaryEntryU5BU5D_t479206547* L_7 = V_1;
		if (!L_7)
		{
			goto IL_0051;
		}
	}
	{
		DictionaryEntryU5BU5D_t479206547* L_8 = V_1;
		int32_t L_9 = ___index1;
		Transform_1_t906965624 * L_10 = ((Dictionary_2_t1674540875_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22)->static_fields)->get_U3CU3Ef__amU24cacheB_15();
		G_B4_0 = L_9;
		G_B4_1 = L_8;
		G_B4_2 = ((Dictionary_2_t1674540875 *)(__this));
		if (L_10)
		{
			G_B5_0 = L_9;
			G_B5_1 = L_8;
			G_B5_2 = ((Dictionary_2_t1674540875 *)(__this));
			goto IL_0046;
		}
	}
	{
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		Transform_1_t906965624 * L_12 = (Transform_1_t906965624 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 24));
		((  void (*) (Transform_1_t906965624 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)(L_12, (Il2CppObject *)NULL, (IntPtr_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		((Dictionary_2_t1674540875_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22)->static_fields)->set_U3CU3Ef__amU24cacheB_15(L_12);
		G_B5_0 = G_B4_0;
		G_B5_1 = G_B4_1;
		G_B5_2 = ((Dictionary_2_t1674540875 *)(G_B4_2));
	}

IL_0046:
	{
		Transform_1_t906965624 * L_13 = ((Dictionary_2_t1674540875_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22)->static_fields)->get_U3CU3Ef__amU24cacheB_15();
		NullCheck((Dictionary_2_t1674540875 *)G_B5_2);
		((  void (*) (Dictionary_2_t1674540875 *, DictionaryEntryU5BU5D_t479206547*, int32_t, Transform_1_t906965624 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)((Dictionary_2_t1674540875 *)G_B5_2, (DictionaryEntryU5BU5D_t479206547*)G_B5_1, (int32_t)G_B5_0, (Transform_1_t906965624 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		return;
	}

IL_0051:
	{
		Il2CppArray * L_14 = ___array0;
		int32_t L_15 = ___index1;
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		Transform_1_t728680591 * L_17 = (Transform_1_t728680591 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 28));
		((  void (*) (Transform_1_t728680591 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)(L_17, (Il2CppObject *)NULL, (IntPtr_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		NullCheck((Dictionary_2_t1674540875 *)__this);
		((  void (*) (Dictionary_2_t1674540875 *, Il2CppArray *, int32_t, Transform_1_t728680591 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((Dictionary_2_t1674540875 *)__this, (Il2CppArray *)L_14, (int32_t)L_15, (Transform_1_t728680591 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3893231391_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2991864267  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m2965822312(&L_0, (Dictionary_2_t1674540875 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 32));
		Enumerator_t2991864267  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 31), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3784229078_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2991864267  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m2965822312(&L_0, (Dictionary_2_t1674540875 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 32));
		Enumerator_t2991864267  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 31), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1153092451_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method)
{
	{
		ShimEnumerator_t1390318902 * L_0 = (ShimEnumerator_t1390318902 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 33));
		((  void (*) (ShimEnumerator_t1390318902 *, Dictionary_2_t1674540875 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34)->methodPointer)(L_0, (Dictionary_2_t1674540875 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m2830948948_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_count_10();
		return L_0;
	}
}
// TValue System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::get_Item(TKey)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* KeyNotFoundException_t876339989_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_get_Item_m1870284851_MetadataUsageId;
extern "C"  Il2CppObject * Dictionary_2_get_Item_m1870284851_gshared (Dictionary_2_t1674540875 * __this, int32_t ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_get_Item_m1870284851_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		goto IL_0016;
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		int32_t L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		Int32U5BU5D_t3230847821* L_5 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_6 = V_0;
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))));
		int32_t L_9 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (int32_t)((int32_t)((int32_t)L_9-(int32_t)1));
		goto IL_009b;
	}

IL_0048:
	{
		LinkU5BU5D_t375419643* L_10 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = (int32_t)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->get_HashCode_0();
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_0089;
		}
	}
	{
		Il2CppObject* L_14 = (Il2CppObject*)__this->get_hcp_12();
		UIModelDisplayTypeU5BU5D_t737309086* L_15 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		int32_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		int32_t L_19 = ___key0;
		NullCheck((Il2CppObject*)L_14);
		bool L_20 = InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_14, (int32_t)L_18, (int32_t)L_19);
		if (!L_20)
		{
			goto IL_0089;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_21 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		int32_t L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = L_22;
		Il2CppObject * L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		return L_24;
	}

IL_0089:
	{
		LinkU5BU5D_t375419643* L_25 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_26 = V_1;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		int32_t L_27 = (int32_t)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))->get_Next_1();
		V_1 = (int32_t)L_27;
	}

IL_009b:
	{
		int32_t L_28 = V_1;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		KeyNotFoundException_t876339989 * L_29 = (KeyNotFoundException_t876339989 *)il2cpp_codegen_object_new(KeyNotFoundException_t876339989_il2cpp_TypeInfo_var);
		KeyNotFoundException__ctor_m3542895460(L_29, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_29);
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::set_Item(TKey,TValue)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_set_Item_m248666100_MetadataUsageId;
extern "C"  void Dictionary_2_set_Item_m248666100_gshared (Dictionary_2_t1674540875 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_set_Item_m248666100_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		goto IL_0016;
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		int32_t L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t3230847821* L_6 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))));
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_2 = (int32_t)((int32_t)((int32_t)L_10-(int32_t)1));
		V_3 = (int32_t)(-1);
		int32_t L_11 = V_2;
		if ((((int32_t)L_11) == ((int32_t)(-1))))
		{
			goto IL_00a2;
		}
	}

IL_004e:
	{
		LinkU5BU5D_t375419643* L_12 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = (int32_t)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_HashCode_0();
		int32_t L_15 = V_0;
		if ((!(((uint32_t)L_14) == ((uint32_t)L_15))))
		{
			goto IL_0087;
		}
	}
	{
		Il2CppObject* L_16 = (Il2CppObject*)__this->get_hcp_12();
		UIModelDisplayTypeU5BU5D_t737309086* L_17 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		int32_t L_18 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		int32_t L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		int32_t L_21 = ___key0;
		NullCheck((Il2CppObject*)L_16);
		bool L_22 = InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_16, (int32_t)L_20, (int32_t)L_21);
		if (!L_22)
		{
			goto IL_0087;
		}
	}
	{
		goto IL_00a2;
	}

IL_0087:
	{
		int32_t L_23 = V_2;
		V_3 = (int32_t)L_23;
		LinkU5BU5D_t375419643* L_24 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_25 = V_2;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		int32_t L_26 = (int32_t)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_25)))->get_Next_1();
		V_2 = (int32_t)L_26;
		int32_t L_27 = V_2;
		if ((!(((uint32_t)L_27) == ((uint32_t)(-1)))))
		{
			goto IL_004e;
		}
	}

IL_00a2:
	{
		int32_t L_28 = V_2;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_0166;
		}
	}
	{
		int32_t L_29 = (int32_t)__this->get_count_10();
		int32_t L_30 = (int32_t)((int32_t)((int32_t)L_29+(int32_t)1));
		V_4 = (int32_t)L_30;
		__this->set_count_10(L_30);
		int32_t L_31 = V_4;
		int32_t L_32 = (int32_t)__this->get_threshold_11();
		if ((((int32_t)L_31) <= ((int32_t)L_32)))
		{
			goto IL_00de;
		}
	}
	{
		NullCheck((Dictionary_2_t1674540875 *)__this);
		((  void (*) (Dictionary_2_t1674540875 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36)->methodPointer)((Dictionary_2_t1674540875 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36));
		int32_t L_33 = V_0;
		Int32U5BU5D_t3230847821* L_34 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_34);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_33&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_34)->max_length))))));
	}

IL_00de:
	{
		int32_t L_35 = (int32_t)__this->get_emptySlot_9();
		V_2 = (int32_t)L_35;
		int32_t L_36 = V_2;
		if ((!(((uint32_t)L_36) == ((uint32_t)(-1)))))
		{
			goto IL_0105;
		}
	}
	{
		int32_t L_37 = (int32_t)__this->get_touchedSlots_8();
		int32_t L_38 = (int32_t)L_37;
		V_4 = (int32_t)L_38;
		__this->set_touchedSlots_8(((int32_t)((int32_t)L_38+(int32_t)1)));
		int32_t L_39 = V_4;
		V_2 = (int32_t)L_39;
		goto IL_011c;
	}

IL_0105:
	{
		LinkU5BU5D_t375419643* L_40 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_41 = V_2;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, L_41);
		int32_t L_42 = (int32_t)((L_40)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_41)))->get_Next_1();
		__this->set_emptySlot_9(L_42);
	}

IL_011c:
	{
		LinkU5BU5D_t375419643* L_43 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_44 = V_2;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		Int32U5BU5D_t3230847821* L_45 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_46 = V_1;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, L_46);
		int32_t L_47 = L_46;
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_44)))->set_Next_1(((int32_t)((int32_t)L_48-(int32_t)1)));
		Int32U5BU5D_t3230847821* L_49 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_50 = V_1;
		int32_t L_51 = V_2;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, L_50);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(L_50), (int32_t)((int32_t)((int32_t)L_51+(int32_t)1)));
		LinkU5BU5D_t375419643* L_52 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_53 = V_2;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, L_53);
		int32_t L_54 = V_0;
		((L_52)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_53)))->set_HashCode_0(L_54);
		UIModelDisplayTypeU5BU5D_t737309086* L_55 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		int32_t L_56 = V_2;
		int32_t L_57 = ___key0;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, L_56);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(L_56), (int32_t)L_57);
		goto IL_01b5;
	}

IL_0166:
	{
		int32_t L_58 = V_3;
		if ((((int32_t)L_58) == ((int32_t)(-1))))
		{
			goto IL_01b5;
		}
	}
	{
		LinkU5BU5D_t375419643* L_59 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_60 = V_3;
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, L_60);
		LinkU5BU5D_t375419643* L_61 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_62 = V_2;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, L_62);
		int32_t L_63 = (int32_t)((L_61)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_62)))->get_Next_1();
		((L_59)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_60)))->set_Next_1(L_63);
		LinkU5BU5D_t375419643* L_64 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_65 = V_2;
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, L_65);
		Int32U5BU5D_t3230847821* L_66 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_67 = V_1;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, L_67);
		int32_t L_68 = L_67;
		int32_t L_69 = (L_66)->GetAt(static_cast<il2cpp_array_size_t>(L_68));
		((L_64)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_65)))->set_Next_1(((int32_t)((int32_t)L_69-(int32_t)1)));
		Int32U5BU5D_t3230847821* L_70 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_71 = V_1;
		int32_t L_72 = V_2;
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, L_71);
		(L_70)->SetAt(static_cast<il2cpp_array_size_t>(L_71), (int32_t)((int32_t)((int32_t)L_72+(int32_t)1)));
	}

IL_01b5:
	{
		ObjectU5BU5D_t1108656482* L_73 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		int32_t L_74 = V_2;
		Il2CppObject * L_75 = ___value1;
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, L_74);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(L_74), (Il2CppObject *)L_75);
		int32_t L_76 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_76+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4227142842;
extern const uint32_t Dictionary_2_Init_m298353836_MetadataUsageId;
extern "C"  void Dictionary_2_Init_m298353836_gshared (Dictionary_2_t1674540875 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Init_m298353836_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Dictionary_2_t1674540875 * G_B4_0 = NULL;
	Dictionary_2_t1674540875 * G_B3_0 = NULL;
	Il2CppObject* G_B5_0 = NULL;
	Dictionary_2_t1674540875 * G_B5_1 = NULL;
	{
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_1 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_1, (String_t*)_stringLiteral4227142842, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Il2CppObject* L_2 = ___hcp1;
		G_B3_0 = ((Dictionary_2_t1674540875 *)(__this));
		if (!L_2)
		{
			G_B4_0 = ((Dictionary_2_t1674540875 *)(__this));
			goto IL_0021;
		}
	}
	{
		Il2CppObject* L_3 = ___hcp1;
		V_0 = (Il2CppObject*)L_3;
		Il2CppObject* L_4 = V_0;
		G_B5_0 = L_4;
		G_B5_1 = ((Dictionary_2_t1674540875 *)(G_B3_0));
		goto IL_0026;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 38));
		EqualityComparer_1_t999589560 * L_5 = ((  EqualityComparer_1_t999589560 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 37)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 37));
		G_B5_0 = ((Il2CppObject*)(L_5));
		G_B5_1 = ((Dictionary_2_t1674540875 *)(G_B4_0));
	}

IL_0026:
	{
		NullCheck(G_B5_1);
		G_B5_1->set_hcp_12(G_B5_0);
		int32_t L_6 = ___capacity0;
		if (L_6)
		{
			goto IL_0035;
		}
	}
	{
		___capacity0 = (int32_t)((int32_t)10);
	}

IL_0035:
	{
		int32_t L_7 = ___capacity0;
		___capacity0 = (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)((float)((float)(((float)((float)L_7)))/(float)(0.9f))))))+(int32_t)1));
		int32_t L_8 = ___capacity0;
		NullCheck((Dictionary_2_t1674540875 *)__this);
		((  void (*) (Dictionary_2_t1674540875 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 39)->methodPointer)((Dictionary_2_t1674540875 *)__this, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 39));
		__this->set_generation_14(0);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::InitArrays(System.Int32)
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern Il2CppClass* LinkU5BU5D_t375419643_il2cpp_TypeInfo_var;
extern const uint32_t Dictionary_2_InitArrays_m2456312971_MetadataUsageId;
extern "C"  void Dictionary_2_InitArrays_m2456312971_gshared (Dictionary_2_t1674540875 * __this, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_InitArrays_m2456312971_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___size0;
		__this->set_table_4(((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)L_0)));
		int32_t L_1 = ___size0;
		__this->set_linkSlots_5(((LinkU5BU5D_t375419643*)SZArrayNew(LinkU5BU5D_t375419643_il2cpp_TypeInfo_var, (uint32_t)L_1)));
		__this->set_emptySlot_9((-1));
		int32_t L_2 = ___size0;
		__this->set_keySlots_6(((UIModelDisplayTypeU5BU5D_t737309086*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 40), (uint32_t)L_2)));
		int32_t L_3 = ___size0;
		__this->set_valueSlots_7(((ObjectU5BU5D_t1108656482*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 41), (uint32_t)L_3)));
		__this->set_touchedSlots_8(0);
		Int32U5BU5D_t3230847821* L_4 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_4);
		__this->set_threshold_11((((int32_t)((int32_t)((float)((float)(((float)((float)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length)))))))*(float)(0.9f)))))));
		int32_t L_5 = (int32_t)__this->get_threshold_11();
		if (L_5)
		{
			goto IL_0074;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_6 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_6);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0074;
		}
	}
	{
		__this->set_threshold_11(1);
	}

IL_0074:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::CopyToCheck(System.Array,System.Int32)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern Il2CppCodeGenString* _stringLiteral1142730282;
extern Il2CppCodeGenString* _stringLiteral2802514764;
extern const uint32_t Dictionary_2_CopyToCheck_m2889673031_MetadataUsageId;
extern "C"  void Dictionary_2_CopyToCheck_m2889673031_gshared (Dictionary_2_t1674540875 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_CopyToCheck_m2889673031_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppArray * L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		int32_t L_4 = ___index1;
		Il2CppArray * L_5 = ___array0;
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)L_6)))
		{
			goto IL_003a;
		}
	}
	{
		ArgumentException_t928607144 * L_7 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_7, (String_t*)_stringLiteral1142730282, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003a:
	{
		Il2CppArray * L_8 = ___array0;
		NullCheck((Il2CppArray *)L_8);
		int32_t L_9 = Array_get_Length_m1203127607((Il2CppArray *)L_8, /*hidden argument*/NULL);
		int32_t L_10 = ___index1;
		NullCheck((Dictionary_2_t1674540875 *)__this);
		int32_t L_11 = ((  int32_t (*) (Dictionary_2_t1674540875 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 42)->methodPointer)((Dictionary_2_t1674540875 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 42));
		if ((((int32_t)((int32_t)((int32_t)L_9-(int32_t)L_10))) >= ((int32_t)L_11)))
		{
			goto IL_0058;
		}
	}
	{
		ArgumentException_t928607144 * L_12 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_12, (String_t*)_stringLiteral2802514764, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0058:
	{
		return;
	}
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t1573321581  Dictionary_2_make_pair_m4019096795_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_t1573321581  L_2;
		memset(&L_2, 0, sizeof(L_2));
		KeyValuePair_2__ctor_m2016324438(&L_2, (int32_t)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 44));
		return L_2;
	}
}
// TKey System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m3431741315_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		return L_0;
	}
}
// TValue System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m3289262495_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value1;
		return L_0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m2879821992_gshared (Dictionary_2_t1674540875 * __this, KeyValuePair_2U5BU5D_t2563117120* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t2563117120* L_0 = ___array0;
		int32_t L_1 = ___index1;
		NullCheck((Dictionary_2_t1674540875 *)__this);
		((  void (*) (Dictionary_2_t1674540875 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((Dictionary_2_t1674540875 *)__this, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		KeyValuePair_2U5BU5D_t2563117120* L_2 = ___array0;
		int32_t L_3 = ___index1;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		Transform_1_t728680591 * L_5 = (Transform_1_t728680591 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 28));
		((  void (*) (Transform_1_t728680591 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)(L_5, (Il2CppObject *)NULL, (IntPtr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		NullCheck((Dictionary_2_t1674540875 *)__this);
		((  void (*) (Dictionary_2_t1674540875 *, KeyValuePair_2U5BU5D_t2563117120*, int32_t, Transform_1_t728680591 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 45)->methodPointer)((Dictionary_2_t1674540875 *)__this, (KeyValuePair_2U5BU5D_t2563117120*)L_2, (int32_t)L_3, (Transform_1_t728680591 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 45));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::Resize()
extern Il2CppClass* Hashtable_t1407064410_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern Il2CppClass* LinkU5BU5D_t375419643_il2cpp_TypeInfo_var;
extern const uint32_t Dictionary_2_Resize_m4072948100_MetadataUsageId;
extern "C"  void Dictionary_2_Resize_m4072948100_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Resize_m4072948100_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Int32U5BU5D_t3230847821* V_1 = NULL;
	LinkU5BU5D_t375419643* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	UIModelDisplayTypeU5BU5D_t737309086* V_7 = NULL;
	ObjectU5BU5D_t1108656482* V_8 = NULL;
	int32_t V_9 = 0;
	{
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Hashtable_t1407064410_il2cpp_TypeInfo_var);
		int32_t L_1 = Hashtable_ToPrime_m840372929(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))<<(int32_t)1))|(int32_t)1)), /*hidden argument*/NULL);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		V_1 = (Int32U5BU5D_t3230847821*)((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)L_2));
		int32_t L_3 = V_0;
		V_2 = (LinkU5BU5D_t375419643*)((LinkU5BU5D_t375419643*)SZArrayNew(LinkU5BU5D_t375419643_il2cpp_TypeInfo_var, (uint32_t)L_3));
		V_3 = (int32_t)0;
		goto IL_00b1;
	}

IL_0027:
	{
		Int32U5BU5D_t3230847821* L_4 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_5 = V_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		int32_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_4 = (int32_t)((int32_t)((int32_t)L_7-(int32_t)1));
		goto IL_00a5;
	}

IL_0038:
	{
		LinkU5BU5D_t375419643* L_8 = V_2;
		int32_t L_9 = V_4;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		Il2CppObject* L_10 = (Il2CppObject*)__this->get_hcp_12();
		UIModelDisplayTypeU5BU5D_t737309086* L_11 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		int32_t L_12 = V_4;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Il2CppObject*)L_10);
		int32_t L_15 = InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_10, (int32_t)L_14);
		int32_t L_16 = (int32_t)((int32_t)((int32_t)L_15|(int32_t)((int32_t)-2147483648LL)));
		V_9 = (int32_t)L_16;
		((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9)))->set_HashCode_0(L_16);
		int32_t L_17 = V_9;
		V_5 = (int32_t)L_17;
		int32_t L_18 = V_5;
		int32_t L_19 = V_0;
		V_6 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_18&(int32_t)((int32_t)2147483647LL)))%(int32_t)L_19));
		LinkU5BU5D_t375419643* L_20 = V_2;
		int32_t L_21 = V_4;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		Int32U5BU5D_t3230847821* L_22 = V_1;
		int32_t L_23 = V_6;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		int32_t L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_21)))->set_Next_1(((int32_t)((int32_t)L_25-(int32_t)1)));
		Int32U5BU5D_t3230847821* L_26 = V_1;
		int32_t L_27 = V_6;
		int32_t L_28 = V_4;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(L_27), (int32_t)((int32_t)((int32_t)L_28+(int32_t)1)));
		LinkU5BU5D_t375419643* L_29 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_30 = V_4;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
		int32_t L_31 = (int32_t)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_30)))->get_Next_1();
		V_4 = (int32_t)L_31;
	}

IL_00a5:
	{
		int32_t L_32 = V_4;
		if ((!(((uint32_t)L_32) == ((uint32_t)(-1)))))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_33 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00b1:
	{
		int32_t L_34 = V_3;
		Int32U5BU5D_t3230847821* L_35 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_35);
		if ((((int32_t)L_34) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_35)->max_length)))))))
		{
			goto IL_0027;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_36 = V_1;
		__this->set_table_4(L_36);
		LinkU5BU5D_t375419643* L_37 = V_2;
		__this->set_linkSlots_5(L_37);
		int32_t L_38 = V_0;
		V_7 = (UIModelDisplayTypeU5BU5D_t737309086*)((UIModelDisplayTypeU5BU5D_t737309086*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 40), (uint32_t)L_38));
		int32_t L_39 = V_0;
		V_8 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 41), (uint32_t)L_39));
		UIModelDisplayTypeU5BU5D_t737309086* L_40 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		UIModelDisplayTypeU5BU5D_t737309086* L_41 = V_7;
		int32_t L_42 = (int32_t)__this->get_touchedSlots_8();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_40, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_41, (int32_t)0, (int32_t)L_42, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_43 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		ObjectU5BU5D_t1108656482* L_44 = V_8;
		int32_t L_45 = (int32_t)__this->get_touchedSlots_8();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_43, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_44, (int32_t)0, (int32_t)L_45, /*hidden argument*/NULL);
		UIModelDisplayTypeU5BU5D_t737309086* L_46 = V_7;
		__this->set_keySlots_6(L_46);
		ObjectU5BU5D_t1108656482* L_47 = V_8;
		__this->set_valueSlots_7(L_47);
		int32_t L_48 = V_0;
		__this->set_threshold_11((((int32_t)((int32_t)((float)((float)(((float)((float)L_48)))*(float)(0.9f)))))));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::Add(TKey,TValue)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern Il2CppCodeGenString* _stringLiteral628480033;
extern const uint32_t Dictionary_2_Add_m3091300865_MetadataUsageId;
extern "C"  void Dictionary_2_Add_m3091300865_gshared (Dictionary_2_t1674540875 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Add_m3091300865_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		goto IL_0016;
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		int32_t L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t3230847821* L_6 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))));
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_2 = (int32_t)((int32_t)((int32_t)L_10-(int32_t)1));
		goto IL_009b;
	}

IL_004a:
	{
		LinkU5BU5D_t375419643* L_11 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = (int32_t)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)))->get_HashCode_0();
		int32_t L_14 = V_0;
		if ((!(((uint32_t)L_13) == ((uint32_t)L_14))))
		{
			goto IL_0089;
		}
	}
	{
		Il2CppObject* L_15 = (Il2CppObject*)__this->get_hcp_12();
		UIModelDisplayTypeU5BU5D_t737309086* L_16 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		int32_t L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		int32_t L_20 = ___key0;
		NullCheck((Il2CppObject*)L_15);
		bool L_21 = InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_15, (int32_t)L_19, (int32_t)L_20);
		if (!L_21)
		{
			goto IL_0089;
		}
	}
	{
		ArgumentException_t928607144 * L_22 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_22, (String_t*)_stringLiteral628480033, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	}

IL_0089:
	{
		LinkU5BU5D_t375419643* L_23 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_24 = V_2;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = (int32_t)((L_23)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_24)))->get_Next_1();
		V_2 = (int32_t)L_25;
	}

IL_009b:
	{
		int32_t L_26 = V_2;
		if ((!(((uint32_t)L_26) == ((uint32_t)(-1)))))
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_27 = (int32_t)__this->get_count_10();
		int32_t L_28 = (int32_t)((int32_t)((int32_t)L_27+(int32_t)1));
		V_3 = (int32_t)L_28;
		__this->set_count_10(L_28);
		int32_t L_29 = V_3;
		int32_t L_30 = (int32_t)__this->get_threshold_11();
		if ((((int32_t)L_29) <= ((int32_t)L_30)))
		{
			goto IL_00d5;
		}
	}
	{
		NullCheck((Dictionary_2_t1674540875 *)__this);
		((  void (*) (Dictionary_2_t1674540875 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36)->methodPointer)((Dictionary_2_t1674540875 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36));
		int32_t L_31 = V_0;
		Int32U5BU5D_t3230847821* L_32 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_32);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_31&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_32)->max_length))))));
	}

IL_00d5:
	{
		int32_t L_33 = (int32_t)__this->get_emptySlot_9();
		V_2 = (int32_t)L_33;
		int32_t L_34 = V_2;
		if ((!(((uint32_t)L_34) == ((uint32_t)(-1)))))
		{
			goto IL_00fa;
		}
	}
	{
		int32_t L_35 = (int32_t)__this->get_touchedSlots_8();
		int32_t L_36 = (int32_t)L_35;
		V_3 = (int32_t)L_36;
		__this->set_touchedSlots_8(((int32_t)((int32_t)L_36+(int32_t)1)));
		int32_t L_37 = V_3;
		V_2 = (int32_t)L_37;
		goto IL_0111;
	}

IL_00fa:
	{
		LinkU5BU5D_t375419643* L_38 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_39 = V_2;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_39);
		int32_t L_40 = (int32_t)((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_39)))->get_Next_1();
		__this->set_emptySlot_9(L_40);
	}

IL_0111:
	{
		LinkU5BU5D_t375419643* L_41 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_42 = V_2;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
		int32_t L_43 = V_0;
		((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_42)))->set_HashCode_0(L_43);
		LinkU5BU5D_t375419643* L_44 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_45 = V_2;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, L_45);
		Int32U5BU5D_t3230847821* L_46 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_47 = V_1;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, L_47);
		int32_t L_48 = L_47;
		int32_t L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_45)))->set_Next_1(((int32_t)((int32_t)L_49-(int32_t)1)));
		Int32U5BU5D_t3230847821* L_50 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_51 = V_1;
		int32_t L_52 = V_2;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, L_51);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(L_51), (int32_t)((int32_t)((int32_t)L_52+(int32_t)1)));
		UIModelDisplayTypeU5BU5D_t737309086* L_53 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		int32_t L_54 = V_2;
		int32_t L_55 = ___key0;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, L_54);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(L_54), (int32_t)L_55);
		ObjectU5BU5D_t1108656482* L_56 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		int32_t L_57 = V_2;
		Il2CppObject * L_58 = ___value1;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, L_57);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(L_57), (Il2CppObject *)L_58);
		int32_t L_59 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_59+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m3908831583_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method)
{
	{
		__this->set_count_10(0);
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		Int32U5BU5D_t3230847821* L_1 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_1);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		UIModelDisplayTypeU5BU5D_t737309086* L_2 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		UIModelDisplayTypeU5BU5D_t737309086* L_3 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		NullCheck(L_3);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))), /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_4 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		ObjectU5BU5D_t1108656482* L_5 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		NullCheck(L_5);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_4, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))), /*hidden argument*/NULL);
		LinkU5BU5D_t375419643* L_6 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		LinkU5BU5D_t375419643* L_7 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		NullCheck(L_7);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))), /*hidden argument*/NULL);
		__this->set_emptySlot_9((-1));
		__this->set_touchedSlots_8(0);
		int32_t L_8 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_8+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::ContainsKey(TKey)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_ContainsKey_m1251474633_MetadataUsageId;
extern "C"  bool Dictionary_2_ContainsKey_m1251474633_gshared (Dictionary_2_t1674540875 * __this, int32_t ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_ContainsKey_m1251474633_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		goto IL_0016;
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		int32_t L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		Int32U5BU5D_t3230847821* L_5 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_6 = V_0;
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))));
		int32_t L_9 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (int32_t)((int32_t)((int32_t)L_9-(int32_t)1));
		goto IL_0090;
	}

IL_0048:
	{
		LinkU5BU5D_t375419643* L_10 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = (int32_t)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->get_HashCode_0();
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_007e;
		}
	}
	{
		Il2CppObject* L_14 = (Il2CppObject*)__this->get_hcp_12();
		UIModelDisplayTypeU5BU5D_t737309086* L_15 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		int32_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		int32_t L_19 = ___key0;
		NullCheck((Il2CppObject*)L_14);
		bool L_20 = InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_14, (int32_t)L_18, (int32_t)L_19);
		if (!L_20)
		{
			goto IL_007e;
		}
	}
	{
		return (bool)1;
	}

IL_007e:
	{
		LinkU5BU5D_t375419643* L_21 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = (int32_t)((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_22)))->get_Next_1();
		V_1 = (int32_t)L_23;
	}

IL_0090:
	{
		int32_t L_24 = V_1;
		if ((!(((uint32_t)L_24) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m94749769_gshared (Dictionary_2_t1674540875 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 47));
		EqualityComparer_1_t3278653252 * L_0 = ((  EqualityComparer_1_t3278653252 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46));
		V_0 = (Il2CppObject*)L_0;
		V_1 = (int32_t)0;
		goto IL_0054;
	}

IL_000d:
	{
		Int32U5BU5D_t3230847821* L_1 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		int32_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_2 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		goto IL_0049;
	}

IL_001d:
	{
		Il2CppObject* L_5 = V_0;
		ObjectU5BU5D_t1108656482* L_6 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		int32_t L_7 = V_2;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		Il2CppObject * L_10 = ___value0;
		NullCheck((Il2CppObject*)L_5);
		bool L_11 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 48), (Il2CppObject*)L_5, (Il2CppObject *)L_9, (Il2CppObject *)L_10);
		if (!L_11)
		{
			goto IL_0037;
		}
	}
	{
		return (bool)1;
	}

IL_0037:
	{
		LinkU5BU5D_t375419643* L_12 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = (int32_t)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_Next_1();
		V_2 = (int32_t)L_14;
	}

IL_0049:
	{
		int32_t L_15 = V_2;
		if ((!(((uint32_t)L_15) == ((uint32_t)(-1)))))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_17 = V_1;
		Int32U5BU5D_t3230847821* L_18 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3237038;
extern Il2CppCodeGenString* _stringLiteral2016261304;
extern Il2CppCodeGenString* _stringLiteral3759850573;
extern Il2CppCodeGenString* _stringLiteral212812367;
extern Il2CppCodeGenString* _stringLiteral961688967;
extern const uint32_t Dictionary_2_GetObjectData_m3027262162_MetadataUsageId;
extern "C"  void Dictionary_2_GetObjectData_m3027262162_gshared (Dictionary_2_t1674540875 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_GetObjectData_m3027262162_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2U5BU5D_t2563117120* V_0 = NULL;
	{
		SerializationInfo_t2185721892 * L_0 = ___info0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral3237038, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		SerializationInfo_t2185721892 * L_2 = ___info0;
		int32_t L_3 = (int32_t)__this->get_generation_14();
		NullCheck((SerializationInfo_t2185721892 *)L_2);
		SerializationInfo_AddValue_m2348540514((SerializationInfo_t2185721892 *)L_2, (String_t*)_stringLiteral2016261304, (int32_t)L_3, /*hidden argument*/NULL);
		SerializationInfo_t2185721892 * L_4 = ___info0;
		Il2CppObject* L_5 = (Il2CppObject*)__this->get_hcp_12();
		NullCheck((SerializationInfo_t2185721892 *)L_4);
		SerializationInfo_AddValue_m469120675((SerializationInfo_t2185721892 *)L_4, (String_t*)_stringLiteral3759850573, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		V_0 = (KeyValuePair_2U5BU5D_t2563117120*)NULL;
		int32_t L_6 = (int32_t)__this->get_count_10();
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_7 = (int32_t)__this->get_count_10();
		V_0 = (KeyValuePair_2U5BU5D_t2563117120*)((KeyValuePair_2U5BU5D_t2563117120*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 49), (uint32_t)L_7));
		KeyValuePair_2U5BU5D_t2563117120* L_8 = V_0;
		NullCheck((Dictionary_2_t1674540875 *)__this);
		((  void (*) (Dictionary_2_t1674540875 *, KeyValuePair_2U5BU5D_t2563117120*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((Dictionary_2_t1674540875 *)__this, (KeyValuePair_2U5BU5D_t2563117120*)L_8, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
	}

IL_0055:
	{
		SerializationInfo_t2185721892 * L_9 = ___info0;
		Int32U5BU5D_t3230847821* L_10 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_10);
		NullCheck((SerializationInfo_t2185721892 *)L_9);
		SerializationInfo_AddValue_m2348540514((SerializationInfo_t2185721892 *)L_9, (String_t*)_stringLiteral212812367, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))), /*hidden argument*/NULL);
		SerializationInfo_t2185721892 * L_11 = ___info0;
		KeyValuePair_2U5BU5D_t2563117120* L_12 = V_0;
		NullCheck((SerializationInfo_t2185721892 *)L_11);
		SerializationInfo_AddValue_m469120675((SerializationInfo_t2185721892 *)L_11, (String_t*)_stringLiteral961688967, (Il2CppObject *)(Il2CppObject *)L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::OnDeserialization(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2016261304;
extern Il2CppCodeGenString* _stringLiteral3759850573;
extern Il2CppCodeGenString* _stringLiteral212812367;
extern Il2CppCodeGenString* _stringLiteral961688967;
extern const uint32_t Dictionary_2_OnDeserialization_m1047664338_MetadataUsageId;
extern "C"  void Dictionary_2_OnDeserialization_m1047664338_gshared (Dictionary_2_t1674540875 * __this, Il2CppObject * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_OnDeserialization_m1047664338_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2U5BU5D_t2563117120* V_1 = NULL;
	int32_t V_2 = 0;
	{
		SerializationInfo_t2185721892 * L_0 = (SerializationInfo_t2185721892 *)__this->get_serialization_info_13();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		SerializationInfo_t2185721892 * L_1 = (SerializationInfo_t2185721892 *)__this->get_serialization_info_13();
		NullCheck((SerializationInfo_t2185721892 *)L_1);
		int32_t L_2 = SerializationInfo_GetInt32_m4048035953((SerializationInfo_t2185721892 *)L_1, (String_t*)_stringLiteral2016261304, /*hidden argument*/NULL);
		__this->set_generation_14(L_2);
		SerializationInfo_t2185721892 * L_3 = (SerializationInfo_t2185721892 *)__this->get_serialization_info_13();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 50)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2185721892 *)L_3);
		Il2CppObject * L_5 = SerializationInfo_GetValue_m4125471336((SerializationInfo_t2185721892 *)L_3, (String_t*)_stringLiteral3759850573, (Type_t *)L_4, /*hidden argument*/NULL);
		__this->set_hcp_12(((Il2CppObject*)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35))));
		SerializationInfo_t2185721892 * L_6 = (SerializationInfo_t2185721892 *)__this->get_serialization_info_13();
		NullCheck((SerializationInfo_t2185721892 *)L_6);
		int32_t L_7 = SerializationInfo_GetInt32_m4048035953((SerializationInfo_t2185721892 *)L_6, (String_t*)_stringLiteral212812367, /*hidden argument*/NULL);
		V_0 = (int32_t)L_7;
		SerializationInfo_t2185721892 * L_8 = (SerializationInfo_t2185721892 *)__this->get_serialization_info_13();
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 51)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2185721892 *)L_8);
		Il2CppObject * L_10 = SerializationInfo_GetValue_m4125471336((SerializationInfo_t2185721892 *)L_8, (String_t*)_stringLiteral961688967, (Type_t *)L_9, /*hidden argument*/NULL);
		V_1 = (KeyValuePair_2U5BU5D_t2563117120*)((KeyValuePair_2U5BU5D_t2563117120*)Castclass(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20)));
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) >= ((int32_t)((int32_t)10))))
		{
			goto IL_0083;
		}
	}
	{
		V_0 = (int32_t)((int32_t)10);
	}

IL_0083:
	{
		int32_t L_12 = V_0;
		NullCheck((Dictionary_2_t1674540875 *)__this);
		((  void (*) (Dictionary_2_t1674540875 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 39)->methodPointer)((Dictionary_2_t1674540875 *)__this, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 39));
		__this->set_count_10(0);
		KeyValuePair_2U5BU5D_t2563117120* L_13 = V_1;
		if (!L_13)
		{
			goto IL_00c9;
		}
	}
	{
		V_2 = (int32_t)0;
		goto IL_00c0;
	}

IL_009e:
	{
		KeyValuePair_2U5BU5D_t2563117120* L_14 = V_1;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1573321581 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_15)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		KeyValuePair_2U5BU5D_t2563117120* L_17 = V_1;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		Il2CppObject * L_19 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1573321581 *)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((Dictionary_2_t1674540875 *)__this);
		((  void (*) (Dictionary_2_t1674540875 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t1674540875 *)__this, (int32_t)L_16, (Il2CppObject *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		int32_t L_20 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_00c0:
	{
		int32_t L_21 = V_2;
		KeyValuePair_2U5BU5D_t2563117120* L_22 = V_1;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_22)->max_length)))))))
		{
			goto IL_009e;
		}
	}

IL_00c9:
	{
		int32_t L_23 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_23+(int32_t)1)));
		__this->set_serialization_info_13((SerializationInfo_t2185721892 *)NULL);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::Remove(TKey)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* UIModelDisplayType_t1891752679_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_Remove_m3194856071_MetadataUsageId;
extern "C"  bool Dictionary_2_Remove_m3194856071_gshared (Dictionary_2_t1674540875 * __this, int32_t ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Remove_m3194856071_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Il2CppObject * V_5 = NULL;
	{
		goto IL_0016;
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		int32_t L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t3230847821* L_6 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))));
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_2 = (int32_t)((int32_t)((int32_t)L_10-(int32_t)1));
		int32_t L_11 = V_2;
		if ((!(((uint32_t)L_11) == ((uint32_t)(-1)))))
		{
			goto IL_004e;
		}
	}
	{
		return (bool)0;
	}

IL_004e:
	{
		V_3 = (int32_t)(-1);
	}

IL_0050:
	{
		LinkU5BU5D_t375419643* L_12 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = (int32_t)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_HashCode_0();
		int32_t L_15 = V_0;
		if ((!(((uint32_t)L_14) == ((uint32_t)L_15))))
		{
			goto IL_0089;
		}
	}
	{
		Il2CppObject* L_16 = (Il2CppObject*)__this->get_hcp_12();
		UIModelDisplayTypeU5BU5D_t737309086* L_17 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		int32_t L_18 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		int32_t L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		int32_t L_21 = ___key0;
		NullCheck((Il2CppObject*)L_16);
		bool L_22 = InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_16, (int32_t)L_20, (int32_t)L_21);
		if (!L_22)
		{
			goto IL_0089;
		}
	}
	{
		goto IL_00a4;
	}

IL_0089:
	{
		int32_t L_23 = V_2;
		V_3 = (int32_t)L_23;
		LinkU5BU5D_t375419643* L_24 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_25 = V_2;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		int32_t L_26 = (int32_t)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_25)))->get_Next_1();
		V_2 = (int32_t)L_26;
		int32_t L_27 = V_2;
		if ((!(((uint32_t)L_27) == ((uint32_t)(-1)))))
		{
			goto IL_0050;
		}
	}

IL_00a4:
	{
		int32_t L_28 = V_2;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_00ad;
		}
	}
	{
		return (bool)0;
	}

IL_00ad:
	{
		int32_t L_29 = (int32_t)__this->get_count_10();
		__this->set_count_10(((int32_t)((int32_t)L_29-(int32_t)1)));
		int32_t L_30 = V_3;
		if ((!(((uint32_t)L_30) == ((uint32_t)(-1)))))
		{
			goto IL_00e2;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_31 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_32 = V_1;
		LinkU5BU5D_t375419643* L_33 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_34 = V_2;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, L_34);
		int32_t L_35 = (int32_t)((L_33)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_34)))->get_Next_1();
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(L_32), (int32_t)((int32_t)((int32_t)L_35+(int32_t)1)));
		goto IL_0104;
	}

IL_00e2:
	{
		LinkU5BU5D_t375419643* L_36 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_37 = V_3;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		LinkU5BU5D_t375419643* L_38 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_39 = V_2;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_39);
		int32_t L_40 = (int32_t)((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_39)))->get_Next_1();
		((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_37)))->set_Next_1(L_40);
	}

IL_0104:
	{
		LinkU5BU5D_t375419643* L_41 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_42 = V_2;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
		int32_t L_43 = (int32_t)__this->get_emptySlot_9();
		((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_42)))->set_Next_1(L_43);
		int32_t L_44 = V_2;
		__this->set_emptySlot_9(L_44);
		LinkU5BU5D_t375419643* L_45 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_46 = V_2;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, L_46);
		((L_45)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_46)))->set_HashCode_0(0);
		UIModelDisplayTypeU5BU5D_t737309086* L_47 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		int32_t L_48 = V_2;
		Initobj (UIModelDisplayType_t1891752679_il2cpp_TypeInfo_var, (&V_4));
		int32_t L_49 = V_4;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, L_48);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(L_48), (int32_t)L_49);
		ObjectU5BU5D_t1108656482* L_50 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		int32_t L_51 = V_2;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_5));
		Il2CppObject * L_52 = V_5;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, L_51);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(L_51), (Il2CppObject *)L_52);
		int32_t L_53 = (int32_t)__this->get_generation_14();
		__this->set_generation_14(((int32_t)((int32_t)L_53+(int32_t)1)));
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::TryGetValue(TKey,TValue&)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern const uint32_t Dictionary_2_TryGetValue_m2586016802_MetadataUsageId;
extern "C"  bool Dictionary_2_TryGetValue_m2586016802_gshared (Dictionary_2_t1674540875 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_TryGetValue_m2586016802_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	{
		goto IL_0016;
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_hcp_12();
		int32_t L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648LL)));
		Int32U5BU5D_t3230847821* L_5 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		int32_t L_6 = V_0;
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get_table_4();
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))));
		int32_t L_9 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (int32_t)((int32_t)((int32_t)L_9-(int32_t)1));
		goto IL_00a2;
	}

IL_0048:
	{
		LinkU5BU5D_t375419643* L_10 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = (int32_t)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->get_HashCode_0();
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_0090;
		}
	}
	{
		Il2CppObject* L_14 = (Il2CppObject*)__this->get_hcp_12();
		UIModelDisplayTypeU5BU5D_t737309086* L_15 = (UIModelDisplayTypeU5BU5D_t737309086*)__this->get_keySlots_6();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		int32_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		int32_t L_19 = ___key0;
		NullCheck((Il2CppObject*)L_14);
		bool L_20 = InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>::Equals(T,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_14, (int32_t)L_18, (int32_t)L_19);
		if (!L_20)
		{
			goto IL_0090;
		}
	}
	{
		Il2CppObject ** L_21 = ___value1;
		ObjectU5BU5D_t1108656482* L_22 = (ObjectU5BU5D_t1108656482*)__this->get_valueSlots_7();
		int32_t L_23 = V_1;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		Il2CppObject * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		(*(Il2CppObject **)L_21) = L_25;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)L_21, L_25);
		return (bool)1;
	}

IL_0090:
	{
		LinkU5BU5D_t375419643* L_26 = (LinkU5BU5D_t375419643*)__this->get_linkSlots_5();
		int32_t L_27 = V_1;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		int32_t L_28 = (int32_t)((L_26)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_27)))->get_Next_1();
		V_1 = (int32_t)L_28;
	}

IL_00a2:
	{
		int32_t L_29 = V_1;
		if ((!(((uint32_t)L_29) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		Il2CppObject ** L_30 = ___value1;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_31 = V_2;
		(*(Il2CppObject **)L_30) = L_31;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)L_30, L_31);
		return (bool)0;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::get_Keys()
extern "C"  KeyCollection_t3301300326 * Dictionary_2_get_Keys_m3898484025_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method)
{
	{
		KeyCollection_t3301300326 * L_0 = (KeyCollection_t3301300326 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 52));
		((  void (*) (KeyCollection_t3301300326 *, Dictionary_2_t1674540875 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 53)->methodPointer)(L_0, (Dictionary_2_t1674540875 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 53));
		return L_0;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::get_Values()
extern "C"  ValueCollection_t375146588 * Dictionary_2_get_Values_m874702741_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method)
{
	{
		ValueCollection_t375146588 * L_0 = (ValueCollection_t375146588 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 54));
		((  void (*) (ValueCollection_t375146588 *, Dictionary_2_t1674540875 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 55)->methodPointer)(L_0, (Dictionary_2_t1674540875 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 55));
		return L_0;
	}
}
// TKey System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::ToTKey(System.Object)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106079;
extern Il2CppCodeGenString* _stringLiteral3927817532;
extern const uint32_t Dictionary_2_ToTKey_m2881600222_MetadataUsageId;
extern "C"  int32_t Dictionary_2_ToTKey_m2881600222_gshared (Dictionary_2_t1674540875 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_ToTKey_m2881600222_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___key0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10))))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 56)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m138640077(NULL /*static, unused*/, (String_t*)_stringLiteral3927817532, (String_t*)L_4, /*hidden argument*/NULL);
		ArgumentException_t928607144 * L_6 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_6, (String_t*)L_5, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0040:
	{
		Il2CppObject * L_7 = ___key0;
		return ((*(int32_t*)((int32_t*)UnBox (L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10)))));
	}
}
// TValue System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::ToTValue(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3927817532;
extern Il2CppCodeGenString* _stringLiteral111972721;
extern const uint32_t Dictionary_2_ToTValue_m1601576634_MetadataUsageId;
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m1601576634_gshared (Dictionary_2_t1674540875 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_ToTValue_m1601576634_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 57)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_1);
		bool L_2 = Type_get_IsValueType_m1914757235((Type_t *)L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		Il2CppObject * L_4 = ___value0;
		if (((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14))))
		{
			goto IL_0053;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 57)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m138640077(NULL /*static, unused*/, (String_t*)_stringLiteral3927817532, (String_t*)L_6, /*hidden argument*/NULL);
		ArgumentException_t928607144 * L_8 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_8, (String_t*)L_7, (String_t*)_stringLiteral111972721, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0053:
	{
		Il2CppObject * L_9 = ___value0;
		return ((Il2CppObject *)Castclass(L_9, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14)));
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m1115101290_gshared (Dictionary_2_t1674540875 * __this, KeyValuePair_2_t1573321581  ___pair0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		int32_t L_0 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1573321581 *)(&___pair0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((Dictionary_2_t1674540875 *)__this);
		bool L_1 = ((  bool (*) (Dictionary_2_t1674540875 *, int32_t, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 58)->methodPointer)((Dictionary_2_t1674540875 *)__this, (int32_t)L_0, (Il2CppObject **)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 58));
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		return (bool)0;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 47));
		EqualityComparer_1_t3278653252 * L_2 = ((  EqualityComparer_1_t3278653252 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46));
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1573321581 *)(&___pair0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Il2CppObject * L_4 = V_0;
		NullCheck((EqualityComparer_1_t3278653252 *)L_2);
		bool L_5 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(T,T) */, (EqualityComparer_1_t3278653252 *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_4);
		return L_5;
	}
}
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2991864267  Dictionary_2_GetEnumerator_m1602013183_gshared (Dictionary_2_t1674540875 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2991864267  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m2965822312(&L_0, (Dictionary_2_t1674540875 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 32));
		return L_0;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m2775660470_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), &L_1);
		Il2CppObject * L_3 = ___value1;
		DictionaryEntry_t1751606614  L_4;
		memset(&L_4, 0, sizeof(L_4));
		DictionaryEntry__ctor_m2600671860(&L_4, (Il2CppObject *)L_2, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<AIEnum.EAIEventtype>::.ctor()
extern "C"  void DefaultComparer__ctor_m204224618_gshared (DefaultComparer_t2344065978 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3004126192 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3004126192 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3004126192 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<AIEnum.EAIEventtype>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1983760385_gshared (DefaultComparer_t2344065978 * __this, uint8_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<AIEnum.EAIEventtype>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1642354747_gshared (DefaultComparer_t2344065978 * __this, uint8_t ___x0, uint8_t ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		uint8_t L_1 = ___y1;
		uint8_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		uint8_t L_4 = ___y1;
		uint8_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<AnimationRunner/AniType>::.ctor()
extern "C"  void DefaultComparer__ctor_m3722141753_gshared (DefaultComparer_t3748982614 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t114075532 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t114075532 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t114075532 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<AnimationRunner/AniType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3040877394_gshared (DefaultComparer_t3748982614 * __this, int32_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<AnimationRunner/AniType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2841276810_gshared (DefaultComparer_t3748982614 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Core.RpsChoice>::.ctor()
extern "C"  void DefaultComparer__ctor_m2396458323_gshared (DefaultComparer_t2795541095 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3455601309 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3455601309 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3455601309 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Core.RpsChoice>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3768131392_gshared (DefaultComparer_t2795541095 * __this, int32_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Core.RpsChoice>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1988489320_gshared (DefaultComparer_t2795541095 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Core.RpsResult>::.ctor()
extern "C"  void DefaultComparer__ctor_m1829866703_gshared (DefaultComparer_t3222338787 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3882399001 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3882399001 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3882399001 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Core.RpsResult>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1936948292_gshared (DefaultComparer_t3222338787 * __this, int32_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Core.RpsResult>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2889634276_gshared (DefaultComparer_t3222338787 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Entity.Behavior.EBehaviorID>::.ctor()
extern "C"  void DefaultComparer__ctor_m1819205919_gshared (DefaultComparer_t3981277099 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t346370017 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t346370017 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t346370017 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Entity.Behavior.EBehaviorID>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3059350956_gshared (DefaultComparer_t3981277099 * __this, uint8_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Entity.Behavior.EBehaviorID>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3439591536_gshared (DefaultComparer_t3981277099 * __this, uint8_t ___x0, uint8_t ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		uint8_t L_1 = ___y1;
		uint8_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		uint8_t L_4 = ___y1;
		uint8_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<FLOAT_TEXT_ID>::.ctor()
extern "C"  void DefaultComparer__ctor_m2404234088_gshared (DefaultComparer_t647122437 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t1307182651 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1307182651 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1307182651 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<FLOAT_TEXT_ID>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2928580675_gshared (DefaultComparer_t647122437 * __this, int32_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<FLOAT_TEXT_ID>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1303841465_gshared (DefaultComparer_t647122437 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<HatredCtrl/stValue>::.ctor()
extern "C"  void DefaultComparer__ctor_m3125470958_gshared (DefaultComparer_t1873722077 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t2533782291 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2533782291 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2533782291 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<HatredCtrl/stValue>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2201616197_gshared (DefaultComparer_t1873722077 * __this, stValue_t3425945410  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<HatredCtrl/stValue>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2252174211_gshared (DefaultComparer_t1873722077 * __this, stValue_t3425945410  ___x0, stValue_t3425945410  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		stValue_t3425945410  L_1 = ___y1;
		stValue_t3425945410  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		stValue_t3425945410  L_4 = ___y1;
		stValue_t3425945410  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m3103776966_gshared (DefaultComparer_t1081757877 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t1741818091 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1741818091 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1741818091 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Mihua.Assets.SubAssetMgr/ErrorInfo>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m4155865709_gshared (DefaultComparer_t1081757877 * __this, ErrorInfo_t2633981210  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Mihua.Assets.SubAssetMgr/ErrorInfo>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m852917595_gshared (DefaultComparer_t1081757877 * __this, ErrorInfo_t2633981210  ___x0, ErrorInfo_t2633981210  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		ErrorInfo_t2633981210  L_1 = ___y1;
		ErrorInfo_t2633981210  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		ErrorInfo_t2633981210  L_4 = ___y1;
		ErrorInfo_t2633981210  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<MScrollView/MoveWay>::.ctor()
extern "C"  void DefaultComparer__ctor_m865225644_gshared (DefaultComparer_t1922718217 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t2582778431 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2582778431 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2582778431 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<MScrollView/MoveWay>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3464095359_gshared (DefaultComparer_t1922718217 * __this, int32_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<MScrollView/MoveWay>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1974139517_gshared (DefaultComparer_t1922718217 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Linq.JTokenType>::.ctor()
extern "C"  void DefaultComparer__ctor_m3270300384_gshared (DefaultComparer_t2364674228 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3024734442 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3024734442 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3024734442 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Linq.JTokenType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3014906571_gshared (DefaultComparer_t2364674228 * __this, int32_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Linq.JTokenType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m940415665_gshared (DefaultComparer_t2364674228 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Schema.JsonSchemaType>::.ctor()
extern "C"  void DefaultComparer__ctor_m3543622105_gshared (DefaultComparer_t563192488 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t1223252702 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1223252702 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1223252702 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Schema.JsonSchemaType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1319396530_gshared (DefaultComparer_t563192488 * __this, int32_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Schema.JsonSchemaType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m697286314_gshared (DefaultComparer_t563192488 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::.ctor()
extern "C"  void DefaultComparer__ctor_m2745096803_gshared (DefaultComparer_t1419621458 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t2079681672 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2079681672 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2079681672 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1761873200_gshared (DefaultComparer_t1419621458 * __this, TypeNameKey_t2971844791  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = TypeNameKey_GetHashCode_m2544152741((TypeNameKey_t2971844791 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m921698296_gshared (DefaultComparer_t1419621458 * __this, TypeNameKey_t2971844791  ___x0, TypeNameKey_t2971844791  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		TypeNameKey_t2971844791  L_1 = ___y1;
		TypeNameKey_t2971844791  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		TypeNameKey_t2971844791  L_4 = ___y1;
		TypeNameKey_t2971844791  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = TypeNameKey_Equals_m2245811073((TypeNameKey_t2971844791 *)(&___x0), (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::.ctor()
extern "C"  void DefaultComparer__ctor_m3194723152_gshared (DefaultComparer_t1340106157 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t2000166371 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2000166371 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2000166371 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2284352091_gshared (DefaultComparer_t1340106157 * __this, int32_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1476036129_gshared (DefaultComparer_t1340106157 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::.ctor()
extern "C"  void DefaultComparer__ctor_m3802309532_gshared (DefaultComparer_t3608878137 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t4268938351 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t4268938351 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t4268938351 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m4048255375_gshared (DefaultComparer_t3608878137 * __this, TypeConvertKey_t866134174  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = TypeConvertKey_GetHashCode_m218825866((TypeConvertKey_t866134174 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1643653869_gshared (DefaultComparer_t3608878137 * __this, TypeConvertKey_t866134174  ___x0, TypeConvertKey_t866134174  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		TypeConvertKey_t866134174  L_1 = ___y1;
		TypeConvertKey_t866134174  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		TypeConvertKey_t866134174  L_4 = ___y1;
		TypeConvertKey_t866134174  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = TypeConvertKey_Equals_m721887858((TypeConvertKey_t866134174 *)(&___x0), (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.AdvancedSmooth/Turn>::.ctor()
extern "C"  void DefaultComparer__ctor_m3939103792_gshared (DefaultComparer_t3207939341 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3867999555 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3867999555 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3867999555 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.AdvancedSmooth/Turn>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3864893307_gshared (DefaultComparer_t3207939341 * __this, Turn_t465195378  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.AdvancedSmooth/Turn>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m4275672065_gshared (DefaultComparer_t3207939341 * __this, Turn_t465195378  ___x0, Turn_t465195378  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		Turn_t465195378  L_1 = ___y1;
		Turn_t465195378  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Turn_t465195378  L_4 = ___y1;
		Turn_t465195378  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.ClipperLib.IntPoint>::.ctor()
extern "C"  void DefaultComparer__ctor_m556725323_gshared (DefaultComparer_t1773902846 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t2433963060 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2433963060 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2433963060 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.ClipperLib.IntPoint>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2568508160_gshared (DefaultComparer_t1773902846 * __this, IntPoint_t3326126179  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = IntPoint_GetHashCode_m2934122383((IntPoint_t3326126179 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.ClipperLib.IntPoint>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2084245148_gshared (DefaultComparer_t1773902846 * __this, IntPoint_t3326126179  ___x0, IntPoint_t3326126179  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		IntPoint_t3326126179  L_1 = ___y1;
		IntPoint_t3326126179  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		IntPoint_t3326126179  L_4 = ___y1;
		IntPoint_t3326126179  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = IntPoint_Equals_m2369764343((IntPoint_t3326126179 *)(&___x0), (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Int2>::.ctor()
extern "C"  void DefaultComparer__ctor_m3960064505_gshared (DefaultComparer_t421822260 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t1081882474 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1081882474 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1081882474 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Int2>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3399561946_gshared (DefaultComparer_t421822260 * __this, Int2_t1974045593  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Int2_GetHashCode_m192259407((Int2_t1974045593 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Int2>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2712916366_gshared (DefaultComparer_t421822260 * __this, Int2_t1974045593  ___x0, Int2_t1974045593  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		Int2_t1974045593  L_1 = ___y1;
		Int2_t1974045593  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Int2_t1974045593  L_4 = ___y1;
		Int2_t1974045593  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = Int2_Equals_m3389407019((Int2_t1974045593 *)(&___x0), (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Int3>::.ctor()
extern "C"  void DefaultComparer__ctor_m2163113146_gshared (DefaultComparer_t421822261 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t1081882475 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1081882475 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1081882475 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Int3>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2402489593_gshared (DefaultComparer_t421822261 * __this, Int3_t1974045594  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Int3_GetHashCode_m3976692526((Int3_t1974045594 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Int3>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m4220468175_gshared (DefaultComparer_t421822261 * __this, Int3_t1974045594  ___x0, Int3_t1974045594  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		Int3_t1974045594  L_1 = ___y1;
		Int3_t1974045594  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Int3_t1974045594  L_4 = ___y1;
		Int3_t1974045594  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = Int3_Equals_m3322400266((Int3_t1974045594 *)(&___x0), (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.IntRect>::.ctor()
extern "C"  void DefaultComparer__ctor_m150590855_gshared (DefaultComparer_t1462834928 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t2122895142 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2122895142 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2122895142 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.IntRect>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3319242820_gshared (DefaultComparer_t1462834928 * __this, IntRect_t3015058261  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = IntRect_GetHashCode_m1882730815((IntRect_t3015058261 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.IntRect>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3593739992_gshared (DefaultComparer_t1462834928 * __this, IntRect_t3015058261  ___x0, IntRect_t3015058261  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		IntRect_t3015058261  L_1 = ___y1;
		IntRect_t3015058261  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		IntRect_t3015058261  L_4 = ___y1;
		IntRect_t3015058261  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = IntRect_Equals_m1847956391((IntRect_t3015058261 *)(&___x0), (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.LocalAvoidance/IntersectionPair>::.ctor()
extern "C"  void DefaultComparer__ctor_m2337999341_gshared (DefaultComparer_t1269096586 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t1929156800 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1929156800 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1929156800 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.LocalAvoidance/IntersectionPair>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2011511070_gshared (DefaultComparer_t1269096586 * __this, IntersectionPair_t2821319919  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.LocalAvoidance/IntersectionPair>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m627168830_gshared (DefaultComparer_t1269096586 * __this, IntersectionPair_t2821319919  ___x0, IntersectionPair_t2821319919  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		IntersectionPair_t2821319919  L_1 = ___y1;
		IntersectionPair_t2821319919  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		IntersectionPair_t2821319919  L_4 = ___y1;
		IntersectionPair_t2821319919  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.LocalAvoidance/VOLine>::.ctor()
extern "C"  void DefaultComparer__ctor_m2228674775_gshared (DefaultComparer_t477708468 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t1137768682 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1137768682 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1137768682 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.LocalAvoidance/VOLine>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1669667316_gshared (DefaultComparer_t477708468 * __this, VOLine_t2029931801  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.LocalAvoidance/VOLine>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1841814952_gshared (DefaultComparer_t477708468 * __this, VOLine_t2029931801  ___x0, VOLine_t2029931801  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		VOLine_t2029931801  L_1 = ___y1;
		VOLine_t2029931801  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		VOLine_t2029931801  L_4 = ___y1;
		VOLine_t2029931801  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Voxels.ExtraMesh>::.ctor()
extern "C"  void DefaultComparer__ctor_m3066981330_gshared (DefaultComparer_t2665806382 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3325866596 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3325866596 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3325866596 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Voxels.ExtraMesh>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m486737121_gshared (DefaultComparer_t2665806382 * __this, ExtraMesh_t4218029715  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Voxels.ExtraMesh>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m924052455_gshared (DefaultComparer_t2665806382 * __this, ExtraMesh_t4218029715  ___x0, ExtraMesh_t4218029715  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		ExtraMesh_t4218029715  L_1 = ___y1;
		ExtraMesh_t4218029715  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		ExtraMesh_t4218029715  L_4 = ___y1;
		ExtraMesh_t4218029715  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Voxels.VoxelContour>::.ctor()
extern "C"  void DefaultComparer__ctor_m1915993417_gshared (DefaultComparer_t2044977683 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t2705037897 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2705037897 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2705037897 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Voxels.VoxelContour>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3624734786_gshared (DefaultComparer_t2044977683 * __this, VoxelContour_t3597201016  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Voxels.VoxelContour>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3147245722_gshared (DefaultComparer_t2044977683 * __this, VoxelContour_t3597201016  ___x0, VoxelContour_t3597201016  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		VoxelContour_t3597201016  L_1 = ___y1;
		VoxelContour_t3597201016  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		VoxelContour_t3597201016  L_4 = ___y1;
		VoxelContour_t3597201016  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<PointCloudRegognizer/Point>::.ctor()
extern "C"  void DefaultComparer__ctor_m117480498_gshared (DefaultComparer_t286608417 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t946668631 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t946668631 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t946668631 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<PointCloudRegognizer/Point>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3640848769_gshared (DefaultComparer_t286608417 * __this, Point_t1838831750  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<PointCloudRegognizer/Point>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m780158151_gshared (DefaultComparer_t286608417 * __this, Point_t1838831750  ___x0, Point_t1838831750  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		Point_t1838831750  L_1 = ___y1;
		Point_t1838831750  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Point_t1838831750  L_4 = ___y1;
		Point_t1838831750  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<ProductsCfgMgr/ProductInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m2670816242_gshared (DefaultComparer_t4048735201 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t413828119 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t413828119 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t413828119 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<ProductsCfgMgr/ProductInfo>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2801592769_gshared (DefaultComparer_t4048735201 * __this, ProductInfo_t1305991238  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<ProductsCfgMgr/ProductInfo>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2655204487_gshared (DefaultComparer_t4048735201 * __this, ProductInfo_t1305991238  ___x0, ProductInfo_t1305991238  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		ProductInfo_t1305991238  L_1 = ___y1;
		ProductInfo_t1305991238  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		ProductInfo_t1305991238  L_4 = ___y1;
		ProductInfo_t1305991238  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<PushType>::.ctor()
extern "C"  void DefaultComparer__ctor_m1116639104_gshared (DefaultComparer_t288419119 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t948479333 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t948479333 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t948479333 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<PushType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2328632179_gshared (DefaultComparer_t288419119 * __this, int32_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<PushType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m596483477_gshared (DefaultComparer_t288419119 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<SoundStatus>::.ctor()
extern "C"  void DefaultComparer__ctor_m1593493567_gshared (DefaultComparer_t2040715612 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t2700775826 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2700775826 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2700775826 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<SoundStatus>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1941932684_gshared (DefaultComparer_t2040715612 * __this, int32_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<SoundStatus>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1284543376_gshared (DefaultComparer_t2040715612 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<SoundTypeID>::.ctor()
extern "C"  void DefaultComparer__ctor_m2611079778_gshared (DefaultComparer_t2074393407 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t2734453621 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2734453621 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2734453621 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<SoundTypeID>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m418859785_gshared (DefaultComparer_t2074393407 * __this, int32_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<SoundTypeID>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3200511027_gshared (DefaultComparer_t2074393407 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Boolean>::.ctor()
extern "C"  void DefaultComparer__ctor_m3591190997_gshared (DefaultComparer_t3219542681 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3879602895 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3879602895 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3879602895 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Boolean>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2763144318_gshared (DefaultComparer_t3219542681 * __this, bool ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Boolean_GetHashCode_m841540860((bool*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Boolean>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2783841258_gshared (DefaultComparer_t3219542681 * __this, bool ___x0, bool ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		bool L_1 = ___y1;
		bool L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		bool L_4 = ___y1;
		bool L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = Boolean_Equals_m1178456600((bool*)(&___x0), (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Byte>::.ctor()
extern "C"  void DefaultComparer__ctor_m895439973_gshared (DefaultComparer_t1310386327 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t1970446541 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1970446541 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1970446541 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Byte>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m4107413414_gshared (DefaultComparer_t1310386327 * __this, uint8_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Byte_GetHashCode_m2610389752((uint8_t*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Byte>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m132553910_gshared (DefaultComparer_t1310386327 * __this, uint8_t ___x0, uint8_t ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		uint8_t L_1 = ___y1;
		uint8_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		uint8_t L_4 = ___y1;
		uint8_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = Byte_Equals_m4077775008((uint8_t*)(&___x0), (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void DefaultComparer__ctor_m699473545_gshared (DefaultComparer_t392445644 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t1052505858 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1052505858 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1052505858 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m782100810_gshared (DefaultComparer_t392445644 * __this, KeyValuePair_2_t1944668977  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3661957278_gshared (DefaultComparer_t392445644 * __this, KeyValuePair_2_t1944668977  ___x0, KeyValuePair_2_t1944668977  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		KeyValuePair_2_t1944668977  L_1 = ___y1;
		KeyValuePair_2_t1944668977  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		KeyValuePair_2_t1944668977  L_4 = ___y1;
		KeyValuePair_2_t1944668977  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::.ctor()
extern "C"  void DefaultComparer__ctor_m967913385_gshared (DefaultComparer_t2735708096 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3395768310 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3395768310 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3395768310 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3547266274_gshared (DefaultComparer_t2735708096 * __this, KeyValuePair_2_t4287931429  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3411651706_gshared (DefaultComparer_t2735708096 * __this, KeyValuePair_2_t4287931429  ___x0, KeyValuePair_2_t4287931429  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		KeyValuePair_2_t4287931429  L_1 = ___y1;
		KeyValuePair_2_t4287931429  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		KeyValuePair_2_t4287931429  L_4 = ___y1;
		KeyValuePair_2_t4287931429  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>::.ctor()
extern "C"  void DefaultComparer__ctor_m3105741624_gshared (DefaultComparer_t2731437994 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3391498208 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3391498208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3391498208 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3158503283_gshared (DefaultComparer_t2731437994 * __this, DateTime_t4283661327  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = DateTime_GetHashCode_m2255586565((DateTime_t4283661327 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2514350857_gshared (DefaultComparer_t2731437994 * __this, DateTime_t4283661327  ___x0, DateTime_t4283661327  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		DateTime_t4283661327  L_1 = ___y1;
		DateTime_t4283661327  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		DateTime_t4283661327  L_4 = ___y1;
		DateTime_t4283661327  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = DateTime_Equals_m13666989((DateTime_t4283661327 *)(&___x0), (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>::.ctor()
extern "C"  void DefaultComparer__ctor_m2725721579_gshared (DefaultComparer_t2332490973 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t2992551187 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2992551187 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2992551187 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2221600864_gshared (DefaultComparer_t2332490973 * __this, DateTimeOffset_t3884714306  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = DateTimeOffset_GetHashCode_m1972583858((DateTimeOffset_t3884714306 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m32601532_gshared (DefaultComparer_t2332490973 * __this, DateTimeOffset_t3884714306  ___x0, DateTimeOffset_t3884714306  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		DateTimeOffset_t3884714306  L_1 = ___y1;
		DateTimeOffset_t3884714306  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		DateTimeOffset_t3884714306  L_4 = ___y1;
		DateTimeOffset_t3884714306  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = DateTimeOffset_Equals_m1431331290((DateTimeOffset_t3884714306 *)(&___x0), (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::.ctor()
extern "C"  void DefaultComparer__ctor_m4213267622_gshared (DefaultComparer_t1310531096 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t1970591310 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1970591310 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1970591310 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m4200825925_gshared (DefaultComparer_t1310531096 * __this, Guid_t2862754429  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Guid_GetHashCode_m885349207((Guid_t2862754429 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2432212087_gshared (DefaultComparer_t1310531096 * __this, Guid_t2862754429  ___x0, Guid_t2862754429  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		Guid_t2862754429  L_1 = ___y1;
		Guid_t2862754429  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Guid_t2862754429  L_4 = ___y1;
		Guid_t2862754429  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = Guid_Equals_m1613304319((Guid_t2862754429 *)(&___x0), (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Int32>::.ctor()
extern "C"  void DefaultComparer__ctor_m1525034683_gshared (DefaultComparer_t3896582463 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t261675381 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t261675381 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t261675381 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Int32>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2684887512_gshared (DefaultComparer_t3896582463 * __this, int32_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Int32_GetHashCode_m3396943446((int32_t*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Int32>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3521926736_gshared (DefaultComparer_t3896582463 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = Int32_Equals_m4061110258((int32_t*)(&___x0), (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Object>::.ctor()
extern "C"  void DefaultComparer__ctor_m1484457948_gshared (DefaultComparer_t2618593038 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3278653252 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3278653252 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3278653252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Object>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2642614607_gshared (DefaultComparer_t2618593038 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		NullCheck((Il2CppObject *)(*(&___obj0)));
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)(*(&___obj0)));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Object>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1585251629_gshared (DefaultComparer_t2618593038 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Il2CppObject * L_2 = ___y1;
		NullCheck((Il2CppObject *)(*(&___x0)));
		bool L_3 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*(&___x0)), (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void DefaultComparer__ctor_m200415707_gshared (DefaultComparer_t1507389656 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t2167449870 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2167449870 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2167449870 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeNamedArgument>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1622792632_gshared (DefaultComparer_t1507389656 * __this, CustomAttributeNamedArgument_t3059612989  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = CustomAttributeNamedArgument_GetHashCode_m910635190((CustomAttributeNamedArgument_t3059612989 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeNamedArgument>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m4152684784_gshared (DefaultComparer_t1507389656 * __this, CustomAttributeNamedArgument_t3059612989  ___x0, CustomAttributeNamedArgument_t3059612989  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		CustomAttributeNamedArgument_t3059612989  L_1 = ___y1;
		CustomAttributeNamedArgument_t3059612989  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		CustomAttributeNamedArgument_t3059612989  L_4 = ___y1;
		CustomAttributeNamedArgument_t3059612989  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = CustomAttributeNamedArgument_Equals_m109042642((CustomAttributeNamedArgument_t3059612989 *)(&___x0), (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void DefaultComparer__ctor_m2339377356_gshared (DefaultComparer_t1749070089 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t2409130303 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2409130303 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2409130303 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m381051303_gshared (DefaultComparer_t1749070089 * __this, CustomAttributeTypedArgument_t3301293422  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = CustomAttributeTypedArgument_GetHashCode_m1079175269((CustomAttributeTypedArgument_t3301293422 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m140248929_gshared (DefaultComparer_t1749070089 * __this, CustomAttributeTypedArgument_t3301293422  ___x0, CustomAttributeTypedArgument_t3301293422  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		CustomAttributeTypedArgument_t3301293422  L_1 = ___y1;
		CustomAttributeTypedArgument_t3301293422  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		CustomAttributeTypedArgument_t3301293422  L_4 = ___y1;
		CustomAttributeTypedArgument_t3301293422  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = CustomAttributeTypedArgument_Equals_m464286849((CustomAttributeTypedArgument_t3301293422 *)(&___x0), (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Single>::.ctor()
extern "C"  void DefaultComparer__ctor_m1142419877_gshared (DefaultComparer_t2739695639 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3399755853 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3399755853 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3399755853 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Single>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m738872166_gshared (DefaultComparer_t2739695639 * __this, float ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Single_GetHashCode_m65342520((float*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Single>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m4203602550_gshared (DefaultComparer_t2739695639 * __this, float ___x0, float ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		float L_1 = ___y1;
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		float L_4 = ___y1;
		float L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = Single_Equals_m2650902624((float*)(&___x0), (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::.ctor()
extern "C"  void DefaultComparer__ctor_m3757819988_gshared (DefaultComparer_t3156266950 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3816327164 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3816327164 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3816327164 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1710312151_gshared (DefaultComparer_t3156266950 * __this, TimeSpan_t413522987  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = TimeSpan_GetHashCode_m3188156777((TimeSpan_t413522987 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m327676453_gshared (DefaultComparer_t3156266950 * __this, TimeSpan_t413522987  ___x0, TimeSpan_t413522987  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		TimeSpan_t413522987  L_1 = ___y1;
		TimeSpan_t413522987  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		TimeSpan_t413522987  L_4 = ___y1;
		TimeSpan_t413522987  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = TimeSpan_Equals_m2969422609((TimeSpan_t413522987 *)(&___x0), (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.UInt16>::.ctor()
extern "C"  void DefaultComparer__ctor_m3431451516_gshared (DefaultComparer_t2767411886 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3427472100 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3427472100 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3427472100 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.UInt16>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m903938479_gshared (DefaultComparer_t2767411886 * __this, uint16_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = UInt16_GetHashCode_m592888001((uint16_t*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.UInt16>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2973694157_gshared (DefaultComparer_t2767411886 * __this, uint16_t ___x0, uint16_t ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		uint16_t L_1 = ___y1;
		uint16_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		uint16_t L_4 = ___y1;
		uint16_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = UInt16_Equals_m857648105((uint16_t*)(&___x0), (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.UInt32>::.ctor()
extern "C"  void DefaultComparer__ctor_m2287487798_gshared (DefaultComparer_t2767411944 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3427472158 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3427472158 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3427472158 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.UInt32>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3203284149_gshared (DefaultComparer_t2767411944 * __this, uint32_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = UInt32_GetHashCode_m1046676807((uint32_t*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.UInt32>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m217385863_gshared (DefaultComparer_t2767411944 * __this, uint32_t ___x0, uint32_t ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		uint32_t L_1 = ___y1;
		uint32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		uint32_t L_4 = ___y1;
		uint32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = UInt32_Equals_m1266223727((uint32_t*)(&___x0), (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<TypewriterEffect/FadeEntry>::.ctor()
extern "C"  void DefaultComparer__ctor_m2301115281_gshared (DefaultComparer_t1306248768 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t1966308982 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1966308982 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1966308982 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<TypewriterEffect/FadeEntry>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1416239426_gshared (DefaultComparer_t1306248768 * __this, FadeEntry_t2858472101  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<TypewriterEffect/FadeEntry>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m184229030_gshared (DefaultComparer_t1306248768 * __this, FadeEntry_t2858472101  ___x0, FadeEntry_t2858472101  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		FadeEntry_t2858472101  L_1 = ___y1;
		FadeEntry_t2858472101  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		FadeEntry_t2858472101  L_4 = ___y1;
		FadeEntry_t2858472101  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UICamera/DepthEntry>::.ctor()
extern "C"  void DefaultComparer__ctor_m2243110147_gshared (DefaultComparer_t3888358432 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t253451350 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t253451350 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t253451350 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UICamera/DepthEntry>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m82390344_gshared (DefaultComparer_t3888358432 * __this, DepthEntry_t1145614469  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UICamera/DepthEntry>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2927710292_gshared (DefaultComparer_t3888358432 * __this, DepthEntry_t1145614469  ___x0, DepthEntry_t1145614469  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		DepthEntry_t1145614469  L_1 = ___y1;
		DepthEntry_t1145614469  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		DepthEntry_t1145614469  L_4 = ___y1;
		DepthEntry_t1145614469  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UIModelDisplayType>::.ctor()
extern "C"  void DefaultComparer__ctor_m1980002387_gshared (DefaultComparer_t339529346 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t999589560 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t999589560 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t999589560 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UIModelDisplayType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1379600960_gshared (DefaultComparer_t339529346 * __this, int32_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UIModelDisplayType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1301922408_gshared (DefaultComparer_t339529346 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Bounds>::.ctor()
extern "C"  void DefaultComparer__ctor_m2256559576_gshared (DefaultComparer_t1159418516 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t1819478730 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1819478730 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1819478730 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Bounds>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1072090395_gshared (DefaultComparer_t1159418516 * __this, Bounds_t2711641849  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Bounds_GetHashCode_m3067388679((Bounds_t2711641849 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Bounds>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3148440941_gshared (DefaultComparer_t1159418516 * __this, Bounds_t2711641849  ___x0, Bounds_t2711641849  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		Bounds_t2711641849  L_1 = ___y1;
		Bounds_t2711641849  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Bounds_t2711641849  L_4 = ___y1;
		Bounds_t2711641849  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = Bounds_Equals_m2517451939((Bounds_t2711641849 *)(&___x0), (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color>::.ctor()
extern "C"  void DefaultComparer__ctor_m2683732618_gshared (DefaultComparer_t2642323572 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3302383786 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3302383786 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3302383786 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m45379809_gshared (DefaultComparer_t2642323572 * __this, Color_t4194546905  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Color_GetHashCode_m170503301((Color_t4194546905 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3300239835_gshared (DefaultComparer_t2642323572 * __this, Color_t4194546905  ___x0, Color_t4194546905  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		Color_t4194546905  L_1 = ___y1;
		Color_t4194546905  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Color_t4194546905  L_4 = ___y1;
		Color_t4194546905  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = Color_Equals_m3016668205((Color_t4194546905 *)(&___x0), (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color32>::.ctor()
extern "C"  void DefaultComparer__ctor_m380784873_gshared (DefaultComparer_t3341597651 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t4001657865 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t4001657865 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t4001657865 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color32>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1306653346_gshared (DefaultComparer_t3341597651 * __this, Color32_t598853688  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color32>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m312219962_gshared (DefaultComparer_t3341597651 * __this, Color32_t598853688  ___x0, Color32_t598853688  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		Color32_t598853688  L_1 = ___y1;
		Color32_t598853688  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Color32_t598853688  L_4 = ___y1;
		Color32_t598853688  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C"  void DefaultComparer__ctor_m3711332581_gshared (DefaultComparer_t2210438031 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t2870498245 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2870498245 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2870498245 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3996321646_gshared (DefaultComparer_t2210438031 * __this, RaycastResult_t3762661364  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m637293818_gshared (DefaultComparer_t2210438031 * __this, RaycastResult_t3762661364  ___x0, RaycastResult_t3762661364  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		RaycastResult_t3762661364  L_1 = ___y1;
		RaycastResult_t3762661364  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		RaycastResult_t3762661364  L_4 = ___y1;
		RaycastResult_t3762661364  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::.ctor()
extern "C"  void DefaultComparer__ctor_m1780700700_gshared (DefaultComparer_t2569742905 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3229803119 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3229803119 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3229803119 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m4242040079_gshared (DefaultComparer_t2569742905 * __this, MeshInstance_t4121966238  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1290398829_gshared (DefaultComparer_t2569742905 * __this, MeshInstance_t4121966238  ___x0, MeshInstance_t4121966238  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		MeshInstance_t4121966238  L_1 = ___y1;
		MeshInstance_t4121966238  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		MeshInstance_t4121966238  L_4 = ___y1;
		MeshInstance_t4121966238  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::.ctor()
extern "C"  void DefaultComparer__ctor_m2380955250_gshared (DefaultComparer_t822616353 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t1482676567 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1482676567 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1482676567 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3110785601_gshared (DefaultComparer_t822616353 * __this, SubMeshInstance_t2374839686  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1084704903_gshared (DefaultComparer_t822616353 * __this, SubMeshInstance_t2374839686  ___x0, SubMeshInstance_t2374839686  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		SubMeshInstance_t2374839686  L_1 = ___y1;
		SubMeshInstance_t2374839686  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		SubMeshInstance_t2374839686  L_4 = ___y1;
		SubMeshInstance_t2374839686  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.RaycastHit>::.ctor()
extern "C"  void DefaultComparer__ctor_m1714860621_gshared (DefaultComparer_t2450952393 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3111012607 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3111012607 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3111012607 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.RaycastHit>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m4011927814_gshared (DefaultComparer_t2450952393 * __this, RaycastHit_t4003175726  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.RaycastHit>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m977725026_gshared (DefaultComparer_t2450952393 * __this, RaycastHit_t4003175726  ___x0, RaycastHit_t4003175726  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		RaycastHit_t4003175726  L_1 = ___y1;
		RaycastHit_t4003175726  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		RaycastHit_t4003175726  L_4 = ___y1;
		RaycastHit_t4003175726  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Rendering.ReflectionProbeBlendInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m2938174079_gshared (DefaultComparer_t3823341701 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t188434619 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t188434619 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t188434619 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Rendering.ReflectionProbeBlendInfo>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m22001812_gshared (DefaultComparer_t3823341701 * __this, ReflectionProbeBlendInfo_t1080597738  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Rendering.ReflectionProbeBlendInfo>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2974474132_gshared (DefaultComparer_t3823341701 * __this, ReflectionProbeBlendInfo_t1080597738  ___x0, ReflectionProbeBlendInfo_t1080597738  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		ReflectionProbeBlendInfo_t1080597738  L_1 = ___y1;
		ReflectionProbeBlendInfo_t1080597738  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		ReflectionProbeBlendInfo_t1080597738  L_4 = ___y1;
		ReflectionProbeBlendInfo_t1080597738  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.TextEditor/TextEditOp>::.ctor()
extern "C"  void DefaultComparer__ctor_m1649439764_gshared (DefaultComparer_t2593737777 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3253797991 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3253797991 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3253797991 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.TextEditor/TextEditOp>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m693622807_gshared (DefaultComparer_t2593737777 * __this, int32_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.TextEditor/TextEditOp>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m26970725_gshared (DefaultComparer_t2593737777 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m259584027_gshared (DefaultComparer_t2808551447 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3468611661 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3468611661 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3468611661 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m4135741304_gshared (DefaultComparer_t2808551447 * __this, UICharInfo_t65807484  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3648621872_gshared (DefaultComparer_t2808551447 * __this, UICharInfo_t65807484  ___x0, UICharInfo_t65807484  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		UICharInfo_t65807484  L_1 = ___y1;
		UICharInfo_t65807484  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		UICharInfo_t65807484  L_4 = ___y1;
		UICharInfo_t65807484  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m763863097_gshared (DefaultComparer_t2561652149 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3221712363 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3221712363 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3221712363 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2280716698_gshared (DefaultComparer_t2561652149 * __this, UILineInfo_t4113875482  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m4065787470_gshared (DefaultComparer_t2561652149 * __this, UILineInfo_t4113875482  ___x0, UILineInfo_t4113875482  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		UILineInfo_t4113875482  L_1 = ___y1;
		UILineInfo_t4113875482  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		UILineInfo_t4113875482  L_4 = ___y1;
		UILineInfo_t4113875482  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::.ctor()
extern "C"  void DefaultComparer__ctor_m642647323_gshared (DefaultComparer_t2691841879 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3351902093 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3351902093 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3351902093 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1002290040_gshared (DefaultComparer_t2691841879 * __this, UIVertex_t4244065212  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2723273392_gshared (DefaultComparer_t2691841879 * __this, UIVertex_t4244065212  ___x0, UIVertex_t4244065212  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		UIVertex_t4244065212  L_1 = ___y1;
		UIVertex_t4244065212  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		UIVertex_t4244065212  L_4 = ___y1;
		UIVertex_t4244065212  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___x0));
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector2>::.ctor()
extern "C"  void DefaultComparer__ctor_m2640835318_gshared (DefaultComparer_t2729843232 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3389903446 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3389903446 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3389903446 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector2>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2254526453_gshared (DefaultComparer_t2729843232 * __this, Vector2_t4282066565  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Vector2_GetHashCode_m128434585((Vector2_t4282066565 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector2>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m577236167_gshared (DefaultComparer_t2729843232 * __this, Vector2_t4282066565  ___x0, Vector2_t4282066565  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		Vector2_t4282066565  L_1 = ___y1;
		Vector2_t4282066565  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Vector2_t4282066565  L_4 = ___y1;
		Vector2_t4282066565  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = Vector2_Equals_m3404198849((Vector2_t4282066565 *)(&___x0), (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector3>::.ctor()
extern "C"  void DefaultComparer__ctor_m843883959_gshared (DefaultComparer_t2729843233 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3389903447 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3389903447 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3389903447 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector3>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1257454100_gshared (DefaultComparer_t2729843233 * __this, Vector3_t4282066566  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Vector3_GetHashCode_m3912867704((Vector3_t4282066566 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector3>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2084787976_gshared (DefaultComparer_t2729843233 * __this, Vector3_t4282066566  ___x0, Vector3_t4282066566  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		Vector3_t4282066566  L_1 = ___y1;
		Vector3_t4282066566  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Vector3_t4282066566  L_4 = ___y1;
		Vector3_t4282066566  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = Vector3_Equals_m3337192096((Vector3_t4282066566 *)(&___x0), (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector4>::.ctor()
extern "C"  void DefaultComparer__ctor_m3341899896_gshared (DefaultComparer_t2729843234 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3389903448 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3389903448 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3389903448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector4>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m260381747_gshared (DefaultComparer_t2729843234 * __this, Vector4_t4282066567  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Vector4_GetHashCode_m3402333527((Vector4_t4282066567 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector4>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3592339785_gshared (DefaultComparer_t2729843234 * __this, Vector4_t4282066567  ___x0, Vector4_t4282066567  ___y1, const MethodInfo* method)
{
	{
		goto IL_0015;
	}
	{
		Vector4_t4282066567  L_1 = ___y1;
		Vector4_t4282066567  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Vector4_t4282066567  L_4 = ___y1;
		Vector4_t4282066567  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		bool L_7 = Vector4_Equals_m3270185343((Vector4_t4282066567 *)(&___x0), (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<AIEnum.EAIEventtype>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m946526027_gshared (EqualityComparer_1_t3004126192 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<AIEnum.EAIEventtype>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m3090406850_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m3090406850_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3090406850_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3004126192_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t3004126192 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2344065978 * L_8 = (DefaultComparer_t2344065978 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2344065978 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3004126192_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<AIEnum.EAIEventtype>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3845787684_gshared (EqualityComparer_1_t3004126192 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3004126192 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, uint8_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<AIEnum.EAIEventtype>::GetHashCode(T) */, (EqualityComparer_1_t3004126192 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<AIEnum.EAIEventtype>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1538945254_gshared (EqualityComparer_1_t3004126192 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3004126192 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, uint8_t, uint8_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<AIEnum.EAIEventtype>::Equals(T,T) */, (EqualityComparer_1_t3004126192 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<AIEnum.EAIEventtype>::get_Default()
extern "C"  EqualityComparer_1_t3004126192 * EqualityComparer_1_get_Default_m639505589_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3004126192 * L_0 = ((EqualityComparer_1_t3004126192_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<AnimationRunner/AniType>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2046666394_gshared (EqualityComparer_1_t114075532 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<AnimationRunner/AniType>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m2835019859_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m2835019859_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2835019859_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t114075532_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t114075532 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3748982614 * L_8 = (DefaultComparer_t3748982614 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3748982614 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t114075532_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<AnimationRunner/AniType>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m249877619_gshared (EqualityComparer_1_t114075532 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t114075532 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<AnimationRunner/AniType>::GetHashCode(T) */, (EqualityComparer_1_t114075532 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<AnimationRunner/AniType>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m981119607_gshared (EqualityComparer_1_t114075532 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t114075532 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<AnimationRunner/AniType>::Equals(T,T) */, (EqualityComparer_1_t114075532 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<AnimationRunner/AniType>::get_Default()
extern "C"  EqualityComparer_1_t114075532 * EqualityComparer_1_get_Default_m1477563076_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t114075532 * L_0 = ((EqualityComparer_1_t114075532_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Core.RpsChoice>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m494331346_gshared (EqualityComparer_1_t3455601309 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Core.RpsChoice>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m1957273627_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m1957273627_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1957273627_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3455601309_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t3455601309 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2795541095 * L_8 = (DefaultComparer_t2795541095 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2795541095 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3455601309_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<Core.RpsChoice>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3510874723_gshared (EqualityComparer_1_t3455601309 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3455601309 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<Core.RpsChoice>::GetHashCode(T) */, (EqualityComparer_1_t3455601309 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<Core.RpsChoice>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2755287547_gshared (EqualityComparer_1_t3455601309 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3455601309 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<Core.RpsChoice>::Equals(T,T) */, (EqualityComparer_1_t3455601309 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Core.RpsChoice>::get_Default()
extern "C"  EqualityComparer_1_t3455601309 * EqualityComparer_1_get_Default_m1784548756_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3455601309 * L_0 = ((EqualityComparer_1_t3455601309_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Core.RpsResult>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m4222707022_gshared (EqualityComparer_1_t3882399001 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Core.RpsResult>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m1572802591_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m1572802591_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1572802591_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3882399001_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t3882399001 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3222338787 * L_8 = (DefaultComparer_t3222338787 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3222338787 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3882399001_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<Core.RpsResult>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1571392991_gshared (EqualityComparer_1_t3882399001 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3882399001 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<Core.RpsResult>::GetHashCode(T) */, (EqualityComparer_1_t3882399001 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<Core.RpsResult>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1624091135_gshared (EqualityComparer_1_t3882399001 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3882399001 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<Core.RpsResult>::Equals(T,T) */, (EqualityComparer_1_t3882399001 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Core.RpsResult>::get_Default()
extern "C"  EqualityComparer_1_t3882399001 * EqualityComparer_1_get_Default_m201457680_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3882399001 * L_0 = ((EqualityComparer_1_t3882399001_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Entity.Behavior.EBehaviorID>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m123014912_gshared (EqualityComparer_1_t346370017 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Entity.Behavior.EBehaviorID>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m3331366061_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m3331366061_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3331366061_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t346370017_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t346370017 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3981277099 * L_8 = (DefaultComparer_t3981277099 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3981277099 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t346370017_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<Entity.Behavior.EBehaviorID>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3912958169_gshared (EqualityComparer_1_t346370017 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t346370017 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, uint8_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<Entity.Behavior.EBehaviorID>::GetHashCode(T) */, (EqualityComparer_1_t346370017 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<Entity.Behavior.EBehaviorID>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3555145169_gshared (EqualityComparer_1_t346370017 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t346370017 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, uint8_t, uint8_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<Entity.Behavior.EBehaviorID>::Equals(T,T) */, (EqualityComparer_1_t346370017 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Entity.Behavior.EBehaviorID>::get_Default()
extern "C"  EqualityComparer_1_t346370017 * EqualityComparer_1_get_Default_m2290072746_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t346370017 * L_0 = ((EqualityComparer_1_t346370017_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<FLOAT_TEXT_ID>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2065780489_gshared (EqualityComparer_1_t1307182651 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<FLOAT_TEXT_ID>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m3427556804_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m3427556804_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3427556804_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1307182651_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t1307182651 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t647122437 * L_8 = (DefaultComparer_t647122437 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t647122437 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1307182651_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<FLOAT_TEXT_ID>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m4222472418_gshared (EqualityComparer_1_t1307182651 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1307182651 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<FLOAT_TEXT_ID>::GetHashCode(T) */, (EqualityComparer_1_t1307182651 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<FLOAT_TEXT_ID>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2878573160_gshared (EqualityComparer_1_t1307182651 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1307182651 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<FLOAT_TEXT_ID>::Equals(T,T) */, (EqualityComparer_1_t1307182651 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<FLOAT_TEXT_ID>::get_Default()
extern "C"  EqualityComparer_1_t1307182651 * EqualityComparer_1_get_Default_m1809566963_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1307182651 * L_0 = ((EqualityComparer_1_t1307182651_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<HatredCtrl/stValue>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3426510829_gshared (EqualityComparer_1_t2533782291 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<HatredCtrl/stValue>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m2660524384_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m2660524384_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2660524384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2533782291_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t2533782291 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1873722077 * L_8 = (DefaultComparer_t1873722077 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1873722077 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2533782291_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<HatredCtrl/stValue>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1492203902_gshared (EqualityComparer_1_t2533782291 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2533782291 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, stValue_t3425945410  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<HatredCtrl/stValue>::GetHashCode(T) */, (EqualityComparer_1_t2533782291 *)__this, (stValue_t3425945410 )((*(stValue_t3425945410 *)((stValue_t3425945410 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<HatredCtrl/stValue>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m578872384_gshared (EqualityComparer_1_t2533782291 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2533782291 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, stValue_t3425945410 , stValue_t3425945410  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<HatredCtrl/stValue>::Equals(T,T) */, (EqualityComparer_1_t2533782291 *)__this, (stValue_t3425945410 )((*(stValue_t3425945410 *)((stValue_t3425945410 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (stValue_t3425945410 )((*(stValue_t3425945410 *)((stValue_t3425945410 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<HatredCtrl/stValue>::get_Default()
extern "C"  EqualityComparer_1_t2533782291 * EqualityComparer_1_get_Default_m4034832751_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2533782291 * L_0 = ((EqualityComparer_1_t2533782291_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m287671237_gshared (EqualityComparer_1_t1741818091 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m4140744840_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m4140744840_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m4140744840_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1741818091_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t1741818091 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1081757877 * L_8 = (DefaultComparer_t1081757877 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1081757877 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1741818091_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2350855510_gshared (EqualityComparer_1_t1741818091 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1741818091 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, ErrorInfo_t2633981210  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::GetHashCode(T) */, (EqualityComparer_1_t1741818091 *)__this, (ErrorInfo_t2633981210 )((*(ErrorInfo_t2633981210 *)((ErrorInfo_t2633981210 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2374682472_gshared (EqualityComparer_1_t1741818091 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1741818091 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, ErrorInfo_t2633981210 , ErrorInfo_t2633981210  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::Equals(T,T) */, (EqualityComparer_1_t1741818091 *)__this, (ErrorInfo_t2633981210 )((*(ErrorInfo_t2633981210 *)((ErrorInfo_t2633981210 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (ErrorInfo_t2633981210 )((*(ErrorInfo_t2633981210 *)((ErrorInfo_t2633981210 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Default()
extern "C"  EqualityComparer_1_t1741818091 * EqualityComparer_1_get_Default_m1874389831_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1741818091 * L_0 = ((EqualityComparer_1_t1741818091_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<MScrollView/MoveWay>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1607527053_gshared (EqualityComparer_1_t2582778431 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<MScrollView/MoveWay>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m2106602176_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m2106602176_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2106602176_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2582778431_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t2582778431 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1922718217 * L_8 = (DefaultComparer_t1922718217 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1922718217 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2582778431_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<MScrollView/MoveWay>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1333097830_gshared (EqualityComparer_1_t2582778431 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2582778431 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<MScrollView/MoveWay>::GetHashCode(T) */, (EqualityComparer_1_t2582778431 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<MScrollView/MoveWay>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m946853348_gshared (EqualityComparer_1_t2582778431 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2582778431 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<MScrollView/MoveWay>::Equals(T,T) */, (EqualityComparer_1_t2582778431 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<MScrollView/MoveWay>::get_Default()
extern "C"  EqualityComparer_1_t2582778431 * EqualityComparer_1_get_Default_m1657089655_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2582778431 * L_0 = ((EqualityComparer_1_t2582778431_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Linq.JTokenType>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m22489153_gshared (EqualityComparer_1_t3024734442 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Linq.JTokenType>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m215067532_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m215067532_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m215067532_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3024734442_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t3024734442 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2364674228 * L_8 = (DefaultComparer_t2364674228 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2364674228 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3024734442_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2026410266_gshared (EqualityComparer_1_t3024734442 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3024734442 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Linq.JTokenType>::GetHashCode(T) */, (EqualityComparer_1_t3024734442 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3375954864_gshared (EqualityComparer_1_t3024734442 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3024734442 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Linq.JTokenType>::Equals(T,T) */, (EqualityComparer_1_t3024734442 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Linq.JTokenType>::get_Default()
extern "C"  EqualityComparer_1_t3024734442 * EqualityComparer_1_get_Default_m753272619_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3024734442 * L_0 = ((EqualityComparer_1_t3024734442_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Schema.JsonSchemaType>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2534042234_gshared (EqualityComparer_1_t1223252702 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Schema.JsonSchemaType>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m763801715_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m763801715_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m763801715_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1223252702_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t1223252702 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t563192488 * L_8 = (DefaultComparer_t563192488 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t563192488 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1223252702_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1822751059_gshared (EqualityComparer_1_t1223252702 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1223252702 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Schema.JsonSchemaType>::GetHashCode(T) */, (EqualityComparer_1_t1223252702 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m567869207_gshared (EqualityComparer_1_t1223252702 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1223252702 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Schema.JsonSchemaType>::Equals(T,T) */, (EqualityComparer_1_t1223252702 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Schema.JsonSchemaType>::get_Default()
extern "C"  EqualityComparer_1_t1223252702 * EqualityComparer_1_get_Default_m2906272676_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1223252702 * L_0 = ((EqualityComparer_1_t1223252702_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1304624546_gshared (EqualityComparer_1_t2079681672 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m1306559051_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m1306559051_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1306559051_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2079681672_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t2079681672 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1419621458 * L_8 = (DefaultComparer_t1419621458 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1419621458 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2079681672_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1469787443_gshared (EqualityComparer_1_t2079681672 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2079681672 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, TypeNameKey_t2971844791  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::GetHashCode(T) */, (EqualityComparer_1_t2079681672 *)__this, (TypeNameKey_t2971844791 )((*(TypeNameKey_t2971844791 *)((TypeNameKey_t2971844791 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m519012779_gshared (EqualityComparer_1_t2079681672 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2079681672 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, TypeNameKey_t2971844791 , TypeNameKey_t2971844791  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::Equals(T,T) */, (EqualityComparer_1_t2079681672 *)__this, (TypeNameKey_t2971844791 )((*(TypeNameKey_t2971844791 *)((TypeNameKey_t2971844791 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (TypeNameKey_t2971844791 )((*(TypeNameKey_t2971844791 *)((TypeNameKey_t2971844791 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::get_Default()
extern "C"  EqualityComparer_1_t2079681672 * EqualityComparer_1_get_Default_m1361635428_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2079681672 * L_0 = ((EqualityComparer_1_t2079681672_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2102666033_gshared (EqualityComparer_1_t2000166371 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m276041372_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m276041372_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m276041372_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2000166371_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t2000166371 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1340106157 * L_8 = (DefaultComparer_t1340106157 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1340106157 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2000166371_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1978136074_gshared (EqualityComparer_1_t2000166371 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2000166371 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::GetHashCode(T) */, (EqualityComparer_1_t2000166371 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3940213696_gshared (EqualityComparer_1_t2000166371 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2000166371 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::Equals(T,T) */, (EqualityComparer_1_t2000166371 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::get_Default()
extern "C"  EqualityComparer_1_t2000166371 * EqualityComparer_1_get_Default_m2184310811_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2000166371 * L_0 = ((EqualityComparer_1_t2000166371_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2855946813_gshared (EqualityComparer_1_t4268938351 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m2152909072_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m2152909072_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2152909072_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t4268938351_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t4268938351 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3608878137 * L_8 = (DefaultComparer_t3608878137 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3608878137 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t4268938351_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3071241238_gshared (EqualityComparer_1_t4268938351 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t4268938351 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, TypeConvertKey_t866134174  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::GetHashCode(T) */, (EqualityComparer_1_t4268938351 *)__this, (TypeConvertKey_t866134174 )((*(TypeConvertKey_t866134174 *)((TypeConvertKey_t866134174 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2576772020_gshared (EqualityComparer_1_t4268938351 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t4268938351 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, TypeConvertKey_t866134174 , TypeConvertKey_t866134174  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::Equals(T,T) */, (EqualityComparer_1_t4268938351 *)__this, (TypeConvertKey_t866134174 )((*(TypeConvertKey_t866134174 *)((TypeConvertKey_t866134174 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (TypeConvertKey_t866134174 )((*(TypeConvertKey_t866134174 *)((TypeConvertKey_t866134174 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::get_Default()
extern "C"  EqualityComparer_1_t4268938351 * EqualityComparer_1_get_Default_m374181159_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t4268938351 * L_0 = ((EqualityComparer_1_t4268938351_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.AdvancedSmooth/Turn>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m691292561_gshared (EqualityComparer_1_t3867999555 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.AdvancedSmooth/Turn>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m3768103996_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m3768103996_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3768103996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3867999555_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t3867999555 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3207939341 * L_8 = (DefaultComparer_t3207939341 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3207939341 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3867999555_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m957448810_gshared (EqualityComparer_1_t3867999555 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3867999555 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, Turn_t465195378  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<Pathfinding.AdvancedSmooth/Turn>::GetHashCode(T) */, (EqualityComparer_1_t3867999555 *)__this, (Turn_t465195378 )((*(Turn_t465195378 *)((Turn_t465195378 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1415742560_gshared (EqualityComparer_1_t3867999555 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3867999555 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, Turn_t465195378 , Turn_t465195378  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<Pathfinding.AdvancedSmooth/Turn>::Equals(T,T) */, (EqualityComparer_1_t3867999555 *)__this, (Turn_t465195378 )((*(Turn_t465195378 *)((Turn_t465195378 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Turn_t465195378 )((*(Turn_t465195378 *)((Turn_t465195378 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Pathfinding.AdvancedSmooth/Turn>::get_Default()
extern "C"  EqualityComparer_1_t3867999555 * EqualityComparer_1_get_Default_m1889070203_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3867999555 * L_0 = ((EqualityComparer_1_t3867999555_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.ClipperLib.IntPoint>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1603881388_gshared (EqualityComparer_1_t2433963060 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.ClipperLib.IntPoint>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m1993586561_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m1993586561_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1993586561_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2433963060_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t2433963060 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1773902846 * L_8 = (DefaultComparer_t1773902846 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1773902846 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2433963060_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m4078138757_gshared (EqualityComparer_1_t2433963060 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2433963060 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, IntPoint_t3326126179  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<Pathfinding.ClipperLib.IntPoint>::GetHashCode(T) */, (EqualityComparer_1_t2433963060 *)__this, (IntPoint_t3326126179 )((*(IntPoint_t3326126179 *)((IntPoint_t3326126179 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3315654565_gshared (EqualityComparer_1_t2433963060 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2433963060 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, IntPoint_t3326126179 , IntPoint_t3326126179  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<Pathfinding.ClipperLib.IntPoint>::Equals(T,T) */, (EqualityComparer_1_t2433963060 *)__this, (IntPoint_t3326126179 )((*(IntPoint_t3326126179 *)((IntPoint_t3326126179 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (IntPoint_t3326126179 )((*(IntPoint_t3326126179 *)((IntPoint_t3326126179 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Pathfinding.ClipperLib.IntPoint>::get_Default()
extern "C"  EqualityComparer_1_t2433963060 * EqualityComparer_1_get_Default_m461778006_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2433963060 * L_0 = ((EqualityComparer_1_t2433963060_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.Int2>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1377140408_gshared (EqualityComparer_1_t1081882474 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.Int2>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m3554550773_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m3554550773_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3554550773_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1081882474_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t1081882474 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t421822260 * L_8 = (DefaultComparer_t421822260 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t421822260 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1081882474_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<Pathfinding.Int2>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1094322761_gshared (EqualityComparer_1_t1081882474 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1081882474 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, Int2_t1974045593  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<Pathfinding.Int2>::GetHashCode(T) */, (EqualityComparer_1_t1081882474 *)__this, (Int2_t1974045593 )((*(Int2_t1974045593 *)((Int2_t1974045593 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<Pathfinding.Int2>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3372176981_gshared (EqualityComparer_1_t1081882474 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1081882474 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, Int2_t1974045593 , Int2_t1974045593  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<Pathfinding.Int2>::Equals(T,T) */, (EqualityComparer_1_t1081882474 *)__this, (Int2_t1974045593 )((*(Int2_t1974045593 *)((Int2_t1974045593 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Int2_t1974045593 )((*(Int2_t1974045593 *)((Int2_t1974045593 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Pathfinding.Int2>::get_Default()
extern "C"  EqualityComparer_1_t1081882474 * EqualityComparer_1_get_Default_m520204026_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1081882474 * L_0 = ((EqualityComparer_1_t1081882474_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.Int3>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3875156345_gshared (EqualityComparer_1_t1081882475 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.Int3>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m3683633492_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m3683633492_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3683633492_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1081882475_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t1081882475 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t421822261 * L_8 = (DefaultComparer_t421822261 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t421822261 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1081882475_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<Pathfinding.Int3>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m486245898_gshared (EqualityComparer_1_t1081882475 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1081882475 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, Int3_t1974045594  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<Pathfinding.Int3>::GetHashCode(T) */, (EqualityComparer_1_t1081882475 *)__this, (Int3_t1974045594 )((*(Int3_t1974045594 *)((Int3_t1974045594 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<Pathfinding.Int3>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m186024884_gshared (EqualityComparer_1_t1081882475 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1081882475 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, Int3_t1974045594 , Int3_t1974045594  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<Pathfinding.Int3>::Equals(T,T) */, (EqualityComparer_1_t1081882475 *)__this, (Int3_t1974045594 )((*(Int3_t1974045594 *)((Int3_t1974045594 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Int3_t1974045594 )((*(Int3_t1974045594 *)((Int3_t1974045594 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Pathfinding.Int3>::get_Default()
extern "C"  EqualityComparer_1_t1081882475 * EqualityComparer_1_get_Default_m1873513723_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1081882475 * L_0 = ((EqualityComparer_1_t1081882475_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.IntRect>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m892892264_gshared (EqualityComparer_1_t2122895142 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.IntRect>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m1427760197_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m1427760197_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1427760197_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2122895142_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t2122895142 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1462834928 * L_8 = (DefaultComparer_t1462834928 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1462834928 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2122895142_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<Pathfinding.IntRect>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m575779393_gshared (EqualityComparer_1_t2122895142 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2122895142 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, IntRect_t3015058261  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<Pathfinding.IntRect>::GetHashCode(T) */, (EqualityComparer_1_t2122895142 *)__this, (IntRect_t3015058261 )((*(IntRect_t3015058261 *)((IntRect_t3015058261 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<Pathfinding.IntRect>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m241885033_gshared (EqualityComparer_1_t2122895142 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2122895142 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, IntRect_t3015058261 , IntRect_t3015058261  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<Pathfinding.IntRect>::Equals(T,T) */, (EqualityComparer_1_t2122895142 *)__this, (IntRect_t3015058261 )((*(IntRect_t3015058261 *)((IntRect_t3015058261 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (IntRect_t3015058261 )((*(IntRect_t3015058261 *)((IntRect_t3015058261 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Pathfinding.IntRect>::get_Default()
extern "C"  EqualityComparer_1_t2122895142 * EqualityComparer_1_get_Default_m3314984978_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2122895142 * L_0 = ((EqualityComparer_1_t2122895142_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.LocalAvoidance/IntersectionPair>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1536382414_gshared (EqualityComparer_1_t1929156800 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.LocalAvoidance/IntersectionPair>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m4196085663_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m4196085663_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m4196085663_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1929156800_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t1929156800 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1269096586 * L_8 = (DefaultComparer_t1269096586 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1269096586 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1929156800_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2528460199_gshared (EqualityComparer_1_t1929156800 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1929156800 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, IntersectionPair_t2821319919  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<Pathfinding.LocalAvoidance/IntersectionPair>::GetHashCode(T) */, (EqualityComparer_1_t1929156800 *)__this, (IntersectionPair_t2821319919 )((*(IntersectionPair_t2821319919 *)((IntersectionPair_t2821319919 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2074145987_gshared (EqualityComparer_1_t1929156800 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1929156800 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, IntersectionPair_t2821319919 , IntersectionPair_t2821319919  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<Pathfinding.LocalAvoidance/IntersectionPair>::Equals(T,T) */, (EqualityComparer_1_t1929156800 *)__this, (IntersectionPair_t2821319919 )((*(IntersectionPair_t2821319919 *)((IntersectionPair_t2821319919 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (IntersectionPair_t2821319919 )((*(IntersectionPair_t2821319919 *)((IntersectionPair_t2821319919 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Pathfinding.LocalAvoidance/IntersectionPair>::get_Default()
extern "C"  EqualityComparer_1_t1929156800 * EqualityComparer_1_get_Default_m2684012792_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1929156800 * L_0 = ((EqualityComparer_1_t1929156800_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.LocalAvoidance/VOLine>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3523305976_gshared (EqualityComparer_1_t1137768682 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.LocalAvoidance/VOLine>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m1366206645_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m1366206645_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1366206645_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1137768682_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t1137768682 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t477708468 * L_8 = (DefaultComparer_t477708468 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t477708468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1137768682_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m493727953_gshared (EqualityComparer_1_t1137768682 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1137768682 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, VOLine_t2029931801  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<Pathfinding.LocalAvoidance/VOLine>::GetHashCode(T) */, (EqualityComparer_1_t1137768682 *)__this, (VOLine_t2029931801 )((*(VOLine_t2029931801 *)((VOLine_t2029931801 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1896216665_gshared (EqualityComparer_1_t1137768682 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1137768682 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, VOLine_t2029931801 , VOLine_t2029931801  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<Pathfinding.LocalAvoidance/VOLine>::Equals(T,T) */, (EqualityComparer_1_t1137768682 *)__this, (VOLine_t2029931801 )((*(VOLine_t2029931801 *)((VOLine_t2029931801 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (VOLine_t2029931801 )((*(VOLine_t2029931801 *)((VOLine_t2029931801 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Pathfinding.LocalAvoidance/VOLine>::get_Default()
extern "C"  EqualityComparer_1_t1137768682 * EqualityComparer_1_get_Default_m1896591010_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1137768682 * L_0 = ((EqualityComparer_1_t1137768682_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.Voxels.ExtraMesh>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2024667665_gshared (EqualityComparer_1_t3325866596 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.Voxels.ExtraMesh>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m2153059260_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m2153059260_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2153059260_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3325866596_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t3325866596 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2665806382 * L_8 = (DefaultComparer_t2665806382 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2665806382 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3325866596_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1132944546_gshared (EqualityComparer_1_t3325866596 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3325866596 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, ExtraMesh_t4218029715  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<Pathfinding.Voxels.ExtraMesh>::GetHashCode(T) */, (EqualityComparer_1_t3325866596 *)__this, (ExtraMesh_t4218029715 )((*(ExtraMesh_t4218029715 *)((ExtraMesh_t4218029715 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m199467292_gshared (EqualityComparer_1_t3325866596 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3325866596 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, ExtraMesh_t4218029715 , ExtraMesh_t4218029715  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<Pathfinding.Voxels.ExtraMesh>::Equals(T,T) */, (EqualityComparer_1_t3325866596 *)__this, (ExtraMesh_t4218029715 )((*(ExtraMesh_t4218029715 *)((ExtraMesh_t4218029715 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (ExtraMesh_t4218029715 )((*(ExtraMesh_t4218029715 *)((ExtraMesh_t4218029715 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Pathfinding.Voxels.ExtraMesh>::get_Default()
extern "C"  EqualityComparer_1_t3325866596 * EqualityComparer_1_get_Default_m2588873107_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3325866596 * L_0 = ((EqualityComparer_1_t3325866596_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.Voxels.VoxelContour>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2963149482_gshared (EqualityComparer_1_t2705037897 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.Voxels.VoxelContour>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m1181224515_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m1181224515_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1181224515_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2705037897_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t2705037897 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2044977683 * L_8 = (DefaultComparer_t2044977683 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2044977683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2705037897_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1476471939_gshared (EqualityComparer_1_t2705037897 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2705037897 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, VoxelContour_t3597201016  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<Pathfinding.Voxels.VoxelContour>::GetHashCode(T) */, (EqualityComparer_1_t2705037897 *)__this, (VoxelContour_t3597201016 )((*(VoxelContour_t3597201016 *)((VoxelContour_t3597201016 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m849225319_gshared (EqualityComparer_1_t2705037897 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2705037897 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, VoxelContour_t3597201016 , VoxelContour_t3597201016  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<Pathfinding.Voxels.VoxelContour>::Equals(T,T) */, (EqualityComparer_1_t2705037897 *)__this, (VoxelContour_t3597201016 )((*(VoxelContour_t3597201016 *)((VoxelContour_t3597201016 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (VoxelContour_t3597201016 )((*(VoxelContour_t3597201016 *)((VoxelContour_t3597201016 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Pathfinding.Voxels.VoxelContour>::get_Default()
extern "C"  EqualityComparer_1_t2705037897 * EqualityComparer_1_get_Default_m4098080468_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2705037897 * L_0 = ((EqualityComparer_1_t2705037897_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<PointCloudRegognizer/Point>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2140974641_gshared (EqualityComparer_1_t946668631 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<PointCloudRegognizer/Point>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m1463608220_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m1463608220_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1463608220_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t946668631_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t946668631 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t286608417 * L_8 = (DefaultComparer_t286608417 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t286608417 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t946668631_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<PointCloudRegognizer/Point>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1937835970_gshared (EqualityComparer_1_t946668631 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t946668631 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, Point_t1838831750  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<PointCloudRegognizer/Point>::GetHashCode(T) */, (EqualityComparer_1_t946668631 *)__this, (Point_t1838831750 )((*(Point_t1838831750 *)((Point_t1838831750 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<PointCloudRegognizer/Point>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m4243122300_gshared (EqualityComparer_1_t946668631 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t946668631 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, Point_t1838831750 , Point_t1838831750  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<PointCloudRegognizer/Point>::Equals(T,T) */, (EqualityComparer_1_t946668631 *)__this, (Point_t1838831750 )((*(Point_t1838831750 *)((Point_t1838831750 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Point_t1838831750 )((*(Point_t1838831750 *)((Point_t1838831750 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<PointCloudRegognizer/Point>::get_Default()
extern "C"  EqualityComparer_1_t946668631 * EqualityComparer_1_get_Default_m3892043955_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t946668631 * L_0 = ((EqualityComparer_1_t946668631_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<ProductsCfgMgr/ProductInfo>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m399343089_gshared (EqualityComparer_1_t413828119 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<ProductsCfgMgr/ProductInfo>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m3307604956_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m3307604956_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3307604956_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t413828119_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t413828119 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t4048735201 * L_8 = (DefaultComparer_t4048735201 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t4048735201 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t413828119_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<ProductsCfgMgr/ProductInfo>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2650773378_gshared (EqualityComparer_1_t413828119 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t413828119 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, ProductInfo_t1305991238  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<ProductsCfgMgr/ProductInfo>::GetHashCode(T) */, (EqualityComparer_1_t413828119 *)__this, (ProductInfo_t1305991238 )((*(ProductInfo_t1305991238 *)((ProductInfo_t1305991238 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<ProductsCfgMgr/ProductInfo>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m4251979964_gshared (EqualityComparer_1_t413828119 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t413828119 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, ProductInfo_t1305991238 , ProductInfo_t1305991238  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<ProductsCfgMgr/ProductInfo>::Equals(T,T) */, (EqualityComparer_1_t413828119 *)__this, (ProductInfo_t1305991238 )((*(ProductInfo_t1305991238 *)((ProductInfo_t1305991238 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (ProductInfo_t1305991238 )((*(ProductInfo_t1305991238 *)((ProductInfo_t1305991238 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<ProductsCfgMgr/ProductInfo>::get_Default()
extern "C"  EqualityComparer_1_t413828119 * EqualityComparer_1_get_Default_m1925308531_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t413828119 * L_0 = ((EqualityComparer_1_t413828119_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<PushType>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1511945023_gshared (EqualityComparer_1_t948479333 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<PushType>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m3438526542_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m3438526542_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3438526542_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t948479333_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t948479333 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t288419119 * L_8 = (DefaultComparer_t288419119 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t288419119 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t948479333_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<PushType>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1302567376_gshared (EqualityComparer_1_t948479333 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t948479333 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<PushType>::GetHashCode(T) */, (EqualityComparer_1_t948479333 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<PushType>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3244542126_gshared (EqualityComparer_1_t948479333 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t948479333 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<PushType>::Equals(T,T) */, (EqualityComparer_1_t948479333 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<PushType>::get_Default()
extern "C"  EqualityComparer_1_t948479333 * EqualityComparer_1_get_Default_m190459457_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t948479333 * L_0 = ((EqualityComparer_1_t948479333_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<SoundStatus>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1351800864_gshared (EqualityComparer_1_t2700775826 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<SoundStatus>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m2769024909_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m2769024909_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2769024909_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2700775826_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t2700775826 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2040715612 * L_8 = (DefaultComparer_t2040715612 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2040715612 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2700775826_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<SoundStatus>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m4243862009_gshared (EqualityComparer_1_t2700775826 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2700775826 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<SoundStatus>::GetHashCode(T) */, (EqualityComparer_1_t2700775826 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<SoundStatus>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m274977969_gshared (EqualityComparer_1_t2700775826 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2700775826 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<SoundStatus>::Equals(T,T) */, (EqualityComparer_1_t2700775826 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<SoundStatus>::get_Default()
extern "C"  EqualityComparer_1_t2700775826 * EqualityComparer_1_get_Default_m4275344842_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2700775826 * L_0 = ((EqualityComparer_1_t2700775826_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<SoundTypeID>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2369387075_gshared (EqualityComparer_1_t2734453621 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<SoundTypeID>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m4249426378_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m4249426378_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m4249426378_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2734453621_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t2734453621 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2074393407 * L_8 = (DefaultComparer_t2074393407 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2074393407 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2734453621_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<SoundTypeID>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3842674460_gshared (EqualityComparer_1_t2734453621 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2734453621 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<SoundTypeID>::GetHashCode(T) */, (EqualityComparer_1_t2734453621 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<SoundTypeID>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m4113600238_gshared (EqualityComparer_1_t2734453621 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2734453621 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<SoundTypeID>::Equals(T,T) */, (EqualityComparer_1_t2734453621 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<SoundTypeID>::get_Default()
extern "C"  EqualityComparer_1_t2734453621 * EqualityComparer_1_get_Default_m2979287469_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2734453621 * L_0 = ((EqualityComparer_1_t2734453621_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Boolean>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1689064020_gshared (EqualityComparer_1_t3879602895 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Boolean>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m339280857_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m339280857_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m339280857_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3879602895_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t3879602895 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3219542681 * L_8 = (DefaultComparer_t3219542681 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3219542681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3879602895_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Boolean>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2234675429_gshared (EqualityComparer_1_t3879602895 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3879602895 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, bool >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Boolean>::GetHashCode(T) */, (EqualityComparer_1_t3879602895 *)__this, (bool)((*(bool*)((bool*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Boolean>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m189582777_gshared (EqualityComparer_1_t3879602895 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3879602895 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, bool, bool >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Boolean>::Equals(T,T) */, (EqualityComparer_1_t3879602895 *)__this, (bool)((*(bool*)((bool*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (bool)((*(bool*)((bool*)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Boolean>::get_Default()
extern "C"  EqualityComparer_1_t3879602895 * EqualityComparer_1_get_Default_m1613582486_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3879602895 * L_0 = ((EqualityComparer_1_t3879602895_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Byte>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m653747270_gshared (EqualityComparer_1_t1970446541 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Byte>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m2604199975_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m2604199975_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2604199975_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1970446541_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t1970446541 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1310386327 * L_8 = (DefaultComparer_t1310386327 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1310386327 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1970446541_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Byte>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3762164767_gshared (EqualityComparer_1_t1970446541 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1970446541 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, uint8_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Byte>::GetHashCode(T) */, (EqualityComparer_1_t1970446541 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Byte>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3892247883_gshared (EqualityComparer_1_t1970446541 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1970446541 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, uint8_t, uint8_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Byte>::Equals(T,T) */, (EqualityComparer_1_t1970446541 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Byte>::get_Default()
extern "C"  EqualityComparer_1_t1970446541 * EqualityComparer_1_get_Default_m1020063088_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1970446541 * L_0 = ((EqualityComparer_1_t1970446541_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3680071176_gshared (EqualityComparer_1_t1052505858 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m1930960549_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m1930960549_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1930960549_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1052505858_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t1052505858 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t392445644 * L_8 = (DefaultComparer_t392445644 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t392445644 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1052505858_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2216959129_gshared (EqualityComparer_1_t1052505858 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1052505858 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, KeyValuePair_2_t1944668977  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetHashCode(T) */, (EqualityComparer_1_t1052505858 *)__this, (KeyValuePair_2_t1944668977 )((*(KeyValuePair_2_t1944668977 *)((KeyValuePair_2_t1944668977 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2044212869_gshared (EqualityComparer_1_t1052505858 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1052505858 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, KeyValuePair_2_t1944668977 , KeyValuePair_2_t1944668977  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Equals(T,T) */, (EqualityComparer_1_t1052505858 *)__this, (KeyValuePair_2_t1944668977 )((*(KeyValuePair_2_t1944668977 *)((KeyValuePair_2_t1944668977 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (KeyValuePair_2_t1944668977 )((*(KeyValuePair_2_t1944668977 *)((KeyValuePair_2_t1944668977 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Default()
extern "C"  EqualityComparer_1_t1052505858 * EqualityComparer_1_get_Default_m2162753866_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1052505858 * L_0 = ((EqualityComparer_1_t1052505858_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3557913674_gshared (EqualityComparer_1_t3395768310 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m2439045283_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m2439045283_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2439045283_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3395768310_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t3395768310 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2735708096 * L_8 = (DefaultComparer_t2735708096 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2735708096 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3395768310_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1981137699_gshared (EqualityComparer_1_t3395768310 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3395768310 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, KeyValuePair_2_t4287931429  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::GetHashCode(T) */, (EqualityComparer_1_t3395768310 *)__this, (KeyValuePair_2_t4287931429 )((*(KeyValuePair_2_t4287931429 *)((KeyValuePair_2_t4287931429 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2913675079_gshared (EqualityComparer_1_t3395768310 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3395768310 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, KeyValuePair_2_t4287931429 , KeyValuePair_2_t4287931429  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::Equals(T,T) */, (EqualityComparer_1_t3395768310 *)__this, (KeyValuePair_2_t4287931429 )((*(KeyValuePair_2_t4287931429 *)((KeyValuePair_2_t4287931429 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (KeyValuePair_2_t4287931429 )((*(KeyValuePair_2_t4287931429 *)((KeyValuePair_2_t4287931429 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_Default()
extern "C"  EqualityComparer_1_t3395768310 * EqualityComparer_1_get_Default_m1111871860_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3395768310 * L_0 = ((EqualityComparer_1_t3395768310_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.DateTime>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m4269347481_gshared (EqualityComparer_1_t3391498208 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.DateTime>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m3018656820_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m3018656820_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3018656820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3391498208_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t3391498208 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2731437994 * L_8 = (DefaultComparer_t2731437994 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2731437994 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3391498208_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.DateTime>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1812781938_gshared (EqualityComparer_1_t3391498208 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3391498208 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, DateTime_t4283661327  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.DateTime>::GetHashCode(T) */, (EqualityComparer_1_t3391498208 *)__this, (DateTime_t4283661327 )((*(DateTime_t4283661327 *)((DateTime_t4283661327 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.DateTime>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2743165016_gshared (EqualityComparer_1_t3391498208 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3391498208 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, DateTime_t4283661327 , DateTime_t4283661327  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.DateTime>::Equals(T,T) */, (EqualityComparer_1_t3391498208 *)__this, (DateTime_t4283661327 )((*(DateTime_t4283661327 *)((DateTime_t4283661327 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (DateTime_t4283661327 )((*(DateTime_t4283661327 *)((DateTime_t4283661327 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.DateTime>::get_Default()
extern "C"  EqualityComparer_1_t3391498208 * EqualityComparer_1_get_Default_m11220867_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3391498208 * L_0 = ((EqualityComparer_1_t3391498208_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3112804492_gshared (EqualityComparer_1_t2992551187 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t961487589_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m1525562529_MetadataUsageId;
extern "C"  void EqualityComparer_1__cctor_m1525562529_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1525562529_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(GenericEqualityComparer_1_t961487589_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3339007067*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2992551187_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((EqualityComparer_1_t2992551187 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2332490973 * L_8 = (DefaultComparer_t2332490973 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2332490973 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2992551187_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3747748197_gshared (EqualityComparer_1_t2992551187 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2992551187 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, DateTimeOffset_t3884714306  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::GetHashCode(T) */, (EqualityComparer_1_t2992551187 *)__this, (DateTimeOffset_t3884714306 )((*(DateTimeOffset_t3884714306 *)((DateTimeOffset_t3884714306 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m688611141_gshared (EqualityComparer_1_t2992551187 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2992551187 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, DateTimeOffset_t3884714306 , DateTimeOffset_t3884714306  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::Equals(T,T) */, (EqualityComparer_1_t2992551187 *)__this, (DateTimeOffset_t3884714306 )((*(DateTimeOffset_t3884714306 *)((DateTimeOffset_t3884714306 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (DateTimeOffset_t3884714306 )((*(DateTimeOffset_t3884714306 *)((DateTimeOffset_t3884714306 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::get_Default()
extern "C"  EqualityComparer_1_t2992551187 * EqualityComparer_1_get_Default_m1859222582_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2992551187 * L_0 = ((EqualityComparer_1_t2992551187_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
