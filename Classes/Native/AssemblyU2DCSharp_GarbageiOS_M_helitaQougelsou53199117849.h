﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_helitaQougelsou53
struct  M_helitaQougelsou53_t199117849  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_helitaQougelsou53::_chemjeNige
	String_t* ____chemjeNige_0;
	// System.Int32 GarbageiOS.M_helitaQougelsou53::_kajall
	int32_t ____kajall_1;
	// System.UInt32 GarbageiOS.M_helitaQougelsou53::_nouxerepooComa
	uint32_t ____nouxerepooComa_2;
	// System.Boolean GarbageiOS.M_helitaQougelsou53::_raiperbear
	bool ____raiperbear_3;
	// System.String GarbageiOS.M_helitaQougelsou53::_whissar
	String_t* ____whissar_4;
	// System.Int32 GarbageiOS.M_helitaQougelsou53::_zallsokirRawkurall
	int32_t ____zallsokirRawkurall_5;
	// System.Boolean GarbageiOS.M_helitaQougelsou53::_walmeLearear
	bool ____walmeLearear_6;
	// System.UInt32 GarbageiOS.M_helitaQougelsou53::_yalyar
	uint32_t ____yalyar_7;
	// System.Int32 GarbageiOS.M_helitaQougelsou53::_jiwa
	int32_t ____jiwa_8;
	// System.String GarbageiOS.M_helitaQougelsou53::_jemdege
	String_t* ____jemdege_9;
	// System.UInt32 GarbageiOS.M_helitaQougelsou53::_durristasFanair
	uint32_t ____durristasFanair_10;
	// System.Boolean GarbageiOS.M_helitaQougelsou53::_lelmu
	bool ____lelmu_11;
	// System.Boolean GarbageiOS.M_helitaQougelsou53::_naseelawMirgoo
	bool ____naseelawMirgoo_12;
	// System.Int32 GarbageiOS.M_helitaQougelsou53::_rurgechiFirso
	int32_t ____rurgechiFirso_13;
	// System.String GarbageiOS.M_helitaQougelsou53::_cirhe
	String_t* ____cirhe_14;
	// System.Boolean GarbageiOS.M_helitaQougelsou53::_whuxahou
	bool ____whuxahou_15;
	// System.Boolean GarbageiOS.M_helitaQougelsou53::_vibarka
	bool ____vibarka_16;
	// System.String GarbageiOS.M_helitaQougelsou53::_saiwhea
	String_t* ____saiwhea_17;

public:
	inline static int32_t get_offset_of__chemjeNige_0() { return static_cast<int32_t>(offsetof(M_helitaQougelsou53_t199117849, ____chemjeNige_0)); }
	inline String_t* get__chemjeNige_0() const { return ____chemjeNige_0; }
	inline String_t** get_address_of__chemjeNige_0() { return &____chemjeNige_0; }
	inline void set__chemjeNige_0(String_t* value)
	{
		____chemjeNige_0 = value;
		Il2CppCodeGenWriteBarrier(&____chemjeNige_0, value);
	}

	inline static int32_t get_offset_of__kajall_1() { return static_cast<int32_t>(offsetof(M_helitaQougelsou53_t199117849, ____kajall_1)); }
	inline int32_t get__kajall_1() const { return ____kajall_1; }
	inline int32_t* get_address_of__kajall_1() { return &____kajall_1; }
	inline void set__kajall_1(int32_t value)
	{
		____kajall_1 = value;
	}

	inline static int32_t get_offset_of__nouxerepooComa_2() { return static_cast<int32_t>(offsetof(M_helitaQougelsou53_t199117849, ____nouxerepooComa_2)); }
	inline uint32_t get__nouxerepooComa_2() const { return ____nouxerepooComa_2; }
	inline uint32_t* get_address_of__nouxerepooComa_2() { return &____nouxerepooComa_2; }
	inline void set__nouxerepooComa_2(uint32_t value)
	{
		____nouxerepooComa_2 = value;
	}

	inline static int32_t get_offset_of__raiperbear_3() { return static_cast<int32_t>(offsetof(M_helitaQougelsou53_t199117849, ____raiperbear_3)); }
	inline bool get__raiperbear_3() const { return ____raiperbear_3; }
	inline bool* get_address_of__raiperbear_3() { return &____raiperbear_3; }
	inline void set__raiperbear_3(bool value)
	{
		____raiperbear_3 = value;
	}

	inline static int32_t get_offset_of__whissar_4() { return static_cast<int32_t>(offsetof(M_helitaQougelsou53_t199117849, ____whissar_4)); }
	inline String_t* get__whissar_4() const { return ____whissar_4; }
	inline String_t** get_address_of__whissar_4() { return &____whissar_4; }
	inline void set__whissar_4(String_t* value)
	{
		____whissar_4 = value;
		Il2CppCodeGenWriteBarrier(&____whissar_4, value);
	}

	inline static int32_t get_offset_of__zallsokirRawkurall_5() { return static_cast<int32_t>(offsetof(M_helitaQougelsou53_t199117849, ____zallsokirRawkurall_5)); }
	inline int32_t get__zallsokirRawkurall_5() const { return ____zallsokirRawkurall_5; }
	inline int32_t* get_address_of__zallsokirRawkurall_5() { return &____zallsokirRawkurall_5; }
	inline void set__zallsokirRawkurall_5(int32_t value)
	{
		____zallsokirRawkurall_5 = value;
	}

	inline static int32_t get_offset_of__walmeLearear_6() { return static_cast<int32_t>(offsetof(M_helitaQougelsou53_t199117849, ____walmeLearear_6)); }
	inline bool get__walmeLearear_6() const { return ____walmeLearear_6; }
	inline bool* get_address_of__walmeLearear_6() { return &____walmeLearear_6; }
	inline void set__walmeLearear_6(bool value)
	{
		____walmeLearear_6 = value;
	}

	inline static int32_t get_offset_of__yalyar_7() { return static_cast<int32_t>(offsetof(M_helitaQougelsou53_t199117849, ____yalyar_7)); }
	inline uint32_t get__yalyar_7() const { return ____yalyar_7; }
	inline uint32_t* get_address_of__yalyar_7() { return &____yalyar_7; }
	inline void set__yalyar_7(uint32_t value)
	{
		____yalyar_7 = value;
	}

	inline static int32_t get_offset_of__jiwa_8() { return static_cast<int32_t>(offsetof(M_helitaQougelsou53_t199117849, ____jiwa_8)); }
	inline int32_t get__jiwa_8() const { return ____jiwa_8; }
	inline int32_t* get_address_of__jiwa_8() { return &____jiwa_8; }
	inline void set__jiwa_8(int32_t value)
	{
		____jiwa_8 = value;
	}

	inline static int32_t get_offset_of__jemdege_9() { return static_cast<int32_t>(offsetof(M_helitaQougelsou53_t199117849, ____jemdege_9)); }
	inline String_t* get__jemdege_9() const { return ____jemdege_9; }
	inline String_t** get_address_of__jemdege_9() { return &____jemdege_9; }
	inline void set__jemdege_9(String_t* value)
	{
		____jemdege_9 = value;
		Il2CppCodeGenWriteBarrier(&____jemdege_9, value);
	}

	inline static int32_t get_offset_of__durristasFanair_10() { return static_cast<int32_t>(offsetof(M_helitaQougelsou53_t199117849, ____durristasFanair_10)); }
	inline uint32_t get__durristasFanair_10() const { return ____durristasFanair_10; }
	inline uint32_t* get_address_of__durristasFanair_10() { return &____durristasFanair_10; }
	inline void set__durristasFanair_10(uint32_t value)
	{
		____durristasFanair_10 = value;
	}

	inline static int32_t get_offset_of__lelmu_11() { return static_cast<int32_t>(offsetof(M_helitaQougelsou53_t199117849, ____lelmu_11)); }
	inline bool get__lelmu_11() const { return ____lelmu_11; }
	inline bool* get_address_of__lelmu_11() { return &____lelmu_11; }
	inline void set__lelmu_11(bool value)
	{
		____lelmu_11 = value;
	}

	inline static int32_t get_offset_of__naseelawMirgoo_12() { return static_cast<int32_t>(offsetof(M_helitaQougelsou53_t199117849, ____naseelawMirgoo_12)); }
	inline bool get__naseelawMirgoo_12() const { return ____naseelawMirgoo_12; }
	inline bool* get_address_of__naseelawMirgoo_12() { return &____naseelawMirgoo_12; }
	inline void set__naseelawMirgoo_12(bool value)
	{
		____naseelawMirgoo_12 = value;
	}

	inline static int32_t get_offset_of__rurgechiFirso_13() { return static_cast<int32_t>(offsetof(M_helitaQougelsou53_t199117849, ____rurgechiFirso_13)); }
	inline int32_t get__rurgechiFirso_13() const { return ____rurgechiFirso_13; }
	inline int32_t* get_address_of__rurgechiFirso_13() { return &____rurgechiFirso_13; }
	inline void set__rurgechiFirso_13(int32_t value)
	{
		____rurgechiFirso_13 = value;
	}

	inline static int32_t get_offset_of__cirhe_14() { return static_cast<int32_t>(offsetof(M_helitaQougelsou53_t199117849, ____cirhe_14)); }
	inline String_t* get__cirhe_14() const { return ____cirhe_14; }
	inline String_t** get_address_of__cirhe_14() { return &____cirhe_14; }
	inline void set__cirhe_14(String_t* value)
	{
		____cirhe_14 = value;
		Il2CppCodeGenWriteBarrier(&____cirhe_14, value);
	}

	inline static int32_t get_offset_of__whuxahou_15() { return static_cast<int32_t>(offsetof(M_helitaQougelsou53_t199117849, ____whuxahou_15)); }
	inline bool get__whuxahou_15() const { return ____whuxahou_15; }
	inline bool* get_address_of__whuxahou_15() { return &____whuxahou_15; }
	inline void set__whuxahou_15(bool value)
	{
		____whuxahou_15 = value;
	}

	inline static int32_t get_offset_of__vibarka_16() { return static_cast<int32_t>(offsetof(M_helitaQougelsou53_t199117849, ____vibarka_16)); }
	inline bool get__vibarka_16() const { return ____vibarka_16; }
	inline bool* get_address_of__vibarka_16() { return &____vibarka_16; }
	inline void set__vibarka_16(bool value)
	{
		____vibarka_16 = value;
	}

	inline static int32_t get_offset_of__saiwhea_17() { return static_cast<int32_t>(offsetof(M_helitaQougelsou53_t199117849, ____saiwhea_17)); }
	inline String_t* get__saiwhea_17() const { return ____saiwhea_17; }
	inline String_t** get_address_of__saiwhea_17() { return &____saiwhea_17; }
	inline void set__saiwhea_17(String_t* value)
	{
		____saiwhea_17 = value;
		Il2CppCodeGenWriteBarrier(&____saiwhea_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
