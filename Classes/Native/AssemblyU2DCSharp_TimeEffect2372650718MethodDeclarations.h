﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TimeEffect
struct TimeEffect_t2372650718;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// EffectCtrl
struct EffectCtrl_t3708787644;
// IEffComponent
struct IEffComponent_t3802264961;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_IEffComponent3802264961.h"

// System.Void TimeEffect::.ctor()
extern "C"  void TimeEffect__ctor_m2616109949 (TimeEffect_t2372650718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeEffect::set_lasttime(System.Single)
extern "C"  void TimeEffect_set_lasttime_m1414682468 (TimeEffect_t2372650718 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TimeEffect::get_lasttime()
extern "C"  float TimeEffect_get_lasttime_m112907263 (TimeEffect_t2372650718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeEffect::Init(UnityEngine.GameObject)
extern "C"  void TimeEffect_Init_m2973932399 (TimeEffect_t2372650718 * __this, GameObject_t3674682005 * ___effGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeEffect::Update()
extern "C"  void TimeEffect_Update_m1221891888 (TimeEffect_t2372650718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeEffect::Play()
extern "C"  void TimeEffect_Play_m3005197403 (TimeEffect_t2372650718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeEffect::Stop()
extern "C"  void TimeEffect_Stop_m3098881449 (TimeEffect_t2372650718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeEffect::Reset()
extern "C"  void TimeEffect_Reset_m262542890 (TimeEffect_t2372650718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeEffect::Clear()
extern "C"  void TimeEffect_Clear_m22243240 (TimeEffect_t2372650718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EffectCtrl TimeEffect::ilo_get_effCtrl1(IEffComponent)
extern "C"  EffectCtrl_t3708787644 * TimeEffect_ilo_get_effCtrl1_m2338687084 (Il2CppObject * __this /* static, unused */, IEffComponent_t3802264961 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
