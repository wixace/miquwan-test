﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Object>
struct List_1_t1244034627;
// Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14B`1<System.Object>
struct U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_t1468122155;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14C`1<System.Object>
struct  U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_t1023097156  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<TSource> Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14C`1::caseSensitiveResults
	List_1_t1244034627 * ___caseSensitiveResults_0;
	// Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14B`1<TSource> Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14C`1::<>f__ref$331
	U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_t1468122155 * ___U3CU3Ef__refU24331_1;

public:
	inline static int32_t get_offset_of_caseSensitiveResults_0() { return static_cast<int32_t>(offsetof(U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_t1023097156, ___caseSensitiveResults_0)); }
	inline List_1_t1244034627 * get_caseSensitiveResults_0() const { return ___caseSensitiveResults_0; }
	inline List_1_t1244034627 ** get_address_of_caseSensitiveResults_0() { return &___caseSensitiveResults_0; }
	inline void set_caseSensitiveResults_0(List_1_t1244034627 * value)
	{
		___caseSensitiveResults_0 = value;
		Il2CppCodeGenWriteBarrier(&___caseSensitiveResults_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24331_1() { return static_cast<int32_t>(offsetof(U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_t1023097156, ___U3CU3Ef__refU24331_1)); }
	inline U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_t1468122155 * get_U3CU3Ef__refU24331_1() const { return ___U3CU3Ef__refU24331_1; }
	inline U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_t1468122155 ** get_address_of_U3CU3Ef__refU24331_1() { return &___U3CU3Ef__refU24331_1; }
	inline void set_U3CU3Ef__refU24331_1(U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_t1468122155 * value)
	{
		___U3CU3Ef__refU24331_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24331_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
