﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.GraphUpdateObject
struct GraphUpdateObject_t430843704;
// System.Object
struct Il2CppObject;
// AstarPath
struct AstarPath_t4090270936;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AstarPath/<UpdateGraphsInteral>c__IteratorC
struct  U3CUpdateGraphsInteralU3Ec__IteratorC_t880590588  : public Il2CppObject
{
public:
	// System.Single AstarPath/<UpdateGraphsInteral>c__IteratorC::t
	float ___t_0;
	// Pathfinding.GraphUpdateObject AstarPath/<UpdateGraphsInteral>c__IteratorC::ob
	GraphUpdateObject_t430843704 * ___ob_1;
	// System.Int32 AstarPath/<UpdateGraphsInteral>c__IteratorC::$PC
	int32_t ___U24PC_2;
	// System.Object AstarPath/<UpdateGraphsInteral>c__IteratorC::$current
	Il2CppObject * ___U24current_3;
	// System.Single AstarPath/<UpdateGraphsInteral>c__IteratorC::<$>t
	float ___U3CU24U3Et_4;
	// Pathfinding.GraphUpdateObject AstarPath/<UpdateGraphsInteral>c__IteratorC::<$>ob
	GraphUpdateObject_t430843704 * ___U3CU24U3Eob_5;
	// AstarPath AstarPath/<UpdateGraphsInteral>c__IteratorC::<>f__this
	AstarPath_t4090270936 * ___U3CU3Ef__this_6;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(U3CUpdateGraphsInteralU3Ec__IteratorC_t880590588, ___t_0)); }
	inline float get_t_0() const { return ___t_0; }
	inline float* get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(float value)
	{
		___t_0 = value;
	}

	inline static int32_t get_offset_of_ob_1() { return static_cast<int32_t>(offsetof(U3CUpdateGraphsInteralU3Ec__IteratorC_t880590588, ___ob_1)); }
	inline GraphUpdateObject_t430843704 * get_ob_1() const { return ___ob_1; }
	inline GraphUpdateObject_t430843704 ** get_address_of_ob_1() { return &___ob_1; }
	inline void set_ob_1(GraphUpdateObject_t430843704 * value)
	{
		___ob_1 = value;
		Il2CppCodeGenWriteBarrier(&___ob_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CUpdateGraphsInteralU3Ec__IteratorC_t880590588, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CUpdateGraphsInteralU3Ec__IteratorC_t880590588, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Et_4() { return static_cast<int32_t>(offsetof(U3CUpdateGraphsInteralU3Ec__IteratorC_t880590588, ___U3CU24U3Et_4)); }
	inline float get_U3CU24U3Et_4() const { return ___U3CU24U3Et_4; }
	inline float* get_address_of_U3CU24U3Et_4() { return &___U3CU24U3Et_4; }
	inline void set_U3CU24U3Et_4(float value)
	{
		___U3CU24U3Et_4 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Eob_5() { return static_cast<int32_t>(offsetof(U3CUpdateGraphsInteralU3Ec__IteratorC_t880590588, ___U3CU24U3Eob_5)); }
	inline GraphUpdateObject_t430843704 * get_U3CU24U3Eob_5() const { return ___U3CU24U3Eob_5; }
	inline GraphUpdateObject_t430843704 ** get_address_of_U3CU24U3Eob_5() { return &___U3CU24U3Eob_5; }
	inline void set_U3CU24U3Eob_5(GraphUpdateObject_t430843704 * value)
	{
		___U3CU24U3Eob_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eob_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_6() { return static_cast<int32_t>(offsetof(U3CUpdateGraphsInteralU3Ec__IteratorC_t880590588, ___U3CU3Ef__this_6)); }
	inline AstarPath_t4090270936 * get_U3CU3Ef__this_6() const { return ___U3CU3Ef__this_6; }
	inline AstarPath_t4090270936 ** get_address_of_U3CU3Ef__this_6() { return &___U3CU3Ef__this_6; }
	inline void set_U3CU3Ef__this_6(AstarPath_t4090270936 * value)
	{
		___U3CU3Ef__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
