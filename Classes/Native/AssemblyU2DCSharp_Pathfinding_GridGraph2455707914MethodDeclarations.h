﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.GridGraph
struct GridGraph_t2455707914;
// Pathfinding.GraphNodeDelegateCancelable
struct GraphNodeDelegateCancelable_t3591372971;
// Pathfinding.GridNode
struct GridNode_t3795753694;
// Pathfinding.NNConstraint
struct NNConstraint_t758567699;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// OnScanStatus
struct OnScanStatus_t2412749870;
// Pathfinding.GridNode[]
struct GridNodeU5BU5D_t2925197291;
// AstarPath
struct AstarPath_t4090270936;
// System.Collections.Generic.List`1<Pathfinding.GraphNode>
struct List_1_t1391797922;
// Pathfinding.GraphUpdateShape
struct GraphUpdateShape_t2348620960;
// Pathfinding.GraphUpdateObject
struct GraphUpdateObject_t430843704;
// Pathfinding.Serialization.GraphSerializationContext
struct GraphSerializationContext_t3256954663;
// Pathfinding.NavGraph
struct NavGraph_t1254319713;
// Pathfinding.AstarData
struct AstarData_t3283402719;
// Pathfinding.GraphCollision
struct GraphCollision_t2160440954;
// Pathfinding.GridGraph/TextureData
struct TextureData_t1394081396;
// Pathfinding.Path
struct Path_t1974241691;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNodeDelegateCan3591372971.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "AssemblyU2DCSharp_Pathfinding_GridNode3795753694.h"
#include "AssemblyU2DCSharp_Pathfinding_NNInfo1570625892.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_NNConstraint758567699.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_OnScanStatus2412749870.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphUpdateShape2348620960.h"
#include "AssemblyU2DCSharp_GraphUpdateThreading3958092737.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphUpdateObject430843704.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphHitInfo3082627369.h"
#include "AssemblyU2DCSharp_Pathfinding_Serialization_GraphS3256954663.h"
#include "AssemblyU2DCSharp_Pathfinding_NavGraph1254319713.h"
#include "AssemblyU2DCSharp_Pathfinding_GridGraph2455707914.h"
#include "AssemblyU2DCSharp_Pathfinding_AstarData3283402719.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphCollision2160440954.h"
#include "AssemblyU2DCSharp_Pathfinding_GridGraph_TextureDat1394081396.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_Pathfinding_IntRect3015058261.h"

// System.Void Pathfinding.GridGraph::.ctor()
extern "C"  void GridGraph__ctor_m3015433069 (GridGraph_t2455707914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::OnDestroy()
extern "C"  void GridGraph_OnDestroy_m36188646 (GridGraph_t2455707914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::RemoveGridGraphFromStatic()
extern "C"  void GridGraph_RemoveGridGraphFromStatic_m2348040167 (GridGraph_t2455707914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridGraph::get_uniformWidthDepthGrid()
extern "C"  bool GridGraph_get_uniformWidthDepthGrid_m3918765581 (GridGraph_t2455707914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::GetNodes(Pathfinding.GraphNodeDelegateCancelable)
extern "C"  void GridGraph_GetNodes_m1325882689 (GridGraph_t2455707914 * __this, GraphNodeDelegateCancelable_t3591372971 * ___del0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridGraph::get_useRaycastNormal()
extern "C"  bool GridGraph_get_useRaycastNormal_m1764918677 (GridGraph_t2455707914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.GridGraph::GetNodePosition(System.Int32,System.Int32)
extern "C"  Int3_t1974045594  GridGraph_GetNodePosition_m1912092619 (GridGraph_t2455707914 * __this, int32_t ___index0, int32_t ___yOffset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.GridGraph::get_Width()
extern "C"  int32_t GridGraph_get_Width_m3933195894 (GridGraph_t2455707914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::set_Width(System.Int32)
extern "C"  void GridGraph_set_Width_m871036997 (GridGraph_t2455707914 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.GridGraph::get_Depth()
extern "C"  int32_t GridGraph_get_Depth_m4147060787 (GridGraph_t2455707914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::set_Depth(System.Int32)
extern "C"  void GridGraph_set_Depth_m2784269954 (GridGraph_t2455707914 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.GridGraph::GetConnectionCost(System.Int32)
extern "C"  uint32_t GridGraph_GetConnectionCost_m1679782712 (GridGraph_t2455707914 * __this, int32_t ___dir0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.GridNode Pathfinding.GridGraph::GetNodeConnection(Pathfinding.GridNode,System.Int32)
extern "C"  GridNode_t3795753694 * GridGraph_GetNodeConnection_m1039087165 (GridGraph_t2455707914 * __this, GridNode_t3795753694 * ___node0, int32_t ___dir1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridGraph::HasNodeConnection(Pathfinding.GridNode,System.Int32)
extern "C"  bool GridGraph_HasNodeConnection_m4047451920 (GridGraph_t2455707914 * __this, GridNode_t3795753694 * ___node0, int32_t ___dir1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::SetNodeConnection(Pathfinding.GridNode,System.Int32,System.Boolean)
extern "C"  void GridGraph_SetNodeConnection_m842755713 (GridGraph_t2455707914 * __this, GridNode_t3795753694 * ___node0, int32_t ___dir1, bool ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.GridNode Pathfinding.GridGraph::GetNodeConnection(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  GridNode_t3795753694 * GridGraph_GetNodeConnection_m3029797338 (GridGraph_t2455707914 * __this, int32_t ___index0, int32_t ___x1, int32_t ___z2, int32_t ___dir3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::SetNodeConnection(System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean)
extern "C"  void GridGraph_SetNodeConnection_m3659511556 (GridGraph_t2455707914 * __this, int32_t ___index0, int32_t ___x1, int32_t ___z2, int32_t ___dir3, bool ___value4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridGraph::HasNodeConnection(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  bool GridGraph_HasNodeConnection_m3075841005 (GridGraph_t2455707914 * __this, int32_t ___index0, int32_t ___x1, int32_t ___z2, int32_t ___dir3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::UpdateSizeFromWidthDepth()
extern "C"  void GridGraph_UpdateSizeFromWidthDepth_m2628750312 (GridGraph_t2455707914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::GenerateMatrix()
extern "C"  void GridGraph_GenerateMatrix_m2451899597 (GridGraph_t2455707914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.GridGraph::GetNearest(UnityEngine.Vector3,Pathfinding.NNConstraint,Pathfinding.GraphNode)
extern "C"  NNInfo_t1570625892  GridGraph_GetNearest_m2545602662 (GridGraph_t2455707914 * __this, Vector3_t4282066566  ___position0, NNConstraint_t758567699 * ___constraint1, GraphNode_t23612370 * ___hint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.GridGraph::GetNearestForce(UnityEngine.Vector3,Pathfinding.NNConstraint)
extern "C"  NNInfo_t1570625892  GridGraph_GetNearestForce_m4237975993 (GridGraph_t2455707914 * __this, Vector3_t4282066566  ___position0, NNConstraint_t758567699 * ___constraint1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::SetUpOffsetsAndCosts()
extern "C"  void GridGraph_SetUpOffsetsAndCosts_m2746823785 (GridGraph_t2455707914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::ScanInternal(OnScanStatus)
extern "C"  void GridGraph_ScanInternal_m2339067299 (GridGraph_t2455707914 * __this, OnScanStatus_t2412749870 * ___statusCallback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::UpdateNodePositionCollision(Pathfinding.GridNode,System.Int32,System.Int32,System.Boolean)
extern "C"  void GridGraph_UpdateNodePositionCollision_m1782412518 (GridGraph_t2455707914 * __this, GridNode_t3795753694 * ___node0, int32_t ___x1, int32_t ___z2, bool ___resetPenalty3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::ErodeWalkableArea()
extern "C"  void GridGraph_ErodeWalkableArea_m1507543966 (GridGraph_t2455707914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::ErodeWalkableArea(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void GridGraph_ErodeWalkableArea_m3326541992 (GridGraph_t2455707914 * __this, int32_t ___xmin0, int32_t ___zmin1, int32_t ___xmax2, int32_t ___zmax3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridGraph::IsValidConnection(Pathfinding.GridNode,Pathfinding.GridNode)
extern "C"  bool GridGraph_IsValidConnection_m1495545463 (GridGraph_t2455707914 * __this, GridNode_t3795753694 * ___n10, GridNode_t3795753694 * ___n21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::CalculateConnections(Pathfinding.GridNode)
extern "C"  void GridGraph_CalculateConnections_m1568126644 (Il2CppObject * __this /* static, unused */, GridNode_t3795753694 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::CalculateConnections(Pathfinding.GridNode[],System.Int32,System.Int32,Pathfinding.GridNode)
extern "C"  void GridGraph_CalculateConnections_m3706414146 (GridGraph_t2455707914 * __this, GridNodeU5BU5D_t2925197291* ___nodes0, int32_t ___x1, int32_t ___z2, GridNode_t3795753694 * ___node3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::OnPostScan(AstarPath)
extern "C"  void GridGraph_OnPostScan_m2226885319 (GridGraph_t2455707914 * __this, AstarPath_t4090270936 * ___script0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::OnDrawGizmos(System.Boolean)
extern "C"  void GridGraph_OnDrawGizmos_m187156810 (GridGraph_t2455707914 * __this, bool ___drawNodes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::GetBoundsMinMax(UnityEngine.Bounds,UnityEngine.Matrix4x4,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void GridGraph_GetBoundsMinMax_m2331453808 (GridGraph_t2455707914 * __this, Bounds_t2711641849  ___b0, Matrix4x4_t1651859333  ___matrix1, Vector3_t4282066566 * ___min2, Vector3_t4282066566 * ___max3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Pathfinding.GraphNode> Pathfinding.GridGraph::GetNodesInArea(UnityEngine.Bounds)
extern "C"  List_1_t1391797922 * GridGraph_GetNodesInArea_m275972689 (GridGraph_t2455707914 * __this, Bounds_t2711641849  ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Pathfinding.GraphNode> Pathfinding.GridGraph::GetNodesInArea(Pathfinding.GraphUpdateShape)
extern "C"  List_1_t1391797922 * GridGraph_GetNodesInArea_m1984301481 (GridGraph_t2455707914 * __this, GraphUpdateShape_t2348620960 * ___shape0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Pathfinding.GraphNode> Pathfinding.GridGraph::GetNodesInArea(UnityEngine.Bounds,Pathfinding.GraphUpdateShape)
extern "C"  List_1_t1391797922 * GridGraph_GetNodesInArea_m446564737 (GridGraph_t2455707914 * __this, Bounds_t2711641849  ___b0, GraphUpdateShape_t2348620960 * ___shape1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GraphUpdateThreading Pathfinding.GridGraph::CanUpdateAsync(Pathfinding.GraphUpdateObject)
extern "C"  int32_t GridGraph_CanUpdateAsync_m493328360 (GridGraph_t2455707914 * __this, GraphUpdateObject_t430843704 * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::UpdateAreaInit(Pathfinding.GraphUpdateObject)
extern "C"  void GridGraph_UpdateAreaInit_m3766849033 (GridGraph_t2455707914 * __this, GraphUpdateObject_t430843704 * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::UpdateArea(Pathfinding.GraphUpdateObject)
extern "C"  void GridGraph_UpdateArea_m367286073 (GridGraph_t2455707914 * __this, GraphUpdateObject_t430843704 * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridGraph::Linecast(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool GridGraph_Linecast_m2066720950 (GridGraph_t2455707914 * __this, Vector3_t4282066566  ____a0, Vector3_t4282066566  ____b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridGraph::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode)
extern "C"  bool GridGraph_Linecast_m1082497628 (GridGraph_t2455707914 * __this, Vector3_t4282066566  ____a0, Vector3_t4282066566  ____b1, GraphNode_t23612370 * ___hint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridGraph::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode,Pathfinding.GraphHitInfo&)
extern "C"  bool GridGraph_Linecast_m3210432255 (GridGraph_t2455707914 * __this, Vector3_t4282066566  ____a0, Vector3_t4282066566  ____b1, GraphNode_t23612370 * ___hint2, GraphHitInfo_t3082627369 * ___hit3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridGraph::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode,Pathfinding.GraphHitInfo&,System.Collections.Generic.List`1<Pathfinding.GraphNode>)
extern "C"  bool GridGraph_Linecast_m2267458545 (GridGraph_t2455707914 * __this, Vector3_t4282066566  ____a0, Vector3_t4282066566  ____b1, GraphNode_t23612370 * ___hint2, GraphHitInfo_t3082627369 * ___hit3, List_1_t1391797922 * ___trace4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridGraph::SnappedLinecast(UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode,Pathfinding.GraphHitInfo&)
extern "C"  bool GridGraph_SnappedLinecast_m2394732862 (GridGraph_t2455707914 * __this, Vector3_t4282066566  ____a0, Vector3_t4282066566  ____b1, GraphNode_t23612370 * ___hint2, GraphHitInfo_t3082627369 * ___hit3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridGraph::CheckConnection(Pathfinding.GridNode,System.Int32)
extern "C"  bool GridGraph_CheckConnection_m1451609540 (GridGraph_t2455707914 * __this, GridNode_t3795753694 * ___node0, int32_t ___dir1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::SerializeExtraInfo(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void GridGraph_SerializeExtraInfo_m1689161816 (GridGraph_t2455707914 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::DeserializeExtraInfo(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void GridGraph_DeserializeExtraInfo_m1224830519 (GridGraph_t2455707914 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::PostDeserialization()
extern "C"  void GridGraph_PostDeserialization_m4017107866 (GridGraph_t2455707914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::ilo_OnDestroy1(Pathfinding.NavGraph)
extern "C"  void GridGraph_ilo_OnDestroy1_m2500412267 (Il2CppObject * __this /* static, unused */, NavGraph_t1254319713 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::ilo_RemoveGridGraphFromStatic2(Pathfinding.GridGraph)
extern "C"  void GridGraph_ilo_RemoveGridGraphFromStatic2_m3683982708 (Il2CppObject * __this /* static, unused */, GridGraph_t2455707914 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridGraph::ilo_Invoke3(Pathfinding.GraphNodeDelegateCancelable,Pathfinding.GraphNode)
extern "C"  bool GridGraph_ilo_Invoke3_m1788984146 (Il2CppObject * __this /* static, unused */, GraphNodeDelegateCancelable_t3591372971 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridGraph::ilo_GetConnectionInternal4(Pathfinding.GridNode,System.Int32)
extern "C"  bool GridGraph_ilo_GetConnectionInternal4_m3252251120 (Il2CppObject * __this /* static, unused */, GridNode_t3795753694 * ____this0, int32_t ___dir1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridGraph::ilo_get_EdgeNode5(Pathfinding.GridNode)
extern "C"  bool GridGraph_ilo_get_EdgeNode5_m1761446695 (Il2CppObject * __this /* static, unused */, GridNode_t3795753694 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.GridGraph::ilo_get_Width6(Pathfinding.GridGraph)
extern "C"  int32_t GridGraph_ilo_get_Width6_m3508815167 (Il2CppObject * __this /* static, unused */, GridGraph_t2455707914 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.GridGraph::ilo_get_NodeInGridIndex7(Pathfinding.GridNode)
extern "C"  int32_t GridGraph_ilo_get_NodeInGridIndex7_m4141995589 (Il2CppObject * __this /* static, unused */, GridNode_t3795753694 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.GridGraph::ilo_get_Depth8(Pathfinding.GridGraph)
extern "C"  int32_t GridGraph_ilo_get_Depth8_m2764183802 (Il2CppObject * __this /* static, unused */, GridGraph_t2455707914 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridGraph::ilo_Suitable9(Pathfinding.NNConstraint,Pathfinding.GraphNode)
extern "C"  bool GridGraph_ilo_Suitable9_m1974396893 (Il2CppObject * __this /* static, unused */, NNConstraint_t758567699 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.GridGraph::ilo_op_Explicit10(Pathfinding.Int3)
extern "C"  Vector3_t4282066566  GridGraph_ilo_op_Explicit10_m3392106453 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.GridGraph::ilo_GetGraphIndex11(Pathfinding.AstarData,Pathfinding.NavGraph)
extern "C"  int32_t GridGraph_ilo_GetGraphIndex11_m2773519858 (Il2CppObject * __this /* static, unused */, AstarData_t3283402719 * ____this0, NavGraph_t1254319713 * ___graph1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::ilo_set_GraphIndex12(Pathfinding.GraphNode,System.UInt32)
extern "C"  void GridGraph_ilo_set_GraphIndex12_m1568294772 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::ilo_Initialize13(Pathfinding.GraphCollision,UnityEngine.Matrix4x4,System.Single)
extern "C"  void GridGraph_ilo_Initialize13_m472390855 (Il2CppObject * __this /* static, unused */, GraphCollision_t2160440954 * ____this0, Matrix4x4_t1651859333  ___matrix1, float ___scale2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::ilo_Initialize14(Pathfinding.GridGraph/TextureData)
extern "C"  void GridGraph_ilo_Initialize14_m2432928193 (Il2CppObject * __this /* static, unused */, TextureData_t1394081396 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.GridGraph::ilo_GetNodePosition15(Pathfinding.GridGraph,System.Int32,System.Int32)
extern "C"  Int3_t1974045594  GridGraph_ilo_GetNodePosition15_m3988872904 (Il2CppObject * __this /* static, unused */, GridGraph_t2455707914 * ____this0, int32_t ___index1, int32_t ___yOffset2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.GridGraph::ilo_CheckHeight16(Pathfinding.GraphCollision,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Boolean&)
extern "C"  Vector3_t4282066566  GridGraph_ilo_CheckHeight16_m1546161377 (Il2CppObject * __this /* static, unused */, GraphCollision_t2160440954 * ____this0, Vector3_t4282066566  ___position1, RaycastHit_t4003175726 * ___hit2, bool* ___walkable3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridGraph::ilo_get_useRaycastNormal17(Pathfinding.GridGraph)
extern "C"  bool GridGraph_ilo_get_useRaycastNormal17_m1380808710 (Il2CppObject * __this /* static, unused */, GridGraph_t2455707914 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::ilo_set_Penalty18(Pathfinding.GraphNode,System.UInt32)
extern "C"  void GridGraph_ilo_set_Penalty18_m2395772213 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridGraph::ilo_Check19(Pathfinding.GraphCollision,UnityEngine.Vector3)
extern "C"  bool GridGraph_ilo_Check19_m2951680897 (Il2CppObject * __this /* static, unused */, GraphCollision_t2160440954 * ____this0, Vector3_t4282066566  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::ilo_set_Walkable20(Pathfinding.GraphNode,System.Boolean)
extern "C"  void GridGraph_ilo_set_Walkable20_m2928941465 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::ilo_ErodeWalkableArea21(Pathfinding.GridGraph,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void GridGraph_ilo_ErodeWalkableArea21_m233412394 (Il2CppObject * __this /* static, unused */, GridGraph_t2455707914 * ____this0, int32_t ___xmin1, int32_t ___zmin2, int32_t ___xmax3, int32_t ___zmax4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridGraph::ilo_get_Walkable22(Pathfinding.GraphNode)
extern "C"  bool GridGraph_ilo_get_Walkable22_m536594378 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.GridGraph::ilo_get_Tag23(Pathfinding.GraphNode)
extern "C"  uint32_t GridGraph_ilo_get_Tag23_m3557592327 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::ilo_set_Tag24(Pathfinding.GraphNode,System.UInt32)
extern "C"  void GridGraph_ilo_set_Tag24_m980792521 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridGraph::ilo_HasNodeConnection25(Pathfinding.GridGraph,Pathfinding.GridNode,System.Int32)
extern "C"  bool GridGraph_ilo_HasNodeConnection25_m1202905486 (Il2CppObject * __this /* static, unused */, GridGraph_t2455707914 * ____this0, GridNode_t3795753694 * ___node1, int32_t ___dir2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::ilo_CalculateConnections26(Pathfinding.GridGraph,Pathfinding.GridNode[],System.Int32,System.Int32,Pathfinding.GridNode)
extern "C"  void GridGraph_ilo_CalculateConnections26_m396129561 (Il2CppObject * __this /* static, unused */, GridGraph_t2455707914 * ____this0, GridNodeU5BU5D_t2925197291* ___nodes1, int32_t ___x2, int32_t ___z3, GridNode_t3795753694 * ___node4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::ilo_ResetConnectionsInternal27(Pathfinding.GridNode)
extern "C"  void GridGraph_ilo_ResetConnectionsInternal27_m2964305792 (Il2CppObject * __this /* static, unused */, GridNode_t3795753694 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridGraph::ilo_IsValidConnection28(Pathfinding.GridGraph,Pathfinding.GridNode,Pathfinding.GridNode)
extern "C"  bool GridGraph_ilo_IsValidConnection28_m1581555026 (Il2CppObject * __this /* static, unused */, GridGraph_t2455707914 * ____this0, GridNode_t3795753694 * ___n11, GridNode_t3795753694 * ___n22, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridGraph::ilo_InSearchTree29(Pathfinding.GraphNode,Pathfinding.Path)
extern "C"  bool GridGraph_ilo_InSearchTree29_m1756685011 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ___node0, Path_t1974241691 * ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Pathfinding.GraphNode> Pathfinding.GridGraph::ilo_GetNodesInArea30(Pathfinding.GridGraph,UnityEngine.Bounds,Pathfinding.GraphUpdateShape)
extern "C"  List_1_t1391797922 * GridGraph_ilo_GetNodesInArea30_m905215647 (Il2CppObject * __this /* static, unused */, GridGraph_t2455707914 * ____this0, Bounds_t2711641849  ___b1, GraphUpdateShape_t2348620960 * ___shape2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.IntRect Pathfinding.GridGraph::ilo_Intersection31(Pathfinding.IntRect,Pathfinding.IntRect)
extern "C"  IntRect_t3015058261  GridGraph_ilo_Intersection31_m3203045509 (Il2CppObject * __this /* static, unused */, IntRect_t3015058261  ___a0, IntRect_t3015058261  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::ilo_UpdateNodePositionCollision32(Pathfinding.GridGraph,Pathfinding.GridNode,System.Int32,System.Int32,System.Boolean)
extern "C"  void GridGraph_ilo_UpdateNodePositionCollision32_m3522662920 (Il2CppObject * __this /* static, unused */, GridGraph_t2455707914 * ____this0, GridNode_t3795753694 * ___node1, int32_t ___x2, int32_t ___z3, bool ___resetPenalty4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::ilo_Apply33(Pathfinding.GraphUpdateObject,Pathfinding.GraphNode)
extern "C"  void GridGraph_ilo_Apply33_m1190024230 (Il2CppObject * __this /* static, unused */, GraphUpdateObject_t430843704 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridGraph::ilo_Contains34(Pathfinding.IntRect&,System.Int32,System.Int32)
extern "C"  bool GridGraph_ilo_Contains34_m1023986169 (Il2CppObject * __this /* static, unused */, IntRect_t3015058261 * ____this0, int32_t ___x1, int32_t ___y2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridGraph::ilo_Linecast35(Pathfinding.GridGraph,UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode,Pathfinding.GraphHitInfo&,System.Collections.Generic.List`1<Pathfinding.GraphNode>)
extern "C"  bool GridGraph_ilo_Linecast35_m376135512 (Il2CppObject * __this /* static, unused */, GridGraph_t2455707914 * ____this0, Vector3_t4282066566  ____a1, Vector3_t4282066566  ____b2, GraphNode_t23612370 * ___hint3, GraphHitInfo_t3082627369 * ___hit4, List_1_t1391797922 * ___trace5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.GridGraph::ilo_SegmentIntersectionPoint36(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Boolean&)
extern "C"  Vector3_t4282066566  GridGraph_ilo_SegmentIntersectionPoint36_m3749121722 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start10, Vector3_t4282066566  ___end11, Vector3_t4282066566  ___start22, Vector3_t4282066566  ___end23, bool* ___intersects4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GridGraph::ilo_Left37(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool GridGraph_ilo_Left37_m2683632702 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, Vector3_t4282066566  ___p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNConstraint Pathfinding.GridGraph::ilo_get_None38()
extern "C"  NNConstraint_t758567699 * GridGraph_ilo_get_None38_m628157178 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.GridGraph::ilo_GetNearest39(Pathfinding.NavGraph,UnityEngine.Vector3,Pathfinding.NNConstraint)
extern "C"  NNInfo_t1570625892  GridGraph_ilo_GetNearest39_m3154087214 (Il2CppObject * __this /* static, unused */, NavGraph_t1254319713 * ____this0, Vector3_t4282066566  ___position1, NNConstraint_t758567699 * ___constraint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::ilo_SetUpOffsetsAndCosts40(Pathfinding.GridGraph)
extern "C"  void GridGraph_ilo_SetUpOffsetsAndCosts40_m2249511836 (Il2CppObject * __this /* static, unused */, GridGraph_t2455707914 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph::ilo_SetGridGraph41(System.Int32,Pathfinding.GridGraph)
extern "C"  void GridGraph_ilo_SetGridGraph41_m243559628 (Il2CppObject * __this /* static, unused */, int32_t ___graphIndex0, GridGraph_t2455707914 * ___graph1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
