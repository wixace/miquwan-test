﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>
struct Collection_1_t2782659174;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Pathfinding.Voxels.VoxelContour[]
struct VoxelContourU5BU5D_t3554406569;
// System.Collections.Generic.IEnumerator`1<Pathfinding.Voxels.VoxelContour>
struct IEnumerator_1_t1214098769;
// System.Collections.Generic.IList`1<Pathfinding.Voxels.VoxelContour>
struct IList_1_t1996880923;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_VoxelContour3597201016.h"

// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::.ctor()
extern "C"  void Collection_1__ctor_m662222798_gshared (Collection_1_t2782659174 * __this, const MethodInfo* method);
#define Collection_1__ctor_m662222798(__this, method) ((  void (*) (Collection_1_t2782659174 *, const MethodInfo*))Collection_1__ctor_m662222798_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2640223981_gshared (Collection_1_t2782659174 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2640223981(__this, method) ((  bool (*) (Collection_1_t2782659174 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2640223981_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m1025827126_gshared (Collection_1_t2782659174 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1025827126(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2782659174 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1025827126_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m3930331825_gshared (Collection_1_t2782659174 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m3930331825(__this, method) ((  Il2CppObject * (*) (Collection_1_t2782659174 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m3930331825_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m3276765408_gshared (Collection_1_t2782659174 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m3276765408(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2782659174 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m3276765408_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m3607635436_gshared (Collection_1_t2782659174 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m3607635436(__this, ___value0, method) ((  bool (*) (Collection_1_t2782659174 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m3607635436_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m3664265016_gshared (Collection_1_t2782659174 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m3664265016(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2782659174 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3664265016_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m3750476131_gshared (Collection_1_t2782659174 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m3750476131(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2782659174 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3750476131_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m1944546021_gshared (Collection_1_t2782659174 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1944546021(__this, ___value0, method) ((  void (*) (Collection_1_t2782659174 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1944546021_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m2095602160_gshared (Collection_1_t2782659174 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m2095602160(__this, method) ((  bool (*) (Collection_1_t2782659174 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m2095602160_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m2281516636_gshared (Collection_1_t2782659174 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2281516636(__this, method) ((  Il2CppObject * (*) (Collection_1_t2782659174 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m2281516636_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m2951591515_gshared (Collection_1_t2782659174 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2951591515(__this, method) ((  bool (*) (Collection_1_t2782659174 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2951591515_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m4012904766_gshared (Collection_1_t2782659174 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m4012904766(__this, method) ((  bool (*) (Collection_1_t2782659174 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m4012904766_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m3169777891_gshared (Collection_1_t2782659174 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m3169777891(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t2782659174 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m3169777891_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m1343953018_gshared (Collection_1_t2782659174 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1343953018(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2782659174 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1343953018_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::Add(T)
extern "C"  void Collection_1_Add_m3336640241_gshared (Collection_1_t2782659174 * __this, VoxelContour_t3597201016  ___item0, const MethodInfo* method);
#define Collection_1_Add_m3336640241(__this, ___item0, method) ((  void (*) (Collection_1_t2782659174 *, VoxelContour_t3597201016 , const MethodInfo*))Collection_1_Add_m3336640241_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::Clear()
extern "C"  void Collection_1_Clear_m2363323385_gshared (Collection_1_t2782659174 * __this, const MethodInfo* method);
#define Collection_1_Clear_m2363323385(__this, method) ((  void (*) (Collection_1_t2782659174 *, const MethodInfo*))Collection_1_Clear_m2363323385_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::ClearItems()
extern "C"  void Collection_1_ClearItems_m853852329_gshared (Collection_1_t2782659174 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m853852329(__this, method) ((  void (*) (Collection_1_t2782659174 *, const MethodInfo*))Collection_1_ClearItems_m853852329_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::Contains(T)
extern "C"  bool Collection_1_Contains_m1489757095_gshared (Collection_1_t2782659174 * __this, VoxelContour_t3597201016  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m1489757095(__this, ___item0, method) ((  bool (*) (Collection_1_t2782659174 *, VoxelContour_t3597201016 , const MethodInfo*))Collection_1_Contains_m1489757095_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m1066632353_gshared (Collection_1_t2782659174 * __this, VoxelContourU5BU5D_t3554406569* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m1066632353(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2782659174 *, VoxelContourU5BU5D_t3554406569*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1066632353_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m3950379402_gshared (Collection_1_t2782659174 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m3950379402(__this, method) ((  Il2CppObject* (*) (Collection_1_t2782659174 *, const MethodInfo*))Collection_1_GetEnumerator_m3950379402_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m1037389285_gshared (Collection_1_t2782659174 * __this, VoxelContour_t3597201016  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m1037389285(__this, ___item0, method) ((  int32_t (*) (Collection_1_t2782659174 *, VoxelContour_t3597201016 , const MethodInfo*))Collection_1_IndexOf_m1037389285_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m2883342168_gshared (Collection_1_t2782659174 * __this, int32_t ___index0, VoxelContour_t3597201016  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m2883342168(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2782659174 *, int32_t, VoxelContour_t3597201016 , const MethodInfo*))Collection_1_Insert_m2883342168_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m497815947_gshared (Collection_1_t2782659174 * __this, int32_t ___index0, VoxelContour_t3597201016  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m497815947(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2782659174 *, int32_t, VoxelContour_t3597201016 , const MethodInfo*))Collection_1_InsertItem_m497815947_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m1015285005_gshared (Collection_1_t2782659174 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m1015285005(__this, method) ((  Il2CppObject* (*) (Collection_1_t2782659174 *, const MethodInfo*))Collection_1_get_Items_m1015285005_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::Remove(T)
extern "C"  bool Collection_1_Remove_m1931698530_gshared (Collection_1_t2782659174 * __this, VoxelContour_t3597201016  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m1931698530(__this, ___item0, method) ((  bool (*) (Collection_1_t2782659174 *, VoxelContour_t3597201016 , const MethodInfo*))Collection_1_Remove_m1931698530_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m757195038_gshared (Collection_1_t2782659174 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m757195038(__this, ___index0, method) ((  void (*) (Collection_1_t2782659174 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m757195038_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m419616190_gshared (Collection_1_t2782659174 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m419616190(__this, ___index0, method) ((  void (*) (Collection_1_t2782659174 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m419616190_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m1911604278_gshared (Collection_1_t2782659174 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1911604278(__this, method) ((  int32_t (*) (Collection_1_t2782659174 *, const MethodInfo*))Collection_1_get_Count_m1911604278_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::get_Item(System.Int32)
extern "C"  VoxelContour_t3597201016  Collection_1_get_Item_m3322095138_gshared (Collection_1_t2782659174 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m3322095138(__this, ___index0, method) ((  VoxelContour_t3597201016  (*) (Collection_1_t2782659174 *, int32_t, const MethodInfo*))Collection_1_get_Item_m3322095138_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m2447514159_gshared (Collection_1_t2782659174 * __this, int32_t ___index0, VoxelContour_t3597201016  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m2447514159(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2782659174 *, int32_t, VoxelContour_t3597201016 , const MethodInfo*))Collection_1_set_Item_m2447514159_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m505883754_gshared (Collection_1_t2782659174 * __this, int32_t ___index0, VoxelContour_t3597201016  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m505883754(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2782659174 *, int32_t, VoxelContour_t3597201016 , const MethodInfo*))Collection_1_SetItem_m505883754_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m3371821989_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m3371821989(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3371821989_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::ConvertItem(System.Object)
extern "C"  VoxelContour_t3597201016  Collection_1_ConvertItem_m2031678465_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m2031678465(__this /* static, unused */, ___item0, method) ((  VoxelContour_t3597201016  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m2031678465_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m3183493857_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m3183493857(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m3183493857_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m51099647_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m51099647(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m51099647_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Voxels.VoxelContour>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m1009952512_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1009952512(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m1009952512_gshared)(__this /* static, unused */, ___list0, method)
