﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FingerEventDetector_1_gen3103176768MethodDeclarations.h"

// System.Void FingerEventDetector`1<FingerUpEvent>::.ctor()
#define FingerEventDetector_1__ctor_m2733713247(__this, method) ((  void (*) (FingerEventDetector_1_t2025375171 *, const MethodInfo*))FingerEventDetector_1__ctor_m3763465287_gshared)(__this, method)
// T FingerEventDetector`1<FingerUpEvent>::CreateFingerEvent()
#define FingerEventDetector_1_CreateFingerEvent_m2945333427(__this, method) ((  FingerUpEvent_t3093014774 * (*) (FingerEventDetector_1_t2025375171 *, const MethodInfo*))FingerEventDetector_1_CreateFingerEvent_m1420222875_gshared)(__this, method)
// System.Type FingerEventDetector`1<FingerUpEvent>::GetEventType()
#define FingerEventDetector_1_GetEventType_m1686883037(__this, method) ((  Type_t * (*) (FingerEventDetector_1_t2025375171 *, const MethodInfo*))FingerEventDetector_1_GetEventType_m3851890933_gshared)(__this, method)
// System.Void FingerEventDetector`1<FingerUpEvent>::Start()
#define FingerEventDetector_1_Start_m1680851039(__this, method) ((  void (*) (FingerEventDetector_1_t2025375171 *, const MethodInfo*))FingerEventDetector_1_Start_m2710603079_gshared)(__this, method)
// System.Void FingerEventDetector`1<FingerUpEvent>::OnDestroy()
#define FingerEventDetector_1_OnDestroy_m2098345176(__this, method) ((  void (*) (FingerEventDetector_1_t2025375171 *, const MethodInfo*))FingerEventDetector_1_OnDestroy_m3778430400_gshared)(__this, method)
// System.Void FingerEventDetector`1<FingerUpEvent>::FingerGestures_OnInputProviderChanged()
#define FingerEventDetector_1_FingerGestures_OnInputProviderChanged_m759679329(__this, method) ((  void (*) (FingerEventDetector_1_t2025375171 *, const MethodInfo*))FingerEventDetector_1_FingerGestures_OnInputProviderChanged_m572190281_gshared)(__this, method)
// System.Void FingerEventDetector`1<FingerUpEvent>::Init()
#define FingerEventDetector_1_Init_m1979383573(__this, method) ((  void (*) (FingerEventDetector_1_t2025375171 *, const MethodInfo*))FingerEventDetector_1_Init_m2289696045_gshared)(__this, method)
// System.Void FingerEventDetector`1<FingerUpEvent>::Init(System.Int32)
#define FingerEventDetector_1_Init_m2042739942(__this, ___fingersCount0, method) ((  void (*) (FingerEventDetector_1_t2025375171 *, int32_t, const MethodInfo*))FingerEventDetector_1_Init_m469522174_gshared)(__this, ___fingersCount0, method)
// T FingerEventDetector`1<FingerUpEvent>::GetEvent(FingerGestures/Finger)
#define FingerEventDetector_1_GetEvent_m1374106309(__this, ___finger0, method) ((  FingerUpEvent_t3093014774 * (*) (FingerEventDetector_1_t2025375171 *, Finger_t182428197 *, const MethodInfo*))FingerEventDetector_1_GetEvent_m906084269_gshared)(__this, ___finger0, method)
// T FingerEventDetector`1<FingerUpEvent>::GetEvent(System.Int32)
#define FingerEventDetector_1_GetEvent_m1357600441(__this, ___fingerIndex0, method) ((  FingerUpEvent_t3093014774 * (*) (FingerEventDetector_1_t2025375171 *, int32_t, const MethodInfo*))FingerEventDetector_1_GetEvent_m3248170193_gshared)(__this, ___fingerIndex0, method)
// FingerGestures FingerEventDetector`1<FingerUpEvent>::ilo_get_Instance1()
#define FingerEventDetector_1_ilo_get_Instance1_m3106253459(__this /* static, unused */, method) ((  FingerGestures_t2907604723 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))FingerEventDetector_1_ilo_get_Instance1_m1581142907_gshared)(__this /* static, unused */, method)
// FingerGestures/Finger FingerEventDetector`1<FingerUpEvent>::ilo_GetFinger2(System.Int32)
#define FingerEventDetector_1_ilo_GetFinger2_m2936230218(__this /* static, unused */, ___index0, method) ((  Finger_t182428197 * (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))FingerEventDetector_1_ilo_GetFinger2_m3652595042_gshared)(__this /* static, unused */, ___index0, method)
