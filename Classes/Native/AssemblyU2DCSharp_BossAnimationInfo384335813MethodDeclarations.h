﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BossAnimationInfo
struct BossAnimationInfo_t384335813;
// ProtoBuf.IExtension
struct IExtension_t1606339106;
// System.Collections.Generic.List`1<AnimationPointJSC>
struct List_1_t1855780574;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_BossAnimationInfo384335813.h"

// System.Void BossAnimationInfo::.ctor()
extern "C"  void BossAnimationInfo__ctor_m831396678 (BossAnimationInfo_t384335813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension BossAnimationInfo::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * BossAnimationInfo_ProtoBuf_IExtensible_GetExtensionObject_m3891108426 (BossAnimationInfo_t384335813 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 BossAnimationInfo::get_TrigerPos()
extern "C"  Vector3_t4282066566  BossAnimationInfo_get_TrigerPos_m1009080832 (BossAnimationInfo_t384335813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 BossAnimationInfo::get_BossPos()
extern "C"  Vector3_t4282066566  BossAnimationInfo_get_BossPos_m4076919772 (BossAnimationInfo_t384335813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BossAnimationInfo::get_id()
extern "C"  int32_t BossAnimationInfo_get_id_m784734388 (BossAnimationInfo_t384335813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationInfo::set_id(System.Int32)
extern "C"  void BossAnimationInfo_set_id_m4100497991 (BossAnimationInfo_t384335813 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BossAnimationInfo::get_trigerPos_x()
extern "C"  float BossAnimationInfo_get_trigerPos_x_m4076682859 (BossAnimationInfo_t384335813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationInfo::set_trigerPos_x(System.Single)
extern "C"  void BossAnimationInfo_set_trigerPos_x_m851513312 (BossAnimationInfo_t384335813 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BossAnimationInfo::get_trigerPos_y()
extern "C"  float BossAnimationInfo_get_trigerPos_y_m4076683820 (BossAnimationInfo_t384335813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationInfo::set_trigerPos_y(System.Single)
extern "C"  void BossAnimationInfo_set_trigerPos_y_m340979135 (BossAnimationInfo_t384335813 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BossAnimationInfo::get_trigerPos_z()
extern "C"  float BossAnimationInfo_get_trigerPos_z_m4076684781 (BossAnimationInfo_t384335813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationInfo::set_trigerPos_z(System.Single)
extern "C"  void BossAnimationInfo_set_trigerPos_z_m4125412254 (BossAnimationInfo_t384335813 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BossAnimationInfo::get_isBoss()
extern "C"  int32_t BossAnimationInfo_get_isBoss_m30310480 (BossAnimationInfo_t384335813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationInfo::set_isBoss(System.Int32)
extern "C"  void BossAnimationInfo_set_isBoss_m3298266851 (BossAnimationInfo_t384335813 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BossAnimationInfo::get_bossPos_x()
extern "C"  float BossAnimationInfo_get_bossPos_x_m1730063815 (BossAnimationInfo_t384335813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationInfo::set_bossPos_x(System.Single)
extern "C"  void BossAnimationInfo_set_bossPos_x_m3489867268 (BossAnimationInfo_t384335813 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BossAnimationInfo::get_bossPos_y()
extern "C"  float BossAnimationInfo_get_bossPos_y_m1730064776 (BossAnimationInfo_t384335813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationInfo::set_bossPos_y(System.Single)
extern "C"  void BossAnimationInfo_set_bossPos_y_m2979333091 (BossAnimationInfo_t384335813 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BossAnimationInfo::get_bossPos_z()
extern "C"  float BossAnimationInfo_get_bossPos_z_m1730065737 (BossAnimationInfo_t384335813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationInfo::set_bossPos_z(System.Single)
extern "C"  void BossAnimationInfo_set_bossPos_z_m2468798914 (BossAnimationInfo_t384335813 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BossAnimationInfo::get_bossRot_y()
extern "C"  float BossAnimationInfo_get_bossRot_y_m3505995659 (BossAnimationInfo_t384335813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationInfo::set_bossRot_y(System.Single)
extern "C"  void BossAnimationInfo_set_bossRot_y_m1164612288 (BossAnimationInfo_t384335813 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<AnimationPointJSC> BossAnimationInfo::get_camera()
extern "C"  List_1_t1855780574 * BossAnimationInfo_get_camera_m2506993629 (BossAnimationInfo_t384335813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<AnimationPointJSC> BossAnimationInfo::get_boss()
extern "C"  List_1_t1855780574 * BossAnimationInfo_get_boss_m510003173 (BossAnimationInfo_t384335813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension BossAnimationInfo::ilo_GetExtensionObject1(ProtoBuf.IExtension&,System.Boolean)
extern "C"  Il2CppObject * BossAnimationInfo_ilo_GetExtensionObject1_m1395994358 (Il2CppObject * __this /* static, unused */, Il2CppObject ** ___extensionObject0, bool ___createIfMissing1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BossAnimationInfo::ilo_get_trigerPos_x2(BossAnimationInfo)
extern "C"  float BossAnimationInfo_ilo_get_trigerPos_x2_m4274073105 (Il2CppObject * __this /* static, unused */, BossAnimationInfo_t384335813 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BossAnimationInfo::ilo_get_trigerPos_z3(BossAnimationInfo)
extern "C"  float BossAnimationInfo_ilo_get_trigerPos_z3_m4287950706 (Il2CppObject * __this /* static, unused */, BossAnimationInfo_t384335813 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
