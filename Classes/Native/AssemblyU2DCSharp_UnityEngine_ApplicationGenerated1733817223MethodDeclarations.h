﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ApplicationGenerated
struct UnityEngine_ApplicationGenerated_t1733817223;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Application/AdvertisingIdentifierCallback
struct AdvertisingIdentifierCallback_t1751144028;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.String
struct String_t;
// System.Delegate
struct Delegate_t3310234105;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_ApplicationGenerated::.ctor()
extern "C"  void UnityEngine_ApplicationGenerated__ctor_m2934573812 (UnityEngine_ApplicationGenerated_t1733817223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ApplicationGenerated::Application_Application1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ApplicationGenerated_Application_Application1_m1571989284 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_streamedBytes(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_streamedBytes_m3482054040 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_isPlaying(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_isPlaying_m2982821760 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_isEditor(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_isEditor_m1222925137 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_isWebPlayer(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_isWebPlayer_m1035087417 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_platform(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_platform_m1269180821 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_isMobilePlatform(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_isMobilePlatform_m1711688329 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_isConsolePlatform(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_isConsolePlatform_m3248427076 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_runInBackground(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_runInBackground_m692883334 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_dataPath(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_dataPath_m866040473 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_streamingAssetsPath(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_streamingAssetsPath_m1402974234 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_persistentDataPath(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_persistentDataPath_m1488292418 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_temporaryCachePath(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_temporaryCachePath_m763043698 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_srcValue(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_srcValue_m528918875 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_absoluteURL(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_absoluteURL_m796033452 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_unityVersion(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_unityVersion_m1420642149 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_version(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_version_m79266892 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_bundleIdentifier(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_bundleIdentifier_m3849221181 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_installMode(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_installMode_m2045270790 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_sandboxType(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_sandboxType_m2246315427 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_productName(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_productName_m764182602 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_companyName(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_companyName_m3867494268 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_cloudProjectId(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_cloudProjectId_m2327960233 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_webSecurityEnabled(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_webSecurityEnabled_m3698309851 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_webSecurityHostUrl(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_webSecurityHostUrl_m46277173 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_targetFrameRate(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_targetFrameRate_m2494999112 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_systemLanguage(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_systemLanguage_m3584991329 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_stackTraceLogType(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_stackTraceLogType_m1016458115 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_backgroundLoadingPriority(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_backgroundLoadingPriority_m3829657330 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_internetReachability(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_internetReachability_m2179233392 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_genuine(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_genuine_m4148346345 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_genuineCheckAvailable(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_genuineCheckAvailable_m1461494536 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::Application_isShowingSplashScreen(JSVCall)
extern "C"  void UnityEngine_ApplicationGenerated_Application_isShowingSplashScreen_m505198806 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ApplicationGenerated::Application_CancelQuit(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ApplicationGenerated_Application_CancelQuit_m305453228 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ApplicationGenerated::Application_CanStreamedLevelBeLoaded__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ApplicationGenerated_Application_CanStreamedLevelBeLoaded__Int32_m499506960 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ApplicationGenerated::Application_CanStreamedLevelBeLoaded__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ApplicationGenerated_Application_CanStreamedLevelBeLoaded__String_m4095901329 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ApplicationGenerated::Application_CaptureScreenshot__String__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ApplicationGenerated_Application_CaptureScreenshot__String__Int32_m2003402708 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ApplicationGenerated::Application_CaptureScreenshot__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ApplicationGenerated_Application_CaptureScreenshot__String_m11138044 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ApplicationGenerated::Application_ExternalCall__String__Object_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ApplicationGenerated_Application_ExternalCall__String__Object_Array_m2234245846 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ApplicationGenerated::Application_GetStreamProgressForLevel__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ApplicationGenerated_Application_GetStreamProgressForLevel__String_m3209931086 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ApplicationGenerated::Application_GetStreamProgressForLevel__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ApplicationGenerated_Application_GetStreamProgressForLevel__Int32_m748021939 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ApplicationGenerated::Application_HasProLicense(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ApplicationGenerated_Application_HasProLicense_m3549557741 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ApplicationGenerated::Application_HasUserAuthorization__UserAuthorization(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ApplicationGenerated_Application_HasUserAuthorization__UserAuthorization_m1636970841 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ApplicationGenerated::Application_OpenURL__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ApplicationGenerated_Application_OpenURL__String_m1667645749 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ApplicationGenerated::Application_Quit(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ApplicationGenerated_Application_Quit_m844582930 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Application/AdvertisingIdentifierCallback UnityEngine_ApplicationGenerated::Application_RequestAdvertisingIdentifierAsync_GetDelegate_member12_arg0(CSRepresentedObject)
extern "C"  AdvertisingIdentifierCallback_t1751144028 * UnityEngine_ApplicationGenerated_Application_RequestAdvertisingIdentifierAsync_GetDelegate_member12_arg0_m1397956247 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ApplicationGenerated::Application_RequestAdvertisingIdentifierAsync__AdvertisingIdentifierCallback(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ApplicationGenerated_Application_RequestAdvertisingIdentifierAsync__AdvertisingIdentifierCallback_m3835206167 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ApplicationGenerated::Application_RequestUserAuthorization__UserAuthorization(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ApplicationGenerated_Application_RequestUserAuthorization__UserAuthorization_m3346968078 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::__Register()
extern "C"  void UnityEngine_ApplicationGenerated___Register_m701246035 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] UnityEngine_ApplicationGenerated::<Application_ExternalCall__String__Object_Array>m__17D()
extern "C"  ObjectU5BU5D_t1108656482* UnityEngine_ApplicationGenerated_U3CApplication_ExternalCall__String__Object_ArrayU3Em__17D_m2852771988 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Application/AdvertisingIdentifierCallback UnityEngine_ApplicationGenerated::<Application_RequestAdvertisingIdentifierAsync__AdvertisingIdentifierCallback>m__17F()
extern "C"  AdvertisingIdentifierCallback_t1751144028 * UnityEngine_ApplicationGenerated_U3CApplication_RequestAdvertisingIdentifierAsync__AdvertisingIdentifierCallbackU3Em__17F_m2843823641 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::ilo_setInt321(System.Int32,System.Int32)
extern "C"  void UnityEngine_ApplicationGenerated_ilo_setInt321_m301315938 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::ilo_setBooleanS2(System.Int32,System.Boolean)
extern "C"  void UnityEngine_ApplicationGenerated_ilo_setBooleanS2_m519717380 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::ilo_setStringS3(System.Int32,System.String)
extern "C"  void UnityEngine_ApplicationGenerated_ilo_setStringS3_m4240096447 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::ilo_setEnum4(System.Int32,System.Int32)
extern "C"  void UnityEngine_ApplicationGenerated_ilo_setEnum4_m2193252018 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ApplicationGenerated::ilo_getInt325(System.Int32)
extern "C"  int32_t UnityEngine_ApplicationGenerated_ilo_getInt325_m3013792319 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_ApplicationGenerated::ilo_getStringS6(System.Int32)
extern "C"  String_t* UnityEngine_ApplicationGenerated_ilo_getStringS6_m3676411383 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::ilo_setSingle7(System.Int32,System.Single)
extern "C"  void UnityEngine_ApplicationGenerated_ilo_setSingle7_m2513560582 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ApplicationGenerated::ilo_getEnum8(System.Int32)
extern "C"  int32_t UnityEngine_ApplicationGenerated_ilo_getEnum8_m1409788131 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated::ilo_addJSFunCSDelegateRel9(System.Int32,System.Delegate)
extern "C"  void UnityEngine_ApplicationGenerated_ilo_addJSFunCSDelegateRel9_m979482726 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, Delegate_t3310234105 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ApplicationGenerated::ilo_setObject10(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_ApplicationGenerated_ilo_setObject10_m3411205062 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ApplicationGenerated::ilo_getObject11(System.Int32)
extern "C"  int32_t UnityEngine_ApplicationGenerated_ilo_getObject11_m638875895 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Application/AdvertisingIdentifierCallback UnityEngine_ApplicationGenerated::ilo_Application_RequestAdvertisingIdentifierAsync_GetDelegate_member12_arg012(CSRepresentedObject)
extern "C"  AdvertisingIdentifierCallback_t1751144028 * UnityEngine_ApplicationGenerated_ilo_Application_RequestAdvertisingIdentifierAsync_GetDelegate_member12_arg012_m2593261577 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_ApplicationGenerated::ilo_getObject13(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_ApplicationGenerated_ilo_getObject13_m3603883560 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
