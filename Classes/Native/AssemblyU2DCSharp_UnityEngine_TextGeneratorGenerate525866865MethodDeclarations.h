﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_TextGeneratorGenerated
struct UnityEngine_TextGeneratorGenerated_t525866865;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_TextGeneratorGenerated::.ctor()
extern "C"  void UnityEngine_TextGeneratorGenerated__ctor_m1130334282 (UnityEngine_TextGeneratorGenerated_t525866865 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextGeneratorGenerated::TextGenerator_TextGenerator1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextGeneratorGenerated_TextGenerator_TextGenerator1_m2453991714 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextGeneratorGenerated::TextGenerator_TextGenerator2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextGeneratorGenerated_TextGenerator_TextGenerator2_m3698756195 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGeneratorGenerated::TextGenerator_verts(JSVCall)
extern "C"  void UnityEngine_TextGeneratorGenerated_TextGenerator_verts_m3744904598 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGeneratorGenerated::TextGenerator_characters(JSVCall)
extern "C"  void UnityEngine_TextGeneratorGenerated_TextGenerator_characters_m3519755882 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGeneratorGenerated::TextGenerator_lines(JSVCall)
extern "C"  void UnityEngine_TextGeneratorGenerated_TextGenerator_lines_m4000047257 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGeneratorGenerated::TextGenerator_rectExtents(JSVCall)
extern "C"  void UnityEngine_TextGeneratorGenerated_TextGenerator_rectExtents_m2655620307 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGeneratorGenerated::TextGenerator_vertexCount(JSVCall)
extern "C"  void UnityEngine_TextGeneratorGenerated_TextGenerator_vertexCount_m3054260685 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGeneratorGenerated::TextGenerator_characterCount(JSVCall)
extern "C"  void UnityEngine_TextGeneratorGenerated_TextGenerator_characterCount_m411749870 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGeneratorGenerated::TextGenerator_characterCountVisible(JSVCall)
extern "C"  void UnityEngine_TextGeneratorGenerated_TextGenerator_characterCountVisible_m3139992588 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGeneratorGenerated::TextGenerator_lineCount(JSVCall)
extern "C"  void UnityEngine_TextGeneratorGenerated_TextGenerator_lineCount_m1130154141 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGeneratorGenerated::TextGenerator_fontSizeUsedForBestFit(JSVCall)
extern "C"  void UnityEngine_TextGeneratorGenerated_TextGenerator_fontSizeUsedForBestFit_m3798128771 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextGeneratorGenerated::TextGenerator_GetCharacters__ListT1_UICharInfo(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextGeneratorGenerated_TextGenerator_GetCharacters__ListT1_UICharInfo_m3175895251 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextGeneratorGenerated::TextGenerator_GetCharactersArray(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextGeneratorGenerated_TextGenerator_GetCharactersArray_m1110798000 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextGeneratorGenerated::TextGenerator_GetLines__ListT1_UILineInfo(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextGeneratorGenerated_TextGenerator_GetLines__ListT1_UILineInfo_m4257939292 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextGeneratorGenerated::TextGenerator_GetLinesArray(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextGeneratorGenerated_TextGenerator_GetLinesArray_m62796123 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextGeneratorGenerated::TextGenerator_GetPreferredHeight__String__TextGenerationSettings(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextGeneratorGenerated_TextGenerator_GetPreferredHeight__String__TextGenerationSettings_m2485905058 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextGeneratorGenerated::TextGenerator_GetPreferredWidth__String__TextGenerationSettings(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextGeneratorGenerated_TextGenerator_GetPreferredWidth__String__TextGenerationSettings_m4002893823 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextGeneratorGenerated::TextGenerator_GetVertices__ListT1_UIVertex(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextGeneratorGenerated_TextGenerator_GetVertices__ListT1_UIVertex_m4129932388 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextGeneratorGenerated::TextGenerator_GetVerticesArray(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextGeneratorGenerated_TextGenerator_GetVerticesArray_m3711411137 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextGeneratorGenerated::TextGenerator_Invalidate(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextGeneratorGenerated_TextGenerator_Invalidate_m2463028370 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextGeneratorGenerated::TextGenerator_Populate__String__TextGenerationSettings(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextGeneratorGenerated_TextGenerator_Populate__String__TextGenerationSettings_m2128799290 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGeneratorGenerated::__Register()
extern "C"  void UnityEngine_TextGeneratorGenerated___Register_m4143813309 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_TextGeneratorGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_TextGeneratorGenerated_ilo_getObject1_m824607112 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_TextGeneratorGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_TextGeneratorGenerated_ilo_setObject2_m1241916225 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGeneratorGenerated::ilo_setInt323(System.Int32,System.Int32)
extern "C"  void UnityEngine_TextGeneratorGenerated_ilo_setInt323_m2368850826 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_TextGeneratorGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_TextGeneratorGenerated_ilo_getObject4_m3648032552 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGeneratorGenerated::ilo_moveSaveID2Arr5(System.Int32)
extern "C"  void UnityEngine_TextGeneratorGenerated_ilo_moveSaveID2Arr5_m93681379 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGeneratorGenerated::ilo_setArrayS6(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_TextGeneratorGenerated_ilo_setArrayS6_m871397310 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_TextGeneratorGenerated::ilo_getStringS7(System.Int32)
extern "C"  String_t* UnityEngine_TextGeneratorGenerated_ilo_getStringS7_m2328022478 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
