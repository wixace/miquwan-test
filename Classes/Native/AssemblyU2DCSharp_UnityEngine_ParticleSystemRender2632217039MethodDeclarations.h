﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ParticleSystemRendererGenerated
struct UnityEngine_ParticleSystemRendererGenerated_t2632217039;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_ParticleSystemRendererGenerated::.ctor()
extern "C"  void UnityEngine_ParticleSystemRendererGenerated__ctor_m3830645372 (UnityEngine_ParticleSystemRendererGenerated_t2632217039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleSystemRendererGenerated::ParticleSystemRenderer_ParticleSystemRenderer1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleSystemRendererGenerated_ParticleSystemRenderer_ParticleSystemRenderer1_m935763766 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemRendererGenerated::ParticleSystemRenderer_renderMode(JSVCall)
extern "C"  void UnityEngine_ParticleSystemRendererGenerated_ParticleSystemRenderer_renderMode_m2190732685 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemRendererGenerated::ParticleSystemRenderer_lengthScale(JSVCall)
extern "C"  void UnityEngine_ParticleSystemRendererGenerated_ParticleSystemRenderer_lengthScale_m2128403394 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemRendererGenerated::ParticleSystemRenderer_velocityScale(JSVCall)
extern "C"  void UnityEngine_ParticleSystemRendererGenerated_ParticleSystemRenderer_velocityScale_m306688377 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemRendererGenerated::ParticleSystemRenderer_cameraVelocityScale(JSVCall)
extern "C"  void UnityEngine_ParticleSystemRendererGenerated_ParticleSystemRenderer_cameraVelocityScale_m1686648574 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemRendererGenerated::ParticleSystemRenderer_normalDirection(JSVCall)
extern "C"  void UnityEngine_ParticleSystemRendererGenerated_ParticleSystemRenderer_normalDirection_m3670071790 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemRendererGenerated::ParticleSystemRenderer_alignment(JSVCall)
extern "C"  void UnityEngine_ParticleSystemRendererGenerated_ParticleSystemRenderer_alignment_m459133635 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemRendererGenerated::ParticleSystemRenderer_pivot(JSVCall)
extern "C"  void UnityEngine_ParticleSystemRendererGenerated_ParticleSystemRenderer_pivot_m928236356 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemRendererGenerated::ParticleSystemRenderer_sortMode(JSVCall)
extern "C"  void UnityEngine_ParticleSystemRendererGenerated_ParticleSystemRenderer_sortMode_m4180088453 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemRendererGenerated::ParticleSystemRenderer_sortingFudge(JSVCall)
extern "C"  void UnityEngine_ParticleSystemRendererGenerated_ParticleSystemRenderer_sortingFudge_m544857271 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemRendererGenerated::ParticleSystemRenderer_minParticleSize(JSVCall)
extern "C"  void UnityEngine_ParticleSystemRendererGenerated_ParticleSystemRenderer_minParticleSize_m1494090317 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemRendererGenerated::ParticleSystemRenderer_maxParticleSize(JSVCall)
extern "C"  void UnityEngine_ParticleSystemRendererGenerated_ParticleSystemRenderer_maxParticleSize_m1220182651 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemRendererGenerated::ParticleSystemRenderer_mesh(JSVCall)
extern "C"  void UnityEngine_ParticleSystemRendererGenerated_ParticleSystemRenderer_mesh_m3602498169 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemRendererGenerated::__Register()
extern "C"  void UnityEngine_ParticleSystemRendererGenerated___Register_m2054727627 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ParticleSystemRendererGenerated::ilo_getEnum1(System.Int32)
extern "C"  int32_t UnityEngine_ParticleSystemRendererGenerated_ilo_getEnum1_m2347034040 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemRendererGenerated::ilo_setSingle2(System.Int32,System.Single)
extern "C"  void UnityEngine_ParticleSystemRendererGenerated_ilo_setSingle2_m433326329 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
