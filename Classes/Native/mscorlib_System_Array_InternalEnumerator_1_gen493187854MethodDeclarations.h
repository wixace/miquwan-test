﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2953159047MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<Pathfinding.RichAI>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2211252415(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t493187854 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2616641763_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.RichAI>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1406344833(__this, method) ((  void (*) (InternalEnumerator_1_t493187854 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224260061_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Pathfinding.RichAI>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m761575213(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t493187854 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m390763987_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.RichAI>::Dispose()
#define InternalEnumerator_1_Dispose_m2396759382(__this, method) ((  void (*) (InternalEnumerator_1_t493187854 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2760671866_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.RichAI>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2008723565(__this, method) ((  bool (*) (InternalEnumerator_1_t493187854 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3716548237_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Pathfinding.RichAI>::get_Current()
#define InternalEnumerator_1_get_Current_m1902185670(__this, method) ((  RichAI_t1710845178 * (*) (InternalEnumerator_1_t493187854 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2178852364_gshared)(__this, method)
