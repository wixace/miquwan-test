﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._fe74a15fede858edf154725875d666eb
struct _fe74a15fede858edf154725875d666eb_t2103432447;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._fe74a15fede858edf154725875d666eb::.ctor()
extern "C"  void _fe74a15fede858edf154725875d666eb__ctor_m1671182 (_fe74a15fede858edf154725875d666eb_t2103432447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._fe74a15fede858edf154725875d666eb::_fe74a15fede858edf154725875d666ebm2(System.Int32)
extern "C"  int32_t _fe74a15fede858edf154725875d666eb__fe74a15fede858edf154725875d666ebm2_m1136949561 (_fe74a15fede858edf154725875d666eb_t2103432447 * __this, int32_t ____fe74a15fede858edf154725875d666eba0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._fe74a15fede858edf154725875d666eb::_fe74a15fede858edf154725875d666ebm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _fe74a15fede858edf154725875d666eb__fe74a15fede858edf154725875d666ebm_m644219293 (_fe74a15fede858edf154725875d666eb_t2103432447 * __this, int32_t ____fe74a15fede858edf154725875d666eba0, int32_t ____fe74a15fede858edf154725875d666eb491, int32_t ____fe74a15fede858edf154725875d666ebc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
