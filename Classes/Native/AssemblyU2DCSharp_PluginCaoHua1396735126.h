﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.String
struct String_t;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginCaoHua
struct  PluginCaoHua_t1396735126  : public MonoBehaviour_t667441552
{
public:
	// Mihua.SDK.PayInfo PluginCaoHua::payInfo
	PayInfo_t1775308120 * ___payInfo_2;
	// System.String PluginCaoHua::Token
	String_t* ___Token_3;
	// System.String PluginCaoHua::UserID
	String_t* ___UserID_4;
	// System.String PluginCaoHua::configId
	String_t* ___configId_5;
	// System.String PluginCaoHua::gameId
	String_t* ___gameId_6;
	// System.String PluginCaoHua::gameExtra
	String_t* ___gameExtra_7;
	// System.String PluginCaoHua::gameName
	String_t* ___gameName_8;
	// System.Boolean PluginCaoHua::isOut
	bool ___isOut_9;

public:
	inline static int32_t get_offset_of_payInfo_2() { return static_cast<int32_t>(offsetof(PluginCaoHua_t1396735126, ___payInfo_2)); }
	inline PayInfo_t1775308120 * get_payInfo_2() const { return ___payInfo_2; }
	inline PayInfo_t1775308120 ** get_address_of_payInfo_2() { return &___payInfo_2; }
	inline void set_payInfo_2(PayInfo_t1775308120 * value)
	{
		___payInfo_2 = value;
		Il2CppCodeGenWriteBarrier(&___payInfo_2, value);
	}

	inline static int32_t get_offset_of_Token_3() { return static_cast<int32_t>(offsetof(PluginCaoHua_t1396735126, ___Token_3)); }
	inline String_t* get_Token_3() const { return ___Token_3; }
	inline String_t** get_address_of_Token_3() { return &___Token_3; }
	inline void set_Token_3(String_t* value)
	{
		___Token_3 = value;
		Il2CppCodeGenWriteBarrier(&___Token_3, value);
	}

	inline static int32_t get_offset_of_UserID_4() { return static_cast<int32_t>(offsetof(PluginCaoHua_t1396735126, ___UserID_4)); }
	inline String_t* get_UserID_4() const { return ___UserID_4; }
	inline String_t** get_address_of_UserID_4() { return &___UserID_4; }
	inline void set_UserID_4(String_t* value)
	{
		___UserID_4 = value;
		Il2CppCodeGenWriteBarrier(&___UserID_4, value);
	}

	inline static int32_t get_offset_of_configId_5() { return static_cast<int32_t>(offsetof(PluginCaoHua_t1396735126, ___configId_5)); }
	inline String_t* get_configId_5() const { return ___configId_5; }
	inline String_t** get_address_of_configId_5() { return &___configId_5; }
	inline void set_configId_5(String_t* value)
	{
		___configId_5 = value;
		Il2CppCodeGenWriteBarrier(&___configId_5, value);
	}

	inline static int32_t get_offset_of_gameId_6() { return static_cast<int32_t>(offsetof(PluginCaoHua_t1396735126, ___gameId_6)); }
	inline String_t* get_gameId_6() const { return ___gameId_6; }
	inline String_t** get_address_of_gameId_6() { return &___gameId_6; }
	inline void set_gameId_6(String_t* value)
	{
		___gameId_6 = value;
		Il2CppCodeGenWriteBarrier(&___gameId_6, value);
	}

	inline static int32_t get_offset_of_gameExtra_7() { return static_cast<int32_t>(offsetof(PluginCaoHua_t1396735126, ___gameExtra_7)); }
	inline String_t* get_gameExtra_7() const { return ___gameExtra_7; }
	inline String_t** get_address_of_gameExtra_7() { return &___gameExtra_7; }
	inline void set_gameExtra_7(String_t* value)
	{
		___gameExtra_7 = value;
		Il2CppCodeGenWriteBarrier(&___gameExtra_7, value);
	}

	inline static int32_t get_offset_of_gameName_8() { return static_cast<int32_t>(offsetof(PluginCaoHua_t1396735126, ___gameName_8)); }
	inline String_t* get_gameName_8() const { return ___gameName_8; }
	inline String_t** get_address_of_gameName_8() { return &___gameName_8; }
	inline void set_gameName_8(String_t* value)
	{
		___gameName_8 = value;
		Il2CppCodeGenWriteBarrier(&___gameName_8, value);
	}

	inline static int32_t get_offset_of_isOut_9() { return static_cast<int32_t>(offsetof(PluginCaoHua_t1396735126, ___isOut_9)); }
	inline bool get_isOut_9() const { return ___isOut_9; }
	inline bool* get_address_of_isOut_9() { return &___isOut_9; }
	inline void set_isOut_9(bool value)
	{
		___isOut_9 = value;
	}
};

struct PluginCaoHua_t1396735126_StaticFields
{
public:
	// System.Action PluginCaoHua::<>f__am$cache8
	Action_t3771233898 * ___U3CU3Ef__amU24cache8_10;
	// System.Action PluginCaoHua::<>f__am$cache9
	Action_t3771233898 * ___U3CU3Ef__amU24cache9_11;
	// System.Action PluginCaoHua::<>f__am$cacheA
	Action_t3771233898 * ___U3CU3Ef__amU24cacheA_12;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_10() { return static_cast<int32_t>(offsetof(PluginCaoHua_t1396735126_StaticFields, ___U3CU3Ef__amU24cache8_10)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache8_10() const { return ___U3CU3Ef__amU24cache8_10; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache8_10() { return &___U3CU3Ef__amU24cache8_10; }
	inline void set_U3CU3Ef__amU24cache8_10(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache8_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_11() { return static_cast<int32_t>(offsetof(PluginCaoHua_t1396735126_StaticFields, ___U3CU3Ef__amU24cache9_11)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache9_11() const { return ___U3CU3Ef__amU24cache9_11; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache9_11() { return &___U3CU3Ef__amU24cache9_11; }
	inline void set_U3CU3Ef__amU24cache9_11(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache9_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_12() { return static_cast<int32_t>(offsetof(PluginCaoHua_t1396735126_StaticFields, ___U3CU3Ef__amU24cacheA_12)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cacheA_12() const { return ___U3CU3Ef__amU24cacheA_12; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cacheA_12() { return &___U3CU3Ef__amU24cacheA_12; }
	inline void set_U3CU3Ef__amU24cacheA_12(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cacheA_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
