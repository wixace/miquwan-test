﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3006170552MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<AnimationRunner/AniType,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m2328700297(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3137553034 *, Dictionary_2_t141980025 *, const MethodInfo*))ValueCollection__ctor_m220130359_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<AnimationRunner/AniType,System.String>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3131163017(__this, ___item0, method) ((  void (*) (ValueCollection_t3137553034 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2093313307_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<AnimationRunner/AniType,System.String>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3376405202(__this, method) ((  void (*) (ValueCollection_t3137553034 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m320610148_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<AnimationRunner/AniType,System.String>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3091604157(__this, ___item0, method) ((  bool (*) (ValueCollection_t3137553034 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2581127787_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<AnimationRunner/AniType,System.String>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1523762850(__this, ___item0, method) ((  bool (*) (ValueCollection_t3137553034 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2412616144_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<AnimationRunner/AniType,System.String>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m796036384(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3137553034 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2580684722_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<AnimationRunner/AniType,System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m3062406486(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3137553034 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m122540904_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<AnimationRunner/AniType,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3383829841(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3137553034 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2346458083_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<AnimationRunner/AniType,System.String>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1366780016(__this, method) ((  bool (*) (ValueCollection_t3137553034 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m856303646_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<AnimationRunner/AniType,System.String>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2801981008(__this, method) ((  bool (*) (ValueCollection_t3137553034 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1885678334_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<AnimationRunner/AniType,System.String>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3384508796(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3137553034 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2381213866_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<AnimationRunner/AniType,System.String>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m3963621264(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3137553034 *, StringU5BU5D_t4054002952*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2338527934_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<AnimationRunner/AniType,System.String>::GetEnumerator()
#define ValueCollection_GetEnumerator_m331449395(__this, method) ((  Enumerator_t2368780729  (*) (ValueCollection_t3137553034 *, const MethodInfo*))ValueCollection_GetEnumerator_m56596449_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<AnimationRunner/AniType,System.String>::get_Count()
#define ValueCollection_get_Count_m1869652758(__this, method) ((  int32_t (*) (ValueCollection_t3137553034 *, const MethodInfo*))ValueCollection_get_Count_m3533159876_gshared)(__this, method)
