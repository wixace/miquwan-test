﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF3109307074.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Serialization.LayerMaskConverter
struct  LayerMaskConverter_t486130797  : public JsonConverter_t3109307074
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
