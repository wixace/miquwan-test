﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_fearte56
struct  M_fearte56_t2319210322  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_fearte56::_matrorseSori
	float ____matrorseSori_0;
	// System.Int32 GarbageiOS.M_fearte56::_sarbemmiKarpere
	int32_t ____sarbemmiKarpere_1;
	// System.String GarbageiOS.M_fearte56::_gaidertru
	String_t* ____gaidertru_2;
	// System.Boolean GarbageiOS.M_fearte56::_xearooSearnor
	bool ____xearooSearnor_3;
	// System.Single GarbageiOS.M_fearte56::_serecaili
	float ____serecaili_4;
	// System.Int32 GarbageiOS.M_fearte56::_xemstirhar
	int32_t ____xemstirhar_5;
	// System.Int32 GarbageiOS.M_fearte56::_bolalceSallqoobea
	int32_t ____bolalceSallqoobea_6;
	// System.UInt32 GarbageiOS.M_fearte56::_bayseSoujoo
	uint32_t ____bayseSoujoo_7;
	// System.Int32 GarbageiOS.M_fearte56::_pila
	int32_t ____pila_8;
	// System.Int32 GarbageiOS.M_fearte56::_chaviherLoumeremay
	int32_t ____chaviherLoumeremay_9;
	// System.Int32 GarbageiOS.M_fearte56::_jelpaWeeraw
	int32_t ____jelpaWeeraw_10;
	// System.Int32 GarbageiOS.M_fearte56::_dachere
	int32_t ____dachere_11;
	// System.Boolean GarbageiOS.M_fearte56::_pokasteNetra
	bool ____pokasteNetra_12;
	// System.String GarbageiOS.M_fearte56::_bestipaZearge
	String_t* ____bestipaZearge_13;
	// System.String GarbageiOS.M_fearte56::_maibelta
	String_t* ____maibelta_14;
	// System.Single GarbageiOS.M_fearte56::_whocisaGisbeber
	float ____whocisaGisbeber_15;
	// System.String GarbageiOS.M_fearte56::_demouri
	String_t* ____demouri_16;
	// System.Int32 GarbageiOS.M_fearte56::_hasjaPakal
	int32_t ____hasjaPakal_17;

public:
	inline static int32_t get_offset_of__matrorseSori_0() { return static_cast<int32_t>(offsetof(M_fearte56_t2319210322, ____matrorseSori_0)); }
	inline float get__matrorseSori_0() const { return ____matrorseSori_0; }
	inline float* get_address_of__matrorseSori_0() { return &____matrorseSori_0; }
	inline void set__matrorseSori_0(float value)
	{
		____matrorseSori_0 = value;
	}

	inline static int32_t get_offset_of__sarbemmiKarpere_1() { return static_cast<int32_t>(offsetof(M_fearte56_t2319210322, ____sarbemmiKarpere_1)); }
	inline int32_t get__sarbemmiKarpere_1() const { return ____sarbemmiKarpere_1; }
	inline int32_t* get_address_of__sarbemmiKarpere_1() { return &____sarbemmiKarpere_1; }
	inline void set__sarbemmiKarpere_1(int32_t value)
	{
		____sarbemmiKarpere_1 = value;
	}

	inline static int32_t get_offset_of__gaidertru_2() { return static_cast<int32_t>(offsetof(M_fearte56_t2319210322, ____gaidertru_2)); }
	inline String_t* get__gaidertru_2() const { return ____gaidertru_2; }
	inline String_t** get_address_of__gaidertru_2() { return &____gaidertru_2; }
	inline void set__gaidertru_2(String_t* value)
	{
		____gaidertru_2 = value;
		Il2CppCodeGenWriteBarrier(&____gaidertru_2, value);
	}

	inline static int32_t get_offset_of__xearooSearnor_3() { return static_cast<int32_t>(offsetof(M_fearte56_t2319210322, ____xearooSearnor_3)); }
	inline bool get__xearooSearnor_3() const { return ____xearooSearnor_3; }
	inline bool* get_address_of__xearooSearnor_3() { return &____xearooSearnor_3; }
	inline void set__xearooSearnor_3(bool value)
	{
		____xearooSearnor_3 = value;
	}

	inline static int32_t get_offset_of__serecaili_4() { return static_cast<int32_t>(offsetof(M_fearte56_t2319210322, ____serecaili_4)); }
	inline float get__serecaili_4() const { return ____serecaili_4; }
	inline float* get_address_of__serecaili_4() { return &____serecaili_4; }
	inline void set__serecaili_4(float value)
	{
		____serecaili_4 = value;
	}

	inline static int32_t get_offset_of__xemstirhar_5() { return static_cast<int32_t>(offsetof(M_fearte56_t2319210322, ____xemstirhar_5)); }
	inline int32_t get__xemstirhar_5() const { return ____xemstirhar_5; }
	inline int32_t* get_address_of__xemstirhar_5() { return &____xemstirhar_5; }
	inline void set__xemstirhar_5(int32_t value)
	{
		____xemstirhar_5 = value;
	}

	inline static int32_t get_offset_of__bolalceSallqoobea_6() { return static_cast<int32_t>(offsetof(M_fearte56_t2319210322, ____bolalceSallqoobea_6)); }
	inline int32_t get__bolalceSallqoobea_6() const { return ____bolalceSallqoobea_6; }
	inline int32_t* get_address_of__bolalceSallqoobea_6() { return &____bolalceSallqoobea_6; }
	inline void set__bolalceSallqoobea_6(int32_t value)
	{
		____bolalceSallqoobea_6 = value;
	}

	inline static int32_t get_offset_of__bayseSoujoo_7() { return static_cast<int32_t>(offsetof(M_fearte56_t2319210322, ____bayseSoujoo_7)); }
	inline uint32_t get__bayseSoujoo_7() const { return ____bayseSoujoo_7; }
	inline uint32_t* get_address_of__bayseSoujoo_7() { return &____bayseSoujoo_7; }
	inline void set__bayseSoujoo_7(uint32_t value)
	{
		____bayseSoujoo_7 = value;
	}

	inline static int32_t get_offset_of__pila_8() { return static_cast<int32_t>(offsetof(M_fearte56_t2319210322, ____pila_8)); }
	inline int32_t get__pila_8() const { return ____pila_8; }
	inline int32_t* get_address_of__pila_8() { return &____pila_8; }
	inline void set__pila_8(int32_t value)
	{
		____pila_8 = value;
	}

	inline static int32_t get_offset_of__chaviherLoumeremay_9() { return static_cast<int32_t>(offsetof(M_fearte56_t2319210322, ____chaviherLoumeremay_9)); }
	inline int32_t get__chaviherLoumeremay_9() const { return ____chaviherLoumeremay_9; }
	inline int32_t* get_address_of__chaviherLoumeremay_9() { return &____chaviherLoumeremay_9; }
	inline void set__chaviherLoumeremay_9(int32_t value)
	{
		____chaviherLoumeremay_9 = value;
	}

	inline static int32_t get_offset_of__jelpaWeeraw_10() { return static_cast<int32_t>(offsetof(M_fearte56_t2319210322, ____jelpaWeeraw_10)); }
	inline int32_t get__jelpaWeeraw_10() const { return ____jelpaWeeraw_10; }
	inline int32_t* get_address_of__jelpaWeeraw_10() { return &____jelpaWeeraw_10; }
	inline void set__jelpaWeeraw_10(int32_t value)
	{
		____jelpaWeeraw_10 = value;
	}

	inline static int32_t get_offset_of__dachere_11() { return static_cast<int32_t>(offsetof(M_fearte56_t2319210322, ____dachere_11)); }
	inline int32_t get__dachere_11() const { return ____dachere_11; }
	inline int32_t* get_address_of__dachere_11() { return &____dachere_11; }
	inline void set__dachere_11(int32_t value)
	{
		____dachere_11 = value;
	}

	inline static int32_t get_offset_of__pokasteNetra_12() { return static_cast<int32_t>(offsetof(M_fearte56_t2319210322, ____pokasteNetra_12)); }
	inline bool get__pokasteNetra_12() const { return ____pokasteNetra_12; }
	inline bool* get_address_of__pokasteNetra_12() { return &____pokasteNetra_12; }
	inline void set__pokasteNetra_12(bool value)
	{
		____pokasteNetra_12 = value;
	}

	inline static int32_t get_offset_of__bestipaZearge_13() { return static_cast<int32_t>(offsetof(M_fearte56_t2319210322, ____bestipaZearge_13)); }
	inline String_t* get__bestipaZearge_13() const { return ____bestipaZearge_13; }
	inline String_t** get_address_of__bestipaZearge_13() { return &____bestipaZearge_13; }
	inline void set__bestipaZearge_13(String_t* value)
	{
		____bestipaZearge_13 = value;
		Il2CppCodeGenWriteBarrier(&____bestipaZearge_13, value);
	}

	inline static int32_t get_offset_of__maibelta_14() { return static_cast<int32_t>(offsetof(M_fearte56_t2319210322, ____maibelta_14)); }
	inline String_t* get__maibelta_14() const { return ____maibelta_14; }
	inline String_t** get_address_of__maibelta_14() { return &____maibelta_14; }
	inline void set__maibelta_14(String_t* value)
	{
		____maibelta_14 = value;
		Il2CppCodeGenWriteBarrier(&____maibelta_14, value);
	}

	inline static int32_t get_offset_of__whocisaGisbeber_15() { return static_cast<int32_t>(offsetof(M_fearte56_t2319210322, ____whocisaGisbeber_15)); }
	inline float get__whocisaGisbeber_15() const { return ____whocisaGisbeber_15; }
	inline float* get_address_of__whocisaGisbeber_15() { return &____whocisaGisbeber_15; }
	inline void set__whocisaGisbeber_15(float value)
	{
		____whocisaGisbeber_15 = value;
	}

	inline static int32_t get_offset_of__demouri_16() { return static_cast<int32_t>(offsetof(M_fearte56_t2319210322, ____demouri_16)); }
	inline String_t* get__demouri_16() const { return ____demouri_16; }
	inline String_t** get_address_of__demouri_16() { return &____demouri_16; }
	inline void set__demouri_16(String_t* value)
	{
		____demouri_16 = value;
		Il2CppCodeGenWriteBarrier(&____demouri_16, value);
	}

	inline static int32_t get_offset_of__hasjaPakal_17() { return static_cast<int32_t>(offsetof(M_fearte56_t2319210322, ____hasjaPakal_17)); }
	inline int32_t get__hasjaPakal_17() const { return ____hasjaPakal_17; }
	inline int32_t* get_address_of__hasjaPakal_17() { return &____hasjaPakal_17; }
	inline void set__hasjaPakal_17(int32_t value)
	{
		____hasjaPakal_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
