﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginSjoy
struct PluginSjoy_t1606406260;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// AnQuIOSSDKMsg
struct AnQuIOSSDKMsg_t2914021123;
// System.Object
struct Il2CppObject;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_AnQuIOSSDKMsg2914021123.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginSjoy::.ctor()
extern "C"  void PluginSjoy__ctor_m3759999655 (PluginSjoy_t1606406260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::Init()
extern "C"  void PluginSjoy_Init_m765563597 (PluginSjoy_t1606406260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::OnDestory()
extern "C"  void PluginSjoy_OnDestory_m2942454458 (PluginSjoy_t1606406260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::OpenUserLogin()
extern "C"  void PluginSjoy_OpenUserLogin_m298882553 (PluginSjoy_t1606406260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::OpenUserSwitch()
extern "C"  void PluginSjoy_OpenUserSwitch_m2862408006 (PluginSjoy_t1606406260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginSjoy::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginSjoy_ReqSDKHttpLogin_m3619605180 (PluginSjoy_t1606406260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::UserPay(CEvent.ZEvent)
extern "C"  void PluginSjoy_UserPay_m850789209 (PluginSjoy_t1606406260 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::CreatRole(CEvent.ZEvent)
extern "C"  void PluginSjoy_CreatRole_m315638871 (PluginSjoy_t1606406260 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::RoleEnterGame(CEvent.ZEvent)
extern "C"  void PluginSjoy_RoleEnterGame_m2508080962 (PluginSjoy_t1606406260 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginSjoy_RoleUpgrade_m517024336 (PluginSjoy_t1606406260 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginSjoy::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginSjoy_IsLoginSuccess_m3883861452 (PluginSjoy_t1606406260 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::OnInitSuccess(System.String)
extern "C"  void PluginSjoy_OnInitSuccess_m852982633 (PluginSjoy_t1606406260 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::OnInitFail(System.String)
extern "C"  void PluginSjoy_OnInitFail_m216585848 (PluginSjoy_t1606406260 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::OnLoginSuccess(System.String)
extern "C"  void PluginSjoy_OnLoginSuccess_m1062369900 (PluginSjoy_t1606406260 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::OnLoginFail(System.String)
extern "C"  void PluginSjoy_OnLoginFail_m1049030229 (PluginSjoy_t1606406260 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::OnUserSwitchSuccess(System.String)
extern "C"  void PluginSjoy_OnUserSwitchSuccess_m1324721080 (PluginSjoy_t1606406260 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::OnUserSwitchFail(System.String)
extern "C"  void PluginSjoy_OnUserSwitchFail_m135866505 (PluginSjoy_t1606406260 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::OnPaySuccess(System.String)
extern "C"  void PluginSjoy_OnPaySuccess_m2670495659 (PluginSjoy_t1606406260 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::PlaySuccess(CEvent.ZEvent)
extern "C"  void PluginSjoy_PlaySuccess_m788554183 (PluginSjoy_t1606406260 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::OnPayFail(System.String)
extern "C"  void PluginSjoy_OnPayFail_m2019492214 (PluginSjoy_t1606406260 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::OnExitGameSuccess(System.String)
extern "C"  void PluginSjoy_OnExitGameSuccess_m417612553 (PluginSjoy_t1606406260 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::OnExitGameFail(System.String)
extern "C"  void PluginSjoy_OnExitGameFail_m1664758488 (PluginSjoy_t1606406260 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::OnLogoutSuccess(System.String)
extern "C"  void PluginSjoy_OnLogoutSuccess_m2872238499 (PluginSjoy_t1606406260 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::OnLogoutFail(System.String)
extern "C"  void PluginSjoy_OnLogoutFail_m2268047998 (PluginSjoy_t1606406260 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::<OnLogoutSuccess>m__456()
extern "C"  void PluginSjoy_U3COnLogoutSuccessU3Em__456_m2230595309 (PluginSjoy_t1606406260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::ilo_RemoveEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginSjoy_ilo_RemoveEventListener1_m1708037740 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AnQuIOSSDKMsg PluginSjoy::ilo_Instance2()
extern "C"  AnQuIOSSDKMsg_t2914021123 * PluginSjoy_ilo_Instance2_m2901373567 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::ilo_Log3(System.Object,System.Boolean)
extern "C"  void PluginSjoy_ilo_Log3_m1305696328 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginSjoy::ilo_get_Instance4()
extern "C"  VersionMgr_t1322950208 * PluginSjoy_ilo_get_Instance4_m43414891 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginSjoy::ilo_get_isSdkLogin5(VersionMgr)
extern "C"  bool PluginSjoy_ilo_get_isSdkLogin5_m3093215025 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::ilo_zhiFu6(AnQuIOSSDKMsg,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginSjoy_ilo_zhiFu6_m1524054607 (Il2CppObject * __this /* static, unused */, AnQuIOSSDKMsg_t2914021123 * ____this0, String_t* ___amount1, String_t* ___name2, String_t* ___Level3, String_t* ___ID4, String_t* ___s_id5, String_t* ___s_name6, String_t* ___subject7, String_t* ___order_no8, String_t* ___ext9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::ilo_chuangJieJueSe7(AnQuIOSSDKMsg,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginSjoy_ilo_chuangJieJueSe7_m4288770578 (Il2CppObject * __this /* static, unused */, AnQuIOSSDKMsg_t2914021123 * ____this0, String_t* ___roleID1, String_t* ___roleName2, String_t* ___roleLevel3, String_t* ___serverID4, String_t* ___serverName5, String_t* ___balance6, String_t* ___vip7, String_t* ___partyName8, String_t* ___extra9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::ilo_jinRuFuWuQiEvent8(AnQuIOSSDKMsg,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginSjoy_ilo_jinRuFuWuQiEvent8_m386017764 (Il2CppObject * __this /* static, unused */, AnQuIOSSDKMsg_t2914021123 * ____this0, String_t* ___roleID1, String_t* ___roleName2, String_t* ___roleLevel3, String_t* ___serverID4, String_t* ___serverName5, String_t* ___balance6, String_t* ___vip7, String_t* ___partyName8, String_t* ___extra9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginSjoy::ilo_get_PluginsSdkMgr9()
extern "C"  PluginsSdkMgr_t3884624670 * PluginSjoy_ilo_get_PluginsSdkMgr9_m586770473 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::ilo_DispatchEvent10(CEvent.ZEvent)
extern "C"  void PluginSjoy_ilo_DispatchEvent10_m1169042570 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoy::ilo_Logout11(System.Action)
extern "C"  void PluginSjoy_ilo_Logout11_m2159491299 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
