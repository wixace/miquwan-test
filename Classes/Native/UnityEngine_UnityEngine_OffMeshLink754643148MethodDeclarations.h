﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.OffMeshLink
struct OffMeshLink_t754643148;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"

// System.Void UnityEngine.OffMeshLink::.ctor()
extern "C"  void OffMeshLink__ctor_m4151890435 (OffMeshLink_t754643148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.OffMeshLink::get_activated()
extern "C"  bool OffMeshLink_get_activated_m1191882319 (OffMeshLink_t754643148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.OffMeshLink::set_activated(System.Boolean)
extern "C"  void OffMeshLink_set_activated_m2988954924 (OffMeshLink_t754643148 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.OffMeshLink::get_occupied()
extern "C"  bool OffMeshLink_get_occupied_m2390473922 (OffMeshLink_t754643148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.OffMeshLink::get_costOverride()
extern "C"  float OffMeshLink_get_costOverride_m1162377399 (OffMeshLink_t754643148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.OffMeshLink::set_costOverride(System.Single)
extern "C"  void OffMeshLink_set_costOverride_m378910292 (OffMeshLink_t754643148 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.OffMeshLink::get_biDirectional()
extern "C"  bool OffMeshLink_get_biDirectional_m2008308385 (OffMeshLink_t754643148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.OffMeshLink::set_biDirectional(System.Boolean)
extern "C"  void OffMeshLink_set_biDirectional_m2633254654 (OffMeshLink_t754643148 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.OffMeshLink::UpdatePositions()
extern "C"  void OffMeshLink_UpdatePositions_m1445752962 (OffMeshLink_t754643148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.OffMeshLink::get_area()
extern "C"  int32_t OffMeshLink_get_area_m2946188439 (OffMeshLink_t754643148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.OffMeshLink::set_area(System.Int32)
extern "C"  void OffMeshLink_set_area_m3099873116 (OffMeshLink_t754643148 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.OffMeshLink::get_autoUpdatePositions()
extern "C"  bool OffMeshLink_get_autoUpdatePositions_m3086894064 (OffMeshLink_t754643148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.OffMeshLink::set_autoUpdatePositions(System.Boolean)
extern "C"  void OffMeshLink_set_autoUpdatePositions_m1375184269 (OffMeshLink_t754643148 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.OffMeshLink::get_startTransform()
extern "C"  Transform_t1659122786 * OffMeshLink_get_startTransform_m4080523350 (OffMeshLink_t754643148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.OffMeshLink::set_startTransform(UnityEngine.Transform)
extern "C"  void OffMeshLink_set_startTransform_m2141056149 (OffMeshLink_t754643148 * __this, Transform_t1659122786 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.OffMeshLink::get_endTransform()
extern "C"  Transform_t1659122786 * OffMeshLink_get_endTransform_m689695997 (OffMeshLink_t754643148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.OffMeshLink::set_endTransform(UnityEngine.Transform)
extern "C"  void OffMeshLink_set_endTransform_m1139227086 (OffMeshLink_t754643148 * __this, Transform_t1659122786 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
