﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CaptureScreenshotMgr
struct  CaptureScreenshotMgr_t3951887692  : public Il2CppObject
{
public:
	// System.String CaptureScreenshotMgr::ScreenshotPath
	String_t* ___ScreenshotPath_0;

public:
	inline static int32_t get_offset_of_ScreenshotPath_0() { return static_cast<int32_t>(offsetof(CaptureScreenshotMgr_t3951887692, ___ScreenshotPath_0)); }
	inline String_t* get_ScreenshotPath_0() const { return ___ScreenshotPath_0; }
	inline String_t** get_address_of_ScreenshotPath_0() { return &___ScreenshotPath_0; }
	inline void set_ScreenshotPath_0(String_t* value)
	{
		___ScreenshotPath_0 = value;
		Il2CppCodeGenWriteBarrier(&___ScreenshotPath_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
