﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_veamorna236
struct  M_veamorna236_t2995932484  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_veamorna236::_lallburcho
	bool ____lallburcho_0;
	// System.Single GarbageiOS.M_veamorna236::_nayrorSurmeve
	float ____nayrorSurmeve_1;
	// System.Single GarbageiOS.M_veamorna236::_keretusow
	float ____keretusow_2;
	// System.Int32 GarbageiOS.M_veamorna236::_soranearSarla
	int32_t ____soranearSarla_3;
	// System.String GarbageiOS.M_veamorna236::_deezere
	String_t* ____deezere_4;
	// System.Single GarbageiOS.M_veamorna236::_sitrowte
	float ____sitrowte_5;
	// System.Boolean GarbageiOS.M_veamorna236::_cara
	bool ____cara_6;
	// System.Boolean GarbageiOS.M_veamorna236::_tistiskar
	bool ____tistiskar_7;
	// System.String GarbageiOS.M_veamorna236::_forhiseBatorme
	String_t* ____forhiseBatorme_8;
	// System.String GarbageiOS.M_veamorna236::_kaslayjayQaima
	String_t* ____kaslayjayQaima_9;

public:
	inline static int32_t get_offset_of__lallburcho_0() { return static_cast<int32_t>(offsetof(M_veamorna236_t2995932484, ____lallburcho_0)); }
	inline bool get__lallburcho_0() const { return ____lallburcho_0; }
	inline bool* get_address_of__lallburcho_0() { return &____lallburcho_0; }
	inline void set__lallburcho_0(bool value)
	{
		____lallburcho_0 = value;
	}

	inline static int32_t get_offset_of__nayrorSurmeve_1() { return static_cast<int32_t>(offsetof(M_veamorna236_t2995932484, ____nayrorSurmeve_1)); }
	inline float get__nayrorSurmeve_1() const { return ____nayrorSurmeve_1; }
	inline float* get_address_of__nayrorSurmeve_1() { return &____nayrorSurmeve_1; }
	inline void set__nayrorSurmeve_1(float value)
	{
		____nayrorSurmeve_1 = value;
	}

	inline static int32_t get_offset_of__keretusow_2() { return static_cast<int32_t>(offsetof(M_veamorna236_t2995932484, ____keretusow_2)); }
	inline float get__keretusow_2() const { return ____keretusow_2; }
	inline float* get_address_of__keretusow_2() { return &____keretusow_2; }
	inline void set__keretusow_2(float value)
	{
		____keretusow_2 = value;
	}

	inline static int32_t get_offset_of__soranearSarla_3() { return static_cast<int32_t>(offsetof(M_veamorna236_t2995932484, ____soranearSarla_3)); }
	inline int32_t get__soranearSarla_3() const { return ____soranearSarla_3; }
	inline int32_t* get_address_of__soranearSarla_3() { return &____soranearSarla_3; }
	inline void set__soranearSarla_3(int32_t value)
	{
		____soranearSarla_3 = value;
	}

	inline static int32_t get_offset_of__deezere_4() { return static_cast<int32_t>(offsetof(M_veamorna236_t2995932484, ____deezere_4)); }
	inline String_t* get__deezere_4() const { return ____deezere_4; }
	inline String_t** get_address_of__deezere_4() { return &____deezere_4; }
	inline void set__deezere_4(String_t* value)
	{
		____deezere_4 = value;
		Il2CppCodeGenWriteBarrier(&____deezere_4, value);
	}

	inline static int32_t get_offset_of__sitrowte_5() { return static_cast<int32_t>(offsetof(M_veamorna236_t2995932484, ____sitrowte_5)); }
	inline float get__sitrowte_5() const { return ____sitrowte_5; }
	inline float* get_address_of__sitrowte_5() { return &____sitrowte_5; }
	inline void set__sitrowte_5(float value)
	{
		____sitrowte_5 = value;
	}

	inline static int32_t get_offset_of__cara_6() { return static_cast<int32_t>(offsetof(M_veamorna236_t2995932484, ____cara_6)); }
	inline bool get__cara_6() const { return ____cara_6; }
	inline bool* get_address_of__cara_6() { return &____cara_6; }
	inline void set__cara_6(bool value)
	{
		____cara_6 = value;
	}

	inline static int32_t get_offset_of__tistiskar_7() { return static_cast<int32_t>(offsetof(M_veamorna236_t2995932484, ____tistiskar_7)); }
	inline bool get__tistiskar_7() const { return ____tistiskar_7; }
	inline bool* get_address_of__tistiskar_7() { return &____tistiskar_7; }
	inline void set__tistiskar_7(bool value)
	{
		____tistiskar_7 = value;
	}

	inline static int32_t get_offset_of__forhiseBatorme_8() { return static_cast<int32_t>(offsetof(M_veamorna236_t2995932484, ____forhiseBatorme_8)); }
	inline String_t* get__forhiseBatorme_8() const { return ____forhiseBatorme_8; }
	inline String_t** get_address_of__forhiseBatorme_8() { return &____forhiseBatorme_8; }
	inline void set__forhiseBatorme_8(String_t* value)
	{
		____forhiseBatorme_8 = value;
		Il2CppCodeGenWriteBarrier(&____forhiseBatorme_8, value);
	}

	inline static int32_t get_offset_of__kaslayjayQaima_9() { return static_cast<int32_t>(offsetof(M_veamorna236_t2995932484, ____kaslayjayQaima_9)); }
	inline String_t* get__kaslayjayQaima_9() const { return ____kaslayjayQaima_9; }
	inline String_t** get_address_of__kaslayjayQaima_9() { return &____kaslayjayQaima_9; }
	inline void set__kaslayjayQaima_9(String_t* value)
	{
		____kaslayjayQaima_9 = value;
		Il2CppCodeGenWriteBarrier(&____kaslayjayQaima_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
