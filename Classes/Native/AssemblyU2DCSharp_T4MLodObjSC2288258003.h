﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Renderer
struct Renderer_t3076687687;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// T4MLodObjSC
struct  T4MLodObjSC_t2288258003  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Renderer T4MLodObjSC::LOD1
	Renderer_t3076687687 * ___LOD1_2;
	// UnityEngine.Renderer T4MLodObjSC::LOD2
	Renderer_t3076687687 * ___LOD2_3;
	// UnityEngine.Renderer T4MLodObjSC::LOD3
	Renderer_t3076687687 * ___LOD3_4;
	// System.Single T4MLodObjSC::Interval
	float ___Interval_5;
	// UnityEngine.Transform T4MLodObjSC::PlayerCamera
	Transform_t1659122786 * ___PlayerCamera_6;
	// System.Int32 T4MLodObjSC::Mode
	int32_t ___Mode_7;
	// UnityEngine.Vector3 T4MLodObjSC::OldPlayerPos
	Vector3_t4282066566  ___OldPlayerPos_8;
	// System.Int32 T4MLodObjSC::ObjLodStatus
	int32_t ___ObjLodStatus_9;
	// System.Single T4MLodObjSC::MaxViewDistance
	float ___MaxViewDistance_10;
	// System.Single T4MLodObjSC::LOD2Start
	float ___LOD2Start_11;
	// System.Single T4MLodObjSC::LOD3Start
	float ___LOD3Start_12;

public:
	inline static int32_t get_offset_of_LOD1_2() { return static_cast<int32_t>(offsetof(T4MLodObjSC_t2288258003, ___LOD1_2)); }
	inline Renderer_t3076687687 * get_LOD1_2() const { return ___LOD1_2; }
	inline Renderer_t3076687687 ** get_address_of_LOD1_2() { return &___LOD1_2; }
	inline void set_LOD1_2(Renderer_t3076687687 * value)
	{
		___LOD1_2 = value;
		Il2CppCodeGenWriteBarrier(&___LOD1_2, value);
	}

	inline static int32_t get_offset_of_LOD2_3() { return static_cast<int32_t>(offsetof(T4MLodObjSC_t2288258003, ___LOD2_3)); }
	inline Renderer_t3076687687 * get_LOD2_3() const { return ___LOD2_3; }
	inline Renderer_t3076687687 ** get_address_of_LOD2_3() { return &___LOD2_3; }
	inline void set_LOD2_3(Renderer_t3076687687 * value)
	{
		___LOD2_3 = value;
		Il2CppCodeGenWriteBarrier(&___LOD2_3, value);
	}

	inline static int32_t get_offset_of_LOD3_4() { return static_cast<int32_t>(offsetof(T4MLodObjSC_t2288258003, ___LOD3_4)); }
	inline Renderer_t3076687687 * get_LOD3_4() const { return ___LOD3_4; }
	inline Renderer_t3076687687 ** get_address_of_LOD3_4() { return &___LOD3_4; }
	inline void set_LOD3_4(Renderer_t3076687687 * value)
	{
		___LOD3_4 = value;
		Il2CppCodeGenWriteBarrier(&___LOD3_4, value);
	}

	inline static int32_t get_offset_of_Interval_5() { return static_cast<int32_t>(offsetof(T4MLodObjSC_t2288258003, ___Interval_5)); }
	inline float get_Interval_5() const { return ___Interval_5; }
	inline float* get_address_of_Interval_5() { return &___Interval_5; }
	inline void set_Interval_5(float value)
	{
		___Interval_5 = value;
	}

	inline static int32_t get_offset_of_PlayerCamera_6() { return static_cast<int32_t>(offsetof(T4MLodObjSC_t2288258003, ___PlayerCamera_6)); }
	inline Transform_t1659122786 * get_PlayerCamera_6() const { return ___PlayerCamera_6; }
	inline Transform_t1659122786 ** get_address_of_PlayerCamera_6() { return &___PlayerCamera_6; }
	inline void set_PlayerCamera_6(Transform_t1659122786 * value)
	{
		___PlayerCamera_6 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerCamera_6, value);
	}

	inline static int32_t get_offset_of_Mode_7() { return static_cast<int32_t>(offsetof(T4MLodObjSC_t2288258003, ___Mode_7)); }
	inline int32_t get_Mode_7() const { return ___Mode_7; }
	inline int32_t* get_address_of_Mode_7() { return &___Mode_7; }
	inline void set_Mode_7(int32_t value)
	{
		___Mode_7 = value;
	}

	inline static int32_t get_offset_of_OldPlayerPos_8() { return static_cast<int32_t>(offsetof(T4MLodObjSC_t2288258003, ___OldPlayerPos_8)); }
	inline Vector3_t4282066566  get_OldPlayerPos_8() const { return ___OldPlayerPos_8; }
	inline Vector3_t4282066566 * get_address_of_OldPlayerPos_8() { return &___OldPlayerPos_8; }
	inline void set_OldPlayerPos_8(Vector3_t4282066566  value)
	{
		___OldPlayerPos_8 = value;
	}

	inline static int32_t get_offset_of_ObjLodStatus_9() { return static_cast<int32_t>(offsetof(T4MLodObjSC_t2288258003, ___ObjLodStatus_9)); }
	inline int32_t get_ObjLodStatus_9() const { return ___ObjLodStatus_9; }
	inline int32_t* get_address_of_ObjLodStatus_9() { return &___ObjLodStatus_9; }
	inline void set_ObjLodStatus_9(int32_t value)
	{
		___ObjLodStatus_9 = value;
	}

	inline static int32_t get_offset_of_MaxViewDistance_10() { return static_cast<int32_t>(offsetof(T4MLodObjSC_t2288258003, ___MaxViewDistance_10)); }
	inline float get_MaxViewDistance_10() const { return ___MaxViewDistance_10; }
	inline float* get_address_of_MaxViewDistance_10() { return &___MaxViewDistance_10; }
	inline void set_MaxViewDistance_10(float value)
	{
		___MaxViewDistance_10 = value;
	}

	inline static int32_t get_offset_of_LOD2Start_11() { return static_cast<int32_t>(offsetof(T4MLodObjSC_t2288258003, ___LOD2Start_11)); }
	inline float get_LOD2Start_11() const { return ___LOD2Start_11; }
	inline float* get_address_of_LOD2Start_11() { return &___LOD2Start_11; }
	inline void set_LOD2Start_11(float value)
	{
		___LOD2Start_11 = value;
	}

	inline static int32_t get_offset_of_LOD3Start_12() { return static_cast<int32_t>(offsetof(T4MLodObjSC_t2288258003, ___LOD3Start_12)); }
	inline float get_LOD3Start_12() const { return ___LOD3Start_12; }
	inline float* get_address_of_LOD3Start_12() { return &___LOD3Start_12; }
	inline void set_LOD3Start_12(float value)
	{
		___LOD3Start_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
