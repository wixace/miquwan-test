﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShortcutsHelper
struct ShortcutsHelper_t3314236123;

#include "codegen/il2cpp-codegen.h"

// System.Void ShortcutsHelper::.ctor()
extern "C"  void ShortcutsHelper__ctor_m3077673712 (ShortcutsHelper_t3314236123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ShortcutsHelper ShortcutsHelper::get_instance()
extern "C"  ShortcutsHelper_t3314236123 * ShortcutsHelper_get_instance_m1005230970 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShortcutsHelper::UpdateInputMessage()
extern "C"  void ShortcutsHelper_UpdateInputMessage_m261334490 (ShortcutsHelper_t3314236123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
