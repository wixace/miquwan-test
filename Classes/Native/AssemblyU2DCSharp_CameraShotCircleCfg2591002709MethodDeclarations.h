﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraShotCircleCfg
struct CameraShotCircleCfg_t2591002709;
// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "codegen/il2cpp-codegen.h"

// System.Void CameraShotCircleCfg::.ctor()
extern "C"  void CameraShotCircleCfg__ctor_m3230864054 (CameraShotCircleCfg_t2591002709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension CameraShotCircleCfg::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * CameraShotCircleCfg_ProtoBuf_IExtensible_GetExtensionObject_m2427853626 (CameraShotCircleCfg_t2591002709 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotCircleCfg::get_isCircle()
extern "C"  int32_t CameraShotCircleCfg_get_isCircle_m2003344003 (CameraShotCircleCfg_t2591002709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotCircleCfg::set_isCircle(System.Int32)
extern "C"  void CameraShotCircleCfg_set_isCircle_m250220694 (CameraShotCircleCfg_t2591002709 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotCircleCfg::get_heroOrNpcId()
extern "C"  int32_t CameraShotCircleCfg_get_heroOrNpcId_m524311864 (CameraShotCircleCfg_t2591002709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotCircleCfg::set_heroOrNpcId(System.Int32)
extern "C"  void CameraShotCircleCfg_set_heroOrNpcId_m650146311 (CameraShotCircleCfg_t2591002709 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotCircleCfg::get_isHero()
extern "C"  int32_t CameraShotCircleCfg_get_isHero_m3162771373 (CameraShotCircleCfg_t2591002709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotCircleCfg::set_isHero(System.Int32)
extern "C"  void CameraShotCircleCfg_set_isHero_m2329640768 (CameraShotCircleCfg_t2591002709 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotCircleCfg::get_posX()
extern "C"  float CameraShotCircleCfg_get_posX_m3991393135 (CameraShotCircleCfg_t2591002709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotCircleCfg::set_posX(System.Single)
extern "C"  void CameraShotCircleCfg_set_posX_m3587149532 (CameraShotCircleCfg_t2591002709 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotCircleCfg::get_posY()
extern "C"  float CameraShotCircleCfg_get_posY_m3991394096 (CameraShotCircleCfg_t2591002709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotCircleCfg::set_posY(System.Single)
extern "C"  void CameraShotCircleCfg_set_posY_m3076615355 (CameraShotCircleCfg_t2591002709 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotCircleCfg::get_posZ()
extern "C"  float CameraShotCircleCfg_get_posZ_m3991395057 (CameraShotCircleCfg_t2591002709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotCircleCfg::set_posZ(System.Single)
extern "C"  void CameraShotCircleCfg_set_posZ_m2566081178 (CameraShotCircleCfg_t2591002709 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotCircleCfg::get_speed()
extern "C"  float CameraShotCircleCfg_get_speed_m1857791454 (CameraShotCircleCfg_t2591002709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotCircleCfg::set_speed(System.Single)
extern "C"  void CameraShotCircleCfg_set_speed_m2358757453 (CameraShotCircleCfg_t2591002709 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotCircleCfg::get_distance()
extern "C"  float CameraShotCircleCfg_get_distance_m2269391264 (CameraShotCircleCfg_t2591002709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotCircleCfg::set_distance(System.Single)
extern "C"  void CameraShotCircleCfg_set_distance_m1727950923 (CameraShotCircleCfg_t2591002709 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotCircleCfg::get_high()
extern "C"  float CameraShotCircleCfg_get_high_m3756476685 (CameraShotCircleCfg_t2591002709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotCircleCfg::set_high(System.Single)
extern "C"  void CameraShotCircleCfg_set_high_m507030014 (CameraShotCircleCfg_t2591002709 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotCircleCfg::get_angle()
extern "C"  float CameraShotCircleCfg_get_angle_m3007392618 (CameraShotCircleCfg_t2591002709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotCircleCfg::set_angle(System.Single)
extern "C"  void CameraShotCircleCfg_set_angle_m1195157057 (CameraShotCircleCfg_t2591002709 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension CameraShotCircleCfg::ilo_GetExtensionObject1(ProtoBuf.IExtension&,System.Boolean)
extern "C"  Il2CppObject * CameraShotCircleCfg_ilo_GetExtensionObject1_m3980903430 (Il2CppObject * __this /* static, unused */, Il2CppObject ** ___extensionObject0, bool ___createIfMissing1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
