﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_kowstere115
struct M_kowstere115_t4149212465;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_kowstere1154149212465.h"

// System.Void GarbageiOS.M_kowstere115::.ctor()
extern "C"  void M_kowstere115__ctor_m4088188242 (M_kowstere115_t4149212465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kowstere115::M_dortawmi0(System.String[],System.Int32)
extern "C"  void M_kowstere115_M_dortawmi0_m3453261574 (M_kowstere115_t4149212465 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kowstere115::M_storsarquNurfee1(System.String[],System.Int32)
extern "C"  void M_kowstere115_M_storsarquNurfee1_m1789652493 (M_kowstere115_t4149212465 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kowstere115::M_bosu2(System.String[],System.Int32)
extern "C"  void M_kowstere115_M_bosu2_m1045195384 (M_kowstere115_t4149212465 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kowstere115::M_selgu3(System.String[],System.Int32)
extern "C"  void M_kowstere115_M_selgu3_m1547075366 (M_kowstere115_t4149212465 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kowstere115::M_teheGaytis4(System.String[],System.Int32)
extern "C"  void M_kowstere115_M_teheGaytis4_m3835166396 (M_kowstere115_t4149212465 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kowstere115::M_trawlemjis5(System.String[],System.Int32)
extern "C"  void M_kowstere115_M_trawlemjis5_m832966550 (M_kowstere115_t4149212465 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kowstere115::M_qetea6(System.String[],System.Int32)
extern "C"  void M_kowstere115_M_qetea6_m4227601077 (M_kowstere115_t4149212465 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kowstere115::M_zartay7(System.String[],System.Int32)
extern "C"  void M_kowstere115_M_zartay7_m690155531 (M_kowstere115_t4149212465 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kowstere115::M_licarJallbu8(System.String[],System.Int32)
extern "C"  void M_kowstere115_M_licarJallbu8_m2774174258 (M_kowstere115_t4149212465 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kowstere115::M_zertotowDaderzur9(System.String[],System.Int32)
extern "C"  void M_kowstere115_M_zertotowDaderzur9_m1258516465 (M_kowstere115_t4149212465 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kowstere115::ilo_M_dortawmi01(GarbageiOS.M_kowstere115,System.String[],System.Int32)
extern "C"  void M_kowstere115_ilo_M_dortawmi01_m3901817327 (Il2CppObject * __this /* static, unused */, M_kowstere115_t4149212465 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kowstere115::ilo_M_bosu22(GarbageiOS.M_kowstere115,System.String[],System.Int32)
extern "C"  void M_kowstere115_ilo_M_bosu22_m2694224960 (Il2CppObject * __this /* static, unused */, M_kowstere115_t4149212465 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kowstere115::ilo_M_teheGaytis43(GarbageiOS.M_kowstere115,System.String[],System.Int32)
extern "C"  void M_kowstere115_ilo_M_teheGaytis43_m1703121059 (Il2CppObject * __this /* static, unused */, M_kowstere115_t4149212465 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kowstere115::ilo_M_zartay74(GarbageiOS.M_kowstere115,System.String[],System.Int32)
extern "C"  void M_kowstere115_ilo_M_zartay74_m214425233 (Il2CppObject * __this /* static, unused */, M_kowstere115_t4149212465 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kowstere115::ilo_M_licarJallbu85(GarbageiOS.M_kowstere115,System.String[],System.Int32)
extern "C"  void M_kowstere115_ilo_M_licarJallbu85_m65944477 (Il2CppObject * __this /* static, unused */, M_kowstere115_t4149212465 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
