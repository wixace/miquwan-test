﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3143023619MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m3531830086(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t1076105873 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m3583267041_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m3538482870(__this, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Transform_1_t1076105873 *, int32_t, List_1_t3730483581 *, const MethodInfo*))Transform_1_Invoke_m239587255_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m161636117(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t1076105873 *, int32_t, List_1_t3730483581 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m571597154_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m66550804(__this, ___result0, method) ((  DictionaryEntry_t1751606614  (*) (Transform_1_t1076105873 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m1052289139_gshared)(__this, ___result0, method)
