﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._9d02bd4963f1f41716c5233c595c1b36
struct _9d02bd4963f1f41716c5233c595c1b36_t2928460535;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._9d02bd4963f1f41716c5233c595c1b36::.ctor()
extern "C"  void _9d02bd4963f1f41716c5233c595c1b36__ctor_m88918294 (_9d02bd4963f1f41716c5233c595c1b36_t2928460535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._9d02bd4963f1f41716c5233c595c1b36::_9d02bd4963f1f41716c5233c595c1b36m2(System.Int32)
extern "C"  int32_t _9d02bd4963f1f41716c5233c595c1b36__9d02bd4963f1f41716c5233c595c1b36m2_m2887382585 (_9d02bd4963f1f41716c5233c595c1b36_t2928460535 * __this, int32_t ____9d02bd4963f1f41716c5233c595c1b36a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._9d02bd4963f1f41716c5233c595c1b36::_9d02bd4963f1f41716c5233c595c1b36m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _9d02bd4963f1f41716c5233c595c1b36__9d02bd4963f1f41716c5233c595c1b36m_m1967596701 (_9d02bd4963f1f41716c5233c595c1b36_t2928460535 * __this, int32_t ____9d02bd4963f1f41716c5233c595c1b36a0, int32_t ____9d02bd4963f1f41716c5233c595c1b36201, int32_t ____9d02bd4963f1f41716c5233c595c1b36c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
