﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraShotNodeCfg
struct  CameraShotNodeCfg_t3577740835  : public Il2CppObject
{
public:
	// System.Single CameraShotNodeCfg::_posX
	float ____posX_0;
	// System.Single CameraShotNodeCfg::_posY
	float ____posY_1;
	// System.Single CameraShotNodeCfg::_posZ
	float ____posZ_2;
	// System.Single CameraShotNodeCfg::_rotX
	float ____rotX_3;
	// System.Single CameraShotNodeCfg::_rotY
	float ____rotY_4;
	// System.Single CameraShotNodeCfg::_rotZ
	float ____rotZ_5;
	// System.Single CameraShotNodeCfg::_speed
	float ____speed_6;
	// System.Int32 CameraShotNodeCfg::_focusId
	int32_t ____focusId_7;
	// System.Single CameraShotNodeCfg::_smooth
	float ____smooth_8;
	// System.Single CameraShotNodeCfg::_view
	float ____view_9;
	// System.Single CameraShotNodeCfg::_bright
	float ____bright_10;
	// System.Single CameraShotNodeCfg::_orthographic
	float ____orthographic_11;
	// System.Int32 CameraShotNodeCfg::_isHero
	int32_t ____isHero_12;
	// ProtoBuf.IExtension CameraShotNodeCfg::extensionObject
	Il2CppObject * ___extensionObject_13;

public:
	inline static int32_t get_offset_of__posX_0() { return static_cast<int32_t>(offsetof(CameraShotNodeCfg_t3577740835, ____posX_0)); }
	inline float get__posX_0() const { return ____posX_0; }
	inline float* get_address_of__posX_0() { return &____posX_0; }
	inline void set__posX_0(float value)
	{
		____posX_0 = value;
	}

	inline static int32_t get_offset_of__posY_1() { return static_cast<int32_t>(offsetof(CameraShotNodeCfg_t3577740835, ____posY_1)); }
	inline float get__posY_1() const { return ____posY_1; }
	inline float* get_address_of__posY_1() { return &____posY_1; }
	inline void set__posY_1(float value)
	{
		____posY_1 = value;
	}

	inline static int32_t get_offset_of__posZ_2() { return static_cast<int32_t>(offsetof(CameraShotNodeCfg_t3577740835, ____posZ_2)); }
	inline float get__posZ_2() const { return ____posZ_2; }
	inline float* get_address_of__posZ_2() { return &____posZ_2; }
	inline void set__posZ_2(float value)
	{
		____posZ_2 = value;
	}

	inline static int32_t get_offset_of__rotX_3() { return static_cast<int32_t>(offsetof(CameraShotNodeCfg_t3577740835, ____rotX_3)); }
	inline float get__rotX_3() const { return ____rotX_3; }
	inline float* get_address_of__rotX_3() { return &____rotX_3; }
	inline void set__rotX_3(float value)
	{
		____rotX_3 = value;
	}

	inline static int32_t get_offset_of__rotY_4() { return static_cast<int32_t>(offsetof(CameraShotNodeCfg_t3577740835, ____rotY_4)); }
	inline float get__rotY_4() const { return ____rotY_4; }
	inline float* get_address_of__rotY_4() { return &____rotY_4; }
	inline void set__rotY_4(float value)
	{
		____rotY_4 = value;
	}

	inline static int32_t get_offset_of__rotZ_5() { return static_cast<int32_t>(offsetof(CameraShotNodeCfg_t3577740835, ____rotZ_5)); }
	inline float get__rotZ_5() const { return ____rotZ_5; }
	inline float* get_address_of__rotZ_5() { return &____rotZ_5; }
	inline void set__rotZ_5(float value)
	{
		____rotZ_5 = value;
	}

	inline static int32_t get_offset_of__speed_6() { return static_cast<int32_t>(offsetof(CameraShotNodeCfg_t3577740835, ____speed_6)); }
	inline float get__speed_6() const { return ____speed_6; }
	inline float* get_address_of__speed_6() { return &____speed_6; }
	inline void set__speed_6(float value)
	{
		____speed_6 = value;
	}

	inline static int32_t get_offset_of__focusId_7() { return static_cast<int32_t>(offsetof(CameraShotNodeCfg_t3577740835, ____focusId_7)); }
	inline int32_t get__focusId_7() const { return ____focusId_7; }
	inline int32_t* get_address_of__focusId_7() { return &____focusId_7; }
	inline void set__focusId_7(int32_t value)
	{
		____focusId_7 = value;
	}

	inline static int32_t get_offset_of__smooth_8() { return static_cast<int32_t>(offsetof(CameraShotNodeCfg_t3577740835, ____smooth_8)); }
	inline float get__smooth_8() const { return ____smooth_8; }
	inline float* get_address_of__smooth_8() { return &____smooth_8; }
	inline void set__smooth_8(float value)
	{
		____smooth_8 = value;
	}

	inline static int32_t get_offset_of__view_9() { return static_cast<int32_t>(offsetof(CameraShotNodeCfg_t3577740835, ____view_9)); }
	inline float get__view_9() const { return ____view_9; }
	inline float* get_address_of__view_9() { return &____view_9; }
	inline void set__view_9(float value)
	{
		____view_9 = value;
	}

	inline static int32_t get_offset_of__bright_10() { return static_cast<int32_t>(offsetof(CameraShotNodeCfg_t3577740835, ____bright_10)); }
	inline float get__bright_10() const { return ____bright_10; }
	inline float* get_address_of__bright_10() { return &____bright_10; }
	inline void set__bright_10(float value)
	{
		____bright_10 = value;
	}

	inline static int32_t get_offset_of__orthographic_11() { return static_cast<int32_t>(offsetof(CameraShotNodeCfg_t3577740835, ____orthographic_11)); }
	inline float get__orthographic_11() const { return ____orthographic_11; }
	inline float* get_address_of__orthographic_11() { return &____orthographic_11; }
	inline void set__orthographic_11(float value)
	{
		____orthographic_11 = value;
	}

	inline static int32_t get_offset_of__isHero_12() { return static_cast<int32_t>(offsetof(CameraShotNodeCfg_t3577740835, ____isHero_12)); }
	inline int32_t get__isHero_12() const { return ____isHero_12; }
	inline int32_t* get_address_of__isHero_12() { return &____isHero_12; }
	inline void set__isHero_12(int32_t value)
	{
		____isHero_12 = value;
	}

	inline static int32_t get_offset_of_extensionObject_13() { return static_cast<int32_t>(offsetof(CameraShotNodeCfg_t3577740835, ___extensionObject_13)); }
	inline Il2CppObject * get_extensionObject_13() const { return ___extensionObject_13; }
	inline Il2CppObject ** get_address_of_extensionObject_13() { return &___extensionObject_13; }
	inline void set_extensionObject_13(Il2CppObject * value)
	{
		___extensionObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___extensionObject_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
