﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_tawgiNarsowgas14
struct M_tawgiNarsowgas14_t1526844356;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_tawgiNarsowgas141526844356.h"

// System.Void GarbageiOS.M_tawgiNarsowgas14::.ctor()
extern "C"  void M_tawgiNarsowgas14__ctor_m647081711 (M_tawgiNarsowgas14_t1526844356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_tawgiNarsowgas14::M_pourefi0(System.String[],System.Int32)
extern "C"  void M_tawgiNarsowgas14_M_pourefi0_m4028872802 (M_tawgiNarsowgas14_t1526844356 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_tawgiNarsowgas14::M_jineresur1(System.String[],System.Int32)
extern "C"  void M_tawgiNarsowgas14_M_jineresur1_m4260181896 (M_tawgiNarsowgas14_t1526844356 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_tawgiNarsowgas14::M_triwemsi2(System.String[],System.Int32)
extern "C"  void M_tawgiNarsowgas14_M_triwemsi2_m2417140538 (M_tawgiNarsowgas14_t1526844356 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_tawgiNarsowgas14::ilo_M_jineresur11(GarbageiOS.M_tawgiNarsowgas14,System.String[],System.Int32)
extern "C"  void M_tawgiNarsowgas14_ilo_M_jineresur11_m1233903638 (Il2CppObject * __this /* static, unused */, M_tawgiNarsowgas14_t1526844356 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
