﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>
struct Dictionary_2_t2163003329;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t666883479;
// System.Collections.Generic.IDictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>
struct IDictionary_2_t1740876674;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t770439062;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>
struct ICollection_1_t887554120;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>[]
struct KeyValuePair_2U5BU5D_t1578592562;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>>
struct IEnumerator_1_t3973649084;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>
struct KeyCollection_t3789762780;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>
struct ValueCollection_t863609042;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22061784035.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24287931429.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3480326721.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::.ctor()
extern "C"  void Dictionary_2__ctor_m2862692620_gshared (Dictionary_2_t2163003329 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m2862692620(__this, method) ((  void (*) (Dictionary_2_t2163003329 *, const MethodInfo*))Dictionary_2__ctor_m2862692620_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3516539267_gshared (Dictionary_2_t2163003329 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3516539267(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t2163003329 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3516539267_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m1654260748_gshared (Dictionary_2_t2163003329 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m1654260748(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t2163003329 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1654260748_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m843431005_gshared (Dictionary_2_t2163003329 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m843431005(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t2163003329 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m843431005_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1926654833_gshared (Dictionary_2_t2163003329 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1926654833(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t2163003329 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1926654833_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2831798349_gshared (Dictionary_2_t2163003329 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m2831798349(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2163003329 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m2831798349_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m624942018_gshared (Dictionary_2_t2163003329 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m624942018(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2163003329 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m624942018_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1582323842_gshared (Dictionary_2_t2163003329 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1582323842(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2163003329 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1582323842_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m4277975506_gshared (Dictionary_2_t2163003329 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m4277975506(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2163003329 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m4277975506_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m3218313792_gshared (Dictionary_2_t2163003329 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m3218313792(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2163003329 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m3218313792_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1064049365_gshared (Dictionary_2_t2163003329 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1064049365(__this, method) ((  bool (*) (Dictionary_2_t2163003329 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1064049365_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2289448324_gshared (Dictionary_2_t2163003329 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2289448324(__this, method) ((  bool (*) (Dictionary_2_t2163003329 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2289448324_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m3755117820_gshared (Dictionary_2_t2163003329 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m3755117820(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2163003329 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m3755117820_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m3549854625_gshared (Dictionary_2_t2163003329 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m3549854625(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2163003329 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3549854625_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3343335952_gshared (Dictionary_2_t2163003329 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m3343335952(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2163003329 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3343335952_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m1961084646_gshared (Dictionary_2_t2163003329 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1961084646(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2163003329 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1961084646_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m2261669855_gshared (Dictionary_2_t2163003329 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m2261669855(__this, ___key0, method) ((  void (*) (Dictionary_2_t2163003329 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2261669855_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1747960366_gshared (Dictionary_2_t2163003329 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1747960366(__this, method) ((  bool (*) (Dictionary_2_t2163003329 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1747960366_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2727620378_gshared (Dictionary_2_t2163003329 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2727620378(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2163003329 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2727620378_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2351432946_gshared (Dictionary_2_t2163003329 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2351432946(__this, method) ((  bool (*) (Dictionary_2_t2163003329 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2351432946_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3814279413_gshared (Dictionary_2_t2163003329 * __this, KeyValuePair_2_t2061784035  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3814279413(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t2163003329 *, KeyValuePair_2_t2061784035 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3814279413_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m373201261_gshared (Dictionary_2_t2163003329 * __this, KeyValuePair_2_t2061784035  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m373201261(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2163003329 *, KeyValuePair_2_t2061784035 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m373201261_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2881734041_gshared (Dictionary_2_t2163003329 * __this, KeyValuePair_2U5BU5D_t1578592562* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2881734041(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2163003329 *, KeyValuePair_2U5BU5D_t1578592562*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2881734041_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2307798994_gshared (Dictionary_2_t2163003329 * __this, KeyValuePair_2_t2061784035  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2307798994(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2163003329 *, KeyValuePair_2_t2061784035 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2307798994_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m1359944120_gshared (Dictionary_2_t2163003329 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1359944120(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2163003329 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1359944120_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1943735219_gshared (Dictionary_2_t2163003329 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1943735219(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2163003329 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1943735219_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m666446960_gshared (Dictionary_2_t2163003329 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m666446960(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2163003329 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m666446960_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m293374283_gshared (Dictionary_2_t2163003329 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m293374283(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2163003329 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m293374283_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m987274868_gshared (Dictionary_2_t2163003329 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m987274868(__this, method) ((  int32_t (*) (Dictionary_2_t2163003329 *, const MethodInfo*))Dictionary_2_get_Count_m987274868_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_Item(TKey)
extern "C"  KeyValuePair_2_t4287931429  Dictionary_2_get_Item_m3263083383_gshared (Dictionary_2_t2163003329 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m3263083383(__this, ___key0, method) ((  KeyValuePair_2_t4287931429  (*) (Dictionary_2_t2163003329 *, Il2CppObject *, const MethodInfo*))Dictionary_2_get_Item_m3263083383_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m3989219788_gshared (Dictionary_2_t2163003329 * __this, Il2CppObject * ___key0, KeyValuePair_2_t4287931429  ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m3989219788(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2163003329 *, Il2CppObject *, KeyValuePair_2_t4287931429 , const MethodInfo*))Dictionary_2_set_Item_m3989219788_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m3448221828_gshared (Dictionary_2_t2163003329 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m3448221828(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t2163003329 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3448221828_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m69770163_gshared (Dictionary_2_t2163003329 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m69770163(__this, ___size0, method) ((  void (*) (Dictionary_2_t2163003329 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m69770163_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m392921711_gshared (Dictionary_2_t2163003329 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m392921711(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2163003329 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m392921711_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t2061784035  Dictionary_2_make_pair_m1040944059_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, KeyValuePair_2_t4287931429  ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m1040944059(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t2061784035  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, KeyValuePair_2_t4287931429 , const MethodInfo*))Dictionary_2_make_pair_m1040944059_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::pick_key(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_key_m2628387035_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, KeyValuePair_2_t4287931429  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m2628387035(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, KeyValuePair_2_t4287931429 , const MethodInfo*))Dictionary_2_pick_key_m2628387035_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::pick_value(TKey,TValue)
extern "C"  KeyValuePair_2_t4287931429  Dictionary_2_pick_value_m726936283_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, KeyValuePair_2_t4287931429  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m726936283(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t4287931429  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, KeyValuePair_2_t4287931429 , const MethodInfo*))Dictionary_2_pick_value_m726936283_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m2858123904_gshared (Dictionary_2_t2163003329 * __this, KeyValuePair_2U5BU5D_t1578592562* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m2858123904(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2163003329 *, KeyValuePair_2U5BU5D_t1578592562*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m2858123904_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::Resize()
extern "C"  void Dictionary_2_Resize_m2901921964_gshared (Dictionary_2_t2163003329 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m2901921964(__this, method) ((  void (*) (Dictionary_2_t2163003329 *, const MethodInfo*))Dictionary_2_Resize_m2901921964_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m4096223529_gshared (Dictionary_2_t2163003329 * __this, Il2CppObject * ___key0, KeyValuePair_2_t4287931429  ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m4096223529(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2163003329 *, Il2CppObject *, KeyValuePair_2_t4287931429 , const MethodInfo*))Dictionary_2_Add_m4096223529_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::Clear()
extern "C"  void Dictionary_2_Clear_m268825911_gshared (Dictionary_2_t2163003329 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m268825911(__this, method) ((  void (*) (Dictionary_2_t2163003329 *, const MethodInfo*))Dictionary_2_Clear_m268825911_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m1047265117_gshared (Dictionary_2_t2163003329 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1047265117(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2163003329 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsKey_m1047265117_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m332291293_gshared (Dictionary_2_t2163003329 * __this, KeyValuePair_2_t4287931429  ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m332291293(__this, ___value0, method) ((  bool (*) (Dictionary_2_t2163003329 *, KeyValuePair_2_t4287931429 , const MethodInfo*))Dictionary_2_ContainsValue_m332291293_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m2194700970_gshared (Dictionary_2_t2163003329 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m2194700970(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2163003329 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m2194700970_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m4063538682_gshared (Dictionary_2_t2163003329 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m4063538682(__this, ___sender0, method) ((  void (*) (Dictionary_2_t2163003329 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m4063538682_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m2430554995_gshared (Dictionary_2_t2163003329 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m2430554995(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2163003329 *, Il2CppObject *, const MethodInfo*))Dictionary_2_Remove_m2430554995_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m3127225014_gshared (Dictionary_2_t2163003329 * __this, Il2CppObject * ___key0, KeyValuePair_2_t4287931429 * ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m3127225014(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t2163003329 *, Il2CppObject *, KeyValuePair_2_t4287931429 *, const MethodInfo*))Dictionary_2_TryGetValue_m3127225014_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_Keys()
extern "C"  KeyCollection_t3789762780 * Dictionary_2_get_Keys_m4212433033_gshared (Dictionary_2_t2163003329 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m4212433033(__this, method) ((  KeyCollection_t3789762780 * (*) (Dictionary_2_t2163003329 *, const MethodInfo*))Dictionary_2_get_Keys_m4212433033_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_Values()
extern "C"  ValueCollection_t863609042 * Dictionary_2_get_Values_m953093321_gshared (Dictionary_2_t2163003329 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m953093321(__this, method) ((  ValueCollection_t863609042 * (*) (Dictionary_2_t2163003329 *, const MethodInfo*))Dictionary_2_get_Values_m953093321_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::ToTKey(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTKey_m2078245942_gshared (Dictionary_2_t2163003329 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m2078245942(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2163003329 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m2078245942_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::ToTValue(System.Object)
extern "C"  KeyValuePair_2_t4287931429  Dictionary_2_ToTValue_m3334217718_gshared (Dictionary_2_t2163003329 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m3334217718(__this, ___value0, method) ((  KeyValuePair_2_t4287931429  (*) (Dictionary_2_t2163003329 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m3334217718_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m1542154582_gshared (Dictionary_2_t2163003329 * __this, KeyValuePair_2_t2061784035  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1542154582(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t2163003329 *, KeyValuePair_2_t2061784035 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1542154582_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::GetEnumerator()
extern "C"  Enumerator_t3480326721  Dictionary_2_GetEnumerator_m1490598417_gshared (Dictionary_2_t2163003329 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1490598417(__this, method) ((  Enumerator_t3480326721  (*) (Dictionary_2_t2163003329 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1490598417_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m1150057760_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, KeyValuePair_2_t4287931429  ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m1150057760(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, KeyValuePair_2_t4287931429 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1150057760_gshared)(__this /* static, unused */, ___key0, ___value1, method)
