﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3476030434MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::.ctor()
#define Dictionary_2__ctor_m790981268(__this, method) ((  void (*) (Dictionary_2_t2126409608 *, const MethodInfo*))Dictionary_2__ctor_m1163564655_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m3039569620(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t2126409608 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2718016358_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m4048070875(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t2126409608 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2669854345_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::.ctor(System.Int32)
#define Dictionary_2__ctor_m2472289966(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t2126409608 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m1434918208_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m1849797122(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t2126409608 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3032139028_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m2984276126(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2126409608 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m769815344_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2059657755(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2126409608 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m443499593_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1756865335(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2126409608 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3407032805_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m2459544795(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2126409608 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m4073818605_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionary.get_Values()
#define Dictionary_2_System_Collections_IDictionary_get_Values_m3758089993(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2126409608 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m297060251_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionary.get_IsFixedSize()
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m787102816(__this, method) ((  bool (*) (Dictionary_2_t2126409608 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m610592526_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionary.get_IsReadOnly()
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2973251225(__this, method) ((  bool (*) (Dictionary_2_t2126409608 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1166442027_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m2367254593(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2126409608 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m3605586031_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m2394349936(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2126409608 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m640641310_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m3703846625(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2126409608 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m847583859_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m3028767793(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2126409608 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m4267099231_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m2663005102(__this, ___key0, method) ((  void (*) (Dictionary_2_t2126409608 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2892226396_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1865494723(__this, method) ((  bool (*) (Dictionary_2_t2126409608 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m487415637_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1129617973(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2126409608 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3679690311_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2008293191(__this, method) ((  bool (*) (Dictionary_2_t2126409608 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m218731353_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3453598020(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t2126409608 *, KeyValuePair_2_t2025190314 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3921882610_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m203347394(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2126409608 *, KeyValuePair_2_t2025190314 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m350821844_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m4170997800(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2126409608 *, KeyValuePair_2U5BU5D_t4091348591*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1247757654_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2307622247(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2126409608 *, KeyValuePair_2_t2025190314 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2920065529_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3945840391(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2126409608 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1301040565_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m188600662(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2126409608 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1931431812_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3389767629(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2126409608 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2500755835_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1039845274(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2126409608 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2782676424_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::get_Count()
#define Dictionary_2_get_Count_m1765050749(__this, method) ((  int32_t (*) (Dictionary_2_t2126409608 *, const MethodInfo*))Dictionary_2_get_Count_m780153359_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::get_Item(TKey)
#define Dictionary_2_get_Item_m1228153104(__this, ___key0, method) ((  ProductInfo_t1305991238  (*) (Dictionary_2_t2126409608 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m2796012696_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m1659618269(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2126409608 *, String_t*, ProductInfo_t1305991238 , const MethodInfo*))Dictionary_2_set_Item_m1177771503_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m3970909269(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t2126409608 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3219420647_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m1287453698(__this, ___size0, method) ((  void (*) (Dictionary_2_t2126409608 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m371151024_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m3482168446(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2126409608 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2540648620_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m2149743122(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t2025190314  (*) (Il2CppObject * /* static, unused */, String_t*, ProductInfo_t1305991238 , const MethodInfo*))Dictionary_2_make_pair_m97395264_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m120677228(__this /* static, unused */, ___key0, ___value1, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, String_t*, ProductInfo_t1305991238 , const MethodInfo*))Dictionary_2_pick_key_m3933797758_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m326922568(__this /* static, unused */, ___key0, ___value1, method) ((  ProductInfo_t1305991238  (*) (Il2CppObject * /* static, unused */, String_t*, ProductInfo_t1305991238 , const MethodInfo*))Dictionary_2_pick_value_m1128648410_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m4097783185(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2126409608 *, KeyValuePair_2U5BU5D_t4091348591*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m140079523_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::Resize()
#define Dictionary_2_Resize_m2325524219(__this, method) ((  void (*) (Dictionary_2_t2126409608 *, const MethodInfo*))Dictionary_2_Resize_m1768562601_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::Add(TKey,TValue)
#define Dictionary_2_Add_m1966228754(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2126409608 *, String_t*, ProductInfo_t1305991238 , const MethodInfo*))Dictionary_2_Add_m2148363046_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::Clear()
#define Dictionary_2_Clear_m2492081855(__this, method) ((  void (*) (Dictionary_2_t2126409608 *, const MethodInfo*))Dictionary_2_Clear_m2864665242_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m3578004439(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2126409608 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m2562332228_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m4136701490(__this, ___value0, method) ((  bool (*) (Dictionary_2_t2126409608 *, ProductInfo_t1305991238 , const MethodInfo*))Dictionary_2_ContainsValue_m3654854724_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m3303946747(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2126409608 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m919964301_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m3515305801(__this, ___sender0, method) ((  void (*) (Dictionary_2_t2126409608 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1406735863_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::Remove(TKey)
#define Dictionary_2_Remove_m3973111422(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2126409608 *, String_t*, const MethodInfo*))Dictionary_2_Remove_m3506063404_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m789948811(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t2126409608 *, String_t*, ProductInfo_t1305991238 *, const MethodInfo*))Dictionary_2_TryGetValue_m2449336989_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::get_Keys()
#define Dictionary_2_get_Keys_m1754998000(__this, method) ((  KeyCollection_t3753169059 * (*) (Dictionary_2_t2126409608 *, const MethodInfo*))Dictionary_2_get_Keys_m3385795102_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::get_Values()
#define Dictionary_2_get_Values_m25905036(__this, method) ((  ValueCollection_t827015321 * (*) (Dictionary_2_t2126409608 *, const MethodInfo*))Dictionary_2_get_Values_m3853824314_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m3865503431(__this, ___key0, method) ((  String_t* (*) (Dictionary_2_t2126409608 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m3383656665_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m2934204003(__this, ___value0, method) ((  ProductInfo_t1305991238  (*) (Dictionary_2_t2126409608 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m3735929845_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m4248771681(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t2126409608 *, KeyValuePair_2_t2025190314 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m2042893839_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m2154591144(__this, method) ((  Enumerator_t3443733000  (*) (Dictionary_2_t2126409608 *, const MethodInfo*))Dictionary_2_GetEnumerator_m4021125946_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m3621982239(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, String_t*, ProductInfo_t1305991238 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m986403121_gshared)(__this /* static, unused */, ___key0, ___value1, method)
