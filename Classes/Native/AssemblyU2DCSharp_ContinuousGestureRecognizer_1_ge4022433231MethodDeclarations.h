﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ContinuousGestureRecognizer`1<System.Object>
struct ContinuousGestureRecognizer_1_t4022433231;
// System.Object
struct Il2CppObject;
// Gesture
struct Gesture_t1589572905;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Gesture1589572905.h"

// System.Void ContinuousGestureRecognizer`1<System.Object>::.ctor()
extern "C"  void ContinuousGestureRecognizer_1__ctor_m2501420654_gshared (ContinuousGestureRecognizer_1_t4022433231 * __this, const MethodInfo* method);
#define ContinuousGestureRecognizer_1__ctor_m2501420654(__this, method) ((  void (*) (ContinuousGestureRecognizer_1_t4022433231 *, const MethodInfo*))ContinuousGestureRecognizer_1__ctor_m2501420654_gshared)(__this, method)
// System.Void ContinuousGestureRecognizer`1<System.Object>::Reset(T)
extern "C"  void ContinuousGestureRecognizer_1_Reset_m288495523_gshared (ContinuousGestureRecognizer_1_t4022433231 * __this, Il2CppObject * ___gesture0, const MethodInfo* method);
#define ContinuousGestureRecognizer_1_Reset_m288495523(__this, ___gesture0, method) ((  void (*) (ContinuousGestureRecognizer_1_t4022433231 *, Il2CppObject *, const MethodInfo*))ContinuousGestureRecognizer_1_Reset_m288495523_gshared)(__this, ___gesture0, method)
// System.Void ContinuousGestureRecognizer`1<System.Object>::OnStateChanged(Gesture)
extern "C"  void ContinuousGestureRecognizer_1_OnStateChanged_m2420751025_gshared (ContinuousGestureRecognizer_1_t4022433231 * __this, Gesture_t1589572905 * ___sender0, const MethodInfo* method);
#define ContinuousGestureRecognizer_1_OnStateChanged_m2420751025(__this, ___sender0, method) ((  void (*) (ContinuousGestureRecognizer_1_t4022433231 *, Gesture_t1589572905 *, const MethodInfo*))ContinuousGestureRecognizer_1_OnStateChanged_m2420751025_gshared)(__this, ___sender0, method)
