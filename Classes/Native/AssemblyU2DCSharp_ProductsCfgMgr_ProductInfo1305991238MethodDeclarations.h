﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProductsCfgMgr/ProductInfo
struct ProductInfo_t1305991238;
struct ProductInfo_t1305991238_marshaled_pinvoke;
struct ProductInfo_t1305991238_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct ProductInfo_t1305991238;
struct ProductInfo_t1305991238_marshaled_pinvoke;

extern "C" void ProductInfo_t1305991238_marshal_pinvoke(const ProductInfo_t1305991238& unmarshaled, ProductInfo_t1305991238_marshaled_pinvoke& marshaled);
extern "C" void ProductInfo_t1305991238_marshal_pinvoke_back(const ProductInfo_t1305991238_marshaled_pinvoke& marshaled, ProductInfo_t1305991238& unmarshaled);
extern "C" void ProductInfo_t1305991238_marshal_pinvoke_cleanup(ProductInfo_t1305991238_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ProductInfo_t1305991238;
struct ProductInfo_t1305991238_marshaled_com;

extern "C" void ProductInfo_t1305991238_marshal_com(const ProductInfo_t1305991238& unmarshaled, ProductInfo_t1305991238_marshaled_com& marshaled);
extern "C" void ProductInfo_t1305991238_marshal_com_back(const ProductInfo_t1305991238_marshaled_com& marshaled, ProductInfo_t1305991238& unmarshaled);
extern "C" void ProductInfo_t1305991238_marshal_com_cleanup(ProductInfo_t1305991238_marshaled_com& marshaled);
