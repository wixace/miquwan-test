﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_satuSojarball215
struct M_satuSojarball215_t3899592855;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_satuSojarball2153899592855.h"

// System.Void GarbageiOS.M_satuSojarball215::.ctor()
extern "C"  void M_satuSojarball215__ctor_m1046030204 (M_satuSojarball215_t3899592855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_satuSojarball215::M_jetirlow0(System.String[],System.Int32)
extern "C"  void M_satuSojarball215_M_jetirlow0_m876090269 (M_satuSojarball215_t3899592855 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_satuSojarball215::M_cociri1(System.String[],System.Int32)
extern "C"  void M_satuSojarball215_M_cociri1_m2356632007 (M_satuSojarball215_t3899592855 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_satuSojarball215::M_vetitiRulo2(System.String[],System.Int32)
extern "C"  void M_satuSojarball215_M_vetitiRulo2_m1699188178 (M_satuSojarball215_t3899592855 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_satuSojarball215::M_kepeaDarearcoo3(System.String[],System.Int32)
extern "C"  void M_satuSojarball215_M_kepeaDarearcoo3_m3223662850 (M_satuSojarball215_t3899592855 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_satuSojarball215::M_nallmoLearstai4(System.String[],System.Int32)
extern "C"  void M_satuSojarball215_M_nallmoLearstai4_m3487236107 (M_satuSojarball215_t3899592855 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_satuSojarball215::M_druser5(System.String[],System.Int32)
extern "C"  void M_satuSojarball215_M_druser5_m1421397755 (M_satuSojarball215_t3899592855 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_satuSojarball215::M_jawrouLogabe6(System.String[],System.Int32)
extern "C"  void M_satuSojarball215_M_jawrouLogabe6_m1568544509 (M_satuSojarball215_t3899592855 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_satuSojarball215::M_tecowtu7(System.String[],System.Int32)
extern "C"  void M_satuSojarball215_M_tecowtu7_m1041418605 (M_satuSojarball215_t3899592855 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_satuSojarball215::ilo_M_kepeaDarearcoo31(GarbageiOS.M_satuSojarball215,System.String[],System.Int32)
extern "C"  void M_satuSojarball215_ilo_M_kepeaDarearcoo31_m4156752181 (Il2CppObject * __this /* static, unused */, M_satuSojarball215_t3899592855 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
