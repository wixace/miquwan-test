﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginYyb
struct  PluginYyb_t190373103  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginYyb::pf
	String_t* ___pf_2;
	// System.String PluginYyb::openId
	String_t* ___openId_3;
	// System.String PluginYyb::openKey
	String_t* ___openKey_4;
	// System.String PluginYyb::payOpenKey
	String_t* ___payOpenKey_5;

public:
	inline static int32_t get_offset_of_pf_2() { return static_cast<int32_t>(offsetof(PluginYyb_t190373103, ___pf_2)); }
	inline String_t* get_pf_2() const { return ___pf_2; }
	inline String_t** get_address_of_pf_2() { return &___pf_2; }
	inline void set_pf_2(String_t* value)
	{
		___pf_2 = value;
		Il2CppCodeGenWriteBarrier(&___pf_2, value);
	}

	inline static int32_t get_offset_of_openId_3() { return static_cast<int32_t>(offsetof(PluginYyb_t190373103, ___openId_3)); }
	inline String_t* get_openId_3() const { return ___openId_3; }
	inline String_t** get_address_of_openId_3() { return &___openId_3; }
	inline void set_openId_3(String_t* value)
	{
		___openId_3 = value;
		Il2CppCodeGenWriteBarrier(&___openId_3, value);
	}

	inline static int32_t get_offset_of_openKey_4() { return static_cast<int32_t>(offsetof(PluginYyb_t190373103, ___openKey_4)); }
	inline String_t* get_openKey_4() const { return ___openKey_4; }
	inline String_t** get_address_of_openKey_4() { return &___openKey_4; }
	inline void set_openKey_4(String_t* value)
	{
		___openKey_4 = value;
		Il2CppCodeGenWriteBarrier(&___openKey_4, value);
	}

	inline static int32_t get_offset_of_payOpenKey_5() { return static_cast<int32_t>(offsetof(PluginYyb_t190373103, ___payOpenKey_5)); }
	inline String_t* get_payOpenKey_5() const { return ___payOpenKey_5; }
	inline String_t** get_address_of_payOpenKey_5() { return &___payOpenKey_5; }
	inline void set_payOpenKey_5(String_t* value)
	{
		___payOpenKey_5 = value;
		Il2CppCodeGenWriteBarrier(&___payOpenKey_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
