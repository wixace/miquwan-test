﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua_Assets_SubAssetMgrGenerated
struct Mihua_Assets_SubAssetMgrGenerated_t2966522940;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// Mihua.Assets.SubAssetMgr
struct SubAssetMgr_t3564963414;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_Assets_SubAssetMgr3564963414.h"

// System.Void Mihua_Assets_SubAssetMgrGenerated::.ctor()
extern "C"  void Mihua_Assets_SubAssetMgrGenerated__ctor_m3093255343 (Mihua_Assets_SubAssetMgrGenerated_t2966522940 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Assets_SubAssetMgrGenerated::SubAssetMgr_SubAssetMgr1(JSVCall,System.Int32)
extern "C"  bool Mihua_Assets_SubAssetMgrGenerated_SubAssetMgr_SubAssetMgr1_m1967485937 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Assets_SubAssetMgrGenerated::SubAssetMgr_SHOW_DOWNASSET_DIALOG(JSVCall)
extern "C"  void Mihua_Assets_SubAssetMgrGenerated_SubAssetMgr_SHOW_DOWNASSET_DIALOG_m3517392662 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Assets_SubAssetMgrGenerated::SubAssetMgr_DOWN_COMPLETE(JSVCall)
extern "C"  void Mihua_Assets_SubAssetMgrGenerated_SubAssetMgr_DOWN_COMPLETE_m1644491899 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Assets_SubAssetMgrGenerated::SubAssetMgr_START_LOAD(JSVCall)
extern "C"  void Mihua_Assets_SubAssetMgrGenerated_SubAssetMgr_START_LOAD_m1590063000 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Assets_SubAssetMgrGenerated::SubAssetMgr_STOP_LOAD(JSVCall)
extern "C"  void Mihua_Assets_SubAssetMgrGenerated_SubAssetMgr_STOP_LOAD_m3375095246 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Assets_SubAssetMgrGenerated::SubAssetMgr_isForce(JSVCall)
extern "C"  void Mihua_Assets_SubAssetMgrGenerated_SubAssetMgr_isForce_m2373117296 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Assets_SubAssetMgrGenerated::SubAssetMgr_isStart(JSVCall)
extern "C"  void Mihua_Assets_SubAssetMgrGenerated_SubAssetMgr_isStart_m892166617 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Assets_SubAssetMgrGenerated::SubAssetMgr_TotalSize(JSVCall)
extern "C"  void Mihua_Assets_SubAssetMgrGenerated_SubAssetMgr_TotalSize_m194372044 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Assets_SubAssetMgrGenerated::SubAssetMgr_isNeedDown(JSVCall)
extern "C"  void Mihua_Assets_SubAssetMgrGenerated_SubAssetMgr_isNeedDown_m248950681 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Assets_SubAssetMgrGenerated::SubAssetMgr_isShowDialog(JSVCall)
extern "C"  void Mihua_Assets_SubAssetMgrGenerated_SubAssetMgr_isShowDialog_m2073795372 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Assets_SubAssetMgrGenerated::SubAssetMgr_isPause(JSVCall)
extern "C"  void Mihua_Assets_SubAssetMgrGenerated_SubAssetMgr_isPause_m3125970629 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Assets_SubAssetMgrGenerated::SubAssetMgr_Progress(JSVCall)
extern "C"  void Mihua_Assets_SubAssetMgrGenerated_SubAssetMgr_Progress_m2708242574 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Assets_SubAssetMgrGenerated::SubAssetMgr_ProgressString(JSVCall)
extern "C"  void Mihua_Assets_SubAssetMgrGenerated_SubAssetMgr_ProgressString_m3263034749 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Assets_SubAssetMgrGenerated::SubAssetMgr_PriorityDown__ListT1_String(JSVCall,System.Int32)
extern "C"  bool Mihua_Assets_SubAssetMgrGenerated_SubAssetMgr_PriorityDown__ListT1_String_m3630654825 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Assets_SubAssetMgrGenerated::SubAssetMgr_SetForceDown__Boolean(JSVCall,System.Int32)
extern "C"  bool Mihua_Assets_SubAssetMgrGenerated_SubAssetMgr_SetForceDown__Boolean_m3052276215 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Assets_SubAssetMgrGenerated::SubAssetMgr_ShowDownassetDialog(JSVCall,System.Int32)
extern "C"  bool Mihua_Assets_SubAssetMgrGenerated_SubAssetMgr_ShowDownassetDialog_m1202426963 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Assets_SubAssetMgrGenerated::SubAssetMgr_StartDown(JSVCall,System.Int32)
extern "C"  bool Mihua_Assets_SubAssetMgrGenerated_SubAssetMgr_StartDown_m414863294 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Assets_SubAssetMgrGenerated::SubAssetMgr_Stop(JSVCall,System.Int32)
extern "C"  bool Mihua_Assets_SubAssetMgrGenerated_SubAssetMgr_Stop_m2159604842 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Assets_SubAssetMgrGenerated::SubAssetMgr_ZUpdate(JSVCall,System.Int32)
extern "C"  bool Mihua_Assets_SubAssetMgrGenerated_SubAssetMgr_ZUpdate_m838908541 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Assets_SubAssetMgrGenerated::__Register()
extern "C"  void Mihua_Assets_SubAssetMgrGenerated___Register_m2455158136 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Assets_SubAssetMgrGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool Mihua_Assets_SubAssetMgrGenerated_ilo_attachFinalizerObject1_m3709421608 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Assets_SubAssetMgrGenerated::ilo_setStringS2(System.Int32,System.String)
extern "C"  void Mihua_Assets_SubAssetMgrGenerated_ilo_setStringS2_m1028071545 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Assets_SubAssetMgrGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void Mihua_Assets_SubAssetMgrGenerated_ilo_setBooleanS3_m2207388446 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Assets_SubAssetMgrGenerated::ilo_get_isNeedDown4(Mihua.Assets.SubAssetMgr)
extern "C"  bool Mihua_Assets_SubAssetMgrGenerated_ilo_get_isNeedDown4_m3899978212 (Il2CppObject * __this /* static, unused */, SubAssetMgr_t3564963414 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Assets_SubAssetMgrGenerated::ilo_get_isPause5(Mihua.Assets.SubAssetMgr)
extern "C"  bool Mihua_Assets_SubAssetMgrGenerated_ilo_get_isPause5_m4151292499 (Il2CppObject * __this /* static, unused */, SubAssetMgr_t3564963414 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mihua_Assets_SubAssetMgrGenerated::ilo_get_Progress6(Mihua.Assets.SubAssetMgr)
extern "C"  float Mihua_Assets_SubAssetMgrGenerated_ilo_get_Progress6_m219276531 (Il2CppObject * __this /* static, unused */, SubAssetMgr_t3564963414 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Assets_SubAssetMgrGenerated::ilo_StartDown7(Mihua.Assets.SubAssetMgr)
extern "C"  void Mihua_Assets_SubAssetMgrGenerated_ilo_StartDown7_m3217691176 (Il2CppObject * __this /* static, unused */, SubAssetMgr_t3564963414 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
