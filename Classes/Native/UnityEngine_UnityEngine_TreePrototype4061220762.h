﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TreePrototype
struct  TreePrototype_t4061220762  : public Il2CppObject
{
public:
	// UnityEngine.GameObject UnityEngine.TreePrototype::m_Prefab
	GameObject_t3674682005 * ___m_Prefab_0;
	// System.Single UnityEngine.TreePrototype::m_BendFactor
	float ___m_BendFactor_1;

public:
	inline static int32_t get_offset_of_m_Prefab_0() { return static_cast<int32_t>(offsetof(TreePrototype_t4061220762, ___m_Prefab_0)); }
	inline GameObject_t3674682005 * get_m_Prefab_0() const { return ___m_Prefab_0; }
	inline GameObject_t3674682005 ** get_address_of_m_Prefab_0() { return &___m_Prefab_0; }
	inline void set_m_Prefab_0(GameObject_t3674682005 * value)
	{
		___m_Prefab_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Prefab_0, value);
	}

	inline static int32_t get_offset_of_m_BendFactor_1() { return static_cast<int32_t>(offsetof(TreePrototype_t4061220762, ___m_BendFactor_1)); }
	inline float get_m_BendFactor_1() const { return ___m_BendFactor_1; }
	inline float* get_address_of_m_BendFactor_1() { return &___m_BendFactor_1; }
	inline void set_m_BendFactor_1(float value)
	{
		___m_BendFactor_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: UnityEngine.TreePrototype
struct TreePrototype_t4061220762_marshaled_pinvoke
{
	GameObject_t3674682005 * ___m_Prefab_0;
	float ___m_BendFactor_1;
};
// Native definition for marshalling of: UnityEngine.TreePrototype
struct TreePrototype_t4061220762_marshaled_com
{
	GameObject_t3674682005 * ___m_Prefab_0;
	float ___m_BendFactor_1;
};
