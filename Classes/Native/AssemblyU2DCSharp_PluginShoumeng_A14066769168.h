﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginShoumeng_A1
struct  PluginShoumeng_A1_t4066769168  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginShoumeng_A1::loginAccount
	String_t* ___loginAccount_2;
	// System.String PluginShoumeng_A1::sessiond
	String_t* ___sessiond_3;
	// System.String[] PluginShoumeng_A1::sdkInfos
	StringU5BU5D_t4054002952* ___sdkInfos_4;
	// System.String PluginShoumeng_A1::configId
	String_t* ___configId_5;
	// System.Boolean PluginShoumeng_A1::isOut
	bool ___isOut_6;

public:
	inline static int32_t get_offset_of_loginAccount_2() { return static_cast<int32_t>(offsetof(PluginShoumeng_A1_t4066769168, ___loginAccount_2)); }
	inline String_t* get_loginAccount_2() const { return ___loginAccount_2; }
	inline String_t** get_address_of_loginAccount_2() { return &___loginAccount_2; }
	inline void set_loginAccount_2(String_t* value)
	{
		___loginAccount_2 = value;
		Il2CppCodeGenWriteBarrier(&___loginAccount_2, value);
	}

	inline static int32_t get_offset_of_sessiond_3() { return static_cast<int32_t>(offsetof(PluginShoumeng_A1_t4066769168, ___sessiond_3)); }
	inline String_t* get_sessiond_3() const { return ___sessiond_3; }
	inline String_t** get_address_of_sessiond_3() { return &___sessiond_3; }
	inline void set_sessiond_3(String_t* value)
	{
		___sessiond_3 = value;
		Il2CppCodeGenWriteBarrier(&___sessiond_3, value);
	}

	inline static int32_t get_offset_of_sdkInfos_4() { return static_cast<int32_t>(offsetof(PluginShoumeng_A1_t4066769168, ___sdkInfos_4)); }
	inline StringU5BU5D_t4054002952* get_sdkInfos_4() const { return ___sdkInfos_4; }
	inline StringU5BU5D_t4054002952** get_address_of_sdkInfos_4() { return &___sdkInfos_4; }
	inline void set_sdkInfos_4(StringU5BU5D_t4054002952* value)
	{
		___sdkInfos_4 = value;
		Il2CppCodeGenWriteBarrier(&___sdkInfos_4, value);
	}

	inline static int32_t get_offset_of_configId_5() { return static_cast<int32_t>(offsetof(PluginShoumeng_A1_t4066769168, ___configId_5)); }
	inline String_t* get_configId_5() const { return ___configId_5; }
	inline String_t** get_address_of_configId_5() { return &___configId_5; }
	inline void set_configId_5(String_t* value)
	{
		___configId_5 = value;
		Il2CppCodeGenWriteBarrier(&___configId_5, value);
	}

	inline static int32_t get_offset_of_isOut_6() { return static_cast<int32_t>(offsetof(PluginShoumeng_A1_t4066769168, ___isOut_6)); }
	inline bool get_isOut_6() const { return ___isOut_6; }
	inline bool* get_address_of_isOut_6() { return &___isOut_6; }
	inline void set_isOut_6(bool value)
	{
		___isOut_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
