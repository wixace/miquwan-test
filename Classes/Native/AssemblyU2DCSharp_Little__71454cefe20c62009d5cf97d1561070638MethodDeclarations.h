﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._71454cefe20c62009d5cf97d1cbb9738
struct _71454cefe20c62009d5cf97d1cbb9738_t1561070638;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._71454cefe20c62009d5cf97d1cbb9738::.ctor()
extern "C"  void _71454cefe20c62009d5cf97d1cbb9738__ctor_m2729897407 (_71454cefe20c62009d5cf97d1cbb9738_t1561070638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._71454cefe20c62009d5cf97d1cbb9738::_71454cefe20c62009d5cf97d1cbb9738m2(System.Int32)
extern "C"  int32_t _71454cefe20c62009d5cf97d1cbb9738__71454cefe20c62009d5cf97d1cbb9738m2_m102052569 (_71454cefe20c62009d5cf97d1cbb9738_t1561070638 * __this, int32_t ____71454cefe20c62009d5cf97d1cbb9738a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._71454cefe20c62009d5cf97d1cbb9738::_71454cefe20c62009d5cf97d1cbb9738m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _71454cefe20c62009d5cf97d1cbb9738__71454cefe20c62009d5cf97d1cbb9738m_m3539688445 (_71454cefe20c62009d5cf97d1cbb9738_t1561070638 * __this, int32_t ____71454cefe20c62009d5cf97d1cbb9738a0, int32_t ____71454cefe20c62009d5cf97d1cbb9738671, int32_t ____71454cefe20c62009d5cf97d1cbb9738c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
