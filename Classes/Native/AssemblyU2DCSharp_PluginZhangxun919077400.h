﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginZhangxun
struct  PluginZhangxun_t919077400  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginZhangxun::uid
	String_t* ___uid_2;
	// System.String PluginZhangxun::token
	String_t* ___token_3;
	// System.String PluginZhangxun::gameName
	String_t* ___gameName_4;
	// System.String PluginZhangxun::gameAppId
	String_t* ___gameAppId_5;
	// System.String PluginZhangxun::gameId
	String_t* ___gameId_6;
	// System.String PluginZhangxun::gameKey
	String_t* ___gameKey_7;
	// System.String PluginZhangxun::promoteid
	String_t* ___promoteid_8;
	// System.String PluginZhangxun::promoteaccount
	String_t* ___promoteaccount_9;
	// System.String PluginZhangxun::configId
	String_t* ___configId_10;
	// System.String[] PluginZhangxun::sdkInfos
	StringU5BU5D_t4054002952* ___sdkInfos_11;
	// System.String PluginZhangxun::time
	String_t* ___time_12;
	// System.Boolean PluginZhangxun::isOut
	bool ___isOut_13;

public:
	inline static int32_t get_offset_of_uid_2() { return static_cast<int32_t>(offsetof(PluginZhangxun_t919077400, ___uid_2)); }
	inline String_t* get_uid_2() const { return ___uid_2; }
	inline String_t** get_address_of_uid_2() { return &___uid_2; }
	inline void set_uid_2(String_t* value)
	{
		___uid_2 = value;
		Il2CppCodeGenWriteBarrier(&___uid_2, value);
	}

	inline static int32_t get_offset_of_token_3() { return static_cast<int32_t>(offsetof(PluginZhangxun_t919077400, ___token_3)); }
	inline String_t* get_token_3() const { return ___token_3; }
	inline String_t** get_address_of_token_3() { return &___token_3; }
	inline void set_token_3(String_t* value)
	{
		___token_3 = value;
		Il2CppCodeGenWriteBarrier(&___token_3, value);
	}

	inline static int32_t get_offset_of_gameName_4() { return static_cast<int32_t>(offsetof(PluginZhangxun_t919077400, ___gameName_4)); }
	inline String_t* get_gameName_4() const { return ___gameName_4; }
	inline String_t** get_address_of_gameName_4() { return &___gameName_4; }
	inline void set_gameName_4(String_t* value)
	{
		___gameName_4 = value;
		Il2CppCodeGenWriteBarrier(&___gameName_4, value);
	}

	inline static int32_t get_offset_of_gameAppId_5() { return static_cast<int32_t>(offsetof(PluginZhangxun_t919077400, ___gameAppId_5)); }
	inline String_t* get_gameAppId_5() const { return ___gameAppId_5; }
	inline String_t** get_address_of_gameAppId_5() { return &___gameAppId_5; }
	inline void set_gameAppId_5(String_t* value)
	{
		___gameAppId_5 = value;
		Il2CppCodeGenWriteBarrier(&___gameAppId_5, value);
	}

	inline static int32_t get_offset_of_gameId_6() { return static_cast<int32_t>(offsetof(PluginZhangxun_t919077400, ___gameId_6)); }
	inline String_t* get_gameId_6() const { return ___gameId_6; }
	inline String_t** get_address_of_gameId_6() { return &___gameId_6; }
	inline void set_gameId_6(String_t* value)
	{
		___gameId_6 = value;
		Il2CppCodeGenWriteBarrier(&___gameId_6, value);
	}

	inline static int32_t get_offset_of_gameKey_7() { return static_cast<int32_t>(offsetof(PluginZhangxun_t919077400, ___gameKey_7)); }
	inline String_t* get_gameKey_7() const { return ___gameKey_7; }
	inline String_t** get_address_of_gameKey_7() { return &___gameKey_7; }
	inline void set_gameKey_7(String_t* value)
	{
		___gameKey_7 = value;
		Il2CppCodeGenWriteBarrier(&___gameKey_7, value);
	}

	inline static int32_t get_offset_of_promoteid_8() { return static_cast<int32_t>(offsetof(PluginZhangxun_t919077400, ___promoteid_8)); }
	inline String_t* get_promoteid_8() const { return ___promoteid_8; }
	inline String_t** get_address_of_promoteid_8() { return &___promoteid_8; }
	inline void set_promoteid_8(String_t* value)
	{
		___promoteid_8 = value;
		Il2CppCodeGenWriteBarrier(&___promoteid_8, value);
	}

	inline static int32_t get_offset_of_promoteaccount_9() { return static_cast<int32_t>(offsetof(PluginZhangxun_t919077400, ___promoteaccount_9)); }
	inline String_t* get_promoteaccount_9() const { return ___promoteaccount_9; }
	inline String_t** get_address_of_promoteaccount_9() { return &___promoteaccount_9; }
	inline void set_promoteaccount_9(String_t* value)
	{
		___promoteaccount_9 = value;
		Il2CppCodeGenWriteBarrier(&___promoteaccount_9, value);
	}

	inline static int32_t get_offset_of_configId_10() { return static_cast<int32_t>(offsetof(PluginZhangxun_t919077400, ___configId_10)); }
	inline String_t* get_configId_10() const { return ___configId_10; }
	inline String_t** get_address_of_configId_10() { return &___configId_10; }
	inline void set_configId_10(String_t* value)
	{
		___configId_10 = value;
		Il2CppCodeGenWriteBarrier(&___configId_10, value);
	}

	inline static int32_t get_offset_of_sdkInfos_11() { return static_cast<int32_t>(offsetof(PluginZhangxun_t919077400, ___sdkInfos_11)); }
	inline StringU5BU5D_t4054002952* get_sdkInfos_11() const { return ___sdkInfos_11; }
	inline StringU5BU5D_t4054002952** get_address_of_sdkInfos_11() { return &___sdkInfos_11; }
	inline void set_sdkInfos_11(StringU5BU5D_t4054002952* value)
	{
		___sdkInfos_11 = value;
		Il2CppCodeGenWriteBarrier(&___sdkInfos_11, value);
	}

	inline static int32_t get_offset_of_time_12() { return static_cast<int32_t>(offsetof(PluginZhangxun_t919077400, ___time_12)); }
	inline String_t* get_time_12() const { return ___time_12; }
	inline String_t** get_address_of_time_12() { return &___time_12; }
	inline void set_time_12(String_t* value)
	{
		___time_12 = value;
		Il2CppCodeGenWriteBarrier(&___time_12, value);
	}

	inline static int32_t get_offset_of_isOut_13() { return static_cast<int32_t>(offsetof(PluginZhangxun_t919077400, ___isOut_13)); }
	inline bool get_isOut_13() const { return ___isOut_13; }
	inline bool* get_address_of_isOut_13() { return &___isOut_13; }
	inline void set_isOut_13(bool value)
	{
		___isOut_13 = value;
	}
};

struct PluginZhangxun_t919077400_StaticFields
{
public:
	// System.Action PluginZhangxun::<>f__am$cacheC
	Action_t3771233898 * ___U3CU3Ef__amU24cacheC_14;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_14() { return static_cast<int32_t>(offsetof(PluginZhangxun_t919077400_StaticFields, ___U3CU3Ef__amU24cacheC_14)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cacheC_14() const { return ___U3CU3Ef__amU24cacheC_14; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cacheC_14() { return &___U3CU3Ef__amU24cacheC_14; }
	inline void set_U3CU3Ef__amU24cacheC_14(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cacheC_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheC_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
