﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ISerializationCallbackReceiverGenerated
struct UnityEngine_ISerializationCallbackReceiverGenerated_t1088022508;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_ISerializationCallbackReceiverGenerated::.ctor()
extern "C"  void UnityEngine_ISerializationCallbackReceiverGenerated__ctor_m2298918143 (UnityEngine_ISerializationCallbackReceiverGenerated_t1088022508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ISerializationCallbackReceiverGenerated::ISerializationCallbackReceiver_OnAfterDeserialize(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ISerializationCallbackReceiverGenerated_ISerializationCallbackReceiver_OnAfterDeserialize_m305454023 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ISerializationCallbackReceiverGenerated::ISerializationCallbackReceiver_OnBeforeSerialize(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ISerializationCallbackReceiverGenerated_ISerializationCallbackReceiver_OnBeforeSerialize_m1393130271 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ISerializationCallbackReceiverGenerated::__Register()
extern "C"  void UnityEngine_ISerializationCallbackReceiverGenerated___Register_m1808731944 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
