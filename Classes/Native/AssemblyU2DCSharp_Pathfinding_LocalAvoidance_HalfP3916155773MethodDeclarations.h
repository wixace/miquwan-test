﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.LocalAvoidance/HalfPlane
struct HalfPlane_t3916155773;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_HalfP3916155773.h"

// System.Void Pathfinding.LocalAvoidance/HalfPlane::.ctor()
extern "C"  void HalfPlane__ctor_m3904240574 (HalfPlane_t3916155773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LocalAvoidance/HalfPlane::Contains(UnityEngine.Vector3)
extern "C"  bool HalfPlane_Contains_m690536160 (HalfPlane_t3916155773 * __this, Vector3_t4282066566  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.LocalAvoidance/HalfPlane::ClosestPoint(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  HalfPlane_ClosestPoint_m1730506818 (HalfPlane_t3916155773 * __this, Vector3_t4282066566  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.LocalAvoidance/HalfPlane::ClosestPoint(UnityEngine.Vector3,System.Single,System.Single)
extern "C"  Vector3_t4282066566  HalfPlane_ClosestPoint_m238549580 (HalfPlane_t3916155773 * __this, Vector3_t4282066566  ___p0, float ___minX1, float ___maxX2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.LocalAvoidance/HalfPlane::Intersection(Pathfinding.LocalAvoidance/HalfPlane)
extern "C"  Vector3_t4282066566  HalfPlane_Intersection_m1559615916 (HalfPlane_t3916155773 * __this, HalfPlane_t3916155773 * ___hp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LocalAvoidance/HalfPlane::DrawBounds(System.Single,System.Single)
extern "C"  void HalfPlane_DrawBounds_m1771613457 (HalfPlane_t3916155773 * __this, float ___left0, float ___right1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LocalAvoidance/HalfPlane::Draw()
extern "C"  void HalfPlane_Draw_m769076746 (HalfPlane_t3916155773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
