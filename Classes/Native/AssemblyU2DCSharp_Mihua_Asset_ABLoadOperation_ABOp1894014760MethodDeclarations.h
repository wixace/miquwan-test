﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua.Asset.ABLoadOperation.ABOperation
struct ABOperation_t1894014760;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Mihua.Asset.ABLoadOperation.ABOperation::.ctor()
extern "C"  void ABOperation__ctor_m2464832231 (ABOperation_t1894014760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mihua.Asset.ABLoadOperation.ABOperation::get_Current()
extern "C"  Il2CppObject * ABOperation_get_Current_m1636407690 (ABOperation_t1894014760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Asset.ABLoadOperation.ABOperation::MoveNext()
extern "C"  bool ABOperation_MoveNext_m2946038189 (ABOperation_t1894014760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.ABLoadOperation.ABOperation::Reset()
extern "C"  void ABOperation_Reset_m111265172 (ABOperation_t1894014760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.ABLoadOperation.ABOperation::ClearOnLoad()
extern "C"  void ABOperation_ClearOnLoad_m1172825239 (ABOperation_t1894014760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.ABLoadOperation.ABOperation::CacheAsset()
extern "C"  void ABOperation_CacheAsset_m1737379211 (ABOperation_t1894014760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
