﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066747055MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Collections.IEnumerator>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2280067942(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3360505891 *, uint32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m4066247973_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Collections.IEnumerator>::get_Key()
#define KeyValuePair_2_get_Key_m748580290(__this, method) ((  uint32_t (*) (KeyValuePair_2_t3360505891 *, const MethodInfo*))KeyValuePair_2_get_Key_m1758015651_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Collections.IEnumerator>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3845603075(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3360505891 *, uint32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1636954596_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Collections.IEnumerator>::get_Value()
#define KeyValuePair_2_get_Value_m2233491458(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t3360505891 *, const MethodInfo*))KeyValuePair_2_get_Value_m2692750471_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Collections.IEnumerator>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3724892547(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3360505891 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m1981395940_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Collections.IEnumerator>::ToString()
#define KeyValuePair_2_ToString_m3566455551(__this, method) ((  String_t* (*) (KeyValuePair_2_t3360505891 *, const MethodInfo*))KeyValuePair_2_ToString_m2106229668_gshared)(__this, method)
