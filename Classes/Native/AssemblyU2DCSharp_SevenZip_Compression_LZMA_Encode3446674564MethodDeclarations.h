﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.Compression.LZMA.Encoder/LenEncoder
struct LenEncoder_t3446674564;
// SevenZip.Compression.RangeCoder.Encoder
struct Encoder_t2248006694;
// System.UInt32[]
struct UInt32U5BU5D_t3230734560;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_2248006694.h"

// System.Void SevenZip.Compression.LZMA.Encoder/LenEncoder::.ctor()
extern "C"  void LenEncoder__ctor_m4145959575 (LenEncoder_t3446674564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder/LenEncoder::Init(System.UInt32)
extern "C"  void LenEncoder_Init_m2706666301 (LenEncoder_t3446674564 * __this, uint32_t ___numPosStates0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder/LenEncoder::Encode(SevenZip.Compression.RangeCoder.Encoder,System.UInt32,System.UInt32)
extern "C"  void LenEncoder_Encode_m3101750431 (LenEncoder_t3446674564 * __this, Encoder_t2248006694 * ___rangeEncoder0, uint32_t ___symbol1, uint32_t ___posState2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder/LenEncoder::SetPrices(System.UInt32,System.UInt32,System.UInt32[],System.UInt32)
extern "C"  void LenEncoder_SetPrices_m272137555 (LenEncoder_t3446674564 * __this, uint32_t ___posState0, uint32_t ___numSymbols1, UInt32U5BU5D_t3230734560* ___prices2, uint32_t ___st3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
