﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnitStateKnockingUp
struct UnitStateKnockingUp_t2754667254;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// CombatEntity
struct CombatEntity_t684137495;
// AnimationRunner
struct AnimationRunner_t1015409588;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UnitStateID1002198824.h"
#include "AssemblyU2DCSharp_UnitStateKnockingUp2754667254.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_AnimationRunner1015409588.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniType1006238651.h"

// System.Void UnitStateKnockingUp::.ctor()
extern "C"  void UnitStateKnockingUp__ctor_m1214129461 (UnitStateKnockingUp_t2754667254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateKnockingUp::.cctor()
extern "C"  void UnitStateKnockingUp__cctor_m2796178712 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnitStateID UnitStateKnockingUp::get_type()
extern "C"  uint8_t UnitStateKnockingUp_get_type_m1926569287 (UnitStateKnockingUp_t2754667254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateKnockingUp::SetParams(System.Object[])
extern "C"  void UnitStateKnockingUp_SetParams_m2312405623 (UnitStateKnockingUp_t2754667254 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateKnockingUp::EnterState()
extern "C"  void UnitStateKnockingUp_EnterState_m1953731912 (UnitStateKnockingUp_t2754667254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateKnockingUp::DoEnter()
extern "C"  void UnitStateKnockingUp_DoEnter_m2383976352 (UnitStateKnockingUp_t2754667254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateKnockingUp::Update(System.Single)
extern "C"  void UnitStateKnockingUp_Update_m2813177907 (UnitStateKnockingUp_t2754667254 * __this, float ___deltatime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateKnockingUp::LeaveState()
extern "C"  void UnitStateKnockingUp_LeaveState_m3740107913 (UnitStateKnockingUp_t2754667254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateKnockingUp::ilo_DoEnter1(UnitStateKnockingUp)
extern "C"  void UnitStateKnockingUp_ilo_DoEnter1_m3204769622 (Il2CppObject * __this /* static, unused */, UnitStateKnockingUp_t2754667254 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnitStateKnockingUp::ilo_get_isDeath2(CombatEntity)
extern "C"  bool UnitStateKnockingUp_ilo_get_isDeath2_m2394749544 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateKnockingUp::ilo_set_isKnockingUp3(CombatEntity,System.Boolean)
extern "C"  void UnitStateKnockingUp_ilo_set_isKnockingUp3_m596242097 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AnimationRunner UnitStateKnockingUp::ilo_get_characterAnim4(CombatEntity)
extern "C"  AnimationRunner_t1015409588 * UnitStateKnockingUp_ilo_get_characterAnim4_m233681711 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateKnockingUp::ilo_Play5(AnimationRunner,System.String,System.Single,System.Boolean)
extern "C"  void UnitStateKnockingUp_ilo_Play5_m3863983615 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, String_t* ___aniName1, float ___playTime2, bool ___isSkill3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateKnockingUp::ilo_Play6(AnimationRunner,AnimationRunner/AniType,System.Single,System.Boolean)
extern "C"  void UnitStateKnockingUp_ilo_Play6_m4063361803 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, int32_t ___aniType1, float ___playTime2, bool ___isskill3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateKnockingUp::ilo_set_forceShift7(CombatEntity,System.Boolean)
extern "C"  void UnitStateKnockingUp_ilo_set_forceShift7_m3474399089 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
