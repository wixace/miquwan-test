﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIWrapContentGenerated
struct UIWrapContentGenerated_t2997283380;
// JSVCall
struct JSVCall_t3708497963;
// UIWrapContent/OnInitializeItem
struct OnInitializeItem_t2759048342;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// UIWrapContent
struct UIWrapContent_t33025435;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_UIWrapContent33025435.h"

// System.Void UIWrapContentGenerated::.ctor()
extern "C"  void UIWrapContentGenerated__ctor_m1197966567 (UIWrapContentGenerated_t2997283380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWrapContentGenerated::UIWrapContent_UIWrapContent1(JSVCall,System.Int32)
extern "C"  bool UIWrapContentGenerated_UIWrapContent_UIWrapContent1_m965101755 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWrapContentGenerated::UIWrapContent_itemSize(JSVCall)
extern "C"  void UIWrapContentGenerated_UIWrapContent_itemSize_m1100493266 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWrapContentGenerated::UIWrapContent_cullContent(JSVCall)
extern "C"  void UIWrapContentGenerated_UIWrapContent_cullContent_m2350366527 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWrapContentGenerated::UIWrapContent_minIndex(JSVCall)
extern "C"  void UIWrapContentGenerated_UIWrapContent_minIndex_m613135206 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWrapContentGenerated::UIWrapContent_maxIndex(JSVCall)
extern "C"  void UIWrapContentGenerated_UIWrapContent_maxIndex_m2593057528 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIWrapContent/OnInitializeItem UIWrapContentGenerated::UIWrapContent_onInitializeItem_GetDelegate_member4_arg0(CSRepresentedObject)
extern "C"  OnInitializeItem_t2759048342 * UIWrapContentGenerated_UIWrapContent_onInitializeItem_GetDelegate_member4_arg0_m634073346 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWrapContentGenerated::UIWrapContent_onInitializeItem(JSVCall)
extern "C"  void UIWrapContentGenerated_UIWrapContent_onInitializeItem_m2973904068 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWrapContentGenerated::UIWrapContent_SortAlphabetically(JSVCall,System.Int32)
extern "C"  bool UIWrapContentGenerated_UIWrapContent_SortAlphabetically_m3338754472 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWrapContentGenerated::UIWrapContent_SortBasedOnScrollMovement(JSVCall,System.Int32)
extern "C"  bool UIWrapContentGenerated_UIWrapContent_SortBasedOnScrollMovement_m3208843117 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWrapContentGenerated::UIWrapContent_WrapContent(JSVCall,System.Int32)
extern "C"  bool UIWrapContentGenerated_UIWrapContent_WrapContent_m3780140748 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWrapContentGenerated::__Register()
extern "C"  void UIWrapContentGenerated___Register_m1887170624 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIWrapContent/OnInitializeItem UIWrapContentGenerated::<UIWrapContent_onInitializeItem>m__178()
extern "C"  OnInitializeItem_t2759048342 * UIWrapContentGenerated_U3CUIWrapContent_onInitializeItemU3Em__178_m4104420229 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWrapContentGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UIWrapContentGenerated_ilo_addJSCSRel1_m1928655907 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIWrapContentGenerated::ilo_getInt322(System.Int32)
extern "C"  int32_t UIWrapContentGenerated_ilo_getInt322_m3215223919 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIWrapContentGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIWrapContentGenerated_ilo_setObject3_m4133919237 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWrapContentGenerated::ilo_SortBasedOnScrollMovement4(UIWrapContent)
extern "C"  void UIWrapContentGenerated_ilo_SortBasedOnScrollMovement4_m3154955203 (Il2CppObject * __this /* static, unused */, UIWrapContent_t33025435 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWrapContentGenerated::ilo_WrapContent5(UIWrapContent)
extern "C"  void UIWrapContentGenerated_ilo_WrapContent5_m1454713089 (Il2CppObject * __this /* static, unused */, UIWrapContent_t33025435 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
