﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.UnityReferenceHelper
struct  UnityReferenceHelper_t2955464730  : public MonoBehaviour_t667441552
{
public:
	// System.String Pathfinding.UnityReferenceHelper::guid
	String_t* ___guid_2;

public:
	inline static int32_t get_offset_of_guid_2() { return static_cast<int32_t>(offsetof(UnityReferenceHelper_t2955464730, ___guid_2)); }
	inline String_t* get_guid_2() const { return ___guid_2; }
	inline String_t** get_address_of_guid_2() { return &___guid_2; }
	inline void set_guid_2(String_t* value)
	{
		___guid_2 = value;
		Il2CppCodeGenWriteBarrier(&___guid_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
