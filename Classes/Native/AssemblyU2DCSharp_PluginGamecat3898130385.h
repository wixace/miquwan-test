﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginGamecat
struct  PluginGamecat_t3898130385  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginGamecat::openid
	String_t* ___openid_2;
	// System.String PluginGamecat::configId
	String_t* ___configId_3;
	// System.Boolean PluginGamecat::isOut
	bool ___isOut_4;
	// System.String PluginGamecat::curSDKVer
	String_t* ___curSDKVer_5;

public:
	inline static int32_t get_offset_of_openid_2() { return static_cast<int32_t>(offsetof(PluginGamecat_t3898130385, ___openid_2)); }
	inline String_t* get_openid_2() const { return ___openid_2; }
	inline String_t** get_address_of_openid_2() { return &___openid_2; }
	inline void set_openid_2(String_t* value)
	{
		___openid_2 = value;
		Il2CppCodeGenWriteBarrier(&___openid_2, value);
	}

	inline static int32_t get_offset_of_configId_3() { return static_cast<int32_t>(offsetof(PluginGamecat_t3898130385, ___configId_3)); }
	inline String_t* get_configId_3() const { return ___configId_3; }
	inline String_t** get_address_of_configId_3() { return &___configId_3; }
	inline void set_configId_3(String_t* value)
	{
		___configId_3 = value;
		Il2CppCodeGenWriteBarrier(&___configId_3, value);
	}

	inline static int32_t get_offset_of_isOut_4() { return static_cast<int32_t>(offsetof(PluginGamecat_t3898130385, ___isOut_4)); }
	inline bool get_isOut_4() const { return ___isOut_4; }
	inline bool* get_address_of_isOut_4() { return &___isOut_4; }
	inline void set_isOut_4(bool value)
	{
		___isOut_4 = value;
	}

	inline static int32_t get_offset_of_curSDKVer_5() { return static_cast<int32_t>(offsetof(PluginGamecat_t3898130385, ___curSDKVer_5)); }
	inline String_t* get_curSDKVer_5() const { return ___curSDKVer_5; }
	inline String_t** get_address_of_curSDKVer_5() { return &___curSDKVer_5; }
	inline void set_curSDKVer_5(String_t* value)
	{
		___curSDKVer_5 = value;
		Il2CppCodeGenWriteBarrier(&___curSDKVer_5, value);
	}
};

struct PluginGamecat_t3898130385_StaticFields
{
public:
	// System.Action PluginGamecat::<>f__am$cache4
	Action_t3771233898 * ___U3CU3Ef__amU24cache4_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_6() { return static_cast<int32_t>(offsetof(PluginGamecat_t3898130385_StaticFields, ___U3CU3Ef__amU24cache4_6)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache4_6() const { return ___U3CU3Ef__amU24cache4_6; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache4_6() { return &___U3CU3Ef__amU24cache4_6; }
	inline void set_U3CU3Ef__amU24cache4_6(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache4_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
