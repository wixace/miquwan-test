﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.CharacterInfo
struct CharacterInfo_t2481726445;
struct CharacterInfo_t2481726445_marshaled_pinvoke;
struct CharacterInfo_t2481726445_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_CharacterInfo2481726445.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Int32 UnityEngine.CharacterInfo::get_advance()
extern "C"  int32_t CharacterInfo_get_advance_m168235289 (CharacterInfo_t2481726445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterInfo::set_advance(System.Int32)
extern "C"  void CharacterInfo_set_advance_m797119606 (CharacterInfo_t2481726445 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_glyphWidth()
extern "C"  int32_t CharacterInfo_get_glyphWidth_m1858054821 (CharacterInfo_t2481726445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterInfo::set_glyphWidth(System.Int32)
extern "C"  void CharacterInfo_set_glyphWidth_m262744426 (CharacterInfo_t2481726445 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_glyphHeight()
extern "C"  int32_t CharacterInfo_get_glyphHeight_m2268938954 (CharacterInfo_t2481726445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterInfo::set_glyphHeight(System.Int32)
extern "C"  void CharacterInfo_set_glyphHeight_m875994791 (CharacterInfo_t2481726445 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_bearing()
extern "C"  int32_t CharacterInfo_get_bearing_m3423257925 (CharacterInfo_t2481726445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterInfo::set_bearing(System.Int32)
extern "C"  void CharacterInfo_set_bearing_m605939362 (CharacterInfo_t2481726445 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_minY()
extern "C"  int32_t CharacterInfo_get_minY_m746266418 (CharacterInfo_t2481726445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterInfo::set_minY(System.Int32)
extern "C"  void CharacterInfo_set_minY_m907278711 (CharacterInfo_t2481726445 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_maxY()
extern "C"  int32_t CharacterInfo_get_maxY_m739176160 (CharacterInfo_t2481726445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterInfo::set_maxY(System.Int32)
extern "C"  void CharacterInfo_set_maxY_m2155328549 (CharacterInfo_t2481726445 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_minX()
extern "C"  int32_t CharacterInfo_get_minX_m746265457 (CharacterInfo_t2481726445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterInfo::set_minX(System.Int32)
extern "C"  void CharacterInfo_set_minX_m3694694198 (CharacterInfo_t2481726445 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_maxX()
extern "C"  int32_t CharacterInfo_get_maxX_m739175199 (CharacterInfo_t2481726445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterInfo::set_maxX(System.Int32)
extern "C"  void CharacterInfo_set_maxX_m647776740 (CharacterInfo_t2481726445 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomLeftUnFlipped()
extern "C"  Vector2_t4282066565  CharacterInfo_get_uvBottomLeftUnFlipped_m2451394632 (CharacterInfo_t2481726445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterInfo::set_uvBottomLeftUnFlipped(UnityEngine.Vector2)
extern "C"  void CharacterInfo_set_uvBottomLeftUnFlipped_m2062831073 (CharacterInfo_t2481726445 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomRightUnFlipped()
extern "C"  Vector2_t4282066565  CharacterInfo_get_uvBottomRightUnFlipped_m1771181289 (CharacterInfo_t2481726445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterInfo::set_uvBottomRightUnFlipped(UnityEngine.Vector2)
extern "C"  void CharacterInfo_set_uvBottomRightUnFlipped_m1379730082 (CharacterInfo_t2481726445 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopRightUnFlipped()
extern "C"  Vector2_t4282066565  CharacterInfo_get_uvTopRightUnFlipped_m2970426419 (CharacterInfo_t2481726445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterInfo::set_uvTopRightUnFlipped(UnityEngine.Vector2)
extern "C"  void CharacterInfo_set_uvTopRightUnFlipped_m997358550 (CharacterInfo_t2481726445 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopLeftUnFlipped()
extern "C"  Vector2_t4282066565  CharacterInfo_get_uvTopLeftUnFlipped_m1658795966 (CharacterInfo_t2481726445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterInfo::set_uvTopLeftUnFlipped(UnityEngine.Vector2)
extern "C"  void CharacterInfo_set_uvTopLeftUnFlipped_m3713064493 (CharacterInfo_t2481726445 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomLeft()
extern "C"  Vector2_t4282066565  CharacterInfo_get_uvBottomLeft_m39059011 (CharacterInfo_t2481726445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterInfo::set_uvBottomLeft(UnityEngine.Vector2)
extern "C"  void CharacterInfo_set_uvBottomLeft_m879886408 (CharacterInfo_t2481726445 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomRight()
extern "C"  Vector2_t4282066565  CharacterInfo_get_uvBottomRight_m2356039810 (CharacterInfo_t2481726445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterInfo::set_uvBottomRight(UnityEngine.Vector2)
extern "C"  void CharacterInfo_set_uvBottomRight_m1797504615 (CharacterInfo_t2481726445 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopRight()
extern "C"  Vector2_t4282066565  CharacterInfo_get_uvTopRight_m1924271608 (CharacterInfo_t2481726445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterInfo::set_uvTopRight(UnityEngine.Vector2)
extern "C"  void CharacterInfo_set_uvTopRight_m1821072051 (CharacterInfo_t2481726445 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopLeft()
extern "C"  Vector2_t4282066565  CharacterInfo_get_uvTopLeft_m579320333 (CharacterInfo_t2481726445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterInfo::set_uvTopLeft(UnityEngine.Vector2)
extern "C"  void CharacterInfo_set_uvTopLeft_m1019193980 (CharacterInfo_t2481726445 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct CharacterInfo_t2481726445;
struct CharacterInfo_t2481726445_marshaled_pinvoke;

extern "C" void CharacterInfo_t2481726445_marshal_pinvoke(const CharacterInfo_t2481726445& unmarshaled, CharacterInfo_t2481726445_marshaled_pinvoke& marshaled);
extern "C" void CharacterInfo_t2481726445_marshal_pinvoke_back(const CharacterInfo_t2481726445_marshaled_pinvoke& marshaled, CharacterInfo_t2481726445& unmarshaled);
extern "C" void CharacterInfo_t2481726445_marshal_pinvoke_cleanup(CharacterInfo_t2481726445_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct CharacterInfo_t2481726445;
struct CharacterInfo_t2481726445_marshaled_com;

extern "C" void CharacterInfo_t2481726445_marshal_com(const CharacterInfo_t2481726445& unmarshaled, CharacterInfo_t2481726445_marshaled_com& marshaled);
extern "C" void CharacterInfo_t2481726445_marshal_com_back(const CharacterInfo_t2481726445_marshaled_com& marshaled, CharacterInfo_t2481726445& unmarshaled);
extern "C" void CharacterInfo_t2481726445_marshal_com_cleanup(CharacterInfo_t2481726445_marshaled_com& marshaled);
