﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BuffCtrlGenerated
struct BuffCtrlGenerated_t2104098225;
// JSVCall
struct JSVCall_t3708497963;
// BuffCtrl
struct BuffCtrl_t2836564350;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// CombatEntity
struct CombatEntity_t684137495;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// Buff
struct Buff_t2081907;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_BuffCtrl2836564350.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_Buff2081907.h"

// System.Void BuffCtrlGenerated::.ctor()
extern "C"  void BuffCtrlGenerated__ctor_m2619180762 (BuffCtrlGenerated_t2104098225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_BuffCtrl1(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_BuffCtrl1_m1205787928 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_curnum(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_curnum_m755253256 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mAnitNum(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mAnitNum_m1179482253 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mAuraAddHPRadius(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mAuraAddHPRadius_m3036203811 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mAuraReduceHPRadius(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mAuraReduceHPRadius_m1588532430 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mHpLineNum(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mHpLineNum_m2270473297 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mRuneTakeEffectNum(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mRuneTakeEffectNum_m309331207 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mGodDownTweenTime(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mGodDownTweenTime_m3091373719 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mAntiDebuffDiscreteAddBuffID(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mAntiDebuffDiscreteAddBuffID_m1931628699 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mAntiDebuffDiscreteID(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mAntiDebuffDiscreteID_m2975054983 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mPetrifiedChangeTime(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mPetrifiedChangeTime_m2940510534 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mChangeOldSkillID(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mChangeOldSkillID_m2909028444 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mWhenCritAddBuffID(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mWhenCritAddBuffID_m1647248480 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mThumpSkillID(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mThumpSkillID_m2915348841 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mTankBuffID(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mTankBuffID_m2169913497 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mReduceRageHpNum(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mReduceRageHpNum_m2474944304 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mGhostIndex(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mGhostIndex_m3561279406 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mSusanooIndex(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mSusanooIndex_m1341434045 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mDistorTionDelayTime(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mDistorTionDelayTime_m3251484232 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mDeathHarvestLine(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mDeathHarvestLine_m1812067784 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mResistanceCountryType(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mResistanceCountryType_m3373233732 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mTauntDis(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mTauntDis_m1260064849 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBuffATNum(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBuffATNum_m3486064635 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBuffPDNum(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBuffPDNum_m597133372 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBuffMDNum(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBuffMDNum_m2113809247 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBuffMaxHPNum(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBuffMaxHPNum_m2625394628 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBuffHitsNum(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBuffHitsNum_m3917534632 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBuffDodgeNum(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBuffDodgeNum_m32788911 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBuffWreckNum(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBuffWreckNum_m213635114 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBuffWithstandNum(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBuffWithstandNum_m3239026472 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBuffCritNum(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBuffCritNum_m3831077186 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBuffTenacityNum(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBuffTenacityNum_m3388186423 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBuffRHurtNum(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBuffRHurtNum_m3016540793 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBuffAT(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBuffAT_m2817902667 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBuffPD(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBuffPD_m482684842 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBuffMD(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBuffMD_m1578571623 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBuffMaxHP(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBuffMaxHP_m3983920418 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBuffHits(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBuffHits_m1874901438 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBuffDodge(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBuffDodge_m2141917975 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBuffWreck(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBuffWreck_m2088869500 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBuffWithstand(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBuffWithstand_m2286051390 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBuffCrit(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBuffCrit_m2598775908 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBuffTenacity(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBuffTenacity_m1166144143 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBuffRHurt(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBuffRHurt_m3166922381 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mHPSecDecPer(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mHPSecDecPer_m3862051767 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mHPSecDecNum(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mHPSecDecNum_m1605035886 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mHPRstNum(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mHPRstNum_m918193590 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mHPRstPer(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mHPRstPer_m3175209471 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mAttackSpeed(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mAttackSpeed_m3253503132 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mModelScale(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mModelScale_m3604967760 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mTreatmentEffect(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mTreatmentEffect_m2682466322 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mHurtEffectPlus(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mHurtEffectPlus_m3329962839 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mHurtEffectReduction(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mHurtEffectReduction_m2048641320 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mVampire(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mVampire_m1934500557 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mAngerSecDecNum(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mAngerSecDecNum_m1717198467 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mRunspeed(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mRunspeed_m3715857781 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mHidePart(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mHidePart_m744226268 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mHPSecDecNumATK(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mHPSecDecNumATK_m114955046 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mHPRstNumATK(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mHPRstNumATK_m3915280350 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mRestrintCountryType(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mRestrintCountryType_m1175301980 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mRestrintPer(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mRestrintPer_m426821327 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mRestrintStateType(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mRestrintStateType_m3110382529 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mRestrintStatePer(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mRestrintStatePer_m3352036916 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mAtkPerHp(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mAtkPerHp_m1806319076 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mAtkNumHp(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mAtkNumHp_m1772541915 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mNDPAddBuff(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mNDPAddBuff_m487174999 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBeAttackedLaterSkillID(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBeAttackedLaterSkillID_m2695871527 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBeAttackedLaterEffectID(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBeAttackedLaterEffectID_m441011693 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mDeathLaterEffectID(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mDeathLaterEffectID_m2465566285 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mDeathLaterHeroID(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mDeathLaterHeroID_m3960167684 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBeattackedLaterSpeed(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBeattackedLaterSpeed_m3487536332 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mDeathLaterSpeed(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mDeathLaterSpeed_m1787828300 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mAnitEndEffectID(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mAnitEndEffectID_m2079256524 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mAuraAddHPNum(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mAuraAddHPNum_m882995505 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mAuraAddHPPer(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mAuraAddHPPer_m3140011386 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mAuraReduceHPNum(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mAuraReduceHPNum_m1090551654 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mAuraReduceHPPer(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mAuraReduceHPPer_m3347567535 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mActiveSkillDamageAddPer(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mActiveSkillDamageAddPer_m1062061637 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mAddAngerRatio(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mAddAngerRatio_m1292726582 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mFreeze(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mFreeze_m3546396826 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mMorph(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mMorph_m172340307 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mAntiControl(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mAntiControl_m236937056 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mAddDamageSkillIDs(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mAddDamageSkillIDs_m3009825892 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mAddDamageValue(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mAddDamageValue_m1986246544 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mHpLineBuffID(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mHpLineBuffID_m4178179943 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mControlBuffTimeBonus(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mControlBuffTimeBonus_m2515598447 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mControlBuffTimeBonusPer(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mControlBuffTimeBonusPer_m2049514912 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mAddDamageValuePer(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mAddDamageValuePer_m3246883551 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mRuneTakeEffectBuffID(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mRuneTakeEffectBuffID_m4234304241 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mRuneTakeEffectBuffID01(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mRuneTakeEffectBuffID01_m2893862736 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mRuneTakeEffectBuffID02(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mRuneTakeEffectBuffID02_m2697349231 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mStable(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mStable_m832495734 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mNotInvade(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mNotInvade_m3101346455 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mUnyielding(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mUnyielding_m2079100099 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mDiscreteBuffIDTime(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mDiscreteBuffIDTime_m4172464861 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mChangeNewSkillID(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mChangeNewSkillID_m2086152661 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mChangeSkillPro(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mChangeSkillPro_m3471650149 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mReduceCDSkillID(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mReduceCDSkillID_m4207138518 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mThumpAddValue(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mThumpAddValue_m3028631175 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mShieldOverSkillID(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mShieldOverSkillID_m1159516460 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mCleaveSkillID(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mCleaveSkillID_m903697923 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mCleaveBuffID(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mCleaveBuffID_m2447981903 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mHpLineUpBuffID(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mHpLineUpBuffID_m30261132 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBuffCritPer(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBuffCritPer_m1793125771 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mBuffCritHurt(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mBuffCritHurt_m2786644501 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mAddHpNum(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mAddHpNum_m1218136564 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mHTBuffID(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mHTBuffID_m3648142103 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mDeathHarvestPer(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mDeathHarvestPer_m2908035219 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mHpLineCleaveAddBuff(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mHpLineCleaveAddBuff_m2567475735 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mResistanceCountryPer(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mResistanceCountryPer_m1143758673 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::BuffCtrl_mNDPAddBuff02(JSVCall)
extern "C"  void BuffCtrlGenerated_BuffCtrl_mNDPAddBuff02_m888741717 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_AddBuff__Int32__CombatEntity(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_AddBuff__Int32__CombatEntity_m3842011662 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_AddBuff__Int32(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_AddBuff__Int32_m2155794391 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_AddBuffState__Buff__EBuffState(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_AddBuffState__Buff__EBuffState_m499659558 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_AddSkillBuff__Skill__ESkillBuffEvent__Boolean(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_AddSkillBuff__Skill__ESkillBuffEvent__Boolean_m1871872864 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_AnitControlBuff(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_AnitControlBuff_m2633496541 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_buffClear(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_buffClear_m574630175 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_CachaBuffAction__Buff__CombatEntity(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_CachaBuffAction__Buff__CombatEntity_m705651478 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_CheckFierceWind__Skill(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_CheckFierceWind__Skill_m2506920780 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_CheckInfection__Buff(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_CheckInfection__Buff_m1008788507 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_CheckPlagueDust(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_CheckPlagueDust_m2168835313 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_CheckTimeBomb__Int32(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_CheckTimeBomb__Int32_m910663188 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_ClearBuffState(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_ClearBuffState_m2393772366 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_DalayAddBuff__Boolean__CombatEntity__Int32(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_DalayAddBuff__Boolean__CombatEntity__Int32_m506502713 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_FierceWindAction__BuffState(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_FierceWindAction__BuffState_m4188977715 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_FreezeBuff(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_FreezeBuff_m2793687527 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_GetBuffIconState(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_GetBuffIconState_m763619436 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_GetBuffState__EBuffState(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_GetBuffState__EBuffState_m1653466238 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_GetGodDownLastTime(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_GetGodDownLastTime_m3269911880 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_GetNDPBuffID__Int32(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_GetNDPBuffID__Int32_m3714356993 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_GetPetrifiedLastTime(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_GetPetrifiedLastTime_m2538137666 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_HasBuffState__EBuffState(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_HasBuffState__EBuffState_m4002807482 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_InfectionAction__Buff(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_InfectionAction__Buff_m4284300609 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_IsControlBuff__buffCfg(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_IsControlBuff__buffCfg_m3170064328 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_IsControlFun__Int32(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_IsControlFun__Int32_m3664638375 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_IsHaveBuff__Int32(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_IsHaveBuff__Int32_m2648467406 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_IsHaveFunBuff__Buff__EBuffFunType(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_IsHaveFunBuff__Buff__EBuffFunType_m314667801 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_IsHaveFunBuff__EBuffFunType(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_IsHaveFunBuff__EBuffFunType_m1428496198 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_MorphBuff(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_MorphBuff_m1858988544 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_PlagueDustAction__BuffState(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_PlagueDustAction__BuffState_m965987369 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_ReduceHpByBuff(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_ReduceHpByBuff_m2751952341 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_RemoveBuffByID__Int32(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_RemoveBuffByID__Int32_m2160603690 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_RemoveBuffState__Int32(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_RemoveBuffState__Int32_m516119121 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_RemoveBuffStateByID__Int32(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_RemoveBuffStateByID__Int32_m3113142975 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_RemoveBuffStateByState__EBuffState(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_RemoveBuffStateByState__EBuffState_m1858895734 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_RemoveNDPBuff__Int32(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_RemoveNDPBuff__Int32_m2205946818 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_TimeBombAction(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_TimeBombAction_m67233858 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::BuffCtrl_Update(JSVCall,System.Int32)
extern "C"  bool BuffCtrlGenerated_BuffCtrl_Update_m4080198630 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::__Register()
extern "C"  void BuffCtrlGenerated___Register_m4043845165 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_curnum1(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_curnum1_m2424986775 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::ilo_setInt322(System.Int32,System.Int32)
extern "C"  void BuffCtrlGenerated_ilo_setInt322_m4122531739 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mAnitNum3(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mAnitNum3_m3959183646 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::ilo_set_mAnitNum4(BuffCtrl,System.Int32)
extern "C"  void BuffCtrlGenerated_ilo_set_mAnitNum4_m2271247002 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mAuraAddHPRadius5(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mAuraAddHPRadius5_m1605702326 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mAuraReduceHPRadius6(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mAuraReduceHPRadius6_m1153845888 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrlGenerated::ilo_get_mGodDownTweenTime7(BuffCtrl)
extern "C"  float BuffCtrlGenerated_ilo_get_mGodDownTweenTime7_m484426764 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::ilo_setSingle8(System.Int32,System.Single)
extern "C"  void BuffCtrlGenerated_ilo_setSingle8_m3186692833 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrlGenerated::ilo_get_mPetrifiedChangeTime9(BuffCtrl)
extern "C"  float BuffCtrlGenerated_ilo_get_mPetrifiedChangeTime9_m3581652955 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mThumpSkillID10(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mThumpSkillID10_m1018868544 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mTankBuffID11(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mTankBuffID11_m3767119121 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mReduceRageHpNum12(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mReduceRageHpNum12_m886334265 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mTauntDis13(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mTauntDis13_m3251371035 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mBuffATNum14(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mBuffATNum14_m2501313360 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mBuffMDNum15(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mBuffMDNum15_m3637788717 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mBuffMaxHPNum16(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mBuffMaxHPNum16_m2275335051 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mBuffCritNum17(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mBuffCritNum17_m3952620844 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 BuffCtrlGenerated::ilo_get_mBuffRHurtNum18(BuffCtrl)
extern "C"  uint32_t BuffCtrlGenerated_ilo_get_mBuffRHurtNum18_m589188037 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mBuffMD19(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mBuffMD19_m2101692939 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mBuffDodge20(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mBuffDodge20_m4030186767 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mBuffWithstand21(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mBuffWithstand21_m2045873609 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mBuffCrit22(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mBuffCrit22_m3652253126 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 BuffCtrlGenerated::ilo_get_mBuffRHurt23(BuffCtrl)
extern "C"  uint32_t BuffCtrlGenerated_ilo_get_mBuffRHurt23_m2958726063 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrlGenerated::ilo_get_mHPSecDecPer24(BuffCtrl)
extern "C"  float BuffCtrlGenerated_ilo_get_mHPSecDecPer24_m1500342325 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrlGenerated::ilo_get_mModelScale25(BuffCtrl)
extern "C"  float BuffCtrlGenerated_ilo_get_mModelScale25_m3828067483 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrlGenerated::ilo_get_mHurtEffectReduction26(BuffCtrl)
extern "C"  float BuffCtrlGenerated_ilo_get_mHurtEffectReduction26_m2192371110 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mAngerSecDecNum27(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mAngerSecDecNum27_m3240881228 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrlGenerated::ilo_get_mHPRstNumATK28(BuffCtrl)
extern "C"  float BuffCtrlGenerated_ilo_get_mHPRstNumATK28_m3792255410 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrlGenerated::ilo_get_mRestrintPer29(BuffCtrl)
extern "C"  float BuffCtrlGenerated_ilo_get_mRestrintPer29_m2636211170 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mAtkPerHp30(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mAtkPerHp30_m139047971 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mBeattackedLaterSpeed31(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mBeattackedLaterSpeed31_m1251802492 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mDeathLaterSpeed32(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mDeathLaterSpeed32_m1085757275 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mAuraAddHPNum33(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mAuraAddHPNum33_m496886137 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mAuraReduceHPNum34(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mAuraReduceHPNum34_m3975071619 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mAddAngerRatio35(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mAddAngerRatio35_m3762399796 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::ilo_get_mMorph36(BuffCtrl)
extern "C"  bool BuffCtrlGenerated_ilo_get_mMorph36_m3996590290 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int32> BuffCtrlGenerated::ilo_get_mAddDamageSkillIDs37(BuffCtrl)
extern "C"  List_1_t2522024052 * BuffCtrlGenerated_ilo_get_mAddDamageSkillIDs37_m1463416116 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mRuneTakeEffectBuffID38(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mRuneTakeEffectBuffID38_m590228606 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mStable39(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mStable39_m2468805978 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mUnyielding40(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mUnyielding40_m2063808003 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mChangeNewSkillID41(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mChangeNewSkillID41_m2194300338 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mChangeSkillPro42(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mChangeSkillPro42_m1011935011 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mShieldOverSkillID43(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mShieldOverSkillID43_m2113636059 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_get_mBuffCritHurt44(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_get_mBuffCritHurt44_m3983048245 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::ilo_AddBuff45(BuffCtrl,System.Int32,CombatEntity)
extern "C"  void BuffCtrlGenerated_ilo_AddBuff45_m3934637140 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___id1, CombatEntity_t684137495 * ___src2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BuffCtrlGenerated::ilo_getObject46(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * BuffCtrlGenerated_ilo_getObject46_m425735160 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_getEnum47(System.Int32)
extern "C"  int32_t BuffCtrlGenerated_ilo_getEnum47_m3749987614 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::ilo_AnitControlBuff48(BuffCtrl)
extern "C"  void BuffCtrlGenerated_ilo_AnitControlBuff48_m3983570627 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::ilo_buffClear49(BuffCtrl)
extern "C"  void BuffCtrlGenerated_ilo_buffClear49_m2407434566 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::ilo_CheckPlagueDust50(BuffCtrl)
extern "C"  void BuffCtrlGenerated_ilo_CheckPlagueDust50_m3272205358 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::ilo_CheckTimeBomb51(BuffCtrl,System.Int32)
extern "C"  void BuffCtrlGenerated_ilo_CheckTimeBomb51_m805465501 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::ilo_DalayAddBuff52(BuffCtrl,System.Boolean,CombatEntity,System.Int32)
extern "C"  void BuffCtrlGenerated_ilo_DalayAddBuff52_m319675772 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, bool ___isHero1, CombatEntity_t684137495 * ___target2, int32_t ___buffID3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::ilo_FreezeBuff53(BuffCtrl)
extern "C"  void BuffCtrlGenerated_ilo_FreezeBuff53_m1071754887 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_GetBuffIconState54(BuffCtrl)
extern "C"  int32_t BuffCtrlGenerated_ilo_GetBuffIconState54_m784917823 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_GetNDPBuffID55(BuffCtrl,System.Int32)
extern "C"  int32_t BuffCtrlGenerated_ilo_GetNDPBuffID55_m1112970228 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::ilo_setBooleanS56(System.Int32,System.Boolean)
extern "C"  void BuffCtrlGenerated_ilo_setBooleanS56_m415151001 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::ilo_InfectionAction57(BuffCtrl,Buff)
extern "C"  void BuffCtrlGenerated_ilo_InfectionAction57_m2176870785 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, Buff_t2081907 * ___buff1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrlGenerated::ilo_IsControlFun58(BuffCtrl,System.Int32)
extern "C"  bool BuffCtrlGenerated_ilo_IsControlFun58_m179486173 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrlGenerated::ilo_getInt3259(System.Int32)
extern "C"  int32_t BuffCtrlGenerated_ilo_getInt3259_m743321596 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::ilo_MorphBuff60(BuffCtrl)
extern "C"  void BuffCtrlGenerated_ilo_MorphBuff60_m3182805340 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrlGenerated::ilo_RemoveBuffByID61(BuffCtrl,System.Int32)
extern "C"  void BuffCtrlGenerated_ilo_RemoveBuffByID61_m3064334356 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
