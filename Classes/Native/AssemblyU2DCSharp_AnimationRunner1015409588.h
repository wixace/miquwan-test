﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<AnimationRunner/AniType,System.String>
struct Dictionary_2_t141980025;
// UnityEngine.Animation[]
struct AnimationU5BU5D_t2624366687;
// System.String
struct String_t;
// CombatEntity
struct CombatEntity_t684137495;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// UnityEngine.AnimationState
struct AnimationState_t3682323633;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniState1127540784.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationRunner
struct  AnimationRunner_t1015409588  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Animation[] AnimationRunner::ani
	AnimationU5BU5D_t2624366687* ___ani_3;
	// System.String AnimationRunner::aniName
	String_t* ___aniName_4;
	// System.String AnimationRunner::curAniName
	String_t* ___curAniName_5;
	// CombatEntity AnimationRunner::unit
	CombatEntity_t684137495 * ___unit_6;
	// System.Collections.Generic.List`1<System.String> AnimationRunner::nameList
	List_1_t1375417109 * ___nameList_7;
	// UnityEngine.AnimationState AnimationRunner::curstate
	AnimationState_t3682323633 * ___curstate_8;
	// System.Single AnimationRunner::runSpeed
	float ___runSpeed_9;
	// System.Single AnimationRunner::_playTime
	float ____playTime_10;
	// System.Boolean AnimationRunner::isHaveAni
	bool ___isHaveAni_11;
	// System.Boolean AnimationRunner::isSkillFrozen
	bool ___isSkillFrozen_12;
	// AnimationRunner/AniState AnimationRunner::<aniState>k__BackingField
	int32_t ___U3CaniStateU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_ani_3() { return static_cast<int32_t>(offsetof(AnimationRunner_t1015409588, ___ani_3)); }
	inline AnimationU5BU5D_t2624366687* get_ani_3() const { return ___ani_3; }
	inline AnimationU5BU5D_t2624366687** get_address_of_ani_3() { return &___ani_3; }
	inline void set_ani_3(AnimationU5BU5D_t2624366687* value)
	{
		___ani_3 = value;
		Il2CppCodeGenWriteBarrier(&___ani_3, value);
	}

	inline static int32_t get_offset_of_aniName_4() { return static_cast<int32_t>(offsetof(AnimationRunner_t1015409588, ___aniName_4)); }
	inline String_t* get_aniName_4() const { return ___aniName_4; }
	inline String_t** get_address_of_aniName_4() { return &___aniName_4; }
	inline void set_aniName_4(String_t* value)
	{
		___aniName_4 = value;
		Il2CppCodeGenWriteBarrier(&___aniName_4, value);
	}

	inline static int32_t get_offset_of_curAniName_5() { return static_cast<int32_t>(offsetof(AnimationRunner_t1015409588, ___curAniName_5)); }
	inline String_t* get_curAniName_5() const { return ___curAniName_5; }
	inline String_t** get_address_of_curAniName_5() { return &___curAniName_5; }
	inline void set_curAniName_5(String_t* value)
	{
		___curAniName_5 = value;
		Il2CppCodeGenWriteBarrier(&___curAniName_5, value);
	}

	inline static int32_t get_offset_of_unit_6() { return static_cast<int32_t>(offsetof(AnimationRunner_t1015409588, ___unit_6)); }
	inline CombatEntity_t684137495 * get_unit_6() const { return ___unit_6; }
	inline CombatEntity_t684137495 ** get_address_of_unit_6() { return &___unit_6; }
	inline void set_unit_6(CombatEntity_t684137495 * value)
	{
		___unit_6 = value;
		Il2CppCodeGenWriteBarrier(&___unit_6, value);
	}

	inline static int32_t get_offset_of_nameList_7() { return static_cast<int32_t>(offsetof(AnimationRunner_t1015409588, ___nameList_7)); }
	inline List_1_t1375417109 * get_nameList_7() const { return ___nameList_7; }
	inline List_1_t1375417109 ** get_address_of_nameList_7() { return &___nameList_7; }
	inline void set_nameList_7(List_1_t1375417109 * value)
	{
		___nameList_7 = value;
		Il2CppCodeGenWriteBarrier(&___nameList_7, value);
	}

	inline static int32_t get_offset_of_curstate_8() { return static_cast<int32_t>(offsetof(AnimationRunner_t1015409588, ___curstate_8)); }
	inline AnimationState_t3682323633 * get_curstate_8() const { return ___curstate_8; }
	inline AnimationState_t3682323633 ** get_address_of_curstate_8() { return &___curstate_8; }
	inline void set_curstate_8(AnimationState_t3682323633 * value)
	{
		___curstate_8 = value;
		Il2CppCodeGenWriteBarrier(&___curstate_8, value);
	}

	inline static int32_t get_offset_of_runSpeed_9() { return static_cast<int32_t>(offsetof(AnimationRunner_t1015409588, ___runSpeed_9)); }
	inline float get_runSpeed_9() const { return ___runSpeed_9; }
	inline float* get_address_of_runSpeed_9() { return &___runSpeed_9; }
	inline void set_runSpeed_9(float value)
	{
		___runSpeed_9 = value;
	}

	inline static int32_t get_offset_of__playTime_10() { return static_cast<int32_t>(offsetof(AnimationRunner_t1015409588, ____playTime_10)); }
	inline float get__playTime_10() const { return ____playTime_10; }
	inline float* get_address_of__playTime_10() { return &____playTime_10; }
	inline void set__playTime_10(float value)
	{
		____playTime_10 = value;
	}

	inline static int32_t get_offset_of_isHaveAni_11() { return static_cast<int32_t>(offsetof(AnimationRunner_t1015409588, ___isHaveAni_11)); }
	inline bool get_isHaveAni_11() const { return ___isHaveAni_11; }
	inline bool* get_address_of_isHaveAni_11() { return &___isHaveAni_11; }
	inline void set_isHaveAni_11(bool value)
	{
		___isHaveAni_11 = value;
	}

	inline static int32_t get_offset_of_isSkillFrozen_12() { return static_cast<int32_t>(offsetof(AnimationRunner_t1015409588, ___isSkillFrozen_12)); }
	inline bool get_isSkillFrozen_12() const { return ___isSkillFrozen_12; }
	inline bool* get_address_of_isSkillFrozen_12() { return &___isSkillFrozen_12; }
	inline void set_isSkillFrozen_12(bool value)
	{
		___isSkillFrozen_12 = value;
	}

	inline static int32_t get_offset_of_U3CaniStateU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(AnimationRunner_t1015409588, ___U3CaniStateU3Ek__BackingField_13)); }
	inline int32_t get_U3CaniStateU3Ek__BackingField_13() const { return ___U3CaniStateU3Ek__BackingField_13; }
	inline int32_t* get_address_of_U3CaniStateU3Ek__BackingField_13() { return &___U3CaniStateU3Ek__BackingField_13; }
	inline void set_U3CaniStateU3Ek__BackingField_13(int32_t value)
	{
		___U3CaniStateU3Ek__BackingField_13 = value;
	}
};

struct AnimationRunner_t1015409588_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<AnimationRunner/AniType,System.String> AnimationRunner::AniTypeDic
	Dictionary_2_t141980025 * ___AniTypeDic_2;

public:
	inline static int32_t get_offset_of_AniTypeDic_2() { return static_cast<int32_t>(offsetof(AnimationRunner_t1015409588_StaticFields, ___AniTypeDic_2)); }
	inline Dictionary_2_t141980025 * get_AniTypeDic_2() const { return ___AniTypeDic_2; }
	inline Dictionary_2_t141980025 ** get_address_of_AniTypeDic_2() { return &___AniTypeDic_2; }
	inline void set_AniTypeDic_2(Dictionary_2_t141980025 * value)
	{
		___AniTypeDic_2 = value;
		Il2CppCodeGenWriteBarrier(&___AniTypeDic_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
