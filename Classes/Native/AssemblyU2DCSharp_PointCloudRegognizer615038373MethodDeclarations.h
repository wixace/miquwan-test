﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PointCloudRegognizer
struct PointCloudRegognizer_t615038373;
// PointCloudRegognizer/NormalizedTemplate
struct NormalizedTemplate_t2023934299;
// PointCloudGestureTemplate
struct PointCloudGestureTemplate_t3506552702;
// System.Collections.Generic.List`1<PointCloudRegognizer/Point>
struct List_1_t3207017302;
// PointCloudGesture
struct PointCloudGesture_t1959506660;
// FingerGestures/IFingerList
struct IFingerList_t1026994100;
// System.String
struct String_t;
// Gesture
struct Gesture_t1589572905;
// PointCloudRegognizer/GestureNormalizer
struct GestureNormalizer_t660715300;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PointCloudGestureTemplate3506552702.h"
#include "AssemblyU2DCSharp_PointCloudGesture1959506660.h"
#include "AssemblyU2DCSharp_GestureRecognitionState3604239971.h"
#include "AssemblyU2DCSharp_PointCloudRegognizer615038373.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_Gesture1589572905.h"
#include "AssemblyU2DCSharp_PointCloudRegognizer_GestureNorma660715300.h"

// System.Void PointCloudRegognizer::.ctor()
extern "C"  void PointCloudRegognizer__ctor_m1532634774 (PointCloudRegognizer_t615038373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PointCloudRegognizer::.cctor()
extern "C"  void PointCloudRegognizer__cctor_m4079908823 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PointCloudRegognizer::Awake()
extern "C"  void PointCloudRegognizer_Awake_m1770239993 (PointCloudRegognizer_t615038373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PointCloudRegognizer/NormalizedTemplate PointCloudRegognizer::FindNormalizedTemplate(PointCloudGestureTemplate)
extern "C"  NormalizedTemplate_t2023934299 * PointCloudRegognizer_FindNormalizedTemplate_m2151274244 (PointCloudRegognizer_t615038373 * __this, PointCloudGestureTemplate_t3506552702 * ___template0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PointCloudRegognizer/Point> PointCloudRegognizer::Normalize(System.Collections.Generic.List`1<PointCloudRegognizer/Point>)
extern "C"  List_1_t3207017302 * PointCloudRegognizer_Normalize_m586181104 (PointCloudRegognizer_t615038373 * __this, List_1_t3207017302 * ___points0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PointCloudRegognizer::AddTemplate(PointCloudGestureTemplate)
extern "C"  bool PointCloudRegognizer_AddTemplate_m22426905 (PointCloudRegognizer_t615038373 * __this, PointCloudGestureTemplate_t3506552702 * ___template0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PointCloudRegognizer::OnBegin(PointCloudGesture,FingerGestures/IFingerList)
extern "C"  void PointCloudRegognizer_OnBegin_m842134274 (PointCloudRegognizer_t615038373 * __this, PointCloudGesture_t1959506660 * ___gesture0, Il2CppObject * ___touches1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PointCloudRegognizer::RecognizePointCloud(PointCloudGesture)
extern "C"  bool PointCloudRegognizer_RecognizePointCloud_m4170662651 (PointCloudRegognizer_t615038373 * __this, PointCloudGesture_t1959506660 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single PointCloudRegognizer::GreedyCloudMatch(System.Collections.Generic.List`1<PointCloudRegognizer/Point>,System.Collections.Generic.List`1<PointCloudRegognizer/Point>)
extern "C"  float PointCloudRegognizer_GreedyCloudMatch_m531174752 (PointCloudRegognizer_t615038373 * __this, List_1_t3207017302 * ___points0, List_1_t3207017302 * ___refPoints1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single PointCloudRegognizer::CloudDistance(System.Collections.Generic.List`1<PointCloudRegognizer/Point>,System.Collections.Generic.List`1<PointCloudRegognizer/Point>,System.Int32)
extern "C"  float PointCloudRegognizer_CloudDistance_m2680430367 (Il2CppObject * __this /* static, unused */, List_1_t3207017302 * ___points10, List_1_t3207017302 * ___points21, int32_t ___startIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PointCloudRegognizer::ResetMatched(System.Int32)
extern "C"  void PointCloudRegognizer_ResetMatched_m2074903380 (Il2CppObject * __this /* static, unused */, int32_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GestureRecognitionState PointCloudRegognizer::OnRecognize(PointCloudGesture,FingerGestures/IFingerList)
extern "C"  int32_t PointCloudRegognizer_OnRecognize_m255479063 (PointCloudRegognizer_t615038373 * __this, PointCloudGesture_t1959506660 * ___gesture0, Il2CppObject * ___touches1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PointCloudRegognizer::GetDefaultEventMessageName()
extern "C"  String_t* PointCloudRegognizer_GetDefaultEventMessageName_m3168427828 (PointCloudRegognizer_t615038373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PointCloudRegognizer::OnDrawGizmosSelected()
extern "C"  void PointCloudRegognizer_OnDrawGizmosSelected_m2474258629 (PointCloudRegognizer_t615038373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PointCloudRegognizer::DrawNormalizedPointCloud(System.Collections.Generic.List`1<PointCloudRegognizer/Point>,System.Single)
extern "C"  void PointCloudRegognizer_DrawNormalizedPointCloud_m3663320047 (PointCloudRegognizer_t615038373 * __this, List_1_t3207017302 * ___points0, float ___scale1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PointCloudRegognizer::ilo_AddTemplate1(PointCloudRegognizer,PointCloudGestureTemplate)
extern "C"  bool PointCloudRegognizer_ilo_AddTemplate1_m1593173780 (Il2CppObject * __this /* static, unused */, PointCloudRegognizer_t615038373 * ____this0, PointCloudGestureTemplate_t3506552702 * ___template1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PointCloudRegognizer::ilo_GetStrokeId2(PointCloudGestureTemplate,System.Int32)
extern "C"  int32_t PointCloudRegognizer_ilo_GetStrokeId2_m3442963579 (Il2CppObject * __this /* static, unused */, PointCloudGestureTemplate_t3506552702 * ____this0, int32_t ___pointIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 PointCloudRegognizer::ilo_GetPosition3(PointCloudGestureTemplate,System.Int32)
extern "C"  Vector2_t4282066565  PointCloudRegognizer_ilo_GetPosition3_m1866407187 (Il2CppObject * __this /* static, unused */, PointCloudGestureTemplate_t3506552702 * ____this0, int32_t ___pointIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PointCloudRegognizer::ilo_get_PointCount4(PointCloudGestureTemplate)
extern "C"  int32_t PointCloudRegognizer_ilo_get_PointCount4_m481823541 (Il2CppObject * __this /* static, unused */, PointCloudGestureTemplate_t3506552702 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PointCloudRegognizer/Point> PointCloudRegognizer::ilo_Normalize5(PointCloudRegognizer,System.Collections.Generic.List`1<PointCloudRegognizer/Point>)
extern "C"  List_1_t3207017302 * PointCloudRegognizer_ilo_Normalize5_m2040002159 (Il2CppObject * __this /* static, unused */, PointCloudRegognizer_t615038373 * ____this0, List_1_t3207017302 * ___points1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 PointCloudRegognizer::ilo_GetAverageStartPosition6(FingerGestures/IFingerList)
extern "C"  Vector2_t4282066565  PointCloudRegognizer_ilo_GetAverageStartPosition6_m1733384442 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 PointCloudRegognizer::ilo_get_Position7(Gesture)
extern "C"  Vector2_t4282066565  PointCloudRegognizer_ilo_get_Position7_m1413514014 (Il2CppObject * __this /* static, unused */, Gesture_t1589572905 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PointCloudRegognizer/Point> PointCloudRegognizer::ilo_Apply8(PointCloudRegognizer/GestureNormalizer,System.Collections.Generic.List`1<PointCloudRegognizer/Point>,System.Int32)
extern "C"  List_1_t3207017302 * PointCloudRegognizer_ilo_Apply8_m821578373 (Il2CppObject * __this /* static, unused */, GestureNormalizer_t660715300 * ____this0, List_1_t3207017302 * ___inputPoints1, int32_t ___normalizedPointsCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single PointCloudRegognizer::ilo_CloudDistance9(System.Collections.Generic.List`1<PointCloudRegognizer/Point>,System.Collections.Generic.List`1<PointCloudRegognizer/Point>,System.Int32)
extern "C"  float PointCloudRegognizer_ilo_CloudDistance9_m1144638089 (Il2CppObject * __this /* static, unused */, List_1_t3207017302 * ___points10, List_1_t3207017302 * ___points21, int32_t ___startIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
