﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Seeker
struct Seeker_t2472610117;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.MonoModifier
struct  MonoModifier_t200043088  : public MonoBehaviour_t667441552
{
public:
	// Seeker Pathfinding.MonoModifier::seeker
	Seeker_t2472610117 * ___seeker_2;
	// System.Int32 Pathfinding.MonoModifier::priority
	int32_t ___priority_3;

public:
	inline static int32_t get_offset_of_seeker_2() { return static_cast<int32_t>(offsetof(MonoModifier_t200043088, ___seeker_2)); }
	inline Seeker_t2472610117 * get_seeker_2() const { return ___seeker_2; }
	inline Seeker_t2472610117 ** get_address_of_seeker_2() { return &___seeker_2; }
	inline void set_seeker_2(Seeker_t2472610117 * value)
	{
		___seeker_2 = value;
		Il2CppCodeGenWriteBarrier(&___seeker_2, value);
	}

	inline static int32_t get_offset_of_priority_3() { return static_cast<int32_t>(offsetof(MonoModifier_t200043088, ___priority_3)); }
	inline int32_t get_priority_3() const { return ___priority_3; }
	inline int32_t* get_address_of_priority_3() { return &___priority_3; }
	inline void set_priority_3(int32_t value)
	{
		___priority_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
