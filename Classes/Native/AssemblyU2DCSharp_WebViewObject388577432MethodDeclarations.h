﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebViewObject
struct WebViewObject_t388577432;
// System.String
struct String_t;
// System.Action`1<System.String>
struct Action_1_t403047693;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_WebViewObject388577432.h"

// System.Void WebViewObject::.ctor()
extern "C"  void WebViewObject__ctor_m3926610899 (WebViewObject_t388577432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebViewObject::get_IsKeyboardVisible()
extern "C"  bool WebViewObject_get_IsKeyboardVisible_m3587886589 (WebViewObject_t388577432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr WebViewObject::_CWebViewPlugin_Init(System.String,System.Boolean,System.String,System.Boolean)
extern "C"  IntPtr_t WebViewObject__CWebViewPlugin_Init_m3645428771 (Il2CppObject * __this /* static, unused */, String_t* ___gameObject0, bool ___transparent1, String_t* ___ua2, bool ___enableWKWebView3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebViewObject::_CWebViewPlugin_Destroy(System.IntPtr)
extern "C"  int32_t WebViewObject__CWebViewPlugin_Destroy_m2353415506 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::_CWebViewPlugin_SetMargins(System.IntPtr,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern "C"  void WebViewObject__CWebViewPlugin_SetMargins_m266041152 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, float ___left1, float ___top2, float ___right3, float ___bottom4, bool ___relative5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::_CWebViewPlugin_SetVisibility(System.IntPtr,System.Boolean)
extern "C"  void WebViewObject__CWebViewPlugin_SetVisibility_m4275967415 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, bool ___visibility1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::_CWebViewPlugin_SetAlertDialogEnabled(System.IntPtr,System.Boolean)
extern "C"  void WebViewObject__CWebViewPlugin_SetAlertDialogEnabled_m2948887362 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, bool ___enabled1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::_CWebViewPlugin_SetScrollBounceEnabled(System.IntPtr,System.Boolean)
extern "C"  void WebViewObject__CWebViewPlugin_SetScrollBounceEnabled_m800673115 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, bool ___enabled1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebViewObject::_CWebViewPlugin_SetURLPattern(System.IntPtr,System.String,System.String)
extern "C"  bool WebViewObject__CWebViewPlugin_SetURLPattern_m1569047259 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, String_t* ___allowPattern1, String_t* ___denyPattern2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::_CWebViewPlugin_LoadURL(System.IntPtr,System.String)
extern "C"  void WebViewObject__CWebViewPlugin_LoadURL_m1779362445 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, String_t* ___url1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::_CWebViewPlugin_LoadHTML(System.IntPtr,System.String,System.String)
extern "C"  void WebViewObject__CWebViewPlugin_LoadHTML_m2784603187 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, String_t* ___html1, String_t* ___baseUrl2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::_CWebViewPlugin_EvaluateJS(System.IntPtr,System.String)
extern "C"  void WebViewObject__CWebViewPlugin_EvaluateJS_m3707640198 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, String_t* ___url1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebViewObject::_CWebViewPlugin_Progress(System.IntPtr)
extern "C"  int32_t WebViewObject__CWebViewPlugin_Progress_m3158698669 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebViewObject::_CWebViewPlugin_CanGoBack(System.IntPtr)
extern "C"  bool WebViewObject__CWebViewPlugin_CanGoBack_m244910887 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebViewObject::_CWebViewPlugin_CanGoForward(System.IntPtr)
extern "C"  bool WebViewObject__CWebViewPlugin_CanGoForward_m583277491 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::_CWebViewPlugin_GoBack(System.IntPtr)
extern "C"  void WebViewObject__CWebViewPlugin_GoBack_m3019603293 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::_CWebViewPlugin_GoForward(System.IntPtr)
extern "C"  void WebViewObject__CWebViewPlugin_GoForward_m504165821 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::_CWebViewPlugin_AddCustomHeader(System.IntPtr,System.String,System.String)
extern "C"  void WebViewObject__CWebViewPlugin_AddCustomHeader_m4125479251 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, String_t* ___headerKey1, String_t* ___headerValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebViewObject::_CWebViewPlugin_GetCustomHeaderValue(System.IntPtr,System.String)
extern "C"  String_t* WebViewObject__CWebViewPlugin_GetCustomHeaderValue_m3717584366 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, String_t* ___headerKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::_CWebViewPlugin_RemoveCustomHeader(System.IntPtr,System.String)
extern "C"  void WebViewObject__CWebViewPlugin_RemoveCustomHeader_m2515269670 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, String_t* ___headerKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::_CWebViewPlugin_ClearCustomHeader(System.IntPtr)
extern "C"  void WebViewObject__CWebViewPlugin_ClearCustomHeader_m3490781551 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::_CWebViewPlugin_ClearCookies()
extern "C"  void WebViewObject__CWebViewPlugin_ClearCookies_m2847599594 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebViewObject::_CWebViewPlugin_GetCookies(System.String)
extern "C"  String_t* WebViewObject__CWebViewPlugin_GetCookies_m2189286148 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebViewObject::IsWebViewAvailable()
extern "C"  bool WebViewObject_IsWebViewAvailable_m1603590487 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::Init(System.Action`1<System.String>,System.Boolean,System.String,System.Action`1<System.String>,System.Action`1<System.String>,System.Action`1<System.String>,System.Boolean,System.Action`1<System.String>)
extern "C"  void WebViewObject_Init_m778993995 (WebViewObject_t388577432 * __this, Action_1_t403047693 * ___cb0, bool ___transparent1, String_t* ___ua2, Action_1_t403047693 * ___err3, Action_1_t403047693 * ___httpErr4, Action_1_t403047693 * ___ld5, bool ___enableWKWebView6, Action_1_t403047693 * ___started7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::OnDestroy()
extern "C"  void WebViewObject_OnDestroy_m429459276 (WebViewObject_t388577432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::SetCenterPositionWithScale(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void WebViewObject_SetCenterPositionWithScale_m3605192217 (WebViewObject_t388577432 * __this, Vector2_t4282066565  ___center0, Vector2_t4282066565  ___scale1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::SetMargins(System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean)
extern "C"  void WebViewObject_SetMargins_m3316795595 (WebViewObject_t388577432 * __this, int32_t ___left0, int32_t ___top1, int32_t ___right2, int32_t ___bottom3, bool ___relative4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::SetVisibility(System.Boolean)
extern "C"  void WebViewObject_SetVisibility_m927614396 (WebViewObject_t388577432 * __this, bool ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebViewObject::GetVisibility()
extern "C"  bool WebViewObject_GetVisibility_m1451916717 (WebViewObject_t388577432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::SetAlertDialogEnabled(System.Boolean)
extern "C"  void WebViewObject_SetAlertDialogEnabled_m4000955271 (WebViewObject_t388577432 * __this, bool ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebViewObject::GetAlertDialogEnabled()
extern "C"  bool WebViewObject_GetAlertDialogEnabled_m2952395448 (WebViewObject_t388577432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::SetScrollBounceEnabled(System.Boolean)
extern "C"  void WebViewObject_SetScrollBounceEnabled_m3878304978 (WebViewObject_t388577432 * __this, bool ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebViewObject::GetScrollBounceEnabled()
extern "C"  bool WebViewObject_GetScrollBounceEnabled_m1726131955 (WebViewObject_t388577432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebViewObject::SetURLPattern(System.String,System.String)
extern "C"  bool WebViewObject_SetURLPattern_m2746814902 (WebViewObject_t388577432 * __this, String_t* ___allowPattern0, String_t* ___denyPattern1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::LoadURL(System.String)
extern "C"  void WebViewObject_LoadURL_m3283687400 (WebViewObject_t388577432 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::LoadHTML(System.String,System.String)
extern "C"  void WebViewObject_LoadHTML_m3362611740 (WebViewObject_t388577432 * __this, String_t* ___html0, String_t* ___baseUrl1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::EvaluateJS(System.String)
extern "C"  void WebViewObject_EvaluateJS_m3850448815 (WebViewObject_t388577432 * __this, String_t* ___js0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebViewObject::Progress()
extern "C"  int32_t WebViewObject_Progress_m52699056 (WebViewObject_t388577432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebViewObject::CanGoBack()
extern "C"  bool WebViewObject_CanGoBack_m3206585540 (WebViewObject_t388577432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebViewObject::CanGoForward()
extern "C"  bool WebViewObject_CanGoForward_m3837965418 (WebViewObject_t388577432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::GoBack()
extern "C"  void WebViewObject_GoBack_m2701779264 (WebViewObject_t388577432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::GoForward()
extern "C"  void WebViewObject_GoForward_m1834700398 (WebViewObject_t388577432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::CallOnError(System.String)
extern "C"  void WebViewObject_CallOnError_m3400224550 (WebViewObject_t388577432 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::CallOnHttpError(System.String)
extern "C"  void WebViewObject_CallOnHttpError_m1059842382 (WebViewObject_t388577432 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::CallOnStarted(System.String)
extern "C"  void WebViewObject_CallOnStarted_m1338740813 (WebViewObject_t388577432 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::CallOnLoaded(System.String)
extern "C"  void WebViewObject_CallOnLoaded_m2319158287 (WebViewObject_t388577432 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::CallFromJS(System.String)
extern "C"  void WebViewObject_CallFromJS_m2466694400 (WebViewObject_t388577432 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::AddCustomHeader(System.String,System.String)
extern "C"  void WebViewObject_AddCustomHeader_m1589785582 (WebViewObject_t388577432 * __this, String_t* ___headerKey0, String_t* ___headerValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebViewObject::GetCustomHeaderValue(System.String)
extern "C"  String_t* WebViewObject_GetCustomHeaderValue_m2492223319 (WebViewObject_t388577432 * __this, String_t* ___headerKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::RemoveCustomHeader(System.String)
extern "C"  void WebViewObject_RemoveCustomHeader_m2263678799 (WebViewObject_t388577432 * __this, String_t* ___headerKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::ClearCustomHeader()
extern "C"  void WebViewObject_ClearCustomHeader_m1457957500 (WebViewObject_t388577432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::ClearCookies()
extern "C"  void WebViewObject_ClearCookies_m1824984595 (WebViewObject_t388577432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebViewObject::GetCookies(System.String)
extern "C"  String_t* WebViewObject_GetCookies_m2763561787 (WebViewObject_t388577432 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebViewObject::ilo__CWebViewPlugin_Destroy1(System.IntPtr)
extern "C"  int32_t WebViewObject_ilo__CWebViewPlugin_Destroy1_m1563799440 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::ilo_SetMargins2(WebViewObject,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean)
extern "C"  void WebViewObject_ilo_SetMargins2_m2098137360 (Il2CppObject * __this /* static, unused */, WebViewObject_t388577432 * ____this0, int32_t ___left1, int32_t ___top2, int32_t ___right3, int32_t ___bottom4, bool ___relative5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::ilo__CWebViewPlugin_SetMargins3(System.IntPtr,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern "C"  void WebViewObject_ilo__CWebViewPlugin_SetMargins3_m2623617868 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, float ___left1, float ___top2, float ___right3, float ___bottom4, bool ___relative5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::ilo__CWebViewPlugin_SetVisibility4(System.IntPtr,System.Boolean)
extern "C"  void WebViewObject_ilo__CWebViewPlugin_SetVisibility4_m3188143396 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, bool ___visibility1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::ilo__CWebViewPlugin_SetAlertDialogEnabled5(System.IntPtr,System.Boolean)
extern "C"  void WebViewObject_ilo__CWebViewPlugin_SetAlertDialogEnabled5_m1365241402 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, bool ___enabled1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::ilo__CWebViewPlugin_SetScrollBounceEnabled6(System.IntPtr,System.Boolean)
extern "C"  void WebViewObject_ilo__CWebViewPlugin_SetScrollBounceEnabled6_m4099652732 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, bool ___enabled1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebViewObject::ilo__CWebViewPlugin_SetURLPattern7(System.IntPtr,System.String,System.String)
extern "C"  bool WebViewObject_ilo__CWebViewPlugin_SetURLPattern7_m375901073 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, String_t* ___allowPattern1, String_t* ___denyPattern2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::ilo__CWebViewPlugin_AddCustomHeader8(System.IntPtr,System.String,System.String)
extern "C"  void WebViewObject_ilo__CWebViewPlugin_AddCustomHeader8_m2846119992 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, String_t* ___headerKey1, String_t* ___headerValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebViewObject::ilo__CWebViewPlugin_ClearCustomHeader9(System.IntPtr)
extern "C"  void WebViewObject_ilo__CWebViewPlugin_ClearCustomHeader9_m1062035659 (Il2CppObject * __this /* static, unused */, IntPtr_t ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebViewObject::ilo__CWebViewPlugin_GetCookies10(System.String)
extern "C"  String_t* WebViewObject_ilo__CWebViewPlugin_GetCookies10_m2311722162 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
