﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<HatredCtrl/stValue>
struct Comparer_1_t1382731401;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.Comparer`1<HatredCtrl/stValue>::.ctor()
extern "C"  void Comparer_1__ctor_m13790279_gshared (Comparer_1_t1382731401 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m13790279(__this, method) ((  void (*) (Comparer_1_t1382731401 *, const MethodInfo*))Comparer_1__ctor_m13790279_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<HatredCtrl/stValue>::.cctor()
extern "C"  void Comparer_1__cctor_m4240369734_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m4240369734(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m4240369734_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<HatredCtrl/stValue>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m1895465644_gshared (Comparer_1_t1382731401 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m1895465644(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t1382731401 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m1895465644_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<HatredCtrl/stValue>::get_Default()
extern "C"  Comparer_1_t1382731401 * Comparer_1_get_Default_m4166414255_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m4166414255(__this /* static, unused */, method) ((  Comparer_1_t1382731401 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m4166414255_gshared)(__this /* static, unused */, method)
