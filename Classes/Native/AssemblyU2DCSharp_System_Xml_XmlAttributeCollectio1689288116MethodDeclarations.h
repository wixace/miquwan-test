﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Xml_XmlAttributeCollectionGenerated
struct System_Xml_XmlAttributeCollectionGenerated_t1689288116;
// JSVCall
struct JSVCall_t3708497963;
// System.Xml.XmlAttribute[]
struct XmlAttributeU5BU5D_t3152553490;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Xml_XmlAttributeCollectionGenerated::.ctor()
extern "C"  void System_Xml_XmlAttributeCollectionGenerated__ctor_m2700404071 (System_Xml_XmlAttributeCollectionGenerated_t1689288116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlAttributeCollectionGenerated::XmlAttributeCollection_ItemOf_String(JSVCall)
extern "C"  void System_Xml_XmlAttributeCollectionGenerated_XmlAttributeCollection_ItemOf_String_m170105888 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlAttributeCollectionGenerated::XmlAttributeCollection_ItemOf_Int32(JSVCall)
extern "C"  void System_Xml_XmlAttributeCollectionGenerated_XmlAttributeCollection_ItemOf_Int32_m319415757 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlAttributeCollectionGenerated::XmlAttributeCollection_ItemOf_String_String(JSVCall)
extern "C"  void System_Xml_XmlAttributeCollectionGenerated_XmlAttributeCollection_ItemOf_String_String_m2359111260 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlAttributeCollectionGenerated::XmlAttributeCollection_Append__XmlAttribute(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlAttributeCollectionGenerated_XmlAttributeCollection_Append__XmlAttribute_m1728799004 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlAttributeCollectionGenerated::XmlAttributeCollection_CopyTo__XmlAttribute_Array__Int32(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlAttributeCollectionGenerated_XmlAttributeCollection_CopyTo__XmlAttribute_Array__Int32_m2604351748 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlAttributeCollectionGenerated::XmlAttributeCollection_InsertAfter__XmlAttribute__XmlAttribute(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlAttributeCollectionGenerated_XmlAttributeCollection_InsertAfter__XmlAttribute__XmlAttribute_m3534255698 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlAttributeCollectionGenerated::XmlAttributeCollection_InsertBefore__XmlAttribute__XmlAttribute(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlAttributeCollectionGenerated_XmlAttributeCollection_InsertBefore__XmlAttribute__XmlAttribute_m186353759 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlAttributeCollectionGenerated::XmlAttributeCollection_Prepend__XmlAttribute(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlAttributeCollectionGenerated_XmlAttributeCollection_Prepend__XmlAttribute_m1610218040 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlAttributeCollectionGenerated::XmlAttributeCollection_Remove__XmlAttribute(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlAttributeCollectionGenerated_XmlAttributeCollection_Remove__XmlAttribute_m3436092806 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlAttributeCollectionGenerated::XmlAttributeCollection_RemoveAll(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlAttributeCollectionGenerated_XmlAttributeCollection_RemoveAll_m422176738 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlAttributeCollectionGenerated::XmlAttributeCollection_RemoveAt__Int32(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlAttributeCollectionGenerated_XmlAttributeCollection_RemoveAt__Int32_m2230848956 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlAttributeCollectionGenerated::XmlAttributeCollection_SetNamedItem__XmlNode(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlAttributeCollectionGenerated_XmlAttributeCollection_SetNamedItem__XmlNode_m2061836340 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlAttributeCollectionGenerated::__Register()
extern "C"  void System_Xml_XmlAttributeCollectionGenerated___Register_m2998329280 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlAttribute[] System_Xml_XmlAttributeCollectionGenerated::<XmlAttributeCollection_CopyTo__XmlAttribute_Array__Int32>m__F4()
extern "C"  XmlAttributeU5BU5D_t3152553490* System_Xml_XmlAttributeCollectionGenerated_U3CXmlAttributeCollection_CopyTo__XmlAttribute_Array__Int32U3Em__F4_m2727181419 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Xml_XmlAttributeCollectionGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_Xml_XmlAttributeCollectionGenerated_ilo_setObject1_m3445107203 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Xml_XmlAttributeCollectionGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_Xml_XmlAttributeCollectionGenerated_ilo_getObject2_m4015464041 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Xml_XmlAttributeCollectionGenerated::ilo_getObject3(System.Int32)
extern "C"  int32_t System_Xml_XmlAttributeCollectionGenerated_ilo_getObject3_m776359693 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
