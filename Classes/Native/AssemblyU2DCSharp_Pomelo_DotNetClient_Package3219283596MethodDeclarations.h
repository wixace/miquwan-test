﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pomelo.DotNetClient.Package
struct Package_t3219283596;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_PackageType3964380262.h"

// System.Void Pomelo.DotNetClient.Package::.ctor(Pomelo.DotNetClient.PackageType,System.Byte[])
extern "C"  void Package__ctor_m1912500912 (Package_t3219283596 * __this, int32_t ___type0, ByteU5BU5D_t4260760469* ___body1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
