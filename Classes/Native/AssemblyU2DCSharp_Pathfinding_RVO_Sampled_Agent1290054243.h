﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.RVO.Sampled.Agent
struct Agent_t1290054243;
// Pathfinding.RVO.Simulator
struct Simulator_t2705969170;
// System.Collections.Generic.List`1<Pathfinding.RVO.Sampled.Agent>
struct List_1_t2658239795;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1365137228;
// System.Collections.Generic.List`1<Pathfinding.RVO.ObstacleVertex>
struct List_1_t1243525355;
// System.Diagnostics.Stopwatch
struct Stopwatch_t3420517611;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_RVOLayer1386980398.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RVO.Sampled.Agent
struct  Agent_t1290054243  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 Pathfinding.RVO.Sampled.Agent::smoothPos
	Vector3_t4282066566  ___smoothPos_1;
	// System.Single Pathfinding.RVO.Sampled.Agent::radius
	float ___radius_2;
	// System.Single Pathfinding.RVO.Sampled.Agent::height
	float ___height_3;
	// System.Single Pathfinding.RVO.Sampled.Agent::maxSpeed
	float ___maxSpeed_4;
	// System.Single Pathfinding.RVO.Sampled.Agent::neighbourDist
	float ___neighbourDist_5;
	// System.Single Pathfinding.RVO.Sampled.Agent::agentTimeHorizon
	float ___agentTimeHorizon_6;
	// System.Single Pathfinding.RVO.Sampled.Agent::obstacleTimeHorizon
	float ___obstacleTimeHorizon_7;
	// System.Single Pathfinding.RVO.Sampled.Agent::weight
	float ___weight_8;
	// System.Boolean Pathfinding.RVO.Sampled.Agent::locked
	bool ___locked_9;
	// Pathfinding.RVO.RVOLayer Pathfinding.RVO.Sampled.Agent::layer
	int32_t ___layer_10;
	// Pathfinding.RVO.RVOLayer Pathfinding.RVO.Sampled.Agent::collidesWith
	int32_t ___collidesWith_11;
	// System.Int32 Pathfinding.RVO.Sampled.Agent::maxNeighbours
	int32_t ___maxNeighbours_12;
	// UnityEngine.Vector3 Pathfinding.RVO.Sampled.Agent::position
	Vector3_t4282066566  ___position_13;
	// UnityEngine.Vector3 Pathfinding.RVO.Sampled.Agent::desiredVelocity
	Vector3_t4282066566  ___desiredVelocity_14;
	// UnityEngine.Vector3 Pathfinding.RVO.Sampled.Agent::prevSmoothPos
	Vector3_t4282066566  ___prevSmoothPos_15;
	// Pathfinding.RVO.Sampled.Agent Pathfinding.RVO.Sampled.Agent::next
	Agent_t1290054243 * ___next_16;
	// UnityEngine.Vector3 Pathfinding.RVO.Sampled.Agent::velocity
	Vector3_t4282066566  ___velocity_17;
	// UnityEngine.Vector3 Pathfinding.RVO.Sampled.Agent::newVelocity
	Vector3_t4282066566  ___newVelocity_18;
	// Pathfinding.RVO.Simulator Pathfinding.RVO.Sampled.Agent::simulator
	Simulator_t2705969170 * ___simulator_19;
	// System.Collections.Generic.List`1<Pathfinding.RVO.Sampled.Agent> Pathfinding.RVO.Sampled.Agent::neighbours
	List_1_t2658239795 * ___neighbours_20;
	// System.Collections.Generic.List`1<System.Single> Pathfinding.RVO.Sampled.Agent::neighbourDists
	List_1_t1365137228 * ___neighbourDists_21;
	// System.Collections.Generic.List`1<Pathfinding.RVO.ObstacleVertex> Pathfinding.RVO.Sampled.Agent::obstaclesBuffered
	List_1_t1243525355 * ___obstaclesBuffered_22;
	// System.Collections.Generic.List`1<Pathfinding.RVO.ObstacleVertex> Pathfinding.RVO.Sampled.Agent::obstacles
	List_1_t1243525355 * ___obstacles_23;
	// System.Collections.Generic.List`1<System.Single> Pathfinding.RVO.Sampled.Agent::obstacleDists
	List_1_t1365137228 * ___obstacleDists_24;
	// UnityEngine.Vector3 Pathfinding.RVO.Sampled.Agent::<Position>k__BackingField
	Vector3_t4282066566  ___U3CPositionU3Ek__BackingField_30;
	// UnityEngine.Vector3 Pathfinding.RVO.Sampled.Agent::<DesiredVelocity>k__BackingField
	Vector3_t4282066566  ___U3CDesiredVelocityU3Ek__BackingField_31;
	// Pathfinding.RVO.RVOLayer Pathfinding.RVO.Sampled.Agent::<Layer>k__BackingField
	int32_t ___U3CLayerU3Ek__BackingField_32;
	// Pathfinding.RVO.RVOLayer Pathfinding.RVO.Sampled.Agent::<CollidesWith>k__BackingField
	int32_t ___U3CCollidesWithU3Ek__BackingField_33;
	// System.Boolean Pathfinding.RVO.Sampled.Agent::<Locked>k__BackingField
	bool ___U3CLockedU3Ek__BackingField_34;
	// System.Single Pathfinding.RVO.Sampled.Agent::<Radius>k__BackingField
	float ___U3CRadiusU3Ek__BackingField_35;
	// System.Single Pathfinding.RVO.Sampled.Agent::<Height>k__BackingField
	float ___U3CHeightU3Ek__BackingField_36;
	// System.Single Pathfinding.RVO.Sampled.Agent::<MaxSpeed>k__BackingField
	float ___U3CMaxSpeedU3Ek__BackingField_37;
	// System.Single Pathfinding.RVO.Sampled.Agent::<NeighbourDist>k__BackingField
	float ___U3CNeighbourDistU3Ek__BackingField_38;
	// System.Single Pathfinding.RVO.Sampled.Agent::<AgentTimeHorizon>k__BackingField
	float ___U3CAgentTimeHorizonU3Ek__BackingField_39;
	// System.Single Pathfinding.RVO.Sampled.Agent::<ObstacleTimeHorizon>k__BackingField
	float ___U3CObstacleTimeHorizonU3Ek__BackingField_40;
	// UnityEngine.Vector3 Pathfinding.RVO.Sampled.Agent::<Velocity>k__BackingField
	Vector3_t4282066566  ___U3CVelocityU3Ek__BackingField_41;
	// System.Boolean Pathfinding.RVO.Sampled.Agent::<DebugDraw>k__BackingField
	bool ___U3CDebugDrawU3Ek__BackingField_42;
	// System.Int32 Pathfinding.RVO.Sampled.Agent::<MaxNeighbours>k__BackingField
	int32_t ___U3CMaxNeighboursU3Ek__BackingField_43;

public:
	inline static int32_t get_offset_of_smoothPos_1() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___smoothPos_1)); }
	inline Vector3_t4282066566  get_smoothPos_1() const { return ___smoothPos_1; }
	inline Vector3_t4282066566 * get_address_of_smoothPos_1() { return &___smoothPos_1; }
	inline void set_smoothPos_1(Vector3_t4282066566  value)
	{
		___smoothPos_1 = value;
	}

	inline static int32_t get_offset_of_radius_2() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___radius_2)); }
	inline float get_radius_2() const { return ___radius_2; }
	inline float* get_address_of_radius_2() { return &___radius_2; }
	inline void set_radius_2(float value)
	{
		___radius_2 = value;
	}

	inline static int32_t get_offset_of_height_3() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___height_3)); }
	inline float get_height_3() const { return ___height_3; }
	inline float* get_address_of_height_3() { return &___height_3; }
	inline void set_height_3(float value)
	{
		___height_3 = value;
	}

	inline static int32_t get_offset_of_maxSpeed_4() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___maxSpeed_4)); }
	inline float get_maxSpeed_4() const { return ___maxSpeed_4; }
	inline float* get_address_of_maxSpeed_4() { return &___maxSpeed_4; }
	inline void set_maxSpeed_4(float value)
	{
		___maxSpeed_4 = value;
	}

	inline static int32_t get_offset_of_neighbourDist_5() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___neighbourDist_5)); }
	inline float get_neighbourDist_5() const { return ___neighbourDist_5; }
	inline float* get_address_of_neighbourDist_5() { return &___neighbourDist_5; }
	inline void set_neighbourDist_5(float value)
	{
		___neighbourDist_5 = value;
	}

	inline static int32_t get_offset_of_agentTimeHorizon_6() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___agentTimeHorizon_6)); }
	inline float get_agentTimeHorizon_6() const { return ___agentTimeHorizon_6; }
	inline float* get_address_of_agentTimeHorizon_6() { return &___agentTimeHorizon_6; }
	inline void set_agentTimeHorizon_6(float value)
	{
		___agentTimeHorizon_6 = value;
	}

	inline static int32_t get_offset_of_obstacleTimeHorizon_7() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___obstacleTimeHorizon_7)); }
	inline float get_obstacleTimeHorizon_7() const { return ___obstacleTimeHorizon_7; }
	inline float* get_address_of_obstacleTimeHorizon_7() { return &___obstacleTimeHorizon_7; }
	inline void set_obstacleTimeHorizon_7(float value)
	{
		___obstacleTimeHorizon_7 = value;
	}

	inline static int32_t get_offset_of_weight_8() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___weight_8)); }
	inline float get_weight_8() const { return ___weight_8; }
	inline float* get_address_of_weight_8() { return &___weight_8; }
	inline void set_weight_8(float value)
	{
		___weight_8 = value;
	}

	inline static int32_t get_offset_of_locked_9() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___locked_9)); }
	inline bool get_locked_9() const { return ___locked_9; }
	inline bool* get_address_of_locked_9() { return &___locked_9; }
	inline void set_locked_9(bool value)
	{
		___locked_9 = value;
	}

	inline static int32_t get_offset_of_layer_10() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___layer_10)); }
	inline int32_t get_layer_10() const { return ___layer_10; }
	inline int32_t* get_address_of_layer_10() { return &___layer_10; }
	inline void set_layer_10(int32_t value)
	{
		___layer_10 = value;
	}

	inline static int32_t get_offset_of_collidesWith_11() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___collidesWith_11)); }
	inline int32_t get_collidesWith_11() const { return ___collidesWith_11; }
	inline int32_t* get_address_of_collidesWith_11() { return &___collidesWith_11; }
	inline void set_collidesWith_11(int32_t value)
	{
		___collidesWith_11 = value;
	}

	inline static int32_t get_offset_of_maxNeighbours_12() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___maxNeighbours_12)); }
	inline int32_t get_maxNeighbours_12() const { return ___maxNeighbours_12; }
	inline int32_t* get_address_of_maxNeighbours_12() { return &___maxNeighbours_12; }
	inline void set_maxNeighbours_12(int32_t value)
	{
		___maxNeighbours_12 = value;
	}

	inline static int32_t get_offset_of_position_13() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___position_13)); }
	inline Vector3_t4282066566  get_position_13() const { return ___position_13; }
	inline Vector3_t4282066566 * get_address_of_position_13() { return &___position_13; }
	inline void set_position_13(Vector3_t4282066566  value)
	{
		___position_13 = value;
	}

	inline static int32_t get_offset_of_desiredVelocity_14() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___desiredVelocity_14)); }
	inline Vector3_t4282066566  get_desiredVelocity_14() const { return ___desiredVelocity_14; }
	inline Vector3_t4282066566 * get_address_of_desiredVelocity_14() { return &___desiredVelocity_14; }
	inline void set_desiredVelocity_14(Vector3_t4282066566  value)
	{
		___desiredVelocity_14 = value;
	}

	inline static int32_t get_offset_of_prevSmoothPos_15() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___prevSmoothPos_15)); }
	inline Vector3_t4282066566  get_prevSmoothPos_15() const { return ___prevSmoothPos_15; }
	inline Vector3_t4282066566 * get_address_of_prevSmoothPos_15() { return &___prevSmoothPos_15; }
	inline void set_prevSmoothPos_15(Vector3_t4282066566  value)
	{
		___prevSmoothPos_15 = value;
	}

	inline static int32_t get_offset_of_next_16() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___next_16)); }
	inline Agent_t1290054243 * get_next_16() const { return ___next_16; }
	inline Agent_t1290054243 ** get_address_of_next_16() { return &___next_16; }
	inline void set_next_16(Agent_t1290054243 * value)
	{
		___next_16 = value;
		Il2CppCodeGenWriteBarrier(&___next_16, value);
	}

	inline static int32_t get_offset_of_velocity_17() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___velocity_17)); }
	inline Vector3_t4282066566  get_velocity_17() const { return ___velocity_17; }
	inline Vector3_t4282066566 * get_address_of_velocity_17() { return &___velocity_17; }
	inline void set_velocity_17(Vector3_t4282066566  value)
	{
		___velocity_17 = value;
	}

	inline static int32_t get_offset_of_newVelocity_18() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___newVelocity_18)); }
	inline Vector3_t4282066566  get_newVelocity_18() const { return ___newVelocity_18; }
	inline Vector3_t4282066566 * get_address_of_newVelocity_18() { return &___newVelocity_18; }
	inline void set_newVelocity_18(Vector3_t4282066566  value)
	{
		___newVelocity_18 = value;
	}

	inline static int32_t get_offset_of_simulator_19() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___simulator_19)); }
	inline Simulator_t2705969170 * get_simulator_19() const { return ___simulator_19; }
	inline Simulator_t2705969170 ** get_address_of_simulator_19() { return &___simulator_19; }
	inline void set_simulator_19(Simulator_t2705969170 * value)
	{
		___simulator_19 = value;
		Il2CppCodeGenWriteBarrier(&___simulator_19, value);
	}

	inline static int32_t get_offset_of_neighbours_20() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___neighbours_20)); }
	inline List_1_t2658239795 * get_neighbours_20() const { return ___neighbours_20; }
	inline List_1_t2658239795 ** get_address_of_neighbours_20() { return &___neighbours_20; }
	inline void set_neighbours_20(List_1_t2658239795 * value)
	{
		___neighbours_20 = value;
		Il2CppCodeGenWriteBarrier(&___neighbours_20, value);
	}

	inline static int32_t get_offset_of_neighbourDists_21() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___neighbourDists_21)); }
	inline List_1_t1365137228 * get_neighbourDists_21() const { return ___neighbourDists_21; }
	inline List_1_t1365137228 ** get_address_of_neighbourDists_21() { return &___neighbourDists_21; }
	inline void set_neighbourDists_21(List_1_t1365137228 * value)
	{
		___neighbourDists_21 = value;
		Il2CppCodeGenWriteBarrier(&___neighbourDists_21, value);
	}

	inline static int32_t get_offset_of_obstaclesBuffered_22() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___obstaclesBuffered_22)); }
	inline List_1_t1243525355 * get_obstaclesBuffered_22() const { return ___obstaclesBuffered_22; }
	inline List_1_t1243525355 ** get_address_of_obstaclesBuffered_22() { return &___obstaclesBuffered_22; }
	inline void set_obstaclesBuffered_22(List_1_t1243525355 * value)
	{
		___obstaclesBuffered_22 = value;
		Il2CppCodeGenWriteBarrier(&___obstaclesBuffered_22, value);
	}

	inline static int32_t get_offset_of_obstacles_23() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___obstacles_23)); }
	inline List_1_t1243525355 * get_obstacles_23() const { return ___obstacles_23; }
	inline List_1_t1243525355 ** get_address_of_obstacles_23() { return &___obstacles_23; }
	inline void set_obstacles_23(List_1_t1243525355 * value)
	{
		___obstacles_23 = value;
		Il2CppCodeGenWriteBarrier(&___obstacles_23, value);
	}

	inline static int32_t get_offset_of_obstacleDists_24() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___obstacleDists_24)); }
	inline List_1_t1365137228 * get_obstacleDists_24() const { return ___obstacleDists_24; }
	inline List_1_t1365137228 ** get_address_of_obstacleDists_24() { return &___obstacleDists_24; }
	inline void set_obstacleDists_24(List_1_t1365137228 * value)
	{
		___obstacleDists_24 = value;
		Il2CppCodeGenWriteBarrier(&___obstacleDists_24, value);
	}

	inline static int32_t get_offset_of_U3CPositionU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___U3CPositionU3Ek__BackingField_30)); }
	inline Vector3_t4282066566  get_U3CPositionU3Ek__BackingField_30() const { return ___U3CPositionU3Ek__BackingField_30; }
	inline Vector3_t4282066566 * get_address_of_U3CPositionU3Ek__BackingField_30() { return &___U3CPositionU3Ek__BackingField_30; }
	inline void set_U3CPositionU3Ek__BackingField_30(Vector3_t4282066566  value)
	{
		___U3CPositionU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_U3CDesiredVelocityU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___U3CDesiredVelocityU3Ek__BackingField_31)); }
	inline Vector3_t4282066566  get_U3CDesiredVelocityU3Ek__BackingField_31() const { return ___U3CDesiredVelocityU3Ek__BackingField_31; }
	inline Vector3_t4282066566 * get_address_of_U3CDesiredVelocityU3Ek__BackingField_31() { return &___U3CDesiredVelocityU3Ek__BackingField_31; }
	inline void set_U3CDesiredVelocityU3Ek__BackingField_31(Vector3_t4282066566  value)
	{
		___U3CDesiredVelocityU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CLayerU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___U3CLayerU3Ek__BackingField_32)); }
	inline int32_t get_U3CLayerU3Ek__BackingField_32() const { return ___U3CLayerU3Ek__BackingField_32; }
	inline int32_t* get_address_of_U3CLayerU3Ek__BackingField_32() { return &___U3CLayerU3Ek__BackingField_32; }
	inline void set_U3CLayerU3Ek__BackingField_32(int32_t value)
	{
		___U3CLayerU3Ek__BackingField_32 = value;
	}

	inline static int32_t get_offset_of_U3CCollidesWithU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___U3CCollidesWithU3Ek__BackingField_33)); }
	inline int32_t get_U3CCollidesWithU3Ek__BackingField_33() const { return ___U3CCollidesWithU3Ek__BackingField_33; }
	inline int32_t* get_address_of_U3CCollidesWithU3Ek__BackingField_33() { return &___U3CCollidesWithU3Ek__BackingField_33; }
	inline void set_U3CCollidesWithU3Ek__BackingField_33(int32_t value)
	{
		___U3CCollidesWithU3Ek__BackingField_33 = value;
	}

	inline static int32_t get_offset_of_U3CLockedU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___U3CLockedU3Ek__BackingField_34)); }
	inline bool get_U3CLockedU3Ek__BackingField_34() const { return ___U3CLockedU3Ek__BackingField_34; }
	inline bool* get_address_of_U3CLockedU3Ek__BackingField_34() { return &___U3CLockedU3Ek__BackingField_34; }
	inline void set_U3CLockedU3Ek__BackingField_34(bool value)
	{
		___U3CLockedU3Ek__BackingField_34 = value;
	}

	inline static int32_t get_offset_of_U3CRadiusU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___U3CRadiusU3Ek__BackingField_35)); }
	inline float get_U3CRadiusU3Ek__BackingField_35() const { return ___U3CRadiusU3Ek__BackingField_35; }
	inline float* get_address_of_U3CRadiusU3Ek__BackingField_35() { return &___U3CRadiusU3Ek__BackingField_35; }
	inline void set_U3CRadiusU3Ek__BackingField_35(float value)
	{
		___U3CRadiusU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___U3CHeightU3Ek__BackingField_36)); }
	inline float get_U3CHeightU3Ek__BackingField_36() const { return ___U3CHeightU3Ek__BackingField_36; }
	inline float* get_address_of_U3CHeightU3Ek__BackingField_36() { return &___U3CHeightU3Ek__BackingField_36; }
	inline void set_U3CHeightU3Ek__BackingField_36(float value)
	{
		___U3CHeightU3Ek__BackingField_36 = value;
	}

	inline static int32_t get_offset_of_U3CMaxSpeedU3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___U3CMaxSpeedU3Ek__BackingField_37)); }
	inline float get_U3CMaxSpeedU3Ek__BackingField_37() const { return ___U3CMaxSpeedU3Ek__BackingField_37; }
	inline float* get_address_of_U3CMaxSpeedU3Ek__BackingField_37() { return &___U3CMaxSpeedU3Ek__BackingField_37; }
	inline void set_U3CMaxSpeedU3Ek__BackingField_37(float value)
	{
		___U3CMaxSpeedU3Ek__BackingField_37 = value;
	}

	inline static int32_t get_offset_of_U3CNeighbourDistU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___U3CNeighbourDistU3Ek__BackingField_38)); }
	inline float get_U3CNeighbourDistU3Ek__BackingField_38() const { return ___U3CNeighbourDistU3Ek__BackingField_38; }
	inline float* get_address_of_U3CNeighbourDistU3Ek__BackingField_38() { return &___U3CNeighbourDistU3Ek__BackingField_38; }
	inline void set_U3CNeighbourDistU3Ek__BackingField_38(float value)
	{
		___U3CNeighbourDistU3Ek__BackingField_38 = value;
	}

	inline static int32_t get_offset_of_U3CAgentTimeHorizonU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___U3CAgentTimeHorizonU3Ek__BackingField_39)); }
	inline float get_U3CAgentTimeHorizonU3Ek__BackingField_39() const { return ___U3CAgentTimeHorizonU3Ek__BackingField_39; }
	inline float* get_address_of_U3CAgentTimeHorizonU3Ek__BackingField_39() { return &___U3CAgentTimeHorizonU3Ek__BackingField_39; }
	inline void set_U3CAgentTimeHorizonU3Ek__BackingField_39(float value)
	{
		___U3CAgentTimeHorizonU3Ek__BackingField_39 = value;
	}

	inline static int32_t get_offset_of_U3CObstacleTimeHorizonU3Ek__BackingField_40() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___U3CObstacleTimeHorizonU3Ek__BackingField_40)); }
	inline float get_U3CObstacleTimeHorizonU3Ek__BackingField_40() const { return ___U3CObstacleTimeHorizonU3Ek__BackingField_40; }
	inline float* get_address_of_U3CObstacleTimeHorizonU3Ek__BackingField_40() { return &___U3CObstacleTimeHorizonU3Ek__BackingField_40; }
	inline void set_U3CObstacleTimeHorizonU3Ek__BackingField_40(float value)
	{
		___U3CObstacleTimeHorizonU3Ek__BackingField_40 = value;
	}

	inline static int32_t get_offset_of_U3CVelocityU3Ek__BackingField_41() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___U3CVelocityU3Ek__BackingField_41)); }
	inline Vector3_t4282066566  get_U3CVelocityU3Ek__BackingField_41() const { return ___U3CVelocityU3Ek__BackingField_41; }
	inline Vector3_t4282066566 * get_address_of_U3CVelocityU3Ek__BackingField_41() { return &___U3CVelocityU3Ek__BackingField_41; }
	inline void set_U3CVelocityU3Ek__BackingField_41(Vector3_t4282066566  value)
	{
		___U3CVelocityU3Ek__BackingField_41 = value;
	}

	inline static int32_t get_offset_of_U3CDebugDrawU3Ek__BackingField_42() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___U3CDebugDrawU3Ek__BackingField_42)); }
	inline bool get_U3CDebugDrawU3Ek__BackingField_42() const { return ___U3CDebugDrawU3Ek__BackingField_42; }
	inline bool* get_address_of_U3CDebugDrawU3Ek__BackingField_42() { return &___U3CDebugDrawU3Ek__BackingField_42; }
	inline void set_U3CDebugDrawU3Ek__BackingField_42(bool value)
	{
		___U3CDebugDrawU3Ek__BackingField_42 = value;
	}

	inline static int32_t get_offset_of_U3CMaxNeighboursU3Ek__BackingField_43() { return static_cast<int32_t>(offsetof(Agent_t1290054243, ___U3CMaxNeighboursU3Ek__BackingField_43)); }
	inline int32_t get_U3CMaxNeighboursU3Ek__BackingField_43() const { return ___U3CMaxNeighboursU3Ek__BackingField_43; }
	inline int32_t* get_address_of_U3CMaxNeighboursU3Ek__BackingField_43() { return &___U3CMaxNeighboursU3Ek__BackingField_43; }
	inline void set_U3CMaxNeighboursU3Ek__BackingField_43(int32_t value)
	{
		___U3CMaxNeighboursU3Ek__BackingField_43 = value;
	}
};

struct Agent_t1290054243_StaticFields
{
public:
	// System.Diagnostics.Stopwatch Pathfinding.RVO.Sampled.Agent::watch1
	Stopwatch_t3420517611 * ___watch1_25;
	// System.Diagnostics.Stopwatch Pathfinding.RVO.Sampled.Agent::watch2
	Stopwatch_t3420517611 * ___watch2_26;
	// System.Single Pathfinding.RVO.Sampled.Agent::DesiredVelocityWeight
	float ___DesiredVelocityWeight_27;
	// System.Single Pathfinding.RVO.Sampled.Agent::DesiredVelocityScale
	float ___DesiredVelocityScale_28;
	// System.Single Pathfinding.RVO.Sampled.Agent::GlobalIncompressibility
	float ___GlobalIncompressibility_29;

public:
	inline static int32_t get_offset_of_watch1_25() { return static_cast<int32_t>(offsetof(Agent_t1290054243_StaticFields, ___watch1_25)); }
	inline Stopwatch_t3420517611 * get_watch1_25() const { return ___watch1_25; }
	inline Stopwatch_t3420517611 ** get_address_of_watch1_25() { return &___watch1_25; }
	inline void set_watch1_25(Stopwatch_t3420517611 * value)
	{
		___watch1_25 = value;
		Il2CppCodeGenWriteBarrier(&___watch1_25, value);
	}

	inline static int32_t get_offset_of_watch2_26() { return static_cast<int32_t>(offsetof(Agent_t1290054243_StaticFields, ___watch2_26)); }
	inline Stopwatch_t3420517611 * get_watch2_26() const { return ___watch2_26; }
	inline Stopwatch_t3420517611 ** get_address_of_watch2_26() { return &___watch2_26; }
	inline void set_watch2_26(Stopwatch_t3420517611 * value)
	{
		___watch2_26 = value;
		Il2CppCodeGenWriteBarrier(&___watch2_26, value);
	}

	inline static int32_t get_offset_of_DesiredVelocityWeight_27() { return static_cast<int32_t>(offsetof(Agent_t1290054243_StaticFields, ___DesiredVelocityWeight_27)); }
	inline float get_DesiredVelocityWeight_27() const { return ___DesiredVelocityWeight_27; }
	inline float* get_address_of_DesiredVelocityWeight_27() { return &___DesiredVelocityWeight_27; }
	inline void set_DesiredVelocityWeight_27(float value)
	{
		___DesiredVelocityWeight_27 = value;
	}

	inline static int32_t get_offset_of_DesiredVelocityScale_28() { return static_cast<int32_t>(offsetof(Agent_t1290054243_StaticFields, ___DesiredVelocityScale_28)); }
	inline float get_DesiredVelocityScale_28() const { return ___DesiredVelocityScale_28; }
	inline float* get_address_of_DesiredVelocityScale_28() { return &___DesiredVelocityScale_28; }
	inline void set_DesiredVelocityScale_28(float value)
	{
		___DesiredVelocityScale_28 = value;
	}

	inline static int32_t get_offset_of_GlobalIncompressibility_29() { return static_cast<int32_t>(offsetof(Agent_t1290054243_StaticFields, ___GlobalIncompressibility_29)); }
	inline float get_GlobalIncompressibility_29() const { return ___GlobalIncompressibility_29; }
	inline float* get_address_of_GlobalIncompressibility_29() { return &___GlobalIncompressibility_29; }
	inline void set_GlobalIncompressibility_29(float value)
	{
		___GlobalIncompressibility_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
