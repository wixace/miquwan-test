﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.NavMeshTriangulation
struct NavMeshTriangulation_t2646108099;
struct NavMeshTriangulation_t2646108099_marshaled_pinvoke;
struct NavMeshTriangulation_t2646108099_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct NavMeshTriangulation_t2646108099;
struct NavMeshTriangulation_t2646108099_marshaled_pinvoke;

extern "C" void NavMeshTriangulation_t2646108099_marshal_pinvoke(const NavMeshTriangulation_t2646108099& unmarshaled, NavMeshTriangulation_t2646108099_marshaled_pinvoke& marshaled);
extern "C" void NavMeshTriangulation_t2646108099_marshal_pinvoke_back(const NavMeshTriangulation_t2646108099_marshaled_pinvoke& marshaled, NavMeshTriangulation_t2646108099& unmarshaled);
extern "C" void NavMeshTriangulation_t2646108099_marshal_pinvoke_cleanup(NavMeshTriangulation_t2646108099_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct NavMeshTriangulation_t2646108099;
struct NavMeshTriangulation_t2646108099_marshaled_com;

extern "C" void NavMeshTriangulation_t2646108099_marshal_com(const NavMeshTriangulation_t2646108099& unmarshaled, NavMeshTriangulation_t2646108099_marshaled_com& marshaled);
extern "C" void NavMeshTriangulation_t2646108099_marshal_com_back(const NavMeshTriangulation_t2646108099_marshaled_com& marshaled, NavMeshTriangulation_t2646108099& unmarshaled);
extern "C" void NavMeshTriangulation_t2646108099_marshal_com_cleanup(NavMeshTriangulation_t2646108099_marshaled_com& marshaled);
