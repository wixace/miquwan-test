﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PostEffect_Bright
struct PostEffect_Bright_t1218249896;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"
#include "AssemblyU2DCSharp_PostEffect_Bright1218249896.h"

// System.Void PostEffect_Bright::.ctor()
extern "C"  void PostEffect_Bright__ctor_m634217923 (PostEffect_Bright_t1218249896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material PostEffect_Bright::get_material()
extern "C"  Material_t3870600107 * PostEffect_Bright_get_material_m3076948274 (PostEffect_Bright_t1218249896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PostEffect_Bright::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void PostEffect_Bright_OnRenderImage_m354967739 (PostEffect_Bright_t1218249896 * __this, RenderTexture_t1963041563 * ___src0, RenderTexture_t1963041563 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material PostEffect_Bright::ilo_get_material1(PostEffect_Bright)
extern "C"  Material_t3870600107 * PostEffect_Bright_ilo_get_material1_m2904160892 (Il2CppObject * __this /* static, unused */, PostEffect_Bright_t1218249896 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
