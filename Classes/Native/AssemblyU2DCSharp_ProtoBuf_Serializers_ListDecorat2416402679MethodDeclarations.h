﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.ListDecorator
struct ListDecorator_t2416402679;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Type
struct Type_t;
// ProtoBuf.Serializers.IProtoSerializer
struct IProtoSerializer_t3033312651;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_ProtoBuf_WireType2355646059.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "AssemblyU2DCSharp_ProtoBuf_Serializers_ListDecorat2416402679.h"
#include "mscorlib_System_String7231557.h"

// System.Void ProtoBuf.Serializers.ListDecorator::.ctor(ProtoBuf.Meta.TypeModel,System.Type,System.Type,ProtoBuf.Serializers.IProtoSerializer,System.Int32,System.Boolean,ProtoBuf.WireType,System.Boolean,System.Boolean,System.Boolean)
extern "C"  void ListDecorator__ctor_m259036193 (ListDecorator_t2416402679 * __this, TypeModel_t2730011105 * ___model0, Type_t * ___declaredType1, Type_t * ___concreteType2, Il2CppObject * ___tail3, int32_t ___fieldNumber4, bool ___writePacked5, int32_t ___packedWireType6, bool ___returnList7, bool ___overwriteList8, bool ___supportNull9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.ListDecorator::.cctor()
extern "C"  void ListDecorator__cctor_m1599898751 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.ListDecorator::CanPack(ProtoBuf.WireType)
extern "C"  bool ListDecorator_CanPack_m2411584047 (Il2CppObject * __this /* static, unused */, int32_t ___wireType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.ListDecorator::get_IsList()
extern "C"  bool ListDecorator_get_IsList_m911534907 (ListDecorator_t2416402679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.ListDecorator::get_SuppressIList()
extern "C"  bool ListDecorator_get_SuppressIList_m2652780513 (ListDecorator_t2416402679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.ListDecorator::get_WritePacked()
extern "C"  bool ListDecorator_get_WritePacked_m3743666278 (ListDecorator_t2416402679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.ListDecorator::get_SupportNull()
extern "C"  bool ListDecorator_get_SupportNull_m3719096677 (ListDecorator_t2416402679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.ListDecorator::get_ReturnList()
extern "C"  bool ListDecorator_get_ReturnList_m3128329665 (ListDecorator_t2416402679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Serializers.ListDecorator ProtoBuf.Serializers.ListDecorator::Create(ProtoBuf.Meta.TypeModel,System.Type,System.Type,ProtoBuf.Serializers.IProtoSerializer,System.Int32,System.Boolean,ProtoBuf.WireType,System.Boolean,System.Boolean,System.Boolean)
extern "C"  ListDecorator_t2416402679 * ListDecorator_Create_m2183113301 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, Type_t * ___declaredType1, Type_t * ___concreteType2, Il2CppObject * ___tail3, int32_t ___fieldNumber4, bool ___writePacked5, int32_t ___packedWireType6, bool ___returnList7, bool ___overwriteList8, bool ___supportNull9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.ListDecorator::get_RequireAdd()
extern "C"  bool ListDecorator_get_RequireAdd_m850852175 (ListDecorator_t2416402679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.ListDecorator::get_ExpectedType()
extern "C"  Type_t * ListDecorator_get_ExpectedType_m2335712331 (ListDecorator_t2416402679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.ListDecorator::get_RequiresOldValue()
extern "C"  bool ListDecorator_get_RequiresOldValue_m312097067 (ListDecorator_t2416402679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.ListDecorator::get_ReturnsValue()
extern "C"  bool ListDecorator_get_ReturnsValue_m3189733697 (ListDecorator_t2416402679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.ListDecorator::get_AppendToCollection()
extern "C"  bool ListDecorator_get_AppendToCollection_m3271537734 (ListDecorator_t2416402679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Serializers.ListDecorator::GetEnumeratorInfo(ProtoBuf.Meta.TypeModel,System.Reflection.MethodInfo&,System.Reflection.MethodInfo&)
extern "C"  MethodInfo_t * ListDecorator_GetEnumeratorInfo_m1367425713 (ListDecorator_t2416402679 * __this, TypeModel_t2730011105 * ___model0, MethodInfo_t ** ___moveNext1, MethodInfo_t ** ___current2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.ListDecorator::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void ListDecorator_Write_m2566910693 (ListDecorator_t2416402679 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.ListDecorator::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * ListDecorator_Read_m164449803 (ListDecorator_t2416402679 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.ListDecorator::ilo_get_AppendToCollection1(ProtoBuf.Serializers.ListDecorator)
extern "C"  bool ListDecorator_ilo_get_AppendToCollection1_m3561674509 (Il2CppObject * __this /* static, unused */, ListDecorator_t2416402679 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.ListDecorator::ilo_MapType2(ProtoBuf.Meta.TypeModel,System.Type)
extern "C"  Type_t * ListDecorator_ilo_MapType2_m2661837533 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyInfo ProtoBuf.Serializers.ListDecorator::ilo_GetProperty3(System.Type,System.String,System.Boolean)
extern "C"  PropertyInfo_t * ListDecorator_ilo_GetProperty3_m1547817263 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, String_t* ___name1, bool ___nonPublic2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Serializers.ListDecorator::ilo_GetInstanceMethod4(System.Type,System.String)
extern "C"  MethodInfo_t * ListDecorator_ilo_GetInstanceMethod4_m970464826 (Il2CppObject * __this /* static, unused */, Type_t * ___declaringType0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.ListDecorator::ilo_WriteFieldHeader5(System.Int32,ProtoBuf.WireType,ProtoBuf.ProtoWriter)
extern "C"  void ListDecorator_ilo_WriteFieldHeader5_m1291582285 (Il2CppObject * __this /* static, unused */, int32_t ___fieldNumber0, int32_t ___wireType1, ProtoWriter_t4117914721 * ___writer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Serializers.ListDecorator::ilo_get_FieldNumber6(ProtoBuf.ProtoReader)
extern "C"  int32_t ListDecorator_ilo_get_FieldNumber6_m1690948811 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.ListDecorator::ilo_get_IsList7(ProtoBuf.Serializers.ListDecorator)
extern "C"  bool ListDecorator_ilo_get_IsList7_m1671971966 (Il2CppObject * __this /* static, unused */, ListDecorator_t2416402679 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.WireType ProtoBuf.Serializers.ListDecorator::ilo_get_WireType8(ProtoBuf.ProtoReader)
extern "C"  int32_t ListDecorator_ilo_get_WireType8_m3050308240 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.ListDecorator::ilo_Read9(ProtoBuf.Serializers.IProtoSerializer,System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * ListDecorator_ilo_Read9_m2584443366 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Il2CppObject * ___value1, ProtoReader_t3962509489 * ___source2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.ListDecorator::ilo_HasSubValue10(ProtoBuf.WireType,ProtoBuf.ProtoReader)
extern "C"  bool ListDecorator_ilo_HasSubValue10_m1754030717 (Il2CppObject * __this /* static, unused */, int32_t ___wireType0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.ListDecorator::ilo_TryReadFieldHeader11(ProtoBuf.ProtoReader,System.Int32)
extern "C"  bool ListDecorator_ilo_TryReadFieldHeader11_m2094309676 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, int32_t ___field1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
