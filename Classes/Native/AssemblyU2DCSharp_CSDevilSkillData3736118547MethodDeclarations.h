﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSDevilSkillData
struct CSDevilSkillData_t3736118547;

#include "codegen/il2cpp-codegen.h"

// System.Void CSDevilSkillData::.ctor()
extern "C"  void CSDevilSkillData__ctor_m117540584 (CSDevilSkillData_t3736118547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
