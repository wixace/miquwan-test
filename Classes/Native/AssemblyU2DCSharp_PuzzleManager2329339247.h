﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// InputItem[]
struct InputItemU5BU5D_t269107728;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// InputItem[,]
struct InputItemU5BU2CU5D_t269107729;
// UnityEngine.UI.Image
struct Image_t538875265;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// UnityEngine.UI.Text
struct Text_t9039225;

#include "AssemblyU2DCSharp_Core_USingleton_1_gen335632631.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuzzleManager
struct  PuzzleManager_t2329339247  : public USingleton_1_t335632631
{
public:
	// InputItem[] PuzzleManager::List
	InputItemU5BU5D_t269107728* ___List_5;
	// System.Int32[] PuzzleManager::RandNum
	Int32U5BU5D_t3230847821* ___RandNum_6;
	// InputItem[,] PuzzleManager::_matrix
	InputItemU5BU2CU5D_t269107729* ____matrix_7;
	// System.Int32 PuzzleManager::_openCount
	int32_t ____openCount_8;
	// UnityEngine.UI.Image PuzzleManager::OutputCover
	Image_t538875265 * ___OutputCover_9;
	// UnityEngine.Sprite PuzzleManager::AvailableCover
	Sprite_t3199167241 * ___AvailableCover_10;
	// UnityEngine.UI.Text PuzzleManager::ResulTextt
	Text_t9039225 * ___ResulTextt_11;

public:
	inline static int32_t get_offset_of_List_5() { return static_cast<int32_t>(offsetof(PuzzleManager_t2329339247, ___List_5)); }
	inline InputItemU5BU5D_t269107728* get_List_5() const { return ___List_5; }
	inline InputItemU5BU5D_t269107728** get_address_of_List_5() { return &___List_5; }
	inline void set_List_5(InputItemU5BU5D_t269107728* value)
	{
		___List_5 = value;
		Il2CppCodeGenWriteBarrier(&___List_5, value);
	}

	inline static int32_t get_offset_of_RandNum_6() { return static_cast<int32_t>(offsetof(PuzzleManager_t2329339247, ___RandNum_6)); }
	inline Int32U5BU5D_t3230847821* get_RandNum_6() const { return ___RandNum_6; }
	inline Int32U5BU5D_t3230847821** get_address_of_RandNum_6() { return &___RandNum_6; }
	inline void set_RandNum_6(Int32U5BU5D_t3230847821* value)
	{
		___RandNum_6 = value;
		Il2CppCodeGenWriteBarrier(&___RandNum_6, value);
	}

	inline static int32_t get_offset_of__matrix_7() { return static_cast<int32_t>(offsetof(PuzzleManager_t2329339247, ____matrix_7)); }
	inline InputItemU5BU2CU5D_t269107729* get__matrix_7() const { return ____matrix_7; }
	inline InputItemU5BU2CU5D_t269107729** get_address_of__matrix_7() { return &____matrix_7; }
	inline void set__matrix_7(InputItemU5BU2CU5D_t269107729* value)
	{
		____matrix_7 = value;
		Il2CppCodeGenWriteBarrier(&____matrix_7, value);
	}

	inline static int32_t get_offset_of__openCount_8() { return static_cast<int32_t>(offsetof(PuzzleManager_t2329339247, ____openCount_8)); }
	inline int32_t get__openCount_8() const { return ____openCount_8; }
	inline int32_t* get_address_of__openCount_8() { return &____openCount_8; }
	inline void set__openCount_8(int32_t value)
	{
		____openCount_8 = value;
	}

	inline static int32_t get_offset_of_OutputCover_9() { return static_cast<int32_t>(offsetof(PuzzleManager_t2329339247, ___OutputCover_9)); }
	inline Image_t538875265 * get_OutputCover_9() const { return ___OutputCover_9; }
	inline Image_t538875265 ** get_address_of_OutputCover_9() { return &___OutputCover_9; }
	inline void set_OutputCover_9(Image_t538875265 * value)
	{
		___OutputCover_9 = value;
		Il2CppCodeGenWriteBarrier(&___OutputCover_9, value);
	}

	inline static int32_t get_offset_of_AvailableCover_10() { return static_cast<int32_t>(offsetof(PuzzleManager_t2329339247, ___AvailableCover_10)); }
	inline Sprite_t3199167241 * get_AvailableCover_10() const { return ___AvailableCover_10; }
	inline Sprite_t3199167241 ** get_address_of_AvailableCover_10() { return &___AvailableCover_10; }
	inline void set_AvailableCover_10(Sprite_t3199167241 * value)
	{
		___AvailableCover_10 = value;
		Il2CppCodeGenWriteBarrier(&___AvailableCover_10, value);
	}

	inline static int32_t get_offset_of_ResulTextt_11() { return static_cast<int32_t>(offsetof(PuzzleManager_t2329339247, ___ResulTextt_11)); }
	inline Text_t9039225 * get_ResulTextt_11() const { return ___ResulTextt_11; }
	inline Text_t9039225 ** get_address_of_ResulTextt_11() { return &___ResulTextt_11; }
	inline void set_ResulTextt_11(Text_t9039225 * value)
	{
		___ResulTextt_11 = value;
		Il2CppCodeGenWriteBarrier(&___ResulTextt_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
