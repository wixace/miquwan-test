﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Byte[]>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2676769374(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2103534935 *, Dictionary_2_t786211543 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Byte[]>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1756659715(__this, method) ((  Il2CppObject * (*) (Enumerator_t2103534935 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Byte[]>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m908513175(__this, method) ((  void (*) (Enumerator_t2103534935 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Byte[]>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3297063392(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2103534935 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Byte[]>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2610327199(__this, method) ((  Il2CppObject * (*) (Enumerator_t2103534935 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Byte[]>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1311973745(__this, method) ((  Il2CppObject * (*) (Enumerator_t2103534935 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Byte[]>::MoveNext()
#define Enumerator_MoveNext_m298880259(__this, method) ((  bool (*) (Enumerator_t2103534935 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Byte[]>::get_Current()
#define Enumerator_get_Current_m2835694157(__this, method) ((  KeyValuePair_2_t684992249  (*) (Enumerator_t2103534935 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Byte[]>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m278166160(__this, method) ((  String_t* (*) (Enumerator_t2103534935 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Byte[]>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m939408180(__this, method) ((  ByteU5BU5D_t4260760469* (*) (Enumerator_t2103534935 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Byte[]>::Reset()
#define Enumerator_Reset_m969158576(__this, method) ((  void (*) (Enumerator_t2103534935 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Byte[]>::VerifyState()
#define Enumerator_VerifyState_m2885745593(__this, method) ((  void (*) (Enumerator_t2103534935 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Byte[]>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1397513441(__this, method) ((  void (*) (Enumerator_t2103534935 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Byte[]>::Dispose()
#define Enumerator_Dispose_m1876917376(__this, method) ((  void (*) (Enumerator_t2103534935 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
