﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel[]
struct UILabelU5BU5D_t1494885441;

#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Float_pet_upTrain
struct  Float_pet_upTrain_t3812277130  : public FloatTextUnit_t2362298029
{
public:
	// UILabel[] Float_pet_upTrain::labelArr
	UILabelU5BU5D_t1494885441* ___labelArr_3;

public:
	inline static int32_t get_offset_of_labelArr_3() { return static_cast<int32_t>(offsetof(Float_pet_upTrain_t3812277130, ___labelArr_3)); }
	inline UILabelU5BU5D_t1494885441* get_labelArr_3() const { return ___labelArr_3; }
	inline UILabelU5BU5D_t1494885441** get_address_of_labelArr_3() { return &___labelArr_3; }
	inline void set_labelArr_3(UILabelU5BU5D_t1494885441* value)
	{
		___labelArr_3 = value;
		Il2CppCodeGenWriteBarrier(&___labelArr_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
