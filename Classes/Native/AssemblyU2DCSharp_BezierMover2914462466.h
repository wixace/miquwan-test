﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform[]
struct TransformU5BU5D_t3792884695;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BezierMover
struct  BezierMover_t2914462466  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Transform[] BezierMover::points
	TransformU5BU5D_t3792884695* ___points_2;
	// System.Single BezierMover::tangentLengths
	float ___tangentLengths_3;
	// System.Single BezierMover::speed
	float ___speed_4;
	// System.Single BezierMover::time
	float ___time_5;

public:
	inline static int32_t get_offset_of_points_2() { return static_cast<int32_t>(offsetof(BezierMover_t2914462466, ___points_2)); }
	inline TransformU5BU5D_t3792884695* get_points_2() const { return ___points_2; }
	inline TransformU5BU5D_t3792884695** get_address_of_points_2() { return &___points_2; }
	inline void set_points_2(TransformU5BU5D_t3792884695* value)
	{
		___points_2 = value;
		Il2CppCodeGenWriteBarrier(&___points_2, value);
	}

	inline static int32_t get_offset_of_tangentLengths_3() { return static_cast<int32_t>(offsetof(BezierMover_t2914462466, ___tangentLengths_3)); }
	inline float get_tangentLengths_3() const { return ___tangentLengths_3; }
	inline float* get_address_of_tangentLengths_3() { return &___tangentLengths_3; }
	inline void set_tangentLengths_3(float value)
	{
		___tangentLengths_3 = value;
	}

	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(BezierMover_t2914462466, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_time_5() { return static_cast<int32_t>(offsetof(BezierMover_t2914462466, ___time_5)); }
	inline float get_time_5() const { return ___time_5; }
	inline float* get_address_of_time_5() { return &___time_5; }
	inline void set_time_5(float value)
	{
		___time_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
