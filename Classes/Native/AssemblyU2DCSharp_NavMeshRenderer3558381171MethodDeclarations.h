﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NavMeshRenderer
struct NavMeshRenderer_t3558381171;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void NavMeshRenderer::.ctor()
extern "C"  void NavMeshRenderer__ctor_m3730162264 (NavMeshRenderer_t3558381171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String NavMeshRenderer::SomeFunction()
extern "C"  String_t* NavMeshRenderer_SomeFunction_m3406099541 (NavMeshRenderer_t3558381171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NavMeshRenderer::Update()
extern "C"  void NavMeshRenderer_Update_m1397775285 (NavMeshRenderer_t3558381171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
