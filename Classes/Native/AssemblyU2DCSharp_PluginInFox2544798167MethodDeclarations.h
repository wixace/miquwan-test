﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginInFox
struct PluginInFox_t2544798167;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.String
struct String_t;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// VersionMgr
struct VersionMgr_t1322950208;
// VersionInfo
struct VersionInfo_t2356638086;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Action
struct Action_t3771233898;
// System.Object
struct Il2CppObject;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// Mihua.SDK.LvUpInfo
struct LvUpInfo_t411687177;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "System_Core_System_Action3771233898.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PluginInFox2544798167.h"

// System.Void PluginInFox::.ctor()
extern "C"  void PluginInFox__ctor_m4134867828 (PluginInFox_t2544798167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginInFox::Init()
extern "C"  void PluginInFox_Init_m2163129440 (PluginInFox_t2544798167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginInFox::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginInFox_ReqSDKHttpLogin_m3231717 (PluginInFox_t2544798167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginInFox::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginInFox_IsLoginSuccess_m3931275639 (PluginInFox_t2544798167 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginInFox::OpenUserLogin()
extern "C"  void PluginInFox_OpenUserLogin_m778928582 (PluginInFox_t2544798167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginInFox::OnLoginSuccess(System.String)
extern "C"  void PluginInFox_OnLoginSuccess_m2049719161 (PluginInFox_t2544798167 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginInFox::OnLogoutSuccess(System.String)
extern "C"  void PluginInFox_OnLogoutSuccess_m3415294518 (PluginInFox_t2544798167 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginInFox::OnSDKLogout(System.String)
extern "C"  void PluginInFox_OnSDKLogout_m3981747851 (PluginInFox_t2544798167 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginInFox::OnGameSwitchSuccess(System.String)
extern "C"  void PluginInFox_OnGameSwitchSuccess_m1653650034 (PluginInFox_t2544798167 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginInFox::UserPay(CEvent.ZEvent)
extern "C"  void PluginInFox_UserPay_m3525027564 (PluginInFox_t2544798167 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginInFox::OnPaySuccess(System.String)
extern "C"  void PluginInFox_OnPaySuccess_m4155320312 (PluginInFox_t2544798167 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginInFox::OnPayFail(System.String)
extern "C"  void PluginInFox_OnPayFail_m3572108361 (PluginInFox_t2544798167 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginInFox::OnEnterGame(CEvent.ZEvent)
extern "C"  void PluginInFox_OnEnterGame_m813157054 (PluginInFox_t2544798167 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginInFox::OnCreatRole(CEvent.ZEvent)
extern "C"  void PluginInFox_OnCreatRole_m1642572905 (PluginInFox_t2544798167 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginInFox::OnLevelUp(CEvent.ZEvent)
extern "C"  void PluginInFox_OnLevelUp_m1563498313 (PluginInFox_t2544798167 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginInFox::ClollectData(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginInFox_ClollectData_m1206213336 (PluginInFox_t2544798167 * __this, String_t* ___type0, String_t* ___serverid1, String_t* ___servername2, String_t* ___roleid3, String_t* ___rolename4, String_t* ___rolelevel5, String_t* ___roleGold6, String_t* ___VIPLv7, String_t* ___GameName8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginInFox::init(System.String,System.String)
extern "C"  void PluginInFox_init_m2296734590 (PluginInFox_t2544798167 * __this, String_t* ___AppID0, String_t* ___channel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginInFox::login()
extern "C"  void PluginInFox_login_m3656882651 (PluginInFox_t2544798167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginInFox::goZF(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginInFox_goZF_m113488602 (PluginInFox_t2544798167 * __this, String_t* ___amount0, String_t* ___orderId1, String_t* ___extra2, String_t* ___tempId3, String_t* ___serverId4, String_t* ___serverName5, String_t* ___roleId6, String_t* ___roleName7, String_t* ___setProductNumber8, String_t* ___setProductName9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginInFox::coloectData(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginInFox_coloectData_m3910746759 (PluginInFox_t2544798167 * __this, String_t* ___serverId0, String_t* ___servername1, String_t* ___roleid2, String_t* ___rolename3, String_t* ___rolelevel4, String_t* ___roleGold5, String_t* ___type6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginInFox::<OnLogoutSuccess>m__42F()
extern "C"  void PluginInFox_U3COnLogoutSuccessU3Em__42F_m3116981613 (PluginInFox_t2544798167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginInFox::ilo_get_Instance1()
extern "C"  VersionMgr_t1322950208 * PluginInFox_ilo_get_Instance1_m1293053327 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginInFox::ilo_get_currentVS2(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginInFox_ilo_get_currentVS2_m1304653691 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginInFox::ilo_get_DeviceID3(PluginsSdkMgr)
extern "C"  String_t* PluginInFox_ilo_get_DeviceID3_m1128015641 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginInFox::ilo_get_PluginsSdkMgr4()
extern "C"  PluginsSdkMgr_t3884624670 * PluginInFox_ilo_get_PluginsSdkMgr4_m1256282349 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginInFox::ilo_Logout5(System.Action)
extern "C"  void PluginInFox_ilo_Logout5_m3672533395 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginInFox::ilo_Log6(System.Object,System.Boolean)
extern "C"  void PluginInFox_ilo_Log6_m1003998878 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProductsCfgMgr PluginInFox::ilo_get_ProductsCfgMgr7()
extern "C"  ProductsCfgMgr_t2493714872 * PluginInFox_ilo_get_ProductsCfgMgr7_m615716282 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginInFox::ilo_goZF8(PluginInFox,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginInFox_ilo_goZF8_m3502229552 (Il2CppObject * __this /* static, unused */, PluginInFox_t2544798167 * ____this0, String_t* ___amount1, String_t* ___orderId2, String_t* ___extra3, String_t* ___tempId4, String_t* ___serverId5, String_t* ___serverName6, String_t* ___roleId7, String_t* ___roleName8, String_t* ___setProductNumber9, String_t* ___setProductName10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.LvUpInfo PluginInFox::ilo_Parse9(System.Object[])
extern "C"  LvUpInfo_t411687177 * PluginInFox_ilo_Parse9_m3468802313 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginInFox::ilo_OpenUserLogin10(PluginInFox)
extern "C"  void PluginInFox_ilo_OpenUserLogin10_m2625493289 (Il2CppObject * __this /* static, unused */, PluginInFox_t2544798167 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
