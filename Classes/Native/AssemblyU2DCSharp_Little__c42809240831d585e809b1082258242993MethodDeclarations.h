﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._c42809240831d585e809b108e4937c60
struct _c42809240831d585e809b108e4937c60_t2258242993;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._c42809240831d585e809b108e4937c60::.ctor()
extern "C"  void _c42809240831d585e809b108e4937c60__ctor_m2750345116 (_c42809240831d585e809b108e4937c60_t2258242993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c42809240831d585e809b108e4937c60::_c42809240831d585e809b108e4937c60m2(System.Int32)
extern "C"  int32_t _c42809240831d585e809b108e4937c60__c42809240831d585e809b108e4937c60m2_m1787898873 (_c42809240831d585e809b108e4937c60_t2258242993 * __this, int32_t ____c42809240831d585e809b108e4937c60a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c42809240831d585e809b108e4937c60::_c42809240831d585e809b108e4937c60m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _c42809240831d585e809b108e4937c60__c42809240831d585e809b108e4937c60m_m2078533341 (_c42809240831d585e809b108e4937c60_t2258242993 * __this, int32_t ____c42809240831d585e809b108e4937c60a0, int32_t ____c42809240831d585e809b108e4937c60331, int32_t ____c42809240831d585e809b108e4937c60c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
