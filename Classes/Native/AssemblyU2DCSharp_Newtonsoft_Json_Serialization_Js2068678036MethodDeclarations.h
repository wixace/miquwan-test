﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonSerializerInternalBase
struct JsonSerializerInternalBase_t2068678036;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;
// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object>
struct BidirectionalDictionary_2_t25693564;
// Newtonsoft.Json.Serialization.ErrorContext
struct ErrorContext_t18794611;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t3991598821;
// Newtonsoft.Json.Serialization.JsonContract
struct JsonContract_t1328848902;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer251850770.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Exception3991598821.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js1328848902.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js2068678036.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Erro18794611.h"

// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalBase::.ctor(Newtonsoft.Json.JsonSerializer)
extern "C"  void JsonSerializerInternalBase__ctor_m270309585 (JsonSerializerInternalBase_t2068678036 * __this, JsonSerializer_t251850770 * ___serializer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonSerializer Newtonsoft.Json.Serialization.JsonSerializerInternalBase::get_Serializer()
extern "C"  JsonSerializer_t251850770 * JsonSerializerInternalBase_get_Serializer_m3359244333 (JsonSerializerInternalBase_t2068678036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalBase::set_Serializer(Newtonsoft.Json.JsonSerializer)
extern "C"  void JsonSerializerInternalBase_set_Serializer_m2116923300 (JsonSerializerInternalBase_t2068678036 * __this, JsonSerializer_t251850770 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object> Newtonsoft.Json.Serialization.JsonSerializerInternalBase::get_DefaultReferenceMappings()
extern "C"  BidirectionalDictionary_2_t25693564 * JsonSerializerInternalBase_get_DefaultReferenceMappings_m2016463219 (JsonSerializerInternalBase_t2068678036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.ErrorContext Newtonsoft.Json.Serialization.JsonSerializerInternalBase::GetErrorContext(System.Object,System.Object,System.Exception)
extern "C"  ErrorContext_t18794611 * JsonSerializerInternalBase_GetErrorContext_m19753964 (JsonSerializerInternalBase_t2068678036 * __this, Il2CppObject * ___currentObject0, Il2CppObject * ___member1, Exception_t3991598821 * ___error2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalBase::ClearErrorContext()
extern "C"  void JsonSerializerInternalBase_ClearErrorContext_m1251934386 (JsonSerializerInternalBase_t2068678036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalBase::IsErrorHandled(System.Object,Newtonsoft.Json.Serialization.JsonContract,System.Object,System.Exception)
extern "C"  bool JsonSerializerInternalBase_IsErrorHandled_m1948851277 (JsonSerializerInternalBase_t2068678036 * __this, Il2CppObject * ___currentObject0, JsonContract_t1328848902 * ___contract1, Il2CppObject * ___keyValue2, Exception_t3991598821 * ___ex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.ErrorContext Newtonsoft.Json.Serialization.JsonSerializerInternalBase::ilo_GetErrorContext1(Newtonsoft.Json.Serialization.JsonSerializerInternalBase,System.Object,System.Object,System.Exception)
extern "C"  ErrorContext_t18794611 * JsonSerializerInternalBase_ilo_GetErrorContext1_m3181072135 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalBase_t2068678036 * ____this0, Il2CppObject * ___currentObject1, Il2CppObject * ___member2, Exception_t3991598821 * ___error3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.StreamingContext Newtonsoft.Json.Serialization.JsonSerializerInternalBase::ilo_get_Context2(Newtonsoft.Json.JsonSerializer)
extern "C"  StreamingContext_t2761351129  JsonSerializerInternalBase_ilo_get_Context2_m90681447 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalBase::ilo_get_Handled3(Newtonsoft.Json.Serialization.ErrorContext)
extern "C"  bool JsonSerializerInternalBase_ilo_get_Handled3_m2536492689 (Il2CppObject * __this /* static, unused */, ErrorContext_t18794611 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonSerializer Newtonsoft.Json.Serialization.JsonSerializerInternalBase::ilo_get_Serializer4(Newtonsoft.Json.Serialization.JsonSerializerInternalBase)
extern "C"  JsonSerializer_t251850770 * JsonSerializerInternalBase_ilo_get_Serializer4_m3291162427 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalBase_t2068678036 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
