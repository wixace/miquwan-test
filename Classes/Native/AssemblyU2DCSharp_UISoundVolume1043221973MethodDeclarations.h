﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UISoundVolume
struct UISoundVolume_t1043221973;
// UIProgressBar
struct UIProgressBar_t168062834;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIProgressBar168062834.h"

// System.Void UISoundVolume::.ctor()
extern "C"  void UISoundVolume__ctor_m342741814 (UISoundVolume_t1043221973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISoundVolume::Awake()
extern "C"  void UISoundVolume_Awake_m580347033 (UISoundVolume_t1043221973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISoundVolume::OnChange()
extern "C"  void UISoundVolume_OnChange_m3345921917 (UISoundVolume_t1043221973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UISoundVolume::ilo_get_value1(UIProgressBar)
extern "C"  float UISoundVolume_ilo_get_value1_m1465609026 (Il2CppObject * __this /* static, unused */, UIProgressBar_t168062834 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISoundVolume::ilo_set_soundVolume2(System.Single)
extern "C"  void UISoundVolume_ilo_set_soundVolume2_m2918493412 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
