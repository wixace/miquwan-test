﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraHelperGenerated
struct CameraHelperGenerated_t34562940;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// CameraSmoothFollow
struct CameraSmoothFollow_t2624612068;
// CameraHelper
struct CameraHelper_t3196871507;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_CameraHelper3196871507.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void CameraHelperGenerated::.ctor()
extern "C"  void CameraHelperGenerated__ctor_m3700015471 (CameraHelperGenerated_t34562940 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraHelperGenerated::CameraHelper_CameraHelper1(JSVCall,System.Int32)
extern "C"  bool CameraHelperGenerated_CameraHelper_CameraHelper1_m347121027 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelperGenerated::CameraHelper_MOVE_COMPLETE(JSVCall)
extern "C"  void CameraHelperGenerated_CameraHelper_MOVE_COMPLETE_m4009919895 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelperGenerated::CameraHelper_CHANGE_TARGET_COMPLETE(JSVCall)
extern "C"  void CameraHelperGenerated_CameraHelper_CHANGE_TARGET_COMPLETE_m1110489046 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelperGenerated::CameraHelper_fieldMin(JSVCall)
extern "C"  void CameraHelperGenerated_CameraHelper_fieldMin_m3015795830 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelperGenerated::CameraHelper_timeScale(JSVCall)
extern "C"  void CameraHelperGenerated_CameraHelper_timeScale_m2444391873 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelperGenerated::CameraHelper_effectCamera(JSVCall)
extern "C"  void CameraHelperGenerated_CameraHelper_effectCamera_m3017837848 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelperGenerated::CameraHelper_instance(JSVCall)
extern "C"  void CameraHelperGenerated_CameraHelper_instance_m619873145 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelperGenerated::CameraHelper_motionBluring(JSVCall)
extern "C"  void CameraHelperGenerated_CameraHelper_motionBluring_m1494022073 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelperGenerated::CameraHelper_csf(JSVCall)
extern "C"  void CameraHelperGenerated_CameraHelper_csf_m3483417704 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelperGenerated::CameraHelper_inScaler(JSVCall)
extern "C"  void CameraHelperGenerated_CameraHelper_inScaler_m1810608417 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraHelperGenerated::CameraHelper_BreakTeamSkillCamera(JSVCall,System.Int32)
extern "C"  bool CameraHelperGenerated_CameraHelper_BreakTeamSkillCamera_m1589200567 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraHelperGenerated::CameraHelper_ChangeCameraTarget__Boolean__Int32__Boolean__Single__String(JSVCall,System.Int32)
extern "C"  bool CameraHelperGenerated_CameraHelper_ChangeCameraTarget__Boolean__Int32__Boolean__Single__String_m3371350938 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraHelperGenerated::CameraHelper_LockView__Boolean__Int32__Single__Single__Single__Single__String(JSVCall,System.Int32)
extern "C"  bool CameraHelperGenerated_CameraHelper_LockView__Boolean__Int32__Single__Single__Single__Single__String_m2649757892 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraHelperGenerated::CameraHelper_MotionBlur__Transform__Single(JSVCall,System.Int32)
extern "C"  bool CameraHelperGenerated_CameraHelper_MotionBlur__Transform__Single_m745903292 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraHelperGenerated::CameraHelper_MotionBlur__Transform(JSVCall,System.Int32)
extern "C"  bool CameraHelperGenerated_CameraHelper_MotionBlur__Transform_m4262820212 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraHelperGenerated::CameraHelper_MoveCamera__Boolean__Int32__Boolean__Single__Int32(JSVCall,System.Int32)
extern "C"  bool CameraHelperGenerated_CameraHelper_MoveCamera__Boolean__Int32__Boolean__Single__Int32_m1715428951 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraHelperGenerated::CameraHelper_PlayTeamSkillCamera(JSVCall,System.Int32)
extern "C"  bool CameraHelperGenerated_CameraHelper_PlayTeamSkillCamera_m3960148042 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraHelperGenerated::CameraHelper_Scaler__Transform__Single__Single__Single__Vector3(JSVCall,System.Int32)
extern "C"  bool CameraHelperGenerated_CameraHelper_Scaler__Transform__Single__Single__Single__Vector3_m2254721777 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraHelperGenerated::CameraHelper_ScalerClear(JSVCall,System.Int32)
extern "C"  bool CameraHelperGenerated_CameraHelper_ScalerClear_m925807274 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraHelperGenerated::CameraHelper_ScreenShake__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool CameraHelperGenerated_CameraHelper_ScreenShake__Single__Single__Single_m1591406839 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraHelperGenerated::CameraHelper_ScreenShake__Single__Single(JSVCall,System.Int32)
extern "C"  bool CameraHelperGenerated_CameraHelper_ScreenShake__Single__Single_m2288795823 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraHelperGenerated::CameraHelper_StopTeamSkillCamera(JSVCall,System.Int32)
extern "C"  bool CameraHelperGenerated_CameraHelper_StopTeamSkillCamera_m3707837500 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraHelperGenerated::CameraHelper_UnLockView(JSVCall,System.Int32)
extern "C"  bool CameraHelperGenerated_CameraHelper_UnLockView_m1727917414 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelperGenerated::__Register()
extern "C"  void CameraHelperGenerated___Register_m192039096 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraHelperGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t CameraHelperGenerated_ilo_getObject1_m1294483111 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelperGenerated::ilo_setSingle2(System.Int32,System.Single)
extern "C"  void CameraHelperGenerated_ilo_setSingle2_m1236733222 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraHelperGenerated::ilo_getSingle3(System.Int32)
extern "C"  float CameraHelperGenerated_ilo_getSingle3_m851064706 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraHelperGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t CameraHelperGenerated_ilo_setObject4_m2414342370 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CameraHelperGenerated::ilo_getObject5(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * CameraHelperGenerated_ilo_getObject5_m2993284442 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraHelperGenerated::ilo_getBooleanS6(System.Int32)
extern "C"  bool CameraHelperGenerated_ilo_getBooleanS6_m2137686234 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CameraSmoothFollow CameraHelperGenerated::ilo_get_csf7(CameraHelper)
extern "C"  CameraSmoothFollow_t2624612068 * CameraHelperGenerated_ilo_get_csf7_m3419244710 (Il2CppObject * __this /* static, unused */, CameraHelper_t3196871507 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelperGenerated::ilo_setBooleanS8(System.Int32,System.Boolean)
extern "C"  void CameraHelperGenerated_ilo_setBooleanS8_m1688488441 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelperGenerated::ilo_BreakTeamSkillCamera9(CameraHelper)
extern "C"  void CameraHelperGenerated_ilo_BreakTeamSkillCamera9_m1640743046 (Il2CppObject * __this /* static, unused */, CameraHelper_t3196871507 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CameraHelperGenerated::ilo_getStringS10(System.Int32)
extern "C"  String_t* CameraHelperGenerated_ilo_getStringS10_m1229130209 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelperGenerated::ilo_ChangeCameraTarget11(CameraHelper,System.Boolean,System.Int32,System.Boolean,System.Single,System.String)
extern "C"  void CameraHelperGenerated_ilo_ChangeCameraTarget11_m119840201 (Il2CppObject * __this /* static, unused */, CameraHelper_t3196871507 * ____this0, bool ___isHero1, int32_t ___id2, bool ___isMove3, float ___time4, String_t* ___objName5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelperGenerated::ilo_MotionBlur12(CameraHelper,UnityEngine.Transform,System.Single)
extern "C"  void CameraHelperGenerated_ilo_MotionBlur12_m3537962159 (Il2CppObject * __this /* static, unused */, CameraHelper_t3196871507 * ____this0, Transform_t1659122786 * ___target1, float ___distance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraHelperGenerated::ilo_getInt3213(System.Int32)
extern "C"  int32_t CameraHelperGenerated_ilo_getInt3213_m3143136901 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelperGenerated::ilo_MoveCamera14(CameraHelper,System.Boolean,System.Int32,System.Boolean,System.Single,System.Int32)
extern "C"  void CameraHelperGenerated_ilo_MoveCamera14_m2355078813 (Il2CppObject * __this /* static, unused */, CameraHelper_t3196871507 * ____this0, bool ___isHero1, int32_t ___id2, bool ___isMove3, float ___time4, int32_t ___AIid5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CameraHelperGenerated::ilo_getVector3S15(System.Int32)
extern "C"  Vector3_t4282066566  CameraHelperGenerated_ilo_getVector3S15_m4126604290 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelperGenerated::ilo_Scaler16(CameraHelper,UnityEngine.Transform,System.Single,System.Single,System.Single,UnityEngine.Vector3)
extern "C"  void CameraHelperGenerated_ilo_Scaler16_m3750824059 (Il2CppObject * __this /* static, unused */, CameraHelper_t3196871507 * ____this0, Transform_t1659122786 * ___target1, float ___zoomInTime2, float ___waitTime3, float ___zoomOutTime4, Vector3_t4282066566  ___zoominoffset5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelperGenerated::ilo_ScalerClear17(CameraHelper)
extern "C"  void CameraHelperGenerated_ilo_ScalerClear17_m359061490 (Il2CppObject * __this /* static, unused */, CameraHelper_t3196871507 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelperGenerated::ilo_StopTeamSkillCamera18(CameraHelper)
extern "C"  void CameraHelperGenerated_ilo_StopTeamSkillCamera18_m38014021 (Il2CppObject * __this /* static, unused */, CameraHelper_t3196871507 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelperGenerated::ilo_UnLockView19(CameraHelper)
extern "C"  void CameraHelperGenerated_ilo_UnLockView19_m2052393926 (Il2CppObject * __this /* static, unused */, CameraHelper_t3196871507 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
