﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIRoot
struct UIRoot_t2503447958;
// UITexture
struct UITexture_t3903132647;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIRootScale
struct  UIRootScale_t1542754644  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 UIRootScale::aspectRatioHeight
	int32_t ___aspectRatioHeight_2;
	// System.Int32 UIRootScale::aspectRatioWidth
	int32_t ___aspectRatioWidth_3;
	// System.Boolean UIRootScale::runOnlyOnce
	bool ___runOnlyOnce_4;
	// UIRoot UIRootScale::mRoot
	UIRoot_t2503447958 * ___mRoot_5;
	// System.Boolean UIRootScale::mStarted
	bool ___mStarted_6;
	// UITexture UIRootScale::shang
	UITexture_t3903132647 * ___shang_7;
	// UITexture UIRootScale::xia
	UITexture_t3903132647 * ___xia_8;

public:
	inline static int32_t get_offset_of_aspectRatioHeight_2() { return static_cast<int32_t>(offsetof(UIRootScale_t1542754644, ___aspectRatioHeight_2)); }
	inline int32_t get_aspectRatioHeight_2() const { return ___aspectRatioHeight_2; }
	inline int32_t* get_address_of_aspectRatioHeight_2() { return &___aspectRatioHeight_2; }
	inline void set_aspectRatioHeight_2(int32_t value)
	{
		___aspectRatioHeight_2 = value;
	}

	inline static int32_t get_offset_of_aspectRatioWidth_3() { return static_cast<int32_t>(offsetof(UIRootScale_t1542754644, ___aspectRatioWidth_3)); }
	inline int32_t get_aspectRatioWidth_3() const { return ___aspectRatioWidth_3; }
	inline int32_t* get_address_of_aspectRatioWidth_3() { return &___aspectRatioWidth_3; }
	inline void set_aspectRatioWidth_3(int32_t value)
	{
		___aspectRatioWidth_3 = value;
	}

	inline static int32_t get_offset_of_runOnlyOnce_4() { return static_cast<int32_t>(offsetof(UIRootScale_t1542754644, ___runOnlyOnce_4)); }
	inline bool get_runOnlyOnce_4() const { return ___runOnlyOnce_4; }
	inline bool* get_address_of_runOnlyOnce_4() { return &___runOnlyOnce_4; }
	inline void set_runOnlyOnce_4(bool value)
	{
		___runOnlyOnce_4 = value;
	}

	inline static int32_t get_offset_of_mRoot_5() { return static_cast<int32_t>(offsetof(UIRootScale_t1542754644, ___mRoot_5)); }
	inline UIRoot_t2503447958 * get_mRoot_5() const { return ___mRoot_5; }
	inline UIRoot_t2503447958 ** get_address_of_mRoot_5() { return &___mRoot_5; }
	inline void set_mRoot_5(UIRoot_t2503447958 * value)
	{
		___mRoot_5 = value;
		Il2CppCodeGenWriteBarrier(&___mRoot_5, value);
	}

	inline static int32_t get_offset_of_mStarted_6() { return static_cast<int32_t>(offsetof(UIRootScale_t1542754644, ___mStarted_6)); }
	inline bool get_mStarted_6() const { return ___mStarted_6; }
	inline bool* get_address_of_mStarted_6() { return &___mStarted_6; }
	inline void set_mStarted_6(bool value)
	{
		___mStarted_6 = value;
	}

	inline static int32_t get_offset_of_shang_7() { return static_cast<int32_t>(offsetof(UIRootScale_t1542754644, ___shang_7)); }
	inline UITexture_t3903132647 * get_shang_7() const { return ___shang_7; }
	inline UITexture_t3903132647 ** get_address_of_shang_7() { return &___shang_7; }
	inline void set_shang_7(UITexture_t3903132647 * value)
	{
		___shang_7 = value;
		Il2CppCodeGenWriteBarrier(&___shang_7, value);
	}

	inline static int32_t get_offset_of_xia_8() { return static_cast<int32_t>(offsetof(UIRootScale_t1542754644, ___xia_8)); }
	inline UITexture_t3903132647 * get_xia_8() const { return ___xia_8; }
	inline UITexture_t3903132647 ** get_address_of_xia_8() { return &___xia_8; }
	inline void set_xia_8(UITexture_t3903132647 * value)
	{
		___xia_8 = value;
		Il2CppCodeGenWriteBarrier(&___xia_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
