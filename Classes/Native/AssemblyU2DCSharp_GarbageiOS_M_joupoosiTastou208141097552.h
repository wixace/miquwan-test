﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_joupoosiTastou208
struct  M_joupoosiTastou208_t141097552  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_joupoosiTastou208::_kumaiwhi
	int32_t ____kumaiwhi_0;
	// System.UInt32 GarbageiOS.M_joupoosiTastou208::_nayxaChaisertro
	uint32_t ____nayxaChaisertro_1;
	// System.Single GarbageiOS.M_joupoosiTastou208::_vewhaituTemrase
	float ____vewhaituTemrase_2;
	// System.String GarbageiOS.M_joupoosiTastou208::_kefar
	String_t* ____kefar_3;
	// System.Boolean GarbageiOS.M_joupoosiTastou208::_xaistiseGissinai
	bool ____xaistiseGissinai_4;
	// System.String GarbageiOS.M_joupoosiTastou208::_pocoPastuhel
	String_t* ____pocoPastuhel_5;
	// System.Single GarbageiOS.M_joupoosiTastou208::_nayjiper
	float ____nayjiper_6;
	// System.Single GarbageiOS.M_joupoosiTastou208::_nokaiMulistor
	float ____nokaiMulistor_7;
	// System.Boolean GarbageiOS.M_joupoosiTastou208::_molurrasHime
	bool ____molurrasHime_8;
	// System.Single GarbageiOS.M_joupoosiTastou208::_towkirFeafo
	float ____towkirFeafo_9;
	// System.Single GarbageiOS.M_joupoosiTastou208::_lesestaw
	float ____lesestaw_10;
	// System.String GarbageiOS.M_joupoosiTastou208::_burha
	String_t* ____burha_11;
	// System.Boolean GarbageiOS.M_joupoosiTastou208::_taljaswa
	bool ____taljaswa_12;
	// System.Single GarbageiOS.M_joupoosiTastou208::_learzallMairmair
	float ____learzallMairmair_13;
	// System.String GarbageiOS.M_joupoosiTastou208::_cordoyasSarner
	String_t* ____cordoyasSarner_14;
	// System.Boolean GarbageiOS.M_joupoosiTastou208::_qayurqawDralem
	bool ____qayurqawDralem_15;
	// System.UInt32 GarbageiOS.M_joupoosiTastou208::_drafow
	uint32_t ____drafow_16;

public:
	inline static int32_t get_offset_of__kumaiwhi_0() { return static_cast<int32_t>(offsetof(M_joupoosiTastou208_t141097552, ____kumaiwhi_0)); }
	inline int32_t get__kumaiwhi_0() const { return ____kumaiwhi_0; }
	inline int32_t* get_address_of__kumaiwhi_0() { return &____kumaiwhi_0; }
	inline void set__kumaiwhi_0(int32_t value)
	{
		____kumaiwhi_0 = value;
	}

	inline static int32_t get_offset_of__nayxaChaisertro_1() { return static_cast<int32_t>(offsetof(M_joupoosiTastou208_t141097552, ____nayxaChaisertro_1)); }
	inline uint32_t get__nayxaChaisertro_1() const { return ____nayxaChaisertro_1; }
	inline uint32_t* get_address_of__nayxaChaisertro_1() { return &____nayxaChaisertro_1; }
	inline void set__nayxaChaisertro_1(uint32_t value)
	{
		____nayxaChaisertro_1 = value;
	}

	inline static int32_t get_offset_of__vewhaituTemrase_2() { return static_cast<int32_t>(offsetof(M_joupoosiTastou208_t141097552, ____vewhaituTemrase_2)); }
	inline float get__vewhaituTemrase_2() const { return ____vewhaituTemrase_2; }
	inline float* get_address_of__vewhaituTemrase_2() { return &____vewhaituTemrase_2; }
	inline void set__vewhaituTemrase_2(float value)
	{
		____vewhaituTemrase_2 = value;
	}

	inline static int32_t get_offset_of__kefar_3() { return static_cast<int32_t>(offsetof(M_joupoosiTastou208_t141097552, ____kefar_3)); }
	inline String_t* get__kefar_3() const { return ____kefar_3; }
	inline String_t** get_address_of__kefar_3() { return &____kefar_3; }
	inline void set__kefar_3(String_t* value)
	{
		____kefar_3 = value;
		Il2CppCodeGenWriteBarrier(&____kefar_3, value);
	}

	inline static int32_t get_offset_of__xaistiseGissinai_4() { return static_cast<int32_t>(offsetof(M_joupoosiTastou208_t141097552, ____xaistiseGissinai_4)); }
	inline bool get__xaistiseGissinai_4() const { return ____xaistiseGissinai_4; }
	inline bool* get_address_of__xaistiseGissinai_4() { return &____xaistiseGissinai_4; }
	inline void set__xaistiseGissinai_4(bool value)
	{
		____xaistiseGissinai_4 = value;
	}

	inline static int32_t get_offset_of__pocoPastuhel_5() { return static_cast<int32_t>(offsetof(M_joupoosiTastou208_t141097552, ____pocoPastuhel_5)); }
	inline String_t* get__pocoPastuhel_5() const { return ____pocoPastuhel_5; }
	inline String_t** get_address_of__pocoPastuhel_5() { return &____pocoPastuhel_5; }
	inline void set__pocoPastuhel_5(String_t* value)
	{
		____pocoPastuhel_5 = value;
		Il2CppCodeGenWriteBarrier(&____pocoPastuhel_5, value);
	}

	inline static int32_t get_offset_of__nayjiper_6() { return static_cast<int32_t>(offsetof(M_joupoosiTastou208_t141097552, ____nayjiper_6)); }
	inline float get__nayjiper_6() const { return ____nayjiper_6; }
	inline float* get_address_of__nayjiper_6() { return &____nayjiper_6; }
	inline void set__nayjiper_6(float value)
	{
		____nayjiper_6 = value;
	}

	inline static int32_t get_offset_of__nokaiMulistor_7() { return static_cast<int32_t>(offsetof(M_joupoosiTastou208_t141097552, ____nokaiMulistor_7)); }
	inline float get__nokaiMulistor_7() const { return ____nokaiMulistor_7; }
	inline float* get_address_of__nokaiMulistor_7() { return &____nokaiMulistor_7; }
	inline void set__nokaiMulistor_7(float value)
	{
		____nokaiMulistor_7 = value;
	}

	inline static int32_t get_offset_of__molurrasHime_8() { return static_cast<int32_t>(offsetof(M_joupoosiTastou208_t141097552, ____molurrasHime_8)); }
	inline bool get__molurrasHime_8() const { return ____molurrasHime_8; }
	inline bool* get_address_of__molurrasHime_8() { return &____molurrasHime_8; }
	inline void set__molurrasHime_8(bool value)
	{
		____molurrasHime_8 = value;
	}

	inline static int32_t get_offset_of__towkirFeafo_9() { return static_cast<int32_t>(offsetof(M_joupoosiTastou208_t141097552, ____towkirFeafo_9)); }
	inline float get__towkirFeafo_9() const { return ____towkirFeafo_9; }
	inline float* get_address_of__towkirFeafo_9() { return &____towkirFeafo_9; }
	inline void set__towkirFeafo_9(float value)
	{
		____towkirFeafo_9 = value;
	}

	inline static int32_t get_offset_of__lesestaw_10() { return static_cast<int32_t>(offsetof(M_joupoosiTastou208_t141097552, ____lesestaw_10)); }
	inline float get__lesestaw_10() const { return ____lesestaw_10; }
	inline float* get_address_of__lesestaw_10() { return &____lesestaw_10; }
	inline void set__lesestaw_10(float value)
	{
		____lesestaw_10 = value;
	}

	inline static int32_t get_offset_of__burha_11() { return static_cast<int32_t>(offsetof(M_joupoosiTastou208_t141097552, ____burha_11)); }
	inline String_t* get__burha_11() const { return ____burha_11; }
	inline String_t** get_address_of__burha_11() { return &____burha_11; }
	inline void set__burha_11(String_t* value)
	{
		____burha_11 = value;
		Il2CppCodeGenWriteBarrier(&____burha_11, value);
	}

	inline static int32_t get_offset_of__taljaswa_12() { return static_cast<int32_t>(offsetof(M_joupoosiTastou208_t141097552, ____taljaswa_12)); }
	inline bool get__taljaswa_12() const { return ____taljaswa_12; }
	inline bool* get_address_of__taljaswa_12() { return &____taljaswa_12; }
	inline void set__taljaswa_12(bool value)
	{
		____taljaswa_12 = value;
	}

	inline static int32_t get_offset_of__learzallMairmair_13() { return static_cast<int32_t>(offsetof(M_joupoosiTastou208_t141097552, ____learzallMairmair_13)); }
	inline float get__learzallMairmair_13() const { return ____learzallMairmair_13; }
	inline float* get_address_of__learzallMairmair_13() { return &____learzallMairmair_13; }
	inline void set__learzallMairmair_13(float value)
	{
		____learzallMairmair_13 = value;
	}

	inline static int32_t get_offset_of__cordoyasSarner_14() { return static_cast<int32_t>(offsetof(M_joupoosiTastou208_t141097552, ____cordoyasSarner_14)); }
	inline String_t* get__cordoyasSarner_14() const { return ____cordoyasSarner_14; }
	inline String_t** get_address_of__cordoyasSarner_14() { return &____cordoyasSarner_14; }
	inline void set__cordoyasSarner_14(String_t* value)
	{
		____cordoyasSarner_14 = value;
		Il2CppCodeGenWriteBarrier(&____cordoyasSarner_14, value);
	}

	inline static int32_t get_offset_of__qayurqawDralem_15() { return static_cast<int32_t>(offsetof(M_joupoosiTastou208_t141097552, ____qayurqawDralem_15)); }
	inline bool get__qayurqawDralem_15() const { return ____qayurqawDralem_15; }
	inline bool* get_address_of__qayurqawDralem_15() { return &____qayurqawDralem_15; }
	inline void set__qayurqawDralem_15(bool value)
	{
		____qayurqawDralem_15 = value;
	}

	inline static int32_t get_offset_of__drafow_16() { return static_cast<int32_t>(offsetof(M_joupoosiTastou208_t141097552, ____drafow_16)); }
	inline uint32_t get__drafow_16() const { return ____drafow_16; }
	inline uint32_t* get_address_of__drafow_16() { return &____drafow_16; }
	inline void set__drafow_16(uint32_t value)
	{
		____drafow_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
