﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.String>
struct Action_1_t403047693;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "mscorlib_System_IntPtr4010401971.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebViewObject
struct  WebViewObject_t388577432  : public MonoBehaviour_t667441552
{
public:
	// System.Action`1<System.String> WebViewObject::onJS
	Action_1_t403047693 * ___onJS_2;
	// System.Action`1<System.String> WebViewObject::onError
	Action_1_t403047693 * ___onError_3;
	// System.Action`1<System.String> WebViewObject::onHttpError
	Action_1_t403047693 * ___onHttpError_4;
	// System.Action`1<System.String> WebViewObject::onStarted
	Action_1_t403047693 * ___onStarted_5;
	// System.Action`1<System.String> WebViewObject::onLoaded
	Action_1_t403047693 * ___onLoaded_6;
	// System.Boolean WebViewObject::visibility
	bool ___visibility_7;
	// System.Boolean WebViewObject::alertDialogEnabled
	bool ___alertDialogEnabled_8;
	// System.Boolean WebViewObject::scrollBounceEnabled
	bool ___scrollBounceEnabled_9;
	// System.Int32 WebViewObject::mMarginLeft
	int32_t ___mMarginLeft_10;
	// System.Int32 WebViewObject::mMarginTop
	int32_t ___mMarginTop_11;
	// System.Int32 WebViewObject::mMarginRight
	int32_t ___mMarginRight_12;
	// System.Int32 WebViewObject::mMarginBottom
	int32_t ___mMarginBottom_13;
	// System.IntPtr WebViewObject::webView
	IntPtr_t ___webView_14;

public:
	inline static int32_t get_offset_of_onJS_2() { return static_cast<int32_t>(offsetof(WebViewObject_t388577432, ___onJS_2)); }
	inline Action_1_t403047693 * get_onJS_2() const { return ___onJS_2; }
	inline Action_1_t403047693 ** get_address_of_onJS_2() { return &___onJS_2; }
	inline void set_onJS_2(Action_1_t403047693 * value)
	{
		___onJS_2 = value;
		Il2CppCodeGenWriteBarrier(&___onJS_2, value);
	}

	inline static int32_t get_offset_of_onError_3() { return static_cast<int32_t>(offsetof(WebViewObject_t388577432, ___onError_3)); }
	inline Action_1_t403047693 * get_onError_3() const { return ___onError_3; }
	inline Action_1_t403047693 ** get_address_of_onError_3() { return &___onError_3; }
	inline void set_onError_3(Action_1_t403047693 * value)
	{
		___onError_3 = value;
		Il2CppCodeGenWriteBarrier(&___onError_3, value);
	}

	inline static int32_t get_offset_of_onHttpError_4() { return static_cast<int32_t>(offsetof(WebViewObject_t388577432, ___onHttpError_4)); }
	inline Action_1_t403047693 * get_onHttpError_4() const { return ___onHttpError_4; }
	inline Action_1_t403047693 ** get_address_of_onHttpError_4() { return &___onHttpError_4; }
	inline void set_onHttpError_4(Action_1_t403047693 * value)
	{
		___onHttpError_4 = value;
		Il2CppCodeGenWriteBarrier(&___onHttpError_4, value);
	}

	inline static int32_t get_offset_of_onStarted_5() { return static_cast<int32_t>(offsetof(WebViewObject_t388577432, ___onStarted_5)); }
	inline Action_1_t403047693 * get_onStarted_5() const { return ___onStarted_5; }
	inline Action_1_t403047693 ** get_address_of_onStarted_5() { return &___onStarted_5; }
	inline void set_onStarted_5(Action_1_t403047693 * value)
	{
		___onStarted_5 = value;
		Il2CppCodeGenWriteBarrier(&___onStarted_5, value);
	}

	inline static int32_t get_offset_of_onLoaded_6() { return static_cast<int32_t>(offsetof(WebViewObject_t388577432, ___onLoaded_6)); }
	inline Action_1_t403047693 * get_onLoaded_6() const { return ___onLoaded_6; }
	inline Action_1_t403047693 ** get_address_of_onLoaded_6() { return &___onLoaded_6; }
	inline void set_onLoaded_6(Action_1_t403047693 * value)
	{
		___onLoaded_6 = value;
		Il2CppCodeGenWriteBarrier(&___onLoaded_6, value);
	}

	inline static int32_t get_offset_of_visibility_7() { return static_cast<int32_t>(offsetof(WebViewObject_t388577432, ___visibility_7)); }
	inline bool get_visibility_7() const { return ___visibility_7; }
	inline bool* get_address_of_visibility_7() { return &___visibility_7; }
	inline void set_visibility_7(bool value)
	{
		___visibility_7 = value;
	}

	inline static int32_t get_offset_of_alertDialogEnabled_8() { return static_cast<int32_t>(offsetof(WebViewObject_t388577432, ___alertDialogEnabled_8)); }
	inline bool get_alertDialogEnabled_8() const { return ___alertDialogEnabled_8; }
	inline bool* get_address_of_alertDialogEnabled_8() { return &___alertDialogEnabled_8; }
	inline void set_alertDialogEnabled_8(bool value)
	{
		___alertDialogEnabled_8 = value;
	}

	inline static int32_t get_offset_of_scrollBounceEnabled_9() { return static_cast<int32_t>(offsetof(WebViewObject_t388577432, ___scrollBounceEnabled_9)); }
	inline bool get_scrollBounceEnabled_9() const { return ___scrollBounceEnabled_9; }
	inline bool* get_address_of_scrollBounceEnabled_9() { return &___scrollBounceEnabled_9; }
	inline void set_scrollBounceEnabled_9(bool value)
	{
		___scrollBounceEnabled_9 = value;
	}

	inline static int32_t get_offset_of_mMarginLeft_10() { return static_cast<int32_t>(offsetof(WebViewObject_t388577432, ___mMarginLeft_10)); }
	inline int32_t get_mMarginLeft_10() const { return ___mMarginLeft_10; }
	inline int32_t* get_address_of_mMarginLeft_10() { return &___mMarginLeft_10; }
	inline void set_mMarginLeft_10(int32_t value)
	{
		___mMarginLeft_10 = value;
	}

	inline static int32_t get_offset_of_mMarginTop_11() { return static_cast<int32_t>(offsetof(WebViewObject_t388577432, ___mMarginTop_11)); }
	inline int32_t get_mMarginTop_11() const { return ___mMarginTop_11; }
	inline int32_t* get_address_of_mMarginTop_11() { return &___mMarginTop_11; }
	inline void set_mMarginTop_11(int32_t value)
	{
		___mMarginTop_11 = value;
	}

	inline static int32_t get_offset_of_mMarginRight_12() { return static_cast<int32_t>(offsetof(WebViewObject_t388577432, ___mMarginRight_12)); }
	inline int32_t get_mMarginRight_12() const { return ___mMarginRight_12; }
	inline int32_t* get_address_of_mMarginRight_12() { return &___mMarginRight_12; }
	inline void set_mMarginRight_12(int32_t value)
	{
		___mMarginRight_12 = value;
	}

	inline static int32_t get_offset_of_mMarginBottom_13() { return static_cast<int32_t>(offsetof(WebViewObject_t388577432, ___mMarginBottom_13)); }
	inline int32_t get_mMarginBottom_13() const { return ___mMarginBottom_13; }
	inline int32_t* get_address_of_mMarginBottom_13() { return &___mMarginBottom_13; }
	inline void set_mMarginBottom_13(int32_t value)
	{
		___mMarginBottom_13 = value;
	}

	inline static int32_t get_offset_of_webView_14() { return static_cast<int32_t>(offsetof(WebViewObject_t388577432, ___webView_14)); }
	inline IntPtr_t get_webView_14() const { return ___webView_14; }
	inline IntPtr_t* get_address_of_webView_14() { return &___webView_14; }
	inline void set_webView_14(IntPtr_t value)
	{
		___webView_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
