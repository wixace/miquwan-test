﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._986e7d6bf24cf1d1e32d088e197b6fc6
struct _986e7d6bf24cf1d1e32d088e197b6fc6_t2438134174;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__986e7d6bf24cf1d1e32d088e2438134174.h"

// System.Void Little._986e7d6bf24cf1d1e32d088e197b6fc6::.ctor()
extern "C"  void _986e7d6bf24cf1d1e32d088e197b6fc6__ctor_m3138133583 (_986e7d6bf24cf1d1e32d088e197b6fc6_t2438134174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._986e7d6bf24cf1d1e32d088e197b6fc6::_986e7d6bf24cf1d1e32d088e197b6fc6m2(System.Int32)
extern "C"  int32_t _986e7d6bf24cf1d1e32d088e197b6fc6__986e7d6bf24cf1d1e32d088e197b6fc6m2_m2463808729 (_986e7d6bf24cf1d1e32d088e197b6fc6_t2438134174 * __this, int32_t ____986e7d6bf24cf1d1e32d088e197b6fc6a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._986e7d6bf24cf1d1e32d088e197b6fc6::_986e7d6bf24cf1d1e32d088e197b6fc6m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _986e7d6bf24cf1d1e32d088e197b6fc6__986e7d6bf24cf1d1e32d088e197b6fc6m_m3836023293 (_986e7d6bf24cf1d1e32d088e197b6fc6_t2438134174 * __this, int32_t ____986e7d6bf24cf1d1e32d088e197b6fc6a0, int32_t ____986e7d6bf24cf1d1e32d088e197b6fc6681, int32_t ____986e7d6bf24cf1d1e32d088e197b6fc6c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._986e7d6bf24cf1d1e32d088e197b6fc6::ilo__986e7d6bf24cf1d1e32d088e197b6fc6m21(Little._986e7d6bf24cf1d1e32d088e197b6fc6,System.Int32)
extern "C"  int32_t _986e7d6bf24cf1d1e32d088e197b6fc6_ilo__986e7d6bf24cf1d1e32d088e197b6fc6m21_m945204517 (Il2CppObject * __this /* static, unused */, _986e7d6bf24cf1d1e32d088e197b6fc6_t2438134174 * ____this0, int32_t ____986e7d6bf24cf1d1e32d088e197b6fc6a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
