﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_kone77
struct M_kone77_t548720011;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_kone77::.ctor()
extern "C"  void M_kone77__ctor_m350830344 (M_kone77_t548720011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kone77::M_setaytrouWimir0(System.String[],System.Int32)
extern "C"  void M_kone77_M_setaytrouWimir0_m4106648853 (M_kone77_t548720011 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kone77::M_foofoostere1(System.String[],System.Int32)
extern "C"  void M_kone77_M_foofoostere1_m3590819903 (M_kone77_t548720011 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kone77::M_nalu2(System.String[],System.Int32)
extern "C"  void M_kone77_M_nalu2_m905025793 (M_kone77_t548720011 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kone77::M_jiscurball3(System.String[],System.Int32)
extern "C"  void M_kone77_M_jiscurball3_m149843187 (M_kone77_t548720011 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
