﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<TwistGesture>
struct List_1_t1271579410;
// FingerGestures/FingerList
struct FingerList_t1886137443;
// GestureRecognizerTS`1/GestureEventHandler<TwistGesture>
struct GestureEventHandler_t2030454774;
// System.Predicate`1<TwistGesture>
struct Predicate_1_t3809418037;

#include "AssemblyU2DCSharp_GestureRecognizer3512875949.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GestureRecognizerTS`1<TwistGesture>
struct  GestureRecognizerTS_1_t1291560060  : public GestureRecognizer_t3512875949
{
public:
	// System.Collections.Generic.List`1<T> GestureRecognizerTS`1::gestures
	List_1_t1271579410 * ___gestures_15;
	// GestureRecognizerTS`1/GestureEventHandler<T> GestureRecognizerTS`1::OnGesture
	GestureEventHandler_t2030454774 * ___OnGesture_17;

public:
	inline static int32_t get_offset_of_gestures_15() { return static_cast<int32_t>(offsetof(GestureRecognizerTS_1_t1291560060, ___gestures_15)); }
	inline List_1_t1271579410 * get_gestures_15() const { return ___gestures_15; }
	inline List_1_t1271579410 ** get_address_of_gestures_15() { return &___gestures_15; }
	inline void set_gestures_15(List_1_t1271579410 * value)
	{
		___gestures_15 = value;
		Il2CppCodeGenWriteBarrier(&___gestures_15, value);
	}

	inline static int32_t get_offset_of_OnGesture_17() { return static_cast<int32_t>(offsetof(GestureRecognizerTS_1_t1291560060, ___OnGesture_17)); }
	inline GestureEventHandler_t2030454774 * get_OnGesture_17() const { return ___OnGesture_17; }
	inline GestureEventHandler_t2030454774 ** get_address_of_OnGesture_17() { return &___OnGesture_17; }
	inline void set_OnGesture_17(GestureEventHandler_t2030454774 * value)
	{
		___OnGesture_17 = value;
		Il2CppCodeGenWriteBarrier(&___OnGesture_17, value);
	}
};

struct GestureRecognizerTS_1_t1291560060_StaticFields
{
public:
	// FingerGestures/FingerList GestureRecognizerTS`1::tempTouchList
	FingerList_t1886137443 * ___tempTouchList_16;
	// System.Predicate`1<T> GestureRecognizerTS`1::<>f__am$cache3
	Predicate_1_t3809418037 * ___U3CU3Ef__amU24cache3_18;

public:
	inline static int32_t get_offset_of_tempTouchList_16() { return static_cast<int32_t>(offsetof(GestureRecognizerTS_1_t1291560060_StaticFields, ___tempTouchList_16)); }
	inline FingerList_t1886137443 * get_tempTouchList_16() const { return ___tempTouchList_16; }
	inline FingerList_t1886137443 ** get_address_of_tempTouchList_16() { return &___tempTouchList_16; }
	inline void set_tempTouchList_16(FingerList_t1886137443 * value)
	{
		___tempTouchList_16 = value;
		Il2CppCodeGenWriteBarrier(&___tempTouchList_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_18() { return static_cast<int32_t>(offsetof(GestureRecognizerTS_1_t1291560060_StaticFields, ___U3CU3Ef__amU24cache3_18)); }
	inline Predicate_1_t3809418037 * get_U3CU3Ef__amU24cache3_18() const { return ___U3CU3Ef__amU24cache3_18; }
	inline Predicate_1_t3809418037 ** get_address_of_U3CU3Ef__amU24cache3_18() { return &___U3CU3Ef__amU24cache3_18; }
	inline void set_U3CU3Ef__amU24cache3_18(Predicate_1_t3809418037 * value)
	{
		___U3CU3Ef__amU24cache3_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
