﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Newtonsoft.Json.Linq.JObject/JPropertKeyedCollection
struct JPropertKeyedCollection_t2889692899;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// System.ComponentModel.PropertyChangedEventHandler
struct PropertyChangedEventHandler_t950507765;
// System.Func`2<Newtonsoft.Json.Linq.JProperty,Newtonsoft.Json.Linq.JToken>
struct Func_2_t3361616243;

#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JContainer3364442311.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JObject
struct  JObject_t1798544199  : public JContainer_t3364442311
{
public:
	// Newtonsoft.Json.Linq.JObject/JPropertKeyedCollection Newtonsoft.Json.Linq.JObject::_properties
	JPropertKeyedCollection_t2889692899 * ____properties_8;
	// System.ComponentModel.PropertyChangedEventHandler Newtonsoft.Json.Linq.JObject::PropertyChanged
	PropertyChangedEventHandler_t950507765 * ___PropertyChanged_10;

public:
	inline static int32_t get_offset_of__properties_8() { return static_cast<int32_t>(offsetof(JObject_t1798544199, ____properties_8)); }
	inline JPropertKeyedCollection_t2889692899 * get__properties_8() const { return ____properties_8; }
	inline JPropertKeyedCollection_t2889692899 ** get_address_of__properties_8() { return &____properties_8; }
	inline void set__properties_8(JPropertKeyedCollection_t2889692899 * value)
	{
		____properties_8 = value;
		Il2CppCodeGenWriteBarrier(&____properties_8, value);
	}

	inline static int32_t get_offset_of_PropertyChanged_10() { return static_cast<int32_t>(offsetof(JObject_t1798544199, ___PropertyChanged_10)); }
	inline PropertyChangedEventHandler_t950507765 * get_PropertyChanged_10() const { return ___PropertyChanged_10; }
	inline PropertyChangedEventHandler_t950507765 ** get_address_of_PropertyChanged_10() { return &___PropertyChanged_10; }
	inline void set_PropertyChanged_10(PropertyChangedEventHandler_t950507765 * value)
	{
		___PropertyChanged_10 = value;
		Il2CppCodeGenWriteBarrier(&___PropertyChanged_10, value);
	}
};

struct JObject_t1798544199_StaticFields
{
public:
	// Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JObject::none
	JObject_t1798544199 * ___none_9;
	// System.Func`2<Newtonsoft.Json.Linq.JProperty,Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject::<>f__am$cache3
	Func_2_t3361616243 * ___U3CU3Ef__amU24cache3_11;

public:
	inline static int32_t get_offset_of_none_9() { return static_cast<int32_t>(offsetof(JObject_t1798544199_StaticFields, ___none_9)); }
	inline JObject_t1798544199 * get_none_9() const { return ___none_9; }
	inline JObject_t1798544199 ** get_address_of_none_9() { return &___none_9; }
	inline void set_none_9(JObject_t1798544199 * value)
	{
		___none_9 = value;
		Il2CppCodeGenWriteBarrier(&___none_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_11() { return static_cast<int32_t>(offsetof(JObject_t1798544199_StaticFields, ___U3CU3Ef__amU24cache3_11)); }
	inline Func_2_t3361616243 * get_U3CU3Ef__amU24cache3_11() const { return ___U3CU3Ef__amU24cache3_11; }
	inline Func_2_t3361616243 ** get_address_of_U3CU3Ef__amU24cache3_11() { return &___U3CU3Ef__amU24cache3_11; }
	inline void set_U3CU3Ef__amU24cache3_11(Func_2_t3361616243 * value)
	{
		___U3CU3Ef__amU24cache3_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
