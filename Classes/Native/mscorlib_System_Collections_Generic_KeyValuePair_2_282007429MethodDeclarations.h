﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_866109363MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2024643531(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t282007429 *, Int2_t1974045593 , ProceduralTile_t3586714437 *, const MethodInfo*))KeyValuePair_2__ctor_m3790321200_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::get_Key()
#define KeyValuePair_2_get_Key_m1085919224(__this, method) ((  Int2_t1974045593  (*) (KeyValuePair_2_t282007429 *, const MethodInfo*))KeyValuePair_2_get_Key_m4013756344_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m4154215294(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t282007429 *, Int2_t1974045593 , const MethodInfo*))KeyValuePair_2_set_Key_m93750777_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::get_Value()
#define KeyValuePair_2_get_Value_m998978443(__this, method) ((  ProceduralTile_t3586714437 * (*) (KeyValuePair_2_t282007429 *, const MethodInfo*))KeyValuePair_2_get_Value_m167978232_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3855200382(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t282007429 *, ProceduralTile_t3586714437 *, const MethodInfo*))KeyValuePair_2_set_Value_m2665231737_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::ToString()
#define KeyValuePair_2_ToString_m3194826954(__this, method) ((  String_t* (*) (KeyValuePair_2_t282007429 *, const MethodInfo*))KeyValuePair_2_ToString_m3662222985_gshared)(__this, method)
