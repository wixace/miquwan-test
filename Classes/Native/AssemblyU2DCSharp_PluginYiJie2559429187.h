﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// SFOnlineUser
struct SFOnlineUser_t2483883889;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginYiJie
struct  PluginYiJie_t2559429187  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginYiJie::roleId
	String_t* ___roleId_3;
	// System.String PluginYiJie::roleName
	String_t* ___roleName_4;
	// System.String PluginYiJie::roleLevel
	String_t* ___roleLevel_5;
	// System.String PluginYiJie::serverId
	String_t* ___serverId_6;
	// System.String PluginYiJie::serverName
	String_t* ___serverName_7;
	// SFOnlineUser PluginYiJie::userInfo
	SFOnlineUser_t2483883889 * ___userInfo_8;
	// System.Single PluginYiJie::lastSdkLoginOkTime
	float ___lastSdkLoginOkTime_9;

public:
	inline static int32_t get_offset_of_roleId_3() { return static_cast<int32_t>(offsetof(PluginYiJie_t2559429187, ___roleId_3)); }
	inline String_t* get_roleId_3() const { return ___roleId_3; }
	inline String_t** get_address_of_roleId_3() { return &___roleId_3; }
	inline void set_roleId_3(String_t* value)
	{
		___roleId_3 = value;
		Il2CppCodeGenWriteBarrier(&___roleId_3, value);
	}

	inline static int32_t get_offset_of_roleName_4() { return static_cast<int32_t>(offsetof(PluginYiJie_t2559429187, ___roleName_4)); }
	inline String_t* get_roleName_4() const { return ___roleName_4; }
	inline String_t** get_address_of_roleName_4() { return &___roleName_4; }
	inline void set_roleName_4(String_t* value)
	{
		___roleName_4 = value;
		Il2CppCodeGenWriteBarrier(&___roleName_4, value);
	}

	inline static int32_t get_offset_of_roleLevel_5() { return static_cast<int32_t>(offsetof(PluginYiJie_t2559429187, ___roleLevel_5)); }
	inline String_t* get_roleLevel_5() const { return ___roleLevel_5; }
	inline String_t** get_address_of_roleLevel_5() { return &___roleLevel_5; }
	inline void set_roleLevel_5(String_t* value)
	{
		___roleLevel_5 = value;
		Il2CppCodeGenWriteBarrier(&___roleLevel_5, value);
	}

	inline static int32_t get_offset_of_serverId_6() { return static_cast<int32_t>(offsetof(PluginYiJie_t2559429187, ___serverId_6)); }
	inline String_t* get_serverId_6() const { return ___serverId_6; }
	inline String_t** get_address_of_serverId_6() { return &___serverId_6; }
	inline void set_serverId_6(String_t* value)
	{
		___serverId_6 = value;
		Il2CppCodeGenWriteBarrier(&___serverId_6, value);
	}

	inline static int32_t get_offset_of_serverName_7() { return static_cast<int32_t>(offsetof(PluginYiJie_t2559429187, ___serverName_7)); }
	inline String_t* get_serverName_7() const { return ___serverName_7; }
	inline String_t** get_address_of_serverName_7() { return &___serverName_7; }
	inline void set_serverName_7(String_t* value)
	{
		___serverName_7 = value;
		Il2CppCodeGenWriteBarrier(&___serverName_7, value);
	}

	inline static int32_t get_offset_of_userInfo_8() { return static_cast<int32_t>(offsetof(PluginYiJie_t2559429187, ___userInfo_8)); }
	inline SFOnlineUser_t2483883889 * get_userInfo_8() const { return ___userInfo_8; }
	inline SFOnlineUser_t2483883889 ** get_address_of_userInfo_8() { return &___userInfo_8; }
	inline void set_userInfo_8(SFOnlineUser_t2483883889 * value)
	{
		___userInfo_8 = value;
		Il2CppCodeGenWriteBarrier(&___userInfo_8, value);
	}

	inline static int32_t get_offset_of_lastSdkLoginOkTime_9() { return static_cast<int32_t>(offsetof(PluginYiJie_t2559429187, ___lastSdkLoginOkTime_9)); }
	inline float get_lastSdkLoginOkTime_9() const { return ___lastSdkLoginOkTime_9; }
	inline float* get_address_of_lastSdkLoginOkTime_9() { return &___lastSdkLoginOkTime_9; }
	inline void set_lastSdkLoginOkTime_9(float value)
	{
		___lastSdkLoginOkTime_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
