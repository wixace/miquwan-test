﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066860316MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,BossAnimationInfo>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m887061287(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t280379758 *, int32_t, BossAnimationInfo_t384335813 *, const MethodInfo*))KeyValuePair_2__ctor_m11197230_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,BossAnimationInfo>::get_Key()
#define KeyValuePair_2_get_Key_m2388953313(__this, method) ((  int32_t (*) (KeyValuePair_2_t280379758 *, const MethodInfo*))KeyValuePair_2_get_Key_m494458106_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,BossAnimationInfo>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1687896738(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t280379758 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m4229413435_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,BossAnimationInfo>::get_Value()
#define KeyValuePair_2_get_Value_m2100361185(__this, method) ((  BossAnimationInfo_t384335813 * (*) (KeyValuePair_2_t280379758 *, const MethodInfo*))KeyValuePair_2_get_Value_m1563175098_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,BossAnimationInfo>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1047557538(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t280379758 *, BossAnimationInfo_t384335813 *, const MethodInfo*))KeyValuePair_2_set_Value_m1296398523_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,BossAnimationInfo>::ToString()
#define KeyValuePair_2_ToString_m537969280(__this, method) ((  String_t* (*) (KeyValuePair_2_t280379758 *, const MethodInfo*))KeyValuePair_2_ToString_m491888647_gshared)(__this, method)
