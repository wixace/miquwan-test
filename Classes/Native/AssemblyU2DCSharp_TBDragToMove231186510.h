﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Collider
struct Collider_t2939674232;
// UnityEngine.Camera
struct Camera_t2727095145;
// FingerGestures/Finger
struct Finger_t182428197;
// GestureRecognizer
struct GestureRecognizer_t3512875949;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TBDragToMove
struct  TBDragToMove_t231186510  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Collider TBDragToMove::DragPlaneCollider
	Collider_t2939674232 * ___DragPlaneCollider_2;
	// System.Single TBDragToMove::DragPlaneOffset
	float ___DragPlaneOffset_3;
	// UnityEngine.Camera TBDragToMove::RaycastCamera
	Camera_t2727095145 * ___RaycastCamera_4;
	// System.Boolean TBDragToMove::DragFromObjectCenter
	bool ___DragFromObjectCenter_5;
	// System.Boolean TBDragToMove::dragging
	bool ___dragging_6;
	// FingerGestures/Finger TBDragToMove::draggingFinger
	Finger_t182428197 * ___draggingFinger_7;
	// GestureRecognizer TBDragToMove::gestureRecognizer
	GestureRecognizer_t3512875949 * ___gestureRecognizer_8;
	// System.Boolean TBDragToMove::oldUseGravity
	bool ___oldUseGravity_9;
	// System.Boolean TBDragToMove::oldIsKinematic
	bool ___oldIsKinematic_10;
	// UnityEngine.Vector3 TBDragToMove::physxDragMove
	Vector3_t4282066566  ___physxDragMove_11;

public:
	inline static int32_t get_offset_of_DragPlaneCollider_2() { return static_cast<int32_t>(offsetof(TBDragToMove_t231186510, ___DragPlaneCollider_2)); }
	inline Collider_t2939674232 * get_DragPlaneCollider_2() const { return ___DragPlaneCollider_2; }
	inline Collider_t2939674232 ** get_address_of_DragPlaneCollider_2() { return &___DragPlaneCollider_2; }
	inline void set_DragPlaneCollider_2(Collider_t2939674232 * value)
	{
		___DragPlaneCollider_2 = value;
		Il2CppCodeGenWriteBarrier(&___DragPlaneCollider_2, value);
	}

	inline static int32_t get_offset_of_DragPlaneOffset_3() { return static_cast<int32_t>(offsetof(TBDragToMove_t231186510, ___DragPlaneOffset_3)); }
	inline float get_DragPlaneOffset_3() const { return ___DragPlaneOffset_3; }
	inline float* get_address_of_DragPlaneOffset_3() { return &___DragPlaneOffset_3; }
	inline void set_DragPlaneOffset_3(float value)
	{
		___DragPlaneOffset_3 = value;
	}

	inline static int32_t get_offset_of_RaycastCamera_4() { return static_cast<int32_t>(offsetof(TBDragToMove_t231186510, ___RaycastCamera_4)); }
	inline Camera_t2727095145 * get_RaycastCamera_4() const { return ___RaycastCamera_4; }
	inline Camera_t2727095145 ** get_address_of_RaycastCamera_4() { return &___RaycastCamera_4; }
	inline void set_RaycastCamera_4(Camera_t2727095145 * value)
	{
		___RaycastCamera_4 = value;
		Il2CppCodeGenWriteBarrier(&___RaycastCamera_4, value);
	}

	inline static int32_t get_offset_of_DragFromObjectCenter_5() { return static_cast<int32_t>(offsetof(TBDragToMove_t231186510, ___DragFromObjectCenter_5)); }
	inline bool get_DragFromObjectCenter_5() const { return ___DragFromObjectCenter_5; }
	inline bool* get_address_of_DragFromObjectCenter_5() { return &___DragFromObjectCenter_5; }
	inline void set_DragFromObjectCenter_5(bool value)
	{
		___DragFromObjectCenter_5 = value;
	}

	inline static int32_t get_offset_of_dragging_6() { return static_cast<int32_t>(offsetof(TBDragToMove_t231186510, ___dragging_6)); }
	inline bool get_dragging_6() const { return ___dragging_6; }
	inline bool* get_address_of_dragging_6() { return &___dragging_6; }
	inline void set_dragging_6(bool value)
	{
		___dragging_6 = value;
	}

	inline static int32_t get_offset_of_draggingFinger_7() { return static_cast<int32_t>(offsetof(TBDragToMove_t231186510, ___draggingFinger_7)); }
	inline Finger_t182428197 * get_draggingFinger_7() const { return ___draggingFinger_7; }
	inline Finger_t182428197 ** get_address_of_draggingFinger_7() { return &___draggingFinger_7; }
	inline void set_draggingFinger_7(Finger_t182428197 * value)
	{
		___draggingFinger_7 = value;
		Il2CppCodeGenWriteBarrier(&___draggingFinger_7, value);
	}

	inline static int32_t get_offset_of_gestureRecognizer_8() { return static_cast<int32_t>(offsetof(TBDragToMove_t231186510, ___gestureRecognizer_8)); }
	inline GestureRecognizer_t3512875949 * get_gestureRecognizer_8() const { return ___gestureRecognizer_8; }
	inline GestureRecognizer_t3512875949 ** get_address_of_gestureRecognizer_8() { return &___gestureRecognizer_8; }
	inline void set_gestureRecognizer_8(GestureRecognizer_t3512875949 * value)
	{
		___gestureRecognizer_8 = value;
		Il2CppCodeGenWriteBarrier(&___gestureRecognizer_8, value);
	}

	inline static int32_t get_offset_of_oldUseGravity_9() { return static_cast<int32_t>(offsetof(TBDragToMove_t231186510, ___oldUseGravity_9)); }
	inline bool get_oldUseGravity_9() const { return ___oldUseGravity_9; }
	inline bool* get_address_of_oldUseGravity_9() { return &___oldUseGravity_9; }
	inline void set_oldUseGravity_9(bool value)
	{
		___oldUseGravity_9 = value;
	}

	inline static int32_t get_offset_of_oldIsKinematic_10() { return static_cast<int32_t>(offsetof(TBDragToMove_t231186510, ___oldIsKinematic_10)); }
	inline bool get_oldIsKinematic_10() const { return ___oldIsKinematic_10; }
	inline bool* get_address_of_oldIsKinematic_10() { return &___oldIsKinematic_10; }
	inline void set_oldIsKinematic_10(bool value)
	{
		___oldIsKinematic_10 = value;
	}

	inline static int32_t get_offset_of_physxDragMove_11() { return static_cast<int32_t>(offsetof(TBDragToMove_t231186510, ___physxDragMove_11)); }
	inline Vector3_t4282066566  get_physxDragMove_11() const { return ___physxDragMove_11; }
	inline Vector3_t4282066566 * get_address_of_physxDragMove_11() { return &___physxDragMove_11; }
	inline void set_physxDragMove_11(Vector3_t4282066566  value)
	{
		___physxDragMove_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
