﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CombatEntity
struct CombatEntity_t684137495;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TargetMgr/AngleEntity
struct  AngleEntity_t3194128142  : public Il2CppObject
{
public:
	// System.Single TargetMgr/AngleEntity::angle
	float ___angle_0;
	// System.Boolean TargetMgr/AngleEntity::IsNull
	bool ___IsNull_1;
	// CombatEntity TargetMgr/AngleEntity::self
	CombatEntity_t684137495 * ___self_2;
	// UnityEngine.Vector3 TargetMgr/AngleEntity::selfTargetPos
	Vector3_t4282066566  ___selfTargetPos_3;

public:
	inline static int32_t get_offset_of_angle_0() { return static_cast<int32_t>(offsetof(AngleEntity_t3194128142, ___angle_0)); }
	inline float get_angle_0() const { return ___angle_0; }
	inline float* get_address_of_angle_0() { return &___angle_0; }
	inline void set_angle_0(float value)
	{
		___angle_0 = value;
	}

	inline static int32_t get_offset_of_IsNull_1() { return static_cast<int32_t>(offsetof(AngleEntity_t3194128142, ___IsNull_1)); }
	inline bool get_IsNull_1() const { return ___IsNull_1; }
	inline bool* get_address_of_IsNull_1() { return &___IsNull_1; }
	inline void set_IsNull_1(bool value)
	{
		___IsNull_1 = value;
	}

	inline static int32_t get_offset_of_self_2() { return static_cast<int32_t>(offsetof(AngleEntity_t3194128142, ___self_2)); }
	inline CombatEntity_t684137495 * get_self_2() const { return ___self_2; }
	inline CombatEntity_t684137495 ** get_address_of_self_2() { return &___self_2; }
	inline void set_self_2(CombatEntity_t684137495 * value)
	{
		___self_2 = value;
		Il2CppCodeGenWriteBarrier(&___self_2, value);
	}

	inline static int32_t get_offset_of_selfTargetPos_3() { return static_cast<int32_t>(offsetof(AngleEntity_t3194128142, ___selfTargetPos_3)); }
	inline Vector3_t4282066566  get_selfTargetPos_3() const { return ___selfTargetPos_3; }
	inline Vector3_t4282066566 * get_address_of_selfTargetPos_3() { return &___selfTargetPos_3; }
	inline void set_selfTargetPos_3(Vector3_t4282066566  value)
	{
		___selfTargetPos_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
