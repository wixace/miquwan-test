﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIPlayAnimationGenerated
struct UIPlayAnimationGenerated_t1638368371;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_String7231557.h"

// System.Void UIPlayAnimationGenerated::.ctor()
extern "C"  void UIPlayAnimationGenerated__ctor_m2798410632 (UIPlayAnimationGenerated_t1638368371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPlayAnimationGenerated::UIPlayAnimation_UIPlayAnimation1(JSVCall,System.Int32)
extern "C"  bool UIPlayAnimationGenerated_UIPlayAnimation_UIPlayAnimation1_m1149961208 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimationGenerated::UIPlayAnimation_current(JSVCall)
extern "C"  void UIPlayAnimationGenerated_UIPlayAnimation_current_m2907284843 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimationGenerated::UIPlayAnimation_target(JSVCall)
extern "C"  void UIPlayAnimationGenerated_UIPlayAnimation_target_m3075486231 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimationGenerated::UIPlayAnimation_animator(JSVCall)
extern "C"  void UIPlayAnimationGenerated_UIPlayAnimation_animator_m4001095937 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimationGenerated::UIPlayAnimation_clipName(JSVCall)
extern "C"  void UIPlayAnimationGenerated_UIPlayAnimation_clipName_m2597869677 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimationGenerated::UIPlayAnimation_trigger(JSVCall)
extern "C"  void UIPlayAnimationGenerated_UIPlayAnimation_trigger_m1421861900 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimationGenerated::UIPlayAnimation_playDirection(JSVCall)
extern "C"  void UIPlayAnimationGenerated_UIPlayAnimation_playDirection_m627129849 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimationGenerated::UIPlayAnimation_resetOnPlay(JSVCall)
extern "C"  void UIPlayAnimationGenerated_UIPlayAnimation_resetOnPlay_m4162953762 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimationGenerated::UIPlayAnimation_clearSelection(JSVCall)
extern "C"  void UIPlayAnimationGenerated_UIPlayAnimation_clearSelection_m2925802409 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimationGenerated::UIPlayAnimation_ifDisabledOnPlay(JSVCall)
extern "C"  void UIPlayAnimationGenerated_UIPlayAnimation_ifDisabledOnPlay_m2585647548 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimationGenerated::UIPlayAnimation_disableWhenFinished(JSVCall)
extern "C"  void UIPlayAnimationGenerated_UIPlayAnimation_disableWhenFinished_m3465067376 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimationGenerated::UIPlayAnimation_onFinished(JSVCall)
extern "C"  void UIPlayAnimationGenerated_UIPlayAnimation_onFinished_m4140862263 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPlayAnimationGenerated::UIPlayAnimation_Play__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool UIPlayAnimationGenerated_UIPlayAnimation_Play__Boolean__Boolean_m2961679703 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPlayAnimationGenerated::UIPlayAnimation_Play__Boolean(JSVCall,System.Int32)
extern "C"  bool UIPlayAnimationGenerated_UIPlayAnimation_Play__Boolean_m804610355 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimationGenerated::__Register()
extern "C"  void UIPlayAnimationGenerated___Register_m1332287039 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimationGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UIPlayAnimationGenerated_ilo_addJSCSRel1_m2545077828 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIPlayAnimationGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIPlayAnimationGenerated_ilo_getObject2_m1247385704 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimationGenerated::ilo_setStringS3(System.Int32,System.String)
extern "C"  void UIPlayAnimationGenerated_ilo_setStringS3_m4206580563 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimationGenerated::ilo_setEnum4(System.Int32,System.Int32)
extern "C"  void UIPlayAnimationGenerated_ilo_setEnum4_m2652200774 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIPlayAnimationGenerated::ilo_getEnum5(System.Int32)
extern "C"  int32_t UIPlayAnimationGenerated_ilo_getEnum5_m2448029452 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimationGenerated::ilo_setBooleanS6(System.Int32,System.Boolean)
extern "C"  void UIPlayAnimationGenerated_ilo_setBooleanS6_m2715955732 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPlayAnimationGenerated::ilo_getBooleanS7(System.Int32)
extern "C"  bool UIPlayAnimationGenerated_ilo_getBooleanS7_m906456522 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
