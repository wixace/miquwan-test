﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_FindLast__PredicateT1_T>c__AnonStorey8A
struct U3CListA1_FindLast__PredicateT1_TU3Ec__AnonStorey8A_t785173783;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_FindLast__PredicateT1_T>c__AnonStorey8A::.ctor()
extern "C"  void U3CListA1_FindLast__PredicateT1_TU3Ec__AnonStorey8A__ctor_m1265542500 (U3CListA1_FindLast__PredicateT1_TU3Ec__AnonStorey8A_t785173783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Collections_Generic_List77Generated/<ListA1_FindLast__PredicateT1_T>c__AnonStorey8A::<>m__AF()
extern "C"  Il2CppObject * U3CListA1_FindLast__PredicateT1_TU3Ec__AnonStorey8A_U3CU3Em__AF_m4197114493 (U3CListA1_FindLast__PredicateT1_TU3Ec__AnonStorey8A_t785173783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
