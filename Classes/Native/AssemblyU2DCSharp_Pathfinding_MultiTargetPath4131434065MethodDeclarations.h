﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.MultiTargetPath
struct MultiTargetPath_t4131434065;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// OnPathDelegate[]
struct OnPathDelegateU5BU5D_t1505600468;
// OnPathDelegate
struct OnPathDelegate_t598607977;
// Pathfinding.PathNode
struct PathNode_t417131581;
// Pathfinding.Path
struct Path_t1974241691;
// System.String
struct String_t;
// Pathfinding.BinaryHeapM
struct BinaryHeapM_t946855490;
// Pathfinding.PathHandler
struct PathHandler_t918952263;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// AstarPath
struct AstarPath_t4090270936;
// Pathfinding.NNConstraint
struct NNConstraint_t758567699;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_OnPathDelegate598607977.h"
#include "AssemblyU2DCSharp_Pathfinding_PathNode417131581.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_PathLog873181375.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_PathCompleteState1625108115.h"
#include "AssemblyU2DCSharp_Pathfinding_MultiTargetPath4131434065.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "AssemblyU2DCSharp_Pathfinding_PathHandler918952263.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_Pathfinding_NNInfo1570625892.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "AssemblyU2DCSharp_Pathfinding_NNConstraint758567699.h"

// System.Void Pathfinding.MultiTargetPath::.ctor()
extern "C"  void MultiTargetPath__ctor_m2386100230 (MultiTargetPath_t4131434065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MultiTargetPath::.ctor(UnityEngine.Vector3[],UnityEngine.Vector3,OnPathDelegate[],OnPathDelegate)
extern "C"  void MultiTargetPath__ctor_m1075378758 (MultiTargetPath_t4131434065 * __this, Vector3U5BU5D_t215400611* ___startPoints0, Vector3_t4282066566  ___target1, OnPathDelegateU5BU5D_t1505600468* ___callbackDelegates2, OnPathDelegate_t598607977 * ___callbackDelegate3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MultiTargetPath::.ctor(UnityEngine.Vector3,UnityEngine.Vector3[],OnPathDelegate[],OnPathDelegate)
extern "C"  void MultiTargetPath__ctor_m3900284038 (MultiTargetPath_t4131434065 * __this, Vector3_t4282066566  ___start0, Vector3U5BU5D_t215400611* ___targets1, OnPathDelegateU5BU5D_t1505600468* ___callbackDelegates2, OnPathDelegate_t598607977 * ___callbackDelegate3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.MultiTargetPath Pathfinding.MultiTargetPath::Construct(UnityEngine.Vector3[],UnityEngine.Vector3,OnPathDelegate[],OnPathDelegate)
extern "C"  MultiTargetPath_t4131434065 * MultiTargetPath_Construct_m602684611 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t215400611* ___startPoints0, Vector3_t4282066566  ___target1, OnPathDelegateU5BU5D_t1505600468* ___callbackDelegates2, OnPathDelegate_t598607977 * ___callback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.MultiTargetPath Pathfinding.MultiTargetPath::Construct(UnityEngine.Vector3,UnityEngine.Vector3[],OnPathDelegate[],OnPathDelegate)
extern "C"  MultiTargetPath_t4131434065 * MultiTargetPath_Construct_m3427589891 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start0, Vector3U5BU5D_t215400611* ___targets1, OnPathDelegateU5BU5D_t1505600468* ___callbackDelegates2, OnPathDelegate_t598607977 * ___callback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MultiTargetPath::Setup(UnityEngine.Vector3,UnityEngine.Vector3[],OnPathDelegate[],OnPathDelegate)
extern "C"  void MultiTargetPath_Setup_m2342631051 (MultiTargetPath_t4131434065 * __this, Vector3_t4282066566  ___start0, Vector3U5BU5D_t215400611* ___targets1, OnPathDelegateU5BU5D_t1505600468* ___callbackDelegates2, OnPathDelegate_t598607977 * ___callback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MultiTargetPath::Recycle()
extern "C"  void MultiTargetPath_Recycle_m443417143 (MultiTargetPath_t4131434065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MultiTargetPath::OnEnterPool()
extern "C"  void MultiTargetPath_OnEnterPool_m3553196441 (MultiTargetPath_t4131434065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MultiTargetPath::ReturnPath()
extern "C"  void MultiTargetPath_ReturnPath_m4010915571 (MultiTargetPath_t4131434065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MultiTargetPath::FoundTarget(Pathfinding.PathNode,System.Int32)
extern "C"  void MultiTargetPath_FoundTarget_m300118161 (MultiTargetPath_t4131434065 * __this, PathNode_t417131581 * ___nodeR0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MultiTargetPath::RebuildOpenList()
extern "C"  void MultiTargetPath_RebuildOpenList_m3849838567 (MultiTargetPath_t4131434065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MultiTargetPath::Prepare()
extern "C"  void MultiTargetPath_Prepare_m2460733355 (MultiTargetPath_t4131434065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MultiTargetPath::Initialize()
extern "C"  void MultiTargetPath_Initialize_m2886756942 (MultiTargetPath_t4131434065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MultiTargetPath::ResetFlags(Pathfinding.Path)
extern "C"  void MultiTargetPath_ResetFlags_m3363721927 (MultiTargetPath_t4131434065 * __this, Path_t1974241691 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MultiTargetPath::CalculateStep(System.Int64)
extern "C"  void MultiTargetPath_CalculateStep_m3788267720 (MultiTargetPath_t4131434065 * __this, int64_t ___targetTick0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MultiTargetPath::Trace(Pathfinding.PathNode)
extern "C"  void MultiTargetPath_Trace_m1202862168 (MultiTargetPath_t4131434065 * __this, PathNode_t417131581 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.MultiTargetPath::DebugString(PathLog)
extern "C"  String_t* MultiTargetPath_DebugString_m384999592 (MultiTargetPath_t4131434065 * __this, int32_t ___logMode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.MultiTargetPath Pathfinding.MultiTargetPath::ilo_Construct1(UnityEngine.Vector3,UnityEngine.Vector3[],OnPathDelegate[],OnPathDelegate)
extern "C"  MultiTargetPath_t4131434065 * MultiTargetPath_ilo_Construct1_m911740463 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start0, Vector3U5BU5D_t215400611* ___targets1, OnPathDelegateU5BU5D_t1505600468* ___callbackDelegates2, OnPathDelegate_t598607977 * ___callback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MultiTargetPath::ilo_LogError2(Pathfinding.Path,System.String)
extern "C"  void MultiTargetPath_ilo_LogError2_m1088035820 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, String_t* ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.MultiTargetPath::ilo_get_error3(Pathfinding.Path)
extern "C"  bool MultiTargetPath_ilo_get_error3_m1850284130 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MultiTargetPath::ilo_set_CompleteState4(Pathfinding.Path,PathCompleteState)
extern "C"  void MultiTargetPath_ilo_set_CompleteState4_m1334642068 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MultiTargetPath::ilo_Invoke5(OnPathDelegate,Pathfinding.Path)
extern "C"  void MultiTargetPath_ilo_Invoke5_m3339900210 (Il2CppObject * __this /* static, unused */, OnPathDelegate_t598607977 * ____this0, Path_t1974241691 * ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MultiTargetPath::ilo_Trace6(Pathfinding.MultiTargetPath,Pathfinding.PathNode)
extern "C"  void MultiTargetPath_ilo_Trace6_m3387908362 (Il2CppObject * __this /* static, unused */, MultiTargetPath_t4131434065 * ____this0, PathNode_t417131581 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MultiTargetPath::ilo_RebuildOpenList7(Pathfinding.MultiTargetPath)
extern "C"  void MultiTargetPath_ilo_RebuildOpenList7_m2957293608 (Il2CppObject * __this /* static, unused */, MultiTargetPath_t4131434065 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.MultiTargetPath::ilo_op_Explicit8(Pathfinding.Int3)
extern "C"  Vector3_t4282066566  MultiTargetPath_ilo_op_Explicit8_m856898191 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.MultiTargetPath::ilo_op_Subtraction9(Pathfinding.Int3,Pathfinding.Int3)
extern "C"  Int3_t1974045594  MultiTargetPath_ilo_op_Subtraction9_m678772803 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___lhs0, Int3_t1974045594  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.BinaryHeapM Pathfinding.MultiTargetPath::ilo_GetHeap10(Pathfinding.PathHandler)
extern "C"  BinaryHeapM_t946855490 * MultiTargetPath_ilo_GetHeap10_m1866786248 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.MultiTargetPath::ilo_CalculateHScore11(Pathfinding.Path,Pathfinding.GraphNode)
extern "C"  uint32_t MultiTargetPath_ilo_CalculateHScore11_m2717335763 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MultiTargetPath::ilo_RebuildHeap12(Pathfinding.PathHandler)
extern "C"  void MultiTargetPath_ilo_RebuildHeap12_m1912472734 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.MultiTargetPath::ilo_GetNearest13(AstarPath,UnityEngine.Vector3,Pathfinding.NNConstraint,Pathfinding.GraphNode)
extern "C"  NNInfo_t1570625892  MultiTargetPath_ilo_GetNearest13_m2641230766 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, Vector3_t4282066566  ___position1, NNConstraint_t758567699 * ___constraint2, GraphNode_t23612370 * ___hint3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MultiTargetPath::ilo_Error14(Pathfinding.Path)
extern "C"  void MultiTargetPath_ilo_Error14_m4282358029 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.MultiTargetPath::ilo_get_Walkable15(Pathfinding.GraphNode)
extern "C"  bool MultiTargetPath_ilo_get_Walkable15_m3825077951 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.MultiTargetPath::ilo_get_Area16(Pathfinding.GraphNode)
extern "C"  uint32_t MultiTargetPath_ilo_get_Area16_m1060171803 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.PathNode Pathfinding.MultiTargetPath::ilo_GetPathNode17(Pathfinding.PathHandler,Pathfinding.GraphNode)
extern "C"  PathNode_t417131581 * MultiTargetPath_ilo_GetPathNode17_m2480989917 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.MultiTargetPath::ilo_GetTraversalCost18(Pathfinding.Path,Pathfinding.GraphNode)
extern "C"  uint32_t MultiTargetPath_ilo_GetTraversalCost18_m2175971849 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MultiTargetPath::ilo_set_G19(Pathfinding.PathNode,System.UInt32)
extern "C"  void MultiTargetPath_ilo_set_G19_m3058487782 (Il2CppObject * __this /* static, unused */, PathNode_t417131581 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MultiTargetPath::ilo_Open20(Pathfinding.GraphNode,Pathfinding.Path,Pathfinding.PathNode,Pathfinding.PathHandler)
extern "C"  void MultiTargetPath_ilo_Open20_m2465519458 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, Path_t1974241691 * ___path1, PathNode_t417131581 * ___pathNode2, PathHandler_t918952263 * ___handler3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.MultiTargetPath::ilo_get_GraphIndex21(Pathfinding.GraphNode)
extern "C"  uint32_t MultiTargetPath_ilo_get_GraphIndex21_m1620842890 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.MultiTargetPath::ilo_get_IsUsingMultithreading22()
extern "C"  bool MultiTargetPath_ilo_get_IsUsingMultithreading22_m4210401397 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
