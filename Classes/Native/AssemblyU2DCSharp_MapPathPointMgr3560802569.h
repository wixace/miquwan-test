﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MapPathPointMgr
struct MapPathPointMgr_t3560802569;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapPathPointMgr
struct  MapPathPointMgr_t3560802569  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject[] MapPathPointMgr::targetObjs
	GameObjectU5BU5D_t2662109048* ___targetObjs_3;

public:
	inline static int32_t get_offset_of_targetObjs_3() { return static_cast<int32_t>(offsetof(MapPathPointMgr_t3560802569, ___targetObjs_3)); }
	inline GameObjectU5BU5D_t2662109048* get_targetObjs_3() const { return ___targetObjs_3; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_targetObjs_3() { return &___targetObjs_3; }
	inline void set_targetObjs_3(GameObjectU5BU5D_t2662109048* value)
	{
		___targetObjs_3 = value;
		Il2CppCodeGenWriteBarrier(&___targetObjs_3, value);
	}
};

struct MapPathPointMgr_t3560802569_StaticFields
{
public:
	// MapPathPointMgr MapPathPointMgr::Instance
	MapPathPointMgr_t3560802569 * ___Instance_2;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(MapPathPointMgr_t3560802569_StaticFields, ___Instance_2)); }
	inline MapPathPointMgr_t3560802569 * get_Instance_2() const { return ___Instance_2; }
	inline MapPathPointMgr_t3560802569 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(MapPathPointMgr_t3560802569 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
