﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginZongYou
struct PluginZongYou_t3982613182;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// VersionMgr
struct VersionMgr_t1322950208;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_PluginZongYou3982613182.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void PluginZongYou::.ctor()
extern "C"  void PluginZongYou__ctor_m1330625645 (PluginZongYou_t3982613182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZongYou::Init()
extern "C"  void PluginZongYou_Init_m2349764679 (PluginZongYou_t3982613182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginZongYou::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginZongYou_ReqSDKHttpLogin_m293810718 (PluginZongYou_t3982613182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginZongYou::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginZongYou_IsLoginSuccess_m3846688094 (PluginZongYou_t3982613182 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZongYou::OpenUserLogin()
extern "C"  void PluginZongYou_OpenUserLogin_m749740479 (PluginZongYou_t3982613182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZongYou::RolelvUpinfo(CEvent.ZEvent)
extern "C"  void PluginZongYou_RolelvUpinfo_m2492541723 (PluginZongYou_t3982613182 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZongYou::UserPay(CEvent.ZEvent)
extern "C"  void PluginZongYou_UserPay_m2323434707 (PluginZongYou_t3982613182 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZongYou::OnInitSuccess(System.String)
extern "C"  void PluginZongYou_OnInitSuccess_m408666467 (PluginZongYou_t3982613182 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZongYou::OnInitFail(System.String)
extern "C"  void PluginZongYou_OnInitFail_m3002655422 (PluginZongYou_t3982613182 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZongYou::OnLoginSuccess(System.String)
extern "C"  void PluginZongYou_OnLoginSuccess_m173470642 (PluginZongYou_t3982613182 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZongYou::OnLoginFail(System.String)
extern "C"  void PluginZongYou_OnLoginFail_m1517841103 (PluginZongYou_t3982613182 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZongYou::OnPaySuccess(System.String)
extern "C"  void PluginZongYou_OnPaySuccess_m23763569 (PluginZongYou_t3982613182 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZongYou::OnPayFail(System.String)
extern "C"  void PluginZongYou_OnPayFail_m4187575408 (PluginZongYou_t3982613182 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZongYou::OnExitGameSuccess(System.String)
extern "C"  void PluginZongYou_OnExitGameSuccess_m1988164611 (PluginZongYou_t3982613182 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZongYou::LogoutSuccess()
extern "C"  void PluginZongYou_LogoutSuccess_m701032548 (PluginZongYou_t3982613182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZongYou::OnLogoutSuccess(System.String)
extern "C"  void PluginZongYou_OnLogoutSuccess_m1086165277 (PluginZongYou_t3982613182 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZongYou::OnLogoutFail(System.String)
extern "C"  void PluginZongYou_OnLogoutFail_m3916283204 (PluginZongYou_t3982613182 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZongYou::ZYInit()
extern "C"  void PluginZongYou_ZYInit_m451658438 (PluginZongYou_t3982613182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZongYou::ZYLogin()
extern "C"  void PluginZongYou_ZYLogin_m3805542421 (PluginZongYou_t3982613182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZongYou::ZYPay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginZongYou_ZYPay_m2562481030 (PluginZongYou_t3982613182 * __this, String_t* ___productId0, String_t* ___productName1, String_t* ___roleId2, String_t* ___roleName3, String_t* ___roleLevel4, String_t* ___Price5, String_t* ___extendInfo6, String_t* ___ProductDesc7, String_t* ___OrderId8, String_t* ___serverId9, String_t* ___serverName10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZongYou::coloectData(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginZongYou_coloectData_m219134762 (PluginZongYou_t3982613182 * __this, String_t* ___serverId0, String_t* ___serverName1, String_t* ___roleID2, String_t* ___roleName3, String_t* ___roleLevel4, String_t* ___vipLevel5, String_t* ___gameCoin6, String_t* ___type7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZongYou::<OnLogoutSuccess>m__484()
extern "C"  void PluginZongYou_U3COnLogoutSuccessU3Em__484_m721785038 (PluginZongYou_t3982613182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginZongYou::ilo_get_Instance1()
extern "C"  VersionMgr_t1322950208 * PluginZongYou_ilo_get_Instance1_m390204872 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZongYou::ilo_ZYInit2(PluginZongYou)
extern "C"  void PluginZongYou_ilo_ZYInit2_m2582288601 (Il2CppObject * __this /* static, unused */, PluginZongYou_t3982613182 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZongYou::ilo_AddEventListener3(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginZongYou_ilo_AddEventListener3_m2946410277 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginZongYou::ilo_get_isSdkLogin4(VersionMgr)
extern "C"  bool PluginZongYou_ilo_get_isSdkLogin4_m2234243934 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginZongYou::ilo_get_DeviceID5(PluginsSdkMgr)
extern "C"  String_t* PluginZongYou_ilo_get_DeviceID5_m3808843966 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZongYou::ilo_OpenUserLogin6(PluginZongYou)
extern "C"  void PluginZongYou_ilo_OpenUserLogin6_m694929992 (Il2CppObject * __this /* static, unused */, PluginZongYou_t3982613182 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginZongYou::ilo_GetProductID7(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginZongYou_ilo_GetProductID7_m1311046146 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZongYou::ilo_ZYPay8(PluginZongYou,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginZongYou_ilo_ZYPay8_m3294387375 (Il2CppObject * __this /* static, unused */, PluginZongYou_t3982613182 * ____this0, String_t* ___productId1, String_t* ___productName2, String_t* ___roleId3, String_t* ___roleName4, String_t* ___roleLevel5, String_t* ___Price6, String_t* ___extendInfo7, String_t* ___ProductDesc8, String_t* ___OrderId9, String_t* ___serverId10, String_t* ___serverName11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZongYou::ilo_Log9(System.Object,System.Boolean)
extern "C"  void PluginZongYou_ilo_Log9_m2807078728 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZongYou::ilo_DispatchEvent10(CEvent.ZEvent)
extern "C"  void PluginZongYou_ilo_DispatchEvent10_m2936049412 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
