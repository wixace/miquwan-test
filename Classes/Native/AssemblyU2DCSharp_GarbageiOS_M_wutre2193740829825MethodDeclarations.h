﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_wutre219
struct M_wutre219_t3740829825;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_wutre2193740829825.h"

// System.Void GarbageiOS.M_wutre219::.ctor()
extern "C"  void M_wutre219__ctor_m1846697682 (M_wutre219_t3740829825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_wutre219::M_dootoha0(System.String[],System.Int32)
extern "C"  void M_wutre219_M_dootoha0_m614582515 (M_wutre219_t3740829825 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_wutre219::M_valpeaTawtel1(System.String[],System.Int32)
extern "C"  void M_wutre219_M_valpeaTawtel1_m1247595050 (M_wutre219_t3740829825 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_wutre219::M_naterToohere2(System.String[],System.Int32)
extern "C"  void M_wutre219_M_naterToohere2_m2634558385 (M_wutre219_t3740829825 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_wutre219::ilo_M_dootoha01(GarbageiOS.M_wutre219,System.String[],System.Int32)
extern "C"  void M_wutre219_ilo_M_dootoha01_m3011395080 (Il2CppObject * __this /* static, unused */, M_wutre219_t3740829825 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
