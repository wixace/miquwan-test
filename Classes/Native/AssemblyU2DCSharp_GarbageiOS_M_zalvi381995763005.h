﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_zalvi38
struct  M_zalvi38_t1995763005  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_zalvi38::_sarmayJochayzar
	bool ____sarmayJochayzar_0;
	// System.Int32 GarbageiOS.M_zalvi38::_lemriRoutre
	int32_t ____lemriRoutre_1;
	// System.Boolean GarbageiOS.M_zalvi38::_ballhu
	bool ____ballhu_2;
	// System.Single GarbageiOS.M_zalvi38::_boucarqaSorhair
	float ____boucarqaSorhair_3;
	// System.Boolean GarbageiOS.M_zalvi38::_jevuta
	bool ____jevuta_4;
	// System.UInt32 GarbageiOS.M_zalvi38::_hallherto
	uint32_t ____hallherto_5;
	// System.String GarbageiOS.M_zalvi38::_curgar
	String_t* ____curgar_6;
	// System.UInt32 GarbageiOS.M_zalvi38::_heredor
	uint32_t ____heredor_7;
	// System.Boolean GarbageiOS.M_zalvi38::_nallcooJeelawra
	bool ____nallcooJeelawra_8;
	// System.UInt32 GarbageiOS.M_zalvi38::_sacerpe
	uint32_t ____sacerpe_9;
	// System.Single GarbageiOS.M_zalvi38::_kekaygi
	float ____kekaygi_10;
	// System.Int32 GarbageiOS.M_zalvi38::_yomasDawdrearraw
	int32_t ____yomasDawdrearraw_11;
	// System.Boolean GarbageiOS.M_zalvi38::_toonoCallroutair
	bool ____toonoCallroutair_12;
	// System.String GarbageiOS.M_zalvi38::_surpearNeabestor
	String_t* ____surpearNeabestor_13;
	// System.Single GarbageiOS.M_zalvi38::_tetooRirall
	float ____tetooRirall_14;
	// System.Boolean GarbageiOS.M_zalvi38::_meneretuSanismou
	bool ____meneretuSanismou_15;
	// System.UInt32 GarbageiOS.M_zalvi38::_sertral
	uint32_t ____sertral_16;
	// System.Int32 GarbageiOS.M_zalvi38::_tawdouJepowhis
	int32_t ____tawdouJepowhis_17;
	// System.Boolean GarbageiOS.M_zalvi38::_helgece
	bool ____helgece_18;
	// System.UInt32 GarbageiOS.M_zalvi38::_soofisere
	uint32_t ____soofisere_19;
	// System.Int32 GarbageiOS.M_zalvi38::_rerneDounall
	int32_t ____rerneDounall_20;
	// System.Boolean GarbageiOS.M_zalvi38::_jexeeceHetaljis
	bool ____jexeeceHetaljis_21;
	// System.UInt32 GarbageiOS.M_zalvi38::_laygopu
	uint32_t ____laygopu_22;
	// System.Int32 GarbageiOS.M_zalvi38::_naysasstal
	int32_t ____naysasstal_23;
	// System.Boolean GarbageiOS.M_zalvi38::_cerpearWiswelkea
	bool ____cerpearWiswelkea_24;
	// System.Boolean GarbageiOS.M_zalvi38::_cearelTemem
	bool ____cearelTemem_25;
	// System.Int32 GarbageiOS.M_zalvi38::_hebasal
	int32_t ____hebasal_26;
	// System.String GarbageiOS.M_zalvi38::_corheretel
	String_t* ____corheretel_27;
	// System.UInt32 GarbageiOS.M_zalvi38::_werarhasPuna
	uint32_t ____werarhasPuna_28;
	// System.Boolean GarbageiOS.M_zalvi38::_whefeazou
	bool ____whefeazou_29;
	// System.Single GarbageiOS.M_zalvi38::_bekiche
	float ____bekiche_30;
	// System.Boolean GarbageiOS.M_zalvi38::_dewheaje
	bool ____dewheaje_31;
	// System.UInt32 GarbageiOS.M_zalvi38::_wije
	uint32_t ____wije_32;
	// System.String GarbageiOS.M_zalvi38::_fobirtiRejai
	String_t* ____fobirtiRejai_33;
	// System.Single GarbageiOS.M_zalvi38::_hopisJikea
	float ____hopisJikea_34;

public:
	inline static int32_t get_offset_of__sarmayJochayzar_0() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____sarmayJochayzar_0)); }
	inline bool get__sarmayJochayzar_0() const { return ____sarmayJochayzar_0; }
	inline bool* get_address_of__sarmayJochayzar_0() { return &____sarmayJochayzar_0; }
	inline void set__sarmayJochayzar_0(bool value)
	{
		____sarmayJochayzar_0 = value;
	}

	inline static int32_t get_offset_of__lemriRoutre_1() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____lemriRoutre_1)); }
	inline int32_t get__lemriRoutre_1() const { return ____lemriRoutre_1; }
	inline int32_t* get_address_of__lemriRoutre_1() { return &____lemriRoutre_1; }
	inline void set__lemriRoutre_1(int32_t value)
	{
		____lemriRoutre_1 = value;
	}

	inline static int32_t get_offset_of__ballhu_2() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____ballhu_2)); }
	inline bool get__ballhu_2() const { return ____ballhu_2; }
	inline bool* get_address_of__ballhu_2() { return &____ballhu_2; }
	inline void set__ballhu_2(bool value)
	{
		____ballhu_2 = value;
	}

	inline static int32_t get_offset_of__boucarqaSorhair_3() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____boucarqaSorhair_3)); }
	inline float get__boucarqaSorhair_3() const { return ____boucarqaSorhair_3; }
	inline float* get_address_of__boucarqaSorhair_3() { return &____boucarqaSorhair_3; }
	inline void set__boucarqaSorhair_3(float value)
	{
		____boucarqaSorhair_3 = value;
	}

	inline static int32_t get_offset_of__jevuta_4() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____jevuta_4)); }
	inline bool get__jevuta_4() const { return ____jevuta_4; }
	inline bool* get_address_of__jevuta_4() { return &____jevuta_4; }
	inline void set__jevuta_4(bool value)
	{
		____jevuta_4 = value;
	}

	inline static int32_t get_offset_of__hallherto_5() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____hallherto_5)); }
	inline uint32_t get__hallherto_5() const { return ____hallherto_5; }
	inline uint32_t* get_address_of__hallherto_5() { return &____hallherto_5; }
	inline void set__hallherto_5(uint32_t value)
	{
		____hallherto_5 = value;
	}

	inline static int32_t get_offset_of__curgar_6() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____curgar_6)); }
	inline String_t* get__curgar_6() const { return ____curgar_6; }
	inline String_t** get_address_of__curgar_6() { return &____curgar_6; }
	inline void set__curgar_6(String_t* value)
	{
		____curgar_6 = value;
		Il2CppCodeGenWriteBarrier(&____curgar_6, value);
	}

	inline static int32_t get_offset_of__heredor_7() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____heredor_7)); }
	inline uint32_t get__heredor_7() const { return ____heredor_7; }
	inline uint32_t* get_address_of__heredor_7() { return &____heredor_7; }
	inline void set__heredor_7(uint32_t value)
	{
		____heredor_7 = value;
	}

	inline static int32_t get_offset_of__nallcooJeelawra_8() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____nallcooJeelawra_8)); }
	inline bool get__nallcooJeelawra_8() const { return ____nallcooJeelawra_8; }
	inline bool* get_address_of__nallcooJeelawra_8() { return &____nallcooJeelawra_8; }
	inline void set__nallcooJeelawra_8(bool value)
	{
		____nallcooJeelawra_8 = value;
	}

	inline static int32_t get_offset_of__sacerpe_9() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____sacerpe_9)); }
	inline uint32_t get__sacerpe_9() const { return ____sacerpe_9; }
	inline uint32_t* get_address_of__sacerpe_9() { return &____sacerpe_9; }
	inline void set__sacerpe_9(uint32_t value)
	{
		____sacerpe_9 = value;
	}

	inline static int32_t get_offset_of__kekaygi_10() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____kekaygi_10)); }
	inline float get__kekaygi_10() const { return ____kekaygi_10; }
	inline float* get_address_of__kekaygi_10() { return &____kekaygi_10; }
	inline void set__kekaygi_10(float value)
	{
		____kekaygi_10 = value;
	}

	inline static int32_t get_offset_of__yomasDawdrearraw_11() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____yomasDawdrearraw_11)); }
	inline int32_t get__yomasDawdrearraw_11() const { return ____yomasDawdrearraw_11; }
	inline int32_t* get_address_of__yomasDawdrearraw_11() { return &____yomasDawdrearraw_11; }
	inline void set__yomasDawdrearraw_11(int32_t value)
	{
		____yomasDawdrearraw_11 = value;
	}

	inline static int32_t get_offset_of__toonoCallroutair_12() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____toonoCallroutair_12)); }
	inline bool get__toonoCallroutair_12() const { return ____toonoCallroutair_12; }
	inline bool* get_address_of__toonoCallroutair_12() { return &____toonoCallroutair_12; }
	inline void set__toonoCallroutair_12(bool value)
	{
		____toonoCallroutair_12 = value;
	}

	inline static int32_t get_offset_of__surpearNeabestor_13() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____surpearNeabestor_13)); }
	inline String_t* get__surpearNeabestor_13() const { return ____surpearNeabestor_13; }
	inline String_t** get_address_of__surpearNeabestor_13() { return &____surpearNeabestor_13; }
	inline void set__surpearNeabestor_13(String_t* value)
	{
		____surpearNeabestor_13 = value;
		Il2CppCodeGenWriteBarrier(&____surpearNeabestor_13, value);
	}

	inline static int32_t get_offset_of__tetooRirall_14() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____tetooRirall_14)); }
	inline float get__tetooRirall_14() const { return ____tetooRirall_14; }
	inline float* get_address_of__tetooRirall_14() { return &____tetooRirall_14; }
	inline void set__tetooRirall_14(float value)
	{
		____tetooRirall_14 = value;
	}

	inline static int32_t get_offset_of__meneretuSanismou_15() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____meneretuSanismou_15)); }
	inline bool get__meneretuSanismou_15() const { return ____meneretuSanismou_15; }
	inline bool* get_address_of__meneretuSanismou_15() { return &____meneretuSanismou_15; }
	inline void set__meneretuSanismou_15(bool value)
	{
		____meneretuSanismou_15 = value;
	}

	inline static int32_t get_offset_of__sertral_16() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____sertral_16)); }
	inline uint32_t get__sertral_16() const { return ____sertral_16; }
	inline uint32_t* get_address_of__sertral_16() { return &____sertral_16; }
	inline void set__sertral_16(uint32_t value)
	{
		____sertral_16 = value;
	}

	inline static int32_t get_offset_of__tawdouJepowhis_17() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____tawdouJepowhis_17)); }
	inline int32_t get__tawdouJepowhis_17() const { return ____tawdouJepowhis_17; }
	inline int32_t* get_address_of__tawdouJepowhis_17() { return &____tawdouJepowhis_17; }
	inline void set__tawdouJepowhis_17(int32_t value)
	{
		____tawdouJepowhis_17 = value;
	}

	inline static int32_t get_offset_of__helgece_18() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____helgece_18)); }
	inline bool get__helgece_18() const { return ____helgece_18; }
	inline bool* get_address_of__helgece_18() { return &____helgece_18; }
	inline void set__helgece_18(bool value)
	{
		____helgece_18 = value;
	}

	inline static int32_t get_offset_of__soofisere_19() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____soofisere_19)); }
	inline uint32_t get__soofisere_19() const { return ____soofisere_19; }
	inline uint32_t* get_address_of__soofisere_19() { return &____soofisere_19; }
	inline void set__soofisere_19(uint32_t value)
	{
		____soofisere_19 = value;
	}

	inline static int32_t get_offset_of__rerneDounall_20() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____rerneDounall_20)); }
	inline int32_t get__rerneDounall_20() const { return ____rerneDounall_20; }
	inline int32_t* get_address_of__rerneDounall_20() { return &____rerneDounall_20; }
	inline void set__rerneDounall_20(int32_t value)
	{
		____rerneDounall_20 = value;
	}

	inline static int32_t get_offset_of__jexeeceHetaljis_21() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____jexeeceHetaljis_21)); }
	inline bool get__jexeeceHetaljis_21() const { return ____jexeeceHetaljis_21; }
	inline bool* get_address_of__jexeeceHetaljis_21() { return &____jexeeceHetaljis_21; }
	inline void set__jexeeceHetaljis_21(bool value)
	{
		____jexeeceHetaljis_21 = value;
	}

	inline static int32_t get_offset_of__laygopu_22() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____laygopu_22)); }
	inline uint32_t get__laygopu_22() const { return ____laygopu_22; }
	inline uint32_t* get_address_of__laygopu_22() { return &____laygopu_22; }
	inline void set__laygopu_22(uint32_t value)
	{
		____laygopu_22 = value;
	}

	inline static int32_t get_offset_of__naysasstal_23() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____naysasstal_23)); }
	inline int32_t get__naysasstal_23() const { return ____naysasstal_23; }
	inline int32_t* get_address_of__naysasstal_23() { return &____naysasstal_23; }
	inline void set__naysasstal_23(int32_t value)
	{
		____naysasstal_23 = value;
	}

	inline static int32_t get_offset_of__cerpearWiswelkea_24() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____cerpearWiswelkea_24)); }
	inline bool get__cerpearWiswelkea_24() const { return ____cerpearWiswelkea_24; }
	inline bool* get_address_of__cerpearWiswelkea_24() { return &____cerpearWiswelkea_24; }
	inline void set__cerpearWiswelkea_24(bool value)
	{
		____cerpearWiswelkea_24 = value;
	}

	inline static int32_t get_offset_of__cearelTemem_25() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____cearelTemem_25)); }
	inline bool get__cearelTemem_25() const { return ____cearelTemem_25; }
	inline bool* get_address_of__cearelTemem_25() { return &____cearelTemem_25; }
	inline void set__cearelTemem_25(bool value)
	{
		____cearelTemem_25 = value;
	}

	inline static int32_t get_offset_of__hebasal_26() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____hebasal_26)); }
	inline int32_t get__hebasal_26() const { return ____hebasal_26; }
	inline int32_t* get_address_of__hebasal_26() { return &____hebasal_26; }
	inline void set__hebasal_26(int32_t value)
	{
		____hebasal_26 = value;
	}

	inline static int32_t get_offset_of__corheretel_27() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____corheretel_27)); }
	inline String_t* get__corheretel_27() const { return ____corheretel_27; }
	inline String_t** get_address_of__corheretel_27() { return &____corheretel_27; }
	inline void set__corheretel_27(String_t* value)
	{
		____corheretel_27 = value;
		Il2CppCodeGenWriteBarrier(&____corheretel_27, value);
	}

	inline static int32_t get_offset_of__werarhasPuna_28() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____werarhasPuna_28)); }
	inline uint32_t get__werarhasPuna_28() const { return ____werarhasPuna_28; }
	inline uint32_t* get_address_of__werarhasPuna_28() { return &____werarhasPuna_28; }
	inline void set__werarhasPuna_28(uint32_t value)
	{
		____werarhasPuna_28 = value;
	}

	inline static int32_t get_offset_of__whefeazou_29() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____whefeazou_29)); }
	inline bool get__whefeazou_29() const { return ____whefeazou_29; }
	inline bool* get_address_of__whefeazou_29() { return &____whefeazou_29; }
	inline void set__whefeazou_29(bool value)
	{
		____whefeazou_29 = value;
	}

	inline static int32_t get_offset_of__bekiche_30() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____bekiche_30)); }
	inline float get__bekiche_30() const { return ____bekiche_30; }
	inline float* get_address_of__bekiche_30() { return &____bekiche_30; }
	inline void set__bekiche_30(float value)
	{
		____bekiche_30 = value;
	}

	inline static int32_t get_offset_of__dewheaje_31() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____dewheaje_31)); }
	inline bool get__dewheaje_31() const { return ____dewheaje_31; }
	inline bool* get_address_of__dewheaje_31() { return &____dewheaje_31; }
	inline void set__dewheaje_31(bool value)
	{
		____dewheaje_31 = value;
	}

	inline static int32_t get_offset_of__wije_32() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____wije_32)); }
	inline uint32_t get__wije_32() const { return ____wije_32; }
	inline uint32_t* get_address_of__wije_32() { return &____wije_32; }
	inline void set__wije_32(uint32_t value)
	{
		____wije_32 = value;
	}

	inline static int32_t get_offset_of__fobirtiRejai_33() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____fobirtiRejai_33)); }
	inline String_t* get__fobirtiRejai_33() const { return ____fobirtiRejai_33; }
	inline String_t** get_address_of__fobirtiRejai_33() { return &____fobirtiRejai_33; }
	inline void set__fobirtiRejai_33(String_t* value)
	{
		____fobirtiRejai_33 = value;
		Il2CppCodeGenWriteBarrier(&____fobirtiRejai_33, value);
	}

	inline static int32_t get_offset_of__hopisJikea_34() { return static_cast<int32_t>(offsetof(M_zalvi38_t1995763005, ____hopisJikea_34)); }
	inline float get__hopisJikea_34() const { return ____hopisJikea_34; }
	inline float* get_address_of__hopisJikea_34() { return &____hopisJikea_34; }
	inline void set__hopisJikea_34(float value)
	{
		____hopisJikea_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
