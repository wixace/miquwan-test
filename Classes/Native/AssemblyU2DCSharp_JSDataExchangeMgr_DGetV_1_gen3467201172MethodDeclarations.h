﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr_DGetV_1_gen4048467712MethodDeclarations.h"

// System.Void JSDataExchangeMgr/DGetV`1<UnityEngine.Keyframe[]>::.ctor(System.Object,System.IntPtr)
#define DGetV_1__ctor_m1179494202(__this, ___object0, ___method1, method) ((  void (*) (DGetV_1_t3467201172 *, Il2CppObject *, IntPtr_t, const MethodInfo*))DGetV_1__ctor_m3356914363_gshared)(__this, ___object0, ___method1, method)
// T JSDataExchangeMgr/DGetV`1<UnityEngine.Keyframe[]>::Invoke()
#define DGetV_1_Invoke_m1540210997(__this, method) ((  KeyframeU5BU5D_t3589549831* (*) (DGetV_1_t3467201172 *, const MethodInfo*))DGetV_1_Invoke_m1509146772_gshared)(__this, method)
// System.IAsyncResult JSDataExchangeMgr/DGetV`1<UnityEngine.Keyframe[]>::BeginInvoke(System.AsyncCallback,System.Object)
#define DGetV_1_BeginInvoke_m540193783(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (DGetV_1_t3467201172 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))DGetV_1_BeginInvoke_m2681184398_gshared)(__this, ___callback0, ___object1, method)
// T JSDataExchangeMgr/DGetV`1<UnityEngine.Keyframe[]>::EndInvoke(System.IAsyncResult)
#define DGetV_1_EndInvoke_m3580862635(__this, ___result0, method) ((  KeyframeU5BU5D_t3589549831* (*) (DGetV_1_t3467201172 *, Il2CppObject *, const MethodInfo*))DGetV_1_EndInvoke_m1776565962_gshared)(__this, ___result0, method)
