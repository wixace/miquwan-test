﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>
struct Dictionary_2_t1453516396;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2770839788.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21352297102.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2853155855_gshared (Enumerator_t2770839788 * __this, Dictionary_2_t1453516396 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m2853155855(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2770839788 *, Dictionary_2_t1453516396 *, const MethodInfo*))Enumerator__ctor_m2853155855_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2332623676_gshared (Enumerator_t2770839788 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2332623676(__this, method) ((  Il2CppObject * (*) (Enumerator_t2770839788 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2332623676_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3227554118_gshared (Enumerator_t2770839788 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3227554118(__this, method) ((  void (*) (Enumerator_t2770839788 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3227554118_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2126232061_gshared (Enumerator_t2770839788 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2126232061(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2770839788 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2126232061_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1182786328_gshared (Enumerator_t2770839788 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1182786328(__this, method) ((  Il2CppObject * (*) (Enumerator_t2770839788 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1182786328_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3834731434_gshared (Enumerator_t2770839788 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3834731434(__this, method) ((  Il2CppObject * (*) (Enumerator_t2770839788 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3834731434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4054555382_gshared (Enumerator_t2770839788 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m4054555382(__this, method) ((  bool (*) (Enumerator_t2770839788 *, const MethodInfo*))Enumerator_MoveNext_m4054555382_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t1352297102  Enumerator_get_Current_m2779016966_gshared (Enumerator_t2770839788 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2779016966(__this, method) ((  KeyValuePair_2_t1352297102  (*) (Enumerator_t2770839788 *, const MethodInfo*))Enumerator_get_Current_m2779016966_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Object>::get_CurrentKey()
extern "C"  Int3_t1974045594  Enumerator_get_CurrentKey_m2864030207_gshared (Enumerator_t2770839788 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m2864030207(__this, method) ((  Int3_t1974045594  (*) (Enumerator_t2770839788 *, const MethodInfo*))Enumerator_get_CurrentKey_m2864030207_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m2181899647_gshared (Enumerator_t2770839788 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m2181899647(__this, method) ((  Il2CppObject * (*) (Enumerator_t2770839788 *, const MethodInfo*))Enumerator_get_CurrentValue_m2181899647_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m3428296673_gshared (Enumerator_t2770839788 * __this, const MethodInfo* method);
#define Enumerator_Reset_m3428296673(__this, method) ((  void (*) (Enumerator_t2770839788 *, const MethodInfo*))Enumerator_Reset_m3428296673_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3492530986_gshared (Enumerator_t2770839788 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3492530986(__this, method) ((  void (*) (Enumerator_t2770839788 *, const MethodInfo*))Enumerator_VerifyState_m3492530986_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m402723858_gshared (Enumerator_t2770839788 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m402723858(__this, method) ((  void (*) (Enumerator_t2770839788 *, const MethodInfo*))Enumerator_VerifyCurrent_m402723858_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2876615793_gshared (Enumerator_t2770839788 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2876615793(__this, method) ((  void (*) (Enumerator_t2770839788 *, const MethodInfo*))Enumerator_Dispose_m2876615793_gshared)(__this, method)
