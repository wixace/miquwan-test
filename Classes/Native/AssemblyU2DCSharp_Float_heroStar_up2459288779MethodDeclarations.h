﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_heroStar_up
struct Float_heroStar_up_t2459288779;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// FloatTextUnit
struct FloatTextUnit_t2362298029;
// EventDelegate/Callback
struct Callback_t1094463061;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "AssemblyU2DCSharp_UITweener_Method1078127180.h"

// System.Void Float_heroStar_up::.ctor()
extern "C"  void Float_heroStar_up__ctor_m230104320 (Float_heroStar_up_t2459288779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_heroStar_up::OnAwake(UnityEngine.GameObject)
extern "C"  void Float_heroStar_up_OnAwake_m662831292 (Float_heroStar_up_t2459288779 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_heroStar_up::FloatID()
extern "C"  int32_t Float_heroStar_up_FloatID_m1108315582 (Float_heroStar_up_t2459288779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_heroStar_up::Init()
extern "C"  void Float_heroStar_up_Init_m3284095316 (Float_heroStar_up_t2459288779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_heroStar_up::SetFile(System.Object[])
extern "C"  void Float_heroStar_up_SetFile_m2055414742 (Float_heroStar_up_t2459288779 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_heroStar_up::ilo_MoveScale1(FloatTextUnit,System.Single,System.Single,System.Single,System.Single,EventDelegate/Callback,UnityEngine.GameObject,UITweener/Method)
extern "C"  void Float_heroStar_up_ilo_MoveScale1_m1044420163 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, float ___from1, float ___to2, float ___duration3, float ___delay4, Callback_t1094463061 * ___call5, GameObject_t3674682005 * ___go6, int32_t ___method7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
