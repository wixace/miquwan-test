﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.NullDecorator
struct NullDecorator_t349487662;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// ProtoBuf.Serializers.IProtoSerializer
struct IProtoSerializer_t3033312651;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"
#include "AssemblyU2DCSharp_ProtoBuf_SubItemToken3365128146.h"

// System.Void ProtoBuf.Serializers.NullDecorator::.ctor(ProtoBuf.Meta.TypeModel,ProtoBuf.Serializers.IProtoSerializer)
extern "C"  void NullDecorator__ctor_m4071037341 (NullDecorator_t349487662 * __this, TypeModel_t2730011105 * ___model0, Il2CppObject * ___tail1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.NullDecorator::get_ExpectedType()
extern "C"  Type_t * NullDecorator_get_ExpectedType_m1365754882 (NullDecorator_t349487662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.NullDecorator::get_ReturnsValue()
extern "C"  bool NullDecorator_get_ReturnsValue_m2219776248 (NullDecorator_t349487662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.NullDecorator::get_RequiresOldValue()
extern "C"  bool NullDecorator_get_RequiresOldValue_m4092929378 (NullDecorator_t349487662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.NullDecorator::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * NullDecorator_Read_m1247820418 (NullDecorator_t349487662 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.NullDecorator::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void NullDecorator_Write_m1791661390 (NullDecorator_t349487662 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.SubItemToken ProtoBuf.Serializers.NullDecorator::ilo_StartSubItem1(ProtoBuf.ProtoReader)
extern "C"  SubItemToken_t3365128146  NullDecorator_ilo_StartSubItem1_m2208264752 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Serializers.NullDecorator::ilo_ReadFieldHeader2(ProtoBuf.ProtoReader)
extern "C"  int32_t NullDecorator_ilo_ReadFieldHeader2_m1481039207 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
