﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2953159047MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<CSProvingGroundsHeroData>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2110666232(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4132026427 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2616641763_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<CSProvingGroundsHeroData>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4125723816(__this, method) ((  void (*) (InternalEnumerator_1_t4132026427 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224260061_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<CSProvingGroundsHeroData>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4144017364(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4132026427 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m390763987_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<CSProvingGroundsHeroData>::Dispose()
#define InternalEnumerator_1_Dispose_m585127375(__this, method) ((  void (*) (InternalEnumerator_1_t4132026427 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2760671866_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<CSProvingGroundsHeroData>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1964877972(__this, method) ((  bool (*) (InternalEnumerator_1_t4132026427 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3716548237_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<CSProvingGroundsHeroData>::get_Current()
#define InternalEnumerator_1_get_Current_m1959341439(__this, method) ((  CSProvingGroundsHeroData_t1054716455 * (*) (InternalEnumerator_1_t4132026427 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2178852364_gshared)(__this, method)
