﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Converters.IsoDateTimeConverter
struct IsoDateTimeConverter_t3280061086;
// System.String
struct String_t;
// System.Globalization.CultureInfo
struct CultureInfo_t1065375142;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// System.Type
struct Type_t;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Globalization_DateTimeStyles1282965087.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Globalization_CultureInfo1065375142.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer251850770.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader816925123.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_IsoDa3280061086.h"

// System.Void Newtonsoft.Json.Converters.IsoDateTimeConverter::.ctor()
extern "C"  void IsoDateTimeConverter__ctor_m2999897029 (IsoDateTimeConverter_t3280061086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.DateTimeStyles Newtonsoft.Json.Converters.IsoDateTimeConverter::get_DateTimeStyles()
extern "C"  int32_t IsoDateTimeConverter_get_DateTimeStyles_m4125279195 (IsoDateTimeConverter_t3280061086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.IsoDateTimeConverter::set_DateTimeStyles(System.Globalization.DateTimeStyles)
extern "C"  void IsoDateTimeConverter_set_DateTimeStyles_m3524457904 (IsoDateTimeConverter_t3280061086 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.IsoDateTimeConverter::get_DateTimeFormat()
extern "C"  String_t* IsoDateTimeConverter_get_DateTimeFormat_m942918871 (IsoDateTimeConverter_t3280061086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.IsoDateTimeConverter::set_DateTimeFormat(System.String)
extern "C"  void IsoDateTimeConverter_set_DateTimeFormat_m476084276 (IsoDateTimeConverter_t3280061086 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo Newtonsoft.Json.Converters.IsoDateTimeConverter::get_Culture()
extern "C"  CultureInfo_t1065375142 * IsoDateTimeConverter_get_Culture_m2613577109 (IsoDateTimeConverter_t3280061086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.IsoDateTimeConverter::set_Culture(System.Globalization.CultureInfo)
extern "C"  void IsoDateTimeConverter_set_Culture_m56879780 (IsoDateTimeConverter_t3280061086 * __this, CultureInfo_t1065375142 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.IsoDateTimeConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  void IsoDateTimeConverter_WriteJson_m1202714223 (IsoDateTimeConverter_t3280061086 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, JsonSerializer_t251850770 * ___serializer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Converters.IsoDateTimeConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  Il2CppObject * IsoDateTimeConverter_ReadJson_m956802062 (IsoDateTimeConverter_t3280061086 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, Il2CppObject * ___existingValue2, JsonSerializer_t251850770 * ___serializer3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.IsoDateTimeConverter::ilo_NullEmptyString1(System.String)
extern "C"  String_t* IsoDateTimeConverter_ilo_NullEmptyString1_m1245719545 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo Newtonsoft.Json.Converters.IsoDateTimeConverter::ilo_get_Culture2(Newtonsoft.Json.Converters.IsoDateTimeConverter)
extern "C"  CultureInfo_t1065375142 * IsoDateTimeConverter_ilo_get_Culture2_m2448300154 (Il2CppObject * __this /* static, unused */, IsoDateTimeConverter_t3280061086 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Converters.IsoDateTimeConverter::ilo_GetObjectType3(System.Object)
extern "C"  Type_t * IsoDateTimeConverter_ilo_GetObjectType3_m1755713368 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.IsoDateTimeConverter::ilo_IsNullableType4(System.Type)
extern "C"  bool IsoDateTimeConverter_ilo_IsNullableType4_m1759854022 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.IsoDateTimeConverter::ilo_FormatWith5(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* IsoDateTimeConverter_ilo_FormatWith5_m989602697 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
