﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.RecastGraph/CapsuleCache>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m969600357(__this, ___l0, method) ((  void (*) (Enumerator_t3420518628 *, List_1_t3400845858 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.RecastGraph/CapsuleCache>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m157588429(__this, method) ((  void (*) (Enumerator_t3420518628 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.RecastGraph/CapsuleCache>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3838233731(__this, method) ((  Il2CppObject * (*) (Enumerator_t3420518628 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.RecastGraph/CapsuleCache>::Dispose()
#define Enumerator_Dispose_m1267295370(__this, method) ((  void (*) (Enumerator_t3420518628 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.RecastGraph/CapsuleCache>::VerifyState()
#define Enumerator_VerifyState_m1359204035(__this, method) ((  void (*) (Enumerator_t3420518628 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.RecastGraph/CapsuleCache>::MoveNext()
#define Enumerator_MoveNext_m1410262525(__this, method) ((  bool (*) (Enumerator_t3420518628 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.RecastGraph/CapsuleCache>::get_Current()
#define Enumerator_get_Current_m3326633948(__this, method) ((  CapsuleCache_t2032660306 * (*) (Enumerator_t3420518628 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
