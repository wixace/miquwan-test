﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.Calendar
struct Calendar_t3558528576;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_DateTime4283661327.h"

// System.Void System.Globalization.Calendar::.ctor()
extern "C"  void Calendar__ctor_m2652724106 (Calendar_t3558528576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.Calendar::M_ValidValues(System.Object,System.Object)
extern "C"  String_t* Calendar_M_ValidValues_m2320264811 (Calendar_t3558528576 * __this, Il2CppObject * ___a0, Il2CppObject * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.Calendar::M_ArgumentInRange(System.String,System.Int32,System.Int32,System.Int32)
extern "C"  void Calendar_M_ArgumentInRange_m3731095878 (Calendar_t3558528576 * __this, String_t* ___param0, int32_t ___arg1, int32_t ___a2, int32_t ___b3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.Calendar::M_CheckHMSM(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void Calendar_M_CheckHMSM_m1849720329 (Calendar_t3558528576 * __this, int32_t ___hour0, int32_t ___minute1, int32_t ___second2, int32_t ___milliseconds3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Globalization.Calendar::Clone()
extern "C"  Il2CppObject * Calendar_Clone_m1933662586 (Calendar_t3558528576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.Calendar::CheckReadOnly()
extern "C"  void Calendar_CheckReadOnly_m955278002 (Calendar_t3558528576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.Calendar::M_CheckYE(System.Int32,System.Int32&)
extern "C"  void Calendar_M_CheckYE_m956280240 (Calendar_t3558528576 * __this, int32_t ___year0, int32_t* ___era1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.Calendar::ToDateTime(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  DateTime_t4283661327  Calendar_ToDateTime_m2244340328 (Calendar_t3558528576 * __this, int32_t ___year0, int32_t ___month1, int32_t ___day2, int32_t ___hour3, int32_t ___minute4, int32_t ___second5, int32_t ___millisecond6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Globalization.Calendar::get_EraNames()
extern "C"  StringU5BU5D_t4054002952* Calendar_get_EraNames_m1732675382 (Calendar_t3558528576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
