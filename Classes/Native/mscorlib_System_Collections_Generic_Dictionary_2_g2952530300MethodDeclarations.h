﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>
struct Dictionary_2_t2952530300;
// System.Collections.Generic.IEqualityComparer`1<UIModelDisplayType>
struct IEqualityComparer_1_t2682787083;
// System.Collections.Generic.IDictionary`2<UIModelDisplayType,System.Int32>
struct IDictionary_2_t2530403645;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<UIModelDisplayType>
struct ICollection_1_t2786342666;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t2048428487;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>[]
struct KeyValuePair_2U5BU5D_t390341163;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>>
struct IEnumerator_1_t468208759;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Int32>
struct KeyCollection_t284322455;
// System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Int32>
struct ValueCollection_t1653136013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22851311006.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_UIModelDisplayType1891752679.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4269853692.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::.ctor()
extern "C"  void Dictionary_2__ctor_m4195593932_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m4195593932(__this, method) ((  void (*) (Dictionary_2_t2952530300 *, const MethodInfo*))Dictionary_2__ctor_m4195593932_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1515362650_gshared (Dictionary_2_t2952530300 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m1515362650(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t2952530300 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1515362650_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m4042295061_gshared (Dictionary_2_t2952530300 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m4042295061(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t2952530300 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m4042295061_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m2673612596_gshared (Dictionary_2_t2952530300 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m2673612596(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t2952530300 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2673612596_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m721632264_gshared (Dictionary_2_t2952530300 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m721632264(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t2952530300 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m721632264_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2818794788_gshared (Dictionary_2_t2952530300 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m2818794788(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2952530300 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m2818794788_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3241164811_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3241164811(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2952530300 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3241164811_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1819571083_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1819571083(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2952530300 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1819571083_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m1430143849_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1430143849(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2952530300 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1430143849_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m2346258967_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m2346258967(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2952530300 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m2346258967_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1793458270_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1793458270(__this, method) ((  bool (*) (Dictionary_2_t2952530300 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1793458270_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1758788315_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1758788315(__this, method) ((  bool (*) (Dictionary_2_t2952530300 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1758788315_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m3619564357_gshared (Dictionary_2_t2952530300 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m3619564357(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2952530300 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m3619564357_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m3877758378_gshared (Dictionary_2_t2952530300 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m3877758378(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2952530300 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3877758378_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3435263591_gshared (Dictionary_2_t2952530300 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m3435263591(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2952530300 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3435263591_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m2748555695_gshared (Dictionary_2_t2952530300 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m2748555695(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2952530300 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2748555695_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m353956840_gshared (Dictionary_2_t2952530300 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m353956840(__this, ___key0, method) ((  void (*) (Dictionary_2_t2952530300 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m353956840_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3329098757_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3329098757(__this, method) ((  bool (*) (Dictionary_2_t2952530300 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3329098757_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3813976753_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3813976753(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2952530300 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3813976753_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m848428041_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m848428041(__this, method) ((  bool (*) (Dictionary_2_t2952530300 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m848428041_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1208716414_gshared (Dictionary_2_t2952530300 * __this, KeyValuePair_2_t2851311006  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1208716414(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t2952530300 *, KeyValuePair_2_t2851311006 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1208716414_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2677761668_gshared (Dictionary_2_t2952530300 * __this, KeyValuePair_2_t2851311006  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2677761668(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2952530300 *, KeyValuePair_2_t2851311006 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2677761668_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2948176098_gshared (Dictionary_2_t2952530300 * __this, KeyValuePair_2U5BU5D_t390341163* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2948176098(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2952530300 *, KeyValuePair_2U5BU5D_t390341163*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2948176098_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3373883049_gshared (Dictionary_2_t2952530300 * __this, KeyValuePair_2_t2851311006  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3373883049(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2952530300 *, KeyValuePair_2_t2851311006 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3373883049_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m4209700929_gshared (Dictionary_2_t2952530300 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m4209700929(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2952530300 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m4209700929_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m934584700_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m934584700(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2952530300 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m934584700_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2236931065_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2236931065(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2952530300 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2236931065_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3102904340_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3102904340(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2952530300 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3102904340_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m33942091_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m33942091(__this, method) ((  int32_t (*) (Dictionary_2_t2952530300 *, const MethodInfo*))Dictionary_2_get_Count_m33942091_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::get_Item(TKey)
extern "C"  int32_t Dictionary_2_get_Item_m2389179986_gshared (Dictionary_2_t2952530300 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m2389179986(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t2952530300 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m2389179986_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m2297180221_gshared (Dictionary_2_t2952530300 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m2297180221(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2952530300 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_set_Item_m2297180221_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m978329051_gshared (Dictionary_2_t2952530300 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m978329051(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t2952530300 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m978329051_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m1474064188_gshared (Dictionary_2_t2952530300 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m1474064188(__this, ___size0, method) ((  void (*) (Dictionary_2_t2952530300 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m1474064188_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m1692267064_gshared (Dictionary_2_t2952530300 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m1692267064(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2952530300 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1692267064_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t2851311006  Dictionary_2_make_pair_m782631812_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m782631812(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t2851311006  (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_make_pair_m782631812_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m1202020850_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m1202020850(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_key_m1202020850_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::pick_value(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_value_m3626985266_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m3626985266(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_value_m3626985266_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m1654411927_gshared (Dictionary_2_t2952530300 * __this, KeyValuePair_2U5BU5D_t390341163* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1654411927(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2952530300 *, KeyValuePair_2U5BU5D_t390341163*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1654411927_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::Resize()
extern "C"  void Dictionary_2_Resize_m812504629_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m812504629(__this, method) ((  void (*) (Dictionary_2_t2952530300 *, const MethodInfo*))Dictionary_2_Resize_m812504629_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m3129406938_gshared (Dictionary_2_t2952530300 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m3129406938(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2952530300 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_Add_m3129406938_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::Clear()
extern "C"  void Dictionary_2_Clear_m1601727223_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1601727223(__this, method) ((  void (*) (Dictionary_2_t2952530300 *, const MethodInfo*))Dictionary_2_Clear_m1601727223_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m2504713972_gshared (Dictionary_2_t2952530300 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m2504713972(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2952530300 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m2504713972_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m2335285492_gshared (Dictionary_2_t2952530300 * __this, int32_t ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m2335285492(__this, ___value0, method) ((  bool (*) (Dictionary_2_t2952530300 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m2335285492_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m1320110209_gshared (Dictionary_2_t2952530300 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1320110209(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2952530300 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m1320110209_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m2735007875_gshared (Dictionary_2_t2952530300 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m2735007875(__this, ___sender0, method) ((  void (*) (Dictionary_2_t2952530300 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2735007875_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m2197145980_gshared (Dictionary_2_t2952530300 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m2197145980(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2952530300 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m2197145980_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m2278230861_gshared (Dictionary_2_t2952530300 * __this, int32_t ___key0, int32_t* ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m2278230861(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t2952530300 *, int32_t, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m2278230861_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::get_Keys()
extern "C"  KeyCollection_t284322455 * Dictionary_2_get_Keys_m1011591634_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m1011591634(__this, method) ((  KeyCollection_t284322455 * (*) (Dictionary_2_t2952530300 *, const MethodInfo*))Dictionary_2_get_Keys_m1011591634_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::get_Values()
extern "C"  ValueCollection_t1653136013 * Dictionary_2_get_Values_m2094050514_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m2094050514(__this, method) ((  ValueCollection_t1653136013 * (*) (Dictionary_2_t2952530300 *, const MethodInfo*))Dictionary_2_get_Values_m2094050514_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m651879757_gshared (Dictionary_2_t2952530300 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m651879757(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t2952530300 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m651879757_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::ToTValue(System.Object)
extern "C"  int32_t Dictionary_2_ToTValue_m1939299405_gshared (Dictionary_2_t2952530300 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1939299405(__this, ___value0, method) ((  int32_t (*) (Dictionary_2_t2952530300 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1939299405_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m2601326431_gshared (Dictionary_2_t2952530300 * __this, KeyValuePair_2_t2851311006  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m2601326431(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t2952530300 *, KeyValuePair_2_t2851311006 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m2601326431_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t4269853692  Dictionary_2_GetEnumerator_m1011803368_gshared (Dictionary_2_t2952530300 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1011803368(__this, method) ((  Enumerator_t4269853692  (*) (Dictionary_2_t2952530300 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1011803368_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m1264190775_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m1264190775(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1264190775_gshared)(__this /* static, unused */, ___key0, ___value1, method)
