﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUITextureGenerated
struct UnityEngine_GUITextureGenerated_t3001473959;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_GUITextureGenerated::.ctor()
extern "C"  void UnityEngine_GUITextureGenerated__ctor_m1432700836 (UnityEngine_GUITextureGenerated_t3001473959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUITextureGenerated::GUITexture_GUITexture1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUITextureGenerated_GUITexture_GUITexture1_m2512917646 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUITextureGenerated::GUITexture_color(JSVCall)
extern "C"  void UnityEngine_GUITextureGenerated_GUITexture_color_m2232312259 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUITextureGenerated::GUITexture_texture(JSVCall)
extern "C"  void UnityEngine_GUITextureGenerated_GUITexture_texture_m2320145163 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUITextureGenerated::GUITexture_pixelInset(JSVCall)
extern "C"  void UnityEngine_GUITextureGenerated_GUITexture_pixelInset_m1788304623 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUITextureGenerated::GUITexture_border(JSVCall)
extern "C"  void UnityEngine_GUITextureGenerated_GUITexture_border_m989621914 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUITextureGenerated::__Register()
extern "C"  void UnityEngine_GUITextureGenerated___Register_m3880475555 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUITextureGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_GUITextureGenerated_ilo_setObject1_m1578440842 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_GUITextureGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_GUITextureGenerated_ilo_getObject2_m2992963522 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
