﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// EffectCtrl
struct EffectCtrl_t3708787644;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IEffComponent
struct  IEffComponent_t3802264961  : public Il2CppObject
{
public:
	// EffectCtrl IEffComponent::<effCtrl>k__BackingField
	EffectCtrl_t3708787644 * ___U3CeffCtrlU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CeffCtrlU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(IEffComponent_t3802264961, ___U3CeffCtrlU3Ek__BackingField_0)); }
	inline EffectCtrl_t3708787644 * get_U3CeffCtrlU3Ek__BackingField_0() const { return ___U3CeffCtrlU3Ek__BackingField_0; }
	inline EffectCtrl_t3708787644 ** get_address_of_U3CeffCtrlU3Ek__BackingField_0() { return &___U3CeffCtrlU3Ek__BackingField_0; }
	inline void set_U3CeffCtrlU3Ek__BackingField_0(EffectCtrl_t3708787644 * value)
	{
		___U3CeffCtrlU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CeffCtrlU3Ek__BackingField_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
