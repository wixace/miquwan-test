﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// ByteReadArray
struct ByteReadArray_t751193627;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// TabData
struct TabData_t110553279;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ByteReadArray751193627.h"
#include "AssemblyU2DCSharp_TabData110553279.h"

// System.Void JsDBMgr::.cctor()
extern "C"  void JsDBMgr__cctor_m1494330707 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> JsDBMgr::GetCsList()
extern "C"  List_1_t1375417109 * JsDBMgr_GetCsList_m3418096077 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsDBMgr::Load(System.Collections.Generic.List`1<System.String>)
extern "C"  void JsDBMgr_Load_m1444451332 (Il2CppObject * __this /* static, unused */, List_1_t1375417109 * ___tabList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JsDBMgr::get_progress()
extern "C"  float JsDBMgr_get_progress_m812030068 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsDBMgr::set_progress(System.Single)
extern "C"  void JsDBMgr_set_progress_m3700025335 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JsDBMgr::get_inited()
extern "C"  bool JsDBMgr_get_inited_m1500562446 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsDBMgr::set_inited(System.Boolean)
extern "C"  void JsDBMgr_set_inited_m64305581 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsDBMgr::Load()
extern "C"  void JsDBMgr_Load_m3883721680 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsDBMgr::Clear()
extern "C"  void JsDBMgr_Clear_m656477637 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JsDBMgr::GetCount(System.String)
extern "C"  int32_t JsDBMgr_GetCount_m1609997549 (Il2CppObject * __this /* static, unused */, String_t* ___tabName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int32> JsDBMgr::GetKeys(System.String)
extern "C"  List_1_t2522024052 * JsDBMgr_GetKeys_m598273918 (Il2CppObject * __this /* static, unused */, String_t* ___tabName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JsDBMgr::GetInfo(System.String,System.Int32)
extern "C"  String_t* JsDBMgr_GetInfo_m2851967188 (Il2CppObject * __this /* static, unused */, String_t* ___tabName0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> JsDBMgr::GetCsInfo(System.String,System.Int32)
extern "C"  Dictionary_2_t827649927 * JsDBMgr_GetCsInfo_m3040490991 (Il2CppObject * __this /* static, unused */, String_t* ___tabName0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JsDBMgr::ilo_ReadUTF1(ByteReadArray)
extern "C"  String_t* JsDBMgr_ilo_ReadUTF1_m1092297405 (Il2CppObject * __this /* static, unused */, ByteReadArray_t751193627 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JsDBMgr::ilo_ReadInt2(ByteReadArray)
extern "C"  int32_t JsDBMgr_ilo_ReadInt2_m3772536143 (Il2CppObject * __this /* static, unused */, ByteReadArray_t751193627 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsDBMgr::ilo_ReadBytes3(ByteReadArray,System.Byte[],System.UInt32,System.Int32)
extern "C"  void JsDBMgr_ilo_ReadBytes3_m583112356 (Il2CppObject * __this /* static, unused */, ByteReadArray_t751193627 * ____this0, ByteU5BU5D_t4260760469* ___bytes1, uint32_t ___offset2, int32_t ___length3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JsDBMgr::ilo_get_Postion4(ByteReadArray)
extern "C"  int32_t JsDBMgr_ilo_get_Postion4_m3495424275 (Il2CppObject * __this /* static, unused */, ByteReadArray_t751193627 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsDBMgr::ilo_set_progress5(System.Single)
extern "C"  void JsDBMgr_ilo_set_progress5_m877101211 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> JsDBMgr::ilo_GetCsInfo6(TabData,System.Int32)
extern "C"  Dictionary_2_t827649927 * JsDBMgr_ilo_GetCsInfo6_m1535820885 (Il2CppObject * __this /* static, unused */, TabData_t110553279 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
