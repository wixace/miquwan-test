﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_MotionGenerated
struct UnityEngine_MotionGenerated_t3313844241;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_MotionGenerated::.ctor()
extern "C"  void UnityEngine_MotionGenerated__ctor_m3057380986 (UnityEngine_MotionGenerated_t3313844241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MotionGenerated::__Register()
extern "C"  void UnityEngine_MotionGenerated___Register_m1306102413 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
