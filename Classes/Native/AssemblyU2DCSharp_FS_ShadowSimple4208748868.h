﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// FS_MeshKey
struct FS_MeshKey_t116578208;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_LayerMask3236759763.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Plane4206452690.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FS_ShadowSimple
struct  FS_ShadowSimple_t4208748868  : public MonoBehaviour_t667441552
{
public:
	// System.Single FS_ShadowSimple::maxProjectionDistance
	float ___maxProjectionDistance_2;
	// System.Single FS_ShadowSimple::girth
	float ___girth_3;
	// System.Single FS_ShadowSimple::shadowHoverHeight
	float ___shadowHoverHeight_4;
	// UnityEngine.LayerMask FS_ShadowSimple::layerMask
	LayerMask_t3236759763  ___layerMask_5;
	// UnityEngine.Material FS_ShadowSimple::shadowMaterial
	Material_t3870600107 * ___shadowMaterial_6;
	// System.Boolean FS_ShadowSimple::isStatic
	bool ___isStatic_7;
	// System.Boolean FS_ShadowSimple::useLightSource
	bool ___useLightSource_8;
	// UnityEngine.GameObject FS_ShadowSimple::lightSource
	GameObject_t3674682005 * ___lightSource_9;
	// UnityEngine.Vector3 FS_ShadowSimple::lightDirection
	Vector3_t4282066566  ___lightDirection_10;
	// System.Boolean FS_ShadowSimple::isPerspectiveProjection
	bool ___isPerspectiveProjection_11;
	// System.Boolean FS_ShadowSimple::doVisibilityCulling
	bool ___doVisibilityCulling_12;
	// UnityEngine.Rect FS_ShadowSimple::uvs
	Rect_t4241904616  ___uvs_13;
	// System.Single FS_ShadowSimple::_girth
	float ____girth_14;
	// UnityEngine.Vector3 FS_ShadowSimple::_lightDirection
	Vector3_t4282066566  ____lightDirection_15;
	// System.Boolean FS_ShadowSimple::isGoodPlaneIntersect
	bool ___isGoodPlaneIntersect_16;
	// UnityEngine.Color FS_ShadowSimple::gizmoColor
	Color_t4194546905  ___gizmoColor_17;
	// UnityEngine.Vector3[] FS_ShadowSimple::_corners
	Vector3U5BU5D_t215400611* ____corners_18;
	// UnityEngine.Ray FS_ShadowSimple::r
	Ray_t3134616544  ___r_19;
	// UnityEngine.RaycastHit FS_ShadowSimple::rh
	RaycastHit_t4003175726  ___rh_20;
	// UnityEngine.Bounds FS_ShadowSimple::bounds
	Bounds_t2711641849  ___bounds_21;
	// UnityEngine.Color FS_ShadowSimple::_color
	Color_t4194546905  ____color_22;
	// UnityEngine.Vector3 FS_ShadowSimple::_normal
	Vector3_t4282066566  ____normal_23;
	// UnityEngine.GameObject[] FS_ShadowSimple::cornerGOs
	GameObjectU5BU5D_t2662109048* ___cornerGOs_24;
	// UnityEngine.GameObject FS_ShadowSimple::shadowCaster
	GameObject_t3674682005 * ___shadowCaster_25;
	// UnityEngine.Plane FS_ShadowSimple::shadowPlane
	Plane_t4206452690  ___shadowPlane_26;
	// FS_MeshKey FS_ShadowSimple::meshKey
	FS_MeshKey_t116578208 * ___meshKey_27;

public:
	inline static int32_t get_offset_of_maxProjectionDistance_2() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ___maxProjectionDistance_2)); }
	inline float get_maxProjectionDistance_2() const { return ___maxProjectionDistance_2; }
	inline float* get_address_of_maxProjectionDistance_2() { return &___maxProjectionDistance_2; }
	inline void set_maxProjectionDistance_2(float value)
	{
		___maxProjectionDistance_2 = value;
	}

	inline static int32_t get_offset_of_girth_3() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ___girth_3)); }
	inline float get_girth_3() const { return ___girth_3; }
	inline float* get_address_of_girth_3() { return &___girth_3; }
	inline void set_girth_3(float value)
	{
		___girth_3 = value;
	}

	inline static int32_t get_offset_of_shadowHoverHeight_4() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ___shadowHoverHeight_4)); }
	inline float get_shadowHoverHeight_4() const { return ___shadowHoverHeight_4; }
	inline float* get_address_of_shadowHoverHeight_4() { return &___shadowHoverHeight_4; }
	inline void set_shadowHoverHeight_4(float value)
	{
		___shadowHoverHeight_4 = value;
	}

	inline static int32_t get_offset_of_layerMask_5() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ___layerMask_5)); }
	inline LayerMask_t3236759763  get_layerMask_5() const { return ___layerMask_5; }
	inline LayerMask_t3236759763 * get_address_of_layerMask_5() { return &___layerMask_5; }
	inline void set_layerMask_5(LayerMask_t3236759763  value)
	{
		___layerMask_5 = value;
	}

	inline static int32_t get_offset_of_shadowMaterial_6() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ___shadowMaterial_6)); }
	inline Material_t3870600107 * get_shadowMaterial_6() const { return ___shadowMaterial_6; }
	inline Material_t3870600107 ** get_address_of_shadowMaterial_6() { return &___shadowMaterial_6; }
	inline void set_shadowMaterial_6(Material_t3870600107 * value)
	{
		___shadowMaterial_6 = value;
		Il2CppCodeGenWriteBarrier(&___shadowMaterial_6, value);
	}

	inline static int32_t get_offset_of_isStatic_7() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ___isStatic_7)); }
	inline bool get_isStatic_7() const { return ___isStatic_7; }
	inline bool* get_address_of_isStatic_7() { return &___isStatic_7; }
	inline void set_isStatic_7(bool value)
	{
		___isStatic_7 = value;
	}

	inline static int32_t get_offset_of_useLightSource_8() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ___useLightSource_8)); }
	inline bool get_useLightSource_8() const { return ___useLightSource_8; }
	inline bool* get_address_of_useLightSource_8() { return &___useLightSource_8; }
	inline void set_useLightSource_8(bool value)
	{
		___useLightSource_8 = value;
	}

	inline static int32_t get_offset_of_lightSource_9() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ___lightSource_9)); }
	inline GameObject_t3674682005 * get_lightSource_9() const { return ___lightSource_9; }
	inline GameObject_t3674682005 ** get_address_of_lightSource_9() { return &___lightSource_9; }
	inline void set_lightSource_9(GameObject_t3674682005 * value)
	{
		___lightSource_9 = value;
		Il2CppCodeGenWriteBarrier(&___lightSource_9, value);
	}

	inline static int32_t get_offset_of_lightDirection_10() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ___lightDirection_10)); }
	inline Vector3_t4282066566  get_lightDirection_10() const { return ___lightDirection_10; }
	inline Vector3_t4282066566 * get_address_of_lightDirection_10() { return &___lightDirection_10; }
	inline void set_lightDirection_10(Vector3_t4282066566  value)
	{
		___lightDirection_10 = value;
	}

	inline static int32_t get_offset_of_isPerspectiveProjection_11() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ___isPerspectiveProjection_11)); }
	inline bool get_isPerspectiveProjection_11() const { return ___isPerspectiveProjection_11; }
	inline bool* get_address_of_isPerspectiveProjection_11() { return &___isPerspectiveProjection_11; }
	inline void set_isPerspectiveProjection_11(bool value)
	{
		___isPerspectiveProjection_11 = value;
	}

	inline static int32_t get_offset_of_doVisibilityCulling_12() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ___doVisibilityCulling_12)); }
	inline bool get_doVisibilityCulling_12() const { return ___doVisibilityCulling_12; }
	inline bool* get_address_of_doVisibilityCulling_12() { return &___doVisibilityCulling_12; }
	inline void set_doVisibilityCulling_12(bool value)
	{
		___doVisibilityCulling_12 = value;
	}

	inline static int32_t get_offset_of_uvs_13() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ___uvs_13)); }
	inline Rect_t4241904616  get_uvs_13() const { return ___uvs_13; }
	inline Rect_t4241904616 * get_address_of_uvs_13() { return &___uvs_13; }
	inline void set_uvs_13(Rect_t4241904616  value)
	{
		___uvs_13 = value;
	}

	inline static int32_t get_offset_of__girth_14() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ____girth_14)); }
	inline float get__girth_14() const { return ____girth_14; }
	inline float* get_address_of__girth_14() { return &____girth_14; }
	inline void set__girth_14(float value)
	{
		____girth_14 = value;
	}

	inline static int32_t get_offset_of__lightDirection_15() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ____lightDirection_15)); }
	inline Vector3_t4282066566  get__lightDirection_15() const { return ____lightDirection_15; }
	inline Vector3_t4282066566 * get_address_of__lightDirection_15() { return &____lightDirection_15; }
	inline void set__lightDirection_15(Vector3_t4282066566  value)
	{
		____lightDirection_15 = value;
	}

	inline static int32_t get_offset_of_isGoodPlaneIntersect_16() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ___isGoodPlaneIntersect_16)); }
	inline bool get_isGoodPlaneIntersect_16() const { return ___isGoodPlaneIntersect_16; }
	inline bool* get_address_of_isGoodPlaneIntersect_16() { return &___isGoodPlaneIntersect_16; }
	inline void set_isGoodPlaneIntersect_16(bool value)
	{
		___isGoodPlaneIntersect_16 = value;
	}

	inline static int32_t get_offset_of_gizmoColor_17() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ___gizmoColor_17)); }
	inline Color_t4194546905  get_gizmoColor_17() const { return ___gizmoColor_17; }
	inline Color_t4194546905 * get_address_of_gizmoColor_17() { return &___gizmoColor_17; }
	inline void set_gizmoColor_17(Color_t4194546905  value)
	{
		___gizmoColor_17 = value;
	}

	inline static int32_t get_offset_of__corners_18() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ____corners_18)); }
	inline Vector3U5BU5D_t215400611* get__corners_18() const { return ____corners_18; }
	inline Vector3U5BU5D_t215400611** get_address_of__corners_18() { return &____corners_18; }
	inline void set__corners_18(Vector3U5BU5D_t215400611* value)
	{
		____corners_18 = value;
		Il2CppCodeGenWriteBarrier(&____corners_18, value);
	}

	inline static int32_t get_offset_of_r_19() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ___r_19)); }
	inline Ray_t3134616544  get_r_19() const { return ___r_19; }
	inline Ray_t3134616544 * get_address_of_r_19() { return &___r_19; }
	inline void set_r_19(Ray_t3134616544  value)
	{
		___r_19 = value;
	}

	inline static int32_t get_offset_of_rh_20() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ___rh_20)); }
	inline RaycastHit_t4003175726  get_rh_20() const { return ___rh_20; }
	inline RaycastHit_t4003175726 * get_address_of_rh_20() { return &___rh_20; }
	inline void set_rh_20(RaycastHit_t4003175726  value)
	{
		___rh_20 = value;
	}

	inline static int32_t get_offset_of_bounds_21() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ___bounds_21)); }
	inline Bounds_t2711641849  get_bounds_21() const { return ___bounds_21; }
	inline Bounds_t2711641849 * get_address_of_bounds_21() { return &___bounds_21; }
	inline void set_bounds_21(Bounds_t2711641849  value)
	{
		___bounds_21 = value;
	}

	inline static int32_t get_offset_of__color_22() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ____color_22)); }
	inline Color_t4194546905  get__color_22() const { return ____color_22; }
	inline Color_t4194546905 * get_address_of__color_22() { return &____color_22; }
	inline void set__color_22(Color_t4194546905  value)
	{
		____color_22 = value;
	}

	inline static int32_t get_offset_of__normal_23() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ____normal_23)); }
	inline Vector3_t4282066566  get__normal_23() const { return ____normal_23; }
	inline Vector3_t4282066566 * get_address_of__normal_23() { return &____normal_23; }
	inline void set__normal_23(Vector3_t4282066566  value)
	{
		____normal_23 = value;
	}

	inline static int32_t get_offset_of_cornerGOs_24() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ___cornerGOs_24)); }
	inline GameObjectU5BU5D_t2662109048* get_cornerGOs_24() const { return ___cornerGOs_24; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_cornerGOs_24() { return &___cornerGOs_24; }
	inline void set_cornerGOs_24(GameObjectU5BU5D_t2662109048* value)
	{
		___cornerGOs_24 = value;
		Il2CppCodeGenWriteBarrier(&___cornerGOs_24, value);
	}

	inline static int32_t get_offset_of_shadowCaster_25() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ___shadowCaster_25)); }
	inline GameObject_t3674682005 * get_shadowCaster_25() const { return ___shadowCaster_25; }
	inline GameObject_t3674682005 ** get_address_of_shadowCaster_25() { return &___shadowCaster_25; }
	inline void set_shadowCaster_25(GameObject_t3674682005 * value)
	{
		___shadowCaster_25 = value;
		Il2CppCodeGenWriteBarrier(&___shadowCaster_25, value);
	}

	inline static int32_t get_offset_of_shadowPlane_26() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ___shadowPlane_26)); }
	inline Plane_t4206452690  get_shadowPlane_26() const { return ___shadowPlane_26; }
	inline Plane_t4206452690 * get_address_of_shadowPlane_26() { return &___shadowPlane_26; }
	inline void set_shadowPlane_26(Plane_t4206452690  value)
	{
		___shadowPlane_26 = value;
	}

	inline static int32_t get_offset_of_meshKey_27() { return static_cast<int32_t>(offsetof(FS_ShadowSimple_t4208748868, ___meshKey_27)); }
	inline FS_MeshKey_t116578208 * get_meshKey_27() const { return ___meshKey_27; }
	inline FS_MeshKey_t116578208 ** get_address_of_meshKey_27() { return &___meshKey_27; }
	inline void set_meshKey_27(FS_MeshKey_t116578208 * value)
	{
		___meshKey_27 = value;
		Il2CppCodeGenWriteBarrier(&___meshKey_27, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
