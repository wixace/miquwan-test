﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>
struct Dictionary_2_t967328657;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1582264711.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Pathfinding.Int2,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1546151804_gshared (Enumerator_t1582264711 * __this, Dictionary_2_t967328657 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1546151804(__this, ___host0, method) ((  void (*) (Enumerator_t1582264711 *, Dictionary_2_t967328657 *, const MethodInfo*))Enumerator__ctor_m1546151804_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Pathfinding.Int2,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1127649967_gshared (Enumerator_t1582264711 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1127649967(__this, method) ((  Il2CppObject * (*) (Enumerator_t1582264711 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1127649967_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Pathfinding.Int2,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2594960313_gshared (Enumerator_t1582264711 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2594960313(__this, method) ((  void (*) (Enumerator_t1582264711 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2594960313_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Pathfinding.Int2,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m530662686_gshared (Enumerator_t1582264711 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m530662686(__this, method) ((  void (*) (Enumerator_t1582264711 *, const MethodInfo*))Enumerator_Dispose_m530662686_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Pathfinding.Int2,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2640104553_gshared (Enumerator_t1582264711 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2640104553(__this, method) ((  bool (*) (Enumerator_t1582264711 *, const MethodInfo*))Enumerator_MoveNext_m2640104553_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Pathfinding.Int2,System.Object>::get_Current()
extern "C"  Int2_t1974045593  Enumerator_get_Current_m2689142031_gshared (Enumerator_t1582264711 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2689142031(__this, method) ((  Int2_t1974045593  (*) (Enumerator_t1582264711 *, const MethodInfo*))Enumerator_get_Current_m2689142031_gshared)(__this, method)
