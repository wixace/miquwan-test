﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Bounds>
struct DefaultComparer_t1159418516;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Bounds>::.ctor()
extern "C"  void DefaultComparer__ctor_m2256559576_gshared (DefaultComparer_t1159418516 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2256559576(__this, method) ((  void (*) (DefaultComparer_t1159418516 *, const MethodInfo*))DefaultComparer__ctor_m2256559576_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Bounds>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1072090395_gshared (DefaultComparer_t1159418516 * __this, Bounds_t2711641849  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1072090395(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1159418516 *, Bounds_t2711641849 , const MethodInfo*))DefaultComparer_GetHashCode_m1072090395_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Bounds>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3148440941_gshared (DefaultComparer_t1159418516 * __this, Bounds_t2711641849  ___x0, Bounds_t2711641849  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3148440941(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1159418516 *, Bounds_t2711641849 , Bounds_t2711641849 , const MethodInfo*))DefaultComparer_Equals_m3148440941_gshared)(__this, ___x0, ___y1, method)
