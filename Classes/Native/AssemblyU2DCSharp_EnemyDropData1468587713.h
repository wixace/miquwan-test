﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyDropData
struct  EnemyDropData_t1468587713  : public Il2CppObject
{
public:
	// System.Int32 EnemyDropData::<dropId>k__BackingField
	int32_t ___U3CdropIdU3Ek__BackingField_0;
	// System.Int32 EnemyDropData::<dropNum>k__BackingField
	int32_t ___U3CdropNumU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CdropIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EnemyDropData_t1468587713, ___U3CdropIdU3Ek__BackingField_0)); }
	inline int32_t get_U3CdropIdU3Ek__BackingField_0() const { return ___U3CdropIdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CdropIdU3Ek__BackingField_0() { return &___U3CdropIdU3Ek__BackingField_0; }
	inline void set_U3CdropIdU3Ek__BackingField_0(int32_t value)
	{
		___U3CdropIdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CdropNumU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(EnemyDropData_t1468587713, ___U3CdropNumU3Ek__BackingField_1)); }
	inline int32_t get_U3CdropNumU3Ek__BackingField_1() const { return ___U3CdropNumU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CdropNumU3Ek__BackingField_1() { return &___U3CdropNumU3Ek__BackingField_1; }
	inline void set_U3CdropNumU3Ek__BackingField_1(int32_t value)
	{
		___U3CdropNumU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
