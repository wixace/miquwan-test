﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HeorSetMaterial
struct HeorSetMaterial_t636587977;

#include "codegen/il2cpp-codegen.h"

// System.Void HeorSetMaterial::.ctor()
extern "C"  void HeorSetMaterial__ctor_m2670782914 (HeorSetMaterial_t636587977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeorSetMaterial::SetMaterials(System.Boolean)
extern "C"  void HeorSetMaterial_SetMaterials_m1168353827 (HeorSetMaterial_t636587977 * __this, bool ___isGaoguang0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
