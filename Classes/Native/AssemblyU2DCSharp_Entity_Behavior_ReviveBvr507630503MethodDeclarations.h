﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.ReviveBvr
struct ReviveBvr_t507630503;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// CombatEntity
struct CombatEntity_t684137495;
// AnimationRunner
struct AnimationRunner_t1015409588;
// EffectMgr
struct EffectMgr_t535289511;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// BuffCtrl
struct BuffCtrl_t2836564350;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_EffectMgr535289511.h"
#include "AssemblyU2DCSharp_AnimationRunner1015409588.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniType1006238651.h"

// System.Void Entity.Behavior.ReviveBvr::.ctor()
extern "C"  void ReviveBvr__ctor_m3909225523 (ReviveBvr_t507630503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID Entity.Behavior.ReviveBvr::get_id()
extern "C"  uint8_t ReviveBvr_get_id_m1518880227 (ReviveBvr_t507630503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Entity.Behavior.ReviveBvr::CanTran2OtherBehavior(Entity.Behavior.IBehavior)
extern "C"  bool ReviveBvr_CanTran2OtherBehavior_m3622993880 (ReviveBvr_t507630503 * __this, IBehavior_t770859129 * ___behavior0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.ReviveBvr::Reason()
extern "C"  void ReviveBvr_Reason_m478295029 (ReviveBvr_t507630503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.ReviveBvr::Action()
extern "C"  void ReviveBvr_Action_m3969968999 (ReviveBvr_t507630503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.ReviveBvr::SetParams(System.Object[])
extern "C"  void ReviveBvr_SetParams_m2709360313 (ReviveBvr_t507630503 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.ReviveBvr::DoEntering()
extern "C"  void ReviveBvr_DoEntering_m1557424422 (ReviveBvr_t507630503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.ReviveBvr::DoLeaving()
extern "C"  void ReviveBvr_DoLeaving_m1326433722 (ReviveBvr_t507630503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Entity.Behavior.ReviveBvr::IsLockingBehavior()
extern "C"  bool ReviveBvr_IsLockingBehavior_m2644695300 (ReviveBvr_t507630503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.ReviveBvr::ilo_get_entity1(Entity.Behavior.IBehavior)
extern "C"  CombatEntity_t684137495 * ReviveBvr_ilo_get_entity1_m3925860063 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AnimationRunner Entity.Behavior.ReviveBvr::ilo_get_characterAnim2(CombatEntity)
extern "C"  AnimationRunner_t1015409588 * ReviveBvr_ilo_get_characterAnim2_m1318730671 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.ReviveBvr::ilo_set_isDeath3(CombatEntity,System.Boolean)
extern "C"  void ReviveBvr_ilo_set_isDeath3_m4128518794 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EffectMgr Entity.Behavior.ReviveBvr::ilo_get_EffectMgr4()
extern "C"  EffectMgr_t535289511 * ReviveBvr_ilo_get_EffectMgr4_m2618998318 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.ReviveBvr::ilo_PlayEffect5(EffectMgr,System.Int32,CombatEntity)
extern "C"  void ReviveBvr_ilo_PlayEffect5_m1495510357 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, int32_t ___id1, CombatEntity_t684137495 * ___srcTf2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl Entity.Behavior.ReviveBvr::ilo_get_bvrCtrl6(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * ReviveBvr_ilo_get_bvrCtrl6_m607951909 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BuffCtrl Entity.Behavior.ReviveBvr::ilo_get_buffCtrl7(CombatEntity)
extern "C"  BuffCtrl_t2836564350 * ReviveBvr_ilo_get_buffCtrl7_m3737748822 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.ReviveBvr::ilo_Play8(AnimationRunner,AnimationRunner/AniType,System.Single,System.Boolean)
extern "C"  void ReviveBvr_ilo_Play8_m2642836107 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, int32_t ___aniType1, float ___playTime2, bool ___isskill3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
