﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MethodID
struct MethodID_t3916401116;
// JSDataExchangeMgr/DGetV`1<System.Boolean[]>
struct DGetV_1_t3333954264;
// JSDataExchangeMgr/DGetV`1<System.Object[]>
struct DGetV_1_t986307823;
// JSDataExchangeMgr/DGetV`1<OnPathDelegate>
struct DGetV_1_t476259318;
// JSDataExchangeMgr/DGetV`1<System.Action>
struct DGetV_1_t3648885239;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CombatEntityGenerated
struct  CombatEntityGenerated_t1251886904  : public Il2CppObject
{
public:

public:
};

struct CombatEntityGenerated_t1251886904_StaticFields
{
public:
	// MethodID CombatEntityGenerated::methodID7
	MethodID_t3916401116 * ___methodID7_0;
	// JSDataExchangeMgr/DGetV`1<System.Boolean[]> CombatEntityGenerated::<>f__am$cache1
	DGetV_1_t3333954264 * ___U3CU3Ef__amU24cache1_1;
	// JSDataExchangeMgr/DGetV`1<System.Object[]> CombatEntityGenerated::<>f__am$cache2
	DGetV_1_t986307823 * ___U3CU3Ef__amU24cache2_2;
	// JSDataExchangeMgr/DGetV`1<OnPathDelegate> CombatEntityGenerated::<>f__am$cache3
	DGetV_1_t476259318 * ___U3CU3Ef__amU24cache3_3;
	// JSDataExchangeMgr/DGetV`1<OnPathDelegate> CombatEntityGenerated::<>f__am$cache4
	DGetV_1_t476259318 * ___U3CU3Ef__amU24cache4_4;
	// JSDataExchangeMgr/DGetV`1<System.Action> CombatEntityGenerated::<>f__am$cache5
	DGetV_1_t3648885239 * ___U3CU3Ef__amU24cache5_5;

public:
	inline static int32_t get_offset_of_methodID7_0() { return static_cast<int32_t>(offsetof(CombatEntityGenerated_t1251886904_StaticFields, ___methodID7_0)); }
	inline MethodID_t3916401116 * get_methodID7_0() const { return ___methodID7_0; }
	inline MethodID_t3916401116 ** get_address_of_methodID7_0() { return &___methodID7_0; }
	inline void set_methodID7_0(MethodID_t3916401116 * value)
	{
		___methodID7_0 = value;
		Il2CppCodeGenWriteBarrier(&___methodID7_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(CombatEntityGenerated_t1251886904_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline DGetV_1_t3333954264 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline DGetV_1_t3333954264 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(DGetV_1_t3333954264 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(CombatEntityGenerated_t1251886904_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline DGetV_1_t986307823 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline DGetV_1_t986307823 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(DGetV_1_t986307823 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(CombatEntityGenerated_t1251886904_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline DGetV_1_t476259318 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline DGetV_1_t476259318 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(DGetV_1_t476259318 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(CombatEntityGenerated_t1251886904_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline DGetV_1_t476259318 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline DGetV_1_t476259318 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(DGetV_1_t476259318 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(CombatEntityGenerated_t1251886904_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline DGetV_1_t3648885239 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline DGetV_1_t3648885239 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(DGetV_1_t3648885239 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
