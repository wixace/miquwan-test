﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GraphicsGenerated
struct UnityEngine_GraphicsGenerated_t1365356252;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t4024180168;
// UnityEngine.RenderBuffer[]
struct RenderBufferU5BU5D_t1970987103;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_GraphicsGenerated::.ctor()
extern "C"  void UnityEngine_GraphicsGenerated__ctor_m1538972175 (UnityEngine_GraphicsGenerated_t1365356252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_Graphics1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_Graphics1_m2996124643 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GraphicsGenerated::Graphics_activeColorBuffer(JSVCall)
extern "C"  void UnityEngine_GraphicsGenerated_Graphics_activeColorBuffer_m3579877865 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GraphicsGenerated::Graphics_activeDepthBuffer(JSVCall)
extern "C"  void UnityEngine_GraphicsGenerated_Graphics_activeDepthBuffer_m2319504009 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_Blit__Texture__RenderTexture__Material__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_Blit__Texture__RenderTexture__Material__Int32_m3523848869 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_Blit__Texture__Material__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_Blit__Texture__Material__Int32_m19566598 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_Blit__Texture__RenderTexture__Material(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_Blit__Texture__RenderTexture__Material_m3988709963 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_Blit__Texture__RenderTexture(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_Blit__Texture__RenderTexture_m764530724 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_Blit__Texture__Material(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_Blit__Texture__Material_m3329284106 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_BlitMultiTap__Texture__RenderTexture__Material__Vector2_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_BlitMultiTap__Texture__RenderTexture__Material__Vector2_Array_m3749617430 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_ClearRandomWriteTargets(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_ClearRandomWriteTargets_m1886318032 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawMesh__Mesh__Vector3__Quaternion__Material__Int32__Camera__Int32__MaterialPropertyBlock__ShadowCastingMode__Boolean__Transform(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawMesh__Mesh__Vector3__Quaternion__Material__Int32__Camera__Int32__MaterialPropertyBlock__ShadowCastingMode__Boolean__Transform_m2539999048 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawMesh__Mesh__Matrix4x4__Material__Int32__Camera__Int32__MaterialPropertyBlock__ShadowCastingMode__Boolean__Transform(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawMesh__Mesh__Matrix4x4__Material__Int32__Camera__Int32__MaterialPropertyBlock__ShadowCastingMode__Boolean__Transform_m845958761 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawMesh__Mesh__Vector3__Quaternion__Material__Int32__Camera__Int32__MaterialPropertyBlock__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawMesh__Mesh__Vector3__Quaternion__Material__Int32__Camera__Int32__MaterialPropertyBlock__Boolean__Boolean_m2938011844 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawMesh__Mesh__Vector3__Quaternion__Material__Int32__Camera__Int32__MaterialPropertyBlock__ShadowCastingMode__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawMesh__Mesh__Vector3__Quaternion__Material__Int32__Camera__Int32__MaterialPropertyBlock__ShadowCastingMode__Boolean_m2209519142 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawMesh__Mesh__Matrix4x4__Material__Int32__Camera__Int32__MaterialPropertyBlock__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawMesh__Mesh__Matrix4x4__Material__Int32__Camera__Int32__MaterialPropertyBlock__Boolean__Boolean_m3558420227 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawMesh__Mesh__Matrix4x4__Material__Int32__Camera__Int32__MaterialPropertyBlock__ShadowCastingMode__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawMesh__Mesh__Matrix4x4__Material__Int32__Camera__Int32__MaterialPropertyBlock__ShadowCastingMode__Boolean_m1307820453 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawMesh__Mesh__Vector3__Quaternion__Material__Int32__Camera__Int32__MaterialPropertyBlock__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawMesh__Mesh__Vector3__Quaternion__Material__Int32__Camera__Int32__MaterialPropertyBlock__Boolean_m3431892774 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawMesh__Mesh__Vector3__Quaternion__Material__Int32__Camera__Int32__MaterialPropertyBlock__ShadowCastingMode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawMesh__Mesh__Vector3__Quaternion__Material__Int32__Camera__Int32__MaterialPropertyBlock__ShadowCastingMode_m1784970116 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawMesh__Mesh__Vector3__Quaternion__Material__Int32__Camera__Int32__MaterialPropertyBlock(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawMesh__Mesh__Vector3__Quaternion__Material__Int32__Camera__Int32__MaterialPropertyBlock_m1780836996 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawMesh__Mesh__Matrix4x4__Material__Int32__Camera__Int32__MaterialPropertyBlock__ShadowCastingMode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawMesh__Mesh__Matrix4x4__Material__Int32__Camera__Int32__MaterialPropertyBlock__ShadowCastingMode_m3837760805 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawMesh__Mesh__Matrix4x4__Material__Int32__Camera__Int32__MaterialPropertyBlock__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawMesh__Mesh__Matrix4x4__Material__Int32__Camera__Int32__MaterialPropertyBlock__Boolean_m883820039 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawMesh__Mesh__Vector3__Quaternion__Material__Int32__Camera__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawMesh__Mesh__Vector3__Quaternion__Material__Int32__Camera__Int32_m3557724399 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawMesh__Mesh__Matrix4x4__Material__Int32__Camera__Int32__MaterialPropertyBlock(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawMesh__Mesh__Matrix4x4__Material__Int32__Camera__Int32__MaterialPropertyBlock_m4142824067 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawMesh__Mesh__Vector3__Quaternion__Material__Int32__Camera(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawMesh__Mesh__Vector3__Quaternion__Material__Int32__Camera_m2538347329 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawMesh__Mesh__Matrix4x4__Material__Int32__Camera__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawMesh__Mesh__Matrix4x4__Material__Int32__Camera__Int32_m4011447248 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawMesh__Mesh__Matrix4x4__Material__Int32__Camera(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawMesh__Mesh__Matrix4x4__Material__Int32__Camera_m119137152 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawMesh__Mesh__Vector3__Quaternion__Material__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawMesh__Mesh__Vector3__Quaternion__Material__Int32_m1153555676 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawMesh__Mesh__Matrix4x4__Material__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawMesh__Mesh__Matrix4x4__Material__Int32_m354374683 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawMeshNow__Mesh__Vector3__Quaternion__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawMeshNow__Mesh__Vector3__Quaternion__Int32_m786024751 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawMeshNow__Mesh__Vector3__Quaternion(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawMeshNow__Mesh__Vector3__Quaternion_m1603513089 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawMeshNow__Mesh__Matrix4x4__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawMeshNow__Mesh__Matrix4x4__Int32_m3100521006 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawMeshNow__Mesh__Matrix4x4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawMeshNow__Mesh__Matrix4x4_m2166215394 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawProcedural__MeshTopology__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawProcedural__MeshTopology__Int32__Int32_m3347785122 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawProcedural__MeshTopology__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawProcedural__MeshTopology__Int32_m4009826798 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawProceduralIndirect__MeshTopology__ComputeBuffer__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawProceduralIndirect__MeshTopology__ComputeBuffer__Int32_m608047975 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawProceduralIndirect__MeshTopology__ComputeBuffer(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawProceduralIndirect__MeshTopology__ComputeBuffer_m1707470793 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawTexture__Rect__Texture__Rect__Int32__Int32__Int32__Int32__Color__Material(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawTexture__Rect__Texture__Rect__Int32__Int32__Int32__Int32__Color__Material_m276603939 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawTexture__Rect__Texture__Rect__Int32__Int32__Int32__Int32__Material(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawTexture__Rect__Texture__Rect__Int32__Int32__Int32__Int32__Material_m3548507696 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawTexture__Rect__Texture__Rect__Int32__Int32__Int32__Int32__Color(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawTexture__Rect__Texture__Rect__Int32__Int32__Int32__Int32__Color_m3748102140 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawTexture__Rect__Texture__Int32__Int32__Int32__Int32__Material(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawTexture__Rect__Texture__Int32__Int32__Int32__Int32__Material_m4143464780 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawTexture__Rect__Texture__Rect__Int32__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawTexture__Rect__Texture__Rect__Int32__Int32__Int32__Int32_m2134017609 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawTexture__Rect__Texture__Int32__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawTexture__Rect__Texture__Int32__Int32__Int32__Int32_m1061473381 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawTexture__Rect__Texture__Material(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawTexture__Rect__Texture__Material_m3519000268 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_DrawTexture__Rect__Texture(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_DrawTexture__Rect__Texture_m1248795109 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_ExecuteCommandBuffer__CommandBuffer(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_ExecuteCommandBuffer__CommandBuffer_m1042405202 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_SetRandomWriteTarget__Int32__ComputeBuffer(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_SetRandomWriteTarget__Int32__ComputeBuffer_m2382161241 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_SetRandomWriteTarget__Int32__RenderTexture(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_SetRandomWriteTarget__Int32__RenderTexture_m3080776231 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_SetRenderTarget__RenderBuffer__RenderBuffer__Int32__CubemapFace(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_SetRenderTarget__RenderBuffer__RenderBuffer__Int32__CubemapFace_m301647624 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_SetRenderTarget__RenderBuffer__RenderBuffer__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_SetRenderTarget__RenderBuffer__RenderBuffer__Int32_m2385418558 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_SetRenderTarget__RenderTexture__Int32__CubemapFace(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_SetRenderTarget__RenderTexture__Int32__CubemapFace_m3623569975 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_SetRenderTarget__RenderBuffer_Array__RenderBuffer(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_SetRenderTarget__RenderBuffer_Array__RenderBuffer_m2531609036 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_SetRenderTarget__RenderTexture__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_SetRenderTarget__RenderTexture__Int32_m1850578351 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_SetRenderTarget__RenderBuffer__RenderBuffer(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_SetRenderTarget__RenderBuffer__RenderBuffer_m1873558994 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_SetRenderTarget__RenderTargetSetup(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_SetRenderTarget__RenderTargetSetup_m3335319442 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::Graphics_SetRenderTarget__RenderTexture(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_Graphics_SetRenderTarget__RenderTexture_m1414852225 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GraphicsGenerated::__Register()
extern "C"  void UnityEngine_GraphicsGenerated___Register_m1748472856 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2[] UnityEngine_GraphicsGenerated::<Graphics_BlitMultiTap__Texture__RenderTexture__Material__Vector2_Array>m__256()
extern "C"  Vector2U5BU5D_t4024180168* UnityEngine_GraphicsGenerated_U3CGraphics_BlitMultiTap__Texture__RenderTexture__Material__Vector2_ArrayU3Em__256_m3988704415 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderBuffer[] UnityEngine_GraphicsGenerated::<Graphics_SetRenderTarget__RenderBuffer_Array__RenderBuffer>m__257()
extern "C"  RenderBufferU5BU5D_t1970987103* UnityEngine_GraphicsGenerated_U3CGraphics_SetRenderTarget__RenderBuffer_Array__RenderBufferU3Em__257_m4168178357 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_ilo_attachFinalizerObject1_m4188026056 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GraphicsGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_GraphicsGenerated_ilo_addJSCSRel2_m243972556 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GraphicsGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_GraphicsGenerated_ilo_setObject3_m3901850625 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_GraphicsGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_GraphicsGenerated_ilo_getObject4_m3925017017 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GraphicsGenerated::ilo_getInt325(System.Int32)
extern "C"  int32_t UnityEngine_GraphicsGenerated_ilo_getInt325_m1533256118 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_GraphicsGenerated::ilo_getVector3S6(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_GraphicsGenerated_ilo_getVector3S6_m3940991580 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GraphicsGenerated::ilo_getEnum7(System.Int32)
extern "C"  int32_t UnityEngine_GraphicsGenerated_ilo_getEnum7_m1239950411 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GraphicsGenerated::ilo_getBooleanS8(System.Int32)
extern "C"  bool UnityEngine_GraphicsGenerated_ilo_getBooleanS8_m3503160508 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GraphicsGenerated::ilo_getArrayLength9(System.Int32)
extern "C"  int32_t UnityEngine_GraphicsGenerated_ilo_getArrayLength9_m4222693577 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GraphicsGenerated::ilo_getObject10(System.Int32)
extern "C"  int32_t UnityEngine_GraphicsGenerated_ilo_getObject10_m2265177197 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GraphicsGenerated::ilo_getElement11(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_GraphicsGenerated_ilo_getElement11_m1106726246 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
