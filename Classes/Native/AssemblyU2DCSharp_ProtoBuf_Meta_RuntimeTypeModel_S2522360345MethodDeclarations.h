﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Meta.RuntimeTypeModel/Singleton
struct Singleton_t2522360345;

#include "codegen/il2cpp-codegen.h"

// System.Void ProtoBuf.Meta.RuntimeTypeModel/Singleton::.ctor()
extern "C"  void Singleton__ctor_m1925546146 (Singleton_t2522360345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel/Singleton::.cctor()
extern "C"  void Singleton__cctor_m3375259467 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
