﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;
// portalCfg
struct portalCfg_t1117034584;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "AssemblyU2DCSharp_Monster2901270458.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Portal
struct  Portal_t2396353676  : public Monster_t2901270458
{
public:
	// System.Collections.Generic.List`1<System.Int32> Portal::_cacheList
	List_1_t2522024052 * ____cacheList_337;
	// System.Collections.Generic.List`1<CombatEntity> Portal::PortalEntityList
	List_1_t2052323047 * ___PortalEntityList_338;
	// portalCfg Portal::portalCfg
	portalCfg_t1117034584 * ___portalCfg_339;
	// System.Single Portal::parY
	float ___parY_340;
	// UnityEngine.GameObject Portal::<children>k__BackingField
	GameObject_t3674682005 * ___U3CchildrenU3Ek__BackingField_341;
	// System.Boolean Portal::<isPortalFinish>k__BackingField
	bool ___U3CisPortalFinishU3Ek__BackingField_342;
	// System.Int32 Portal::<curRound>k__BackingField
	int32_t ___U3CcurRoundU3Ek__BackingField_343;
	// System.Int32 Portal::<MonseterNum>k__BackingField
	int32_t ___U3CMonseterNumU3Ek__BackingField_344;

public:
	inline static int32_t get_offset_of__cacheList_337() { return static_cast<int32_t>(offsetof(Portal_t2396353676, ____cacheList_337)); }
	inline List_1_t2522024052 * get__cacheList_337() const { return ____cacheList_337; }
	inline List_1_t2522024052 ** get_address_of__cacheList_337() { return &____cacheList_337; }
	inline void set__cacheList_337(List_1_t2522024052 * value)
	{
		____cacheList_337 = value;
		Il2CppCodeGenWriteBarrier(&____cacheList_337, value);
	}

	inline static int32_t get_offset_of_PortalEntityList_338() { return static_cast<int32_t>(offsetof(Portal_t2396353676, ___PortalEntityList_338)); }
	inline List_1_t2052323047 * get_PortalEntityList_338() const { return ___PortalEntityList_338; }
	inline List_1_t2052323047 ** get_address_of_PortalEntityList_338() { return &___PortalEntityList_338; }
	inline void set_PortalEntityList_338(List_1_t2052323047 * value)
	{
		___PortalEntityList_338 = value;
		Il2CppCodeGenWriteBarrier(&___PortalEntityList_338, value);
	}

	inline static int32_t get_offset_of_portalCfg_339() { return static_cast<int32_t>(offsetof(Portal_t2396353676, ___portalCfg_339)); }
	inline portalCfg_t1117034584 * get_portalCfg_339() const { return ___portalCfg_339; }
	inline portalCfg_t1117034584 ** get_address_of_portalCfg_339() { return &___portalCfg_339; }
	inline void set_portalCfg_339(portalCfg_t1117034584 * value)
	{
		___portalCfg_339 = value;
		Il2CppCodeGenWriteBarrier(&___portalCfg_339, value);
	}

	inline static int32_t get_offset_of_parY_340() { return static_cast<int32_t>(offsetof(Portal_t2396353676, ___parY_340)); }
	inline float get_parY_340() const { return ___parY_340; }
	inline float* get_address_of_parY_340() { return &___parY_340; }
	inline void set_parY_340(float value)
	{
		___parY_340 = value;
	}

	inline static int32_t get_offset_of_U3CchildrenU3Ek__BackingField_341() { return static_cast<int32_t>(offsetof(Portal_t2396353676, ___U3CchildrenU3Ek__BackingField_341)); }
	inline GameObject_t3674682005 * get_U3CchildrenU3Ek__BackingField_341() const { return ___U3CchildrenU3Ek__BackingField_341; }
	inline GameObject_t3674682005 ** get_address_of_U3CchildrenU3Ek__BackingField_341() { return &___U3CchildrenU3Ek__BackingField_341; }
	inline void set_U3CchildrenU3Ek__BackingField_341(GameObject_t3674682005 * value)
	{
		___U3CchildrenU3Ek__BackingField_341 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchildrenU3Ek__BackingField_341, value);
	}

	inline static int32_t get_offset_of_U3CisPortalFinishU3Ek__BackingField_342() { return static_cast<int32_t>(offsetof(Portal_t2396353676, ___U3CisPortalFinishU3Ek__BackingField_342)); }
	inline bool get_U3CisPortalFinishU3Ek__BackingField_342() const { return ___U3CisPortalFinishU3Ek__BackingField_342; }
	inline bool* get_address_of_U3CisPortalFinishU3Ek__BackingField_342() { return &___U3CisPortalFinishU3Ek__BackingField_342; }
	inline void set_U3CisPortalFinishU3Ek__BackingField_342(bool value)
	{
		___U3CisPortalFinishU3Ek__BackingField_342 = value;
	}

	inline static int32_t get_offset_of_U3CcurRoundU3Ek__BackingField_343() { return static_cast<int32_t>(offsetof(Portal_t2396353676, ___U3CcurRoundU3Ek__BackingField_343)); }
	inline int32_t get_U3CcurRoundU3Ek__BackingField_343() const { return ___U3CcurRoundU3Ek__BackingField_343; }
	inline int32_t* get_address_of_U3CcurRoundU3Ek__BackingField_343() { return &___U3CcurRoundU3Ek__BackingField_343; }
	inline void set_U3CcurRoundU3Ek__BackingField_343(int32_t value)
	{
		___U3CcurRoundU3Ek__BackingField_343 = value;
	}

	inline static int32_t get_offset_of_U3CMonseterNumU3Ek__BackingField_344() { return static_cast<int32_t>(offsetof(Portal_t2396353676, ___U3CMonseterNumU3Ek__BackingField_344)); }
	inline int32_t get_U3CMonseterNumU3Ek__BackingField_344() const { return ___U3CMonseterNumU3Ek__BackingField_344; }
	inline int32_t* get_address_of_U3CMonseterNumU3Ek__BackingField_344() { return &___U3CMonseterNumU3Ek__BackingField_344; }
	inline void set_U3CMonseterNumU3Ek__BackingField_344(int32_t value)
	{
		___U3CMonseterNumU3Ek__BackingField_344 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
