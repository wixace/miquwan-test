﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1528377789.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo2746035113.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m304743021_gshared (InternalEnumerator_1_t1528377789 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m304743021(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1528377789 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m304743021_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3917036051_gshared (InternalEnumerator_1_t1528377789 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3917036051(__this, method) ((  void (*) (InternalEnumerator_1_t1528377789 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3917036051_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1786958719_gshared (InternalEnumerator_1_t1528377789 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1786958719(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1528377789 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1786958719_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3362144388_gshared (InternalEnumerator_1_t1528377789 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3362144388(__this, method) ((  void (*) (InternalEnumerator_1_t1528377789 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3362144388_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3534062975_gshared (InternalEnumerator_1_t1528377789 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3534062975(__this, method) ((  bool (*) (InternalEnumerator_1_t1528377789 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3534062975_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::get_Current()
extern "C"  AnimatorClipInfo_t2746035113  InternalEnumerator_1_get_Current_m3776355892_gshared (InternalEnumerator_1_t1528377789 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3776355892(__this, method) ((  AnimatorClipInfo_t2746035113  (*) (InternalEnumerator_1_t1528377789 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3776355892_gshared)(__this, method)
