﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginQiangWan
struct  PluginQiangWan_t3301124917  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginQiangWan::openid
	String_t* ___openid_7;
	// System.String PluginQiangWan::token
	String_t* ___token_8;
	// System.String PluginQiangWan::sign
	String_t* ___sign_9;
	// System.String PluginQiangWan::configId
	String_t* ___configId_10;
	// System.String PluginQiangWan::serverid
	String_t* ___serverid_11;
	// System.String PluginQiangWan::servername
	String_t* ___servername_12;
	// System.String PluginQiangWan::roleid
	String_t* ___roleid_13;
	// System.String PluginQiangWan::rolename
	String_t* ___rolename_14;
	// System.String PluginQiangWan::rolelevel
	String_t* ___rolelevel_15;
	// System.String PluginQiangWan::CreatRoleTime
	String_t* ___CreatRoleTime_16;
	// System.String PluginQiangWan::gold
	String_t* ___gold_17;
	// System.String PluginQiangWan::VipLevel
	String_t* ___VipLevel_18;
	// System.Boolean PluginQiangWan::isFirstCollectData
	bool ___isFirstCollectData_19;
	// System.String PluginQiangWan::appid
	String_t* ___appid_20;
	// System.String PluginQiangWan::codeversion
	String_t* ___codeversion_21;
	// System.String PluginQiangWan::os
	String_t* ___os_22;
	// System.String PluginQiangWan::username
	String_t* ___username_23;
	// System.String PluginQiangWan::sdkid
	String_t* ___sdkid_24;
	// System.String PluginQiangWan::sdappname
	String_t* ___sdappname_25;
	// System.String PluginQiangWan::sdkappid
	String_t* ___sdkappid_26;
	// System.String PluginQiangWan::sdkpromoteid
	String_t* ___sdkpromoteid_27;
	// System.String PluginQiangWan::sdkpromotename
	String_t* ___sdkpromotename_28;
	// System.String PluginQiangWan::sdktesttype
	String_t* ___sdktesttype_29;
	// System.String PluginQiangWan::sdktrackkey
	String_t* ___sdktrackkey_30;
	// Mihua.SDK.PayInfo PluginQiangWan::payInfo
	PayInfo_t1775308120 * ___payInfo_31;

public:
	inline static int32_t get_offset_of_openid_7() { return static_cast<int32_t>(offsetof(PluginQiangWan_t3301124917, ___openid_7)); }
	inline String_t* get_openid_7() const { return ___openid_7; }
	inline String_t** get_address_of_openid_7() { return &___openid_7; }
	inline void set_openid_7(String_t* value)
	{
		___openid_7 = value;
		Il2CppCodeGenWriteBarrier(&___openid_7, value);
	}

	inline static int32_t get_offset_of_token_8() { return static_cast<int32_t>(offsetof(PluginQiangWan_t3301124917, ___token_8)); }
	inline String_t* get_token_8() const { return ___token_8; }
	inline String_t** get_address_of_token_8() { return &___token_8; }
	inline void set_token_8(String_t* value)
	{
		___token_8 = value;
		Il2CppCodeGenWriteBarrier(&___token_8, value);
	}

	inline static int32_t get_offset_of_sign_9() { return static_cast<int32_t>(offsetof(PluginQiangWan_t3301124917, ___sign_9)); }
	inline String_t* get_sign_9() const { return ___sign_9; }
	inline String_t** get_address_of_sign_9() { return &___sign_9; }
	inline void set_sign_9(String_t* value)
	{
		___sign_9 = value;
		Il2CppCodeGenWriteBarrier(&___sign_9, value);
	}

	inline static int32_t get_offset_of_configId_10() { return static_cast<int32_t>(offsetof(PluginQiangWan_t3301124917, ___configId_10)); }
	inline String_t* get_configId_10() const { return ___configId_10; }
	inline String_t** get_address_of_configId_10() { return &___configId_10; }
	inline void set_configId_10(String_t* value)
	{
		___configId_10 = value;
		Il2CppCodeGenWriteBarrier(&___configId_10, value);
	}

	inline static int32_t get_offset_of_serverid_11() { return static_cast<int32_t>(offsetof(PluginQiangWan_t3301124917, ___serverid_11)); }
	inline String_t* get_serverid_11() const { return ___serverid_11; }
	inline String_t** get_address_of_serverid_11() { return &___serverid_11; }
	inline void set_serverid_11(String_t* value)
	{
		___serverid_11 = value;
		Il2CppCodeGenWriteBarrier(&___serverid_11, value);
	}

	inline static int32_t get_offset_of_servername_12() { return static_cast<int32_t>(offsetof(PluginQiangWan_t3301124917, ___servername_12)); }
	inline String_t* get_servername_12() const { return ___servername_12; }
	inline String_t** get_address_of_servername_12() { return &___servername_12; }
	inline void set_servername_12(String_t* value)
	{
		___servername_12 = value;
		Il2CppCodeGenWriteBarrier(&___servername_12, value);
	}

	inline static int32_t get_offset_of_roleid_13() { return static_cast<int32_t>(offsetof(PluginQiangWan_t3301124917, ___roleid_13)); }
	inline String_t* get_roleid_13() const { return ___roleid_13; }
	inline String_t** get_address_of_roleid_13() { return &___roleid_13; }
	inline void set_roleid_13(String_t* value)
	{
		___roleid_13 = value;
		Il2CppCodeGenWriteBarrier(&___roleid_13, value);
	}

	inline static int32_t get_offset_of_rolename_14() { return static_cast<int32_t>(offsetof(PluginQiangWan_t3301124917, ___rolename_14)); }
	inline String_t* get_rolename_14() const { return ___rolename_14; }
	inline String_t** get_address_of_rolename_14() { return &___rolename_14; }
	inline void set_rolename_14(String_t* value)
	{
		___rolename_14 = value;
		Il2CppCodeGenWriteBarrier(&___rolename_14, value);
	}

	inline static int32_t get_offset_of_rolelevel_15() { return static_cast<int32_t>(offsetof(PluginQiangWan_t3301124917, ___rolelevel_15)); }
	inline String_t* get_rolelevel_15() const { return ___rolelevel_15; }
	inline String_t** get_address_of_rolelevel_15() { return &___rolelevel_15; }
	inline void set_rolelevel_15(String_t* value)
	{
		___rolelevel_15 = value;
		Il2CppCodeGenWriteBarrier(&___rolelevel_15, value);
	}

	inline static int32_t get_offset_of_CreatRoleTime_16() { return static_cast<int32_t>(offsetof(PluginQiangWan_t3301124917, ___CreatRoleTime_16)); }
	inline String_t* get_CreatRoleTime_16() const { return ___CreatRoleTime_16; }
	inline String_t** get_address_of_CreatRoleTime_16() { return &___CreatRoleTime_16; }
	inline void set_CreatRoleTime_16(String_t* value)
	{
		___CreatRoleTime_16 = value;
		Il2CppCodeGenWriteBarrier(&___CreatRoleTime_16, value);
	}

	inline static int32_t get_offset_of_gold_17() { return static_cast<int32_t>(offsetof(PluginQiangWan_t3301124917, ___gold_17)); }
	inline String_t* get_gold_17() const { return ___gold_17; }
	inline String_t** get_address_of_gold_17() { return &___gold_17; }
	inline void set_gold_17(String_t* value)
	{
		___gold_17 = value;
		Il2CppCodeGenWriteBarrier(&___gold_17, value);
	}

	inline static int32_t get_offset_of_VipLevel_18() { return static_cast<int32_t>(offsetof(PluginQiangWan_t3301124917, ___VipLevel_18)); }
	inline String_t* get_VipLevel_18() const { return ___VipLevel_18; }
	inline String_t** get_address_of_VipLevel_18() { return &___VipLevel_18; }
	inline void set_VipLevel_18(String_t* value)
	{
		___VipLevel_18 = value;
		Il2CppCodeGenWriteBarrier(&___VipLevel_18, value);
	}

	inline static int32_t get_offset_of_isFirstCollectData_19() { return static_cast<int32_t>(offsetof(PluginQiangWan_t3301124917, ___isFirstCollectData_19)); }
	inline bool get_isFirstCollectData_19() const { return ___isFirstCollectData_19; }
	inline bool* get_address_of_isFirstCollectData_19() { return &___isFirstCollectData_19; }
	inline void set_isFirstCollectData_19(bool value)
	{
		___isFirstCollectData_19 = value;
	}

	inline static int32_t get_offset_of_appid_20() { return static_cast<int32_t>(offsetof(PluginQiangWan_t3301124917, ___appid_20)); }
	inline String_t* get_appid_20() const { return ___appid_20; }
	inline String_t** get_address_of_appid_20() { return &___appid_20; }
	inline void set_appid_20(String_t* value)
	{
		___appid_20 = value;
		Il2CppCodeGenWriteBarrier(&___appid_20, value);
	}

	inline static int32_t get_offset_of_codeversion_21() { return static_cast<int32_t>(offsetof(PluginQiangWan_t3301124917, ___codeversion_21)); }
	inline String_t* get_codeversion_21() const { return ___codeversion_21; }
	inline String_t** get_address_of_codeversion_21() { return &___codeversion_21; }
	inline void set_codeversion_21(String_t* value)
	{
		___codeversion_21 = value;
		Il2CppCodeGenWriteBarrier(&___codeversion_21, value);
	}

	inline static int32_t get_offset_of_os_22() { return static_cast<int32_t>(offsetof(PluginQiangWan_t3301124917, ___os_22)); }
	inline String_t* get_os_22() const { return ___os_22; }
	inline String_t** get_address_of_os_22() { return &___os_22; }
	inline void set_os_22(String_t* value)
	{
		___os_22 = value;
		Il2CppCodeGenWriteBarrier(&___os_22, value);
	}

	inline static int32_t get_offset_of_username_23() { return static_cast<int32_t>(offsetof(PluginQiangWan_t3301124917, ___username_23)); }
	inline String_t* get_username_23() const { return ___username_23; }
	inline String_t** get_address_of_username_23() { return &___username_23; }
	inline void set_username_23(String_t* value)
	{
		___username_23 = value;
		Il2CppCodeGenWriteBarrier(&___username_23, value);
	}

	inline static int32_t get_offset_of_sdkid_24() { return static_cast<int32_t>(offsetof(PluginQiangWan_t3301124917, ___sdkid_24)); }
	inline String_t* get_sdkid_24() const { return ___sdkid_24; }
	inline String_t** get_address_of_sdkid_24() { return &___sdkid_24; }
	inline void set_sdkid_24(String_t* value)
	{
		___sdkid_24 = value;
		Il2CppCodeGenWriteBarrier(&___sdkid_24, value);
	}

	inline static int32_t get_offset_of_sdappname_25() { return static_cast<int32_t>(offsetof(PluginQiangWan_t3301124917, ___sdappname_25)); }
	inline String_t* get_sdappname_25() const { return ___sdappname_25; }
	inline String_t** get_address_of_sdappname_25() { return &___sdappname_25; }
	inline void set_sdappname_25(String_t* value)
	{
		___sdappname_25 = value;
		Il2CppCodeGenWriteBarrier(&___sdappname_25, value);
	}

	inline static int32_t get_offset_of_sdkappid_26() { return static_cast<int32_t>(offsetof(PluginQiangWan_t3301124917, ___sdkappid_26)); }
	inline String_t* get_sdkappid_26() const { return ___sdkappid_26; }
	inline String_t** get_address_of_sdkappid_26() { return &___sdkappid_26; }
	inline void set_sdkappid_26(String_t* value)
	{
		___sdkappid_26 = value;
		Il2CppCodeGenWriteBarrier(&___sdkappid_26, value);
	}

	inline static int32_t get_offset_of_sdkpromoteid_27() { return static_cast<int32_t>(offsetof(PluginQiangWan_t3301124917, ___sdkpromoteid_27)); }
	inline String_t* get_sdkpromoteid_27() const { return ___sdkpromoteid_27; }
	inline String_t** get_address_of_sdkpromoteid_27() { return &___sdkpromoteid_27; }
	inline void set_sdkpromoteid_27(String_t* value)
	{
		___sdkpromoteid_27 = value;
		Il2CppCodeGenWriteBarrier(&___sdkpromoteid_27, value);
	}

	inline static int32_t get_offset_of_sdkpromotename_28() { return static_cast<int32_t>(offsetof(PluginQiangWan_t3301124917, ___sdkpromotename_28)); }
	inline String_t* get_sdkpromotename_28() const { return ___sdkpromotename_28; }
	inline String_t** get_address_of_sdkpromotename_28() { return &___sdkpromotename_28; }
	inline void set_sdkpromotename_28(String_t* value)
	{
		___sdkpromotename_28 = value;
		Il2CppCodeGenWriteBarrier(&___sdkpromotename_28, value);
	}

	inline static int32_t get_offset_of_sdktesttype_29() { return static_cast<int32_t>(offsetof(PluginQiangWan_t3301124917, ___sdktesttype_29)); }
	inline String_t* get_sdktesttype_29() const { return ___sdktesttype_29; }
	inline String_t** get_address_of_sdktesttype_29() { return &___sdktesttype_29; }
	inline void set_sdktesttype_29(String_t* value)
	{
		___sdktesttype_29 = value;
		Il2CppCodeGenWriteBarrier(&___sdktesttype_29, value);
	}

	inline static int32_t get_offset_of_sdktrackkey_30() { return static_cast<int32_t>(offsetof(PluginQiangWan_t3301124917, ___sdktrackkey_30)); }
	inline String_t* get_sdktrackkey_30() const { return ___sdktrackkey_30; }
	inline String_t** get_address_of_sdktrackkey_30() { return &___sdktrackkey_30; }
	inline void set_sdktrackkey_30(String_t* value)
	{
		___sdktrackkey_30 = value;
		Il2CppCodeGenWriteBarrier(&___sdktrackkey_30, value);
	}

	inline static int32_t get_offset_of_payInfo_31() { return static_cast<int32_t>(offsetof(PluginQiangWan_t3301124917, ___payInfo_31)); }
	inline PayInfo_t1775308120 * get_payInfo_31() const { return ___payInfo_31; }
	inline PayInfo_t1775308120 ** get_address_of_payInfo_31() { return &___payInfo_31; }
	inline void set_payInfo_31(PayInfo_t1775308120 * value)
	{
		___payInfo_31 = value;
		Il2CppCodeGenWriteBarrier(&___payInfo_31, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
