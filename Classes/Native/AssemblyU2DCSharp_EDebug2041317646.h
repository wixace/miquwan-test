﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LogWriter
struct LogWriter_t4068659383;
// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_LogLevel2060375744.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EDebug
struct  EDebug_t2041317646  : public Il2CppObject
{
public:

public:
};

struct EDebug_t2041317646_StaticFields
{
public:
	// LogLevel EDebug::CurrentLogLevels
	int32_t ___CurrentLogLevels_1;
	// LogWriter EDebug::m_logWriter
	LogWriter_t4068659383 * ___m_logWriter_2;
	// System.String EDebug::DebugFilterStr
	String_t* ___DebugFilterStr_3;
	// System.UInt64 EDebug::index
	uint64_t ___index_4;

public:
	inline static int32_t get_offset_of_CurrentLogLevels_1() { return static_cast<int32_t>(offsetof(EDebug_t2041317646_StaticFields, ___CurrentLogLevels_1)); }
	inline int32_t get_CurrentLogLevels_1() const { return ___CurrentLogLevels_1; }
	inline int32_t* get_address_of_CurrentLogLevels_1() { return &___CurrentLogLevels_1; }
	inline void set_CurrentLogLevels_1(int32_t value)
	{
		___CurrentLogLevels_1 = value;
	}

	inline static int32_t get_offset_of_m_logWriter_2() { return static_cast<int32_t>(offsetof(EDebug_t2041317646_StaticFields, ___m_logWriter_2)); }
	inline LogWriter_t4068659383 * get_m_logWriter_2() const { return ___m_logWriter_2; }
	inline LogWriter_t4068659383 ** get_address_of_m_logWriter_2() { return &___m_logWriter_2; }
	inline void set_m_logWriter_2(LogWriter_t4068659383 * value)
	{
		___m_logWriter_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_logWriter_2, value);
	}

	inline static int32_t get_offset_of_DebugFilterStr_3() { return static_cast<int32_t>(offsetof(EDebug_t2041317646_StaticFields, ___DebugFilterStr_3)); }
	inline String_t* get_DebugFilterStr_3() const { return ___DebugFilterStr_3; }
	inline String_t** get_address_of_DebugFilterStr_3() { return &___DebugFilterStr_3; }
	inline void set_DebugFilterStr_3(String_t* value)
	{
		___DebugFilterStr_3 = value;
		Il2CppCodeGenWriteBarrier(&___DebugFilterStr_3, value);
	}

	inline static int32_t get_offset_of_index_4() { return static_cast<int32_t>(offsetof(EDebug_t2041317646_StaticFields, ___index_4)); }
	inline uint64_t get_index_4() const { return ___index_4; }
	inline uint64_t* get_address_of_index_4() { return &___index_4; }
	inline void set_index_4(uint64_t value)
	{
		___index_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
