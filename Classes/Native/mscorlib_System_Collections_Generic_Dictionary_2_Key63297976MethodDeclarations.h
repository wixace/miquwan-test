﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Int32>
struct KeyCollection_t63297976;
// System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>
struct Dictionary_2_t2731505821;
// System.Collections.Generic.IEnumerator`1<Pathfinding.Int3>
struct IEnumerator_1_t3885910643;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Pathfinding.Int3[]
struct Int3U5BU5D_t516284607;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3346441875.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m3434533733_gshared (KeyCollection_t63297976 * __this, Dictionary_2_t2731505821 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m3434533733(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t63297976 *, Dictionary_2_t2731505821 *, const MethodInfo*))KeyCollection__ctor_m3434533733_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Int32>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4011277009_gshared (KeyCollection_t63297976 * __this, Int3_t1974045594  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4011277009(__this, ___item0, method) ((  void (*) (KeyCollection_t63297976 *, Int3_t1974045594 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4011277009_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Int32>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m652206216_gshared (KeyCollection_t63297976 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m652206216(__this, method) ((  void (*) (KeyCollection_t63297976 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m652206216_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Int32>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1256134489_gshared (KeyCollection_t63297976 * __this, Int3_t1974045594  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1256134489(__this, ___item0, method) ((  bool (*) (KeyCollection_t63297976 *, Int3_t1974045594 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1256134489_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Int32>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1619757822_gshared (KeyCollection_t63297976 * __this, Int3_t1974045594  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1619757822(__this, ___item0, method) ((  bool (*) (KeyCollection_t63297976 *, Int3_t1974045594 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1619757822_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Int32>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3028649220_gshared (KeyCollection_t63297976 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3028649220(__this, method) ((  Il2CppObject* (*) (KeyCollection_t63297976 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3028649220_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3960289530_gshared (KeyCollection_t63297976 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m3960289530(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t63297976 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3960289530_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m877110645_gshared (KeyCollection_t63297976 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m877110645(__this, method) ((  Il2CppObject * (*) (KeyCollection_t63297976 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m877110645_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Int32>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3140492410_gshared (KeyCollection_t63297976 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3140492410(__this, method) ((  bool (*) (KeyCollection_t63297976 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3140492410_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m257594796_gshared (KeyCollection_t63297976 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m257594796(__this, method) ((  bool (*) (KeyCollection_t63297976 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m257594796_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2087774744_gshared (KeyCollection_t63297976 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2087774744(__this, method) ((  Il2CppObject * (*) (KeyCollection_t63297976 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2087774744_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Int32>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3840085530_gshared (KeyCollection_t63297976 * __this, Int3U5BU5D_t516284607* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m3840085530(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t63297976 *, Int3U5BU5D_t516284607*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3840085530_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t3346441875  KeyCollection_GetEnumerator_m3377297469_gshared (KeyCollection_t63297976 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m3377297469(__this, method) ((  Enumerator_t3346441875  (*) (KeyCollection_t63297976 *, const MethodInfo*))KeyCollection_GetEnumerator_m3377297469_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Int32>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m2969232882_gshared (KeyCollection_t63297976 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m2969232882(__this, method) ((  int32_t (*) (KeyCollection_t63297976 *, const MethodInfo*))KeyCollection_get_Count_m2969232882_gshared)(__this, method)
