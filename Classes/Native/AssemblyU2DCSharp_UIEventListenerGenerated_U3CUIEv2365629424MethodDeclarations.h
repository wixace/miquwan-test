﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIEventListenerGenerated/<UIEventListener_onDoubleClick_GetDelegate_member3_arg0>c__AnonStoreyB7
struct U3CUIEventListener_onDoubleClick_GetDelegate_member3_arg0U3Ec__AnonStoreyB7_t2365629424;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UIEventListenerGenerated/<UIEventListener_onDoubleClick_GetDelegate_member3_arg0>c__AnonStoreyB7::.ctor()
extern "C"  void U3CUIEventListener_onDoubleClick_GetDelegate_member3_arg0U3Ec__AnonStoreyB7__ctor_m1302986923 (U3CUIEventListener_onDoubleClick_GetDelegate_member3_arg0U3Ec__AnonStoreyB7_t2365629424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated/<UIEventListener_onDoubleClick_GetDelegate_member3_arg0>c__AnonStoreyB7::<>m__132(UnityEngine.GameObject)
extern "C"  void U3CUIEventListener_onDoubleClick_GetDelegate_member3_arg0U3Ec__AnonStoreyB7_U3CU3Em__132_m2239806198 (U3CUIEventListener_onDoubleClick_GetDelegate_member3_arg0U3Ec__AnonStoreyB7_t2365629424 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
