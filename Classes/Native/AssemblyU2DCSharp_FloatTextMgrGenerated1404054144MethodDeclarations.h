﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FloatTextMgrGenerated
struct FloatTextMgrGenerated_t1404054144;
// JSVCall
struct JSVCall_t3708497963;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_FloatTextMgr630384591.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void FloatTextMgrGenerated::.ctor()
extern "C"  void FloatTextMgrGenerated__ctor_m1191091947 (FloatTextMgrGenerated_t1404054144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FloatTextMgrGenerated::FloatTextMgr_FloatTextMgr1(JSVCall,System.Int32)
extern "C"  bool FloatTextMgrGenerated_FloatTextMgr_FloatTextMgr1_m3416476167 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextMgrGenerated::FloatTextMgr_FLOAT_TEXT_PATH(JSVCall)
extern "C"  void FloatTextMgrGenerated_FloatTextMgr_FLOAT_TEXT_PATH_m2753164778 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FloatTextMgrGenerated::FloatTextMgr_Clear(JSVCall,System.Int32)
extern "C"  bool FloatTextMgrGenerated_FloatTextMgr_Clear_m1217716146 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FloatTextMgrGenerated::FloatTextMgr_ClearTeamTips(JSVCall,System.Int32)
extern "C"  bool FloatTextMgrGenerated_FloatTextMgr_ClearTeamTips_m2364753991 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FloatTextMgrGenerated::FloatTextMgr_FloatText__FLOAT_TEXT_ID__Vector3__Object_Array(JSVCall,System.Int32)
extern "C"  bool FloatTextMgrGenerated_FloatTextMgr_FloatText__FLOAT_TEXT_ID__Vector3__Object_Array_m1782453197 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FloatTextMgrGenerated::FloatTextMgr_FloatText__FLOAT_TEXT_ID__Int32__Object_Array(JSVCall,System.Int32)
extern "C"  bool FloatTextMgrGenerated_FloatTextMgr_FloatText__FLOAT_TEXT_ID__Int32__Object_Array_m922354891 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextMgrGenerated::__Register()
extern "C"  void FloatTextMgrGenerated___Register_m4229462204 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] FloatTextMgrGenerated::<FloatTextMgr_FloatText__FLOAT_TEXT_ID__Vector3__Object_Array>m__56()
extern "C"  ObjectU5BU5D_t1108656482* FloatTextMgrGenerated_U3CFloatTextMgr_FloatText__FLOAT_TEXT_ID__Vector3__Object_ArrayU3Em__56_m1849755712 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] FloatTextMgrGenerated::<FloatTextMgr_FloatText__FLOAT_TEXT_ID__Int32__Object_Array>m__57()
extern "C"  ObjectU5BU5D_t1108656482* FloatTextMgrGenerated_U3CFloatTextMgr_FloatText__FLOAT_TEXT_ID__Int32__Object_ArrayU3Em__57_m3575544383 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextMgrGenerated::ilo_Clear1(FloatTextMgr)
extern "C"  void FloatTextMgrGenerated_ilo_Clear1_m3500767297 (Il2CppObject * __this /* static, unused */, FloatTextMgr_t630384591 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FloatTextMgrGenerated::ilo_getEnum2(System.Int32)
extern "C"  int32_t FloatTextMgrGenerated_ilo_getEnum2_m167696554 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 FloatTextMgrGenerated::ilo_getVector3S3(System.Int32)
extern "C"  Vector3_t4282066566  FloatTextMgrGenerated_ilo_getVector3S3_m1595878717 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FloatTextMgrGenerated::ilo_getInt324(System.Int32)
extern "C"  int32_t FloatTextMgrGenerated_ilo_getInt324_m2887869521 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextMgrGenerated::ilo_FloatText5(FloatTextMgr,FLOAT_TEXT_ID,System.Int32,System.Object[])
extern "C"  void FloatTextMgrGenerated_ilo_FloatText5_m2172154424 (Il2CppObject * __this /* static, unused */, FloatTextMgr_t630384591 * ____this0, int32_t ___textId1, int32_t ___offsetY2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FloatTextMgrGenerated::ilo_getArrayLength6(System.Int32)
extern "C"  int32_t FloatTextMgrGenerated_ilo_getArrayLength6_m2315389410 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FloatTextMgrGenerated::ilo_getElement7(System.Int32,System.Int32)
extern "C"  int32_t FloatTextMgrGenerated_ilo_getElement7_m1005495281 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FloatTextMgrGenerated::ilo_getWhatever8(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * FloatTextMgrGenerated_ilo_getWhatever8_m1358991902 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FloatTextMgrGenerated::ilo_getObject9(System.Int32)
extern "C"  int32_t FloatTextMgrGenerated_ilo_getObject9_m485570739 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
