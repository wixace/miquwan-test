﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RemoveEffect
struct RemoveEffect_t3191766869;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void RemoveEffect::.ctor(System.String,System.Int32,System.Int32)
extern "C"  void RemoveEffect__ctor_m1759386684 (RemoveEffect_t3191766869 * __this, String_t* ___type0, int32_t ___id1, int32_t ___instanceID2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RemoveEffect::.ctor(System.String)
extern "C"  void RemoveEffect__ctor_m1830363612 (RemoveEffect_t3191766869 * __this, String_t* ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
