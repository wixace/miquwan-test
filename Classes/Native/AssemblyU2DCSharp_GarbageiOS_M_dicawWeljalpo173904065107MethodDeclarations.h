﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_dicawWeljalpo173
struct M_dicawWeljalpo173_t904065107;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_dicawWeljalpo173::.ctor()
extern "C"  void M_dicawWeljalpo173__ctor_m3250535232 (M_dicawWeljalpo173_t904065107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_dicawWeljalpo173::M_jallkelsa0(System.String[],System.Int32)
extern "C"  void M_dicawWeljalpo173_M_jallkelsa0_m1341747540 (M_dicawWeljalpo173_t904065107 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
