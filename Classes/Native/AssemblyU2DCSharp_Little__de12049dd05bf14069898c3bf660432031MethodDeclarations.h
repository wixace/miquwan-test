﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._de12049dd05bf14069898c3bfc1269b8
struct _de12049dd05bf14069898c3bfc1269b8_t660432031;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._de12049dd05bf14069898c3bfc1269b8::.ctor()
extern "C"  void _de12049dd05bf14069898c3bfc1269b8__ctor_m4141057134 (_de12049dd05bf14069898c3bfc1269b8_t660432031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._de12049dd05bf14069898c3bfc1269b8::_de12049dd05bf14069898c3bfc1269b8m2(System.Int32)
extern "C"  int32_t _de12049dd05bf14069898c3bfc1269b8__de12049dd05bf14069898c3bfc1269b8m2_m2372752697 (_de12049dd05bf14069898c3bfc1269b8_t660432031 * __this, int32_t ____de12049dd05bf14069898c3bfc1269b8a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._de12049dd05bf14069898c3bfc1269b8::_de12049dd05bf14069898c3bfc1269b8m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _de12049dd05bf14069898c3bfc1269b8__de12049dd05bf14069898c3bfc1269b8m_m1294771613 (_de12049dd05bf14069898c3bfc1269b8_t660432031 * __this, int32_t ____de12049dd05bf14069898c3bfc1269b8a0, int32_t ____de12049dd05bf14069898c3bfc1269b8281, int32_t ____de12049dd05bf14069898c3bfc1269b8c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
