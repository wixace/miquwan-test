﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FingerEventDetector`1<System.Object>
struct FingerEventDetector_1_t3103176768;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// FingerGestures/Finger
struct Finger_t182428197;
// FingerGestures
struct FingerGestures_t2907604723;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FingerGestures_Finger182428197.h"

// System.Void FingerEventDetector`1<System.Object>::.ctor()
extern "C"  void FingerEventDetector_1__ctor_m3763465287_gshared (FingerEventDetector_1_t3103176768 * __this, const MethodInfo* method);
#define FingerEventDetector_1__ctor_m3763465287(__this, method) ((  void (*) (FingerEventDetector_1_t3103176768 *, const MethodInfo*))FingerEventDetector_1__ctor_m3763465287_gshared)(__this, method)
// T FingerEventDetector`1<System.Object>::CreateFingerEvent()
extern "C"  Il2CppObject * FingerEventDetector_1_CreateFingerEvent_m1420222875_gshared (FingerEventDetector_1_t3103176768 * __this, const MethodInfo* method);
#define FingerEventDetector_1_CreateFingerEvent_m1420222875(__this, method) ((  Il2CppObject * (*) (FingerEventDetector_1_t3103176768 *, const MethodInfo*))FingerEventDetector_1_CreateFingerEvent_m1420222875_gshared)(__this, method)
// System.Type FingerEventDetector`1<System.Object>::GetEventType()
extern "C"  Type_t * FingerEventDetector_1_GetEventType_m3851890933_gshared (FingerEventDetector_1_t3103176768 * __this, const MethodInfo* method);
#define FingerEventDetector_1_GetEventType_m3851890933(__this, method) ((  Type_t * (*) (FingerEventDetector_1_t3103176768 *, const MethodInfo*))FingerEventDetector_1_GetEventType_m3851890933_gshared)(__this, method)
// System.Void FingerEventDetector`1<System.Object>::Start()
extern "C"  void FingerEventDetector_1_Start_m2710603079_gshared (FingerEventDetector_1_t3103176768 * __this, const MethodInfo* method);
#define FingerEventDetector_1_Start_m2710603079(__this, method) ((  void (*) (FingerEventDetector_1_t3103176768 *, const MethodInfo*))FingerEventDetector_1_Start_m2710603079_gshared)(__this, method)
// System.Void FingerEventDetector`1<System.Object>::OnDestroy()
extern "C"  void FingerEventDetector_1_OnDestroy_m3778430400_gshared (FingerEventDetector_1_t3103176768 * __this, const MethodInfo* method);
#define FingerEventDetector_1_OnDestroy_m3778430400(__this, method) ((  void (*) (FingerEventDetector_1_t3103176768 *, const MethodInfo*))FingerEventDetector_1_OnDestroy_m3778430400_gshared)(__this, method)
// System.Void FingerEventDetector`1<System.Object>::FingerGestures_OnInputProviderChanged()
extern "C"  void FingerEventDetector_1_FingerGestures_OnInputProviderChanged_m572190281_gshared (FingerEventDetector_1_t3103176768 * __this, const MethodInfo* method);
#define FingerEventDetector_1_FingerGestures_OnInputProviderChanged_m572190281(__this, method) ((  void (*) (FingerEventDetector_1_t3103176768 *, const MethodInfo*))FingerEventDetector_1_FingerGestures_OnInputProviderChanged_m572190281_gshared)(__this, method)
// System.Void FingerEventDetector`1<System.Object>::Init()
extern "C"  void FingerEventDetector_1_Init_m2289696045_gshared (FingerEventDetector_1_t3103176768 * __this, const MethodInfo* method);
#define FingerEventDetector_1_Init_m2289696045(__this, method) ((  void (*) (FingerEventDetector_1_t3103176768 *, const MethodInfo*))FingerEventDetector_1_Init_m2289696045_gshared)(__this, method)
// System.Void FingerEventDetector`1<System.Object>::Init(System.Int32)
extern "C"  void FingerEventDetector_1_Init_m469522174_gshared (FingerEventDetector_1_t3103176768 * __this, int32_t ___fingersCount0, const MethodInfo* method);
#define FingerEventDetector_1_Init_m469522174(__this, ___fingersCount0, method) ((  void (*) (FingerEventDetector_1_t3103176768 *, int32_t, const MethodInfo*))FingerEventDetector_1_Init_m469522174_gshared)(__this, ___fingersCount0, method)
// T FingerEventDetector`1<System.Object>::GetEvent(FingerGestures/Finger)
extern "C"  Il2CppObject * FingerEventDetector_1_GetEvent_m906084269_gshared (FingerEventDetector_1_t3103176768 * __this, Finger_t182428197 * ___finger0, const MethodInfo* method);
#define FingerEventDetector_1_GetEvent_m906084269(__this, ___finger0, method) ((  Il2CppObject * (*) (FingerEventDetector_1_t3103176768 *, Finger_t182428197 *, const MethodInfo*))FingerEventDetector_1_GetEvent_m906084269_gshared)(__this, ___finger0, method)
// T FingerEventDetector`1<System.Object>::GetEvent(System.Int32)
extern "C"  Il2CppObject * FingerEventDetector_1_GetEvent_m3248170193_gshared (FingerEventDetector_1_t3103176768 * __this, int32_t ___fingerIndex0, const MethodInfo* method);
#define FingerEventDetector_1_GetEvent_m3248170193(__this, ___fingerIndex0, method) ((  Il2CppObject * (*) (FingerEventDetector_1_t3103176768 *, int32_t, const MethodInfo*))FingerEventDetector_1_GetEvent_m3248170193_gshared)(__this, ___fingerIndex0, method)
// FingerGestures FingerEventDetector`1<System.Object>::ilo_get_Instance1()
extern "C"  FingerGestures_t2907604723 * FingerEventDetector_1_ilo_get_Instance1_m1581142907_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define FingerEventDetector_1_ilo_get_Instance1_m1581142907(__this /* static, unused */, method) ((  FingerGestures_t2907604723 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))FingerEventDetector_1_ilo_get_Instance1_m1581142907_gshared)(__this /* static, unused */, method)
// FingerGestures/Finger FingerEventDetector`1<System.Object>::ilo_GetFinger2(System.Int32)
extern "C"  Finger_t182428197 * FingerEventDetector_1_ilo_GetFinger2_m3652595042_gshared (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method);
#define FingerEventDetector_1_ilo_GetFinger2_m3652595042(__this /* static, unused */, ___index0, method) ((  Finger_t182428197 * (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))FingerEventDetector_1_ilo_GetFinger2_m3652595042_gshared)(__this /* static, unused */, ___index0, method)
