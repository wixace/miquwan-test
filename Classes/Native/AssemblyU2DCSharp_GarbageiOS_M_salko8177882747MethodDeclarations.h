﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_salko81
struct M_salko81_t77882747;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_salko81::.ctor()
extern "C"  void M_salko81__ctor_m4185381960 (M_salko81_t77882747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_salko81::M_niroustaJaymar0(System.String[],System.Int32)
extern "C"  void M_salko81_M_niroustaJaymar0_m726091644 (M_salko81_t77882747 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
