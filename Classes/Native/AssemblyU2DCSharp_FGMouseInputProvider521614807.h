﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t4024180168;

#include "AssemblyU2DCSharp_FGInputProvider1238597786.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FGMouseInputProvider
struct  FGMouseInputProvider_t521614807  : public FGInputProvider_t1238597786
{
public:
	// System.Int32 FGMouseInputProvider::maxButtons
	int32_t ___maxButtons_2;
	// System.String FGMouseInputProvider::pinchAxis
	String_t* ___pinchAxis_3;
	// System.Single FGMouseInputProvider::pinchAxisScale
	float ___pinchAxisScale_4;
	// System.Single FGMouseInputProvider::pinchResetTimeDelay
	float ___pinchResetTimeDelay_5;
	// System.Single FGMouseInputProvider::initialPinchDistance
	float ___initialPinchDistance_6;
	// System.String FGMouseInputProvider::twistAxis
	String_t* ___twistAxis_7;
	// System.Single FGMouseInputProvider::twistAxisScale
	float ___twistAxisScale_8;
	// UnityEngine.KeyCode FGMouseInputProvider::twistKey
	int32_t ___twistKey_9;
	// System.Single FGMouseInputProvider::twistResetTimeDelay
	float ___twistResetTimeDelay_10;
	// UnityEngine.KeyCode FGMouseInputProvider::pivotKey
	int32_t ___pivotKey_11;
	// System.Boolean FGMouseInputProvider::pivoting
	bool ___pivoting_12;
	// UnityEngine.KeyCode FGMouseInputProvider::twistAndPinchKey
	int32_t ___twistAndPinchKey_13;
	// UnityEngine.Vector2 FGMouseInputProvider::pivot
	Vector2_t4282066565  ___pivot_14;
	// UnityEngine.Vector2[] FGMouseInputProvider::pos
	Vector2U5BU5D_t4024180168* ___pos_15;
	// System.Boolean FGMouseInputProvider::pinching
	bool ___pinching_16;
	// System.Single FGMouseInputProvider::pinchResetTime
	float ___pinchResetTime_17;
	// System.Single FGMouseInputProvider::pinchDistance
	float ___pinchDistance_18;
	// System.Boolean FGMouseInputProvider::twisting
	bool ___twisting_19;
	// System.Single FGMouseInputProvider::twistAngle
	float ___twistAngle_20;
	// System.Single FGMouseInputProvider::twistResetTime
	float ___twistResetTime_21;

public:
	inline static int32_t get_offset_of_maxButtons_2() { return static_cast<int32_t>(offsetof(FGMouseInputProvider_t521614807, ___maxButtons_2)); }
	inline int32_t get_maxButtons_2() const { return ___maxButtons_2; }
	inline int32_t* get_address_of_maxButtons_2() { return &___maxButtons_2; }
	inline void set_maxButtons_2(int32_t value)
	{
		___maxButtons_2 = value;
	}

	inline static int32_t get_offset_of_pinchAxis_3() { return static_cast<int32_t>(offsetof(FGMouseInputProvider_t521614807, ___pinchAxis_3)); }
	inline String_t* get_pinchAxis_3() const { return ___pinchAxis_3; }
	inline String_t** get_address_of_pinchAxis_3() { return &___pinchAxis_3; }
	inline void set_pinchAxis_3(String_t* value)
	{
		___pinchAxis_3 = value;
		Il2CppCodeGenWriteBarrier(&___pinchAxis_3, value);
	}

	inline static int32_t get_offset_of_pinchAxisScale_4() { return static_cast<int32_t>(offsetof(FGMouseInputProvider_t521614807, ___pinchAxisScale_4)); }
	inline float get_pinchAxisScale_4() const { return ___pinchAxisScale_4; }
	inline float* get_address_of_pinchAxisScale_4() { return &___pinchAxisScale_4; }
	inline void set_pinchAxisScale_4(float value)
	{
		___pinchAxisScale_4 = value;
	}

	inline static int32_t get_offset_of_pinchResetTimeDelay_5() { return static_cast<int32_t>(offsetof(FGMouseInputProvider_t521614807, ___pinchResetTimeDelay_5)); }
	inline float get_pinchResetTimeDelay_5() const { return ___pinchResetTimeDelay_5; }
	inline float* get_address_of_pinchResetTimeDelay_5() { return &___pinchResetTimeDelay_5; }
	inline void set_pinchResetTimeDelay_5(float value)
	{
		___pinchResetTimeDelay_5 = value;
	}

	inline static int32_t get_offset_of_initialPinchDistance_6() { return static_cast<int32_t>(offsetof(FGMouseInputProvider_t521614807, ___initialPinchDistance_6)); }
	inline float get_initialPinchDistance_6() const { return ___initialPinchDistance_6; }
	inline float* get_address_of_initialPinchDistance_6() { return &___initialPinchDistance_6; }
	inline void set_initialPinchDistance_6(float value)
	{
		___initialPinchDistance_6 = value;
	}

	inline static int32_t get_offset_of_twistAxis_7() { return static_cast<int32_t>(offsetof(FGMouseInputProvider_t521614807, ___twistAxis_7)); }
	inline String_t* get_twistAxis_7() const { return ___twistAxis_7; }
	inline String_t** get_address_of_twistAxis_7() { return &___twistAxis_7; }
	inline void set_twistAxis_7(String_t* value)
	{
		___twistAxis_7 = value;
		Il2CppCodeGenWriteBarrier(&___twistAxis_7, value);
	}

	inline static int32_t get_offset_of_twistAxisScale_8() { return static_cast<int32_t>(offsetof(FGMouseInputProvider_t521614807, ___twistAxisScale_8)); }
	inline float get_twistAxisScale_8() const { return ___twistAxisScale_8; }
	inline float* get_address_of_twistAxisScale_8() { return &___twistAxisScale_8; }
	inline void set_twistAxisScale_8(float value)
	{
		___twistAxisScale_8 = value;
	}

	inline static int32_t get_offset_of_twistKey_9() { return static_cast<int32_t>(offsetof(FGMouseInputProvider_t521614807, ___twistKey_9)); }
	inline int32_t get_twistKey_9() const { return ___twistKey_9; }
	inline int32_t* get_address_of_twistKey_9() { return &___twistKey_9; }
	inline void set_twistKey_9(int32_t value)
	{
		___twistKey_9 = value;
	}

	inline static int32_t get_offset_of_twistResetTimeDelay_10() { return static_cast<int32_t>(offsetof(FGMouseInputProvider_t521614807, ___twistResetTimeDelay_10)); }
	inline float get_twistResetTimeDelay_10() const { return ___twistResetTimeDelay_10; }
	inline float* get_address_of_twistResetTimeDelay_10() { return &___twistResetTimeDelay_10; }
	inline void set_twistResetTimeDelay_10(float value)
	{
		___twistResetTimeDelay_10 = value;
	}

	inline static int32_t get_offset_of_pivotKey_11() { return static_cast<int32_t>(offsetof(FGMouseInputProvider_t521614807, ___pivotKey_11)); }
	inline int32_t get_pivotKey_11() const { return ___pivotKey_11; }
	inline int32_t* get_address_of_pivotKey_11() { return &___pivotKey_11; }
	inline void set_pivotKey_11(int32_t value)
	{
		___pivotKey_11 = value;
	}

	inline static int32_t get_offset_of_pivoting_12() { return static_cast<int32_t>(offsetof(FGMouseInputProvider_t521614807, ___pivoting_12)); }
	inline bool get_pivoting_12() const { return ___pivoting_12; }
	inline bool* get_address_of_pivoting_12() { return &___pivoting_12; }
	inline void set_pivoting_12(bool value)
	{
		___pivoting_12 = value;
	}

	inline static int32_t get_offset_of_twistAndPinchKey_13() { return static_cast<int32_t>(offsetof(FGMouseInputProvider_t521614807, ___twistAndPinchKey_13)); }
	inline int32_t get_twistAndPinchKey_13() const { return ___twistAndPinchKey_13; }
	inline int32_t* get_address_of_twistAndPinchKey_13() { return &___twistAndPinchKey_13; }
	inline void set_twistAndPinchKey_13(int32_t value)
	{
		___twistAndPinchKey_13 = value;
	}

	inline static int32_t get_offset_of_pivot_14() { return static_cast<int32_t>(offsetof(FGMouseInputProvider_t521614807, ___pivot_14)); }
	inline Vector2_t4282066565  get_pivot_14() const { return ___pivot_14; }
	inline Vector2_t4282066565 * get_address_of_pivot_14() { return &___pivot_14; }
	inline void set_pivot_14(Vector2_t4282066565  value)
	{
		___pivot_14 = value;
	}

	inline static int32_t get_offset_of_pos_15() { return static_cast<int32_t>(offsetof(FGMouseInputProvider_t521614807, ___pos_15)); }
	inline Vector2U5BU5D_t4024180168* get_pos_15() const { return ___pos_15; }
	inline Vector2U5BU5D_t4024180168** get_address_of_pos_15() { return &___pos_15; }
	inline void set_pos_15(Vector2U5BU5D_t4024180168* value)
	{
		___pos_15 = value;
		Il2CppCodeGenWriteBarrier(&___pos_15, value);
	}

	inline static int32_t get_offset_of_pinching_16() { return static_cast<int32_t>(offsetof(FGMouseInputProvider_t521614807, ___pinching_16)); }
	inline bool get_pinching_16() const { return ___pinching_16; }
	inline bool* get_address_of_pinching_16() { return &___pinching_16; }
	inline void set_pinching_16(bool value)
	{
		___pinching_16 = value;
	}

	inline static int32_t get_offset_of_pinchResetTime_17() { return static_cast<int32_t>(offsetof(FGMouseInputProvider_t521614807, ___pinchResetTime_17)); }
	inline float get_pinchResetTime_17() const { return ___pinchResetTime_17; }
	inline float* get_address_of_pinchResetTime_17() { return &___pinchResetTime_17; }
	inline void set_pinchResetTime_17(float value)
	{
		___pinchResetTime_17 = value;
	}

	inline static int32_t get_offset_of_pinchDistance_18() { return static_cast<int32_t>(offsetof(FGMouseInputProvider_t521614807, ___pinchDistance_18)); }
	inline float get_pinchDistance_18() const { return ___pinchDistance_18; }
	inline float* get_address_of_pinchDistance_18() { return &___pinchDistance_18; }
	inline void set_pinchDistance_18(float value)
	{
		___pinchDistance_18 = value;
	}

	inline static int32_t get_offset_of_twisting_19() { return static_cast<int32_t>(offsetof(FGMouseInputProvider_t521614807, ___twisting_19)); }
	inline bool get_twisting_19() const { return ___twisting_19; }
	inline bool* get_address_of_twisting_19() { return &___twisting_19; }
	inline void set_twisting_19(bool value)
	{
		___twisting_19 = value;
	}

	inline static int32_t get_offset_of_twistAngle_20() { return static_cast<int32_t>(offsetof(FGMouseInputProvider_t521614807, ___twistAngle_20)); }
	inline float get_twistAngle_20() const { return ___twistAngle_20; }
	inline float* get_address_of_twistAngle_20() { return &___twistAngle_20; }
	inline void set_twistAngle_20(float value)
	{
		___twistAngle_20 = value;
	}

	inline static int32_t get_offset_of_twistResetTime_21() { return static_cast<int32_t>(offsetof(FGMouseInputProvider_t521614807, ___twistResetTime_21)); }
	inline float get_twistResetTime_21() const { return ___twistResetTime_21; }
	inline float* get_address_of_twistResetTime_21() { return &___twistResetTime_21; }
	inline void set_twistResetTime_21(float value)
	{
		___twistResetTime_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
