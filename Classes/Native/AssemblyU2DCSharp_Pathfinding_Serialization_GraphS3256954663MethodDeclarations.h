﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Serialization.GraphSerializationContext
struct GraphSerializationContext_t3256954663;
// System.IO.BinaryReader
struct BinaryReader_t3990958868;
// Pathfinding.GraphNode[]
struct GraphNodeU5BU5D_t927449255;
// System.IO.BinaryWriter
struct BinaryWriter_t4146364100;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_BinaryReader3990958868.h"
#include "mscorlib_System_IO_BinaryWriter4146364100.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.Serialization.GraphSerializationContext::.ctor(System.IO.BinaryReader,Pathfinding.GraphNode[],System.Int32)
extern "C"  void GraphSerializationContext__ctor_m1175840698 (GraphSerializationContext_t3256954663 * __this, BinaryReader_t3990958868 * ___reader0, GraphNodeU5BU5D_t927449255* ___id2NodeMapping1, int32_t ___graphIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.GraphSerializationContext::.ctor(System.IO.BinaryWriter)
extern "C"  void GraphSerializationContext__ctor_m2278175177 (GraphSerializationContext_t3256954663 * __this, BinaryWriter_t4146364100 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Serialization.GraphSerializationContext::GetNodeIdentifier(Pathfinding.GraphNode)
extern "C"  int32_t GraphSerializationContext_GetNodeIdentifier_m4079208217 (GraphSerializationContext_t3256954663 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.GraphNode Pathfinding.Serialization.GraphSerializationContext::GetNodeFromIdentifier(System.Int32)
extern "C"  GraphNode_t23612370 * GraphSerializationContext_GetNodeFromIdentifier_m2490173645 (GraphSerializationContext_t3256954663 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
