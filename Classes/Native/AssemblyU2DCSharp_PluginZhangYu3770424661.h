﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginZhangYu
struct  PluginZhangYu_t3770424661  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean PluginZhangYu::isOut
	bool ___isOut_2;
	// System.String PluginZhangYu::uid
	String_t* ___uid_3;
	// System.String PluginZhangYu::token
	String_t* ___token_4;
	// System.String[] PluginZhangYu::sdkInfos
	StringU5BU5D_t4054002952* ___sdkInfos_5;
	// System.String PluginZhangYu::configId
	String_t* ___configId_6;

public:
	inline static int32_t get_offset_of_isOut_2() { return static_cast<int32_t>(offsetof(PluginZhangYu_t3770424661, ___isOut_2)); }
	inline bool get_isOut_2() const { return ___isOut_2; }
	inline bool* get_address_of_isOut_2() { return &___isOut_2; }
	inline void set_isOut_2(bool value)
	{
		___isOut_2 = value;
	}

	inline static int32_t get_offset_of_uid_3() { return static_cast<int32_t>(offsetof(PluginZhangYu_t3770424661, ___uid_3)); }
	inline String_t* get_uid_3() const { return ___uid_3; }
	inline String_t** get_address_of_uid_3() { return &___uid_3; }
	inline void set_uid_3(String_t* value)
	{
		___uid_3 = value;
		Il2CppCodeGenWriteBarrier(&___uid_3, value);
	}

	inline static int32_t get_offset_of_token_4() { return static_cast<int32_t>(offsetof(PluginZhangYu_t3770424661, ___token_4)); }
	inline String_t* get_token_4() const { return ___token_4; }
	inline String_t** get_address_of_token_4() { return &___token_4; }
	inline void set_token_4(String_t* value)
	{
		___token_4 = value;
		Il2CppCodeGenWriteBarrier(&___token_4, value);
	}

	inline static int32_t get_offset_of_sdkInfos_5() { return static_cast<int32_t>(offsetof(PluginZhangYu_t3770424661, ___sdkInfos_5)); }
	inline StringU5BU5D_t4054002952* get_sdkInfos_5() const { return ___sdkInfos_5; }
	inline StringU5BU5D_t4054002952** get_address_of_sdkInfos_5() { return &___sdkInfos_5; }
	inline void set_sdkInfos_5(StringU5BU5D_t4054002952* value)
	{
		___sdkInfos_5 = value;
		Il2CppCodeGenWriteBarrier(&___sdkInfos_5, value);
	}

	inline static int32_t get_offset_of_configId_6() { return static_cast<int32_t>(offsetof(PluginZhangYu_t3770424661, ___configId_6)); }
	inline String_t* get_configId_6() const { return ___configId_6; }
	inline String_t** get_address_of_configId_6() { return &___configId_6; }
	inline void set_configId_6(String_t* value)
	{
		___configId_6 = value;
		Il2CppCodeGenWriteBarrier(&___configId_6, value);
	}
};

struct PluginZhangYu_t3770424661_StaticFields
{
public:
	// System.Action PluginZhangYu::<>f__am$cache5
	Action_t3771233898 * ___U3CU3Ef__amU24cache5_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_7() { return static_cast<int32_t>(offsetof(PluginZhangYu_t3770424661_StaticFields, ___U3CU3Ef__amU24cache5_7)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache5_7() const { return ___U3CU3Ef__amU24cache5_7; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache5_7() { return &___U3CU3Ef__amU24cache5_7; }
	inline void set_U3CU3Ef__amU24cache5_7(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache5_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
