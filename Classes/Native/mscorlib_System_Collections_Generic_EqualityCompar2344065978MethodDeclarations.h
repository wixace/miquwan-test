﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<AIEnum.EAIEventtype>
struct DefaultComparer_t2344065978;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AIEnum_EAIEventtype3896289311.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<AIEnum.EAIEventtype>::.ctor()
extern "C"  void DefaultComparer__ctor_m204224618_gshared (DefaultComparer_t2344065978 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m204224618(__this, method) ((  void (*) (DefaultComparer_t2344065978 *, const MethodInfo*))DefaultComparer__ctor_m204224618_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<AIEnum.EAIEventtype>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1983760385_gshared (DefaultComparer_t2344065978 * __this, uint8_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1983760385(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2344065978 *, uint8_t, const MethodInfo*))DefaultComparer_GetHashCode_m1983760385_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<AIEnum.EAIEventtype>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1642354747_gshared (DefaultComparer_t2344065978 * __this, uint8_t ___x0, uint8_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1642354747(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2344065978 *, uint8_t, uint8_t, const MethodInfo*))DefaultComparer_Equals_m1642354747_gshared)(__this, ___x0, ___y1, method)
