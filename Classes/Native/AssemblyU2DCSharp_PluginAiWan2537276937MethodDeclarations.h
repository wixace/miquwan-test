﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginAiWan
struct PluginAiWan_t2537276937;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.String
struct String_t;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// VersionMgr
struct VersionMgr_t1322950208;
// System.Action
struct Action_t3771233898;
// VersionInfo
struct VersionInfo_t2356638086;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginAiWan2537276937.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void PluginAiWan::.ctor()
extern "C"  void PluginAiWan__ctor_m308506498 (PluginAiWan_t2537276937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiWan::Init()
extern "C"  void PluginAiWan_Init_m2732435090 (PluginAiWan_t2537276937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginAiWan::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginAiWan_ReqSDKHttpLogin_m3747508851 (PluginAiWan_t2537276937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginAiWan::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginAiWan_IsLoginSuccess_m2584901033 (PluginAiWan_t2537276937 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiWan::OpenUserLogin()
extern "C"  void PluginAiWan_OpenUserLogin_m805171156 (PluginAiWan_t2537276937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiWan::OnLogoutSuccess(System.String)
extern "C"  void PluginAiWan_OnLogoutSuccess_m2068919912 (PluginAiWan_t2537276937 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiWan::UserPay(CEvent.ZEvent)
extern "C"  void PluginAiWan_UserPay_m1579412766 (PluginAiWan_t2537276937 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiWan::OnLoginSuccess(System.String)
extern "C"  void PluginAiWan_OnLoginSuccess_m2976119047 (PluginAiWan_t2537276937 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiWan::OnLoginFaild(System.String)
extern "C"  void PluginAiWan_OnLoginFaild_m4257645540 (PluginAiWan_t2537276937 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiWan::OnRegisterSuccess(System.String)
extern "C"  void PluginAiWan_OnRegisterSuccess_m4292986561 (PluginAiWan_t2537276937 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiWan::OnRegisterFaild(System.String)
extern "C"  void PluginAiWan_OnRegisterFaild_m4008736798 (PluginAiWan_t2537276937 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiWan::OnPaySuccess(System.String)
extern "C"  void PluginAiWan_OnPaySuccess_m2033381638 (PluginAiWan_t2537276937 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiWan::OnPayFaild(System.String)
extern "C"  void PluginAiWan_OnPayFaild_m279015331 (PluginAiWan_t2537276937 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiWan::OnPayCancel(System.String)
extern "C"  void PluginAiWan_OnPayCancel_m3885409567 (PluginAiWan_t2537276937 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiWan::OnLoginout(System.String)
extern "C"  void PluginAiWan_OnLoginout_m129186492 (PluginAiWan_t2537276937 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiWan::init(System.String,System.String,System.String,System.String)
extern "C"  void PluginAiWan_init_m2315032452 (PluginAiWan_t2537276937 * __this, String_t* ___gameId0, String_t* ___gameKey1, String_t* ___chinnelId2, String_t* ___trackingAppKey3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiWan::login()
extern "C"  void PluginAiWan_login_m4125488617 (PluginAiWan_t2537276937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiWan::pay(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginAiWan_pay_m1038232710 (PluginAiWan_t2537276937 * __this, String_t* ___AWPayAmount0, String_t* ___product1, String_t* ___description2, String_t* ___CPOrderId3, String_t* ___IAP_productId4, String_t* ___extra5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiWan::<OnLogoutSuccess>m__40A()
extern "C"  void PluginAiWan_U3COnLogoutSuccessU3Em__40A_m1890129528 (PluginAiWan_t2537276937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiWan::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginAiWan_ilo_AddEventListener1_m646093106 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginAiWan::ilo_get_DeviceID2(PluginsSdkMgr)
extern "C"  String_t* PluginAiWan_ilo_get_DeviceID2_m557709548 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginAiWan::ilo_get_Instance3()
extern "C"  VersionMgr_t1322950208 * PluginAiWan_ilo_get_Instance3_m360786975 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiWan::ilo_Logout4(System.Action)
extern "C"  void PluginAiWan_ilo_Logout4_m512387302 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginAiWan::ilo_get_currentVS5(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginAiWan_ilo_get_currentVS5_m3373037360 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProductsCfgMgr PluginAiWan::ilo_get_ProductsCfgMgr6()
extern "C"  ProductsCfgMgr_t2493714872 * PluginAiWan_ilo_get_ProductsCfgMgr6_m3683894919 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiWan::ilo_pay7(PluginAiWan,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginAiWan_ilo_pay7_m1415369933 (Il2CppObject * __this /* static, unused */, PluginAiWan_t2537276937 * ____this0, String_t* ___AWPayAmount1, String_t* ___product2, String_t* ___description3, String_t* ___CPOrderId4, String_t* ___IAP_productId5, String_t* ___extra6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginAiWan::ilo_GetProductID8(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginAiWan_ilo_GetProductID8_m26368280 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiWan::ilo_Log9(System.Object,System.Boolean)
extern "C"  void PluginAiWan_ilo_Log9_m712755603 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiWan::ilo_DispatchEvent10(CEvent.ZEvent)
extern "C"  void PluginAiWan_ilo_DispatchEvent10_m2170197711 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiWan::ilo_OpenUserLogin11(PluginAiWan)
extern "C"  void PluginAiWan_ilo_OpenUserLogin11_m805936264 (Il2CppObject * __this /* static, unused */, PluginAiWan_t2537276937 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
