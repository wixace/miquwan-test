﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt32,System.Object>
struct ShimEnumerator_t3883744376;
// System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>
struct Dictionary_2_t4167966349;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1839974612_gshared (ShimEnumerator_t3883744376 * __this, Dictionary_2_t4167966349 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m1839974612(__this, ___host0, method) ((  void (*) (ShimEnumerator_t3883744376 *, Dictionary_2_t4167966349 *, const MethodInfo*))ShimEnumerator__ctor_m1839974612_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt32,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3963210957_gshared (ShimEnumerator_t3883744376 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m3963210957(__this, method) ((  bool (*) (ShimEnumerator_t3883744376 *, const MethodInfo*))ShimEnumerator_MoveNext_m3963210957_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt32,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m2926266535_gshared (ShimEnumerator_t3883744376 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2926266535(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t3883744376 *, const MethodInfo*))ShimEnumerator_get_Entry_m2926266535_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt32,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3180862018_gshared (ShimEnumerator_t3883744376 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m3180862018(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3883744376 *, const MethodInfo*))ShimEnumerator_get_Key_m3180862018_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt32,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m4135088212_gshared (ShimEnumerator_t3883744376 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m4135088212(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3883744376 *, const MethodInfo*))ShimEnumerator_get_Value_m4135088212_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt32,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2399713756_gshared (ShimEnumerator_t3883744376 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m2399713756(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3883744376 *, const MethodInfo*))ShimEnumerator_get_Current_m2399713756_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt32,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m1830184230_gshared (ShimEnumerator_t3883744376 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1830184230(__this, method) ((  void (*) (ShimEnumerator_t3883744376 *, const MethodInfo*))ShimEnumerator_Reset_m1830184230_gshared)(__this, method)
