﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._d62457a234dec5aae622cb6f2f24f145
struct _d62457a234dec5aae622cb6f2f24f145_t3043218550;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._d62457a234dec5aae622cb6f2f24f145::.ctor()
extern "C"  void _d62457a234dec5aae622cb6f2f24f145__ctor_m3511154295 (_d62457a234dec5aae622cb6f2f24f145_t3043218550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._d62457a234dec5aae622cb6f2f24f145::_d62457a234dec5aae622cb6f2f24f145m2(System.Int32)
extern "C"  int32_t _d62457a234dec5aae622cb6f2f24f145__d62457a234dec5aae622cb6f2f24f145m2_m1903300057 (_d62457a234dec5aae622cb6f2f24f145_t3043218550 * __this, int32_t ____d62457a234dec5aae622cb6f2f24f145a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._d62457a234dec5aae622cb6f2f24f145::_d62457a234dec5aae622cb6f2f24f145m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _d62457a234dec5aae622cb6f2f24f145__d62457a234dec5aae622cb6f2f24f145m_m2436555005 (_d62457a234dec5aae622cb6f2f24f145_t3043218550 * __this, int32_t ____d62457a234dec5aae622cb6f2f24f145a0, int32_t ____d62457a234dec5aae622cb6f2f24f14521, int32_t ____d62457a234dec5aae622cb6f2f24f145c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
