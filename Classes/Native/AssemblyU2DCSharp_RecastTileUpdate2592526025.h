﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<UnityEngine.Bounds>
struct Action_1_t3107457985;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RecastTileUpdate
struct  RecastTileUpdate_t2592526025  : public MonoBehaviour_t667441552
{
public:

public:
};

struct RecastTileUpdate_t2592526025_StaticFields
{
public:
	// System.Action`1<UnityEngine.Bounds> RecastTileUpdate::OnNeedUpdates
	Action_1_t3107457985 * ___OnNeedUpdates_2;

public:
	inline static int32_t get_offset_of_OnNeedUpdates_2() { return static_cast<int32_t>(offsetof(RecastTileUpdate_t2592526025_StaticFields, ___OnNeedUpdates_2)); }
	inline Action_1_t3107457985 * get_OnNeedUpdates_2() const { return ___OnNeedUpdates_2; }
	inline Action_1_t3107457985 ** get_address_of_OnNeedUpdates_2() { return &___OnNeedUpdates_2; }
	inline void set_OnNeedUpdates_2(Action_1_t3107457985 * value)
	{
		___OnNeedUpdates_2 = value;
		Il2CppCodeGenWriteBarrier(&___OnNeedUpdates_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
