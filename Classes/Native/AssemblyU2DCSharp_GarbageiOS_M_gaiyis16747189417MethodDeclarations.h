﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_gaiyis16
struct M_gaiyis16_t747189417;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_gaiyis16747189417.h"

// System.Void GarbageiOS.M_gaiyis16::.ctor()
extern "C"  void M_gaiyis16__ctor_m3858554794 (M_gaiyis16_t747189417 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gaiyis16::M_jearluSelkere0(System.String[],System.Int32)
extern "C"  void M_gaiyis16_M_jearluSelkere0_m1423822369 (M_gaiyis16_t747189417 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gaiyis16::M_cubem1(System.String[],System.Int32)
extern "C"  void M_gaiyis16_M_cubem1_m3025266492 (M_gaiyis16_t747189417 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gaiyis16::M_whourouhu2(System.String[],System.Int32)
extern "C"  void M_gaiyis16_M_whourouhu2_m988958535 (M_gaiyis16_t747189417 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gaiyis16::M_jissidir3(System.String[],System.Int32)
extern "C"  void M_gaiyis16_M_jissidir3_m2417550749 (M_gaiyis16_t747189417 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gaiyis16::M_wafegeRojame4(System.String[],System.Int32)
extern "C"  void M_gaiyis16_M_wafegeRojame4_m1873410542 (M_gaiyis16_t747189417 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gaiyis16::M_yeasu5(System.String[],System.Int32)
extern "C"  void M_gaiyis16_M_yeasu5_m2114348865 (M_gaiyis16_t747189417 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gaiyis16::M_telnederSearerece6(System.String[],System.Int32)
extern "C"  void M_gaiyis16_M_telnederSearerece6_m2618175553 (M_gaiyis16_t747189417 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gaiyis16::M_heelou7(System.String[],System.Int32)
extern "C"  void M_gaiyis16_M_heelou7_m1460604826 (M_gaiyis16_t747189417 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gaiyis16::M_lemfurgaw8(System.String[],System.Int32)
extern "C"  void M_gaiyis16_M_lemfurgaw8_m2393222669 (M_gaiyis16_t747189417 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gaiyis16::M_hailarall9(System.String[],System.Int32)
extern "C"  void M_gaiyis16_M_hailarall9_m507162024 (M_gaiyis16_t747189417 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gaiyis16::M_yerpu10(System.String[],System.Int32)
extern "C"  void M_gaiyis16_M_yerpu10_m110419831 (M_gaiyis16_t747189417 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gaiyis16::M_jeha11(System.String[],System.Int32)
extern "C"  void M_gaiyis16_M_jeha11_m9512503 (M_gaiyis16_t747189417 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gaiyis16::M_chepu12(System.String[],System.Int32)
extern "C"  void M_gaiyis16_M_chepu12_m2746625843 (M_gaiyis16_t747189417 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gaiyis16::ilo_M_cubem11(GarbageiOS.M_gaiyis16,System.String[],System.Int32)
extern "C"  void M_gaiyis16_ilo_M_cubem11_m260112135 (Il2CppObject * __this /* static, unused */, M_gaiyis16_t747189417 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gaiyis16::ilo_M_whourouhu22(GarbageiOS.M_gaiyis16,System.String[],System.Int32)
extern "C"  void M_gaiyis16_ilo_M_whourouhu22_m167775645 (Il2CppObject * __this /* static, unused */, M_gaiyis16_t747189417 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gaiyis16::ilo_M_jissidir33(GarbageiOS.M_gaiyis16,System.String[],System.Int32)
extern "C"  void M_gaiyis16_ilo_M_jissidir33_m3968759054 (Il2CppObject * __this /* static, unused */, M_gaiyis16_t747189417 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gaiyis16::ilo_M_telnederSearerece64(GarbageiOS.M_gaiyis16,System.String[],System.Int32)
extern "C"  void M_gaiyis16_ilo_M_telnederSearerece64_m3001948517 (Il2CppObject * __this /* static, unused */, M_gaiyis16_t747189417 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gaiyis16::ilo_M_yerpu105(GarbageiOS.M_gaiyis16,System.String[],System.Int32)
extern "C"  void M_gaiyis16_ilo_M_yerpu105_m3968219574 (Il2CppObject * __this /* static, unused */, M_gaiyis16_t747189417 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
