﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// System.Object
struct Il2CppObject;
// System.Attribute
struct Attribute_t2523058482;
// System.Runtime.InteropServices._Attribute
struct _Attribute_t3253047175;
// System.IConvertible
struct IConvertible_t2116191568;
// System.IComparable
struct IComparable_t1391370361;
// System.IComparable`1<System.Char>
struct IComparable_1_t2772552329;
// System.IEquatable`1<System.Char>
struct IEquatable_1_t2411901105;
// System.ValueType
struct ValueType_t1744280289;
// System.String
struct String_t;
// System.Collections.IEnumerable
struct IEnumerable_t3464557803;
// System.ICloneable
struct ICloneable_t1025544834;
// System.IComparable`1<System.String>
struct IComparable_1_t4212128644;
// System.IEquatable`1<System.String>
struct IEquatable_1_t3851477420;
// System.Type
struct Type_t;
// System.Reflection.IReflect
struct IReflect_t2853506214;
// System.Runtime.InteropServices._Type
struct _Type_t2149739635;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.ICustomAttributeProvider
struct ICustomAttributeProvider_t1425685797;
// System.Runtime.InteropServices._MemberInfo
struct _MemberInfo_t3353101921;
// System.IFormattable
struct IFormattable_t382002946;
// System.IComparable`1<System.Int32>
struct IComparable_1_t1063768291;
// System.IEquatable`1<System.Int32>
struct IEquatable_1_t703117067;
// System.IComparable`1<System.Double>
struct IComparable_1_t3778156356;
// System.IEquatable`1<System.Double>
struct IEquatable_1_t3417505132;
// System.IComparable`1<System.UInt32>
struct IComparable_1_t4229565068;
// System.IEquatable`1<System.UInt32>
struct IEquatable_1_t3868913844;
// System.IComparable`1<System.Byte>
struct IComparable_1_t2772539451;
// System.IEquatable`1<System.Byte>
struct IEquatable_1_t2411888227;
// System.IComparable`1<System.Single>
struct IComparable_1_t4201848763;
// System.IEquatable`1<System.Single>
struct IEquatable_1_t3841197539;
// System.Delegate
struct Delegate_t3310234105;
// System.Runtime.Serialization.ISerializable
struct ISerializable_t867484142;
// System.Reflection.ParameterInfo
struct ParameterInfo_t2235474049;
// System.Runtime.InteropServices._ParameterInfo
struct _ParameterInfo_t2787166306;
// System.IComparable`1<System.UInt16>
struct IComparable_1_t4229565010;
// System.IEquatable`1<System.UInt16>
struct IEquatable_1_t3868913786;
// System.IComparable`1<System.UInt64>
struct IComparable_1_t4229565163;
// System.IEquatable`1<System.UInt64>
struct IEquatable_1_t3868913939;
// System.IComparable`1<System.Int16>
struct IComparable_1_t1063768233;
// System.IEquatable`1<System.Int16>
struct IEquatable_1_t703117009;
// System.IComparable`1<System.SByte>
struct IComparable_1_t1071699568;
// System.IEquatable`1<System.SByte>
struct IEquatable_1_t711048344;
// System.IComparable`1<System.Int64>
struct IComparable_1_t1063768386;
// System.IEquatable`1<System.Int64>
struct IEquatable_1_t703117162;
// System.Reflection.EventInfo
struct EventInfo_t;
// System.Runtime.InteropServices._EventInfo
struct _EventInfo_t3271054163;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Runtime.InteropServices._FieldInfo
struct _FieldInfo_t209867187;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.InteropServices._MethodInfo
struct _MethodInfo_t3971289384;
// System.Reflection.MethodBase
struct MethodBase_t318515428;
// System.Runtime.InteropServices._MethodBase
struct _MethodBase_t3971068747;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Runtime.InteropServices._PropertyInfo
struct _PropertyInfo_t3711326812;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t4136801618;
// System.Runtime.InteropServices._ConstructorInfo
struct _ConstructorInfo_t3408715251;
// Mono.Globalization.Unicode.TailoringInfo
struct TailoringInfo_t3025807515;
// Mono.Globalization.Unicode.Contraction
struct Contraction_t3998770676;
// Mono.Globalization.Unicode.Level2Map
struct Level2Map_t3664214860;
// Mono.Math.BigInteger
struct BigInteger_t3334373498;
// System.Security.Cryptography.KeySizes
struct KeySizes_t2106826975;
// System.IComparable`1<System.Boolean>
struct IComparable_1_t386728509;
// System.IEquatable`1<System.Boolean>
struct IEquatable_1_t26077285;
// System.Diagnostics.StackFrame
struct StackFrame_t1034942277;
// System.Globalization.Calendar
struct Calendar_t3558528576;
// System.Globalization.CultureInfo
struct CultureInfo_t1065375142;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.IO.FileInfo
struct FileInfo_t3233670074;
// System.IO.FileSystemInfo
struct FileSystemInfo_t2605906633;
// System.MarshalByRefObject
struct MarshalByRefObject_t1219038801;
// System.IO.DirectoryInfo
struct DirectoryInfo_t89154617;
// System.Reflection.Emit.ModuleBuilder
struct ModuleBuilder_t595214213;
// System.Runtime.InteropServices._ModuleBuilder
struct _ModuleBuilder_t1764509690;
// System.Reflection.Module
struct Module_t1394482686;
// System.Runtime.InteropServices._Module
struct _Module_t2601912805;
// System.Reflection.Emit.ParameterBuilder
struct ParameterBuilder_t3159962230;
// System.Runtime.InteropServices._ParameterBuilder
struct _ParameterBuilder_t4122453611;
// System.Type[]
struct TypeU5BU5D_t3339007067;
// System.Array
struct Il2CppArray;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Collections.IList
struct IList_t1751339649;
// System.Reflection.Emit.LocalBuilder
struct LocalBuilder_t194563060;
// System.Runtime.InteropServices._LocalBuilder
struct _LocalBuilder_t3375243241;
// System.Reflection.LocalVariableInfo
struct LocalVariableInfo_t962988767;
// System.Reflection.Emit.GenericTypeParameterBuilder
struct GenericTypeParameterBuilder_t553556921;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t1918497079;
// System.Runtime.InteropServices._TypeBuilder
struct _TypeBuilder_t3501492652;
// System.Reflection.Emit.MethodBuilder
struct MethodBuilder_t302405488;
// System.Runtime.InteropServices._MethodBuilder
struct _MethodBuilder_t1471700965;
// System.Reflection.Emit.ConstructorBuilder
struct ConstructorBuilder_t3217839941;
// System.Runtime.InteropServices._ConstructorBuilder
struct _ConstructorBuilder_t788093754;
// System.Reflection.Emit.PropertyBuilder
struct PropertyBuilder_t2012258748;
// System.Runtime.InteropServices._PropertyBuilder
struct _PropertyBuilder_t752753201;
// System.Reflection.Emit.FieldBuilder
struct FieldBuilder_t1754069893;
// System.Runtime.InteropServices._FieldBuilder
struct _FieldBuilder_t639782778;
// System.Runtime.Remoting.Contexts.IContextProperty
struct IContextProperty_t82913453;
// System.Runtime.Remoting.Messaging.Header
struct Header_t1689611527;
// System.Runtime.Remoting.Services.ITrackingHandler
struct ITrackingHandler_t2228500544;
// System.Runtime.Remoting.Contexts.IContextAttribute
struct IContextAttribute_t3913746816;
// System.IComparable`1<System.DateTime>
struct IComparable_1_t4193591118;
// System.IEquatable`1<System.DateTime>
struct IEquatable_1_t3832939894;
// System.IComparable`1<System.Decimal>
struct IComparable_1_t1864280422;
// System.IEquatable`1<System.Decimal>
struct IEquatable_1_t1503629198;
// System.IComparable`1<System.TimeSpan>
struct IComparable_1_t323452778;
// System.IEquatable`1<System.TimeSpan>
struct IEquatable_1_t4257768850;
// System.Enum
struct Enum_t2862688501;
// System.MonoType
struct MonoType_t;
// System.Security.Policy.StrongName
struct StrongName_t2878058698;
// System.Text.EncodingInfo
struct EncodingInfo_t1898473639;
// System.Reflection.Assembly
struct Assembly_t1418687608;
// System.Runtime.InteropServices._Assembly
struct _Assembly_t3789461407;
// System.Reflection.CustomAttributeData
struct CustomAttributeData_t2955630591;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t3076817455;
// System.Runtime.Serialization.IDeserializationCallback
struct IDeserializationCallback_t675596727;
// System.Attribute[]
struct AttributeU5BU5D_t4055800263;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1244034627;
// System.IDisposable
struct IDisposable_t1423340799;
// System.Collections.Generic.List`1<Pathfinding.ClipperLib.TEdge>
struct List_1_t1024024395;
// System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>
struct List_1_t399344435;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>
struct Dictionary_2_t520966972;
// System.SystemException
struct SystemException_t4206535862;
// System.Exception
struct Exception_t3991598821;
// System.Runtime.InteropServices._Exception
struct _Exception_t426620218;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>
struct List_1_t2498711176;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t574734531;
// System.Collections.Generic.List`1<UnityEngine.Canvas>
struct List_1_t4095326316;
// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct List_1_t1377224777;
// System.Collections.Generic.List`1<UnityEngine.UI.Mask>
struct List_1_t1377012232;
// System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>
struct List_1_t430297630;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t1967039240;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1355284821;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t1355284823;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t1317283468;
// System.MulticastDelegate
struct MulticastDelegate_t3389745971;
// System.Collections.Generic.Dictionary`2<System.String,ACData>
struct Dictionary_2_t2745311790;
// System.WeakReference
struct WeakReference_t2199479497;
// System.Collections.Generic.List`1<CEvent.EventFunc`1<System.Object>>
struct List_1_t3015897733;
// System.Collections.Generic.List`1<CEvent.EventFunc`1<CEvent.ZEvent>>
struct List_1_t2483099862;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<Pathfinding.GraphNode>
struct List_1_t1391797922;
// System.Threading.Thread
struct Thread_t1973216770;
// System.Runtime.InteropServices._Thread
struct _Thread_t2796253571;
// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
struct CriticalFinalizerObject_t3073722122;
// System.Collections.Generic.List`1<System.UInt32>
struct List_1_t1392853533;
// System.Collections.Generic.List`1<Pathfinding.Int3>
struct List_1_t3342231146;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.List`1<Pathfinding.RecastMeshObj>
struct List_1_t3437381290;
// System.Collections.Generic.List`1<Pathfinding.MeshNode>
struct List_1_t78271701;
// System.Collections.Generic.List`1<Pathfinding.NavmeshCut>
struct List_1_t1669178200;
// System.Collections.Generic.List`1<Pathfinding.IntRect>
struct List_1_t88276517;
// System.Collections.Generic.List`1<Pathfinding.Int2>
struct List_1_t3342231145;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t1844984270;
// System.IComparable`1<Pathfinding.LocalAvoidance/IntersectionPair>
struct IComparable_1_t2731249710;
// System.IComparable`1<Pathfinding.AdvancedSmooth/Turn>
struct IComparable_1_t375125169;
// System.Collections.Generic.List`1<Pathfinding.NavmeshAdd>
struct List_1_t1669175735;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>
struct List_1_t1767529987;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1365137228;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512;
// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode>
struct List_1_t3717937726;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t2570496278;
// System.IEquatable`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>
struct IEquatable_1_t415412741;
// System.IComparable`1<ProtoBuf.ProtoMemberAttribute>
struct IComparable_1_t1917508069;
// System.Collections.Generic.List`1<AIEventInfo>
struct List_1_t1104940528;
// System.Collections.Generic.List`1<ReplayBase>
struct List_1_t2147888712;
// System.Collections.Generic.List`1<TargetMgr/AngleEntity>
struct List_1_t267346398;
// System.Collections.Generic.List`1<EffectCtrl>
struct List_1_t782005900;
// System.Collections.Generic.List`1<FloatTextUnit>
struct List_1_t3730483581;
// System.Collections.Generic.List`1<FileInfoRes>
struct List_1_t2585245350;

#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Attribute2523058482.h"
#include "mscorlib_System_Char2862622538.h"
#include "mscorlib_System_ValueType1744280289.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_System_Double3868226565.h"
#include "mscorlib_System_UInt3224667981.h"
#include "mscorlib_System_Byte2862609660.h"
#include "mscorlib_System_Single4291918972.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "mscorlib_System_Reflection_ParameterInfo2235474049.h"
#include "mscorlib_System_Reflection_ParameterModifier741930026.h"
#include "mscorlib_System_UInt1624667923.h"
#include "mscorlib_System_UInt6424668076.h"
#include "mscorlib_System_Int161153838442.h"
#include "mscorlib_System_SByte1161769777.h"
#include "mscorlib_System_Int641153838595.h"
#include "mscorlib_System_Reflection_EventInfo2739272946.h"
#include "mscorlib_System_Reflection_FieldInfo3973053266.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "mscorlib_System_Reflection_MethodBase318515428.h"
#include "mscorlib_System_Reflection_PropertyInfo924268725.h"
#include "mscorlib_System_Reflection_ConstructorInfo4136801618.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndex3372848153.h"
#include "mscorlib_Mono_Globalization_Unicode_TailoringInfo3025807515.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23222658402.h"
#include "mscorlib_System_Collections_Generic_Link2063667470.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21873037576.h"
#include "mscorlib_Mono_Globalization_Unicode_Contraction3998770676.h"
#include "mscorlib_Mono_Globalization_Unicode_Level2Map3664214860.h"
#include "mscorlib_Mono_Math_BigInteger3334373498.h"
#include "mscorlib_System_Security_Cryptography_KeySizes2106826975.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Collections_Hashtable_Slot2260530181.h"
#include "mscorlib_System_Collections_SortedList_Slot2072023290.h"
#include "mscorlib_System_Diagnostics_StackFrame1034942277.h"
#include "mscorlib_System_Globalization_Calendar3558528576.h"
#include "mscorlib_System_Globalization_CultureInfo1065375142.h"
#include "mscorlib_System_IO_FileInfo3233670074.h"
#include "mscorlib_System_IO_FileSystemInfo2605906633.h"
#include "mscorlib_System_MarshalByRefObject1219038801.h"
#include "mscorlib_System_IO_DirectoryInfo89154617.h"
#include "mscorlib_System_Reflection_Emit_ModuleBuilder595214213.h"
#include "mscorlib_System_Reflection_Module1394482686.h"
#include "mscorlib_System_Reflection_Emit_MonoResource1505432149.h"
#include "mscorlib_System_Reflection_Emit_ParameterBuilder3159962230.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Reflection_Emit_LocalBuilder194563060.h"
#include "mscorlib_System_Reflection_LocalVariableInfo962988767.h"
#include "mscorlib_System_Reflection_Emit_ILTokenInfo1354080954.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelD3207823784.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFi660379442.h"
#include "mscorlib_System_Reflection_Emit_GenericTypeParamete553556921.h"
#include "mscorlib_System_Reflection_Emit_TypeBuilder1918497079.h"
#include "mscorlib_System_Reflection_Emit_MethodBuilder302405488.h"
#include "mscorlib_System_Reflection_Emit_ConstructorBuilder3217839941.h"
#include "mscorlib_System_Reflection_Emit_PropertyBuilder2012258748.h"
#include "mscorlib_System_Reflection_Emit_FieldBuilder1754069893.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg3301293422.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArg3059612989.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceI4013605874.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceC2113902833.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Header1689611527.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_Decimal1954350631.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2420703430.h"
#include "mscorlib_System_Enum2862688501.h"
#include "mscorlib_System_MonoType2166710577.h"
#include "mscorlib_System_Security_Policy_StrongName2878058698.h"
#include "mscorlib_System_Text_EncodingInfo1898473639.h"
#include "mscorlib_System_Reflection_Assembly1418687608.h"
#include "mscorlib_System_Reflection_CustomAttributeData2955630591.h"
#include "mscorlib_System_Security_Cryptography_X509Certific3076817455.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21421923377.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21472936237.h"
#include "mscorlib_System_ArraySegment_1_gen2188033608.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22545618620.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21195997794.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312854529.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_104685482.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_726430633.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21328122633.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066860316.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_775952400.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23710127902.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21919813716.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21289591971.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1024024395.h"
#include "mscorlib_System_Collections_Generic_List_1_gen399344435.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_595048151.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23506074049.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_419747678.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge520966972.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_525170163.h"
#include "mscorlib_System_SystemException4206535862.h"
#include "mscorlib_System_Exception3991598821.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22758969756.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2498711176.h"
#include "mscorlib_System_Collections_Generic_List_1_gen574734531.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21744794968.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4095326316.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1377224777.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21496360359.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23226014568.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21354567995.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23983107678.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1377012232.h"
#include "mscorlib_System_Collections_Generic_List_1_gen430297630.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21329917009.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1355284822.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1967039240.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1355284821.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1355284823.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2522024052.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1317283468.h"
#include "mscorlib_System_MulticastDelegate3389745971.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22644092496.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2745311790.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23464510866.h"
#include "mscorlib_System_WeakReference2199479497.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22095523442.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23570725950.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066747055.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_328219207.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21049882445.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3015897733.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23735096809.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2483099862.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_478234732.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1375417109.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23075837162.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22373599864.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_260361757.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g11434748.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23203016565.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23582344850.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23450147721.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21327956382.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23632691627.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1391797922.h"
#include "mscorlib_System_Threading_Thread1973216770.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Criti3073722122.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21352297102.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_856162736.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23533017339.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23533017340.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1392853533.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3342231146.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_866109363.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_282007429.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22630286527.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22616509037.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24238261807.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22144098788.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22934690859.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21655170773.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3437381290.h"
#include "mscorlib_System_Collections_Generic_List_1_gen78271701.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22108115480.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1669178200.h"
#include "mscorlib_System_Collections_Generic_List_1_gen88276517.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3342231145.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1844984270.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1669175735.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1767529987.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21911225045.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21659289992.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23041451175.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1365137228.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_161279237.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3717937726.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_142169506.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24131445027.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23820648199.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21179766679.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24184235723.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22834614897.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22673780974.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22834426169.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23754817214.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24013503581.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21873923011.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_666182096.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_595436130.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22815902970.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_615096650.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21953420278.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21272974082.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_280379758.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_684992249.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23113378547.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23360505891.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24169023944.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24030168595.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22651095675.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_884060697.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21703319198.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21678415096.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_780579183.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_848696968.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23761112653.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21797357785.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23601132939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_124007610.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_677201358.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22712151909.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21889206235.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22722323132.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_471955825.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_923004647.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23786354602.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22877087089.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23572978580.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21438440308.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21844374148.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23352780754.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_826241115.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21013078529.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22038469116.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_686770431.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22920175223.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21164766315.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21651900817.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22900699950.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21307143445.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23322162674.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_829752355.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23790677735.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21209901952.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22104912006.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23529636437.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1104940528.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_463760594.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2147888712.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22867087788.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g98913785.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21641495614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21199798016.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23657452364.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22918678573.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22065771578.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_228458630.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23222289511.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24187962917.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24117548601.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_152597490.h"
#include "mscorlib_System_Collections_Generic_List_1_gen267346398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_163390343.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22620512916.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22790158764.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22061784035.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24287931429.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_712163209.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21936258874.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_407833816.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23353180286.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23660289130.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23987514235.h"
#include "mscorlib_System_Collections_Generic_List_1_gen782005900.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_678049845.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g25818750.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3730483581.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23880453256.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22102583517.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23374811140.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22025190314.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21947212572.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24241366511.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23488982890.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_716150752.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22752180667.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2585245350.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22481289295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22851311006.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21573321581.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23427993095.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22093487825.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_743866999.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21637661969.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21769044451.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24198129537.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_519615340.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23933281296.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24153532974.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_967237170.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24204345545.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g40760731.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21341048298.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_706219505.h"

#pragma once
// System.Object[]
struct ObjectU5BU5D_t1108656482  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Attribute[]
struct AttributeU5BU5D_t4055800263  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Attribute_t2523058482 * m_Items[1];

public:
	inline Attribute_t2523058482 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Attribute_t2523058482 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Attribute_t2523058482 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._Attribute[]
struct _AttributeU5BU5D_t302724734  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Char[]
struct CharU5BU5D_t3324145743  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.IConvertible[]
struct IConvertibleU5BU5D_t2953096049  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IComparable[]
struct IComparableU5BU5D_t3856259460  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IComparable`1<System.Char>[]
struct IComparable_1U5BU5D_t3342660916  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.Char>[]
struct IEquatable_1U5BU5D_t406268268  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.ValueType[]
struct ValueTypeU5BU5D_t2089672828  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ValueType_t1744280289 * m_Items[1];

public:
	inline ValueType_t1744280289 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ValueType_t1744280289 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ValueType_t1744280289 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.String[]
struct StringU5BU5D_t4054002952  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline String_t** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.IEnumerable[]
struct IEnumerableU5BU5D_t617364234  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.ICloneable[]
struct ICloneableU5BU5D_t847747383  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IComparable`1<System.String>[]
struct IComparable_1U5BU5D_t4072518125  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.String>[]
struct IEquatable_1U5BU5D_t1136125477  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Type[]
struct TypeU5BU5D_t3339007067  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.IReflect[]
struct IReflectU5BU5D_t1411309059  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._Type[]
struct _TypeU5BU5D_t3798043746  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t674955999  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MemberInfo_t * m_Items[1];

public:
	inline MemberInfo_t * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MemberInfo_t ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MemberInfo_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.ICustomAttributeProvider[]
struct ICustomAttributeProviderU5BU5D_t3471161512  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._MemberInfo[]
struct _MemberInfoU5BU5D_t3031668476  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t3230847821  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.IFormattable[]
struct IFormattableU5BU5D_t3834962615  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IComparable`1<System.Int32>[]
struct IComparable_1U5BU5D_t3249362994  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.Int32>[]
struct IEquatable_1U5BU5D_t312970346  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Double[]
struct DoubleU5BU5D_t2145413704  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) double m_Items[1];

public:
	inline double GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline double* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, double value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.Double>[]
struct IComparable_1U5BU5D_t2163928877  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.Double>[]
struct IEquatable_1U5BU5D_t3522503525  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.UInt32[]
struct UInt32U5BU5D_t3230734560  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint32_t m_Items[1];

public:
	inline uint32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline uint32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, uint32_t value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.UInt32>[]
struct IComparable_1U5BU5D_t3249249733  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.UInt32>[]
struct IEquatable_1U5BU5D_t312857085  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Byte[]
struct ByteU5BU5D_t4260760469  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.Byte>[]
struct IComparable_1U5BU5D_t4279275642  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.Byte>[]
struct IEquatable_1U5BU5D_t1342882994  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Single[,]
struct SingleU5BU2CU5D_t2316563990  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline float* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
	inline float GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, float value)
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		m_Items[index] = value;
	}
};
// System.Single[]
struct SingleU5BU5D_t2316563989  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline float* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.Single>[]
struct IComparable_1U5BU5D_t2335079162  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.Single>[]
struct IEquatable_1U5BU5D_t3693653810  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Delegate[]
struct DelegateU5BU5D_t2039970308  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Delegate_t3310234105 * m_Items[1];

public:
	inline Delegate_t3310234105 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Delegate_t3310234105 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Delegate_t3310234105 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.Serialization.ISerializable[]
struct ISerializableU5BU5D_t147227291  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t2015293532  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ParameterInfo_t2235474049 * m_Items[1];

public:
	inline ParameterInfo_t2235474049 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ParameterInfo_t2235474049 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ParameterInfo_t2235474049 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._ParameterInfo[]
struct _ParameterInfoU5BU5D_t1150119895  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t3896472559  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ParameterModifier_t741930026  m_Items[1];

public:
	inline ParameterModifier_t741930026  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ParameterModifier_t741930026 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ParameterModifier_t741930026  value)
	{
		m_Items[index] = value;
	}
};
// System.UInt16[]
struct UInt16U5BU5D_t801649474  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint16_t m_Items[1];

public:
	inline uint16_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline uint16_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, uint16_t value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.UInt16>[]
struct IComparable_1U5BU5D_t820164647  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.UInt16>[]
struct IEquatable_1U5BU5D_t2178739295  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.UInt64[]
struct UInt64U5BU5D_t2173929509  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint64_t m_Items[1];

public:
	inline uint64_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline uint64_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, uint64_t value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.UInt64>[]
struct IComparable_1U5BU5D_t2192444682  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.UInt64>[]
struct IEquatable_1U5BU5D_t3551019330  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int16[]
struct Int16U5BU5D_t801762735  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int16_t m_Items[1];

public:
	inline int16_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int16_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int16_t value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.Int16>[]
struct IComparable_1U5BU5D_t820277908  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.Int16>[]
struct IEquatable_1U5BU5D_t2178852556  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.SByte[]
struct SByteU5BU5D_t2505034988  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int8_t m_Items[1];

public:
	inline int8_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int8_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int8_t value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.SByte>[]
struct IComparable_1U5BU5D_t2523550161  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.SByte>[]
struct IEquatable_1U5BU5D_t3882124809  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int64[]
struct Int64U5BU5D_t2174042770  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int64_t m_Items[1];

public:
	inline int64_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int64_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int64_t value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.Int64>[]
struct IComparable_1U5BU5D_t2192557943  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.Int64>[]
struct IEquatable_1U5BU5D_t3551132591  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.EventInfo[]
struct EventInfoU5BU5D_t1933870855  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) EventInfo_t * m_Items[1];

public:
	inline EventInfo_t * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline EventInfo_t ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, EventInfo_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._EventInfo[]
struct _EventInfoU5BU5D_t1647824386  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t2567562023  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FieldInfo_t * m_Items[1];

public:
	inline FieldInfo_t * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FieldInfo_t ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FieldInfo_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._FieldInfo[]
struct _FieldInfoU5BU5D_t2281515554  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t2824366364  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MethodInfo_t * m_Items[1];

public:
	inline MethodInfo_t * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MethodInfo_t ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MethodInfo_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._MethodInfo[]
struct _MethodInfoU5BU5D_t886111545  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.MethodBase[]
struct MethodBaseU5BU5D_t2923381517  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MethodBase_t318515428 * m_Items[1];

public:
	inline MethodBase_t318515428 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MethodBase_t318515428 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MethodBase_t318515428 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._MethodBase[]
struct _MethodBaseU5BU5D_t985126698  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t4286713048  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PropertyInfo_t * m_Items[1];

public:
	inline PropertyInfo_t * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PropertyInfo_t ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PropertyInfo_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._PropertyInfo[]
struct _PropertyInfoU5BU5D_t4035282101  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.ConstructorInfo[]
struct ConstructorInfoU5BU5D_t2079826215  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ConstructorInfo_t4136801618 * m_Items[1];

public:
	inline ConstructorInfo_t4136801618 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ConstructorInfo_t4136801618 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ConstructorInfo_t4136801618 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._ConstructorInfo[]
struct _ConstructorInfoU5BU5D_t2267260130  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IntPtr[]
struct IntPtrU5BU5D_t3228729122  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) IntPtr_t m_Items[1];

public:
	inline IntPtr_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline IntPtr_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, IntPtr_t value)
	{
		m_Items[index] = value;
	}
};
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
struct TableRangeU5BU5D_t1733526372  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TableRange_t3372848153  m_Items[1];

public:
	inline TableRange_t3372848153  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TableRange_t3372848153 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TableRange_t3372848153  value)
	{
		m_Items[index] = value;
	}
};
// Mono.Globalization.Unicode.TailoringInfo[]
struct TailoringInfoU5BU5D_t4063285914  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TailoringInfo_t3025807515 * m_Items[1];

public:
	inline TailoringInfo_t3025807515 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TailoringInfo_t3025807515 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TailoringInfo_t3025807515 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct KeyValuePair_2U5BU5D_t310404823  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3222658402  m_Items[1];

public:
	inline KeyValuePair_2_t3222658402  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3222658402 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3222658402  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t375419643  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t2063667470  m_Items[1];

public:
	inline Link_t2063667470  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t2063667470 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t2063667470  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.DictionaryEntry[]
struct DictionaryEntryU5BU5D_t479206547  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DictionaryEntry_t1751606614  m_Items[1];

public:
	inline DictionaryEntry_t1751606614  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DictionaryEntry_t1751606614 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DictionaryEntry_t1751606614  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
struct KeyValuePair_2U5BU5D_t2844887513  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1873037576  m_Items[1];

public:
	inline KeyValuePair_2_t1873037576  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1873037576 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1873037576  value)
	{
		m_Items[index] = value;
	}
};
// Mono.Globalization.Unicode.Contraction[]
struct ContractionU5BU5D_t376151997  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Contraction_t3998770676 * m_Items[1];

public:
	inline Contraction_t3998770676 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Contraction_t3998770676 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Contraction_t3998770676 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Mono.Globalization.Unicode.Level2Map[]
struct Level2MapU5BU5D_t180654597  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Level2Map_t3664214860 * m_Items[1];

public:
	inline Level2Map_t3664214860 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Level2Map_t3664214860 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Level2Map_t3664214860 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Mono.Math.BigInteger[]
struct BigIntegerU5BU5D_t1634278495  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BigInteger_t3334373498 * m_Items[1];

public:
	inline BigInteger_t3334373498 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BigInteger_t3334373498 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BigInteger_t3334373498 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Security.Cryptography.KeySizes[]
struct KeySizesU5BU5D_t1457372358  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeySizes_t2106826975 * m_Items[1];

public:
	inline KeySizes_t2106826975 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeySizes_t2106826975 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeySizes_t2106826975 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2483180780  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1944668977  m_Items[1];

public:
	inline KeyValuePair_2_t1944668977  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1944668977 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1944668977  value)
	{
		m_Items[index] = value;
	}
};
// System.Boolean[]
struct BooleanU5BU5D_t3456302923  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) bool m_Items[1];

public:
	inline bool GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline bool* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, bool value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.Boolean>[]
struct IComparable_1U5BU5D_t3474818096  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.Boolean>[]
struct IEquatable_1U5BU5D_t538425448  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Hashtable/Slot[]
struct SlotU5BU5D_t2935089736  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Slot_t2260530181  m_Items[1];

public:
	inline Slot_t2260530181  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Slot_t2260530181 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Slot_t2260530181  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.SortedList/Slot[]
struct SlotU5BU5D_t4007541215  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Slot_t2072023290  m_Items[1];

public:
	inline Slot_t2072023290  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Slot_t2072023290 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Slot_t2072023290  value)
	{
		m_Items[index] = value;
	}
};
// System.Diagnostics.StackFrame[]
struct StackFrameU5BU5D_t627323400  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) StackFrame_t1034942277 * m_Items[1];

public:
	inline StackFrame_t1034942277 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline StackFrame_t1034942277 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, StackFrame_t1034942277 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Globalization.Calendar[]
struct CalendarU5BU5D_t2835990721  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Calendar_t3558528576 * m_Items[1];

public:
	inline Calendar_t3558528576 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Calendar_t3558528576 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Calendar_t3558528576 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Globalization.CultureInfo[]
struct CultureInfoU5BU5D_t1427656963  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CultureInfo_t1065375142 * m_Items[1];

public:
	inline CultureInfo_t1065375142 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CultureInfo_t1065375142 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CultureInfo_t1065375142 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IFormatProvider[]
struct IFormatProviderU5BU5D_t3850808286  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IO.FileInfo[]
struct FileInfoU5BU5D_t3358688287  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FileInfo_t3233670074 * m_Items[1];

public:
	inline FileInfo_t3233670074 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FileInfo_t3233670074 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FileInfo_t3233670074 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IO.FileSystemInfo[]
struct FileSystemInfoU5BU5D_t1190771700  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FileSystemInfo_t2605906633 * m_Items[1];

public:
	inline FileSystemInfo_t2605906633 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FileSystemInfo_t2605906633 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FileSystemInfo_t2605906633 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.MarshalByRefObject[]
struct MarshalByRefObjectU5BU5D_t4207444300  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MarshalByRefObject_t1219038801 * m_Items[1];

public:
	inline MarshalByRefObject_t1219038801 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MarshalByRefObject_t1219038801 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MarshalByRefObject_t1219038801 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IO.DirectoryInfo[]
struct DirectoryInfoU5BU5D_t2262552260  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DirectoryInfo_t89154617 * m_Items[1];

public:
	inline DirectoryInfo_t89154617 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DirectoryInfo_t89154617 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DirectoryInfo_t89154617 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.Emit.ModuleBuilder[]
struct ModuleBuilderU5BU5D_t1235779784  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ModuleBuilder_t595214213 * m_Items[1];

public:
	inline ModuleBuilder_t595214213 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ModuleBuilder_t595214213 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ModuleBuilder_t595214213 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._ModuleBuilder[]
struct _ModuleBuilderU5BU5D_t2693908191  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.Module[]
struct ModuleU5BU5D_t1003119691  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Module_t1394482686 * m_Items[1];

public:
	inline Module_t1394482686 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Module_t1394482686 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Module_t1394482686 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._Module[]
struct _ModuleU5BU5D_t1405051112  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.Emit.MonoResource[]
struct MonoResourceU5BU5D_t1470396600  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MonoResource_t1505432149  m_Items[1];

public:
	inline MonoResource_t1505432149  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MonoResource_t1505432149 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MonoResource_t1505432149  value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.Emit.ParameterBuilder[]
struct ParameterBuilderU5BU5D_t3245922035  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ParameterBuilder_t3159962230 * m_Items[1];

public:
	inline ParameterBuilder_t3159962230 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ParameterBuilder_t3159962230 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ParameterBuilder_t3159962230 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._ParameterBuilder[]
struct _ParameterBuilderU5BU5D_t2749961098  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Type[][]
struct TypeU5BU5DU5BU5D_t2708692954  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TypeU5BU5D_t3339007067* m_Items[1];

public:
	inline TypeU5BU5D_t3339007067* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TypeU5BU5D_t3339007067** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TypeU5BU5D_t3339007067* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Array[]
struct ArrayU5BU5D_t3094558710  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppArray * m_Items[1];

public:
	inline Il2CppArray * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppArray ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppArray * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.ICollection[]
struct ICollectionU5BU5D_t3870071836  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.IList[]
struct IListU5BU5D_t3576116828  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.Emit.LocalBuilder[]
struct LocalBuilderU5BU5D_t2031018429  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LocalBuilder_t194563060 * m_Items[1];

public:
	inline LocalBuilder_t194563060 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LocalBuilder_t194563060 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LocalBuilder_t194563060 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._LocalBuilder[]
struct _LocalBuilderU5BU5D_t4029726292  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.LocalVariableInfo[]
struct LocalVariableInfoU5BU5D_t2935122630  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LocalVariableInfo_t962988767 * m_Items[1];

public:
	inline LocalVariableInfo_t962988767 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LocalVariableInfo_t962988767 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LocalVariableInfo_t962988767 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.Emit.ILTokenInfo[]
struct ILTokenInfoU5BU5D_t1040181535  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ILTokenInfo_t1354080954  m_Items[1];

public:
	inline ILTokenInfo_t1354080954  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ILTokenInfo_t1354080954 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ILTokenInfo_t1354080954  value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.Emit.ILGenerator/LabelData[]
struct LabelDataU5BU5D_t657210041  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LabelData_t3207823784  m_Items[1];

public:
	inline LabelData_t3207823784  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LabelData_t3207823784 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LabelData_t3207823784  value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.Emit.ILGenerator/LabelFixup[]
struct LabelFixupU5BU5D_t1656159175  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LabelFixup_t660379442  m_Items[1];

public:
	inline LabelFixup_t660379442  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LabelFixup_t660379442 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LabelFixup_t660379442  value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.Emit.GenericTypeParameterBuilder[]
struct GenericTypeParameterBuilderU5BU5D_t2802075972  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GenericTypeParameterBuilder_t553556921 * m_Items[1];

public:
	inline GenericTypeParameterBuilder_t553556921 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GenericTypeParameterBuilder_t553556921 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GenericTypeParameterBuilder_t553556921 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.Emit.TypeBuilder[]
struct TypeBuilderU5BU5D_t1363945486  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TypeBuilder_t1918497079 * m_Items[1];

public:
	inline TypeBuilder_t1918497079 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TypeBuilder_t1918497079 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TypeBuilder_t1918497079 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._TypeBuilder[]
struct _TypeBuilderU5BU5D_t193502757  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.Emit.MethodBuilder[]
struct MethodBuilderU5BU5D_t304196817  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MethodBuilder_t302405488 * m_Items[1];

public:
	inline MethodBuilder_t302405488 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MethodBuilder_t302405488 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MethodBuilder_t302405488 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._MethodBuilder[]
struct _MethodBuilderU5BU5D_t1762325224  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.Emit.ConstructorBuilder[]
struct ConstructorBuilderU5BU5D_t678056456  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ConstructorBuilder_t3217839941 * m_Items[1];

public:
	inline ConstructorBuilder_t3217839941 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ConstructorBuilder_t3217839941 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ConstructorBuilder_t3217839941 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._ConstructorBuilder[]
struct _ConstructorBuilderU5BU5D_t1773125279  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.Emit.PropertyBuilder[]
struct PropertyBuilderU5BU5D_t1657492437  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PropertyBuilder_t2012258748 * m_Items[1];

public:
	inline PropertyBuilder_t2012258748 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PropertyBuilder_t2012258748 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PropertyBuilder_t2012258748 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._PropertyBuilder[]
struct _PropertyBuilderU5BU5D_t3731712492  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.Emit.FieldBuilder[]
struct FieldBuilderU5BU5D_t3550958792  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FieldBuilder_t1754069893 * m_Items[1];

public:
	inline FieldBuilder_t1754069893 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FieldBuilder_t1754069893 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FieldBuilder_t1754069893 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._FieldBuilder[]
struct _FieldBuilderU5BU5D_t1254699359  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t2088020251  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CustomAttributeTypedArgument_t3301293422  m_Items[1];

public:
	inline CustomAttributeTypedArgument_t3301293422  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CustomAttributeTypedArgument_t3301293422 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CustomAttributeTypedArgument_t3301293422  value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1983528240  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CustomAttributeNamedArgument_t3059612989  m_Items[1];

public:
	inline CustomAttributeNamedArgument_t3059612989  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CustomAttributeNamedArgument_t3059612989 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CustomAttributeNamedArgument_t3059612989  value)
	{
		m_Items[index] = value;
	}
};
// System.Resources.ResourceReader/ResourceInfo[]
struct ResourceInfoU5BU5D_t2845361159  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ResourceInfo_t4013605874  m_Items[1];

public:
	inline ResourceInfo_t4013605874  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ResourceInfo_t4013605874 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ResourceInfo_t4013605874  value)
	{
		m_Items[index] = value;
	}
};
// System.Resources.ResourceReader/ResourceCacheItem[]
struct ResourceCacheItemU5BU5D_t1070225452  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ResourceCacheItem_t2113902833  m_Items[1];

public:
	inline ResourceCacheItem_t2113902833  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ResourceCacheItem_t2113902833 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ResourceCacheItem_t2113902833  value)
	{
		m_Items[index] = value;
	}
};
// System.Runtime.Remoting.Contexts.IContextProperty[]
struct IContextPropertyU5BU5D_t2063418880  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t856208126  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Header_t1689611527 * m_Items[1];

public:
	inline Header_t1689611527 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Header_t1689611527 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Header_t1689611527 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.Remoting.Services.ITrackingHandler[]
struct ITrackingHandlerU5BU5D_t3684998849  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.Remoting.Contexts.IContextAttribute[]
struct IContextAttributeU5BU5D_t3402113153  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.DateTime[]
struct DateTimeU5BU5D_t194158294  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DateTime_t4283661327  m_Items[1];

public:
	inline DateTime_t4283661327  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DateTime_t4283661327 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DateTime_t4283661327  value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.DateTime>[]
struct IComparable_1U5BU5D_t212673467  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.DateTime>[]
struct IEquatable_1U5BU5D_t1571248115  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Decimal[]
struct DecimalU5BU5D_t183431518  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Decimal_t1954350631  m_Items[1];

public:
	inline Decimal_t1954350631  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Decimal_t1954350631 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Decimal_t1954350631  value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.Decimal>[]
struct IComparable_1U5BU5D_t201946691  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.Decimal>[]
struct IEquatable_1U5BU5D_t1560521339  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.TimeSpan[]
struct TimeSpanU5BU5D_t3241447114  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TimeSpan_t413522987  m_Items[1];

public:
	inline TimeSpan_t413522987  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TimeSpan_t413522987 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TimeSpan_t413522987  value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.TimeSpan>[]
struct IComparable_1U5BU5D_t3259962287  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.TimeSpan>[]
struct IEquatable_1U5BU5D_t323569639  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
struct TypeTagU5BU5D_t172815715  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Enum[]
struct EnumU5BU5D_t3205174168  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Enum_t2862688501 * m_Items[1];

public:
	inline Enum_t2862688501 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Enum_t2862688501 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Enum_t2862688501 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.MonoType[]
struct MonoTypeU5BU5D_t1750410988  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MonoType_t * m_Items[1];

public:
	inline MonoType_t * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MonoType_t ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MonoType_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Byte[,]
struct ByteU5BU2CU5D_t4260760470  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
	inline uint8_t GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, uint8_t value)
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		m_Items[index] = value;
	}
};
// System.Security.Policy.StrongName[]
struct StrongNameU5BU5D_t3819180239  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) StrongName_t2878058698 * m_Items[1];

public:
	inline StrongName_t2878058698 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline StrongName_t2878058698 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, StrongName_t2878058698 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Text.EncodingInfo[]
struct EncodingInfoU5BU5D_t3605083358  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) EncodingInfo_t1898473639 * m_Items[1];

public:
	inline EncodingInfo_t1898473639 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline EncodingInfo_t1898473639 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, EncodingInfo_t1898473639 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.Assembly[]
struct AssemblyU5BU5D_t4221342377  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Assembly_t1418687608 * m_Items[1];

public:
	inline Assembly_t1418687608 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Assembly_t1418687608 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Assembly_t1418687608 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._Assembly[]
struct _AssemblyU5BU5D_t2326024966  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.CustomAttributeData[]
struct CustomAttributeDataU5BU5D_t4123632934  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CustomAttributeData_t2955630591 * m_Items[1];

public:
	inline CustomAttributeData_t2955630591 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CustomAttributeData_t2955630591 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CustomAttributeData_t2955630591 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Byte[][]
struct ByteU5BU5DU5BU5D_t2421305976  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ByteU5BU5D_t4260760469* m_Items[1];

public:
	inline ByteU5BU5D_t4260760469* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ByteU5BU5D_t4260760469** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ByteU5BU5D_t4260760469* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Security.Cryptography.X509Certificates.X509Certificate[]
struct X509CertificateU5BU5D_t3275955254  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) X509Certificate_t3076817455 * m_Items[1];

public:
	inline X509Certificate_t3076817455 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline X509Certificate_t3076817455 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, X509Certificate_t3076817455 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.Serialization.IDeserializationCallback[]
struct IDeserializationCallbackU5BU5D_t4214513038  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Attribute[][]
struct AttributeU5BU5DU5BU5D_t1538489150  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AttributeU5BU5D_t4055800263* m_Items[1];

public:
	inline AttributeU5BU5D_t4055800263* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AttributeU5BU5D_t4055800263** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AttributeU5BU5D_t4055800263* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.LinkedList`1<System.ComponentModel.TypeDescriptionProvider>>[]
struct KeyValuePair_2U5BU5D_t454461420  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1421923377  m_Items[1];

public:
	inline KeyValuePair_2_t1421923377  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1421923377 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1421923377  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.ComponentModel.WeakObjectWrapper,System.Collections.Generic.LinkedList`1<System.ComponentModel.TypeDescriptionProvider>>[]
struct KeyValuePair_2U5BU5D_t1996120960  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1472936237  m_Items[1];

public:
	inline KeyValuePair_2_t1472936237  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1472936237 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1472936237  value)
	{
		m_Items[index] = value;
	}
};
// System.ArraySegment`1<System.Byte>[]
struct ArraySegment_1U5BU5D_t3490338713  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ArraySegment_1_t2188033608  m_Items[1];

public:
	inline ArraySegment_1_t2188033608  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ArraySegment_1_t2188033608 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ArraySegment_1_t2188033608  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t535859925  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2545618620  m_Items[1];

public:
	inline KeyValuePair_2_t2545618620  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2545618620 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2545618620  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t3070342615  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1195997794  m_Items[1];

public:
	inline KeyValuePair_2_t1195997794  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1195997794 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1195997794  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<System.Object>[]
struct List_1U5BU5D_t2536214866  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1244034627 * m_Items[1];

public:
	inline List_1_t1244034627 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1244034627 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1244034627 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.List`1<System.Object>>[]
struct KeyValuePair_2U5BU5D_t3910739164  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3312854529  m_Items[1];

public:
	inline KeyValuePair_2_t3312854529  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3312854529 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3312854529  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>[]
struct KeyValuePair_2U5BU5D_t823028335  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t104685482  m_Items[1];

public:
	inline KeyValuePair_2_t104685482  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t104685482 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t104685482  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>[]
struct KeyValuePair_2U5BU5D_t3668042644  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t726430633  m_Items[1];

public:
	inline KeyValuePair_2_t726430633  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t726430633 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t726430633  value)
	{
		m_Items[index] = value;
	}
};
// System.IDisposable[]
struct IDisposableU5BU5D_t1854239782  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.AnimationClip,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t3310330548  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1328122633  m_Items[1];

public:
	inline KeyValuePair_2_t1328122633  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1328122633 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1328122633  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct KeyValuePair_2U5BU5D_t3299892981  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t4066860316  m_Items[1];

public:
	inline KeyValuePair_2_t4066860316  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t4066860316 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t4066860316  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>[]
struct KeyValuePair_2U5BU5D_t1605285297  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t775952400  m_Items[1];

public:
	inline KeyValuePair_2_t775952400  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t775952400 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t775952400  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>[]
struct KeyValuePair_2U5BU5D_t179694251  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3710127902  m_Items[1];

public:
	inline KeyValuePair_2_t3710127902  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3710127902 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3710127902  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t3614328797  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1919813716  m_Items[1];

public:
	inline KeyValuePair_2_t1919813716  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1919813716 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1919813716  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t3387041138  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1289591971  m_Items[1];

public:
	inline KeyValuePair_2_t1289591971  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1289591971 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1289591971  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<Pathfinding.ClipperLib.TEdge>[]
struct List_1U5BU5D_t1754266410  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1024024395 * m_Items[1];

public:
	inline List_1_t1024024395 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1024024395 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1024024395 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>[]
struct List_1U5BU5D_t497255074  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t399344435 * m_Items[1];

public:
	inline List_1_t399344435 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t399344435 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t399344435 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[]
struct KeyValuePair_2U5BU5D_t722696174  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t595048151  m_Items[1];

public:
	inline KeyValuePair_2_t595048151  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t595048151 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t595048151  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,Pathfinding.Ionic.Zip.ZipEntry>[]
struct KeyValuePair_2U5BU5D_t1958048284  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3506074049  m_Items[1];

public:
	inline KeyValuePair_2_t3506074049  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3506074049 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3506074049  value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Int32U5BU5D_t3230847821* m_Items[1];

public:
	inline Int32U5BU5D_t3230847821* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Int32U5BU5D_t3230847821** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Int32U5BU5D_t3230847821* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Reflection.MemberInfo>[]
struct KeyValuePair_2U5BU5D_t288995691  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t419747678  m_Items[1];

public:
	inline KeyValuePair_2_t419747678  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t419747678 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t419747678  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>[]
struct Dictionary_2U5BU5D_t3618253909  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Dictionary_2_t520966972 * m_Items[1];

public:
	inline Dictionary_2_t520966972 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Dictionary_2_t520966972 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Dictionary_2_t520966972 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>>[]
struct KeyValuePair_2U5BU5D_t2402659554  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t525170163  m_Items[1];

public:
	inline KeyValuePair_2_t525170163  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t525170163 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t525170163  value)
	{
		m_Items[index] = value;
	}
};
// System.SystemException[]
struct SystemExceptionU5BU5D_t1537348531  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SystemException_t4206535862 * m_Items[1];

public:
	inline SystemException_t4206535862 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SystemException_t4206535862 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SystemException_t4206535862 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Exception[]
struct ExceptionU5BU5D_t1608523752  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Exception_t3991598821 * m_Items[1];

public:
	inline Exception_t3991598821 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Exception_t3991598821 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Exception_t3991598821 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._Exception[]
struct _ExceptionU5BU5D_t2150415519  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>[]
struct KeyValuePair_2U5BU5D_t2607313013  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2758969756  m_Items[1];

public:
	inline KeyValuePair_2_t2758969756  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2758969756 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2758969756  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>[]
struct List_1U5BU5D_t1765424729  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t2498711176 * m_Items[1];

public:
	inline List_1_t2498711176 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t2498711176 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t2498711176 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<UnityEngine.Component>[]
struct List_1U5BU5D_t2091470034  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t574734531 * m_Items[1];

public:
	inline List_1_t574734531 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t574734531 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t574734531 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>[]
struct KeyValuePair_2U5BU5D_t3619361353  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1744794968  m_Items[1];

public:
	inline KeyValuePair_2_t1744794968  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1744794968 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1744794968  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<UnityEngine.Canvas>[]
struct List_1U5BU5D_t36510821  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t4095326316 * m_Items[1];

public:
	inline List_1_t4095326316 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t4095326316 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t4095326316 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<UnityEngine.UI.Text>[]
struct List_1U5BU5D_t931498100  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1377224777 * m_Items[1];

public:
	inline List_1_t1377224777 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1377224777 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1377224777 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>[]
struct KeyValuePair_2U5BU5D_t512189406  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1496360359  m_Items[1];

public:
	inline KeyValuePair_2_t1496360359  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1496360359 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1496360359  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>[]
struct KeyValuePair_2U5BU5D_t274426361  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3226014568  m_Items[1];

public:
	inline KeyValuePair_2_t3226014568  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3226014568 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3226014568  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>[]
struct KeyValuePair_2U5BU5D_t4265808762  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1354567995  m_Items[1];

public:
	inline KeyValuePair_2_t1354567995  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1354567995 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1354567995  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>[]
struct KeyValuePair_2U5BU5D_t197004907  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3983107678  m_Items[1];

public:
	inline KeyValuePair_2_t3983107678  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3983107678 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3983107678  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<UnityEngine.UI.Mask>[]
struct List_1U5BU5D_t1071654105  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1377012232 * m_Items[1];

public:
	inline List_1_t1377012232 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1377012232 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1377012232 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>[]
struct List_1U5BU5D_t1495120811  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t430297630 * m_Items[1];

public:
	inline List_1_t430297630 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t430297630 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t430297630 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.IClipper,System.Int32>[]
struct KeyValuePair_2U5BU5D_t278621004  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1329917009  m_Items[1];

public:
	inline KeyValuePair_2_t1329917009  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1329917009 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1329917009  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<UnityEngine.Vector3>[]
struct List_1U5BU5D_t1642958995  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1355284822 * m_Items[1];

public:
	inline List_1_t1355284822 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1355284822 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1355284822 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<UnityEngine.Color32>[]
struct List_1U5BU5D_t93358041  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1967039240 * m_Items[1];

public:
	inline List_1_t1967039240 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1967039240 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1967039240 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<UnityEngine.Vector2>[]
struct List_1U5BU5D_t1156771256  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1355284821 * m_Items[1];

public:
	inline List_1_t1355284821 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1355284821 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1355284821 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<UnityEngine.Vector4>[]
struct List_1U5BU5D_t2129146734  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1355284823 * m_Items[1];

public:
	inline List_1_t1355284823 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1355284823 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1355284823 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<System.Int32>[]
struct List_1U5BU5D_t363438909  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t2522024052 * m_Items[1];

public:
	inline List_1_t2522024052 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t2522024052 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t2522024052 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<UnityEngine.UIVertex>[]
struct List_1U5BU5D_t3223949765  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1317283468 * m_Items[1];

public:
	inline List_1_t1317283468 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1317283468 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1317283468 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.MulticastDelegate[]
struct MulticastDelegateU5BU5D_t1432943266  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MulticastDelegate_t3389745971 * m_Items[1];

public:
	inline MulticastDelegate_t3389745971 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MulticastDelegate_t3389745971 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MulticastDelegate_t3389745971 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,ACData>[]
struct KeyValuePair_2U5BU5D_t936371825  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2644092496  m_Items[1];

public:
	inline KeyValuePair_2_t2644092496  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2644092496 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2644092496  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.Dictionary`2<System.String,ACData>[]
struct Dictionary_2U5BU5D_t4265630043  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Dictionary_2_t2745311790 * m_Items[1];

public:
	inline Dictionary_2_t2745311790 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Dictionary_2_t2745311790 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Dictionary_2_t2745311790 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.Dictionary`2<System.String,ACData>>[]
struct KeyValuePair_2U5BU5D_t3879669735  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3464510866  m_Items[1];

public:
	inline KeyValuePair_2_t3464510866  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3464510866 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3464510866  value)
	{
		m_Items[index] = value;
	}
};
// System.WeakReference[]
struct WeakReferenceU5BU5D_t2697619956  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) WeakReference_t2199479497 * m_Items[1];

public:
	inline WeakReference_t2199479497 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline WeakReference_t2199479497 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, WeakReference_t2199479497 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.WeakReference>[]
struct KeyValuePair_2U5BU5D_t593889159  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2095523442  m_Items[1];

public:
	inline KeyValuePair_2_t2095523442  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2095523442 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2095523442  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GameObject>[]
struct KeyValuePair_2U5BU5D_t558378251  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3570725950  m_Items[1];

public:
	inline KeyValuePair_2_t3570725950  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3570725950 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3570725950  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>[]
struct KeyValuePair_2U5BU5D_t2966088118  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t4066747055  m_Items[1];

public:
	inline KeyValuePair_2_t4066747055  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t4066747055 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t4066747055  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.UInt32,CSSkillData>[]
struct KeyValuePair_2U5BU5D_t2929248446  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t328219207  m_Items[1];

public:
	inline KeyValuePair_2_t328219207  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t328219207 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t328219207  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
struct KeyValuePair_2U5BU5D_t1127117024  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1049882445  m_Items[1];

public:
	inline KeyValuePair_2_t1049882445  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1049882445 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1049882445  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<CEvent.EventFunc`1<System.Object>>[]
struct List_1U5BU5D_t2266997192  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t3015897733 * m_Items[1];

public:
	inline List_1_t3015897733 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t3015897733 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t3015897733 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<CEvent.EventFunc`1<System.Object>>>[]
struct KeyValuePair_2U5BU5D_t1881036884  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3735096809  m_Items[1];

public:
	inline KeyValuePair_2_t3735096809  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3735096809 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3735096809  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<CEvent.EventFunc`1<CEvent.ZEvent>>[]
struct List_1U5BU5D_t4154698515  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t2483099862 * m_Items[1];

public:
	inline List_1_t2483099862 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t2483099862 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t2483099862 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.String[][]
struct StringU5BU5DU5BU5D_t3538327001  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) StringU5BU5D_t4054002952* m_Items[1];

public:
	inline StringU5BU5D_t4054002952* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline StringU5BU5D_t4054002952** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, StringU5BU5D_t4054002952* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.String[]>[]
struct KeyValuePair_2U5BU5D_t3152366693  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t478234732  m_Items[1];

public:
	inline KeyValuePair_2_t478234732  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t478234732 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t478234732  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<System.String>[]
struct List_1U5BU5D_t1186594040  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1375417109 * m_Items[1];

public:
	inline List_1_t1375417109 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1375417109 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1375417109 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,VersionInfo>[]
struct KeyValuePair_2U5BU5D_t769630255  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3075837162  m_Items[1];

public:
	inline KeyValuePair_2_t3075837162  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3075837162 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3075837162  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Type,GenericTypeCache/TypeMembers>[]
struct KeyValuePair_2U5BU5D_t1859570857  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2373599864  m_Items[1];

public:
	inline KeyValuePair_2_t2373599864  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2373599864 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2373599864  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.TextAsset>[]
struct KeyValuePair_2U5BU5D_t3311185104  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t260361757  m_Items[1];

public:
	inline KeyValuePair_2_t260361757  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t260361757 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t260361757  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Type,System.String>[]
struct KeyValuePair_2U5BU5D_t2838408597  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t11434748  m_Items[1];

public:
	inline KeyValuePair_2_t11434748  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t11434748 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t11434748  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Type,JSCache/TypeInfo>[]
struct KeyValuePair_2U5BU5D_t3549023000  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3203016565  m_Items[1];

public:
	inline KeyValuePair_2_t3203016565  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3203016565 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3203016565  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Type>[]
struct KeyValuePair_2U5BU5D_t2953046759  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3582344850  m_Items[1];

public:
	inline KeyValuePair_2_t3582344850  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3582344850 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3582344850  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,JSMgr/JS_CS_Rel>[]
struct KeyValuePair_2U5BU5D_t1918789684  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3450147721  m_Items[1];

public:
	inline KeyValuePair_2_t3450147721  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3450147721 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3450147721  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,JSMgr/JS_CS_Rel>[]
struct KeyValuePair_2U5BU5D_t1102077483  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1327956382  m_Items[1];

public:
	inline KeyValuePair_2_t1327956382  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1327956382 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1327956382  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,Scripts.JSBindingClasses.JsRepresentClass>[]
struct KeyValuePair_2U5BU5D_t3641068362  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3632691627  m_Items[1];

public:
	inline KeyValuePair_2_t3632691627  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3632691627 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3632691627  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<Pathfinding.GraphNode>[]
struct List_1U5BU5D_t2355007639  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1391797922 * m_Items[1];

public:
	inline List_1_t1391797922 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1391797922 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1391797922 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Threading.Thread[]
struct ThreadU5BU5D_t3166308279  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Thread_t1973216770 * m_Items[1];

public:
	inline Thread_t1973216770 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Thread_t1973216770 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Thread_t1973216770 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._Thread[]
struct _ThreadU5BU5D_t1080885522  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.ConstrainedExecution.CriticalFinalizerObject[]
struct CriticalFinalizerObjectU5BU5D_t3914078607  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CriticalFinalizerObject_t3073722122 * m_Items[1];

public:
	inline CriticalFinalizerObject_t3073722122 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CriticalFinalizerObject_t3073722122 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CriticalFinalizerObject_t3073722122 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>[]
struct KeyValuePair_2U5BU5D_t1110639483  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1352297102  m_Items[1];

public:
	inline KeyValuePair_2_t1352297102  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1352297102 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1352297102  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,UnityEngine.GameObject>[]
struct KeyValuePair_2U5BU5D_t2664092049  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t856162736  m_Items[1];

public:
	inline KeyValuePair_2_t856162736  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t856162736 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t856162736  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<Pathfinding.GraphNode,Pathfinding.NodeLink2>[]
struct KeyValuePair_2U5BU5D_t3570392250  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3533017339  m_Items[1];

public:
	inline KeyValuePair_2_t3533017339  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3533017339 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3533017339  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<Pathfinding.GraphNode,Pathfinding.NodeLink3>[]
struct KeyValuePair_2U5BU5D_t4056579989  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3533017340  m_Items[1];

public:
	inline KeyValuePair_2_t3533017340  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3533017340 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3533017340  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<System.UInt32>[]
struct List_1U5BU5D_t363325648  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1392853533 * m_Items[1];

public:
	inline List_1_t1392853533 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1392853533 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1392853533 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<Pathfinding.Int3>[]
struct List_1U5BU5D_t1943842991  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t3342231146 * m_Items[1];

public:
	inline List_1_t3342231146 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t3342231146 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t3342231146 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.IEnumerator[]
struct IEnumeratorU5BU5D_t1143200670  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>[]
struct KeyValuePair_2U5BU5D_t3739032610  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t866109363  m_Items[1];

public:
	inline KeyValuePair_2_t866109363  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t866109363 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t866109363  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,ProceduralWorld/ProceduralTile>[]
struct KeyValuePair_2U5BU5D_t4058859720  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t282007429  m_Items[1];

public:
	inline KeyValuePair_2_t282007429  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t282007429 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t282007429  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3232830822  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2630286527  m_Items[1];

public:
	inline KeyValuePair_2_t2630286527  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2630286527 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2630286527  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,Pathfinding.TriangleMeshNode>[]
struct KeyValuePair_2U5BU5D_t400379200  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2616509037  m_Items[1];

public:
	inline KeyValuePair_2_t2616509037  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2616509037 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2616509037  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,Pathfinding.PointNode>[]
struct KeyValuePair_2U5BU5D_t1846308918  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t4238261807  m_Items[1];

public:
	inline KeyValuePair_2_t4238261807  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t4238261807 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t4238261807  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>[]
struct KeyValuePair_2U5BU5D_t1566256653  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2144098788  m_Items[1];

public:
	inline KeyValuePair_2_t2144098788  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2144098788 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2144098788  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Mesh,UnityEngine.Vector3[]>[]
struct KeyValuePair_2U5BU5D_t3321870026  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2934690859  m_Items[1];

public:
	inline KeyValuePair_2_t2934690859  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2934690859 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2934690859  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Mesh,System.Int32[]>[]
struct KeyValuePair_2U5BU5D_t1572291128  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1655170773  m_Items[1];

public:
	inline KeyValuePair_2_t1655170773  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1655170773 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1655170773  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<Pathfinding.RecastMeshObj>[]
struct List_1U5BU5D_t273549167  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t3437381290 * m_Items[1];

public:
	inline List_1_t3437381290 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t3437381290 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t3437381290 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<Pathfinding.MeshNode>[]
struct List_1U5BU5D_t3949419576  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t78271701 * m_Items[1];

public:
	inline List_1_t78271701 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t78271701 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t78271701 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<Pathfinding.Poly2Tri.TriangulationPoint,System.Int32>[]
struct KeyValuePair_2U5BU5D_t1450152585  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2108115480  m_Items[1];

public:
	inline KeyValuePair_2_t2108115480  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2108115480 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2108115480  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<Pathfinding.NavmeshCut>[]
struct List_1U5BU5D_t3251137609  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1669178200 * m_Items[1];

public:
	inline List_1_t1669178200 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1669178200 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1669178200 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<Pathfinding.IntRect>[]
struct List_1U5BU5D_t558158760  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t88276517 * m_Items[1];

public:
	inline List_1_t88276517 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t88276517 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t88276517 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<Pathfinding.Int2>[]
struct List_1U5BU5D_t1457655252  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t3342231145 * m_Items[1];

public:
	inline List_1_t3342231145 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t3342231145 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t3342231145 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<System.Boolean>[]
struct List_1U5BU5D_t588894011  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1844984270 * m_Items[1];

public:
	inline List_1_t1844984270 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1844984270 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1844984270 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IComparable`1<Pathfinding.LocalAvoidance/IntersectionPair>[]
struct IComparable_1U5BU5D_t3930944347  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IComparable`1<Pathfinding.AdvancedSmooth/Turn>[]
struct IComparable_1U5BU5D_t2723960172  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<Pathfinding.NavmeshAdd>[]
struct List_1U5BU5D_t3094236558  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1669175735 * m_Items[1];

public:
	inline List_1_t1669175735 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1669175735 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1669175735 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>[]
struct List_1U5BU5D_t1924813458  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1767529987 * m_Items[1];

public:
	inline List_1_t1767529987 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1767529987 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1767529987 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<Pathfinding.GraphNode,Pathfinding.GraphNode>[]
struct KeyValuePair_2U5BU5D_t1723948600  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1911225045  m_Items[1];

public:
	inline KeyValuePair_2_t1911225045  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1911225045 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1911225045  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,Pathfinding.AstarProfiler/ProfilePoint>[]
struct KeyValuePair_2U5BU5D_t2158993241  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1659289992  m_Items[1];

public:
	inline KeyValuePair_2_t1659289992  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1659289992 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1659289992  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<Pathfinding.GraphNode,System.Int32>[]
struct KeyValuePair_2U5BU5D_t4027347166  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3041451175  m_Items[1];

public:
	inline KeyValuePair_2_t3041451175  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3041451175 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3041451175  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<System.Single>[]
struct List_1U5BU5D_t3744122373  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1365137228 * m_Items[1];

public:
	inline List_1_t1365137228 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1365137228 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1365137228 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int32[][][]
struct Int32U5BU5DU5BU5DU5BU5D_t2084672417  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Int32U5BU5DU5BU5D_t1820556512* m_Items[1];

public:
	inline Int32U5BU5DU5BU5D_t1820556512* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Int32U5BU5DU5BU5D_t1820556512** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Int32U5BU5DU5BU5D_t1820556512* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int32[,]
struct Int32U5BU2CU5D_t3230847822  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
	inline int32_t GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, int32_t value)
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Type,Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>>[]
struct KeyValuePair_2U5BU5D_t2317217096  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t161279237  m_Items[1];

public:
	inline KeyValuePair_2_t161279237  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t161279237 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t161279237  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode>[]
struct List_1U5BU5D_t1999769355  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t3717937726 * m_Items[1];

public:
	inline List_1_t3717937726 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t3717937726 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t3717937726 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode>>[]
struct KeyValuePair_2U5BU5D_t1613809047  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t142169506  m_Items[1];

public:
	inline KeyValuePair_2_t142169506  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t142169506 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t142169506  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>[]
struct KeyValuePair_2U5BU5D_t2467292914  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t4131445027  m_Items[1];

public:
	inline KeyValuePair_2_t4131445027  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t4131445027 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t4131445027  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Linq.JToken,System.String>[]
struct KeyValuePair_2U5BU5D_t3500949758  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3820648199  m_Items[1];

public:
	inline KeyValuePair_2_t3820648199  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3820648199 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3820648199  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchema>[]
struct KeyValuePair_2U5BU5D_t718283822  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1179766679  m_Items[1];

public:
	inline KeyValuePair_2_t1179766679  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1179766679 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1179766679  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>[]
struct KeyValuePair_2U5BU5D_t3905785258  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t4184235723  m_Items[1];

public:
	inline KeyValuePair_2_t4184235723  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t4184235723 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t4184235723  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>[]
struct KeyValuePair_2U5BU5D_t2145300652  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2834614897  m_Items[1];

public:
	inline KeyValuePair_2_t2834614897  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2834614897 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2834614897  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Schema.JsonSchemaNode,Newtonsoft.Json.Schema.JsonSchemaModel>[]
struct KeyValuePair_2U5BU5D_t1028464539  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2673780974  m_Items[1];

public:
	inline KeyValuePair_2_t2673780974  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2673780974 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2673780974  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode>[]
struct KeyValuePair_2U5BU5D_t2587006404  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2834426169  m_Items[1];

public:
	inline KeyValuePair_2_t2834426169  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2834426169 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2834426169  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaModel>[]
struct KeyValuePair_2U5BU5D_t2296533131  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3754817214  m_Items[1];

public:
	inline KeyValuePair_2_t3754817214  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3754817214 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3754817214  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>[]
struct KeyValuePair_2U5BU5D_t669296016  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t4013503581  m_Items[1];

public:
	inline KeyValuePair_2_t4013503581  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t4013503581 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t4013503581  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.JsonProperty,System.Object>[]
struct KeyValuePair_2U5BU5D_t1618523602  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1873923011  m_Items[1];

public:
	inline KeyValuePair_2_t1873923011  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1873923011 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1873923011  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>[]
struct KeyValuePair_2U5BU5D_t1551469809  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t666182096  m_Items[1];

public:
	inline KeyValuePair_2_t666182096  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t666182096 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t666182096  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>[]
struct KeyValuePair_2U5BU5D_t686812631  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t595436130  m_Items[1];

public:
	inline KeyValuePair_2_t595436130  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t595436130 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t595436130  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.IList`1<System.Object>[]
struct IList_1U5BU5D_t809406931  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>[]
struct KeyValuePair_2U5BU5D_t2196911583  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2815902970  m_Items[1];

public:
	inline KeyValuePair_2_t2815902970  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2815902970 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2815902970  value)
	{
		m_Items[index] = value;
	}
};
// System.IEquatable`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>[]
struct IEquatable_1U5BU5D_t3356980296  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,BMGlyph>[]
struct KeyValuePair_2U5BU5D_t1332858447  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t615096650  m_Items[1];

public:
	inline KeyValuePair_2_t615096650  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t615096650 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t615096650  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,UICamera/MouseOrTouch>[]
struct KeyValuePair_2U5BU5D_t2737635187  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1953420278  m_Items[1];

public:
	inline KeyValuePair_2_t1953420278  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1953420278 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1953420278  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Int32>[]
struct KeyValuePair_2U5BU5D_t2811539127  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1272974082  m_Items[1];

public:
	inline KeyValuePair_2_t1272974082  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1272974082 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1272974082  value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<ProtoBuf.ProtoMemberAttribute>[]
struct IComparable_1U5BU5D_t4186788072  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,BossAnimationInfo>[]
struct KeyValuePair_2U5BU5D_t2394789147  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t280379758  m_Items[1];

public:
	inline KeyValuePair_2_t280379758  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t280379758 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t280379758  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Byte[]>[]
struct KeyValuePair_2U5BU5D_t2035345668  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t684992249  m_Items[1];

public:
	inline KeyValuePair_2_t684992249  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t684992249 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t684992249  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.UInt32,Mihua.Utils.WaitForSeconds>[]
struct KeyValuePair_2U5BU5D_t2552654818  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3113378547  m_Items[1];

public:
	inline KeyValuePair_2_t3113378547  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3113378547 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3113378547  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Collections.IEnumerator>[]
struct KeyValuePair_2U5BU5D_t3000632306  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3360505891  m_Items[1];

public:
	inline KeyValuePair_2_t3360505891  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3360505891 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3360505891  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,ChannelConfigCfg>[]
struct KeyValuePair_2U5BU5D_t2532496921  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t4169023944  m_Items[1];

public:
	inline KeyValuePair_2_t4169023944  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t4169023944 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t4169023944  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,HeroEmbattleCfg>[]
struct KeyValuePair_2U5BU5D_t3486441538  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t4030168595  m_Items[1];

public:
	inline KeyValuePair_2_t4030168595  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t4030168595 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t4030168595  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,JJCCoefficientCfg>[]
struct KeyValuePair_2U5BU5D_t3901453626  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2651095675  m_Items[1];

public:
	inline KeyValuePair_2_t2651095675  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2651095675 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2651095675  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,PetsCfg>[]
struct KeyValuePair_2U5BU5D_t790703460  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t884060697  m_Items[1];

public:
	inline KeyValuePair_2_t884060697  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t884060697 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t884060697  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,SoundCfg>[]
struct KeyValuePair_2U5BU5D_t1673450795  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1703319198  m_Items[1];

public:
	inline KeyValuePair_2_t1703319198  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1703319198 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1703319198  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,StoryCfg>[]
struct KeyValuePair_2U5BU5D_t3783417897  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1678415096  m_Items[1];

public:
	inline KeyValuePair_2_t1678415096  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1678415096 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1678415096  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,TextStringCfg>[]
struct KeyValuePair_2U5BU5D_t1944586230  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t780579183  m_Items[1];

public:
	inline KeyValuePair_2_t780579183  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t780579183 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t780579183  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,UIEffectCfg>[]
struct KeyValuePair_2U5BU5D_t561469017  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t848696968  m_Items[1];

public:
	inline KeyValuePair_2_t848696968  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t848696968 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t848696968  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,all_configCfg>[]
struct KeyValuePair_2U5BU5D_t2056134112  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3761112653  m_Items[1];

public:
	inline KeyValuePair_2_t3761112653  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3761112653 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3761112653  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,asset_sharedCfg>[]
struct KeyValuePair_2U5BU5D_t1763478436  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1797357785  m_Items[1];

public:
	inline KeyValuePair_2_t1797357785  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1797357785 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1797357785  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,barrierGateCfg>[]
struct KeyValuePair_2U5BU5D_t723987434  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3601132939  m_Items[1];

public:
	inline KeyValuePair_2_t3601132939  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3601132939 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3601132939  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,buffCfg>[]
struct KeyValuePair_2U5BU5D_t2444939039  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t124007610  m_Items[1];

public:
	inline KeyValuePair_2_t124007610  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t124007610 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t124007610  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,cameraShotCfg>[]
struct KeyValuePair_2U5BU5D_t1092817723  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t677201358  m_Items[1];

public:
	inline KeyValuePair_2_t677201358  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t677201358 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t677201358  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,checkpointCfg>[]
struct KeyValuePair_2U5BU5D_t946445672  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2712151909  m_Items[1];

public:
	inline KeyValuePair_2_t2712151909  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2712151909 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2712151909  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,demoStoryCfg>[]
struct KeyValuePair_2U5BU5D_t979334746  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1889206235  m_Items[1];

public:
	inline KeyValuePair_2_t1889206235  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1889206235 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1889206235  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,effectCfg>[]
struct KeyValuePair_2U5BU5D_t2594281173  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2722323132  m_Items[1];

public:
	inline KeyValuePair_2_t2722323132  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2722323132 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2722323132  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,elementCfg>[]
struct KeyValuePair_2U5BU5D_t869688748  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t471955825  m_Items[1];

public:
	inline KeyValuePair_2_t471955825  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t471955825 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t471955825  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,enemy_dropCfg>[]
struct KeyValuePair_2U5BU5D_t522292638  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t923004647  m_Items[1];

public:
	inline KeyValuePair_2_t923004647  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t923004647 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t923004647  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,friendNpcCfg>[]
struct KeyValuePair_2U5BU5D_t285956719  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3786354602  m_Items[1];

public:
	inline KeyValuePair_2_t3786354602  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3786354602 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3786354602  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,guideCfg>[]
struct KeyValuePair_2U5BU5D_t1414328748  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2877087089  m_Items[1];

public:
	inline KeyValuePair_2_t2877087089  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2877087089 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2877087089  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,herosCfg>[]
struct KeyValuePair_2U5BU5D_t2164271005  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3572978580  m_Items[1];

public:
	inline KeyValuePair_2_t3572978580  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3572978580 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3572978580  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,monstersCfg>[]
struct KeyValuePair_2U5BU5D_t3669435453  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1438440308  m_Items[1];

public:
	inline KeyValuePair_2_t1438440308  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1438440308 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1438440308  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,npcAICfg>[]
struct KeyValuePair_2U5BU5D_t2964465389  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1844374148  m_Items[1];

public:
	inline KeyValuePair_2_t1844374148  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1844374148 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1844374148  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,npcAIBehaviorCfg>[]
struct KeyValuePair_2U5BU5D_t3405110439  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3352780754  m_Items[1];

public:
	inline KeyValuePair_2_t3352780754  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3352780754 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3352780754  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,npcAIConditionCfg>[]
struct KeyValuePair_2U5BU5D_t4080841690  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t826241115  m_Items[1];

public:
	inline KeyValuePair_2_t826241115  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t826241115 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t826241115  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,portalCfg>[]
struct KeyValuePair_2U5BU5D_t3859914972  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1013078529  m_Items[1];

public:
	inline KeyValuePair_2_t1013078529  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1013078529 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1013078529  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,skillCfg>[]
struct KeyValuePair_2U5BU5D_t1836472981  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2038469116  m_Items[1];

public:
	inline KeyValuePair_2_t2038469116  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2038469116 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2038469116  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,skill_upgradeCfg>[]
struct KeyValuePair_2U5BU5D_t2649411622  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t686770431  m_Items[1];

public:
	inline KeyValuePair_2_t686770431  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t686770431 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t686770431  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,superskillCfg>[]
struct KeyValuePair_2U5BU5D_t1831945678  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2920175223  m_Items[1];

public:
	inline KeyValuePair_2_t2920175223  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2920175223 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2920175223  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,taixuTipCfg>[]
struct KeyValuePair_2U5BU5D_t3863475594  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1164766315  m_Items[1];

public:
	inline KeyValuePair_2_t1164766315  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1164766315 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1164766315  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,towerbuffCfg>[]
struct KeyValuePair_2U5BU5D_t1267407116  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1651900817  m_Items[1];

public:
	inline KeyValuePair_2_t1651900817  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1651900817 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1651900817  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,CameraAniMap>[]
struct KeyValuePair_2U5BU5D_t541789275  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2900699950  m_Items[1];

public:
	inline KeyValuePair_2_t2900699950  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2900699950 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2900699950  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,JSCLevelConfig>[]
struct KeyValuePair_2U5BU5D_t809081592  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1307143445  m_Items[1];

public:
	inline KeyValuePair_2_t1307143445  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1307143445 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1307143445  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,JSCMapPathConfig>[]
struct KeyValuePair_2U5BU5D_t1627025927  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3322162674  m_Items[1];

public:
	inline KeyValuePair_2_t3322162674  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3322162674 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3322162674  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,TabData>[]
struct KeyValuePair_2U5BU5D_t971419634  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t829752355  m_Items[1];

public:
	inline KeyValuePair_2_t829752355  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t829752355 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t829752355  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.Object>[]
struct KeyValuePair_2U5BU5D_t629175710  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3790677735  m_Items[1];

public:
	inline KeyValuePair_2_t3790677735  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3790677735 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3790677735  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>[]
struct KeyValuePair_2U5BU5D_t500293249  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1209901952  m_Items[1];

public:
	inline KeyValuePair_2_t1209901952  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1209901952 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1209901952  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>[]
struct KeyValuePair_2U5BU5D_t4249630371  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2104912006  m_Items[1];

public:
	inline KeyValuePair_2_t2104912006  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2104912006 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2104912006  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<AIEnum.EAIEventtype,System.Object>[]
struct KeyValuePair_2U5BU5D_t3174887608  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3529636437  m_Items[1];

public:
	inline KeyValuePair_2_t3529636437  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3529636437 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3529636437  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<AIEventInfo>[]
struct List_1U5BU5D_t1953614929  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1104940528 * m_Items[1];

public:
	inline List_1_t1104940528 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1104940528 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1104940528 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>[]
struct KeyValuePair_2U5BU5D_t4019846055  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t463760594  m_Items[1];

public:
	inline KeyValuePair_2_t463760594  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t463760594 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t463760594  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<ReplayBase>[]
struct List_1U5BU5D_t2162370457  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t2147888712 * m_Items[1];

public:
	inline List_1_t2147888712 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t2147888712 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t2147888712 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<ReplayBase>>[]
struct KeyValuePair_2U5BU5D_t1776410149  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2867087788  m_Items[1];

public:
	inline KeyValuePair_2_t2867087788  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2867087788 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2867087788  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject>[]
struct KeyValuePair_2U5BU5D_t2276148740  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t98913785  m_Items[1];

public:
	inline KeyValuePair_2_t98913785  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t98913785 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t98913785  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,ElementVO>[]
struct KeyValuePair_2U5BU5D_t2211169547  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1641495614  m_Items[1];

public:
	inline KeyValuePair_2_t1641495614  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1641495614 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1641495614  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>[]
struct KeyValuePair_2U5BU5D_t2495925505  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1199798016  m_Items[1];

public:
	inline KeyValuePair_2_t1199798016  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1199798016 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1199798016  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<CombatEntity,HatredCtrl/stValue>[]
struct KeyValuePair_2U5BU5D_t2250146309  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3657452364  m_Items[1];

public:
	inline KeyValuePair_2_t3657452364  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3657452364 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3657452364  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.WeakReference>[]
struct KeyValuePair_2U5BU5D_t2311659648  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2918678573  m_Items[1];

public:
	inline KeyValuePair_2_t2918678573  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2918678573 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2918678573  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>[]
struct KeyValuePair_2U5BU5D_t3691088287  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2065771578  m_Items[1];

public:
	inline KeyValuePair_2_t2065771578  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2065771578 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2065771578  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<CombatEntity,System.Single>[]
struct KeyValuePair_2U5BU5D_t3445309091  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t228458630  m_Items[1];

public:
	inline KeyValuePair_2_t228458630  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t228458630 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t228458630  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,UIFont>[]
struct KeyValuePair_2U5BU5D_t3568491038  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3222289511  m_Items[1];

public:
	inline KeyValuePair_2_t3222289511  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3222289511 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3222289511  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>[]
struct KeyValuePair_2U5BU5D_t212833192  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t4187962917  m_Items[1];

public:
	inline KeyValuePair_2_t4187962917  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t4187962917 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t4187962917  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,JSCWaveNpcConfig>[]
struct KeyValuePair_2U5BU5D_t1849588932  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t4117548601  m_Items[1];

public:
	inline KeyValuePair_2_t4117548601  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t4117548601 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t4117548601  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<CombatEntity,TargetMgr/FightPoint>[]
struct KeyValuePair_2U5BU5D_t4087239687  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t152597490  m_Items[1];

public:
	inline KeyValuePair_2_t152597490  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t152597490 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t152597490  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<TargetMgr/AngleEntity>[]
struct List_1U5BU5D_t4169348331  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t267346398 * m_Items[1];

public:
	inline List_1_t267346398 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t267346398 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t267346398 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Collections.Generic.List`1<TargetMgr/AngleEntity>>[]
struct KeyValuePair_2U5BU5D_t2065617534  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t163390343  m_Items[1];

public:
	inline KeyValuePair_2_t163390343  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t163390343 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t163390343  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,asset_sharedCfg>[]
struct KeyValuePair_2U5BU5D_t3481248925  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2620512916  m_Items[1];

public:
	inline KeyValuePair_2_t2620512916  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2620512916 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2620512916  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.AssetBundle>[]
struct KeyValuePair_2U5BU5D_t3137476133  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2790158764  m_Items[1];

public:
	inline KeyValuePair_2_t2790158764  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2790158764 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2790158764  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>[]
struct KeyValuePair_2U5BU5D_t1578592562  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2061784035  m_Items[1];

public:
	inline KeyValuePair_2_t2061784035  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2061784035 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2061784035  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>[]
struct KeyValuePair_2U5BU5D_t204068264  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t4287931429  m_Items[1];

public:
	inline KeyValuePair_2_t4287931429  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t4287931429 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t4287931429  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>[]
struct KeyValuePair_2U5BU5D_t4113075252  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t712163209  m_Items[1];

public:
	inline KeyValuePair_2_t712163209  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t712163209 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t712163209  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,FileInfoRes>[]
struct KeyValuePair_2U5BU5D_t3991024287  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1936258874  m_Items[1];

public:
	inline KeyValuePair_2_t1936258874  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1936258874 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1936258874  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>[]
struct KeyValuePair_2U5BU5D_t4239437001  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t407833816  m_Items[1];

public:
	inline KeyValuePair_2_t407833816  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t407833816 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t407833816  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>[]
struct KeyValuePair_2U5BU5D_t2478952395  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3353180286  m_Items[1];

public:
	inline KeyValuePair_2_t3353180286  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3353180286 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3353180286  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.UInt32,CSHeroUnit>[]
struct KeyValuePair_2U5BU5D_t3199666863  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3660289130  m_Items[1];

public:
	inline KeyValuePair_2_t3660289130  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3660289130 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3660289130  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,CSPlusAtt>[]
struct KeyValuePair_2U5BU5D_t2889986106  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3987514235  m_Items[1];

public:
	inline KeyValuePair_2_t3987514235  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3987514235 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3987514235  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<EffectCtrl>[]
struct List_1U5BU5D_t4153803205  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t782005900 * m_Items[1];

public:
	inline List_1_t782005900 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t782005900 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t782005900 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Collections.Generic.List`1<EffectCtrl>>[]
struct KeyValuePair_2U5BU5D_t2050072408  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t678049845  m_Items[1];

public:
	inline KeyValuePair_2_t678049845  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t678049845 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t678049845  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<FLOAT_TEXT_ID,System.Object>[]
struct KeyValuePair_2U5BU5D_t504207819  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t25818750  m_Items[1];

public:
	inline KeyValuePair_2_t25818750  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t25818750 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t25818750  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<FloatTextUnit>[]
struct List_1U5BU5D_t3336706032  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t3730483581 * m_Items[1];

public:
	inline List_1_t3730483581 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t3730483581 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t3730483581 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>[]
struct KeyValuePair_2U5BU5D_t2732257369  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3880453256  m_Items[1];

public:
	inline KeyValuePair_2_t3880453256  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3880453256 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3880453256  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,StaticParticle>[]
struct KeyValuePair_2U5BU5D_t1812215568  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2102583517  m_Items[1];

public:
	inline KeyValuePair_2_t2102583517  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2102583517 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2102583517  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,ProductsCfgMgr/ProductInfo>[]
struct KeyValuePair_2U5BU5D_t1556865901  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3374811140  m_Items[1];

public:
	inline KeyValuePair_2_t3374811140  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3374811140 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3374811140  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,ProductsCfgMgr/ProductInfo>[]
struct KeyValuePair_2U5BU5D_t4091348591  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2025190314  m_Items[1];

public:
	inline KeyValuePair_2_t2025190314  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2025190314 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2025190314  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<SoundTypeID,System.Object>[]
struct KeyValuePair_2U5BU5D_t71756021  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1947212572  m_Items[1];

public:
	inline KeyValuePair_2_t1947212572  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1947212572 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1947212572  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<SoundTypeID,ISound>[]
struct KeyValuePair_2U5BU5D_t313186678  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t4241366511  m_Items[1];

public:
	inline KeyValuePair_2_t4241366511  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t4241366511 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t4241366511  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>[]
struct KeyValuePair_2U5BU5D_t1889225647  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3488982890  m_Items[1];

public:
	inline KeyValuePair_2_t3488982890  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3488982890 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3488982890  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Single>[]
struct KeyValuePair_2U5BU5D_t1930603681  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t716150752  m_Items[1];

public:
	inline KeyValuePair_2_t716150752  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t716150752 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t716150752  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,AssetDownMgr/SetBytes>[]
struct KeyValuePair_2U5BU5D_t3337822458  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2752180667  m_Items[1];

public:
	inline KeyValuePair_2_t2752180667  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2752180667 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2752180667  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<FileInfoRes>[]
struct List_1U5BU5D_t1509575683  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t2585245350 * m_Items[1];

public:
	inline List_1_t2585245350 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t2585245350 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t2585245350 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Collections.Generic.List`1<FileInfoRes>>[]
struct KeyValuePair_2U5BU5D_t3700812182  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2481289295  m_Items[1];

public:
	inline KeyValuePair_2_t2481289295  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2481289295 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2481289295  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>[]
struct KeyValuePair_2U5BU5D_t390341163  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2851311006  m_Items[1];

public:
	inline KeyValuePair_2_t2851311006  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2851311006 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2851311006  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>[]
struct KeyValuePair_2U5BU5D_t2563117120  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1573321581  m_Items[1];

public:
	inline KeyValuePair_2_t1573321581  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1573321581 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1573321581  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,UIModelDisplay>[]
struct KeyValuePair_2U5BU5D_t4222391806  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3427993095  m_Items[1];

public:
	inline KeyValuePair_2_t3427993095  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3427993095 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3427993095  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>[]
struct KeyValuePair_2U5BU5D_t2176173772  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2093487825  m_Items[1];

public:
	inline KeyValuePair_2_t2093487825  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2093487825 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2093487825  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.UInt16>[]
struct KeyValuePair_2U5BU5D_t415689166  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t743866999  m_Items[1];

public:
	inline KeyValuePair_2_t743866999  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t743866999 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t743866999  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>[]
struct KeyValuePair_2U5BU5D_t794066828  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1637661969  m_Items[1];

public:
	inline KeyValuePair_2_t1637661969  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1637661969 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1637661969  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.UInt16,System.String>[]
struct KeyValuePair_2U5BU5D_t3739413298  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1769044451  m_Items[1];

public:
	inline KeyValuePair_2_t1769044451  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1769044451 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1769044451  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.UInt32,System.String>[]
struct KeyValuePair_2U5BU5D_t1616467292  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t4198129537  m_Items[1];

public:
	inline KeyValuePair_2_t4198129537  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t4198129537 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t4198129537  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<PushType,System.Object>[]
struct KeyValuePair_2U5BU5D_t3888371045  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t519615340  m_Items[1];

public:
	inline KeyValuePair_2_t519615340  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t519615340 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t519615340  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<PushType,PluginPush/NotificationData>[]
struct KeyValuePair_2U5BU5D_t1243947441  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3933281296  m_Items[1];

public:
	inline KeyValuePair_2_t3933281296  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3933281296 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3933281296  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t64592219  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t4153532974  m_Items[1];

public:
	inline KeyValuePair_2_t4153532974  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t4153532974 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t4153532974  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,UpdateForJs>[]
struct KeyValuePair_2U5BU5D_t1596963015  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t967237170  m_Items[1];

public:
	inline KeyValuePair_2_t967237170  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t967237170 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t967237170  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.Object>[]
struct KeyValuePair_2U5BU5D_t3343697396  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t4204345545  m_Items[1];

public:
	inline KeyValuePair_2_t4204345545  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t4204345545 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t4204345545  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.String>[]
struct KeyValuePair_2U5BU5D_t1994076570  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t40760731  m_Items[1];

public:
	inline KeyValuePair_2_t40760731  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t40760731 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t40760731  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<MScrollView/MoveWay,System.Object>[]
struct KeyValuePair_2U5BU5D_t1493525295  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1341048298  m_Items[1];

public:
	inline KeyValuePair_2_t1341048298  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1341048298 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1341048298  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>[]
struct KeyValuePair_2U5BU5D_t885026604  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t706219505  m_Items[1];

public:
	inline KeyValuePair_2_t706219505  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t706219505 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t706219505  value)
	{
		m_Items[index] = value;
	}
};
