﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>,System.Boolean>
struct Func_2_t212717111;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22545618620.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m3426930465_gshared (Func_2_t212717111 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_2__ctor_m3426930465(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t212717111 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3426930465_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>,System.Boolean>::Invoke(T)
extern "C"  bool Func_2_Invoke_m2666468261_gshared (Func_2_t212717111 * __this, KeyValuePair_2_t2545618620  ___arg10, const MethodInfo* method);
#define Func_2_Invoke_m2666468261(__this, ___arg10, method) ((  bool (*) (Func_2_t212717111 *, KeyValuePair_2_t2545618620 , const MethodInfo*))Func_2_Invoke_m2666468261_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m2630802776_gshared (Func_2_t212717111 * __this, KeyValuePair_2_t2545618620  ___arg10, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Func_2_BeginInvoke_m2630802776(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t212717111 *, KeyValuePair_2_t2545618620 , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m2630802776_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_2_EndInvoke_m1011898703_gshared (Func_2_t212717111 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_2_EndInvoke_m1011898703(__this, ___result0, method) ((  bool (*) (Func_2_t212717111 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m1011898703_gshared)(__this, ___result0, method)
