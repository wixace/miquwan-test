﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.EncodingInfo
struct EncodingInfo_t1898473639;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Text.EncodingInfo::.ctor(System.Int32)
extern "C"  void EncodingInfo__ctor_m2085233792 (EncodingInfo_t1898473639 * __this, int32_t ___cp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.EncodingInfo::Equals(System.Object)
extern "C"  bool EncodingInfo_Equals_m1504812574 (EncodingInfo_t1898473639 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.EncodingInfo::GetHashCode()
extern "C"  int32_t EncodingInfo_GetHashCode_m3965017986 (EncodingInfo_t1898473639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
