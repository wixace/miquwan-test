﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// effectCfg
struct effectCfg_t2826279187;
// System.String
struct String_t;
// System.Collections.Generic.List`1<IEffComponent>
struct List_1_t875483217;
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t1536434148;
// UnityEngine.Animation
struct Animation_t1724966010;
// UnityEngine.Animator[]
struct AnimatorU5BU5D_t618511498;
// UnityEngine.TrailRenderer
struct TrailRenderer_t2401074527;
// UnityEngine.Transform
struct Transform_t1659122786;
// ActorLine
struct ActorLine_t2375805801;
// ActorBullet
struct ActorBullet_t2246481719;
// LoopEffect
struct LoopEffect_t2053225845;
// TimeEffect
struct TimeEffect_t2372650718;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectCtrl
struct  EffectCtrl_t3708787644  : public Il2CppObject
{
public:
	// System.Int32 EffectCtrl::id
	int32_t ___id_0;
	// System.Int32 EffectCtrl::CacheId
	int32_t ___CacheId_1;
	// System.Boolean EffectCtrl::isFree
	bool ___isFree_2;
	// effectCfg EffectCtrl::effectVO
	effectCfg_t2826279187 * ___effectVO_3;
	// System.Int32 EffectCtrl::instanceID
	int32_t ___instanceID_4;
	// System.String EffectCtrl::name
	String_t* ___name_5;
	// System.Int32 EffectCtrl::srcID
	int32_t ___srcID_6;
	// System.Single EffectCtrl::startTime
	float ___startTime_7;
	// System.Collections.Generic.List`1<IEffComponent> EffectCtrl::effComList
	List_1_t875483217 * ___effComList_8;
	// UnityEngine.Vector3 EffectCtrl::scale
	Vector3_t4282066566  ___scale_9;
	// System.Single EffectCtrl::targetScale
	float ___targetScale_10;
	// System.UInt32 EffectCtrl::soundDelayID
	uint32_t ___soundDelayID_11;
	// System.Single EffectCtrl::playSpeed
	float ___playSpeed_12;
	// System.Single EffectCtrl::curPlaySpeed
	float ___curPlaySpeed_13;
	// UnityEngine.ParticleSystem[] EffectCtrl::particleSystems
	ParticleSystemU5BU5D_t1536434148* ___particleSystems_14;
	// UnityEngine.Animation EffectCtrl::animation
	Animation_t1724966010 * ___animation_15;
	// UnityEngine.Animator[] EffectCtrl::animator
	AnimatorU5BU5D_t618511498* ___animator_16;
	// System.Single EffectCtrl::externalChange
	float ___externalChange_17;
	// UnityEngine.TrailRenderer EffectCtrl::trailrendere
	TrailRenderer_t2401074527 * ___trailrendere_18;
	// System.Boolean EffectCtrl::isTrail
	bool ___isTrail_19;
	// System.Single EffectCtrl::trailtime
	float ___trailtime_20;
	// UnityEngine.Transform EffectCtrl::trailTr
	Transform_t1659122786 * ___trailTr_21;
	// ActorLine EffectCtrl::<actorLine>k__BackingField
	ActorLine_t2375805801 * ___U3CactorLineU3Ek__BackingField_22;
	// ActorBullet EffectCtrl::<actorBullet>k__BackingField
	ActorBullet_t2246481719 * ___U3CactorBulletU3Ek__BackingField_23;
	// LoopEffect EffectCtrl::<loopEffect>k__BackingField
	LoopEffect_t2053225845 * ___U3CloopEffectU3Ek__BackingField_24;
	// TimeEffect EffectCtrl::<timeEffect>k__BackingField
	TimeEffect_t2372650718 * ___U3CtimeEffectU3Ek__BackingField_25;
	// UnityEngine.GameObject EffectCtrl::<effGO>k__BackingField
	GameObject_t3674682005 * ___U3CeffGOU3Ek__BackingField_26;
	// UnityEngine.Transform EffectCtrl::<effTr>k__BackingField
	Transform_t1659122786 * ___U3CeffTrU3Ek__BackingField_27;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_CacheId_1() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___CacheId_1)); }
	inline int32_t get_CacheId_1() const { return ___CacheId_1; }
	inline int32_t* get_address_of_CacheId_1() { return &___CacheId_1; }
	inline void set_CacheId_1(int32_t value)
	{
		___CacheId_1 = value;
	}

	inline static int32_t get_offset_of_isFree_2() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___isFree_2)); }
	inline bool get_isFree_2() const { return ___isFree_2; }
	inline bool* get_address_of_isFree_2() { return &___isFree_2; }
	inline void set_isFree_2(bool value)
	{
		___isFree_2 = value;
	}

	inline static int32_t get_offset_of_effectVO_3() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___effectVO_3)); }
	inline effectCfg_t2826279187 * get_effectVO_3() const { return ___effectVO_3; }
	inline effectCfg_t2826279187 ** get_address_of_effectVO_3() { return &___effectVO_3; }
	inline void set_effectVO_3(effectCfg_t2826279187 * value)
	{
		___effectVO_3 = value;
		Il2CppCodeGenWriteBarrier(&___effectVO_3, value);
	}

	inline static int32_t get_offset_of_instanceID_4() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___instanceID_4)); }
	inline int32_t get_instanceID_4() const { return ___instanceID_4; }
	inline int32_t* get_address_of_instanceID_4() { return &___instanceID_4; }
	inline void set_instanceID_4(int32_t value)
	{
		___instanceID_4 = value;
	}

	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier(&___name_5, value);
	}

	inline static int32_t get_offset_of_srcID_6() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___srcID_6)); }
	inline int32_t get_srcID_6() const { return ___srcID_6; }
	inline int32_t* get_address_of_srcID_6() { return &___srcID_6; }
	inline void set_srcID_6(int32_t value)
	{
		___srcID_6 = value;
	}

	inline static int32_t get_offset_of_startTime_7() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___startTime_7)); }
	inline float get_startTime_7() const { return ___startTime_7; }
	inline float* get_address_of_startTime_7() { return &___startTime_7; }
	inline void set_startTime_7(float value)
	{
		___startTime_7 = value;
	}

	inline static int32_t get_offset_of_effComList_8() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___effComList_8)); }
	inline List_1_t875483217 * get_effComList_8() const { return ___effComList_8; }
	inline List_1_t875483217 ** get_address_of_effComList_8() { return &___effComList_8; }
	inline void set_effComList_8(List_1_t875483217 * value)
	{
		___effComList_8 = value;
		Il2CppCodeGenWriteBarrier(&___effComList_8, value);
	}

	inline static int32_t get_offset_of_scale_9() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___scale_9)); }
	inline Vector3_t4282066566  get_scale_9() const { return ___scale_9; }
	inline Vector3_t4282066566 * get_address_of_scale_9() { return &___scale_9; }
	inline void set_scale_9(Vector3_t4282066566  value)
	{
		___scale_9 = value;
	}

	inline static int32_t get_offset_of_targetScale_10() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___targetScale_10)); }
	inline float get_targetScale_10() const { return ___targetScale_10; }
	inline float* get_address_of_targetScale_10() { return &___targetScale_10; }
	inline void set_targetScale_10(float value)
	{
		___targetScale_10 = value;
	}

	inline static int32_t get_offset_of_soundDelayID_11() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___soundDelayID_11)); }
	inline uint32_t get_soundDelayID_11() const { return ___soundDelayID_11; }
	inline uint32_t* get_address_of_soundDelayID_11() { return &___soundDelayID_11; }
	inline void set_soundDelayID_11(uint32_t value)
	{
		___soundDelayID_11 = value;
	}

	inline static int32_t get_offset_of_playSpeed_12() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___playSpeed_12)); }
	inline float get_playSpeed_12() const { return ___playSpeed_12; }
	inline float* get_address_of_playSpeed_12() { return &___playSpeed_12; }
	inline void set_playSpeed_12(float value)
	{
		___playSpeed_12 = value;
	}

	inline static int32_t get_offset_of_curPlaySpeed_13() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___curPlaySpeed_13)); }
	inline float get_curPlaySpeed_13() const { return ___curPlaySpeed_13; }
	inline float* get_address_of_curPlaySpeed_13() { return &___curPlaySpeed_13; }
	inline void set_curPlaySpeed_13(float value)
	{
		___curPlaySpeed_13 = value;
	}

	inline static int32_t get_offset_of_particleSystems_14() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___particleSystems_14)); }
	inline ParticleSystemU5BU5D_t1536434148* get_particleSystems_14() const { return ___particleSystems_14; }
	inline ParticleSystemU5BU5D_t1536434148** get_address_of_particleSystems_14() { return &___particleSystems_14; }
	inline void set_particleSystems_14(ParticleSystemU5BU5D_t1536434148* value)
	{
		___particleSystems_14 = value;
		Il2CppCodeGenWriteBarrier(&___particleSystems_14, value);
	}

	inline static int32_t get_offset_of_animation_15() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___animation_15)); }
	inline Animation_t1724966010 * get_animation_15() const { return ___animation_15; }
	inline Animation_t1724966010 ** get_address_of_animation_15() { return &___animation_15; }
	inline void set_animation_15(Animation_t1724966010 * value)
	{
		___animation_15 = value;
		Il2CppCodeGenWriteBarrier(&___animation_15, value);
	}

	inline static int32_t get_offset_of_animator_16() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___animator_16)); }
	inline AnimatorU5BU5D_t618511498* get_animator_16() const { return ___animator_16; }
	inline AnimatorU5BU5D_t618511498** get_address_of_animator_16() { return &___animator_16; }
	inline void set_animator_16(AnimatorU5BU5D_t618511498* value)
	{
		___animator_16 = value;
		Il2CppCodeGenWriteBarrier(&___animator_16, value);
	}

	inline static int32_t get_offset_of_externalChange_17() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___externalChange_17)); }
	inline float get_externalChange_17() const { return ___externalChange_17; }
	inline float* get_address_of_externalChange_17() { return &___externalChange_17; }
	inline void set_externalChange_17(float value)
	{
		___externalChange_17 = value;
	}

	inline static int32_t get_offset_of_trailrendere_18() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___trailrendere_18)); }
	inline TrailRenderer_t2401074527 * get_trailrendere_18() const { return ___trailrendere_18; }
	inline TrailRenderer_t2401074527 ** get_address_of_trailrendere_18() { return &___trailrendere_18; }
	inline void set_trailrendere_18(TrailRenderer_t2401074527 * value)
	{
		___trailrendere_18 = value;
		Il2CppCodeGenWriteBarrier(&___trailrendere_18, value);
	}

	inline static int32_t get_offset_of_isTrail_19() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___isTrail_19)); }
	inline bool get_isTrail_19() const { return ___isTrail_19; }
	inline bool* get_address_of_isTrail_19() { return &___isTrail_19; }
	inline void set_isTrail_19(bool value)
	{
		___isTrail_19 = value;
	}

	inline static int32_t get_offset_of_trailtime_20() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___trailtime_20)); }
	inline float get_trailtime_20() const { return ___trailtime_20; }
	inline float* get_address_of_trailtime_20() { return &___trailtime_20; }
	inline void set_trailtime_20(float value)
	{
		___trailtime_20 = value;
	}

	inline static int32_t get_offset_of_trailTr_21() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___trailTr_21)); }
	inline Transform_t1659122786 * get_trailTr_21() const { return ___trailTr_21; }
	inline Transform_t1659122786 ** get_address_of_trailTr_21() { return &___trailTr_21; }
	inline void set_trailTr_21(Transform_t1659122786 * value)
	{
		___trailTr_21 = value;
		Il2CppCodeGenWriteBarrier(&___trailTr_21, value);
	}

	inline static int32_t get_offset_of_U3CactorLineU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___U3CactorLineU3Ek__BackingField_22)); }
	inline ActorLine_t2375805801 * get_U3CactorLineU3Ek__BackingField_22() const { return ___U3CactorLineU3Ek__BackingField_22; }
	inline ActorLine_t2375805801 ** get_address_of_U3CactorLineU3Ek__BackingField_22() { return &___U3CactorLineU3Ek__BackingField_22; }
	inline void set_U3CactorLineU3Ek__BackingField_22(ActorLine_t2375805801 * value)
	{
		___U3CactorLineU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CactorLineU3Ek__BackingField_22, value);
	}

	inline static int32_t get_offset_of_U3CactorBulletU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___U3CactorBulletU3Ek__BackingField_23)); }
	inline ActorBullet_t2246481719 * get_U3CactorBulletU3Ek__BackingField_23() const { return ___U3CactorBulletU3Ek__BackingField_23; }
	inline ActorBullet_t2246481719 ** get_address_of_U3CactorBulletU3Ek__BackingField_23() { return &___U3CactorBulletU3Ek__BackingField_23; }
	inline void set_U3CactorBulletU3Ek__BackingField_23(ActorBullet_t2246481719 * value)
	{
		___U3CactorBulletU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CactorBulletU3Ek__BackingField_23, value);
	}

	inline static int32_t get_offset_of_U3CloopEffectU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___U3CloopEffectU3Ek__BackingField_24)); }
	inline LoopEffect_t2053225845 * get_U3CloopEffectU3Ek__BackingField_24() const { return ___U3CloopEffectU3Ek__BackingField_24; }
	inline LoopEffect_t2053225845 ** get_address_of_U3CloopEffectU3Ek__BackingField_24() { return &___U3CloopEffectU3Ek__BackingField_24; }
	inline void set_U3CloopEffectU3Ek__BackingField_24(LoopEffect_t2053225845 * value)
	{
		___U3CloopEffectU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CloopEffectU3Ek__BackingField_24, value);
	}

	inline static int32_t get_offset_of_U3CtimeEffectU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___U3CtimeEffectU3Ek__BackingField_25)); }
	inline TimeEffect_t2372650718 * get_U3CtimeEffectU3Ek__BackingField_25() const { return ___U3CtimeEffectU3Ek__BackingField_25; }
	inline TimeEffect_t2372650718 ** get_address_of_U3CtimeEffectU3Ek__BackingField_25() { return &___U3CtimeEffectU3Ek__BackingField_25; }
	inline void set_U3CtimeEffectU3Ek__BackingField_25(TimeEffect_t2372650718 * value)
	{
		___U3CtimeEffectU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtimeEffectU3Ek__BackingField_25, value);
	}

	inline static int32_t get_offset_of_U3CeffGOU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___U3CeffGOU3Ek__BackingField_26)); }
	inline GameObject_t3674682005 * get_U3CeffGOU3Ek__BackingField_26() const { return ___U3CeffGOU3Ek__BackingField_26; }
	inline GameObject_t3674682005 ** get_address_of_U3CeffGOU3Ek__BackingField_26() { return &___U3CeffGOU3Ek__BackingField_26; }
	inline void set_U3CeffGOU3Ek__BackingField_26(GameObject_t3674682005 * value)
	{
		___U3CeffGOU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier(&___U3CeffGOU3Ek__BackingField_26, value);
	}

	inline static int32_t get_offset_of_U3CeffTrU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(EffectCtrl_t3708787644, ___U3CeffTrU3Ek__BackingField_27)); }
	inline Transform_t1659122786 * get_U3CeffTrU3Ek__BackingField_27() const { return ___U3CeffTrU3Ek__BackingField_27; }
	inline Transform_t1659122786 ** get_address_of_U3CeffTrU3Ek__BackingField_27() { return &___U3CeffTrU3Ek__BackingField_27; }
	inline void set_U3CeffTrU3Ek__BackingField_27(Transform_t1659122786 * value)
	{
		___U3CeffTrU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CeffTrU3Ek__BackingField_27, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
