﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Pathfinding_PathModifier2857972114.h"
#include "AssemblyU2DCSharp_Pathfinding_StartEndModifier_Exa3849634337.h"
#include "UnityEngine_UnityEngine_LayerMask3236759763.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.StartEndModifier
struct  StartEndModifier_t2926562694  : public PathModifier_t2857972114
{
public:
	// System.Boolean Pathfinding.StartEndModifier::addPoints
	bool ___addPoints_2;
	// Pathfinding.StartEndModifier/Exactness Pathfinding.StartEndModifier::exactStartPoint
	int32_t ___exactStartPoint_3;
	// Pathfinding.StartEndModifier/Exactness Pathfinding.StartEndModifier::exactEndPoint
	int32_t ___exactEndPoint_4;
	// System.Boolean Pathfinding.StartEndModifier::useRaycasting
	bool ___useRaycasting_5;
	// UnityEngine.LayerMask Pathfinding.StartEndModifier::mask
	LayerMask_t3236759763  ___mask_6;
	// System.Boolean Pathfinding.StartEndModifier::useGraphRaycasting
	bool ___useGraphRaycasting_7;

public:
	inline static int32_t get_offset_of_addPoints_2() { return static_cast<int32_t>(offsetof(StartEndModifier_t2926562694, ___addPoints_2)); }
	inline bool get_addPoints_2() const { return ___addPoints_2; }
	inline bool* get_address_of_addPoints_2() { return &___addPoints_2; }
	inline void set_addPoints_2(bool value)
	{
		___addPoints_2 = value;
	}

	inline static int32_t get_offset_of_exactStartPoint_3() { return static_cast<int32_t>(offsetof(StartEndModifier_t2926562694, ___exactStartPoint_3)); }
	inline int32_t get_exactStartPoint_3() const { return ___exactStartPoint_3; }
	inline int32_t* get_address_of_exactStartPoint_3() { return &___exactStartPoint_3; }
	inline void set_exactStartPoint_3(int32_t value)
	{
		___exactStartPoint_3 = value;
	}

	inline static int32_t get_offset_of_exactEndPoint_4() { return static_cast<int32_t>(offsetof(StartEndModifier_t2926562694, ___exactEndPoint_4)); }
	inline int32_t get_exactEndPoint_4() const { return ___exactEndPoint_4; }
	inline int32_t* get_address_of_exactEndPoint_4() { return &___exactEndPoint_4; }
	inline void set_exactEndPoint_4(int32_t value)
	{
		___exactEndPoint_4 = value;
	}

	inline static int32_t get_offset_of_useRaycasting_5() { return static_cast<int32_t>(offsetof(StartEndModifier_t2926562694, ___useRaycasting_5)); }
	inline bool get_useRaycasting_5() const { return ___useRaycasting_5; }
	inline bool* get_address_of_useRaycasting_5() { return &___useRaycasting_5; }
	inline void set_useRaycasting_5(bool value)
	{
		___useRaycasting_5 = value;
	}

	inline static int32_t get_offset_of_mask_6() { return static_cast<int32_t>(offsetof(StartEndModifier_t2926562694, ___mask_6)); }
	inline LayerMask_t3236759763  get_mask_6() const { return ___mask_6; }
	inline LayerMask_t3236759763 * get_address_of_mask_6() { return &___mask_6; }
	inline void set_mask_6(LayerMask_t3236759763  value)
	{
		___mask_6 = value;
	}

	inline static int32_t get_offset_of_useGraphRaycasting_7() { return static_cast<int32_t>(offsetof(StartEndModifier_t2926562694, ___useGraphRaycasting_7)); }
	inline bool get_useGraphRaycasting_7() const { return ___useGraphRaycasting_7; }
	inline bool* get_address_of_useGraphRaycasting_7() { return &___useGraphRaycasting_7; }
	inline void set_useGraphRaycasting_7(bool value)
	{
		___useGraphRaycasting_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
