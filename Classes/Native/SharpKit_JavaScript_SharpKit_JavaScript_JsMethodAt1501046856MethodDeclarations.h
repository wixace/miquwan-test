﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SharpKit.JavaScript.JsMethodAttribute
struct JsMethodAttribute_t1501046856;
// System.String
struct String_t;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Type2863145774.h"

// System.Void SharpKit.JavaScript.JsMethodAttribute::set_NativeParams(System.Boolean)
extern "C"  void JsMethodAttribute_set_NativeParams_m3635227870 (JsMethodAttribute_t1501046856 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsMethodAttribute::set_TargetMethod(System.String)
extern "C"  void JsMethodAttribute_set_TargetMethod_m120501446 (JsMethodAttribute_t1501046856 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsMethodAttribute::set_TargetType(System.Type)
extern "C"  void JsMethodAttribute_set_TargetType_m3033924100 (JsMethodAttribute_t1501046856 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsMethodAttribute::set_Name(System.String)
extern "C"  void JsMethodAttribute_set_Name_m1404384749 (JsMethodAttribute_t1501046856 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsMethodAttribute::set_NativeOverloads(System.Boolean)
extern "C"  void JsMethodAttribute_set_NativeOverloads_m329298961 (JsMethodAttribute_t1501046856 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsMethodAttribute::set_IgnoreGenericArguments(System.Boolean)
extern "C"  void JsMethodAttribute_set_IgnoreGenericArguments_m3456535634 (JsMethodAttribute_t1501046856 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsMethodAttribute::set_Global(System.Boolean)
extern "C"  void JsMethodAttribute_set_Global_m735858372 (JsMethodAttribute_t1501046856 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsMethodAttribute::set_Export(System.Boolean)
extern "C"  void JsMethodAttribute_set_Export_m2722372821 (JsMethodAttribute_t1501046856 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsMethodAttribute::.ctor()
extern "C"  void JsMethodAttribute__ctor_m1275070327 (JsMethodAttribute_t1501046856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
