﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Int32,System.Int32>
struct U3CDelayCallExecU3Ec__Iterator40_2_t197972780;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Int32,System.Int32>::.ctor()
extern "C"  void U3CDelayCallExecU3Ec__Iterator40_2__ctor_m1962563780_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t197972780 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2__ctor_m1962563780(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator40_2_t197972780 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2__ctor_m1962563780_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Int32,System.Int32>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1434486286_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t197972780 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1434486286(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator40_2_t197972780 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1434486286_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_IEnumerator_get_Current_m4134299042_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t197972780 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_IEnumerator_get_Current_m4134299042(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator40_2_t197972780 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_IEnumerator_get_Current_m4134299042_gshared)(__this, method)
// System.Boolean BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Int32,System.Int32>::MoveNext()
extern "C"  bool U3CDelayCallExecU3Ec__Iterator40_2_MoveNext_m3288998576_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t197972780 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2_MoveNext_m3288998576(__this, method) ((  bool (*) (U3CDelayCallExecU3Ec__Iterator40_2_t197972780 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2_MoveNext_m3288998576_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Int32,System.Int32>::Dispose()
extern "C"  void U3CDelayCallExecU3Ec__Iterator40_2_Dispose_m431432705_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t197972780 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2_Dispose_m431432705(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator40_2_t197972780 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2_Dispose_m431432705_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Int32,System.Int32>::Reset()
extern "C"  void U3CDelayCallExecU3Ec__Iterator40_2_Reset_m3903964017_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t197972780 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2_Reset_m3903964017(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator40_2_t197972780 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2_Reset_m3903964017_gshared)(__this, method)
