﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Pathfinding_MonoModifier200043088.h"
#include "AssemblyU2DCSharp_Pathfinding_SimpleSmoothModifier3763208150.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.SimpleSmoothModifier
struct  SimpleSmoothModifier_t1067843181  : public MonoModifier_t200043088
{
public:
	// Pathfinding.SimpleSmoothModifier/SmoothType Pathfinding.SimpleSmoothModifier::smoothType
	int32_t ___smoothType_4;
	// System.Int32 Pathfinding.SimpleSmoothModifier::subdivisions
	int32_t ___subdivisions_5;
	// System.Int32 Pathfinding.SimpleSmoothModifier::iterations
	int32_t ___iterations_6;
	// System.Single Pathfinding.SimpleSmoothModifier::strength
	float ___strength_7;
	// System.Boolean Pathfinding.SimpleSmoothModifier::uniformLength
	bool ___uniformLength_8;
	// System.Single Pathfinding.SimpleSmoothModifier::maxSegmentLength
	float ___maxSegmentLength_9;
	// System.Single Pathfinding.SimpleSmoothModifier::bezierTangentLength
	float ___bezierTangentLength_10;
	// System.Single Pathfinding.SimpleSmoothModifier::offset
	float ___offset_11;
	// System.Single Pathfinding.SimpleSmoothModifier::factor
	float ___factor_12;

public:
	inline static int32_t get_offset_of_smoothType_4() { return static_cast<int32_t>(offsetof(SimpleSmoothModifier_t1067843181, ___smoothType_4)); }
	inline int32_t get_smoothType_4() const { return ___smoothType_4; }
	inline int32_t* get_address_of_smoothType_4() { return &___smoothType_4; }
	inline void set_smoothType_4(int32_t value)
	{
		___smoothType_4 = value;
	}

	inline static int32_t get_offset_of_subdivisions_5() { return static_cast<int32_t>(offsetof(SimpleSmoothModifier_t1067843181, ___subdivisions_5)); }
	inline int32_t get_subdivisions_5() const { return ___subdivisions_5; }
	inline int32_t* get_address_of_subdivisions_5() { return &___subdivisions_5; }
	inline void set_subdivisions_5(int32_t value)
	{
		___subdivisions_5 = value;
	}

	inline static int32_t get_offset_of_iterations_6() { return static_cast<int32_t>(offsetof(SimpleSmoothModifier_t1067843181, ___iterations_6)); }
	inline int32_t get_iterations_6() const { return ___iterations_6; }
	inline int32_t* get_address_of_iterations_6() { return &___iterations_6; }
	inline void set_iterations_6(int32_t value)
	{
		___iterations_6 = value;
	}

	inline static int32_t get_offset_of_strength_7() { return static_cast<int32_t>(offsetof(SimpleSmoothModifier_t1067843181, ___strength_7)); }
	inline float get_strength_7() const { return ___strength_7; }
	inline float* get_address_of_strength_7() { return &___strength_7; }
	inline void set_strength_7(float value)
	{
		___strength_7 = value;
	}

	inline static int32_t get_offset_of_uniformLength_8() { return static_cast<int32_t>(offsetof(SimpleSmoothModifier_t1067843181, ___uniformLength_8)); }
	inline bool get_uniformLength_8() const { return ___uniformLength_8; }
	inline bool* get_address_of_uniformLength_8() { return &___uniformLength_8; }
	inline void set_uniformLength_8(bool value)
	{
		___uniformLength_8 = value;
	}

	inline static int32_t get_offset_of_maxSegmentLength_9() { return static_cast<int32_t>(offsetof(SimpleSmoothModifier_t1067843181, ___maxSegmentLength_9)); }
	inline float get_maxSegmentLength_9() const { return ___maxSegmentLength_9; }
	inline float* get_address_of_maxSegmentLength_9() { return &___maxSegmentLength_9; }
	inline void set_maxSegmentLength_9(float value)
	{
		___maxSegmentLength_9 = value;
	}

	inline static int32_t get_offset_of_bezierTangentLength_10() { return static_cast<int32_t>(offsetof(SimpleSmoothModifier_t1067843181, ___bezierTangentLength_10)); }
	inline float get_bezierTangentLength_10() const { return ___bezierTangentLength_10; }
	inline float* get_address_of_bezierTangentLength_10() { return &___bezierTangentLength_10; }
	inline void set_bezierTangentLength_10(float value)
	{
		___bezierTangentLength_10 = value;
	}

	inline static int32_t get_offset_of_offset_11() { return static_cast<int32_t>(offsetof(SimpleSmoothModifier_t1067843181, ___offset_11)); }
	inline float get_offset_11() const { return ___offset_11; }
	inline float* get_address_of_offset_11() { return &___offset_11; }
	inline void set_offset_11(float value)
	{
		___offset_11 = value;
	}

	inline static int32_t get_offset_of_factor_12() { return static_cast<int32_t>(offsetof(SimpleSmoothModifier_t1067843181, ___factor_12)); }
	inline float get_factor_12() const { return ___factor_12; }
	inline float* get_address_of_factor_12() { return &___factor_12; }
	inline void set_factor_12(float value)
	{
		___factor_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
