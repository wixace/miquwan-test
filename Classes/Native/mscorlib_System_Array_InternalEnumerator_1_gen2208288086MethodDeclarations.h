﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2208288086.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_HatredCtrl_stValue3425945410.h"

// System.Void System.Array/InternalEnumerator`1<HatredCtrl/stValue>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1682270547_gshared (InternalEnumerator_1_t2208288086 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1682270547(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2208288086 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1682270547_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<HatredCtrl/stValue>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2320412013_gshared (InternalEnumerator_1_t2208288086 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2320412013(__this, method) ((  void (*) (InternalEnumerator_1_t2208288086 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2320412013_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<HatredCtrl/stValue>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3569116441_gshared (InternalEnumerator_1_t2208288086 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3569116441(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2208288086 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3569116441_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<HatredCtrl/stValue>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2653155050_gshared (InternalEnumerator_1_t2208288086 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2653155050(__this, method) ((  void (*) (InternalEnumerator_1_t2208288086 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2653155050_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<HatredCtrl/stValue>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1367054681_gshared (InternalEnumerator_1_t2208288086 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1367054681(__this, method) ((  bool (*) (InternalEnumerator_1_t2208288086 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1367054681_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<HatredCtrl/stValue>::get_Current()
extern "C"  stValue_t3425945410  InternalEnumerator_1_get_Current_m2843896922_gshared (InternalEnumerator_1_t2208288086 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2843896922(__this, method) ((  stValue_t3425945410  (*) (InternalEnumerator_1_t2208288086 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2843896922_gshared)(__this, method)
