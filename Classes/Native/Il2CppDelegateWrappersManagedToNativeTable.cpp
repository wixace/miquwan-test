﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t691583313 ();
extern "C" void DelegatePInvokeWrapper_Swapper_t4166107989 ();
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t1428404869 ();
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t2583486074 ();
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t651537830 ();
extern "C" void DelegatePInvokeWrapper_CallbackHandler_t1474775431 ();
extern "C" void DelegatePInvokeWrapper_ThreadStart_t124146534 ();
extern "C" void DelegatePInvokeWrapper_ReadMethod_t1873379884 ();
extern "C" void DelegatePInvokeWrapper_UnmanagedReadOrWrite_t2055733333 ();
extern "C" void DelegatePInvokeWrapper_WriteMethod_t3250749483 ();
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t3891738222 ();
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t1637408689 ();
extern "C" void DelegatePInvokeWrapper_SocketAsyncCall_t742231849 ();
extern "C" void DelegatePInvokeWrapper_CostDelegate_t1292950321 ();
extern "C" void DelegatePInvokeWrapper_Action_t3771233898 ();
extern "C" void DelegatePInvokeWrapper_DispatcherFactory_t1500081684 ();
extern "C" void DelegatePInvokeWrapper_UnityAdsDelegate_t2487421136 ();
extern "C" void DelegatePInvokeWrapper_AndroidJavaRunnable_t1289602340 ();
extern "C" void DelegatePInvokeWrapper_AdvertisingIdentifierCallback_t1751144028 ();
extern "C" void DelegatePInvokeWrapper_LogCallback_t2984951347 ();
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t83861602 ();
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t4244274966 ();
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t1377657005 ();
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t4247149838 ();
extern "C" void DelegatePInvokeWrapper_StateChanged_t2578300556 ();
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t581305515 ();
extern "C" void DelegatePInvokeWrapper_UnityAction_t594794173 ();
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t4168056797 ();
extern "C" void DelegatePInvokeWrapper_WindowFunction_t2749288659 ();
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t4002872878 ();
extern "C" void DelegatePInvokeWrapper_BannerFailedToLoadDelegate_t2244784594 ();
extern "C" void DelegatePInvokeWrapper_BannerWasClickedDelegate_t3510840818 ();
extern "C" void DelegatePInvokeWrapper_BannerWasLoadedDelegate_t3604098244 ();
extern "C" void DelegatePInvokeWrapper_InterstitialWasLoadedDelegate_t3068494210 ();
extern "C" void DelegatePInvokeWrapper_InterstitialWasViewedDelegate_t507319425 ();
extern "C" void DelegatePInvokeWrapper_SetCompressionCallback_t735449941 ();
extern "C" void DelegatePInvokeWrapper_CompressFunc_t3378609318 ();
extern "C" void DelegatePInvokeWrapper_CharGetter_t4120438118 ();
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t3952708057 ();
extern "C" void DelegatePInvokeWrapper_LogCallbackDelegate_t45039171 ();
extern "C" void DelegatePInvokeWrapper_SetBytes_t2856136722 ();
extern "C" void DelegatePInvokeWrapper_JSDelayFun0_t1074711451 ();
extern "C" void DelegatePInvokeWrapper_PushCallBack_t4203546979 ();
extern "C" void DelegatePInvokeWrapper_ResponseCallBack_t59495818 ();
extern "C" void DelegatePInvokeWrapper_Callback_t1094463061 ();
extern "C" void DelegatePInvokeWrapper_EventHandler_t2445892876 ();
extern "C" void DelegatePInvokeWrapper_GlobalTouchFilterDelegate_t316184925 ();
extern "C" void DelegatePInvokeWrapper_setValue_t3220385890 ();
extern "C" void DelegatePInvokeWrapper_Function_t4102301030 ();
extern "C" void DelegatePInvokeWrapper_ApplyTween_t882368618 ();
extern "C" void DelegatePInvokeWrapper_EasingFunction_t1323017328 ();
extern "C" void DelegatePInvokeWrapper_CSEntry_t1794636260 ();
extern "C" void DelegatePInvokeWrapper_JSErrorReporter_t2324290114 ();
extern "C" void DelegatePInvokeWrapper_JSFinalizeOp_t2235302406 ();
extern "C" void DelegatePInvokeWrapper_JSNative_t3654534910 ();
extern "C" void DelegatePInvokeWrapper_OnObjCollected_t2008609135 ();
extern "C" void DelegatePInvokeWrapper_OnLoadJS_t2928531685 ();
extern "C" void DelegatePInvokeWrapper_OnInitJSEngine_t2416054170 ();
extern "C" void DelegatePInvokeWrapper_LoadFunction_t2234103444 ();
extern "C" void DelegatePInvokeWrapper_RequestDelegate_t1822766829 ();
extern "C" void DelegatePInvokeWrapper_SdkwwwCallBack_t3004559384 ();
extern "C" void DelegatePInvokeWrapper_OnMoveCallBackFun_t3535987578 ();
extern "C" void DelegatePInvokeWrapper_OnScanStatus_t2412749870 ();
extern "C" void DelegatePInvokeWrapper_OnVoidDelegate_t2787120856 ();
extern "C" void DelegatePInvokeWrapper_OnFinished_t3316389065 ();
extern "C" void DelegatePInvokeWrapper_OnFinished_t2399837386 ();
extern "C" void DelegatePInvokeWrapper_OnFinish_t840705683 ();
extern "C" void DelegatePInvokeWrapper_GetAxisFunc_t783615269 ();
extern "C" void DelegatePInvokeWrapper_GetKeyStateFunc_t2745734774 ();
extern "C" void DelegatePInvokeWrapper_MoveDelegate_t1906135052 ();
extern "C" void DelegatePInvokeWrapper_OnCustomInput_t736305828 ();
extern "C" void DelegatePInvokeWrapper_OnScreenResize_t3828578773 ();
extern "C" void DelegatePInvokeWrapper_OnReposition_t1310478256 ();
extern "C" void DelegatePInvokeWrapper_OnValidate_t2690340430 ();
extern "C" void DelegatePInvokeWrapper_OnGeometryUpdated_t4285773675 ();
extern "C" void DelegatePInvokeWrapper_LegacyEvent_t524649176 ();
extern "C" void DelegatePInvokeWrapper_OnDragFinished_t4207031714 ();
extern "C" void DelegatePInvokeWrapper_OnDragNotification_t2323474503 ();
extern "C" void DelegatePInvokeWrapper_OnReposition_t213079568 ();
extern "C" void DelegatePInvokeWrapper_Validate_t2927776861 ();
extern "C" void DelegatePInvokeWrapper_HitCheck_t3889696652 ();
extern "C" void DelegatePInvokeWrapper_OnDimensionsChanged_t3695058769 ();
extern const Il2CppMethodPointer g_DelegateWrappersManagedToNative[82] = 
{
	DelegatePInvokeWrapper_AppDomainInitializer_t691583313,
	DelegatePInvokeWrapper_Swapper_t4166107989,
	DelegatePInvokeWrapper_ReadDelegate_t1428404869,
	DelegatePInvokeWrapper_WriteDelegate_t2583486074,
	DelegatePInvokeWrapper_CrossContextDelegate_t651537830,
	DelegatePInvokeWrapper_CallbackHandler_t1474775431,
	DelegatePInvokeWrapper_ThreadStart_t124146534,
	DelegatePInvokeWrapper_ReadMethod_t1873379884,
	DelegatePInvokeWrapper_UnmanagedReadOrWrite_t2055733333,
	DelegatePInvokeWrapper_WriteMethod_t3250749483,
	DelegatePInvokeWrapper_ReadDelegate_t3891738222,
	DelegatePInvokeWrapper_WriteDelegate_t1637408689,
	DelegatePInvokeWrapper_SocketAsyncCall_t742231849,
	DelegatePInvokeWrapper_CostDelegate_t1292950321,
	DelegatePInvokeWrapper_Action_t3771233898,
	DelegatePInvokeWrapper_DispatcherFactory_t1500081684,
	DelegatePInvokeWrapper_UnityAdsDelegate_t2487421136,
	DelegatePInvokeWrapper_AndroidJavaRunnable_t1289602340,
	DelegatePInvokeWrapper_AdvertisingIdentifierCallback_t1751144028,
	DelegatePInvokeWrapper_LogCallback_t2984951347,
	DelegatePInvokeWrapper_PCMReaderCallback_t83861602,
	DelegatePInvokeWrapper_PCMSetPositionCallback_t4244274966,
	DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t1377657005,
	DelegatePInvokeWrapper_WillRenderCanvases_t4247149838,
	DelegatePInvokeWrapper_StateChanged_t2578300556,
	DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t581305515,
	DelegatePInvokeWrapper_UnityAction_t594794173,
	DelegatePInvokeWrapper_FontTextureRebuildCallback_t4168056797,
	DelegatePInvokeWrapper_WindowFunction_t2749288659,
	DelegatePInvokeWrapper_SkinChangedDelegate_t4002872878,
	DelegatePInvokeWrapper_BannerFailedToLoadDelegate_t2244784594,
	DelegatePInvokeWrapper_BannerWasClickedDelegate_t3510840818,
	DelegatePInvokeWrapper_BannerWasLoadedDelegate_t3604098244,
	DelegatePInvokeWrapper_InterstitialWasLoadedDelegate_t3068494210,
	DelegatePInvokeWrapper_InterstitialWasViewedDelegate_t507319425,
	DelegatePInvokeWrapper_SetCompressionCallback_t735449941,
	DelegatePInvokeWrapper_CompressFunc_t3378609318,
	DelegatePInvokeWrapper_CharGetter_t4120438118,
	DelegatePInvokeWrapper_OnValidateInput_t3952708057,
	DelegatePInvokeWrapper_LogCallbackDelegate_t45039171,
	DelegatePInvokeWrapper_SetBytes_t2856136722,
	DelegatePInvokeWrapper_JSDelayFun0_t1074711451,
	DelegatePInvokeWrapper_PushCallBack_t4203546979,
	DelegatePInvokeWrapper_ResponseCallBack_t59495818,
	DelegatePInvokeWrapper_Callback_t1094463061,
	DelegatePInvokeWrapper_EventHandler_t2445892876,
	DelegatePInvokeWrapper_GlobalTouchFilterDelegate_t316184925,
	DelegatePInvokeWrapper_setValue_t3220385890,
	DelegatePInvokeWrapper_Function_t4102301030,
	DelegatePInvokeWrapper_ApplyTween_t882368618,
	DelegatePInvokeWrapper_EasingFunction_t1323017328,
	DelegatePInvokeWrapper_CSEntry_t1794636260,
	DelegatePInvokeWrapper_JSErrorReporter_t2324290114,
	DelegatePInvokeWrapper_JSFinalizeOp_t2235302406,
	DelegatePInvokeWrapper_JSNative_t3654534910,
	DelegatePInvokeWrapper_OnObjCollected_t2008609135,
	DelegatePInvokeWrapper_OnLoadJS_t2928531685,
	DelegatePInvokeWrapper_OnInitJSEngine_t2416054170,
	DelegatePInvokeWrapper_LoadFunction_t2234103444,
	DelegatePInvokeWrapper_RequestDelegate_t1822766829,
	DelegatePInvokeWrapper_SdkwwwCallBack_t3004559384,
	DelegatePInvokeWrapper_OnMoveCallBackFun_t3535987578,
	DelegatePInvokeWrapper_OnScanStatus_t2412749870,
	DelegatePInvokeWrapper_OnVoidDelegate_t2787120856,
	DelegatePInvokeWrapper_OnFinished_t3316389065,
	DelegatePInvokeWrapper_OnFinished_t2399837386,
	DelegatePInvokeWrapper_OnFinish_t840705683,
	DelegatePInvokeWrapper_GetAxisFunc_t783615269,
	DelegatePInvokeWrapper_GetKeyStateFunc_t2745734774,
	DelegatePInvokeWrapper_MoveDelegate_t1906135052,
	DelegatePInvokeWrapper_OnCustomInput_t736305828,
	DelegatePInvokeWrapper_OnScreenResize_t3828578773,
	DelegatePInvokeWrapper_OnReposition_t1310478256,
	DelegatePInvokeWrapper_OnValidate_t2690340430,
	DelegatePInvokeWrapper_OnGeometryUpdated_t4285773675,
	DelegatePInvokeWrapper_LegacyEvent_t524649176,
	DelegatePInvokeWrapper_OnDragFinished_t4207031714,
	DelegatePInvokeWrapper_OnDragNotification_t2323474503,
	DelegatePInvokeWrapper_OnReposition_t213079568,
	DelegatePInvokeWrapper_Validate_t2927776861,
	DelegatePInvokeWrapper_HitCheck_t3889696652,
	DelegatePInvokeWrapper_OnDimensionsChanged_t3695058769,
};
