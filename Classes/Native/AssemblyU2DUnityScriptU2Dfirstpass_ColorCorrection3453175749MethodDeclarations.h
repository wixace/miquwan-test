﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ColorCorrectionCurves
struct ColorCorrectionCurves_t3453175749;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void ColorCorrectionCurves::.ctor()
extern "C"  void ColorCorrectionCurves__ctor_m2903622781 (ColorCorrectionCurves_t3453175749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorCorrectionCurves::Start()
extern "C"  void ColorCorrectionCurves_Start_m1850760573 (ColorCorrectionCurves_t3453175749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorCorrectionCurves::Awake()
extern "C"  void ColorCorrectionCurves_Awake_m3141228000 (ColorCorrectionCurves_t3453175749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ColorCorrectionCurves::CheckResources()
extern "C"  bool ColorCorrectionCurves_CheckResources_m631174462 (ColorCorrectionCurves_t3453175749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorCorrectionCurves::UpdateParameters()
extern "C"  void ColorCorrectionCurves_UpdateParameters_m102789882 (ColorCorrectionCurves_t3453175749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorCorrectionCurves::UpdateTextures()
extern "C"  void ColorCorrectionCurves_UpdateTextures_m1175575560 (ColorCorrectionCurves_t3453175749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorCorrectionCurves::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void ColorCorrectionCurves_OnRenderImage_m4141024705 (ColorCorrectionCurves_t3453175749 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorCorrectionCurves::Main()
extern "C"  void ColorCorrectionCurves_Main_m3472842912 (ColorCorrectionCurves_t3453175749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
