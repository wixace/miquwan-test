﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._f7d06a788686998f13bf3273d9b4bac6
struct _f7d06a788686998f13bf3273d9b4bac6_t621400070;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._f7d06a788686998f13bf3273d9b4bac6::.ctor()
extern "C"  void _f7d06a788686998f13bf3273d9b4bac6__ctor_m2524708071 (_f7d06a788686998f13bf3273d9b4bac6_t621400070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f7d06a788686998f13bf3273d9b4bac6::_f7d06a788686998f13bf3273d9b4bac6m2(System.Int32)
extern "C"  int32_t _f7d06a788686998f13bf3273d9b4bac6__f7d06a788686998f13bf3273d9b4bac6m2_m3166430169 (_f7d06a788686998f13bf3273d9b4bac6_t621400070 * __this, int32_t ____f7d06a788686998f13bf3273d9b4bac6a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f7d06a788686998f13bf3273d9b4bac6::_f7d06a788686998f13bf3273d9b4bac6m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _f7d06a788686998f13bf3273d9b4bac6__f7d06a788686998f13bf3273d9b4bac6m_m93178621 (_f7d06a788686998f13bf3273d9b4bac6_t621400070 * __this, int32_t ____f7d06a788686998f13bf3273d9b4bac6a0, int32_t ____f7d06a788686998f13bf3273d9b4bac6391, int32_t ____f7d06a788686998f13bf3273d9b4bac6c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
