﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<CameraShotNodeCfg>
struct List_1_t650959091;
// System.Collections.Generic.List`1<CameraShotHeroOrNpcCfg>
struct List_1_t4076138479;
// System.Collections.Generic.List`1<CameraShotStoryCfg>
struct List_1_t1204171518;
// System.Collections.Generic.List`1<CameraShotGameSpeedCfg>
struct List_1_t1676810494;
// System.Collections.Generic.List`1<CameraShotActionCfg>
struct List_1_t3362023423;
// System.Collections.Generic.List`1<CameraShotSkillCfg>
struct List_1_t1628869666;
// System.Collections.Generic.List`1<CameraShotMoveCfg>
struct List_1_t4075046084;
// System.Collections.Generic.List`1<CameraShotCircleCfg>
struct List_1_t3959188261;
// System.Collections.Generic.List`1<CameraShotEffectCfg>
struct List_1_t1695066820;
// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CurCameraShotCfg
struct  CurCameraShotCfg_t224861669  : public Il2CppObject
{
public:
	// System.Int32 CurCameraShotCfg::_id
	int32_t ____id_0;
	// System.Int32 CurCameraShotCfg::_storyId
	int32_t ____storyId_1;
	// System.Int32 CurCameraShotCfg::_funType
	int32_t ____funType_2;
	// System.Int32 CurCameraShotCfg::_withId
	int32_t ____withId_3;
	// System.String CurCameraShotCfg::_remark
	String_t* ____remark_4;
	// System.Int32 CurCameraShotCfg::_preId
	int32_t ____preId_5;
	// System.Int32 CurCameraShotCfg::_triggerCondition
	int32_t ____triggerCondition_6;
	// System.Int32 CurCameraShotCfg::_triggerConditionHeroOrNpcId
	int32_t ____triggerConditionHeroOrNpcId_7;
	// System.Int32 CurCameraShotCfg::_triggerConditionWave
	int32_t ____triggerConditionWave_8;
	// System.Int32 CurCameraShotCfg::_isLockNpc
	int32_t ____isLockNpc_9;
	// System.Int32 CurCameraShotCfg::_isCloseFightCtrlUpdate
	int32_t ____isCloseFightCtrlUpdate_10;
	// System.Int32 CurCameraShotCfg::_isStopCameraShot
	int32_t ____isStopCameraShot_11;
	// System.Int32 CurCameraShotCfg::_clearAniShow
	int32_t ____clearAniShow_12;
	// System.Collections.Generic.List`1<CameraShotNodeCfg> CurCameraShotCfg::_cameraShotNode
	List_1_t650959091 * ____cameraShotNode_13;
	// System.Collections.Generic.List`1<CameraShotHeroOrNpcCfg> CurCameraShotCfg::_hero
	List_1_t4076138479 * ____hero_14;
	// System.Collections.Generic.List`1<CameraShotHeroOrNpcCfg> CurCameraShotCfg::_monster
	List_1_t4076138479 * ____monster_15;
	// System.Collections.Generic.List`1<CameraShotStoryCfg> CurCameraShotCfg::_story
	List_1_t1204171518 * ____story_16;
	// System.Collections.Generic.List`1<CameraShotGameSpeedCfg> CurCameraShotCfg::_gameSpeed
	List_1_t1676810494 * ____gameSpeed_17;
	// System.Collections.Generic.List`1<CameraShotActionCfg> CurCameraShotCfg::_action
	List_1_t3362023423 * ____action_18;
	// System.Collections.Generic.List`1<CameraShotSkillCfg> CurCameraShotCfg::_skill
	List_1_t1628869666 * ____skill_19;
	// System.Collections.Generic.List`1<CameraShotMoveCfg> CurCameraShotCfg::_move
	List_1_t4075046084 * ____move_20;
	// System.Collections.Generic.List`1<CameraShotCircleCfg> CurCameraShotCfg::_circle
	List_1_t3959188261 * ____circle_21;
	// System.Collections.Generic.List`1<CameraShotEffectCfg> CurCameraShotCfg::_effect
	List_1_t1695066820 * ____effect_22;
	// ProtoBuf.IExtension CurCameraShotCfg::extensionObject
	Il2CppObject * ___extensionObject_23;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(CurCameraShotCfg_t224861669, ____id_0)); }
	inline int32_t get__id_0() const { return ____id_0; }
	inline int32_t* get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(int32_t value)
	{
		____id_0 = value;
	}

	inline static int32_t get_offset_of__storyId_1() { return static_cast<int32_t>(offsetof(CurCameraShotCfg_t224861669, ____storyId_1)); }
	inline int32_t get__storyId_1() const { return ____storyId_1; }
	inline int32_t* get_address_of__storyId_1() { return &____storyId_1; }
	inline void set__storyId_1(int32_t value)
	{
		____storyId_1 = value;
	}

	inline static int32_t get_offset_of__funType_2() { return static_cast<int32_t>(offsetof(CurCameraShotCfg_t224861669, ____funType_2)); }
	inline int32_t get__funType_2() const { return ____funType_2; }
	inline int32_t* get_address_of__funType_2() { return &____funType_2; }
	inline void set__funType_2(int32_t value)
	{
		____funType_2 = value;
	}

	inline static int32_t get_offset_of__withId_3() { return static_cast<int32_t>(offsetof(CurCameraShotCfg_t224861669, ____withId_3)); }
	inline int32_t get__withId_3() const { return ____withId_3; }
	inline int32_t* get_address_of__withId_3() { return &____withId_3; }
	inline void set__withId_3(int32_t value)
	{
		____withId_3 = value;
	}

	inline static int32_t get_offset_of__remark_4() { return static_cast<int32_t>(offsetof(CurCameraShotCfg_t224861669, ____remark_4)); }
	inline String_t* get__remark_4() const { return ____remark_4; }
	inline String_t** get_address_of__remark_4() { return &____remark_4; }
	inline void set__remark_4(String_t* value)
	{
		____remark_4 = value;
		Il2CppCodeGenWriteBarrier(&____remark_4, value);
	}

	inline static int32_t get_offset_of__preId_5() { return static_cast<int32_t>(offsetof(CurCameraShotCfg_t224861669, ____preId_5)); }
	inline int32_t get__preId_5() const { return ____preId_5; }
	inline int32_t* get_address_of__preId_5() { return &____preId_5; }
	inline void set__preId_5(int32_t value)
	{
		____preId_5 = value;
	}

	inline static int32_t get_offset_of__triggerCondition_6() { return static_cast<int32_t>(offsetof(CurCameraShotCfg_t224861669, ____triggerCondition_6)); }
	inline int32_t get__triggerCondition_6() const { return ____triggerCondition_6; }
	inline int32_t* get_address_of__triggerCondition_6() { return &____triggerCondition_6; }
	inline void set__triggerCondition_6(int32_t value)
	{
		____triggerCondition_6 = value;
	}

	inline static int32_t get_offset_of__triggerConditionHeroOrNpcId_7() { return static_cast<int32_t>(offsetof(CurCameraShotCfg_t224861669, ____triggerConditionHeroOrNpcId_7)); }
	inline int32_t get__triggerConditionHeroOrNpcId_7() const { return ____triggerConditionHeroOrNpcId_7; }
	inline int32_t* get_address_of__triggerConditionHeroOrNpcId_7() { return &____triggerConditionHeroOrNpcId_7; }
	inline void set__triggerConditionHeroOrNpcId_7(int32_t value)
	{
		____triggerConditionHeroOrNpcId_7 = value;
	}

	inline static int32_t get_offset_of__triggerConditionWave_8() { return static_cast<int32_t>(offsetof(CurCameraShotCfg_t224861669, ____triggerConditionWave_8)); }
	inline int32_t get__triggerConditionWave_8() const { return ____triggerConditionWave_8; }
	inline int32_t* get_address_of__triggerConditionWave_8() { return &____triggerConditionWave_8; }
	inline void set__triggerConditionWave_8(int32_t value)
	{
		____triggerConditionWave_8 = value;
	}

	inline static int32_t get_offset_of__isLockNpc_9() { return static_cast<int32_t>(offsetof(CurCameraShotCfg_t224861669, ____isLockNpc_9)); }
	inline int32_t get__isLockNpc_9() const { return ____isLockNpc_9; }
	inline int32_t* get_address_of__isLockNpc_9() { return &____isLockNpc_9; }
	inline void set__isLockNpc_9(int32_t value)
	{
		____isLockNpc_9 = value;
	}

	inline static int32_t get_offset_of__isCloseFightCtrlUpdate_10() { return static_cast<int32_t>(offsetof(CurCameraShotCfg_t224861669, ____isCloseFightCtrlUpdate_10)); }
	inline int32_t get__isCloseFightCtrlUpdate_10() const { return ____isCloseFightCtrlUpdate_10; }
	inline int32_t* get_address_of__isCloseFightCtrlUpdate_10() { return &____isCloseFightCtrlUpdate_10; }
	inline void set__isCloseFightCtrlUpdate_10(int32_t value)
	{
		____isCloseFightCtrlUpdate_10 = value;
	}

	inline static int32_t get_offset_of__isStopCameraShot_11() { return static_cast<int32_t>(offsetof(CurCameraShotCfg_t224861669, ____isStopCameraShot_11)); }
	inline int32_t get__isStopCameraShot_11() const { return ____isStopCameraShot_11; }
	inline int32_t* get_address_of__isStopCameraShot_11() { return &____isStopCameraShot_11; }
	inline void set__isStopCameraShot_11(int32_t value)
	{
		____isStopCameraShot_11 = value;
	}

	inline static int32_t get_offset_of__clearAniShow_12() { return static_cast<int32_t>(offsetof(CurCameraShotCfg_t224861669, ____clearAniShow_12)); }
	inline int32_t get__clearAniShow_12() const { return ____clearAniShow_12; }
	inline int32_t* get_address_of__clearAniShow_12() { return &____clearAniShow_12; }
	inline void set__clearAniShow_12(int32_t value)
	{
		____clearAniShow_12 = value;
	}

	inline static int32_t get_offset_of__cameraShotNode_13() { return static_cast<int32_t>(offsetof(CurCameraShotCfg_t224861669, ____cameraShotNode_13)); }
	inline List_1_t650959091 * get__cameraShotNode_13() const { return ____cameraShotNode_13; }
	inline List_1_t650959091 ** get_address_of__cameraShotNode_13() { return &____cameraShotNode_13; }
	inline void set__cameraShotNode_13(List_1_t650959091 * value)
	{
		____cameraShotNode_13 = value;
		Il2CppCodeGenWriteBarrier(&____cameraShotNode_13, value);
	}

	inline static int32_t get_offset_of__hero_14() { return static_cast<int32_t>(offsetof(CurCameraShotCfg_t224861669, ____hero_14)); }
	inline List_1_t4076138479 * get__hero_14() const { return ____hero_14; }
	inline List_1_t4076138479 ** get_address_of__hero_14() { return &____hero_14; }
	inline void set__hero_14(List_1_t4076138479 * value)
	{
		____hero_14 = value;
		Il2CppCodeGenWriteBarrier(&____hero_14, value);
	}

	inline static int32_t get_offset_of__monster_15() { return static_cast<int32_t>(offsetof(CurCameraShotCfg_t224861669, ____monster_15)); }
	inline List_1_t4076138479 * get__monster_15() const { return ____monster_15; }
	inline List_1_t4076138479 ** get_address_of__monster_15() { return &____monster_15; }
	inline void set__monster_15(List_1_t4076138479 * value)
	{
		____monster_15 = value;
		Il2CppCodeGenWriteBarrier(&____monster_15, value);
	}

	inline static int32_t get_offset_of__story_16() { return static_cast<int32_t>(offsetof(CurCameraShotCfg_t224861669, ____story_16)); }
	inline List_1_t1204171518 * get__story_16() const { return ____story_16; }
	inline List_1_t1204171518 ** get_address_of__story_16() { return &____story_16; }
	inline void set__story_16(List_1_t1204171518 * value)
	{
		____story_16 = value;
		Il2CppCodeGenWriteBarrier(&____story_16, value);
	}

	inline static int32_t get_offset_of__gameSpeed_17() { return static_cast<int32_t>(offsetof(CurCameraShotCfg_t224861669, ____gameSpeed_17)); }
	inline List_1_t1676810494 * get__gameSpeed_17() const { return ____gameSpeed_17; }
	inline List_1_t1676810494 ** get_address_of__gameSpeed_17() { return &____gameSpeed_17; }
	inline void set__gameSpeed_17(List_1_t1676810494 * value)
	{
		____gameSpeed_17 = value;
		Il2CppCodeGenWriteBarrier(&____gameSpeed_17, value);
	}

	inline static int32_t get_offset_of__action_18() { return static_cast<int32_t>(offsetof(CurCameraShotCfg_t224861669, ____action_18)); }
	inline List_1_t3362023423 * get__action_18() const { return ____action_18; }
	inline List_1_t3362023423 ** get_address_of__action_18() { return &____action_18; }
	inline void set__action_18(List_1_t3362023423 * value)
	{
		____action_18 = value;
		Il2CppCodeGenWriteBarrier(&____action_18, value);
	}

	inline static int32_t get_offset_of__skill_19() { return static_cast<int32_t>(offsetof(CurCameraShotCfg_t224861669, ____skill_19)); }
	inline List_1_t1628869666 * get__skill_19() const { return ____skill_19; }
	inline List_1_t1628869666 ** get_address_of__skill_19() { return &____skill_19; }
	inline void set__skill_19(List_1_t1628869666 * value)
	{
		____skill_19 = value;
		Il2CppCodeGenWriteBarrier(&____skill_19, value);
	}

	inline static int32_t get_offset_of__move_20() { return static_cast<int32_t>(offsetof(CurCameraShotCfg_t224861669, ____move_20)); }
	inline List_1_t4075046084 * get__move_20() const { return ____move_20; }
	inline List_1_t4075046084 ** get_address_of__move_20() { return &____move_20; }
	inline void set__move_20(List_1_t4075046084 * value)
	{
		____move_20 = value;
		Il2CppCodeGenWriteBarrier(&____move_20, value);
	}

	inline static int32_t get_offset_of__circle_21() { return static_cast<int32_t>(offsetof(CurCameraShotCfg_t224861669, ____circle_21)); }
	inline List_1_t3959188261 * get__circle_21() const { return ____circle_21; }
	inline List_1_t3959188261 ** get_address_of__circle_21() { return &____circle_21; }
	inline void set__circle_21(List_1_t3959188261 * value)
	{
		____circle_21 = value;
		Il2CppCodeGenWriteBarrier(&____circle_21, value);
	}

	inline static int32_t get_offset_of__effect_22() { return static_cast<int32_t>(offsetof(CurCameraShotCfg_t224861669, ____effect_22)); }
	inline List_1_t1695066820 * get__effect_22() const { return ____effect_22; }
	inline List_1_t1695066820 ** get_address_of__effect_22() { return &____effect_22; }
	inline void set__effect_22(List_1_t1695066820 * value)
	{
		____effect_22 = value;
		Il2CppCodeGenWriteBarrier(&____effect_22, value);
	}

	inline static int32_t get_offset_of_extensionObject_23() { return static_cast<int32_t>(offsetof(CurCameraShotCfg_t224861669, ___extensionObject_23)); }
	inline Il2CppObject * get_extensionObject_23() const { return ___extensionObject_23; }
	inline Il2CppObject ** get_address_of_extensionObject_23() { return &___extensionObject_23; }
	inline void set_extensionObject_23(Il2CppObject * value)
	{
		___extensionObject_23 = value;
		Il2CppCodeGenWriteBarrier(&___extensionObject_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
