﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.FightBvr
struct FightBvr_t869234382;
// CombatEntity
struct CombatEntity_t684137495;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;
// FightCtrl
struct FightCtrl_t648967803;
// skillCfg
struct skillCfg_t2142425171;
// Skill
struct Skill_t79944241;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Skill79944241.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitType643359572.h"
#include "AssemblyU2DCSharp_FightCtrl648967803.h"

// System.Void Entity.Behavior.FightBvr::.ctor()
extern "C"  void FightBvr__ctor_m555306524 (FightBvr_t869234382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID Entity.Behavior.FightBvr::get_id()
extern "C"  uint8_t FightBvr_get_id_m2572077054 (FightBvr_t869234382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FightBvr::Action()
extern "C"  void FightBvr_Action_m3077695134 (FightBvr_t869234382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FightBvr::Reason()
extern "C"  void FightBvr_Reason_m3880988460 (FightBvr_t869234382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FightBvr::DoEntering()
extern "C"  void FightBvr_DoEntering_m330756317 (FightBvr_t869234382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FightBvr::DoLeaving()
extern "C"  void FightBvr_DoLeaving_m1148316451 (FightBvr_t869234382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Entity.Behavior.FightBvr::IsLockingBehavior()
extern "C"  bool FightBvr_IsLockingBehavior_m2875561861 (FightBvr_t869234382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.FightBvr::ilo_get_entity1(Entity.Behavior.IBehavior)
extern "C"  CombatEntity_t684137495 * FightBvr_ilo_get_entity1_m1667487650 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightCtrl Entity.Behavior.FightBvr::ilo_get_fightCtrl2(CombatEntity)
extern "C"  FightCtrl_t648967803 * FightBvr_ilo_get_fightCtrl2_m358049276 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// skillCfg Entity.Behavior.FightBvr::ilo_get_mpJson3(Skill)
extern "C"  skillCfg_t2142425171 * FightBvr_ilo_get_mpJson3_m255967141 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Entity.Behavior.FightBvr::ilo_get_atkRange4(CombatEntity)
extern "C"  float FightBvr_ilo_get_atkRange4_m706211050 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Entity.Behavior.FightBvr::ilo_get_position5(CombatEntity)
extern "C"  Vector3_t4282066566  FightBvr_ilo_get_position5_m1594232345 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.FightBvr::ilo_get_atkTarget6(CombatEntity)
extern "C"  CombatEntity_t684137495 * FightBvr_ilo_get_atkTarget6_m3951815136 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl Entity.Behavior.FightBvr::ilo_get_bvrCtrl7(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * FightBvr_ilo_get_bvrCtrl7_m1395358809 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FightBvr::ilo_TranBehavior8(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void FightBvr_ilo_TranBehavior8_m555096690 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, uint8_t ___id1, ObjectU5BU5D_t1108656482* ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitType Entity.Behavior.FightBvr::ilo_get_unitType9(CombatEntity)
extern "C"  int32_t FightBvr_ilo_get_unitType9_m4200442839 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Entity.Behavior.FightBvr::ilo_get_isKnockingUp10(CombatEntity)
extern "C"  bool FightBvr_ilo_get_isKnockingUp10_m2851450547 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FightBvr::ilo_Fight11(FightCtrl)
extern "C"  void FightBvr_ilo_Fight11_m3635314464 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Entity.Behavior.FightBvr::ilo_get_halfwidth12(CombatEntity)
extern "C"  float FightBvr_ilo_get_halfwidth12_m567551855 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
