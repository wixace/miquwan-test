﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FingerGestures/Finger
struct Finger_t182428197;

#include "AssemblyU2DCSharp_ContinuousGesture3323503386.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TwistGesture
struct  TwistGesture_t4198361154  : public ContinuousGesture_t3323503386
{
public:
	// System.Single TwistGesture::<DeltaRotation>k__BackingField
	float ___U3CDeltaRotationU3Ek__BackingField_12;
	// System.Single TwistGesture::<TotalRotation>k__BackingField
	float ___U3CTotalRotationU3Ek__BackingField_13;
	// FingerGestures/Finger TwistGesture::<Pivot>k__BackingField
	Finger_t182428197 * ___U3CPivotU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CDeltaRotationU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(TwistGesture_t4198361154, ___U3CDeltaRotationU3Ek__BackingField_12)); }
	inline float get_U3CDeltaRotationU3Ek__BackingField_12() const { return ___U3CDeltaRotationU3Ek__BackingField_12; }
	inline float* get_address_of_U3CDeltaRotationU3Ek__BackingField_12() { return &___U3CDeltaRotationU3Ek__BackingField_12; }
	inline void set_U3CDeltaRotationU3Ek__BackingField_12(float value)
	{
		___U3CDeltaRotationU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CTotalRotationU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(TwistGesture_t4198361154, ___U3CTotalRotationU3Ek__BackingField_13)); }
	inline float get_U3CTotalRotationU3Ek__BackingField_13() const { return ___U3CTotalRotationU3Ek__BackingField_13; }
	inline float* get_address_of_U3CTotalRotationU3Ek__BackingField_13() { return &___U3CTotalRotationU3Ek__BackingField_13; }
	inline void set_U3CTotalRotationU3Ek__BackingField_13(float value)
	{
		___U3CTotalRotationU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CPivotU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(TwistGesture_t4198361154, ___U3CPivotU3Ek__BackingField_14)); }
	inline Finger_t182428197 * get_U3CPivotU3Ek__BackingField_14() const { return ___U3CPivotU3Ek__BackingField_14; }
	inline Finger_t182428197 ** get_address_of_U3CPivotU3Ek__BackingField_14() { return &___U3CPivotU3Ek__BackingField_14; }
	inline void set_U3CPivotU3Ek__BackingField_14(Finger_t182428197 * value)
	{
		___U3CPivotU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPivotU3Ek__BackingField_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
