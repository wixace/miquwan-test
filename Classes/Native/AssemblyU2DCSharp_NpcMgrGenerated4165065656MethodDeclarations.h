﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NpcMgrGenerated
struct NpcMgrGenerated_t4165065656;
// JSVCall
struct JSVCall_t3708497963;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// CSPVPRobotNPC[]
struct CSPVPRobotNPCU5BU5D_t1447782092;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// NpcMgr
struct NpcMgr_t2339534743;
// CombatEntity
struct CombatEntity_t684137495;
// Monster
struct Monster_t2901270458;
// JSCLevelMonsterConfig
struct JSCLevelMonsterConfig_t1924079698;
// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;
// System.Collections.Generic.List`1<CSPVPRobotNPC>
struct List_1_t225956257;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_NpcMgr2339534743.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_JSCLevelMonsterConfig1924079698.h"
#include "AssemblyU2DCSharp_NpcMgr_EditorNpcType2400575222.h"
#include "AssemblyU2DCSharp_HERO_ELEMENT2692579223.h"
#include "AssemblyU2DCSharp_HERO_COUNTRY1018279985.h"

// System.Void NpcMgrGenerated::.ctor()
extern "C"  void NpcMgrGenerated__ctor_m4220885683 (NpcMgrGenerated_t4165065656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_NpcMgr1(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_NpcMgr1_m2374663999 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::NpcMgr_BOSS_DEAD(JSVCall)
extern "C"  void NpcMgrGenerated_NpcMgr_BOSS_DEAD_m1989039336 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::NpcMgr_CurentGroupDeath_NPC(JSVCall)
extern "C"  void NpcMgrGenerated_NpcMgr_CurentGroupDeath_NPC_m2486898124 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::NpcMgr_KillAllMonster(JSVCall)
extern "C"  void NpcMgrGenerated_NpcMgr_KillAllMonster_m3809667255 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::NpcMgr_DEVILHEROID(JSVCall)
extern "C"  void NpcMgrGenerated_NpcMgr_DEVILHEROID_m3120142929 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::NpcMgr_monsterContainer(JSVCall)
extern "C"  void NpcMgrGenerated_NpcMgr_monsterContainer_m3200327687 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::NpcMgr_deathList(JSVCall)
extern "C"  void NpcMgrGenerated_NpcMgr_deathList_m3357143340 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::NpcMgr_IsNotCreateMonster(JSVCall)
extern "C"  void NpcMgrGenerated_NpcMgr_IsNotCreateMonster_m3428511481 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::NpcMgr_isPowerBuff(JSVCall)
extern "C"  void NpcMgrGenerated_NpcMgr_isPowerBuff_m3623220976 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::NpcMgr_devilMonster(JSVCall)
extern "C"  void NpcMgrGenerated_NpcMgr_devilMonster_m259522412 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::NpcMgr_scrAtkInAllBuff(JSVCall)
extern "C"  void NpcMgrGenerated_NpcMgr_scrAtkInAllBuff_m3707390469 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::NpcMgr__BossConsumeHp(JSVCall)
extern "C"  void NpcMgrGenerated_NpcMgr__BossConsumeHp_m621311958 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::NpcMgr__ShiliBossConsumeHp(JSVCall)
extern "C"  void NpcMgrGenerated_NpcMgr__ShiliBossConsumeHp_m1777439609 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::NpcMgr_instance(JSVCall)
extern "C"  void NpcMgrGenerated_NpcMgr_instance_m528749817 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::NpcMgr_boss(JSVCall)
extern "C"  void NpcMgrGenerated_NpcMgr_boss_m1500583201 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::NpcMgr_curRound(JSVCall)
extern "C"  void NpcMgrGenerated_NpcMgr_curRound_m3217415808 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::NpcMgr_curOtherRound(JSVCall)
extern "C"  void NpcMgrGenerated_NpcMgr_curOtherRound_m2845115680 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::NpcMgr_isKillBoss(JSVCall)
extern "C"  void NpcMgrGenerated_NpcMgr_isKillBoss_m2997547097 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::NpcMgr_TotalRound(JSVCall)
extern "C"  void NpcMgrGenerated_NpcMgr_TotalRound_m2448780900 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::NpcMgr_BossConsumeHpPer(JSVCall)
extern "C"  void NpcMgrGenerated_NpcMgr_BossConsumeHpPer_m2483136552 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::NpcMgr_BossConsumeHp(JSVCall)
extern "C"  void NpcMgrGenerated_NpcMgr_BossConsumeHp_m2579480295 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::NpcMgr_ShiliBossConsumeHp(JSVCall)
extern "C"  void NpcMgrGenerated_NpcMgr_ShiliBossConsumeHp_m2494631688 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_AddGlobalBuff__Int32(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_AddGlobalBuff__Int32_m3380544724 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_AddNpc__CombatEntity(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_AddNpc__CombatEntity_m2550802100 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_bossIsKilled(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_bossIsKilled_m3370376497 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_CameraShotBattle__Int32__Int32_Array(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_CameraShotBattle__Int32__Int32_Array_m1635835406 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_ChangeHeroToDead__Int32(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_ChangeHeroToDead__Int32_m948728298 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_ChangeHeroToDead__CombatEntity(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_ChangeHeroToDead__CombatEntity_m4092959389 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_Clear(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_Clear_m3887492786 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_ClearAniShowList__Int32_Array(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_ClearAniShowList__Int32_Array_m3504493699 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_ClearFriendNpcEff(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_ClearFriendNpcEff_m1223231828 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_ClearMonsterUnits(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_ClearMonsterUnits_m2145162055 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_CreateAniShowNpcList__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_CreateAniShowNpcList__Int32__Int32__Int32_m330092209 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_CreatGuideEnemyNpc__JSCLevelMonsterConfig__Boolean(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_CreatGuideEnemyNpc__JSCLevelMonsterConfig__Boolean_m3654021183 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_CreatGuideNpcs__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_CreatGuideNpcs__Int32__Boolean_m4242702044 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_CreatNextNpcList__ZEvent(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_CreatNextNpcList__ZEvent_m3608909024 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_CreatNpc__JSCLevelMonsterConfig__EditorNpcType__Int32__CombatEntity__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_CreatNpc__JSCLevelMonsterConfig__EditorNpcType__Int32__CombatEntity__Boolean__Boolean_m1018161718 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_Dead__CombatEntity(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_Dead__CombatEntity_m2110243224 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetAllAniList__CombatEntity(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetAllAniList__CombatEntity_m1200955915 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetAllAniList(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetAllAniList_m1182437012 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetAllFriendNpc(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetAllFriendNpc_m3771094621 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetAllMonsters(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetAllMonsters_m804326625 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetAllNeutral(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetAllNeutral_m2113637377 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetAniShowNpc__Int32(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetAniShowNpc__Int32_m1611150125 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetBiaoche(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetBiaoche_m798061138 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetCameraShotFriends(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetCameraShotFriends_m733592477 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetCameraShotMonsters(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetCameraShotMonsters_m1655006291 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetCureNpc(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetCureNpc_m3527770179 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetCurHostilNpcCount(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetCurHostilNpcCount_m2021983616 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetCurRoundGuide(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetCurRoundGuide_m2493607989 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetCurRoundMonster(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetCurRoundMonster_m3193616531 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetCurRoundOther(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetCurRoundOther_m2577837385 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetCurUnFinishNpc(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetCurUnFinishNpc_m882030896 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetElement__Int32(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetElement__Int32_m3798471981 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetFriendsList(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetFriendsList_m591669082 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetHeroIntelligenceNum__Int32(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetHeroIntelligenceNum__Int32_m862611932 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetHeroNpcList(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetHeroNpcList_m2183977932 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetHostilNpcById__Int32(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetHostilNpcById__Int32_m564025633 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetHPMinNpc(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetHPMinNpc_m2459618578 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetHurtMaxNpc(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetHurtMaxNpc_m2758392423 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetMonsters(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetMonsters_m1699675444 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetNearestNpc__CombatEntity(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetNearestNpc__CombatEntity_m751381237 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetNeutralNpcById__Int32(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetNeutralNpcById__Int32_m2714377545 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetNpc__Int32(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetNpc__Int32_m3678225512 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetPathNpc(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetPathNpc_m2469241411 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetRemoteNpc(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetRemoteNpc_m2960437186 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetRobotMonsters__CSPVPRobotNPC_Array(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetRobotMonsters__CSPVPRobotNPC_Array_m112099971 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetSexNpc__Int32(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetSexNpc__Int32_m4212525978 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetTriggerDistanceByRound__EditorNpcType(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetTriggerDistanceByRound__EditorNpcType_m723966635 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_GetWaveNpcCfg__Int32(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_GetWaveNpcCfg__Int32_m783084217 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_Init(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_Init_m1964447885 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_InitNpcEntity__JSCLevelMonsterConfig__EditorNpcType__CombatEntity__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_InitNpcEntity__JSCLevelMonsterConfig__EditorNpcType__CombatEntity__Boolean__Boolean_m2941484012 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_IsAllElement(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_IsAllElement_m1358666914 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_IsCurElementNpc__HERO_ELEMENT(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_IsCurElementNpc__HERO_ELEMENT_m945566327 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_IsKillAllMonster(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_IsKillAllMonster_m3544393598 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_IsThreeCountry__HERO_COUNTRY(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_IsThreeCountry__HERO_COUNTRY_m4094738992 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_IsThreeElement__HERO_ELEMENT(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_IsThreeElement__HERO_ELEMENT_m2685592956 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_KillTaixuAllEnemy__CombatEntity(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_KillTaixuAllEnemy__CombatEntity_m833342782 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_LeaveBattle(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_LeaveBattle_m675000884 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_OnTriggerNpcs__CombatEntity(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_OnTriggerNpcs__CombatEntity_m962116743 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_OnTriggerOthers__CombatEntity(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_OnTriggerOthers__CombatEntity_m2204359288 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_OnTriggerUnfinish__CombatEntity(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_OnTriggerUnfinish__CombatEntity_m805596097 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_RandomNpcPosition__ListT1_JSCLevelMonsterConfig(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_RandomNpcPosition__ListT1_JSCLevelMonsterConfig_m1149092890 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_SetMonsterNotSelected__Boolean(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_SetMonsterNotSelected__Boolean_m3645357455 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_SetNpcModelView__Boolean__Int32(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_SetNpcModelView__Boolean__Int32_m2549534170 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::NpcMgr_shiliBossIsKilled(JSVCall,System.Int32)
extern "C"  bool NpcMgrGenerated_NpcMgr_shiliBossIsKilled_m3200650538 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::__Register()
extern "C"  void NpcMgrGenerated___Register_m1525651956 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] NpcMgrGenerated::<NpcMgr_CameraShotBattle__Int32__Int32_Array>m__8A()
extern "C"  Int32U5BU5D_t3230847821* NpcMgrGenerated_U3CNpcMgr_CameraShotBattle__Int32__Int32_ArrayU3Em__8A_m337720794 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] NpcMgrGenerated::<NpcMgr_ClearAniShowList__Int32_Array>m__8B()
extern "C"  Int32U5BU5D_t3230847821* NpcMgrGenerated_U3CNpcMgr_ClearAniShowList__Int32_ArrayU3Em__8B_m556230088 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSPVPRobotNPC[] NpcMgrGenerated::<NpcMgr_GetRobotMonsters__CSPVPRobotNPC_Array>m__8C()
extern "C"  CSPVPRobotNPCU5BU5D_t1447782092* NpcMgrGenerated_U3CNpcMgr_GetRobotMonsters__CSPVPRobotNPC_ArrayU3Em__8C_m1893022109 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NpcMgrGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t NpcMgrGenerated_ilo_getObject1_m1114858851 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void NpcMgrGenerated_ilo_addJSCSRel2_m3213424752 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::ilo_setStringS3(System.Int32,System.String)
extern "C"  void NpcMgrGenerated_ilo_setStringS3_m4181825790 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::ilo_setInt324(System.Int32,System.Int32)
extern "C"  void NpcMgrGenerated_ilo_setInt324_m1918792096 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NpcMgrGenerated::ilo_setObject5(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t NpcMgrGenerated_ilo_setObject5_m2130922975 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object NpcMgrGenerated::ilo_getObject6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * NpcMgrGenerated_ilo_getObject6_m4206235607 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NpcMgrGenerated::ilo_get_curRound7(NpcMgr)
extern "C"  int32_t NpcMgrGenerated_ilo_get_curRound7_m2592684693 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::ilo_setBooleanS8(System.Int32,System.Boolean)
extern "C"  void NpcMgrGenerated_ilo_setBooleanS8_m424087613 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NpcMgrGenerated::ilo_get_TotalRound9(NpcMgr)
extern "C"  int32_t NpcMgrGenerated_ilo_get_TotalRound9_m2449312251 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::ilo_setSingle10(System.Int32,System.Single)
extern "C"  void NpcMgrGenerated_ilo_setSingle10_m3182669053 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NpcMgrGenerated::ilo_get_BossConsumeHp11(NpcMgr)
extern "C"  int32_t NpcMgrGenerated_ilo_get_BossConsumeHp11_m150386115 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NpcMgrGenerated::ilo_getInt3212(System.Int32)
extern "C"  int32_t NpcMgrGenerated_ilo_getInt3212_m1455960832 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::ilo_AddGlobalBuff13(NpcMgr,System.Int32)
extern "C"  void NpcMgrGenerated_ilo_AddGlobalBuff13_m2823647703 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::ilo_CameraShotBattle14(NpcMgr,System.Int32,System.Int32[])
extern "C"  void NpcMgrGenerated_ilo_CameraShotBattle14_m411422117 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, int32_t ___isTruth1, Int32U5BU5D_t3230847821* ___specialInTruth2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::ilo_ChangeHeroToDead15(NpcMgr,CombatEntity)
extern "C"  void NpcMgrGenerated_ilo_ChangeHeroToDead15_m3971750741 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, CombatEntity_t684137495 * ___entity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::ilo_ClearAniShowList16(NpcMgr,System.Int32[])
extern "C"  void NpcMgrGenerated_ilo_ClearAniShowList16_m460594553 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, Int32U5BU5D_t3230847821* ___special1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::ilo_CreateAniShowNpcList17(NpcMgr,System.Int32,System.Int32,System.Int32)
extern "C"  void NpcMgrGenerated_ilo_CreateAniShowNpcList17_m282742210 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, int32_t ___type1, int32_t ___roundIndex2, int32_t ___npcId3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::ilo_getBooleanS18(System.Int32)
extern "C"  bool NpcMgrGenerated_ilo_getBooleanS18_m3096055531 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::ilo_CreatGuideNpcs19(NpcMgr,System.Int32,System.Boolean)
extern "C"  void NpcMgrGenerated_ilo_CreatGuideNpcs19_m2388661664 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, int32_t ___round1, bool ___canFight2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NpcMgrGenerated::ilo_getEnum20(System.Int32)
extern "C"  int32_t NpcMgrGenerated_ilo_getEnum20_m2690013810 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Monster NpcMgrGenerated::ilo_CreatNpc21(NpcMgr,JSCLevelMonsterConfig,NpcMgr/EditorNpcType,System.Int32,CombatEntity,System.Boolean,System.Boolean)
extern "C"  Monster_t2901270458 * NpcMgrGenerated_ilo_CreatNpc21_m1433249795 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, JSCLevelMonsterConfig_t1924079698 * ___lconfig1, int32_t ___type2, int32_t ___index3, CombatEntity_t684137495 * ___SummonEntity4, bool ____isSummon5, bool ___isFindPos6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgrGenerated::ilo_GetAllAniList22(NpcMgr,CombatEntity)
extern "C"  List_1_t2052323047 * NpcMgrGenerated_ilo_GetAllAniList22_m328236143 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, CombatEntity_t684137495 * ___entity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgrGenerated::ilo_GetAllAniList23(NpcMgr)
extern "C"  List_1_t2052323047 * NpcMgrGenerated_ilo_GetAllAniList23_m1783247457 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgrGenerated::ilo_GetCameraShotMonsters24(NpcMgr)
extern "C"  List_1_t2052323047 * NpcMgrGenerated_ilo_GetCameraShotMonsters24_m3842005665 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgrGenerated::ilo_GetCurRoundOther25(NpcMgr)
extern "C"  List_1_t2052323047 * NpcMgrGenerated_ilo_GetCurRoundOther25_m107153426 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HERO_ELEMENT NpcMgrGenerated::ilo_GetElement26(NpcMgr,System.Int32)
extern "C"  int32_t NpcMgrGenerated_ilo_GetElement26_m4183236998 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgrGenerated::ilo_GetHeroNpcList27(NpcMgr)
extern "C"  List_1_t2052323047 * NpcMgrGenerated_ilo_GetHeroNpcList27_m58896471 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity NpcMgrGenerated::ilo_GetHostilNpcById28(NpcMgr,System.Int32)
extern "C"  CombatEntity_t684137495 * NpcMgrGenerated_ilo_GetHostilNpcById28_m3360269816 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgrGenerated::ilo_GetMonsters29(NpcMgr)
extern "C"  List_1_t2052323047 * NpcMgrGenerated_ilo_GetMonsters29_m1724466119 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity NpcMgrGenerated::ilo_GetRemoteNpc30(NpcMgr)
extern "C"  CombatEntity_t684137495 * NpcMgrGenerated_ilo_GetRemoteNpc30_m1581239241 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSPVPRobotNPC> NpcMgrGenerated::ilo_GetRobotMonsters31(NpcMgr,CSPVPRobotNPC[])
extern "C"  List_1_t225956257 * NpcMgrGenerated_ilo_GetRobotMonsters31_m3865085759 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, CSPVPRobotNPCU5BU5D_t1447782092* ___robotNpcs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity NpcMgrGenerated::ilo_GetSexNpc32(NpcMgr,System.Int32)
extern "C"  CombatEntity_t684137495 * NpcMgrGenerated_ilo_GetSexNpc32_m1120640826 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, int32_t ___sexType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::ilo_Init33(NpcMgr)
extern "C"  void NpcMgrGenerated_ilo_Init33_m2209133021 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Monster NpcMgrGenerated::ilo_InitNpcEntity34(NpcMgr,JSCLevelMonsterConfig,NpcMgr/EditorNpcType,CombatEntity,System.Boolean,System.Boolean)
extern "C"  Monster_t2901270458 * NpcMgrGenerated_ilo_InitNpcEntity34_m2499214382 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, JSCLevelMonsterConfig_t1924079698 * ___lconfig1, int32_t ___type2, CombatEntity_t684137495 * ___SummonEntity3, bool ____isSummon4, bool ___isFindPos5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::ilo_IsCurElementNpc35(NpcMgr,HERO_ELEMENT)
extern "C"  bool NpcMgrGenerated_ilo_IsCurElementNpc35_m3307303703 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, int32_t ___element1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgrGenerated::ilo_IsThreeCountry36(NpcMgr,HERO_COUNTRY)
extern "C"  bool NpcMgrGenerated_ilo_IsThreeCountry36_m1104867319 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, int32_t ___contyr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::ilo_KillTaixuAllEnemy37(NpcMgr,CombatEntity)
extern "C"  void NpcMgrGenerated_ilo_KillTaixuAllEnemy37_m2095100834 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, CombatEntity_t684137495 * ___cbe1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::ilo_OnTriggerOthers38(NpcMgr,CombatEntity)
extern "C"  void NpcMgrGenerated_ilo_OnTriggerOthers38_m2511617159 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, CombatEntity_t684137495 * ___main1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgrGenerated::ilo_OnTriggerUnfinish39(NpcMgr,CombatEntity)
extern "C"  void NpcMgrGenerated_ilo_OnTriggerUnfinish39_m2676306301 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, CombatEntity_t684137495 * ___main1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NpcMgrGenerated::ilo_getArrayLength40(System.Int32)
extern "C"  int32_t NpcMgrGenerated_ilo_getArrayLength40_m1069244140 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NpcMgrGenerated::ilo_getElement41(System.Int32,System.Int32)
extern "C"  int32_t NpcMgrGenerated_ilo_getElement41_m2897291789 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
