﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenRaycastData
struct  ScreenRaycastData_t1110901127 
{
public:
	// System.Boolean ScreenRaycastData::Is2D
	bool ___Is2D_0;
	// UnityEngine.RaycastHit ScreenRaycastData::Hit3D
	RaycastHit_t4003175726  ___Hit3D_1;
	// UnityEngine.RaycastHit2D ScreenRaycastData::Hit2D
	RaycastHit2D_t1374744384  ___Hit2D_2;

public:
	inline static int32_t get_offset_of_Is2D_0() { return static_cast<int32_t>(offsetof(ScreenRaycastData_t1110901127, ___Is2D_0)); }
	inline bool get_Is2D_0() const { return ___Is2D_0; }
	inline bool* get_address_of_Is2D_0() { return &___Is2D_0; }
	inline void set_Is2D_0(bool value)
	{
		___Is2D_0 = value;
	}

	inline static int32_t get_offset_of_Hit3D_1() { return static_cast<int32_t>(offsetof(ScreenRaycastData_t1110901127, ___Hit3D_1)); }
	inline RaycastHit_t4003175726  get_Hit3D_1() const { return ___Hit3D_1; }
	inline RaycastHit_t4003175726 * get_address_of_Hit3D_1() { return &___Hit3D_1; }
	inline void set_Hit3D_1(RaycastHit_t4003175726  value)
	{
		___Hit3D_1 = value;
	}

	inline static int32_t get_offset_of_Hit2D_2() { return static_cast<int32_t>(offsetof(ScreenRaycastData_t1110901127, ___Hit2D_2)); }
	inline RaycastHit2D_t1374744384  get_Hit2D_2() const { return ___Hit2D_2; }
	inline RaycastHit2D_t1374744384 * get_address_of_Hit2D_2() { return &___Hit2D_2; }
	inline void set_Hit2D_2(RaycastHit2D_t1374744384  value)
	{
		___Hit2D_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: ScreenRaycastData
struct ScreenRaycastData_t1110901127_marshaled_pinvoke
{
	int32_t ___Is2D_0;
	RaycastHit_t4003175726_marshaled_pinvoke ___Hit3D_1;
	RaycastHit2D_t1374744384_marshaled_pinvoke ___Hit2D_2;
};
// Native definition for marshalling of: ScreenRaycastData
struct ScreenRaycastData_t1110901127_marshaled_com
{
	int32_t ___Is2D_0;
	RaycastHit_t4003175726_marshaled_com ___Hit3D_1;
	RaycastHit2D_t1374744384_marshaled_com ___Hit2D_2;
};
