﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ProductsCfgMgr/ProductInfo>
struct KeyCollection_t807822589;
// System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>
struct Dictionary_2_t3476030434;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4090966488.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ProductsCfgMgr/ProductInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m3248115228_gshared (KeyCollection_t807822589 * __this, Dictionary_2_t3476030434 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m3248115228(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t807822589 *, Dictionary_2_t3476030434 *, const MethodInfo*))KeyCollection__ctor_m3248115228_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m857844154_gshared (KeyCollection_t807822589 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m857844154(__this, ___item0, method) ((  void (*) (KeyCollection_t807822589 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m857844154_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m492500401_gshared (KeyCollection_t807822589 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m492500401(__this, method) ((  void (*) (KeyCollection_t807822589 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m492500401_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2885203156_gshared (KeyCollection_t807822589 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2885203156(__this, ___item0, method) ((  bool (*) (KeyCollection_t807822589 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2885203156_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2966702905_gshared (KeyCollection_t807822589 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2966702905(__this, ___item0, method) ((  bool (*) (KeyCollection_t807822589 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2966702905_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2396336835_gshared (KeyCollection_t807822589 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2396336835(__this, method) ((  Il2CppObject* (*) (KeyCollection_t807822589 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2396336835_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m4102871203_gshared (KeyCollection_t807822589 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m4102871203(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t807822589 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m4102871203_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1074808946_gshared (KeyCollection_t807822589 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1074808946(__this, method) ((  Il2CppObject * (*) (KeyCollection_t807822589 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1074808946_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1012418357_gshared (KeyCollection_t807822589 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1012418357(__this, method) ((  bool (*) (KeyCollection_t807822589 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1012418357_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4099595815_gshared (KeyCollection_t807822589 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4099595815(__this, method) ((  bool (*) (KeyCollection_t807822589 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4099595815_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m666869081_gshared (KeyCollection_t807822589 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m666869081(__this, method) ((  Il2CppObject * (*) (KeyCollection_t807822589 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m666869081_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ProductsCfgMgr/ProductInfo>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m1785144529_gshared (KeyCollection_t807822589 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m1785144529(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t807822589 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1785144529_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ProductsCfgMgr/ProductInfo>::GetEnumerator()
extern "C"  Enumerator_t4090966488  KeyCollection_GetEnumerator_m3123480542_gshared (KeyCollection_t807822589 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m3123480542(__this, method) ((  Enumerator_t4090966488  (*) (KeyCollection_t807822589 *, const MethodInfo*))KeyCollection_GetEnumerator_m3123480542_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ProductsCfgMgr/ProductInfo>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m2445219169_gshared (KeyCollection_t807822589 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m2445219169(__this, method) ((  int32_t (*) (KeyCollection_t807822589 *, const MethodInfo*))KeyCollection_get_Count_m2445219169_gshared)(__this, method)
