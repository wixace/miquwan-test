﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._a68d852d3d0d498e2cc6c927f5f6f402
struct _a68d852d3d0d498e2cc6c927f5f6f402_t4230635182;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._a68d852d3d0d498e2cc6c927f5f6f402::.ctor()
extern "C"  void _a68d852d3d0d498e2cc6c927f5f6f402__ctor_m3494734655 (_a68d852d3d0d498e2cc6c927f5f6f402_t4230635182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._a68d852d3d0d498e2cc6c927f5f6f402::_a68d852d3d0d498e2cc6c927f5f6f402m2(System.Int32)
extern "C"  int32_t _a68d852d3d0d498e2cc6c927f5f6f402__a68d852d3d0d498e2cc6c927f5f6f402m2_m3441435353 (_a68d852d3d0d498e2cc6c927f5f6f402_t4230635182 * __this, int32_t ____a68d852d3d0d498e2cc6c927f5f6f402a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._a68d852d3d0d498e2cc6c927f5f6f402::_a68d852d3d0d498e2cc6c927f5f6f402m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _a68d852d3d0d498e2cc6c927f5f6f402__a68d852d3d0d498e2cc6c927f5f6f402m_m3911085053 (_a68d852d3d0d498e2cc6c927f5f6f402_t4230635182 * __this, int32_t ____a68d852d3d0d498e2cc6c927f5f6f402a0, int32_t ____a68d852d3d0d498e2cc6c927f5f6f402351, int32_t ____a68d852d3d0d498e2cc6c927f5f6f402c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
