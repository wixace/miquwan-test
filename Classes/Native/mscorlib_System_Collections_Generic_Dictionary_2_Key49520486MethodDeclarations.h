﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2594088108MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m2662497112(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t49520486 *, Dictionary_2_t2717728331 *, const MethodInfo*))KeyCollection__ctor_m2069746375_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1849239294(__this, ___item0, method) ((  void (*) (KeyCollection_t49520486 *, Int2_t1974045593 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3124930095_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2281239541(__this, method) ((  void (*) (KeyCollection_t49520486 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m302680934_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m479494988(__this, ___item0, method) ((  bool (*) (KeyCollection_t49520486 *, Int2_t1974045593 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m163836927_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1484871601(__this, ___item0, method) ((  bool (*) (KeyCollection_t49520486 *, Int2_t1974045593 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2284542244_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m213315953(__this, method) ((  Il2CppObject* (*) (KeyCollection_t49520486 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2963816760_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m3068891367(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t49520486 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1076397784_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m674833058(__this, method) ((  Il2CppObject * (*) (KeyCollection_t49520486 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4286215783_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4114241453(__this, method) ((  bool (*) (KeyCollection_t49520486 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1414555552_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4094207135(__this, method) ((  bool (*) (KeyCollection_t49520486 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m635533394_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2114122059(__this, method) ((  Il2CppObject * (*) (KeyCollection_t49520486 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3949889412_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1218658829(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t49520486 *, Int2U5BU5D_t30096868*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1959794044_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::GetEnumerator()
#define KeyCollection_GetEnumerator_m3545606832(__this, method) ((  Enumerator_t3332664385  (*) (KeyCollection_t49520486 *, const MethodInfo*))KeyCollection_GetEnumerator_m4101563209_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::get_Count()
#define KeyCollection_get_Count_m3701217253(__this, method) ((  int32_t (*) (KeyCollection_t49520486 *, const MethodInfo*))KeyCollection_get_Count_m2937548556_gshared)(__this, method)
