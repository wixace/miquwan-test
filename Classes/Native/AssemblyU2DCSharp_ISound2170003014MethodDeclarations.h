﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISound
struct ISound_t2170003014;

#include "codegen/il2cpp-codegen.h"

// System.Void ISound::.ctor()
extern "C"  void ISound__ctor_m2092332309 (ISound_t2170003014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISound::OnSceneLoaded()
extern "C"  void ISound_OnSceneLoaded_m1947000837 (ISound_t2170003014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ISound::get_mute()
extern "C"  bool ISound_get_mute_m2470045957 (ISound_t2170003014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISound::set_mute(System.Boolean)
extern "C"  void ISound_set_mute_m2932245884 (ISound_t2170003014 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ISound::get_volume()
extern "C"  float ISound_get_volume_m1113396510 (ISound_t2170003014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISound::set_volume(System.Single)
extern "C"  void ISound_set_volume_m1428462757 (ISound_t2170003014 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
