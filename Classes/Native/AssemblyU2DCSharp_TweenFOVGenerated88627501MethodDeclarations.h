﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TweenFOVGenerated
struct TweenFOVGenerated_t88627501;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Camera
struct Camera_t2727095145;
// TweenFOV
struct TweenFOV_t1594102402;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_TweenFOV1594102402.h"

// System.Void TweenFOVGenerated::.ctor()
extern "C"  void TweenFOVGenerated__ctor_m2093504222 (TweenFOVGenerated_t88627501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenFOVGenerated::TweenFOV_TweenFOV1(JSVCall,System.Int32)
extern "C"  bool TweenFOVGenerated_TweenFOV_TweenFOV1_m1973692692 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenFOVGenerated::TweenFOV_from(JSVCall)
extern "C"  void TweenFOVGenerated_TweenFOV_from_m620680612 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenFOVGenerated::TweenFOV_to(JSVCall)
extern "C"  void TweenFOVGenerated_TweenFOV_to_m2252307443 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenFOVGenerated::TweenFOV_cachedCamera(JSVCall)
extern "C"  void TweenFOVGenerated_TweenFOV_cachedCamera_m3615271687 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenFOVGenerated::TweenFOV_value(JSVCall)
extern "C"  void TweenFOVGenerated_TweenFOV_value_m2946628525 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenFOVGenerated::TweenFOV_SetEndToCurrentValue(JSVCall,System.Int32)
extern "C"  bool TweenFOVGenerated_TweenFOV_SetEndToCurrentValue_m3382935401 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenFOVGenerated::TweenFOV_SetStartToCurrentValue(JSVCall,System.Int32)
extern "C"  bool TweenFOVGenerated_TweenFOV_SetStartToCurrentValue_m2736621808 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenFOVGenerated::TweenFOV_Begin__GameObject__Single__Single(JSVCall,System.Int32)
extern "C"  bool TweenFOVGenerated_TweenFOV_Begin__GameObject__Single__Single_m4243074639 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenFOVGenerated::__Register()
extern "C"  void TweenFOVGenerated___Register_m3782394025 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenFOVGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool TweenFOVGenerated_ilo_attachFinalizerObject1_m2104532953 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TweenFOVGenerated::ilo_getSingle2(System.Int32)
extern "C"  float TweenFOVGenerated_ilo_getSingle2_m3744152882 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera TweenFOVGenerated::ilo_get_cachedCamera3(TweenFOV)
extern "C"  Camera_t2727095145 * TweenFOVGenerated_ilo_get_cachedCamera3_m2927134891 (Il2CppObject * __this /* static, unused */, TweenFOV_t1594102402 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TweenFOVGenerated::ilo_get_value4(TweenFOV)
extern "C"  float TweenFOVGenerated_ilo_get_value4_m336784983 (Il2CppObject * __this /* static, unused */, TweenFOV_t1594102402 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenFOVGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void TweenFOVGenerated_ilo_setSingle5_m4292966106 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenFOVGenerated::ilo_set_value6(TweenFOV,System.Single)
extern "C"  void TweenFOVGenerated_ilo_set_value6_m2915372990 (Il2CppObject * __this /* static, unused */, TweenFOV_t1594102402 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenFOVGenerated::ilo_SetEndToCurrentValue7(TweenFOV)
extern "C"  void TweenFOVGenerated_ilo_SetEndToCurrentValue7_m3498985362 (Il2CppObject * __this /* static, unused */, TweenFOV_t1594102402 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenFOVGenerated::ilo_SetStartToCurrentValue8(TweenFOV)
extern "C"  void TweenFOVGenerated_ilo_SetStartToCurrentValue8_m3413983788 (Il2CppObject * __this /* static, unused */, TweenFOV_t1594102402 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
