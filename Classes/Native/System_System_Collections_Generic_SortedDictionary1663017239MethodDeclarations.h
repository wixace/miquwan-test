﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>
struct ValueCollection_t1663017239;
// System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>
struct SortedDictionary_2_t186228230;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "System_System_Collections_Generic_SortedDictionary_250107896.h"

// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m3245595774_gshared (ValueCollection_t1663017239 * __this, SortedDictionary_2_t186228230 * ___dic0, const MethodInfo* method);
#define ValueCollection__ctor_m3245595774(__this, ___dic0, method) ((  void (*) (ValueCollection_t1663017239 *, SortedDictionary_2_t186228230 *, const MethodInfo*))ValueCollection__ctor_m3245595774_gshared)(__this, ___dic0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2235292465_gshared (ValueCollection_t1663017239 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2235292465(__this, ___item0, method) ((  void (*) (ValueCollection_t1663017239 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2235292465_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2499348602_gshared (ValueCollection_t1663017239 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2499348602(__this, method) ((  void (*) (ValueCollection_t1663017239 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2499348602_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2177710849_gshared (ValueCollection_t1663017239 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2177710849(__this, ___item0, method) ((  bool (*) (ValueCollection_t1663017239 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2177710849_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m452886708_gshared (ValueCollection_t1663017239 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m452886708(__this, method) ((  bool (*) (ValueCollection_t1663017239 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m452886708_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3507167206_gshared (ValueCollection_t1663017239 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3507167206(__this, ___item0, method) ((  bool (*) (ValueCollection_t1663017239 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3507167206_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2103690332_gshared (ValueCollection_t1663017239 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2103690332(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1663017239 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2103690332_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m3632453886_gshared (ValueCollection_t1663017239 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m3632453886(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1663017239 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3632453886_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1247360148_gshared (ValueCollection_t1663017239 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1247360148(__this, method) ((  bool (*) (ValueCollection_t1663017239 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1247360148_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1877520852_gshared (ValueCollection_t1663017239 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1877520852(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1663017239 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1877520852_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1245069965_gshared (ValueCollection_t1663017239 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1245069965(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1663017239 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1245069965_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m574297320_gshared (ValueCollection_t1663017239 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define ValueCollection_CopyTo_m574297320(__this, ___array0, ___arrayIndex1, method) ((  void (*) (ValueCollection_t1663017239 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m574297320_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m167761370_gshared (ValueCollection_t1663017239 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m167761370(__this, method) ((  int32_t (*) (ValueCollection_t1663017239 *, const MethodInfo*))ValueCollection_get_Count_m167761370_gshared)(__this, method)
// System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::GetEnumerator()
extern "C"  Enumerator_t50107896  ValueCollection_GetEnumerator_m86662184_gshared (ValueCollection_t1663017239 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m86662184(__this, method) ((  Enumerator_t50107896  (*) (ValueCollection_t1663017239 *, const MethodInfo*))ValueCollection_GetEnumerator_m86662184_gshared)(__this, method)
