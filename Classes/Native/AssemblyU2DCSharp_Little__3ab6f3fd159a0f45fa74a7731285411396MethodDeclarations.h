﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._3ab6f3fd159a0f45fa74a7733adfa48f
struct _3ab6f3fd159a0f45fa74a7733adfa48f_t1285411396;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._3ab6f3fd159a0f45fa74a7733adfa48f::.ctor()
extern "C"  void _3ab6f3fd159a0f45fa74a7733adfa48f__ctor_m1913888873 (_3ab6f3fd159a0f45fa74a7733adfa48f_t1285411396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._3ab6f3fd159a0f45fa74a7733adfa48f::_3ab6f3fd159a0f45fa74a7733adfa48fm2(System.Int32)
extern "C"  int32_t _3ab6f3fd159a0f45fa74a7733adfa48f__3ab6f3fd159a0f45fa74a7733adfa48fm2_m2630208281 (_3ab6f3fd159a0f45fa74a7733adfa48f_t1285411396 * __this, int32_t ____3ab6f3fd159a0f45fa74a7733adfa48fa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._3ab6f3fd159a0f45fa74a7733adfa48f::_3ab6f3fd159a0f45fa74a7733adfa48fm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _3ab6f3fd159a0f45fa74a7733adfa48f__3ab6f3fd159a0f45fa74a7733adfa48fm_m2717551549 (_3ab6f3fd159a0f45fa74a7733adfa48f_t1285411396 * __this, int32_t ____3ab6f3fd159a0f45fa74a7733adfa48fa0, int32_t ____3ab6f3fd159a0f45fa74a7733adfa48f401, int32_t ____3ab6f3fd159a0f45fa74a7733adfa48fc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
