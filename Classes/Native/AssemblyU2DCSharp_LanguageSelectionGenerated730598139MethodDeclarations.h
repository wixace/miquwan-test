﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LanguageSelectionGenerated
struct LanguageSelectionGenerated_t730598139;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void LanguageSelectionGenerated::.ctor()
extern "C"  void LanguageSelectionGenerated__ctor_m1543079936 (LanguageSelectionGenerated_t730598139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LanguageSelectionGenerated::LanguageSelection_LanguageSelection1(JSVCall,System.Int32)
extern "C"  bool LanguageSelectionGenerated_LanguageSelection_LanguageSelection1_m354007632 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LanguageSelectionGenerated::__Register()
extern "C"  void LanguageSelectionGenerated___Register_m2958881991 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LanguageSelectionGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void LanguageSelectionGenerated_ilo_addJSCSRel1_m157621948 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
