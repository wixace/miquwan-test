﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._6aabfcbd84df6da350a38cb91b69e653
struct _6aabfcbd84df6da350a38cb91b69e653_t2550955956;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._6aabfcbd84df6da350a38cb91b69e653::.ctor()
extern "C"  void _6aabfcbd84df6da350a38cb91b69e653__ctor_m2751009017 (_6aabfcbd84df6da350a38cb91b69e653_t2550955956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6aabfcbd84df6da350a38cb91b69e653::_6aabfcbd84df6da350a38cb91b69e653m2(System.Int32)
extern "C"  int32_t _6aabfcbd84df6da350a38cb91b69e653__6aabfcbd84df6da350a38cb91b69e653m2_m4201862425 (_6aabfcbd84df6da350a38cb91b69e653_t2550955956 * __this, int32_t ____6aabfcbd84df6da350a38cb91b69e653a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6aabfcbd84df6da350a38cb91b69e653::_6aabfcbd84df6da350a38cb91b69e653m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _6aabfcbd84df6da350a38cb91b69e653__6aabfcbd84df6da350a38cb91b69e653m_m935084477 (_6aabfcbd84df6da350a38cb91b69e653_t2550955956 * __this, int32_t ____6aabfcbd84df6da350a38cb91b69e653a0, int32_t ____6aabfcbd84df6da350a38cb91b69e653141, int32_t ____6aabfcbd84df6da350a38cb91b69e653c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
