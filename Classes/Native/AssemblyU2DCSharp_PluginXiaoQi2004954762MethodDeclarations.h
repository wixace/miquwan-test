﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginXiaoQi
struct PluginXiaoQi_t2004954762;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// VersionInfo
struct VersionInfo_t2356638086;
// Mihua.SDK.EnterGameInfo
struct EnterGameInfo_t1897532954;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_PluginXiaoQi2004954762.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"

// System.Void PluginXiaoQi::.ctor()
extern "C"  void PluginXiaoQi__ctor_m3545333585 (PluginXiaoQi_t2004954762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoQi::Init()
extern "C"  void PluginXiaoQi_Init_m2836848867 (PluginXiaoQi_t2004954762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginXiaoQi::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginXiaoQi_ReqSDKHttpLogin_m3338405478 (PluginXiaoQi_t2004954762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXiaoQi::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginXiaoQi_IsLoginSuccess_m4130624738 (PluginXiaoQi_t2004954762 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoQi::OpenUserLogin()
extern "C"  void PluginXiaoQi_OpenUserLogin_m146981027 (PluginXiaoQi_t2004954762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoQi::GameLoginSuccess(CEvent.ZEvent)
extern "C"  void PluginXiaoQi_GameLoginSuccess_m4160795548 (PluginXiaoQi_t2004954762 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginXiaoQi::JsonParse(System.String)
extern "C"  String_t* PluginXiaoQi_JsonParse_m3861257163 (PluginXiaoQi_t2004954762 * __this, String_t* ___arg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoQi::UserPay(CEvent.ZEvent)
extern "C"  void PluginXiaoQi_UserPay_m3563611503 (PluginXiaoQi_t2004954762 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoQi::SignCallBack(System.Boolean,System.String)
extern "C"  void PluginXiaoQi_SignCallBack_m590974600 (PluginXiaoQi_t2004954762 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginXiaoQi::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginXiaoQi_BuildOrderParam2WWWForm_m1241669488 (PluginXiaoQi_t2004954762 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoQi::CreateRole(CEvent.ZEvent)
extern "C"  void PluginXiaoQi_CreateRole_m2683800374 (PluginXiaoQi_t2004954762 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoQi::EnterGame(CEvent.ZEvent)
extern "C"  void PluginXiaoQi_EnterGame_m3758266178 (PluginXiaoQi_t2004954762 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoQi::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginXiaoQi_RoleUpgrade_m4256731494 (PluginXiaoQi_t2004954762 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoQi::OnLoginSuccess(System.String)
extern "C"  void PluginXiaoQi_OnLoginSuccess_m3521622934 (PluginXiaoQi_t2004954762 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoQi::OnLogoutSuccess(System.String)
extern "C"  void PluginXiaoQi_OnLogoutSuccess_m1799671225 (PluginXiaoQi_t2004954762 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoQi::OnLogout(System.String)
extern "C"  void PluginXiaoQi_OnLogout_m541695910 (PluginXiaoQi_t2004954762 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoQi::initSdk(System.String)
extern "C"  void PluginXiaoQi_initSdk_m2809951241 (PluginXiaoQi_t2004954762 * __this, String_t* ___appKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoQi::login()
extern "C"  void PluginXiaoQi_login_m3067348408 (PluginXiaoQi_t2004954762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoQi::logout()
extern "C"  void PluginXiaoQi_logout_m604342845 (PluginXiaoQi_t2004954762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoQi::creatRole(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginXiaoQi_creatRole_m4218243004 (PluginXiaoQi_t2004954762 * __this, String_t* ___role_id0, String_t* ___role_name1, String_t* ___role_level2, String_t* ___gold3, String_t* ___vipLv4, String_t* ___server_id5, String_t* ___server_name6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoQi::roleUpgrade(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginXiaoQi_roleUpgrade_m1524345269 (PluginXiaoQi_t2004954762 * __this, String_t* ___role_id0, String_t* ___role_name1, String_t* ___role_level2, String_t* ___gold3, String_t* ___vipLv4, String_t* ___server_id5, String_t* ___server_name6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoQi::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginXiaoQi_pay_m588174979 (PluginXiaoQi_t2004954762 * __this, String_t* ___orderId0, String_t* ___cpSign1, String_t* ___amount2, String_t* ___productName3, String_t* ___serverName4, String_t* ___roleLv5, String_t* ___roleId6, String_t* ___roleName7, String_t* ___notifyId8, String_t* ___extra9, String_t* ___openId10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoQi::<OnLogoutSuccess>m__46A()
extern "C"  void PluginXiaoQi_U3COnLogoutSuccessU3Em__46A_m1519987393 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoQi::<OnLogout>m__46B()
extern "C"  void PluginXiaoQi_U3COnLogoutU3Em__46B_m1603523839 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoQi::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginXiaoQi_ilo_AddEventListener1_m3749759875 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginXiaoQi::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginXiaoQi_ilo_get_Instance2_m2855794387 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoQi::ilo_OpenUserLogin3(PluginXiaoQi)
extern "C"  void PluginXiaoQi_ilo_OpenUserLogin3_m2555249915 (Il2CppObject * __this /* static, unused */, PluginXiaoQi_t2004954762 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoQi::ilo_Log4(System.Object,System.Boolean)
extern "C"  void PluginXiaoQi_ilo_Log4_m1443075743 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXiaoQi::ilo_ContainsKey5(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  bool PluginXiaoQi_ilo_ContainsKey5_m984474715 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken PluginXiaoQi::ilo_get_Item6(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  JToken_t3412245951 * PluginXiaoQi_ilo_get_Item6_m3326505207 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginXiaoQi::ilo_ToString7(Newtonsoft.Json.Linq.JToken)
extern "C"  String_t* PluginXiaoQi_ilo_ToString7_m2339384326 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginXiaoQi::ilo_BuildOrderParam2WWWForm8(PluginXiaoQi,Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginXiaoQi_ilo_BuildOrderParam2WWWForm8_m148642861 (Il2CppObject * __this /* static, unused */, PluginXiaoQi_t2004954762 * ____this0, PayInfo_t1775308120 * ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginXiaoQi::ilo_get_currentVS9(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginXiaoQi_ilo_get_currentVS9_m53099711 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoQi::ilo_LogError10(System.Object,System.Boolean)
extern "C"  void PluginXiaoQi_ilo_LogError10_m128633970 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.EnterGameInfo PluginXiaoQi::ilo_Parse11(System.Object[])
extern "C"  EnterGameInfo_t1897532954 * PluginXiaoQi_ilo_Parse11_m2969381096 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoQi::ilo_creatRole12(PluginXiaoQi,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginXiaoQi_ilo_creatRole12_m624570646 (Il2CppObject * __this /* static, unused */, PluginXiaoQi_t2004954762 * ____this0, String_t* ___role_id1, String_t* ___role_name2, String_t* ___role_level3, String_t* ___gold4, String_t* ___vipLv5, String_t* ___server_id6, String_t* ___server_name7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginXiaoQi::ilo_get_PluginsSdkMgr13()
extern "C"  PluginsSdkMgr_t3884624670 * PluginXiaoQi_ilo_get_PluginsSdkMgr13_m437797566 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoQi::ilo_ReqSDKHttpLogin14(PluginsSdkMgr)
extern "C"  void PluginXiaoQi_ilo_ReqSDKHttpLogin14_m2981008944 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
