﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>
struct Dictionary_2_t1453516396;
// System.Collections.Generic.IEqualityComparer`1<Pathfinding.Int3>
struct IEqualityComparer_1_t2765079998;
// System.Collections.Generic.IDictionary`2<Pathfinding.Int3,System.Object>
struct IDictionary_2_t1031389741;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<Pathfinding.Int3>
struct ICollection_1_t2868635581;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t770439062;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>[]
struct KeyValuePair_2U5BU5D_t1110639483;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>>
struct IEnumerator_1_t3264162151;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Object>
struct KeyCollection_t3080275847;
// System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int3,System.Object>
struct ValueCollection_t154122109;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21352297102.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2770839788.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m2893642011_gshared (Dictionary_2_t1453516396 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m2893642011(__this, method) ((  void (*) (Dictionary_2_t1453516396 *, const MethodInfo*))Dictionary_2__ctor_m2893642011_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m172148754_gshared (Dictionary_2_t1453516396 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m172148754(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t1453516396 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m172148754_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m1057369949_gshared (Dictionary_2_t1453516396 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m1057369949(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t1453516396 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1057369949_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m3949682156_gshared (Dictionary_2_t1453516396 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m3949682156(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t1453516396 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3949682156_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1658990784_gshared (Dictionary_2_t1453516396 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1658990784(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t1453516396 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1658990784_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2194439132_gshared (Dictionary_2_t1453516396 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m2194439132(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1453516396 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m2194439132_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2441655901_gshared (Dictionary_2_t1453516396 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2441655901(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1453516396 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2441655901_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3145572473_gshared (Dictionary_2_t1453516396 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3145572473(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1453516396 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3145572473_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m877989017_gshared (Dictionary_2_t1453516396 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m877989017(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1453516396 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m877989017_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m6442823_gshared (Dictionary_2_t1453516396 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m6442823(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1453516396 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m6442823_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m7039842_gshared (Dictionary_2_t1453516396 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m7039842(__this, method) ((  bool (*) (Dictionary_2_t1453516396 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m7039842_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2670993239_gshared (Dictionary_2_t1453516396 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2670993239(__this, method) ((  bool (*) (Dictionary_2_t1453516396 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2670993239_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m4066372099_gshared (Dictionary_2_t1453516396 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m4066372099(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1453516396 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m4066372099_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m3964585458_gshared (Dictionary_2_t1453516396 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m3964585458(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1453516396 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3964585458_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m2534148383_gshared (Dictionary_2_t1453516396 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m2534148383(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1453516396 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m2534148383_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m1341503923_gshared (Dictionary_2_t1453516396 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1341503923(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1453516396 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1341503923_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m1700750384_gshared (Dictionary_2_t1453516396 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1700750384(__this, ___key0, method) ((  void (*) (Dictionary_2_t1453516396 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1700750384_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3077474945_gshared (Dictionary_2_t1453516396 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3077474945(__this, method) ((  bool (*) (Dictionary_2_t1453516396 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3077474945_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m641973811_gshared (Dictionary_2_t1453516396 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m641973811(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1453516396 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m641973811_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m617492357_gshared (Dictionary_2_t1453516396 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m617492357(__this, method) ((  bool (*) (Dictionary_2_t1453516396 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m617492357_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m965163206_gshared (Dictionary_2_t1453516396 * __this, KeyValuePair_2_t1352297102  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m965163206(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t1453516396 *, KeyValuePair_2_t1352297102 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m965163206_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3660329472_gshared (Dictionary_2_t1453516396 * __this, KeyValuePair_2_t1352297102  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3660329472(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1453516396 *, KeyValuePair_2_t1352297102 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3660329472_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2853028650_gshared (Dictionary_2_t1453516396 * __this, KeyValuePair_2U5BU5D_t1110639483* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2853028650(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1453516396 *, KeyValuePair_2U5BU5D_t1110639483*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2853028650_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1864292645_gshared (Dictionary_2_t1453516396 * __this, KeyValuePair_2_t1352297102  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1864292645(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1453516396 *, KeyValuePair_2_t1352297102 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1864292645_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m2044933257_gshared (Dictionary_2_t1453516396 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m2044933257(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1453516396 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m2044933257_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1944244184_gshared (Dictionary_2_t1453516396 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1944244184(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1453516396 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1944244184_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m204954383_gshared (Dictionary_2_t1453516396 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m204954383(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1453516396 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m204954383_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2608333212_gshared (Dictionary_2_t1453516396 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2608333212(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1453516396 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2608333212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m1167446459_gshared (Dictionary_2_t1453516396 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m1167446459(__this, method) ((  int32_t (*) (Dictionary_2_t1453516396 *, const MethodInfo*))Dictionary_2_get_Count_m1167446459_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m2781619948_gshared (Dictionary_2_t1453516396 * __this, Int3_t1974045594  ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m2781619948(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1453516396 *, Int3_t1974045594 , const MethodInfo*))Dictionary_2_get_Item_m2781619948_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m4094305179_gshared (Dictionary_2_t1453516396 * __this, Int3_t1974045594  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m4094305179(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1453516396 *, Int3_t1974045594 , Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m4094305179_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m2073557139_gshared (Dictionary_2_t1453516396 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m2073557139(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t1453516396 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m2073557139_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m3932461956_gshared (Dictionary_2_t1453516396 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m3932461956(__this, ___size0, method) ((  void (*) (Dictionary_2_t1453516396 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3932461956_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m3848208512_gshared (Dictionary_2_t1453516396 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m3848208512(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1453516396 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3848208512_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t1352297102  Dictionary_2_make_pair_m3166166804_gshared (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m3166166804(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t1352297102  (*) (Il2CppObject * /* static, unused */, Int3_t1974045594 , Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m3166166804_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::pick_key(TKey,TValue)
extern "C"  Int3_t1974045594  Dictionary_2_pick_key_m4285207338_gshared (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m4285207338(__this /* static, unused */, ___key0, ___value1, method) ((  Int3_t1974045594  (*) (Il2CppObject * /* static, unused */, Int3_t1974045594 , Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m4285207338_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m1376307974_gshared (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m1376307974(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Int3_t1974045594 , Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m1376307974_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m1720927055_gshared (Dictionary_2_t1453516396 * __this, KeyValuePair_2U5BU5D_t1110639483* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1720927055(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1453516396 *, KeyValuePair_2U5BU5D_t1110639483*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1720927055_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m3861353085_gshared (Dictionary_2_t1453516396 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m3861353085(__this, method) ((  void (*) (Dictionary_2_t1453516396 *, const MethodInfo*))Dictionary_2_Resize_m3861353085_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m837077498_gshared (Dictionary_2_t1453516396 * __this, Int3_t1974045594  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m837077498(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1453516396 *, Int3_t1974045594 , Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m837077498_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m299775302_gshared (Dictionary_2_t1453516396 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m299775302(__this, method) ((  void (*) (Dictionary_2_t1453516396 *, const MethodInfo*))Dictionary_2_Clear_m299775302_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m2965884016_gshared (Dictionary_2_t1453516396 * __this, Int3_t1974045594  ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m2965884016(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1453516396 *, Int3_t1974045594 , const MethodInfo*))Dictionary_2_ContainsKey_m2965884016_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m713517168_gshared (Dictionary_2_t1453516396 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m713517168(__this, ___value0, method) ((  bool (*) (Dictionary_2_t1453516396 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m713517168_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m3274843961_gshared (Dictionary_2_t1453516396 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m3274843961(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1453516396 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m3274843961_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m2749385931_gshared (Dictionary_2_t1453516396 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m2749385931(__this, ___sender0, method) ((  void (*) (Dictionary_2_t1453516396 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2749385931_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m891709056_gshared (Dictionary_2_t1453516396 * __this, Int3_t1974045594  ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m891709056(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1453516396 *, Int3_t1974045594 , const MethodInfo*))Dictionary_2_Remove_m891709056_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m1874375881_gshared (Dictionary_2_t1453516396 * __this, Int3_t1974045594  ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m1874375881(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t1453516396 *, Int3_t1974045594 , Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m1874375881_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::get_Keys()
extern "C"  KeyCollection_t3080275847 * Dictionary_2_get_Keys_m3983636210_gshared (Dictionary_2_t1453516396 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m3983636210(__this, method) ((  KeyCollection_t3080275847 * (*) (Dictionary_2_t1453516396 *, const MethodInfo*))Dictionary_2_get_Keys_m3983636210_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::get_Values()
extern "C"  ValueCollection_t154122109 * Dictionary_2_get_Values_m1275773582_gshared (Dictionary_2_t1453516396 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m1275773582(__this, method) ((  ValueCollection_t154122109 * (*) (Dictionary_2_t1453516396 *, const MethodInfo*))Dictionary_2_get_Values_m1275773582_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::ToTKey(System.Object)
extern "C"  Int3_t1974045594  Dictionary_2_ToTKey_m3735066245_gshared (Dictionary_2_t1453516396 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m3735066245(__this, ___key0, method) ((  Int3_t1974045594  (*) (Dictionary_2_t1453516396 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m3735066245_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m3983589409_gshared (Dictionary_2_t1453516396 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m3983589409(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t1453516396 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m3983589409_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m1892914275_gshared (Dictionary_2_t1453516396 * __this, KeyValuePair_2_t1352297102  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1892914275(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t1453516396 *, KeyValuePair_2_t1352297102 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1892914275_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2770839788  Dictionary_2_GetEnumerator_m4057390502_gshared (Dictionary_2_t1453516396 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m4057390502(__this, method) ((  Enumerator_t2770839788  (*) (Dictionary_2_t1453516396 *, const MethodInfo*))Dictionary_2_GetEnumerator_m4057390502_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m3176811933_gshared (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m3176811933(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, Int3_t1974045594 , Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m3176811933_gshared)(__this /* static, unused */, ___key0, ___value1, method)
