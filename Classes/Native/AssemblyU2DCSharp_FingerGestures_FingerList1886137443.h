﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<FingerGestures/Finger>
struct List_1_t1550613749;
// FingerGestures/FingerList/FingerPropertyGetterDelegate`1<UnityEngine.Vector2>
struct FingerPropertyGetterDelegate_1_t1594829085;
// FingerGestures/FingerList/FingerPropertyGetterDelegate`1<System.Single>
struct FingerPropertyGetterDelegate_1_t1604681492;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FingerGestures/FingerList
struct  FingerList_t1886137443  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<FingerGestures/Finger> FingerGestures/FingerList::list
	List_1_t1550613749 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(FingerList_t1886137443, ___list_0)); }
	inline List_1_t1550613749 * get_list_0() const { return ___list_0; }
	inline List_1_t1550613749 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t1550613749 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier(&___list_0, value);
	}
};

struct FingerList_t1886137443_StaticFields
{
public:
	// FingerGestures/FingerList/FingerPropertyGetterDelegate`1<UnityEngine.Vector2> FingerGestures/FingerList::delGetFingerStartPosition
	FingerPropertyGetterDelegate_1_t1594829085 * ___delGetFingerStartPosition_1;
	// FingerGestures/FingerList/FingerPropertyGetterDelegate`1<UnityEngine.Vector2> FingerGestures/FingerList::delGetFingerPosition
	FingerPropertyGetterDelegate_1_t1594829085 * ___delGetFingerPosition_2;
	// FingerGestures/FingerList/FingerPropertyGetterDelegate`1<UnityEngine.Vector2> FingerGestures/FingerList::delGetFingerPreviousPosition
	FingerPropertyGetterDelegate_1_t1594829085 * ___delGetFingerPreviousPosition_3;
	// FingerGestures/FingerList/FingerPropertyGetterDelegate`1<System.Single> FingerGestures/FingerList::delGetFingerDistanceFromStart
	FingerPropertyGetterDelegate_1_t1604681492 * ___delGetFingerDistanceFromStart_4;

public:
	inline static int32_t get_offset_of_delGetFingerStartPosition_1() { return static_cast<int32_t>(offsetof(FingerList_t1886137443_StaticFields, ___delGetFingerStartPosition_1)); }
	inline FingerPropertyGetterDelegate_1_t1594829085 * get_delGetFingerStartPosition_1() const { return ___delGetFingerStartPosition_1; }
	inline FingerPropertyGetterDelegate_1_t1594829085 ** get_address_of_delGetFingerStartPosition_1() { return &___delGetFingerStartPosition_1; }
	inline void set_delGetFingerStartPosition_1(FingerPropertyGetterDelegate_1_t1594829085 * value)
	{
		___delGetFingerStartPosition_1 = value;
		Il2CppCodeGenWriteBarrier(&___delGetFingerStartPosition_1, value);
	}

	inline static int32_t get_offset_of_delGetFingerPosition_2() { return static_cast<int32_t>(offsetof(FingerList_t1886137443_StaticFields, ___delGetFingerPosition_2)); }
	inline FingerPropertyGetterDelegate_1_t1594829085 * get_delGetFingerPosition_2() const { return ___delGetFingerPosition_2; }
	inline FingerPropertyGetterDelegate_1_t1594829085 ** get_address_of_delGetFingerPosition_2() { return &___delGetFingerPosition_2; }
	inline void set_delGetFingerPosition_2(FingerPropertyGetterDelegate_1_t1594829085 * value)
	{
		___delGetFingerPosition_2 = value;
		Il2CppCodeGenWriteBarrier(&___delGetFingerPosition_2, value);
	}

	inline static int32_t get_offset_of_delGetFingerPreviousPosition_3() { return static_cast<int32_t>(offsetof(FingerList_t1886137443_StaticFields, ___delGetFingerPreviousPosition_3)); }
	inline FingerPropertyGetterDelegate_1_t1594829085 * get_delGetFingerPreviousPosition_3() const { return ___delGetFingerPreviousPosition_3; }
	inline FingerPropertyGetterDelegate_1_t1594829085 ** get_address_of_delGetFingerPreviousPosition_3() { return &___delGetFingerPreviousPosition_3; }
	inline void set_delGetFingerPreviousPosition_3(FingerPropertyGetterDelegate_1_t1594829085 * value)
	{
		___delGetFingerPreviousPosition_3 = value;
		Il2CppCodeGenWriteBarrier(&___delGetFingerPreviousPosition_3, value);
	}

	inline static int32_t get_offset_of_delGetFingerDistanceFromStart_4() { return static_cast<int32_t>(offsetof(FingerList_t1886137443_StaticFields, ___delGetFingerDistanceFromStart_4)); }
	inline FingerPropertyGetterDelegate_1_t1604681492 * get_delGetFingerDistanceFromStart_4() const { return ___delGetFingerDistanceFromStart_4; }
	inline FingerPropertyGetterDelegate_1_t1604681492 ** get_address_of_delGetFingerDistanceFromStart_4() { return &___delGetFingerDistanceFromStart_4; }
	inline void set_delGetFingerDistanceFromStart_4(FingerPropertyGetterDelegate_1_t1604681492 * value)
	{
		___delGetFingerDistanceFromStart_4 = value;
		Il2CppCodeGenWriteBarrier(&___delGetFingerDistanceFromStart_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
