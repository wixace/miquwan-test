﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24187962917.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3045927365_gshared (KeyValuePair_2_t4187962917 * __this, int32_t ___key0, float ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m3045927365(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t4187962917 *, int32_t, float, const MethodInfo*))KeyValuePair_2__ctor_m3045927365_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2508353667_gshared (KeyValuePair_2_t4187962917 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2508353667(__this, method) ((  int32_t (*) (KeyValuePair_2_t4187962917 *, const MethodInfo*))KeyValuePair_2_get_Key_m2508353667_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3613813060_gshared (KeyValuePair_2_t4187962917 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m3613813060(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4187962917 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m3613813060_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::get_Value()
extern "C"  float KeyValuePair_2_get_Value_m4181526019_gshared (KeyValuePair_2_t4187962917 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m4181526019(__this, method) ((  float (*) (KeyValuePair_2_t4187962917 *, const MethodInfo*))KeyValuePair_2_get_Value_m4181526019_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1948482372_gshared (KeyValuePair_2_t4187962917 * __this, float ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1948482372(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4187962917 *, float, const MethodInfo*))KeyValuePair_2_set_Value_m1948482372_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2793108894_gshared (KeyValuePair_2_t4187962917 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2793108894(__this, method) ((  String_t* (*) (KeyValuePair_2_t4187962917 *, const MethodInfo*))KeyValuePair_2_ToString_m2793108894_gshared)(__this, method)
