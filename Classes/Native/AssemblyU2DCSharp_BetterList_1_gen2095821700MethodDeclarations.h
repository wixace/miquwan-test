﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BetterList`1<UnityEngine.Color32>
struct BetterList_1_t2095821700;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Color32>
struct IEnumerator_1_t2510718737;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2960766953;
// BetterList`1/CompareFunc<UnityEngine.Color32>
struct CompareFunc_t1140572558;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"

// System.Void BetterList`1<UnityEngine.Color32>::.ctor()
extern "C"  void BetterList_1__ctor_m4247846203_gshared (BetterList_1_t2095821700 * __this, const MethodInfo* method);
#define BetterList_1__ctor_m4247846203(__this, method) ((  void (*) (BetterList_1_t2095821700 *, const MethodInfo*))BetterList_1__ctor_m4247846203_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> BetterList`1<UnityEngine.Color32>::GetEnumerator()
extern "C"  Il2CppObject* BetterList_1_GetEnumerator_m2633926575_gshared (BetterList_1_t2095821700 * __this, const MethodInfo* method);
#define BetterList_1_GetEnumerator_m2633926575(__this, method) ((  Il2CppObject* (*) (BetterList_1_t2095821700 *, const MethodInfo*))BetterList_1_GetEnumerator_m2633926575_gshared)(__this, method)
// T BetterList`1<UnityEngine.Color32>::get_Item(System.Int32)
extern "C"  Color32_t598853688  BetterList_1_get_Item_m3645095735_gshared (BetterList_1_t2095821700 * __this, int32_t ___i0, const MethodInfo* method);
#define BetterList_1_get_Item_m3645095735(__this, ___i0, method) ((  Color32_t598853688  (*) (BetterList_1_t2095821700 *, int32_t, const MethodInfo*))BetterList_1_get_Item_m3645095735_gshared)(__this, ___i0, method)
// System.Void BetterList`1<UnityEngine.Color32>::set_Item(System.Int32,T)
extern "C"  void BetterList_1_set_Item_m3879320674_gshared (BetterList_1_t2095821700 * __this, int32_t ___i0, Color32_t598853688  ___value1, const MethodInfo* method);
#define BetterList_1_set_Item_m3879320674(__this, ___i0, ___value1, method) ((  void (*) (BetterList_1_t2095821700 *, int32_t, Color32_t598853688 , const MethodInfo*))BetterList_1_set_Item_m3879320674_gshared)(__this, ___i0, ___value1, method)
// System.Void BetterList`1<UnityEngine.Color32>::AllocateMore()
extern "C"  void BetterList_1_AllocateMore_m3120891995_gshared (BetterList_1_t2095821700 * __this, const MethodInfo* method);
#define BetterList_1_AllocateMore_m3120891995(__this, method) ((  void (*) (BetterList_1_t2095821700 *, const MethodInfo*))BetterList_1_AllocateMore_m3120891995_gshared)(__this, method)
// System.Void BetterList`1<UnityEngine.Color32>::Trim()
extern "C"  void BetterList_1_Trim_m3870855243_gshared (BetterList_1_t2095821700 * __this, const MethodInfo* method);
#define BetterList_1_Trim_m3870855243(__this, method) ((  void (*) (BetterList_1_t2095821700 *, const MethodInfo*))BetterList_1_Trim_m3870855243_gshared)(__this, method)
// System.Void BetterList`1<UnityEngine.Color32>::Clear()
extern "C"  void BetterList_1_Clear_m1653979494_gshared (BetterList_1_t2095821700 * __this, const MethodInfo* method);
#define BetterList_1_Clear_m1653979494(__this, method) ((  void (*) (BetterList_1_t2095821700 *, const MethodInfo*))BetterList_1_Clear_m1653979494_gshared)(__this, method)
// System.Void BetterList`1<UnityEngine.Color32>::Release()
extern "C"  void BetterList_1_Release_m1698279072_gshared (BetterList_1_t2095821700 * __this, const MethodInfo* method);
#define BetterList_1_Release_m1698279072(__this, method) ((  void (*) (BetterList_1_t2095821700 *, const MethodInfo*))BetterList_1_Release_m1698279072_gshared)(__this, method)
// System.Void BetterList`1<UnityEngine.Color32>::Add(T)
extern "C"  void BetterList_1_Add_m3313758180_gshared (BetterList_1_t2095821700 * __this, Color32_t598853688  ___item0, const MethodInfo* method);
#define BetterList_1_Add_m3313758180(__this, ___item0, method) ((  void (*) (BetterList_1_t2095821700 *, Color32_t598853688 , const MethodInfo*))BetterList_1_Add_m3313758180_gshared)(__this, ___item0, method)
// System.Void BetterList`1<UnityEngine.Color32>::Insert(System.Int32,T)
extern "C"  void BetterList_1_Insert_m185393739_gshared (BetterList_1_t2095821700 * __this, int32_t ___index0, Color32_t598853688  ___item1, const MethodInfo* method);
#define BetterList_1_Insert_m185393739(__this, ___index0, ___item1, method) ((  void (*) (BetterList_1_t2095821700 *, int32_t, Color32_t598853688 , const MethodInfo*))BetterList_1_Insert_m185393739_gshared)(__this, ___index0, ___item1, method)
// System.Boolean BetterList`1<UnityEngine.Color32>::Contains(T)
extern "C"  bool BetterList_1_Contains_m3836725002_gshared (BetterList_1_t2095821700 * __this, Color32_t598853688  ___item0, const MethodInfo* method);
#define BetterList_1_Contains_m3836725002(__this, ___item0, method) ((  bool (*) (BetterList_1_t2095821700 *, Color32_t598853688 , const MethodInfo*))BetterList_1_Contains_m3836725002_gshared)(__this, ___item0, method)
// System.Int32 BetterList`1<UnityEngine.Color32>::IndexOf(T)
extern "C"  int32_t BetterList_1_IndexOf_m373303790_gshared (BetterList_1_t2095821700 * __this, Color32_t598853688  ___item0, const MethodInfo* method);
#define BetterList_1_IndexOf_m373303790(__this, ___item0, method) ((  int32_t (*) (BetterList_1_t2095821700 *, Color32_t598853688 , const MethodInfo*))BetterList_1_IndexOf_m373303790_gshared)(__this, ___item0, method)
// System.Boolean BetterList`1<UnityEngine.Color32>::Remove(T)
extern "C"  bool BetterList_1_Remove_m1616822661_gshared (BetterList_1_t2095821700 * __this, Color32_t598853688  ___item0, const MethodInfo* method);
#define BetterList_1_Remove_m1616822661(__this, ___item0, method) ((  bool (*) (BetterList_1_t2095821700 *, Color32_t598853688 , const MethodInfo*))BetterList_1_Remove_m1616822661_gshared)(__this, ___item0, method)
// System.Void BetterList`1<UnityEngine.Color32>::RemoveAt(System.Int32)
extern "C"  void BetterList_1_RemoveAt_m2354213905_gshared (BetterList_1_t2095821700 * __this, int32_t ___index0, const MethodInfo* method);
#define BetterList_1_RemoveAt_m2354213905(__this, ___index0, method) ((  void (*) (BetterList_1_t2095821700 *, int32_t, const MethodInfo*))BetterList_1_RemoveAt_m2354213905_gshared)(__this, ___index0, method)
// T BetterList`1<UnityEngine.Color32>::Pop()
extern "C"  Color32_t598853688  BetterList_1_Pop_m1769585385_gshared (BetterList_1_t2095821700 * __this, const MethodInfo* method);
#define BetterList_1_Pop_m1769585385(__this, method) ((  Color32_t598853688  (*) (BetterList_1_t2095821700 *, const MethodInfo*))BetterList_1_Pop_m1769585385_gshared)(__this, method)
// T[] BetterList`1<UnityEngine.Color32>::ToArray()
extern "C"  Color32U5BU5D_t2960766953* BetterList_1_ToArray_m3421136020_gshared (BetterList_1_t2095821700 * __this, const MethodInfo* method);
#define BetterList_1_ToArray_m3421136020(__this, method) ((  Color32U5BU5D_t2960766953* (*) (BetterList_1_t2095821700 *, const MethodInfo*))BetterList_1_ToArray_m3421136020_gshared)(__this, method)
// System.Void BetterList`1<UnityEngine.Color32>::Sort(BetterList`1/CompareFunc<T>)
extern "C"  void BetterList_1_Sort_m410803614_gshared (BetterList_1_t2095821700 * __this, CompareFunc_t1140572558 * ___comparer0, const MethodInfo* method);
#define BetterList_1_Sort_m410803614(__this, ___comparer0, method) ((  void (*) (BetterList_1_t2095821700 *, CompareFunc_t1140572558 *, const MethodInfo*))BetterList_1_Sort_m410803614_gshared)(__this, ___comparer0, method)
