﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AudioListenerGenerated
struct UnityEngine_AudioListenerGenerated_t3574437229;
// JSVCall
struct JSVCall_t3708497963;
// System.Single[]
struct SingleU5BU5D_t2316563989;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_AudioListenerGenerated::.ctor()
extern "C"  void UnityEngine_AudioListenerGenerated__ctor_m2110916046 (UnityEngine_AudioListenerGenerated_t3574437229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioListenerGenerated::AudioListener_AudioListener1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioListenerGenerated_AudioListener_AudioListener1_m321710614 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioListenerGenerated::AudioListener_volume(JSVCall)
extern "C"  void UnityEngine_AudioListenerGenerated_AudioListener_volume_m917456834 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioListenerGenerated::AudioListener_pause(JSVCall)
extern "C"  void UnityEngine_AudioListenerGenerated_AudioListener_pause_m562814330 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioListenerGenerated::AudioListener_velocityUpdateMode(JSVCall)
extern "C"  void UnityEngine_AudioListenerGenerated_AudioListener_velocityUpdateMode_m799846387 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioListenerGenerated::AudioListener_GetOutputData__Single_Array__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioListenerGenerated_AudioListener_GetOutputData__Single_Array__Int32_m820805754 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioListenerGenerated::AudioListener_GetSpectrumData__Single_Array__Int32__FFTWindow(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioListenerGenerated_AudioListener_GetSpectrumData__Single_Array__Int32__FFTWindow_m1094635676 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioListenerGenerated::__Register()
extern "C"  void UnityEngine_AudioListenerGenerated___Register_m2731599801 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] UnityEngine_AudioListenerGenerated::<AudioListener_GetOutputData__Single_Array__Int32>m__18C()
extern "C"  SingleU5BU5D_t2316563989* UnityEngine_AudioListenerGenerated_U3CAudioListener_GetOutputData__Single_Array__Int32U3Em__18C_m559215537 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] UnityEngine_AudioListenerGenerated::<AudioListener_GetSpectrumData__Single_Array__Int32__FFTWindow>m__18D()
extern "C"  SingleU5BU5D_t2316563989* UnityEngine_AudioListenerGenerated_U3CAudioListener_GetSpectrumData__Single_Array__Int32__FFTWindowU3Em__18D_m149932216 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_AudioListenerGenerated::ilo_getSingle1(System.Int32)
extern "C"  float UnityEngine_AudioListenerGenerated_ilo_getSingle1_m926037273 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioListenerGenerated::ilo_setEnum2(System.Int32,System.Int32)
extern "C"  void UnityEngine_AudioListenerGenerated_ilo_setEnum2_m3947782862 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AudioListenerGenerated::ilo_getInt323(System.Int32)
extern "C"  int32_t UnityEngine_AudioListenerGenerated_ilo_getInt323_m2443748119 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AudioListenerGenerated::ilo_getArrayLength4(System.Int32)
extern "C"  int32_t UnityEngine_AudioListenerGenerated_ilo_getArrayLength4_m4086742311 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AudioListenerGenerated::ilo_getObject5(System.Int32)
extern "C"  int32_t UnityEngine_AudioListenerGenerated_ilo_getObject5_m1651902344 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AudioListenerGenerated::ilo_getElement6(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_AudioListenerGenerated_ilo_getElement6_m2448483499 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
