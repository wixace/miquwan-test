﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t2727095145;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenShake
struct  ScreenShake_t1613343194  : public MonoBehaviour_t667441552
{
public:
	// System.Single ScreenShake::shakeTime
	float ___shakeTime_2;
	// System.Single ScreenShake::fps
	float ___fps_3;
	// System.Single ScreenShake::shakeDelta
	float ___shakeDelta_4;
	// UnityEngine.Camera ScreenShake::cam
	Camera_t2727095145 * ___cam_5;
	// System.Boolean ScreenShake::isshakeCamera
	bool ___isshakeCamera_6;
	// System.Single ScreenShake::frameTime
	float ___frameTime_7;

public:
	inline static int32_t get_offset_of_shakeTime_2() { return static_cast<int32_t>(offsetof(ScreenShake_t1613343194, ___shakeTime_2)); }
	inline float get_shakeTime_2() const { return ___shakeTime_2; }
	inline float* get_address_of_shakeTime_2() { return &___shakeTime_2; }
	inline void set_shakeTime_2(float value)
	{
		___shakeTime_2 = value;
	}

	inline static int32_t get_offset_of_fps_3() { return static_cast<int32_t>(offsetof(ScreenShake_t1613343194, ___fps_3)); }
	inline float get_fps_3() const { return ___fps_3; }
	inline float* get_address_of_fps_3() { return &___fps_3; }
	inline void set_fps_3(float value)
	{
		___fps_3 = value;
	}

	inline static int32_t get_offset_of_shakeDelta_4() { return static_cast<int32_t>(offsetof(ScreenShake_t1613343194, ___shakeDelta_4)); }
	inline float get_shakeDelta_4() const { return ___shakeDelta_4; }
	inline float* get_address_of_shakeDelta_4() { return &___shakeDelta_4; }
	inline void set_shakeDelta_4(float value)
	{
		___shakeDelta_4 = value;
	}

	inline static int32_t get_offset_of_cam_5() { return static_cast<int32_t>(offsetof(ScreenShake_t1613343194, ___cam_5)); }
	inline Camera_t2727095145 * get_cam_5() const { return ___cam_5; }
	inline Camera_t2727095145 ** get_address_of_cam_5() { return &___cam_5; }
	inline void set_cam_5(Camera_t2727095145 * value)
	{
		___cam_5 = value;
		Il2CppCodeGenWriteBarrier(&___cam_5, value);
	}

	inline static int32_t get_offset_of_isshakeCamera_6() { return static_cast<int32_t>(offsetof(ScreenShake_t1613343194, ___isshakeCamera_6)); }
	inline bool get_isshakeCamera_6() const { return ___isshakeCamera_6; }
	inline bool* get_address_of_isshakeCamera_6() { return &___isshakeCamera_6; }
	inline void set_isshakeCamera_6(bool value)
	{
		___isshakeCamera_6 = value;
	}

	inline static int32_t get_offset_of_frameTime_7() { return static_cast<int32_t>(offsetof(ScreenShake_t1613343194, ___frameTime_7)); }
	inline float get_frameTime_7() const { return ___frameTime_7; }
	inline float* get_address_of_frameTime_7() { return &___frameTime_7; }
	inline void set_frameTime_7(float value)
	{
		___frameTime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
