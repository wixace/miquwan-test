﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.SystemTypeSerializer
struct SystemTypeSerializer_t2087191573;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "mscorlib_System_Type2863145774.h"

// System.Void ProtoBuf.Serializers.SystemTypeSerializer::.ctor(ProtoBuf.Meta.TypeModel)
extern "C"  void SystemTypeSerializer__ctor_m2861904649 (SystemTypeSerializer_t2087191573 * __this, TypeModel_t2730011105 * ___model0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.SystemTypeSerializer::.cctor()
extern "C"  void SystemTypeSerializer__cctor_m4234432333 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.SystemTypeSerializer::ProtoBuf.Serializers.IProtoSerializer.Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void SystemTypeSerializer_ProtoBuf_Serializers_IProtoSerializer_Write_m141831498 (SystemTypeSerializer_t2087191573 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.SystemTypeSerializer::ProtoBuf.Serializers.IProtoSerializer.Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * SystemTypeSerializer_ProtoBuf_Serializers_IProtoSerializer_Read_m4163799740 (SystemTypeSerializer_t2087191573 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.SystemTypeSerializer::ProtoBuf.Serializers.IProtoSerializer.get_RequiresOldValue()
extern "C"  bool SystemTypeSerializer_ProtoBuf_Serializers_IProtoSerializer_get_RequiresOldValue_m2636210174 (SystemTypeSerializer_t2087191573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.SystemTypeSerializer::ProtoBuf.Serializers.IProtoSerializer.get_ReturnsValue()
extern "C"  bool SystemTypeSerializer_ProtoBuf_Serializers_IProtoSerializer_get_ReturnsValue_m1813578132 (SystemTypeSerializer_t2087191573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.SystemTypeSerializer::get_ExpectedType()
extern "C"  Type_t * SystemTypeSerializer_get_ExpectedType_m1170973989 (SystemTypeSerializer_t2087191573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.SystemTypeSerializer::ilo_WriteType1(System.Type,ProtoBuf.ProtoWriter)
extern "C"  void SystemTypeSerializer_ilo_WriteType1_m2241994532 (Il2CppObject * __this /* static, unused */, Type_t * ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
