﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.ThreadControlQueue/QueueTerminationException
struct QueueTerminationException_t3888566389;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.ThreadControlQueue/QueueTerminationException::.ctor()
extern "C"  void QueueTerminationException__ctor_m139923910 (QueueTerminationException_t3888566389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
