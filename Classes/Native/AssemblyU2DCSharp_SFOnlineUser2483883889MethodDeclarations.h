﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SFOnlineUser
struct SFOnlineUser_t2483883889;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void SFOnlineUser::.ctor(System.Int64,System.String,System.String,System.String,System.String,System.String)
extern "C"  void SFOnlineUser__ctor_m2604809800 (SFOnlineUser_t2483883889 * __this, int64_t ___id0, String_t* ___channelId1, String_t* ___channelUserId2, String_t* ___userName3, String_t* ___token4, String_t* ___productCode5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 SFOnlineUser::getId()
extern "C"  int64_t SFOnlineUser_getId_m2506116138 (SFOnlineUser_t2483883889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SFOnlineUser::setId(System.Int64)
extern "C"  void SFOnlineUser_setId_m2697114295 (SFOnlineUser_t2483883889 * __this, int64_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SFOnlineUser::getChannelId()
extern "C"  String_t* SFOnlineUser_getChannelId_m2632916229 (SFOnlineUser_t2483883889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SFOnlineUser::setChannelId(System.String)
extern "C"  void SFOnlineUser_setChannelId_m2173800940 (SFOnlineUser_t2483883889 * __this, String_t* ___channelId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SFOnlineUser::getChannelUserId()
extern "C"  String_t* SFOnlineUser_getChannelUserId_m2326373104 (SFOnlineUser_t2483883889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SFOnlineUser::setChannelUserId(System.String)
extern "C"  void SFOnlineUser_setChannelUserId_m1044169313 (SFOnlineUser_t2483883889 * __this, String_t* ___channelUserId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SFOnlineUser::getUserName()
extern "C"  String_t* SFOnlineUser_getUserName_m3169949105 (SFOnlineUser_t2483883889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SFOnlineUser::setUserName(System.String)
extern "C"  void SFOnlineUser_setUserName_m771283778 (SFOnlineUser_t2483883889 * __this, String_t* ___userName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SFOnlineUser::getToken()
extern "C"  String_t* SFOnlineUser_getToken_m1066485184 (SFOnlineUser_t2483883889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SFOnlineUser::setToken(System.String)
extern "C"  void SFOnlineUser_setToken_m3205558417 (SFOnlineUser_t2483883889 * __this, String_t* ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SFOnlineUser::getProductCode()
extern "C"  String_t* SFOnlineUser_getProductCode_m4238326275 (SFOnlineUser_t2483883889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SFOnlineUser::setProductCode(System.String)
extern "C"  void SFOnlineUser_setProductCode_m134990318 (SFOnlineUser_t2483883889 * __this, String_t* ___productCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
