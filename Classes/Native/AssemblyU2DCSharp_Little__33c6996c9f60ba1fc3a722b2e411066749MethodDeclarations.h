﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._33c6996c9f60ba1fc3a722b2e3b286a8
struct _33c6996c9f60ba1fc3a722b2e3b286a8_t411066749;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._33c6996c9f60ba1fc3a722b2e3b286a8::.ctor()
extern "C"  void _33c6996c9f60ba1fc3a722b2e3b286a8__ctor_m1325146448 (_33c6996c9f60ba1fc3a722b2e3b286a8_t411066749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._33c6996c9f60ba1fc3a722b2e3b286a8::_33c6996c9f60ba1fc3a722b2e3b286a8m2(System.Int32)
extern "C"  int32_t _33c6996c9f60ba1fc3a722b2e3b286a8__33c6996c9f60ba1fc3a722b2e3b286a8m2_m231749753 (_33c6996c9f60ba1fc3a722b2e3b286a8_t411066749 * __this, int32_t ____33c6996c9f60ba1fc3a722b2e3b286a8a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._33c6996c9f60ba1fc3a722b2e3b286a8::_33c6996c9f60ba1fc3a722b2e3b286a8m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _33c6996c9f60ba1fc3a722b2e3b286a8__33c6996c9f60ba1fc3a722b2e3b286a8m_m3390302813 (_33c6996c9f60ba1fc3a722b2e3b286a8_t411066749 * __this, int32_t ____33c6996c9f60ba1fc3a722b2e3b286a8a0, int32_t ____33c6996c9f60ba1fc3a722b2e3b286a8891, int32_t ____33c6996c9f60ba1fc3a722b2e3b286a8c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
