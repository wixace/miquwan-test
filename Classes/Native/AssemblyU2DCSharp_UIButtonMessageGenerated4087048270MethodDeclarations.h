﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIButtonMessageGenerated
struct UIButtonMessageGenerated_t4087048270;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"

// System.Void UIButtonMessageGenerated::.ctor()
extern "C"  void UIButtonMessageGenerated__ctor_m2958794253 (UIButtonMessageGenerated_t4087048270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonMessageGenerated::UIButtonMessage_UIButtonMessage1(JSVCall,System.Int32)
extern "C"  bool UIButtonMessageGenerated_UIButtonMessage_UIButtonMessage1_m421795337 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonMessageGenerated::UIButtonMessage_target(JSVCall)
extern "C"  void UIButtonMessageGenerated_UIButtonMessage_target_m1407817057 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonMessageGenerated::UIButtonMessage_functionName(JSVCall)
extern "C"  void UIButtonMessageGenerated_UIButtonMessage_functionName_m3841972143 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonMessageGenerated::UIButtonMessage_trigger(JSVCall)
extern "C"  void UIButtonMessageGenerated_UIButtonMessage_trigger_m1263725058 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonMessageGenerated::UIButtonMessage_includeChildren(JSVCall)
extern "C"  void UIButtonMessageGenerated_UIButtonMessage_includeChildren_m3877674451 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonMessageGenerated::__Register()
extern "C"  void UIButtonMessageGenerated___Register_m1778884314 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIButtonMessageGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UIButtonMessageGenerated_ilo_getObject1_m3374140709 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonMessageGenerated::ilo_setStringS2(System.Int32,System.String)
extern "C"  void UIButtonMessageGenerated_ilo_setStringS2_m4197618391 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIButtonMessageGenerated::ilo_getEnum3(System.Int32)
extern "C"  int32_t UIButtonMessageGenerated_ilo_getEnum3_m77397989 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
