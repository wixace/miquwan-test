﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua.Asset.ABLoadOperation.LevelOperation
struct LevelOperation_t1284581641;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Mihua.Asset.ABLoadOperation.LevelOperation::.ctor(System.String,System.String)
extern "C"  void LevelOperation__ctor_m2975182504 (LevelOperation_t1284581641 * __this, String_t* ___assetbundleName0, String_t* ___levelName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Asset.ABLoadOperation.LevelOperation::IsDone()
extern "C"  bool LevelOperation_IsDone_m3508585838 (LevelOperation_t1284581641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mihua.Asset.ABLoadOperation.LevelOperation::get_progress()
extern "C"  float LevelOperation_get_progress_m1841856336 (LevelOperation_t1284581641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.ABLoadOperation.LevelOperation::Clear()
extern "C"  void LevelOperation_Clear_m3534914433 (LevelOperation_t1284581641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
