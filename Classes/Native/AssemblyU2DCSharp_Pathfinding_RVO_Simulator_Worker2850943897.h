﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.RVO.Sampled.Agent/VO[]
struct VOU5BU5D_t2243217837;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t4024180168;
// System.Single[]
struct SingleU5BU5D_t2316563989;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RVO.Simulator/WorkerContext
struct  WorkerContext_t2850943897  : public Il2CppObject
{
public:
	// Pathfinding.RVO.Sampled.Agent/VO[] Pathfinding.RVO.Simulator/WorkerContext::vos
	VOU5BU5D_t2243217837* ___vos_1;
	// UnityEngine.Vector2[] Pathfinding.RVO.Simulator/WorkerContext::bestPos
	Vector2U5BU5D_t4024180168* ___bestPos_2;
	// System.Single[] Pathfinding.RVO.Simulator/WorkerContext::bestSizes
	SingleU5BU5D_t2316563989* ___bestSizes_3;
	// System.Single[] Pathfinding.RVO.Simulator/WorkerContext::bestScores
	SingleU5BU5D_t2316563989* ___bestScores_4;
	// UnityEngine.Vector2[] Pathfinding.RVO.Simulator/WorkerContext::samplePos
	Vector2U5BU5D_t4024180168* ___samplePos_5;
	// System.Single[] Pathfinding.RVO.Simulator/WorkerContext::sampleSize
	SingleU5BU5D_t2316563989* ___sampleSize_6;

public:
	inline static int32_t get_offset_of_vos_1() { return static_cast<int32_t>(offsetof(WorkerContext_t2850943897, ___vos_1)); }
	inline VOU5BU5D_t2243217837* get_vos_1() const { return ___vos_1; }
	inline VOU5BU5D_t2243217837** get_address_of_vos_1() { return &___vos_1; }
	inline void set_vos_1(VOU5BU5D_t2243217837* value)
	{
		___vos_1 = value;
		Il2CppCodeGenWriteBarrier(&___vos_1, value);
	}

	inline static int32_t get_offset_of_bestPos_2() { return static_cast<int32_t>(offsetof(WorkerContext_t2850943897, ___bestPos_2)); }
	inline Vector2U5BU5D_t4024180168* get_bestPos_2() const { return ___bestPos_2; }
	inline Vector2U5BU5D_t4024180168** get_address_of_bestPos_2() { return &___bestPos_2; }
	inline void set_bestPos_2(Vector2U5BU5D_t4024180168* value)
	{
		___bestPos_2 = value;
		Il2CppCodeGenWriteBarrier(&___bestPos_2, value);
	}

	inline static int32_t get_offset_of_bestSizes_3() { return static_cast<int32_t>(offsetof(WorkerContext_t2850943897, ___bestSizes_3)); }
	inline SingleU5BU5D_t2316563989* get_bestSizes_3() const { return ___bestSizes_3; }
	inline SingleU5BU5D_t2316563989** get_address_of_bestSizes_3() { return &___bestSizes_3; }
	inline void set_bestSizes_3(SingleU5BU5D_t2316563989* value)
	{
		___bestSizes_3 = value;
		Il2CppCodeGenWriteBarrier(&___bestSizes_3, value);
	}

	inline static int32_t get_offset_of_bestScores_4() { return static_cast<int32_t>(offsetof(WorkerContext_t2850943897, ___bestScores_4)); }
	inline SingleU5BU5D_t2316563989* get_bestScores_4() const { return ___bestScores_4; }
	inline SingleU5BU5D_t2316563989** get_address_of_bestScores_4() { return &___bestScores_4; }
	inline void set_bestScores_4(SingleU5BU5D_t2316563989* value)
	{
		___bestScores_4 = value;
		Il2CppCodeGenWriteBarrier(&___bestScores_4, value);
	}

	inline static int32_t get_offset_of_samplePos_5() { return static_cast<int32_t>(offsetof(WorkerContext_t2850943897, ___samplePos_5)); }
	inline Vector2U5BU5D_t4024180168* get_samplePos_5() const { return ___samplePos_5; }
	inline Vector2U5BU5D_t4024180168** get_address_of_samplePos_5() { return &___samplePos_5; }
	inline void set_samplePos_5(Vector2U5BU5D_t4024180168* value)
	{
		___samplePos_5 = value;
		Il2CppCodeGenWriteBarrier(&___samplePos_5, value);
	}

	inline static int32_t get_offset_of_sampleSize_6() { return static_cast<int32_t>(offsetof(WorkerContext_t2850943897, ___sampleSize_6)); }
	inline SingleU5BU5D_t2316563989* get_sampleSize_6() const { return ___sampleSize_6; }
	inline SingleU5BU5D_t2316563989** get_address_of_sampleSize_6() { return &___sampleSize_6; }
	inline void set_sampleSize_6(SingleU5BU5D_t2316563989* value)
	{
		___sampleSize_6 = value;
		Il2CppCodeGenWriteBarrier(&___sampleSize_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
