﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<AnimationRunner/AniType,System.Object>
struct Dictionary_2_t10597543;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2237398247.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<AnimationRunner/AniType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3122902782_gshared (Enumerator_t2237398247 * __this, Dictionary_2_t10597543 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3122902782(__this, ___host0, method) ((  void (*) (Enumerator_t2237398247 *, Dictionary_2_t10597543 *, const MethodInfo*))Enumerator__ctor_m3122902782_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<AnimationRunner/AniType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m545673123_gshared (Enumerator_t2237398247 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m545673123(__this, method) ((  Il2CppObject * (*) (Enumerator_t2237398247 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m545673123_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<AnimationRunner/AniType,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2845554423_gshared (Enumerator_t2237398247 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2845554423(__this, method) ((  void (*) (Enumerator_t2237398247 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2845554423_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<AnimationRunner/AniType,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1085495584_gshared (Enumerator_t2237398247 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1085495584(__this, method) ((  void (*) (Enumerator_t2237398247 *, const MethodInfo*))Enumerator_Dispose_m1085495584_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<AnimationRunner/AniType,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1889137379_gshared (Enumerator_t2237398247 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1889137379(__this, method) ((  bool (*) (Enumerator_t2237398247 *, const MethodInfo*))Enumerator_MoveNext_m1889137379_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<AnimationRunner/AniType,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1879888319_gshared (Enumerator_t2237398247 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1879888319(__this, method) ((  Il2CppObject * (*) (Enumerator_t2237398247 *, const MethodInfo*))Enumerator_get_Current_m1879888319_gshared)(__this, method)
