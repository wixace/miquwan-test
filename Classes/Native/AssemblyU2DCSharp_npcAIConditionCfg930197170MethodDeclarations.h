﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// npcAIConditionCfg
struct npcAIConditionCfg_t930197170;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void npcAIConditionCfg::.ctor()
extern "C"  void npcAIConditionCfg__ctor_m4118177529 (npcAIConditionCfg_t930197170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void npcAIConditionCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void npcAIConditionCfg_Init_m3897506284 (npcAIConditionCfg_t930197170 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
