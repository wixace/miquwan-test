﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._574f9e3afc0310a86ffd2bd98144ab6e
struct _574f9e3afc0310a86ffd2bd98144ab6e_t2054624985;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._574f9e3afc0310a86ffd2bd98144ab6e::.ctor()
extern "C"  void _574f9e3afc0310a86ffd2bd98144ab6e__ctor_m2032513908 (_574f9e3afc0310a86ffd2bd98144ab6e_t2054624985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._574f9e3afc0310a86ffd2bd98144ab6e::_574f9e3afc0310a86ffd2bd98144ab6em2(System.Int32)
extern "C"  int32_t _574f9e3afc0310a86ffd2bd98144ab6e__574f9e3afc0310a86ffd2bd98144ab6em2_m3490667257 (_574f9e3afc0310a86ffd2bd98144ab6e_t2054624985 * __this, int32_t ____574f9e3afc0310a86ffd2bd98144ab6ea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._574f9e3afc0310a86ffd2bd98144ab6e::_574f9e3afc0310a86ffd2bd98144ab6em(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _574f9e3afc0310a86ffd2bd98144ab6e__574f9e3afc0310a86ffd2bd98144ab6em_m2255001565 (_574f9e3afc0310a86ffd2bd98144ab6e_t2054624985 * __this, int32_t ____574f9e3afc0310a86ffd2bd98144ab6ea0, int32_t ____574f9e3afc0310a86ffd2bd98144ab6e691, int32_t ____574f9e3afc0310a86ffd2bd98144ab6ec2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
