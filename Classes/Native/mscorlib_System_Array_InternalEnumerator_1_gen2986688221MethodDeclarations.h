﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2986688221.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24204345545.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m583828209_gshared (InternalEnumerator_1_t2986688221 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m583828209(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2986688221 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m583828209_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3694740239_gshared (InternalEnumerator_1_t2986688221 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3694740239(__this, method) ((  void (*) (InternalEnumerator_1_t2986688221 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3694740239_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1799897083_gshared (InternalEnumerator_1_t2986688221 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1799897083(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2986688221 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1799897083_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m504550920_gshared (InternalEnumerator_1_t2986688221 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m504550920(__this, method) ((  void (*) (InternalEnumerator_1_t2986688221 *, const MethodInfo*))InternalEnumerator_1_Dispose_m504550920_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2643713403_gshared (InternalEnumerator_1_t2986688221 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2643713403(__this, method) ((  bool (*) (InternalEnumerator_1_t2986688221 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2643713403_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t4204345545  InternalEnumerator_1_get_Current_m2630017080_gshared (InternalEnumerator_1_t2986688221 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2630017080(__this, method) ((  KeyValuePair_2_t4204345545  (*) (InternalEnumerator_1_t2986688221 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2630017080_gshared)(__this, method)
