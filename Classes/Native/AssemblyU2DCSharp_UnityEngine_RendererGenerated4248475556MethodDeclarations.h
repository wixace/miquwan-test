﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_RendererGenerated
struct UnityEngine_RendererGenerated_t4248475556;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Material[]
struct MaterialU5BU5D_t170856778;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_RendererGenerated::.ctor()
extern "C"  void UnityEngine_RendererGenerated__ctor_m3938931271 (UnityEngine_RendererGenerated_t4248475556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RendererGenerated::Renderer_Renderer1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RendererGenerated_Renderer_Renderer1_m1632783275 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::Renderer_isPartOfStaticBatch(JSVCall)
extern "C"  void UnityEngine_RendererGenerated_Renderer_isPartOfStaticBatch_m2499259406 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::Renderer_worldToLocalMatrix(JSVCall)
extern "C"  void UnityEngine_RendererGenerated_Renderer_worldToLocalMatrix_m506084327 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::Renderer_localToWorldMatrix(JSVCall)
extern "C"  void UnityEngine_RendererGenerated_Renderer_localToWorldMatrix_m1234417657 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::Renderer_enabled(JSVCall)
extern "C"  void UnityEngine_RendererGenerated_Renderer_enabled_m3797843141 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::Renderer_shadowCastingMode(JSVCall)
extern "C"  void UnityEngine_RendererGenerated_Renderer_shadowCastingMode_m839947872 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::Renderer_receiveShadows(JSVCall)
extern "C"  void UnityEngine_RendererGenerated_Renderer_receiveShadows_m2230981430 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::Renderer_material(JSVCall)
extern "C"  void UnityEngine_RendererGenerated_Renderer_material_m3455258335 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::Renderer_sharedMaterial(JSVCall)
extern "C"  void UnityEngine_RendererGenerated_Renderer_sharedMaterial_m3942375674 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::Renderer_materials(JSVCall)
extern "C"  void UnityEngine_RendererGenerated_Renderer_materials_m2852079674 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::Renderer_sharedMaterials(JSVCall)
extern "C"  void UnityEngine_RendererGenerated_Renderer_sharedMaterials_m772847999 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::Renderer_bounds(JSVCall)
extern "C"  void UnityEngine_RendererGenerated_Renderer_bounds_m2342683921 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::Renderer_lightmapIndex(JSVCall)
extern "C"  void UnityEngine_RendererGenerated_Renderer_lightmapIndex_m3624060026 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::Renderer_realtimeLightmapIndex(JSVCall)
extern "C"  void UnityEngine_RendererGenerated_Renderer_realtimeLightmapIndex_m3288402885 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::Renderer_lightmapScaleOffset(JSVCall)
extern "C"  void UnityEngine_RendererGenerated_Renderer_lightmapScaleOffset_m509519535 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::Renderer_realtimeLightmapScaleOffset(JSVCall)
extern "C"  void UnityEngine_RendererGenerated_Renderer_realtimeLightmapScaleOffset_m3988720058 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::Renderer_isVisible(JSVCall)
extern "C"  void UnityEngine_RendererGenerated_Renderer_isVisible_m3381281790 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::Renderer_useLightProbes(JSVCall)
extern "C"  void UnityEngine_RendererGenerated_Renderer_useLightProbes_m2972827892 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::Renderer_probeAnchor(JSVCall)
extern "C"  void UnityEngine_RendererGenerated_Renderer_probeAnchor_m3929362945 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::Renderer_reflectionProbeUsage(JSVCall)
extern "C"  void UnityEngine_RendererGenerated_Renderer_reflectionProbeUsage_m1864019274 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::Renderer_sortingLayerName(JSVCall)
extern "C"  void UnityEngine_RendererGenerated_Renderer_sortingLayerName_m1337632974 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::Renderer_sortingLayerID(JSVCall)
extern "C"  void UnityEngine_RendererGenerated_Renderer_sortingLayerID_m3563951390 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::Renderer_sortingOrder(JSVCall)
extern "C"  void UnityEngine_RendererGenerated_Renderer_sortingOrder_m129581884 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RendererGenerated::Renderer_GetClosestReflectionProbes__ListT1_ReflectionProbeBlendInfo(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RendererGenerated_Renderer_GetClosestReflectionProbes__ListT1_ReflectionProbeBlendInfo_m964753130 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RendererGenerated::Renderer_GetPropertyBlock__MaterialPropertyBlock(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RendererGenerated_Renderer_GetPropertyBlock__MaterialPropertyBlock_m2081153964 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RendererGenerated::Renderer_SetPropertyBlock__MaterialPropertyBlock(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RendererGenerated_Renderer_SetPropertyBlock__MaterialPropertyBlock_m965700024 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::__Register()
extern "C"  void UnityEngine_RendererGenerated___Register_m2244312800 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material[] UnityEngine_RendererGenerated::<Renderer_materials>m__2CF()
extern "C"  MaterialU5BU5D_t170856778* UnityEngine_RendererGenerated_U3CRenderer_materialsU3Em__2CF_m1566476986 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material[] UnityEngine_RendererGenerated::<Renderer_sharedMaterials>m__2D0()
extern "C"  MaterialU5BU5D_t170856778* UnityEngine_RendererGenerated_U3CRenderer_sharedMaterialsU3Em__2D0_m4154079880 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RendererGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_RendererGenerated_ilo_getObject1_m1387844303 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::ilo_setBooleanS2(System.Int32,System.Boolean)
extern "C"  void UnityEngine_RendererGenerated_ilo_setBooleanS2_m4244271895 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RendererGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_RendererGenerated_ilo_setObject3_m1113833161 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_RendererGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_RendererGenerated_ilo_getObject4_m3705420417 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::ilo_moveSaveID2Arr5(System.Int32)
extern "C"  void UnityEngine_RendererGenerated_ilo_moveSaveID2Arr5_m4118908576 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RendererGenerated::ilo_setInt326(System.Int32,System.Int32)
extern "C"  void UnityEngine_RendererGenerated_ilo_setInt326_m3665759050 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RendererGenerated::ilo_getInt327(System.Int32)
extern "C"  int32_t UnityEngine_RendererGenerated_ilo_getInt327_m347120240 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
