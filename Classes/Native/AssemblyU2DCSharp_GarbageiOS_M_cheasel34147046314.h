﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_cheasel3
struct  M_cheasel3_t4147046314  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_cheasel3::_boujoogas
	float ____boujoogas_0;
	// System.String GarbageiOS.M_cheasel3::_karwo
	String_t* ____karwo_1;
	// System.String GarbageiOS.M_cheasel3::_zoudasMirceecai
	String_t* ____zoudasMirceecai_2;
	// System.Int32 GarbageiOS.M_cheasel3::_fecouzaDelcairda
	int32_t ____fecouzaDelcairda_3;
	// System.Boolean GarbageiOS.M_cheasel3::_cabexeCayca
	bool ____cabexeCayca_4;
	// System.String GarbageiOS.M_cheasel3::_tahereRitawzere
	String_t* ____tahereRitawzere_5;
	// System.Single GarbageiOS.M_cheasel3::_lisku
	float ____lisku_6;
	// System.Int32 GarbageiOS.M_cheasel3::_drasfoudalWhaswhi
	int32_t ____drasfoudalWhaswhi_7;
	// System.Single GarbageiOS.M_cheasel3::_sasxesiWirbi
	float ____sasxesiWirbi_8;
	// System.Boolean GarbageiOS.M_cheasel3::_decheexaiLoowamis
	bool ____decheexaiLoowamis_9;
	// System.Int32 GarbageiOS.M_cheasel3::_palwairSirmallnoo
	int32_t ____palwairSirmallnoo_10;
	// System.UInt32 GarbageiOS.M_cheasel3::_jooseru
	uint32_t ____jooseru_11;
	// System.Int32 GarbageiOS.M_cheasel3::_cepuCooribo
	int32_t ____cepuCooribo_12;
	// System.Boolean GarbageiOS.M_cheasel3::_lawcay
	bool ____lawcay_13;
	// System.Single GarbageiOS.M_cheasel3::_rorqe
	float ____rorqe_14;
	// System.Boolean GarbageiOS.M_cheasel3::_tearlea
	bool ____tearlea_15;
	// System.UInt32 GarbageiOS.M_cheasel3::_noutaYuwapall
	uint32_t ____noutaYuwapall_16;
	// System.UInt32 GarbageiOS.M_cheasel3::_sitriKouya
	uint32_t ____sitriKouya_17;
	// System.Single GarbageiOS.M_cheasel3::_borearcawSuneevair
	float ____borearcawSuneevair_18;
	// System.Boolean GarbageiOS.M_cheasel3::_jayball
	bool ____jayball_19;
	// System.Single GarbageiOS.M_cheasel3::_qarfou
	float ____qarfou_20;
	// System.Int32 GarbageiOS.M_cheasel3::_siqar
	int32_t ____siqar_21;
	// System.String GarbageiOS.M_cheasel3::_nouneetemMaltreyar
	String_t* ____nouneetemMaltreyar_22;
	// System.Int32 GarbageiOS.M_cheasel3::_salpo
	int32_t ____salpo_23;
	// System.Single GarbageiOS.M_cheasel3::_yobisja
	float ____yobisja_24;
	// System.String GarbageiOS.M_cheasel3::_jelasGirmal
	String_t* ____jelasGirmal_25;
	// System.String GarbageiOS.M_cheasel3::_qorki
	String_t* ____qorki_26;
	// System.Single GarbageiOS.M_cheasel3::_bepouJoudoo
	float ____bepouJoudoo_27;
	// System.String GarbageiOS.M_cheasel3::_heler
	String_t* ____heler_28;
	// System.UInt32 GarbageiOS.M_cheasel3::_stasdar
	uint32_t ____stasdar_29;

public:
	inline static int32_t get_offset_of__boujoogas_0() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____boujoogas_0)); }
	inline float get__boujoogas_0() const { return ____boujoogas_0; }
	inline float* get_address_of__boujoogas_0() { return &____boujoogas_0; }
	inline void set__boujoogas_0(float value)
	{
		____boujoogas_0 = value;
	}

	inline static int32_t get_offset_of__karwo_1() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____karwo_1)); }
	inline String_t* get__karwo_1() const { return ____karwo_1; }
	inline String_t** get_address_of__karwo_1() { return &____karwo_1; }
	inline void set__karwo_1(String_t* value)
	{
		____karwo_1 = value;
		Il2CppCodeGenWriteBarrier(&____karwo_1, value);
	}

	inline static int32_t get_offset_of__zoudasMirceecai_2() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____zoudasMirceecai_2)); }
	inline String_t* get__zoudasMirceecai_2() const { return ____zoudasMirceecai_2; }
	inline String_t** get_address_of__zoudasMirceecai_2() { return &____zoudasMirceecai_2; }
	inline void set__zoudasMirceecai_2(String_t* value)
	{
		____zoudasMirceecai_2 = value;
		Il2CppCodeGenWriteBarrier(&____zoudasMirceecai_2, value);
	}

	inline static int32_t get_offset_of__fecouzaDelcairda_3() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____fecouzaDelcairda_3)); }
	inline int32_t get__fecouzaDelcairda_3() const { return ____fecouzaDelcairda_3; }
	inline int32_t* get_address_of__fecouzaDelcairda_3() { return &____fecouzaDelcairda_3; }
	inline void set__fecouzaDelcairda_3(int32_t value)
	{
		____fecouzaDelcairda_3 = value;
	}

	inline static int32_t get_offset_of__cabexeCayca_4() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____cabexeCayca_4)); }
	inline bool get__cabexeCayca_4() const { return ____cabexeCayca_4; }
	inline bool* get_address_of__cabexeCayca_4() { return &____cabexeCayca_4; }
	inline void set__cabexeCayca_4(bool value)
	{
		____cabexeCayca_4 = value;
	}

	inline static int32_t get_offset_of__tahereRitawzere_5() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____tahereRitawzere_5)); }
	inline String_t* get__tahereRitawzere_5() const { return ____tahereRitawzere_5; }
	inline String_t** get_address_of__tahereRitawzere_5() { return &____tahereRitawzere_5; }
	inline void set__tahereRitawzere_5(String_t* value)
	{
		____tahereRitawzere_5 = value;
		Il2CppCodeGenWriteBarrier(&____tahereRitawzere_5, value);
	}

	inline static int32_t get_offset_of__lisku_6() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____lisku_6)); }
	inline float get__lisku_6() const { return ____lisku_6; }
	inline float* get_address_of__lisku_6() { return &____lisku_6; }
	inline void set__lisku_6(float value)
	{
		____lisku_6 = value;
	}

	inline static int32_t get_offset_of__drasfoudalWhaswhi_7() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____drasfoudalWhaswhi_7)); }
	inline int32_t get__drasfoudalWhaswhi_7() const { return ____drasfoudalWhaswhi_7; }
	inline int32_t* get_address_of__drasfoudalWhaswhi_7() { return &____drasfoudalWhaswhi_7; }
	inline void set__drasfoudalWhaswhi_7(int32_t value)
	{
		____drasfoudalWhaswhi_7 = value;
	}

	inline static int32_t get_offset_of__sasxesiWirbi_8() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____sasxesiWirbi_8)); }
	inline float get__sasxesiWirbi_8() const { return ____sasxesiWirbi_8; }
	inline float* get_address_of__sasxesiWirbi_8() { return &____sasxesiWirbi_8; }
	inline void set__sasxesiWirbi_8(float value)
	{
		____sasxesiWirbi_8 = value;
	}

	inline static int32_t get_offset_of__decheexaiLoowamis_9() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____decheexaiLoowamis_9)); }
	inline bool get__decheexaiLoowamis_9() const { return ____decheexaiLoowamis_9; }
	inline bool* get_address_of__decheexaiLoowamis_9() { return &____decheexaiLoowamis_9; }
	inline void set__decheexaiLoowamis_9(bool value)
	{
		____decheexaiLoowamis_9 = value;
	}

	inline static int32_t get_offset_of__palwairSirmallnoo_10() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____palwairSirmallnoo_10)); }
	inline int32_t get__palwairSirmallnoo_10() const { return ____palwairSirmallnoo_10; }
	inline int32_t* get_address_of__palwairSirmallnoo_10() { return &____palwairSirmallnoo_10; }
	inline void set__palwairSirmallnoo_10(int32_t value)
	{
		____palwairSirmallnoo_10 = value;
	}

	inline static int32_t get_offset_of__jooseru_11() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____jooseru_11)); }
	inline uint32_t get__jooseru_11() const { return ____jooseru_11; }
	inline uint32_t* get_address_of__jooseru_11() { return &____jooseru_11; }
	inline void set__jooseru_11(uint32_t value)
	{
		____jooseru_11 = value;
	}

	inline static int32_t get_offset_of__cepuCooribo_12() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____cepuCooribo_12)); }
	inline int32_t get__cepuCooribo_12() const { return ____cepuCooribo_12; }
	inline int32_t* get_address_of__cepuCooribo_12() { return &____cepuCooribo_12; }
	inline void set__cepuCooribo_12(int32_t value)
	{
		____cepuCooribo_12 = value;
	}

	inline static int32_t get_offset_of__lawcay_13() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____lawcay_13)); }
	inline bool get__lawcay_13() const { return ____lawcay_13; }
	inline bool* get_address_of__lawcay_13() { return &____lawcay_13; }
	inline void set__lawcay_13(bool value)
	{
		____lawcay_13 = value;
	}

	inline static int32_t get_offset_of__rorqe_14() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____rorqe_14)); }
	inline float get__rorqe_14() const { return ____rorqe_14; }
	inline float* get_address_of__rorqe_14() { return &____rorqe_14; }
	inline void set__rorqe_14(float value)
	{
		____rorqe_14 = value;
	}

	inline static int32_t get_offset_of__tearlea_15() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____tearlea_15)); }
	inline bool get__tearlea_15() const { return ____tearlea_15; }
	inline bool* get_address_of__tearlea_15() { return &____tearlea_15; }
	inline void set__tearlea_15(bool value)
	{
		____tearlea_15 = value;
	}

	inline static int32_t get_offset_of__noutaYuwapall_16() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____noutaYuwapall_16)); }
	inline uint32_t get__noutaYuwapall_16() const { return ____noutaYuwapall_16; }
	inline uint32_t* get_address_of__noutaYuwapall_16() { return &____noutaYuwapall_16; }
	inline void set__noutaYuwapall_16(uint32_t value)
	{
		____noutaYuwapall_16 = value;
	}

	inline static int32_t get_offset_of__sitriKouya_17() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____sitriKouya_17)); }
	inline uint32_t get__sitriKouya_17() const { return ____sitriKouya_17; }
	inline uint32_t* get_address_of__sitriKouya_17() { return &____sitriKouya_17; }
	inline void set__sitriKouya_17(uint32_t value)
	{
		____sitriKouya_17 = value;
	}

	inline static int32_t get_offset_of__borearcawSuneevair_18() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____borearcawSuneevair_18)); }
	inline float get__borearcawSuneevair_18() const { return ____borearcawSuneevair_18; }
	inline float* get_address_of__borearcawSuneevair_18() { return &____borearcawSuneevair_18; }
	inline void set__borearcawSuneevair_18(float value)
	{
		____borearcawSuneevair_18 = value;
	}

	inline static int32_t get_offset_of__jayball_19() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____jayball_19)); }
	inline bool get__jayball_19() const { return ____jayball_19; }
	inline bool* get_address_of__jayball_19() { return &____jayball_19; }
	inline void set__jayball_19(bool value)
	{
		____jayball_19 = value;
	}

	inline static int32_t get_offset_of__qarfou_20() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____qarfou_20)); }
	inline float get__qarfou_20() const { return ____qarfou_20; }
	inline float* get_address_of__qarfou_20() { return &____qarfou_20; }
	inline void set__qarfou_20(float value)
	{
		____qarfou_20 = value;
	}

	inline static int32_t get_offset_of__siqar_21() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____siqar_21)); }
	inline int32_t get__siqar_21() const { return ____siqar_21; }
	inline int32_t* get_address_of__siqar_21() { return &____siqar_21; }
	inline void set__siqar_21(int32_t value)
	{
		____siqar_21 = value;
	}

	inline static int32_t get_offset_of__nouneetemMaltreyar_22() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____nouneetemMaltreyar_22)); }
	inline String_t* get__nouneetemMaltreyar_22() const { return ____nouneetemMaltreyar_22; }
	inline String_t** get_address_of__nouneetemMaltreyar_22() { return &____nouneetemMaltreyar_22; }
	inline void set__nouneetemMaltreyar_22(String_t* value)
	{
		____nouneetemMaltreyar_22 = value;
		Il2CppCodeGenWriteBarrier(&____nouneetemMaltreyar_22, value);
	}

	inline static int32_t get_offset_of__salpo_23() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____salpo_23)); }
	inline int32_t get__salpo_23() const { return ____salpo_23; }
	inline int32_t* get_address_of__salpo_23() { return &____salpo_23; }
	inline void set__salpo_23(int32_t value)
	{
		____salpo_23 = value;
	}

	inline static int32_t get_offset_of__yobisja_24() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____yobisja_24)); }
	inline float get__yobisja_24() const { return ____yobisja_24; }
	inline float* get_address_of__yobisja_24() { return &____yobisja_24; }
	inline void set__yobisja_24(float value)
	{
		____yobisja_24 = value;
	}

	inline static int32_t get_offset_of__jelasGirmal_25() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____jelasGirmal_25)); }
	inline String_t* get__jelasGirmal_25() const { return ____jelasGirmal_25; }
	inline String_t** get_address_of__jelasGirmal_25() { return &____jelasGirmal_25; }
	inline void set__jelasGirmal_25(String_t* value)
	{
		____jelasGirmal_25 = value;
		Il2CppCodeGenWriteBarrier(&____jelasGirmal_25, value);
	}

	inline static int32_t get_offset_of__qorki_26() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____qorki_26)); }
	inline String_t* get__qorki_26() const { return ____qorki_26; }
	inline String_t** get_address_of__qorki_26() { return &____qorki_26; }
	inline void set__qorki_26(String_t* value)
	{
		____qorki_26 = value;
		Il2CppCodeGenWriteBarrier(&____qorki_26, value);
	}

	inline static int32_t get_offset_of__bepouJoudoo_27() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____bepouJoudoo_27)); }
	inline float get__bepouJoudoo_27() const { return ____bepouJoudoo_27; }
	inline float* get_address_of__bepouJoudoo_27() { return &____bepouJoudoo_27; }
	inline void set__bepouJoudoo_27(float value)
	{
		____bepouJoudoo_27 = value;
	}

	inline static int32_t get_offset_of__heler_28() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____heler_28)); }
	inline String_t* get__heler_28() const { return ____heler_28; }
	inline String_t** get_address_of__heler_28() { return &____heler_28; }
	inline void set__heler_28(String_t* value)
	{
		____heler_28 = value;
		Il2CppCodeGenWriteBarrier(&____heler_28, value);
	}

	inline static int32_t get_offset_of__stasdar_29() { return static_cast<int32_t>(offsetof(M_cheasel3_t4147046314, ____stasdar_29)); }
	inline uint32_t get__stasdar_29() const { return ____stasdar_29; }
	inline uint32_t* get_address_of__stasdar_29() { return &____stasdar_29; }
	inline void set__stasdar_29(uint32_t value)
	{
		____stasdar_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
