﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat3401177016MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Collections.IEnumerator>::.ctor(System.Collections.Generic.Queue`1<T>)
#define Enumerator__ctor_m700443138(__this, ___q0, method) ((  void (*) (Enumerator_t2694935852 *, Queue_1_t1405850340 *, const MethodInfo*))Enumerator__ctor_m3846579967_gshared)(__this, ___q0, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Collections.IEnumerator>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2526240061(__this, method) ((  void (*) (Enumerator_t2694935852 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2396412922_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<System.Collections.IEnumerator>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1217664361(__this, method) ((  Il2CppObject * (*) (Enumerator_t2694935852 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m280731440_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Collections.IEnumerator>::Dispose()
#define Enumerator_Dispose_m592456986(__this, method) ((  void (*) (Enumerator_t2694935852 *, const MethodInfo*))Enumerator_Dispose_m3069945149_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<System.Collections.IEnumerator>::MoveNext()
#define Enumerator_MoveNext_m2342194197(__this, method) ((  bool (*) (Enumerator_t2694935852 *, const MethodInfo*))Enumerator_MoveNext_m262168254_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<System.Collections.IEnumerator>::get_Current()
#define Enumerator_get_Current_m1377360138(__this, method) ((  Il2CppObject * (*) (Enumerator_t2694935852 *, const MethodInfo*))Enumerator_get_Current_m3381813839_gshared)(__this, method)
