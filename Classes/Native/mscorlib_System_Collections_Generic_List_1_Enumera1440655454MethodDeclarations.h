﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Core.RpsChoice>
struct List_1_t1420982684;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1440655454.h"
#include "AssemblyU2DCSharp_Core_RpsChoice52797132.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Core.RpsChoice>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m347781690_gshared (Enumerator_t1440655454 * __this, List_1_t1420982684 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m347781690(__this, ___l0, method) ((  void (*) (Enumerator_t1440655454 *, List_1_t1420982684 *, const MethodInfo*))Enumerator__ctor_m347781690_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Core.RpsChoice>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1411603928_gshared (Enumerator_t1440655454 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1411603928(__this, method) ((  void (*) (Enumerator_t1440655454 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1411603928_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Core.RpsChoice>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1771233294_gshared (Enumerator_t1440655454 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1771233294(__this, method) ((  Il2CppObject * (*) (Enumerator_t1440655454 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1771233294_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Core.RpsChoice>::Dispose()
extern "C"  void Enumerator_Dispose_m1280702111_gshared (Enumerator_t1440655454 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1280702111(__this, method) ((  void (*) (Enumerator_t1440655454 *, const MethodInfo*))Enumerator_Dispose_m1280702111_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Core.RpsChoice>::VerifyState()
extern "C"  void Enumerator_VerifyState_m375344728_gshared (Enumerator_t1440655454 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m375344728(__this, method) ((  void (*) (Enumerator_t1440655454 *, const MethodInfo*))Enumerator_VerifyState_m375344728_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Core.RpsChoice>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4131464200_gshared (Enumerator_t1440655454 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m4131464200(__this, method) ((  bool (*) (Enumerator_t1440655454 *, const MethodInfo*))Enumerator_MoveNext_m4131464200_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Core.RpsChoice>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2760603185_gshared (Enumerator_t1440655454 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2760603185(__this, method) ((  int32_t (*) (Enumerator_t1440655454 *, const MethodInfo*))Enumerator_get_Current_m2760603185_gshared)(__this, method)
