﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;
// System.UInt32[]
struct UInt32U5BU5D_t3230734560;
// Pathfinding.GraphNode[]
struct GraphNodeU5BU5D_t927449255;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_HeuristicOptimizatio2263189308.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.EuclideanEmbedding
struct  EuclideanEmbedding_t908139023  : public Il2CppObject
{
public:
	// Pathfinding.HeuristicOptimizationMode Pathfinding.EuclideanEmbedding::mode
	int32_t ___mode_0;
	// System.Int32 Pathfinding.EuclideanEmbedding::seed
	int32_t ___seed_1;
	// UnityEngine.Transform Pathfinding.EuclideanEmbedding::pivotPointRoot
	Transform_t1659122786 * ___pivotPointRoot_2;
	// System.Int32 Pathfinding.EuclideanEmbedding::spreadOutCount
	int32_t ___spreadOutCount_3;
	// System.UInt32[] Pathfinding.EuclideanEmbedding::costs
	UInt32U5BU5D_t3230734560* ___costs_4;
	// System.Int32 Pathfinding.EuclideanEmbedding::maxNodeIndex
	int32_t ___maxNodeIndex_5;
	// System.Int32 Pathfinding.EuclideanEmbedding::pivotCount
	int32_t ___pivotCount_6;
	// System.Boolean Pathfinding.EuclideanEmbedding::dirty
	bool ___dirty_7;
	// Pathfinding.GraphNode[] Pathfinding.EuclideanEmbedding::pivots
	GraphNodeU5BU5D_t927449255* ___pivots_8;
	// System.UInt32 Pathfinding.EuclideanEmbedding::ra
	uint32_t ___ra_9;
	// System.UInt32 Pathfinding.EuclideanEmbedding::rc
	uint32_t ___rc_10;
	// System.UInt32 Pathfinding.EuclideanEmbedding::rval
	uint32_t ___rval_11;
	// System.Object Pathfinding.EuclideanEmbedding::lockObj
	Il2CppObject * ___lockObj_12;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(EuclideanEmbedding_t908139023, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_seed_1() { return static_cast<int32_t>(offsetof(EuclideanEmbedding_t908139023, ___seed_1)); }
	inline int32_t get_seed_1() const { return ___seed_1; }
	inline int32_t* get_address_of_seed_1() { return &___seed_1; }
	inline void set_seed_1(int32_t value)
	{
		___seed_1 = value;
	}

	inline static int32_t get_offset_of_pivotPointRoot_2() { return static_cast<int32_t>(offsetof(EuclideanEmbedding_t908139023, ___pivotPointRoot_2)); }
	inline Transform_t1659122786 * get_pivotPointRoot_2() const { return ___pivotPointRoot_2; }
	inline Transform_t1659122786 ** get_address_of_pivotPointRoot_2() { return &___pivotPointRoot_2; }
	inline void set_pivotPointRoot_2(Transform_t1659122786 * value)
	{
		___pivotPointRoot_2 = value;
		Il2CppCodeGenWriteBarrier(&___pivotPointRoot_2, value);
	}

	inline static int32_t get_offset_of_spreadOutCount_3() { return static_cast<int32_t>(offsetof(EuclideanEmbedding_t908139023, ___spreadOutCount_3)); }
	inline int32_t get_spreadOutCount_3() const { return ___spreadOutCount_3; }
	inline int32_t* get_address_of_spreadOutCount_3() { return &___spreadOutCount_3; }
	inline void set_spreadOutCount_3(int32_t value)
	{
		___spreadOutCount_3 = value;
	}

	inline static int32_t get_offset_of_costs_4() { return static_cast<int32_t>(offsetof(EuclideanEmbedding_t908139023, ___costs_4)); }
	inline UInt32U5BU5D_t3230734560* get_costs_4() const { return ___costs_4; }
	inline UInt32U5BU5D_t3230734560** get_address_of_costs_4() { return &___costs_4; }
	inline void set_costs_4(UInt32U5BU5D_t3230734560* value)
	{
		___costs_4 = value;
		Il2CppCodeGenWriteBarrier(&___costs_4, value);
	}

	inline static int32_t get_offset_of_maxNodeIndex_5() { return static_cast<int32_t>(offsetof(EuclideanEmbedding_t908139023, ___maxNodeIndex_5)); }
	inline int32_t get_maxNodeIndex_5() const { return ___maxNodeIndex_5; }
	inline int32_t* get_address_of_maxNodeIndex_5() { return &___maxNodeIndex_5; }
	inline void set_maxNodeIndex_5(int32_t value)
	{
		___maxNodeIndex_5 = value;
	}

	inline static int32_t get_offset_of_pivotCount_6() { return static_cast<int32_t>(offsetof(EuclideanEmbedding_t908139023, ___pivotCount_6)); }
	inline int32_t get_pivotCount_6() const { return ___pivotCount_6; }
	inline int32_t* get_address_of_pivotCount_6() { return &___pivotCount_6; }
	inline void set_pivotCount_6(int32_t value)
	{
		___pivotCount_6 = value;
	}

	inline static int32_t get_offset_of_dirty_7() { return static_cast<int32_t>(offsetof(EuclideanEmbedding_t908139023, ___dirty_7)); }
	inline bool get_dirty_7() const { return ___dirty_7; }
	inline bool* get_address_of_dirty_7() { return &___dirty_7; }
	inline void set_dirty_7(bool value)
	{
		___dirty_7 = value;
	}

	inline static int32_t get_offset_of_pivots_8() { return static_cast<int32_t>(offsetof(EuclideanEmbedding_t908139023, ___pivots_8)); }
	inline GraphNodeU5BU5D_t927449255* get_pivots_8() const { return ___pivots_8; }
	inline GraphNodeU5BU5D_t927449255** get_address_of_pivots_8() { return &___pivots_8; }
	inline void set_pivots_8(GraphNodeU5BU5D_t927449255* value)
	{
		___pivots_8 = value;
		Il2CppCodeGenWriteBarrier(&___pivots_8, value);
	}

	inline static int32_t get_offset_of_ra_9() { return static_cast<int32_t>(offsetof(EuclideanEmbedding_t908139023, ___ra_9)); }
	inline uint32_t get_ra_9() const { return ___ra_9; }
	inline uint32_t* get_address_of_ra_9() { return &___ra_9; }
	inline void set_ra_9(uint32_t value)
	{
		___ra_9 = value;
	}

	inline static int32_t get_offset_of_rc_10() { return static_cast<int32_t>(offsetof(EuclideanEmbedding_t908139023, ___rc_10)); }
	inline uint32_t get_rc_10() const { return ___rc_10; }
	inline uint32_t* get_address_of_rc_10() { return &___rc_10; }
	inline void set_rc_10(uint32_t value)
	{
		___rc_10 = value;
	}

	inline static int32_t get_offset_of_rval_11() { return static_cast<int32_t>(offsetof(EuclideanEmbedding_t908139023, ___rval_11)); }
	inline uint32_t get_rval_11() const { return ___rval_11; }
	inline uint32_t* get_address_of_rval_11() { return &___rval_11; }
	inline void set_rval_11(uint32_t value)
	{
		___rval_11 = value;
	}

	inline static int32_t get_offset_of_lockObj_12() { return static_cast<int32_t>(offsetof(EuclideanEmbedding_t908139023, ___lockObj_12)); }
	inline Il2CppObject * get_lockObj_12() const { return ___lockObj_12; }
	inline Il2CppObject ** get_address_of_lockObj_12() { return &___lockObj_12; }
	inline void set_lockObj_12(Il2CppObject * value)
	{
		___lockObj_12 = value;
		Il2CppCodeGenWriteBarrier(&___lockObj_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
