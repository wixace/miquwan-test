﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// FloatTweener/setValue
struct setValue_t3220385890;

#include "AssemblyU2DCSharp_UITweener105489188.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FloatTweener
struct  FloatTweener_t1127747196  : public UITweener_t105489188
{
public:
	// System.Single FloatTweener::from
	float ___from_21;
	// System.Single FloatTweener::to
	float ___to_22;
	// FloatTweener/setValue FloatTweener::fn
	setValue_t3220385890 * ___fn_23;

public:
	inline static int32_t get_offset_of_from_21() { return static_cast<int32_t>(offsetof(FloatTweener_t1127747196, ___from_21)); }
	inline float get_from_21() const { return ___from_21; }
	inline float* get_address_of_from_21() { return &___from_21; }
	inline void set_from_21(float value)
	{
		___from_21 = value;
	}

	inline static int32_t get_offset_of_to_22() { return static_cast<int32_t>(offsetof(FloatTweener_t1127747196, ___to_22)); }
	inline float get_to_22() const { return ___to_22; }
	inline float* get_address_of_to_22() { return &___to_22; }
	inline void set_to_22(float value)
	{
		___to_22 = value;
	}

	inline static int32_t get_offset_of_fn_23() { return static_cast<int32_t>(offsetof(FloatTweener_t1127747196, ___fn_23)); }
	inline setValue_t3220385890 * get_fn_23() const { return ___fn_23; }
	inline setValue_t3220385890 ** get_address_of_fn_23() { return &___fn_23; }
	inline void set_fn_23(setValue_t3220385890 * value)
	{
		___fn_23 = value;
		Il2CppCodeGenWriteBarrier(&___fn_23, value);
	}
};

struct FloatTweener_t1127747196_StaticFields
{
public:
	// UnityEngine.GameObject FloatTweener::placeholder_
	GameObject_t3674682005 * ___placeholder__20;

public:
	inline static int32_t get_offset_of_placeholder__20() { return static_cast<int32_t>(offsetof(FloatTweener_t1127747196_StaticFields, ___placeholder__20)); }
	inline GameObject_t3674682005 * get_placeholder__20() const { return ___placeholder__20; }
	inline GameObject_t3674682005 ** get_address_of_placeholder__20() { return &___placeholder__20; }
	inline void set_placeholder__20(GameObject_t3674682005 * value)
	{
		___placeholder__20 = value;
		Il2CppCodeGenWriteBarrier(&___placeholder__20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
