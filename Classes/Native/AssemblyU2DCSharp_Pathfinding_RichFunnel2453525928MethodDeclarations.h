﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RichFunnel
struct RichFunnel_t2453525928;
// Pathfinding.RichPath
struct RichPath_t1926198167;
// Pathfinding.IFunnelGraph
struct IFunnelGraph_t2367181445;
// System.Collections.Generic.List`1<Pathfinding.GraphNode>
struct List_1_t1391797922;
// Pathfinding.IRaycastableGraph
struct IRaycastableGraph_t2032416694;
// Pathfinding.TriangleMeshNode
struct TriangleMeshNode_t1626248749;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// Pathfinding.MeshNode
struct MeshNode_t3005053445;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_RichPath1926198167.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_TriangleMeshNode1626248749.h"
#include "AssemblyU2DCSharp_Pathfinding_MeshNode3005053445.h"
#include "AssemblyU2DCSharp_Pathfinding_RichFunnel2453525928.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphHitInfo3082627369.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

// System.Void Pathfinding.RichFunnel::.ctor()
extern "C"  void RichFunnel__ctor_m1913697215 (RichFunnel_t2453525928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RichFunnel Pathfinding.RichFunnel::Initialize(Pathfinding.RichPath,Pathfinding.IFunnelGraph)
extern "C"  RichFunnel_t2453525928 * RichFunnel_Initialize_m2991769186 (RichFunnel_t2453525928 * __this, RichPath_t1926198167 * ___path0, Il2CppObject * ___graph1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichFunnel::OnEnterPool()
extern "C"  void RichFunnel_OnEnterPool_m1882177170 (RichFunnel_t2453525928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichFunnel::BuildFunnelCorridor(System.Collections.Generic.List`1<Pathfinding.GraphNode>,System.Int32,System.Int32)
extern "C"  void RichFunnel_BuildFunnelCorridor_m3999044053 (RichFunnel_t2453525928 * __this, List_1_t1391797922 * ___nodes0, int32_t ___start1, int32_t ___end2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichFunnel::SimplifyPath3(Pathfinding.IRaycastableGraph,System.Collections.Generic.List`1<Pathfinding.GraphNode>,System.Int32,System.Int32,System.Collections.Generic.List`1<Pathfinding.GraphNode>,UnityEngine.Vector3,UnityEngine.Vector3,System.Int32)
extern "C"  void RichFunnel_SimplifyPath3_m1462593035 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___rcg0, List_1_t1391797922 * ___nodes1, int32_t ___start2, int32_t ___end3, List_1_t1391797922 * ___result4, Vector3_t4282066566  ___startPoint5, Vector3_t4282066566  ___endPoint6, int32_t ___depth7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichFunnel::SimplifyPath2(Pathfinding.IRaycastableGraph,System.Collections.Generic.List`1<Pathfinding.GraphNode>,System.Int32,System.Int32,System.Collections.Generic.List`1<Pathfinding.GraphNode>,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void RichFunnel_SimplifyPath2_m645424109 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___rcg0, List_1_t1391797922 * ___nodes1, int32_t ___start2, int32_t ___end3, List_1_t1391797922 * ___result4, Vector3_t4282066566  ___startPoint5, Vector3_t4282066566  ___endPoint6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichFunnel::SimplifyPath(Pathfinding.IRaycastableGraph,System.Collections.Generic.List`1<Pathfinding.GraphNode>,System.Int32,System.Int32,System.Collections.Generic.List`1<Pathfinding.GraphNode>,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void RichFunnel_SimplifyPath_m3009119291 (RichFunnel_t2453525928 * __this, Il2CppObject * ___graph0, List_1_t1391797922 * ___nodes1, int32_t ___start2, int32_t ___end3, List_1_t1391797922 * ___result4, Vector3_t4282066566  ___startPoint5, Vector3_t4282066566  ___endPoint6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichFunnel::UpdateFunnelCorridor(System.Int32,Pathfinding.TriangleMeshNode)
extern "C"  void RichFunnel_UpdateFunnelCorridor_m85247494 (RichFunnel_t2453525928 * __this, int32_t ___splitIndex0, TriangleMeshNode_t1626248749 * ___prefix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RichFunnel::Update(UnityEngine.Vector3,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Int32,System.Boolean&,System.Boolean&)
extern "C"  Vector3_t4282066566  RichFunnel_Update_m3381921923 (RichFunnel_t2453525928 * __this, Vector3_t4282066566  ___position0, List_1_t1355284822 * ___buffer1, int32_t ___numCorners2, bool* ___lastCorner3, bool* ___requiresRepath4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichFunnel::FindWalls(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Single)
extern "C"  void RichFunnel_FindWalls_m1764291311 (RichFunnel_t2453525928 * __this, List_1_t1355284822 * ___wallBuffer0, float ___range1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichFunnel::FindWalls(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector3>,UnityEngine.Vector3,System.Single)
extern "C"  void RichFunnel_FindWalls_m1905435293 (RichFunnel_t2453525928 * __this, int32_t ___nodeIndex0, List_1_t1355284822 * ___wallBuffer1, Vector3_t4282066566  ___position2, float ___range3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RichFunnel::FindNextCorners(UnityEngine.Vector3,System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Int32,System.Boolean&)
extern "C"  bool RichFunnel_FindNextCorners_m2401298428 (RichFunnel_t2453525928 * __this, Vector3_t4282066566  ___origin0, int32_t ___startIndex1, List_1_t1355284822 * ___funnelPath2, int32_t ___numCorners3, bool* ___lastCorner4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RichFunnel::ilo_ClosestPointOnNode1(Pathfinding.MeshNode,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  RichFunnel_ilo_ClosestPointOnNode1_m2900607523 (Il2CppObject * __this /* static, unused */, MeshNode_t3005053445 * ____this0, Vector3_t4282066566  ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichFunnel::ilo_SimplifyPath2(Pathfinding.RichFunnel,Pathfinding.IRaycastableGraph,System.Collections.Generic.List`1<Pathfinding.GraphNode>,System.Int32,System.Int32,System.Collections.Generic.List`1<Pathfinding.GraphNode>,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void RichFunnel_ilo_SimplifyPath2_m1052176474 (Il2CppObject * __this /* static, unused */, RichFunnel_t2453525928 * ____this0, Il2CppObject * ___graph1, List_1_t1391797922 * ___nodes2, int32_t ___start3, int32_t ___end4, List_1_t1391797922 * ___result5, Vector3_t4282066566  ___startPoint6, Vector3_t4282066566  ___endPoint7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichFunnel::ilo_SimplifyPath23(Pathfinding.IRaycastableGraph,System.Collections.Generic.List`1<Pathfinding.GraphNode>,System.Int32,System.Int32,System.Collections.Generic.List`1<Pathfinding.GraphNode>,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void RichFunnel_ilo_SimplifyPath23_m2749613831 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___rcg0, List_1_t1391797922 * ___nodes1, int32_t ___start2, int32_t ___end3, List_1_t1391797922 * ___result4, Vector3_t4282066566  ___startPoint5, Vector3_t4282066566  ___endPoint6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RichFunnel::ilo_GetPortal4(Pathfinding.TriangleMeshNode,Pathfinding.GraphNode,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Boolean)
extern "C"  bool RichFunnel_ilo_GetPortal4_m612491258 (Il2CppObject * __this /* static, unused */, TriangleMeshNode_t1626248749 * ____this0, GraphNode_t23612370 * ____other1, List_1_t1355284822 * ___left2, List_1_t1355284822 * ___right3, bool ___backwards4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RichFunnel::ilo_Linecast5(Pathfinding.IRaycastableGraph,UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode,Pathfinding.GraphHitInfo&,System.Collections.Generic.List`1<Pathfinding.GraphNode>)
extern "C"  bool RichFunnel_ilo_Linecast5_m4096351217 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Vector3_t4282066566  ___start1, Vector3_t4282066566  ___end2, GraphNode_t23612370 * ___hint3, GraphHitInfo_t3082627369 * ___hit4, List_1_t1391797922 * ___trace5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RichFunnel::ilo_op_Explicit6(Pathfinding.Int3)
extern "C"  Vector3_t4282066566  RichFunnel_ilo_op_Explicit6_m4224662888 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichFunnel::ilo_SimplifyPath37(Pathfinding.IRaycastableGraph,System.Collections.Generic.List`1<Pathfinding.GraphNode>,System.Int32,System.Int32,System.Collections.Generic.List`1<Pathfinding.GraphNode>,UnityEngine.Vector3,UnityEngine.Vector3,System.Int32)
extern "C"  void RichFunnel_ilo_SimplifyPath37_m1414669651 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___rcg0, List_1_t1391797922 * ___nodes1, int32_t ___start2, int32_t ___end3, List_1_t1391797922 * ___result4, Vector3_t4282066566  ___startPoint5, Vector3_t4282066566  ___endPoint6, int32_t ___depth7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RichFunnel::ilo_DistancePointSegmentStrict8(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float RichFunnel_ilo_DistancePointSegmentStrict8_m1109825162 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, Vector3_t4282066566  ___p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.RichFunnel::ilo_get_Penalty9(Pathfinding.GraphNode)
extern "C"  uint32_t RichFunnel_ilo_get_Penalty9_m1335216672 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.RichFunnel::ilo_get_Tag10(Pathfinding.GraphNode)
extern "C"  uint32_t RichFunnel_ilo_get_Tag10_m2506802925 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RichFunnel::ilo_get_Destroyed11(Pathfinding.GraphNode)
extern "C"  bool RichFunnel_ilo_get_Destroyed11_m1646582854 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RichFunnel::ilo_ContainsPoint12(Pathfinding.TriangleMeshNode,Pathfinding.Int3)
extern "C"  bool RichFunnel_ilo_ContainsPoint12_m2863631921 (Il2CppObject * __this /* static, unused */, TriangleMeshNode_t1626248749 * ____this0, Int3_t1974045594  ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichFunnel::ilo_UpdateFunnelCorridor13(Pathfinding.RichFunnel,System.Int32,Pathfinding.TriangleMeshNode)
extern "C"  void RichFunnel_ilo_UpdateFunnelCorridor13_m1417048469 (Il2CppObject * __this /* static, unused */, RichFunnel_t2453525928 * ____this0, int32_t ___splitIndex1, TriangleMeshNode_t1626248749 * ___prefix2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.RichFunnel::ilo_GetVertex14(Pathfinding.TriangleMeshNode,System.Int32)
extern "C"  Int3_t1974045594  RichFunnel_ilo_GetVertex14_m2250612288 (Il2CppObject * __this /* static, unused */, TriangleMeshNode_t1626248749 * ____this0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RichFunnel::ilo_op_Equality15(Pathfinding.Int3,Pathfinding.Int3)
extern "C"  bool RichFunnel_ilo_op_Equality15_m1635119348 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___lhs0, Int3_t1974045594  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RichFunnel::ilo_IsColinear16(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool RichFunnel_ilo_IsColinear16_m1508490099 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, Vector3_t4282066566  ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RichFunnel::ilo_Left17(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool RichFunnel_ilo_Left17_m3152902950 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, Vector3_t4282066566  ___p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RichFunnel::ilo_TriangleArea218(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float RichFunnel_ilo_TriangleArea218_m1751904893 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, Vector3_t4282066566  ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
