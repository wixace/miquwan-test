﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,TabData>
struct Dictionary_2_t930971649;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JsDBMgr
struct  JsDBMgr_t312990961  : public Il2CppObject
{
public:

public:
};

struct JsDBMgr_t312990961_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,TabData> JsDBMgr::allInfoDic
	Dictionary_2_t930971649 * ___allInfoDic_0;
	// System.Single JsDBMgr::<progress>k__BackingField
	float ___U3CprogressU3Ek__BackingField_1;
	// System.Boolean JsDBMgr::<inited>k__BackingField
	bool ___U3CinitedU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_allInfoDic_0() { return static_cast<int32_t>(offsetof(JsDBMgr_t312990961_StaticFields, ___allInfoDic_0)); }
	inline Dictionary_2_t930971649 * get_allInfoDic_0() const { return ___allInfoDic_0; }
	inline Dictionary_2_t930971649 ** get_address_of_allInfoDic_0() { return &___allInfoDic_0; }
	inline void set_allInfoDic_0(Dictionary_2_t930971649 * value)
	{
		___allInfoDic_0 = value;
		Il2CppCodeGenWriteBarrier(&___allInfoDic_0, value);
	}

	inline static int32_t get_offset_of_U3CprogressU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsDBMgr_t312990961_StaticFields, ___U3CprogressU3Ek__BackingField_1)); }
	inline float get_U3CprogressU3Ek__BackingField_1() const { return ___U3CprogressU3Ek__BackingField_1; }
	inline float* get_address_of_U3CprogressU3Ek__BackingField_1() { return &___U3CprogressU3Ek__BackingField_1; }
	inline void set_U3CprogressU3Ek__BackingField_1(float value)
	{
		___U3CprogressU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CinitedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(JsDBMgr_t312990961_StaticFields, ___U3CinitedU3Ek__BackingField_2)); }
	inline bool get_U3CinitedU3Ek__BackingField_2() const { return ___U3CinitedU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CinitedU3Ek__BackingField_2() { return &___U3CinitedU3Ek__BackingField_2; }
	inline void set_U3CinitedU3Ek__BackingField_2(bool value)
	{
		___U3CinitedU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
