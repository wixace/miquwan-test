﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iTweenGenerated
struct iTweenGenerated_t504945709;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3792884695;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Hashtable
struct Hashtable_t1407064410;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.AudioClip
struct AudioClip_t794140988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_Collections_Hashtable1407064410.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_AudioClip794140988.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void iTweenGenerated::.ctor()
extern "C"  void iTweenGenerated__ctor_m489639390 (iTweenGenerated_t504945709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_iTween1(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_iTween1_m1545099860 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::iTween_tweens(JSVCall)
extern "C"  void iTweenGenerated_iTween_tweens_m2477739238 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::iTween_id(JSVCall)
extern "C"  void iTweenGenerated_iTween_id_m3164447891 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::iTween_type(JSVCall)
extern "C"  void iTweenGenerated_iTween_type_m72860500 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::iTween_method(JSVCall)
extern "C"  void iTweenGenerated_iTween_method_m2456853389 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::iTween_easeType(JSVCall)
extern "C"  void iTweenGenerated_iTween_easeType_m1053474310 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::iTween_time(JSVCall)
extern "C"  void iTweenGenerated_iTween_time_m3398391073 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::iTween_delay(JSVCall)
extern "C"  void iTweenGenerated_iTween_delay_m3346484379 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::iTween_loopType(JSVCall)
extern "C"  void iTweenGenerated_iTween_loopType_m377746192 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::iTween_isRunning(JSVCall)
extern "C"  void iTweenGenerated_iTween_isRunning_m3756152105 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::iTween_isPaused(JSVCall)
extern "C"  void iTweenGenerated_iTween_isPaused_m3285842262 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::iTween__name(JSVCall)
extern "C"  void iTweenGenerated_iTween__name_m1360463508 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_AudioFrom__GameObject__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_AudioFrom__GameObject__Single__Single__Single_m3625213710 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_AudioFrom__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_AudioFrom__GameObject__Hashtable_m1282314572 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_AudioTo__GameObject__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_AudioTo__GameObject__Single__Single__Single_m3975868703 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_AudioTo__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_AudioTo__GameObject__Hashtable_m3041851291 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_AudioUpdate__GameObject__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_AudioUpdate__GameObject__Single__Single__Single_m1263909101 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_AudioUpdate__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_AudioUpdate__GameObject__Hashtable_m3343227149 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_CameraFadeAdd__Texture2D__Int32(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_CameraFadeAdd__Texture2D__Int32_m329338534 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_CameraFadeAdd__Texture2D(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_CameraFadeAdd__Texture2D_m3731705194 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_CameraFadeAdd(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_CameraFadeAdd_m494850341 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_CameraFadeDepth__Int32(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_CameraFadeDepth__Int32_m1020265577 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_CameraFadeDestroy(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_CameraFadeDestroy_m1054322238 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_CameraFadeFrom__Single__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_CameraFadeFrom__Single__Single_m1286579192 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_CameraFadeFrom__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_CameraFadeFrom__Hashtable_m581962714 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_CameraFadeSwap__Texture2D(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_CameraFadeSwap__Texture2D_m3514822782 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_CameraFadeTo__Single__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_CameraFadeTo__Single__Single_m680566089 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_CameraFadeTo__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_CameraFadeTo__Hashtable_m375047145 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_CameraTexture__Color(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_CameraTexture__Color_m474122442 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_ColorFrom__GameObject__Color__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_ColorFrom__GameObject__Color__Single_m2424963402 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_ColorFrom__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_ColorFrom__GameObject__Hashtable_m172612959 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_ColorTo__GameObject__Color__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_ColorTo__GameObject__Color__Single_m1459106009 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_ColorTo__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_ColorTo__GameObject__Hashtable_m3013880942 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_ColorUpdate__GameObject__Color__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_ColorUpdate__GameObject__Color__Single_m2701977547 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_ColorUpdate__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_ColorUpdate__GameObject__Hashtable_m2071866464 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Count__GameObject__String(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Count__GameObject__String_m3371996982 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Count__String(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Count__String_m1053653413 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Count__GameObject(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Count__GameObject_m2601749989 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Count(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Count_m1955155796 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_DrawLine__Vector3_Array__Color(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_DrawLine__Vector3_Array__Color_m1395002190 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_DrawLine__Transform_Array__Color(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_DrawLine__Transform_Array__Color_m652232882 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_DrawLine__Vector3_Array(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_DrawLine__Vector3_Array_m1192759 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_DrawLine__Transform_Array(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_DrawLine__Transform_Array_m2879290963 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_DrawLineGizmos__Transform_Array__Color(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_DrawLineGizmos__Transform_Array__Color_m2947223115 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_DrawLineGizmos__Vector3_Array__Color(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_DrawLineGizmos__Vector3_Array__Color_m3337052967 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_DrawLineGizmos__Transform_Array(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_DrawLineGizmos__Transform_Array_m2612650714 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_DrawLineGizmos__Vector3_Array(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_DrawLineGizmos__Vector3_Array_m117116286 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_DrawLineHandles__Vector3_Array__Color(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_DrawLineHandles__Vector3_Array__Color_m1273558705 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_DrawLineHandles__Transform_Array__Color(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_DrawLineHandles__Transform_Array__Color_m4204128085 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_DrawLineHandles__Transform_Array(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_DrawLineHandles__Transform_Array_m2910372112 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_DrawLineHandles__Vector3_Array(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_DrawLineHandles__Vector3_Array_m2955411764 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_DrawPath__Vector3_Array__Color(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_DrawPath__Vector3_Array__Color_m3073309183 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_DrawPath__Transform_Array__Color(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_DrawPath__Transform_Array__Color_m2892517155 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_DrawPath__Transform_Array(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_DrawPath__Transform_Array_m1452631298 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_DrawPath__Vector3_Array(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_DrawPath__Vector3_Array_m1403058598 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_DrawPathGizmos__Transform_Array__Color(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_DrawPathGizmos__Transform_Array__Color_m2922822652 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_DrawPathGizmos__Vector3_Array__Color(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_DrawPathGizmos__Vector3_Array__Color_m2738145560 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_DrawPathGizmos__Vector3_Array(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_DrawPathGizmos__Vector3_Array_m4189127853 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_DrawPathGizmos__Transform_Array(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_DrawPathGizmos__Transform_Array_m3100559945 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_DrawPathHandles__Vector3_Array__Color(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_DrawPathHandles__Vector3_Array__Color_m4182265568 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_DrawPathHandles__Transform_Array__Color(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_DrawPathHandles__Transform_Array__Color_m3447713732 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_DrawPathHandles__Transform_Array(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_DrawPathHandles__Transform_Array_m855689089 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_DrawPathHandles__Vector3_Array(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_DrawPathHandles__Vector3_Array_m338751461 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_FadeFrom__GameObject__Single__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_FadeFrom__GameObject__Single__Single_m1221256324 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_FadeFrom__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_FadeFrom__GameObject__Hashtable_m341418958 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_FadeTo__GameObject__Single__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_FadeTo__GameObject__Single__Single_m2913054485 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_FadeTo__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_FadeTo__GameObject__Hashtable_m41992861 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_FadeUpdate__GameObject__Single__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_FadeUpdate__GameObject__Single__Single_m1321653731 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_FadeUpdate__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_FadeUpdate__GameObject__Hashtable_m1085674255 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_FloatUpdate__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_FloatUpdate__Single__Single__Single_m2503415010 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Hash__Object_Array(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Hash__Object_Array_m1284811172 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Init__GameObject(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Init__GameObject_m3829910078 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_LookFrom__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_LookFrom__GameObject__Vector3__Single_m80239779 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_LookFrom__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_LookFrom__GameObject__Hashtable_m1988520459 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_LookTo__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_LookTo__GameObject__Vector3__Single_m3533954930 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_LookTo__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_LookTo__GameObject__Hashtable_m615773210 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_LookUpdate__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_LookUpdate__GameObject__Vector3__Single_m3099814884 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_LookUpdate__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_LookUpdate__GameObject__Hashtable_m3402251788 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_MoveAdd__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_MoveAdd__GameObject__Vector3__Single_m4064645556 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_MoveAdd__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_MoveAdd__GameObject__Hashtable_m1125187548 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_MoveBy__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_MoveBy__GameObject__Vector3__Single_m1496764964 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_MoveBy__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_MoveBy__GameObject__Hashtable_m106386508 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_MoveFrom__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_MoveFrom__GameObject__Vector3__Single_m356808497 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_MoveFrom__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_MoveFrom__GameObject__Hashtable_m848627481 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_MoveTo__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_MoveTo__GameObject__Vector3__Single_m2095138176 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_MoveTo__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_MoveTo__GameObject__Hashtable_m172129448 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_MoveUpdate__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_MoveUpdate__GameObject__Vector3__Single_m2594380530 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_MoveUpdate__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_MoveUpdate__GameObject__Hashtable_m3181760410 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_PathLength__Transform_Array(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_PathLength__Transform_Array_m3901326816 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_PathLength__Vector3_Array(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_PathLength__Vector3_Array_m1128512004 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Pause__GameObject__String__Boolean(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Pause__GameObject__String__Boolean_m2310240877 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Pause__GameObject__String(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Pause__GameObject__String_m4218379101 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Pause__GameObject__Boolean(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Pause__GameObject__Boolean_m2106679710 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Pause__GameObject(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Pause__GameObject_m3156502796 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Pause__String(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Pause__String_m2405433420 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Pause(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Pause_m3605294331 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_PointOnPath__Transform_Array__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_PointOnPath__Transform_Array__Single_m1376997559 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_PointOnPath__Vector3_Array__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_PointOnPath__Vector3_Array__Single_m2999498907 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_PunchPosition__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_PunchPosition__GameObject__Vector3__Single_m2871290861 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_PunchPosition__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_PunchPosition__GameObject__Hashtable_m3653908693 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_PunchRotation__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_PunchRotation__GameObject__Vector3__Single_m3899750264 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_PunchRotation__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_PunchRotation__GameObject__Hashtable_m2860353696 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_PunchScale__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_PunchScale__GameObject__Vector3__Single_m1648202480 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_PunchScale__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_PunchScale__GameObject__Hashtable_m142712344 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_PutOnPath__GameObject__Transform_Array__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_PutOnPath__GameObject__Transform_Array__Single_m1186182183 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_PutOnPath__Transform__Transform_Array__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_PutOnPath__Transform__Transform_Array__Single_m1625513690 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_PutOnPath__Transform__Vector3_Array__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_PutOnPath__Transform__Vector3_Array__Single_m2709255038 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_PutOnPath__GameObject__Vector3_Array__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_PutOnPath__GameObject__Vector3_Array__Single_m1846229003 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_RectUpdate__Rect__Rect__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_RectUpdate__Rect__Rect__Single_m1273185722 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Resume__GameObject__String__Boolean(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Resume__GameObject__String__Boolean_m1243697214 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Resume__GameObject__String(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Resume__GameObject__String_m4249847404 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Resume__GameObject__Boolean(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Resume__GameObject__Boolean_m3082197103 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Resume__String(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Resume__String_m2186611163 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Resume__GameObject(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Resume__GameObject_m3008107291 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Resume(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Resume_m3341251978 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_RotateAdd__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_RotateAdd__GameObject__Vector3__Single_m1278923966 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_RotateAdd__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_RotateAdd__GameObject__Hashtable_m2194657894 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_RotateBy__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_RotateBy__GameObject__Vector3__Single_m2238186970 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_RotateBy__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_RotateBy__GameObject__Hashtable_m2911832194 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_RotateFrom__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_RotateFrom__GameObject__Vector3__Single_m4193752423 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_RotateFrom__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_RotateFrom__GameObject__Hashtable_m3937437135 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_RotateTo__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_RotateTo__GameObject__Vector3__Single_m2836560182 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_RotateTo__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_RotateTo__GameObject__Hashtable_m2977575134 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_RotateUpdate__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_RotateUpdate__GameObject__Vector3__Single_m520586152 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_RotateUpdate__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_RotateUpdate__GameObject__Hashtable_m3705436368 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_ScaleAdd__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_ScaleAdd__GameObject__Vector3__Single_m630308309 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_ScaleAdd__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_ScaleAdd__GameObject__Hashtable_m3178567869 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_ScaleBy__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_ScaleBy__GameObject__Vector3__Single_m831790563 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_ScaleBy__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_ScaleBy__GameObject__Hashtable_m311171915 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_ScaleFrom__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_ScaleFrom__GameObject__Vector3__Single_m1266536240 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_ScaleFrom__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_ScaleFrom__GameObject__Hashtable_m78907992 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_ScaleTo__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_ScaleTo__GameObject__Vector3__Single_m1430163775 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_ScaleTo__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_ScaleTo__GameObject__Hashtable_m376914855 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_ScaleUpdate__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_ScaleUpdate__GameObject__Vector3__Single_m669413169 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_ScaleUpdate__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_ScaleUpdate__GameObject__Hashtable_m2215706393 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_ShakePosition__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_ShakePosition__GameObject__Vector3__Single_m41137941 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_ShakePosition__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_ShakePosition__GameObject__Hashtable_m2224751101 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_ShakeRotation__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_ShakeRotation__GameObject__Vector3__Single_m1069597344 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_ShakeRotation__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_ShakeRotation__GameObject__Hashtable_m1431196104 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_ShakeScale__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_ShakeScale__GameObject__Vector3__Single_m1625184456 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_ShakeScale__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_ShakeScale__GameObject__Hashtable_m2839651824 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Stab__GameObject__AudioClip__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Stab__GameObject__AudioClip__Single_m1303537888 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Stab__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Stab__GameObject__Hashtable_m1647658322 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Stop__GameObject__String__Boolean(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Stop__GameObject__String__Boolean_m3372904265 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Stop__GameObject__Boolean(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Stop__GameObject__Boolean_m3668505210 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Stop__GameObject__String(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Stop__GameObject__String_m250887937 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Stop__GameObject(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Stop__GameObject_m1166618800 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Stop__String(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Stop__String_m3447863792 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Stop(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Stop_m2426178207 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_StopByName__GameObject__String__Boolean(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_StopByName__GameObject__String__Boolean_m2134650599 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_StopByName__GameObject__String(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_StopByName__GameObject__String_m3649361315 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_StopByName__String(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_StopByName__String_m3696636818 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_ValueTo__GameObject__Hashtable(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_ValueTo__GameObject__Hashtable_m1997235360 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Vector2Update__Vector2__Vector2__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Vector2Update__Vector2__Vector2__Single_m2714958501 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::iTween_Vector3Update__Vector3__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool iTweenGenerated_iTween_Vector3Update__Vector3__Vector3__Single_m925763846 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::__Register()
extern "C"  void iTweenGenerated___Register_m231614889 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] iTweenGenerated::<iTween_DrawLine__Vector3_Array__Color>m__2FD()
extern "C"  Vector3U5BU5D_t215400611* iTweenGenerated_U3CiTween_DrawLine__Vector3_Array__ColorU3Em__2FD_m464519491 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform[] iTweenGenerated::<iTween_DrawLine__Transform_Array__Color>m__2FE()
extern "C"  TransformU5BU5D_t3792884695* iTweenGenerated_U3CiTween_DrawLine__Transform_Array__ColorU3Em__2FE_m694576772 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] iTweenGenerated::<iTween_DrawLine__Vector3_Array>m__2FF()
extern "C"  Vector3U5BU5D_t215400611* iTweenGenerated_U3CiTween_DrawLine__Vector3_ArrayU3Em__2FF_m1658398784 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform[] iTweenGenerated::<iTween_DrawLine__Transform_Array>m__300()
extern "C"  TransformU5BU5D_t3792884695* iTweenGenerated_U3CiTween_DrawLine__Transform_ArrayU3Em__300_m3689495425 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform[] iTweenGenerated::<iTween_DrawLineGizmos__Transform_Array__Color>m__301()
extern "C"  TransformU5BU5D_t3792884695* iTweenGenerated_U3CiTween_DrawLineGizmos__Transform_Array__ColorU3Em__301_m605409038 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] iTweenGenerated::<iTween_DrawLineGizmos__Vector3_Array__Color>m__302()
extern "C"  Vector3U5BU5D_t215400611* iTweenGenerated_U3CiTween_DrawLineGizmos__Vector3_Array__ColorU3Em__302_m1181340175 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform[] iTweenGenerated::<iTween_DrawLineGizmos__Transform_Array>m__303()
extern "C"  TransformU5BU5D_t3792884695* iTweenGenerated_U3CiTween_DrawLineGizmos__Transform_ArrayU3Em__303_m1798298717 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] iTweenGenerated::<iTween_DrawLineGizmos__Vector3_Array>m__304()
extern "C"  Vector3U5BU5D_t215400611* iTweenGenerated_U3CiTween_DrawLineGizmos__Vector3_ArrayU3Em__304_m1999669470 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] iTweenGenerated::<iTween_DrawLineHandles__Vector3_Array__Color>m__305()
extern "C"  Vector3U5BU5D_t215400611* iTweenGenerated_U3CiTween_DrawLineHandles__Vector3_Array__ColorU3Em__305_m3505334924 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform[] iTweenGenerated::<iTween_DrawLineHandles__Transform_Array__Color>m__306()
extern "C"  TransformU5BU5D_t3792884695* iTweenGenerated_U3CiTween_DrawLineHandles__Transform_Array__ColorU3Em__306_m7854405 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform[] iTweenGenerated::<iTween_DrawLineHandles__Transform_Array>m__307()
extern "C"  TransformU5BU5D_t3792884695* iTweenGenerated_U3CiTween_DrawLineHandles__Transform_ArrayU3Em__307_m333418543 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] iTweenGenerated::<iTween_DrawLineHandles__Vector3_Array>m__308()
extern "C"  Vector3U5BU5D_t215400611* iTweenGenerated_U3CiTween_DrawLineHandles__Vector3_ArrayU3Em__308_m3380238312 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] iTweenGenerated::<iTween_DrawPath__Vector3_Array__Color>m__309()
extern "C"  Vector3U5BU5D_t215400611* iTweenGenerated_U3CiTween_DrawPath__Vector3_Array__ColorU3Em__309_m1505228030 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform[] iTweenGenerated::<iTween_DrawPath__Transform_Array__Color>m__30A()
extern "C"  TransformU5BU5D_t3792884695* iTweenGenerated_U3CiTween_DrawPath__Transform_Array__ColorU3Em__30A_m4135830726 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform[] iTweenGenerated::<iTween_DrawPath__Transform_Array>m__30B()
extern "C"  TransformU5BU5D_t3792884695* iTweenGenerated_U3CiTween_DrawPath__Transform_ArrayU3Em__30B_m3841353732 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] iTweenGenerated::<iTween_DrawPath__Vector3_Array>m__30C()
extern "C"  Vector3U5BU5D_t215400611* iTweenGenerated_U3CiTween_DrawPath__Vector3_ArrayU3Em__30C_m4081165701 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform[] iTweenGenerated::<iTween_DrawPathGizmos__Transform_Array__Color>m__30D()
extern "C"  TransformU5BU5D_t3792884695* iTweenGenerated_U3CiTween_DrawPathGizmos__Transform_Array__ColorU3Em__30D_m3473734928 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] iTweenGenerated::<iTween_DrawPathGizmos__Vector3_Array__Color>m__30E()
extern "C"  Vector3U5BU5D_t215400611* iTweenGenerated_U3CiTween_DrawPathGizmos__Vector3_Array__ColorU3Em__30E_m2583224273 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] iTweenGenerated::<iTween_DrawPathGizmos__Vector3_Array>m__30F()
extern "C"  Vector3U5BU5D_t215400611* iTweenGenerated_U3CiTween_DrawPathGizmos__Vector3_ArrayU3Em__30F_m3972912353 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform[] iTweenGenerated::<iTween_DrawPathGizmos__Transform_Array>m__310()
extern "C"  TransformU5BU5D_t3792884695* iTweenGenerated_U3CiTween_DrawPathGizmos__Transform_ArrayU3Em__310_m3987535274 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] iTweenGenerated::<iTween_DrawPathHandles__Vector3_Array__Color>m__311()
extern "C"  Vector3U5BU5D_t215400611* iTweenGenerated_U3CiTween_DrawPathHandles__Vector3_Array__ColorU3Em__311_m4013528920 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform[] iTweenGenerated::<iTween_DrawPathHandles__Transform_Array__Color>m__312()
extern "C"  TransformU5BU5D_t3792884695* iTweenGenerated_U3CiTween_DrawPathHandles__Transform_Array__ColorU3Em__312_m3026070993 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform[] iTweenGenerated::<iTween_DrawPathHandles__Transform_Array>m__313()
extern "C"  TransformU5BU5D_t3792884695* iTweenGenerated_U3CiTween_DrawPathHandles__Transform_ArrayU3Em__313_m3774434169 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] iTweenGenerated::<iTween_DrawPathHandles__Vector3_Array>m__314()
extern "C"  Vector3U5BU5D_t215400611* iTweenGenerated_U3CiTween_DrawPathHandles__Vector3_ArrayU3Em__314_m125747954 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] iTweenGenerated::<iTween_Hash__Object_Array>m__315()
extern "C"  ObjectU5BU5D_t1108656482* iTweenGenerated_U3CiTween_Hash__Object_ArrayU3Em__315_m4160015503 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform[] iTweenGenerated::<iTween_PathLength__Transform_Array>m__316()
extern "C"  TransformU5BU5D_t3792884695* iTweenGenerated_U3CiTween_PathLength__Transform_ArrayU3Em__316_m1799435833 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] iTweenGenerated::<iTween_PathLength__Vector3_Array>m__317()
extern "C"  Vector3U5BU5D_t215400611* iTweenGenerated_U3CiTween_PathLength__Vector3_ArrayU3Em__317_m2773613370 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform[] iTweenGenerated::<iTween_PointOnPath__Transform_Array__Single>m__318()
extern "C"  TransformU5BU5D_t3792884695* iTweenGenerated_U3CiTween_PointOnPath__Transform_Array__SingleU3Em__318_m1453302856 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] iTweenGenerated::<iTween_PointOnPath__Vector3_Array__Single>m__319()
extern "C"  Vector3U5BU5D_t215400611* iTweenGenerated_U3CiTween_PointOnPath__Vector3_Array__SingleU3Em__319_m4257087041 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform[] iTweenGenerated::<iTween_PutOnPath__GameObject__Transform_Array__Single>m__31A()
extern "C"  TransformU5BU5D_t3792884695* iTweenGenerated_U3CiTween_PutOnPath__GameObject__Transform_Array__SingleU3Em__31A_m1442226721 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform[] iTweenGenerated::<iTween_PutOnPath__Transform__Transform_Array__Single>m__31B()
extern "C"  TransformU5BU5D_t3792884695* iTweenGenerated_U3CiTween_PutOnPath__Transform__Transform_Array__SingleU3Em__31B_m559241355 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] iTweenGenerated::<iTween_PutOnPath__Transform__Vector3_Array__Single>m__31C()
extern "C"  Vector3U5BU5D_t215400611* iTweenGenerated_U3CiTween_PutOnPath__Transform__Vector3_Array__SingleU3Em__31C_m270154764 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] iTweenGenerated::<iTween_PutOnPath__GameObject__Vector3_Array__Single>m__31D()
extern "C"  Vector3U5BU5D_t215400611* iTweenGenerated_U3CiTween_PutOnPath__GameObject__Vector3_Array__SingleU3Em__31D_m3894563996 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 iTweenGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t iTweenGenerated_ilo_getObject1_m1684925208 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 iTweenGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t iTweenGenerated_ilo_setObject2_m3871558865 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String iTweenGenerated::ilo_getStringS3(System.Int32)
extern "C"  String_t* iTweenGenerated_ilo_getStringS3_m232920580 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_setStringS4(System.Int32,System.String)
extern "C"  void iTweenGenerated_ilo_setStringS4_m3518904106 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void iTweenGenerated_ilo_setSingle5_m3575586778 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_setBooleanS6(System.Int32,System.Boolean)
extern "C"  void iTweenGenerated_ilo_setBooleanS6_m2933044202 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenGenerated::ilo_getBooleanS7(System.Int32)
extern "C"  bool iTweenGenerated_ilo_getBooleanS7_m117054156 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single iTweenGenerated::ilo_getSingle8(System.Int32)
extern "C"  float iTweenGenerated_ilo_getSingle8_m3800957112 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTweenGenerated::ilo_getObject9(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * iTweenGenerated_ilo_getObject9_m3120014927 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_AudioFrom10(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void iTweenGenerated_ilo_AudioFrom10_m2970338782 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Hashtable_t1407064410 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject iTweenGenerated::ilo_CameraFadeAdd11()
extern "C"  GameObject_t3674682005 * iTweenGenerated_ilo_CameraFadeAdd11_m2392805534 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 iTweenGenerated::ilo_getInt3212(System.Int32)
extern "C"  int32_t iTweenGenerated_ilo_getInt3212_m2026027189 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_CameraFadeDepth13(System.Int32)
extern "C"  void iTweenGenerated_ilo_CameraFadeDepth13_m3074763358 (Il2CppObject * __this /* static, unused */, int32_t ___depth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_CameraFadeDestroy14()
extern "C"  void iTweenGenerated_ilo_CameraFadeDestroy14_m4271749189 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_CameraFadeFrom15(System.Single,System.Single)
extern "C"  void iTweenGenerated_ilo_CameraFadeFrom15_m385747208 (Il2CppObject * __this /* static, unused */, float ___amount0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_CameraFadeSwap16(UnityEngine.Texture2D)
extern "C"  void iTweenGenerated_ilo_CameraFadeSwap16_m372780266 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D iTweenGenerated::ilo_CameraTexture17(UnityEngine.Color)
extern "C"  Texture2D_t3884108195 * iTweenGenerated_ilo_CameraTexture17_m2074735396 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_ColorFrom18(UnityEngine.GameObject,UnityEngine.Color,System.Single)
extern "C"  void iTweenGenerated_ilo_ColorFrom18_m2780771962 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Color_t4194546905  ___color1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_ColorTo19(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void iTweenGenerated_ilo_ColorTo19_m1590060311 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Hashtable_t1407064410 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_ColorUpdate20(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void iTweenGenerated_ilo_ColorUpdate20_m2955827 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Hashtable_t1407064410 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 iTweenGenerated::ilo_Count21(System.String)
extern "C"  int32_t iTweenGenerated_ilo_Count21_m360161181 (Il2CppObject * __this /* static, unused */, String_t* ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 iTweenGenerated::ilo_Count22(UnityEngine.GameObject)
extern "C"  int32_t iTweenGenerated_ilo_Count22_m1817721406 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_setInt3223(System.Int32,System.Int32)
extern "C"  void iTweenGenerated_ilo_setInt3223_m2364797856 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_DrawLine24(UnityEngine.Vector3[])
extern "C"  void iTweenGenerated_ilo_DrawLine24_m391272036 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t215400611* ___line0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_DrawLineGizmos25(UnityEngine.Transform[],UnityEngine.Color)
extern "C"  void iTweenGenerated_ilo_DrawLineGizmos25_m553234318 (Il2CppObject * __this /* static, unused */, TransformU5BU5D_t3792884695* ___line0, Color_t4194546905  ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_DrawLineGizmos26(UnityEngine.Vector3[],UnityEngine.Color)
extern "C"  void iTweenGenerated_ilo_DrawLineGizmos26_m1261631497 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t215400611* ___line0, Color_t4194546905  ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_DrawLineGizmos27(UnityEngine.Transform[])
extern "C"  void iTweenGenerated_ilo_DrawLineGizmos27_m758709996 (Il2CppObject * __this /* static, unused */, TransformU5BU5D_t3792884695* ___line0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_DrawLineHandles28(UnityEngine.Transform[],UnityEngine.Color)
extern "C"  void iTweenGenerated_ilo_DrawLineHandles28_m1502240505 (Il2CppObject * __this /* static, unused */, TransformU5BU5D_t3792884695* ___line0, Color_t4194546905  ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_DrawPathGizmos29(UnityEngine.Vector3[],UnityEngine.Color)
extern "C"  void iTweenGenerated_ilo_DrawPathGizmos29_m2445815637 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t215400611* ___path0, Color_t4194546905  ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_DrawPathGizmos30(UnityEngine.Transform[])
extern "C"  void iTweenGenerated_ilo_DrawPathGizmos30_m649207747 (Il2CppObject * __this /* static, unused */, TransformU5BU5D_t3792884695* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_DrawPathHandles31(UnityEngine.Vector3[],UnityEngine.Color)
extern "C"  void iTweenGenerated_ilo_DrawPathHandles31_m1792258542 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t215400611* ___path0, Color_t4194546905  ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_FadeUpdate32(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void iTweenGenerated_ilo_FadeUpdate32_m2832531721 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Hashtable_t1407064410 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 iTweenGenerated::ilo_getVector3S33(System.Int32)
extern "C"  Vector3_t4282066566  iTweenGenerated_ilo_getVector3S33_m3389347373 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_LookTo34(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void iTweenGenerated_ilo_LookTo34_m1174774884 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Vector3_t4282066566  ___looktarget1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_LookTo35(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void iTweenGenerated_ilo_LookTo35_m2879432241 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Hashtable_t1407064410 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_MoveTo36(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void iTweenGenerated_ilo_MoveTo36_m2431305624 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Vector3_t4282066566  ___position1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_MoveUpdate37(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void iTweenGenerated_ilo_MoveUpdate37_m1426081007 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Hashtable_t1407064410 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_Pause38(UnityEngine.GameObject)
extern "C"  void iTweenGenerated_ilo_Pause38_m3971839612 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_Pause39(System.String)
extern "C"  void iTweenGenerated_ilo_Pause39_m735822173 (Il2CppObject * __this /* static, unused */, String_t* ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 iTweenGenerated::ilo_PointOnPath40(UnityEngine.Transform[],System.Single)
extern "C"  Vector3_t4282066566  iTweenGenerated_ilo_PointOnPath40_m3847787629 (Il2CppObject * __this /* static, unused */, TransformU5BU5D_t3792884695* ___path0, float ___percent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_setVector3S41(System.Int32,UnityEngine.Vector3)
extern "C"  void iTweenGenerated_ilo_setVector3S41_m1031573167 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_PunchPosition42(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void iTweenGenerated_ilo_PunchPosition42_m2430037134 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Vector3_t4282066566  ___amount1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_PunchRotation43(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void iTweenGenerated_ilo_PunchRotation43_m3600611876 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Vector3_t4282066566  ___amount1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_PunchRotation44(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void iTweenGenerated_ilo_PunchRotation44_m3216685169 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Hashtable_t1407064410 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_PunchScale45(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void iTweenGenerated_ilo_PunchScale45_m559493318 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Vector3_t4282066566  ___amount1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_PutOnPath46(UnityEngine.Transform,UnityEngine.Transform[],System.Single)
extern "C"  void iTweenGenerated_ilo_PutOnPath46_m3101369273 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ___target0, TransformU5BU5D_t3792884695* ___path1, float ___percent2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_PutOnPath47(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern "C"  void iTweenGenerated_ilo_PutOnPath47_m3810727053 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Vector3U5BU5D_t215400611* ___path1, float ___percent2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect iTweenGenerated::ilo_RectUpdate48(UnityEngine.Rect,UnityEngine.Rect,System.Single)
extern "C"  Rect_t4241904616  iTweenGenerated_ilo_RectUpdate48_m2551016803 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___currentValue0, Rect_t4241904616  ___targetValue1, float ___speed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_Resume49(UnityEngine.GameObject,System.Boolean)
extern "C"  void iTweenGenerated_ilo_Resume49_m3580795866 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, bool ___includechildren1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_RotateFrom50(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void iTweenGenerated_ilo_RotateFrom50_m1138798861 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Hashtable_t1407064410 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_RotateTo51(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void iTweenGenerated_ilo_RotateTo51_m93413627 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Hashtable_t1407064410 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_ScaleAdd52(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void iTweenGenerated_ilo_ScaleAdd52_m3751948189 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Vector3_t4282066566  ___amount1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_ScaleUpdate53(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void iTweenGenerated_ilo_ScaleUpdate53_m4168488268 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Hashtable_t1407064410 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_ShakePosition54(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void iTweenGenerated_ilo_ShakePosition54_m2994616015 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Hashtable_t1407064410 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_ShakeScale55(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void iTweenGenerated_ilo_ShakeScale55_m1052854605 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Vector3_t4282066566  ___amount1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_ShakeScale56(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void iTweenGenerated_ilo_ShakeScale56_m631886760 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Hashtable_t1407064410 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_Stab57(UnityEngine.GameObject,UnityEngine.AudioClip,System.Single)
extern "C"  void iTweenGenerated_ilo_Stab57_m2035638455 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, AudioClip_t794140988 * ___audioclip1, float ___delay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_Stop58(UnityEngine.GameObject)
extern "C"  void iTweenGenerated_ilo_Stop58_m2911552758 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenGenerated::ilo_setVector2S59(System.Int32,UnityEngine.Vector2)
extern "C"  void iTweenGenerated_ilo_setVector2S59_m2268014680 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 iTweenGenerated::ilo_Vector3Update60(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t4282066566  iTweenGenerated_ilo_Vector3Update60_m3499700923 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___currentValue0, Vector3_t4282066566  ___targetValue1, float ___speed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 iTweenGenerated::ilo_getArrayLength61(System.Int32)
extern "C"  int32_t iTweenGenerated_ilo_getArrayLength61_m2663722720 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 iTweenGenerated::ilo_getElement62(System.Int32,System.Int32)
extern "C"  int32_t iTweenGenerated_ilo_getElement62_m2672735257 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTweenGenerated::ilo_getWhatever63(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * iTweenGenerated_ilo_getWhatever63_m2708545946 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
