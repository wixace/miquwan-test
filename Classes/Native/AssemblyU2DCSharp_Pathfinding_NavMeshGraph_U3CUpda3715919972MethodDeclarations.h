﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.NavMeshGraph/<UpdateArea>c__AnonStorey118
struct U3CUpdateAreaU3Ec__AnonStorey118_t3715919972;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.NavMeshGraph/<UpdateArea>c__AnonStorey118::.ctor()
extern "C"  void U3CUpdateAreaU3Ec__AnonStorey118__ctor_m3713735047 (U3CUpdateAreaU3Ec__AnonStorey118_t3715919972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavMeshGraph/<UpdateArea>c__AnonStorey118::<>m__34D(Pathfinding.GraphNode)
extern "C"  bool U3CUpdateAreaU3Ec__AnonStorey118_U3CU3Em__34D_m3398332203 (U3CUpdateAreaU3Ec__AnonStorey118_t3715919972 * __this, GraphNode_t23612370 * ____node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
