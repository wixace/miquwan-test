﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// AmbientObscurance
struct AmbientObscurance_t2181203751;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;
// UnityEngine.Camera
struct Camera_t2727095145;
// System.Object
struct Il2CppObject;
// AntialiasingAsPostEffect
struct AntialiasingAsPostEffect_t2121704279;
// UnityEngine.Material
struct Material_t3870600107;
// Bloom
struct Bloom_t64280035;
// BloomAndLensFlares
struct BloomAndLensFlares_t2492782871;
// Blur
struct Blur_t2073735;
// CameraMotionBlur
struct CameraMotionBlur_t2114294370;
// ColorCorrectionCurves
struct ColorCorrectionCurves_t3453175749;
// ColorCorrectionLut
struct ColorCorrectionLut_t1443265866;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// System.String
struct String_t;
// ContrastEnhance
struct ContrastEnhance_t4202625324;
// Crease
struct Crease_t2026540285;
// DepthOfField34
struct DepthOfField34_t2156099489;
// DepthOfFieldScatter
struct DepthOfFieldScatter_t1867756894;
// DragRigidbody
struct DragRigidbody_t2531437401;
// UnityEngine.Rigidbody
struct Rigidbody_t3346577219;
// UnityEngine.SpringJoint
struct SpringJoint_t558455091;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// DragRigidbody/$DragObject$58
struct U24DragObjectU2458_t1090936582;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// DragRigidbody/$DragObject$58/$
struct U24_t418036571;
// EdgeDetectEffectNormals
struct EdgeDetectEffectNormals_t728271515;
// FastBloom
struct FastBloom_t1950086887;
// Fisheye
struct Fisheye_t816213177;
// GlobalFog
struct GlobalFog_t2391894523;
// MouseOrbit
struct MouseOrbit_t2986430149;
// NoiseAndGrain
struct NoiseAndGrain_t429516286;
// PostEffectsBase
struct PostEffectsBase_t1820837395;
// UnityEngine.Shader
struct Shader_t3191267369;
// PostEffectsHelper
struct PostEffectsHelper_t1948321392;
// Quads
struct Quads_t78387180;
// UnityEngine.Mesh[]
struct MeshU5BU5D_t1759126828;
// UnityEngine.Mesh
struct Mesh_t4241756145;
// ScreenOverlay
struct ScreenOverlay_t1089288740;
// SmoothFollow
struct SmoothFollow_t651130655;
// SmoothLookAt
struct SmoothLookAt_t822992544;
// SunShafts
struct SunShafts_t3666895493;
// TiltShiftHdr
struct TiltShiftHdr_t3131000081;
// Tonemapping
struct Tonemapping_t2852991740;
// Triangles
struct Triangles_t1189959499;
// Vignetting
struct Vignetting_t439778199;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_U3CModuleU3E86524790.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_AAMode1923327459.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_AAMode1923327459MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_AmbientObscuran2181203751.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_AmbientObscuran2181203751MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_PostEffectsBase1820837395MethodDeclarations.h"
#include "mscorlib_System_Single4291918972.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_System_Boolean476798718.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_PostEffectsBase1820837395.h"
#include "UnityEngine_UnityEngine_Shader3191267369.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"
#include "UnityEngine_UnityEngine_Graphics3672240399MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2727095145MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen3187157168MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material3870600107MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector44282066567MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_AntialiasingAsP2121704279.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_AntialiasingAsP2121704279MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Shader3191267369MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2526458961MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Bloom64280035.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Bloom64280035MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Bloom_BloomScree506375889.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Bloom_HDRBloomMo627529916.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Bloom_BloomQuali903541320.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Bloom_LensFlare2288999405.h"
#include "UnityEngine_UnityEngine_GL2267613321MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat2841883826.h"
#include "UnityEngine_UnityEngine_Mathf4203372500.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Bloom_BloomQuali903541320MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Bloom_BloomScree506375889MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Bloom_HDRBloomMo627529916MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Bloom_LensFlare2288999405MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Bloom_TweakMode3998816355.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Bloom_TweakMode3998816355MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_BloomAndLensFla2492782871.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_BloomAndLensFla2492782871MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_BloomScreenBlen3633501733.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_HDRBloomMode3057783824.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_LensflareStyle3465124418.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_BloomScreenBlen3633501733MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Blur2073735.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Blur2073735MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Blur_BlurType2982670569.h"
#include "UnityEngine_UnityEngine_FilterMode1625068031.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Blur_BlurType2982670569MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_BokehDestinatio3447600781.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_BokehDestinatio3447600781MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_CameraMotionBlu2114294370.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_CameraMotionBlu2114294370MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_CameraMotionBlu1204810562.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_DepthTextureMode658977311.h"
#include "UnityEngine_UnityEngine_SystemInfo3820892225MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LayerMask3236759763MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time4241968337MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextureWrapMode1899634046.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_LayerMask3236759763.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices3947355960MethodDeclarations.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour200106419MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "UnityEngine_UnityEngine_HideFlags1436803931.h"
#include "UnityEngine_UnityEngine_CameraClearFlags2093155523.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_CameraMotionBlu1204810562MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_ColorCorrection3453175749.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_ColorCorrection3453175749MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_ColorCorrection1791592516.h"
#include "UnityEngine_UnityEngine_TextureFormat4189619560.h"
#include "UnityEngine_UnityEngine_AnimationCurve3667593487MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationCurve3667593487.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_ColorCorrection1443265866.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_ColorCorrection1443265866MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture3D3884108226.h"
#include "UnityEngine_UnityEngine_Texture3D3884108226MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "mscorlib_System_Array1146569071MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_QualitySettings719345784MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ColorSpace161844263.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_ColorCorrection1791592516MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_ContrastEnhance4202625324.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_ContrastEnhance4202625324MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Crease2026540285.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Crease2026540285MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DepthOfField342156099489.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DepthOfField342156099489MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Dof34QualitySet3100877869.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DofResolution459734055.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DofBlurriness1555378096.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Quads78387180MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DepthOfFieldSca1867756894.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DepthOfFieldSca1867756894MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DepthOfFieldSca3721650674.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DepthOfFieldSca3650130893.h"
#include "UnityEngine_UnityEngine_ComputeBuffer37359565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ComputeBuffer37359565.h"
#include "UnityEngine_UnityEngine_ComputeBufferType2569032487.h"
#include "UnityEngine_UnityEngine_MeshTopology2348044928.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DepthOfFieldSca3650130893MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DepthOfFieldSca3721650674MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Dof34QualitySet3100877869MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DofBlurriness1555378096MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DofResolution459734055MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DragRigidbody2531437401.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DragRigidbody2531437401MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input4200062272MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics3358180733MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Joint4201008640MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpringJoint558455091MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"
#include "UnityEngine_UnityEngine_SpringJoint558455091.h"
#include "UnityEngine_UnityEngine_Coroutine3621161934.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DragRigidbody_U1090936582MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DragRigidbody_U1090936582.h"
#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen2762607199MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DragRigidbody_U2418036571MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DragRigidbody_U2418036571.h"
#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen67576739MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Ray3134616544MethodDeclarations.h"
#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen67576739.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_EdgeDetectEffect728271515.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_EdgeDetectEffect728271515MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_EdgeDetectMode3850410147.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_EdgeDetectMode3850410147MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_FastBloom1950086887.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_FastBloom1950086887MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_FastBloom_Resol1015720180.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_FastBloom_BlurT1056066185.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_FastBloom_BlurT1056066185MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_FastBloom_Resol1015720180MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Fisheye816213177.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Fisheye816213177MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_GlobalFog2391894523.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_GlobalFog2391894523MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_GlobalFog_FogMod716619597.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_GlobalFog_FogMod716619597MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_HDRBloomMode3057783824MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_LensflareStyle3465124418MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_MouseOrbit2986430149.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_MouseOrbit2986430149MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_NoiseAndGrain429516286.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_NoiseAndGrain429516286MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random3156561159MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_PostEffectsHelp1948321392.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_PostEffectsHelp1948321392MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Quads78387180.h"
#include "UnityEngine_UnityEngine_Mesh4241756145MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_ScreenOverlay1089288740.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_ScreenOverlay1089288740MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_ScreenOverlay_O3200601039.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_ScreenOverlay_O3200601039MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_ShaftsScreenBle1836688367.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_ShaftsScreenBle1836688367MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_SmoothFollow651130655.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_SmoothFollow651130655MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_SmoothLookAt822992544.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_SmoothLookAt822992544MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_SunShafts3666895493.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_SunShafts3666895493MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_SunShaftsResolu3846313233.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_SunShaftsResolu3846313233MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_TiltShiftHdr3131000081.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_TiltShiftHdr3131000081MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_TiltShiftHdr_Ti1687708266.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_TiltShiftHdr_Ti1054150360.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_TiltShiftHdr_Ti1687708266MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_TiltShiftHdr_Ti1054150360MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Tonemapping2852991740.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Tonemapping2852991740MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Tonemapping_Ton1910882208.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Tonemapping_Ada3269953599.h"
#include "UnityScript_Lang_UnityScript_Lang_Extensions1192295802MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Keyframe4079056114MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Keyframe4079056114.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Tonemapping_Ada3269953599MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Tonemapping_Ton1910882208MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Triangles1189959499.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Triangles1189959499MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_TweakMode343932888112.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_TweakMode343932888112MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Vignetting439778199.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Vignetting439778199MethodDeclarations.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Vignetting_Aber2450707116.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Vignetting_Aber2450707116MethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, method) ((  Camera_t2727095145 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m3652735468(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Camera>()
#define GameObject_GetComponent_TisCamera_t2727095145_m1498907256(__this, method) ((  Camera_t2727095145 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m337943659_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m337943659(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Rigidbody>()
#define GameObject_AddComponent_TisRigidbody_t3346577219_m107677727(__this, method) ((  Rigidbody_t3346577219 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.SpringJoint>()
#define GameObject_AddComponent_TisSpringJoint_t558455091_m1843786415(__this, method) ((  SpringJoint_t558455091 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t3346577219_m354583034(__this, method) ((  Rigidbody_t3346577219 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AmbientObscurance::.ctor()
extern "C"  void AmbientObscurance__ctor_m3363609307 (AmbientObscurance_t2181203751 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase__ctor_m3869393199(__this, /*hidden argument*/NULL);
		__this->set_intensity_5((0.5f));
		__this->set_radius_6((0.2f));
		__this->set_blurIterations_7(1);
		__this->set_blurFilterDistance_8((1.25f));
		return;
	}
}
// System.Boolean AmbientObscurance::CheckResources()
extern "C"  bool AmbientObscurance_CheckResources_m385541536 (AmbientObscurance_t2181203751 * __this, const MethodInfo* method)
{
	{
		VirtFuncInvoker1< bool, bool >::Invoke(10 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean) */, __this, (bool)1);
		Shader_t3191267369 * L_0 = __this->get_aoShader_11();
		Material_t3870600107 * L_1 = __this->get_aoMaterial_12();
		Material_t3870600107 * L_2 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_0, L_1);
		__this->set_aoMaterial_12(L_2);
		bool L_3 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		if (L_3)
		{
			goto IL_0031;
		}
	}
	{
		VirtActionInvoker0::Invoke(13 /* System.Void PostEffectsBase::ReportAutoDisable() */, __this);
	}

IL_0031:
	{
		bool L_4 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		return L_4;
	}
}
// System.Void AmbientObscurance::OnDisable()
extern "C"  void AmbientObscurance_OnDisable_m3199535554 (AmbientObscurance_t2181203751 * __this, const MethodInfo* method)
{
	{
		Material_t3870600107 * L_0 = __this->get_aoMaterial_12();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t3870600107 * L_2 = __this->get_aoMaterial_12();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		__this->set_aoMaterial_12((Material_t3870600107 *)NULL);
		return;
	}
}
// System.Void AmbientObscurance::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo* Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3450608426;
extern Il2CppCodeGenString* _stringLiteral3654353155;
extern Il2CppCodeGenString* _stringLiteral90274084;
extern Il2CppCodeGenString* _stringLiteral853759441;
extern Il2CppCodeGenString* _stringLiteral696738945;
extern Il2CppCodeGenString* _stringLiteral1014379156;
extern Il2CppCodeGenString* _stringLiteral3964961427;
extern Il2CppCodeGenString* _stringLiteral89789600;
extern Il2CppCodeGenString* _stringLiteral2782235674;
extern const uint32_t AmbientObscurance_OnRenderImage_m2458852515_MetadataUsageId;
extern "C"  void AmbientObscurance_OnRenderImage_m2458852515 (AmbientObscurance_t2181203751 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AmbientObscurance_OnRenderImage_m2458852515_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t1651859333  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t1651859333  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector4_t4282066567  V_2;
	memset(&V_2, 0, sizeof(V_2));
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	RenderTexture_t1963041563 * V_5 = NULL;
	RenderTexture_t1963041563 * V_6 = NULL;
	int32_t V_7 = 0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean AmbientObscurance::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		RenderTexture_t1963041563 * L_1 = ___source0;
		RenderTexture_t1963041563 * L_2 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_0250;
	}

IL_0017:
	{
		Camera_t2727095145 * L_3 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_3);
		Matrix4x4_t1651859333  L_4 = Camera_get_projectionMatrix_m3070982480(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Matrix4x4_t1651859333  L_5 = Matrix4x4_get_inverse_m2596073482((&V_0), /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = Matrix4x4_get_Item_m1280478331((&V_0), 0, /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = Matrix4x4_get_Item_m1280478331((&V_0), 5, /*hidden argument*/NULL);
		float L_10 = Matrix4x4_get_Item_m1280478331((&V_0), 2, /*hidden argument*/NULL);
		float L_11 = Matrix4x4_get_Item_m1280478331((&V_0), 0, /*hidden argument*/NULL);
		float L_12 = Matrix4x4_get_Item_m1280478331((&V_0), 6, /*hidden argument*/NULL);
		float L_13 = Matrix4x4_get_Item_m1280478331((&V_0), 5, /*hidden argument*/NULL);
		Vector4_t4282066567  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector4__ctor_m2441427762(&L_14, ((float)((float)(-2.0f)/(float)((float)((float)(((float)((float)L_6)))*(float)L_7)))), ((float)((float)(-2.0f)/(float)((float)((float)(((float)((float)L_8)))*(float)L_9)))), ((float)((float)((float)((float)(1.0f)-(float)L_10))/(float)L_11)), ((float)((float)((float)((float)(1.0f)+(float)L_12))/(float)L_13)), /*hidden argument*/NULL);
		V_2 = L_14;
		Material_t3870600107 * L_15 = __this->get_aoMaterial_12();
		Vector4_t4282066567  L_16 = V_2;
		NullCheck(L_15);
		Material_SetVector_m3505096203(L_15, _stringLiteral3450608426, L_16, /*hidden argument*/NULL);
		Material_t3870600107 * L_17 = __this->get_aoMaterial_12();
		Matrix4x4_t1651859333  L_18 = V_1;
		NullCheck(L_17);
		Material_SetMatrix_m3693790735(L_17, _stringLiteral3654353155, L_18, /*hidden argument*/NULL);
		Material_t3870600107 * L_19 = __this->get_aoMaterial_12();
		Texture2D_t3884108195 * L_20 = __this->get_rand_10();
		NullCheck(L_19);
		Material_SetTexture_m1833724755(L_19, _stringLiteral90274084, L_20, /*hidden argument*/NULL);
		Material_t3870600107 * L_21 = __this->get_aoMaterial_12();
		float L_22 = __this->get_radius_6();
		NullCheck(L_21);
		Material_SetFloat_m981710063(L_21, _stringLiteral853759441, L_22, /*hidden argument*/NULL);
		Material_t3870600107 * L_23 = __this->get_aoMaterial_12();
		float L_24 = __this->get_radius_6();
		float L_25 = __this->get_radius_6();
		NullCheck(L_23);
		Material_SetFloat_m981710063(L_23, _stringLiteral696738945, ((float)((float)L_24*(float)L_25)), /*hidden argument*/NULL);
		Material_t3870600107 * L_26 = __this->get_aoMaterial_12();
		float L_27 = __this->get_intensity_5();
		NullCheck(L_26);
		Material_SetFloat_m981710063(L_26, _stringLiteral1014379156, L_27, /*hidden argument*/NULL);
		Material_t3870600107 * L_28 = __this->get_aoMaterial_12();
		float L_29 = __this->get_blurFilterDistance_8();
		NullCheck(L_28);
		Material_SetFloat_m981710063(L_28, _stringLiteral3964961427, L_29, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_30 = ___source0;
		NullCheck(L_30);
		int32_t L_31 = RenderTexture_get_width_m1498578543(L_30, /*hidden argument*/NULL);
		V_3 = L_31;
		RenderTexture_t1963041563 * L_32 = ___source0;
		NullCheck(L_32);
		int32_t L_33 = RenderTexture_get_height_m4010076224(L_32, /*hidden argument*/NULL);
		V_4 = L_33;
		int32_t L_34 = V_3;
		int32_t L_35 = __this->get_downsample_9();
		int32_t L_36 = V_4;
		int32_t L_37 = __this->get_downsample_9();
		RenderTexture_t1963041563 * L_38 = RenderTexture_GetTemporary_m469965696(NULL /*static, unused*/, ((int32_t)((int32_t)L_34>>(int32_t)L_35)), ((int32_t)((int32_t)L_36>>(int32_t)L_37)), /*hidden argument*/NULL);
		V_5 = L_38;
		V_6 = (RenderTexture_t1963041563 *)NULL;
		RenderTexture_t1963041563 * L_39 = ___source0;
		RenderTexture_t1963041563 * L_40 = V_5;
		Material_t3870600107 * L_41 = __this->get_aoMaterial_12();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_39, L_40, L_41, 0, /*hidden argument*/NULL);
		int32_t L_42 = __this->get_downsample_9();
		if ((((int32_t)L_42) <= ((int32_t)0)))
		{
			goto IL_018a;
		}
	}
	{
		int32_t L_43 = V_3;
		int32_t L_44 = V_4;
		RenderTexture_t1963041563 * L_45 = RenderTexture_GetTemporary_m469965696(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		V_6 = L_45;
		RenderTexture_t1963041563 * L_46 = V_5;
		RenderTexture_t1963041563 * L_47 = V_6;
		Material_t3870600107 * L_48 = __this->get_aoMaterial_12();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_46, L_47, L_48, 4, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_49 = V_5;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_50 = V_6;
		V_5 = L_50;
	}

IL_018a:
	{
		V_7 = 0;
		goto IL_021c;
	}

IL_0192:
	{
		Material_t3870600107 * L_51 = __this->get_aoMaterial_12();
		Vector2_t4282066565  L_52;
		memset(&L_52, 0, sizeof(L_52));
		Vector2__ctor_m1517109030(&L_52, (1.0f), (((float)((float)0))), /*hidden argument*/NULL);
		Vector4_t4282066567  L_53 = Vector4_op_Implicit_m331673240(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		NullCheck(L_51);
		Material_SetVector_m3505096203(L_51, _stringLiteral89789600, L_53, /*hidden argument*/NULL);
		int32_t L_54 = V_3;
		int32_t L_55 = V_4;
		RenderTexture_t1963041563 * L_56 = RenderTexture_GetTemporary_m469965696(NULL /*static, unused*/, L_54, L_55, /*hidden argument*/NULL);
		V_6 = L_56;
		RenderTexture_t1963041563 * L_57 = V_5;
		RenderTexture_t1963041563 * L_58 = V_6;
		Material_t3870600107 * L_59 = __this->get_aoMaterial_12();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_57, L_58, L_59, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_60 = V_5;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_60, /*hidden argument*/NULL);
		Material_t3870600107 * L_61 = __this->get_aoMaterial_12();
		Vector2_t4282066565  L_62;
		memset(&L_62, 0, sizeof(L_62));
		Vector2__ctor_m1517109030(&L_62, (((float)((float)0))), (1.0f), /*hidden argument*/NULL);
		Vector4_t4282066567  L_63 = Vector4_op_Implicit_m331673240(NULL /*static, unused*/, L_62, /*hidden argument*/NULL);
		NullCheck(L_61);
		Material_SetVector_m3505096203(L_61, _stringLiteral89789600, L_63, /*hidden argument*/NULL);
		int32_t L_64 = V_3;
		int32_t L_65 = V_4;
		RenderTexture_t1963041563 * L_66 = RenderTexture_GetTemporary_m469965696(NULL /*static, unused*/, L_64, L_65, /*hidden argument*/NULL);
		V_5 = L_66;
		RenderTexture_t1963041563 * L_67 = V_6;
		RenderTexture_t1963041563 * L_68 = V_5;
		Material_t3870600107 * L_69 = __this->get_aoMaterial_12();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_67, L_68, L_69, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_70 = V_6;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_70, /*hidden argument*/NULL);
		int32_t L_71 = V_7;
		V_7 = ((int32_t)((int32_t)L_71+(int32_t)1));
	}

IL_021c:
	{
		int32_t L_72 = V_7;
		int32_t L_73 = __this->get_blurIterations_7();
		if ((((int32_t)L_72) < ((int32_t)L_73)))
		{
			goto IL_0192;
		}
	}
	{
		Material_t3870600107 * L_74 = __this->get_aoMaterial_12();
		RenderTexture_t1963041563 * L_75 = V_5;
		NullCheck(L_74);
		Material_SetTexture_m1833724755(L_74, _stringLiteral2782235674, L_75, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_76 = ___source0;
		RenderTexture_t1963041563 * L_77 = ___destination1;
		Material_t3870600107 * L_78 = __this->get_aoMaterial_12();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_76, L_77, L_78, 2, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_79 = V_5;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_79, /*hidden argument*/NULL);
	}

IL_0250:
	{
		return;
	}
}
// System.Void AmbientObscurance::Main()
extern "C"  void AmbientObscurance_Main_m2379302530 (AmbientObscurance_t2181203751 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void AntialiasingAsPostEffect::.ctor()
extern "C"  void AntialiasingAsPostEffect__ctor_m3203954829 (AntialiasingAsPostEffect_t2121704279 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase__ctor_m3869393199(__this, /*hidden argument*/NULL);
		__this->set_mode_5(1);
		__this->set_offsetScale_7((0.2f));
		__this->set_blurRadius_8((18.0f));
		__this->set_edgeThresholdMin_9((0.05f));
		__this->set_edgeThreshold_10((0.2f));
		__this->set_edgeSharpness_11((4.0f));
		return;
	}
}
// UnityEngine.Material AntialiasingAsPostEffect::CurrentAAMaterial()
extern "C"  Material_t3870600107 * AntialiasingAsPostEffect_CurrentAAMaterial_m3800539482 (AntialiasingAsPostEffect_t2121704279 * __this, const MethodInfo* method)
{
	Material_t3870600107 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		V_0 = (Material_t3870600107 *)NULL;
		int32_t L_0 = __this->get_mode_5();
		V_1 = L_0;
		int32_t L_1 = V_1;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001c;
		}
	}
	{
		Material_t3870600107 * L_2 = __this->get_materialFXAAIII_26();
		V_0 = L_2;
		goto IL_0095;
	}

IL_001c:
	{
		int32_t L_3 = V_1;
		if ((!(((uint32_t)L_3) == ((uint32_t)0))))
		{
			goto IL_002f;
		}
	}
	{
		Material_t3870600107 * L_4 = __this->get_materialFXAAII_24();
		V_0 = L_4;
		goto IL_0095;
	}

IL_002f:
	{
		int32_t L_5 = V_1;
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_0042;
		}
	}
	{
		Material_t3870600107 * L_6 = __this->get_materialFXAAPreset2_20();
		V_0 = L_6;
		goto IL_0095;
	}

IL_0042:
	{
		int32_t L_7 = V_1;
		if ((!(((uint32_t)L_7) == ((uint32_t)3))))
		{
			goto IL_0055;
		}
	}
	{
		Material_t3870600107 * L_8 = __this->get_materialFXAAPreset3_22();
		V_0 = L_8;
		goto IL_0095;
	}

IL_0055:
	{
		int32_t L_9 = V_1;
		if ((!(((uint32_t)L_9) == ((uint32_t)4))))
		{
			goto IL_0068;
		}
	}
	{
		Material_t3870600107 * L_10 = __this->get_nfaa_18();
		V_0 = L_10;
		goto IL_0095;
	}

IL_0068:
	{
		int32_t L_11 = V_1;
		if ((!(((uint32_t)L_11) == ((uint32_t)5))))
		{
			goto IL_007b;
		}
	}
	{
		Material_t3870600107 * L_12 = __this->get_ssaa_14();
		V_0 = L_12;
		goto IL_0095;
	}

IL_007b:
	{
		int32_t L_13 = V_1;
		if ((!(((uint32_t)L_13) == ((uint32_t)6))))
		{
			goto IL_008e;
		}
	}
	{
		Material_t3870600107 * L_14 = __this->get_dlaa_16();
		V_0 = L_14;
		goto IL_0095;
	}

IL_008e:
	{
		V_0 = (Material_t3870600107 *)NULL;
		goto IL_0095;
	}

IL_0095:
	{
		Material_t3870600107 * L_15 = V_0;
		return L_15;
	}
}
// System.Boolean AntialiasingAsPostEffect::CheckResources()
extern "C"  bool AntialiasingAsPostEffect_CheckResources_m2907376442 (AntialiasingAsPostEffect_t2121704279 * __this, const MethodInfo* method)
{
	{
		VirtFuncInvoker1< bool, bool >::Invoke(10 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean) */, __this, (bool)0);
		Shader_t3191267369 * L_0 = __this->get_shaderFXAAPreset2_19();
		Material_t3870600107 * L_1 = __this->get_materialFXAAPreset2_20();
		Material_t3870600107 * L_2 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(5 /* UnityEngine.Material PostEffectsBase::CreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_0, L_1);
		__this->set_materialFXAAPreset2_20(L_2);
		Shader_t3191267369 * L_3 = __this->get_shaderFXAAPreset3_21();
		Material_t3870600107 * L_4 = __this->get_materialFXAAPreset3_22();
		Material_t3870600107 * L_5 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(5 /* UnityEngine.Material PostEffectsBase::CreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_3, L_4);
		__this->set_materialFXAAPreset3_22(L_5);
		Shader_t3191267369 * L_6 = __this->get_shaderFXAAII_23();
		Material_t3870600107 * L_7 = __this->get_materialFXAAII_24();
		Material_t3870600107 * L_8 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(5 /* UnityEngine.Material PostEffectsBase::CreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_6, L_7);
		__this->set_materialFXAAII_24(L_8);
		Shader_t3191267369 * L_9 = __this->get_shaderFXAAIII_25();
		Material_t3870600107 * L_10 = __this->get_materialFXAAIII_26();
		Material_t3870600107 * L_11 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(5 /* UnityEngine.Material PostEffectsBase::CreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_9, L_10);
		__this->set_materialFXAAIII_26(L_11);
		Shader_t3191267369 * L_12 = __this->get_nfaaShader_17();
		Material_t3870600107 * L_13 = __this->get_nfaa_18();
		Material_t3870600107 * L_14 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(5 /* UnityEngine.Material PostEffectsBase::CreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_12, L_13);
		__this->set_nfaa_18(L_14);
		Shader_t3191267369 * L_15 = __this->get_ssaaShader_13();
		Material_t3870600107 * L_16 = __this->get_ssaa_14();
		Material_t3870600107 * L_17 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(5 /* UnityEngine.Material PostEffectsBase::CreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_15, L_16);
		__this->set_ssaa_14(L_17);
		Shader_t3191267369 * L_18 = __this->get_dlaaShader_15();
		Material_t3870600107 * L_19 = __this->get_dlaa_16();
		Material_t3870600107 * L_20 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(5 /* UnityEngine.Material PostEffectsBase::CreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_18, L_19);
		__this->set_dlaa_16(L_20);
		Shader_t3191267369 * L_21 = __this->get_ssaaShader_13();
		NullCheck(L_21);
		bool L_22 = Shader_get_isSupported_m1422621179(L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_00cc;
		}
	}
	{
		VirtActionInvoker0::Invoke(15 /* System.Void PostEffectsBase::NotSupported() */, __this);
		VirtActionInvoker0::Invoke(13 /* System.Void PostEffectsBase::ReportAutoDisable() */, __this);
	}

IL_00cc:
	{
		bool L_23 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		return L_23;
	}
}
// System.Void AntialiasingAsPostEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral85861059;
extern Il2CppCodeGenString* _stringLiteral2153037071;
extern Il2CppCodeGenString* _stringLiteral2127408613;
extern Il2CppCodeGenString* _stringLiteral2310381208;
extern Il2CppCodeGenString* _stringLiteral1653576088;
extern const uint32_t AntialiasingAsPostEffect_OnRenderImage_m141068209_MetadataUsageId;
extern "C"  void AntialiasingAsPostEffect_OnRenderImage_m141068209 (AntialiasingAsPostEffect_t2121704279 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AntialiasingAsPostEffect_OnRenderImage_m141068209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RenderTexture_t1963041563 * V_0 = NULL;
	Material_t3870600107 * G_B21_0 = NULL;
	RenderTexture_t1963041563 * G_B21_1 = NULL;
	RenderTexture_t1963041563 * G_B21_2 = NULL;
	Material_t3870600107 * G_B20_0 = NULL;
	RenderTexture_t1963041563 * G_B20_1 = NULL;
	RenderTexture_t1963041563 * G_B20_2 = NULL;
	int32_t G_B22_0 = 0;
	Material_t3870600107 * G_B22_1 = NULL;
	RenderTexture_t1963041563 * G_B22_2 = NULL;
	RenderTexture_t1963041563 * G_B22_3 = NULL;
	Material_t3870600107 * G_B27_0 = NULL;
	RenderTexture_t1963041563 * G_B27_1 = NULL;
	RenderTexture_t1963041563 * G_B27_2 = NULL;
	Material_t3870600107 * G_B26_0 = NULL;
	RenderTexture_t1963041563 * G_B26_1 = NULL;
	RenderTexture_t1963041563 * G_B26_2 = NULL;
	int32_t G_B28_0 = 0;
	Material_t3870600107 * G_B28_1 = NULL;
	RenderTexture_t1963041563 * G_B28_2 = NULL;
	RenderTexture_t1963041563 * G_B28_3 = NULL;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean AntialiasingAsPostEffect::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		RenderTexture_t1963041563 * L_1 = ___source0;
		RenderTexture_t1963041563 * L_2 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_023b;
	}

IL_0017:
	{
		int32_t L_3 = __this->get_mode_5();
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_0088;
		}
	}
	{
		Material_t3870600107 * L_4 = __this->get_materialFXAAIII_26();
		bool L_5 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0088;
		}
	}
	{
		Material_t3870600107 * L_6 = __this->get_materialFXAAIII_26();
		float L_7 = __this->get_edgeThresholdMin_9();
		NullCheck(L_6);
		Material_SetFloat_m981710063(L_6, _stringLiteral85861059, L_7, /*hidden argument*/NULL);
		Material_t3870600107 * L_8 = __this->get_materialFXAAIII_26();
		float L_9 = __this->get_edgeThreshold_10();
		NullCheck(L_8);
		Material_SetFloat_m981710063(L_8, _stringLiteral2153037071, L_9, /*hidden argument*/NULL);
		Material_t3870600107 * L_10 = __this->get_materialFXAAIII_26();
		float L_11 = __this->get_edgeSharpness_11();
		NullCheck(L_10);
		Material_SetFloat_m981710063(L_10, _stringLiteral2127408613, L_11, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_12 = ___source0;
		RenderTexture_t1963041563 * L_13 = ___destination1;
		Material_t3870600107 * L_14 = __this->get_materialFXAAIII_26();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		goto IL_023b;
	}

IL_0088:
	{
		int32_t L_15 = __this->get_mode_5();
		if ((!(((uint32_t)L_15) == ((uint32_t)3))))
		{
			goto IL_00b7;
		}
	}
	{
		Material_t3870600107 * L_16 = __this->get_materialFXAAPreset3_22();
		bool L_17 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_16, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00b7;
		}
	}
	{
		RenderTexture_t1963041563 * L_18 = ___source0;
		RenderTexture_t1963041563 * L_19 = ___destination1;
		Material_t3870600107 * L_20 = __this->get_materialFXAAPreset3_22();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_18, L_19, L_20, /*hidden argument*/NULL);
		goto IL_023b;
	}

IL_00b7:
	{
		int32_t L_21 = __this->get_mode_5();
		if ((!(((uint32_t)L_21) == ((uint32_t)2))))
		{
			goto IL_00f4;
		}
	}
	{
		Material_t3870600107 * L_22 = __this->get_materialFXAAPreset2_20();
		bool L_23 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_22, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00f4;
		}
	}
	{
		RenderTexture_t1963041563 * L_24 = ___source0;
		NullCheck(L_24);
		Texture_set_anisoLevel_m1894923584(L_24, 4, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_25 = ___source0;
		RenderTexture_t1963041563 * L_26 = ___destination1;
		Material_t3870600107 * L_27 = __this->get_materialFXAAPreset2_20();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_25, L_26, L_27, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_28 = ___source0;
		NullCheck(L_28);
		Texture_set_anisoLevel_m1894923584(L_28, 0, /*hidden argument*/NULL);
		goto IL_023b;
	}

IL_00f4:
	{
		int32_t L_29 = __this->get_mode_5();
		if ((!(((uint32_t)L_29) == ((uint32_t)0))))
		{
			goto IL_0123;
		}
	}
	{
		Material_t3870600107 * L_30 = __this->get_materialFXAAII_24();
		bool L_31 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_30, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_0123;
		}
	}
	{
		RenderTexture_t1963041563 * L_32 = ___source0;
		RenderTexture_t1963041563 * L_33 = ___destination1;
		Material_t3870600107 * L_34 = __this->get_materialFXAAII_24();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_32, L_33, L_34, /*hidden argument*/NULL);
		goto IL_023b;
	}

IL_0123:
	{
		int32_t L_35 = __this->get_mode_5();
		if ((!(((uint32_t)L_35) == ((uint32_t)5))))
		{
			goto IL_0152;
		}
	}
	{
		Material_t3870600107 * L_36 = __this->get_ssaa_14();
		bool L_37 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_36, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0152;
		}
	}
	{
		RenderTexture_t1963041563 * L_38 = ___source0;
		RenderTexture_t1963041563 * L_39 = ___destination1;
		Material_t3870600107 * L_40 = __this->get_ssaa_14();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_38, L_39, L_40, /*hidden argument*/NULL);
		goto IL_023b;
	}

IL_0152:
	{
		int32_t L_41 = __this->get_mode_5();
		if ((!(((uint32_t)L_41) == ((uint32_t)6))))
		{
			goto IL_01c0;
		}
	}
	{
		Material_t3870600107 * L_42 = __this->get_dlaa_16();
		bool L_43 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_42, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_01c0;
		}
	}
	{
		RenderTexture_t1963041563 * L_44 = ___source0;
		NullCheck(L_44);
		Texture_set_anisoLevel_m1894923584(L_44, 0, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_45 = ___source0;
		NullCheck(L_45);
		int32_t L_46 = RenderTexture_get_width_m1498578543(L_45, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_47 = ___source0;
		NullCheck(L_47);
		int32_t L_48 = RenderTexture_get_height_m4010076224(L_47, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_49 = RenderTexture_GetTemporary_m469965696(NULL /*static, unused*/, L_46, L_48, /*hidden argument*/NULL);
		V_0 = L_49;
		RenderTexture_t1963041563 * L_50 = ___source0;
		RenderTexture_t1963041563 * L_51 = V_0;
		Material_t3870600107 * L_52 = __this->get_dlaa_16();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_50, L_51, L_52, 0, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_53 = V_0;
		RenderTexture_t1963041563 * L_54 = ___destination1;
		Material_t3870600107 * L_55 = __this->get_dlaa_16();
		bool L_56 = __this->get_dlaaSharp_12();
		G_B20_0 = L_55;
		G_B20_1 = L_54;
		G_B20_2 = L_53;
		if (!L_56)
		{
			G_B21_0 = L_55;
			G_B21_1 = L_54;
			G_B21_2 = L_53;
			goto IL_01af;
		}
	}
	{
		G_B22_0 = 2;
		G_B22_1 = G_B20_0;
		G_B22_2 = G_B20_1;
		G_B22_3 = G_B20_2;
		goto IL_01b0;
	}

IL_01af:
	{
		G_B22_0 = 1;
		G_B22_1 = G_B21_0;
		G_B22_2 = G_B21_1;
		G_B22_3 = G_B21_2;
	}

IL_01b0:
	{
		Graphics_Blit_m336256356(NULL /*static, unused*/, G_B22_3, G_B22_2, G_B22_1, G_B22_0, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_57 = V_0;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		goto IL_023b;
	}

IL_01c0:
	{
		int32_t L_58 = __this->get_mode_5();
		if ((!(((uint32_t)L_58) == ((uint32_t)4))))
		{
			goto IL_0234;
		}
	}
	{
		Material_t3870600107 * L_59 = __this->get_nfaa_18();
		bool L_60 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_59, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_60)
		{
			goto IL_0234;
		}
	}
	{
		RenderTexture_t1963041563 * L_61 = ___source0;
		NullCheck(L_61);
		Texture_set_anisoLevel_m1894923584(L_61, 0, /*hidden argument*/NULL);
		Material_t3870600107 * L_62 = __this->get_nfaa_18();
		float L_63 = __this->get_offsetScale_7();
		NullCheck(L_62);
		Material_SetFloat_m981710063(L_62, _stringLiteral2310381208, L_63, /*hidden argument*/NULL);
		Material_t3870600107 * L_64 = __this->get_nfaa_18();
		float L_65 = __this->get_blurRadius_8();
		NullCheck(L_64);
		Material_SetFloat_m981710063(L_64, _stringLiteral1653576088, L_65, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_66 = ___source0;
		RenderTexture_t1963041563 * L_67 = ___destination1;
		Material_t3870600107 * L_68 = __this->get_nfaa_18();
		bool L_69 = __this->get_showGeneratedNormals_6();
		G_B26_0 = L_68;
		G_B26_1 = L_67;
		G_B26_2 = L_66;
		if (!L_69)
		{
			G_B27_0 = L_68;
			G_B27_1 = L_67;
			G_B27_2 = L_66;
			goto IL_0229;
		}
	}
	{
		G_B28_0 = 1;
		G_B28_1 = G_B26_0;
		G_B28_2 = G_B26_1;
		G_B28_3 = G_B26_2;
		goto IL_022a;
	}

IL_0229:
	{
		G_B28_0 = 0;
		G_B28_1 = G_B27_0;
		G_B28_2 = G_B27_1;
		G_B28_3 = G_B27_2;
	}

IL_022a:
	{
		Graphics_Blit_m336256356(NULL /*static, unused*/, G_B28_3, G_B28_2, G_B28_1, G_B28_0, /*hidden argument*/NULL);
		goto IL_023b;
	}

IL_0234:
	{
		RenderTexture_t1963041563 * L_70 = ___source0;
		RenderTexture_t1963041563 * L_71 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_70, L_71, /*hidden argument*/NULL);
	}

IL_023b:
	{
		return;
	}
}
// System.Void AntialiasingAsPostEffect::Main()
extern "C"  void AntialiasingAsPostEffect_Main_m711584400 (AntialiasingAsPostEffect_t2121704279 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Bloom::.ctor()
extern "C"  void Bloom__ctor_m1759598111 (Bloom_t64280035 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase__ctor_m3869393199(__this, /*hidden argument*/NULL);
		__this->set_screenBlendMode_6(1);
		__this->set_hdr_7(0);
		__this->set_sepBlurSpread_9((2.5f));
		__this->set_quality_10(1);
		__this->set_bloomIntensity_11((0.5f));
		__this->set_bloomThreshhold_12((0.5f));
		Color_t4194546905  L_0 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_bloomThreshholdColor_13(L_0);
		__this->set_bloomBlurIterations_14(2);
		__this->set_hollywoodFlareBlurIterations_15(2);
		__this->set_lensflareMode_17(1);
		__this->set_hollyStretchWidth_18((2.5f));
		__this->set_lensflareThreshhold_20((0.3f));
		__this->set_lensFlareSaturation_21((0.75f));
		Color_t4194546905  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Color__ctor_m2252924356(&L_1, (0.4f), (0.4f), (0.8f), (0.75f), /*hidden argument*/NULL);
		__this->set_flareColorA_22(L_1);
		Color_t4194546905  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Color__ctor_m2252924356(&L_2, (0.4f), (0.8f), (0.8f), (0.75f), /*hidden argument*/NULL);
		__this->set_flareColorB_23(L_2);
		Color_t4194546905  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Color__ctor_m2252924356(&L_3, (0.8f), (0.4f), (0.8f), (0.75f), /*hidden argument*/NULL);
		__this->set_flareColorC_24(L_3);
		Color_t4194546905  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Color__ctor_m2252924356(&L_4, (0.8f), (0.4f), (((float)((float)0))), (0.75f), /*hidden argument*/NULL);
		__this->set_flareColorD_25(L_4);
		__this->set_blurWidth_26((1.0f));
		return;
	}
}
// System.Boolean Bloom::CheckResources()
extern "C"  bool Bloom_CheckResources_m601689564 (Bloom_t64280035 * __this, const MethodInfo* method)
{
	{
		VirtFuncInvoker1< bool, bool >::Invoke(10 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean) */, __this, (bool)0);
		Shader_t3191267369 * L_0 = __this->get_screenBlendShader_30();
		Material_t3870600107 * L_1 = __this->get_screenBlend_31();
		Material_t3870600107 * L_2 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_0, L_1);
		__this->set_screenBlend_31(L_2);
		Shader_t3191267369 * L_3 = __this->get_lensFlareShader_28();
		Material_t3870600107 * L_4 = __this->get_lensFlareMaterial_29();
		Material_t3870600107 * L_5 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_3, L_4);
		__this->set_lensFlareMaterial_29(L_5);
		Shader_t3191267369 * L_6 = __this->get_blurAndFlaresShader_32();
		Material_t3870600107 * L_7 = __this->get_blurAndFlaresMaterial_33();
		Material_t3870600107 * L_8 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_6, L_7);
		__this->set_blurAndFlaresMaterial_33(L_8);
		Shader_t3191267369 * L_9 = __this->get_brightPassFilterShader_34();
		Material_t3870600107 * L_10 = __this->get_brightPassFilterMaterial_35();
		Material_t3870600107 * L_11 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_9, L_10);
		__this->set_brightPassFilterMaterial_35(L_11);
		bool L_12 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		if (L_12)
		{
			goto IL_0079;
		}
	}
	{
		VirtActionInvoker0::Invoke(13 /* System.Void PostEffectsBase::ReportAutoDisable() */, __this);
	}

IL_0079:
	{
		bool L_13 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		return L_13;
	}
}
// System.Void Bloom::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2474470625;
extern Il2CppCodeGenString* _stringLiteral2416425146;
extern Il2CppCodeGenString* _stringLiteral1842654089;
extern Il2CppCodeGenString* _stringLiteral2851286417;
extern Il2CppCodeGenString* _stringLiteral4262571968;
extern Il2CppCodeGenString* _stringLiteral1014379156;
extern Il2CppCodeGenString* _stringLiteral1541889956;
extern const uint32_t Bloom_OnRenderImage_m3258681055_MetadataUsageId;
extern "C"  void Bloom_OnRenderImage_m3258681055 (Bloom_t64280035 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Bloom_OnRenderImage_m3258681055_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	RenderTexture_t1963041563 * V_8 = NULL;
	RenderTexture_t1963041563 * V_9 = NULL;
	RenderTexture_t1963041563 * V_10 = NULL;
	RenderTexture_t1963041563 * V_11 = NULL;
	int32_t V_12 = 0;
	float V_13 = 0.0f;
	RenderTexture_t1963041563 * V_14 = NULL;
	RenderTexture_t1963041563 * V_15 = NULL;
	float V_16 = 0.0f;
	float V_17 = 0.0f;
	float V_18 = 0.0f;
	float V_19 = 0.0f;
	int32_t V_20 = 0;
	RenderTexture_t1963041563 * V_21 = NULL;
	int32_t G_B5_0 = 0;
	Bloom_t64280035 * G_B5_1 = NULL;
	int32_t G_B4_0 = 0;
	Bloom_t64280035 * G_B4_1 = NULL;
	bool G_B9_0 = false;
	Bloom_t64280035 * G_B9_1 = NULL;
	bool G_B8_0 = false;
	Bloom_t64280035 * G_B8_1 = NULL;
	int32_t G_B14_0 = 0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Bloom::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		RenderTexture_t1963041563 * L_1 = ___source0;
		RenderTexture_t1963041563 * L_2 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_0762;
	}

IL_0017:
	{
		__this->set_doHdr_8((bool)0);
		int32_t L_3 = __this->get_hdr_7();
		if ((!(((uint32_t)L_3) == ((uint32_t)0))))
		{
			goto IL_0050;
		}
	}
	{
		RenderTexture_t1963041563 * L_4 = ___source0;
		NullCheck(L_4);
		int32_t L_5 = RenderTexture_get_format_m3502109954(L_4, /*hidden argument*/NULL);
		int32_t L_6 = ((((int32_t)L_5) == ((int32_t)2))? 1 : 0);
		G_B4_0 = L_6;
		G_B4_1 = __this;
		if (!L_6)
		{
			G_B5_0 = L_6;
			G_B5_1 = __this;
			goto IL_0046;
		}
	}
	{
		Camera_t2727095145 * L_7 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_7);
		bool L_8 = Camera_get_hdr_m1509362829(L_7, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_8));
		G_B5_1 = G_B4_1;
	}

IL_0046:
	{
		NullCheck(G_B5_1);
		G_B5_1->set_doHdr_8((bool)G_B5_0);
		goto IL_005f;
	}

IL_0050:
	{
		int32_t L_9 = __this->get_hdr_7();
		__this->set_doHdr_8((bool)((((int32_t)L_9) == ((int32_t)1))? 1 : 0));
	}

IL_005f:
	{
		bool L_10 = __this->get_doHdr_8();
		bool L_11 = L_10;
		G_B8_0 = L_11;
		G_B8_1 = __this;
		if (!L_11)
		{
			G_B9_0 = L_11;
			G_B9_1 = __this;
			goto IL_0073;
		}
	}
	{
		bool L_12 = ((PostEffectsBase_t1820837395 *)__this)->get_supportHDRTextures_2();
		G_B9_0 = L_12;
		G_B9_1 = G_B8_1;
	}

IL_0073:
	{
		NullCheck(G_B9_1);
		G_B9_1->set_doHdr_8(G_B9_0);
		int32_t L_13 = __this->get_screenBlendMode_6();
		V_0 = L_13;
		bool L_14 = __this->get_doHdr_8();
		if (!L_14)
		{
			goto IL_008c;
		}
	}
	{
		V_0 = 1;
	}

IL_008c:
	{
		bool L_15 = __this->get_doHdr_8();
		if (!L_15)
		{
			goto IL_009d;
		}
	}
	{
		G_B14_0 = 2;
		goto IL_009e;
	}

IL_009d:
	{
		G_B14_0 = 7;
	}

IL_009e:
	{
		V_1 = G_B14_0;
		RenderTexture_t1963041563 * L_16 = ___source0;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m1498578543(L_16, /*hidden argument*/NULL);
		V_2 = ((int32_t)((int32_t)L_17/(int32_t)2));
		RenderTexture_t1963041563 * L_18 = ___source0;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m4010076224(L_18, /*hidden argument*/NULL);
		V_3 = ((int32_t)((int32_t)L_19/(int32_t)2));
		RenderTexture_t1963041563 * L_20 = ___source0;
		NullCheck(L_20);
		int32_t L_21 = RenderTexture_get_width_m1498578543(L_20, /*hidden argument*/NULL);
		V_4 = ((int32_t)((int32_t)L_21/(int32_t)4));
		RenderTexture_t1963041563 * L_22 = ___source0;
		NullCheck(L_22);
		int32_t L_23 = RenderTexture_get_height_m4010076224(L_22, /*hidden argument*/NULL);
		V_5 = ((int32_t)((int32_t)L_23/(int32_t)4));
		RenderTexture_t1963041563 * L_24 = ___source0;
		NullCheck(L_24);
		int32_t L_25 = RenderTexture_get_width_m1498578543(L_24, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_26 = ___source0;
		NullCheck(L_26);
		int32_t L_27 = RenderTexture_get_height_m4010076224(L_26, /*hidden argument*/NULL);
		V_6 = ((float)((float)((float)((float)(1.0f)*(float)(((float)((float)L_25)))))/(float)((float)((float)(1.0f)*(float)(((float)((float)L_27)))))));
		V_7 = (0.001953125f);
		int32_t L_28 = V_4;
		int32_t L_29 = V_5;
		int32_t L_30 = V_1;
		RenderTexture_t1963041563 * L_31 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, L_28, L_29, 0, L_30, /*hidden argument*/NULL);
		V_8 = L_31;
		int32_t L_32 = V_2;
		int32_t L_33 = V_3;
		int32_t L_34 = V_1;
		RenderTexture_t1963041563 * L_35 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, L_32, L_33, 0, L_34, /*hidden argument*/NULL);
		V_9 = L_35;
		int32_t L_36 = __this->get_quality_10();
		if ((((int32_t)L_36) <= ((int32_t)0)))
		{
			goto IL_0155;
		}
	}
	{
		RenderTexture_t1963041563 * L_37 = ___source0;
		RenderTexture_t1963041563 * L_38 = V_9;
		Material_t3870600107 * L_39 = __this->get_screenBlend_31();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_37, L_38, L_39, 2, /*hidden argument*/NULL);
		int32_t L_40 = V_4;
		int32_t L_41 = V_5;
		int32_t L_42 = V_1;
		RenderTexture_t1963041563 * L_43 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, L_40, L_41, 0, L_42, /*hidden argument*/NULL);
		V_10 = L_43;
		RenderTexture_t1963041563 * L_44 = V_9;
		RenderTexture_t1963041563 * L_45 = V_10;
		Material_t3870600107 * L_46 = __this->get_screenBlend_31();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_44, L_45, L_46, 2, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_47 = V_10;
		RenderTexture_t1963041563 * L_48 = V_8;
		Material_t3870600107 * L_49 = __this->get_screenBlend_31();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_47, L_48, L_49, 6, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_50 = V_10;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		goto IL_016d;
	}

IL_0155:
	{
		RenderTexture_t1963041563 * L_51 = ___source0;
		RenderTexture_t1963041563 * L_52 = V_9;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_51, L_52, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_53 = V_9;
		RenderTexture_t1963041563 * L_54 = V_8;
		Material_t3870600107 * L_55 = __this->get_screenBlend_31();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_53, L_54, L_55, 6, /*hidden argument*/NULL);
	}

IL_016d:
	{
		RenderTexture_t1963041563 * L_56 = V_9;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_56, /*hidden argument*/NULL);
		int32_t L_57 = V_4;
		int32_t L_58 = V_5;
		int32_t L_59 = V_1;
		RenderTexture_t1963041563 * L_60 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, L_57, L_58, 0, L_59, /*hidden argument*/NULL);
		V_11 = L_60;
		float L_61 = __this->get_bloomThreshhold_12();
		Color_t4194546905  L_62 = __this->get_bloomThreshholdColor_13();
		Color_t4194546905  L_63 = Color_op_Multiply_m3650220910(NULL /*static, unused*/, L_61, L_62, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_64 = V_8;
		RenderTexture_t1963041563 * L_65 = V_11;
		Bloom_BrightFilter_m3623342315(__this, L_63, L_64, L_65, /*hidden argument*/NULL);
		int32_t L_66 = __this->get_bloomBlurIterations_14();
		if ((((int32_t)L_66) >= ((int32_t)1)))
		{
			goto IL_01b4;
		}
	}
	{
		__this->set_bloomBlurIterations_14(1);
		goto IL_01c9;
	}

IL_01b4:
	{
		int32_t L_67 = __this->get_bloomBlurIterations_14();
		if ((((int32_t)L_67) <= ((int32_t)((int32_t)10))))
		{
			goto IL_01c9;
		}
	}
	{
		__this->set_bloomBlurIterations_14(((int32_t)10));
	}

IL_01c9:
	{
		V_12 = 0;
		goto IL_02ce;
	}

IL_01d1:
	{
		int32_t L_68 = V_12;
		float L_69 = __this->get_sepBlurSpread_9();
		V_13 = ((float)((float)((float)((float)(1.0f)+(float)((float)((float)(((float)((float)L_68)))*(float)(0.25f)))))*(float)L_69));
		int32_t L_70 = V_4;
		int32_t L_71 = V_5;
		int32_t L_72 = V_1;
		RenderTexture_t1963041563 * L_73 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, L_70, L_71, 0, L_72, /*hidden argument*/NULL);
		V_14 = L_73;
		Material_t3870600107 * L_74 = __this->get_blurAndFlaresMaterial_33();
		float L_75 = V_13;
		float L_76 = V_7;
		Vector4_t4282066567  L_77;
		memset(&L_77, 0, sizeof(L_77));
		Vector4__ctor_m2441427762(&L_77, (((float)((float)0))), ((float)((float)L_75*(float)L_76)), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_74);
		Material_SetVector_m3505096203(L_74, _stringLiteral2474470625, L_77, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_78 = V_11;
		RenderTexture_t1963041563 * L_79 = V_14;
		Material_t3870600107 * L_80 = __this->get_blurAndFlaresMaterial_33();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_78, L_79, L_80, 4, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_81 = V_11;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_81, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_82 = V_14;
		V_11 = L_82;
		int32_t L_83 = V_4;
		int32_t L_84 = V_5;
		int32_t L_85 = V_1;
		RenderTexture_t1963041563 * L_86 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, L_83, L_84, 0, L_85, /*hidden argument*/NULL);
		V_14 = L_86;
		Material_t3870600107 * L_87 = __this->get_blurAndFlaresMaterial_33();
		float L_88 = V_13;
		float L_89 = V_6;
		float L_90 = V_7;
		Vector4_t4282066567  L_91;
		memset(&L_91, 0, sizeof(L_91));
		Vector4__ctor_m2441427762(&L_91, ((float)((float)((float)((float)L_88/(float)L_89))*(float)L_90)), (((float)((float)0))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_87);
		Material_SetVector_m3505096203(L_87, _stringLiteral2474470625, L_91, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_92 = V_11;
		RenderTexture_t1963041563 * L_93 = V_14;
		Material_t3870600107 * L_94 = __this->get_blurAndFlaresMaterial_33();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_92, L_93, L_94, 4, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_95 = V_11;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_95, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_96 = V_14;
		V_11 = L_96;
		int32_t L_97 = __this->get_quality_10();
		if ((((int32_t)L_97) <= ((int32_t)0)))
		{
			goto IL_02c8;
		}
	}
	{
		int32_t L_98 = V_12;
		if (L_98)
		{
			goto IL_02b0;
		}
	}
	{
		RenderTexture_t1963041563 * L_99 = V_8;
		Graphics_SetRenderTarget_m3051614107(NULL /*static, unused*/, L_99, /*hidden argument*/NULL);
		Color_t4194546905  L_100 = Color_get_black_m1687201969(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_Clear_m2980983731(NULL /*static, unused*/, (bool)0, (bool)1, L_100, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_101 = V_11;
		RenderTexture_t1963041563 * L_102 = V_8;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_101, L_102, /*hidden argument*/NULL);
		goto IL_02c8;
	}

IL_02b0:
	{
		RenderTexture_t1963041563 * L_103 = V_8;
		NullCheck(L_103);
		RenderTexture_MarkRestoreExpected_m2220245707(L_103, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_104 = V_11;
		RenderTexture_t1963041563 * L_105 = V_8;
		Material_t3870600107 * L_106 = __this->get_screenBlend_31();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_104, L_105, L_106, ((int32_t)10), /*hidden argument*/NULL);
	}

IL_02c8:
	{
		int32_t L_107 = V_12;
		V_12 = ((int32_t)((int32_t)L_107+(int32_t)1));
	}

IL_02ce:
	{
		int32_t L_108 = V_12;
		int32_t L_109 = __this->get_bloomBlurIterations_14();
		if ((((int32_t)L_108) < ((int32_t)L_109)))
		{
			goto IL_01d1;
		}
	}
	{
		int32_t L_110 = __this->get_quality_10();
		if ((((int32_t)L_110) <= ((int32_t)0)))
		{
			goto IL_030a;
		}
	}
	{
		RenderTexture_t1963041563 * L_111 = V_11;
		Graphics_SetRenderTarget_m3051614107(NULL /*static, unused*/, L_111, /*hidden argument*/NULL);
		Color_t4194546905  L_112 = Color_get_black_m1687201969(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_Clear_m2980983731(NULL /*static, unused*/, (bool)0, (bool)1, L_112, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_113 = V_8;
		RenderTexture_t1963041563 * L_114 = V_11;
		Material_t3870600107 * L_115 = __this->get_screenBlend_31();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_113, L_114, L_115, 6, /*hidden argument*/NULL);
	}

IL_030a:
	{
		float L_116 = __this->get_lensflareIntensity_19();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_117 = ((Mathf_t4203372500_StaticFields*)Mathf_t4203372500_il2cpp_TypeInfo_var->static_fields)->get_Epsilon_0();
		if ((((float)L_116) <= ((float)L_117)))
		{
			goto IL_06de;
		}
	}
	{
		int32_t L_118 = V_4;
		int32_t L_119 = V_5;
		int32_t L_120 = V_1;
		RenderTexture_t1963041563 * L_121 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, L_118, L_119, 0, L_120, /*hidden argument*/NULL);
		V_15 = L_121;
		int32_t L_122 = __this->get_lensflareMode_17();
		if (L_122)
		{
			goto IL_0410;
		}
	}
	{
		float L_123 = __this->get_lensflareThreshhold_20();
		RenderTexture_t1963041563 * L_124 = V_11;
		RenderTexture_t1963041563 * L_125 = V_15;
		Bloom_BrightFilter_m174238832(__this, L_123, L_124, L_125, /*hidden argument*/NULL);
		int32_t L_126 = __this->get_quality_10();
		if ((((int32_t)L_126) <= ((int32_t)0)))
		{
			goto IL_03f2;
		}
	}
	{
		Material_t3870600107 * L_127 = __this->get_blurAndFlaresMaterial_33();
		RenderTexture_t1963041563 * L_128 = V_8;
		NullCheck(L_128);
		int32_t L_129 = RenderTexture_get_height_m4010076224(L_128, /*hidden argument*/NULL);
		Vector4_t4282066567  L_130;
		memset(&L_130, 0, sizeof(L_130));
		Vector4__ctor_m2441427762(&L_130, (((float)((float)0))), ((float)((float)(1.5f)/(float)((float)((float)(1.0f)*(float)(((float)((float)L_129))))))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_127);
		Material_SetVector_m3505096203(L_127, _stringLiteral2474470625, L_130, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_131 = V_8;
		Graphics_SetRenderTarget_m3051614107(NULL /*static, unused*/, L_131, /*hidden argument*/NULL);
		Color_t4194546905  L_132 = Color_get_black_m1687201969(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_Clear_m2980983731(NULL /*static, unused*/, (bool)0, (bool)1, L_132, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_133 = V_15;
		RenderTexture_t1963041563 * L_134 = V_8;
		Material_t3870600107 * L_135 = __this->get_blurAndFlaresMaterial_33();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_133, L_134, L_135, 4, /*hidden argument*/NULL);
		Material_t3870600107 * L_136 = __this->get_blurAndFlaresMaterial_33();
		RenderTexture_t1963041563 * L_137 = V_8;
		NullCheck(L_137);
		int32_t L_138 = RenderTexture_get_width_m1498578543(L_137, /*hidden argument*/NULL);
		Vector4_t4282066567  L_139;
		memset(&L_139, 0, sizeof(L_139));
		Vector4__ctor_m2441427762(&L_139, ((float)((float)(1.5f)/(float)((float)((float)(1.0f)*(float)(((float)((float)L_138))))))), (((float)((float)0))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_136);
		Material_SetVector_m3505096203(L_136, _stringLiteral2474470625, L_139, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_140 = V_15;
		Graphics_SetRenderTarget_m3051614107(NULL /*static, unused*/, L_140, /*hidden argument*/NULL);
		Color_t4194546905  L_141 = Color_get_black_m1687201969(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_Clear_m2980983731(NULL /*static, unused*/, (bool)0, (bool)1, L_141, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_142 = V_8;
		RenderTexture_t1963041563 * L_143 = V_15;
		Material_t3870600107 * L_144 = __this->get_blurAndFlaresMaterial_33();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_142, L_143, L_144, 4, /*hidden argument*/NULL);
	}

IL_03f2:
	{
		RenderTexture_t1963041563 * L_145 = V_15;
		RenderTexture_t1963041563 * L_146 = V_15;
		Bloom_Vignette_m1189655368(__this, (0.975f), L_145, L_146, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_147 = V_15;
		RenderTexture_t1963041563 * L_148 = V_11;
		Bloom_BlendFlares_m3656378831(__this, L_147, L_148, /*hidden argument*/NULL);
		goto IL_06d7;
	}

IL_0410:
	{
		float L_149 = __this->get_flareRotation_16();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_150 = cosf(L_149);
		V_16 = ((float)((float)(1.0f)*(float)L_150));
		float L_151 = __this->get_flareRotation_16();
		float L_152 = sinf(L_151);
		V_17 = ((float)((float)(1.0f)*(float)L_152));
		float L_153 = __this->get_hollyStretchWidth_18();
		float L_154 = V_6;
		float L_155 = V_7;
		V_18 = ((float)((float)((float)((float)((float)((float)L_153*(float)(1.0f)))/(float)L_154))*(float)L_155));
		float L_156 = __this->get_hollyStretchWidth_18();
		float L_157 = V_7;
		V_19 = ((float)((float)L_156*(float)L_157));
		Material_t3870600107 * L_158 = __this->get_blurAndFlaresMaterial_33();
		float L_159 = V_16;
		float L_160 = V_17;
		Vector4_t4282066567  L_161;
		memset(&L_161, 0, sizeof(L_161));
		Vector4__ctor_m2441427762(&L_161, L_159, L_160, (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_158);
		Material_SetVector_m3505096203(L_158, _stringLiteral2474470625, L_161, /*hidden argument*/NULL);
		Material_t3870600107 * L_162 = __this->get_blurAndFlaresMaterial_33();
		float L_163 = __this->get_lensflareThreshhold_20();
		Vector4_t4282066567  L_164;
		memset(&L_164, 0, sizeof(L_164));
		Vector4__ctor_m2441427762(&L_164, L_163, (1.0f), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_162);
		Material_SetVector_m3505096203(L_162, _stringLiteral2416425146, L_164, /*hidden argument*/NULL);
		Material_t3870600107 * L_165 = __this->get_blurAndFlaresMaterial_33();
		Color_t4194546905 * L_166 = __this->get_address_of_flareColorA_22();
		float L_167 = L_166->get_r_0();
		Color_t4194546905 * L_168 = __this->get_address_of_flareColorA_22();
		float L_169 = L_168->get_g_1();
		Color_t4194546905 * L_170 = __this->get_address_of_flareColorA_22();
		float L_171 = L_170->get_b_2();
		Color_t4194546905 * L_172 = __this->get_address_of_flareColorA_22();
		float L_173 = L_172->get_a_3();
		Vector4_t4282066567  L_174;
		memset(&L_174, 0, sizeof(L_174));
		Vector4__ctor_m2441427762(&L_174, L_167, L_169, L_171, L_173, /*hidden argument*/NULL);
		Color_t4194546905 * L_175 = __this->get_address_of_flareColorA_22();
		float L_176 = L_175->get_a_3();
		Vector4_t4282066567  L_177 = Vector4_op_Multiply_m209031836(NULL /*static, unused*/, L_174, L_176, /*hidden argument*/NULL);
		float L_178 = __this->get_lensflareIntensity_19();
		Vector4_t4282066567  L_179 = Vector4_op_Multiply_m209031836(NULL /*static, unused*/, L_177, L_178, /*hidden argument*/NULL);
		NullCheck(L_165);
		Material_SetVector_m3505096203(L_165, _stringLiteral1842654089, L_179, /*hidden argument*/NULL);
		Material_t3870600107 * L_180 = __this->get_blurAndFlaresMaterial_33();
		float L_181 = __this->get_lensFlareSaturation_21();
		NullCheck(L_180);
		Material_SetFloat_m981710063(L_180, _stringLiteral2851286417, L_181, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_182 = V_8;
		NullCheck(L_182);
		RenderTexture_DiscardContents_m1898044554(L_182, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_183 = V_15;
		RenderTexture_t1963041563 * L_184 = V_8;
		Material_t3870600107 * L_185 = __this->get_blurAndFlaresMaterial_33();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_183, L_184, L_185, 2, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_186 = V_15;
		NullCheck(L_186);
		RenderTexture_DiscardContents_m1898044554(L_186, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_187 = V_8;
		RenderTexture_t1963041563 * L_188 = V_15;
		Material_t3870600107 * L_189 = __this->get_blurAndFlaresMaterial_33();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_187, L_188, L_189, 3, /*hidden argument*/NULL);
		Material_t3870600107 * L_190 = __this->get_blurAndFlaresMaterial_33();
		float L_191 = V_16;
		float L_192 = V_18;
		float L_193 = V_17;
		float L_194 = V_18;
		Vector4_t4282066567  L_195;
		memset(&L_195, 0, sizeof(L_195));
		Vector4__ctor_m2441427762(&L_195, ((float)((float)L_191*(float)L_192)), ((float)((float)L_193*(float)L_194)), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_190);
		Material_SetVector_m3505096203(L_190, _stringLiteral2474470625, L_195, /*hidden argument*/NULL);
		Material_t3870600107 * L_196 = __this->get_blurAndFlaresMaterial_33();
		float L_197 = __this->get_hollyStretchWidth_18();
		NullCheck(L_196);
		Material_SetFloat_m981710063(L_196, _stringLiteral4262571968, L_197, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_198 = V_8;
		NullCheck(L_198);
		RenderTexture_DiscardContents_m1898044554(L_198, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_199 = V_15;
		RenderTexture_t1963041563 * L_200 = V_8;
		Material_t3870600107 * L_201 = __this->get_blurAndFlaresMaterial_33();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_199, L_200, L_201, 1, /*hidden argument*/NULL);
		Material_t3870600107 * L_202 = __this->get_blurAndFlaresMaterial_33();
		float L_203 = __this->get_hollyStretchWidth_18();
		NullCheck(L_202);
		Material_SetFloat_m981710063(L_202, _stringLiteral4262571968, ((float)((float)L_203*(float)(2.0f))), /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_204 = V_15;
		NullCheck(L_204);
		RenderTexture_DiscardContents_m1898044554(L_204, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_205 = V_8;
		RenderTexture_t1963041563 * L_206 = V_15;
		Material_t3870600107 * L_207 = __this->get_blurAndFlaresMaterial_33();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_205, L_206, L_207, 1, /*hidden argument*/NULL);
		Material_t3870600107 * L_208 = __this->get_blurAndFlaresMaterial_33();
		float L_209 = __this->get_hollyStretchWidth_18();
		NullCheck(L_208);
		Material_SetFloat_m981710063(L_208, _stringLiteral4262571968, ((float)((float)L_209*(float)(4.0f))), /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_210 = V_8;
		NullCheck(L_210);
		RenderTexture_DiscardContents_m1898044554(L_210, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_211 = V_15;
		RenderTexture_t1963041563 * L_212 = V_8;
		Material_t3870600107 * L_213 = __this->get_blurAndFlaresMaterial_33();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_211, L_212, L_213, 1, /*hidden argument*/NULL);
		V_12 = 0;
		goto IL_0682;
	}

IL_05f4:
	{
		float L_214 = __this->get_hollyStretchWidth_18();
		float L_215 = V_6;
		float L_216 = V_7;
		V_18 = ((float)((float)((float)((float)((float)((float)L_214*(float)(2.0f)))/(float)L_215))*(float)L_216));
		Material_t3870600107 * L_217 = __this->get_blurAndFlaresMaterial_33();
		float L_218 = V_18;
		float L_219 = V_16;
		float L_220 = V_18;
		float L_221 = V_17;
		Vector4_t4282066567  L_222;
		memset(&L_222, 0, sizeof(L_222));
		Vector4__ctor_m2441427762(&L_222, ((float)((float)L_218*(float)L_219)), ((float)((float)L_220*(float)L_221)), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_217);
		Material_SetVector_m3505096203(L_217, _stringLiteral2474470625, L_222, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_223 = V_15;
		NullCheck(L_223);
		RenderTexture_DiscardContents_m1898044554(L_223, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_224 = V_8;
		RenderTexture_t1963041563 * L_225 = V_15;
		Material_t3870600107 * L_226 = __this->get_blurAndFlaresMaterial_33();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_224, L_225, L_226, 4, /*hidden argument*/NULL);
		Material_t3870600107 * L_227 = __this->get_blurAndFlaresMaterial_33();
		float L_228 = V_18;
		float L_229 = V_16;
		float L_230 = V_18;
		float L_231 = V_17;
		Vector4_t4282066567  L_232;
		memset(&L_232, 0, sizeof(L_232));
		Vector4__ctor_m2441427762(&L_232, ((float)((float)L_228*(float)L_229)), ((float)((float)L_230*(float)L_231)), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_227);
		Material_SetVector_m3505096203(L_227, _stringLiteral2474470625, L_232, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_233 = V_8;
		NullCheck(L_233);
		RenderTexture_DiscardContents_m1898044554(L_233, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_234 = V_15;
		RenderTexture_t1963041563 * L_235 = V_8;
		Material_t3870600107 * L_236 = __this->get_blurAndFlaresMaterial_33();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_234, L_235, L_236, 4, /*hidden argument*/NULL);
		int32_t L_237 = V_12;
		V_12 = ((int32_t)((int32_t)L_237+(int32_t)1));
	}

IL_0682:
	{
		int32_t L_238 = V_12;
		int32_t L_239 = __this->get_hollywoodFlareBlurIterations_15();
		if ((((int32_t)L_238) < ((int32_t)L_239)))
		{
			goto IL_05f4;
		}
	}
	{
		int32_t L_240 = __this->get_lensflareMode_17();
		if ((!(((uint32_t)L_240) == ((uint32_t)1))))
		{
			goto IL_06af;
		}
	}
	{
		RenderTexture_t1963041563 * L_241 = V_8;
		RenderTexture_t1963041563 * L_242 = V_11;
		Bloom_AddTo_m3061610318(__this, (1.0f), L_241, L_242, /*hidden argument*/NULL);
		goto IL_06d7;
	}

IL_06af:
	{
		RenderTexture_t1963041563 * L_243 = V_8;
		RenderTexture_t1963041563 * L_244 = V_15;
		Bloom_Vignette_m1189655368(__this, (1.0f), L_243, L_244, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_245 = V_15;
		RenderTexture_t1963041563 * L_246 = V_8;
		Bloom_BlendFlares_m3656378831(__this, L_245, L_246, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_247 = V_8;
		RenderTexture_t1963041563 * L_248 = V_11;
		Bloom_AddTo_m3061610318(__this, (1.0f), L_247, L_248, /*hidden argument*/NULL);
	}

IL_06d7:
	{
		RenderTexture_t1963041563 * L_249 = V_15;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_249, /*hidden argument*/NULL);
	}

IL_06de:
	{
		int32_t L_250 = V_0;
		V_20 = L_250;
		Material_t3870600107 * L_251 = __this->get_screenBlend_31();
		float L_252 = __this->get_bloomIntensity_11();
		NullCheck(L_251);
		Material_SetFloat_m981710063(L_251, _stringLiteral1014379156, L_252, /*hidden argument*/NULL);
		Material_t3870600107 * L_253 = __this->get_screenBlend_31();
		RenderTexture_t1963041563 * L_254 = ___source0;
		NullCheck(L_253);
		Material_SetTexture_m1833724755(L_253, _stringLiteral1541889956, L_254, /*hidden argument*/NULL);
		int32_t L_255 = __this->get_quality_10();
		if ((((int32_t)L_255) <= ((int32_t)0)))
		{
			goto IL_0744;
		}
	}
	{
		int32_t L_256 = V_2;
		int32_t L_257 = V_3;
		int32_t L_258 = V_1;
		RenderTexture_t1963041563 * L_259 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, L_256, L_257, 0, L_258, /*hidden argument*/NULL);
		V_21 = L_259;
		RenderTexture_t1963041563 * L_260 = V_11;
		RenderTexture_t1963041563 * L_261 = V_21;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_260, L_261, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_262 = V_21;
		RenderTexture_t1963041563 * L_263 = ___destination1;
		Material_t3870600107 * L_264 = __this->get_screenBlend_31();
		int32_t L_265 = V_20;
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_262, L_263, L_264, L_265, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_266 = V_21;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_266, /*hidden argument*/NULL);
		goto IL_0754;
	}

IL_0744:
	{
		RenderTexture_t1963041563 * L_267 = V_11;
		RenderTexture_t1963041563 * L_268 = ___destination1;
		Material_t3870600107 * L_269 = __this->get_screenBlend_31();
		int32_t L_270 = V_20;
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_267, L_268, L_269, L_270, /*hidden argument*/NULL);
	}

IL_0754:
	{
		RenderTexture_t1963041563 * L_271 = V_8;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_271, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_272 = V_11;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_272, /*hidden argument*/NULL);
	}

IL_0762:
	{
		return;
	}
}
// System.Void Bloom::AddTo(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1014379156;
extern const uint32_t Bloom_AddTo_m3061610318_MetadataUsageId;
extern "C"  void Bloom_AddTo_m3061610318 (Bloom_t64280035 * __this, float ___intensity_0, RenderTexture_t1963041563 * ___from1, RenderTexture_t1963041563 * ___to2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Bloom_AddTo_m3061610318_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t3870600107 * L_0 = __this->get_screenBlend_31();
		float L_1 = ___intensity_0;
		NullCheck(L_0);
		Material_SetFloat_m981710063(L_0, _stringLiteral1014379156, L_1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_2 = ___to2;
		NullCheck(L_2);
		RenderTexture_MarkRestoreExpected_m2220245707(L_2, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_3 = ___from1;
		RenderTexture_t1963041563 * L_4 = ___to2;
		Material_t3870600107 * L_5 = __this->get_screenBlend_31();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_3, L_4, L_5, ((int32_t)9), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Bloom::BlendFlares(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral2940124478;
extern Il2CppCodeGenString* _stringLiteral2940124479;
extern Il2CppCodeGenString* _stringLiteral2940124480;
extern Il2CppCodeGenString* _stringLiteral2940124481;
extern const uint32_t Bloom_BlendFlares_m3656378831_MetadataUsageId;
extern "C"  void Bloom_BlendFlares_m3656378831 (Bloom_t64280035 * __this, RenderTexture_t1963041563 * ___from0, RenderTexture_t1963041563 * ___to1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Bloom_BlendFlares_m3656378831_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t3870600107 * L_0 = __this->get_lensFlareMaterial_29();
		Color_t4194546905 * L_1 = __this->get_address_of_flareColorA_22();
		float L_2 = L_1->get_r_0();
		Color_t4194546905 * L_3 = __this->get_address_of_flareColorA_22();
		float L_4 = L_3->get_g_1();
		Color_t4194546905 * L_5 = __this->get_address_of_flareColorA_22();
		float L_6 = L_5->get_b_2();
		Color_t4194546905 * L_7 = __this->get_address_of_flareColorA_22();
		float L_8 = L_7->get_a_3();
		Vector4_t4282066567  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector4__ctor_m2441427762(&L_9, L_2, L_4, L_6, L_8, /*hidden argument*/NULL);
		float L_10 = __this->get_lensflareIntensity_19();
		Vector4_t4282066567  L_11 = Vector4_op_Multiply_m209031836(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_0);
		Material_SetVector_m3505096203(L_0, _stringLiteral2940124478, L_11, /*hidden argument*/NULL);
		Material_t3870600107 * L_12 = __this->get_lensFlareMaterial_29();
		Color_t4194546905 * L_13 = __this->get_address_of_flareColorB_23();
		float L_14 = L_13->get_r_0();
		Color_t4194546905 * L_15 = __this->get_address_of_flareColorB_23();
		float L_16 = L_15->get_g_1();
		Color_t4194546905 * L_17 = __this->get_address_of_flareColorB_23();
		float L_18 = L_17->get_b_2();
		Color_t4194546905 * L_19 = __this->get_address_of_flareColorB_23();
		float L_20 = L_19->get_a_3();
		Vector4_t4282066567  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector4__ctor_m2441427762(&L_21, L_14, L_16, L_18, L_20, /*hidden argument*/NULL);
		float L_22 = __this->get_lensflareIntensity_19();
		Vector4_t4282066567  L_23 = Vector4_op_Multiply_m209031836(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_12);
		Material_SetVector_m3505096203(L_12, _stringLiteral2940124479, L_23, /*hidden argument*/NULL);
		Material_t3870600107 * L_24 = __this->get_lensFlareMaterial_29();
		Color_t4194546905 * L_25 = __this->get_address_of_flareColorC_24();
		float L_26 = L_25->get_r_0();
		Color_t4194546905 * L_27 = __this->get_address_of_flareColorC_24();
		float L_28 = L_27->get_g_1();
		Color_t4194546905 * L_29 = __this->get_address_of_flareColorC_24();
		float L_30 = L_29->get_b_2();
		Color_t4194546905 * L_31 = __this->get_address_of_flareColorC_24();
		float L_32 = L_31->get_a_3();
		Vector4_t4282066567  L_33;
		memset(&L_33, 0, sizeof(L_33));
		Vector4__ctor_m2441427762(&L_33, L_26, L_28, L_30, L_32, /*hidden argument*/NULL);
		float L_34 = __this->get_lensflareIntensity_19();
		Vector4_t4282066567  L_35 = Vector4_op_Multiply_m209031836(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		NullCheck(L_24);
		Material_SetVector_m3505096203(L_24, _stringLiteral2940124480, L_35, /*hidden argument*/NULL);
		Material_t3870600107 * L_36 = __this->get_lensFlareMaterial_29();
		Color_t4194546905 * L_37 = __this->get_address_of_flareColorD_25();
		float L_38 = L_37->get_r_0();
		Color_t4194546905 * L_39 = __this->get_address_of_flareColorD_25();
		float L_40 = L_39->get_g_1();
		Color_t4194546905 * L_41 = __this->get_address_of_flareColorD_25();
		float L_42 = L_41->get_b_2();
		Color_t4194546905 * L_43 = __this->get_address_of_flareColorD_25();
		float L_44 = L_43->get_a_3();
		Vector4_t4282066567  L_45;
		memset(&L_45, 0, sizeof(L_45));
		Vector4__ctor_m2441427762(&L_45, L_38, L_40, L_42, L_44, /*hidden argument*/NULL);
		float L_46 = __this->get_lensflareIntensity_19();
		Vector4_t4282066567  L_47 = Vector4_op_Multiply_m209031836(NULL /*static, unused*/, L_45, L_46, /*hidden argument*/NULL);
		NullCheck(L_36);
		Material_SetVector_m3505096203(L_36, _stringLiteral2940124481, L_47, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_48 = ___to1;
		NullCheck(L_48);
		RenderTexture_MarkRestoreExpected_m2220245707(L_48, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_49 = ___from0;
		RenderTexture_t1963041563 * L_50 = ___to1;
		Material_t3870600107 * L_51 = __this->get_lensFlareMaterial_29();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_49, L_50, L_51, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Bloom::BrightFilter(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral2416425146;
extern const uint32_t Bloom_BrightFilter_m174238832_MetadataUsageId;
extern "C"  void Bloom_BrightFilter_m174238832 (Bloom_t64280035 * __this, float ___thresh0, RenderTexture_t1963041563 * ___from1, RenderTexture_t1963041563 * ___to2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Bloom_BrightFilter_m174238832_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t3870600107 * L_0 = __this->get_brightPassFilterMaterial_35();
		float L_1 = ___thresh0;
		float L_2 = ___thresh0;
		float L_3 = ___thresh0;
		float L_4 = ___thresh0;
		Vector4_t4282066567  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector4__ctor_m2441427762(&L_5, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		Material_SetVector_m3505096203(L_0, _stringLiteral2416425146, L_5, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_6 = ___from1;
		RenderTexture_t1963041563 * L_7 = ___to2;
		Material_t3870600107 * L_8 = __this->get_brightPassFilterMaterial_35();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_6, L_7, L_8, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Bloom::BrightFilter(UnityEngine.Color,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral2416425146;
extern const uint32_t Bloom_BrightFilter_m3623342315_MetadataUsageId;
extern "C"  void Bloom_BrightFilter_m3623342315 (Bloom_t64280035 * __this, Color_t4194546905  ___threshColor0, RenderTexture_t1963041563 * ___from1, RenderTexture_t1963041563 * ___to2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Bloom_BrightFilter_m3623342315_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t3870600107 * L_0 = __this->get_brightPassFilterMaterial_35();
		Color_t4194546905  L_1 = ___threshColor0;
		Vector4_t4282066567  L_2 = Color_op_Implicit_m2638307542(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Material_SetVector_m3505096203(L_0, _stringLiteral2416425146, L_2, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_3 = ___from1;
		RenderTexture_t1963041563 * L_4 = ___to2;
		Material_t3870600107 * L_5 = __this->get_brightPassFilterMaterial_35();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_3, L_4, L_5, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Bloom::Vignette(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1541889956;
extern const uint32_t Bloom_Vignette_m1189655368_MetadataUsageId;
extern "C"  void Bloom_Vignette_m1189655368 (Bloom_t64280035 * __this, float ___amount0, RenderTexture_t1963041563 * ___from1, RenderTexture_t1963041563 * ___to2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Bloom_Vignette_m1189655368_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RenderTexture_t1963041563 * G_B4_0 = NULL;
	Material_t3870600107 * G_B6_0 = NULL;
	RenderTexture_t1963041563 * G_B6_1 = NULL;
	RenderTexture_t1963041563 * G_B6_2 = NULL;
	Material_t3870600107 * G_B5_0 = NULL;
	RenderTexture_t1963041563 * G_B5_1 = NULL;
	RenderTexture_t1963041563 * G_B5_2 = NULL;
	int32_t G_B7_0 = 0;
	Material_t3870600107 * G_B7_1 = NULL;
	RenderTexture_t1963041563 * G_B7_2 = NULL;
	RenderTexture_t1963041563 * G_B7_3 = NULL;
	{
		Texture2D_t3884108195 * L_0 = __this->get_lensFlareVignetteMask_27();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0063;
		}
	}
	{
		Material_t3870600107 * L_2 = __this->get_screenBlend_31();
		Texture2D_t3884108195 * L_3 = __this->get_lensFlareVignetteMask_27();
		NullCheck(L_2);
		Material_SetTexture_m1833724755(L_2, _stringLiteral1541889956, L_3, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_4 = ___to2;
		NullCheck(L_4);
		RenderTexture_MarkRestoreExpected_m2220245707(L_4, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_5 = ___from1;
		RenderTexture_t1963041563 * L_6 = ___to2;
		bool L_7 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003e;
		}
	}
	{
		G_B4_0 = ((RenderTexture_t1963041563 *)(NULL));
		goto IL_003f;
	}

IL_003e:
	{
		RenderTexture_t1963041563 * L_8 = ___from1;
		G_B4_0 = L_8;
	}

IL_003f:
	{
		RenderTexture_t1963041563 * L_9 = ___to2;
		Material_t3870600107 * L_10 = __this->get_screenBlend_31();
		RenderTexture_t1963041563 * L_11 = ___from1;
		RenderTexture_t1963041563 * L_12 = ___to2;
		bool L_13 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		G_B5_0 = L_10;
		G_B5_1 = L_9;
		G_B5_2 = G_B4_0;
		if (!L_13)
		{
			G_B6_0 = L_10;
			G_B6_1 = L_9;
			G_B6_2 = G_B4_0;
			goto IL_0058;
		}
	}
	{
		G_B7_0 = 7;
		G_B7_1 = G_B5_0;
		G_B7_2 = G_B5_1;
		G_B7_3 = G_B5_2;
		goto IL_0059;
	}

IL_0058:
	{
		G_B7_0 = 3;
		G_B7_1 = G_B6_0;
		G_B7_2 = G_B6_1;
		G_B7_3 = G_B6_2;
	}

IL_0059:
	{
		Graphics_Blit_m336256356(NULL /*static, unused*/, G_B7_3, G_B7_2, G_B7_1, G_B7_0, /*hidden argument*/NULL);
		goto IL_0088;
	}

IL_0063:
	{
		RenderTexture_t1963041563 * L_14 = ___from1;
		RenderTexture_t1963041563 * L_15 = ___to2;
		bool L_16 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0088;
		}
	}
	{
		RenderTexture_t1963041563 * L_17 = ___to2;
		Graphics_SetRenderTarget_m3051614107(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		Color_t4194546905  L_18 = Color_get_black_m1687201969(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_Clear_m2980983731(NULL /*static, unused*/, (bool)0, (bool)1, L_18, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_19 = ___from1;
		RenderTexture_t1963041563 * L_20 = ___to2;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
	}

IL_0088:
	{
		return;
	}
}
// System.Void Bloom::Main()
extern "C"  void Bloom_Main_m3020296894 (Bloom_t64280035 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void BloomAndLensFlares::.ctor()
extern "C"  void BloomAndLensFlares__ctor_m2439393165 (BloomAndLensFlares_t2492782871 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase__ctor_m3869393199(__this, /*hidden argument*/NULL);
		__this->set_screenBlendMode_6(1);
		__this->set_hdr_7(0);
		__this->set_sepBlurSpread_9((1.5f));
		__this->set_useSrcAlphaAsMask_10((0.5f));
		__this->set_bloomIntensity_11((1.0f));
		__this->set_bloomThreshhold_12((0.5f));
		__this->set_bloomBlurIterations_13(2);
		__this->set_hollywoodFlareBlurIterations_15(2);
		__this->set_lensflareMode_16(1);
		__this->set_hollyStretchWidth_17((3.5f));
		__this->set_lensflareIntensity_18((1.0f));
		__this->set_lensflareThreshhold_19((0.3f));
		Color_t4194546905  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2252924356(&L_0, (0.4f), (0.4f), (0.8f), (0.75f), /*hidden argument*/NULL);
		__this->set_flareColorA_20(L_0);
		Color_t4194546905  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Color__ctor_m2252924356(&L_1, (0.4f), (0.8f), (0.8f), (0.75f), /*hidden argument*/NULL);
		__this->set_flareColorB_21(L_1);
		Color_t4194546905  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Color__ctor_m2252924356(&L_2, (0.8f), (0.4f), (0.8f), (0.75f), /*hidden argument*/NULL);
		__this->set_flareColorC_22(L_2);
		Color_t4194546905  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Color__ctor_m2252924356(&L_3, (0.8f), (0.4f), (((float)((float)0))), (0.75f), /*hidden argument*/NULL);
		__this->set_flareColorD_23(L_3);
		__this->set_blurWidth_24((1.0f));
		return;
	}
}
// System.Boolean BloomAndLensFlares::CheckResources()
extern "C"  bool BloomAndLensFlares_CheckResources_m2156746938 (BloomAndLensFlares_t2492782871 * __this, const MethodInfo* method)
{
	{
		VirtFuncInvoker1< bool, bool >::Invoke(10 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean) */, __this, (bool)0);
		Shader_t3191267369 * L_0 = __this->get_screenBlendShader_34();
		Material_t3870600107 * L_1 = __this->get_screenBlend_35();
		Material_t3870600107 * L_2 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_0, L_1);
		__this->set_screenBlend_35(L_2);
		Shader_t3191267369 * L_3 = __this->get_lensFlareShader_26();
		Material_t3870600107 * L_4 = __this->get_lensFlareMaterial_27();
		Material_t3870600107 * L_5 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_3, L_4);
		__this->set_lensFlareMaterial_27(L_5);
		Shader_t3191267369 * L_6 = __this->get_vignetteShader_28();
		Material_t3870600107 * L_7 = __this->get_vignetteMaterial_29();
		Material_t3870600107 * L_8 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_6, L_7);
		__this->set_vignetteMaterial_29(L_8);
		Shader_t3191267369 * L_9 = __this->get_separableBlurShader_30();
		Material_t3870600107 * L_10 = __this->get_separableBlurMaterial_31();
		Material_t3870600107 * L_11 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_9, L_10);
		__this->set_separableBlurMaterial_31(L_11);
		Shader_t3191267369 * L_12 = __this->get_addBrightStuffOneOneShader_32();
		Material_t3870600107 * L_13 = __this->get_addBrightStuffBlendOneOneMaterial_33();
		Material_t3870600107 * L_14 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_12, L_13);
		__this->set_addBrightStuffBlendOneOneMaterial_33(L_14);
		Shader_t3191267369 * L_15 = __this->get_hollywoodFlaresShader_36();
		Material_t3870600107 * L_16 = __this->get_hollywoodFlaresMaterial_37();
		Material_t3870600107 * L_17 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_15, L_16);
		__this->set_hollywoodFlaresMaterial_37(L_17);
		Shader_t3191267369 * L_18 = __this->get_brightPassFilterShader_38();
		Material_t3870600107 * L_19 = __this->get_brightPassFilterMaterial_39();
		Material_t3870600107 * L_20 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_18, L_19);
		__this->set_brightPassFilterMaterial_39(L_20);
		bool L_21 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		if (L_21)
		{
			goto IL_00c1;
		}
	}
	{
		VirtActionInvoker0::Invoke(13 /* System.Void PostEffectsBase::ReportAutoDisable() */, __this);
	}

IL_00c1:
	{
		bool L_22 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		return L_22;
	}
}
// System.Void BloomAndLensFlares::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo* Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2746560064;
extern Il2CppCodeGenString* _stringLiteral2416425146;
extern Il2CppCodeGenString* _stringLiteral1327599912;
extern Il2CppCodeGenString* _stringLiteral1906766273;
extern Il2CppCodeGenString* _stringLiteral1014379156;
extern Il2CppCodeGenString* _stringLiteral1541889956;
extern const uint32_t BloomAndLensFlares_OnRenderImage_m1144811697_MetadataUsageId;
extern "C"  void BloomAndLensFlares_OnRenderImage_m1144811697 (BloomAndLensFlares_t2492782871 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BloomAndLensFlares_OnRenderImage_m1144811697_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RenderTexture_t1963041563 * V_2 = NULL;
	RenderTexture_t1963041563 * V_3 = NULL;
	RenderTexture_t1963041563 * V_4 = NULL;
	RenderTexture_t1963041563 * V_5 = NULL;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	int32_t V_8 = 0;
	float V_9 = 0.0f;
	RenderTexture_t1963041563 * V_10 = NULL;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t G_B5_0 = 0;
	BloomAndLensFlares_t2492782871 * G_B5_1 = NULL;
	int32_t G_B4_0 = 0;
	BloomAndLensFlares_t2492782871 * G_B4_1 = NULL;
	bool G_B9_0 = false;
	BloomAndLensFlares_t2492782871 * G_B9_1 = NULL;
	bool G_B8_0 = false;
	BloomAndLensFlares_t2492782871 * G_B8_1 = NULL;
	int32_t G_B14_0 = 0;
	RenderTexture_t1963041563 * G_B20_0 = NULL;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean BloomAndLensFlares::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		RenderTexture_t1963041563 * L_1 = ___source0;
		RenderTexture_t1963041563 * L_2 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_05e7;
	}

IL_0017:
	{
		__this->set_doHdr_8((bool)0);
		int32_t L_3 = __this->get_hdr_7();
		if ((!(((uint32_t)L_3) == ((uint32_t)0))))
		{
			goto IL_0050;
		}
	}
	{
		RenderTexture_t1963041563 * L_4 = ___source0;
		NullCheck(L_4);
		int32_t L_5 = RenderTexture_get_format_m3502109954(L_4, /*hidden argument*/NULL);
		int32_t L_6 = ((((int32_t)L_5) == ((int32_t)2))? 1 : 0);
		G_B4_0 = L_6;
		G_B4_1 = __this;
		if (!L_6)
		{
			G_B5_0 = L_6;
			G_B5_1 = __this;
			goto IL_0046;
		}
	}
	{
		Camera_t2727095145 * L_7 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_7);
		bool L_8 = Camera_get_hdr_m1509362829(L_7, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_8));
		G_B5_1 = G_B4_1;
	}

IL_0046:
	{
		NullCheck(G_B5_1);
		G_B5_1->set_doHdr_8((bool)G_B5_0);
		goto IL_005f;
	}

IL_0050:
	{
		int32_t L_9 = __this->get_hdr_7();
		__this->set_doHdr_8((bool)((((int32_t)L_9) == ((int32_t)1))? 1 : 0));
	}

IL_005f:
	{
		bool L_10 = __this->get_doHdr_8();
		bool L_11 = L_10;
		G_B8_0 = L_11;
		G_B8_1 = __this;
		if (!L_11)
		{
			G_B9_0 = L_11;
			G_B9_1 = __this;
			goto IL_0073;
		}
	}
	{
		bool L_12 = ((PostEffectsBase_t1820837395 *)__this)->get_supportHDRTextures_2();
		G_B9_0 = L_12;
		G_B9_1 = G_B8_1;
	}

IL_0073:
	{
		NullCheck(G_B9_1);
		G_B9_1->set_doHdr_8(G_B9_0);
		int32_t L_13 = __this->get_screenBlendMode_6();
		V_0 = L_13;
		bool L_14 = __this->get_doHdr_8();
		if (!L_14)
		{
			goto IL_008c;
		}
	}
	{
		V_0 = 1;
	}

IL_008c:
	{
		bool L_15 = __this->get_doHdr_8();
		if (!L_15)
		{
			goto IL_009d;
		}
	}
	{
		G_B14_0 = 2;
		goto IL_009e;
	}

IL_009d:
	{
		G_B14_0 = 7;
	}

IL_009e:
	{
		V_1 = G_B14_0;
		RenderTexture_t1963041563 * L_16 = ___source0;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m1498578543(L_16, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_18 = ___source0;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m4010076224(L_18, /*hidden argument*/NULL);
		int32_t L_20 = V_1;
		RenderTexture_t1963041563 * L_21 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, ((int32_t)((int32_t)L_17/(int32_t)2)), ((int32_t)((int32_t)L_19/(int32_t)2)), 0, L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		RenderTexture_t1963041563 * L_22 = ___source0;
		NullCheck(L_22);
		int32_t L_23 = RenderTexture_get_width_m1498578543(L_22, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_24 = ___source0;
		NullCheck(L_24);
		int32_t L_25 = RenderTexture_get_height_m4010076224(L_24, /*hidden argument*/NULL);
		int32_t L_26 = V_1;
		RenderTexture_t1963041563 * L_27 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, ((int32_t)((int32_t)L_23/(int32_t)4)), ((int32_t)((int32_t)L_25/(int32_t)4)), 0, L_26, /*hidden argument*/NULL);
		V_3 = L_27;
		RenderTexture_t1963041563 * L_28 = ___source0;
		NullCheck(L_28);
		int32_t L_29 = RenderTexture_get_width_m1498578543(L_28, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_30 = ___source0;
		NullCheck(L_30);
		int32_t L_31 = RenderTexture_get_height_m4010076224(L_30, /*hidden argument*/NULL);
		int32_t L_32 = V_1;
		RenderTexture_t1963041563 * L_33 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, ((int32_t)((int32_t)L_29/(int32_t)4)), ((int32_t)((int32_t)L_31/(int32_t)4)), 0, L_32, /*hidden argument*/NULL);
		V_4 = L_33;
		RenderTexture_t1963041563 * L_34 = ___source0;
		NullCheck(L_34);
		int32_t L_35 = RenderTexture_get_width_m1498578543(L_34, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_36 = ___source0;
		NullCheck(L_36);
		int32_t L_37 = RenderTexture_get_height_m4010076224(L_36, /*hidden argument*/NULL);
		int32_t L_38 = V_1;
		RenderTexture_t1963041563 * L_39 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, ((int32_t)((int32_t)L_35/(int32_t)4)), ((int32_t)((int32_t)L_37/(int32_t)4)), 0, L_38, /*hidden argument*/NULL);
		V_5 = L_39;
		RenderTexture_t1963041563 * L_40 = ___source0;
		NullCheck(L_40);
		int32_t L_41 = RenderTexture_get_width_m1498578543(L_40, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_42 = ___source0;
		NullCheck(L_42);
		int32_t L_43 = RenderTexture_get_height_m4010076224(L_42, /*hidden argument*/NULL);
		V_6 = ((float)((float)((float)((float)(1.0f)*(float)(((float)((float)L_41)))))/(float)((float)((float)(1.0f)*(float)(((float)((float)L_43)))))));
		V_7 = (0.001953125f);
		RenderTexture_t1963041563 * L_44 = ___source0;
		RenderTexture_t1963041563 * L_45 = V_2;
		Material_t3870600107 * L_46 = __this->get_screenBlend_35();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_44, L_45, L_46, 2, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_47 = V_2;
		RenderTexture_t1963041563 * L_48 = V_3;
		Material_t3870600107 * L_49 = __this->get_screenBlend_35();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_47, L_48, L_49, 2, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_50 = V_2;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		float L_51 = __this->get_bloomThreshhold_12();
		float L_52 = __this->get_useSrcAlphaAsMask_10();
		RenderTexture_t1963041563 * L_53 = V_3;
		RenderTexture_t1963041563 * L_54 = V_4;
		BloomAndLensFlares_BrightFilter_m3496269507(__this, L_51, L_52, L_53, L_54, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_55 = V_3;
		NullCheck(L_55);
		RenderTexture_DiscardContents_m1898044554(L_55, /*hidden argument*/NULL);
		int32_t L_56 = __this->get_bloomBlurIterations_13();
		if ((((int32_t)L_56) >= ((int32_t)1)))
		{
			goto IL_0175;
		}
	}
	{
		__this->set_bloomBlurIterations_13(1);
	}

IL_0175:
	{
		V_8 = 0;
		goto IL_021a;
	}

IL_017d:
	{
		int32_t L_57 = V_8;
		float L_58 = __this->get_sepBlurSpread_9();
		V_9 = ((float)((float)((float)((float)(1.0f)+(float)((float)((float)(((float)((float)L_57)))*(float)(0.5f)))))*(float)L_58));
		Material_t3870600107 * L_59 = __this->get_separableBlurMaterial_31();
		float L_60 = V_9;
		float L_61 = V_7;
		Vector4_t4282066567  L_62;
		memset(&L_62, 0, sizeof(L_62));
		Vector4__ctor_m2441427762(&L_62, (((float)((float)0))), ((float)((float)L_60*(float)L_61)), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_59);
		Material_SetVector_m3505096203(L_59, _stringLiteral2746560064, L_62, /*hidden argument*/NULL);
		int32_t L_63 = V_8;
		if (L_63)
		{
			goto IL_01c3;
		}
	}
	{
		RenderTexture_t1963041563 * L_64 = V_4;
		G_B20_0 = L_64;
		goto IL_01c4;
	}

IL_01c3:
	{
		RenderTexture_t1963041563 * L_65 = V_3;
		G_B20_0 = L_65;
	}

IL_01c4:
	{
		V_10 = G_B20_0;
		RenderTexture_t1963041563 * L_66 = V_10;
		RenderTexture_t1963041563 * L_67 = V_5;
		Material_t3870600107 * L_68 = __this->get_separableBlurMaterial_31();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_66, L_67, L_68, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_69 = V_10;
		NullCheck(L_69);
		RenderTexture_DiscardContents_m1898044554(L_69, /*hidden argument*/NULL);
		Material_t3870600107 * L_70 = __this->get_separableBlurMaterial_31();
		float L_71 = V_9;
		float L_72 = V_6;
		float L_73 = V_7;
		Vector4_t4282066567  L_74;
		memset(&L_74, 0, sizeof(L_74));
		Vector4__ctor_m2441427762(&L_74, ((float)((float)((float)((float)L_71/(float)L_72))*(float)L_73)), (((float)((float)0))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_70);
		Material_SetVector_m3505096203(L_70, _stringLiteral2746560064, L_74, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_75 = V_5;
		RenderTexture_t1963041563 * L_76 = V_3;
		Material_t3870600107 * L_77 = __this->get_separableBlurMaterial_31();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_75, L_76, L_77, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_78 = V_5;
		NullCheck(L_78);
		RenderTexture_DiscardContents_m1898044554(L_78, /*hidden argument*/NULL);
		int32_t L_79 = V_8;
		V_8 = ((int32_t)((int32_t)L_79+(int32_t)1));
	}

IL_021a:
	{
		int32_t L_80 = V_8;
		int32_t L_81 = __this->get_bloomBlurIterations_13();
		if ((((int32_t)L_80) < ((int32_t)L_81)))
		{
			goto IL_017d;
		}
	}
	{
		bool L_82 = __this->get_lensflares_14();
		if (!L_82)
		{
			goto IL_059e;
		}
	}
	{
		int32_t L_83 = __this->get_lensflareMode_16();
		if (L_83)
		{
			goto IL_027f;
		}
	}
	{
		float L_84 = __this->get_lensflareThreshhold_19();
		RenderTexture_t1963041563 * L_85 = V_3;
		RenderTexture_t1963041563 * L_86 = V_5;
		BloomAndLensFlares_BrightFilter_m3496269507(__this, L_84, (((float)((float)0))), L_85, L_86, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_87 = V_3;
		NullCheck(L_87);
		RenderTexture_DiscardContents_m1898044554(L_87, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_88 = V_5;
		RenderTexture_t1963041563 * L_89 = V_4;
		BloomAndLensFlares_Vignette_m1092400566(__this, (0.975f), L_88, L_89, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_90 = V_5;
		NullCheck(L_90);
		RenderTexture_DiscardContents_m1898044554(L_90, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_91 = V_4;
		RenderTexture_t1963041563 * L_92 = V_3;
		BloomAndLensFlares_BlendFlares_m610607137(__this, L_91, L_92, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_93 = V_4;
		NullCheck(L_93);
		RenderTexture_DiscardContents_m1898044554(L_93, /*hidden argument*/NULL);
		goto IL_059e;
	}

IL_027f:
	{
		Material_t3870600107 * L_94 = __this->get_hollywoodFlaresMaterial_37();
		float L_95 = __this->get_lensflareThreshhold_19();
		float L_96 = __this->get_lensflareThreshhold_19();
		Vector4_t4282066567  L_97;
		memset(&L_97, 0, sizeof(L_97));
		Vector4__ctor_m2441427762(&L_97, L_95, ((float)((float)(1.0f)/(float)((float)((float)(1.0f)-(float)L_96)))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_94);
		Material_SetVector_m3505096203(L_94, _stringLiteral2416425146, L_97, /*hidden argument*/NULL);
		Material_t3870600107 * L_98 = __this->get_hollywoodFlaresMaterial_37();
		Color_t4194546905 * L_99 = __this->get_address_of_flareColorA_20();
		float L_100 = L_99->get_r_0();
		Color_t4194546905 * L_101 = __this->get_address_of_flareColorA_20();
		float L_102 = L_101->get_g_1();
		Color_t4194546905 * L_103 = __this->get_address_of_flareColorA_20();
		float L_104 = L_103->get_b_2();
		Color_t4194546905 * L_105 = __this->get_address_of_flareColorA_20();
		float L_106 = L_105->get_a_3();
		Vector4_t4282066567  L_107;
		memset(&L_107, 0, sizeof(L_107));
		Vector4__ctor_m2441427762(&L_107, L_100, L_102, L_104, L_106, /*hidden argument*/NULL);
		Color_t4194546905 * L_108 = __this->get_address_of_flareColorA_20();
		float L_109 = L_108->get_a_3();
		Vector4_t4282066567  L_110 = Vector4_op_Multiply_m209031836(NULL /*static, unused*/, L_107, L_109, /*hidden argument*/NULL);
		float L_111 = __this->get_lensflareIntensity_18();
		Vector4_t4282066567  L_112 = Vector4_op_Multiply_m209031836(NULL /*static, unused*/, L_110, L_111, /*hidden argument*/NULL);
		NullCheck(L_98);
		Material_SetVector_m3505096203(L_98, _stringLiteral1327599912, L_112, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_113 = V_5;
		RenderTexture_t1963041563 * L_114 = V_4;
		Material_t3870600107 * L_115 = __this->get_hollywoodFlaresMaterial_37();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_113, L_114, L_115, 2, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_116 = V_5;
		NullCheck(L_116);
		RenderTexture_DiscardContents_m1898044554(L_116, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_117 = V_4;
		RenderTexture_t1963041563 * L_118 = V_5;
		Material_t3870600107 * L_119 = __this->get_hollywoodFlaresMaterial_37();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_117, L_118, L_119, 3, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_120 = V_4;
		NullCheck(L_120);
		RenderTexture_DiscardContents_m1898044554(L_120, /*hidden argument*/NULL);
		Material_t3870600107 * L_121 = __this->get_hollywoodFlaresMaterial_37();
		float L_122 = __this->get_sepBlurSpread_9();
		float L_123 = V_6;
		float L_124 = V_7;
		Vector4_t4282066567  L_125;
		memset(&L_125, 0, sizeof(L_125));
		Vector4__ctor_m2441427762(&L_125, ((float)((float)((float)((float)((float)((float)L_122*(float)(1.0f)))/(float)L_123))*(float)L_124)), (((float)((float)0))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_121);
		Material_SetVector_m3505096203(L_121, _stringLiteral2746560064, L_125, /*hidden argument*/NULL);
		Material_t3870600107 * L_126 = __this->get_hollywoodFlaresMaterial_37();
		float L_127 = __this->get_hollyStretchWidth_17();
		NullCheck(L_126);
		Material_SetFloat_m981710063(L_126, _stringLiteral1906766273, L_127, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_128 = V_5;
		RenderTexture_t1963041563 * L_129 = V_4;
		Material_t3870600107 * L_130 = __this->get_hollywoodFlaresMaterial_37();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_128, L_129, L_130, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_131 = V_5;
		NullCheck(L_131);
		RenderTexture_DiscardContents_m1898044554(L_131, /*hidden argument*/NULL);
		Material_t3870600107 * L_132 = __this->get_hollywoodFlaresMaterial_37();
		float L_133 = __this->get_hollyStretchWidth_17();
		NullCheck(L_132);
		Material_SetFloat_m981710063(L_132, _stringLiteral1906766273, ((float)((float)L_133*(float)(2.0f))), /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_134 = V_4;
		RenderTexture_t1963041563 * L_135 = V_5;
		Material_t3870600107 * L_136 = __this->get_hollywoodFlaresMaterial_37();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_134, L_135, L_136, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_137 = V_4;
		NullCheck(L_137);
		RenderTexture_DiscardContents_m1898044554(L_137, /*hidden argument*/NULL);
		Material_t3870600107 * L_138 = __this->get_hollywoodFlaresMaterial_37();
		float L_139 = __this->get_hollyStretchWidth_17();
		NullCheck(L_138);
		Material_SetFloat_m981710063(L_138, _stringLiteral1906766273, ((float)((float)L_139*(float)(4.0f))), /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_140 = V_5;
		RenderTexture_t1963041563 * L_141 = V_4;
		Material_t3870600107 * L_142 = __this->get_hollywoodFlaresMaterial_37();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_140, L_141, L_142, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_143 = V_5;
		NullCheck(L_143);
		RenderTexture_DiscardContents_m1898044554(L_143, /*hidden argument*/NULL);
		int32_t L_144 = __this->get_lensflareMode_16();
		if ((!(((uint32_t)L_144) == ((uint32_t)1))))
		{
			goto IL_04c1;
		}
	}
	{
		V_11 = 0;
		goto IL_049a;
	}

IL_040e:
	{
		Material_t3870600107 * L_145 = __this->get_separableBlurMaterial_31();
		float L_146 = __this->get_hollyStretchWidth_17();
		float L_147 = V_6;
		float L_148 = V_7;
		Vector4_t4282066567  L_149;
		memset(&L_149, 0, sizeof(L_149));
		Vector4__ctor_m2441427762(&L_149, ((float)((float)((float)((float)((float)((float)L_146*(float)(2.0f)))/(float)L_147))*(float)L_148)), (((float)((float)0))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_145);
		Material_SetVector_m3505096203(L_145, _stringLiteral2746560064, L_149, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_150 = V_4;
		RenderTexture_t1963041563 * L_151 = V_5;
		Material_t3870600107 * L_152 = __this->get_separableBlurMaterial_31();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_150, L_151, L_152, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_153 = V_4;
		NullCheck(L_153);
		RenderTexture_DiscardContents_m1898044554(L_153, /*hidden argument*/NULL);
		Material_t3870600107 * L_154 = __this->get_separableBlurMaterial_31();
		float L_155 = __this->get_hollyStretchWidth_17();
		float L_156 = V_6;
		float L_157 = V_7;
		Vector4_t4282066567  L_158;
		memset(&L_158, 0, sizeof(L_158));
		Vector4__ctor_m2441427762(&L_158, ((float)((float)((float)((float)((float)((float)L_155*(float)(2.0f)))/(float)L_156))*(float)L_157)), (((float)((float)0))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_154);
		Material_SetVector_m3505096203(L_154, _stringLiteral2746560064, L_158, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_159 = V_5;
		RenderTexture_t1963041563 * L_160 = V_4;
		Material_t3870600107 * L_161 = __this->get_separableBlurMaterial_31();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_159, L_160, L_161, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_162 = V_5;
		NullCheck(L_162);
		RenderTexture_DiscardContents_m1898044554(L_162, /*hidden argument*/NULL);
		int32_t L_163 = V_11;
		V_11 = ((int32_t)((int32_t)L_163+(int32_t)1));
	}

IL_049a:
	{
		int32_t L_164 = V_11;
		int32_t L_165 = __this->get_hollywoodFlareBlurIterations_15();
		if ((((int32_t)L_164) < ((int32_t)L_165)))
		{
			goto IL_040e;
		}
	}
	{
		RenderTexture_t1963041563 * L_166 = V_4;
		RenderTexture_t1963041563 * L_167 = V_3;
		BloomAndLensFlares_AddTo_m2840017824(__this, (1.0f), L_166, L_167, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_168 = V_4;
		NullCheck(L_168);
		RenderTexture_DiscardContents_m1898044554(L_168, /*hidden argument*/NULL);
		goto IL_059e;
	}

IL_04c1:
	{
		V_12 = 0;
		goto IL_0555;
	}

IL_04c9:
	{
		Material_t3870600107 * L_169 = __this->get_separableBlurMaterial_31();
		float L_170 = __this->get_hollyStretchWidth_17();
		float L_171 = V_6;
		float L_172 = V_7;
		Vector4_t4282066567  L_173;
		memset(&L_173, 0, sizeof(L_173));
		Vector4__ctor_m2441427762(&L_173, ((float)((float)((float)((float)((float)((float)L_170*(float)(2.0f)))/(float)L_171))*(float)L_172)), (((float)((float)0))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_169);
		Material_SetVector_m3505096203(L_169, _stringLiteral2746560064, L_173, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_174 = V_4;
		RenderTexture_t1963041563 * L_175 = V_5;
		Material_t3870600107 * L_176 = __this->get_separableBlurMaterial_31();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_174, L_175, L_176, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_177 = V_4;
		NullCheck(L_177);
		RenderTexture_DiscardContents_m1898044554(L_177, /*hidden argument*/NULL);
		Material_t3870600107 * L_178 = __this->get_separableBlurMaterial_31();
		float L_179 = __this->get_hollyStretchWidth_17();
		float L_180 = V_6;
		float L_181 = V_7;
		Vector4_t4282066567  L_182;
		memset(&L_182, 0, sizeof(L_182));
		Vector4__ctor_m2441427762(&L_182, ((float)((float)((float)((float)((float)((float)L_179*(float)(2.0f)))/(float)L_180))*(float)L_181)), (((float)((float)0))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_178);
		Material_SetVector_m3505096203(L_178, _stringLiteral2746560064, L_182, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_183 = V_5;
		RenderTexture_t1963041563 * L_184 = V_4;
		Material_t3870600107 * L_185 = __this->get_separableBlurMaterial_31();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_183, L_184, L_185, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_186 = V_5;
		NullCheck(L_186);
		RenderTexture_DiscardContents_m1898044554(L_186, /*hidden argument*/NULL);
		int32_t L_187 = V_12;
		V_12 = ((int32_t)((int32_t)L_187+(int32_t)1));
	}

IL_0555:
	{
		int32_t L_188 = V_12;
		int32_t L_189 = __this->get_hollywoodFlareBlurIterations_15();
		if ((((int32_t)L_188) < ((int32_t)L_189)))
		{
			goto IL_04c9;
		}
	}
	{
		RenderTexture_t1963041563 * L_190 = V_4;
		RenderTexture_t1963041563 * L_191 = V_5;
		BloomAndLensFlares_Vignette_m1092400566(__this, (1.0f), L_190, L_191, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_192 = V_4;
		NullCheck(L_192);
		RenderTexture_DiscardContents_m1898044554(L_192, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_193 = V_5;
		RenderTexture_t1963041563 * L_194 = V_4;
		BloomAndLensFlares_BlendFlares_m610607137(__this, L_193, L_194, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_195 = V_5;
		NullCheck(L_195);
		RenderTexture_DiscardContents_m1898044554(L_195, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_196 = V_4;
		RenderTexture_t1963041563 * L_197 = V_3;
		BloomAndLensFlares_AddTo_m2840017824(__this, (1.0f), L_196, L_197, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_198 = V_4;
		NullCheck(L_198);
		RenderTexture_DiscardContents_m1898044554(L_198, /*hidden argument*/NULL);
	}

IL_059e:
	{
		Material_t3870600107 * L_199 = __this->get_screenBlend_35();
		float L_200 = __this->get_bloomIntensity_11();
		NullCheck(L_199);
		Material_SetFloat_m981710063(L_199, _stringLiteral1014379156, L_200, /*hidden argument*/NULL);
		Material_t3870600107 * L_201 = __this->get_screenBlend_35();
		RenderTexture_t1963041563 * L_202 = ___source0;
		NullCheck(L_201);
		Material_SetTexture_m1833724755(L_201, _stringLiteral1541889956, L_202, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_203 = V_3;
		RenderTexture_t1963041563 * L_204 = ___destination1;
		Material_t3870600107 * L_205 = __this->get_screenBlend_35();
		int32_t L_206 = V_0;
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_203, L_204, L_205, L_206, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_207 = V_3;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_207, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_208 = V_4;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_208, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_209 = V_5;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_209, /*hidden argument*/NULL);
	}

IL_05e7:
	{
		return;
	}
}
// System.Void BloomAndLensFlares::AddTo(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1014379156;
extern const uint32_t BloomAndLensFlares_AddTo_m2840017824_MetadataUsageId;
extern "C"  void BloomAndLensFlares_AddTo_m2840017824 (BloomAndLensFlares_t2492782871 * __this, float ___intensity_0, RenderTexture_t1963041563 * ___from1, RenderTexture_t1963041563 * ___to2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BloomAndLensFlares_AddTo_m2840017824_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t3870600107 * L_0 = __this->get_addBrightStuffBlendOneOneMaterial_33();
		float L_1 = ___intensity_0;
		NullCheck(L_0);
		Material_SetFloat_m981710063(L_0, _stringLiteral1014379156, L_1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_2 = ___from1;
		RenderTexture_t1963041563 * L_3 = ___to2;
		Material_t3870600107 * L_4 = __this->get_addBrightStuffBlendOneOneMaterial_33();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BloomAndLensFlares::BlendFlares(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral2940124478;
extern Il2CppCodeGenString* _stringLiteral2940124479;
extern Il2CppCodeGenString* _stringLiteral2940124480;
extern Il2CppCodeGenString* _stringLiteral2940124481;
extern const uint32_t BloomAndLensFlares_BlendFlares_m610607137_MetadataUsageId;
extern "C"  void BloomAndLensFlares_BlendFlares_m610607137 (BloomAndLensFlares_t2492782871 * __this, RenderTexture_t1963041563 * ___from0, RenderTexture_t1963041563 * ___to1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BloomAndLensFlares_BlendFlares_m610607137_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t3870600107 * L_0 = __this->get_lensFlareMaterial_27();
		Color_t4194546905 * L_1 = __this->get_address_of_flareColorA_20();
		float L_2 = L_1->get_r_0();
		Color_t4194546905 * L_3 = __this->get_address_of_flareColorA_20();
		float L_4 = L_3->get_g_1();
		Color_t4194546905 * L_5 = __this->get_address_of_flareColorA_20();
		float L_6 = L_5->get_b_2();
		Color_t4194546905 * L_7 = __this->get_address_of_flareColorA_20();
		float L_8 = L_7->get_a_3();
		Vector4_t4282066567  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector4__ctor_m2441427762(&L_9, L_2, L_4, L_6, L_8, /*hidden argument*/NULL);
		float L_10 = __this->get_lensflareIntensity_18();
		Vector4_t4282066567  L_11 = Vector4_op_Multiply_m209031836(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_0);
		Material_SetVector_m3505096203(L_0, _stringLiteral2940124478, L_11, /*hidden argument*/NULL);
		Material_t3870600107 * L_12 = __this->get_lensFlareMaterial_27();
		Color_t4194546905 * L_13 = __this->get_address_of_flareColorB_21();
		float L_14 = L_13->get_r_0();
		Color_t4194546905 * L_15 = __this->get_address_of_flareColorB_21();
		float L_16 = L_15->get_g_1();
		Color_t4194546905 * L_17 = __this->get_address_of_flareColorB_21();
		float L_18 = L_17->get_b_2();
		Color_t4194546905 * L_19 = __this->get_address_of_flareColorB_21();
		float L_20 = L_19->get_a_3();
		Vector4_t4282066567  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector4__ctor_m2441427762(&L_21, L_14, L_16, L_18, L_20, /*hidden argument*/NULL);
		float L_22 = __this->get_lensflareIntensity_18();
		Vector4_t4282066567  L_23 = Vector4_op_Multiply_m209031836(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_12);
		Material_SetVector_m3505096203(L_12, _stringLiteral2940124479, L_23, /*hidden argument*/NULL);
		Material_t3870600107 * L_24 = __this->get_lensFlareMaterial_27();
		Color_t4194546905 * L_25 = __this->get_address_of_flareColorC_22();
		float L_26 = L_25->get_r_0();
		Color_t4194546905 * L_27 = __this->get_address_of_flareColorC_22();
		float L_28 = L_27->get_g_1();
		Color_t4194546905 * L_29 = __this->get_address_of_flareColorC_22();
		float L_30 = L_29->get_b_2();
		Color_t4194546905 * L_31 = __this->get_address_of_flareColorC_22();
		float L_32 = L_31->get_a_3();
		Vector4_t4282066567  L_33;
		memset(&L_33, 0, sizeof(L_33));
		Vector4__ctor_m2441427762(&L_33, L_26, L_28, L_30, L_32, /*hidden argument*/NULL);
		float L_34 = __this->get_lensflareIntensity_18();
		Vector4_t4282066567  L_35 = Vector4_op_Multiply_m209031836(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		NullCheck(L_24);
		Material_SetVector_m3505096203(L_24, _stringLiteral2940124480, L_35, /*hidden argument*/NULL);
		Material_t3870600107 * L_36 = __this->get_lensFlareMaterial_27();
		Color_t4194546905 * L_37 = __this->get_address_of_flareColorD_23();
		float L_38 = L_37->get_r_0();
		Color_t4194546905 * L_39 = __this->get_address_of_flareColorD_23();
		float L_40 = L_39->get_g_1();
		Color_t4194546905 * L_41 = __this->get_address_of_flareColorD_23();
		float L_42 = L_41->get_b_2();
		Color_t4194546905 * L_43 = __this->get_address_of_flareColorD_23();
		float L_44 = L_43->get_a_3();
		Vector4_t4282066567  L_45;
		memset(&L_45, 0, sizeof(L_45));
		Vector4__ctor_m2441427762(&L_45, L_38, L_40, L_42, L_44, /*hidden argument*/NULL);
		float L_46 = __this->get_lensflareIntensity_18();
		Vector4_t4282066567  L_47 = Vector4_op_Multiply_m209031836(NULL /*static, unused*/, L_45, L_46, /*hidden argument*/NULL);
		NullCheck(L_36);
		Material_SetVector_m3505096203(L_36, _stringLiteral2940124481, L_47, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_48 = ___from0;
		RenderTexture_t1963041563 * L_49 = ___to1;
		Material_t3870600107 * L_50 = __this->get_lensFlareMaterial_27();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_48, L_49, L_50, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BloomAndLensFlares::BrightFilter(System.Single,System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral3629614843;
extern Il2CppCodeGenString* _stringLiteral2143726111;
extern const uint32_t BloomAndLensFlares_BrightFilter_m3496269507_MetadataUsageId;
extern "C"  void BloomAndLensFlares_BrightFilter_m3496269507 (BloomAndLensFlares_t2492782871 * __this, float ___thresh0, float ___useAlphaAsMask1, RenderTexture_t1963041563 * ___from2, RenderTexture_t1963041563 * ___to3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BloomAndLensFlares_BrightFilter_m3496269507_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_doHdr_8();
		if (!L_0)
		{
			goto IL_002f;
		}
	}
	{
		Material_t3870600107 * L_1 = __this->get_brightPassFilterMaterial_39();
		float L_2 = ___thresh0;
		Vector4_t4282066567  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector4__ctor_m2441427762(&L_3, L_2, (1.0f), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_1);
		Material_SetVector_m3505096203(L_1, _stringLiteral3629614843, L_3, /*hidden argument*/NULL);
		goto IL_0056;
	}

IL_002f:
	{
		Material_t3870600107 * L_4 = __this->get_brightPassFilterMaterial_39();
		float L_5 = ___thresh0;
		float L_6 = ___thresh0;
		Vector4_t4282066567  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector4__ctor_m2441427762(&L_7, L_5, ((float)((float)(1.0f)/(float)((float)((float)(1.0f)-(float)L_6)))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_4);
		Material_SetVector_m3505096203(L_4, _stringLiteral3629614843, L_7, /*hidden argument*/NULL);
	}

IL_0056:
	{
		Material_t3870600107 * L_8 = __this->get_brightPassFilterMaterial_39();
		float L_9 = ___useAlphaAsMask1;
		NullCheck(L_8);
		Material_SetFloat_m981710063(L_8, _stringLiteral2143726111, L_9, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_10 = ___from2;
		RenderTexture_t1963041563 * L_11 = ___to3;
		Material_t3870600107 * L_12 = __this->get_brightPassFilterMaterial_39();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_10, L_11, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BloomAndLensFlares::Vignette(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1541889956;
extern Il2CppCodeGenString* _stringLiteral3954511897;
extern const uint32_t BloomAndLensFlares_Vignette_m1092400566_MetadataUsageId;
extern "C"  void BloomAndLensFlares_Vignette_m1092400566 (BloomAndLensFlares_t2492782871 * __this, float ___amount0, RenderTexture_t1963041563 * ___from1, RenderTexture_t1963041563 * ___to2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BloomAndLensFlares_Vignette_m1092400566_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture2D_t3884108195 * L_0 = __this->get_lensFlareVignetteMask_25();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		Material_t3870600107 * L_2 = __this->get_screenBlend_35();
		Texture2D_t3884108195 * L_3 = __this->get_lensFlareVignetteMask_25();
		NullCheck(L_2);
		Material_SetTexture_m1833724755(L_2, _stringLiteral1541889956, L_3, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_4 = ___from1;
		RenderTexture_t1963041563 * L_5 = ___to2;
		Material_t3870600107 * L_6 = __this->get_screenBlend_35();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_4, L_5, L_6, 3, /*hidden argument*/NULL);
		goto IL_0057;
	}

IL_0039:
	{
		Material_t3870600107 * L_7 = __this->get_vignetteMaterial_29();
		float L_8 = ___amount0;
		NullCheck(L_7);
		Material_SetFloat_m981710063(L_7, _stringLiteral3954511897, L_8, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_9 = ___from1;
		RenderTexture_t1963041563 * L_10 = ___to2;
		Material_t3870600107 * L_11 = __this->get_vignetteMaterial_29();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void BloomAndLensFlares::Main()
extern "C"  void BloomAndLensFlares_Main_m132731792 (BloomAndLensFlares_t2492782871 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Blur::.ctor()
extern "C"  void Blur__ctor_m812308445 (Blur_t2073735 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase__ctor_m3869393199(__this, /*hidden argument*/NULL);
		__this->set_downsample_5(1);
		__this->set_blurSize_6((3.0f));
		__this->set_blurIterations_7(2);
		__this->set_blurType_8(0);
		return;
	}
}
// System.Boolean Blur::CheckResources()
extern "C"  bool Blur_CheckResources_m42718442 (Blur_t2073735 * __this, const MethodInfo* method)
{
	{
		VirtFuncInvoker1< bool, bool >::Invoke(10 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean) */, __this, (bool)0);
		Shader_t3191267369 * L_0 = __this->get_blurShader_9();
		Material_t3870600107 * L_1 = __this->get_blurMaterial_10();
		Material_t3870600107 * L_2 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_0, L_1);
		__this->set_blurMaterial_10(L_2);
		bool L_3 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		if (L_3)
		{
			goto IL_0031;
		}
	}
	{
		VirtActionInvoker0::Invoke(13 /* System.Void PostEffectsBase::ReportAutoDisable() */, __this);
	}

IL_0031:
	{
		bool L_4 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		return L_4;
	}
}
// System.Void Blur::OnDisable()
extern "C"  void Blur_OnDisable_m3680040388 (Blur_t2073735 * __this, const MethodInfo* method)
{
	{
		Material_t3870600107 * L_0 = __this->get_blurMaterial_10();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t3870600107 * L_2 = __this->get_blurMaterial_10();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void Blur::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral2469514762;
extern const uint32_t Blur_OnRenderImage_m3512620641_MetadataUsageId;
extern "C"  void Blur_OnRenderImage_m3512620641 (Blur_t2073735 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Blur_OnRenderImage_m3512620641_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	RenderTexture_t1963041563 * V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	RenderTexture_t1963041563 * V_7 = NULL;
	int32_t G_B5_0 = 0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Blur::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		RenderTexture_t1963041563 * L_1 = ___source0;
		RenderTexture_t1963041563 * L_2 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_017c;
	}

IL_0017:
	{
		int32_t L_3 = __this->get_downsample_5();
		V_0 = ((float)((float)(1.0f)/(float)((float)((float)(1.0f)*(float)(((float)((float)((int32_t)((int32_t)1<<(int32_t)L_3)))))))));
		Material_t3870600107 * L_4 = __this->get_blurMaterial_10();
		float L_5 = __this->get_blurSize_6();
		float L_6 = V_0;
		float L_7 = __this->get_blurSize_6();
		float L_8 = V_0;
		Vector4_t4282066567  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector4__ctor_m2441427762(&L_9, ((float)((float)L_5*(float)L_6)), ((float)((float)((-L_7))*(float)L_8)), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_4);
		Material_SetVector_m3505096203(L_4, _stringLiteral2469514762, L_9, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_10 = ___source0;
		NullCheck(L_10);
		Texture_set_filterMode_m3842701708(L_10, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_11 = ___source0;
		NullCheck(L_11);
		int32_t L_12 = RenderTexture_get_width_m1498578543(L_11, /*hidden argument*/NULL);
		int32_t L_13 = __this->get_downsample_5();
		V_1 = ((int32_t)((int32_t)L_12>>(int32_t)L_13));
		RenderTexture_t1963041563 * L_14 = ___source0;
		NullCheck(L_14);
		int32_t L_15 = RenderTexture_get_height_m4010076224(L_14, /*hidden argument*/NULL);
		int32_t L_16 = __this->get_downsample_5();
		V_2 = ((int32_t)((int32_t)L_15>>(int32_t)L_16));
		int32_t L_17 = V_1;
		int32_t L_18 = V_2;
		RenderTexture_t1963041563 * L_19 = ___source0;
		NullCheck(L_19);
		int32_t L_20 = RenderTexture_get_format_m3502109954(L_19, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_21 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, L_17, L_18, 0, L_20, /*hidden argument*/NULL);
		V_3 = L_21;
		RenderTexture_t1963041563 * L_22 = V_3;
		NullCheck(L_22);
		Texture_set_filterMode_m3842701708(L_22, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_23 = ___source0;
		RenderTexture_t1963041563 * L_24 = V_3;
		Material_t3870600107 * L_25 = __this->get_blurMaterial_10();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_23, L_24, L_25, 0, /*hidden argument*/NULL);
		int32_t L_26 = __this->get_blurType_8();
		if ((!(((uint32_t)L_26) == ((uint32_t)0))))
		{
			goto IL_00b0;
		}
	}
	{
		G_B5_0 = 0;
		goto IL_00b1;
	}

IL_00b0:
	{
		G_B5_0 = 2;
	}

IL_00b1:
	{
		V_4 = G_B5_0;
		V_5 = 0;
		goto IL_0162;
	}

IL_00bb:
	{
		int32_t L_27 = V_5;
		V_6 = ((float)((float)(((float)((float)L_27)))*(float)(1.0f)));
		Material_t3870600107 * L_28 = __this->get_blurMaterial_10();
		float L_29 = __this->get_blurSize_6();
		float L_30 = V_0;
		float L_31 = V_6;
		float L_32 = __this->get_blurSize_6();
		float L_33 = V_0;
		float L_34 = V_6;
		Vector4_t4282066567  L_35;
		memset(&L_35, 0, sizeof(L_35));
		Vector4__ctor_m2441427762(&L_35, ((float)((float)((float)((float)L_29*(float)L_30))+(float)L_31)), ((float)((float)((float)((float)((-L_32))*(float)L_33))-(float)L_34)), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_28);
		Material_SetVector_m3505096203(L_28, _stringLiteral2469514762, L_35, /*hidden argument*/NULL);
		int32_t L_36 = V_1;
		int32_t L_37 = V_2;
		RenderTexture_t1963041563 * L_38 = ___source0;
		NullCheck(L_38);
		int32_t L_39 = RenderTexture_get_format_m3502109954(L_38, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_40 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, L_36, L_37, 0, L_39, /*hidden argument*/NULL);
		V_7 = L_40;
		RenderTexture_t1963041563 * L_41 = V_7;
		NullCheck(L_41);
		Texture_set_filterMode_m3842701708(L_41, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_42 = V_3;
		RenderTexture_t1963041563 * L_43 = V_7;
		Material_t3870600107 * L_44 = __this->get_blurMaterial_10();
		int32_t L_45 = V_4;
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_42, L_43, L_44, ((int32_t)((int32_t)1+(int32_t)L_45)), /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_46 = V_3;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_47 = V_7;
		V_3 = L_47;
		int32_t L_48 = V_1;
		int32_t L_49 = V_2;
		RenderTexture_t1963041563 * L_50 = ___source0;
		NullCheck(L_50);
		int32_t L_51 = RenderTexture_get_format_m3502109954(L_50, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_52 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, L_48, L_49, 0, L_51, /*hidden argument*/NULL);
		V_7 = L_52;
		RenderTexture_t1963041563 * L_53 = V_7;
		NullCheck(L_53);
		Texture_set_filterMode_m3842701708(L_53, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_54 = V_3;
		RenderTexture_t1963041563 * L_55 = V_7;
		Material_t3870600107 * L_56 = __this->get_blurMaterial_10();
		int32_t L_57 = V_4;
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_54, L_55, L_56, ((int32_t)((int32_t)2+(int32_t)L_57)), /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_58 = V_3;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_58, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_59 = V_7;
		V_3 = L_59;
		int32_t L_60 = V_5;
		V_5 = ((int32_t)((int32_t)L_60+(int32_t)1));
	}

IL_0162:
	{
		int32_t L_61 = V_5;
		int32_t L_62 = __this->get_blurIterations_7();
		if ((((int32_t)L_61) < ((int32_t)L_62)))
		{
			goto IL_00bb;
		}
	}
	{
		RenderTexture_t1963041563 * L_63 = V_3;
		RenderTexture_t1963041563 * L_64 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_63, L_64, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_65 = V_3;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_65, /*hidden argument*/NULL);
	}

IL_017c:
	{
		return;
	}
}
// System.Void Blur::Main()
extern "C"  void Blur_Main_m4236665152 (Blur_t2073735 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraMotionBlur::.ctor()
extern "C"  void CameraMotionBlur__ctor_m2724484834 (CameraMotionBlur_t2114294370 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase__ctor_m3869393199(__this, /*hidden argument*/NULL);
		__this->set_filterType_6(2);
		Vector3_t4282066566  L_0 = Vector3_get_one_m886467710(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_previewScale_8(L_0);
		__this->set_rotationScale_10((1.0f));
		__this->set_maxVelocity_11((8.0f));
		__this->set_minVelocity_12((0.1f));
		__this->set_velocityScale_13((0.375f));
		__this->set_softZDistance_14((0.005f));
		__this->set_velocityDownsample_15(1);
		__this->set_jitter_24((0.05f));
		__this->set_showVelocityScale_26((1.0f));
		Vector3_t4282066566  L_1 = Vector3_get_forward_m1039372701(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_prevFrameForward_31(L_1);
		Vector3_t4282066566  L_2 = Vector3_get_right_m4015137012(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_prevFrameRight_32(L_2);
		Vector3_t4282066566  L_3 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_prevFrameUp_33(L_3);
		Vector3_t4282066566  L_4 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_prevFramePos_34(L_4);
		return;
	}
}
// System.Void CameraMotionBlur::.cctor()
extern Il2CppClass* CameraMotionBlur_t2114294370_il2cpp_TypeInfo_var;
extern const uint32_t CameraMotionBlur__cctor_m2372555019_MetadataUsageId;
extern "C"  void CameraMotionBlur__cctor_m2372555019 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CameraMotionBlur__cctor_m2372555019_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((CameraMotionBlur_t2114294370_StaticFields*)CameraMotionBlur_t2114294370_il2cpp_TypeInfo_var->static_fields)->set_MAX_RADIUS_5((((int32_t)((int32_t)(10.0f)))));
		return;
	}
}
// System.Void CameraMotionBlur::CalculateViewProjection()
extern const MethodInfo* Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var;
extern const uint32_t CameraMotionBlur_CalculateViewProjection_m3450963866_MetadataUsageId;
extern "C"  void CameraMotionBlur_CalculateViewProjection_m3450963866 (CameraMotionBlur_t2114294370 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CameraMotionBlur_CalculateViewProjection_m3450963866_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t1651859333  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t1651859333  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_t2727095145 * L_0 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_0);
		Matrix4x4_t1651859333  L_1 = Camera_get_worldToCameraMatrix_m1346206741(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Camera_t2727095145 * L_2 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_2);
		Matrix4x4_t1651859333  L_3 = Camera_get_projectionMatrix_m3070982480(L_2, /*hidden argument*/NULL);
		Matrix4x4_t1651859333  L_4 = GL_GetGPUProjectionMatrix_m1961192416(NULL /*static, unused*/, L_3, (bool)1, /*hidden argument*/NULL);
		V_1 = L_4;
		Matrix4x4_t1651859333  L_5 = V_1;
		Matrix4x4_t1651859333  L_6 = V_0;
		Matrix4x4_t1651859333  L_7 = Matrix4x4_op_Multiply_m4108203689(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		__this->set_currentViewProjMat_27(L_7);
		return;
	}
}
// System.Void CameraMotionBlur::Start()
extern "C"  void CameraMotionBlur_Start_m1671622626 (CameraMotionBlur_t2114294370 * __this, const MethodInfo* method)
{
	{
		VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean CameraMotionBlur::CheckResources() */, __this);
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = GameObject_get_activeInHierarchy_m612450965(L_0, /*hidden argument*/NULL);
		__this->set_wasActive_30(L_1);
		CameraMotionBlur_CalculateViewProjection_m3450963866(__this, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(20 /* System.Void CameraMotionBlur::Remember() */, __this);
		__this->set_wasActive_30((bool)0);
		return;
	}
}
// System.Void CameraMotionBlur::OnEnable()
extern const MethodInfo* Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var;
extern const uint32_t CameraMotionBlur_OnEnable_m907308388_MetadataUsageId;
extern "C"  void CameraMotionBlur_OnEnable_m907308388 (CameraMotionBlur_t2114294370 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CameraMotionBlur_OnEnable_m907308388_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t2727095145 * L_0 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		Camera_t2727095145 * L_1 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_1);
		int32_t L_2 = Camera_get_depthTextureMode_m2117446653(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Camera_set_depthTextureMode_m2368326786(L_0, ((int32_t)((int32_t)L_2|(int32_t)1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraMotionBlur::OnDisable()
extern "C"  void CameraMotionBlur_OnDisable_m2797693513 (CameraMotionBlur_t2114294370 * __this, const MethodInfo* method)
{
	{
		Material_t3870600107 * L_0 = __this->get_motionBlurMaterial_21();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, (Object_t3071478659 *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		Material_t3870600107 * L_2 = __this->get_motionBlurMaterial_21();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_motionBlurMaterial_21((Material_t3870600107 *)NULL);
	}

IL_0023:
	{
		Material_t3870600107 * L_3 = __this->get_dx11MotionBlurMaterial_22();
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, (Object_t3071478659 *)NULL, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0046;
		}
	}
	{
		Material_t3870600107 * L_5 = __this->get_dx11MotionBlurMaterial_22();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->set_dx11MotionBlurMaterial_22((Material_t3870600107 *)NULL);
	}

IL_0046:
	{
		GameObject_t3674682005 * L_6 = __this->get_tmpCam_17();
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, (Object_t3071478659 *)NULL, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0069;
		}
	}
	{
		GameObject_t3674682005 * L_8 = __this->get_tmpCam_17();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		__this->set_tmpCam_17((GameObject_t3674682005 *)NULL);
	}

IL_0069:
	{
		return;
	}
}
// System.Boolean CameraMotionBlur::CheckResources()
extern "C"  bool CameraMotionBlur_CheckResources_m2324546181 (CameraMotionBlur_t2114294370 * __this, const MethodInfo* method)
{
	{
		VirtFuncInvoker2< bool, bool, bool >::Invoke(11 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean,System.Boolean) */, __this, (bool)1, (bool)1);
		Shader_t3191267369 * L_0 = __this->get_shader_18();
		Material_t3870600107 * L_1 = __this->get_motionBlurMaterial_21();
		Material_t3870600107 * L_2 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_0, L_1);
		__this->set_motionBlurMaterial_21(L_2);
		bool L_3 = ((PostEffectsBase_t1820837395 *)__this)->get_supportDX11_3();
		if (!L_3)
		{
			goto IL_0050;
		}
	}
	{
		int32_t L_4 = __this->get_filterType_6();
		if ((!(((uint32_t)L_4) == ((uint32_t)3))))
		{
			goto IL_0050;
		}
	}
	{
		Shader_t3191267369 * L_5 = __this->get_dx11MotionBlurShader_19();
		Material_t3870600107 * L_6 = __this->get_dx11MotionBlurMaterial_22();
		Material_t3870600107 * L_7 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_5, L_6);
		__this->set_dx11MotionBlurMaterial_22(L_7);
	}

IL_0050:
	{
		bool L_8 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		if (L_8)
		{
			goto IL_0061;
		}
	}
	{
		VirtActionInvoker0::Invoke(13 /* System.Void PostEffectsBase::ReportAutoDisable() */, __this);
	}

IL_0061:
	{
		bool L_9 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		return L_9;
	}
}
// System.Void CameraMotionBlur::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppClass* CameraMotionBlur_t2114294370_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3931961556;
extern Il2CppCodeGenString* _stringLiteral1000298740;
extern Il2CppCodeGenString* _stringLiteral1496491892;
extern Il2CppCodeGenString* _stringLiteral917416066;
extern Il2CppCodeGenString* _stringLiteral2355817558;
extern Il2CppCodeGenString* _stringLiteral239983472;
extern Il2CppCodeGenString* _stringLiteral1947951182;
extern Il2CppCodeGenString* _stringLiteral632601131;
extern Il2CppCodeGenString* _stringLiteral1434789932;
extern Il2CppCodeGenString* _stringLiteral972187785;
extern Il2CppCodeGenString* _stringLiteral4207496257;
extern Il2CppCodeGenString* _stringLiteral1847446841;
extern Il2CppCodeGenString* _stringLiteral1680901777;
extern Il2CppCodeGenString* _stringLiteral3234500554;
extern Il2CppCodeGenString* _stringLiteral3041989478;
extern const uint32_t CameraMotionBlur_OnRenderImage_m2411491388_MetadataUsageId;
extern "C"  void CameraMotionBlur_OnRenderImage_m2411491388 (CameraMotionBlur_t2114294370 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CameraMotionBlur_OnRenderImage_m2411491388_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RenderTexture_t1963041563 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	bool V_5 = false;
	RenderTexture_t1963041563 * V_6 = NULL;
	RenderTexture_t1963041563 * V_7 = NULL;
	Matrix4x4_t1651859333  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Matrix4x4_t1651859333  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Matrix4x4_t1651859333  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Matrix4x4_t1651859333  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector4_t4282066567  V_12;
	memset(&V_12, 0, sizeof(V_12));
	float V_13 = 0.0f;
	Vector3_t4282066566  V_14;
	memset(&V_14, 0, sizeof(V_14));
	float V_15 = 0.0f;
	float V_16 = 0.0f;
	Camera_t2727095145 * V_17 = NULL;
	int32_t G_B7_0 = 0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean CameraMotionBlur::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		RenderTexture_t1963041563 * L_1 = ___source0;
		RenderTexture_t1963041563 * L_2 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_08cf;
	}

IL_0017:
	{
		int32_t L_3 = __this->get_filterType_6();
		if ((!(((uint32_t)L_3) == ((uint32_t)0))))
		{
			goto IL_0029;
		}
	}
	{
		VirtActionInvoker0::Invoke(22 /* System.Void CameraMotionBlur::StartFrame() */, __this);
	}

IL_0029:
	{
		bool L_4 = SystemInfo_SupportsRenderTextureFormat_m1773213581(NULL /*static, unused*/, ((int32_t)13), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003c;
		}
	}
	{
		G_B7_0 = ((int32_t)13);
		goto IL_003d;
	}

IL_003c:
	{
		G_B7_0 = 2;
	}

IL_003d:
	{
		V_0 = G_B7_0;
		RenderTexture_t1963041563 * L_5 = ___source0;
		NullCheck(L_5);
		int32_t L_6 = RenderTexture_get_width_m1498578543(L_5, /*hidden argument*/NULL);
		int32_t L_7 = __this->get_velocityDownsample_15();
		int32_t L_8 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(23 /* System.Int32 CameraMotionBlur::divRoundUp(System.Int32,System.Int32) */, __this, L_6, L_7);
		RenderTexture_t1963041563 * L_9 = ___source0;
		NullCheck(L_9);
		int32_t L_10 = RenderTexture_get_height_m4010076224(L_9, /*hidden argument*/NULL);
		int32_t L_11 = __this->get_velocityDownsample_15();
		int32_t L_12 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(23 /* System.Int32 CameraMotionBlur::divRoundUp(System.Int32,System.Int32) */, __this, L_10, L_11);
		int32_t L_13 = V_0;
		RenderTexture_t1963041563 * L_14 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, L_8, L_12, 0, L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		V_2 = 1;
		V_3 = 1;
		float L_15 = __this->get_maxVelocity_11();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_16 = Mathf_Max_m3923796455(NULL /*static, unused*/, (2.0f), L_15, /*hidden argument*/NULL);
		__this->set_maxVelocity_11(L_16);
		float L_17 = __this->get_maxVelocity_11();
		V_4 = L_17;
		V_5 = (bool)0;
		int32_t L_18 = __this->get_filterType_6();
		if ((!(((uint32_t)L_18) == ((uint32_t)3))))
		{
			goto IL_00af;
		}
	}
	{
		Material_t3870600107 * L_19 = __this->get_dx11MotionBlurMaterial_22();
		bool L_20 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_19, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00af;
		}
	}
	{
		V_5 = (bool)1;
	}

IL_00af:
	{
		int32_t L_21 = __this->get_filterType_6();
		if ((((int32_t)L_21) == ((int32_t)2)))
		{
			goto IL_00ce;
		}
	}
	{
		bool L_22 = V_5;
		if (L_22)
		{
			goto IL_00ce;
		}
	}
	{
		int32_t L_23 = __this->get_filterType_6();
		if ((!(((uint32_t)L_23) == ((uint32_t)4))))
		{
			goto IL_011d;
		}
	}

IL_00ce:
	{
		float L_24 = __this->get_maxVelocity_11();
		IL2CPP_RUNTIME_CLASS_INIT(CameraMotionBlur_t2114294370_il2cpp_TypeInfo_var);
		int32_t L_25 = ((CameraMotionBlur_t2114294370_StaticFields*)CameraMotionBlur_t2114294370_il2cpp_TypeInfo_var->static_fields)->get_MAX_RADIUS_5();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_26 = Mathf_Min_m2322067385(NULL /*static, unused*/, L_24, (((float)((float)L_25))), /*hidden argument*/NULL);
		__this->set_maxVelocity_11(L_26);
		RenderTexture_t1963041563 * L_27 = V_1;
		NullCheck(L_27);
		int32_t L_28 = RenderTexture_get_width_m1498578543(L_27, /*hidden argument*/NULL);
		float L_29 = __this->get_maxVelocity_11();
		int32_t L_30 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(23 /* System.Int32 CameraMotionBlur::divRoundUp(System.Int32,System.Int32) */, __this, L_28, (((int32_t)((int32_t)L_29))));
		V_2 = L_30;
		RenderTexture_t1963041563 * L_31 = V_1;
		NullCheck(L_31);
		int32_t L_32 = RenderTexture_get_height_m4010076224(L_31, /*hidden argument*/NULL);
		float L_33 = __this->get_maxVelocity_11();
		int32_t L_34 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(23 /* System.Int32 CameraMotionBlur::divRoundUp(System.Int32,System.Int32) */, __this, L_32, (((int32_t)((int32_t)L_33))));
		V_3 = L_34;
		RenderTexture_t1963041563 * L_35 = V_1;
		NullCheck(L_35);
		int32_t L_36 = RenderTexture_get_width_m1498578543(L_35, /*hidden argument*/NULL);
		int32_t L_37 = V_2;
		V_4 = (((float)((float)((int32_t)((int32_t)L_36/(int32_t)L_37)))));
		goto IL_0150;
	}

IL_011d:
	{
		RenderTexture_t1963041563 * L_38 = V_1;
		NullCheck(L_38);
		int32_t L_39 = RenderTexture_get_width_m1498578543(L_38, /*hidden argument*/NULL);
		float L_40 = __this->get_maxVelocity_11();
		int32_t L_41 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(23 /* System.Int32 CameraMotionBlur::divRoundUp(System.Int32,System.Int32) */, __this, L_39, (((int32_t)((int32_t)L_40))));
		V_2 = L_41;
		RenderTexture_t1963041563 * L_42 = V_1;
		NullCheck(L_42);
		int32_t L_43 = RenderTexture_get_height_m4010076224(L_42, /*hidden argument*/NULL);
		float L_44 = __this->get_maxVelocity_11();
		int32_t L_45 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(23 /* System.Int32 CameraMotionBlur::divRoundUp(System.Int32,System.Int32) */, __this, L_43, (((int32_t)((int32_t)L_44))));
		V_3 = L_45;
		RenderTexture_t1963041563 * L_46 = V_1;
		NullCheck(L_46);
		int32_t L_47 = RenderTexture_get_width_m1498578543(L_46, /*hidden argument*/NULL);
		int32_t L_48 = V_2;
		V_4 = (((float)((float)((int32_t)((int32_t)L_47/(int32_t)L_48)))));
	}

IL_0150:
	{
		int32_t L_49 = V_2;
		int32_t L_50 = V_3;
		int32_t L_51 = V_0;
		RenderTexture_t1963041563 * L_52 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, L_49, L_50, 0, L_51, /*hidden argument*/NULL);
		V_6 = L_52;
		int32_t L_53 = V_2;
		int32_t L_54 = V_3;
		int32_t L_55 = V_0;
		RenderTexture_t1963041563 * L_56 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, L_53, L_54, 0, L_55, /*hidden argument*/NULL);
		V_7 = L_56;
		RenderTexture_t1963041563 * L_57 = V_1;
		NullCheck(L_57);
		Texture_set_filterMode_m3842701708(L_57, 0, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_58 = V_6;
		NullCheck(L_58);
		Texture_set_filterMode_m3842701708(L_58, 0, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_59 = V_7;
		NullCheck(L_59);
		Texture_set_filterMode_m3842701708(L_59, 0, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_60 = __this->get_noiseTexture_23();
		bool L_61 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_60, /*hidden argument*/NULL);
		if (!L_61)
		{
			goto IL_0199;
		}
	}
	{
		Texture2D_t3884108195 * L_62 = __this->get_noiseTexture_23();
		NullCheck(L_62);
		Texture_set_filterMode_m3842701708(L_62, 0, /*hidden argument*/NULL);
	}

IL_0199:
	{
		RenderTexture_t1963041563 * L_63 = ___source0;
		NullCheck(L_63);
		Texture_set_wrapMode_m3720633937(L_63, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_64 = V_1;
		NullCheck(L_64);
		Texture_set_wrapMode_m3720633937(L_64, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_65 = V_7;
		NullCheck(L_65);
		Texture_set_wrapMode_m3720633937(L_65, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_66 = V_6;
		NullCheck(L_66);
		Texture_set_wrapMode_m3720633937(L_66, 1, /*hidden argument*/NULL);
		CameraMotionBlur_CalculateViewProjection_m3450963866(__this, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_67 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_67);
		bool L_68 = GameObject_get_activeInHierarchy_m612450965(L_67, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_01de;
		}
	}
	{
		bool L_69 = __this->get_wasActive_30();
		if (L_69)
		{
			goto IL_01de;
		}
	}
	{
		VirtActionInvoker0::Invoke(20 /* System.Void CameraMotionBlur::Remember() */, __this);
	}

IL_01de:
	{
		GameObject_t3674682005 * L_70 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_70);
		bool L_71 = GameObject_get_activeInHierarchy_m612450965(L_70, /*hidden argument*/NULL);
		__this->set_wasActive_30(L_71);
		Matrix4x4_t1651859333  L_72 = __this->get_currentViewProjMat_27();
		Matrix4x4_t1651859333  L_73 = Matrix4x4_Inverse_m1483646919(NULL /*static, unused*/, L_72, /*hidden argument*/NULL);
		V_8 = L_73;
		Material_t3870600107 * L_74 = __this->get_motionBlurMaterial_21();
		Matrix4x4_t1651859333  L_75 = V_8;
		NullCheck(L_74);
		Material_SetMatrix_m3693790735(L_74, _stringLiteral3931961556, L_75, /*hidden argument*/NULL);
		Material_t3870600107 * L_76 = __this->get_motionBlurMaterial_21();
		Matrix4x4_t1651859333  L_77 = __this->get_prevViewProjMat_28();
		NullCheck(L_76);
		Material_SetMatrix_m3693790735(L_76, _stringLiteral1000298740, L_77, /*hidden argument*/NULL);
		Material_t3870600107 * L_78 = __this->get_motionBlurMaterial_21();
		Matrix4x4_t1651859333  L_79 = __this->get_prevViewProjMat_28();
		Matrix4x4_t1651859333  L_80 = V_8;
		Matrix4x4_t1651859333  L_81 = Matrix4x4_op_Multiply_m4108203689(NULL /*static, unused*/, L_79, L_80, /*hidden argument*/NULL);
		NullCheck(L_78);
		Material_SetMatrix_m3693790735(L_78, _stringLiteral1496491892, L_81, /*hidden argument*/NULL);
		Material_t3870600107 * L_82 = __this->get_motionBlurMaterial_21();
		float L_83 = V_4;
		NullCheck(L_82);
		Material_SetFloat_m981710063(L_82, _stringLiteral917416066, L_83, /*hidden argument*/NULL);
		Material_t3870600107 * L_84 = __this->get_motionBlurMaterial_21();
		float L_85 = V_4;
		NullCheck(L_84);
		Material_SetFloat_m981710063(L_84, _stringLiteral2355817558, L_85, /*hidden argument*/NULL);
		Material_t3870600107 * L_86 = __this->get_motionBlurMaterial_21();
		float L_87 = __this->get_minVelocity_12();
		NullCheck(L_86);
		Material_SetFloat_m981710063(L_86, _stringLiteral239983472, L_87, /*hidden argument*/NULL);
		Material_t3870600107 * L_88 = __this->get_motionBlurMaterial_21();
		float L_89 = __this->get_velocityScale_13();
		NullCheck(L_88);
		Material_SetFloat_m981710063(L_88, _stringLiteral1947951182, L_89, /*hidden argument*/NULL);
		Material_t3870600107 * L_90 = __this->get_motionBlurMaterial_21();
		float L_91 = __this->get_jitter_24();
		NullCheck(L_90);
		Material_SetFloat_m981710063(L_90, _stringLiteral632601131, L_91, /*hidden argument*/NULL);
		Material_t3870600107 * L_92 = __this->get_motionBlurMaterial_21();
		Texture2D_t3884108195 * L_93 = __this->get_noiseTexture_23();
		NullCheck(L_92);
		Material_SetTexture_m1833724755(L_92, _stringLiteral1434789932, L_93, /*hidden argument*/NULL);
		Material_t3870600107 * L_94 = __this->get_motionBlurMaterial_21();
		RenderTexture_t1963041563 * L_95 = V_1;
		NullCheck(L_94);
		Material_SetTexture_m1833724755(L_94, _stringLiteral972187785, L_95, /*hidden argument*/NULL);
		Material_t3870600107 * L_96 = __this->get_motionBlurMaterial_21();
		RenderTexture_t1963041563 * L_97 = V_7;
		NullCheck(L_96);
		Material_SetTexture_m1833724755(L_96, _stringLiteral4207496257, L_97, /*hidden argument*/NULL);
		Material_t3870600107 * L_98 = __this->get_motionBlurMaterial_21();
		RenderTexture_t1963041563 * L_99 = V_6;
		NullCheck(L_98);
		Material_SetTexture_m1833724755(L_98, _stringLiteral1847446841, L_99, /*hidden argument*/NULL);
		bool L_100 = __this->get_preview_7();
		if (!L_100)
		{
			goto IL_038e;
		}
	}
	{
		Camera_t2727095145 * L_101 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_101);
		Matrix4x4_t1651859333  L_102 = Camera_get_worldToCameraMatrix_m1346206741(L_101, /*hidden argument*/NULL);
		V_9 = L_102;
		Matrix4x4_t1651859333  L_103 = Matrix4x4_get_identity_m3946683782(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_10 = L_103;
		Vector3_t4282066566  L_104 = __this->get_previewScale_8();
		Vector3_t4282066566  L_105 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_104, (0.3333f), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_106 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_107 = Vector3_get_one_m886467710(NULL /*static, unused*/, /*hidden argument*/NULL);
		Matrix4x4_SetTRS_m4126930040((&V_10), L_105, L_106, L_107, /*hidden argument*/NULL);
		Camera_t2727095145 * L_108 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_108);
		Matrix4x4_t1651859333  L_109 = Camera_get_projectionMatrix_m3070982480(L_108, /*hidden argument*/NULL);
		Matrix4x4_t1651859333  L_110 = GL_GetGPUProjectionMatrix_m1961192416(NULL /*static, unused*/, L_109, (bool)1, /*hidden argument*/NULL);
		V_11 = L_110;
		Matrix4x4_t1651859333  L_111 = V_11;
		Matrix4x4_t1651859333  L_112 = V_10;
		Matrix4x4_t1651859333  L_113 = Matrix4x4_op_Multiply_m4108203689(NULL /*static, unused*/, L_111, L_112, /*hidden argument*/NULL);
		Matrix4x4_t1651859333  L_114 = V_9;
		Matrix4x4_t1651859333  L_115 = Matrix4x4_op_Multiply_m4108203689(NULL /*static, unused*/, L_113, L_114, /*hidden argument*/NULL);
		__this->set_prevViewProjMat_28(L_115);
		Material_t3870600107 * L_116 = __this->get_motionBlurMaterial_21();
		Matrix4x4_t1651859333  L_117 = __this->get_prevViewProjMat_28();
		NullCheck(L_116);
		Material_SetMatrix_m3693790735(L_116, _stringLiteral1000298740, L_117, /*hidden argument*/NULL);
		Material_t3870600107 * L_118 = __this->get_motionBlurMaterial_21();
		Matrix4x4_t1651859333  L_119 = __this->get_prevViewProjMat_28();
		Matrix4x4_t1651859333  L_120 = V_8;
		Matrix4x4_t1651859333  L_121 = Matrix4x4_op_Multiply_m4108203689(NULL /*static, unused*/, L_119, L_120, /*hidden argument*/NULL);
		NullCheck(L_118);
		Material_SetMatrix_m3693790735(L_118, _stringLiteral1496491892, L_121, /*hidden argument*/NULL);
	}

IL_038e:
	{
		int32_t L_122 = __this->get_filterType_6();
		if ((!(((uint32_t)L_122) == ((uint32_t)0))))
		{
			goto IL_05e1;
		}
	}
	{
		Vector4_t4282066567  L_123 = Vector4_get_zero_m3835647092(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_12 = L_123;
		Transform_t1659122786 * L_124 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_124);
		Vector3_t4282066566  L_125 = Transform_get_up_m297874561(L_124, /*hidden argument*/NULL);
		Vector3_t4282066566  L_126 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_127 = Vector3_Dot_m2370485424(NULL /*static, unused*/, L_125, L_126, /*hidden argument*/NULL);
		V_13 = L_127;
		Vector3_t4282066566  L_128 = __this->get_prevFramePos_34();
		Transform_t1659122786 * L_129 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_129);
		Vector3_t4282066566  L_130 = Transform_get_position_m2211398607(L_129, /*hidden argument*/NULL);
		Vector3_t4282066566  L_131 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_128, L_130, /*hidden argument*/NULL);
		V_14 = L_131;
		float L_132 = Vector3_get_magnitude_m989985786((&V_14), /*hidden argument*/NULL);
		V_15 = L_132;
		V_16 = (1.0f);
		Transform_t1659122786 * L_133 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_133);
		Vector3_t4282066566  L_134 = Transform_get_up_m297874561(L_133, /*hidden argument*/NULL);
		Vector3_t4282066566  L_135 = __this->get_prevFrameUp_33();
		float L_136 = Vector3_Angle_m1904328934(NULL /*static, unused*/, L_134, L_135, /*hidden argument*/NULL);
		Camera_t2727095145 * L_137 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_137);
		float L_138 = Camera_get_fieldOfView_m65126887(L_137, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_139 = ___source0;
		NullCheck(L_139);
		int32_t L_140 = RenderTexture_get_width_m1498578543(L_139, /*hidden argument*/NULL);
		V_16 = ((float)((float)((float)((float)L_136/(float)L_138))*(float)((float)((float)(((float)((float)L_140)))*(float)(0.75f)))));
		float L_141 = __this->get_rotationScale_10();
		float L_142 = V_16;
		(&V_12)->set_x_1(((float)((float)L_141*(float)L_142)));
		Transform_t1659122786 * L_143 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_143);
		Vector3_t4282066566  L_144 = Transform_get_forward_m877665793(L_143, /*hidden argument*/NULL);
		Vector3_t4282066566  L_145 = __this->get_prevFrameForward_31();
		float L_146 = Vector3_Angle_m1904328934(NULL /*static, unused*/, L_144, L_145, /*hidden argument*/NULL);
		Camera_t2727095145 * L_147 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_147);
		float L_148 = Camera_get_fieldOfView_m65126887(L_147, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_149 = ___source0;
		NullCheck(L_149);
		int32_t L_150 = RenderTexture_get_width_m1498578543(L_149, /*hidden argument*/NULL);
		V_16 = ((float)((float)((float)((float)L_146/(float)L_148))*(float)((float)((float)(((float)((float)L_150)))*(float)(0.75f)))));
		float L_151 = __this->get_rotationScale_10();
		float L_152 = V_13;
		float L_153 = V_16;
		(&V_12)->set_y_2(((float)((float)((float)((float)L_151*(float)L_152))*(float)L_153)));
		Transform_t1659122786 * L_154 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_154);
		Vector3_t4282066566  L_155 = Transform_get_forward_m877665793(L_154, /*hidden argument*/NULL);
		Vector3_t4282066566  L_156 = __this->get_prevFrameForward_31();
		float L_157 = Vector3_Angle_m1904328934(NULL /*static, unused*/, L_155, L_156, /*hidden argument*/NULL);
		Camera_t2727095145 * L_158 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_158);
		float L_159 = Camera_get_fieldOfView_m65126887(L_158, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_160 = ___source0;
		NullCheck(L_160);
		int32_t L_161 = RenderTexture_get_width_m1498578543(L_160, /*hidden argument*/NULL);
		V_16 = ((float)((float)((float)((float)L_157/(float)L_159))*(float)((float)((float)(((float)((float)L_161)))*(float)(0.75f)))));
		float L_162 = __this->get_rotationScale_10();
		float L_163 = V_13;
		float L_164 = V_16;
		(&V_12)->set_z_3(((float)((float)((float)((float)L_162*(float)((float)((float)(1.0f)-(float)L_163))))*(float)L_164)));
		float L_165 = V_15;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_166 = ((Mathf_t4203372500_StaticFields*)Mathf_t4203372500_il2cpp_TypeInfo_var->static_fields)->get_Epsilon_0();
		if ((((float)L_165) <= ((float)L_166)))
		{
			goto IL_0568;
		}
	}
	{
		float L_167 = __this->get_movementScale_9();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_168 = ((Mathf_t4203372500_StaticFields*)Mathf_t4203372500_il2cpp_TypeInfo_var->static_fields)->get_Epsilon_0();
		if ((((float)L_167) <= ((float)L_168)))
		{
			goto IL_0568;
		}
	}
	{
		float L_169 = __this->get_movementScale_9();
		Transform_t1659122786 * L_170 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_170);
		Vector3_t4282066566  L_171 = Transform_get_forward_m877665793(L_170, /*hidden argument*/NULL);
		Vector3_t4282066566  L_172 = V_14;
		float L_173 = Vector3_Dot_m2370485424(NULL /*static, unused*/, L_171, L_172, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_174 = ___source0;
		NullCheck(L_174);
		int32_t L_175 = RenderTexture_get_width_m1498578543(L_174, /*hidden argument*/NULL);
		(&V_12)->set_w_4(((float)((float)((float)((float)L_169*(float)L_173))*(float)((float)((float)(((float)((float)L_175)))*(float)(0.5f))))));
		float L_176 = (&V_12)->get_x_1();
		float L_177 = __this->get_movementScale_9();
		Transform_t1659122786 * L_178 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_178);
		Vector3_t4282066566  L_179 = Transform_get_up_m297874561(L_178, /*hidden argument*/NULL);
		Vector3_t4282066566  L_180 = V_14;
		float L_181 = Vector3_Dot_m2370485424(NULL /*static, unused*/, L_179, L_180, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_182 = ___source0;
		NullCheck(L_182);
		int32_t L_183 = RenderTexture_get_width_m1498578543(L_182, /*hidden argument*/NULL);
		(&V_12)->set_x_1(((float)((float)L_176+(float)((float)((float)((float)((float)L_177*(float)L_181))*(float)((float)((float)(((float)((float)L_183)))*(float)(0.5f))))))));
		float L_184 = (&V_12)->get_y_2();
		float L_185 = __this->get_movementScale_9();
		Transform_t1659122786 * L_186 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_186);
		Vector3_t4282066566  L_187 = Transform_get_right_m2070836824(L_186, /*hidden argument*/NULL);
		Vector3_t4282066566  L_188 = V_14;
		float L_189 = Vector3_Dot_m2370485424(NULL /*static, unused*/, L_187, L_188, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_190 = ___source0;
		NullCheck(L_190);
		int32_t L_191 = RenderTexture_get_width_m1498578543(L_190, /*hidden argument*/NULL);
		(&V_12)->set_y_2(((float)((float)L_184+(float)((float)((float)((float)((float)L_185*(float)L_189))*(float)((float)((float)(((float)((float)L_191)))*(float)(0.5f))))))));
	}

IL_0568:
	{
		bool L_192 = __this->get_preview_7();
		if (!L_192)
		{
			goto IL_05ca;
		}
	}
	{
		Material_t3870600107 * L_193 = __this->get_motionBlurMaterial_21();
		Vector3_t4282066566 * L_194 = __this->get_address_of_previewScale_8();
		float L_195 = L_194->get_y_2();
		Vector3_t4282066566 * L_196 = __this->get_address_of_previewScale_8();
		float L_197 = L_196->get_x_1();
		Vector3_t4282066566 * L_198 = __this->get_address_of_previewScale_8();
		float L_199 = L_198->get_z_3();
		Vector4_t4282066567  L_200;
		memset(&L_200, 0, sizeof(L_200));
		Vector4__ctor_m2441427762(&L_200, L_195, L_197, (((float)((float)0))), L_199, /*hidden argument*/NULL);
		Vector4_t4282066567  L_201 = Vector4_op_Multiply_m209031836(NULL /*static, unused*/, L_200, (0.5f), /*hidden argument*/NULL);
		Camera_t2727095145 * L_202 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_202);
		float L_203 = Camera_get_fieldOfView_m65126887(L_202, /*hidden argument*/NULL);
		Vector4_t4282066567  L_204 = Vector4_op_Multiply_m209031836(NULL /*static, unused*/, L_201, L_203, /*hidden argument*/NULL);
		NullCheck(L_193);
		Material_SetVector_m3505096203(L_193, _stringLiteral1680901777, L_204, /*hidden argument*/NULL);
		goto IL_05dc;
	}

IL_05ca:
	{
		Material_t3870600107 * L_205 = __this->get_motionBlurMaterial_21();
		Vector4_t4282066567  L_206 = V_12;
		NullCheck(L_205);
		Material_SetVector_m3505096203(L_205, _stringLiteral1680901777, L_206, /*hidden argument*/NULL);
	}

IL_05dc:
	{
		goto IL_0672;
	}

IL_05e1:
	{
		RenderTexture_t1963041563 * L_207 = ___source0;
		RenderTexture_t1963041563 * L_208 = V_1;
		Material_t3870600107 * L_209 = __this->get_motionBlurMaterial_21();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_207, L_208, L_209, 0, /*hidden argument*/NULL);
		V_17 = (Camera_t2727095145 *)NULL;
		LayerMask_t3236759763 * L_210 = __this->get_address_of_excludeLayers_16();
		int32_t L_211 = LayerMask_get_value_m1804554274(L_210, /*hidden argument*/NULL);
		if (!L_211)
		{
			goto IL_060a;
		}
	}
	{
		Camera_t2727095145 * L_212 = VirtFuncInvoker0< Camera_t2727095145 * >::Invoke(21 /* UnityEngine.Camera CameraMotionBlur::GetTmpCam() */, __this);
		V_17 = L_212;
	}

IL_060a:
	{
		Camera_t2727095145 * L_213 = V_17;
		bool L_214 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_213, /*hidden argument*/NULL);
		if (!L_214)
		{
			goto IL_0672;
		}
	}
	{
		LayerMask_t3236759763 * L_215 = __this->get_address_of_excludeLayers_16();
		int32_t L_216 = LayerMask_get_value_m1804554274(L_215, /*hidden argument*/NULL);
		if (!L_216)
		{
			goto IL_0672;
		}
	}
	{
		Shader_t3191267369 * L_217 = __this->get_replacementClear_20();
		bool L_218 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_217, /*hidden argument*/NULL);
		if (!L_218)
		{
			goto IL_0672;
		}
	}
	{
		Shader_t3191267369 * L_219 = __this->get_replacementClear_20();
		NullCheck(L_219);
		bool L_220 = Shader_get_isSupported_m1422621179(L_219, /*hidden argument*/NULL);
		if (!L_220)
		{
			goto IL_0672;
		}
	}
	{
		Camera_t2727095145 * L_221 = V_17;
		RenderTexture_t1963041563 * L_222 = V_1;
		NullCheck(L_221);
		Camera_set_targetTexture_m671169649(L_221, L_222, /*hidden argument*/NULL);
		Camera_t2727095145 * L_223 = V_17;
		LayerMask_t3236759763  L_224 = __this->get_excludeLayers_16();
		int32_t L_225 = LayerMask_op_Implicit_m1595580047(NULL /*static, unused*/, L_224, /*hidden argument*/NULL);
		NullCheck(L_223);
		Camera_set_cullingMask_m2181279574(L_223, L_225, /*hidden argument*/NULL);
		Camera_t2727095145 * L_226 = V_17;
		Shader_t3191267369 * L_227 = __this->get_replacementClear_20();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_228 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_226);
		Camera_RenderWithShader_m1338167485(L_226, L_227, L_228, /*hidden argument*/NULL);
	}

IL_0672:
	{
		bool L_229 = __this->get_preview_7();
		if (L_229)
		{
			goto IL_069e;
		}
	}
	{
		int32_t L_230 = Time_get_frameCount_m3434184975(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_231 = __this->get_prevFrameCount_29();
		if ((((int32_t)L_230) == ((int32_t)L_231)))
		{
			goto IL_069e;
		}
	}
	{
		int32_t L_232 = Time_get_frameCount_m3434184975(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_prevFrameCount_29(L_232);
		VirtActionInvoker0::Invoke(20 /* System.Void CameraMotionBlur::Remember() */, __this);
	}

IL_069e:
	{
		RenderTexture_t1963041563 * L_233 = ___source0;
		NullCheck(L_233);
		Texture_set_filterMode_m3842701708(L_233, 1, /*hidden argument*/NULL);
		bool L_234 = __this->get_showVelocity_25();
		if (!L_234)
		{
			goto IL_06d9;
		}
	}
	{
		Material_t3870600107 * L_235 = __this->get_motionBlurMaterial_21();
		float L_236 = __this->get_showVelocityScale_26();
		NullCheck(L_235);
		Material_SetFloat_m981710063(L_235, _stringLiteral3234500554, L_236, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_237 = V_1;
		RenderTexture_t1963041563 * L_238 = ___destination1;
		Material_t3870600107 * L_239 = __this->get_motionBlurMaterial_21();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_237, L_238, L_239, 1, /*hidden argument*/NULL);
		goto IL_08bb;
	}

IL_06d9:
	{
		int32_t L_240 = __this->get_filterType_6();
		if ((!(((uint32_t)L_240) == ((uint32_t)3))))
		{
			goto IL_07cb;
		}
	}
	{
		bool L_241 = V_5;
		if (L_241)
		{
			goto IL_07cb;
		}
	}
	{
		Material_t3870600107 * L_242 = __this->get_dx11MotionBlurMaterial_22();
		float L_243 = __this->get_minVelocity_12();
		NullCheck(L_242);
		Material_SetFloat_m981710063(L_242, _stringLiteral239983472, L_243, /*hidden argument*/NULL);
		Material_t3870600107 * L_244 = __this->get_dx11MotionBlurMaterial_22();
		float L_245 = __this->get_velocityScale_13();
		NullCheck(L_244);
		Material_SetFloat_m981710063(L_244, _stringLiteral1947951182, L_245, /*hidden argument*/NULL);
		Material_t3870600107 * L_246 = __this->get_dx11MotionBlurMaterial_22();
		float L_247 = __this->get_jitter_24();
		NullCheck(L_246);
		Material_SetFloat_m981710063(L_246, _stringLiteral632601131, L_247, /*hidden argument*/NULL);
		Material_t3870600107 * L_248 = __this->get_dx11MotionBlurMaterial_22();
		Texture2D_t3884108195 * L_249 = __this->get_noiseTexture_23();
		NullCheck(L_248);
		Material_SetTexture_m1833724755(L_248, _stringLiteral1434789932, L_249, /*hidden argument*/NULL);
		Material_t3870600107 * L_250 = __this->get_dx11MotionBlurMaterial_22();
		RenderTexture_t1963041563 * L_251 = V_1;
		NullCheck(L_250);
		Material_SetTexture_m1833724755(L_250, _stringLiteral972187785, L_251, /*hidden argument*/NULL);
		Material_t3870600107 * L_252 = __this->get_dx11MotionBlurMaterial_22();
		RenderTexture_t1963041563 * L_253 = V_7;
		NullCheck(L_252);
		Material_SetTexture_m1833724755(L_252, _stringLiteral4207496257, L_253, /*hidden argument*/NULL);
		Material_t3870600107 * L_254 = __this->get_dx11MotionBlurMaterial_22();
		float L_255 = __this->get_softZDistance_14();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_256 = Mathf_Max_m3923796455(NULL /*static, unused*/, (0.00025f), L_255, /*hidden argument*/NULL);
		NullCheck(L_254);
		Material_SetFloat_m981710063(L_254, _stringLiteral3041989478, L_256, /*hidden argument*/NULL);
		Material_t3870600107 * L_257 = __this->get_dx11MotionBlurMaterial_22();
		float L_258 = V_4;
		NullCheck(L_257);
		Material_SetFloat_m981710063(L_257, _stringLiteral2355817558, L_258, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_259 = V_1;
		RenderTexture_t1963041563 * L_260 = V_6;
		Material_t3870600107 * L_261 = __this->get_dx11MotionBlurMaterial_22();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_259, L_260, L_261, 0, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_262 = V_6;
		RenderTexture_t1963041563 * L_263 = V_7;
		Material_t3870600107 * L_264 = __this->get_dx11MotionBlurMaterial_22();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_262, L_263, L_264, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_265 = ___source0;
		RenderTexture_t1963041563 * L_266 = ___destination1;
		Material_t3870600107 * L_267 = __this->get_dx11MotionBlurMaterial_22();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_265, L_266, L_267, 2, /*hidden argument*/NULL);
		goto IL_08bb;
	}

IL_07cb:
	{
		int32_t L_268 = __this->get_filterType_6();
		if ((((int32_t)L_268) == ((int32_t)2)))
		{
			goto IL_07de;
		}
	}
	{
		bool L_269 = V_5;
		if (!L_269)
		{
			goto IL_0830;
		}
	}

IL_07de:
	{
		Material_t3870600107 * L_270 = __this->get_motionBlurMaterial_21();
		float L_271 = __this->get_softZDistance_14();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_272 = Mathf_Max_m3923796455(NULL /*static, unused*/, (0.00025f), L_271, /*hidden argument*/NULL);
		NullCheck(L_270);
		Material_SetFloat_m981710063(L_270, _stringLiteral3041989478, L_272, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_273 = V_1;
		RenderTexture_t1963041563 * L_274 = V_6;
		Material_t3870600107 * L_275 = __this->get_motionBlurMaterial_21();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_273, L_274, L_275, 2, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_276 = V_6;
		RenderTexture_t1963041563 * L_277 = V_7;
		Material_t3870600107 * L_278 = __this->get_motionBlurMaterial_21();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_276, L_277, L_278, 3, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_279 = ___source0;
		RenderTexture_t1963041563 * L_280 = ___destination1;
		Material_t3870600107 * L_281 = __this->get_motionBlurMaterial_21();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_279, L_280, L_281, 4, /*hidden argument*/NULL);
		goto IL_08bb;
	}

IL_0830:
	{
		int32_t L_282 = __this->get_filterType_6();
		if ((!(((uint32_t)L_282) == ((uint32_t)0))))
		{
			goto IL_084f;
		}
	}
	{
		RenderTexture_t1963041563 * L_283 = ___source0;
		RenderTexture_t1963041563 * L_284 = ___destination1;
		Material_t3870600107 * L_285 = __this->get_motionBlurMaterial_21();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_283, L_284, L_285, 6, /*hidden argument*/NULL);
		goto IL_08bb;
	}

IL_084f:
	{
		int32_t L_286 = __this->get_filterType_6();
		if ((!(((uint32_t)L_286) == ((uint32_t)4))))
		{
			goto IL_08ad;
		}
	}
	{
		Material_t3870600107 * L_287 = __this->get_motionBlurMaterial_21();
		float L_288 = __this->get_softZDistance_14();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_289 = Mathf_Max_m3923796455(NULL /*static, unused*/, (0.00025f), L_288, /*hidden argument*/NULL);
		NullCheck(L_287);
		Material_SetFloat_m981710063(L_287, _stringLiteral3041989478, L_289, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_290 = V_1;
		RenderTexture_t1963041563 * L_291 = V_6;
		Material_t3870600107 * L_292 = __this->get_motionBlurMaterial_21();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_290, L_291, L_292, 2, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_293 = V_6;
		RenderTexture_t1963041563 * L_294 = V_7;
		Material_t3870600107 * L_295 = __this->get_motionBlurMaterial_21();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_293, L_294, L_295, 3, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_296 = ___source0;
		RenderTexture_t1963041563 * L_297 = ___destination1;
		Material_t3870600107 * L_298 = __this->get_motionBlurMaterial_21();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_296, L_297, L_298, 7, /*hidden argument*/NULL);
		goto IL_08bb;
	}

IL_08ad:
	{
		RenderTexture_t1963041563 * L_299 = ___source0;
		RenderTexture_t1963041563 * L_300 = ___destination1;
		Material_t3870600107 * L_301 = __this->get_motionBlurMaterial_21();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_299, L_300, L_301, 5, /*hidden argument*/NULL);
	}

IL_08bb:
	{
		RenderTexture_t1963041563 * L_302 = V_1;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_302, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_303 = V_6;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_303, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_304 = V_7;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_304, /*hidden argument*/NULL);
	}

IL_08cf:
	{
		return;
	}
}
// System.Void CameraMotionBlur::Remember()
extern "C"  void CameraMotionBlur_Remember_m1343680751 (CameraMotionBlur_t2114294370 * __this, const MethodInfo* method)
{
	{
		Matrix4x4_t1651859333  L_0 = __this->get_currentViewProjMat_27();
		__this->set_prevViewProjMat_28(L_0);
		Transform_t1659122786 * L_1 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t4282066566  L_2 = Transform_get_forward_m877665793(L_1, /*hidden argument*/NULL);
		__this->set_prevFrameForward_31(L_2);
		Transform_t1659122786 * L_3 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t4282066566  L_4 = Transform_get_right_m2070836824(L_3, /*hidden argument*/NULL);
		__this->set_prevFrameRight_32(L_4);
		Transform_t1659122786 * L_5 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t4282066566  L_6 = Transform_get_up_m297874561(L_5, /*hidden argument*/NULL);
		__this->set_prevFrameUp_33(L_6);
		Transform_t1659122786 * L_7 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t4282066566  L_8 = Transform_get_position_m2211398607(L_7, /*hidden argument*/NULL);
		__this->set_prevFramePos_34(L_8);
		return;
	}
}
// UnityEngine.Camera CameraMotionBlur::GetTmpCam()
extern const Il2CppType* Camera_t2727095145_0_0_0_var;
extern Il2CppClass* RuntimeServices_t3947355960_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t3674682005_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCamera_t2727095145_m1498907256_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral95;
extern Il2CppCodeGenString* _stringLiteral1616749908;
extern const uint32_t CameraMotionBlur_GetTmpCam_m4157442043_MetadataUsageId;
extern "C"  Camera_t2727095145 * CameraMotionBlur_GetTmpCam_m4157442043 (CameraMotionBlur_t2114294370 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CameraMotionBlur_GetTmpCam_m4157442043_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	GameObject_t3674682005 * V_1 = NULL;
	{
		GameObject_t3674682005 * L_0 = __this->get_tmpCam_17();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_006f;
		}
	}
	{
		Camera_t2727095145 * L_2 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_2);
		String_t* L_3 = Object_get_name_m3709440845(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t3947355960_il2cpp_TypeInfo_var);
		String_t* L_4 = RuntimeServices_op_Addition_m3391097550(NULL /*static, unused*/, _stringLiteral95, L_3, /*hidden argument*/NULL);
		String_t* L_5 = RuntimeServices_op_Addition_m3391097550(NULL /*static, unused*/, L_4, _stringLiteral1616749908, /*hidden argument*/NULL);
		V_0 = L_5;
		String_t* L_6 = V_0;
		GameObject_t3674682005 * L_7 = GameObject_Find_m332785498(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		GameObject_t3674682005 * L_8 = V_1;
		bool L_9 = Object_op_Equality_m3964590952(NULL /*static, unused*/, (Object_t3071478659 *)NULL, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0068;
		}
	}
	{
		String_t* L_10 = V_0;
		TypeU5BU5D_t3339007067* L_11 = ((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Camera_t2727095145_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_12);
		GameObject_t3674682005 * L_13 = (GameObject_t3674682005 *)il2cpp_codegen_object_new(GameObject_t3674682005_il2cpp_TypeInfo_var);
		GameObject__ctor_m176066391(L_13, L_10, L_11, /*hidden argument*/NULL);
		__this->set_tmpCam_17(L_13);
		goto IL_006f;
	}

IL_0068:
	{
		GameObject_t3674682005 * L_14 = V_1;
		__this->set_tmpCam_17(L_14);
	}

IL_006f:
	{
		GameObject_t3674682005 * L_15 = __this->get_tmpCam_17();
		NullCheck(L_15);
		Object_set_hideFlags_m41317712(L_15, ((int32_t)52), /*hidden argument*/NULL);
		GameObject_t3674682005 * L_16 = __this->get_tmpCam_17();
		NullCheck(L_16);
		Transform_t1659122786 * L_17 = GameObject_get_transform_m1278640159(L_16, /*hidden argument*/NULL);
		Camera_t2727095145 * L_18 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_18);
		Transform_t1659122786 * L_19 = Component_get_transform_m4257140443(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_t4282066566  L_20 = Transform_get_position_m2211398607(L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_position_m3111394108(L_17, L_20, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_21 = __this->get_tmpCam_17();
		NullCheck(L_21);
		Transform_t1659122786 * L_22 = GameObject_get_transform_m1278640159(L_21, /*hidden argument*/NULL);
		Camera_t2727095145 * L_23 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_23);
		Transform_t1659122786 * L_24 = Component_get_transform_m4257140443(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Quaternion_t1553702882  L_25 = Transform_get_rotation_m11483428(L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_set_rotation_m1525803229(L_22, L_25, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_26 = __this->get_tmpCam_17();
		NullCheck(L_26);
		Transform_t1659122786 * L_27 = GameObject_get_transform_m1278640159(L_26, /*hidden argument*/NULL);
		Camera_t2727095145 * L_28 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_28);
		Transform_t1659122786 * L_29 = Component_get_transform_m4257140443(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Vector3_t4282066566  L_30 = Transform_get_localScale_m3886572677(L_29, /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_set_localScale_m310756934(L_27, L_30, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_31 = __this->get_tmpCam_17();
		NullCheck(L_31);
		Camera_t2727095145 * L_32 = GameObject_GetComponent_TisCamera_t2727095145_m1498907256(L_31, /*hidden argument*/GameObject_GetComponent_TisCamera_t2727095145_m1498907256_MethodInfo_var);
		Camera_t2727095145 * L_33 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_32);
		Camera_CopyFrom_m2180612415(L_32, L_33, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_34 = __this->get_tmpCam_17();
		NullCheck(L_34);
		Camera_t2727095145 * L_35 = GameObject_GetComponent_TisCamera_t2727095145_m1498907256(L_34, /*hidden argument*/GameObject_GetComponent_TisCamera_t2727095145_m1498907256_MethodInfo_var);
		NullCheck(L_35);
		Behaviour_set_enabled_m2046806933(L_35, (bool)0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_36 = __this->get_tmpCam_17();
		NullCheck(L_36);
		Camera_t2727095145 * L_37 = GameObject_GetComponent_TisCamera_t2727095145_m1498907256(L_36, /*hidden argument*/GameObject_GetComponent_TisCamera_t2727095145_m1498907256_MethodInfo_var);
		NullCheck(L_37);
		Camera_set_depthTextureMode_m2368326786(L_37, 0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_38 = __this->get_tmpCam_17();
		NullCheck(L_38);
		Camera_t2727095145 * L_39 = GameObject_GetComponent_TisCamera_t2727095145_m1498907256(L_38, /*hidden argument*/GameObject_GetComponent_TisCamera_t2727095145_m1498907256_MethodInfo_var);
		NullCheck(L_39);
		Camera_set_clearFlags_m175420861(L_39, 4, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_40 = __this->get_tmpCam_17();
		NullCheck(L_40);
		Camera_t2727095145 * L_41 = GameObject_GetComponent_TisCamera_t2727095145_m1498907256(L_40, /*hidden argument*/GameObject_GetComponent_TisCamera_t2727095145_m1498907256_MethodInfo_var);
		return L_41;
	}
}
// System.Void CameraMotionBlur::StartFrame()
extern "C"  void CameraMotionBlur_StartFrame_m2660784333 (CameraMotionBlur_t2114294370 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_prevFramePos_34();
		Transform_t1659122786 * L_1 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t4282066566  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		Vector3_t4282066566  L_3 = Vector3_Slerp_m1274749478(NULL /*static, unused*/, L_0, L_2, (0.75f), /*hidden argument*/NULL);
		__this->set_prevFramePos_34(L_3);
		return;
	}
}
// System.Int32 CameraMotionBlur::divRoundUp(System.Int32,System.Int32)
extern "C"  int32_t CameraMotionBlur_divRoundUp_m3584197676 (CameraMotionBlur_t2114294370 * __this, int32_t ___x0, int32_t ___d1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___x0;
		int32_t L_1 = ___d1;
		int32_t L_2 = ___d1;
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1))-(int32_t)1))/(int32_t)L_2));
	}
}
// System.Void CameraMotionBlur::Main()
extern "C"  void CameraMotionBlur_Main_m1804496283 (CameraMotionBlur_t2114294370 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ColorCorrectionCurves::.ctor()
extern "C"  void ColorCorrectionCurves__ctor_m2903622781 (ColorCorrectionCurves_t3453175749 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase__ctor_m3869393199(__this, /*hidden argument*/NULL);
		__this->set_saturation_19((1.0f));
		Color_t4194546905  L_0 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_selectiveFromColor_21(L_0);
		Color_t4194546905  L_1 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_selectiveToColor_22(L_1);
		__this->set_updateTextures_24((bool)1);
		__this->set_updateTexturesOnStartup_28((bool)1);
		return;
	}
}
// System.Void ColorCorrectionCurves::Start()
extern "C"  void ColorCorrectionCurves_Start_m1850760573 (ColorCorrectionCurves_t3453175749 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase_Start_m2816530991(__this, /*hidden argument*/NULL);
		__this->set_updateTexturesOnStartup_28((bool)1);
		return;
	}
}
// System.Void ColorCorrectionCurves::Awake()
extern "C"  void ColorCorrectionCurves_Awake_m3141228000 (ColorCorrectionCurves_t3453175749 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean ColorCorrectionCurves::CheckResources()
extern Il2CppClass* Texture2D_t3884108195_il2cpp_TypeInfo_var;
extern const uint32_t ColorCorrectionCurves_CheckResources_m631174462_MetadataUsageId;
extern "C"  bool ColorCorrectionCurves_CheckResources_m631174462 (ColorCorrectionCurves_t3453175749 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ColorCorrectionCurves_CheckResources_m631174462_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_mode_23();
		VirtFuncInvoker1< bool, bool >::Invoke(10 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean) */, __this, (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0));
		Shader_t3191267369 * L_1 = __this->get_simpleColorCorrectionCurvesShader_26();
		Material_t3870600107 * L_2 = __this->get_ccMaterial_13();
		Material_t3870600107 * L_3 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_1, L_2);
		__this->set_ccMaterial_13(L_3);
		Shader_t3191267369 * L_4 = __this->get_colorCorrectionCurvesShader_25();
		Material_t3870600107 * L_5 = __this->get_ccDepthMaterial_14();
		Material_t3870600107 * L_6 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_4, L_5);
		__this->set_ccDepthMaterial_14(L_6);
		Shader_t3191267369 * L_7 = __this->get_colorCorrectionSelectiveShader_27();
		Material_t3870600107 * L_8 = __this->get_selectiveCcMaterial_15();
		Material_t3870600107 * L_9 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_7, L_8);
		__this->set_selectiveCcMaterial_15(L_9);
		Texture2D_t3884108195 * L_10 = __this->get_rgbChannelTex_16();
		bool L_11 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_007c;
		}
	}
	{
		Texture2D_t3884108195 * L_12 = (Texture2D_t3884108195 *)il2cpp_codegen_object_new(Texture2D_t3884108195_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1918985003(L_12, ((int32_t)256), 4, 5, (bool)0, (bool)1, /*hidden argument*/NULL);
		__this->set_rgbChannelTex_16(L_12);
	}

IL_007c:
	{
		Texture2D_t3884108195 * L_13 = __this->get_rgbDepthChannelTex_17();
		bool L_14 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_00a0;
		}
	}
	{
		Texture2D_t3884108195 * L_15 = (Texture2D_t3884108195 *)il2cpp_codegen_object_new(Texture2D_t3884108195_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1918985003(L_15, ((int32_t)256), 4, 5, (bool)0, (bool)1, /*hidden argument*/NULL);
		__this->set_rgbDepthChannelTex_17(L_15);
	}

IL_00a0:
	{
		Texture2D_t3884108195 * L_16 = __this->get_zCurveTex_18();
		bool L_17 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_00c4;
		}
	}
	{
		Texture2D_t3884108195 * L_18 = (Texture2D_t3884108195 *)il2cpp_codegen_object_new(Texture2D_t3884108195_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1918985003(L_18, ((int32_t)256), 1, 5, (bool)0, (bool)1, /*hidden argument*/NULL);
		__this->set_zCurveTex_18(L_18);
	}

IL_00c4:
	{
		Texture2D_t3884108195 * L_19 = __this->get_rgbChannelTex_16();
		NullCheck(L_19);
		Object_set_hideFlags_m41317712(L_19, ((int32_t)52), /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_20 = __this->get_rgbDepthChannelTex_17();
		NullCheck(L_20);
		Object_set_hideFlags_m41317712(L_20, ((int32_t)52), /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_21 = __this->get_zCurveTex_18();
		NullCheck(L_21);
		Object_set_hideFlags_m41317712(L_21, ((int32_t)52), /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_22 = __this->get_rgbChannelTex_16();
		NullCheck(L_22);
		Texture_set_wrapMode_m3720633937(L_22, 1, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_23 = __this->get_rgbDepthChannelTex_17();
		NullCheck(L_23);
		Texture_set_wrapMode_m3720633937(L_23, 1, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_24 = __this->get_zCurveTex_18();
		NullCheck(L_24);
		Texture_set_wrapMode_m3720633937(L_24, 1, /*hidden argument*/NULL);
		bool L_25 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		if (L_25)
		{
			goto IL_0120;
		}
	}
	{
		VirtActionInvoker0::Invoke(13 /* System.Void PostEffectsBase::ReportAutoDisable() */, __this);
	}

IL_0120:
	{
		bool L_26 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		return L_26;
	}
}
// System.Void ColorCorrectionCurves::UpdateParameters()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t ColorCorrectionCurves_UpdateParameters_m102789882_MetadataUsageId;
extern "C"  void ColorCorrectionCurves_UpdateParameters_m102789882 (ColorCorrectionCurves_t3453175749 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ColorCorrectionCurves_UpdateParameters_m102789882_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean ColorCorrectionCurves::CheckResources() */, __this);
		AnimationCurve_t3667593487 * L_0 = __this->get_redChannel_5();
		if (!((!(((Il2CppObject*)(AnimationCurve_t3667593487 *)L_0) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0))
		{
			goto IL_0207;
		}
	}
	{
		AnimationCurve_t3667593487 * L_1 = __this->get_greenChannel_6();
		if (!((!(((Il2CppObject*)(AnimationCurve_t3667593487 *)L_1) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0))
		{
			goto IL_0207;
		}
	}
	{
		AnimationCurve_t3667593487 * L_2 = __this->get_blueChannel_7();
		if (!((!(((Il2CppObject*)(AnimationCurve_t3667593487 *)L_2) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0))
		{
			goto IL_0207;
		}
	}
	{
		V_0 = (((float)((float)0)));
		goto IL_01db;
	}

IL_0039:
	{
		AnimationCurve_t3667593487 * L_3 = __this->get_redChannel_5();
		float L_4 = V_0;
		NullCheck(L_3);
		float L_5 = AnimationCurve_Evaluate_m547727012(L_3, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_6 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_5, (((float)((float)0))), (1.0f), /*hidden argument*/NULL);
		V_1 = L_6;
		AnimationCurve_t3667593487 * L_7 = __this->get_greenChannel_6();
		float L_8 = V_0;
		NullCheck(L_7);
		float L_9 = AnimationCurve_Evaluate_m547727012(L_7, L_8, /*hidden argument*/NULL);
		float L_10 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_9, (((float)((float)0))), (1.0f), /*hidden argument*/NULL);
		V_2 = L_10;
		AnimationCurve_t3667593487 * L_11 = __this->get_blueChannel_7();
		float L_12 = V_0;
		NullCheck(L_11);
		float L_13 = AnimationCurve_Evaluate_m547727012(L_11, L_12, /*hidden argument*/NULL);
		float L_14 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_13, (((float)((float)0))), (1.0f), /*hidden argument*/NULL);
		V_3 = L_14;
		Texture2D_t3884108195 * L_15 = __this->get_rgbChannelTex_16();
		float L_16 = V_0;
		float L_17 = floorf(((float)((float)L_16*(float)(255.0f))));
		float L_18 = V_1;
		float L_19 = V_1;
		float L_20 = V_1;
		Color_t4194546905  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Color__ctor_m103496991(&L_21, L_18, L_19, L_20, /*hidden argument*/NULL);
		NullCheck(L_15);
		Texture2D_SetPixel_m378278602(L_15, (((int32_t)((int32_t)L_17))), 0, L_21, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_22 = __this->get_rgbChannelTex_16();
		float L_23 = V_0;
		float L_24 = floorf(((float)((float)L_23*(float)(255.0f))));
		float L_25 = V_2;
		float L_26 = V_2;
		float L_27 = V_2;
		Color_t4194546905  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Color__ctor_m103496991(&L_28, L_25, L_26, L_27, /*hidden argument*/NULL);
		NullCheck(L_22);
		Texture2D_SetPixel_m378278602(L_22, (((int32_t)((int32_t)L_24))), 1, L_28, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_29 = __this->get_rgbChannelTex_16();
		float L_30 = V_0;
		float L_31 = floorf(((float)((float)L_30*(float)(255.0f))));
		float L_32 = V_3;
		float L_33 = V_3;
		float L_34 = V_3;
		Color_t4194546905  L_35;
		memset(&L_35, 0, sizeof(L_35));
		Color__ctor_m103496991(&L_35, L_32, L_33, L_34, /*hidden argument*/NULL);
		NullCheck(L_29);
		Texture2D_SetPixel_m378278602(L_29, (((int32_t)((int32_t)L_31))), 2, L_35, /*hidden argument*/NULL);
		AnimationCurve_t3667593487 * L_36 = __this->get_zCurve_9();
		float L_37 = V_0;
		NullCheck(L_36);
		float L_38 = AnimationCurve_Evaluate_m547727012(L_36, L_37, /*hidden argument*/NULL);
		float L_39 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_38, (((float)((float)0))), (1.0f), /*hidden argument*/NULL);
		V_4 = L_39;
		Texture2D_t3884108195 * L_40 = __this->get_zCurveTex_18();
		float L_41 = V_0;
		float L_42 = floorf(((float)((float)L_41*(float)(255.0f))));
		float L_43 = V_4;
		float L_44 = V_4;
		float L_45 = V_4;
		Color_t4194546905  L_46;
		memset(&L_46, 0, sizeof(L_46));
		Color__ctor_m103496991(&L_46, L_43, L_44, L_45, /*hidden argument*/NULL);
		NullCheck(L_40);
		Texture2D_SetPixel_m378278602(L_40, (((int32_t)((int32_t)L_42))), 0, L_46, /*hidden argument*/NULL);
		AnimationCurve_t3667593487 * L_47 = __this->get_depthRedChannel_10();
		float L_48 = V_0;
		NullCheck(L_47);
		float L_49 = AnimationCurve_Evaluate_m547727012(L_47, L_48, /*hidden argument*/NULL);
		float L_50 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_49, (((float)((float)0))), (1.0f), /*hidden argument*/NULL);
		V_1 = L_50;
		AnimationCurve_t3667593487 * L_51 = __this->get_depthGreenChannel_11();
		float L_52 = V_0;
		NullCheck(L_51);
		float L_53 = AnimationCurve_Evaluate_m547727012(L_51, L_52, /*hidden argument*/NULL);
		float L_54 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_53, (((float)((float)0))), (1.0f), /*hidden argument*/NULL);
		V_2 = L_54;
		AnimationCurve_t3667593487 * L_55 = __this->get_depthBlueChannel_12();
		float L_56 = V_0;
		NullCheck(L_55);
		float L_57 = AnimationCurve_Evaluate_m547727012(L_55, L_56, /*hidden argument*/NULL);
		float L_58 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_57, (((float)((float)0))), (1.0f), /*hidden argument*/NULL);
		V_3 = L_58;
		Texture2D_t3884108195 * L_59 = __this->get_rgbDepthChannelTex_17();
		float L_60 = V_0;
		float L_61 = floorf(((float)((float)L_60*(float)(255.0f))));
		float L_62 = V_1;
		float L_63 = V_1;
		float L_64 = V_1;
		Color_t4194546905  L_65;
		memset(&L_65, 0, sizeof(L_65));
		Color__ctor_m103496991(&L_65, L_62, L_63, L_64, /*hidden argument*/NULL);
		NullCheck(L_59);
		Texture2D_SetPixel_m378278602(L_59, (((int32_t)((int32_t)L_61))), 0, L_65, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_66 = __this->get_rgbDepthChannelTex_17();
		float L_67 = V_0;
		float L_68 = floorf(((float)((float)L_67*(float)(255.0f))));
		float L_69 = V_2;
		float L_70 = V_2;
		float L_71 = V_2;
		Color_t4194546905  L_72;
		memset(&L_72, 0, sizeof(L_72));
		Color__ctor_m103496991(&L_72, L_69, L_70, L_71, /*hidden argument*/NULL);
		NullCheck(L_66);
		Texture2D_SetPixel_m378278602(L_66, (((int32_t)((int32_t)L_68))), 1, L_72, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_73 = __this->get_rgbDepthChannelTex_17();
		float L_74 = V_0;
		float L_75 = floorf(((float)((float)L_74*(float)(255.0f))));
		float L_76 = V_3;
		float L_77 = V_3;
		float L_78 = V_3;
		Color_t4194546905  L_79;
		memset(&L_79, 0, sizeof(L_79));
		Color__ctor_m103496991(&L_79, L_76, L_77, L_78, /*hidden argument*/NULL);
		NullCheck(L_73);
		Texture2D_SetPixel_m378278602(L_73, (((int32_t)((int32_t)L_75))), 2, L_79, /*hidden argument*/NULL);
		float L_80 = V_0;
		V_0 = ((float)((float)L_80+(float)(0.003921569f)));
	}

IL_01db:
	{
		float L_81 = V_0;
		if ((((float)L_81) <= ((float)(1.0f))))
		{
			goto IL_0039;
		}
	}
	{
		Texture2D_t3884108195 * L_82 = __this->get_rgbChannelTex_16();
		NullCheck(L_82);
		Texture2D_Apply_m1364130776(L_82, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_83 = __this->get_rgbDepthChannelTex_17();
		NullCheck(L_83);
		Texture2D_Apply_m1364130776(L_83, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_84 = __this->get_zCurveTex_18();
		NullCheck(L_84);
		Texture2D_Apply_m1364130776(L_84, /*hidden argument*/NULL);
	}

IL_0207:
	{
		return;
	}
}
// System.Void ColorCorrectionCurves::UpdateTextures()
extern "C"  void ColorCorrectionCurves_UpdateTextures_m1175575560 (ColorCorrectionCurves_t3453175749 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(19 /* System.Void ColorCorrectionCurves::UpdateParameters() */, __this);
		return;
	}
}
// System.Void ColorCorrectionCurves::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo* Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral859220313;
extern Il2CppCodeGenString* _stringLiteral1055602132;
extern Il2CppCodeGenString* _stringLiteral3561220210;
extern Il2CppCodeGenString* _stringLiteral2851286417;
extern Il2CppCodeGenString* _stringLiteral1160522537;
extern Il2CppCodeGenString* _stringLiteral2184388690;
extern const uint32_t ColorCorrectionCurves_OnRenderImage_m4141024705_MetadataUsageId;
extern "C"  void ColorCorrectionCurves_OnRenderImage_m4141024705 (ColorCorrectionCurves_t3453175749 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ColorCorrectionCurves_OnRenderImage_m4141024705_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RenderTexture_t1963041563 * V_0 = NULL;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean ColorCorrectionCurves::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		RenderTexture_t1963041563 * L_1 = ___source0;
		RenderTexture_t1963041563 * L_2 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_0169;
	}

IL_0017:
	{
		bool L_3 = __this->get_updateTexturesOnStartup_28();
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		VirtActionInvoker0::Invoke(19 /* System.Void ColorCorrectionCurves::UpdateParameters() */, __this);
		__this->set_updateTexturesOnStartup_28((bool)0);
	}

IL_002f:
	{
		bool L_4 = __this->get_useDepthCorrection_8();
		if (!L_4)
		{
			goto IL_0052;
		}
	}
	{
		Camera_t2727095145 * L_5 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		Camera_t2727095145 * L_6 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_6);
		int32_t L_7 = Camera_get_depthTextureMode_m2117446653(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Camera_set_depthTextureMode_m2368326786(L_5, ((int32_t)((int32_t)L_7|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0052:
	{
		RenderTexture_t1963041563 * L_8 = ___destination1;
		V_0 = L_8;
		bool L_9 = __this->get_selectiveCc_20();
		if (!L_9)
		{
			goto IL_0071;
		}
	}
	{
		RenderTexture_t1963041563 * L_10 = ___source0;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_width_m1498578543(L_10, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_12 = ___source0;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_height_m4010076224(L_12, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_14 = RenderTexture_GetTemporary_m469965696(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
	}

IL_0071:
	{
		bool L_15 = __this->get_useDepthCorrection_8();
		if (!L_15)
		{
			goto IL_00e6;
		}
	}
	{
		Material_t3870600107 * L_16 = __this->get_ccDepthMaterial_14();
		Texture2D_t3884108195 * L_17 = __this->get_rgbChannelTex_16();
		NullCheck(L_16);
		Material_SetTexture_m1833724755(L_16, _stringLiteral859220313, L_17, /*hidden argument*/NULL);
		Material_t3870600107 * L_18 = __this->get_ccDepthMaterial_14();
		Texture2D_t3884108195 * L_19 = __this->get_zCurveTex_18();
		NullCheck(L_18);
		Material_SetTexture_m1833724755(L_18, _stringLiteral1055602132, L_19, /*hidden argument*/NULL);
		Material_t3870600107 * L_20 = __this->get_ccDepthMaterial_14();
		Texture2D_t3884108195 * L_21 = __this->get_rgbDepthChannelTex_17();
		NullCheck(L_20);
		Material_SetTexture_m1833724755(L_20, _stringLiteral3561220210, L_21, /*hidden argument*/NULL);
		Material_t3870600107 * L_22 = __this->get_ccDepthMaterial_14();
		float L_23 = __this->get_saturation_19();
		NullCheck(L_22);
		Material_SetFloat_m981710063(L_22, _stringLiteral2851286417, L_23, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_24 = ___source0;
		RenderTexture_t1963041563 * L_25 = V_0;
		Material_t3870600107 * L_26 = __this->get_ccDepthMaterial_14();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_24, L_25, L_26, /*hidden argument*/NULL);
		goto IL_011f;
	}

IL_00e6:
	{
		Material_t3870600107 * L_27 = __this->get_ccMaterial_13();
		Texture2D_t3884108195 * L_28 = __this->get_rgbChannelTex_16();
		NullCheck(L_27);
		Material_SetTexture_m1833724755(L_27, _stringLiteral859220313, L_28, /*hidden argument*/NULL);
		Material_t3870600107 * L_29 = __this->get_ccMaterial_13();
		float L_30 = __this->get_saturation_19();
		NullCheck(L_29);
		Material_SetFloat_m981710063(L_29, _stringLiteral2851286417, L_30, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_31 = ___source0;
		RenderTexture_t1963041563 * L_32 = V_0;
		Material_t3870600107 * L_33 = __this->get_ccMaterial_13();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_31, L_32, L_33, /*hidden argument*/NULL);
	}

IL_011f:
	{
		bool L_34 = __this->get_selectiveCc_20();
		if (!L_34)
		{
			goto IL_0169;
		}
	}
	{
		Material_t3870600107 * L_35 = __this->get_selectiveCcMaterial_15();
		Color_t4194546905  L_36 = __this->get_selectiveFromColor_21();
		NullCheck(L_35);
		Material_SetColor_m1918430019(L_35, _stringLiteral1160522537, L_36, /*hidden argument*/NULL);
		Material_t3870600107 * L_37 = __this->get_selectiveCcMaterial_15();
		Color_t4194546905  L_38 = __this->get_selectiveToColor_22();
		NullCheck(L_37);
		Material_SetColor_m1918430019(L_37, _stringLiteral2184388690, L_38, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_39 = V_0;
		RenderTexture_t1963041563 * L_40 = ___destination1;
		Material_t3870600107 * L_41 = __this->get_selectiveCcMaterial_15();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_39, L_40, L_41, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_42 = V_0;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
	}

IL_0169:
	{
		return;
	}
}
// System.Void ColorCorrectionCurves::Main()
extern "C"  void ColorCorrectionCurves_Main_m3472842912 (ColorCorrectionCurves_t3453175749 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ColorCorrectionLut::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t ColorCorrectionLut__ctor_m2519635130_MetadataUsageId;
extern "C"  void ColorCorrectionLut__ctor_m2519635130 (ColorCorrectionLut_t1443265866 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ColorCorrectionLut__ctor_m2519635130_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PostEffectsBase__ctor_m3869393199(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_basedOnTempTex_8(L_0);
		return;
	}
}
// System.Boolean ColorCorrectionLut::CheckResources()
extern "C"  bool ColorCorrectionLut_CheckResources_m1196389933 (ColorCorrectionLut_t1443265866 * __this, const MethodInfo* method)
{
	{
		VirtFuncInvoker1< bool, bool >::Invoke(10 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean) */, __this, (bool)0);
		Shader_t3191267369 * L_0 = __this->get_shader_5();
		Material_t3870600107 * L_1 = __this->get_material_6();
		Material_t3870600107 * L_2 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_0, L_1);
		__this->set_material_6(L_2);
		bool L_3 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		bool L_4 = SystemInfo_get_supports3DTextures_m3554473744(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_003b;
		}
	}

IL_0035:
	{
		VirtActionInvoker0::Invoke(13 /* System.Void PostEffectsBase::ReportAutoDisable() */, __this);
	}

IL_003b:
	{
		bool L_5 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		return L_5;
	}
}
// System.Void ColorCorrectionLut::OnDisable()
extern "C"  void ColorCorrectionLut_OnDisable_m218692641 (ColorCorrectionLut_t1443265866 * __this, const MethodInfo* method)
{
	{
		Material_t3870600107 * L_0 = __this->get_material_6();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Material_t3870600107 * L_2 = __this->get_material_6();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_material_6((Material_t3870600107 *)NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void ColorCorrectionLut::OnDestroy()
extern "C"  void ColorCorrectionLut_OnDestroy_m2396224691 (ColorCorrectionLut_t1443265866 * __this, const MethodInfo* method)
{
	{
		Texture3D_t3884108226 * L_0 = __this->get_converted3DLut_7();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Texture3D_t3884108226 * L_2 = __this->get_converted3DLut_7();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		__this->set_converted3DLut_7((Texture3D_t3884108226 *)NULL);
		return;
	}
}
// System.Void ColorCorrectionLut::SetIdentityLut()
extern Il2CppClass* ColorU5BU5D_t2441545636_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture3D_t3884108226_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t ColorCorrectionLut_SetIdentityLut_m3536906229_MetadataUsageId;
extern "C"  void ColorCorrectionLut_SetIdentityLut_m3536906229 (ColorCorrectionLut_t1443265866 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ColorCorrectionLut_SetIdentityLut_m3536906229_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ColorU5BU5D_t2441545636* V_1 = NULL;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		V_0 = ((int32_t)16);
		int32_t L_0 = V_0;
		int32_t L_1 = V_0;
		int32_t L_2 = V_0;
		V_1 = ((ColorU5BU5D_t2441545636*)SZArrayNew(ColorU5BU5D_t2441545636_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0*(int32_t)L_1))*(int32_t)L_2))));
		int32_t L_3 = V_0;
		V_2 = ((float)((float)(1.0f)/(float)((float)((float)((float)((float)(1.0f)*(float)(((float)((float)L_3)))))-(float)(1.0f)))));
		V_3 = 0;
		goto IL_009c;
	}

IL_002a:
	{
		V_4 = 0;
		goto IL_0090;
	}

IL_0032:
	{
		V_5 = 0;
		goto IL_0082;
	}

IL_003a:
	{
		ColorU5BU5D_t2441545636* L_4 = V_1;
		int32_t L_5 = V_3;
		int32_t L_6 = V_4;
		int32_t L_7 = V_0;
		int32_t L_8 = V_5;
		int32_t L_9 = V_0;
		int32_t L_10 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, ((int32_t)((int32_t)((int32_t)((int32_t)L_5+(int32_t)((int32_t)((int32_t)L_6*(int32_t)L_7))))+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_8*(int32_t)L_9))*(int32_t)L_10)))));
		int32_t L_11 = V_3;
		float L_12 = V_2;
		int32_t L_13 = V_4;
		float L_14 = V_2;
		int32_t L_15 = V_5;
		float L_16 = V_2;
		Color_t4194546905  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Color__ctor_m2252924356(&L_17, ((float)((float)((float)((float)(((float)((float)L_11)))*(float)(1.0f)))*(float)L_12)), ((float)((float)((float)((float)(((float)((float)L_13)))*(float)(1.0f)))*(float)L_14)), ((float)((float)((float)((float)(((float)((float)L_15)))*(float)(1.0f)))*(float)L_16)), (1.0f), /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)L_5+(int32_t)((int32_t)((int32_t)L_6*(int32_t)L_7))))+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_8*(int32_t)L_9))*(int32_t)L_10)))))))) = L_17;
		int32_t L_18 = V_5;
		V_5 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0082:
	{
		int32_t L_19 = V_5;
		int32_t L_20 = V_0;
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_21 = V_4;
		V_4 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_0090:
	{
		int32_t L_22 = V_4;
		int32_t L_23 = V_0;
		if ((((int32_t)L_22) < ((int32_t)L_23)))
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_24 = V_3;
		V_3 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_009c:
	{
		int32_t L_25 = V_3;
		int32_t L_26 = V_0;
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_002a;
		}
	}
	{
		Texture3D_t3884108226 * L_27 = __this->get_converted3DLut_7();
		bool L_28 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00be;
		}
	}
	{
		Texture3D_t3884108226 * L_29 = __this->get_converted3DLut_7();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
	}

IL_00be:
	{
		int32_t L_30 = V_0;
		int32_t L_31 = V_0;
		int32_t L_32 = V_0;
		Texture3D_t3884108226 * L_33 = (Texture3D_t3884108226 *)il2cpp_codegen_object_new(Texture3D_t3884108226_il2cpp_TypeInfo_var);
		Texture3D__ctor_m967628078(L_33, L_30, L_31, L_32, 5, (bool)0, /*hidden argument*/NULL);
		__this->set_converted3DLut_7(L_33);
		Texture3D_t3884108226 * L_34 = __this->get_converted3DLut_7();
		ColorU5BU5D_t2441545636* L_35 = V_1;
		NullCheck(L_34);
		Texture3D_SetPixels_m2314823146(L_34, L_35, /*hidden argument*/NULL);
		Texture3D_t3884108226 * L_36 = __this->get_converted3DLut_7();
		NullCheck(L_36);
		Texture3D_Apply_m3862146713(L_36, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_37 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_basedOnTempTex_8(L_37);
		return;
	}
}
// System.Boolean ColorCorrectionLut::ValidDimensions(UnityEngine.Texture2D)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t ColorCorrectionLut_ValidDimensions_m3371052513_MetadataUsageId;
extern "C"  bool ColorCorrectionLut_ValidDimensions_m3371052513 (ColorCorrectionLut_t1443265866 * __this, Texture2D_t3884108195 * ___tex2d0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ColorCorrectionLut_ValidDimensions_m3371052513_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		Texture2D_t3884108195 * L_0 = ___tex2d0;
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		G_B5_0 = 0;
		goto IL_0036;
	}

IL_0011:
	{
		Texture2D_t3884108195 * L_2 = ___tex2d0;
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_2);
		V_0 = L_3;
		int32_t L_4 = V_0;
		Texture2D_t3884108195 * L_5 = ___tex2d0;
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_7 = sqrtf((((float)((float)L_6))));
		int32_t L_8 = Mathf_FloorToInt_m268511322(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)L_8)))
		{
			goto IL_0035;
		}
	}
	{
		G_B5_0 = 0;
		goto IL_0036;
	}

IL_0035:
	{
		G_B5_0 = 1;
	}

IL_0036:
	{
		return (bool)G_B5_0;
	}
}
// System.Void ColorCorrectionLut::Convert(UnityEngine.Texture2D,System.String)
extern Il2CppClass* RuntimeServices_t3947355960_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ColorU5BU5D_t2441545636_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture3D_t3884108226_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral716009025;
extern Il2CppCodeGenString* _stringLiteral1404729992;
extern Il2CppCodeGenString* _stringLiteral2211240067;
extern const uint32_t ColorCorrectionLut_Convert_m4237580397_MetadataUsageId;
extern "C"  void ColorCorrectionLut_Convert_m4237580397 (ColorCorrectionLut_t1443265866 * __this, Texture2D_t3884108195 * ___temp2DTex0, String_t* ___path1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ColorCorrectionLut_Convert_m4237580397_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ColorU5BU5D_t2441545636* V_1 = NULL;
	ColorU5BU5D_t2441545636* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	{
		Texture2D_t3884108195 * L_0 = ___temp2DTex0;
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0132;
		}
	}
	{
		Texture2D_t3884108195 * L_2 = ___temp2DTex0;
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_2);
		Texture2D_t3884108195 * L_4 = ___temp2DTex0;
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_4);
		V_0 = ((int32_t)((int32_t)L_3*(int32_t)L_5));
		Texture2D_t3884108195 * L_6 = ___temp2DTex0;
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_6);
		V_0 = L_7;
		Texture2D_t3884108195 * L_8 = ___temp2DTex0;
		bool L_9 = VirtFuncInvoker1< bool, Texture2D_t3884108195 * >::Invoke(21 /* System.Boolean ColorCorrectionLut::ValidDimensions(UnityEngine.Texture2D) */, __this, L_8);
		if (L_9)
		{
			goto IL_005b;
		}
	}
	{
		Texture2D_t3884108195 * L_10 = ___temp2DTex0;
		NullCheck(L_10);
		String_t* L_11 = Object_get_name_m3709440845(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t3947355960_il2cpp_TypeInfo_var);
		String_t* L_12 = RuntimeServices_op_Addition_m3391097550(NULL /*static, unused*/, _stringLiteral716009025, L_11, /*hidden argument*/NULL);
		String_t* L_13 = RuntimeServices_op_Addition_m3391097550(NULL /*static, unused*/, L_12, _stringLiteral1404729992, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_basedOnTempTex_8(L_14);
		goto IL_013c;
	}

IL_005b:
	{
		Texture2D_t3884108195 * L_15 = ___temp2DTex0;
		NullCheck(L_15);
		ColorU5BU5D_t2441545636* L_16 = Texture2D_GetPixels_m1759701362(L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		ColorU5BU5D_t2441545636* L_17 = V_1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_17);
		int32_t L_18 = Array_get_Length_m1203127607((Il2CppArray *)(Il2CppArray *)L_17, /*hidden argument*/NULL);
		V_2 = ((ColorU5BU5D_t2441545636*)SZArrayNew(ColorU5BU5D_t2441545636_il2cpp_TypeInfo_var, (uint32_t)L_18));
		V_3 = 0;
		goto IL_00dd;
	}

IL_0075:
	{
		V_4 = 0;
		goto IL_00d1;
	}

IL_007d:
	{
		V_5 = 0;
		goto IL_00c3;
	}

IL_0085:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = V_4;
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)L_19-(int32_t)L_20))-(int32_t)1));
		ColorU5BU5D_t2441545636* L_21 = V_2;
		int32_t L_22 = V_3;
		int32_t L_23 = V_4;
		int32_t L_24 = V_0;
		int32_t L_25 = V_5;
		int32_t L_26 = V_0;
		int32_t L_27 = V_0;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, ((int32_t)((int32_t)((int32_t)((int32_t)L_22+(int32_t)((int32_t)((int32_t)L_23*(int32_t)L_24))))+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_25*(int32_t)L_26))*(int32_t)L_27)))));
		ColorU5BU5D_t2441545636* L_28 = V_1;
		int32_t L_29 = V_5;
		int32_t L_30 = V_0;
		int32_t L_31 = V_3;
		int32_t L_32 = V_6;
		int32_t L_33 = V_0;
		int32_t L_34 = V_0;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_29*(int32_t)L_30))+(int32_t)L_31))+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_32*(int32_t)L_33))*(int32_t)L_34)))));
		(*(Color_t4194546905 *)((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)L_22+(int32_t)((int32_t)((int32_t)L_23*(int32_t)L_24))))+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_25*(int32_t)L_26))*(int32_t)L_27)))))))) = (*(Color_t4194546905 *)((L_28)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_29*(int32_t)L_30))+(int32_t)L_31))+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_32*(int32_t)L_33))*(int32_t)L_34))))))));
		int32_t L_35 = V_5;
		V_5 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_00c3:
	{
		int32_t L_36 = V_5;
		int32_t L_37 = V_0;
		if ((((int32_t)L_36) < ((int32_t)L_37)))
		{
			goto IL_0085;
		}
	}
	{
		int32_t L_38 = V_4;
		V_4 = ((int32_t)((int32_t)L_38+(int32_t)1));
	}

IL_00d1:
	{
		int32_t L_39 = V_4;
		int32_t L_40 = V_0;
		if ((((int32_t)L_39) < ((int32_t)L_40)))
		{
			goto IL_007d;
		}
	}
	{
		int32_t L_41 = V_3;
		V_3 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_00dd:
	{
		int32_t L_42 = V_3;
		int32_t L_43 = V_0;
		if ((((int32_t)L_42) < ((int32_t)L_43)))
		{
			goto IL_0075;
		}
	}
	{
		Texture3D_t3884108226 * L_44 = __this->get_converted3DLut_7();
		bool L_45 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_00ff;
		}
	}
	{
		Texture3D_t3884108226 * L_46 = __this->get_converted3DLut_7();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
	}

IL_00ff:
	{
		int32_t L_47 = V_0;
		int32_t L_48 = V_0;
		int32_t L_49 = V_0;
		Texture3D_t3884108226 * L_50 = (Texture3D_t3884108226 *)il2cpp_codegen_object_new(Texture3D_t3884108226_il2cpp_TypeInfo_var);
		Texture3D__ctor_m967628078(L_50, L_47, L_48, L_49, 5, (bool)0, /*hidden argument*/NULL);
		__this->set_converted3DLut_7(L_50);
		Texture3D_t3884108226 * L_51 = __this->get_converted3DLut_7();
		ColorU5BU5D_t2441545636* L_52 = V_2;
		NullCheck(L_51);
		Texture3D_SetPixels_m2314823146(L_51, L_52, /*hidden argument*/NULL);
		Texture3D_t3884108226 * L_53 = __this->get_converted3DLut_7();
		NullCheck(L_53);
		Texture3D_Apply_m3862146713(L_53, /*hidden argument*/NULL);
		String_t* L_54 = ___path1;
		__this->set_basedOnTempTex_8(L_54);
		goto IL_013c;
	}

IL_0132:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral2211240067, /*hidden argument*/NULL);
	}

IL_013c:
	{
		return;
	}
}
// System.Void ColorCorrectionLut::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral2799467563;
extern Il2CppCodeGenString* _stringLiteral772558290;
extern Il2CppCodeGenString* _stringLiteral600001760;
extern const uint32_t ColorCorrectionLut_OnRenderImage_m811314020_MetadataUsageId;
extern "C"  void ColorCorrectionLut_OnRenderImage_m811314020 (ColorCorrectionLut_t1443265866 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ColorCorrectionLut_OnRenderImage_m811314020_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Material_t3870600107 * G_B7_0 = NULL;
	RenderTexture_t1963041563 * G_B7_1 = NULL;
	RenderTexture_t1963041563 * G_B7_2 = NULL;
	Material_t3870600107 * G_B6_0 = NULL;
	RenderTexture_t1963041563 * G_B6_1 = NULL;
	RenderTexture_t1963041563 * G_B6_2 = NULL;
	int32_t G_B8_0 = 0;
	Material_t3870600107 * G_B8_1 = NULL;
	RenderTexture_t1963041563 * G_B8_2 = NULL;
	RenderTexture_t1963041563 * G_B8_3 = NULL;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean ColorCorrectionLut::CheckResources() */, __this);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		bool L_1 = SystemInfo_get_supports3DTextures_m3554473744(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0021;
		}
	}

IL_0015:
	{
		RenderTexture_t1963041563 * L_2 = ___source0;
		RenderTexture_t1963041563 * L_3 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		goto IL_00c0;
	}

IL_0021:
	{
		Texture3D_t3884108226 * L_4 = __this->get_converted3DLut_7();
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0038;
		}
	}
	{
		VirtActionInvoker0::Invoke(20 /* System.Void ColorCorrectionLut::SetIdentityLut() */, __this);
	}

IL_0038:
	{
		Texture3D_t3884108226 * L_6 = __this->get_converted3DLut_7();
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_6);
		V_0 = L_7;
		Texture3D_t3884108226 * L_8 = __this->get_converted3DLut_7();
		NullCheck(L_8);
		Texture_set_wrapMode_m3720633937(L_8, 1, /*hidden argument*/NULL);
		Material_t3870600107 * L_9 = __this->get_material_6();
		int32_t L_10 = V_0;
		int32_t L_11 = V_0;
		NullCheck(L_9);
		Material_SetFloat_m981710063(L_9, _stringLiteral2799467563, ((float)((float)(((float)((float)((int32_t)((int32_t)L_10-(int32_t)1)))))/(float)((float)((float)(1.0f)*(float)(((float)((float)L_11))))))), /*hidden argument*/NULL);
		Material_t3870600107 * L_12 = __this->get_material_6();
		int32_t L_13 = V_0;
		NullCheck(L_12);
		Material_SetFloat_m981710063(L_12, _stringLiteral772558290, ((float)((float)(1.0f)/(float)((float)((float)(2.0f)*(float)(((float)((float)L_13))))))), /*hidden argument*/NULL);
		Material_t3870600107 * L_14 = __this->get_material_6();
		Texture3D_t3884108226 * L_15 = __this->get_converted3DLut_7();
		NullCheck(L_14);
		Material_SetTexture_m1833724755(L_14, _stringLiteral600001760, L_15, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_16 = ___source0;
		RenderTexture_t1963041563 * L_17 = ___destination1;
		Material_t3870600107 * L_18 = __this->get_material_6();
		int32_t L_19 = QualitySettings_get_activeColorSpace_m2993616266(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = L_18;
		G_B6_1 = L_17;
		G_B6_2 = L_16;
		if ((!(((uint32_t)L_19) == ((uint32_t)1))))
		{
			G_B7_0 = L_18;
			G_B7_1 = L_17;
			G_B7_2 = L_16;
			goto IL_00ba;
		}
	}
	{
		G_B8_0 = 1;
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		G_B8_3 = G_B6_2;
		goto IL_00bb;
	}

IL_00ba:
	{
		G_B8_0 = 0;
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
		G_B8_3 = G_B7_2;
	}

IL_00bb:
	{
		Graphics_Blit_m336256356(NULL /*static, unused*/, G_B8_3, G_B8_2, G_B8_1, G_B8_0, /*hidden argument*/NULL);
	}

IL_00c0:
	{
		return;
	}
}
// System.Void ColorCorrectionLut::Main()
extern "C"  void ColorCorrectionLut_Main_m689509571 (ColorCorrectionLut_t1443265866 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ContrastEnhance::.ctor()
extern "C"  void ContrastEnhance__ctor_m2604694070 (ContrastEnhance_t4202625324 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase__ctor_m3869393199(__this, /*hidden argument*/NULL);
		__this->set_intensity_5((0.5f));
		__this->set_blurSpread_9((1.0f));
		return;
	}
}
// System.Boolean ContrastEnhance::CheckResources()
extern "C"  bool ContrastEnhance_CheckResources_m827509541 (ContrastEnhance_t4202625324 * __this, const MethodInfo* method)
{
	{
		VirtFuncInvoker1< bool, bool >::Invoke(10 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean) */, __this, (bool)0);
		Shader_t3191267369 * L_0 = __this->get_contrastCompositeShader_11();
		Material_t3870600107 * L_1 = __this->get_contrastCompositeMaterial_8();
		Material_t3870600107 * L_2 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_0, L_1);
		__this->set_contrastCompositeMaterial_8(L_2);
		Shader_t3191267369 * L_3 = __this->get_separableBlurShader_10();
		Material_t3870600107 * L_4 = __this->get_separableBlurMaterial_7();
		Material_t3870600107 * L_5 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_3, L_4);
		__this->set_separableBlurMaterial_7(L_5);
		bool L_6 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		if (L_6)
		{
			goto IL_0049;
		}
	}
	{
		VirtActionInvoker0::Invoke(13 /* System.Void PostEffectsBase::ReportAutoDisable() */, __this);
	}

IL_0049:
	{
		bool L_7 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		return L_7;
	}
}
// System.Void ContrastEnhance::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral2746560064;
extern Il2CppCodeGenString* _stringLiteral2229014907;
extern Il2CppCodeGenString* _stringLiteral499324979;
extern Il2CppCodeGenString* _stringLiteral3629614843;
extern const uint32_t ContrastEnhance_OnRenderImage_m3681364072_MetadataUsageId;
extern "C"  void ContrastEnhance_OnRenderImage_m3681364072 (ContrastEnhance_t4202625324 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContrastEnhance_OnRenderImage_m3681364072_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RenderTexture_t1963041563 * V_2 = NULL;
	RenderTexture_t1963041563 * V_3 = NULL;
	RenderTexture_t1963041563 * V_4 = NULL;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean ContrastEnhance::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		RenderTexture_t1963041563 * L_1 = ___source0;
		RenderTexture_t1963041563 * L_2 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_0145;
	}

IL_0017:
	{
		RenderTexture_t1963041563 * L_3 = ___source0;
		NullCheck(L_3);
		int32_t L_4 = RenderTexture_get_width_m1498578543(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		RenderTexture_t1963041563 * L_5 = ___source0;
		NullCheck(L_5);
		int32_t L_6 = RenderTexture_get_height_m4010076224(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		int32_t L_7 = V_0;
		int32_t L_8 = V_1;
		RenderTexture_t1963041563 * L_9 = RenderTexture_GetTemporary_m52998487(NULL /*static, unused*/, ((int32_t)((int32_t)L_7/(int32_t)2)), ((int32_t)((int32_t)L_8/(int32_t)2)), 0, /*hidden argument*/NULL);
		V_2 = L_9;
		RenderTexture_t1963041563 * L_10 = ___source0;
		RenderTexture_t1963041563 * L_11 = V_2;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		int32_t L_12 = V_0;
		int32_t L_13 = V_1;
		RenderTexture_t1963041563 * L_14 = RenderTexture_GetTemporary_m52998487(NULL /*static, unused*/, ((int32_t)((int32_t)L_12/(int32_t)4)), ((int32_t)((int32_t)L_13/(int32_t)4)), 0, /*hidden argument*/NULL);
		V_3 = L_14;
		RenderTexture_t1963041563 * L_15 = V_2;
		RenderTexture_t1963041563 * L_16 = V_3;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_17 = V_2;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		Material_t3870600107 * L_18 = __this->get_separableBlurMaterial_7();
		float L_19 = __this->get_blurSpread_9();
		RenderTexture_t1963041563 * L_20 = V_3;
		NullCheck(L_20);
		int32_t L_21 = RenderTexture_get_height_m4010076224(L_20, /*hidden argument*/NULL);
		Vector4_t4282066567  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Vector4__ctor_m2441427762(&L_22, (((float)((float)0))), ((float)((float)((float)((float)L_19*(float)(1.0f)))/(float)(((float)((float)L_21))))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_18);
		Material_SetVector_m3505096203(L_18, _stringLiteral2746560064, L_22, /*hidden argument*/NULL);
		int32_t L_23 = V_0;
		int32_t L_24 = V_1;
		RenderTexture_t1963041563 * L_25 = RenderTexture_GetTemporary_m52998487(NULL /*static, unused*/, ((int32_t)((int32_t)L_23/(int32_t)4)), ((int32_t)((int32_t)L_24/(int32_t)4)), 0, /*hidden argument*/NULL);
		V_4 = L_25;
		RenderTexture_t1963041563 * L_26 = V_3;
		RenderTexture_t1963041563 * L_27 = V_4;
		Material_t3870600107 * L_28 = __this->get_separableBlurMaterial_7();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_26, L_27, L_28, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_29 = V_3;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		Material_t3870600107 * L_30 = __this->get_separableBlurMaterial_7();
		float L_31 = __this->get_blurSpread_9();
		RenderTexture_t1963041563 * L_32 = V_3;
		NullCheck(L_32);
		int32_t L_33 = RenderTexture_get_width_m1498578543(L_32, /*hidden argument*/NULL);
		Vector4_t4282066567  L_34;
		memset(&L_34, 0, sizeof(L_34));
		Vector4__ctor_m2441427762(&L_34, ((float)((float)((float)((float)L_31*(float)(1.0f)))/(float)(((float)((float)L_33))))), (((float)((float)0))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_30);
		Material_SetVector_m3505096203(L_30, _stringLiteral2746560064, L_34, /*hidden argument*/NULL);
		int32_t L_35 = V_0;
		int32_t L_36 = V_1;
		RenderTexture_t1963041563 * L_37 = RenderTexture_GetTemporary_m52998487(NULL /*static, unused*/, ((int32_t)((int32_t)L_35/(int32_t)4)), ((int32_t)((int32_t)L_36/(int32_t)4)), 0, /*hidden argument*/NULL);
		V_3 = L_37;
		RenderTexture_t1963041563 * L_38 = V_4;
		RenderTexture_t1963041563 * L_39 = V_3;
		Material_t3870600107 * L_40 = __this->get_separableBlurMaterial_7();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_38, L_39, L_40, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_41 = V_4;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		Material_t3870600107 * L_42 = __this->get_contrastCompositeMaterial_8();
		RenderTexture_t1963041563 * L_43 = V_3;
		NullCheck(L_42);
		Material_SetTexture_m1833724755(L_42, _stringLiteral2229014907, L_43, /*hidden argument*/NULL);
		Material_t3870600107 * L_44 = __this->get_contrastCompositeMaterial_8();
		float L_45 = __this->get_intensity_5();
		NullCheck(L_44);
		Material_SetFloat_m981710063(L_44, _stringLiteral499324979, L_45, /*hidden argument*/NULL);
		Material_t3870600107 * L_46 = __this->get_contrastCompositeMaterial_8();
		float L_47 = __this->get_threshhold_6();
		NullCheck(L_46);
		Material_SetFloat_m981710063(L_46, _stringLiteral3629614843, L_47, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_48 = ___source0;
		RenderTexture_t1963041563 * L_49 = ___destination1;
		Material_t3870600107 * L_50 = __this->get_contrastCompositeMaterial_8();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_48, L_49, L_50, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_51 = V_3;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
	}

IL_0145:
	{
		return;
	}
}
// System.Void ContrastEnhance::Main()
extern "C"  void ContrastEnhance_Main_m4155936711 (ContrastEnhance_t4202625324 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Crease::.ctor()
extern "C"  void Crease__ctor_m4015790055 (Crease_t2026540285 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase__ctor_m3869393199(__this, /*hidden argument*/NULL);
		__this->set_intensity_5((0.5f));
		__this->set_softness_6(1);
		__this->set_spread_7((1.0f));
		return;
	}
}
// System.Boolean Crease::CheckResources()
extern "C"  bool Crease_CheckResources_m4147814048 (Crease_t2026540285 * __this, const MethodInfo* method)
{
	{
		VirtFuncInvoker1< bool, bool >::Invoke(10 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean) */, __this, (bool)1);
		Shader_t3191267369 * L_0 = __this->get_blurShader_8();
		Material_t3870600107 * L_1 = __this->get_blurMaterial_9();
		Material_t3870600107 * L_2 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_0, L_1);
		__this->set_blurMaterial_9(L_2);
		Shader_t3191267369 * L_3 = __this->get_depthFetchShader_10();
		Material_t3870600107 * L_4 = __this->get_depthFetchMaterial_11();
		Material_t3870600107 * L_5 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_3, L_4);
		__this->set_depthFetchMaterial_11(L_5);
		Shader_t3191267369 * L_6 = __this->get_creaseApplyShader_12();
		Material_t3870600107 * L_7 = __this->get_creaseApplyMaterial_13();
		Material_t3870600107 * L_8 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_6, L_7);
		__this->set_creaseApplyMaterial_13(L_8);
		bool L_9 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		if (L_9)
		{
			goto IL_0061;
		}
	}
	{
		VirtActionInvoker0::Invoke(13 /* System.Void PostEffectsBase::ReportAutoDisable() */, __this);
	}

IL_0061:
	{
		bool L_10 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		return L_10;
	}
}
// System.Void Crease::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral2746560064;
extern Il2CppCodeGenString* _stringLiteral945216333;
extern Il2CppCodeGenString* _stringLiteral159162313;
extern Il2CppCodeGenString* _stringLiteral499324979;
extern const uint32_t Crease_OnRenderImage_m4140501015_MetadataUsageId;
extern "C"  void Crease_OnRenderImage_m4140501015 (Crease_t2026540285 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Crease_OnRenderImage_m4140501015_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	RenderTexture_t1963041563 * V_4 = NULL;
	RenderTexture_t1963041563 * V_5 = NULL;
	int32_t V_6 = 0;
	RenderTexture_t1963041563 * V_7 = NULL;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Crease::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		RenderTexture_t1963041563 * L_1 = ___source0;
		RenderTexture_t1963041563 * L_2 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_0174;
	}

IL_0017:
	{
		RenderTexture_t1963041563 * L_3 = ___source0;
		NullCheck(L_3);
		int32_t L_4 = RenderTexture_get_width_m1498578543(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		RenderTexture_t1963041563 * L_5 = ___source0;
		NullCheck(L_5);
		int32_t L_6 = RenderTexture_get_height_m4010076224(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		int32_t L_7 = V_0;
		int32_t L_8 = V_1;
		V_2 = ((float)((float)((float)((float)(1.0f)*(float)(((float)((float)L_7)))))/(float)((float)((float)(1.0f)*(float)(((float)((float)L_8)))))));
		V_3 = (0.001953125f);
		int32_t L_9 = V_0;
		int32_t L_10 = V_1;
		RenderTexture_t1963041563 * L_11 = RenderTexture_GetTemporary_m52998487(NULL /*static, unused*/, L_9, L_10, 0, /*hidden argument*/NULL);
		V_4 = L_11;
		int32_t L_12 = V_0;
		int32_t L_13 = V_1;
		RenderTexture_t1963041563 * L_14 = RenderTexture_GetTemporary_m52998487(NULL /*static, unused*/, ((int32_t)((int32_t)L_12/(int32_t)2)), ((int32_t)((int32_t)L_13/(int32_t)2)), 0, /*hidden argument*/NULL);
		V_5 = L_14;
		RenderTexture_t1963041563 * L_15 = ___source0;
		RenderTexture_t1963041563 * L_16 = V_4;
		Material_t3870600107 * L_17 = __this->get_depthFetchMaterial_11();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_18 = V_4;
		RenderTexture_t1963041563 * L_19 = V_5;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		V_6 = 0;
		goto IL_0112;
	}

IL_0074:
	{
		int32_t L_20 = V_0;
		int32_t L_21 = V_1;
		RenderTexture_t1963041563 * L_22 = RenderTexture_GetTemporary_m52998487(NULL /*static, unused*/, ((int32_t)((int32_t)L_20/(int32_t)2)), ((int32_t)((int32_t)L_21/(int32_t)2)), 0, /*hidden argument*/NULL);
		V_7 = L_22;
		Material_t3870600107 * L_23 = __this->get_blurMaterial_9();
		float L_24 = __this->get_spread_7();
		float L_25 = V_3;
		Vector4_t4282066567  L_26;
		memset(&L_26, 0, sizeof(L_26));
		Vector4__ctor_m2441427762(&L_26, (((float)((float)0))), ((float)((float)L_24*(float)L_25)), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_23);
		Material_SetVector_m3505096203(L_23, _stringLiteral2746560064, L_26, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_27 = V_5;
		RenderTexture_t1963041563 * L_28 = V_7;
		Material_t3870600107 * L_29 = __this->get_blurMaterial_9();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_27, L_28, L_29, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_30 = V_5;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_31 = V_7;
		V_5 = L_31;
		int32_t L_32 = V_0;
		int32_t L_33 = V_1;
		RenderTexture_t1963041563 * L_34 = RenderTexture_GetTemporary_m52998487(NULL /*static, unused*/, ((int32_t)((int32_t)L_32/(int32_t)2)), ((int32_t)((int32_t)L_33/(int32_t)2)), 0, /*hidden argument*/NULL);
		V_7 = L_34;
		Material_t3870600107 * L_35 = __this->get_blurMaterial_9();
		float L_36 = __this->get_spread_7();
		float L_37 = V_3;
		float L_38 = V_2;
		Vector4_t4282066567  L_39;
		memset(&L_39, 0, sizeof(L_39));
		Vector4__ctor_m2441427762(&L_39, ((float)((float)((float)((float)L_36*(float)L_37))/(float)L_38)), (((float)((float)0))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_35);
		Material_SetVector_m3505096203(L_35, _stringLiteral2746560064, L_39, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_40 = V_5;
		RenderTexture_t1963041563 * L_41 = V_7;
		Material_t3870600107 * L_42 = __this->get_blurMaterial_9();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_40, L_41, L_42, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_43 = V_5;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_44 = V_7;
		V_5 = L_44;
		int32_t L_45 = V_6;
		V_6 = ((int32_t)((int32_t)L_45+(int32_t)1));
	}

IL_0112:
	{
		int32_t L_46 = V_6;
		int32_t L_47 = __this->get_softness_6();
		if ((((int32_t)L_46) < ((int32_t)L_47)))
		{
			goto IL_0074;
		}
	}
	{
		Material_t3870600107 * L_48 = __this->get_creaseApplyMaterial_13();
		RenderTexture_t1963041563 * L_49 = V_4;
		NullCheck(L_48);
		Material_SetTexture_m1833724755(L_48, _stringLiteral945216333, L_49, /*hidden argument*/NULL);
		Material_t3870600107 * L_50 = __this->get_creaseApplyMaterial_13();
		RenderTexture_t1963041563 * L_51 = V_5;
		NullCheck(L_50);
		Material_SetTexture_m1833724755(L_50, _stringLiteral159162313, L_51, /*hidden argument*/NULL);
		Material_t3870600107 * L_52 = __this->get_creaseApplyMaterial_13();
		float L_53 = __this->get_intensity_5();
		NullCheck(L_52);
		Material_SetFloat_m981710063(L_52, _stringLiteral499324979, L_53, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_54 = ___source0;
		RenderTexture_t1963041563 * L_55 = ___destination1;
		Material_t3870600107 * L_56 = __this->get_creaseApplyMaterial_13();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_54, L_55, L_56, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_57 = V_4;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_58 = V_5;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_58, /*hidden argument*/NULL);
	}

IL_0174:
	{
		return;
	}
}
// System.Void Crease::Main()
extern "C"  void Crease_Main_m1707603958 (Crease_t2026540285 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DepthOfField34::.ctor()
extern "C"  void DepthOfField34__ctor_m3144651971 (DepthOfField34_t2156099489 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase__ctor_m3869393199(__this, /*hidden argument*/NULL);
		__this->set_quality_7(1);
		__this->set_resolution_8(4);
		__this->set_simpleTweakMode_9((bool)1);
		__this->set_focalPoint_10((1.0f));
		__this->set_smoothness_11((0.5f));
		__this->set_focalZStartCurve_13((1.0f));
		__this->set_focalZEndCurve_14((1.0f));
		__this->set_focalStartCurve_15((2.0f));
		__this->set_focalEndCurve_16((2.0f));
		__this->set_focalDistance01_17((0.1f));
		__this->set_bluriness_20(2);
		__this->set_maxBlurSpread_21((1.75f));
		__this->set_foregroundBlurExtrude_22((1.15f));
		__this->set_bokehDestination_28(1);
		__this->set_widthOverHeight_29((1.25f));
		__this->set_oneOverBaseSize_30((0.001953125f));
		__this->set_bokehSupport_32((bool)1);
		__this->set_bokehScale_35((2.4f));
		__this->set_bokehIntensity_36((0.15f));
		__this->set_bokehThreshholdContrast_37((0.1f));
		__this->set_bokehThreshholdLuminance_38((0.55f));
		__this->set_bokehDownsample_39(1);
		return;
	}
}
// System.Void DepthOfField34::.cctor()
extern Il2CppClass* DepthOfField34_t2156099489_il2cpp_TypeInfo_var;
extern const uint32_t DepthOfField34__cctor_m2512834378_MetadataUsageId;
extern "C"  void DepthOfField34__cctor_m2512834378 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DepthOfField34__cctor_m2512834378_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((DepthOfField34_t2156099489_StaticFields*)DepthOfField34_t2156099489_il2cpp_TypeInfo_var->static_fields)->set_SMOOTH_DOWNSAMPLE_PASS_5(6);
		((DepthOfField34_t2156099489_StaticFields*)DepthOfField34_t2156099489_il2cpp_TypeInfo_var->static_fields)->set_BOKEH_EXTRA_BLUR_6((2.0f));
		return;
	}
}
// System.Void DepthOfField34::CreateMaterials()
extern "C"  void DepthOfField34_CreateMaterials_m4196866161 (DepthOfField34_t2156099489 * __this, const MethodInfo* method)
{
	{
		Shader_t3191267369 * L_0 = __this->get_dofBlurShader_23();
		Material_t3870600107 * L_1 = __this->get_dofBlurMaterial_24();
		Material_t3870600107 * L_2 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_0, L_1);
		__this->set_dofBlurMaterial_24(L_2);
		Shader_t3191267369 * L_3 = __this->get_dofShader_25();
		Material_t3870600107 * L_4 = __this->get_dofMaterial_26();
		Material_t3870600107 * L_5 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_3, L_4);
		__this->set_dofMaterial_26(L_5);
		Shader_t3191267369 * L_6 = __this->get_bokehShader_33();
		NullCheck(L_6);
		bool L_7 = Shader_get_isSupported_m1422621179(L_6, /*hidden argument*/NULL);
		__this->set_bokehSupport_32(L_7);
		bool L_8 = __this->get_bokeh_31();
		if (!L_8)
		{
			goto IL_007f;
		}
	}
	{
		bool L_9 = __this->get_bokehSupport_32();
		if (!L_9)
		{
			goto IL_007f;
		}
	}
	{
		Shader_t3191267369 * L_10 = __this->get_bokehShader_33();
		bool L_11 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_007f;
		}
	}
	{
		Shader_t3191267369 * L_12 = __this->get_bokehShader_33();
		Material_t3870600107 * L_13 = __this->get_bokehMaterial_40();
		Material_t3870600107 * L_14 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_12, L_13);
		__this->set_bokehMaterial_40(L_14);
	}

IL_007f:
	{
		return;
	}
}
// System.Boolean DepthOfField34::CheckResources()
extern "C"  bool DepthOfField34_CheckResources_m2543719236 (DepthOfField34_t2156099489 * __this, const MethodInfo* method)
{
	{
		VirtFuncInvoker1< bool, bool >::Invoke(10 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean) */, __this, (bool)1);
		Shader_t3191267369 * L_0 = __this->get_dofBlurShader_23();
		Material_t3870600107 * L_1 = __this->get_dofBlurMaterial_24();
		Material_t3870600107 * L_2 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_0, L_1);
		__this->set_dofBlurMaterial_24(L_2);
		Shader_t3191267369 * L_3 = __this->get_dofShader_25();
		Material_t3870600107 * L_4 = __this->get_dofMaterial_26();
		Material_t3870600107 * L_5 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_3, L_4);
		__this->set_dofMaterial_26(L_5);
		Shader_t3191267369 * L_6 = __this->get_bokehShader_33();
		NullCheck(L_6);
		bool L_7 = Shader_get_isSupported_m1422621179(L_6, /*hidden argument*/NULL);
		__this->set_bokehSupport_32(L_7);
		bool L_8 = __this->get_bokeh_31();
		if (!L_8)
		{
			goto IL_0087;
		}
	}
	{
		bool L_9 = __this->get_bokehSupport_32();
		if (!L_9)
		{
			goto IL_0087;
		}
	}
	{
		Shader_t3191267369 * L_10 = __this->get_bokehShader_33();
		bool L_11 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0087;
		}
	}
	{
		Shader_t3191267369 * L_12 = __this->get_bokehShader_33();
		Material_t3870600107 * L_13 = __this->get_bokehMaterial_40();
		Material_t3870600107 * L_14 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_12, L_13);
		__this->set_bokehMaterial_40(L_14);
	}

IL_0087:
	{
		bool L_15 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		if (L_15)
		{
			goto IL_0098;
		}
	}
	{
		VirtActionInvoker0::Invoke(13 /* System.Void PostEffectsBase::ReportAutoDisable() */, __this);
	}

IL_0098:
	{
		bool L_16 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		return L_16;
	}
}
// System.Void DepthOfField34::OnDisable()
extern Il2CppClass* Quads_t78387180_il2cpp_TypeInfo_var;
extern const uint32_t DepthOfField34_OnDisable_m2856898474_MetadataUsageId;
extern "C"  void DepthOfField34_OnDisable_m2856898474 (DepthOfField34_t2156099489 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DepthOfField34_OnDisable_m2856898474_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t78387180_il2cpp_TypeInfo_var);
		Quads_Cleanup_m1235265592(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DepthOfField34::OnEnable()
extern const MethodInfo* Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var;
extern const uint32_t DepthOfField34_OnEnable_m2571786211_MetadataUsageId;
extern "C"  void DepthOfField34_OnEnable_m2571786211 (DepthOfField34_t2156099489 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DepthOfField34_OnEnable_m2571786211_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t2727095145 * L_0 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		Camera_t2727095145 * L_1 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_1);
		int32_t L_2 = Camera_get_depthTextureMode_m2117446653(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Camera_set_depthTextureMode_m2368326786(L_0, ((int32_t)((int32_t)L_2|(int32_t)1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Single DepthOfField34::FocalDistance01(System.Single)
extern const MethodInfo* Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var;
extern const uint32_t DepthOfField34_FocalDistance01_m2973206811_MetadataUsageId;
extern "C"  float DepthOfField34_FocalDistance01_m2973206811 (DepthOfField34_t2156099489 * __this, float ___worldDist0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DepthOfField34_FocalDistance01_m2973206811_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_t2727095145 * L_0 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		float L_1 = ___worldDist0;
		Camera_t2727095145 * L_2 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_2);
		float L_3 = Camera_get_nearClipPlane_m4074655061(L_2, /*hidden argument*/NULL);
		Camera_t2727095145 * L_4 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = Component_get_transform_m4257140443(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t4282066566  L_6 = Transform_get_forward_m877665793(L_5, /*hidden argument*/NULL);
		Vector3_t4282066566  L_7 = Vector3_op_Multiply_m3809076219(NULL /*static, unused*/, ((float)((float)L_1-(float)L_3)), L_6, /*hidden argument*/NULL);
		Camera_t2727095145 * L_8 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_8);
		Transform_t1659122786 * L_9 = Component_get_transform_m4257140443(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t4282066566  L_10 = Transform_get_position_m2211398607(L_9, /*hidden argument*/NULL);
		Vector3_t4282066566  L_11 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_7, L_10, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t4282066566  L_12 = Camera_WorldToViewportPoint_m3480725126(L_0, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		float L_13 = (&V_0)->get_z_3();
		Camera_t2727095145 * L_14 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_14);
		float L_15 = Camera_get_farClipPlane_m388706726(L_14, /*hidden argument*/NULL);
		Camera_t2727095145 * L_16 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_16);
		float L_17 = Camera_get_nearClipPlane_m4074655061(L_16, /*hidden argument*/NULL);
		return ((float)((float)L_13/(float)((float)((float)L_15-(float)L_17))));
	}
}
// System.Int32 DepthOfField34::GetDividerBasedOnQuality()
extern "C"  int32_t DepthOfField34_GetDividerBasedOnQuality_m763614097 (DepthOfField34_t2156099489 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 1;
		int32_t L_0 = __this->get_resolution_8();
		if ((!(((uint32_t)L_0) == ((uint32_t)3))))
		{
			goto IL_0015;
		}
	}
	{
		V_0 = 2;
		goto IL_0023;
	}

IL_0015:
	{
		int32_t L_1 = __this->get_resolution_8();
		if ((!(((uint32_t)L_1) == ((uint32_t)4))))
		{
			goto IL_0023;
		}
	}
	{
		V_0 = 2;
	}

IL_0023:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Int32 DepthOfField34::GetLowResolutionDividerBasedOnQuality(System.Int32)
extern "C"  int32_t DepthOfField34_GetLowResolutionDividerBasedOnQuality_m1635427470 (DepthOfField34_t2156099489 * __this, int32_t ___baseDivider0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___baseDivider0;
		V_0 = L_0;
		int32_t L_1 = __this->get_resolution_8();
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_2 = V_0;
		V_0 = ((int32_t)((int32_t)L_2*(int32_t)2));
	}

IL_0012:
	{
		int32_t L_3 = __this->get_resolution_8();
		if ((!(((uint32_t)L_3) == ((uint32_t)4))))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4*(int32_t)2));
	}

IL_0022:
	{
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.Void DepthOfField34::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* DepthOfField34_t2156099489_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral309728476;
extern Il2CppCodeGenString* _stringLiteral3447799158;
extern Il2CppCodeGenString* _stringLiteral388325018;
extern Il2CppCodeGenString* _stringLiteral2416425146;
extern Il2CppCodeGenString* _stringLiteral911347184;
extern Il2CppCodeGenString* _stringLiteral1474613081;
extern Il2CppCodeGenString* _stringLiteral1387762494;
extern Il2CppCodeGenString* _stringLiteral409446227;
extern const uint32_t DepthOfField34_OnRenderImage_m3298611643_MetadataUsageId;
extern "C"  void DepthOfField34_OnRenderImage_m3298611643 (DepthOfField34_t2156099489 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DepthOfField34_OnRenderImage_m3298611643_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	float V_2 = 0.0f;
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	Vector3_t4282066566  V_6;
	memset(&V_6, 0, sizeof(V_6));
	bool G_B6_0 = false;
	DepthOfField34_t2156099489 * G_B6_1 = NULL;
	bool G_B5_0 = false;
	DepthOfField34_t2156099489 * G_B5_1 = NULL;
	float G_B9_0 = 0.0f;
	DepthOfField34_t2156099489 * G_B12_0 = NULL;
	DepthOfField34_t2156099489 * G_B11_0 = NULL;
	float G_B13_0 = 0.0f;
	DepthOfField34_t2156099489 * G_B13_1 = NULL;
	int32_t G_B15_0 = 0;
	bool G_B14_0 = false;
	int32_t G_B21_0 = 0;
	bool G_B20_0 = false;
	String_t* G_B24_0 = NULL;
	Material_t3870600107 * G_B24_1 = NULL;
	String_t* G_B23_0 = NULL;
	Material_t3870600107 * G_B23_1 = NULL;
	float G_B25_0 = 0.0f;
	String_t* G_B25_1 = NULL;
	Material_t3870600107 * G_B25_2 = NULL;
	float G_B27_0 = 0.0f;
	String_t* G_B27_1 = NULL;
	Material_t3870600107 * G_B27_2 = NULL;
	float G_B26_0 = 0.0f;
	String_t* G_B26_1 = NULL;
	Material_t3870600107 * G_B26_2 = NULL;
	float G_B28_0 = 0.0f;
	float G_B28_1 = 0.0f;
	String_t* G_B28_2 = NULL;
	Material_t3870600107 * G_B28_3 = NULL;
	RenderTexture_t1963041563 * G_B37_0 = NULL;
	RenderTexture_t1963041563 * G_B36_0 = NULL;
	RenderTexture_t1963041563 * G_B38_0 = NULL;
	RenderTexture_t1963041563 * G_B38_1 = NULL;
	Material_t3870600107 * G_B40_0 = NULL;
	RenderTexture_t1963041563 * G_B40_1 = NULL;
	RenderTexture_t1963041563 * G_B40_2 = NULL;
	Material_t3870600107 * G_B39_0 = NULL;
	RenderTexture_t1963041563 * G_B39_1 = NULL;
	RenderTexture_t1963041563 * G_B39_2 = NULL;
	int32_t G_B41_0 = 0;
	Material_t3870600107 * G_B41_1 = NULL;
	RenderTexture_t1963041563 * G_B41_2 = NULL;
	RenderTexture_t1963041563 * G_B41_3 = NULL;
	Material_t3870600107 * G_B48_0 = NULL;
	RenderTexture_t1963041563 * G_B48_1 = NULL;
	RenderTexture_t1963041563 * G_B48_2 = NULL;
	Material_t3870600107 * G_B47_0 = NULL;
	RenderTexture_t1963041563 * G_B47_1 = NULL;
	RenderTexture_t1963041563 * G_B47_2 = NULL;
	int32_t G_B49_0 = 0;
	Material_t3870600107 * G_B49_1 = NULL;
	RenderTexture_t1963041563 * G_B49_2 = NULL;
	RenderTexture_t1963041563 * G_B49_3 = NULL;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean DepthOfField34::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		RenderTexture_t1963041563 * L_1 = ___source0;
		RenderTexture_t1963041563 * L_2 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_0600;
	}

IL_0017:
	{
		float L_3 = __this->get_smoothness_11();
		if ((((float)L_3) >= ((float)(0.1f))))
		{
			goto IL_0032;
		}
	}
	{
		__this->set_smoothness_11((0.1f));
	}

IL_0032:
	{
		bool L_4 = __this->get_bokeh_31();
		bool L_5 = L_4;
		G_B5_0 = L_5;
		G_B5_1 = __this;
		if (!L_5)
		{
			G_B6_0 = L_5;
			G_B6_1 = __this;
			goto IL_0046;
		}
	}
	{
		bool L_6 = __this->get_bokehSupport_32();
		G_B6_0 = L_6;
		G_B6_1 = G_B5_1;
	}

IL_0046:
	{
		NullCheck(G_B6_1);
		G_B6_1->set_bokeh_31(G_B6_0);
		bool L_7 = __this->get_bokeh_31();
		if (!L_7)
		{
			goto IL_0060;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DepthOfField34_t2156099489_il2cpp_TypeInfo_var);
		float L_8 = ((DepthOfField34_t2156099489_StaticFields*)DepthOfField34_t2156099489_il2cpp_TypeInfo_var->static_fields)->get_BOKEH_EXTRA_BLUR_6();
		G_B9_0 = L_8;
		goto IL_0065;
	}

IL_0060:
	{
		G_B9_0 = (1.0f);
	}

IL_0065:
	{
		V_0 = G_B9_0;
		int32_t L_9 = __this->get_quality_7();
		V_1 = (bool)((((int32_t)L_9) > ((int32_t)1))? 1 : 0);
		float L_10 = __this->get_focalSize_19();
		Camera_t2727095145 * L_11 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_11);
		float L_12 = Camera_get_farClipPlane_m388706726(L_11, /*hidden argument*/NULL);
		Camera_t2727095145 * L_13 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_13);
		float L_14 = Camera_get_nearClipPlane_m4074655061(L_13, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_10/(float)((float)((float)L_12-(float)L_14))));
		bool L_15 = __this->get_simpleTweakMode_9();
		if (!L_15)
		{
			goto IL_0132;
		}
	}
	{
		Transform_t1659122786 * L_16 = __this->get_objectFocus_18();
		bool L_17 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		G_B11_0 = __this;
		if (!L_17)
		{
			G_B12_0 = __this;
			goto IL_00db;
		}
	}
	{
		Camera_t2727095145 * L_18 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		Transform_t1659122786 * L_19 = __this->get_objectFocus_18();
		NullCheck(L_19);
		Vector3_t4282066566  L_20 = Transform_get_position_m2211398607(L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_t4282066566  L_21 = Camera_WorldToViewportPoint_m3480725126(L_18, L_20, /*hidden argument*/NULL);
		V_6 = L_21;
		float L_22 = (&V_6)->get_z_3();
		Camera_t2727095145 * L_23 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_23);
		float L_24 = Camera_get_farClipPlane_m388706726(L_23, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)L_22/(float)L_24));
		G_B13_1 = G_B11_0;
		goto IL_00e7;
	}

IL_00db:
	{
		float L_25 = __this->get_focalPoint_10();
		float L_26 = VirtFuncInvoker1< float, float >::Invoke(20 /* System.Single DepthOfField34::FocalDistance01(System.Single) */, __this, L_25);
		G_B13_0 = L_26;
		G_B13_1 = G_B12_0;
	}

IL_00e7:
	{
		NullCheck(G_B13_1);
		G_B13_1->set_focalDistance01_17(G_B13_0);
		float L_27 = __this->get_focalDistance01_17();
		float L_28 = __this->get_smoothness_11();
		__this->set_focalStartCurve_15(((float)((float)L_27*(float)L_28)));
		float L_29 = __this->get_focalStartCurve_15();
		__this->set_focalEndCurve_16(L_29);
		bool L_30 = V_1;
		bool L_31 = L_30;
		G_B14_0 = L_31;
		if (!L_31)
		{
			G_B15_0 = ((int32_t)(L_31));
			goto IL_012c;
		}
	}
	{
		float L_32 = __this->get_focalPoint_10();
		Camera_t2727095145 * L_33 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_33);
		float L_34 = Camera_get_nearClipPlane_m4074655061(L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_35 = ((Mathf_t4203372500_StaticFields*)Mathf_t4203372500_il2cpp_TypeInfo_var->static_fields)->get_Epsilon_0();
		G_B15_0 = ((((float)L_32) > ((float)((float)((float)L_34+(float)L_35))))? 1 : 0);
	}

IL_012c:
	{
		V_1 = (bool)G_B15_0;
		goto IL_01d1;
	}

IL_0132:
	{
		Transform_t1659122786 * L_36 = __this->get_objectFocus_18();
		bool L_37 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0185;
		}
	}
	{
		Camera_t2727095145 * L_38 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		Transform_t1659122786 * L_39 = __this->get_objectFocus_18();
		NullCheck(L_39);
		Vector3_t4282066566  L_40 = Transform_get_position_m2211398607(L_39, /*hidden argument*/NULL);
		NullCheck(L_38);
		Vector3_t4282066566  L_41 = Camera_WorldToViewportPoint_m3480725126(L_38, L_40, /*hidden argument*/NULL);
		V_3 = L_41;
		float L_42 = (&V_3)->get_z_3();
		Camera_t2727095145 * L_43 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_43);
		float L_44 = Camera_get_farClipPlane_m388706726(L_43, /*hidden argument*/NULL);
		(&V_3)->set_z_3(((float)((float)L_42/(float)L_44)));
		float L_45 = (&V_3)->get_z_3();
		__this->set_focalDistance01_17(L_45);
		goto IL_0197;
	}

IL_0185:
	{
		float L_46 = __this->get_focalZDistance_12();
		float L_47 = VirtFuncInvoker1< float, float >::Invoke(20 /* System.Single DepthOfField34::FocalDistance01(System.Single) */, __this, L_46);
		__this->set_focalDistance01_17(L_47);
	}

IL_0197:
	{
		float L_48 = __this->get_focalZStartCurve_13();
		__this->set_focalStartCurve_15(L_48);
		float L_49 = __this->get_focalZEndCurve_14();
		__this->set_focalEndCurve_16(L_49);
		bool L_50 = V_1;
		bool L_51 = L_50;
		G_B20_0 = L_51;
		if (!L_51)
		{
			G_B21_0 = ((int32_t)(L_51));
			goto IL_01d0;
		}
	}
	{
		float L_52 = __this->get_focalPoint_10();
		Camera_t2727095145 * L_53 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_53);
		float L_54 = Camera_get_nearClipPlane_m4074655061(L_53, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_55 = ((Mathf_t4203372500_StaticFields*)Mathf_t4203372500_il2cpp_TypeInfo_var->static_fields)->get_Epsilon_0();
		G_B21_0 = ((((float)L_52) > ((float)((float)((float)L_54+(float)L_55))))? 1 : 0);
	}

IL_01d0:
	{
		V_1 = (bool)G_B21_0;
	}

IL_01d1:
	{
		RenderTexture_t1963041563 * L_56 = ___source0;
		NullCheck(L_56);
		int32_t L_57 = RenderTexture_get_width_m1498578543(L_56, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_58 = ___source0;
		NullCheck(L_58);
		int32_t L_59 = RenderTexture_get_height_m4010076224(L_58, /*hidden argument*/NULL);
		__this->set_widthOverHeight_29(((float)((float)((float)((float)(1.0f)*(float)(((float)((float)L_57)))))/(float)((float)((float)(1.0f)*(float)(((float)((float)L_59))))))));
		__this->set_oneOverBaseSize_30((0.001953125f));
		Material_t3870600107 * L_60 = __this->get_dofMaterial_26();
		float L_61 = __this->get_foregroundBlurExtrude_22();
		NullCheck(L_60);
		Material_SetFloat_m981710063(L_60, _stringLiteral309728476, L_61, /*hidden argument*/NULL);
		Material_t3870600107 * L_62 = __this->get_dofMaterial_26();
		bool L_63 = __this->get_simpleTweakMode_9();
		G_B23_0 = _stringLiteral3447799158;
		G_B23_1 = L_62;
		if (!L_63)
		{
			G_B24_0 = _stringLiteral3447799158;
			G_B24_1 = L_62;
			goto IL_023a;
		}
	}
	{
		float L_64 = __this->get_focalStartCurve_15();
		G_B25_0 = ((float)((float)(1.0f)/(float)L_64));
		G_B25_1 = G_B23_0;
		G_B25_2 = G_B23_1;
		goto IL_0240;
	}

IL_023a:
	{
		float L_65 = __this->get_focalStartCurve_15();
		G_B25_0 = L_65;
		G_B25_1 = G_B24_0;
		G_B25_2 = G_B24_1;
	}

IL_0240:
	{
		bool L_66 = __this->get_simpleTweakMode_9();
		G_B26_0 = G_B25_0;
		G_B26_1 = G_B25_1;
		G_B26_2 = G_B25_2;
		if (!L_66)
		{
			G_B27_0 = G_B25_0;
			G_B27_1 = G_B25_1;
			G_B27_2 = G_B25_2;
			goto IL_025c;
		}
	}
	{
		float L_67 = __this->get_focalEndCurve_16();
		G_B28_0 = ((float)((float)(1.0f)/(float)L_67));
		G_B28_1 = G_B26_0;
		G_B28_2 = G_B26_1;
		G_B28_3 = G_B26_2;
		goto IL_0262;
	}

IL_025c:
	{
		float L_68 = __this->get_focalEndCurve_16();
		G_B28_0 = L_68;
		G_B28_1 = G_B27_0;
		G_B28_2 = G_B27_1;
		G_B28_3 = G_B27_2;
	}

IL_0262:
	{
		float L_69 = V_2;
		float L_70 = __this->get_focalDistance01_17();
		Vector4_t4282066567  L_71;
		memset(&L_71, 0, sizeof(L_71));
		Vector4__ctor_m2441427762(&L_71, G_B28_1, G_B28_0, ((float)((float)L_69*(float)(0.5f))), L_70, /*hidden argument*/NULL);
		NullCheck(G_B28_3);
		Material_SetVector_m3505096203(G_B28_3, G_B28_2, L_71, /*hidden argument*/NULL);
		Material_t3870600107 * L_72 = __this->get_dofMaterial_26();
		RenderTexture_t1963041563 * L_73 = ___source0;
		NullCheck(L_73);
		int32_t L_74 = RenderTexture_get_width_m1498578543(L_73, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_75 = ___source0;
		NullCheck(L_75);
		int32_t L_76 = RenderTexture_get_height_m4010076224(L_75, /*hidden argument*/NULL);
		Vector4_t4282066567  L_77;
		memset(&L_77, 0, sizeof(L_77));
		Vector4__ctor_m2441427762(&L_77, ((float)((float)(1.0f)/(float)((float)((float)(1.0f)*(float)(((float)((float)L_74))))))), ((float)((float)(1.0f)/(float)((float)((float)(1.0f)*(float)(((float)((float)L_76))))))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_72);
		Material_SetVector_m3505096203(L_72, _stringLiteral388325018, L_77, /*hidden argument*/NULL);
		int32_t L_78 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Int32 DepthOfField34::GetDividerBasedOnQuality() */, __this);
		V_4 = L_78;
		int32_t L_79 = V_4;
		int32_t L_80 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(22 /* System.Int32 DepthOfField34::GetLowResolutionDividerBasedOnQuality(System.Int32) */, __this, L_79);
		V_5 = L_80;
		bool L_81 = V_1;
		RenderTexture_t1963041563 * L_82 = ___source0;
		int32_t L_83 = V_4;
		int32_t L_84 = V_5;
		VirtActionInvoker4< bool, RenderTexture_t1963041563 *, int32_t, int32_t >::Invoke(30 /* System.Void DepthOfField34::AllocateTextures(System.Boolean,UnityEngine.RenderTexture,System.Int32,System.Int32) */, __this, L_81, L_82, L_83, L_84);
		RenderTexture_t1963041563 * L_85 = ___source0;
		RenderTexture_t1963041563 * L_86 = ___source0;
		Material_t3870600107 * L_87 = __this->get_dofMaterial_26();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_85, L_86, L_87, 3, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_88 = ___source0;
		RenderTexture_t1963041563 * L_89 = __this->get_mediumRezWorkTexture_42();
		VirtActionInvoker2< RenderTexture_t1963041563 *, RenderTexture_t1963041563 * >::Invoke(27 /* System.Void DepthOfField34::Downsample(UnityEngine.RenderTexture,UnityEngine.RenderTexture) */, __this, L_88, L_89);
		RenderTexture_t1963041563 * L_90 = __this->get_mediumRezWorkTexture_42();
		RenderTexture_t1963041563 * L_91 = __this->get_mediumRezWorkTexture_42();
		float L_92 = __this->get_maxBlurSpread_21();
		VirtActionInvoker5< RenderTexture_t1963041563 *, RenderTexture_t1963041563 *, int32_t, int32_t, float >::Invoke(24 /* System.Void DepthOfField34::Blur(UnityEngine.RenderTexture,UnityEngine.RenderTexture,DofBlurriness,System.Int32,System.Single) */, __this, L_90, L_91, 1, 4, L_92);
		bool L_93 = __this->get_bokeh_31();
		if (!L_93)
		{
			goto IL_039b;
		}
	}
	{
		int32_t L_94 = __this->get_bokehDestination_28();
		if (!((int32_t)((int32_t)L_94&(int32_t)1)))
		{
			goto IL_039b;
		}
	}
	{
		Material_t3870600107 * L_95 = __this->get_dofMaterial_26();
		float L_96 = __this->get_bokehThreshholdContrast_37();
		float L_97 = __this->get_bokehThreshholdLuminance_38();
		Vector4_t4282066567  L_98;
		memset(&L_98, 0, sizeof(L_98));
		Vector4__ctor_m2441427762(&L_98, L_96, L_97, (0.95f), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_95);
		Material_SetVector_m3505096203(L_95, _stringLiteral2416425146, L_98, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_99 = __this->get_mediumRezWorkTexture_42();
		RenderTexture_t1963041563 * L_100 = __this->get_bokehSource2_46();
		Material_t3870600107 * L_101 = __this->get_dofMaterial_26();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_99, L_100, L_101, ((int32_t)11), /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_102 = __this->get_mediumRezWorkTexture_42();
		RenderTexture_t1963041563 * L_103 = __this->get_lowRezWorkTexture_44();
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_102, L_103, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_104 = __this->get_lowRezWorkTexture_44();
		RenderTexture_t1963041563 * L_105 = __this->get_lowRezWorkTexture_44();
		int32_t L_106 = __this->get_bluriness_20();
		float L_107 = __this->get_maxBlurSpread_21();
		float L_108 = V_0;
		VirtActionInvoker5< RenderTexture_t1963041563 *, RenderTexture_t1963041563 *, int32_t, int32_t, float >::Invoke(24 /* System.Void DepthOfField34::Blur(UnityEngine.RenderTexture,UnityEngine.RenderTexture,DofBlurriness,System.Int32,System.Single) */, __this, L_104, L_105, L_106, 0, ((float)((float)L_107*(float)L_108)));
		goto IL_03cc;
	}

IL_039b:
	{
		RenderTexture_t1963041563 * L_109 = __this->get_mediumRezWorkTexture_42();
		RenderTexture_t1963041563 * L_110 = __this->get_lowRezWorkTexture_44();
		VirtActionInvoker2< RenderTexture_t1963041563 *, RenderTexture_t1963041563 * >::Invoke(27 /* System.Void DepthOfField34::Downsample(UnityEngine.RenderTexture,UnityEngine.RenderTexture) */, __this, L_109, L_110);
		RenderTexture_t1963041563 * L_111 = __this->get_lowRezWorkTexture_44();
		RenderTexture_t1963041563 * L_112 = __this->get_lowRezWorkTexture_44();
		int32_t L_113 = __this->get_bluriness_20();
		float L_114 = __this->get_maxBlurSpread_21();
		VirtActionInvoker5< RenderTexture_t1963041563 *, RenderTexture_t1963041563 *, int32_t, int32_t, float >::Invoke(24 /* System.Void DepthOfField34::Blur(UnityEngine.RenderTexture,UnityEngine.RenderTexture,DofBlurriness,System.Int32,System.Single) */, __this, L_111, L_112, L_113, 0, L_114);
	}

IL_03cc:
	{
		Material_t3870600107 * L_115 = __this->get_dofBlurMaterial_24();
		RenderTexture_t1963041563 * L_116 = __this->get_lowRezWorkTexture_44();
		NullCheck(L_115);
		Material_SetTexture_m1833724755(L_115, _stringLiteral911347184, L_116, /*hidden argument*/NULL);
		Material_t3870600107 * L_117 = __this->get_dofBlurMaterial_24();
		RenderTexture_t1963041563 * L_118 = __this->get_mediumRezWorkTexture_42();
		NullCheck(L_117);
		Material_SetTexture_m1833724755(L_117, _stringLiteral1474613081, L_118, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_119 = __this->get_finalDefocus_43();
		Material_t3870600107 * L_120 = __this->get_dofBlurMaterial_24();
		Graphics_Blit_m336256356(NULL /*static, unused*/, (Texture_t2526458961 *)NULL, L_119, L_120, 3, /*hidden argument*/NULL);
		bool L_121 = __this->get_bokeh_31();
		if (!L_121)
		{
			goto IL_043b;
		}
	}
	{
		int32_t L_122 = __this->get_bokehDestination_28();
		if (!((int32_t)((int32_t)L_122&(int32_t)1)))
		{
			goto IL_043b;
		}
	}
	{
		RenderTexture_t1963041563 * L_123 = __this->get_bokehSource2_46();
		RenderTexture_t1963041563 * L_124 = __this->get_bokehSource_45();
		RenderTexture_t1963041563 * L_125 = __this->get_finalDefocus_43();
		VirtActionInvoker3< RenderTexture_t1963041563 *, RenderTexture_t1963041563 *, RenderTexture_t1963041563 * >::Invoke(28 /* System.Void DepthOfField34::AddBokeh(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.RenderTexture) */, __this, L_123, L_124, L_125);
	}

IL_043b:
	{
		Material_t3870600107 * L_126 = __this->get_dofMaterial_26();
		RenderTexture_t1963041563 * L_127 = __this->get_finalDefocus_43();
		NullCheck(L_126);
		Material_SetTexture_m1833724755(L_126, _stringLiteral1387762494, L_127, /*hidden argument*/NULL);
		Material_t3870600107 * L_128 = __this->get_dofMaterial_26();
		RenderTexture_t1963041563 * L_129 = __this->get_mediumRezWorkTexture_42();
		NullCheck(L_128);
		Material_SetTexture_m1833724755(L_128, _stringLiteral1474613081, L_129, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_130 = ___source0;
		bool L_131 = V_1;
		G_B36_0 = L_130;
		if (!L_131)
		{
			G_B37_0 = L_130;
			goto IL_0479;
		}
	}
	{
		RenderTexture_t1963041563 * L_132 = __this->get_foregroundTexture_41();
		G_B38_0 = L_132;
		G_B38_1 = G_B36_0;
		goto IL_047a;
	}

IL_0479:
	{
		RenderTexture_t1963041563 * L_133 = ___destination1;
		G_B38_0 = L_133;
		G_B38_1 = G_B37_0;
	}

IL_047a:
	{
		Material_t3870600107 * L_134 = __this->get_dofMaterial_26();
		bool L_135 = __this->get_visualize_27();
		G_B39_0 = L_134;
		G_B39_1 = G_B38_0;
		G_B39_2 = G_B38_1;
		if (!L_135)
		{
			G_B40_0 = L_134;
			G_B40_1 = G_B38_0;
			G_B40_2 = G_B38_1;
			goto IL_0491;
		}
	}
	{
		G_B41_0 = 2;
		G_B41_1 = G_B39_0;
		G_B41_2 = G_B39_1;
		G_B41_3 = G_B39_2;
		goto IL_0492;
	}

IL_0491:
	{
		G_B41_0 = 0;
		G_B41_1 = G_B40_0;
		G_B41_2 = G_B40_1;
		G_B41_3 = G_B40_2;
	}

IL_0492:
	{
		Graphics_Blit_m336256356(NULL /*static, unused*/, G_B41_3, G_B41_2, G_B41_1, G_B41_0, /*hidden argument*/NULL);
		bool L_136 = V_1;
		if (!L_136)
		{
			goto IL_05fa;
		}
	}
	{
		RenderTexture_t1963041563 * L_137 = __this->get_foregroundTexture_41();
		RenderTexture_t1963041563 * L_138 = ___source0;
		Material_t3870600107 * L_139 = __this->get_dofMaterial_26();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_137, L_138, L_139, 5, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_140 = ___source0;
		RenderTexture_t1963041563 * L_141 = __this->get_mediumRezWorkTexture_42();
		VirtActionInvoker2< RenderTexture_t1963041563 *, RenderTexture_t1963041563 * >::Invoke(27 /* System.Void DepthOfField34::Downsample(UnityEngine.RenderTexture,UnityEngine.RenderTexture) */, __this, L_140, L_141);
		RenderTexture_t1963041563 * L_142 = __this->get_mediumRezWorkTexture_42();
		RenderTexture_t1963041563 * L_143 = __this->get_mediumRezWorkTexture_42();
		float L_144 = __this->get_maxBlurSpread_21();
		VirtActionInvoker5< RenderTexture_t1963041563 *, RenderTexture_t1963041563 *, int32_t, int32_t, float >::Invoke(25 /* System.Void DepthOfField34::BlurFg(UnityEngine.RenderTexture,UnityEngine.RenderTexture,DofBlurriness,System.Int32,System.Single) */, __this, L_142, L_143, 1, 2, L_144);
		bool L_145 = __this->get_bokeh_31();
		if (!L_145)
		{
			goto IL_056a;
		}
	}
	{
		int32_t L_146 = __this->get_bokehDestination_28();
		if (!((int32_t)((int32_t)L_146&(int32_t)2)))
		{
			goto IL_056a;
		}
	}
	{
		Material_t3870600107 * L_147 = __this->get_dofMaterial_26();
		float L_148 = __this->get_bokehThreshholdContrast_37();
		float L_149 = __this->get_bokehThreshholdLuminance_38();
		Vector4_t4282066567  L_150;
		memset(&L_150, 0, sizeof(L_150));
		Vector4__ctor_m2441427762(&L_150, ((float)((float)L_148*(float)(0.5f))), L_149, (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_147);
		Material_SetVector_m3505096203(L_147, _stringLiteral2416425146, L_150, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_151 = __this->get_mediumRezWorkTexture_42();
		RenderTexture_t1963041563 * L_152 = __this->get_bokehSource2_46();
		Material_t3870600107 * L_153 = __this->get_dofMaterial_26();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_151, L_152, L_153, ((int32_t)11), /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_154 = __this->get_mediumRezWorkTexture_42();
		RenderTexture_t1963041563 * L_155 = __this->get_lowRezWorkTexture_44();
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_154, L_155, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_156 = __this->get_lowRezWorkTexture_44();
		RenderTexture_t1963041563 * L_157 = __this->get_lowRezWorkTexture_44();
		int32_t L_158 = __this->get_bluriness_20();
		float L_159 = __this->get_maxBlurSpread_21();
		float L_160 = V_0;
		VirtActionInvoker5< RenderTexture_t1963041563 *, RenderTexture_t1963041563 *, int32_t, int32_t, float >::Invoke(25 /* System.Void DepthOfField34::BlurFg(UnityEngine.RenderTexture,UnityEngine.RenderTexture,DofBlurriness,System.Int32,System.Single) */, __this, L_156, L_157, L_158, 1, ((float)((float)L_159*(float)L_160)));
		goto IL_0589;
	}

IL_056a:
	{
		RenderTexture_t1963041563 * L_161 = __this->get_mediumRezWorkTexture_42();
		RenderTexture_t1963041563 * L_162 = __this->get_lowRezWorkTexture_44();
		int32_t L_163 = __this->get_bluriness_20();
		float L_164 = __this->get_maxBlurSpread_21();
		VirtActionInvoker5< RenderTexture_t1963041563 *, RenderTexture_t1963041563 *, int32_t, int32_t, float >::Invoke(25 /* System.Void DepthOfField34::BlurFg(UnityEngine.RenderTexture,UnityEngine.RenderTexture,DofBlurriness,System.Int32,System.Single) */, __this, L_161, L_162, L_163, 1, L_164);
	}

IL_0589:
	{
		RenderTexture_t1963041563 * L_165 = __this->get_lowRezWorkTexture_44();
		RenderTexture_t1963041563 * L_166 = __this->get_finalDefocus_43();
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_165, L_166, /*hidden argument*/NULL);
		Material_t3870600107 * L_167 = __this->get_dofMaterial_26();
		RenderTexture_t1963041563 * L_168 = __this->get_finalDefocus_43();
		NullCheck(L_167);
		Material_SetTexture_m1833724755(L_167, _stringLiteral409446227, L_168, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_169 = ___source0;
		RenderTexture_t1963041563 * L_170 = ___destination1;
		Material_t3870600107 * L_171 = __this->get_dofMaterial_26();
		bool L_172 = __this->get_visualize_27();
		G_B47_0 = L_171;
		G_B47_1 = L_170;
		G_B47_2 = L_169;
		if (!L_172)
		{
			G_B48_0 = L_171;
			G_B48_1 = L_170;
			G_B48_2 = L_169;
			goto IL_05c9;
		}
	}
	{
		G_B49_0 = 1;
		G_B49_1 = G_B47_0;
		G_B49_2 = G_B47_1;
		G_B49_3 = G_B47_2;
		goto IL_05ca;
	}

IL_05c9:
	{
		G_B49_0 = 4;
		G_B49_1 = G_B48_0;
		G_B49_2 = G_B48_1;
		G_B49_3 = G_B48_2;
	}

IL_05ca:
	{
		Graphics_Blit_m336256356(NULL /*static, unused*/, G_B49_3, G_B49_2, G_B49_1, G_B49_0, /*hidden argument*/NULL);
		bool L_173 = __this->get_bokeh_31();
		if (!L_173)
		{
			goto IL_05fa;
		}
	}
	{
		int32_t L_174 = __this->get_bokehDestination_28();
		if (!((int32_t)((int32_t)L_174&(int32_t)2)))
		{
			goto IL_05fa;
		}
	}
	{
		RenderTexture_t1963041563 * L_175 = __this->get_bokehSource2_46();
		RenderTexture_t1963041563 * L_176 = __this->get_bokehSource_45();
		RenderTexture_t1963041563 * L_177 = ___destination1;
		VirtActionInvoker3< RenderTexture_t1963041563 *, RenderTexture_t1963041563 *, RenderTexture_t1963041563 * >::Invoke(28 /* System.Void DepthOfField34::AddBokeh(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.RenderTexture) */, __this, L_175, L_176, L_177);
	}

IL_05fa:
	{
		VirtActionInvoker0::Invoke(29 /* System.Void DepthOfField34::ReleaseTextures() */, __this);
	}

IL_0600:
	{
		return;
	}
}
// System.Void DepthOfField34::Blur(UnityEngine.RenderTexture,UnityEngine.RenderTexture,DofBlurriness,System.Int32,System.Single)
extern Il2CppCodeGenString* _stringLiteral2746560064;
extern const uint32_t DepthOfField34_Blur_m3990620742_MetadataUsageId;
extern "C"  void DepthOfField34_Blur_m3990620742 (DepthOfField34_t2156099489 * __this, RenderTexture_t1963041563 * ___from0, RenderTexture_t1963041563 * ___to1, int32_t ___iterations2, int32_t ___blurPass3, float ___spread4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DepthOfField34_Blur_m3990620742_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RenderTexture_t1963041563 * V_0 = NULL;
	{
		RenderTexture_t1963041563 * L_0 = ___to1;
		NullCheck(L_0);
		int32_t L_1 = RenderTexture_get_width_m1498578543(L_0, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_2 = ___to1;
		NullCheck(L_2);
		int32_t L_3 = RenderTexture_get_height_m4010076224(L_2, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_4 = RenderTexture_GetTemporary_m469965696(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = ___iterations2;
		if ((((int32_t)L_5) <= ((int32_t)1)))
		{
			goto IL_00b1;
		}
	}
	{
		RenderTexture_t1963041563 * L_6 = ___from0;
		RenderTexture_t1963041563 * L_7 = ___to1;
		int32_t L_8 = ___blurPass3;
		float L_9 = ___spread4;
		RenderTexture_t1963041563 * L_10 = V_0;
		VirtActionInvoker5< RenderTexture_t1963041563 *, RenderTexture_t1963041563 *, int32_t, float, RenderTexture_t1963041563 * >::Invoke(26 /* System.Void DepthOfField34::BlurHex(UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Int32,System.Single,UnityEngine.RenderTexture) */, __this, L_6, L_7, L_8, L_9, L_10);
		int32_t L_11 = ___iterations2;
		if ((((int32_t)L_11) <= ((int32_t)2)))
		{
			goto IL_00ac;
		}
	}
	{
		Material_t3870600107 * L_12 = __this->get_dofBlurMaterial_24();
		float L_13 = ___spread4;
		float L_14 = __this->get_oneOverBaseSize_30();
		Vector4_t4282066567  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Vector4__ctor_m2441427762(&L_15, (((float)((float)0))), ((float)((float)L_13*(float)L_14)), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_12);
		Material_SetVector_m3505096203(L_12, _stringLiteral2746560064, L_15, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_16 = ___to1;
		RenderTexture_t1963041563 * L_17 = V_0;
		Material_t3870600107 * L_18 = __this->get_dofBlurMaterial_24();
		int32_t L_19 = ___blurPass3;
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_16, L_17, L_18, L_19, /*hidden argument*/NULL);
		Material_t3870600107 * L_20 = __this->get_dofBlurMaterial_24();
		float L_21 = ___spread4;
		float L_22 = __this->get_widthOverHeight_29();
		float L_23 = __this->get_oneOverBaseSize_30();
		Vector4_t4282066567  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector4__ctor_m2441427762(&L_24, ((float)((float)((float)((float)L_21/(float)L_22))*(float)L_23)), (((float)((float)0))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_20);
		Material_SetVector_m3505096203(L_20, _stringLiteral2746560064, L_24, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_25 = V_0;
		RenderTexture_t1963041563 * L_26 = ___to1;
		Material_t3870600107 * L_27 = __this->get_dofBlurMaterial_24();
		int32_t L_28 = ___blurPass3;
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_25, L_26, L_27, L_28, /*hidden argument*/NULL);
	}

IL_00ac:
	{
		goto IL_012a;
	}

IL_00b1:
	{
		Material_t3870600107 * L_29 = __this->get_dofBlurMaterial_24();
		float L_30 = ___spread4;
		float L_31 = __this->get_oneOverBaseSize_30();
		Vector4_t4282066567  L_32;
		memset(&L_32, 0, sizeof(L_32));
		Vector4__ctor_m2441427762(&L_32, (((float)((float)0))), ((float)((float)L_30*(float)L_31)), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_29);
		Material_SetVector_m3505096203(L_29, _stringLiteral2746560064, L_32, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_33 = ___from0;
		RenderTexture_t1963041563 * L_34 = V_0;
		Material_t3870600107 * L_35 = __this->get_dofBlurMaterial_24();
		int32_t L_36 = ___blurPass3;
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_33, L_34, L_35, L_36, /*hidden argument*/NULL);
		Material_t3870600107 * L_37 = __this->get_dofBlurMaterial_24();
		float L_38 = ___spread4;
		float L_39 = __this->get_widthOverHeight_29();
		float L_40 = __this->get_oneOverBaseSize_30();
		Vector4_t4282066567  L_41;
		memset(&L_41, 0, sizeof(L_41));
		Vector4__ctor_m2441427762(&L_41, ((float)((float)((float)((float)L_38/(float)L_39))*(float)L_40)), (((float)((float)0))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_37);
		Material_SetVector_m3505096203(L_37, _stringLiteral2746560064, L_41, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_42 = V_0;
		RenderTexture_t1963041563 * L_43 = ___to1;
		Material_t3870600107 * L_44 = __this->get_dofBlurMaterial_24();
		int32_t L_45 = ___blurPass3;
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_42, L_43, L_44, L_45, /*hidden argument*/NULL);
	}

IL_012a:
	{
		RenderTexture_t1963041563 * L_46 = V_0;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DepthOfField34::BlurFg(UnityEngine.RenderTexture,UnityEngine.RenderTexture,DofBlurriness,System.Int32,System.Single)
extern Il2CppCodeGenString* _stringLiteral2481833606;
extern Il2CppCodeGenString* _stringLiteral2746560064;
extern const uint32_t DepthOfField34_BlurFg_m767514023_MetadataUsageId;
extern "C"  void DepthOfField34_BlurFg_m767514023 (DepthOfField34_t2156099489 * __this, RenderTexture_t1963041563 * ___from0, RenderTexture_t1963041563 * ___to1, int32_t ___iterations2, int32_t ___blurPass3, float ___spread4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DepthOfField34_BlurFg_m767514023_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RenderTexture_t1963041563 * V_0 = NULL;
	{
		Material_t3870600107 * L_0 = __this->get_dofBlurMaterial_24();
		RenderTexture_t1963041563 * L_1 = ___from0;
		NullCheck(L_0);
		Material_SetTexture_m1833724755(L_0, _stringLiteral2481833606, L_1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_2 = ___to1;
		NullCheck(L_2);
		int32_t L_3 = RenderTexture_get_width_m1498578543(L_2, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_4 = ___to1;
		NullCheck(L_4);
		int32_t L_5 = RenderTexture_get_height_m4010076224(L_4, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_6 = RenderTexture_GetTemporary_m469965696(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		int32_t L_7 = ___iterations2;
		if ((((int32_t)L_7) <= ((int32_t)1)))
		{
			goto IL_00c2;
		}
	}
	{
		RenderTexture_t1963041563 * L_8 = ___from0;
		RenderTexture_t1963041563 * L_9 = ___to1;
		int32_t L_10 = ___blurPass3;
		float L_11 = ___spread4;
		RenderTexture_t1963041563 * L_12 = V_0;
		VirtActionInvoker5< RenderTexture_t1963041563 *, RenderTexture_t1963041563 *, int32_t, float, RenderTexture_t1963041563 * >::Invoke(26 /* System.Void DepthOfField34::BlurHex(UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Int32,System.Single,UnityEngine.RenderTexture) */, __this, L_8, L_9, L_10, L_11, L_12);
		int32_t L_13 = ___iterations2;
		if ((((int32_t)L_13) <= ((int32_t)2)))
		{
			goto IL_00bd;
		}
	}
	{
		Material_t3870600107 * L_14 = __this->get_dofBlurMaterial_24();
		float L_15 = ___spread4;
		float L_16 = __this->get_oneOverBaseSize_30();
		Vector4_t4282066567  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector4__ctor_m2441427762(&L_17, (((float)((float)0))), ((float)((float)L_15*(float)L_16)), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_14);
		Material_SetVector_m3505096203(L_14, _stringLiteral2746560064, L_17, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_18 = ___to1;
		RenderTexture_t1963041563 * L_19 = V_0;
		Material_t3870600107 * L_20 = __this->get_dofBlurMaterial_24();
		int32_t L_21 = ___blurPass3;
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_18, L_19, L_20, L_21, /*hidden argument*/NULL);
		Material_t3870600107 * L_22 = __this->get_dofBlurMaterial_24();
		float L_23 = ___spread4;
		float L_24 = __this->get_widthOverHeight_29();
		float L_25 = __this->get_oneOverBaseSize_30();
		Vector4_t4282066567  L_26;
		memset(&L_26, 0, sizeof(L_26));
		Vector4__ctor_m2441427762(&L_26, ((float)((float)((float)((float)L_23/(float)L_24))*(float)L_25)), (((float)((float)0))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_22);
		Material_SetVector_m3505096203(L_22, _stringLiteral2746560064, L_26, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_27 = V_0;
		RenderTexture_t1963041563 * L_28 = ___to1;
		Material_t3870600107 * L_29 = __this->get_dofBlurMaterial_24();
		int32_t L_30 = ___blurPass3;
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_27, L_28, L_29, L_30, /*hidden argument*/NULL);
	}

IL_00bd:
	{
		goto IL_013b;
	}

IL_00c2:
	{
		Material_t3870600107 * L_31 = __this->get_dofBlurMaterial_24();
		float L_32 = ___spread4;
		float L_33 = __this->get_oneOverBaseSize_30();
		Vector4_t4282066567  L_34;
		memset(&L_34, 0, sizeof(L_34));
		Vector4__ctor_m2441427762(&L_34, (((float)((float)0))), ((float)((float)L_32*(float)L_33)), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_31);
		Material_SetVector_m3505096203(L_31, _stringLiteral2746560064, L_34, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_35 = ___from0;
		RenderTexture_t1963041563 * L_36 = V_0;
		Material_t3870600107 * L_37 = __this->get_dofBlurMaterial_24();
		int32_t L_38 = ___blurPass3;
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_35, L_36, L_37, L_38, /*hidden argument*/NULL);
		Material_t3870600107 * L_39 = __this->get_dofBlurMaterial_24();
		float L_40 = ___spread4;
		float L_41 = __this->get_widthOverHeight_29();
		float L_42 = __this->get_oneOverBaseSize_30();
		Vector4_t4282066567  L_43;
		memset(&L_43, 0, sizeof(L_43));
		Vector4__ctor_m2441427762(&L_43, ((float)((float)((float)((float)L_40/(float)L_41))*(float)L_42)), (((float)((float)0))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_39);
		Material_SetVector_m3505096203(L_39, _stringLiteral2746560064, L_43, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_44 = V_0;
		RenderTexture_t1963041563 * L_45 = ___to1;
		Material_t3870600107 * L_46 = __this->get_dofBlurMaterial_24();
		int32_t L_47 = ___blurPass3;
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_44, L_45, L_46, L_47, /*hidden argument*/NULL);
	}

IL_013b:
	{
		RenderTexture_t1963041563 * L_48 = V_0;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DepthOfField34::BlurHex(UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Int32,System.Single,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral2746560064;
extern const uint32_t DepthOfField34_BlurHex_m2599315629_MetadataUsageId;
extern "C"  void DepthOfField34_BlurHex_m2599315629 (DepthOfField34_t2156099489 * __this, RenderTexture_t1963041563 * ___from0, RenderTexture_t1963041563 * ___to1, int32_t ___blurPass2, float ___spread3, RenderTexture_t1963041563 * ___tmp4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DepthOfField34_BlurHex_m2599315629_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t3870600107 * L_0 = __this->get_dofBlurMaterial_24();
		float L_1 = ___spread3;
		float L_2 = __this->get_oneOverBaseSize_30();
		Vector4_t4282066567  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector4__ctor_m2441427762(&L_3, (((float)((float)0))), ((float)((float)L_1*(float)L_2)), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_0);
		Material_SetVector_m3505096203(L_0, _stringLiteral2746560064, L_3, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_4 = ___from0;
		RenderTexture_t1963041563 * L_5 = ___tmp4;
		Material_t3870600107 * L_6 = __this->get_dofBlurMaterial_24();
		int32_t L_7 = ___blurPass2;
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		Material_t3870600107 * L_8 = __this->get_dofBlurMaterial_24();
		float L_9 = ___spread3;
		float L_10 = __this->get_widthOverHeight_29();
		float L_11 = __this->get_oneOverBaseSize_30();
		Vector4_t4282066567  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector4__ctor_m2441427762(&L_12, ((float)((float)((float)((float)L_9/(float)L_10))*(float)L_11)), (((float)((float)0))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_8);
		Material_SetVector_m3505096203(L_8, _stringLiteral2746560064, L_12, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_13 = ___tmp4;
		RenderTexture_t1963041563 * L_14 = ___to1;
		Material_t3870600107 * L_15 = __this->get_dofBlurMaterial_24();
		int32_t L_16 = ___blurPass2;
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_13, L_14, L_15, L_16, /*hidden argument*/NULL);
		Material_t3870600107 * L_17 = __this->get_dofBlurMaterial_24();
		float L_18 = ___spread3;
		float L_19 = __this->get_widthOverHeight_29();
		float L_20 = __this->get_oneOverBaseSize_30();
		float L_21 = ___spread3;
		float L_22 = __this->get_oneOverBaseSize_30();
		Vector4_t4282066567  L_23;
		memset(&L_23, 0, sizeof(L_23));
		Vector4__ctor_m2441427762(&L_23, ((float)((float)((float)((float)L_18/(float)L_19))*(float)L_20)), ((float)((float)L_21*(float)L_22)), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_17);
		Material_SetVector_m3505096203(L_17, _stringLiteral2746560064, L_23, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_24 = ___to1;
		RenderTexture_t1963041563 * L_25 = ___tmp4;
		Material_t3870600107 * L_26 = __this->get_dofBlurMaterial_24();
		int32_t L_27 = ___blurPass2;
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_24, L_25, L_26, L_27, /*hidden argument*/NULL);
		Material_t3870600107 * L_28 = __this->get_dofBlurMaterial_24();
		float L_29 = ___spread3;
		float L_30 = __this->get_widthOverHeight_29();
		float L_31 = __this->get_oneOverBaseSize_30();
		float L_32 = ___spread3;
		float L_33 = __this->get_oneOverBaseSize_30();
		Vector4_t4282066567  L_34;
		memset(&L_34, 0, sizeof(L_34));
		Vector4__ctor_m2441427762(&L_34, ((float)((float)((float)((float)L_29/(float)L_30))*(float)L_31)), ((float)((float)((-L_32))*(float)L_33)), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_28);
		Material_SetVector_m3505096203(L_28, _stringLiteral2746560064, L_34, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_35 = ___tmp4;
		RenderTexture_t1963041563 * L_36 = ___to1;
		Material_t3870600107 * L_37 = __this->get_dofBlurMaterial_24();
		int32_t L_38 = ___blurPass2;
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_35, L_36, L_37, L_38, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DepthOfField34::Downsample(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* DepthOfField34_t2156099489_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral388325018;
extern const uint32_t DepthOfField34_Downsample_m1940966517_MetadataUsageId;
extern "C"  void DepthOfField34_Downsample_m1940966517 (DepthOfField34_t2156099489 * __this, RenderTexture_t1963041563 * ___from0, RenderTexture_t1963041563 * ___to1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DepthOfField34_Downsample_m1940966517_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t3870600107 * L_0 = __this->get_dofMaterial_26();
		RenderTexture_t1963041563 * L_1 = ___to1;
		NullCheck(L_1);
		int32_t L_2 = RenderTexture_get_width_m1498578543(L_1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_3 = ___to1;
		NullCheck(L_3);
		int32_t L_4 = RenderTexture_get_height_m4010076224(L_3, /*hidden argument*/NULL);
		Vector4_t4282066567  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector4__ctor_m2441427762(&L_5, ((float)((float)(1.0f)/(float)((float)((float)(1.0f)*(float)(((float)((float)L_2))))))), ((float)((float)(1.0f)/(float)((float)((float)(1.0f)*(float)(((float)((float)L_4))))))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_0);
		Material_SetVector_m3505096203(L_0, _stringLiteral388325018, L_5, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_6 = ___from0;
		RenderTexture_t1963041563 * L_7 = ___to1;
		Material_t3870600107 * L_8 = __this->get_dofMaterial_26();
		IL2CPP_RUNTIME_CLASS_INIT(DepthOfField34_t2156099489_il2cpp_TypeInfo_var);
		int32_t L_9 = ((DepthOfField34_t2156099489_StaticFields*)DepthOfField34_t2156099489_il2cpp_TypeInfo_var->static_fields)->get_SMOOTH_DOWNSAMPLE_PASS_5();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DepthOfField34::AddBokeh(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* Quads_t78387180_il2cpp_TypeInfo_var;
extern Il2CppClass* DepthOfField34_t2156099489_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral895832410;
extern Il2CppCodeGenString* _stringLiteral558922319;
extern Il2CppCodeGenString* _stringLiteral3259843130;
extern Il2CppCodeGenString* _stringLiteral1014379156;
extern const uint32_t DepthOfField34_AddBokeh_m903980319_MetadataUsageId;
extern "C"  void DepthOfField34_AddBokeh_m903980319 (DepthOfField34_t2156099489 * __this, RenderTexture_t1963041563 * ___bokehInfo0, RenderTexture_t1963041563 * ___tempTex1, RenderTexture_t1963041563 * ___finalTarget2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DepthOfField34_AddBokeh_m903980319_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MeshU5BU5D_t1759126828* V_0 = NULL;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Mesh_t4241756145 * V_3 = NULL;
	int32_t V_4 = 0;
	MeshU5BU5D_t1759126828* V_5 = NULL;
	int32_t V_6 = 0;
	{
		Material_t3870600107 * L_0 = __this->get_bokehMaterial_40();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0164;
		}
	}
	{
		RenderTexture_t1963041563 * L_2 = ___tempTex1;
		NullCheck(L_2);
		int32_t L_3 = RenderTexture_get_width_m1498578543(L_2, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_4 = ___tempTex1;
		NullCheck(L_4);
		int32_t L_5 = RenderTexture_get_height_m4010076224(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t78387180_il2cpp_TypeInfo_var);
		MeshU5BU5D_t1759126828* L_6 = Quads_GetMeshes_m2562620632(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		RenderTexture_t1963041563 * L_7 = ___tempTex1;
		RenderTexture_set_active_m1002947377(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Color_t4194546905  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Color__ctor_m2252924356(&L_8, (((float)((float)0))), (((float)((float)0))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		GL_Clear_m2980983731(NULL /*static, unused*/, (bool)0, (bool)1, L_8, /*hidden argument*/NULL);
		GL_PushMatrix_m626765559(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_LoadIdentity_m1417984576(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_9 = ___bokehInfo0;
		NullCheck(L_9);
		Texture_set_filterMode_m3842701708(L_9, 0, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_10 = ___bokehInfo0;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_width_m1498578543(L_10, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_12 = ___bokehInfo0;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_height_m4010076224(L_12, /*hidden argument*/NULL);
		V_1 = ((float)((float)((float)((float)(((float)((float)L_11)))*(float)(1.0f)))/(float)((float)((float)(((float)((float)L_13)))*(float)(1.0f)))));
		RenderTexture_t1963041563 * L_14 = ___bokehInfo0;
		NullCheck(L_14);
		int32_t L_15 = RenderTexture_get_width_m1498578543(L_14, /*hidden argument*/NULL);
		V_2 = ((float)((float)(2.0f)/(float)((float)((float)(1.0f)*(float)(((float)((float)L_15)))))));
		float L_16 = V_2;
		float L_17 = __this->get_bokehScale_35();
		float L_18 = __this->get_maxBlurSpread_21();
		IL2CPP_RUNTIME_CLASS_INIT(DepthOfField34_t2156099489_il2cpp_TypeInfo_var);
		float L_19 = ((DepthOfField34_t2156099489_StaticFields*)DepthOfField34_t2156099489_il2cpp_TypeInfo_var->static_fields)->get_BOKEH_EXTRA_BLUR_6();
		float L_20 = __this->get_oneOverBaseSize_30();
		V_2 = ((float)((float)L_16+(float)((float)((float)((float)((float)((float)((float)L_17*(float)L_18))*(float)L_19))*(float)L_20))));
		Material_t3870600107 * L_21 = __this->get_bokehMaterial_40();
		RenderTexture_t1963041563 * L_22 = ___bokehInfo0;
		NullCheck(L_21);
		Material_SetTexture_m1833724755(L_21, _stringLiteral895832410, L_22, /*hidden argument*/NULL);
		Material_t3870600107 * L_23 = __this->get_bokehMaterial_40();
		Texture2D_t3884108195 * L_24 = __this->get_bokehTexture_34();
		NullCheck(L_23);
		Material_SetTexture_m1833724755(L_23, _stringLiteral558922319, L_24, /*hidden argument*/NULL);
		Material_t3870600107 * L_25 = __this->get_bokehMaterial_40();
		float L_26 = V_2;
		float L_27 = V_2;
		float L_28 = V_1;
		float L_29 = V_1;
		Vector4_t4282066567  L_30;
		memset(&L_30, 0, sizeof(L_30));
		Vector4__ctor_m2441427762(&L_30, L_26, ((float)((float)L_27*(float)L_28)), (0.5f), ((float)((float)(0.5f)*(float)L_29)), /*hidden argument*/NULL);
		NullCheck(L_25);
		Material_SetVector_m3505096203(L_25, _stringLiteral3259843130, L_30, /*hidden argument*/NULL);
		Material_t3870600107 * L_31 = __this->get_bokehMaterial_40();
		float L_32 = __this->get_bokehIntensity_36();
		NullCheck(L_31);
		Material_SetFloat_m981710063(L_31, _stringLiteral1014379156, L_32, /*hidden argument*/NULL);
		Material_t3870600107 * L_33 = __this->get_bokehMaterial_40();
		NullCheck(L_33);
		Material_SetPass_m4241824642(L_33, 0, /*hidden argument*/NULL);
		V_4 = 0;
		MeshU5BU5D_t1759126828* L_34 = V_0;
		V_5 = L_34;
		MeshU5BU5D_t1759126828* L_35 = V_5;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_35);
		int32_t L_36 = Array_get_Length_m1203127607((Il2CppArray *)(Il2CppArray *)L_35, /*hidden argument*/NULL);
		V_6 = L_36;
		goto IL_0141;
	}

IL_011d:
	{
		MeshU5BU5D_t1759126828* L_37 = V_5;
		int32_t L_38 = V_4;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, L_38);
		int32_t L_39 = L_38;
		Mesh_t4241756145 * L_40 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		bool L_41 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_013b;
		}
	}
	{
		MeshU5BU5D_t1759126828* L_42 = V_5;
		int32_t L_43 = V_4;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, L_43);
		int32_t L_44 = L_43;
		Mesh_t4241756145 * L_45 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_44));
		Matrix4x4_t1651859333  L_46 = Matrix4x4_get_identity_m3946683782(NULL /*static, unused*/, /*hidden argument*/NULL);
		Graphics_DrawMeshNow_m2524242293(NULL /*static, unused*/, L_45, L_46, /*hidden argument*/NULL);
	}

IL_013b:
	{
		int32_t L_47 = V_4;
		V_4 = ((int32_t)((int32_t)L_47+(int32_t)1));
	}

IL_0141:
	{
		int32_t L_48 = V_4;
		int32_t L_49 = V_6;
		if ((((int32_t)L_48) < ((int32_t)L_49)))
		{
			goto IL_011d;
		}
	}
	{
		GL_PopMatrix_m3073322328(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_50 = ___tempTex1;
		RenderTexture_t1963041563 * L_51 = ___finalTarget2;
		Material_t3870600107 * L_52 = __this->get_dofMaterial_26();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_50, L_51, L_52, 8, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_53 = ___bokehInfo0;
		NullCheck(L_53);
		Texture_set_filterMode_m3842701708(L_53, 1, /*hidden argument*/NULL);
	}

IL_0164:
	{
		return;
	}
}
// System.Void DepthOfField34::ReleaseTextures()
extern "C"  void DepthOfField34_ReleaseTextures_m113527296 (DepthOfField34_t2156099489 * __this, const MethodInfo* method)
{
	{
		RenderTexture_t1963041563 * L_0 = __this->get_foregroundTexture_41();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		RenderTexture_t1963041563 * L_2 = __this->get_foregroundTexture_41();
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		RenderTexture_t1963041563 * L_3 = __this->get_finalDefocus_43();
		bool L_4 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		RenderTexture_t1963041563 * L_5 = __this->get_finalDefocus_43();
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0036:
	{
		RenderTexture_t1963041563 * L_6 = __this->get_mediumRezWorkTexture_42();
		bool L_7 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0051;
		}
	}
	{
		RenderTexture_t1963041563 * L_8 = __this->get_mediumRezWorkTexture_42();
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0051:
	{
		RenderTexture_t1963041563 * L_9 = __this->get_lowRezWorkTexture_44();
		bool L_10 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006c;
		}
	}
	{
		RenderTexture_t1963041563 * L_11 = __this->get_lowRezWorkTexture_44();
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
	}

IL_006c:
	{
		RenderTexture_t1963041563 * L_12 = __this->get_bokehSource_45();
		bool L_13 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0087;
		}
	}
	{
		RenderTexture_t1963041563 * L_14 = __this->get_bokehSource_45();
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
	}

IL_0087:
	{
		RenderTexture_t1963041563 * L_15 = __this->get_bokehSource2_46();
		bool L_16 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00a2;
		}
	}
	{
		RenderTexture_t1963041563 * L_17 = __this->get_bokehSource2_46();
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
	}

IL_00a2:
	{
		return;
	}
}
// System.Void DepthOfField34::AllocateTextures(System.Boolean,UnityEngine.RenderTexture,System.Int32,System.Int32)
extern "C"  void DepthOfField34_AllocateTextures_m2541777835 (DepthOfField34_t2156099489 * __this, bool ___blurForeground0, RenderTexture_t1963041563 * ___source1, int32_t ___divider2, int32_t ___lowTexDivider3, const MethodInfo* method)
{
	{
		__this->set_foregroundTexture_41((RenderTexture_t1963041563 *)NULL);
		bool L_0 = ___blurForeground0;
		if (!L_0)
		{
			goto IL_0025;
		}
	}
	{
		RenderTexture_t1963041563 * L_1 = ___source1;
		NullCheck(L_1);
		int32_t L_2 = RenderTexture_get_width_m1498578543(L_1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_3 = ___source1;
		NullCheck(L_3);
		int32_t L_4 = RenderTexture_get_height_m4010076224(L_3, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_5 = RenderTexture_GetTemporary_m52998487(NULL /*static, unused*/, L_2, L_4, 0, /*hidden argument*/NULL);
		__this->set_foregroundTexture_41(L_5);
	}

IL_0025:
	{
		RenderTexture_t1963041563 * L_6 = ___source1;
		NullCheck(L_6);
		int32_t L_7 = RenderTexture_get_width_m1498578543(L_6, /*hidden argument*/NULL);
		int32_t L_8 = ___divider2;
		RenderTexture_t1963041563 * L_9 = ___source1;
		NullCheck(L_9);
		int32_t L_10 = RenderTexture_get_height_m4010076224(L_9, /*hidden argument*/NULL);
		int32_t L_11 = ___divider2;
		RenderTexture_t1963041563 * L_12 = RenderTexture_GetTemporary_m52998487(NULL /*static, unused*/, ((int32_t)((int32_t)L_7/(int32_t)L_8)), ((int32_t)((int32_t)L_10/(int32_t)L_11)), 0, /*hidden argument*/NULL);
		__this->set_mediumRezWorkTexture_42(L_12);
		RenderTexture_t1963041563 * L_13 = ___source1;
		NullCheck(L_13);
		int32_t L_14 = RenderTexture_get_width_m1498578543(L_13, /*hidden argument*/NULL);
		int32_t L_15 = ___divider2;
		RenderTexture_t1963041563 * L_16 = ___source1;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_height_m4010076224(L_16, /*hidden argument*/NULL);
		int32_t L_18 = ___divider2;
		RenderTexture_t1963041563 * L_19 = RenderTexture_GetTemporary_m52998487(NULL /*static, unused*/, ((int32_t)((int32_t)L_14/(int32_t)L_15)), ((int32_t)((int32_t)L_17/(int32_t)L_18)), 0, /*hidden argument*/NULL);
		__this->set_finalDefocus_43(L_19);
		RenderTexture_t1963041563 * L_20 = ___source1;
		NullCheck(L_20);
		int32_t L_21 = RenderTexture_get_width_m1498578543(L_20, /*hidden argument*/NULL);
		int32_t L_22 = ___lowTexDivider3;
		RenderTexture_t1963041563 * L_23 = ___source1;
		NullCheck(L_23);
		int32_t L_24 = RenderTexture_get_height_m4010076224(L_23, /*hidden argument*/NULL);
		int32_t L_25 = ___lowTexDivider3;
		RenderTexture_t1963041563 * L_26 = RenderTexture_GetTemporary_m52998487(NULL /*static, unused*/, ((int32_t)((int32_t)L_21/(int32_t)L_22)), ((int32_t)((int32_t)L_24/(int32_t)L_25)), 0, /*hidden argument*/NULL);
		__this->set_lowRezWorkTexture_44(L_26);
		__this->set_bokehSource_45((RenderTexture_t1963041563 *)NULL);
		__this->set_bokehSource2_46((RenderTexture_t1963041563 *)NULL);
		bool L_27 = __this->get_bokeh_31();
		if (!L_27)
		{
			goto IL_0137;
		}
	}
	{
		RenderTexture_t1963041563 * L_28 = ___source1;
		NullCheck(L_28);
		int32_t L_29 = RenderTexture_get_width_m1498578543(L_28, /*hidden argument*/NULL);
		int32_t L_30 = ___lowTexDivider3;
		int32_t L_31 = __this->get_bokehDownsample_39();
		RenderTexture_t1963041563 * L_32 = ___source1;
		NullCheck(L_32);
		int32_t L_33 = RenderTexture_get_height_m4010076224(L_32, /*hidden argument*/NULL);
		int32_t L_34 = ___lowTexDivider3;
		int32_t L_35 = __this->get_bokehDownsample_39();
		RenderTexture_t1963041563 * L_36 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, ((int32_t)((int32_t)L_29/(int32_t)((int32_t)((int32_t)L_30*(int32_t)L_31)))), ((int32_t)((int32_t)L_33/(int32_t)((int32_t)((int32_t)L_34*(int32_t)L_35)))), 0, 2, /*hidden argument*/NULL);
		__this->set_bokehSource_45(L_36);
		RenderTexture_t1963041563 * L_37 = ___source1;
		NullCheck(L_37);
		int32_t L_38 = RenderTexture_get_width_m1498578543(L_37, /*hidden argument*/NULL);
		int32_t L_39 = ___lowTexDivider3;
		int32_t L_40 = __this->get_bokehDownsample_39();
		RenderTexture_t1963041563 * L_41 = ___source1;
		NullCheck(L_41);
		int32_t L_42 = RenderTexture_get_height_m4010076224(L_41, /*hidden argument*/NULL);
		int32_t L_43 = ___lowTexDivider3;
		int32_t L_44 = __this->get_bokehDownsample_39();
		RenderTexture_t1963041563 * L_45 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, ((int32_t)((int32_t)L_38/(int32_t)((int32_t)((int32_t)L_39*(int32_t)L_40)))), ((int32_t)((int32_t)L_42/(int32_t)((int32_t)((int32_t)L_43*(int32_t)L_44)))), 0, 2, /*hidden argument*/NULL);
		__this->set_bokehSource2_46(L_45);
		RenderTexture_t1963041563 * L_46 = __this->get_bokehSource_45();
		NullCheck(L_46);
		Texture_set_filterMode_m3842701708(L_46, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_47 = __this->get_bokehSource2_46();
		NullCheck(L_47);
		Texture_set_filterMode_m3842701708(L_47, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_48 = __this->get_bokehSource2_46();
		RenderTexture_set_active_m1002947377(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		Color_t4194546905  L_49;
		memset(&L_49, 0, sizeof(L_49));
		Color__ctor_m2252924356(&L_49, (((float)((float)0))), (((float)((float)0))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		GL_Clear_m2980983731(NULL /*static, unused*/, (bool)0, (bool)1, L_49, /*hidden argument*/NULL);
	}

IL_0137:
	{
		RenderTexture_t1963041563 * L_50 = ___source1;
		NullCheck(L_50);
		Texture_set_filterMode_m3842701708(L_50, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_51 = __this->get_finalDefocus_43();
		NullCheck(L_51);
		Texture_set_filterMode_m3842701708(L_51, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_52 = __this->get_mediumRezWorkTexture_42();
		NullCheck(L_52);
		Texture_set_filterMode_m3842701708(L_52, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_53 = __this->get_lowRezWorkTexture_44();
		NullCheck(L_53);
		Texture_set_filterMode_m3842701708(L_53, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_54 = __this->get_foregroundTexture_41();
		bool L_55 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_017e;
		}
	}
	{
		RenderTexture_t1963041563 * L_56 = __this->get_foregroundTexture_41();
		NullCheck(L_56);
		Texture_set_filterMode_m3842701708(L_56, 1, /*hidden argument*/NULL);
	}

IL_017e:
	{
		return;
	}
}
// System.Void DepthOfField34::Main()
extern "C"  void DepthOfField34_Main_m2233692058 (DepthOfField34_t2156099489 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DepthOfFieldScatter::.ctor()
extern "C"  void DepthOfFieldScatter__ctor_m2805829316 (DepthOfFieldScatter_t1867756894 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase__ctor_m3869393199(__this, /*hidden argument*/NULL);
		__this->set_focalLength_6((10.0f));
		__this->set_focalSize_7((0.05f));
		__this->set_aperture_8((11.5f));
		__this->set_maxBlurSize_10((2.0f));
		__this->set_blurType_12(0);
		__this->set_blurSampleCount_13(2);
		__this->set_foregroundOverlap_15((1.0f));
		__this->set_dx11BokehThreshhold_20((0.5f));
		__this->set_dx11SpawnHeuristic_21((0.0875f));
		__this->set_dx11BokehScale_23((1.2f));
		__this->set_dx11BokehIntensity_24((2.5f));
		__this->set_focalDistance01_25((10.0f));
		__this->set_internalBlurWidth_28((1.0f));
		return;
	}
}
// System.Boolean DepthOfFieldScatter::CheckResources()
extern "C"  bool DepthOfFieldScatter_CheckResources_m1510947159 (DepthOfFieldScatter_t1867756894 * __this, const MethodInfo* method)
{
	{
		VirtFuncInvoker1< bool, bool >::Invoke(10 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean) */, __this, (bool)1);
		Shader_t3191267369 * L_0 = __this->get_dofHdrShader_16();
		Material_t3870600107 * L_1 = __this->get_dofHdrMaterial_17();
		Material_t3870600107 * L_2 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_0, L_1);
		__this->set_dofHdrMaterial_17(L_2);
		bool L_3 = ((PostEffectsBase_t1820837395 *)__this)->get_supportDX11_3();
		if (!L_3)
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_4 = __this->get_blurType_12();
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0055;
		}
	}
	{
		Shader_t3191267369 * L_5 = __this->get_dx11BokehShader_18();
		Material_t3870600107 * L_6 = __this->get_dx11bokehMaterial_19();
		Material_t3870600107 * L_7 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_5, L_6);
		__this->set_dx11bokehMaterial_19(L_7);
		VirtActionInvoker0::Invoke(20 /* System.Void DepthOfFieldScatter::CreateComputeResources() */, __this);
	}

IL_0055:
	{
		bool L_8 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		if (L_8)
		{
			goto IL_0066;
		}
	}
	{
		VirtActionInvoker0::Invoke(13 /* System.Void PostEffectsBase::ReportAutoDisable() */, __this);
	}

IL_0066:
	{
		bool L_9 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		return L_9;
	}
}
// System.Void DepthOfFieldScatter::OnEnable()
extern const MethodInfo* Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var;
extern const uint32_t DepthOfFieldScatter_OnEnable_m1879216706_MetadataUsageId;
extern "C"  void DepthOfFieldScatter_OnEnable_m1879216706 (DepthOfFieldScatter_t1867756894 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DepthOfFieldScatter_OnEnable_m1879216706_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t2727095145 * L_0 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		Camera_t2727095145 * L_1 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_1);
		int32_t L_2 = Camera_get_depthTextureMode_m2117446653(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Camera_set_depthTextureMode_m2368326786(L_0, ((int32_t)((int32_t)L_2|(int32_t)1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void DepthOfFieldScatter::OnDisable()
extern "C"  void DepthOfFieldScatter_OnDisable_m2862080299 (DepthOfFieldScatter_t1867756894 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(19 /* System.Void DepthOfFieldScatter::ReleaseComputeResources() */, __this);
		Material_t3870600107 * L_0 = __this->get_dofHdrMaterial_17();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Material_t3870600107 * L_2 = __this->get_dofHdrMaterial_17();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0021:
	{
		__this->set_dofHdrMaterial_17((Material_t3870600107 *)NULL);
		Material_t3870600107 * L_3 = __this->get_dx11bokehMaterial_19();
		bool L_4 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0043;
		}
	}
	{
		Material_t3870600107 * L_5 = __this->get_dx11bokehMaterial_19();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0043:
	{
		__this->set_dx11bokehMaterial_19((Material_t3870600107 *)NULL);
		return;
	}
}
// System.Void DepthOfFieldScatter::ReleaseComputeResources()
extern "C"  void DepthOfFieldScatter_ReleaseComputeResources_m732061239 (DepthOfFieldScatter_t1867756894 * __this, const MethodInfo* method)
{
	{
		ComputeBuffer_t37359565 * L_0 = __this->get_cbDrawArgs_26();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ComputeBuffer_t37359565 * L_1 = __this->get_cbDrawArgs_26();
		NullCheck(L_1);
		ComputeBuffer_Release_m1932213255(L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		__this->set_cbDrawArgs_26((ComputeBuffer_t37359565 *)NULL);
		ComputeBuffer_t37359565 * L_2 = __this->get_cbPoints_27();
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		ComputeBuffer_t37359565 * L_3 = __this->get_cbPoints_27();
		NullCheck(L_3);
		ComputeBuffer_Release_m1932213255(L_3, /*hidden argument*/NULL);
	}

IL_0033:
	{
		__this->set_cbPoints_27((ComputeBuffer_t37359565 *)NULL);
		return;
	}
}
// System.Void DepthOfFieldScatter::CreateComputeResources()
extern Il2CppClass* RuntimeServices_t3947355960_il2cpp_TypeInfo_var;
extern Il2CppClass* ComputeBuffer_t37359565_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern const uint32_t DepthOfFieldScatter_CreateComputeResources_m412588682_MetadataUsageId;
extern "C"  void DepthOfFieldScatter_CreateComputeResources_m412588682 (DepthOfFieldScatter_t1867756894 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DepthOfFieldScatter_CreateComputeResources_m412588682_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Int32U5BU5D_t3230847821* V_0 = NULL;
	{
		ComputeBuffer_t37359565 * L_0 = __this->get_cbDrawArgs_26();
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t3947355960_il2cpp_TypeInfo_var);
		bool L_1 = RuntimeServices_EqualityOperator_m1206578319(NULL /*static, unused*/, L_0, NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0047;
		}
	}
	{
		ComputeBuffer_t37359565 * L_2 = (ComputeBuffer_t37359565 *)il2cpp_codegen_object_new(ComputeBuffer_t37359565_il2cpp_TypeInfo_var);
		ComputeBuffer__ctor_m3093304150(L_2, 1, ((int32_t)16), ((int32_t)256), /*hidden argument*/NULL);
		__this->set_cbDrawArgs_26(L_2);
		V_0 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)4));
		Int32U5BU5D_t3230847821* L_3 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)0);
		Int32U5BU5D_t3230847821* L_4 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)1);
		Int32U5BU5D_t3230847821* L_5 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (int32_t)0);
		Int32U5BU5D_t3230847821* L_6 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (int32_t)0);
		ComputeBuffer_t37359565 * L_7 = __this->get_cbDrawArgs_26();
		Int32U5BU5D_t3230847821* L_8 = V_0;
		NullCheck(L_7);
		ComputeBuffer_SetData_m3962017490(L_7, (Il2CppArray *)(Il2CppArray *)L_8, /*hidden argument*/NULL);
	}

IL_0047:
	{
		ComputeBuffer_t37359565 * L_9 = __this->get_cbPoints_27();
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t3947355960_il2cpp_TypeInfo_var);
		bool L_10 = RuntimeServices_EqualityOperator_m1206578319(NULL /*static, unused*/, L_9, NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006b;
		}
	}
	{
		ComputeBuffer_t37359565 * L_11 = (ComputeBuffer_t37359565 *)il2cpp_codegen_object_new(ComputeBuffer_t37359565_il2cpp_TypeInfo_var);
		ComputeBuffer__ctor_m3093304150(L_11, ((int32_t)90000), ((int32_t)28), 2, /*hidden argument*/NULL);
		__this->set_cbPoints_27(L_11);
	}

IL_006b:
	{
		return;
	}
}
// System.Single DepthOfFieldScatter::FocalDistance01(System.Single)
extern const MethodInfo* Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var;
extern const uint32_t DepthOfFieldScatter_FocalDistance01_m2342287714_MetadataUsageId;
extern "C"  float DepthOfFieldScatter_FocalDistance01_m2342287714 (DepthOfFieldScatter_t1867756894 * __this, float ___worldDist0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DepthOfFieldScatter_FocalDistance01_m2342287714_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_t2727095145 * L_0 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		float L_1 = ___worldDist0;
		Camera_t2727095145 * L_2 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_2);
		float L_3 = Camera_get_nearClipPlane_m4074655061(L_2, /*hidden argument*/NULL);
		Camera_t2727095145 * L_4 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = Component_get_transform_m4257140443(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t4282066566  L_6 = Transform_get_forward_m877665793(L_5, /*hidden argument*/NULL);
		Vector3_t4282066566  L_7 = Vector3_op_Multiply_m3809076219(NULL /*static, unused*/, ((float)((float)L_1-(float)L_3)), L_6, /*hidden argument*/NULL);
		Camera_t2727095145 * L_8 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_8);
		Transform_t1659122786 * L_9 = Component_get_transform_m4257140443(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t4282066566  L_10 = Transform_get_position_m2211398607(L_9, /*hidden argument*/NULL);
		Vector3_t4282066566  L_11 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_7, L_10, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t4282066566  L_12 = Camera_WorldToViewportPoint_m3480725126(L_0, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		float L_13 = (&V_0)->get_z_3();
		Camera_t2727095145 * L_14 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_14);
		float L_15 = Camera_get_farClipPlane_m388706726(L_14, /*hidden argument*/NULL);
		Camera_t2727095145 * L_16 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_16);
		float L_17 = Camera_get_nearClipPlane_m4074655061(L_16, /*hidden argument*/NULL);
		return ((float)((float)L_13/(float)((float)((float)L_15-(float)L_17))));
	}
}
// System.Void DepthOfFieldScatter::WriteCoc(UnityEngine.RenderTexture,System.Boolean)
extern Il2CppCodeGenString* _stringLiteral532496039;
extern Il2CppCodeGenString* _stringLiteral2474470625;
extern const uint32_t DepthOfFieldScatter_WriteCoc_m1274203505_MetadataUsageId;
extern "C"  void DepthOfFieldScatter_WriteCoc_m1274203505 (DepthOfFieldScatter_t1867756894 * __this, RenderTexture_t1963041563 * ___fromTo0, bool ___fgDilate1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DepthOfFieldScatter_WriteCoc_m1274203505_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RenderTexture_t1963041563 * V_2 = NULL;
	float V_3 = 0.0f;
	RenderTexture_t1963041563 * V_4 = NULL;
	{
		Material_t3870600107 * L_0 = __this->get_dofHdrMaterial_17();
		NullCheck(L_0);
		Material_SetTexture_m1833724755(L_0, _stringLiteral532496039, (Texture_t2526458961 *)NULL, /*hidden argument*/NULL);
		bool L_1 = __this->get_nearBlur_14();
		if (!L_1)
		{
			goto IL_0110;
		}
	}
	{
		bool L_2 = ___fgDilate1;
		if (!L_2)
		{
			goto IL_0110;
		}
	}
	{
		RenderTexture_t1963041563 * L_3 = ___fromTo0;
		NullCheck(L_3);
		int32_t L_4 = RenderTexture_get_width_m1498578543(L_3, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_4/(int32_t)2));
		RenderTexture_t1963041563 * L_5 = ___fromTo0;
		NullCheck(L_5);
		int32_t L_6 = RenderTexture_get_height_m4010076224(L_5, /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_6/(int32_t)2));
		int32_t L_7 = V_0;
		int32_t L_8 = V_1;
		RenderTexture_t1963041563 * L_9 = ___fromTo0;
		NullCheck(L_9);
		int32_t L_10 = RenderTexture_get_format_m3502109954(L_9, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_11 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, L_7, L_8, 0, L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		RenderTexture_t1963041563 * L_12 = ___fromTo0;
		RenderTexture_t1963041563 * L_13 = V_2;
		Material_t3870600107 * L_14 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_12, L_13, L_14, 4, /*hidden argument*/NULL);
		float L_15 = __this->get_internalBlurWidth_28();
		float L_16 = __this->get_foregroundOverlap_15();
		V_3 = ((float)((float)L_15*(float)L_16));
		Material_t3870600107 * L_17 = __this->get_dofHdrMaterial_17();
		float L_18 = V_3;
		float L_19 = V_3;
		Vector4_t4282066567  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector4__ctor_m2441427762(&L_20, (((float)((float)0))), L_18, (((float)((float)0))), L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		Material_SetVector_m3505096203(L_17, _stringLiteral2474470625, L_20, /*hidden argument*/NULL);
		int32_t L_21 = V_0;
		int32_t L_22 = V_1;
		RenderTexture_t1963041563 * L_23 = ___fromTo0;
		NullCheck(L_23);
		int32_t L_24 = RenderTexture_get_format_m3502109954(L_23, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_25 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, L_21, L_22, 0, L_24, /*hidden argument*/NULL);
		V_4 = L_25;
		RenderTexture_t1963041563 * L_26 = V_2;
		RenderTexture_t1963041563 * L_27 = V_4;
		Material_t3870600107 * L_28 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_26, L_27, L_28, 2, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_29 = V_2;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		Material_t3870600107 * L_30 = __this->get_dofHdrMaterial_17();
		float L_31 = V_3;
		float L_32 = V_3;
		Vector4_t4282066567  L_33;
		memset(&L_33, 0, sizeof(L_33));
		Vector4__ctor_m2441427762(&L_33, L_31, (((float)((float)0))), (((float)((float)0))), L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		Material_SetVector_m3505096203(L_30, _stringLiteral2474470625, L_33, /*hidden argument*/NULL);
		int32_t L_34 = V_0;
		int32_t L_35 = V_1;
		RenderTexture_t1963041563 * L_36 = ___fromTo0;
		NullCheck(L_36);
		int32_t L_37 = RenderTexture_get_format_m3502109954(L_36, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_38 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, L_34, L_35, 0, L_37, /*hidden argument*/NULL);
		V_2 = L_38;
		RenderTexture_t1963041563 * L_39 = V_4;
		RenderTexture_t1963041563 * L_40 = V_2;
		Material_t3870600107 * L_41 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_39, L_40, L_41, 2, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_42 = V_4;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		Material_t3870600107 * L_43 = __this->get_dofHdrMaterial_17();
		RenderTexture_t1963041563 * L_44 = V_2;
		NullCheck(L_43);
		Material_SetTexture_m1833724755(L_43, _stringLiteral532496039, L_44, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_45 = ___fromTo0;
		NullCheck(L_45);
		RenderTexture_MarkRestoreExpected_m2220245707(L_45, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_46 = ___fromTo0;
		RenderTexture_t1963041563 * L_47 = ___fromTo0;
		Material_t3870600107 * L_48 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_46, L_47, L_48, ((int32_t)13), /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_49 = V_2;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		goto IL_011e;
	}

IL_0110:
	{
		RenderTexture_t1963041563 * L_50 = ___fromTo0;
		RenderTexture_t1963041563 * L_51 = ___fromTo0;
		Material_t3870600107 * L_52 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_50, L_51, L_52, 0, /*hidden argument*/NULL);
	}

IL_011e:
	{
		return;
	}
}
// System.Void DepthOfFieldScatter::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3447799158;
extern Il2CppCodeGenString* _stringLiteral2474470625;
extern Il2CppCodeGenString* _stringLiteral3398815928;
extern Il2CppCodeGenString* _stringLiteral2846499854;
extern Il2CppCodeGenString* _stringLiteral700682728;
extern Il2CppCodeGenString* _stringLiteral2563990371;
extern Il2CppCodeGenString* _stringLiteral4065094128;
extern Il2CppCodeGenString* _stringLiteral558922319;
extern Il2CppCodeGenString* _stringLiteral884648363;
extern Il2CppCodeGenString* _stringLiteral695457266;
extern Il2CppCodeGenString* _stringLiteral532496039;
extern const uint32_t DepthOfFieldScatter_OnRenderImage_m2404279066_MetadataUsageId;
extern "C"  void DepthOfFieldScatter_OnRenderImage_m2404279066 (DepthOfFieldScatter_t1867756894 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DepthOfFieldScatter_OnRenderImage_m2404279066_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RenderTexture_t1963041563 * V_0 = NULL;
	RenderTexture_t1963041563 * V_1 = NULL;
	RenderTexture_t1963041563 * V_2 = NULL;
	RenderTexture_t1963041563 * V_3 = NULL;
	float V_4 = 0.0f;
	RenderTexture_t1963041563 * V_5 = NULL;
	RenderTexture_t1963041563 * V_6 = NULL;
	int32_t V_7 = 0;
	Vector3_t4282066566  V_8;
	memset(&V_8, 0, sizeof(V_8));
	DepthOfFieldScatter_t1867756894 * G_B8_0 = NULL;
	DepthOfFieldScatter_t1867756894 * G_B7_0 = NULL;
	float G_B9_0 = 0.0f;
	DepthOfFieldScatter_t1867756894 * G_B9_1 = NULL;
	DepthOfFieldScatter_t1867756894 * G_B16_0 = NULL;
	DepthOfFieldScatter_t1867756894 * G_B15_0 = NULL;
	float G_B17_0 = 0.0f;
	DepthOfFieldScatter_t1867756894 * G_B17_1 = NULL;
	String_t* G_B21_0 = NULL;
	Material_t3870600107 * G_B21_1 = NULL;
	String_t* G_B20_0 = NULL;
	Material_t3870600107 * G_B20_1 = NULL;
	RenderTexture_t1963041563 * G_B22_0 = NULL;
	String_t* G_B22_1 = NULL;
	Material_t3870600107 * G_B22_2 = NULL;
	int32_t G_B38_0 = 0;
	Material_t3870600107 * G_B42_0 = NULL;
	RenderTexture_t1963041563 * G_B42_1 = NULL;
	RenderTexture_t1963041563 * G_B42_2 = NULL;
	Material_t3870600107 * G_B41_0 = NULL;
	RenderTexture_t1963041563 * G_B41_1 = NULL;
	RenderTexture_t1963041563 * G_B41_2 = NULL;
	int32_t G_B43_0 = 0;
	Material_t3870600107 * G_B43_1 = NULL;
	RenderTexture_t1963041563 * G_B43_2 = NULL;
	RenderTexture_t1963041563 * G_B43_3 = NULL;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean DepthOfFieldScatter::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		RenderTexture_t1963041563 * L_1 = ___source0;
		RenderTexture_t1963041563 * L_2 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_0a8c;
	}

IL_0017:
	{
		float L_3 = __this->get_aperture_8();
		if ((((float)L_3) >= ((float)(((float)((float)0))))))
		{
			goto IL_002c;
		}
	}
	{
		__this->set_aperture_8((((float)((float)0))));
	}

IL_002c:
	{
		float L_4 = __this->get_maxBlurSize_10();
		if ((((float)L_4) >= ((float)(0.1f))))
		{
			goto IL_0047;
		}
	}
	{
		__this->set_maxBlurSize_10((0.1f));
	}

IL_0047:
	{
		float L_5 = __this->get_focalSize_7();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_6 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_5, (((float)((float)0))), (2.0f), /*hidden argument*/NULL);
		__this->set_focalSize_7(L_6);
		float L_7 = __this->get_maxBlurSize_10();
		float L_8 = Mathf_Max_m3923796455(NULL /*static, unused*/, L_7, (((float)((float)0))), /*hidden argument*/NULL);
		__this->set_internalBlurWidth_28(L_8);
		Transform_t1659122786 * L_9 = __this->get_focalTransform_9();
		bool L_10 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		G_B7_0 = __this;
		if (!L_10)
		{
			G_B8_0 = __this;
			goto IL_00b3;
		}
	}
	{
		Camera_t2727095145 * L_11 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		Transform_t1659122786 * L_12 = __this->get_focalTransform_9();
		NullCheck(L_12);
		Vector3_t4282066566  L_13 = Transform_get_position_m2211398607(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t4282066566  L_14 = Camera_WorldToViewportPoint_m3480725126(L_11, L_13, /*hidden argument*/NULL);
		V_8 = L_14;
		float L_15 = (&V_8)->get_z_3();
		Camera_t2727095145 * L_16 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_16);
		float L_17 = Camera_get_farClipPlane_m388706726(L_16, /*hidden argument*/NULL);
		G_B9_0 = ((float)((float)L_15/(float)L_17));
		G_B9_1 = G_B7_0;
		goto IL_00bf;
	}

IL_00b3:
	{
		float L_18 = __this->get_focalLength_6();
		float L_19 = VirtFuncInvoker1< float, float >::Invoke(21 /* System.Single DepthOfFieldScatter::FocalDistance01(System.Single) */, __this, L_18);
		G_B9_0 = L_19;
		G_B9_1 = G_B8_0;
	}

IL_00bf:
	{
		NullCheck(G_B9_1);
		G_B9_1->set_focalDistance01_25(G_B9_0);
		Material_t3870600107 * L_20 = __this->get_dofHdrMaterial_17();
		float L_21 = __this->get_focalSize_7();
		float L_22 = __this->get_aperture_8();
		float L_23 = __this->get_focalDistance01_25();
		Vector4_t4282066567  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector4__ctor_m2441427762(&L_24, (1.0f), L_21, ((float)((float)L_22/(float)(10.0f))), L_23, /*hidden argument*/NULL);
		NullCheck(L_20);
		Material_SetVector_m3505096203(L_20, _stringLiteral3447799158, L_24, /*hidden argument*/NULL);
		V_0 = (RenderTexture_t1963041563 *)NULL;
		V_1 = (RenderTexture_t1963041563 *)NULL;
		V_2 = (RenderTexture_t1963041563 *)NULL;
		V_3 = (RenderTexture_t1963041563 *)NULL;
		float L_25 = __this->get_internalBlurWidth_28();
		float L_26 = __this->get_foregroundOverlap_15();
		V_4 = ((float)((float)L_25*(float)L_26));
		bool L_27 = __this->get_visualizeFocus_5();
		if (!L_27)
		{
			goto IL_0134;
		}
	}
	{
		RenderTexture_t1963041563 * L_28 = ___source0;
		DepthOfFieldScatter_WriteCoc_m1274203505(__this, L_28, (bool)1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_29 = ___source0;
		RenderTexture_t1963041563 * L_30 = ___destination1;
		Material_t3870600107 * L_31 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_29, L_30, L_31, ((int32_t)16), /*hidden argument*/NULL);
		goto IL_0a6a;
	}

IL_0134:
	{
		int32_t L_32 = __this->get_blurType_12();
		if ((!(((uint32_t)L_32) == ((uint32_t)1))))
		{
			goto IL_08d1;
		}
	}
	{
		Material_t3870600107 * L_33 = __this->get_dx11bokehMaterial_19();
		bool L_34 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_08d1;
		}
	}
	{
		bool L_35 = __this->get_highResolution_11();
		if (!L_35)
		{
			goto IL_04e1;
		}
	}
	{
		float L_36 = __this->get_internalBlurWidth_28();
		G_B15_0 = __this;
		if ((((float)L_36) >= ((float)(0.1f))))
		{
			G_B16_0 = __this;
			goto IL_0176;
		}
	}
	{
		G_B17_0 = (0.1f);
		G_B17_1 = G_B15_0;
		goto IL_017c;
	}

IL_0176:
	{
		float L_37 = __this->get_internalBlurWidth_28();
		G_B17_0 = L_37;
		G_B17_1 = G_B16_0;
	}

IL_017c:
	{
		NullCheck(G_B17_1);
		G_B17_1->set_internalBlurWidth_28(G_B17_0);
		float L_38 = __this->get_internalBlurWidth_28();
		float L_39 = __this->get_foregroundOverlap_15();
		V_4 = ((float)((float)L_38*(float)L_39));
		RenderTexture_t1963041563 * L_40 = ___source0;
		NullCheck(L_40);
		int32_t L_41 = RenderTexture_get_width_m1498578543(L_40, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_42 = ___source0;
		NullCheck(L_42);
		int32_t L_43 = RenderTexture_get_height_m4010076224(L_42, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_44 = ___source0;
		NullCheck(L_44);
		int32_t L_45 = RenderTexture_get_format_m3502109954(L_44, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_46 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, L_41, L_43, 0, L_45, /*hidden argument*/NULL);
		V_0 = L_46;
		RenderTexture_t1963041563 * L_47 = ___source0;
		NullCheck(L_47);
		int32_t L_48 = RenderTexture_get_width_m1498578543(L_47, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_49 = ___source0;
		NullCheck(L_49);
		int32_t L_50 = RenderTexture_get_height_m4010076224(L_49, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_51 = ___source0;
		NullCheck(L_51);
		int32_t L_52 = RenderTexture_get_format_m3502109954(L_51, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_53 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, L_48, L_50, 0, L_52, /*hidden argument*/NULL);
		V_5 = L_53;
		RenderTexture_t1963041563 * L_54 = ___source0;
		DepthOfFieldScatter_WriteCoc_m1274203505(__this, L_54, (bool)0, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_55 = ___source0;
		NullCheck(L_55);
		int32_t L_56 = RenderTexture_get_width_m1498578543(L_55, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_57 = ___source0;
		NullCheck(L_57);
		int32_t L_58 = RenderTexture_get_height_m4010076224(L_57, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_59 = ___source0;
		NullCheck(L_59);
		int32_t L_60 = RenderTexture_get_format_m3502109954(L_59, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_61 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, ((int32_t)((int32_t)L_56>>(int32_t)1)), ((int32_t)((int32_t)L_58>>(int32_t)1)), 0, L_60, /*hidden argument*/NULL);
		V_2 = L_61;
		RenderTexture_t1963041563 * L_62 = ___source0;
		NullCheck(L_62);
		int32_t L_63 = RenderTexture_get_width_m1498578543(L_62, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_64 = ___source0;
		NullCheck(L_64);
		int32_t L_65 = RenderTexture_get_height_m4010076224(L_64, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_66 = ___source0;
		NullCheck(L_66);
		int32_t L_67 = RenderTexture_get_format_m3502109954(L_66, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_68 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, ((int32_t)((int32_t)L_63>>(int32_t)1)), ((int32_t)((int32_t)L_65>>(int32_t)1)), 0, L_67, /*hidden argument*/NULL);
		V_3 = L_68;
		RenderTexture_t1963041563 * L_69 = ___source0;
		RenderTexture_t1963041563 * L_70 = V_2;
		Material_t3870600107 * L_71 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_69, L_70, L_71, ((int32_t)15), /*hidden argument*/NULL);
		Material_t3870600107 * L_72 = __this->get_dofHdrMaterial_17();
		Vector4_t4282066567  L_73;
		memset(&L_73, 0, sizeof(L_73));
		Vector4__ctor_m2441427762(&L_73, (((float)((float)0))), (1.5f), (((float)((float)0))), (1.5f), /*hidden argument*/NULL);
		NullCheck(L_72);
		Material_SetVector_m3505096203(L_72, _stringLiteral2474470625, L_73, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_74 = V_2;
		RenderTexture_t1963041563 * L_75 = V_3;
		Material_t3870600107 * L_76 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_74, L_75, L_76, ((int32_t)19), /*hidden argument*/NULL);
		Material_t3870600107 * L_77 = __this->get_dofHdrMaterial_17();
		Vector4_t4282066567  L_78;
		memset(&L_78, 0, sizeof(L_78));
		Vector4__ctor_m2441427762(&L_78, (1.5f), (((float)((float)0))), (((float)((float)0))), (1.5f), /*hidden argument*/NULL);
		NullCheck(L_77);
		Material_SetVector_m3505096203(L_77, _stringLiteral2474470625, L_78, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_79 = V_3;
		RenderTexture_t1963041563 * L_80 = V_2;
		Material_t3870600107 * L_81 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_79, L_80, L_81, ((int32_t)19), /*hidden argument*/NULL);
		bool L_82 = __this->get_nearBlur_14();
		if (!L_82)
		{
			goto IL_0291;
		}
	}
	{
		RenderTexture_t1963041563 * L_83 = ___source0;
		RenderTexture_t1963041563 * L_84 = V_3;
		Material_t3870600107 * L_85 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_83, L_84, L_85, 4, /*hidden argument*/NULL);
	}

IL_0291:
	{
		Material_t3870600107 * L_86 = __this->get_dx11bokehMaterial_19();
		RenderTexture_t1963041563 * L_87 = V_2;
		NullCheck(L_86);
		Material_SetTexture_m1833724755(L_86, _stringLiteral3398815928, L_87, /*hidden argument*/NULL);
		Material_t3870600107 * L_88 = __this->get_dx11bokehMaterial_19();
		float L_89 = __this->get_dx11SpawnHeuristic_21();
		NullCheck(L_88);
		Material_SetFloat_m981710063(L_88, _stringLiteral2846499854, L_89, /*hidden argument*/NULL);
		Material_t3870600107 * L_90 = __this->get_dx11bokehMaterial_19();
		float L_91 = __this->get_dx11BokehScale_23();
		float L_92 = __this->get_dx11BokehIntensity_24();
		float L_93 = __this->get_dx11BokehThreshhold_20();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_94 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_93, (0.005f), (4.0f), /*hidden argument*/NULL);
		float L_95 = __this->get_internalBlurWidth_28();
		Vector4_t4282066567  L_96;
		memset(&L_96, 0, sizeof(L_96));
		Vector4__ctor_m2441427762(&L_96, L_91, L_92, L_94, L_95, /*hidden argument*/NULL);
		NullCheck(L_90);
		Material_SetVector_m3505096203(L_90, _stringLiteral700682728, L_96, /*hidden argument*/NULL);
		Material_t3870600107 * L_97 = __this->get_dx11bokehMaterial_19();
		bool L_98 = __this->get_nearBlur_14();
		G_B20_0 = _stringLiteral2563990371;
		G_B20_1 = L_97;
		if (!L_98)
		{
			G_B21_0 = _stringLiteral2563990371;
			G_B21_1 = L_97;
			goto IL_0310;
		}
	}
	{
		RenderTexture_t1963041563 * L_99 = V_3;
		G_B22_0 = L_99;
		G_B22_1 = G_B20_0;
		G_B22_2 = G_B20_1;
		goto IL_0311;
	}

IL_0310:
	{
		G_B22_0 = ((RenderTexture_t1963041563 *)(NULL));
		G_B22_1 = G_B21_0;
		G_B22_2 = G_B21_1;
	}

IL_0311:
	{
		NullCheck(G_B22_2);
		Material_SetTexture_m1833724755(G_B22_2, G_B22_1, G_B22_0, /*hidden argument*/NULL);
		ComputeBuffer_t37359565 * L_100 = __this->get_cbPoints_27();
		Graphics_SetRandomWriteTarget_m2786492810(NULL /*static, unused*/, 1, L_100, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_101 = ___source0;
		RenderTexture_t1963041563 * L_102 = V_0;
		Material_t3870600107 * L_103 = __this->get_dx11bokehMaterial_19();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_101, L_102, L_103, 0, /*hidden argument*/NULL);
		Graphics_ClearRandomWriteTargets_m223572883(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_104 = __this->get_nearBlur_14();
		if (!L_104)
		{
			goto IL_03a4;
		}
	}
	{
		Material_t3870600107 * L_105 = __this->get_dofHdrMaterial_17();
		float L_106 = V_4;
		float L_107 = V_4;
		Vector4_t4282066567  L_108;
		memset(&L_108, 0, sizeof(L_108));
		Vector4__ctor_m2441427762(&L_108, (((float)((float)0))), L_106, (((float)((float)0))), L_107, /*hidden argument*/NULL);
		NullCheck(L_105);
		Material_SetVector_m3505096203(L_105, _stringLiteral2474470625, L_108, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_109 = V_3;
		RenderTexture_t1963041563 * L_110 = V_2;
		Material_t3870600107 * L_111 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_109, L_110, L_111, 2, /*hidden argument*/NULL);
		Material_t3870600107 * L_112 = __this->get_dofHdrMaterial_17();
		float L_113 = V_4;
		float L_114 = V_4;
		Vector4_t4282066567  L_115;
		memset(&L_115, 0, sizeof(L_115));
		Vector4__ctor_m2441427762(&L_115, L_113, (((float)((float)0))), (((float)((float)0))), L_114, /*hidden argument*/NULL);
		NullCheck(L_112);
		Material_SetVector_m3505096203(L_112, _stringLiteral2474470625, L_115, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_116 = V_2;
		RenderTexture_t1963041563 * L_117 = V_3;
		Material_t3870600107 * L_118 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_116, L_117, L_118, 2, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_119 = V_3;
		RenderTexture_t1963041563 * L_120 = V_0;
		Material_t3870600107 * L_121 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_119, L_120, L_121, 3, /*hidden argument*/NULL);
	}

IL_03a4:
	{
		RenderTexture_t1963041563 * L_122 = V_0;
		RenderTexture_t1963041563 * L_123 = V_5;
		Material_t3870600107 * L_124 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_122, L_123, L_124, ((int32_t)20), /*hidden argument*/NULL);
		Material_t3870600107 * L_125 = __this->get_dofHdrMaterial_17();
		float L_126 = __this->get_internalBlurWidth_28();
		float L_127 = __this->get_internalBlurWidth_28();
		Vector4_t4282066567  L_128;
		memset(&L_128, 0, sizeof(L_128));
		Vector4__ctor_m2441427762(&L_128, L_126, (((float)((float)0))), (((float)((float)0))), L_127, /*hidden argument*/NULL);
		NullCheck(L_125);
		Material_SetVector_m3505096203(L_125, _stringLiteral2474470625, L_128, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_129 = V_0;
		RenderTexture_t1963041563 * L_130 = ___source0;
		Material_t3870600107 * L_131 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_129, L_130, L_131, 5, /*hidden argument*/NULL);
		Material_t3870600107 * L_132 = __this->get_dofHdrMaterial_17();
		float L_133 = __this->get_internalBlurWidth_28();
		float L_134 = __this->get_internalBlurWidth_28();
		Vector4_t4282066567  L_135;
		memset(&L_135, 0, sizeof(L_135));
		Vector4__ctor_m2441427762(&L_135, (((float)((float)0))), L_133, (((float)((float)0))), L_134, /*hidden argument*/NULL);
		NullCheck(L_132);
		Material_SetVector_m3505096203(L_132, _stringLiteral2474470625, L_135, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_136 = ___source0;
		RenderTexture_t1963041563 * L_137 = V_5;
		Material_t3870600107 * L_138 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_136, L_137, L_138, ((int32_t)21), /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_139 = V_5;
		Graphics_SetRenderTarget_m3051614107(NULL /*static, unused*/, L_139, /*hidden argument*/NULL);
		ComputeBuffer_t37359565 * L_140 = __this->get_cbPoints_27();
		ComputeBuffer_t37359565 * L_141 = __this->get_cbDrawArgs_26();
		ComputeBuffer_CopyCount_m63772307(NULL /*static, unused*/, L_140, L_141, 0, /*hidden argument*/NULL);
		Material_t3870600107 * L_142 = __this->get_dx11bokehMaterial_19();
		ComputeBuffer_t37359565 * L_143 = __this->get_cbPoints_27();
		NullCheck(L_142);
		Material_SetBuffer_m3711483432(L_142, _stringLiteral4065094128, L_143, /*hidden argument*/NULL);
		Material_t3870600107 * L_144 = __this->get_dx11bokehMaterial_19();
		Texture2D_t3884108195 * L_145 = __this->get_dx11BokehTexture_22();
		NullCheck(L_144);
		Material_SetTexture_m1833724755(L_144, _stringLiteral558922319, L_145, /*hidden argument*/NULL);
		Material_t3870600107 * L_146 = __this->get_dx11bokehMaterial_19();
		RenderTexture_t1963041563 * L_147 = ___source0;
		NullCheck(L_147);
		int32_t L_148 = RenderTexture_get_width_m1498578543(L_147, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_149 = ___source0;
		NullCheck(L_149);
		int32_t L_150 = RenderTexture_get_height_m4010076224(L_149, /*hidden argument*/NULL);
		float L_151 = __this->get_internalBlurWidth_28();
		Vector3_t4282066566  L_152;
		memset(&L_152, 0, sizeof(L_152));
		Vector3__ctor_m2926210380(&L_152, ((float)((float)(1.0f)/(float)((float)((float)(1.0f)*(float)(((float)((float)L_148))))))), ((float)((float)(1.0f)/(float)((float)((float)(1.0f)*(float)(((float)((float)L_150))))))), L_151, /*hidden argument*/NULL);
		Vector4_t4282066567  L_153 = Vector4_op_Implicit_m331673271(NULL /*static, unused*/, L_152, /*hidden argument*/NULL);
		NullCheck(L_146);
		Material_SetVector_m3505096203(L_146, _stringLiteral884648363, L_153, /*hidden argument*/NULL);
		Material_t3870600107 * L_154 = __this->get_dx11bokehMaterial_19();
		NullCheck(L_154);
		Material_SetPass_m4241824642(L_154, 2, /*hidden argument*/NULL);
		ComputeBuffer_t37359565 * L_155 = __this->get_cbDrawArgs_26();
		Graphics_DrawProceduralIndirect_m2816657261(NULL /*static, unused*/, 5, L_155, 0, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_156 = V_5;
		RenderTexture_t1963041563 * L_157 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_156, L_157, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_158 = V_5;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_158, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_159 = V_2;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_159, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_160 = V_3;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_160, /*hidden argument*/NULL);
		goto IL_08cc;
	}

IL_04e1:
	{
		RenderTexture_t1963041563 * L_161 = ___source0;
		NullCheck(L_161);
		int32_t L_162 = RenderTexture_get_width_m1498578543(L_161, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_163 = ___source0;
		NullCheck(L_163);
		int32_t L_164 = RenderTexture_get_height_m4010076224(L_163, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_165 = ___source0;
		NullCheck(L_165);
		int32_t L_166 = RenderTexture_get_format_m3502109954(L_165, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_167 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, ((int32_t)((int32_t)L_162>>(int32_t)1)), ((int32_t)((int32_t)L_164>>(int32_t)1)), 0, L_166, /*hidden argument*/NULL);
		V_0 = L_167;
		RenderTexture_t1963041563 * L_168 = ___source0;
		NullCheck(L_168);
		int32_t L_169 = RenderTexture_get_width_m1498578543(L_168, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_170 = ___source0;
		NullCheck(L_170);
		int32_t L_171 = RenderTexture_get_height_m4010076224(L_170, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_172 = ___source0;
		NullCheck(L_172);
		int32_t L_173 = RenderTexture_get_format_m3502109954(L_172, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_174 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, ((int32_t)((int32_t)L_169>>(int32_t)1)), ((int32_t)((int32_t)L_171>>(int32_t)1)), 0, L_173, /*hidden argument*/NULL);
		V_1 = L_174;
		float L_175 = __this->get_internalBlurWidth_28();
		float L_176 = __this->get_foregroundOverlap_15();
		V_4 = ((float)((float)L_175*(float)L_176));
		RenderTexture_t1963041563 * L_177 = ___source0;
		DepthOfFieldScatter_WriteCoc_m1274203505(__this, L_177, (bool)0, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_178 = ___source0;
		NullCheck(L_178);
		Texture_set_filterMode_m3842701708(L_178, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_179 = ___source0;
		RenderTexture_t1963041563 * L_180 = V_0;
		Material_t3870600107 * L_181 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_179, L_180, L_181, 6, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_182 = V_0;
		NullCheck(L_182);
		int32_t L_183 = RenderTexture_get_width_m1498578543(L_182, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_184 = V_0;
		NullCheck(L_184);
		int32_t L_185 = RenderTexture_get_height_m4010076224(L_184, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_186 = V_0;
		NullCheck(L_186);
		int32_t L_187 = RenderTexture_get_format_m3502109954(L_186, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_188 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, ((int32_t)((int32_t)L_183>>(int32_t)1)), ((int32_t)((int32_t)L_185>>(int32_t)1)), 0, L_187, /*hidden argument*/NULL);
		V_2 = L_188;
		RenderTexture_t1963041563 * L_189 = V_0;
		NullCheck(L_189);
		int32_t L_190 = RenderTexture_get_width_m1498578543(L_189, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_191 = V_0;
		NullCheck(L_191);
		int32_t L_192 = RenderTexture_get_height_m4010076224(L_191, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_193 = V_0;
		NullCheck(L_193);
		int32_t L_194 = RenderTexture_get_format_m3502109954(L_193, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_195 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, ((int32_t)((int32_t)L_190>>(int32_t)1)), ((int32_t)((int32_t)L_192>>(int32_t)1)), 0, L_194, /*hidden argument*/NULL);
		V_3 = L_195;
		RenderTexture_t1963041563 * L_196 = V_0;
		RenderTexture_t1963041563 * L_197 = V_2;
		Material_t3870600107 * L_198 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_196, L_197, L_198, ((int32_t)15), /*hidden argument*/NULL);
		Material_t3870600107 * L_199 = __this->get_dofHdrMaterial_17();
		Vector4_t4282066567  L_200;
		memset(&L_200, 0, sizeof(L_200));
		Vector4__ctor_m2441427762(&L_200, (((float)((float)0))), (1.5f), (((float)((float)0))), (1.5f), /*hidden argument*/NULL);
		NullCheck(L_199);
		Material_SetVector_m3505096203(L_199, _stringLiteral2474470625, L_200, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_201 = V_2;
		RenderTexture_t1963041563 * L_202 = V_3;
		Material_t3870600107 * L_203 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_201, L_202, L_203, ((int32_t)19), /*hidden argument*/NULL);
		Material_t3870600107 * L_204 = __this->get_dofHdrMaterial_17();
		Vector4_t4282066567  L_205;
		memset(&L_205, 0, sizeof(L_205));
		Vector4__ctor_m2441427762(&L_205, (1.5f), (((float)((float)0))), (((float)((float)0))), (1.5f), /*hidden argument*/NULL);
		NullCheck(L_204);
		Material_SetVector_m3505096203(L_204, _stringLiteral2474470625, L_205, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_206 = V_3;
		RenderTexture_t1963041563 * L_207 = V_2;
		Material_t3870600107 * L_208 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_206, L_207, L_208, ((int32_t)19), /*hidden argument*/NULL);
		V_6 = (RenderTexture_t1963041563 *)NULL;
		bool L_209 = __this->get_nearBlur_14();
		if (!L_209)
		{
			goto IL_062f;
		}
	}
	{
		RenderTexture_t1963041563 * L_210 = ___source0;
		NullCheck(L_210);
		int32_t L_211 = RenderTexture_get_width_m1498578543(L_210, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_212 = ___source0;
		NullCheck(L_212);
		int32_t L_213 = RenderTexture_get_height_m4010076224(L_212, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_214 = ___source0;
		NullCheck(L_214);
		int32_t L_215 = RenderTexture_get_format_m3502109954(L_214, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_216 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, ((int32_t)((int32_t)L_211>>(int32_t)1)), ((int32_t)((int32_t)L_213>>(int32_t)1)), 0, L_215, /*hidden argument*/NULL);
		V_6 = L_216;
		RenderTexture_t1963041563 * L_217 = ___source0;
		RenderTexture_t1963041563 * L_218 = V_6;
		Material_t3870600107 * L_219 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_217, L_218, L_219, 4, /*hidden argument*/NULL);
	}

IL_062f:
	{
		Material_t3870600107 * L_220 = __this->get_dx11bokehMaterial_19();
		RenderTexture_t1963041563 * L_221 = V_2;
		NullCheck(L_220);
		Material_SetTexture_m1833724755(L_220, _stringLiteral3398815928, L_221, /*hidden argument*/NULL);
		Material_t3870600107 * L_222 = __this->get_dx11bokehMaterial_19();
		float L_223 = __this->get_dx11SpawnHeuristic_21();
		NullCheck(L_222);
		Material_SetFloat_m981710063(L_222, _stringLiteral2846499854, L_223, /*hidden argument*/NULL);
		Material_t3870600107 * L_224 = __this->get_dx11bokehMaterial_19();
		float L_225 = __this->get_dx11BokehScale_23();
		float L_226 = __this->get_dx11BokehIntensity_24();
		float L_227 = __this->get_dx11BokehThreshhold_20();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_228 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_227, (0.005f), (4.0f), /*hidden argument*/NULL);
		float L_229 = __this->get_internalBlurWidth_28();
		Vector4_t4282066567  L_230;
		memset(&L_230, 0, sizeof(L_230));
		Vector4__ctor_m2441427762(&L_230, L_225, L_226, L_228, L_229, /*hidden argument*/NULL);
		NullCheck(L_224);
		Material_SetVector_m3505096203(L_224, _stringLiteral700682728, L_230, /*hidden argument*/NULL);
		Material_t3870600107 * L_231 = __this->get_dx11bokehMaterial_19();
		RenderTexture_t1963041563 * L_232 = V_6;
		NullCheck(L_231);
		Material_SetTexture_m1833724755(L_231, _stringLiteral2563990371, L_232, /*hidden argument*/NULL);
		ComputeBuffer_t37359565 * L_233 = __this->get_cbPoints_27();
		Graphics_SetRandomWriteTarget_m2786492810(NULL /*static, unused*/, 1, L_233, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_234 = V_0;
		RenderTexture_t1963041563 * L_235 = V_1;
		Material_t3870600107 * L_236 = __this->get_dx11bokehMaterial_19();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_234, L_235, L_236, 0, /*hidden argument*/NULL);
		Graphics_ClearRandomWriteTargets_m223572883(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_237 = V_2;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_237, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_238 = V_3;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_238, /*hidden argument*/NULL);
		bool L_239 = __this->get_nearBlur_14();
		if (!L_239)
		{
			goto IL_0741;
		}
	}
	{
		Material_t3870600107 * L_240 = __this->get_dofHdrMaterial_17();
		float L_241 = V_4;
		float L_242 = V_4;
		Vector4_t4282066567  L_243;
		memset(&L_243, 0, sizeof(L_243));
		Vector4__ctor_m2441427762(&L_243, (((float)((float)0))), L_241, (((float)((float)0))), L_242, /*hidden argument*/NULL);
		NullCheck(L_240);
		Material_SetVector_m3505096203(L_240, _stringLiteral2474470625, L_243, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_244 = V_6;
		RenderTexture_t1963041563 * L_245 = V_0;
		Material_t3870600107 * L_246 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_244, L_245, L_246, 2, /*hidden argument*/NULL);
		Material_t3870600107 * L_247 = __this->get_dofHdrMaterial_17();
		float L_248 = V_4;
		float L_249 = V_4;
		Vector4_t4282066567  L_250;
		memset(&L_250, 0, sizeof(L_250));
		Vector4__ctor_m2441427762(&L_250, L_248, (((float)((float)0))), (((float)((float)0))), L_249, /*hidden argument*/NULL);
		NullCheck(L_247);
		Material_SetVector_m3505096203(L_247, _stringLiteral2474470625, L_250, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_251 = V_0;
		RenderTexture_t1963041563 * L_252 = V_6;
		Material_t3870600107 * L_253 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_251, L_252, L_253, 2, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_254 = V_6;
		RenderTexture_t1963041563 * L_255 = V_1;
		Material_t3870600107 * L_256 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_254, L_255, L_256, 3, /*hidden argument*/NULL);
	}

IL_0741:
	{
		Material_t3870600107 * L_257 = __this->get_dofHdrMaterial_17();
		float L_258 = __this->get_internalBlurWidth_28();
		float L_259 = __this->get_internalBlurWidth_28();
		Vector4_t4282066567  L_260;
		memset(&L_260, 0, sizeof(L_260));
		Vector4__ctor_m2441427762(&L_260, L_258, (((float)((float)0))), (((float)((float)0))), L_259, /*hidden argument*/NULL);
		NullCheck(L_257);
		Material_SetVector_m3505096203(L_257, _stringLiteral2474470625, L_260, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_261 = V_1;
		RenderTexture_t1963041563 * L_262 = V_0;
		Material_t3870600107 * L_263 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_261, L_262, L_263, 5, /*hidden argument*/NULL);
		Material_t3870600107 * L_264 = __this->get_dofHdrMaterial_17();
		float L_265 = __this->get_internalBlurWidth_28();
		float L_266 = __this->get_internalBlurWidth_28();
		Vector4_t4282066567  L_267;
		memset(&L_267, 0, sizeof(L_267));
		Vector4__ctor_m2441427762(&L_267, (((float)((float)0))), L_265, (((float)((float)0))), L_266, /*hidden argument*/NULL);
		NullCheck(L_264);
		Material_SetVector_m3505096203(L_264, _stringLiteral2474470625, L_267, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_268 = V_0;
		RenderTexture_t1963041563 * L_269 = V_1;
		Material_t3870600107 * L_270 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_268, L_269, L_270, 5, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_271 = V_1;
		Graphics_SetRenderTarget_m3051614107(NULL /*static, unused*/, L_271, /*hidden argument*/NULL);
		ComputeBuffer_t37359565 * L_272 = __this->get_cbPoints_27();
		ComputeBuffer_t37359565 * L_273 = __this->get_cbDrawArgs_26();
		ComputeBuffer_CopyCount_m63772307(NULL /*static, unused*/, L_272, L_273, 0, /*hidden argument*/NULL);
		Material_t3870600107 * L_274 = __this->get_dx11bokehMaterial_19();
		ComputeBuffer_t37359565 * L_275 = __this->get_cbPoints_27();
		NullCheck(L_274);
		Material_SetBuffer_m3711483432(L_274, _stringLiteral4065094128, L_275, /*hidden argument*/NULL);
		Material_t3870600107 * L_276 = __this->get_dx11bokehMaterial_19();
		Texture2D_t3884108195 * L_277 = __this->get_dx11BokehTexture_22();
		NullCheck(L_276);
		Material_SetTexture_m1833724755(L_276, _stringLiteral558922319, L_277, /*hidden argument*/NULL);
		Material_t3870600107 * L_278 = __this->get_dx11bokehMaterial_19();
		RenderTexture_t1963041563 * L_279 = V_1;
		NullCheck(L_279);
		int32_t L_280 = RenderTexture_get_width_m1498578543(L_279, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_281 = V_1;
		NullCheck(L_281);
		int32_t L_282 = RenderTexture_get_height_m4010076224(L_281, /*hidden argument*/NULL);
		float L_283 = __this->get_internalBlurWidth_28();
		Vector3_t4282066566  L_284;
		memset(&L_284, 0, sizeof(L_284));
		Vector3__ctor_m2926210380(&L_284, ((float)((float)(1.0f)/(float)((float)((float)(1.0f)*(float)(((float)((float)L_280))))))), ((float)((float)(1.0f)/(float)((float)((float)(1.0f)*(float)(((float)((float)L_282))))))), L_283, /*hidden argument*/NULL);
		Vector4_t4282066567  L_285 = Vector4_op_Implicit_m331673271(NULL /*static, unused*/, L_284, /*hidden argument*/NULL);
		NullCheck(L_278);
		Material_SetVector_m3505096203(L_278, _stringLiteral884648363, L_285, /*hidden argument*/NULL);
		Material_t3870600107 * L_286 = __this->get_dx11bokehMaterial_19();
		NullCheck(L_286);
		Material_SetPass_m4241824642(L_286, 1, /*hidden argument*/NULL);
		ComputeBuffer_t37359565 * L_287 = __this->get_cbDrawArgs_26();
		Graphics_DrawProceduralIndirect_m2816657261(NULL /*static, unused*/, 5, L_287, 0, /*hidden argument*/NULL);
		Material_t3870600107 * L_288 = __this->get_dofHdrMaterial_17();
		RenderTexture_t1963041563 * L_289 = V_1;
		NullCheck(L_288);
		Material_SetTexture_m1833724755(L_288, _stringLiteral695457266, L_289, /*hidden argument*/NULL);
		Material_t3870600107 * L_290 = __this->get_dofHdrMaterial_17();
		RenderTexture_t1963041563 * L_291 = V_6;
		NullCheck(L_290);
		Material_SetTexture_m1833724755(L_290, _stringLiteral532496039, L_291, /*hidden argument*/NULL);
		Material_t3870600107 * L_292 = __this->get_dofHdrMaterial_17();
		RenderTexture_t1963041563 * L_293 = ___source0;
		NullCheck(L_293);
		int32_t L_294 = RenderTexture_get_width_m1498578543(L_293, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_295 = V_1;
		NullCheck(L_295);
		int32_t L_296 = RenderTexture_get_width_m1498578543(L_295, /*hidden argument*/NULL);
		float L_297 = __this->get_internalBlurWidth_28();
		Vector4_t4282066567  L_298 = Vector4_get_one_m3300413884(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector4_t4282066567  L_299 = Vector4_op_Multiply_m3555003804(NULL /*static, unused*/, ((float)((float)((float)((float)((float)((float)(1.0f)*(float)(((float)((float)L_294)))))/(float)((float)((float)(1.0f)*(float)(((float)((float)L_296)))))))*(float)L_297)), L_298, /*hidden argument*/NULL);
		NullCheck(L_292);
		Material_SetVector_m3505096203(L_292, _stringLiteral2474470625, L_299, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_300 = ___source0;
		RenderTexture_t1963041563 * L_301 = ___destination1;
		Material_t3870600107 * L_302 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_300, L_301, L_302, ((int32_t)9), /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_303 = V_6;
		bool L_304 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_303, /*hidden argument*/NULL);
		if (!L_304)
		{
			goto IL_08cc;
		}
	}
	{
		RenderTexture_t1963041563 * L_305 = V_6;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_305, /*hidden argument*/NULL);
	}

IL_08cc:
	{
		goto IL_0a6a;
	}

IL_08d1:
	{
		RenderTexture_t1963041563 * L_306 = ___source0;
		NullCheck(L_306);
		Texture_set_filterMode_m3842701708(L_306, 1, /*hidden argument*/NULL);
		bool L_307 = __this->get_highResolution_11();
		if (!L_307)
		{
			goto IL_08f5;
		}
	}
	{
		float L_308 = __this->get_internalBlurWidth_28();
		__this->set_internalBlurWidth_28(((float)((float)L_308*(float)(2.0f))));
	}

IL_08f5:
	{
		RenderTexture_t1963041563 * L_309 = ___source0;
		DepthOfFieldScatter_WriteCoc_m1274203505(__this, L_309, (bool)1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_310 = ___source0;
		NullCheck(L_310);
		int32_t L_311 = RenderTexture_get_width_m1498578543(L_310, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_312 = ___source0;
		NullCheck(L_312);
		int32_t L_313 = RenderTexture_get_height_m4010076224(L_312, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_314 = ___source0;
		NullCheck(L_314);
		int32_t L_315 = RenderTexture_get_format_m3502109954(L_314, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_316 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, ((int32_t)((int32_t)L_311>>(int32_t)1)), ((int32_t)((int32_t)L_313>>(int32_t)1)), 0, L_315, /*hidden argument*/NULL);
		V_0 = L_316;
		RenderTexture_t1963041563 * L_317 = ___source0;
		NullCheck(L_317);
		int32_t L_318 = RenderTexture_get_width_m1498578543(L_317, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_319 = ___source0;
		NullCheck(L_319);
		int32_t L_320 = RenderTexture_get_height_m4010076224(L_319, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_321 = ___source0;
		NullCheck(L_321);
		int32_t L_322 = RenderTexture_get_format_m3502109954(L_321, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_323 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, ((int32_t)((int32_t)L_318>>(int32_t)1)), ((int32_t)((int32_t)L_320>>(int32_t)1)), 0, L_322, /*hidden argument*/NULL);
		V_1 = L_323;
		int32_t L_324 = __this->get_blurSampleCount_13();
		if ((((int32_t)L_324) == ((int32_t)2)))
		{
			goto IL_094f;
		}
	}
	{
		int32_t L_325 = __this->get_blurSampleCount_13();
		if ((!(((uint32_t)L_325) == ((uint32_t)1))))
		{
			goto IL_0956;
		}
	}

IL_094f:
	{
		G_B38_0 = ((int32_t)17);
		goto IL_0958;
	}

IL_0956:
	{
		G_B38_0 = ((int32_t)11);
	}

IL_0958:
	{
		V_7 = G_B38_0;
		bool L_326 = __this->get_highResolution_11();
		if (!L_326)
		{
			goto IL_09a1;
		}
	}
	{
		Material_t3870600107 * L_327 = __this->get_dofHdrMaterial_17();
		float L_328 = __this->get_internalBlurWidth_28();
		float L_329 = __this->get_internalBlurWidth_28();
		Vector4_t4282066567  L_330;
		memset(&L_330, 0, sizeof(L_330));
		Vector4__ctor_m2441427762(&L_330, (((float)((float)0))), L_328, (0.025f), L_329, /*hidden argument*/NULL);
		NullCheck(L_327);
		Material_SetVector_m3505096203(L_327, _stringLiteral2474470625, L_330, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_331 = ___source0;
		RenderTexture_t1963041563 * L_332 = ___destination1;
		Material_t3870600107 * L_333 = __this->get_dofHdrMaterial_17();
		int32_t L_334 = V_7;
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_331, L_332, L_333, L_334, /*hidden argument*/NULL);
		goto IL_0a6a;
	}

IL_09a1:
	{
		Material_t3870600107 * L_335 = __this->get_dofHdrMaterial_17();
		float L_336 = __this->get_internalBlurWidth_28();
		float L_337 = __this->get_internalBlurWidth_28();
		Vector4_t4282066567  L_338;
		memset(&L_338, 0, sizeof(L_338));
		Vector4__ctor_m2441427762(&L_338, (((float)((float)0))), L_336, (0.1f), L_337, /*hidden argument*/NULL);
		NullCheck(L_335);
		Material_SetVector_m3505096203(L_335, _stringLiteral2474470625, L_338, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_339 = ___source0;
		RenderTexture_t1963041563 * L_340 = V_0;
		Material_t3870600107 * L_341 = __this->get_dofHdrMaterial_17();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_339, L_340, L_341, 6, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_342 = V_0;
		RenderTexture_t1963041563 * L_343 = V_1;
		Material_t3870600107 * L_344 = __this->get_dofHdrMaterial_17();
		int32_t L_345 = V_7;
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_342, L_343, L_344, L_345, /*hidden argument*/NULL);
		Material_t3870600107 * L_346 = __this->get_dofHdrMaterial_17();
		RenderTexture_t1963041563 * L_347 = V_1;
		NullCheck(L_346);
		Material_SetTexture_m1833724755(L_346, _stringLiteral695457266, L_347, /*hidden argument*/NULL);
		Material_t3870600107 * L_348 = __this->get_dofHdrMaterial_17();
		NullCheck(L_348);
		Material_SetTexture_m1833724755(L_348, _stringLiteral532496039, (Texture_t2526458961 *)NULL, /*hidden argument*/NULL);
		Material_t3870600107 * L_349 = __this->get_dofHdrMaterial_17();
		Vector4_t4282066567  L_350 = Vector4_get_one_m3300413884(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_351 = ___source0;
		NullCheck(L_351);
		int32_t L_352 = RenderTexture_get_width_m1498578543(L_351, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_353 = V_1;
		NullCheck(L_353);
		int32_t L_354 = RenderTexture_get_width_m1498578543(L_353, /*hidden argument*/NULL);
		Vector4_t4282066567  L_355 = Vector4_op_Multiply_m209031836(NULL /*static, unused*/, L_350, ((float)((float)((float)((float)(1.0f)*(float)(((float)((float)L_352)))))/(float)((float)((float)(1.0f)*(float)(((float)((float)L_354))))))), /*hidden argument*/NULL);
		float L_356 = __this->get_internalBlurWidth_28();
		Vector4_t4282066567  L_357 = Vector4_op_Multiply_m209031836(NULL /*static, unused*/, L_355, L_356, /*hidden argument*/NULL);
		NullCheck(L_349);
		Material_SetVector_m3505096203(L_349, _stringLiteral2474470625, L_357, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_358 = ___source0;
		RenderTexture_t1963041563 * L_359 = ___destination1;
		Material_t3870600107 * L_360 = __this->get_dofHdrMaterial_17();
		int32_t L_361 = __this->get_blurSampleCount_13();
		G_B41_0 = L_360;
		G_B41_1 = L_359;
		G_B41_2 = L_358;
		if ((!(((uint32_t)L_361) == ((uint32_t)2))))
		{
			G_B42_0 = L_360;
			G_B42_1 = L_359;
			G_B42_2 = L_358;
			goto IL_0a63;
		}
	}
	{
		G_B43_0 = ((int32_t)18);
		G_B43_1 = G_B41_0;
		G_B43_2 = G_B41_1;
		G_B43_3 = G_B41_2;
		goto IL_0a65;
	}

IL_0a63:
	{
		G_B43_0 = ((int32_t)12);
		G_B43_1 = G_B42_0;
		G_B43_2 = G_B42_1;
		G_B43_3 = G_B42_2;
	}

IL_0a65:
	{
		Graphics_Blit_m336256356(NULL /*static, unused*/, G_B43_3, G_B43_2, G_B43_1, G_B43_0, /*hidden argument*/NULL);
	}

IL_0a6a:
	{
		RenderTexture_t1963041563 * L_362 = V_0;
		bool L_363 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_362, /*hidden argument*/NULL);
		if (!L_363)
		{
			goto IL_0a7b;
		}
	}
	{
		RenderTexture_t1963041563 * L_364 = V_0;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_364, /*hidden argument*/NULL);
	}

IL_0a7b:
	{
		RenderTexture_t1963041563 * L_365 = V_1;
		bool L_366 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_365, /*hidden argument*/NULL);
		if (!L_366)
		{
			goto IL_0a8c;
		}
	}
	{
		RenderTexture_t1963041563 * L_367 = V_1;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_367, /*hidden argument*/NULL);
	}

IL_0a8c:
	{
		return;
	}
}
// System.Void DepthOfFieldScatter::Main()
extern "C"  void DepthOfFieldScatter_Main_m144552313 (DepthOfFieldScatter_t1867756894 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DragRigidbody::.ctor()
extern "C"  void DragRigidbody__ctor_m1875031657 (DragRigidbody_t2531437401 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		__this->set_spring_2((50.0f));
		__this->set_damper_3((5.0f));
		__this->set_drag_4((10.0f));
		__this->set_angularDrag_5((5.0f));
		__this->set_distance_6((0.2f));
		return;
	}
}
// System.Void DragRigidbody::Update()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* RaycastHit_t4003175726_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t3674682005_il2cpp_TypeInfo_var;
extern Il2CppClass* Rigidbody_t3346577219_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisRigidbody_t3346577219_m107677727_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisSpringJoint_t558455091_m1843786415_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3254981293;
extern Il2CppCodeGenString* _stringLiteral458547635;
extern const uint32_t DragRigidbody_Update_m4018268612_MetadataUsageId;
extern "C"  void DragRigidbody_Update_m4018268612 (DragRigidbody_t2531437401 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DragRigidbody_Update_m4018268612_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Camera_t2727095145 * V_0 = NULL;
	RaycastHit_t4003175726  V_1;
	memset(&V_1, 0, sizeof(V_1));
	GameObject_t3674682005 * V_2 = NULL;
	Rigidbody_t3346577219 * V_3 = NULL;
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonDown_m2031691843(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		goto IL_0183;
	}

IL_0010:
	{
		Camera_t2727095145 * L_1 = VirtFuncInvoker0< Camera_t2727095145 * >::Invoke(6 /* UnityEngine.Camera DragRigidbody::FindCamera() */, __this);
		V_0 = L_1;
		Initobj (RaycastHit_t4003175726_il2cpp_TypeInfo_var, (&V_1));
		Camera_t2727095145 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		Vector3_t4282066566  L_3 = Input_get_mousePosition_m4020233228(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Ray_t3134616544  L_4 = Camera_ScreenPointToRay_m1733083890(L_2, L_3, /*hidden argument*/NULL);
		bool L_5 = Physics_Raycast_m1235528076(NULL /*static, unused*/, L_4, (&V_1), (((float)((float)((int32_t)100)))), /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_003e;
		}
	}
	{
		goto IL_0183;
	}

IL_003e:
	{
		Rigidbody_t3346577219 * L_6 = RaycastHit_get_rigidbody_m4137883432((&V_1), /*hidden argument*/NULL);
		bool L_7 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0060;
		}
	}
	{
		Rigidbody_t3346577219 * L_8 = RaycastHit_get_rigidbody_m4137883432((&V_1), /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_9 = Rigidbody_get_isKinematic_m3963857442(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0065;
		}
	}

IL_0060:
	{
		goto IL_0183;
	}

IL_0065:
	{
		SpringJoint_t558455091 * L_10 = __this->get_springJoint_8();
		bool L_11 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_009f;
		}
	}
	{
		GameObject_t3674682005 * L_12 = (GameObject_t3674682005 *)il2cpp_codegen_object_new(GameObject_t3674682005_il2cpp_TypeInfo_var);
		GameObject__ctor_m3920833606(L_12, _stringLiteral3254981293, /*hidden argument*/NULL);
		V_2 = L_12;
		GameObject_t3674682005 * L_13 = V_2;
		NullCheck(L_13);
		Rigidbody_t3346577219 * L_14 = GameObject_AddComponent_TisRigidbody_t3346577219_m107677727(L_13, /*hidden argument*/GameObject_AddComponent_TisRigidbody_t3346577219_m107677727_MethodInfo_var);
		V_3 = ((Rigidbody_t3346577219 *)IsInstSealed(L_14, Rigidbody_t3346577219_il2cpp_TypeInfo_var));
		GameObject_t3674682005 * L_15 = V_2;
		NullCheck(L_15);
		SpringJoint_t558455091 * L_16 = GameObject_AddComponent_TisSpringJoint_t558455091_m1843786415(L_15, /*hidden argument*/GameObject_AddComponent_TisSpringJoint_t558455091_m1843786415_MethodInfo_var);
		__this->set_springJoint_8(L_16);
		Rigidbody_t3346577219 * L_17 = V_3;
		NullCheck(L_17);
		Rigidbody_set_isKinematic_m294703295(L_17, (bool)1, /*hidden argument*/NULL);
	}

IL_009f:
	{
		SpringJoint_t558455091 * L_18 = __this->get_springJoint_8();
		NullCheck(L_18);
		Transform_t1659122786 * L_19 = Component_get_transform_m4257140443(L_18, /*hidden argument*/NULL);
		Vector3_t4282066566  L_20 = RaycastHit_get_point_m4165497838((&V_1), /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_set_position_m3111394108(L_19, L_20, /*hidden argument*/NULL);
		bool L_21 = __this->get_attachToCenterOfMass_7();
		if (!L_21)
		{
			goto IL_0116;
		}
	}
	{
		Transform_t1659122786 * L_22 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Rigidbody_t3346577219 * L_23 = RaycastHit_get_rigidbody_m4137883432((&V_1), /*hidden argument*/NULL);
		NullCheck(L_23);
		Vector3_t4282066566  L_24 = Rigidbody_get_centerOfMass_m3434430695(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t4282066566  L_25 = Transform_TransformDirection_m83001769(L_22, L_24, /*hidden argument*/NULL);
		Rigidbody_t3346577219 * L_26 = RaycastHit_get_rigidbody_m4137883432((&V_1), /*hidden argument*/NULL);
		NullCheck(L_26);
		Transform_t1659122786 * L_27 = Component_get_transform_m4257140443(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		Vector3_t4282066566  L_28 = Transform_get_position_m2211398607(L_27, /*hidden argument*/NULL);
		Vector3_t4282066566  L_29 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_25, L_28, /*hidden argument*/NULL);
		V_4 = L_29;
		SpringJoint_t558455091 * L_30 = __this->get_springJoint_8();
		NullCheck(L_30);
		Transform_t1659122786 * L_31 = Component_get_transform_m4257140443(L_30, /*hidden argument*/NULL);
		Vector3_t4282066566  L_32 = V_4;
		NullCheck(L_31);
		Vector3_t4282066566  L_33 = Transform_InverseTransformPoint_m1626812000(L_31, L_32, /*hidden argument*/NULL);
		V_4 = L_33;
		SpringJoint_t558455091 * L_34 = __this->get_springJoint_8();
		Vector3_t4282066566  L_35 = V_4;
		NullCheck(L_34);
		Joint_set_anchor_m1010170386(L_34, L_35, /*hidden argument*/NULL);
		goto IL_0126;
	}

IL_0116:
	{
		SpringJoint_t558455091 * L_36 = __this->get_springJoint_8();
		Vector3_t4282066566  L_37 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_36);
		Joint_set_anchor_m1010170386(L_36, L_37, /*hidden argument*/NULL);
	}

IL_0126:
	{
		SpringJoint_t558455091 * L_38 = __this->get_springJoint_8();
		float L_39 = __this->get_spring_2();
		NullCheck(L_38);
		SpringJoint_set_spring_m3720718841(L_38, L_39, /*hidden argument*/NULL);
		SpringJoint_t558455091 * L_40 = __this->get_springJoint_8();
		float L_41 = __this->get_damper_3();
		NullCheck(L_40);
		SpringJoint_set_damper_m3309877369(L_40, L_41, /*hidden argument*/NULL);
		SpringJoint_t558455091 * L_42 = __this->get_springJoint_8();
		float L_43 = __this->get_distance_6();
		NullCheck(L_42);
		SpringJoint_set_maxDistance_m68119509(L_42, L_43, /*hidden argument*/NULL);
		SpringJoint_t558455091 * L_44 = __this->get_springJoint_8();
		Rigidbody_t3346577219 * L_45 = RaycastHit_get_rigidbody_m4137883432((&V_1), /*hidden argument*/NULL);
		NullCheck(L_44);
		Joint_set_connectedBody_m2794572257(L_44, L_45, /*hidden argument*/NULL);
		float L_46 = RaycastHit_get_distance_m800944203((&V_1), /*hidden argument*/NULL);
		float L_47 = L_46;
		Il2CppObject * L_48 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_47);
		MonoBehaviour_StartCoroutine_m2964903975(__this, _stringLiteral458547635, L_48, /*hidden argument*/NULL);
	}

IL_0183:
	{
		return;
	}
}
// System.Collections.IEnumerator DragRigidbody::DragObject(System.Single)
extern Il2CppClass* U24DragObjectU2458_t1090936582_il2cpp_TypeInfo_var;
extern const uint32_t DragRigidbody_DragObject_m3675163991_MetadataUsageId;
extern "C"  Il2CppObject * DragRigidbody_DragObject_m3675163991 (DragRigidbody_t2531437401 * __this, float ___distance0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DragRigidbody_DragObject_m3675163991_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___distance0;
		U24DragObjectU2458_t1090936582 * L_1 = (U24DragObjectU2458_t1090936582 *)il2cpp_codegen_object_new(U24DragObjectU2458_t1090936582_il2cpp_TypeInfo_var);
		U24DragObjectU2458__ctor_m1025218848(L_1, L_0, __this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Il2CppObject* L_2 = U24DragObjectU2458_GetEnumerator_m704288176(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Camera DragRigidbody::FindCamera()
extern const MethodInfo* Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var;
extern const uint32_t DragRigidbody_FindCamera_m2906847494_MetadataUsageId;
extern "C"  Camera_t2727095145 * DragRigidbody_FindCamera_m2906847494 (DragRigidbody_t2531437401 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DragRigidbody_FindCamera_m2906847494_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Camera_t2727095145 * G_B3_0 = NULL;
	{
		Camera_t2727095145 * L_0 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Camera_t2727095145 * L_2 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		G_B3_0 = L_2;
		goto IL_0025;
	}

IL_001b:
	{
		Camera_t2727095145 * L_3 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0025;
	}

IL_0025:
	{
		return G_B3_0;
	}
}
// System.Void DragRigidbody::Main()
extern "C"  void DragRigidbody_Main_m2192736564 (DragRigidbody_t2531437401 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DragRigidbody/$DragObject$58::.ctor(System.Single,DragRigidbody)
extern const MethodInfo* GenericGenerator_1__ctor_m3362830377_MethodInfo_var;
extern const uint32_t U24DragObjectU2458__ctor_m1025218848_MetadataUsageId;
extern "C"  void U24DragObjectU2458__ctor_m1025218848 (U24DragObjectU2458_t1090936582 * __this, float ___distance0, DragRigidbody_t2531437401 * ___self_1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24DragObjectU2458__ctor_m1025218848_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGenerator_1__ctor_m3362830377(__this, /*hidden argument*/GenericGenerator_1__ctor_m3362830377_MethodInfo_var);
		float L_0 = ___distance0;
		__this->set_U24distanceU2465_0(L_0);
		DragRigidbody_t2531437401 * L_1 = ___self_1;
		__this->set_U24self_U2466_1(L_1);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Object> DragRigidbody/$DragObject$58::GetEnumerator()
extern Il2CppClass* U24_t418036571_il2cpp_TypeInfo_var;
extern const uint32_t U24DragObjectU2458_GetEnumerator_m704288176_MetadataUsageId;
extern "C"  Il2CppObject* U24DragObjectU2458_GetEnumerator_m704288176 (U24DragObjectU2458_t1090936582 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24DragObjectU2458_GetEnumerator_m704288176_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_U24distanceU2465_0();
		DragRigidbody_t2531437401 * L_1 = __this->get_U24self_U2466_1();
		U24_t418036571 * L_2 = (U24_t418036571 *)il2cpp_codegen_object_new(U24_t418036571_il2cpp_TypeInfo_var);
		U24__ctor_m4141945653(L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void DragRigidbody/$DragObject$58/$::.ctor(System.Single,DragRigidbody)
extern const MethodInfo* GenericGeneratorEnumerator_1__ctor_m1767157197_MethodInfo_var;
extern const uint32_t U24__ctor_m4141945653_MetadataUsageId;
extern "C"  void U24__ctor_m4141945653 (U24_t418036571 * __this, float ___distance0, DragRigidbody_t2531437401 * ___self_1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24__ctor_m4141945653_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGeneratorEnumerator_1__ctor_m1767157197(__this, /*hidden argument*/GenericGeneratorEnumerator_1__ctor_m1767157197_MethodInfo_var);
		float L_0 = ___distance0;
		__this->set_U24distanceU2463_6(L_0);
		DragRigidbody_t2531437401 * L_1 = ___self_1;
		__this->set_U24self_U2464_7(L_1);
		return;
	}
}
// System.Boolean DragRigidbody/$DragObject$58/$::MoveNext()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_YieldDefault_m2922167810_MethodInfo_var;
extern const uint32_t U24_MoveNext_m3111963941_MetadataUsageId;
extern "C"  bool U24_MoveNext_m3111963941 (U24_t418036571 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24_MoveNext_m3111963941_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B8_0 = 0;
	{
		int32_t L_0 = ((GenericGeneratorEnumerator_1_t67576739 *)__this)->get__state_1();
		if (L_0 == 0)
		{
			goto IL_0017;
		}
		if (L_0 == 1)
		{
			goto IL_015f;
		}
		if (L_0 == 2)
		{
			goto IL_00eb;
		}
	}

IL_0017:
	{
		DragRigidbody_t2531437401 * L_1 = __this->get_U24self_U2464_7();
		NullCheck(L_1);
		SpringJoint_t558455091 * L_2 = L_1->get_springJoint_8();
		NullCheck(L_2);
		Rigidbody_t3346577219 * L_3 = Joint_get_connectedBody_m3483565964(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		float L_4 = Rigidbody_get_drag_m3710595753(L_3, /*hidden argument*/NULL);
		__this->set_U24oldDragU2459_2(L_4);
		DragRigidbody_t2531437401 * L_5 = __this->get_U24self_U2464_7();
		NullCheck(L_5);
		SpringJoint_t558455091 * L_6 = L_5->get_springJoint_8();
		NullCheck(L_6);
		Rigidbody_t3346577219 * L_7 = Joint_get_connectedBody_m3483565964(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		float L_8 = Rigidbody_get_angularDrag_m2925737411(L_7, /*hidden argument*/NULL);
		__this->set_U24oldAngularDragU2460_3(L_8);
		DragRigidbody_t2531437401 * L_9 = __this->get_U24self_U2464_7();
		NullCheck(L_9);
		SpringJoint_t558455091 * L_10 = L_9->get_springJoint_8();
		NullCheck(L_10);
		Rigidbody_t3346577219 * L_11 = Joint_get_connectedBody_m3483565964(L_10, /*hidden argument*/NULL);
		DragRigidbody_t2531437401 * L_12 = __this->get_U24self_U2464_7();
		NullCheck(L_12);
		float L_13 = L_12->get_drag_4();
		NullCheck(L_11);
		Rigidbody_set_drag_m4061586082(L_11, L_13, /*hidden argument*/NULL);
		DragRigidbody_t2531437401 * L_14 = __this->get_U24self_U2464_7();
		NullCheck(L_14);
		SpringJoint_t558455091 * L_15 = L_14->get_springJoint_8();
		NullCheck(L_15);
		Rigidbody_t3346577219 * L_16 = Joint_get_connectedBody_m3483565964(L_15, /*hidden argument*/NULL);
		DragRigidbody_t2531437401 * L_17 = __this->get_U24self_U2464_7();
		NullCheck(L_17);
		float L_18 = L_17->get_angularDrag_5();
		NullCheck(L_16);
		Rigidbody_set_angularDrag_m2909317064(L_16, L_18, /*hidden argument*/NULL);
		DragRigidbody_t2531437401 * L_19 = __this->get_U24self_U2464_7();
		NullCheck(L_19);
		Camera_t2727095145 * L_20 = VirtFuncInvoker0< Camera_t2727095145 * >::Invoke(6 /* UnityEngine.Camera DragRigidbody::FindCamera() */, L_19);
		__this->set_U24mainCameraU2461_4(L_20);
		goto IL_00eb;
	}

IL_00a3:
	{
		Camera_t2727095145 * L_21 = __this->get_U24mainCameraU2461_4();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		Vector3_t4282066566  L_22 = Input_get_mousePosition_m4020233228(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		Ray_t3134616544  L_23 = Camera_ScreenPointToRay_m1733083890(L_21, L_22, /*hidden argument*/NULL);
		__this->set_U24rayU2462_5(L_23);
		DragRigidbody_t2531437401 * L_24 = __this->get_U24self_U2464_7();
		NullCheck(L_24);
		SpringJoint_t558455091 * L_25 = L_24->get_springJoint_8();
		NullCheck(L_25);
		Transform_t1659122786 * L_26 = Component_get_transform_m4257140443(L_25, /*hidden argument*/NULL);
		Ray_t3134616544 * L_27 = __this->get_address_of_U24rayU2462_5();
		float L_28 = __this->get_U24distanceU2463_6();
		Vector3_t4282066566  L_29 = Ray_GetPoint_m1171104822(L_27, L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		Transform_set_position_m3111394108(L_26, L_29, /*hidden argument*/NULL);
		bool L_30 = GenericGeneratorEnumerator_1_YieldDefault_m2922167810(__this, 2, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m2922167810_MethodInfo_var);
		G_B8_0 = ((int32_t)(L_30));
		goto IL_0160;
	}

IL_00eb:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_31 = Input_GetMouseButton_m4080958081(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (L_31)
		{
			goto IL_00a3;
		}
	}
	{
		DragRigidbody_t2531437401 * L_32 = __this->get_U24self_U2464_7();
		NullCheck(L_32);
		SpringJoint_t558455091 * L_33 = L_32->get_springJoint_8();
		NullCheck(L_33);
		Rigidbody_t3346577219 * L_34 = Joint_get_connectedBody_m3483565964(L_33, /*hidden argument*/NULL);
		bool L_35 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_0157;
		}
	}
	{
		DragRigidbody_t2531437401 * L_36 = __this->get_U24self_U2464_7();
		NullCheck(L_36);
		SpringJoint_t558455091 * L_37 = L_36->get_springJoint_8();
		NullCheck(L_37);
		Rigidbody_t3346577219 * L_38 = Joint_get_connectedBody_m3483565964(L_37, /*hidden argument*/NULL);
		float L_39 = __this->get_U24oldDragU2459_2();
		NullCheck(L_38);
		Rigidbody_set_drag_m4061586082(L_38, L_39, /*hidden argument*/NULL);
		DragRigidbody_t2531437401 * L_40 = __this->get_U24self_U2464_7();
		NullCheck(L_40);
		SpringJoint_t558455091 * L_41 = L_40->get_springJoint_8();
		NullCheck(L_41);
		Rigidbody_t3346577219 * L_42 = Joint_get_connectedBody_m3483565964(L_41, /*hidden argument*/NULL);
		float L_43 = __this->get_U24oldAngularDragU2460_3();
		NullCheck(L_42);
		Rigidbody_set_angularDrag_m2909317064(L_42, L_43, /*hidden argument*/NULL);
		DragRigidbody_t2531437401 * L_44 = __this->get_U24self_U2464_7();
		NullCheck(L_44);
		SpringJoint_t558455091 * L_45 = L_44->get_springJoint_8();
		NullCheck(L_45);
		Joint_set_connectedBody_m2794572257(L_45, (Rigidbody_t3346577219 *)NULL, /*hidden argument*/NULL);
	}

IL_0157:
	{
		GenericGeneratorEnumerator_1_YieldDefault_m2922167810(__this, 1, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m2922167810_MethodInfo_var);
	}

IL_015f:
	{
		G_B8_0 = 0;
	}

IL_0160:
	{
		return (bool)G_B8_0;
	}
}
// System.Void EdgeDetectEffectNormals::.ctor()
extern "C"  void EdgeDetectEffectNormals__ctor_m2019444391 (EdgeDetectEffectNormals_t728271515 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase__ctor_m3869393199(__this, /*hidden argument*/NULL);
		__this->set_mode_5(3);
		__this->set_sensitivityDepth_6((1.0f));
		__this->set_sensitivityNormals_7((1.0f));
		__this->set_lumThreshhold_8((0.2f));
		__this->set_edgeExp_9((1.0f));
		__this->set_sampleDist_10((1.0f));
		Color_t4194546905  L_0 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_edgesOnlyBgColor_12(L_0);
		__this->set_oldMode_15(3);
		return;
	}
}
// System.Boolean EdgeDetectEffectNormals::CheckResources()
extern "C"  bool EdgeDetectEffectNormals_CheckResources_m2979558356 (EdgeDetectEffectNormals_t728271515 * __this, const MethodInfo* method)
{
	{
		VirtFuncInvoker1< bool, bool >::Invoke(10 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean) */, __this, (bool)1);
		Shader_t3191267369 * L_0 = __this->get_edgeDetectShader_13();
		Material_t3870600107 * L_1 = __this->get_edgeDetectMaterial_14();
		Material_t3870600107 * L_2 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_0, L_1);
		__this->set_edgeDetectMaterial_14(L_2);
		int32_t L_3 = __this->get_mode_5();
		int32_t L_4 = __this->get_oldMode_15();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0037;
		}
	}
	{
		VirtActionInvoker0::Invoke(18 /* System.Void EdgeDetectEffectNormals::SetCameraFlag() */, __this);
	}

IL_0037:
	{
		int32_t L_5 = __this->get_mode_5();
		__this->set_oldMode_15(L_5);
		bool L_6 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		if (L_6)
		{
			goto IL_0054;
		}
	}
	{
		VirtActionInvoker0::Invoke(13 /* System.Void PostEffectsBase::ReportAutoDisable() */, __this);
	}

IL_0054:
	{
		bool L_7 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		return L_7;
	}
}
// System.Void EdgeDetectEffectNormals::Start()
extern "C"  void EdgeDetectEffectNormals_Start_m966582183 (EdgeDetectEffectNormals_t728271515 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mode_5();
		__this->set_oldMode_15(L_0);
		return;
	}
}
// System.Void EdgeDetectEffectNormals::SetCameraFlag()
extern const MethodInfo* Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var;
extern const uint32_t EdgeDetectEffectNormals_SetCameraFlag_m2533427512_MetadataUsageId;
extern "C"  void EdgeDetectEffectNormals_SetCameraFlag_m2533427512 (EdgeDetectEffectNormals_t728271515 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EdgeDetectEffectNormals_SetCameraFlag_m2533427512_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_mode_5();
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = __this->get_mode_5();
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0035;
		}
	}

IL_0018:
	{
		Camera_t2727095145 * L_2 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		Camera_t2727095145 * L_3 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_3);
		int32_t L_4 = Camera_get_depthTextureMode_m2117446653(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Camera_set_depthTextureMode_m2368326786(L_2, ((int32_t)((int32_t)L_4|(int32_t)1)), /*hidden argument*/NULL);
		goto IL_0065;
	}

IL_0035:
	{
		int32_t L_5 = __this->get_mode_5();
		if ((((int32_t)L_5) == ((int32_t)0)))
		{
			goto IL_004d;
		}
	}
	{
		int32_t L_6 = __this->get_mode_5();
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_0065;
		}
	}

IL_004d:
	{
		Camera_t2727095145 * L_7 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		Camera_t2727095145 * L_8 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_8);
		int32_t L_9 = Camera_get_depthTextureMode_m2117446653(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Camera_set_depthTextureMode_m2368326786(L_7, ((int32_t)((int32_t)L_9|(int32_t)2)), /*hidden argument*/NULL);
	}

IL_0065:
	{
		return;
	}
}
// System.Void EdgeDetectEffectNormals::OnEnable()
extern "C"  void EdgeDetectEffectNormals_OnEnable_m3732515711 (EdgeDetectEffectNormals_t728271515 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(18 /* System.Void EdgeDetectEffectNormals::SetCameraFlag() */, __this);
		return;
	}
}
// System.Void EdgeDetectEffectNormals::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1610228928;
extern Il2CppCodeGenString* _stringLiteral400332192;
extern Il2CppCodeGenString* _stringLiteral11752958;
extern Il2CppCodeGenString* _stringLiteral3818017983;
extern Il2CppCodeGenString* _stringLiteral2523959342;
extern Il2CppCodeGenString* _stringLiteral3264544460;
extern const uint32_t EdgeDetectEffectNormals_OnRenderImage_m2515312983_MetadataUsageId;
extern "C"  void EdgeDetectEffectNormals_OnRenderImage_m2515312983 (EdgeDetectEffectNormals_t728271515 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EdgeDetectEffectNormals_OnRenderImage_m2515312983_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean EdgeDetectEffectNormals::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		RenderTexture_t1963041563 * L_1 = ___source0;
		RenderTexture_t1963041563 * L_2 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_00de;
	}

IL_0017:
	{
		float L_3 = __this->get_sensitivityDepth_6();
		float L_4 = __this->get_sensitivityNormals_7();
		Vector2_t4282066565  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector2__ctor_m1517109030(&L_5, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Material_t3870600107 * L_6 = __this->get_edgeDetectMaterial_14();
		float L_7 = (&V_0)->get_x_1();
		float L_8 = (&V_0)->get_y_2();
		float L_9 = (&V_0)->get_y_2();
		Vector4_t4282066567  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector4__ctor_m2441427762(&L_10, L_7, L_8, (1.0f), L_9, /*hidden argument*/NULL);
		NullCheck(L_6);
		Material_SetVector_m3505096203(L_6, _stringLiteral1610228928, L_10, /*hidden argument*/NULL);
		Material_t3870600107 * L_11 = __this->get_edgeDetectMaterial_14();
		float L_12 = __this->get_edgesOnly_11();
		NullCheck(L_11);
		Material_SetFloat_m981710063(L_11, _stringLiteral400332192, L_12, /*hidden argument*/NULL);
		Material_t3870600107 * L_13 = __this->get_edgeDetectMaterial_14();
		float L_14 = __this->get_sampleDist_10();
		NullCheck(L_13);
		Material_SetFloat_m981710063(L_13, _stringLiteral11752958, L_14, /*hidden argument*/NULL);
		Material_t3870600107 * L_15 = __this->get_edgeDetectMaterial_14();
		Color_t4194546905  L_16 = __this->get_edgesOnlyBgColor_12();
		Vector4_t4282066567  L_17 = Color_op_Implicit_m2638307542(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m3505096203(L_15, _stringLiteral3818017983, L_17, /*hidden argument*/NULL);
		Material_t3870600107 * L_18 = __this->get_edgeDetectMaterial_14();
		float L_19 = __this->get_edgeExp_9();
		NullCheck(L_18);
		Material_SetFloat_m981710063(L_18, _stringLiteral2523959342, L_19, /*hidden argument*/NULL);
		Material_t3870600107 * L_20 = __this->get_edgeDetectMaterial_14();
		float L_21 = __this->get_lumThreshhold_8();
		NullCheck(L_20);
		Material_SetFloat_m981710063(L_20, _stringLiteral3264544460, L_21, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_22 = ___source0;
		RenderTexture_t1963041563 * L_23 = ___destination1;
		Material_t3870600107 * L_24 = __this->get_edgeDetectMaterial_14();
		int32_t L_25 = __this->get_mode_5();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_22, L_23, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00de:
	{
		return;
	}
}
// System.Void EdgeDetectEffectNormals::Main()
extern "C"  void EdgeDetectEffectNormals_Main_m811921718 (EdgeDetectEffectNormals_t728271515 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FastBloom::.ctor()
extern "C"  void FastBloom__ctor_m2303717915 (FastBloom_t1950086887 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase__ctor_m3869393199(__this, /*hidden argument*/NULL);
		__this->set_threshhold_5((0.25f));
		__this->set_intensity_6((0.75f));
		__this->set_blurSize_7((1.0f));
		__this->set_resolution_8(0);
		__this->set_blurIterations_9(1);
		__this->set_blurType_10(0);
		return;
	}
}
// System.Boolean FastBloom::CheckResources()
extern "C"  bool FastBloom_CheckResources_m3169202272 (FastBloom_t1950086887 * __this, const MethodInfo* method)
{
	{
		VirtFuncInvoker1< bool, bool >::Invoke(10 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean) */, __this, (bool)0);
		Shader_t3191267369 * L_0 = __this->get_fastBloomShader_11();
		Material_t3870600107 * L_1 = __this->get_fastBloomMaterial_12();
		Material_t3870600107 * L_2 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_0, L_1);
		__this->set_fastBloomMaterial_12(L_2);
		bool L_3 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		if (L_3)
		{
			goto IL_0031;
		}
	}
	{
		VirtActionInvoker0::Invoke(13 /* System.Void PostEffectsBase::ReportAutoDisable() */, __this);
	}

IL_0031:
	{
		bool L_4 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		return L_4;
	}
}
// System.Void FastBloom::OnDisable()
extern "C"  void FastBloom_OnDisable_m2877997314 (FastBloom_t1950086887 * __this, const MethodInfo* method)
{
	{
		Material_t3870600107 * L_0 = __this->get_fastBloomMaterial_12();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t3870600107 * L_2 = __this->get_fastBloomMaterial_12();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void FastBloom::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral2469514762;
extern Il2CppCodeGenString* _stringLiteral2784049380;
extern const uint32_t FastBloom_OnRenderImage_m1022115171_MetadataUsageId;
extern "C"  void FastBloom_OnRenderImage_m1022115171 (FastBloom_t1950086887 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FastBloom_OnRenderImage_m1022115171_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	RenderTexture_t1963041563 * V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	RenderTexture_t1963041563 * V_7 = NULL;
	int32_t G_B5_0 = 0;
	float G_B8_0 = 0.0f;
	int32_t G_B11_0 = 0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean FastBloom::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		RenderTexture_t1963041563 * L_1 = ___source0;
		RenderTexture_t1963041563 * L_2 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_01aa;
	}

IL_0017:
	{
		int32_t L_3 = __this->get_resolution_8();
		if ((!(((uint32_t)L_3) == ((uint32_t)0))))
		{
			goto IL_0029;
		}
	}
	{
		G_B5_0 = 4;
		goto IL_002a;
	}

IL_0029:
	{
		G_B5_0 = 2;
	}

IL_002a:
	{
		V_0 = G_B5_0;
		int32_t L_4 = __this->get_resolution_8();
		if ((!(((uint32_t)L_4) == ((uint32_t)0))))
		{
			goto IL_0041;
		}
	}
	{
		G_B8_0 = (0.5f);
		goto IL_0046;
	}

IL_0041:
	{
		G_B8_0 = (1.0f);
	}

IL_0046:
	{
		V_1 = G_B8_0;
		Material_t3870600107 * L_5 = __this->get_fastBloomMaterial_12();
		float L_6 = __this->get_blurSize_7();
		float L_7 = V_1;
		float L_8 = __this->get_threshhold_5();
		float L_9 = __this->get_intensity_6();
		Vector4_t4282066567  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector4__ctor_m2441427762(&L_10, ((float)((float)L_6*(float)L_7)), (((float)((float)0))), L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_5);
		Material_SetVector_m3505096203(L_5, _stringLiteral2469514762, L_10, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_11 = ___source0;
		NullCheck(L_11);
		Texture_set_filterMode_m3842701708(L_11, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_12 = ___source0;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_width_m1498578543(L_12, /*hidden argument*/NULL);
		int32_t L_14 = V_0;
		V_2 = ((int32_t)((int32_t)L_13/(int32_t)L_14));
		RenderTexture_t1963041563 * L_15 = ___source0;
		NullCheck(L_15);
		int32_t L_16 = RenderTexture_get_height_m4010076224(L_15, /*hidden argument*/NULL);
		int32_t L_17 = V_0;
		V_3 = ((int32_t)((int32_t)L_16/(int32_t)L_17));
		int32_t L_18 = V_2;
		int32_t L_19 = V_3;
		RenderTexture_t1963041563 * L_20 = ___source0;
		NullCheck(L_20);
		int32_t L_21 = RenderTexture_get_format_m3502109954(L_20, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_22 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, L_18, L_19, 0, L_21, /*hidden argument*/NULL);
		V_4 = L_22;
		RenderTexture_t1963041563 * L_23 = V_4;
		NullCheck(L_23);
		Texture_set_filterMode_m3842701708(L_23, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_24 = ___source0;
		RenderTexture_t1963041563 * L_25 = V_4;
		Material_t3870600107 * L_26 = __this->get_fastBloomMaterial_12();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_24, L_25, L_26, 1, /*hidden argument*/NULL);
		int32_t L_27 = __this->get_blurType_10();
		if ((!(((uint32_t)L_27) == ((uint32_t)0))))
		{
			goto IL_00c4;
		}
	}
	{
		G_B11_0 = 0;
		goto IL_00c5;
	}

IL_00c4:
	{
		G_B11_0 = 2;
	}

IL_00c5:
	{
		V_5 = G_B11_0;
		V_6 = 0;
		goto IL_0176;
	}

IL_00cf:
	{
		Material_t3870600107 * L_28 = __this->get_fastBloomMaterial_12();
		float L_29 = __this->get_blurSize_7();
		float L_30 = V_1;
		int32_t L_31 = V_6;
		float L_32 = __this->get_threshhold_5();
		float L_33 = __this->get_intensity_6();
		Vector4_t4282066567  L_34;
		memset(&L_34, 0, sizeof(L_34));
		Vector4__ctor_m2441427762(&L_34, ((float)((float)((float)((float)L_29*(float)L_30))+(float)((float)((float)(((float)((float)L_31)))*(float)(1.0f))))), (((float)((float)0))), L_32, L_33, /*hidden argument*/NULL);
		NullCheck(L_28);
		Material_SetVector_m3505096203(L_28, _stringLiteral2469514762, L_34, /*hidden argument*/NULL);
		int32_t L_35 = V_2;
		int32_t L_36 = V_3;
		RenderTexture_t1963041563 * L_37 = ___source0;
		NullCheck(L_37);
		int32_t L_38 = RenderTexture_get_format_m3502109954(L_37, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_39 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, L_35, L_36, 0, L_38, /*hidden argument*/NULL);
		V_7 = L_39;
		RenderTexture_t1963041563 * L_40 = V_7;
		NullCheck(L_40);
		Texture_set_filterMode_m3842701708(L_40, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_41 = V_4;
		RenderTexture_t1963041563 * L_42 = V_7;
		Material_t3870600107 * L_43 = __this->get_fastBloomMaterial_12();
		int32_t L_44 = V_5;
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_41, L_42, L_43, ((int32_t)((int32_t)2+(int32_t)L_44)), /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_45 = V_4;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_46 = V_7;
		V_4 = L_46;
		int32_t L_47 = V_2;
		int32_t L_48 = V_3;
		RenderTexture_t1963041563 * L_49 = ___source0;
		NullCheck(L_49);
		int32_t L_50 = RenderTexture_get_format_m3502109954(L_49, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_51 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, L_47, L_48, 0, L_50, /*hidden argument*/NULL);
		V_7 = L_51;
		RenderTexture_t1963041563 * L_52 = V_7;
		NullCheck(L_52);
		Texture_set_filterMode_m3842701708(L_52, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_53 = V_4;
		RenderTexture_t1963041563 * L_54 = V_7;
		Material_t3870600107 * L_55 = __this->get_fastBloomMaterial_12();
		int32_t L_56 = V_5;
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_53, L_54, L_55, ((int32_t)((int32_t)3+(int32_t)L_56)), /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_57 = V_4;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_58 = V_7;
		V_4 = L_58;
		int32_t L_59 = V_6;
		V_6 = ((int32_t)((int32_t)L_59+(int32_t)1));
	}

IL_0176:
	{
		int32_t L_60 = V_6;
		int32_t L_61 = __this->get_blurIterations_9();
		if ((((int32_t)L_60) < ((int32_t)L_61)))
		{
			goto IL_00cf;
		}
	}
	{
		Material_t3870600107 * L_62 = __this->get_fastBloomMaterial_12();
		RenderTexture_t1963041563 * L_63 = V_4;
		NullCheck(L_62);
		Material_SetTexture_m1833724755(L_62, _stringLiteral2784049380, L_63, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_64 = ___source0;
		RenderTexture_t1963041563 * L_65 = ___destination1;
		Material_t3870600107 * L_66 = __this->get_fastBloomMaterial_12();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_64, L_65, L_66, 0, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_67 = V_4;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_67, /*hidden argument*/NULL);
	}

IL_01aa:
	{
		return;
	}
}
// System.Void FastBloom::Main()
extern "C"  void FastBloom_Main_m3314943810 (FastBloom_t1950086887 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Fisheye::.ctor()
extern "C"  void Fisheye__ctor_m1686144073 (Fisheye_t816213177 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase__ctor_m3869393199(__this, /*hidden argument*/NULL);
		__this->set_strengthX_5((0.05f));
		__this->set_strengthY_6((0.05f));
		return;
	}
}
// System.Boolean Fisheye::CheckResources()
extern "C"  bool Fisheye_CheckResources_m794321010 (Fisheye_t816213177 * __this, const MethodInfo* method)
{
	{
		VirtFuncInvoker1< bool, bool >::Invoke(10 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean) */, __this, (bool)0);
		Shader_t3191267369 * L_0 = __this->get_fishEyeShader_7();
		Material_t3870600107 * L_1 = __this->get_fisheyeMaterial_8();
		Material_t3870600107 * L_2 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_0, L_1);
		__this->set_fisheyeMaterial_8(L_2);
		bool L_3 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		if (L_3)
		{
			goto IL_0031;
		}
	}
	{
		VirtActionInvoker0::Invoke(13 /* System.Void PostEffectsBase::ReportAutoDisable() */, __this);
	}

IL_0031:
	{
		bool L_4 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		return L_4;
	}
}
// System.Void Fisheye::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral499324979;
extern const uint32_t Fisheye_OnRenderImage_m3798398581_MetadataUsageId;
extern "C"  void Fisheye_OnRenderImage_m3798398581 (Fisheye_t816213177 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Fisheye_OnRenderImage_m3798398581_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Fisheye::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		RenderTexture_t1963041563 * L_1 = ___source0;
		RenderTexture_t1963041563 * L_2 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_007f;
	}

IL_0017:
	{
		V_0 = (0.15625f);
		RenderTexture_t1963041563 * L_3 = ___source0;
		NullCheck(L_3);
		int32_t L_4 = RenderTexture_get_width_m1498578543(L_3, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_5 = ___source0;
		NullCheck(L_5);
		int32_t L_6 = RenderTexture_get_height_m4010076224(L_5, /*hidden argument*/NULL);
		V_1 = ((float)((float)((float)((float)(((float)((float)L_4)))*(float)(1.0f)))/(float)((float)((float)(((float)((float)L_6)))*(float)(1.0f)))));
		Material_t3870600107 * L_7 = __this->get_fisheyeMaterial_8();
		float L_8 = __this->get_strengthX_5();
		float L_9 = V_1;
		float L_10 = V_0;
		float L_11 = __this->get_strengthY_6();
		float L_12 = V_0;
		float L_13 = __this->get_strengthX_5();
		float L_14 = V_1;
		float L_15 = V_0;
		float L_16 = __this->get_strengthY_6();
		float L_17 = V_0;
		Vector4_t4282066567  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector4__ctor_m2441427762(&L_18, ((float)((float)((float)((float)L_8*(float)L_9))*(float)L_10)), ((float)((float)L_11*(float)L_12)), ((float)((float)((float)((float)L_13*(float)L_14))*(float)L_15)), ((float)((float)L_16*(float)L_17)), /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_SetVector_m3505096203(L_7, _stringLiteral499324979, L_18, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_19 = ___source0;
		RenderTexture_t1963041563 * L_20 = ___destination1;
		Material_t3870600107 * L_21 = __this->get_fisheyeMaterial_8();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_19, L_20, L_21, /*hidden argument*/NULL);
	}

IL_007f:
	{
		return;
	}
}
// System.Void Fisheye::Main()
extern "C"  void Fisheye_Main_m2048096084 (Fisheye_t816213177 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GlobalFog::.ctor()
extern "C"  void GlobalFog__ctor_m1378399623 (GlobalFog_t2391894523 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase__ctor_m3869393199(__this, /*hidden argument*/NULL);
		__this->set_fogMode_5(0);
		__this->set_CAMERA_NEAR_6((0.5f));
		__this->set_CAMERA_FAR_7((50.0f));
		__this->set_CAMERA_FOV_8((60.0f));
		__this->set_CAMERA_ASPECT_RATIO_9((1.333333f));
		__this->set_startDistance_10((200.0f));
		__this->set_globalDensity_11((1.0f));
		__this->set_heightScale_12((100.0f));
		Color_t4194546905  L_0 = Color_get_grey_m3805481615(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_globalFogColor_14(L_0);
		return;
	}
}
// System.Boolean GlobalFog::CheckResources()
extern "C"  bool GlobalFog_CheckResources_m1731495540 (GlobalFog_t2391894523 * __this, const MethodInfo* method)
{
	{
		VirtFuncInvoker1< bool, bool >::Invoke(10 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean) */, __this, (bool)1);
		Shader_t3191267369 * L_0 = __this->get_fogShader_15();
		Material_t3870600107 * L_1 = __this->get_fogMaterial_16();
		Material_t3870600107 * L_2 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_0, L_1);
		__this->set_fogMaterial_16(L_2);
		bool L_3 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		if (L_3)
		{
			goto IL_0031;
		}
	}
	{
		VirtActionInvoker0::Invoke(13 /* System.Void PostEffectsBase::ReportAutoDisable() */, __this);
	}

IL_0031:
	{
		bool L_4 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		return L_4;
	}
}
// System.Void GlobalFog::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* Vector4_t4282066567_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3031435095;
extern Il2CppCodeGenString* _stringLiteral5582400;
extern Il2CppCodeGenString* _stringLiteral431109208;
extern Il2CppCodeGenString* _stringLiteral3035;
extern Il2CppCodeGenString* _stringLiteral177167782;
extern Il2CppCodeGenString* _stringLiteral275943108;
extern const uint32_t GlobalFog_OnRenderImage_m2056742519_MetadataUsageId;
extern "C"  void GlobalFog_OnRenderImage_m2056742519 (GlobalFog_t2391894523 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GlobalFog_OnRenderImage_m2056742519_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t1651859333  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t4282066567  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t4282066566  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t4282066566  V_6;
	memset(&V_6, 0, sizeof(V_6));
	float V_7 = 0.0f;
	Vector3_t4282066566  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t4282066566  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t4282066566  V_10;
	memset(&V_10, 0, sizeof(V_10));
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean GlobalFog::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		RenderTexture_t1963041563 * L_1 = ___source0;
		RenderTexture_t1963041563 * L_2 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_02f8;
	}

IL_0017:
	{
		Camera_t2727095145 * L_3 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_3);
		float L_4 = Camera_get_nearClipPlane_m4074655061(L_3, /*hidden argument*/NULL);
		__this->set_CAMERA_NEAR_6(L_4);
		Camera_t2727095145 * L_5 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_5);
		float L_6 = Camera_get_farClipPlane_m388706726(L_5, /*hidden argument*/NULL);
		__this->set_CAMERA_FAR_7(L_6);
		Camera_t2727095145 * L_7 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_7);
		float L_8 = Camera_get_fieldOfView_m65126887(L_7, /*hidden argument*/NULL);
		__this->set_CAMERA_FOV_8(L_8);
		Camera_t2727095145 * L_9 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_9);
		float L_10 = Camera_get_aspect_m4145685929(L_9, /*hidden argument*/NULL);
		__this->set_CAMERA_ASPECT_RATIO_9(L_10);
		Matrix4x4_t1651859333  L_11 = Matrix4x4_get_identity_m3946683782(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_11;
		Initobj (Vector4_t4282066567_il2cpp_TypeInfo_var, (&V_1));
		Initobj (Vector3_t4282066566_il2cpp_TypeInfo_var, (&V_2));
		float L_12 = __this->get_CAMERA_FOV_8();
		V_3 = ((float)((float)L_12*(float)(0.5f)));
		Camera_t2727095145 * L_13 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_13);
		Transform_t1659122786 * L_14 = Component_get_transform_m4257140443(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t4282066566  L_15 = Transform_get_right_m2070836824(L_14, /*hidden argument*/NULL);
		float L_16 = __this->get_CAMERA_NEAR_6();
		Vector3_t4282066566  L_17 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		float L_18 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_19 = tanf(((float)((float)L_18*(float)(0.0174532924f))));
		Vector3_t4282066566  L_20 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_17, L_19, /*hidden argument*/NULL);
		float L_21 = __this->get_CAMERA_ASPECT_RATIO_9();
		Vector3_t4282066566  L_22 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		V_4 = L_22;
		Camera_t2727095145 * L_23 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_23);
		Transform_t1659122786 * L_24 = Component_get_transform_m4257140443(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_t4282066566  L_25 = Transform_get_up_m297874561(L_24, /*hidden argument*/NULL);
		float L_26 = __this->get_CAMERA_NEAR_6();
		Vector3_t4282066566  L_27 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
		float L_28 = V_3;
		float L_29 = tanf(((float)((float)L_28*(float)(0.0174532924f))));
		Vector3_t4282066566  L_30 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_27, L_29, /*hidden argument*/NULL);
		V_5 = L_30;
		Camera_t2727095145 * L_31 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_31);
		Transform_t1659122786 * L_32 = Component_get_transform_m4257140443(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		Vector3_t4282066566  L_33 = Transform_get_forward_m877665793(L_32, /*hidden argument*/NULL);
		float L_34 = __this->get_CAMERA_NEAR_6();
		Vector3_t4282066566  L_35 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		Vector3_t4282066566  L_36 = V_4;
		Vector3_t4282066566  L_37 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		Vector3_t4282066566  L_38 = V_5;
		Vector3_t4282066566  L_39 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_37, L_38, /*hidden argument*/NULL);
		V_6 = L_39;
		float L_40 = Vector3_get_magnitude_m989985786((&V_6), /*hidden argument*/NULL);
		float L_41 = __this->get_CAMERA_FAR_7();
		float L_42 = __this->get_CAMERA_NEAR_6();
		V_7 = ((float)((float)((float)((float)L_40*(float)L_41))/(float)L_42));
		Vector3_Normalize_m3984983796((&V_6), /*hidden argument*/NULL);
		Vector3_t4282066566  L_43 = V_6;
		float L_44 = V_7;
		Vector3_t4282066566  L_45 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		V_6 = L_45;
		Camera_t2727095145 * L_46 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_46);
		Transform_t1659122786 * L_47 = Component_get_transform_m4257140443(L_46, /*hidden argument*/NULL);
		NullCheck(L_47);
		Vector3_t4282066566  L_48 = Transform_get_forward_m877665793(L_47, /*hidden argument*/NULL);
		float L_49 = __this->get_CAMERA_NEAR_6();
		Vector3_t4282066566  L_50 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_48, L_49, /*hidden argument*/NULL);
		Vector3_t4282066566  L_51 = V_4;
		Vector3_t4282066566  L_52 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_50, L_51, /*hidden argument*/NULL);
		Vector3_t4282066566  L_53 = V_5;
		Vector3_t4282066566  L_54 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_52, L_53, /*hidden argument*/NULL);
		V_8 = L_54;
		Vector3_Normalize_m3984983796((&V_8), /*hidden argument*/NULL);
		Vector3_t4282066566  L_55 = V_8;
		float L_56 = V_7;
		Vector3_t4282066566  L_57 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_55, L_56, /*hidden argument*/NULL);
		V_8 = L_57;
		Camera_t2727095145 * L_58 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_58);
		Transform_t1659122786 * L_59 = Component_get_transform_m4257140443(L_58, /*hidden argument*/NULL);
		NullCheck(L_59);
		Vector3_t4282066566  L_60 = Transform_get_forward_m877665793(L_59, /*hidden argument*/NULL);
		float L_61 = __this->get_CAMERA_NEAR_6();
		Vector3_t4282066566  L_62 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_60, L_61, /*hidden argument*/NULL);
		Vector3_t4282066566  L_63 = V_4;
		Vector3_t4282066566  L_64 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_62, L_63, /*hidden argument*/NULL);
		Vector3_t4282066566  L_65 = V_5;
		Vector3_t4282066566  L_66 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_64, L_65, /*hidden argument*/NULL);
		V_9 = L_66;
		Vector3_Normalize_m3984983796((&V_9), /*hidden argument*/NULL);
		Vector3_t4282066566  L_67 = V_9;
		float L_68 = V_7;
		Vector3_t4282066566  L_69 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_67, L_68, /*hidden argument*/NULL);
		V_9 = L_69;
		Camera_t2727095145 * L_70 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_70);
		Transform_t1659122786 * L_71 = Component_get_transform_m4257140443(L_70, /*hidden argument*/NULL);
		NullCheck(L_71);
		Vector3_t4282066566  L_72 = Transform_get_forward_m877665793(L_71, /*hidden argument*/NULL);
		float L_73 = __this->get_CAMERA_NEAR_6();
		Vector3_t4282066566  L_74 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_72, L_73, /*hidden argument*/NULL);
		Vector3_t4282066566  L_75 = V_4;
		Vector3_t4282066566  L_76 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_74, L_75, /*hidden argument*/NULL);
		Vector3_t4282066566  L_77 = V_5;
		Vector3_t4282066566  L_78 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_76, L_77, /*hidden argument*/NULL);
		V_10 = L_78;
		Vector3_Normalize_m3984983796((&V_10), /*hidden argument*/NULL);
		Vector3_t4282066566  L_79 = V_10;
		float L_80 = V_7;
		Vector3_t4282066566  L_81 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_79, L_80, /*hidden argument*/NULL);
		V_10 = L_81;
		Vector3_t4282066566  L_82 = V_6;
		Vector4_t4282066567  L_83 = Vector4_op_Implicit_m331673271(NULL /*static, unused*/, L_82, /*hidden argument*/NULL);
		Matrix4x4_SetRow_m242342709((&V_0), 0, L_83, /*hidden argument*/NULL);
		Vector3_t4282066566  L_84 = V_8;
		Vector4_t4282066567  L_85 = Vector4_op_Implicit_m331673271(NULL /*static, unused*/, L_84, /*hidden argument*/NULL);
		Matrix4x4_SetRow_m242342709((&V_0), 1, L_85, /*hidden argument*/NULL);
		Vector3_t4282066566  L_86 = V_9;
		Vector4_t4282066567  L_87 = Vector4_op_Implicit_m331673271(NULL /*static, unused*/, L_86, /*hidden argument*/NULL);
		Matrix4x4_SetRow_m242342709((&V_0), 2, L_87, /*hidden argument*/NULL);
		Vector3_t4282066566  L_88 = V_10;
		Vector4_t4282066567  L_89 = Vector4_op_Implicit_m331673271(NULL /*static, unused*/, L_88, /*hidden argument*/NULL);
		Matrix4x4_SetRow_m242342709((&V_0), 3, L_89, /*hidden argument*/NULL);
		Material_t3870600107 * L_90 = __this->get_fogMaterial_16();
		Matrix4x4_t1651859333  L_91 = V_0;
		NullCheck(L_90);
		Material_SetMatrix_m3693790735(L_90, _stringLiteral3031435095, L_91, /*hidden argument*/NULL);
		Material_t3870600107 * L_92 = __this->get_fogMaterial_16();
		Camera_t2727095145 * L_93 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_93);
		Transform_t1659122786 * L_94 = Component_get_transform_m4257140443(L_93, /*hidden argument*/NULL);
		NullCheck(L_94);
		Vector3_t4282066566  L_95 = Transform_get_position_m2211398607(L_94, /*hidden argument*/NULL);
		Vector4_t4282066567  L_96 = Vector4_op_Implicit_m331673271(NULL /*static, unused*/, L_95, /*hidden argument*/NULL);
		NullCheck(L_92);
		Material_SetVector_m3505096203(L_92, _stringLiteral5582400, L_96, /*hidden argument*/NULL);
		Material_t3870600107 * L_97 = __this->get_fogMaterial_16();
		float L_98 = __this->get_startDistance_10();
		float L_99 = V_7;
		float L_100 = __this->get_startDistance_10();
		Vector4_t4282066567  L_101;
		memset(&L_101, 0, sizeof(L_101));
		Vector4__ctor_m2176640552(&L_101, ((float)((float)(1.0f)/(float)L_98)), ((float)((float)L_99-(float)L_100)), /*hidden argument*/NULL);
		NullCheck(L_97);
		Material_SetVector_m3505096203(L_97, _stringLiteral431109208, L_101, /*hidden argument*/NULL);
		Material_t3870600107 * L_102 = __this->get_fogMaterial_16();
		float L_103 = __this->get_height_13();
		float L_104 = __this->get_heightScale_12();
		Vector4_t4282066567  L_105;
		memset(&L_105, 0, sizeof(L_105));
		Vector4__ctor_m2176640552(&L_105, L_103, ((float)((float)(1.0f)/(float)L_104)), /*hidden argument*/NULL);
		NullCheck(L_102);
		Material_SetVector_m3505096203(L_102, _stringLiteral3035, L_105, /*hidden argument*/NULL);
		Material_t3870600107 * L_106 = __this->get_fogMaterial_16();
		float L_107 = __this->get_globalDensity_11();
		NullCheck(L_106);
		Material_SetFloat_m981710063(L_106, _stringLiteral177167782, ((float)((float)L_107*(float)(0.01f))), /*hidden argument*/NULL);
		Material_t3870600107 * L_108 = __this->get_fogMaterial_16();
		Color_t4194546905  L_109 = __this->get_globalFogColor_14();
		NullCheck(L_108);
		Material_SetColor_m1918430019(L_108, _stringLiteral275943108, L_109, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_110 = ___source0;
		RenderTexture_t1963041563 * L_111 = ___destination1;
		Material_t3870600107 * L_112 = __this->get_fogMaterial_16();
		int32_t L_113 = __this->get_fogMode_5();
		GlobalFog_CustomGraphicsBlit_m1154040163(NULL /*static, unused*/, L_110, L_111, L_112, L_113, /*hidden argument*/NULL);
	}

IL_02f8:
	{
		return;
	}
}
// System.Void GlobalFog::CustomGraphicsBlit(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32)
extern Il2CppCodeGenString* _stringLiteral558922319;
extern const uint32_t GlobalFog_CustomGraphicsBlit_m1154040163_MetadataUsageId;
extern "C"  void GlobalFog_CustomGraphicsBlit_m1154040163 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___dest1, Material_t3870600107 * ___fxMaterial2, int32_t ___passNr3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GlobalFog_CustomGraphicsBlit_m1154040163_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		RenderTexture_t1963041563 * L_0 = ___dest1;
		RenderTexture_set_active_m1002947377(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Material_t3870600107 * L_1 = ___fxMaterial2;
		RenderTexture_t1963041563 * L_2 = ___source0;
		NullCheck(L_1);
		Material_SetTexture_m1833724755(L_1, _stringLiteral558922319, L_2, /*hidden argument*/NULL);
		GL_PushMatrix_m626765559(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_LoadOrtho_m1297524312(NULL /*static, unused*/, /*hidden argument*/NULL);
		Material_t3870600107 * L_3 = ___fxMaterial2;
		int32_t L_4 = ___passNr3;
		NullCheck(L_3);
		Material_SetPass_m4241824642(L_3, L_4, /*hidden argument*/NULL);
		GL_Begin_m3089952800(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		GL_MultiTexCoord2_m4253939586(NULL /*static, unused*/, 0, (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		GL_Vertex3_m3691855584(NULL /*static, unused*/, (((float)((float)0))), (((float)((float)0))), (3.0f), /*hidden argument*/NULL);
		GL_MultiTexCoord2_m4253939586(NULL /*static, unused*/, 0, (1.0f), (((float)((float)0))), /*hidden argument*/NULL);
		GL_Vertex3_m3691855584(NULL /*static, unused*/, (1.0f), (((float)((float)0))), (2.0f), /*hidden argument*/NULL);
		GL_MultiTexCoord2_m4253939586(NULL /*static, unused*/, 0, (1.0f), (1.0f), /*hidden argument*/NULL);
		GL_Vertex3_m3691855584(NULL /*static, unused*/, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		GL_MultiTexCoord2_m4253939586(NULL /*static, unused*/, 0, (((float)((float)0))), (1.0f), /*hidden argument*/NULL);
		GL_Vertex3_m3691855584(NULL /*static, unused*/, (((float)((float)0))), (1.0f), (((float)((float)0))), /*hidden argument*/NULL);
		GL_End_m2013837889(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m3073322328(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GlobalFog::Main()
extern "C"  void GlobalFog_Main_m237053526 (GlobalFog_t2391894523 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MouseOrbit::.ctor()
extern "C"  void MouseOrbit__ctor_m2913930911 (MouseOrbit_t2986430149 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		__this->set_distance_3((10.0f));
		__this->set_xSpeed_4((250.0f));
		__this->set_ySpeed_5((120.0f));
		__this->set_yMinLimit_6(((int32_t)-20));
		__this->set_yMaxLimit_7(((int32_t)80));
		return;
	}
}
// System.Void MouseOrbit::Start()
extern const MethodInfo* Component_GetComponent_TisRigidbody_t3346577219_m354583034_MethodInfo_var;
extern const uint32_t MouseOrbit_Start_m1861068703_MetadataUsageId;
extern "C"  void MouseOrbit_Start_m1861068703 (MouseOrbit_t2986430149 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MouseOrbit_Start_m1861068703_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t1659122786 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t4282066566  L_1 = Transform_get_eulerAngles_m1058084741(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_y_2();
		__this->set_x_8(L_2);
		float L_3 = (&V_0)->get_x_1();
		__this->set_y_9(L_3);
		Rigidbody_t3346577219 * L_4 = Component_GetComponent_TisRigidbody_t3346577219_m354583034(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t3346577219_m354583034_MethodInfo_var);
		bool L_5 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0042;
		}
	}
	{
		Rigidbody_t3346577219 * L_6 = Component_GetComponent_TisRigidbody_t3346577219_m354583034(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t3346577219_m354583034_MethodInfo_var);
		NullCheck(L_6);
		Rigidbody_set_freezeRotation_m3989473889(L_6, (bool)1, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.Void MouseOrbit::LateUpdate()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2907718525;
extern Il2CppCodeGenString* _stringLiteral2907718526;
extern const uint32_t MouseOrbit_LateUpdate_m4088497940_MetadataUsageId;
extern "C"  void MouseOrbit_LateUpdate_m4088497940 (MouseOrbit_t2986430149 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MouseOrbit_LateUpdate_m4088497940_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_t1659122786 * L_0 = __this->get_target_2();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00ca;
		}
	}
	{
		float L_2 = __this->get_x_8();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		float L_3 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2907718525, /*hidden argument*/NULL);
		float L_4 = __this->get_xSpeed_4();
		__this->set_x_8(((float)((float)L_2+(float)((float)((float)((float)((float)L_3*(float)L_4))*(float)(0.02f))))));
		float L_5 = __this->get_y_9();
		float L_6 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2907718526, /*hidden argument*/NULL);
		float L_7 = __this->get_ySpeed_5();
		__this->set_y_9(((float)((float)L_5-(float)((float)((float)((float)((float)L_6*(float)L_7))*(float)(0.02f))))));
		float L_8 = __this->get_y_9();
		int32_t L_9 = __this->get_yMinLimit_6();
		int32_t L_10 = __this->get_yMaxLimit_7();
		float L_11 = MouseOrbit_ClampAngle_m512396460(NULL /*static, unused*/, L_8, (((float)((float)L_9))), (((float)((float)L_10))), /*hidden argument*/NULL);
		__this->set_y_9(L_11);
		float L_12 = __this->get_y_9();
		float L_13 = __this->get_x_8();
		Quaternion_t1553702882  L_14 = Quaternion_Euler_m1204688217(NULL /*static, unused*/, L_12, L_13, (((float)((float)0))), /*hidden argument*/NULL);
		V_0 = L_14;
		Quaternion_t1553702882  L_15 = V_0;
		float L_16 = __this->get_distance_3();
		Vector3_t4282066566  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector3__ctor_m2926210380(&L_17, (((float)((float)0))), (((float)((float)0))), ((-L_16)), /*hidden argument*/NULL);
		Vector3_t4282066566  L_18 = Quaternion_op_Multiply_m3771288979(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/NULL);
		Transform_t1659122786 * L_19 = __this->get_target_2();
		NullCheck(L_19);
		Vector3_t4282066566  L_20 = Transform_get_position_m2211398607(L_19, /*hidden argument*/NULL);
		Vector3_t4282066566  L_21 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_18, L_20, /*hidden argument*/NULL);
		V_1 = L_21;
		Transform_t1659122786 * L_22 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_23 = V_0;
		NullCheck(L_22);
		Transform_set_rotation_m1525803229(L_22, L_23, /*hidden argument*/NULL);
		Transform_t1659122786 * L_24 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_25 = V_1;
		NullCheck(L_24);
		Transform_set_position_m3111394108(L_24, L_25, /*hidden argument*/NULL);
	}

IL_00ca:
	{
		return;
	}
}
// System.Single MouseOrbit::ClampAngle(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t MouseOrbit_ClampAngle_m512396460_MetadataUsageId;
extern "C"  float MouseOrbit_ClampAngle_m512396460 (Il2CppObject * __this /* static, unused */, float ___angle0, float ___min1, float ___max2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MouseOrbit_ClampAngle_m512396460_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___angle0;
		if ((((float)L_0) >= ((float)(((float)((float)((int32_t)-360)))))))
		{
			goto IL_001a;
		}
	}
	{
		float L_1 = ___angle0;
		___angle0 = ((float)((float)L_1+(float)(((float)((float)((int32_t)360))))));
	}

IL_001a:
	{
		float L_2 = ___angle0;
		if ((((float)L_2) <= ((float)(((float)((float)((int32_t)360)))))))
		{
			goto IL_0034;
		}
	}
	{
		float L_3 = ___angle0;
		___angle0 = ((float)((float)L_3-(float)(((float)((float)((int32_t)360))))));
	}

IL_0034:
	{
		float L_4 = ___angle0;
		float L_5 = ___min1;
		float L_6 = ___max2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void MouseOrbit::Main()
extern "C"  void MouseOrbit_Main_m979323454 (MouseOrbit_t2986430149 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NoiseAndGrain::.ctor()
extern "C"  void NoiseAndGrain__ctor_m568501604 (NoiseAndGrain_t429516286 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase__ctor_m3869393199(__this, /*hidden argument*/NULL);
		__this->set_intensityMultiplier_5((0.25f));
		__this->set_generalIntensity_6((0.5f));
		__this->set_blackIntensity_7((1.0f));
		__this->set_whiteIntensity_8((1.0f));
		__this->set_midGrey_9((0.2f));
		Vector3_t4282066566  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2926210380(&L_0, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_intensities_13(L_0);
		Vector3_t4282066566  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector3__ctor_m2926210380(&L_1, (64.0f), (64.0f), (64.0f), /*hidden argument*/NULL);
		__this->set_tiling_14(L_1);
		__this->set_monochromeTiling_15((64.0f));
		__this->set_filterMode_16(1);
		return;
	}
}
// System.Void NoiseAndGrain::.cctor()
extern Il2CppClass* NoiseAndGrain_t429516286_il2cpp_TypeInfo_var;
extern const uint32_t NoiseAndGrain__cctor_m4256551625_MetadataUsageId;
extern "C"  void NoiseAndGrain__cctor_m4256551625 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NoiseAndGrain__cctor_m4256551625_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((NoiseAndGrain_t429516286_StaticFields*)NoiseAndGrain_t429516286_il2cpp_TypeInfo_var->static_fields)->set_TILE_AMOUNT_22((64.0f));
		return;
	}
}
// System.Boolean NoiseAndGrain::CheckResources()
extern "C"  bool NoiseAndGrain_CheckResources_m1557307959 (NoiseAndGrain_t429516286 * __this, const MethodInfo* method)
{
	{
		VirtFuncInvoker1< bool, bool >::Invoke(10 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean) */, __this, (bool)0);
		Shader_t3191267369 * L_0 = __this->get_noiseShader_18();
		Material_t3870600107 * L_1 = __this->get_noiseMaterial_19();
		Material_t3870600107 * L_2 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_0, L_1);
		__this->set_noiseMaterial_19(L_2);
		bool L_3 = __this->get_dx11Grain_10();
		if (!L_3)
		{
			goto IL_004e;
		}
	}
	{
		bool L_4 = ((PostEffectsBase_t1820837395 *)__this)->get_supportDX11_3();
		if (!L_4)
		{
			goto IL_004e;
		}
	}
	{
		Shader_t3191267369 * L_5 = __this->get_dx11NoiseShader_20();
		Material_t3870600107 * L_6 = __this->get_dx11NoiseMaterial_21();
		Material_t3870600107 * L_7 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_5, L_6);
		__this->set_dx11NoiseMaterial_21(L_7);
	}

IL_004e:
	{
		bool L_8 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		if (L_8)
		{
			goto IL_005f;
		}
	}
	{
		VirtActionInvoker0::Invoke(13 /* System.Void PostEffectsBase::ReportAutoDisable() */, __this);
	}

IL_005f:
	{
		bool L_9 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		return L_9;
	}
}
// System.Void NoiseAndGrain::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppClass* NoiseAndGrain_t429516286_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3400769081;
extern Il2CppCodeGenString* _stringLiteral3336582068;
extern Il2CppCodeGenString* _stringLiteral1434789932;
extern Il2CppCodeGenString* _stringLiteral3009991041;
extern Il2CppCodeGenString* _stringLiteral782204904;
extern Il2CppCodeGenString* _stringLiteral4070583859;
extern Il2CppCodeGenString* _stringLiteral2262208396;
extern const uint32_t NoiseAndGrain_OnRenderImage_m4284655738_MetadataUsageId;
extern "C"  void NoiseAndGrain_OnRenderImage_m4284655738 (NoiseAndGrain_t429516286 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NoiseAndGrain_OnRenderImage_m4284655738_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RenderTexture_t1963041563 * V_0 = NULL;
	RenderTexture_t1963041563 * V_1 = NULL;
	String_t* G_B9_0 = NULL;
	Material_t3870600107 * G_B9_1 = NULL;
	String_t* G_B8_0 = NULL;
	Material_t3870600107 * G_B8_1 = NULL;
	Vector3_t4282066566  G_B10_0;
	memset(&G_B10_0, 0, sizeof(G_B10_0));
	String_t* G_B10_1 = NULL;
	Material_t3870600107 * G_B10_2 = NULL;
	Texture2D_t3884108195 * G_B13_0 = NULL;
	Material_t3870600107 * G_B13_1 = NULL;
	RenderTexture_t1963041563 * G_B13_2 = NULL;
	RenderTexture_t1963041563 * G_B13_3 = NULL;
	Texture2D_t3884108195 * G_B12_0 = NULL;
	Material_t3870600107 * G_B12_1 = NULL;
	RenderTexture_t1963041563 * G_B12_2 = NULL;
	RenderTexture_t1963041563 * G_B12_3 = NULL;
	int32_t G_B14_0 = 0;
	Texture2D_t3884108195 * G_B14_1 = NULL;
	Material_t3870600107 * G_B14_2 = NULL;
	RenderTexture_t1963041563 * G_B14_3 = NULL;
	RenderTexture_t1963041563 * G_B14_4 = NULL;
	Texture2D_t3884108195 * G_B17_0 = NULL;
	Material_t3870600107 * G_B17_1 = NULL;
	RenderTexture_t1963041563 * G_B17_2 = NULL;
	RenderTexture_t1963041563 * G_B17_3 = NULL;
	Texture2D_t3884108195 * G_B16_0 = NULL;
	Material_t3870600107 * G_B16_1 = NULL;
	RenderTexture_t1963041563 * G_B16_2 = NULL;
	RenderTexture_t1963041563 * G_B16_3 = NULL;
	int32_t G_B18_0 = 0;
	Texture2D_t3884108195 * G_B18_1 = NULL;
	Material_t3870600107 * G_B18_2 = NULL;
	RenderTexture_t1963041563 * G_B18_3 = NULL;
	RenderTexture_t1963041563 * G_B18_4 = NULL;
	String_t* G_B24_0 = NULL;
	Material_t3870600107 * G_B24_1 = NULL;
	String_t* G_B23_0 = NULL;
	Material_t3870600107 * G_B23_1 = NULL;
	Vector3_t4282066566  G_B25_0;
	memset(&G_B25_0, 0, sizeof(G_B25_0));
	String_t* G_B25_1 = NULL;
	Material_t3870600107 * G_B25_2 = NULL;
	String_t* G_B27_0 = NULL;
	Material_t3870600107 * G_B27_1 = NULL;
	String_t* G_B26_0 = NULL;
	Material_t3870600107 * G_B26_1 = NULL;
	Vector3_t4282066566  G_B28_0;
	memset(&G_B28_0, 0, sizeof(G_B28_0));
	String_t* G_B28_1 = NULL;
	Material_t3870600107 * G_B28_2 = NULL;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean NoiseAndGrain::CheckResources() */, __this);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Texture2D_t3884108195 * L_1 = __this->get_noiseTexture_17();
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, (Object_t3071478659 *)NULL, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0049;
		}
	}

IL_001c:
	{
		RenderTexture_t1963041563 * L_3 = ___source0;
		RenderTexture_t1963041563 * L_4 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_5 = __this->get_noiseTexture_17();
		bool L_6 = Object_op_Equality_m3964590952(NULL /*static, unused*/, (Object_t3071478659 *)NULL, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0044;
		}
	}
	{
		Transform_t1659122786 * L_7 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogWarning_m4097176146(NULL /*static, unused*/, _stringLiteral3400769081, L_7, /*hidden argument*/NULL);
	}

IL_0044:
	{
		goto IL_03b6;
	}

IL_0049:
	{
		float L_8 = __this->get_softness_11();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_9 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_8, (((float)((float)0))), (0.99f), /*hidden argument*/NULL);
		__this->set_softness_11(L_9);
		bool L_10 = __this->get_dx11Grain_10();
		if (!L_10)
		{
			goto IL_0201;
		}
	}
	{
		bool L_11 = ((PostEffectsBase_t1820837395 *)__this)->get_supportDX11_3();
		if (!L_11)
		{
			goto IL_0201;
		}
	}
	{
		Material_t3870600107 * L_12 = __this->get_dx11NoiseMaterial_21();
		int32_t L_13 = Time_get_frameCount_m3434184975(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		Material_SetFloat_m981710063(L_12, _stringLiteral3336582068, (((float)((float)L_13))), /*hidden argument*/NULL);
		Material_t3870600107 * L_14 = __this->get_dx11NoiseMaterial_21();
		Texture2D_t3884108195 * L_15 = __this->get_noiseTexture_17();
		NullCheck(L_14);
		Material_SetTexture_m1833724755(L_14, _stringLiteral1434789932, L_15, /*hidden argument*/NULL);
		Material_t3870600107 * L_16 = __this->get_dx11NoiseMaterial_21();
		bool L_17 = __this->get_monochrome_12();
		G_B8_0 = _stringLiteral3009991041;
		G_B8_1 = L_16;
		if (!L_17)
		{
			G_B9_0 = _stringLiteral3009991041;
			G_B9_1 = L_16;
			goto IL_00c3;
		}
	}
	{
		Vector3_t4282066566  L_18 = Vector3_get_one_m886467710(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B10_0 = L_18;
		G_B10_1 = G_B8_0;
		G_B10_2 = G_B8_1;
		goto IL_00c9;
	}

IL_00c3:
	{
		Vector3_t4282066566  L_19 = __this->get_intensities_13();
		G_B10_0 = L_19;
		G_B10_1 = G_B9_0;
		G_B10_2 = G_B9_1;
	}

IL_00c9:
	{
		Vector4_t4282066567  L_20 = Vector4_op_Implicit_m331673271(NULL /*static, unused*/, G_B10_0, /*hidden argument*/NULL);
		NullCheck(G_B10_2);
		Material_SetVector_m3505096203(G_B10_2, G_B10_1, L_20, /*hidden argument*/NULL);
		Material_t3870600107 * L_21 = __this->get_dx11NoiseMaterial_21();
		float L_22 = __this->get_midGrey_9();
		float L_23 = __this->get_midGrey_9();
		float L_24 = __this->get_midGrey_9();
		Vector3_t4282066566  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Vector3__ctor_m2926210380(&L_25, L_22, ((float)((float)(1.0f)/(float)((float)((float)(1.0f)-(float)L_23)))), ((float)((float)(-1.0f)/(float)L_24)), /*hidden argument*/NULL);
		Vector4_t4282066567  L_26 = Vector4_op_Implicit_m331673271(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		NullCheck(L_21);
		Material_SetVector_m3505096203(L_21, _stringLiteral782204904, L_26, /*hidden argument*/NULL);
		Material_t3870600107 * L_27 = __this->get_dx11NoiseMaterial_21();
		float L_28 = __this->get_generalIntensity_6();
		float L_29 = __this->get_blackIntensity_7();
		float L_30 = __this->get_whiteIntensity_8();
		Vector3_t4282066566  L_31;
		memset(&L_31, 0, sizeof(L_31));
		Vector3__ctor_m2926210380(&L_31, L_28, L_29, L_30, /*hidden argument*/NULL);
		float L_32 = __this->get_intensityMultiplier_5();
		Vector3_t4282066566  L_33 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		Vector4_t4282066567  L_34 = Vector4_op_Implicit_m331673271(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		NullCheck(L_27);
		Material_SetVector_m3505096203(L_27, _stringLiteral4070583859, L_34, /*hidden argument*/NULL);
		float L_35 = __this->get_softness_11();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_36 = ((Mathf_t4203372500_StaticFields*)Mathf_t4203372500_il2cpp_TypeInfo_var->static_fields)->get_Epsilon_0();
		if ((((float)L_35) <= ((float)L_36)))
		{
			goto IL_01d7;
		}
	}
	{
		RenderTexture_t1963041563 * L_37 = ___source0;
		NullCheck(L_37);
		int32_t L_38 = RenderTexture_get_width_m1498578543(L_37, /*hidden argument*/NULL);
		float L_39 = __this->get_softness_11();
		RenderTexture_t1963041563 * L_40 = ___source0;
		NullCheck(L_40);
		int32_t L_41 = RenderTexture_get_height_m4010076224(L_40, /*hidden argument*/NULL);
		float L_42 = __this->get_softness_11();
		RenderTexture_t1963041563 * L_43 = RenderTexture_GetTemporary_m469965696(NULL /*static, unused*/, (((int32_t)((int32_t)((float)((float)(((float)((float)L_38)))*(float)((float)((float)(1.0f)-(float)L_39))))))), (((int32_t)((int32_t)((float)((float)(((float)((float)L_41)))*(float)((float)((float)(1.0f)-(float)L_42))))))), /*hidden argument*/NULL);
		V_0 = L_43;
		RenderTexture_t1963041563 * L_44 = ___source0;
		RenderTexture_t1963041563 * L_45 = V_0;
		Material_t3870600107 * L_46 = __this->get_dx11NoiseMaterial_21();
		Texture2D_t3884108195 * L_47 = __this->get_noiseTexture_17();
		bool L_48 = __this->get_monochrome_12();
		G_B12_0 = L_47;
		G_B12_1 = L_46;
		G_B12_2 = L_45;
		G_B12_3 = L_44;
		if (!L_48)
		{
			G_B13_0 = L_47;
			G_B13_1 = L_46;
			G_B13_2 = L_45;
			G_B13_3 = L_44;
			goto IL_01a7;
		}
	}
	{
		G_B14_0 = 3;
		G_B14_1 = G_B12_0;
		G_B14_2 = G_B12_1;
		G_B14_3 = G_B12_2;
		G_B14_4 = G_B12_3;
		goto IL_01a8;
	}

IL_01a7:
	{
		G_B14_0 = 2;
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
		G_B14_3 = G_B13_2;
		G_B14_4 = G_B13_3;
	}

IL_01a8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NoiseAndGrain_t429516286_il2cpp_TypeInfo_var);
		NoiseAndGrain_DrawNoiseQuadGrid_m937261686(NULL /*static, unused*/, G_B14_4, G_B14_3, G_B14_2, G_B14_1, G_B14_0, /*hidden argument*/NULL);
		Material_t3870600107 * L_49 = __this->get_dx11NoiseMaterial_21();
		RenderTexture_t1963041563 * L_50 = V_0;
		NullCheck(L_49);
		Material_SetTexture_m1833724755(L_49, _stringLiteral1434789932, L_50, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_51 = ___source0;
		RenderTexture_t1963041563 * L_52 = ___destination1;
		Material_t3870600107 * L_53 = __this->get_dx11NoiseMaterial_21();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_51, L_52, L_53, 4, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_54 = V_0;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		goto IL_01fc;
	}

IL_01d7:
	{
		RenderTexture_t1963041563 * L_55 = ___source0;
		RenderTexture_t1963041563 * L_56 = ___destination1;
		Material_t3870600107 * L_57 = __this->get_dx11NoiseMaterial_21();
		Texture2D_t3884108195 * L_58 = __this->get_noiseTexture_17();
		bool L_59 = __this->get_monochrome_12();
		G_B16_0 = L_58;
		G_B16_1 = L_57;
		G_B16_2 = L_56;
		G_B16_3 = L_55;
		if (!L_59)
		{
			G_B17_0 = L_58;
			G_B17_1 = L_57;
			G_B17_2 = L_56;
			G_B17_3 = L_55;
			goto IL_01f6;
		}
	}
	{
		G_B18_0 = 1;
		G_B18_1 = G_B16_0;
		G_B18_2 = G_B16_1;
		G_B18_3 = G_B16_2;
		G_B18_4 = G_B16_3;
		goto IL_01f7;
	}

IL_01f6:
	{
		G_B18_0 = 0;
		G_B18_1 = G_B17_0;
		G_B18_2 = G_B17_1;
		G_B18_3 = G_B17_2;
		G_B18_4 = G_B17_3;
	}

IL_01f7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NoiseAndGrain_t429516286_il2cpp_TypeInfo_var);
		NoiseAndGrain_DrawNoiseQuadGrid_m937261686(NULL /*static, unused*/, G_B18_4, G_B18_3, G_B18_2, G_B18_1, G_B18_0, /*hidden argument*/NULL);
	}

IL_01fc:
	{
		goto IL_03b6;
	}

IL_0201:
	{
		Texture2D_t3884108195 * L_60 = __this->get_noiseTexture_17();
		bool L_61 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_60, /*hidden argument*/NULL);
		if (!L_61)
		{
			goto IL_022e;
		}
	}
	{
		Texture2D_t3884108195 * L_62 = __this->get_noiseTexture_17();
		NullCheck(L_62);
		Texture_set_wrapMode_m3720633937(L_62, 0, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_63 = __this->get_noiseTexture_17();
		int32_t L_64 = __this->get_filterMode_16();
		NullCheck(L_63);
		Texture_set_filterMode_m3842701708(L_63, L_64, /*hidden argument*/NULL);
	}

IL_022e:
	{
		Material_t3870600107 * L_65 = __this->get_noiseMaterial_19();
		Texture2D_t3884108195 * L_66 = __this->get_noiseTexture_17();
		NullCheck(L_65);
		Material_SetTexture_m1833724755(L_65, _stringLiteral1434789932, L_66, /*hidden argument*/NULL);
		Material_t3870600107 * L_67 = __this->get_noiseMaterial_19();
		bool L_68 = __this->get_monochrome_12();
		G_B23_0 = _stringLiteral3009991041;
		G_B23_1 = L_67;
		if (!L_68)
		{
			G_B24_0 = _stringLiteral3009991041;
			G_B24_1 = L_67;
			goto IL_0264;
		}
	}
	{
		Vector3_t4282066566  L_69 = Vector3_get_one_m886467710(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B25_0 = L_69;
		G_B25_1 = G_B23_0;
		G_B25_2 = G_B23_1;
		goto IL_026a;
	}

IL_0264:
	{
		Vector3_t4282066566  L_70 = __this->get_intensities_13();
		G_B25_0 = L_70;
		G_B25_1 = G_B24_0;
		G_B25_2 = G_B24_1;
	}

IL_026a:
	{
		Vector4_t4282066567  L_71 = Vector4_op_Implicit_m331673271(NULL /*static, unused*/, G_B25_0, /*hidden argument*/NULL);
		NullCheck(G_B25_2);
		Material_SetVector_m3505096203(G_B25_2, G_B25_1, L_71, /*hidden argument*/NULL);
		Material_t3870600107 * L_72 = __this->get_noiseMaterial_19();
		bool L_73 = __this->get_monochrome_12();
		G_B26_0 = _stringLiteral2262208396;
		G_B26_1 = L_72;
		if (!L_73)
		{
			G_B27_0 = _stringLiteral2262208396;
			G_B27_1 = L_72;
			goto IL_029f;
		}
	}
	{
		Vector3_t4282066566  L_74 = Vector3_get_one_m886467710(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_75 = __this->get_monochromeTiling_15();
		Vector3_t4282066566  L_76 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_74, L_75, /*hidden argument*/NULL);
		G_B28_0 = L_76;
		G_B28_1 = G_B26_0;
		G_B28_2 = G_B26_1;
		goto IL_02a5;
	}

IL_029f:
	{
		Vector3_t4282066566  L_77 = __this->get_tiling_14();
		G_B28_0 = L_77;
		G_B28_1 = G_B27_0;
		G_B28_2 = G_B27_1;
	}

IL_02a5:
	{
		Vector4_t4282066567  L_78 = Vector4_op_Implicit_m331673271(NULL /*static, unused*/, G_B28_0, /*hidden argument*/NULL);
		NullCheck(G_B28_2);
		Material_SetVector_m3505096203(G_B28_2, G_B28_1, L_78, /*hidden argument*/NULL);
		Material_t3870600107 * L_79 = __this->get_noiseMaterial_19();
		float L_80 = __this->get_midGrey_9();
		float L_81 = __this->get_midGrey_9();
		float L_82 = __this->get_midGrey_9();
		Vector3_t4282066566  L_83;
		memset(&L_83, 0, sizeof(L_83));
		Vector3__ctor_m2926210380(&L_83, L_80, ((float)((float)(1.0f)/(float)((float)((float)(1.0f)-(float)L_81)))), ((float)((float)(-1.0f)/(float)L_82)), /*hidden argument*/NULL);
		Vector4_t4282066567  L_84 = Vector4_op_Implicit_m331673271(NULL /*static, unused*/, L_83, /*hidden argument*/NULL);
		NullCheck(L_79);
		Material_SetVector_m3505096203(L_79, _stringLiteral782204904, L_84, /*hidden argument*/NULL);
		Material_t3870600107 * L_85 = __this->get_noiseMaterial_19();
		float L_86 = __this->get_generalIntensity_6();
		float L_87 = __this->get_blackIntensity_7();
		float L_88 = __this->get_whiteIntensity_8();
		Vector3_t4282066566  L_89;
		memset(&L_89, 0, sizeof(L_89));
		Vector3__ctor_m2926210380(&L_89, L_86, L_87, L_88, /*hidden argument*/NULL);
		float L_90 = __this->get_intensityMultiplier_5();
		Vector3_t4282066566  L_91 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_89, L_90, /*hidden argument*/NULL);
		Vector4_t4282066567  L_92 = Vector4_op_Implicit_m331673271(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
		NullCheck(L_85);
		Material_SetVector_m3505096203(L_85, _stringLiteral4070583859, L_92, /*hidden argument*/NULL);
		float L_93 = __this->get_softness_11();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_94 = ((Mathf_t4203372500_StaticFields*)Mathf_t4203372500_il2cpp_TypeInfo_var->static_fields)->get_Epsilon_0();
		if ((((float)L_93) <= ((float)L_94)))
		{
			goto IL_03a2;
		}
	}
	{
		RenderTexture_t1963041563 * L_95 = ___source0;
		NullCheck(L_95);
		int32_t L_96 = RenderTexture_get_width_m1498578543(L_95, /*hidden argument*/NULL);
		float L_97 = __this->get_softness_11();
		RenderTexture_t1963041563 * L_98 = ___source0;
		NullCheck(L_98);
		int32_t L_99 = RenderTexture_get_height_m4010076224(L_98, /*hidden argument*/NULL);
		float L_100 = __this->get_softness_11();
		RenderTexture_t1963041563 * L_101 = RenderTexture_GetTemporary_m469965696(NULL /*static, unused*/, (((int32_t)((int32_t)((float)((float)(((float)((float)L_96)))*(float)((float)((float)(1.0f)-(float)L_97))))))), (((int32_t)((int32_t)((float)((float)(((float)((float)L_99)))*(float)((float)((float)(1.0f)-(float)L_100))))))), /*hidden argument*/NULL);
		V_1 = L_101;
		RenderTexture_t1963041563 * L_102 = ___source0;
		RenderTexture_t1963041563 * L_103 = V_1;
		Material_t3870600107 * L_104 = __this->get_noiseMaterial_19();
		Texture2D_t3884108195 * L_105 = __this->get_noiseTexture_17();
		IL2CPP_RUNTIME_CLASS_INIT(NoiseAndGrain_t429516286_il2cpp_TypeInfo_var);
		NoiseAndGrain_DrawNoiseQuadGrid_m937261686(NULL /*static, unused*/, L_102, L_103, L_104, L_105, 2, /*hidden argument*/NULL);
		Material_t3870600107 * L_106 = __this->get_noiseMaterial_19();
		RenderTexture_t1963041563 * L_107 = V_1;
		NullCheck(L_106);
		Material_SetTexture_m1833724755(L_106, _stringLiteral1434789932, L_107, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_108 = ___source0;
		RenderTexture_t1963041563 * L_109 = ___destination1;
		Material_t3870600107 * L_110 = __this->get_noiseMaterial_19();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_108, L_109, L_110, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_111 = V_1;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_111, /*hidden argument*/NULL);
		goto IL_03b6;
	}

IL_03a2:
	{
		RenderTexture_t1963041563 * L_112 = ___source0;
		RenderTexture_t1963041563 * L_113 = ___destination1;
		Material_t3870600107 * L_114 = __this->get_noiseMaterial_19();
		Texture2D_t3884108195 * L_115 = __this->get_noiseTexture_17();
		IL2CPP_RUNTIME_CLASS_INIT(NoiseAndGrain_t429516286_il2cpp_TypeInfo_var);
		NoiseAndGrain_DrawNoiseQuadGrid_m937261686(NULL /*static, unused*/, L_112, L_113, L_114, L_115, 0, /*hidden argument*/NULL);
	}

IL_03b6:
	{
		return;
	}
}
// System.Void NoiseAndGrain::DrawNoiseQuadGrid(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material,UnityEngine.Texture2D,System.Int32)
extern Il2CppClass* NoiseAndGrain_t429516286_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral558922319;
extern const uint32_t NoiseAndGrain_DrawNoiseQuadGrid_m937261686_MetadataUsageId;
extern "C"  void NoiseAndGrain_DrawNoiseQuadGrid_m937261686 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___dest1, Material_t3870600107 * ___fxMaterial2, Texture2D_t3884108195 * ___noise3, int32_t ___passNr4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NoiseAndGrain_DrawNoiseQuadGrid_m937261686_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	{
		RenderTexture_t1963041563 * L_0 = ___dest1;
		RenderTexture_set_active_m1002947377(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_1 = ___noise3;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_1);
		V_0 = ((float)((float)(((float)((float)L_2)))*(float)(1.0f)));
		RenderTexture_t1963041563 * L_3 = ___source0;
		NullCheck(L_3);
		int32_t L_4 = RenderTexture_get_width_m1498578543(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NoiseAndGrain_t429516286_il2cpp_TypeInfo_var);
		float L_5 = ((NoiseAndGrain_t429516286_StaticFields*)NoiseAndGrain_t429516286_il2cpp_TypeInfo_var->static_fields)->get_TILE_AMOUNT_22();
		V_1 = ((float)((float)((float)((float)(1.0f)*(float)(((float)((float)L_4)))))/(float)L_5));
		Material_t3870600107 * L_6 = ___fxMaterial2;
		RenderTexture_t1963041563 * L_7 = ___source0;
		NullCheck(L_6);
		Material_SetTexture_m1833724755(L_6, _stringLiteral558922319, L_7, /*hidden argument*/NULL);
		GL_PushMatrix_m626765559(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_LoadOrtho_m1297524312(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_8 = ___source0;
		NullCheck(L_8);
		int32_t L_9 = RenderTexture_get_width_m1498578543(L_8, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_10 = ___source0;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_height_m4010076224(L_10, /*hidden argument*/NULL);
		V_2 = ((float)((float)((float)((float)(1.0f)*(float)(((float)((float)L_9)))))/(float)((float)((float)(1.0f)*(float)(((float)((float)L_11)))))));
		float L_12 = V_1;
		V_3 = ((float)((float)(1.0f)/(float)L_12));
		float L_13 = V_3;
		float L_14 = V_2;
		V_4 = ((float)((float)L_13*(float)L_14));
		float L_15 = V_0;
		Texture2D_t3884108195 * L_16 = ___noise3;
		NullCheck(L_16);
		int32_t L_17 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_16);
		V_5 = ((float)((float)L_15/(float)((float)((float)(((float)((float)L_17)))*(float)(1.0f)))));
		Material_t3870600107 * L_18 = ___fxMaterial2;
		int32_t L_19 = ___passNr4;
		NullCheck(L_18);
		Material_SetPass_m4241824642(L_18, L_19, /*hidden argument*/NULL);
		GL_Begin_m3089952800(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		V_6 = (((float)((float)0)));
		goto IL_01aa;
	}

IL_0093:
	{
		V_7 = (((float)((float)0)));
		goto IL_0198;
	}

IL_009c:
	{
		float L_20 = Random_Range_m3362417303(NULL /*static, unused*/, (((float)((float)0))), (1.0f), /*hidden argument*/NULL);
		V_8 = L_20;
		float L_21 = Random_Range_m3362417303(NULL /*static, unused*/, (((float)((float)0))), (1.0f), /*hidden argument*/NULL);
		V_9 = L_21;
		float L_22 = V_8;
		float L_23 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_24 = floorf(((float)((float)L_22*(float)L_23)));
		float L_25 = V_0;
		V_8 = ((float)((float)L_24/(float)L_25));
		float L_26 = V_9;
		float L_27 = V_0;
		float L_28 = floorf(((float)((float)L_26*(float)L_27)));
		float L_29 = V_0;
		V_9 = ((float)((float)L_28/(float)L_29));
		float L_30 = V_0;
		V_10 = ((float)((float)(1.0f)/(float)L_30));
		float L_31 = V_8;
		float L_32 = V_9;
		GL_MultiTexCoord2_m4253939586(NULL /*static, unused*/, 0, L_31, L_32, /*hidden argument*/NULL);
		GL_MultiTexCoord2_m4253939586(NULL /*static, unused*/, 1, (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		float L_33 = V_6;
		float L_34 = V_7;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_33, L_34, (0.1f), /*hidden argument*/NULL);
		float L_35 = V_8;
		float L_36 = V_5;
		float L_37 = V_10;
		float L_38 = V_9;
		GL_MultiTexCoord2_m4253939586(NULL /*static, unused*/, 0, ((float)((float)L_35+(float)((float)((float)L_36*(float)L_37)))), L_38, /*hidden argument*/NULL);
		GL_MultiTexCoord2_m4253939586(NULL /*static, unused*/, 1, (1.0f), (((float)((float)0))), /*hidden argument*/NULL);
		float L_39 = V_6;
		float L_40 = V_3;
		float L_41 = V_7;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, ((float)((float)L_39+(float)L_40)), L_41, (0.1f), /*hidden argument*/NULL);
		float L_42 = V_8;
		float L_43 = V_5;
		float L_44 = V_10;
		float L_45 = V_9;
		float L_46 = V_5;
		float L_47 = V_10;
		GL_MultiTexCoord2_m4253939586(NULL /*static, unused*/, 0, ((float)((float)L_42+(float)((float)((float)L_43*(float)L_44)))), ((float)((float)L_45+(float)((float)((float)L_46*(float)L_47)))), /*hidden argument*/NULL);
		GL_MultiTexCoord2_m4253939586(NULL /*static, unused*/, 1, (1.0f), (1.0f), /*hidden argument*/NULL);
		float L_48 = V_6;
		float L_49 = V_3;
		float L_50 = V_7;
		float L_51 = V_4;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, ((float)((float)L_48+(float)L_49)), ((float)((float)L_50+(float)L_51)), (0.1f), /*hidden argument*/NULL);
		float L_52 = V_8;
		float L_53 = V_9;
		float L_54 = V_5;
		float L_55 = V_10;
		GL_MultiTexCoord2_m4253939586(NULL /*static, unused*/, 0, L_52, ((float)((float)L_53+(float)((float)((float)L_54*(float)L_55)))), /*hidden argument*/NULL);
		GL_MultiTexCoord2_m4253939586(NULL /*static, unused*/, 1, (((float)((float)0))), (1.0f), /*hidden argument*/NULL);
		float L_56 = V_6;
		float L_57 = V_7;
		float L_58 = V_4;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_56, ((float)((float)L_57+(float)L_58)), (0.1f), /*hidden argument*/NULL);
		float L_59 = V_7;
		float L_60 = V_4;
		V_7 = ((float)((float)L_59+(float)L_60));
	}

IL_0198:
	{
		float L_61 = V_7;
		if ((((float)L_61) < ((float)(1.0f))))
		{
			goto IL_009c;
		}
	}
	{
		float L_62 = V_6;
		float L_63 = V_3;
		V_6 = ((float)((float)L_62+(float)L_63));
	}

IL_01aa:
	{
		float L_64 = V_6;
		if ((((float)L_64) < ((float)(1.0f))))
		{
			goto IL_0093;
		}
	}
	{
		GL_End_m2013837889(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m3073322328(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NoiseAndGrain::Main()
extern "C"  void NoiseAndGrain_Main_m1873495769 (NoiseAndGrain_t429516286 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PostEffectsBase::.ctor()
extern "C"  void PostEffectsBase__ctor_m3869393199 (PostEffectsBase_t1820837395 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		__this->set_supportHDRTextures_2((bool)1);
		__this->set_isSupported_4((bool)1);
		return;
	}
}
// UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material)
extern Il2CppClass* RuntimeServices_t3947355960_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* Material_t3870600107_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1857353722;
extern Il2CppCodeGenString* _stringLiteral242255884;
extern Il2CppCodeGenString* _stringLiteral3433835406;
extern Il2CppCodeGenString* _stringLiteral3507231192;
extern const uint32_t PostEffectsBase_CheckShaderAndCreateMaterial_m4154378035_MetadataUsageId;
extern "C"  Material_t3870600107 * PostEffectsBase_CheckShaderAndCreateMaterial_m4154378035 (PostEffectsBase_t1820837395 * __this, Shader_t3191267369 * ___s0, Material_t3870600107 * ___m2Create1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_CheckShaderAndCreateMaterial_m4154378035_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Material_t3870600107 * G_B11_0 = NULL;
	{
		Shader_t3191267369 * L_0 = ___s0;
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t3947355960_il2cpp_TypeInfo_var);
		String_t* L_3 = RuntimeServices_op_Addition_m3391097550(NULL /*static, unused*/, _stringLiteral1857353722, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
		G_B11_0 = ((Material_t3870600107 *)(NULL));
		goto IL_00d0;
	}

IL_002d:
	{
		Shader_t3191267369 * L_4 = ___s0;
		NullCheck(L_4);
		bool L_5 = Shader_get_isSupported_m1422621179(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_005a;
		}
	}
	{
		Material_t3870600107 * L_6 = ___m2Create1;
		bool L_7 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005a;
		}
	}
	{
		Material_t3870600107 * L_8 = ___m2Create1;
		NullCheck(L_8);
		Shader_t3191267369 * L_9 = Material_get_shader_m2881845503(L_8, /*hidden argument*/NULL);
		Shader_t3191267369 * L_10 = ___s0;
		bool L_11 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_005a;
		}
	}
	{
		Material_t3870600107 * L_12 = ___m2Create1;
		G_B11_0 = L_12;
		goto IL_00d0;
	}

IL_005a:
	{
		Shader_t3191267369 * L_13 = ___s0;
		NullCheck(L_13);
		bool L_14 = Shader_get_isSupported_m1422621179(L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_00a5;
		}
	}
	{
		VirtActionInvoker0::Invoke(15 /* System.Void PostEffectsBase::NotSupported() */, __this);
		Shader_t3191267369 * L_15 = ___s0;
		NullCheck(L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, L_15);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t3947355960_il2cpp_TypeInfo_var);
		String_t* L_17 = RuntimeServices_op_Addition_m3391097550(NULL /*static, unused*/, _stringLiteral242255884, L_16, /*hidden argument*/NULL);
		String_t* L_18 = RuntimeServices_op_Addition_m3391097550(NULL /*static, unused*/, L_17, _stringLiteral3433835406, /*hidden argument*/NULL);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, __this);
		String_t* L_20 = RuntimeServices_op_Addition_m3391097550(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		String_t* L_21 = RuntimeServices_op_Addition_m3391097550(NULL /*static, unused*/, L_20, _stringLiteral3507231192, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		G_B11_0 = ((Material_t3870600107 *)(NULL));
		goto IL_00d0;
	}

IL_00a5:
	{
		Shader_t3191267369 * L_22 = ___s0;
		Material_t3870600107 * L_23 = (Material_t3870600107 *)il2cpp_codegen_object_new(Material_t3870600107_il2cpp_TypeInfo_var);
		Material__ctor_m2685909642(L_23, L_22, /*hidden argument*/NULL);
		___m2Create1 = L_23;
		Material_t3870600107 * L_24 = ___m2Create1;
		NullCheck(L_24);
		Object_set_hideFlags_m41317712(L_24, ((int32_t)52), /*hidden argument*/NULL);
		Material_t3870600107 * L_25 = ___m2Create1;
		bool L_26 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00ca;
		}
	}
	{
		Material_t3870600107 * L_27 = ___m2Create1;
		G_B11_0 = L_27;
		goto IL_00d0;
	}

IL_00ca:
	{
		G_B11_0 = ((Material_t3870600107 *)(NULL));
		goto IL_00d0;
	}

IL_00d0:
	{
		return G_B11_0;
	}
}
// UnityEngine.Material PostEffectsBase::CreateMaterial(UnityEngine.Shader,UnityEngine.Material)
extern Il2CppClass* RuntimeServices_t3947355960_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* Material_t3870600107_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1857353722;
extern const uint32_t PostEffectsBase_CreateMaterial_m2171637981_MetadataUsageId;
extern "C"  Material_t3870600107 * PostEffectsBase_CreateMaterial_m2171637981 (PostEffectsBase_t1820837395 * __this, Shader_t3191267369 * ___s0, Material_t3870600107 * ___m2Create1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_CreateMaterial_m2171637981_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Material_t3870600107 * G_B11_0 = NULL;
	{
		Shader_t3191267369 * L_0 = ___s0;
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t3947355960_il2cpp_TypeInfo_var);
		String_t* L_3 = RuntimeServices_op_Addition_m3391097550(NULL /*static, unused*/, _stringLiteral1857353722, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		G_B11_0 = ((Material_t3870600107 *)(NULL));
		goto IL_008f;
	}

IL_0026:
	{
		Material_t3870600107 * L_4 = ___m2Create1;
		bool L_5 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0053;
		}
	}
	{
		Material_t3870600107 * L_6 = ___m2Create1;
		NullCheck(L_6);
		Shader_t3191267369 * L_7 = Material_get_shader_m2881845503(L_6, /*hidden argument*/NULL);
		Shader_t3191267369 * L_8 = ___s0;
		bool L_9 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0053;
		}
	}
	{
		Shader_t3191267369 * L_10 = ___s0;
		NullCheck(L_10);
		bool L_11 = Shader_get_isSupported_m1422621179(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0053;
		}
	}
	{
		Material_t3870600107 * L_12 = ___m2Create1;
		G_B11_0 = L_12;
		goto IL_008f;
	}

IL_0053:
	{
		Shader_t3191267369 * L_13 = ___s0;
		NullCheck(L_13);
		bool L_14 = Shader_get_isSupported_m1422621179(L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0064;
		}
	}
	{
		G_B11_0 = ((Material_t3870600107 *)(NULL));
		goto IL_008f;
	}

IL_0064:
	{
		Shader_t3191267369 * L_15 = ___s0;
		Material_t3870600107 * L_16 = (Material_t3870600107 *)il2cpp_codegen_object_new(Material_t3870600107_il2cpp_TypeInfo_var);
		Material__ctor_m2685909642(L_16, L_15, /*hidden argument*/NULL);
		___m2Create1 = L_16;
		Material_t3870600107 * L_17 = ___m2Create1;
		NullCheck(L_17);
		Object_set_hideFlags_m41317712(L_17, ((int32_t)52), /*hidden argument*/NULL);
		Material_t3870600107 * L_18 = ___m2Create1;
		bool L_19 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0089;
		}
	}
	{
		Material_t3870600107 * L_20 = ___m2Create1;
		G_B11_0 = L_20;
		goto IL_008f;
	}

IL_0089:
	{
		G_B11_0 = ((Material_t3870600107 *)(NULL));
		goto IL_008f;
	}

IL_008f:
	{
		return G_B11_0;
	}
}
// System.Void PostEffectsBase::OnEnable()
extern "C"  void PostEffectsBase_OnEnable_m2537112567 (PostEffectsBase_t1820837395 * __this, const MethodInfo* method)
{
	{
		__this->set_isSupported_4((bool)1);
		return;
	}
}
// System.Boolean PostEffectsBase::CheckSupport()
extern "C"  bool PostEffectsBase_CheckSupport_m2350066934 (PostEffectsBase_t1820837395 * __this, const MethodInfo* method)
{
	{
		bool L_0 = VirtFuncInvoker1< bool, bool >::Invoke(10 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean) */, __this, (bool)0);
		return L_0;
	}
}
// System.Boolean PostEffectsBase::CheckResources()
extern Il2CppClass* RuntimeServices_t3947355960_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral280628531;
extern Il2CppCodeGenString* _stringLiteral642934587;
extern const uint32_t PostEffectsBase_CheckResources_m2348731468_MetadataUsageId;
extern "C"  bool PostEffectsBase_CheckResources_m2348731468 (PostEffectsBase_t1820837395 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_CheckResources_m2348731468_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t3947355960_il2cpp_TypeInfo_var);
		String_t* L_1 = RuntimeServices_op_Addition_m3391097550(NULL /*static, unused*/, _stringLiteral280628531, L_0, /*hidden argument*/NULL);
		String_t* L_2 = RuntimeServices_op_Addition_m3391097550(NULL /*static, unused*/, L_1, _stringLiteral642934587, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		bool L_3 = __this->get_isSupported_4();
		return L_3;
	}
}
// System.Void PostEffectsBase::Start()
extern "C"  void PostEffectsBase_Start_m2816530991 (PostEffectsBase_t1820837395 * __this, const MethodInfo* method)
{
	{
		VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean PostEffectsBase::CheckResources() */, __this);
		return;
	}
}
// System.Boolean PostEffectsBase::CheckSupport(System.Boolean)
extern const MethodInfo* Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var;
extern const uint32_t PostEffectsBase_CheckSupport_m2153053165_MetadataUsageId;
extern "C"  bool PostEffectsBase_CheckSupport_m2153053165 (PostEffectsBase_t1820837395 * __this, bool ___needDepth0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_CheckSupport_m2153053165_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	PostEffectsBase_t1820837395 * G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	PostEffectsBase_t1820837395 * G_B1_1 = NULL;
	int32_t G_B11_0 = 0;
	{
		__this->set_isSupported_4((bool)1);
		bool L_0 = SystemInfo_SupportsRenderTextureFormat_m1773213581(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		__this->set_supportHDRTextures_2(L_0);
		int32_t L_1 = SystemInfo_get_graphicsShaderLevel_m1169417593(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = ((((int32_t)((((int32_t)L_1) < ((int32_t)((int32_t)50)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		G_B1_0 = L_2;
		G_B1_1 = __this;
		if (!L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = __this;
			goto IL_002c;
		}
	}
	{
		bool L_3 = SystemInfo_get_supportsComputeShaders_m940660062(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B2_0 = ((int32_t)(L_3));
		G_B2_1 = G_B1_1;
	}

IL_002c:
	{
		NullCheck(G_B2_1);
		G_B2_1->set_supportDX11_3((bool)G_B2_0);
		bool L_4 = SystemInfo_get_supportsImageEffects_m2392300814(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0045;
		}
	}
	{
		bool L_5 = SystemInfo_get_supportsRenderTextures_m3098351893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0051;
		}
	}

IL_0045:
	{
		VirtActionInvoker0::Invoke(15 /* System.Void PostEffectsBase::NotSupported() */, __this);
		G_B11_0 = 0;
		goto IL_008d;
	}

IL_0051:
	{
		bool L_6 = ___needDepth0;
		if (!L_6)
		{
			goto IL_006e;
		}
	}
	{
		bool L_7 = SystemInfo_SupportsRenderTextureFormat_m1773213581(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_006e;
		}
	}
	{
		VirtActionInvoker0::Invoke(15 /* System.Void PostEffectsBase::NotSupported() */, __this);
		G_B11_0 = 0;
		goto IL_008d;
	}

IL_006e:
	{
		bool L_8 = ___needDepth0;
		if (!L_8)
		{
			goto IL_008c;
		}
	}
	{
		Camera_t2727095145 * L_9 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		Camera_t2727095145 * L_10 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_10);
		int32_t L_11 = Camera_get_depthTextureMode_m2117446653(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		Camera_set_depthTextureMode_m2368326786(L_9, ((int32_t)((int32_t)L_11|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_008c:
	{
		G_B11_0 = 1;
	}

IL_008d:
	{
		return (bool)G_B11_0;
	}
}
// System.Boolean PostEffectsBase::CheckSupport(System.Boolean,System.Boolean)
extern "C"  bool PostEffectsBase_CheckSupport_m822108144 (PostEffectsBase_t1820837395 * __this, bool ___needDepth0, bool ___needHdr1, const MethodInfo* method)
{
	int32_t G_B6_0 = 0;
	{
		bool L_0 = ___needDepth0;
		bool L_1 = VirtFuncInvoker1< bool, bool >::Invoke(10 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean) */, __this, L_0);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B6_0 = 0;
		goto IL_0030;
	}

IL_0012:
	{
		bool L_2 = ___needHdr1;
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		bool L_3 = __this->get_supportHDRTextures_2();
		if (L_3)
		{
			goto IL_002f;
		}
	}
	{
		VirtActionInvoker0::Invoke(15 /* System.Void PostEffectsBase::NotSupported() */, __this);
		G_B6_0 = 0;
		goto IL_0030;
	}

IL_002f:
	{
		G_B6_0 = 1;
	}

IL_0030:
	{
		return (bool)G_B6_0;
	}
}
// System.Boolean PostEffectsBase::Dx11Support()
extern "C"  bool PostEffectsBase_Dx11Support_m3110085614 (PostEffectsBase_t1820837395 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_supportDX11_3();
		return L_0;
	}
}
// System.Void PostEffectsBase::ReportAutoDisable()
extern Il2CppClass* RuntimeServices_t3947355960_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2796834139;
extern Il2CppCodeGenString* _stringLiteral1329418804;
extern const uint32_t PostEffectsBase_ReportAutoDisable_m44450674_MetadataUsageId;
extern "C"  void PostEffectsBase_ReportAutoDisable_m44450674 (PostEffectsBase_t1820837395 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_ReportAutoDisable_m44450674_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t3947355960_il2cpp_TypeInfo_var);
		String_t* L_1 = RuntimeServices_op_Addition_m3391097550(NULL /*static, unused*/, _stringLiteral2796834139, L_0, /*hidden argument*/NULL);
		String_t* L_2 = RuntimeServices_op_Addition_m3391097550(NULL /*static, unused*/, L_1, _stringLiteral1329418804, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PostEffectsBase::CheckShader(UnityEngine.Shader)
extern Il2CppClass* RuntimeServices_t3947355960_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral242255884;
extern Il2CppCodeGenString* _stringLiteral3433835406;
extern Il2CppCodeGenString* _stringLiteral1291402339;
extern const uint32_t PostEffectsBase_CheckShader_m636992228_MetadataUsageId;
extern "C"  bool PostEffectsBase_CheckShader_m636992228 (PostEffectsBase_t1820837395 * __this, Shader_t3191267369 * ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_CheckShader_m636992228_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		Shader_t3191267369 * L_0 = ___s0;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t3947355960_il2cpp_TypeInfo_var);
		String_t* L_2 = RuntimeServices_op_Addition_m3391097550(NULL /*static, unused*/, _stringLiteral242255884, L_1, /*hidden argument*/NULL);
		String_t* L_3 = RuntimeServices_op_Addition_m3391097550(NULL /*static, unused*/, L_2, _stringLiteral3433835406, /*hidden argument*/NULL);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, __this);
		String_t* L_5 = RuntimeServices_op_Addition_m3391097550(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		String_t* L_6 = RuntimeServices_op_Addition_m3391097550(NULL /*static, unused*/, L_5, _stringLiteral1291402339, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Shader_t3191267369 * L_7 = ___s0;
		NullCheck(L_7);
		bool L_8 = Shader_get_isSupported_m1422621179(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_004b;
		}
	}
	{
		VirtActionInvoker0::Invoke(15 /* System.Void PostEffectsBase::NotSupported() */, __this);
		G_B3_0 = 0;
		goto IL_0051;
	}

IL_004b:
	{
		G_B3_0 = 0;
		goto IL_0051;
	}

IL_0051:
	{
		return (bool)G_B3_0;
	}
}
// System.Void PostEffectsBase::NotSupported()
extern "C"  void PostEffectsBase_NotSupported_m925958800 (PostEffectsBase_t1820837395 * __this, const MethodInfo* method)
{
	{
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
		__this->set_isSupported_4((bool)0);
		return;
	}
}
// System.Void PostEffectsBase::DrawBorder(UnityEngine.RenderTexture,UnityEngine.Material)
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t PostEffectsBase_DrawBorder_m722325705_MetadataUsageId;
extern "C"  void PostEffectsBase_DrawBorder_m722325705 (PostEffectsBase_t1820837395 * __this, RenderTexture_t1963041563 * ___dest0, Material_t3870600107 * ___material1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_DrawBorder_m722325705_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	bool V_4 = false;
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	{
		Initobj (Single_t4291918972_il2cpp_TypeInfo_var, (&V_0));
		Initobj (Single_t4291918972_il2cpp_TypeInfo_var, (&V_1));
		Initobj (Single_t4291918972_il2cpp_TypeInfo_var, (&V_2));
		Initobj (Single_t4291918972_il2cpp_TypeInfo_var, (&V_3));
		RenderTexture_t1963041563 * L_0 = ___dest0;
		RenderTexture_set_active_m1002947377(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_4 = (bool)1;
		GL_PushMatrix_m626765559(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_LoadOrtho_m1297524312(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = 0;
		goto IL_0287;
	}

IL_003b:
	{
		Material_t3870600107 * L_1 = ___material1;
		int32_t L_2 = V_5;
		NullCheck(L_1);
		Material_SetPass_m4241824642(L_1, L_2, /*hidden argument*/NULL);
		Initobj (Single_t4291918972_il2cpp_TypeInfo_var, (&V_6));
		Initobj (Single_t4291918972_il2cpp_TypeInfo_var, (&V_7));
		bool L_3 = V_4;
		if (!L_3)
		{
			goto IL_006b;
		}
	}
	{
		V_6 = (1.0f);
		V_7 = (((float)((float)0)));
		goto IL_0076;
	}

IL_006b:
	{
		V_6 = (((float)((float)0)));
		V_7 = (1.0f);
	}

IL_0076:
	{
		V_0 = (((float)((float)0)));
		RenderTexture_t1963041563 * L_4 = ___dest0;
		NullCheck(L_4);
		int32_t L_5 = RenderTexture_get_width_m1498578543(L_4, /*hidden argument*/NULL);
		V_1 = ((float)((float)(((float)((float)0)))+(float)((float)((float)(1.0f)/(float)((float)((float)(((float)((float)L_5)))*(float)(1.0f)))))));
		V_2 = (((float)((float)0)));
		V_3 = (1.0f);
		GL_Begin_m3089952800(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		float L_6 = V_6;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (((float)((float)0))), L_6, /*hidden argument*/NULL);
		float L_7 = V_0;
		float L_8 = V_2;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_7, L_8, (0.1f), /*hidden argument*/NULL);
		float L_9 = V_6;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (1.0f), L_9, /*hidden argument*/NULL);
		float L_10 = V_1;
		float L_11 = V_2;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_10, L_11, (0.1f), /*hidden argument*/NULL);
		float L_12 = V_7;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (1.0f), L_12, /*hidden argument*/NULL);
		float L_13 = V_1;
		float L_14 = V_3;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_13, L_14, (0.1f), /*hidden argument*/NULL);
		float L_15 = V_7;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (((float)((float)0))), L_15, /*hidden argument*/NULL);
		float L_16 = V_0;
		float L_17 = V_3;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_16, L_17, (0.1f), /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_18 = ___dest0;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_width_m1498578543(L_18, /*hidden argument*/NULL);
		V_0 = ((float)((float)(1.0f)-(float)((float)((float)(1.0f)/(float)((float)((float)(((float)((float)L_19)))*(float)(1.0f)))))));
		V_1 = (1.0f);
		V_2 = (((float)((float)0)));
		V_3 = (1.0f);
		float L_20 = V_6;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (((float)((float)0))), L_20, /*hidden argument*/NULL);
		float L_21 = V_0;
		float L_22 = V_2;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_21, L_22, (0.1f), /*hidden argument*/NULL);
		float L_23 = V_6;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (1.0f), L_23, /*hidden argument*/NULL);
		float L_24 = V_1;
		float L_25 = V_2;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_24, L_25, (0.1f), /*hidden argument*/NULL);
		float L_26 = V_7;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (1.0f), L_26, /*hidden argument*/NULL);
		float L_27 = V_1;
		float L_28 = V_3;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_27, L_28, (0.1f), /*hidden argument*/NULL);
		float L_29 = V_7;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (((float)((float)0))), L_29, /*hidden argument*/NULL);
		float L_30 = V_0;
		float L_31 = V_3;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_30, L_31, (0.1f), /*hidden argument*/NULL);
		V_0 = (((float)((float)0)));
		V_1 = (1.0f);
		V_2 = (((float)((float)0)));
		RenderTexture_t1963041563 * L_32 = ___dest0;
		NullCheck(L_32);
		int32_t L_33 = RenderTexture_get_height_m4010076224(L_32, /*hidden argument*/NULL);
		V_3 = ((float)((float)(((float)((float)0)))+(float)((float)((float)(1.0f)/(float)((float)((float)(((float)((float)L_33)))*(float)(1.0f)))))));
		float L_34 = V_6;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (((float)((float)0))), L_34, /*hidden argument*/NULL);
		float L_35 = V_0;
		float L_36 = V_2;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_35, L_36, (0.1f), /*hidden argument*/NULL);
		float L_37 = V_6;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (1.0f), L_37, /*hidden argument*/NULL);
		float L_38 = V_1;
		float L_39 = V_2;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_38, L_39, (0.1f), /*hidden argument*/NULL);
		float L_40 = V_7;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (1.0f), L_40, /*hidden argument*/NULL);
		float L_41 = V_1;
		float L_42 = V_3;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_41, L_42, (0.1f), /*hidden argument*/NULL);
		float L_43 = V_7;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (((float)((float)0))), L_43, /*hidden argument*/NULL);
		float L_44 = V_0;
		float L_45 = V_3;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_44, L_45, (0.1f), /*hidden argument*/NULL);
		V_0 = (((float)((float)0)));
		V_1 = (1.0f);
		RenderTexture_t1963041563 * L_46 = ___dest0;
		NullCheck(L_46);
		int32_t L_47 = RenderTexture_get_height_m4010076224(L_46, /*hidden argument*/NULL);
		V_2 = ((float)((float)(1.0f)-(float)((float)((float)(1.0f)/(float)((float)((float)(((float)((float)L_47)))*(float)(1.0f)))))));
		V_3 = (1.0f);
		float L_48 = V_6;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (((float)((float)0))), L_48, /*hidden argument*/NULL);
		float L_49 = V_0;
		float L_50 = V_2;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_49, L_50, (0.1f), /*hidden argument*/NULL);
		float L_51 = V_6;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (1.0f), L_51, /*hidden argument*/NULL);
		float L_52 = V_1;
		float L_53 = V_2;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_52, L_53, (0.1f), /*hidden argument*/NULL);
		float L_54 = V_7;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (1.0f), L_54, /*hidden argument*/NULL);
		float L_55 = V_1;
		float L_56 = V_3;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_55, L_56, (0.1f), /*hidden argument*/NULL);
		float L_57 = V_7;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (((float)((float)0))), L_57, /*hidden argument*/NULL);
		float L_58 = V_0;
		float L_59 = V_3;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_58, L_59, (0.1f), /*hidden argument*/NULL);
		GL_End_m2013837889(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_60 = V_5;
		V_5 = ((int32_t)((int32_t)L_60+(int32_t)1));
	}

IL_0287:
	{
		int32_t L_61 = V_5;
		Material_t3870600107 * L_62 = ___material1;
		NullCheck(L_62);
		int32_t L_63 = Material_get_passCount_m2926759545(L_62, /*hidden argument*/NULL);
		if ((((int32_t)L_61) < ((int32_t)L_63)))
		{
			goto IL_003b;
		}
	}
	{
		GL_PopMatrix_m3073322328(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PostEffectsBase::Main()
extern "C"  void PostEffectsBase_Main_m871597486 (PostEffectsBase_t1820837395 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PostEffectsHelper::.ctor()
extern "C"  void PostEffectsHelper__ctor_m1882078642 (PostEffectsHelper_t1948321392 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PostEffectsHelper::Start()
extern "C"  void PostEffectsHelper_Start_m829216434 (PostEffectsHelper_t1948321392 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PostEffectsHelper::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2335973020;
extern const uint32_t PostEffectsHelper_OnRenderImage_m2619344748_MetadataUsageId;
extern "C"  void PostEffectsHelper_OnRenderImage_m2619344748 (PostEffectsHelper_t1948321392 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsHelper_OnRenderImage_m2619344748_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2335973020, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PostEffectsHelper::DrawLowLevelPlaneAlignedWithCamera(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material,UnityEngine.Camera)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral558922319;
extern const uint32_t PostEffectsHelper_DrawLowLevelPlaneAlignedWithCamera_m4064300496_MetadataUsageId;
extern "C"  void PostEffectsHelper_DrawLowLevelPlaneAlignedWithCamera_m4064300496 (Il2CppObject * __this /* static, unused */, float ___dist0, RenderTexture_t1963041563 * ___source1, RenderTexture_t1963041563 * ___dest2, Material_t3870600107 * ___material3, Camera_t2727095145 * ___cameraForProjectionMatrix4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsHelper_DrawLowLevelPlaneAlignedWithCamera_m4064300496_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	int32_t V_10 = 0;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	{
		RenderTexture_t1963041563 * L_0 = ___dest2;
		RenderTexture_set_active_m1002947377(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Material_t3870600107 * L_1 = ___material3;
		RenderTexture_t1963041563 * L_2 = ___source1;
		NullCheck(L_1);
		Material_SetTexture_m1833724755(L_1, _stringLiteral558922319, L_2, /*hidden argument*/NULL);
		V_0 = (bool)1;
		GL_PushMatrix_m626765559(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_LoadIdentity_m1417984576(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t2727095145 * L_3 = ___cameraForProjectionMatrix4;
		NullCheck(L_3);
		Matrix4x4_t1651859333  L_4 = Camera_get_projectionMatrix_m3070982480(L_3, /*hidden argument*/NULL);
		GL_LoadProjectionMatrix_m1743857864(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Camera_t2727095145 * L_5 = ___cameraForProjectionMatrix4;
		NullCheck(L_5);
		float L_6 = Camera_get_fieldOfView_m65126887(L_5, /*hidden argument*/NULL);
		V_1 = ((float)((float)((float)((float)L_6*(float)(0.5f)))*(float)(0.0174532924f)));
		float L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_8 = cosf(L_7);
		float L_9 = V_1;
		float L_10 = sinf(L_9);
		V_2 = ((float)((float)L_8/(float)L_10));
		Camera_t2727095145 * L_11 = ___cameraForProjectionMatrix4;
		NullCheck(L_11);
		float L_12 = Camera_get_aspect_m4145685929(L_11, /*hidden argument*/NULL);
		V_3 = L_12;
		float L_13 = V_3;
		float L_14 = V_2;
		V_4 = ((float)((float)L_13/(float)((-L_14))));
		float L_15 = V_3;
		float L_16 = V_2;
		V_5 = ((float)((float)L_15/(float)L_16));
		float L_17 = V_2;
		V_6 = ((float)((float)(1.0f)/(float)((-L_17))));
		float L_18 = V_2;
		V_7 = ((float)((float)(1.0f)/(float)L_18));
		V_8 = (1.0f);
		float L_19 = V_4;
		float L_20 = ___dist0;
		float L_21 = V_8;
		V_4 = ((float)((float)L_19*(float)((float)((float)L_20*(float)L_21))));
		float L_22 = V_5;
		float L_23 = ___dist0;
		float L_24 = V_8;
		V_5 = ((float)((float)L_22*(float)((float)((float)L_23*(float)L_24))));
		float L_25 = V_6;
		float L_26 = ___dist0;
		float L_27 = V_8;
		V_6 = ((float)((float)L_25*(float)((float)((float)L_26*(float)L_27))));
		float L_28 = V_7;
		float L_29 = ___dist0;
		float L_30 = V_8;
		V_7 = ((float)((float)L_28*(float)((float)((float)L_29*(float)L_30))));
		float L_31 = ___dist0;
		V_9 = ((-L_31));
		V_10 = 0;
		goto IL_0153;
	}

IL_00b2:
	{
		Material_t3870600107 * L_32 = ___material3;
		int32_t L_33 = V_10;
		NullCheck(L_32);
		Material_SetPass_m4241824642(L_32, L_33, /*hidden argument*/NULL);
		GL_Begin_m3089952800(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		Initobj (Single_t4291918972_il2cpp_TypeInfo_var, (&V_11));
		Initobj (Single_t4291918972_il2cpp_TypeInfo_var, (&V_12));
		bool L_34 = V_0;
		if (!L_34)
		{
			goto IL_00e7;
		}
	}
	{
		V_11 = (1.0f);
		V_12 = (((float)((float)0)));
		goto IL_00f2;
	}

IL_00e7:
	{
		V_11 = (((float)((float)0)));
		V_12 = (1.0f);
	}

IL_00f2:
	{
		float L_35 = V_11;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (((float)((float)0))), L_35, /*hidden argument*/NULL);
		float L_36 = V_4;
		float L_37 = V_6;
		float L_38 = V_9;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_36, L_37, L_38, /*hidden argument*/NULL);
		float L_39 = V_11;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (1.0f), L_39, /*hidden argument*/NULL);
		float L_40 = V_5;
		float L_41 = V_6;
		float L_42 = V_9;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_40, L_41, L_42, /*hidden argument*/NULL);
		float L_43 = V_12;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (1.0f), L_43, /*hidden argument*/NULL);
		float L_44 = V_5;
		float L_45 = V_7;
		float L_46 = V_9;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_44, L_45, L_46, /*hidden argument*/NULL);
		float L_47 = V_12;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (((float)((float)0))), L_47, /*hidden argument*/NULL);
		float L_48 = V_4;
		float L_49 = V_7;
		float L_50 = V_9;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_48, L_49, L_50, /*hidden argument*/NULL);
		GL_End_m2013837889(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_51 = V_10;
		V_10 = ((int32_t)((int32_t)L_51+(int32_t)1));
	}

IL_0153:
	{
		int32_t L_52 = V_10;
		Material_t3870600107 * L_53 = ___material3;
		NullCheck(L_53);
		int32_t L_54 = Material_get_passCount_m2926759545(L_53, /*hidden argument*/NULL);
		if ((((int32_t)L_52) < ((int32_t)L_54)))
		{
			goto IL_00b2;
		}
	}
	{
		GL_PopMatrix_m3073322328(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PostEffectsHelper::DrawBorder(UnityEngine.RenderTexture,UnityEngine.Material)
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t PostEffectsHelper_DrawBorder_m2171556710_MetadataUsageId;
extern "C"  void PostEffectsHelper_DrawBorder_m2171556710 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___dest0, Material_t3870600107 * ___material1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsHelper_DrawBorder_m2171556710_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	bool V_4 = false;
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	{
		Initobj (Single_t4291918972_il2cpp_TypeInfo_var, (&V_0));
		Initobj (Single_t4291918972_il2cpp_TypeInfo_var, (&V_1));
		Initobj (Single_t4291918972_il2cpp_TypeInfo_var, (&V_2));
		Initobj (Single_t4291918972_il2cpp_TypeInfo_var, (&V_3));
		RenderTexture_t1963041563 * L_0 = ___dest0;
		RenderTexture_set_active_m1002947377(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_4 = (bool)1;
		GL_PushMatrix_m626765559(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_LoadOrtho_m1297524312(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = 0;
		goto IL_0287;
	}

IL_003b:
	{
		Material_t3870600107 * L_1 = ___material1;
		int32_t L_2 = V_5;
		NullCheck(L_1);
		Material_SetPass_m4241824642(L_1, L_2, /*hidden argument*/NULL);
		Initobj (Single_t4291918972_il2cpp_TypeInfo_var, (&V_6));
		Initobj (Single_t4291918972_il2cpp_TypeInfo_var, (&V_7));
		bool L_3 = V_4;
		if (!L_3)
		{
			goto IL_006b;
		}
	}
	{
		V_6 = (1.0f);
		V_7 = (((float)((float)0)));
		goto IL_0076;
	}

IL_006b:
	{
		V_6 = (((float)((float)0)));
		V_7 = (1.0f);
	}

IL_0076:
	{
		V_0 = (((float)((float)0)));
		RenderTexture_t1963041563 * L_4 = ___dest0;
		NullCheck(L_4);
		int32_t L_5 = RenderTexture_get_width_m1498578543(L_4, /*hidden argument*/NULL);
		V_1 = ((float)((float)(((float)((float)0)))+(float)((float)((float)(1.0f)/(float)((float)((float)(((float)((float)L_5)))*(float)(1.0f)))))));
		V_2 = (((float)((float)0)));
		V_3 = (1.0f);
		GL_Begin_m3089952800(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		float L_6 = V_6;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (((float)((float)0))), L_6, /*hidden argument*/NULL);
		float L_7 = V_0;
		float L_8 = V_2;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_7, L_8, (0.1f), /*hidden argument*/NULL);
		float L_9 = V_6;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (1.0f), L_9, /*hidden argument*/NULL);
		float L_10 = V_1;
		float L_11 = V_2;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_10, L_11, (0.1f), /*hidden argument*/NULL);
		float L_12 = V_7;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (1.0f), L_12, /*hidden argument*/NULL);
		float L_13 = V_1;
		float L_14 = V_3;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_13, L_14, (0.1f), /*hidden argument*/NULL);
		float L_15 = V_7;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (((float)((float)0))), L_15, /*hidden argument*/NULL);
		float L_16 = V_0;
		float L_17 = V_3;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_16, L_17, (0.1f), /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_18 = ___dest0;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_width_m1498578543(L_18, /*hidden argument*/NULL);
		V_0 = ((float)((float)(1.0f)-(float)((float)((float)(1.0f)/(float)((float)((float)(((float)((float)L_19)))*(float)(1.0f)))))));
		V_1 = (1.0f);
		V_2 = (((float)((float)0)));
		V_3 = (1.0f);
		float L_20 = V_6;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (((float)((float)0))), L_20, /*hidden argument*/NULL);
		float L_21 = V_0;
		float L_22 = V_2;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_21, L_22, (0.1f), /*hidden argument*/NULL);
		float L_23 = V_6;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (1.0f), L_23, /*hidden argument*/NULL);
		float L_24 = V_1;
		float L_25 = V_2;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_24, L_25, (0.1f), /*hidden argument*/NULL);
		float L_26 = V_7;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (1.0f), L_26, /*hidden argument*/NULL);
		float L_27 = V_1;
		float L_28 = V_3;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_27, L_28, (0.1f), /*hidden argument*/NULL);
		float L_29 = V_7;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (((float)((float)0))), L_29, /*hidden argument*/NULL);
		float L_30 = V_0;
		float L_31 = V_3;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_30, L_31, (0.1f), /*hidden argument*/NULL);
		V_0 = (((float)((float)0)));
		V_1 = (1.0f);
		V_2 = (((float)((float)0)));
		RenderTexture_t1963041563 * L_32 = ___dest0;
		NullCheck(L_32);
		int32_t L_33 = RenderTexture_get_height_m4010076224(L_32, /*hidden argument*/NULL);
		V_3 = ((float)((float)(((float)((float)0)))+(float)((float)((float)(1.0f)/(float)((float)((float)(((float)((float)L_33)))*(float)(1.0f)))))));
		float L_34 = V_6;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (((float)((float)0))), L_34, /*hidden argument*/NULL);
		float L_35 = V_0;
		float L_36 = V_2;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_35, L_36, (0.1f), /*hidden argument*/NULL);
		float L_37 = V_6;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (1.0f), L_37, /*hidden argument*/NULL);
		float L_38 = V_1;
		float L_39 = V_2;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_38, L_39, (0.1f), /*hidden argument*/NULL);
		float L_40 = V_7;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (1.0f), L_40, /*hidden argument*/NULL);
		float L_41 = V_1;
		float L_42 = V_3;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_41, L_42, (0.1f), /*hidden argument*/NULL);
		float L_43 = V_7;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (((float)((float)0))), L_43, /*hidden argument*/NULL);
		float L_44 = V_0;
		float L_45 = V_3;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_44, L_45, (0.1f), /*hidden argument*/NULL);
		V_0 = (((float)((float)0)));
		V_1 = (1.0f);
		RenderTexture_t1963041563 * L_46 = ___dest0;
		NullCheck(L_46);
		int32_t L_47 = RenderTexture_get_height_m4010076224(L_46, /*hidden argument*/NULL);
		V_2 = ((float)((float)(1.0f)-(float)((float)((float)(1.0f)/(float)((float)((float)(((float)((float)L_47)))*(float)(1.0f)))))));
		V_3 = (1.0f);
		float L_48 = V_6;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (((float)((float)0))), L_48, /*hidden argument*/NULL);
		float L_49 = V_0;
		float L_50 = V_2;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_49, L_50, (0.1f), /*hidden argument*/NULL);
		float L_51 = V_6;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (1.0f), L_51, /*hidden argument*/NULL);
		float L_52 = V_1;
		float L_53 = V_2;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_52, L_53, (0.1f), /*hidden argument*/NULL);
		float L_54 = V_7;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (1.0f), L_54, /*hidden argument*/NULL);
		float L_55 = V_1;
		float L_56 = V_3;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_55, L_56, (0.1f), /*hidden argument*/NULL);
		float L_57 = V_7;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (((float)((float)0))), L_57, /*hidden argument*/NULL);
		float L_58 = V_0;
		float L_59 = V_3;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_58, L_59, (0.1f), /*hidden argument*/NULL);
		GL_End_m2013837889(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_60 = V_5;
		V_5 = ((int32_t)((int32_t)L_60+(int32_t)1));
	}

IL_0287:
	{
		int32_t L_61 = V_5;
		Material_t3870600107 * L_62 = ___material1;
		NullCheck(L_62);
		int32_t L_63 = Material_get_passCount_m2926759545(L_62, /*hidden argument*/NULL);
		if ((((int32_t)L_61) < ((int32_t)L_63)))
		{
			goto IL_003b;
		}
	}
	{
		GL_PopMatrix_m3073322328(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PostEffectsHelper::DrawLowLevelQuad(System.Single,System.Single,System.Single,System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material)
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral558922319;
extern const uint32_t PostEffectsHelper_DrawLowLevelQuad_m4130177535_MetadataUsageId;
extern "C"  void PostEffectsHelper_DrawLowLevelQuad_m4130177535 (Il2CppObject * __this /* static, unused */, float ___x10, float ___x21, float ___y12, float ___y23, RenderTexture_t1963041563 * ___source4, RenderTexture_t1963041563 * ___dest5, Material_t3870600107 * ___material6, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsHelper_DrawLowLevelQuad_m4130177535_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		RenderTexture_t1963041563 * L_0 = ___dest5;
		RenderTexture_set_active_m1002947377(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Material_t3870600107 * L_1 = ___material6;
		RenderTexture_t1963041563 * L_2 = ___source4;
		NullCheck(L_1);
		Material_SetTexture_m1833724755(L_1, _stringLiteral558922319, L_2, /*hidden argument*/NULL);
		V_0 = (bool)1;
		GL_PushMatrix_m626765559(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_LoadOrtho_m1297524312(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = 0;
		goto IL_00cf;
	}

IL_0031:
	{
		Material_t3870600107 * L_3 = ___material6;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		Material_SetPass_m4241824642(L_3, L_4, /*hidden argument*/NULL);
		GL_Begin_m3089952800(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		Initobj (Single_t4291918972_il2cpp_TypeInfo_var, (&V_2));
		Initobj (Single_t4291918972_il2cpp_TypeInfo_var, (&V_3));
		bool L_5 = V_0;
		if (!L_5)
		{
			goto IL_0067;
		}
	}
	{
		V_2 = (1.0f);
		V_3 = (((float)((float)0)));
		goto IL_0070;
	}

IL_0067:
	{
		V_2 = (((float)((float)0)));
		V_3 = (1.0f);
	}

IL_0070:
	{
		float L_6 = V_2;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (((float)((float)0))), L_6, /*hidden argument*/NULL);
		float L_7 = ___x10;
		float L_8 = ___y12;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_7, L_8, (0.1f), /*hidden argument*/NULL);
		float L_9 = V_2;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (1.0f), L_9, /*hidden argument*/NULL);
		float L_10 = ___x21;
		float L_11 = ___y12;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_10, L_11, (0.1f), /*hidden argument*/NULL);
		float L_12 = V_3;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (1.0f), L_12, /*hidden argument*/NULL);
		float L_13 = ___x21;
		float L_14 = ___y23;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_13, L_14, (0.1f), /*hidden argument*/NULL);
		float L_15 = V_3;
		GL_TexCoord2_m3945835718(NULL /*static, unused*/, (((float)((float)0))), L_15, /*hidden argument*/NULL);
		float L_16 = ___x10;
		float L_17 = ___y23;
		GL_Vertex3_m3691855584(NULL /*static, unused*/, L_16, L_17, (0.1f), /*hidden argument*/NULL);
		GL_End_m2013837889(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_18 = V_1;
		V_1 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_00cf:
	{
		int32_t L_19 = V_1;
		Material_t3870600107 * L_20 = ___material6;
		NullCheck(L_20);
		int32_t L_21 = Material_get_passCount_m2926759545(L_20, /*hidden argument*/NULL);
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0031;
		}
	}
	{
		GL_PopMatrix_m3073322328(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PostEffectsHelper::Main()
extern "C"  void PostEffectsHelper_Main_m3162795211 (PostEffectsHelper_t1948321392 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Quads::.ctor()
extern "C"  void Quads__ctor_m4020850230 (Quads_t78387180 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Quads::.cctor()
extern "C"  void Quads__cctor_m3905176631 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean Quads::HasMeshes()
extern Il2CppClass* Quads_t78387180_il2cpp_TypeInfo_var;
extern const uint32_t Quads_HasMeshes_m3391617551_MetadataUsageId;
extern "C"  bool Quads_HasMeshes_m3391617551 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Quads_HasMeshes_m3391617551_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Mesh_t4241756145 * V_0 = NULL;
	int32_t V_1 = 0;
	MeshU5BU5D_t1759126828* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t G_B8_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t78387180_il2cpp_TypeInfo_var);
		MeshU5BU5D_t1759126828* L_0 = ((Quads_t78387180_StaticFields*)Quads_t78387180_il2cpp_TypeInfo_var->static_fields)->get_meshes_2();
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		G_B8_0 = 0;
		goto IL_0044;
	}

IL_0010:
	{
		V_1 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t78387180_il2cpp_TypeInfo_var);
		MeshU5BU5D_t1759126828* L_1 = ((Quads_t78387180_StaticFields*)Quads_t78387180_il2cpp_TypeInfo_var->static_fields)->get_meshes_2();
		V_2 = L_1;
		MeshU5BU5D_t1759126828* L_2 = V_2;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_2);
		int32_t L_3 = Array_get_Length_m1203127607((Il2CppArray *)(Il2CppArray *)L_2, /*hidden argument*/NULL);
		V_3 = L_3;
		goto IL_003c;
	}

IL_0024:
	{
		MeshU5BU5D_t1759126828* L_4 = V_2;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		Mesh_t4241756145 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, (Object_t3071478659 *)NULL, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0038;
		}
	}
	{
		G_B8_0 = 0;
		goto IL_0044;
	}

IL_0038:
	{
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = V_3;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0024;
		}
	}
	{
		G_B8_0 = 1;
	}

IL_0044:
	{
		return (bool)G_B8_0;
	}
}
// System.Void Quads::Cleanup()
extern Il2CppClass* Quads_t78387180_il2cpp_TypeInfo_var;
extern const uint32_t Quads_Cleanup_m1235265592_MetadataUsageId;
extern "C"  void Quads_Cleanup_m1235265592 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Quads_Cleanup_m1235265592_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Mesh_t4241756145 * V_0 = NULL;
	int32_t V_1 = 0;
	MeshU5BU5D_t1759126828* V_2 = NULL;
	int32_t V_3 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t78387180_il2cpp_TypeInfo_var);
		MeshU5BU5D_t1759126828* L_0 = ((Quads_t78387180_StaticFields*)Quads_t78387180_il2cpp_TypeInfo_var->static_fields)->get_meshes_2();
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		goto IL_004e;
	}

IL_000f:
	{
		V_1 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t78387180_il2cpp_TypeInfo_var);
		MeshU5BU5D_t1759126828* L_1 = ((Quads_t78387180_StaticFields*)Quads_t78387180_il2cpp_TypeInfo_var->static_fields)->get_meshes_2();
		V_2 = L_1;
		MeshU5BU5D_t1759126828* L_2 = V_2;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_2);
		int32_t L_3 = Array_get_Length_m1203127607((Il2CppArray *)(Il2CppArray *)L_2, /*hidden argument*/NULL);
		V_3 = L_3;
		goto IL_0041;
	}

IL_0023:
	{
		MeshU5BU5D_t1759126828* L_4 = V_2;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		Mesh_t4241756145 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, (Object_t3071478659 *)NULL, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_003d;
		}
	}
	{
		MeshU5BU5D_t1759126828* L_9 = V_2;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Mesh_t4241756145 * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		MeshU5BU5D_t1759126828* L_13 = V_2;
		int32_t L_14 = V_1;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		ArrayElementTypeCheck (L_13, NULL);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (Mesh_t4241756145 *)NULL);
	}

IL_003d:
	{
		int32_t L_15 = V_1;
		V_1 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0041:
	{
		int32_t L_16 = V_1;
		int32_t L_17 = V_3;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t78387180_il2cpp_TypeInfo_var);
		((Quads_t78387180_StaticFields*)Quads_t78387180_il2cpp_TypeInfo_var->static_fields)->set_meshes_2((MeshU5BU5D_t1759126828*)NULL);
	}

IL_004e:
	{
		return;
	}
}
// UnityEngine.Mesh[] Quads::GetMeshes(System.Int32,System.Int32)
extern Il2CppClass* Quads_t78387180_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppClass* MeshU5BU5D_t1759126828_il2cpp_TypeInfo_var;
extern const uint32_t Quads_GetMeshes_m2562620632_MetadataUsageId;
extern "C"  MeshU5BU5D_t1759126828* Quads_GetMeshes_m2562620632 (Il2CppObject * __this /* static, unused */, int32_t ___totalWidth0, int32_t ___totalHeight1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Quads_GetMeshes_m2562620632_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	MeshU5BU5D_t1759126828* G_B7_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t78387180_il2cpp_TypeInfo_var);
		bool L_0 = Quads_HasMeshes_m3391617551(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t78387180_il2cpp_TypeInfo_var);
		int32_t L_1 = ((Quads_t78387180_StaticFields*)Quads_t78387180_il2cpp_TypeInfo_var->static_fields)->get_currentQuads_3();
		int32_t L_2 = ___totalWidth0;
		int32_t L_3 = ___totalHeight1;
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)((int32_t)L_2*(int32_t)L_3))))))
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t78387180_il2cpp_TypeInfo_var);
		MeshU5BU5D_t1759126828* L_4 = ((Quads_t78387180_StaticFields*)Quads_t78387180_il2cpp_TypeInfo_var->static_fields)->get_meshes_2();
		G_B7_0 = L_4;
		goto IL_0099;
	}

IL_0021:
	{
		V_0 = ((int32_t)10833);
		int32_t L_5 = ___totalWidth0;
		int32_t L_6 = ___totalHeight1;
		V_1 = ((int32_t)((int32_t)L_5*(int32_t)L_6));
		int32_t L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t78387180_il2cpp_TypeInfo_var);
		((Quads_t78387180_StaticFields*)Quads_t78387180_il2cpp_TypeInfo_var->static_fields)->set_currentQuads_3(L_7);
		int32_t L_8 = V_1;
		int32_t L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_10 = Mathf_CeilToInt_m3621832739(NULL /*static, unused*/, ((float)((float)((float)((float)(1.0f)*(float)(((float)((float)L_8)))))/(float)((float)((float)(1.0f)*(float)(((float)((float)L_9))))))), /*hidden argument*/NULL);
		V_2 = L_10;
		int32_t L_11 = V_2;
		((Quads_t78387180_StaticFields*)Quads_t78387180_il2cpp_TypeInfo_var->static_fields)->set_meshes_2(((MeshU5BU5D_t1759126828*)SZArrayNew(MeshU5BU5D_t1759126828_il2cpp_TypeInfo_var, (uint32_t)L_11)));
		V_3 = 0;
		V_4 = 0;
		V_3 = 0;
		goto IL_008d;
	}

IL_005f:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_3;
		int32_t L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_15 = Mathf_Clamp_m510460741(NULL /*static, unused*/, ((int32_t)((int32_t)L_12-(int32_t)L_13)), 0, L_14, /*hidden argument*/NULL);
		int32_t L_16 = Mathf_FloorToInt_m268511322(NULL /*static, unused*/, (((float)((float)L_15))), /*hidden argument*/NULL);
		V_5 = L_16;
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t78387180_il2cpp_TypeInfo_var);
		MeshU5BU5D_t1759126828* L_17 = ((Quads_t78387180_StaticFields*)Quads_t78387180_il2cpp_TypeInfo_var->static_fields)->get_meshes_2();
		int32_t L_18 = V_4;
		int32_t L_19 = V_5;
		int32_t L_20 = V_3;
		int32_t L_21 = ___totalWidth0;
		int32_t L_22 = ___totalHeight1;
		Mesh_t4241756145 * L_23 = Quads_GetMesh_m740128644(NULL /*static, unused*/, L_19, L_20, L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		ArrayElementTypeCheck (L_17, L_23);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (Mesh_t4241756145 *)L_23);
		int32_t L_24 = V_4;
		V_4 = ((int32_t)((int32_t)L_24+(int32_t)1));
		int32_t L_25 = V_3;
		int32_t L_26 = V_0;
		V_3 = ((int32_t)((int32_t)L_25+(int32_t)L_26));
	}

IL_008d:
	{
		int32_t L_27 = V_3;
		int32_t L_28 = V_1;
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_005f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t78387180_il2cpp_TypeInfo_var);
		MeshU5BU5D_t1759126828* L_29 = ((Quads_t78387180_StaticFields*)Quads_t78387180_il2cpp_TypeInfo_var->static_fields)->get_meshes_2();
		G_B7_0 = L_29;
	}

IL_0099:
	{
		return G_B7_0;
	}
}
// UnityEngine.Mesh Quads::GetMesh(System.Int32,System.Int32,System.Int32,System.Int32)
extern Il2CppClass* Mesh_t4241756145_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3U5BU5D_t215400611_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector2U5BU5D_t4024180168_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Quads_GetMesh_m740128644_MetadataUsageId;
extern "C"  Mesh_t4241756145 * Quads_GetMesh_m740128644 (Il2CppObject * __this /* static, unused */, int32_t ___triCount0, int32_t ___triOffset1, int32_t ___totalWidth2, int32_t ___totalHeight3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Quads_GetMesh_m740128644_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Mesh_t4241756145 * V_0 = NULL;
	Vector3U5BU5D_t215400611* V_1 = NULL;
	Vector2U5BU5D_t4024180168* V_2 = NULL;
	Vector2U5BU5D_t4024180168* V_3 = NULL;
	Int32U5BU5D_t3230847821* V_4 = NULL;
	float V_5 = 0.0f;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	Vector3_t4282066566  V_12;
	memset(&V_12, 0, sizeof(V_12));
	{
		Mesh_t4241756145 * L_0 = (Mesh_t4241756145 *)il2cpp_codegen_object_new(Mesh_t4241756145_il2cpp_TypeInfo_var);
		Mesh__ctor_m2684203808(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Mesh_t4241756145 * L_1 = V_0;
		NullCheck(L_1);
		Object_set_hideFlags_m41317712(L_1, ((int32_t)52), /*hidden argument*/NULL);
		int32_t L_2 = ___triCount0;
		V_1 = ((Vector3U5BU5D_t215400611*)SZArrayNew(Vector3U5BU5D_t215400611_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_2*(int32_t)4))));
		int32_t L_3 = ___triCount0;
		V_2 = ((Vector2U5BU5D_t4024180168*)SZArrayNew(Vector2U5BU5D_t4024180168_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_3*(int32_t)4))));
		int32_t L_4 = ___triCount0;
		V_3 = ((Vector2U5BU5D_t4024180168*)SZArrayNew(Vector2U5BU5D_t4024180168_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_4*(int32_t)4))));
		int32_t L_5 = ___triCount0;
		V_4 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_5*(int32_t)6))));
		V_5 = (0.0075f);
		V_6 = 0;
		goto IL_01e6;
	}

IL_0042:
	{
		int32_t L_6 = V_6;
		V_7 = ((int32_t)((int32_t)L_6*(int32_t)4));
		int32_t L_7 = V_6;
		V_8 = ((int32_t)((int32_t)L_7*(int32_t)6));
		int32_t L_8 = ___triOffset1;
		int32_t L_9 = V_6;
		V_9 = ((int32_t)((int32_t)L_8+(int32_t)L_9));
		int32_t L_10 = V_9;
		int32_t L_11 = ___totalWidth2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_12 = floorf((((float)((float)((int32_t)((int32_t)L_10%(int32_t)L_11))))));
		int32_t L_13 = ___totalWidth2;
		V_10 = ((float)((float)L_12/(float)(((float)((float)L_13)))));
		int32_t L_14 = V_9;
		int32_t L_15 = ___totalWidth2;
		float L_16 = floorf((((float)((float)((int32_t)((int32_t)L_14/(int32_t)L_15))))));
		int32_t L_17 = ___totalHeight3;
		V_11 = ((float)((float)L_16/(float)(((float)((float)L_17)))));
		float L_18 = V_10;
		float L_19 = V_11;
		Vector3_t4282066566  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector3__ctor_m2926210380(&L_20, ((float)((float)((float)((float)L_18*(float)(((float)((float)2)))))-(float)(((float)((float)1))))), ((float)((float)((float)((float)L_19*(float)(((float)((float)2)))))-(float)(((float)((float)1))))), (1.0f), /*hidden argument*/NULL);
		V_12 = L_20;
		Vector3U5BU5D_t215400611* L_21 = V_1;
		int32_t L_22 = V_7;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, ((int32_t)((int32_t)L_22+(int32_t)0)));
		Vector3_t4282066566  L_23 = V_12;
		(*(Vector3_t4282066566 *)((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_22+(int32_t)0)))))) = L_23;
		Vector3U5BU5D_t215400611* L_24 = V_1;
		int32_t L_25 = V_7;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, ((int32_t)((int32_t)L_25+(int32_t)1)));
		Vector3_t4282066566  L_26 = V_12;
		(*(Vector3_t4282066566 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_25+(int32_t)1)))))) = L_26;
		Vector3U5BU5D_t215400611* L_27 = V_1;
		int32_t L_28 = V_7;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, ((int32_t)((int32_t)L_28+(int32_t)2)));
		Vector3_t4282066566  L_29 = V_12;
		(*(Vector3_t4282066566 *)((L_27)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_28+(int32_t)2)))))) = L_29;
		Vector3U5BU5D_t215400611* L_30 = V_1;
		int32_t L_31 = V_7;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, ((int32_t)((int32_t)L_31+(int32_t)3)));
		Vector3_t4282066566  L_32 = V_12;
		(*(Vector3_t4282066566 *)((L_30)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_31+(int32_t)3)))))) = L_32;
		Vector2U5BU5D_t4024180168* L_33 = V_2;
		int32_t L_34 = V_7;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, ((int32_t)((int32_t)L_34+(int32_t)0)));
		Vector2_t4282066565  L_35;
		memset(&L_35, 0, sizeof(L_35));
		Vector2__ctor_m1517109030(&L_35, (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_33)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_34+(int32_t)0)))))) = L_35;
		Vector2U5BU5D_t4024180168* L_36 = V_2;
		int32_t L_37 = V_7;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)((int32_t)L_37+(int32_t)1)));
		Vector2_t4282066565  L_38;
		memset(&L_38, 0, sizeof(L_38));
		Vector2__ctor_m1517109030(&L_38, (1.0f), (((float)((float)0))), /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_37+(int32_t)1)))))) = L_38;
		Vector2U5BU5D_t4024180168* L_39 = V_2;
		int32_t L_40 = V_7;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, ((int32_t)((int32_t)L_40+(int32_t)2)));
		Vector2_t4282066565  L_41;
		memset(&L_41, 0, sizeof(L_41));
		Vector2__ctor_m1517109030(&L_41, (((float)((float)0))), (1.0f), /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_40+(int32_t)2)))))) = L_41;
		Vector2U5BU5D_t4024180168* L_42 = V_2;
		int32_t L_43 = V_7;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, ((int32_t)((int32_t)L_43+(int32_t)3)));
		Vector2_t4282066565  L_44;
		memset(&L_44, 0, sizeof(L_44));
		Vector2__ctor_m1517109030(&L_44, (1.0f), (1.0f), /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_42)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_43+(int32_t)3)))))) = L_44;
		Vector2U5BU5D_t4024180168* L_45 = V_3;
		int32_t L_46 = V_7;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, ((int32_t)((int32_t)L_46+(int32_t)0)));
		float L_47 = V_10;
		float L_48 = V_11;
		Vector2_t4282066565  L_49;
		memset(&L_49, 0, sizeof(L_49));
		Vector2__ctor_m1517109030(&L_49, L_47, L_48, /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_45)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_46+(int32_t)0)))))) = L_49;
		Vector2U5BU5D_t4024180168* L_50 = V_3;
		int32_t L_51 = V_7;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, ((int32_t)((int32_t)L_51+(int32_t)1)));
		float L_52 = V_10;
		float L_53 = V_11;
		Vector2_t4282066565  L_54;
		memset(&L_54, 0, sizeof(L_54));
		Vector2__ctor_m1517109030(&L_54, L_52, L_53, /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_50)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_51+(int32_t)1)))))) = L_54;
		Vector2U5BU5D_t4024180168* L_55 = V_3;
		int32_t L_56 = V_7;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, ((int32_t)((int32_t)L_56+(int32_t)2)));
		float L_57 = V_10;
		float L_58 = V_11;
		Vector2_t4282066565  L_59;
		memset(&L_59, 0, sizeof(L_59));
		Vector2__ctor_m1517109030(&L_59, L_57, L_58, /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_55)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_56+(int32_t)2)))))) = L_59;
		Vector2U5BU5D_t4024180168* L_60 = V_3;
		int32_t L_61 = V_7;
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, ((int32_t)((int32_t)L_61+(int32_t)3)));
		float L_62 = V_10;
		float L_63 = V_11;
		Vector2_t4282066565  L_64;
		memset(&L_64, 0, sizeof(L_64));
		Vector2__ctor_m1517109030(&L_64, L_62, L_63, /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_60)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_61+(int32_t)3)))))) = L_64;
		Int32U5BU5D_t3230847821* L_65 = V_4;
		int32_t L_66 = V_8;
		int32_t L_67 = V_7;
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, ((int32_t)((int32_t)L_66+(int32_t)0)));
		(L_65)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_66+(int32_t)0))), (int32_t)((int32_t)((int32_t)L_67+(int32_t)0)));
		Int32U5BU5D_t3230847821* L_68 = V_4;
		int32_t L_69 = V_8;
		int32_t L_70 = V_7;
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, ((int32_t)((int32_t)L_69+(int32_t)1)));
		(L_68)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_69+(int32_t)1))), (int32_t)((int32_t)((int32_t)L_70+(int32_t)1)));
		Int32U5BU5D_t3230847821* L_71 = V_4;
		int32_t L_72 = V_8;
		int32_t L_73 = V_7;
		NullCheck(L_71);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_71, ((int32_t)((int32_t)L_72+(int32_t)2)));
		(L_71)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_72+(int32_t)2))), (int32_t)((int32_t)((int32_t)L_73+(int32_t)2)));
		Int32U5BU5D_t3230847821* L_74 = V_4;
		int32_t L_75 = V_8;
		int32_t L_76 = V_7;
		NullCheck(L_74);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_74, ((int32_t)((int32_t)L_75+(int32_t)3)));
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_75+(int32_t)3))), (int32_t)((int32_t)((int32_t)L_76+(int32_t)1)));
		Int32U5BU5D_t3230847821* L_77 = V_4;
		int32_t L_78 = V_8;
		int32_t L_79 = V_7;
		NullCheck(L_77);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_77, ((int32_t)((int32_t)L_78+(int32_t)4)));
		(L_77)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_78+(int32_t)4))), (int32_t)((int32_t)((int32_t)L_79+(int32_t)2)));
		Int32U5BU5D_t3230847821* L_80 = V_4;
		int32_t L_81 = V_8;
		int32_t L_82 = V_7;
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, ((int32_t)((int32_t)L_81+(int32_t)5)));
		(L_80)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_81+(int32_t)5))), (int32_t)((int32_t)((int32_t)L_82+(int32_t)3)));
		int32_t L_83 = V_6;
		V_6 = ((int32_t)((int32_t)L_83+(int32_t)1));
	}

IL_01e6:
	{
		int32_t L_84 = V_6;
		int32_t L_85 = ___triCount0;
		if ((((int32_t)L_84) < ((int32_t)L_85)))
		{
			goto IL_0042;
		}
	}
	{
		Mesh_t4241756145 * L_86 = V_0;
		Vector3U5BU5D_t215400611* L_87 = V_1;
		NullCheck(L_86);
		Mesh_set_vertices_m2628866109(L_86, L_87, /*hidden argument*/NULL);
		Mesh_t4241756145 * L_88 = V_0;
		Int32U5BU5D_t3230847821* L_89 = V_4;
		NullCheck(L_88);
		Mesh_set_triangles_m2341339867(L_88, L_89, /*hidden argument*/NULL);
		Mesh_t4241756145 * L_90 = V_0;
		Vector2U5BU5D_t4024180168* L_91 = V_2;
		NullCheck(L_90);
		Mesh_set_uv_m498907190(L_90, L_91, /*hidden argument*/NULL);
		Mesh_t4241756145 * L_92 = V_0;
		Vector2U5BU5D_t4024180168* L_93 = V_3;
		NullCheck(L_92);
		Mesh_set_uv2_m1515914022(L_92, L_93, /*hidden argument*/NULL);
		Mesh_t4241756145 * L_94 = V_0;
		return L_94;
	}
}
// System.Void Quads::Main()
extern "C"  void Quads_Main_m3370335175 (Quads_t78387180 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ScreenOverlay::.ctor()
extern "C"  void ScreenOverlay__ctor_m3824458750 (ScreenOverlay_t1089288740 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase__ctor_m3869393199(__this, /*hidden argument*/NULL);
		__this->set_blendMode_5(3);
		__this->set_intensity_6((1.0f));
		return;
	}
}
// System.Boolean ScreenOverlay::CheckResources()
extern "C"  bool ScreenOverlay_CheckResources_m3838015709 (ScreenOverlay_t1089288740 * __this, const MethodInfo* method)
{
	{
		VirtFuncInvoker1< bool, bool >::Invoke(10 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean) */, __this, (bool)0);
		Shader_t3191267369 * L_0 = __this->get_overlayShader_8();
		Material_t3870600107 * L_1 = __this->get_overlayMaterial_9();
		Material_t3870600107 * L_2 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_0, L_1);
		__this->set_overlayMaterial_9(L_2);
		bool L_3 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		if (L_3)
		{
			goto IL_0031;
		}
	}
	{
		VirtActionInvoker0::Invoke(13 /* System.Void PostEffectsBase::ReportAutoDisable() */, __this);
	}

IL_0031:
	{
		bool L_4 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		return L_4;
	}
}
// System.Void ScreenOverlay::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1927341325;
extern Il2CppCodeGenString* _stringLiteral1014379156;
extern Il2CppCodeGenString* _stringLiteral2931589873;
extern const uint32_t ScreenOverlay_OnRenderImage_m3261548960_MetadataUsageId;
extern "C"  void ScreenOverlay_OnRenderImage_m3261548960 (ScreenOverlay_t1089288740 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScreenOverlay_OnRenderImage_m3261548960_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector4_t4282066567  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean ScreenOverlay::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		RenderTexture_t1963041563 * L_1 = ___source0;
		RenderTexture_t1963041563 * L_2 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_0075;
	}

IL_0017:
	{
		Vector4_t4282066567  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector4__ctor_m2441427762(&L_3, (((float)((float)1))), (((float)((float)0))), (((float)((float)0))), (((float)((float)1))), /*hidden argument*/NULL);
		V_0 = L_3;
		Material_t3870600107 * L_4 = __this->get_overlayMaterial_9();
		Vector4_t4282066567  L_5 = V_0;
		NullCheck(L_4);
		Material_SetVector_m3505096203(L_4, _stringLiteral1927341325, L_5, /*hidden argument*/NULL);
		Material_t3870600107 * L_6 = __this->get_overlayMaterial_9();
		float L_7 = __this->get_intensity_6();
		NullCheck(L_6);
		Material_SetFloat_m981710063(L_6, _stringLiteral1014379156, L_7, /*hidden argument*/NULL);
		Material_t3870600107 * L_8 = __this->get_overlayMaterial_9();
		Texture2D_t3884108195 * L_9 = __this->get_texture_7();
		NullCheck(L_8);
		Material_SetTexture_m1833724755(L_8, _stringLiteral2931589873, L_9, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_10 = ___source0;
		RenderTexture_t1963041563 * L_11 = ___destination1;
		Material_t3870600107 * L_12 = __this->get_overlayMaterial_9();
		int32_t L_13 = __this->get_blendMode_5();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
	}

IL_0075:
	{
		return;
	}
}
// System.Void ScreenOverlay::Main()
extern "C"  void ScreenOverlay_Main_m177411327 (ScreenOverlay_t1089288740 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SmoothFollow::.ctor()
extern "C"  void SmoothFollow__ctor_m3116031813 (SmoothFollow_t651130655 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		__this->set_distance_3((10.0f));
		__this->set_height_4((5.0f));
		__this->set_heightDamping_5((2.0f));
		__this->set_rotationDamping_6((3.0f));
		return;
	}
}
// System.Void SmoothFollow::LateUpdate()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t SmoothFollow_LateUpdate_m3251383854_MetadataUsageId;
extern "C"  void SmoothFollow_LateUpdate_m3251383854 (SmoothFollow_t651130655 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SmoothFollow_LateUpdate_m3251383854_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Quaternion_t1553702882  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	Vector3_t4282066566  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t4282066566  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t4282066566  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t4282066566  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t4282066566  V_10;
	memset(&V_10, 0, sizeof(V_10));
	float V_11 = 0.0f;
	Vector3_t4282066566  V_12;
	memset(&V_12, 0, sizeof(V_12));
	{
		Transform_t1659122786 * L_0 = __this->get_target_2();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		goto IL_0133;
	}

IL_0015:
	{
		Transform_t1659122786 * L_2 = __this->get_target_2();
		NullCheck(L_2);
		Vector3_t4282066566  L_3 = Transform_get_eulerAngles_m1058084741(L_2, /*hidden argument*/NULL);
		V_7 = L_3;
		float L_4 = (&V_7)->get_y_2();
		V_0 = L_4;
		Transform_t1659122786 * L_5 = __this->get_target_2();
		NullCheck(L_5);
		Vector3_t4282066566  L_6 = Transform_get_position_m2211398607(L_5, /*hidden argument*/NULL);
		V_8 = L_6;
		float L_7 = (&V_8)->get_y_2();
		float L_8 = __this->get_height_4();
		V_1 = ((float)((float)L_7+(float)L_8));
		Transform_t1659122786 * L_9 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t4282066566  L_10 = Transform_get_eulerAngles_m1058084741(L_9, /*hidden argument*/NULL);
		V_9 = L_10;
		float L_11 = (&V_9)->get_y_2();
		V_2 = L_11;
		Transform_t1659122786 * L_12 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t4282066566  L_13 = Transform_get_position_m2211398607(L_12, /*hidden argument*/NULL);
		V_10 = L_13;
		float L_14 = (&V_10)->get_y_2();
		V_3 = L_14;
		float L_15 = V_2;
		float L_16 = V_0;
		float L_17 = __this->get_rotationDamping_6();
		float L_18 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_19 = Mathf_LerpAngle_m1852538964(NULL /*static, unused*/, L_15, L_16, ((float)((float)L_17*(float)L_18)), /*hidden argument*/NULL);
		V_2 = L_19;
		float L_20 = V_3;
		float L_21 = V_1;
		float L_22 = __this->get_heightDamping_5();
		float L_23 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_24 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_20, L_21, ((float)((float)L_22*(float)L_23)), /*hidden argument*/NULL);
		V_3 = L_24;
		float L_25 = V_2;
		Quaternion_t1553702882  L_26 = Quaternion_Euler_m1204688217(NULL /*static, unused*/, (((float)((float)0))), L_25, (((float)((float)0))), /*hidden argument*/NULL);
		V_4 = L_26;
		Transform_t1659122786 * L_27 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t1659122786 * L_28 = __this->get_target_2();
		NullCheck(L_28);
		Vector3_t4282066566  L_29 = Transform_get_position_m2211398607(L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_set_position_m3111394108(L_27, L_29, /*hidden argument*/NULL);
		Transform_t1659122786 * L_30 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t1659122786 * L_31 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_31);
		Vector3_t4282066566  L_32 = Transform_get_position_m2211398607(L_31, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_33 = V_4;
		Vector3_t4282066566  L_34 = Vector3_get_forward_m1039372701(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_35 = Quaternion_op_Multiply_m3771288979(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		float L_36 = __this->get_distance_3();
		Vector3_t4282066566  L_37 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		Vector3_t4282066566  L_38 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_32, L_37, /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_set_position_m3111394108(L_30, L_38, /*hidden argument*/NULL);
		float L_39 = V_3;
		float L_40 = L_39;
		V_5 = L_40;
		Transform_t1659122786 * L_41 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_41);
		Vector3_t4282066566  L_42 = Transform_get_position_m2211398607(L_41, /*hidden argument*/NULL);
		Vector3_t4282066566  L_43 = L_42;
		V_6 = L_43;
		float L_44 = V_5;
		float L_45 = L_44;
		V_11 = L_45;
		(&V_6)->set_y_2(L_45);
		float L_46 = V_11;
		Transform_t1659122786 * L_47 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_48 = V_6;
		Vector3_t4282066566  L_49 = L_48;
		V_12 = L_49;
		NullCheck(L_47);
		Transform_set_position_m3111394108(L_47, L_49, /*hidden argument*/NULL);
		Vector3_t4282066566  L_50 = V_12;
		Transform_t1659122786 * L_51 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t1659122786 * L_52 = __this->get_target_2();
		NullCheck(L_51);
		Transform_LookAt_m2663225588(L_51, L_52, /*hidden argument*/NULL);
	}

IL_0133:
	{
		return;
	}
}
// System.Void SmoothFollow::Main()
extern "C"  void SmoothFollow_Main_m3202600152 (SmoothFollow_t651130655 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SmoothLookAt::.ctor()
extern "C"  void SmoothLookAt__ctor_m4207040740 (SmoothLookAt_t822992544 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		__this->set_damping_3((6.0f));
		__this->set_smooth_4((bool)1);
		return;
	}
}
// System.Void SmoothLookAt::LateUpdate()
extern "C"  void SmoothLookAt_LateUpdate_m2530926575 (SmoothLookAt_t822992544 * __this, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t1659122786 * L_0 = __this->get_target_2();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_007a;
		}
	}
	{
		bool L_2 = __this->get_smooth_4();
		if (!L_2)
		{
			goto IL_0069;
		}
	}
	{
		Transform_t1659122786 * L_3 = __this->get_target_2();
		NullCheck(L_3);
		Vector3_t4282066566  L_4 = Transform_get_position_m2211398607(L_3, /*hidden argument*/NULL);
		Transform_t1659122786 * L_5 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t4282066566  L_6 = Transform_get_position_m2211398607(L_5, /*hidden argument*/NULL);
		Vector3_t4282066566  L_7 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_8 = Quaternion_LookRotation_m1257501645(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		Transform_t1659122786 * L_9 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t1659122786 * L_10 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Quaternion_t1553702882  L_11 = Transform_get_rotation_m11483428(L_10, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_12 = V_0;
		float L_13 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_14 = __this->get_damping_3();
		Quaternion_t1553702882  L_15 = Quaternion_Slerp_m844700366(NULL /*static, unused*/, L_11, L_12, ((float)((float)L_13*(float)L_14)), /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_rotation_m1525803229(L_9, L_15, /*hidden argument*/NULL);
		goto IL_007a;
	}

IL_0069:
	{
		Transform_t1659122786 * L_16 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t1659122786 * L_17 = __this->get_target_2();
		NullCheck(L_16);
		Transform_LookAt_m2663225588(L_16, L_17, /*hidden argument*/NULL);
	}

IL_007a:
	{
		return;
	}
}
// System.Void SmoothLookAt::Start()
extern const MethodInfo* Component_GetComponent_TisRigidbody_t3346577219_m354583034_MethodInfo_var;
extern const uint32_t SmoothLookAt_Start_m3154178532_MetadataUsageId;
extern "C"  void SmoothLookAt_Start_m3154178532 (SmoothLookAt_t822992544 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SmoothLookAt_Start_m3154178532_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rigidbody_t3346577219 * L_0 = Component_GetComponent_TisRigidbody_t3346577219_m354583034(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t3346577219_m354583034_MethodInfo_var);
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Rigidbody_t3346577219 * L_2 = Component_GetComponent_TisRigidbody_t3346577219_m354583034(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t3346577219_m354583034_MethodInfo_var);
		NullCheck(L_2);
		Rigidbody_set_freezeRotation_m3989473889(L_2, (bool)1, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void SmoothLookAt::Main()
extern "C"  void SmoothLookAt_Main_m3930530649 (SmoothLookAt_t822992544 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SunShafts::.ctor()
extern "C"  void SunShafts__ctor_m2031597629 (SunShafts_t3666895493 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase__ctor_m3869393199(__this, /*hidden argument*/NULL);
		__this->set_resolution_5(1);
		__this->set_screenBlendMode_6(0);
		__this->set_radialBlurIterations_8(2);
		Color_t4194546905  L_0 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_sunColor_9(L_0);
		__this->set_sunShaftBlurRadius_10((2.5f));
		__this->set_sunShaftIntensity_11((1.15f));
		__this->set_useSkyBoxAlpha_12((0.75f));
		__this->set_maxRadius_13((0.75f));
		__this->set_useDepthTexture_14((bool)1);
		return;
	}
}
// System.Boolean SunShafts::CheckResources()
extern "C"  bool SunShafts_CheckResources_m3695063166 (SunShafts_t3666895493 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_useDepthTexture_14();
		VirtFuncInvoker1< bool, bool >::Invoke(10 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean) */, __this, L_0);
		Shader_t3191267369 * L_1 = __this->get_sunShaftsShader_15();
		Material_t3870600107 * L_2 = __this->get_sunShaftsMaterial_16();
		Material_t3870600107 * L_3 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_1, L_2);
		__this->set_sunShaftsMaterial_16(L_3);
		Shader_t3191267369 * L_4 = __this->get_simpleClearShader_17();
		Material_t3870600107 * L_5 = __this->get_simpleClearMaterial_18();
		Material_t3870600107 * L_6 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_4, L_5);
		__this->set_simpleClearMaterial_18(L_6);
		bool L_7 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		if (L_7)
		{
			goto IL_004e;
		}
	}
	{
		VirtActionInvoker0::Invoke(13 /* System.Void PostEffectsBase::ReportAutoDisable() */, __this);
	}

IL_004e:
	{
		bool L_8 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		return L_8;
	}
}
// System.Void SunShafts::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4016218524;
extern Il2CppCodeGenString* _stringLiteral2893526454;
extern Il2CppCodeGenString* _stringLiteral390482326;
extern Il2CppCodeGenString* _stringLiteral892242505;
extern Il2CppCodeGenString* _stringLiteral2688099830;
extern Il2CppCodeGenString* _stringLiteral1541889956;
extern const uint32_t SunShafts_OnRenderImage_m3230775809_MetadataUsageId;
extern "C"  void SunShafts_OnRenderImage_m3230775809 (SunShafts_t3666895493 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SunShafts_OnRenderImage_m3230775809_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	RenderTexture_t1963041563 * V_4 = NULL;
	RenderTexture_t1963041563 * V_5 = NULL;
	RenderTexture_t1963041563 * V_6 = NULL;
	float V_7 = 0.0f;
	int32_t V_8 = 0;
	Material_t3870600107 * G_B22_0 = NULL;
	RenderTexture_t1963041563 * G_B22_1 = NULL;
	RenderTexture_t1963041563 * G_B22_2 = NULL;
	Material_t3870600107 * G_B21_0 = NULL;
	RenderTexture_t1963041563 * G_B21_1 = NULL;
	RenderTexture_t1963041563 * G_B21_2 = NULL;
	int32_t G_B23_0 = 0;
	Material_t3870600107 * G_B23_1 = NULL;
	RenderTexture_t1963041563 * G_B23_2 = NULL;
	RenderTexture_t1963041563 * G_B23_3 = NULL;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean SunShafts::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		RenderTexture_t1963041563 * L_1 = ___source0;
		RenderTexture_t1963041563 * L_2 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_03ba;
	}

IL_0017:
	{
		bool L_3 = __this->get_useDepthTexture_14();
		if (!L_3)
		{
			goto IL_003a;
		}
	}
	{
		Camera_t2727095145 * L_4 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		Camera_t2727095145 * L_5 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_5);
		int32_t L_6 = Camera_get_depthTextureMode_m2117446653(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Camera_set_depthTextureMode_m2368326786(L_4, ((int32_t)((int32_t)L_6|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_003a:
	{
		V_0 = 4;
		int32_t L_7 = __this->get_resolution_5();
		if ((!(((uint32_t)L_7) == ((uint32_t)1))))
		{
			goto IL_004f;
		}
	}
	{
		V_0 = 2;
		goto IL_005d;
	}

IL_004f:
	{
		int32_t L_8 = __this->get_resolution_5();
		if ((!(((uint32_t)L_8) == ((uint32_t)2))))
		{
			goto IL_005d;
		}
	}
	{
		V_0 = 1;
	}

IL_005d:
	{
		Vector3_t4282066566  L_9 = Vector3_get_one_m886467710(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_10 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_9, (0.5f), /*hidden argument*/NULL);
		V_1 = L_10;
		Transform_t1659122786 * L_11 = __this->get_sunTransform_7();
		bool L_12 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0099;
		}
	}
	{
		Camera_t2727095145 * L_13 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		Transform_t1659122786 * L_14 = __this->get_sunTransform_7();
		NullCheck(L_14);
		Vector3_t4282066566  L_15 = Transform_get_position_m2211398607(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t4282066566  L_16 = Camera_WorldToViewportPoint_m3480725126(L_13, L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		goto IL_00ab;
	}

IL_0099:
	{
		Vector3_t4282066566  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector3__ctor_m2926210380(&L_17, (0.5f), (0.5f), (((float)((float)0))), /*hidden argument*/NULL);
		V_1 = L_17;
	}

IL_00ab:
	{
		RenderTexture_t1963041563 * L_18 = ___source0;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_width_m1498578543(L_18, /*hidden argument*/NULL);
		int32_t L_20 = V_0;
		V_2 = ((int32_t)((int32_t)L_19/(int32_t)L_20));
		RenderTexture_t1963041563 * L_21 = ___source0;
		NullCheck(L_21);
		int32_t L_22 = RenderTexture_get_height_m4010076224(L_21, /*hidden argument*/NULL);
		int32_t L_23 = V_0;
		V_3 = ((int32_t)((int32_t)L_22/(int32_t)L_23));
		V_4 = (RenderTexture_t1963041563 *)NULL;
		int32_t L_24 = V_2;
		int32_t L_25 = V_3;
		RenderTexture_t1963041563 * L_26 = RenderTexture_GetTemporary_m52998487(NULL /*static, unused*/, L_24, L_25, 0, /*hidden argument*/NULL);
		V_5 = L_26;
		Material_t3870600107 * L_27 = __this->get_sunShaftsMaterial_16();
		Vector4_t4282066567  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Vector4__ctor_m2441427762(&L_28, (1.0f), (1.0f), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		float L_29 = __this->get_sunShaftBlurRadius_10();
		Vector4_t4282066567  L_30 = Vector4_op_Multiply_m209031836(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		NullCheck(L_27);
		Material_SetVector_m3505096203(L_27, _stringLiteral4016218524, L_30, /*hidden argument*/NULL);
		Material_t3870600107 * L_31 = __this->get_sunShaftsMaterial_16();
		float L_32 = (&V_1)->get_x_1();
		float L_33 = (&V_1)->get_y_2();
		float L_34 = (&V_1)->get_z_3();
		float L_35 = __this->get_maxRadius_13();
		Vector4_t4282066567  L_36;
		memset(&L_36, 0, sizeof(L_36));
		Vector4__ctor_m2441427762(&L_36, L_32, L_33, L_34, L_35, /*hidden argument*/NULL);
		NullCheck(L_31);
		Material_SetVector_m3505096203(L_31, _stringLiteral2893526454, L_36, /*hidden argument*/NULL);
		Material_t3870600107 * L_37 = __this->get_sunShaftsMaterial_16();
		float L_38 = __this->get_useSkyBoxAlpha_12();
		NullCheck(L_37);
		Material_SetFloat_m981710063(L_37, _stringLiteral390482326, ((float)((float)(1.0f)-(float)L_38)), /*hidden argument*/NULL);
		bool L_39 = __this->get_useDepthTexture_14();
		if (L_39)
		{
			goto IL_01a3;
		}
	}
	{
		RenderTexture_t1963041563 * L_40 = ___source0;
		NullCheck(L_40);
		int32_t L_41 = RenderTexture_get_width_m1498578543(L_40, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_42 = ___source0;
		NullCheck(L_42);
		int32_t L_43 = RenderTexture_get_height_m4010076224(L_42, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_44 = RenderTexture_GetTemporary_m52998487(NULL /*static, unused*/, L_41, L_43, 0, /*hidden argument*/NULL);
		V_6 = L_44;
		RenderTexture_t1963041563 * L_45 = V_6;
		RenderTexture_set_active_m1002947377(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		Camera_t2727095145 * L_46 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		GL_ClearWithSkybox_m2059598896(NULL /*static, unused*/, (bool)0, L_46, /*hidden argument*/NULL);
		Material_t3870600107 * L_47 = __this->get_sunShaftsMaterial_16();
		RenderTexture_t1963041563 * L_48 = V_6;
		NullCheck(L_47);
		Material_SetTexture_m1833724755(L_47, _stringLiteral892242505, L_48, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_49 = ___source0;
		RenderTexture_t1963041563 * L_50 = V_5;
		Material_t3870600107 * L_51 = __this->get_sunShaftsMaterial_16();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_49, L_50, L_51, 3, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_52 = V_6;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		goto IL_01b2;
	}

IL_01a3:
	{
		RenderTexture_t1963041563 * L_53 = ___source0;
		RenderTexture_t1963041563 * L_54 = V_5;
		Material_t3870600107 * L_55 = __this->get_sunShaftsMaterial_16();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_53, L_54, L_55, 2, /*hidden argument*/NULL);
	}

IL_01b2:
	{
		RenderTexture_t1963041563 * L_56 = V_5;
		Material_t3870600107 * L_57 = __this->get_simpleClearMaterial_18();
		VirtActionInvoker2< RenderTexture_t1963041563 *, Material_t3870600107 * >::Invoke(16 /* System.Void PostEffectsBase::DrawBorder(UnityEngine.RenderTexture,UnityEngine.Material) */, __this, L_56, L_57);
		int32_t L_58 = __this->get_radialBlurIterations_8();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_59 = Mathf_Clamp_m510460741(NULL /*static, unused*/, L_58, 1, 4, /*hidden argument*/NULL);
		__this->set_radialBlurIterations_8(L_59);
		float L_60 = __this->get_sunShaftBlurRadius_10();
		V_7 = ((float)((float)L_60*(float)(0.00130208337f)));
		Material_t3870600107 * L_61 = __this->get_sunShaftsMaterial_16();
		float L_62 = V_7;
		float L_63 = V_7;
		Vector4_t4282066567  L_64;
		memset(&L_64, 0, sizeof(L_64));
		Vector4__ctor_m2441427762(&L_64, L_62, L_63, (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_61);
		Material_SetVector_m3505096203(L_61, _stringLiteral4016218524, L_64, /*hidden argument*/NULL);
		Material_t3870600107 * L_65 = __this->get_sunShaftsMaterial_16();
		float L_66 = (&V_1)->get_x_1();
		float L_67 = (&V_1)->get_y_2();
		float L_68 = (&V_1)->get_z_3();
		float L_69 = __this->get_maxRadius_13();
		Vector4_t4282066567  L_70;
		memset(&L_70, 0, sizeof(L_70));
		Vector4__ctor_m2441427762(&L_70, L_66, L_67, L_68, L_69, /*hidden argument*/NULL);
		NullCheck(L_65);
		Material_SetVector_m3505096203(L_65, _stringLiteral2893526454, L_70, /*hidden argument*/NULL);
		V_8 = 0;
		goto IL_0300;
	}

IL_0236:
	{
		int32_t L_71 = V_2;
		int32_t L_72 = V_3;
		RenderTexture_t1963041563 * L_73 = RenderTexture_GetTemporary_m52998487(NULL /*static, unused*/, L_71, L_72, 0, /*hidden argument*/NULL);
		V_4 = L_73;
		RenderTexture_t1963041563 * L_74 = V_5;
		RenderTexture_t1963041563 * L_75 = V_4;
		Material_t3870600107 * L_76 = __this->get_sunShaftsMaterial_16();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_74, L_75, L_76, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_77 = V_5;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_77, /*hidden argument*/NULL);
		float L_78 = __this->get_sunShaftBlurRadius_10();
		int32_t L_79 = V_8;
		V_7 = ((float)((float)((float)((float)L_78*(float)((float)((float)((float)((float)((float)((float)(((float)((float)L_79)))*(float)(2.0f)))+(float)(1.0f)))*(float)(6.0f)))))/(float)(768.0f)));
		Material_t3870600107 * L_80 = __this->get_sunShaftsMaterial_16();
		float L_81 = V_7;
		float L_82 = V_7;
		Vector4_t4282066567  L_83;
		memset(&L_83, 0, sizeof(L_83));
		Vector4__ctor_m2441427762(&L_83, L_81, L_82, (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_80);
		Material_SetVector_m3505096203(L_80, _stringLiteral4016218524, L_83, /*hidden argument*/NULL);
		int32_t L_84 = V_2;
		int32_t L_85 = V_3;
		RenderTexture_t1963041563 * L_86 = RenderTexture_GetTemporary_m52998487(NULL /*static, unused*/, L_84, L_85, 0, /*hidden argument*/NULL);
		V_5 = L_86;
		RenderTexture_t1963041563 * L_87 = V_4;
		RenderTexture_t1963041563 * L_88 = V_5;
		Material_t3870600107 * L_89 = __this->get_sunShaftsMaterial_16();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_87, L_88, L_89, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_90 = V_4;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_90, /*hidden argument*/NULL);
		float L_91 = __this->get_sunShaftBlurRadius_10();
		int32_t L_92 = V_8;
		V_7 = ((float)((float)((float)((float)L_91*(float)((float)((float)((float)((float)((float)((float)(((float)((float)L_92)))*(float)(2.0f)))+(float)(2.0f)))*(float)(6.0f)))))/(float)(768.0f)));
		Material_t3870600107 * L_93 = __this->get_sunShaftsMaterial_16();
		float L_94 = V_7;
		float L_95 = V_7;
		Vector4_t4282066567  L_96;
		memset(&L_96, 0, sizeof(L_96));
		Vector4__ctor_m2441427762(&L_96, L_94, L_95, (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_93);
		Material_SetVector_m3505096203(L_93, _stringLiteral4016218524, L_96, /*hidden argument*/NULL);
		int32_t L_97 = V_8;
		V_8 = ((int32_t)((int32_t)L_97+(int32_t)1));
	}

IL_0300:
	{
		int32_t L_98 = V_8;
		int32_t L_99 = __this->get_radialBlurIterations_8();
		if ((((int32_t)L_98) < ((int32_t)L_99)))
		{
			goto IL_0236;
		}
	}
	{
		float L_100 = (&V_1)->get_z_3();
		if ((((float)L_100) < ((float)(((float)((float)0))))))
		{
			goto IL_036c;
		}
	}
	{
		Material_t3870600107 * L_101 = __this->get_sunShaftsMaterial_16();
		Color_t4194546905 * L_102 = __this->get_address_of_sunColor_9();
		float L_103 = L_102->get_r_0();
		Color_t4194546905 * L_104 = __this->get_address_of_sunColor_9();
		float L_105 = L_104->get_g_1();
		Color_t4194546905 * L_106 = __this->get_address_of_sunColor_9();
		float L_107 = L_106->get_b_2();
		Color_t4194546905 * L_108 = __this->get_address_of_sunColor_9();
		float L_109 = L_108->get_a_3();
		Vector4_t4282066567  L_110;
		memset(&L_110, 0, sizeof(L_110));
		Vector4__ctor_m2441427762(&L_110, L_103, L_105, L_107, L_109, /*hidden argument*/NULL);
		float L_111 = __this->get_sunShaftIntensity_11();
		Vector4_t4282066567  L_112 = Vector4_op_Multiply_m209031836(NULL /*static, unused*/, L_110, L_111, /*hidden argument*/NULL);
		NullCheck(L_101);
		Material_SetVector_m3505096203(L_101, _stringLiteral2688099830, L_112, /*hidden argument*/NULL);
		goto IL_0381;
	}

IL_036c:
	{
		Material_t3870600107 * L_113 = __this->get_sunShaftsMaterial_16();
		Vector4_t4282066567  L_114 = Vector4_get_zero_m3835647092(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_113);
		Material_SetVector_m3505096203(L_113, _stringLiteral2688099830, L_114, /*hidden argument*/NULL);
	}

IL_0381:
	{
		Material_t3870600107 * L_115 = __this->get_sunShaftsMaterial_16();
		RenderTexture_t1963041563 * L_116 = V_5;
		NullCheck(L_115);
		Material_SetTexture_m1833724755(L_115, _stringLiteral1541889956, L_116, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_117 = ___source0;
		RenderTexture_t1963041563 * L_118 = ___destination1;
		Material_t3870600107 * L_119 = __this->get_sunShaftsMaterial_16();
		int32_t L_120 = __this->get_screenBlendMode_6();
		G_B21_0 = L_119;
		G_B21_1 = L_118;
		G_B21_2 = L_117;
		if ((!(((uint32_t)L_120) == ((uint32_t)0))))
		{
			G_B22_0 = L_119;
			G_B22_1 = L_118;
			G_B22_2 = L_117;
			goto IL_03ad;
		}
	}
	{
		G_B23_0 = 0;
		G_B23_1 = G_B21_0;
		G_B23_2 = G_B21_1;
		G_B23_3 = G_B21_2;
		goto IL_03ae;
	}

IL_03ad:
	{
		G_B23_0 = 4;
		G_B23_1 = G_B22_0;
		G_B23_2 = G_B22_1;
		G_B23_3 = G_B22_2;
	}

IL_03ae:
	{
		Graphics_Blit_m336256356(NULL /*static, unused*/, G_B23_3, G_B23_2, G_B23_1, G_B23_0, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_121 = V_5;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_121, /*hidden argument*/NULL);
	}

IL_03ba:
	{
		return;
	}
}
// System.Void SunShafts::Main()
extern "C"  void SunShafts_Main_m3029071072 (SunShafts_t3666895493 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void TiltShiftHdr::.ctor()
extern "C"  void TiltShiftHdr__ctor_m3150881171 (TiltShiftHdr_t3131000081 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase__ctor_m3869393199(__this, /*hidden argument*/NULL);
		__this->set_mode_5(0);
		__this->set_quality_6(1);
		__this->set_blurArea_7((1.0f));
		__this->set_maxBlurSize_8((5.0f));
		return;
	}
}
// System.Boolean TiltShiftHdr::CheckResources()
extern "C"  bool TiltShiftHdr_CheckResources_m4228207092 (TiltShiftHdr_t3131000081 * __this, const MethodInfo* method)
{
	{
		VirtFuncInvoker1< bool, bool >::Invoke(10 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean) */, __this, (bool)1);
		Shader_t3191267369 * L_0 = __this->get_tiltShiftShader_10();
		Material_t3870600107 * L_1 = __this->get_tiltShiftMaterial_11();
		Material_t3870600107 * L_2 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_0, L_1);
		__this->set_tiltShiftMaterial_11(L_2);
		bool L_3 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		if (L_3)
		{
			goto IL_0031;
		}
	}
	{
		VirtActionInvoker0::Invoke(13 /* System.Void PostEffectsBase::ReportAutoDisable() */, __this);
	}

IL_0031:
	{
		bool L_4 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		return L_4;
	}
}
// System.Void TiltShiftHdr::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral3970469511;
extern Il2CppCodeGenString* _stringLiteral3969941267;
extern Il2CppCodeGenString* _stringLiteral4007434603;
extern const uint32_t TiltShiftHdr_OnRenderImage_m1299940075_MetadataUsageId;
extern "C"  void TiltShiftHdr_OnRenderImage_m1299940075 (TiltShiftHdr_t3131000081 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TiltShiftHdr_OnRenderImage_m1299940075_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RenderTexture_t1963041563 * V_0 = NULL;
	int32_t V_1 = 0;
	String_t* G_B4_0 = NULL;
	Material_t3870600107 * G_B4_1 = NULL;
	String_t* G_B3_0 = NULL;
	Material_t3870600107 * G_B3_1 = NULL;
	float G_B5_0 = 0.0f;
	String_t* G_B5_1 = NULL;
	Material_t3870600107 * G_B5_2 = NULL;
	Material_t3870600107 * G_B9_0 = NULL;
	RenderTexture_t1963041563 * G_B9_1 = NULL;
	RenderTexture_t1963041563 * G_B9_2 = NULL;
	Material_t3870600107 * G_B8_0 = NULL;
	RenderTexture_t1963041563 * G_B8_1 = NULL;
	RenderTexture_t1963041563 * G_B8_2 = NULL;
	int32_t G_B10_0 = 0;
	Material_t3870600107 * G_B10_1 = NULL;
	RenderTexture_t1963041563 * G_B10_2 = NULL;
	RenderTexture_t1963041563 * G_B10_3 = NULL;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean TiltShiftHdr::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		RenderTexture_t1963041563 * L_1 = ___source0;
		RenderTexture_t1963041563 * L_2 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_0102;
	}

IL_0017:
	{
		Material_t3870600107 * L_3 = __this->get_tiltShiftMaterial_11();
		float L_4 = __this->get_maxBlurSize_8();
		G_B3_0 = _stringLiteral3970469511;
		G_B3_1 = L_3;
		if ((((float)L_4) >= ((float)(((float)((float)0))))))
		{
			G_B4_0 = _stringLiteral3970469511;
			G_B4_1 = L_3;
			goto IL_0036;
		}
	}
	{
		G_B5_0 = (((float)((float)0)));
		G_B5_1 = G_B3_0;
		G_B5_2 = G_B3_1;
		goto IL_003c;
	}

IL_0036:
	{
		float L_5 = __this->get_maxBlurSize_8();
		G_B5_0 = L_5;
		G_B5_1 = G_B4_0;
		G_B5_2 = G_B4_1;
	}

IL_003c:
	{
		NullCheck(G_B5_2);
		Material_SetFloat_m981710063(G_B5_2, G_B5_1, G_B5_0, /*hidden argument*/NULL);
		Material_t3870600107 * L_6 = __this->get_tiltShiftMaterial_11();
		float L_7 = __this->get_blurArea_7();
		NullCheck(L_6);
		Material_SetFloat_m981710063(L_6, _stringLiteral3969941267, L_7, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_8 = ___source0;
		NullCheck(L_8);
		Texture_set_filterMode_m3842701708(L_8, 1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_9 = ___destination1;
		V_0 = L_9;
		int32_t L_10 = __this->get_downsample_9();
		if (!L_10)
		{
			goto IL_0099;
		}
	}
	{
		RenderTexture_t1963041563 * L_11 = ___source0;
		NullCheck(L_11);
		int32_t L_12 = RenderTexture_get_width_m1498578543(L_11, /*hidden argument*/NULL);
		int32_t L_13 = __this->get_downsample_9();
		RenderTexture_t1963041563 * L_14 = ___source0;
		NullCheck(L_14);
		int32_t L_15 = RenderTexture_get_height_m4010076224(L_14, /*hidden argument*/NULL);
		int32_t L_16 = __this->get_downsample_9();
		RenderTexture_t1963041563 * L_17 = ___source0;
		NullCheck(L_17);
		int32_t L_18 = RenderTexture_get_format_m3502109954(L_17, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_19 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, ((int32_t)((int32_t)L_12>>(int32_t)L_13)), ((int32_t)((int32_t)L_15>>(int32_t)L_16)), 0, L_18, /*hidden argument*/NULL);
		V_0 = L_19;
		RenderTexture_t1963041563 * L_20 = V_0;
		NullCheck(L_20);
		Texture_set_filterMode_m3842701708(L_20, 1, /*hidden argument*/NULL);
	}

IL_0099:
	{
		int32_t L_21 = __this->get_quality_6();
		V_1 = L_21;
		int32_t L_22 = V_1;
		V_1 = ((int32_t)((int32_t)L_22*(int32_t)2));
		RenderTexture_t1963041563 * L_23 = ___source0;
		RenderTexture_t1963041563 * L_24 = V_0;
		Material_t3870600107 * L_25 = __this->get_tiltShiftMaterial_11();
		int32_t L_26 = __this->get_mode_5();
		G_B8_0 = L_25;
		G_B8_1 = L_24;
		G_B8_2 = L_23;
		if ((!(((uint32_t)L_26) == ((uint32_t)0))))
		{
			G_B9_0 = L_25;
			G_B9_1 = L_24;
			G_B9_2 = L_23;
			goto IL_00be;
		}
	}
	{
		int32_t L_27 = V_1;
		G_B10_0 = L_27;
		G_B10_1 = G_B8_0;
		G_B10_2 = G_B8_1;
		G_B10_3 = G_B8_2;
		goto IL_00c1;
	}

IL_00be:
	{
		int32_t L_28 = V_1;
		G_B10_0 = ((int32_t)((int32_t)L_28+(int32_t)1));
		G_B10_1 = G_B9_0;
		G_B10_2 = G_B9_1;
		G_B10_3 = G_B9_2;
	}

IL_00c1:
	{
		Graphics_Blit_m336256356(NULL /*static, unused*/, G_B10_3, G_B10_2, G_B10_1, G_B10_0, /*hidden argument*/NULL);
		int32_t L_29 = __this->get_downsample_9();
		if (!L_29)
		{
			goto IL_00f0;
		}
	}
	{
		Material_t3870600107 * L_30 = __this->get_tiltShiftMaterial_11();
		RenderTexture_t1963041563 * L_31 = V_0;
		NullCheck(L_30);
		Material_SetTexture_m1833724755(L_30, _stringLiteral4007434603, L_31, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_32 = ___source0;
		RenderTexture_t1963041563 * L_33 = ___destination1;
		Material_t3870600107 * L_34 = __this->get_tiltShiftMaterial_11();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_32, L_33, L_34, 6, /*hidden argument*/NULL);
	}

IL_00f0:
	{
		RenderTexture_t1963041563 * L_35 = V_0;
		RenderTexture_t1963041563 * L_36 = ___destination1;
		bool L_37 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0102;
		}
	}
	{
		RenderTexture_t1963041563 * L_38 = V_0;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
	}

IL_0102:
	{
		return;
	}
}
// System.Void TiltShiftHdr::Main()
extern "C"  void TiltShiftHdr_Main_m155683018 (TiltShiftHdr_t3131000081 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Tonemapping::.ctor()
extern "C"  void Tonemapping__ctor_m2071450086 (Tonemapping_t2852991740 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase__ctor_m3869393199(__this, /*hidden argument*/NULL);
		__this->set_type_5(3);
		__this->set_adaptiveTextureSize_6(((int32_t)256));
		__this->set_exposureAdjustment_9((1.5f));
		__this->set_middleGrey_10((0.4f));
		__this->set_white_11((2.0f));
		__this->set_adaptionSpeed_12((1.5f));
		__this->set_validRenderTextureFormat_14((bool)1);
		__this->set_rtFormat_17(2);
		return;
	}
}
// System.Boolean Tonemapping::CheckResources()
extern Il2CppClass* Texture2D_t3884108195_il2cpp_TypeInfo_var;
extern const uint32_t Tonemapping_CheckResources_m1355503221_MetadataUsageId;
extern "C"  bool Tonemapping_CheckResources_m1355503221 (Tonemapping_t2852991740 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Tonemapping_CheckResources_m1355503221_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		VirtFuncInvoker2< bool, bool, bool >::Invoke(11 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean,System.Boolean) */, __this, (bool)0, (bool)1);
		Shader_t3191267369 * L_0 = __this->get_tonemapper_13();
		Material_t3870600107 * L_1 = __this->get_tonemapMaterial_15();
		Material_t3870600107 * L_2 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_0, L_1);
		__this->set_tonemapMaterial_15(L_2);
		Texture2D_t3884108195 * L_3 = __this->get_curveTex_8();
		bool L_4 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0076;
		}
	}
	{
		int32_t L_5 = __this->get_type_5();
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_0076;
		}
	}
	{
		Texture2D_t3884108195 * L_6 = (Texture2D_t3884108195 *)il2cpp_codegen_object_new(Texture2D_t3884108195_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1918985003(L_6, ((int32_t)256), 1, 5, (bool)0, (bool)1, /*hidden argument*/NULL);
		__this->set_curveTex_8(L_6);
		Texture2D_t3884108195 * L_7 = __this->get_curveTex_8();
		NullCheck(L_7);
		Texture_set_filterMode_m3842701708(L_7, 1, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_8 = __this->get_curveTex_8();
		NullCheck(L_8);
		Texture_set_wrapMode_m3720633937(L_8, 1, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_9 = __this->get_curveTex_8();
		NullCheck(L_9);
		Object_set_hideFlags_m41317712(L_9, ((int32_t)52), /*hidden argument*/NULL);
	}

IL_0076:
	{
		bool L_10 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		if (L_10)
		{
			goto IL_0087;
		}
	}
	{
		VirtActionInvoker0::Invoke(13 /* System.Void PostEffectsBase::ReportAutoDisable() */, __this);
	}

IL_0087:
	{
		bool L_11 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		return L_11;
	}
}
// System.Single Tonemapping::UpdateCurve()
extern Il2CppClass* KeyframeU5BU5D_t3589549831_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationCurve_t3667593487_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Tonemapping_UpdateCurve_m1290119414_MetadataUsageId;
extern "C"  float Tonemapping_UpdateCurve_m1290119414 (Tonemapping_t2852991740 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Tonemapping_UpdateCurve_m1290119414_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Keyframe_t4079056114  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		V_0 = (1.0f);
		AnimationCurve_t3667593487 * L_0 = __this->get_remapCurve_7();
		NullCheck(L_0);
		KeyframeU5BU5D_t3589549831* L_1 = AnimationCurve_get_keys_m450535207(L_0, /*hidden argument*/NULL);
		int32_t L_2 = Extensions_get_length_m1049594766(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) >= ((int32_t)1)))
		{
			goto IL_0057;
		}
	}
	{
		KeyframeU5BU5D_t3589549831* L_3 = ((KeyframeU5BU5D_t3589549831*)SZArrayNew(KeyframeU5BU5D_t3589549831_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		Keyframe_t4079056114  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Keyframe__ctor_m2655645489(&L_4, (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		(*(Keyframe_t4079056114 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_4;
		KeyframeU5BU5D_t3589549831* L_5 = L_3;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		Keyframe_t4079056114  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Keyframe__ctor_m2655645489(&L_6, (((float)((float)2))), (((float)((float)1))), /*hidden argument*/NULL);
		(*(Keyframe_t4079056114 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_6;
		AnimationCurve_t3667593487 * L_7 = (AnimationCurve_t3667593487 *)il2cpp_codegen_object_new(AnimationCurve_t3667593487_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m2436282331(L_7, L_5, /*hidden argument*/NULL);
		__this->set_remapCurve_7(L_7);
	}

IL_0057:
	{
		AnimationCurve_t3667593487 * L_8 = __this->get_remapCurve_7();
		if (!L_8)
		{
			goto IL_00ef;
		}
	}
	{
		AnimationCurve_t3667593487 * L_9 = __this->get_remapCurve_7();
		NullCheck(L_9);
		int32_t L_10 = AnimationCurve_get_length_m3019229777(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0093;
		}
	}
	{
		AnimationCurve_t3667593487 * L_11 = __this->get_remapCurve_7();
		AnimationCurve_t3667593487 * L_12 = __this->get_remapCurve_7();
		NullCheck(L_12);
		int32_t L_13 = AnimationCurve_get_length_m3019229777(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		Keyframe_t4079056114  L_14 = AnimationCurve_get_Item_m2285797849(L_11, ((int32_t)((int32_t)L_13-(int32_t)1)), /*hidden argument*/NULL);
		V_3 = L_14;
		float L_15 = Keyframe_get_time_m1367974951((&V_3), /*hidden argument*/NULL);
		V_0 = L_15;
	}

IL_0093:
	{
		V_1 = (((float)((float)0)));
		goto IL_00d9;
	}

IL_009b:
	{
		AnimationCurve_t3667593487 * L_16 = __this->get_remapCurve_7();
		float L_17 = V_1;
		float L_18 = V_0;
		NullCheck(L_16);
		float L_19 = AnimationCurve_Evaluate_m547727012(L_16, ((float)((float)((float)((float)L_17*(float)(1.0f)))*(float)L_18)), /*hidden argument*/NULL);
		V_2 = L_19;
		Texture2D_t3884108195 * L_20 = __this->get_curveTex_8();
		float L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_22 = floorf(((float)((float)L_21*(float)(255.0f))));
		float L_23 = V_2;
		float L_24 = V_2;
		float L_25 = V_2;
		Color_t4194546905  L_26;
		memset(&L_26, 0, sizeof(L_26));
		Color__ctor_m103496991(&L_26, L_23, L_24, L_25, /*hidden argument*/NULL);
		NullCheck(L_20);
		Texture2D_SetPixel_m378278602(L_20, (((int32_t)((int32_t)L_22))), 0, L_26, /*hidden argument*/NULL);
		float L_27 = V_1;
		V_1 = ((float)((float)L_27+(float)(0.003921569f)));
	}

IL_00d9:
	{
		float L_28 = V_1;
		if ((((float)L_28) <= ((float)(1.0f))))
		{
			goto IL_009b;
		}
	}
	{
		Texture2D_t3884108195 * L_29 = __this->get_curveTex_8();
		NullCheck(L_29);
		Texture2D_Apply_m1364130776(L_29, /*hidden argument*/NULL);
	}

IL_00ef:
	{
		float L_30 = V_0;
		return ((float)((float)(1.0f)/(float)L_30));
	}
}
// System.Void Tonemapping::OnDisable()
extern "C"  void Tonemapping_OnDisable_m2211955533 (Tonemapping_t2852991740 * __this, const MethodInfo* method)
{
	{
		RenderTexture_t1963041563 * L_0 = __this->get_rt_16();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		RenderTexture_t1963041563 * L_2 = __this->get_rt_16();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_rt_16((RenderTexture_t1963041563 *)NULL);
	}

IL_0022:
	{
		Material_t3870600107 * L_3 = __this->get_tonemapMaterial_15();
		bool L_4 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0044;
		}
	}
	{
		Material_t3870600107 * L_5 = __this->get_tonemapMaterial_15();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->set_tonemapMaterial_15((Material_t3870600107 *)NULL);
	}

IL_0044:
	{
		Texture2D_t3884108195 * L_6 = __this->get_curveTex_8();
		bool L_7 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0066;
		}
	}
	{
		Texture2D_t3884108195 * L_8 = __this->get_curveTex_8();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		__this->set_curveTex_8((Texture2D_t3884108195 *)NULL);
	}

IL_0066:
	{
		return;
	}
}
// System.Boolean Tonemapping::CreateInternalRenderTexture()
extern Il2CppClass* RenderTexture_t1963041563_il2cpp_TypeInfo_var;
extern const uint32_t Tonemapping_CreateInternalRenderTexture_m72730614_MetadataUsageId;
extern "C"  bool Tonemapping_CreateInternalRenderTexture_m72730614 (Tonemapping_t2852991740 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Tonemapping_CreateInternalRenderTexture_m72730614_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B6_0 = 0;
	Tonemapping_t2852991740 * G_B4_0 = NULL;
	Tonemapping_t2852991740 * G_B3_0 = NULL;
	int32_t G_B5_0 = 0;
	Tonemapping_t2852991740 * G_B5_1 = NULL;
	{
		RenderTexture_t1963041563 * L_0 = __this->get_rt_16();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		G_B6_0 = 0;
		goto IL_0052;
	}

IL_0016:
	{
		bool L_2 = SystemInfo_SupportsRenderTextureFormat_m1773213581(NULL /*static, unused*/, ((int32_t)13), /*hidden argument*/NULL);
		G_B3_0 = __this;
		if (!L_2)
		{
			G_B4_0 = __this;
			goto IL_002a;
		}
	}
	{
		G_B5_0 = ((int32_t)13);
		G_B5_1 = G_B3_0;
		goto IL_002b;
	}

IL_002a:
	{
		G_B5_0 = 2;
		G_B5_1 = G_B4_0;
	}

IL_002b:
	{
		NullCheck(G_B5_1);
		G_B5_1->set_rtFormat_17(G_B5_0);
		int32_t L_3 = __this->get_rtFormat_17();
		RenderTexture_t1963041563 * L_4 = (RenderTexture_t1963041563 *)il2cpp_codegen_object_new(RenderTexture_t1963041563_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m2927948204(L_4, 1, 1, 0, L_3, /*hidden argument*/NULL);
		__this->set_rt_16(L_4);
		RenderTexture_t1963041563 * L_5 = __this->get_rt_16();
		NullCheck(L_5);
		Object_set_hideFlags_m41317712(L_5, ((int32_t)52), /*hidden argument*/NULL);
		G_B6_0 = 1;
	}

IL_0052:
	{
		return (bool)G_B6_0;
	}
}
// System.Void Tonemapping::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppClass* RenderTextureU5BU5D_t1576771098_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1255100364;
extern Il2CppCodeGenString* _stringLiteral2785244112;
extern Il2CppCodeGenString* _stringLiteral1403251859;
extern Il2CppCodeGenString* _stringLiteral661443618;
extern Il2CppCodeGenString* _stringLiteral185336445;
extern Il2CppCodeGenString* _stringLiteral3843577631;
extern Il2CppCodeGenString* _stringLiteral3851645919;
extern const uint32_t Tonemapping_OnRenderImage_m428819128_MetadataUsageId;
extern "C"  void Tonemapping_OnRenderImage_m428819128 (Tonemapping_t2852991740 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Tonemapping_OnRenderImage_m428819128_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	RenderTexture_t1963041563 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	RenderTextureU5BU5D_t1576771098* V_5 = NULL;
	int32_t V_6 = 0;
	float V_7 = 0.0f;
	RenderTexture_t1963041563 * V_8 = NULL;
	Tonemapping_t2852991740 * G_B4_0 = NULL;
	Tonemapping_t2852991740 * G_B3_0 = NULL;
	float G_B5_0 = 0.0f;
	Tonemapping_t2852991740 * G_B5_1 = NULL;
	Tonemapping_t2852991740 * G_B29_0 = NULL;
	Tonemapping_t2852991740 * G_B28_0 = NULL;
	float G_B30_0 = 0.0f;
	Tonemapping_t2852991740 * G_B30_1 = NULL;
	Material_t3870600107 * G_B32_0 = NULL;
	RenderTexture_t1963041563 * G_B32_1 = NULL;
	RenderTexture_t1963041563 * G_B32_2 = NULL;
	Material_t3870600107 * G_B31_0 = NULL;
	RenderTexture_t1963041563 * G_B31_1 = NULL;
	RenderTexture_t1963041563 * G_B31_2 = NULL;
	int32_t G_B33_0 = 0;
	Material_t3870600107 * G_B33_1 = NULL;
	RenderTexture_t1963041563 * G_B33_2 = NULL;
	RenderTexture_t1963041563 * G_B33_3 = NULL;
	Tonemapping_t2852991740 * G_B35_0 = NULL;
	Tonemapping_t2852991740 * G_B34_0 = NULL;
	float G_B36_0 = 0.0f;
	Tonemapping_t2852991740 * G_B36_1 = NULL;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Tonemapping::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		RenderTexture_t1963041563 * L_1 = ___source0;
		RenderTexture_t1963041563 * L_2 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_03f7;
	}

IL_0017:
	{
		float L_3 = __this->get_exposureAdjustment_9();
		G_B3_0 = __this;
		if ((((float)L_3) >= ((float)(0.001f))))
		{
			G_B4_0 = __this;
			goto IL_0032;
		}
	}
	{
		G_B5_0 = (0.001f);
		G_B5_1 = G_B3_0;
		goto IL_0038;
	}

IL_0032:
	{
		float L_4 = __this->get_exposureAdjustment_9();
		G_B5_0 = L_4;
		G_B5_1 = G_B4_0;
	}

IL_0038:
	{
		NullCheck(G_B5_1);
		G_B5_1->set_exposureAdjustment_9(G_B5_0);
		int32_t L_5 = __this->get_type_5();
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_008a;
		}
	}
	{
		float L_6 = VirtFuncInvoker0< float >::Invoke(18 /* System.Single Tonemapping::UpdateCurve() */, __this);
		V_0 = L_6;
		Material_t3870600107 * L_7 = __this->get_tonemapMaterial_15();
		float L_8 = V_0;
		NullCheck(L_7);
		Material_SetFloat_m981710063(L_7, _stringLiteral1255100364, L_8, /*hidden argument*/NULL);
		Material_t3870600107 * L_9 = __this->get_tonemapMaterial_15();
		Texture2D_t3884108195 * L_10 = __this->get_curveTex_8();
		NullCheck(L_9);
		Material_SetTexture_m1833724755(L_9, _stringLiteral2785244112, L_10, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_11 = ___source0;
		RenderTexture_t1963041563 * L_12 = ___destination1;
		Material_t3870600107 * L_13 = __this->get_tonemapMaterial_15();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_11, L_12, L_13, 4, /*hidden argument*/NULL);
		goto IL_03f7;
	}

IL_008a:
	{
		int32_t L_14 = __this->get_type_5();
		if ((!(((uint32_t)L_14) == ((uint32_t)0))))
		{
			goto IL_00bf;
		}
	}
	{
		Material_t3870600107 * L_15 = __this->get_tonemapMaterial_15();
		float L_16 = __this->get_exposureAdjustment_9();
		NullCheck(L_15);
		Material_SetFloat_m981710063(L_15, _stringLiteral1403251859, L_16, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_17 = ___source0;
		RenderTexture_t1963041563 * L_18 = ___destination1;
		Material_t3870600107 * L_19 = __this->get_tonemapMaterial_15();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_17, L_18, L_19, 6, /*hidden argument*/NULL);
		goto IL_03f7;
	}

IL_00bf:
	{
		int32_t L_20 = __this->get_type_5();
		if ((!(((uint32_t)L_20) == ((uint32_t)2))))
		{
			goto IL_00f4;
		}
	}
	{
		Material_t3870600107 * L_21 = __this->get_tonemapMaterial_15();
		float L_22 = __this->get_exposureAdjustment_9();
		NullCheck(L_21);
		Material_SetFloat_m981710063(L_21, _stringLiteral1403251859, L_22, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_23 = ___source0;
		RenderTexture_t1963041563 * L_24 = ___destination1;
		Material_t3870600107 * L_25 = __this->get_tonemapMaterial_15();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_23, L_24, L_25, 5, /*hidden argument*/NULL);
		goto IL_03f7;
	}

IL_00f4:
	{
		int32_t L_26 = __this->get_type_5();
		if ((!(((uint32_t)L_26) == ((uint32_t)3))))
		{
			goto IL_0129;
		}
	}
	{
		Material_t3870600107 * L_27 = __this->get_tonemapMaterial_15();
		float L_28 = __this->get_exposureAdjustment_9();
		NullCheck(L_27);
		Material_SetFloat_m981710063(L_27, _stringLiteral1403251859, L_28, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_29 = ___source0;
		RenderTexture_t1963041563 * L_30 = ___destination1;
		Material_t3870600107 * L_31 = __this->get_tonemapMaterial_15();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_29, L_30, L_31, 8, /*hidden argument*/NULL);
		goto IL_03f7;
	}

IL_0129:
	{
		int32_t L_32 = __this->get_type_5();
		if ((!(((uint32_t)L_32) == ((uint32_t)4))))
		{
			goto IL_0164;
		}
	}
	{
		Material_t3870600107 * L_33 = __this->get_tonemapMaterial_15();
		float L_34 = __this->get_exposureAdjustment_9();
		NullCheck(L_33);
		Material_SetFloat_m981710063(L_33, _stringLiteral1403251859, ((float)((float)(0.5f)*(float)L_34)), /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_35 = ___source0;
		RenderTexture_t1963041563 * L_36 = ___destination1;
		Material_t3870600107 * L_37 = __this->get_tonemapMaterial_15();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_35, L_36, L_37, 7, /*hidden argument*/NULL);
		goto IL_03f7;
	}

IL_0164:
	{
		bool L_38 = VirtFuncInvoker0< bool >::Invoke(20 /* System.Boolean Tonemapping::CreateInternalRenderTexture() */, __this);
		V_1 = L_38;
		int32_t L_39 = __this->get_adaptiveTextureSize_6();
		int32_t L_40 = __this->get_adaptiveTextureSize_6();
		int32_t L_41 = __this->get_rtFormat_17();
		RenderTexture_t1963041563 * L_42 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, L_39, L_40, 0, L_41, /*hidden argument*/NULL);
		V_2 = L_42;
		RenderTexture_t1963041563 * L_43 = ___source0;
		RenderTexture_t1963041563 * L_44 = V_2;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_45 = V_2;
		NullCheck(L_45);
		int32_t L_46 = RenderTexture_get_width_m1498578543(L_45, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_47 = Mathf_Log_m2062790663(NULL /*static, unused*/, ((float)((float)(((float)((float)L_46)))*(float)(1.0f))), (((float)((float)2))), /*hidden argument*/NULL);
		V_3 = (((int32_t)((int32_t)L_47)));
		V_4 = 2;
		int32_t L_48 = V_3;
		V_5 = ((RenderTextureU5BU5D_t1576771098*)SZArrayNew(RenderTextureU5BU5D_t1576771098_il2cpp_TypeInfo_var, (uint32_t)L_48));
		V_6 = 0;
		goto IL_01e3;
	}

IL_01b4:
	{
		RenderTextureU5BU5D_t1576771098* L_49 = V_5;
		int32_t L_50 = V_6;
		RenderTexture_t1963041563 * L_51 = V_2;
		NullCheck(L_51);
		int32_t L_52 = RenderTexture_get_width_m1498578543(L_51, /*hidden argument*/NULL);
		int32_t L_53 = V_4;
		RenderTexture_t1963041563 * L_54 = V_2;
		NullCheck(L_54);
		int32_t L_55 = RenderTexture_get_width_m1498578543(L_54, /*hidden argument*/NULL);
		int32_t L_56 = V_4;
		int32_t L_57 = __this->get_rtFormat_17();
		RenderTexture_t1963041563 * L_58 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, ((int32_t)((int32_t)L_52/(int32_t)L_53)), ((int32_t)((int32_t)L_55/(int32_t)L_56)), 0, L_57, /*hidden argument*/NULL);
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, L_50);
		ArrayElementTypeCheck (L_49, L_58);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(L_50), (RenderTexture_t1963041563 *)L_58);
		int32_t L_59 = V_4;
		V_4 = ((int32_t)((int32_t)L_59*(int32_t)2));
		int32_t L_60 = V_6;
		V_6 = ((int32_t)((int32_t)L_60+(int32_t)1));
	}

IL_01e3:
	{
		int32_t L_61 = V_6;
		int32_t L_62 = V_3;
		if ((((int32_t)L_61) < ((int32_t)L_62)))
		{
			goto IL_01b4;
		}
	}
	{
		RenderTexture_t1963041563 * L_63 = ___source0;
		NullCheck(L_63);
		int32_t L_64 = RenderTexture_get_width_m1498578543(L_63, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_65 = ___source0;
		NullCheck(L_65);
		int32_t L_66 = RenderTexture_get_height_m4010076224(L_65, /*hidden argument*/NULL);
		V_7 = ((float)((float)((float)((float)(((float)((float)L_64)))*(float)(1.0f)))/(float)((float)((float)(((float)((float)L_66)))*(float)(1.0f)))));
		RenderTextureU5BU5D_t1576771098* L_67 = V_5;
		int32_t L_68 = V_3;
		NullCheck(L_67);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_67, ((int32_t)((int32_t)L_68-(int32_t)1)));
		int32_t L_69 = ((int32_t)((int32_t)L_68-(int32_t)1));
		RenderTexture_t1963041563 * L_70 = (L_67)->GetAt(static_cast<il2cpp_array_size_t>(L_69));
		V_8 = L_70;
		RenderTexture_t1963041563 * L_71 = V_2;
		RenderTextureU5BU5D_t1576771098* L_72 = V_5;
		NullCheck(L_72);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_72, 0);
		int32_t L_73 = 0;
		RenderTexture_t1963041563 * L_74 = (L_72)->GetAt(static_cast<il2cpp_array_size_t>(L_73));
		Material_t3870600107 * L_75 = __this->get_tonemapMaterial_15();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_71, L_74, L_75, 1, /*hidden argument*/NULL);
		int32_t L_76 = __this->get_type_5();
		if ((!(((uint32_t)L_76) == ((uint32_t)6))))
		{
			goto IL_026c;
		}
	}
	{
		V_6 = 0;
		goto IL_025d;
	}

IL_0235:
	{
		RenderTextureU5BU5D_t1576771098* L_77 = V_5;
		int32_t L_78 = V_6;
		NullCheck(L_77);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_77, L_78);
		int32_t L_79 = L_78;
		RenderTexture_t1963041563 * L_80 = (L_77)->GetAt(static_cast<il2cpp_array_size_t>(L_79));
		RenderTextureU5BU5D_t1576771098* L_81 = V_5;
		int32_t L_82 = V_6;
		NullCheck(L_81);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_81, ((int32_t)((int32_t)L_82+(int32_t)1)));
		int32_t L_83 = ((int32_t)((int32_t)L_82+(int32_t)1));
		RenderTexture_t1963041563 * L_84 = (L_81)->GetAt(static_cast<il2cpp_array_size_t>(L_83));
		Material_t3870600107 * L_85 = __this->get_tonemapMaterial_15();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_80, L_84, L_85, ((int32_t)9), /*hidden argument*/NULL);
		RenderTextureU5BU5D_t1576771098* L_86 = V_5;
		int32_t L_87 = V_6;
		NullCheck(L_86);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_86, ((int32_t)((int32_t)L_87+(int32_t)1)));
		int32_t L_88 = ((int32_t)((int32_t)L_87+(int32_t)1));
		RenderTexture_t1963041563 * L_89 = (L_86)->GetAt(static_cast<il2cpp_array_size_t>(L_88));
		V_8 = L_89;
		int32_t L_90 = V_6;
		V_6 = ((int32_t)((int32_t)L_90+(int32_t)1));
	}

IL_025d:
	{
		int32_t L_91 = V_6;
		int32_t L_92 = V_3;
		if ((((int32_t)L_91) < ((int32_t)((int32_t)((int32_t)L_92-(int32_t)1)))))
		{
			goto IL_0235;
		}
	}
	{
		goto IL_02aa;
	}

IL_026c:
	{
		int32_t L_93 = __this->get_type_5();
		if ((!(((uint32_t)L_93) == ((uint32_t)5))))
		{
			goto IL_02aa;
		}
	}
	{
		V_6 = 0;
		goto IL_02a0;
	}

IL_0280:
	{
		RenderTextureU5BU5D_t1576771098* L_94 = V_5;
		int32_t L_95 = V_6;
		NullCheck(L_94);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_94, L_95);
		int32_t L_96 = L_95;
		RenderTexture_t1963041563 * L_97 = (L_94)->GetAt(static_cast<il2cpp_array_size_t>(L_96));
		RenderTextureU5BU5D_t1576771098* L_98 = V_5;
		int32_t L_99 = V_6;
		NullCheck(L_98);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_98, ((int32_t)((int32_t)L_99+(int32_t)1)));
		int32_t L_100 = ((int32_t)((int32_t)L_99+(int32_t)1));
		RenderTexture_t1963041563 * L_101 = (L_98)->GetAt(static_cast<il2cpp_array_size_t>(L_100));
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_97, L_101, /*hidden argument*/NULL);
		RenderTextureU5BU5D_t1576771098* L_102 = V_5;
		int32_t L_103 = V_6;
		NullCheck(L_102);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_102, ((int32_t)((int32_t)L_103+(int32_t)1)));
		int32_t L_104 = ((int32_t)((int32_t)L_103+(int32_t)1));
		RenderTexture_t1963041563 * L_105 = (L_102)->GetAt(static_cast<il2cpp_array_size_t>(L_104));
		V_8 = L_105;
		int32_t L_106 = V_6;
		V_6 = ((int32_t)((int32_t)L_106+(int32_t)1));
	}

IL_02a0:
	{
		int32_t L_107 = V_6;
		int32_t L_108 = V_3;
		if ((((int32_t)L_107) < ((int32_t)((int32_t)((int32_t)L_108-(int32_t)1)))))
		{
			goto IL_0280;
		}
	}

IL_02aa:
	{
		float L_109 = __this->get_adaptionSpeed_12();
		G_B28_0 = __this;
		if ((((float)L_109) >= ((float)(0.001f))))
		{
			G_B29_0 = __this;
			goto IL_02c5;
		}
	}
	{
		G_B30_0 = (0.001f);
		G_B30_1 = G_B28_0;
		goto IL_02cb;
	}

IL_02c5:
	{
		float L_110 = __this->get_adaptionSpeed_12();
		G_B30_0 = L_110;
		G_B30_1 = G_B29_0;
	}

IL_02cb:
	{
		NullCheck(G_B30_1);
		G_B30_1->set_adaptionSpeed_12(G_B30_0);
		Material_t3870600107 * L_111 = __this->get_tonemapMaterial_15();
		float L_112 = __this->get_adaptionSpeed_12();
		NullCheck(L_111);
		Material_SetFloat_m981710063(L_111, _stringLiteral661443618, L_112, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_113 = __this->get_rt_16();
		NullCheck(L_113);
		RenderTexture_MarkRestoreExpected_m2220245707(L_113, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_114 = V_8;
		RenderTexture_t1963041563 * L_115 = __this->get_rt_16();
		Material_t3870600107 * L_116 = __this->get_tonemapMaterial_15();
		bool L_117 = V_1;
		G_B31_0 = L_116;
		G_B31_1 = L_115;
		G_B31_2 = L_114;
		if (!L_117)
		{
			G_B32_0 = L_116;
			G_B32_1 = L_115;
			G_B32_2 = L_114;
			goto IL_030b;
		}
	}
	{
		G_B33_0 = 3;
		G_B33_1 = G_B31_0;
		G_B33_2 = G_B31_1;
		G_B33_3 = G_B31_2;
		goto IL_030c;
	}

IL_030b:
	{
		G_B33_0 = 2;
		G_B33_1 = G_B32_0;
		G_B33_2 = G_B32_1;
		G_B33_3 = G_B32_2;
	}

IL_030c:
	{
		Graphics_Blit_m336256356(NULL /*static, unused*/, G_B33_3, G_B33_2, G_B33_1, G_B33_0, /*hidden argument*/NULL);
		float L_118 = __this->get_middleGrey_10();
		G_B34_0 = __this;
		if ((((float)L_118) >= ((float)(0.001f))))
		{
			G_B35_0 = __this;
			goto IL_032c;
		}
	}
	{
		G_B36_0 = (0.001f);
		G_B36_1 = G_B34_0;
		goto IL_0332;
	}

IL_032c:
	{
		float L_119 = __this->get_middleGrey_10();
		G_B36_0 = L_119;
		G_B36_1 = G_B35_0;
	}

IL_0332:
	{
		NullCheck(G_B36_1);
		G_B36_1->set_middleGrey_10(G_B36_0);
		Material_t3870600107 * L_120 = __this->get_tonemapMaterial_15();
		float L_121 = __this->get_middleGrey_10();
		float L_122 = __this->get_middleGrey_10();
		float L_123 = __this->get_middleGrey_10();
		float L_124 = __this->get_white_11();
		float L_125 = __this->get_white_11();
		Vector4_t4282066567  L_126;
		memset(&L_126, 0, sizeof(L_126));
		Vector4__ctor_m2441427762(&L_126, L_121, L_122, L_123, ((float)((float)L_124*(float)L_125)), /*hidden argument*/NULL);
		NullCheck(L_120);
		Material_SetVector_m3505096203(L_120, _stringLiteral185336445, L_126, /*hidden argument*/NULL);
		Material_t3870600107 * L_127 = __this->get_tonemapMaterial_15();
		RenderTexture_t1963041563 * L_128 = __this->get_rt_16();
		NullCheck(L_127);
		Material_SetTexture_m1833724755(L_127, _stringLiteral3843577631, L_128, /*hidden argument*/NULL);
		int32_t L_129 = __this->get_type_5();
		if ((!(((uint32_t)L_129) == ((uint32_t)5))))
		{
			goto IL_03a0;
		}
	}
	{
		RenderTexture_t1963041563 * L_130 = ___source0;
		RenderTexture_t1963041563 * L_131 = ___destination1;
		Material_t3870600107 * L_132 = __this->get_tonemapMaterial_15();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_130, L_131, L_132, 0, /*hidden argument*/NULL);
		goto IL_03d1;
	}

IL_03a0:
	{
		int32_t L_133 = __this->get_type_5();
		if ((!(((uint32_t)L_133) == ((uint32_t)6))))
		{
			goto IL_03c0;
		}
	}
	{
		RenderTexture_t1963041563 * L_134 = ___source0;
		RenderTexture_t1963041563 * L_135 = ___destination1;
		Material_t3870600107 * L_136 = __this->get_tonemapMaterial_15();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_134, L_135, L_136, ((int32_t)10), /*hidden argument*/NULL);
		goto IL_03d1;
	}

IL_03c0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral3851645919, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_137 = ___source0;
		RenderTexture_t1963041563 * L_138 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_137, L_138, /*hidden argument*/NULL);
	}

IL_03d1:
	{
		V_6 = 0;
		goto IL_03e9;
	}

IL_03d9:
	{
		RenderTextureU5BU5D_t1576771098* L_139 = V_5;
		int32_t L_140 = V_6;
		NullCheck(L_139);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_139, L_140);
		int32_t L_141 = L_140;
		RenderTexture_t1963041563 * L_142 = (L_139)->GetAt(static_cast<il2cpp_array_size_t>(L_141));
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_142, /*hidden argument*/NULL);
		int32_t L_143 = V_6;
		V_6 = ((int32_t)((int32_t)L_143+(int32_t)1));
	}

IL_03e9:
	{
		int32_t L_144 = V_6;
		int32_t L_145 = V_3;
		if ((((int32_t)L_144) < ((int32_t)L_145)))
		{
			goto IL_03d9;
		}
	}
	{
		RenderTexture_t1963041563 * L_146 = V_2;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_146, /*hidden argument*/NULL);
	}

IL_03f7:
	{
		return;
	}
}
// System.Void Tonemapping::Main()
extern "C"  void Tonemapping_Main_m2891809303 (Tonemapping_t2852991740 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Triangles::.ctor()
extern "C"  void Triangles__ctor_m618316343 (Triangles_t1189959499 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Triangles::.cctor()
extern "C"  void Triangles__cctor_m1505841238 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean Triangles::HasMeshes()
extern Il2CppClass* Triangles_t1189959499_il2cpp_TypeInfo_var;
extern const uint32_t Triangles_HasMeshes_m1638507152_MetadataUsageId;
extern "C"  bool Triangles_HasMeshes_m1638507152 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Triangles_HasMeshes_m1638507152_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Mesh_t4241756145 * V_0 = NULL;
	int32_t V_1 = 0;
	MeshU5BU5D_t1759126828* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t G_B8_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Triangles_t1189959499_il2cpp_TypeInfo_var);
		MeshU5BU5D_t1759126828* L_0 = ((Triangles_t1189959499_StaticFields*)Triangles_t1189959499_il2cpp_TypeInfo_var->static_fields)->get_meshes_2();
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		G_B8_0 = 0;
		goto IL_0044;
	}

IL_0010:
	{
		V_1 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Triangles_t1189959499_il2cpp_TypeInfo_var);
		MeshU5BU5D_t1759126828* L_1 = ((Triangles_t1189959499_StaticFields*)Triangles_t1189959499_il2cpp_TypeInfo_var->static_fields)->get_meshes_2();
		V_2 = L_1;
		MeshU5BU5D_t1759126828* L_2 = V_2;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_2);
		int32_t L_3 = Array_get_Length_m1203127607((Il2CppArray *)(Il2CppArray *)L_2, /*hidden argument*/NULL);
		V_3 = L_3;
		goto IL_003c;
	}

IL_0024:
	{
		MeshU5BU5D_t1759126828* L_4 = V_2;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		Mesh_t4241756145 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, (Object_t3071478659 *)NULL, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0038;
		}
	}
	{
		G_B8_0 = 0;
		goto IL_0044;
	}

IL_0038:
	{
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = V_3;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0024;
		}
	}
	{
		G_B8_0 = 1;
	}

IL_0044:
	{
		return (bool)G_B8_0;
	}
}
// System.Void Triangles::Cleanup()
extern Il2CppClass* Triangles_t1189959499_il2cpp_TypeInfo_var;
extern const uint32_t Triangles_Cleanup_m4165279737_MetadataUsageId;
extern "C"  void Triangles_Cleanup_m4165279737 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Triangles_Cleanup_m4165279737_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Mesh_t4241756145 * V_0 = NULL;
	int32_t V_1 = 0;
	MeshU5BU5D_t1759126828* V_2 = NULL;
	int32_t V_3 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Triangles_t1189959499_il2cpp_TypeInfo_var);
		MeshU5BU5D_t1759126828* L_0 = ((Triangles_t1189959499_StaticFields*)Triangles_t1189959499_il2cpp_TypeInfo_var->static_fields)->get_meshes_2();
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		goto IL_004e;
	}

IL_000f:
	{
		V_1 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Triangles_t1189959499_il2cpp_TypeInfo_var);
		MeshU5BU5D_t1759126828* L_1 = ((Triangles_t1189959499_StaticFields*)Triangles_t1189959499_il2cpp_TypeInfo_var->static_fields)->get_meshes_2();
		V_2 = L_1;
		MeshU5BU5D_t1759126828* L_2 = V_2;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_2);
		int32_t L_3 = Array_get_Length_m1203127607((Il2CppArray *)(Il2CppArray *)L_2, /*hidden argument*/NULL);
		V_3 = L_3;
		goto IL_0041;
	}

IL_0023:
	{
		MeshU5BU5D_t1759126828* L_4 = V_2;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		Mesh_t4241756145 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, (Object_t3071478659 *)NULL, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_003d;
		}
	}
	{
		MeshU5BU5D_t1759126828* L_9 = V_2;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Mesh_t4241756145 * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		MeshU5BU5D_t1759126828* L_13 = V_2;
		int32_t L_14 = V_1;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		ArrayElementTypeCheck (L_13, NULL);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (Mesh_t4241756145 *)NULL);
	}

IL_003d:
	{
		int32_t L_15 = V_1;
		V_1 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0041:
	{
		int32_t L_16 = V_1;
		int32_t L_17 = V_3;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Triangles_t1189959499_il2cpp_TypeInfo_var);
		((Triangles_t1189959499_StaticFields*)Triangles_t1189959499_il2cpp_TypeInfo_var->static_fields)->set_meshes_2((MeshU5BU5D_t1759126828*)NULL);
	}

IL_004e:
	{
		return;
	}
}
// UnityEngine.Mesh[] Triangles::GetMeshes(System.Int32,System.Int32)
extern Il2CppClass* Triangles_t1189959499_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppClass* MeshU5BU5D_t1759126828_il2cpp_TypeInfo_var;
extern const uint32_t Triangles_GetMeshes_m767058167_MetadataUsageId;
extern "C"  MeshU5BU5D_t1759126828* Triangles_GetMeshes_m767058167 (Il2CppObject * __this /* static, unused */, int32_t ___totalWidth0, int32_t ___totalHeight1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Triangles_GetMeshes_m767058167_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	MeshU5BU5D_t1759126828* G_B7_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Triangles_t1189959499_il2cpp_TypeInfo_var);
		bool L_0 = Triangles_HasMeshes_m1638507152(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Triangles_t1189959499_il2cpp_TypeInfo_var);
		int32_t L_1 = ((Triangles_t1189959499_StaticFields*)Triangles_t1189959499_il2cpp_TypeInfo_var->static_fields)->get_currentTris_3();
		int32_t L_2 = ___totalWidth0;
		int32_t L_3 = ___totalHeight1;
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)((int32_t)L_2*(int32_t)L_3))))))
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Triangles_t1189959499_il2cpp_TypeInfo_var);
		MeshU5BU5D_t1759126828* L_4 = ((Triangles_t1189959499_StaticFields*)Triangles_t1189959499_il2cpp_TypeInfo_var->static_fields)->get_meshes_2();
		G_B7_0 = L_4;
		goto IL_0099;
	}

IL_0021:
	{
		V_0 = ((int32_t)21666);
		int32_t L_5 = ___totalWidth0;
		int32_t L_6 = ___totalHeight1;
		V_1 = ((int32_t)((int32_t)L_5*(int32_t)L_6));
		int32_t L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Triangles_t1189959499_il2cpp_TypeInfo_var);
		((Triangles_t1189959499_StaticFields*)Triangles_t1189959499_il2cpp_TypeInfo_var->static_fields)->set_currentTris_3(L_7);
		int32_t L_8 = V_1;
		int32_t L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_10 = Mathf_CeilToInt_m3621832739(NULL /*static, unused*/, ((float)((float)((float)((float)(1.0f)*(float)(((float)((float)L_8)))))/(float)((float)((float)(1.0f)*(float)(((float)((float)L_9))))))), /*hidden argument*/NULL);
		V_2 = L_10;
		int32_t L_11 = V_2;
		((Triangles_t1189959499_StaticFields*)Triangles_t1189959499_il2cpp_TypeInfo_var->static_fields)->set_meshes_2(((MeshU5BU5D_t1759126828*)SZArrayNew(MeshU5BU5D_t1759126828_il2cpp_TypeInfo_var, (uint32_t)L_11)));
		V_3 = 0;
		V_4 = 0;
		V_3 = 0;
		goto IL_008d;
	}

IL_005f:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_3;
		int32_t L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_15 = Mathf_Clamp_m510460741(NULL /*static, unused*/, ((int32_t)((int32_t)L_12-(int32_t)L_13)), 0, L_14, /*hidden argument*/NULL);
		int32_t L_16 = Mathf_FloorToInt_m268511322(NULL /*static, unused*/, (((float)((float)L_15))), /*hidden argument*/NULL);
		V_5 = L_16;
		IL2CPP_RUNTIME_CLASS_INIT(Triangles_t1189959499_il2cpp_TypeInfo_var);
		MeshU5BU5D_t1759126828* L_17 = ((Triangles_t1189959499_StaticFields*)Triangles_t1189959499_il2cpp_TypeInfo_var->static_fields)->get_meshes_2();
		int32_t L_18 = V_4;
		int32_t L_19 = V_5;
		int32_t L_20 = V_3;
		int32_t L_21 = ___totalWidth0;
		int32_t L_22 = ___totalHeight1;
		Mesh_t4241756145 * L_23 = Triangles_GetMesh_m1521348515(NULL /*static, unused*/, L_19, L_20, L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		ArrayElementTypeCheck (L_17, L_23);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (Mesh_t4241756145 *)L_23);
		int32_t L_24 = V_4;
		V_4 = ((int32_t)((int32_t)L_24+(int32_t)1));
		int32_t L_25 = V_3;
		int32_t L_26 = V_0;
		V_3 = ((int32_t)((int32_t)L_25+(int32_t)L_26));
	}

IL_008d:
	{
		int32_t L_27 = V_3;
		int32_t L_28 = V_1;
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_005f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Triangles_t1189959499_il2cpp_TypeInfo_var);
		MeshU5BU5D_t1759126828* L_29 = ((Triangles_t1189959499_StaticFields*)Triangles_t1189959499_il2cpp_TypeInfo_var->static_fields)->get_meshes_2();
		G_B7_0 = L_29;
	}

IL_0099:
	{
		return G_B7_0;
	}
}
// UnityEngine.Mesh Triangles::GetMesh(System.Int32,System.Int32,System.Int32,System.Int32)
extern Il2CppClass* Mesh_t4241756145_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3U5BU5D_t215400611_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector2U5BU5D_t4024180168_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Triangles_GetMesh_m1521348515_MetadataUsageId;
extern "C"  Mesh_t4241756145 * Triangles_GetMesh_m1521348515 (Il2CppObject * __this /* static, unused */, int32_t ___triCount0, int32_t ___triOffset1, int32_t ___totalWidth2, int32_t ___totalHeight3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Triangles_GetMesh_m1521348515_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Mesh_t4241756145 * V_0 = NULL;
	Vector3U5BU5D_t215400611* V_1 = NULL;
	Vector2U5BU5D_t4024180168* V_2 = NULL;
	Vector2U5BU5D_t4024180168* V_3 = NULL;
	Int32U5BU5D_t3230847821* V_4 = NULL;
	float V_5 = 0.0f;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	Vector3_t4282066566  V_11;
	memset(&V_11, 0, sizeof(V_11));
	{
		Mesh_t4241756145 * L_0 = (Mesh_t4241756145 *)il2cpp_codegen_object_new(Mesh_t4241756145_il2cpp_TypeInfo_var);
		Mesh__ctor_m2684203808(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Mesh_t4241756145 * L_1 = V_0;
		NullCheck(L_1);
		Object_set_hideFlags_m41317712(L_1, ((int32_t)52), /*hidden argument*/NULL);
		int32_t L_2 = ___triCount0;
		V_1 = ((Vector3U5BU5D_t215400611*)SZArrayNew(Vector3U5BU5D_t215400611_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_2*(int32_t)3))));
		int32_t L_3 = ___triCount0;
		V_2 = ((Vector2U5BU5D_t4024180168*)SZArrayNew(Vector2U5BU5D_t4024180168_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_3*(int32_t)3))));
		int32_t L_4 = ___triCount0;
		V_3 = ((Vector2U5BU5D_t4024180168*)SZArrayNew(Vector2U5BU5D_t4024180168_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_4*(int32_t)3))));
		int32_t L_5 = ___triCount0;
		V_4 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_5*(int32_t)3))));
		V_5 = (0.0075f);
		V_6 = 0;
		goto IL_0178;
	}

IL_0042:
	{
		int32_t L_6 = V_6;
		V_7 = ((int32_t)((int32_t)L_6*(int32_t)3));
		int32_t L_7 = ___triOffset1;
		int32_t L_8 = V_6;
		V_8 = ((int32_t)((int32_t)L_7+(int32_t)L_8));
		int32_t L_9 = V_8;
		int32_t L_10 = ___totalWidth2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_11 = floorf((((float)((float)((int32_t)((int32_t)L_9%(int32_t)L_10))))));
		int32_t L_12 = ___totalWidth2;
		V_9 = ((float)((float)L_11/(float)(((float)((float)L_12)))));
		int32_t L_13 = V_8;
		int32_t L_14 = ___totalWidth2;
		float L_15 = floorf((((float)((float)((int32_t)((int32_t)L_13/(int32_t)L_14))))));
		int32_t L_16 = ___totalHeight3;
		V_10 = ((float)((float)L_15/(float)(((float)((float)L_16)))));
		float L_17 = V_9;
		float L_18 = V_10;
		Vector3_t4282066566  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector3__ctor_m2926210380(&L_19, ((float)((float)((float)((float)L_17*(float)(((float)((float)2)))))-(float)(((float)((float)1))))), ((float)((float)((float)((float)L_18*(float)(((float)((float)2)))))-(float)(((float)((float)1))))), (1.0f), /*hidden argument*/NULL);
		V_11 = L_19;
		Vector3U5BU5D_t215400611* L_20 = V_1;
		int32_t L_21 = V_7;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, ((int32_t)((int32_t)L_21+(int32_t)0)));
		Vector3_t4282066566  L_22 = V_11;
		(*(Vector3_t4282066566 *)((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_21+(int32_t)0)))))) = L_22;
		Vector3U5BU5D_t215400611* L_23 = V_1;
		int32_t L_24 = V_7;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, ((int32_t)((int32_t)L_24+(int32_t)1)));
		Vector3_t4282066566  L_25 = V_11;
		(*(Vector3_t4282066566 *)((L_23)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_24+(int32_t)1)))))) = L_25;
		Vector3U5BU5D_t215400611* L_26 = V_1;
		int32_t L_27 = V_7;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, ((int32_t)((int32_t)L_27+(int32_t)2)));
		Vector3_t4282066566  L_28 = V_11;
		(*(Vector3_t4282066566 *)((L_26)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_27+(int32_t)2)))))) = L_28;
		Vector2U5BU5D_t4024180168* L_29 = V_2;
		int32_t L_30 = V_7;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, ((int32_t)((int32_t)L_30+(int32_t)0)));
		Vector2_t4282066565  L_31;
		memset(&L_31, 0, sizeof(L_31));
		Vector2__ctor_m1517109030(&L_31, (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_30+(int32_t)0)))))) = L_31;
		Vector2U5BU5D_t4024180168* L_32 = V_2;
		int32_t L_33 = V_7;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, ((int32_t)((int32_t)L_33+(int32_t)1)));
		Vector2_t4282066565  L_34;
		memset(&L_34, 0, sizeof(L_34));
		Vector2__ctor_m1517109030(&L_34, (1.0f), (((float)((float)0))), /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_32)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_33+(int32_t)1)))))) = L_34;
		Vector2U5BU5D_t4024180168* L_35 = V_2;
		int32_t L_36 = V_7;
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, ((int32_t)((int32_t)L_36+(int32_t)2)));
		Vector2_t4282066565  L_37;
		memset(&L_37, 0, sizeof(L_37));
		Vector2__ctor_m1517109030(&L_37, (((float)((float)0))), (1.0f), /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_35)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_36+(int32_t)2)))))) = L_37;
		Vector2U5BU5D_t4024180168* L_38 = V_3;
		int32_t L_39 = V_7;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, ((int32_t)((int32_t)L_39+(int32_t)0)));
		float L_40 = V_9;
		float L_41 = V_10;
		Vector2_t4282066565  L_42;
		memset(&L_42, 0, sizeof(L_42));
		Vector2__ctor_m1517109030(&L_42, L_40, L_41, /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_39+(int32_t)0)))))) = L_42;
		Vector2U5BU5D_t4024180168* L_43 = V_3;
		int32_t L_44 = V_7;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, ((int32_t)((int32_t)L_44+(int32_t)1)));
		float L_45 = V_9;
		float L_46 = V_10;
		Vector2_t4282066565  L_47;
		memset(&L_47, 0, sizeof(L_47));
		Vector2__ctor_m1517109030(&L_47, L_45, L_46, /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_44+(int32_t)1)))))) = L_47;
		Vector2U5BU5D_t4024180168* L_48 = V_3;
		int32_t L_49 = V_7;
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, ((int32_t)((int32_t)L_49+(int32_t)2)));
		float L_50 = V_9;
		float L_51 = V_10;
		Vector2_t4282066565  L_52;
		memset(&L_52, 0, sizeof(L_52));
		Vector2__ctor_m1517109030(&L_52, L_50, L_51, /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_48)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_49+(int32_t)2)))))) = L_52;
		Int32U5BU5D_t3230847821* L_53 = V_4;
		int32_t L_54 = V_7;
		int32_t L_55 = V_7;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, ((int32_t)((int32_t)L_54+(int32_t)0)));
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_54+(int32_t)0))), (int32_t)((int32_t)((int32_t)L_55+(int32_t)0)));
		Int32U5BU5D_t3230847821* L_56 = V_4;
		int32_t L_57 = V_7;
		int32_t L_58 = V_7;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, ((int32_t)((int32_t)L_57+(int32_t)1)));
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_57+(int32_t)1))), (int32_t)((int32_t)((int32_t)L_58+(int32_t)1)));
		Int32U5BU5D_t3230847821* L_59 = V_4;
		int32_t L_60 = V_7;
		int32_t L_61 = V_7;
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, ((int32_t)((int32_t)L_60+(int32_t)2)));
		(L_59)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_60+(int32_t)2))), (int32_t)((int32_t)((int32_t)L_61+(int32_t)2)));
		int32_t L_62 = V_6;
		V_6 = ((int32_t)((int32_t)L_62+(int32_t)1));
	}

IL_0178:
	{
		int32_t L_63 = V_6;
		int32_t L_64 = ___triCount0;
		if ((((int32_t)L_63) < ((int32_t)L_64)))
		{
			goto IL_0042;
		}
	}
	{
		Mesh_t4241756145 * L_65 = V_0;
		Vector3U5BU5D_t215400611* L_66 = V_1;
		NullCheck(L_65);
		Mesh_set_vertices_m2628866109(L_65, L_66, /*hidden argument*/NULL);
		Mesh_t4241756145 * L_67 = V_0;
		Int32U5BU5D_t3230847821* L_68 = V_4;
		NullCheck(L_67);
		Mesh_set_triangles_m2341339867(L_67, L_68, /*hidden argument*/NULL);
		Mesh_t4241756145 * L_69 = V_0;
		Vector2U5BU5D_t4024180168* L_70 = V_2;
		NullCheck(L_69);
		Mesh_set_uv_m498907190(L_69, L_70, /*hidden argument*/NULL);
		Mesh_t4241756145 * L_71 = V_0;
		Vector2U5BU5D_t4024180168* L_72 = V_3;
		NullCheck(L_71);
		Mesh_set_uv2_m1515914022(L_71, L_72, /*hidden argument*/NULL);
		Mesh_t4241756145 * L_73 = V_0;
		return L_73;
	}
}
// System.Void Triangles::Main()
extern "C"  void Triangles_Main_m3953312678 (Triangles_t1189959499 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vignetting::.ctor()
extern "C"  void Vignetting__ctor_m3033148941 (Vignetting_t439778199 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase__ctor_m3869393199(__this, /*hidden argument*/NULL);
		__this->set_mode_5(0);
		__this->set_intensity_6((0.375f));
		__this->set_chromaticAberration_7((0.2f));
		__this->set_axialAberration_8((0.5f));
		__this->set_blurSpread_10((0.75f));
		__this->set_luminanceDependency_11((0.25f));
		__this->set_blurDistance_12((2.5f));
		return;
	}
}
// System.Boolean Vignetting::CheckResources()
extern "C"  bool Vignetting_CheckResources_m2415816762 (Vignetting_t439778199 * __this, const MethodInfo* method)
{
	{
		VirtFuncInvoker1< bool, bool >::Invoke(10 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean) */, __this, (bool)0);
		Shader_t3191267369 * L_0 = __this->get_vignetteShader_13();
		Material_t3870600107 * L_1 = __this->get_vignetteMaterial_14();
		Material_t3870600107 * L_2 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_0, L_1);
		__this->set_vignetteMaterial_14(L_2);
		Shader_t3191267369 * L_3 = __this->get_separableBlurShader_15();
		Material_t3870600107 * L_4 = __this->get_separableBlurMaterial_16();
		Material_t3870600107 * L_5 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_3, L_4);
		__this->set_separableBlurMaterial_16(L_5);
		Shader_t3191267369 * L_6 = __this->get_chromAberrationShader_17();
		Material_t3870600107 * L_7 = __this->get_chromAberrationMaterial_18();
		Material_t3870600107 * L_8 = VirtFuncInvoker2< Material_t3870600107 *, Shader_t3191267369 *, Material_t3870600107 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_6, L_7);
		__this->set_chromAberrationMaterial_18(L_8);
		bool L_9 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		if (L_9)
		{
			goto IL_0061;
		}
	}
	{
		VirtActionInvoker0::Invoke(13 /* System.Void PostEffectsBase::ReportAutoDisable() */, __this);
	}

IL_0061:
	{
		bool L_10 = ((PostEffectsBase_t1820837395 *)__this)->get_isSupported_4();
		return L_10;
	}
}
// System.Void Vignetting::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2746560064;
extern Il2CppCodeGenString* _stringLiteral1014379156;
extern Il2CppCodeGenString* _stringLiteral89808230;
extern Il2CppCodeGenString* _stringLiteral126034606;
extern Il2CppCodeGenString* _stringLiteral540776810;
extern Il2CppCodeGenString* _stringLiteral262246415;
extern Il2CppCodeGenString* _stringLiteral268273531;
extern Il2CppCodeGenString* _stringLiteral1693146969;
extern const uint32_t Vignetting_OnRenderImage_m2896650801_MetadataUsageId;
extern "C"  void Vignetting_OnRenderImage_m2896650801 (Vignetting_t439778199 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vignetting_OnRenderImage_m2896650801_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	RenderTexture_t1963041563 * V_5 = NULL;
	RenderTexture_t1963041563 * V_6 = NULL;
	RenderTexture_t1963041563 * V_7 = NULL;
	int32_t V_8 = 0;
	int32_t G_B4_0 = 0;
	int32_t G_B3_0 = 0;
	RenderTexture_t1963041563 * G_B16_0 = NULL;
	Material_t3870600107 * G_B18_0 = NULL;
	RenderTexture_t1963041563 * G_B18_1 = NULL;
	RenderTexture_t1963041563 * G_B18_2 = NULL;
	Material_t3870600107 * G_B17_0 = NULL;
	RenderTexture_t1963041563 * G_B17_1 = NULL;
	RenderTexture_t1963041563 * G_B17_2 = NULL;
	int32_t G_B19_0 = 0;
	Material_t3870600107 * G_B19_1 = NULL;
	RenderTexture_t1963041563 * G_B19_2 = NULL;
	RenderTexture_t1963041563 * G_B19_3 = NULL;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Vignetting::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		RenderTexture_t1963041563 * L_1 = ___source0;
		RenderTexture_t1963041563 * L_2 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_0287;
	}

IL_0017:
	{
		RenderTexture_t1963041563 * L_3 = ___source0;
		NullCheck(L_3);
		int32_t L_4 = RenderTexture_get_width_m1498578543(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		RenderTexture_t1963041563 * L_5 = ___source0;
		NullCheck(L_5);
		int32_t L_6 = RenderTexture_get_height_m4010076224(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = __this->get_blur_9();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_8 = fabsf(L_7);
		int32_t L_9 = ((((float)L_8) > ((float)(((float)((float)0)))))? 1 : 0);
		G_B3_0 = L_9;
		if (L_9)
		{
			G_B4_0 = L_9;
			goto IL_004a;
		}
	}
	{
		float L_10 = __this->get_intensity_6();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_11 = fabsf(L_10);
		G_B4_0 = ((((float)L_11) > ((float)(((float)((float)0)))))? 1 : 0);
	}

IL_004a:
	{
		V_2 = (bool)G_B4_0;
		int32_t L_12 = V_0;
		int32_t L_13 = V_1;
		V_3 = ((float)((float)((float)((float)(1.0f)*(float)(((float)((float)L_12)))))/(float)((float)((float)(1.0f)*(float)(((float)((float)L_13)))))));
		V_4 = (0.001953125f);
		V_5 = (RenderTexture_t1963041563 *)NULL;
		V_6 = (RenderTexture_t1963041563 *)NULL;
		V_7 = (RenderTexture_t1963041563 *)NULL;
		bool L_14 = V_2;
		if (!L_14)
		{
			goto IL_01b9;
		}
	}
	{
		int32_t L_15 = V_0;
		int32_t L_16 = V_1;
		RenderTexture_t1963041563 * L_17 = ___source0;
		NullCheck(L_17);
		int32_t L_18 = RenderTexture_get_format_m3502109954(L_17, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_19 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, L_15, L_16, 0, L_18, /*hidden argument*/NULL);
		V_5 = L_19;
		float L_20 = __this->get_blur_9();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_21 = fabsf(L_20);
		if ((((float)L_21) <= ((float)(((float)((float)0))))))
		{
			goto IL_016c;
		}
	}
	{
		int32_t L_22 = V_0;
		int32_t L_23 = V_1;
		RenderTexture_t1963041563 * L_24 = ___source0;
		NullCheck(L_24);
		int32_t L_25 = RenderTexture_get_format_m3502109954(L_24, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_26 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, ((int32_t)((int32_t)L_22/(int32_t)2)), ((int32_t)((int32_t)L_23/(int32_t)2)), 0, L_25, /*hidden argument*/NULL);
		V_6 = L_26;
		RenderTexture_t1963041563 * L_27 = ___source0;
		RenderTexture_t1963041563 * L_28 = V_6;
		Material_t3870600107 * L_29 = __this->get_chromAberrationMaterial_18();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_27, L_28, L_29, 0, /*hidden argument*/NULL);
		V_8 = 0;
		goto IL_0164;
	}

IL_00c0:
	{
		Material_t3870600107 * L_30 = __this->get_separableBlurMaterial_16();
		float L_31 = __this->get_blurSpread_10();
		float L_32 = V_4;
		Vector4_t4282066567  L_33;
		memset(&L_33, 0, sizeof(L_33));
		Vector4__ctor_m2441427762(&L_33, (((float)((float)0))), ((float)((float)L_31*(float)L_32)), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_30);
		Material_SetVector_m3505096203(L_30, _stringLiteral2746560064, L_33, /*hidden argument*/NULL);
		int32_t L_34 = V_0;
		int32_t L_35 = V_1;
		RenderTexture_t1963041563 * L_36 = ___source0;
		NullCheck(L_36);
		int32_t L_37 = RenderTexture_get_format_m3502109954(L_36, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_38 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, ((int32_t)((int32_t)L_34/(int32_t)2)), ((int32_t)((int32_t)L_35/(int32_t)2)), 0, L_37, /*hidden argument*/NULL);
		V_7 = L_38;
		RenderTexture_t1963041563 * L_39 = V_6;
		RenderTexture_t1963041563 * L_40 = V_7;
		Material_t3870600107 * L_41 = __this->get_separableBlurMaterial_16();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_39, L_40, L_41, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_42 = V_6;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		Material_t3870600107 * L_43 = __this->get_separableBlurMaterial_16();
		float L_44 = __this->get_blurSpread_10();
		float L_45 = V_4;
		float L_46 = V_3;
		Vector4_t4282066567  L_47;
		memset(&L_47, 0, sizeof(L_47));
		Vector4__ctor_m2441427762(&L_47, ((float)((float)((float)((float)L_44*(float)L_45))/(float)L_46)), (((float)((float)0))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_43);
		Material_SetVector_m3505096203(L_43, _stringLiteral2746560064, L_47, /*hidden argument*/NULL);
		int32_t L_48 = V_0;
		int32_t L_49 = V_1;
		RenderTexture_t1963041563 * L_50 = ___source0;
		NullCheck(L_50);
		int32_t L_51 = RenderTexture_get_format_m3502109954(L_50, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_52 = RenderTexture_GetTemporary_m1328055742(NULL /*static, unused*/, ((int32_t)((int32_t)L_48/(int32_t)2)), ((int32_t)((int32_t)L_49/(int32_t)2)), 0, L_51, /*hidden argument*/NULL);
		V_6 = L_52;
		RenderTexture_t1963041563 * L_53 = V_7;
		RenderTexture_t1963041563 * L_54 = V_6;
		Material_t3870600107 * L_55 = __this->get_separableBlurMaterial_16();
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_53, L_54, L_55, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_56 = V_7;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_56, /*hidden argument*/NULL);
		int32_t L_57 = V_8;
		V_8 = ((int32_t)((int32_t)L_57+(int32_t)1));
	}

IL_0164:
	{
		int32_t L_58 = V_8;
		if ((((int32_t)L_58) < ((int32_t)2)))
		{
			goto IL_00c0;
		}
	}

IL_016c:
	{
		Material_t3870600107 * L_59 = __this->get_vignetteMaterial_14();
		float L_60 = __this->get_intensity_6();
		NullCheck(L_59);
		Material_SetFloat_m981710063(L_59, _stringLiteral1014379156, L_60, /*hidden argument*/NULL);
		Material_t3870600107 * L_61 = __this->get_vignetteMaterial_14();
		float L_62 = __this->get_blur_9();
		NullCheck(L_61);
		Material_SetFloat_m981710063(L_61, _stringLiteral89808230, L_62, /*hidden argument*/NULL);
		Material_t3870600107 * L_63 = __this->get_vignetteMaterial_14();
		RenderTexture_t1963041563 * L_64 = V_6;
		NullCheck(L_63);
		Material_SetTexture_m1833724755(L_63, _stringLiteral126034606, L_64, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_65 = ___source0;
		RenderTexture_t1963041563 * L_66 = V_5;
		Material_t3870600107 * L_67 = __this->get_vignetteMaterial_14();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_65, L_66, L_67, 0, /*hidden argument*/NULL);
	}

IL_01b9:
	{
		Material_t3870600107 * L_68 = __this->get_chromAberrationMaterial_18();
		float L_69 = __this->get_chromaticAberration_7();
		NullCheck(L_68);
		Material_SetFloat_m981710063(L_68, _stringLiteral540776810, L_69, /*hidden argument*/NULL);
		Material_t3870600107 * L_70 = __this->get_chromAberrationMaterial_18();
		float L_71 = __this->get_axialAberration_8();
		NullCheck(L_70);
		Material_SetFloat_m981710063(L_70, _stringLiteral262246415, L_71, /*hidden argument*/NULL);
		Material_t3870600107 * L_72 = __this->get_chromAberrationMaterial_18();
		float L_73 = __this->get_blurDistance_12();
		float L_74 = __this->get_blurDistance_12();
		Vector2_t4282066565  L_75;
		memset(&L_75, 0, sizeof(L_75));
		Vector2__ctor_m1517109030(&L_75, ((-L_73)), L_74, /*hidden argument*/NULL);
		Vector4_t4282066567  L_76 = Vector4_op_Implicit_m331673240(NULL /*static, unused*/, L_75, /*hidden argument*/NULL);
		NullCheck(L_72);
		Material_SetVector_m3505096203(L_72, _stringLiteral268273531, L_76, /*hidden argument*/NULL);
		Material_t3870600107 * L_77 = __this->get_chromAberrationMaterial_18();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_78 = ((Mathf_t4203372500_StaticFields*)Mathf_t4203372500_il2cpp_TypeInfo_var->static_fields)->get_Epsilon_0();
		float L_79 = __this->get_luminanceDependency_11();
		float L_80 = Mathf_Max_m3923796455(NULL /*static, unused*/, L_78, L_79, /*hidden argument*/NULL);
		NullCheck(L_77);
		Material_SetFloat_m981710063(L_77, _stringLiteral1693146969, ((float)((float)(1.0f)/(float)L_80)), /*hidden argument*/NULL);
		bool L_81 = V_2;
		if (!L_81)
		{
			goto IL_0245;
		}
	}
	{
		RenderTexture_t1963041563 * L_82 = V_5;
		NullCheck(L_82);
		Texture_set_wrapMode_m3720633937(L_82, 1, /*hidden argument*/NULL);
		goto IL_024c;
	}

IL_0245:
	{
		RenderTexture_t1963041563 * L_83 = ___source0;
		NullCheck(L_83);
		Texture_set_wrapMode_m3720633937(L_83, 1, /*hidden argument*/NULL);
	}

IL_024c:
	{
		bool L_84 = V_2;
		if (!L_84)
		{
			goto IL_0259;
		}
	}
	{
		RenderTexture_t1963041563 * L_85 = V_5;
		G_B16_0 = L_85;
		goto IL_025a;
	}

IL_0259:
	{
		RenderTexture_t1963041563 * L_86 = ___source0;
		G_B16_0 = L_86;
	}

IL_025a:
	{
		RenderTexture_t1963041563 * L_87 = ___destination1;
		Material_t3870600107 * L_88 = __this->get_chromAberrationMaterial_18();
		int32_t L_89 = __this->get_mode_5();
		G_B17_0 = L_88;
		G_B17_1 = L_87;
		G_B17_2 = G_B16_0;
		if ((!(((uint32_t)L_89) == ((uint32_t)1))))
		{
			G_B18_0 = L_88;
			G_B18_1 = L_87;
			G_B18_2 = G_B16_0;
			goto IL_0273;
		}
	}
	{
		G_B19_0 = 2;
		G_B19_1 = G_B17_0;
		G_B19_2 = G_B17_1;
		G_B19_3 = G_B17_2;
		goto IL_0274;
	}

IL_0273:
	{
		G_B19_0 = 1;
		G_B19_1 = G_B18_0;
		G_B19_2 = G_B18_1;
		G_B19_3 = G_B18_2;
	}

IL_0274:
	{
		Graphics_Blit_m336256356(NULL /*static, unused*/, G_B19_3, G_B19_2, G_B19_1, G_B19_0, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_90 = V_5;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_90, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_91 = V_6;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
	}

IL_0287:
	{
		return;
	}
}
// System.Void Vignetting::Main()
extern "C"  void Vignetting_Main_m13337872 (Vignetting_t439778199 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
