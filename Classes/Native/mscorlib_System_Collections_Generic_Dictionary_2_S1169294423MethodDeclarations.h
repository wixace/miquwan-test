﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int3,System.Object>
struct ShimEnumerator_t1169294423;
// System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>
struct Dictionary_2_t1453516396;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int3,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m4231391254_gshared (ShimEnumerator_t1169294423 * __this, Dictionary_2_t1453516396 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m4231391254(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1169294423 *, Dictionary_2_t1453516396 *, const MethodInfo*))ShimEnumerator__ctor_m4231391254_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int3,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1183428367_gshared (ShimEnumerator_t1169294423 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1183428367(__this, method) ((  bool (*) (ShimEnumerator_t1169294423 *, const MethodInfo*))ShimEnumerator_MoveNext_m1183428367_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int3,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m1876615803_gshared (ShimEnumerator_t1169294423 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1876615803(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t1169294423 *, const MethodInfo*))ShimEnumerator_get_Entry_m1876615803_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int3,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1664278138_gshared (ShimEnumerator_t1169294423 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1664278138(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1169294423 *, const MethodInfo*))ShimEnumerator_get_Key_m1664278138_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int3,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2691892876_gshared (ShimEnumerator_t1169294423 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m2691892876(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1169294423 *, const MethodInfo*))ShimEnumerator_get_Value_m2691892876_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int3,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2763432468_gshared (ShimEnumerator_t1169294423 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m2763432468(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1169294423 *, const MethodInfo*))ShimEnumerator_get_Current_m2763432468_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int3,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m3988046184_gshared (ShimEnumerator_t1169294423 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m3988046184(__this, method) ((  void (*) (ShimEnumerator_t1169294423 *, const MethodInfo*))ShimEnumerator_Reset_m3988046184_gshared)(__this, method)
