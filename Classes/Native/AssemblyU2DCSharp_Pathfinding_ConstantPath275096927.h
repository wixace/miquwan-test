﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.GraphNode
struct GraphNode_t23612370;
// System.Collections.Generic.List`1<Pathfinding.GraphNode>
struct List_1_t1391797922;
// Pathfinding.PathEndingCondition
struct PathEndingCondition_t3410840753;

#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.ConstantPath
struct  ConstantPath_t275096927  : public Path_t1974241691
{
public:
	// Pathfinding.GraphNode Pathfinding.ConstantPath::startNode
	GraphNode_t23612370 * ___startNode_38;
	// UnityEngine.Vector3 Pathfinding.ConstantPath::startPoint
	Vector3_t4282066566  ___startPoint_39;
	// UnityEngine.Vector3 Pathfinding.ConstantPath::originalStartPoint
	Vector3_t4282066566  ___originalStartPoint_40;
	// System.Collections.Generic.List`1<Pathfinding.GraphNode> Pathfinding.ConstantPath::allNodes
	List_1_t1391797922 * ___allNodes_41;
	// Pathfinding.PathEndingCondition Pathfinding.ConstantPath::endingCondition
	PathEndingCondition_t3410840753 * ___endingCondition_42;

public:
	inline static int32_t get_offset_of_startNode_38() { return static_cast<int32_t>(offsetof(ConstantPath_t275096927, ___startNode_38)); }
	inline GraphNode_t23612370 * get_startNode_38() const { return ___startNode_38; }
	inline GraphNode_t23612370 ** get_address_of_startNode_38() { return &___startNode_38; }
	inline void set_startNode_38(GraphNode_t23612370 * value)
	{
		___startNode_38 = value;
		Il2CppCodeGenWriteBarrier(&___startNode_38, value);
	}

	inline static int32_t get_offset_of_startPoint_39() { return static_cast<int32_t>(offsetof(ConstantPath_t275096927, ___startPoint_39)); }
	inline Vector3_t4282066566  get_startPoint_39() const { return ___startPoint_39; }
	inline Vector3_t4282066566 * get_address_of_startPoint_39() { return &___startPoint_39; }
	inline void set_startPoint_39(Vector3_t4282066566  value)
	{
		___startPoint_39 = value;
	}

	inline static int32_t get_offset_of_originalStartPoint_40() { return static_cast<int32_t>(offsetof(ConstantPath_t275096927, ___originalStartPoint_40)); }
	inline Vector3_t4282066566  get_originalStartPoint_40() const { return ___originalStartPoint_40; }
	inline Vector3_t4282066566 * get_address_of_originalStartPoint_40() { return &___originalStartPoint_40; }
	inline void set_originalStartPoint_40(Vector3_t4282066566  value)
	{
		___originalStartPoint_40 = value;
	}

	inline static int32_t get_offset_of_allNodes_41() { return static_cast<int32_t>(offsetof(ConstantPath_t275096927, ___allNodes_41)); }
	inline List_1_t1391797922 * get_allNodes_41() const { return ___allNodes_41; }
	inline List_1_t1391797922 ** get_address_of_allNodes_41() { return &___allNodes_41; }
	inline void set_allNodes_41(List_1_t1391797922 * value)
	{
		___allNodes_41 = value;
		Il2CppCodeGenWriteBarrier(&___allNodes_41, value);
	}

	inline static int32_t get_offset_of_endingCondition_42() { return static_cast<int32_t>(offsetof(ConstantPath_t275096927, ___endingCondition_42)); }
	inline PathEndingCondition_t3410840753 * get_endingCondition_42() const { return ___endingCondition_42; }
	inline PathEndingCondition_t3410840753 ** get_address_of_endingCondition_42() { return &___endingCondition_42; }
	inline void set_endingCondition_42(PathEndingCondition_t3410840753 * value)
	{
		___endingCondition_42 = value;
		Il2CppCodeGenWriteBarrier(&___endingCondition_42, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
