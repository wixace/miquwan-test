﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua.SDK.SdkwwwCallBack
struct SdkwwwCallBack_t3004559384;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void Mihua.SDK.SdkwwwCallBack::.ctor(System.Object,System.IntPtr)
extern "C"  void SdkwwwCallBack__ctor_m1647049139 (SdkwwwCallBack_t3004559384 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.SDK.SdkwwwCallBack::Invoke(System.Boolean,System.String)
extern "C"  void SdkwwwCallBack_Invoke_m3884374976 (SdkwwwCallBack_t3004559384 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Mihua.SDK.SdkwwwCallBack::BeginInvoke(System.Boolean,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SdkwwwCallBack_BeginInvoke_m2543051701 (SdkwwwCallBack_t3004559384 * __this, bool ___success0, String_t* ___arg1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.SDK.SdkwwwCallBack::EndInvoke(System.IAsyncResult)
extern "C"  void SdkwwwCallBack_EndInvoke_m2282634563 (SdkwwwCallBack_t3004559384 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
