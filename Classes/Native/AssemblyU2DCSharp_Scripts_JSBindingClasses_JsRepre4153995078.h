﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,Scripts.JSBindingClasses.JsRepresentClass>
struct Dictionary_2_t3733910921;
// Scripts.JSBindingClasses.JsRepresentClassManager
struct JsRepresentClassManager_t4153995078;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Scripts.JSBindingClasses.JsRepresentClassManager
struct  JsRepresentClassManager_t4153995078  : public MonoBehaviour_t667441552
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Scripts.JSBindingClasses.JsRepresentClass> Scripts.JSBindingClasses.JsRepresentClassManager::StaticDictionary
	Dictionary_2_t3733910921 * ___StaticDictionary_2;

public:
	inline static int32_t get_offset_of_StaticDictionary_2() { return static_cast<int32_t>(offsetof(JsRepresentClassManager_t4153995078, ___StaticDictionary_2)); }
	inline Dictionary_2_t3733910921 * get_StaticDictionary_2() const { return ___StaticDictionary_2; }
	inline Dictionary_2_t3733910921 ** get_address_of_StaticDictionary_2() { return &___StaticDictionary_2; }
	inline void set_StaticDictionary_2(Dictionary_2_t3733910921 * value)
	{
		___StaticDictionary_2 = value;
		Il2CppCodeGenWriteBarrier(&___StaticDictionary_2, value);
	}
};

struct JsRepresentClassManager_t4153995078_StaticFields
{
public:
	// Scripts.JSBindingClasses.JsRepresentClassManager Scripts.JSBindingClasses.JsRepresentClassManager::_instance
	JsRepresentClassManager_t4153995078 * ____instance_3;

public:
	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(JsRepresentClassManager_t4153995078_StaticFields, ____instance_3)); }
	inline JsRepresentClassManager_t4153995078 * get__instance_3() const { return ____instance_3; }
	inline JsRepresentClassManager_t4153995078 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(JsRepresentClassManager_t4153995078 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier(&____instance_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
