﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.IEnumerable
struct IEnumerable_t3464557803;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Type
struct Type_t;
// ProtoBuf.IExtensible
struct IExtensible_t1056931882;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_ProtoBuf_DataFormat274207885.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"

// System.Collections.IEnumerable ProtoBuf.ExtensibleUtil::GetExtendedValues(ProtoBuf.Meta.TypeModel,System.Type,ProtoBuf.IExtensible,System.Int32,ProtoBuf.DataFormat,System.Boolean,System.Boolean)
extern "C"  Il2CppObject * ExtensibleUtil_GetExtendedValues_m2668747672 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, Type_t * ___type1, Il2CppObject * ___instance2, int32_t ___tag3, int32_t ___format4, bool ___singleton5, bool ___allowDefinedTag6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ExtensibleUtil::AppendExtendValue(ProtoBuf.Meta.TypeModel,ProtoBuf.IExtensible,System.Int32,ProtoBuf.DataFormat,System.Object)
extern "C"  void ExtensibleUtil_AppendExtendValue_m1965894475 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, Il2CppObject * ___instance1, int32_t ___tag2, int32_t ___format3, Il2CppObject * ___value4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ExtensibleUtil::ilo_Close1(ProtoBuf.ProtoWriter)
extern "C"  void ExtensibleUtil_ilo_Close1_m3763263917 (Il2CppObject * __this /* static, unused */, ProtoWriter_t4117914721 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
