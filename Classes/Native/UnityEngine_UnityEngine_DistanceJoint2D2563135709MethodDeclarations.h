﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.DistanceJoint2D
struct DistanceJoint2D_t2563135709;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.DistanceJoint2D::.ctor()
extern "C"  void DistanceJoint2D__ctor_m3022275986 (DistanceJoint2D_t2563135709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.DistanceJoint2D::get_autoConfigureDistance()
extern "C"  bool DistanceJoint2D_get_autoConfigureDistance_m1486824537 (DistanceJoint2D_t2563135709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DistanceJoint2D::set_autoConfigureDistance(System.Boolean)
extern "C"  void DistanceJoint2D_set_autoConfigureDistance_m3946367926 (DistanceJoint2D_t2563135709 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.DistanceJoint2D::get_distance()
extern "C"  float DistanceJoint2D_get_distance_m2936806660 (DistanceJoint2D_t2563135709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DistanceJoint2D::set_distance(System.Single)
extern "C"  void DistanceJoint2D_set_distance_m2608176423 (DistanceJoint2D_t2563135709 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.DistanceJoint2D::get_maxDistanceOnly()
extern "C"  bool DistanceJoint2D_get_maxDistanceOnly_m3957358802 (DistanceJoint2D_t2563135709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DistanceJoint2D::set_maxDistanceOnly(System.Boolean)
extern "C"  void DistanceJoint2D_set_maxDistanceOnly_m957486319 (DistanceJoint2D_t2563135709 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
