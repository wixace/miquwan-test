﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<PushType,System.Object>
struct ShimEnumerator_t336612661;
// System.Collections.Generic.Dictionary`2<PushType,System.Object>
struct Dictionary_2_t620834634;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<PushType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m2672046172_gshared (ShimEnumerator_t336612661 * __this, Dictionary_2_t620834634 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m2672046172(__this, ___host0, method) ((  void (*) (ShimEnumerator_t336612661 *, Dictionary_2_t620834634 *, const MethodInfo*))ShimEnumerator__ctor_m2672046172_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<PushType,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2827584521_gshared (ShimEnumerator_t336612661 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m2827584521(__this, method) ((  bool (*) (ShimEnumerator_t336612661 *, const MethodInfo*))ShimEnumerator_MoveNext_m2827584521_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<PushType,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m3400644545_gshared (ShimEnumerator_t336612661 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m3400644545(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t336612661 *, const MethodInfo*))ShimEnumerator_get_Entry_m3400644545_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<PushType,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1466679104_gshared (ShimEnumerator_t336612661 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1466679104(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t336612661 *, const MethodInfo*))ShimEnumerator_get_Key_m1466679104_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<PushType,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1777782226_gshared (ShimEnumerator_t336612661 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1777782226(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t336612661 *, const MethodInfo*))ShimEnumerator_get_Value_m1777782226_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<PushType,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m476426202_gshared (ShimEnumerator_t336612661 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m476426202(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t336612661 *, const MethodInfo*))ShimEnumerator_get_Current_m476426202_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<PushType,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m881328302_gshared (ShimEnumerator_t336612661 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m881328302(__this, method) ((  void (*) (ShimEnumerator_t336612661 *, const MethodInfo*))ShimEnumerator_Reset_m881328302_gshared)(__this, method)
