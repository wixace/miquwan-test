﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.RVO.IAgent
struct IAgent_t2802767396;
// Pathfinding.RVO.Simulator
struct Simulator_t2705969170;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_LayerMask3236759763.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_RVOLayer1386980398.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RVO.RVOController
struct  RVOController_t1639282485  : public MonoBehaviour_t667441552
{
public:
	// System.Single Pathfinding.RVO.RVOController::radius
	float ___radius_2;
	// System.Single Pathfinding.RVO.RVOController::maxSpeed
	float ___maxSpeed_3;
	// System.Single Pathfinding.RVO.RVOController::height
	float ___height_4;
	// System.Boolean Pathfinding.RVO.RVOController::locked
	bool ___locked_5;
	// System.Boolean Pathfinding.RVO.RVOController::lockWhenNotMoving
	bool ___lockWhenNotMoving_6;
	// System.Single Pathfinding.RVO.RVOController::agentTimeHorizon
	float ___agentTimeHorizon_7;
	// System.Single Pathfinding.RVO.RVOController::obstacleTimeHorizon
	float ___obstacleTimeHorizon_8;
	// System.Single Pathfinding.RVO.RVOController::neighbourDist
	float ___neighbourDist_9;
	// System.Int32 Pathfinding.RVO.RVOController::maxNeighbours
	int32_t ___maxNeighbours_10;
	// UnityEngine.LayerMask Pathfinding.RVO.RVOController::mask
	LayerMask_t3236759763  ___mask_11;
	// Pathfinding.RVO.RVOLayer Pathfinding.RVO.RVOController::layer
	int32_t ___layer_12;
	// Pathfinding.RVO.RVOLayer Pathfinding.RVO.RVOController::collidesWith
	int32_t ___collidesWith_13;
	// System.Single Pathfinding.RVO.RVOController::wallAvoidForce
	float ___wallAvoidForce_14;
	// System.Single Pathfinding.RVO.RVOController::wallAvoidFalloff
	float ___wallAvoidFalloff_15;
	// UnityEngine.Vector3 Pathfinding.RVO.RVOController::center
	Vector3_t4282066566  ___center_16;
	// Pathfinding.RVO.IAgent Pathfinding.RVO.RVOController::rvoAgent
	Il2CppObject * ___rvoAgent_17;
	// System.Boolean Pathfinding.RVO.RVOController::enableRotation
	bool ___enableRotation_18;
	// System.Single Pathfinding.RVO.RVOController::rotationSpeed
	float ___rotationSpeed_19;
	// Pathfinding.RVO.Simulator Pathfinding.RVO.RVOController::simulator
	Simulator_t2705969170 * ___simulator_20;
	// System.Single Pathfinding.RVO.RVOController::adjustedY
	float ___adjustedY_21;
	// UnityEngine.Transform Pathfinding.RVO.RVOController::tr
	Transform_t1659122786 * ___tr_22;
	// UnityEngine.Vector3 Pathfinding.RVO.RVOController::desiredVelocity
	Vector3_t4282066566  ___desiredVelocity_23;
	// System.Boolean Pathfinding.RVO.RVOController::debug
	bool ___debug_24;
	// UnityEngine.Vector3 Pathfinding.RVO.RVOController::lastPosition
	Vector3_t4282066566  ___lastPosition_25;

public:
	inline static int32_t get_offset_of_radius_2() { return static_cast<int32_t>(offsetof(RVOController_t1639282485, ___radius_2)); }
	inline float get_radius_2() const { return ___radius_2; }
	inline float* get_address_of_radius_2() { return &___radius_2; }
	inline void set_radius_2(float value)
	{
		___radius_2 = value;
	}

	inline static int32_t get_offset_of_maxSpeed_3() { return static_cast<int32_t>(offsetof(RVOController_t1639282485, ___maxSpeed_3)); }
	inline float get_maxSpeed_3() const { return ___maxSpeed_3; }
	inline float* get_address_of_maxSpeed_3() { return &___maxSpeed_3; }
	inline void set_maxSpeed_3(float value)
	{
		___maxSpeed_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(RVOController_t1639282485, ___height_4)); }
	inline float get_height_4() const { return ___height_4; }
	inline float* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(float value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_locked_5() { return static_cast<int32_t>(offsetof(RVOController_t1639282485, ___locked_5)); }
	inline bool get_locked_5() const { return ___locked_5; }
	inline bool* get_address_of_locked_5() { return &___locked_5; }
	inline void set_locked_5(bool value)
	{
		___locked_5 = value;
	}

	inline static int32_t get_offset_of_lockWhenNotMoving_6() { return static_cast<int32_t>(offsetof(RVOController_t1639282485, ___lockWhenNotMoving_6)); }
	inline bool get_lockWhenNotMoving_6() const { return ___lockWhenNotMoving_6; }
	inline bool* get_address_of_lockWhenNotMoving_6() { return &___lockWhenNotMoving_6; }
	inline void set_lockWhenNotMoving_6(bool value)
	{
		___lockWhenNotMoving_6 = value;
	}

	inline static int32_t get_offset_of_agentTimeHorizon_7() { return static_cast<int32_t>(offsetof(RVOController_t1639282485, ___agentTimeHorizon_7)); }
	inline float get_agentTimeHorizon_7() const { return ___agentTimeHorizon_7; }
	inline float* get_address_of_agentTimeHorizon_7() { return &___agentTimeHorizon_7; }
	inline void set_agentTimeHorizon_7(float value)
	{
		___agentTimeHorizon_7 = value;
	}

	inline static int32_t get_offset_of_obstacleTimeHorizon_8() { return static_cast<int32_t>(offsetof(RVOController_t1639282485, ___obstacleTimeHorizon_8)); }
	inline float get_obstacleTimeHorizon_8() const { return ___obstacleTimeHorizon_8; }
	inline float* get_address_of_obstacleTimeHorizon_8() { return &___obstacleTimeHorizon_8; }
	inline void set_obstacleTimeHorizon_8(float value)
	{
		___obstacleTimeHorizon_8 = value;
	}

	inline static int32_t get_offset_of_neighbourDist_9() { return static_cast<int32_t>(offsetof(RVOController_t1639282485, ___neighbourDist_9)); }
	inline float get_neighbourDist_9() const { return ___neighbourDist_9; }
	inline float* get_address_of_neighbourDist_9() { return &___neighbourDist_9; }
	inline void set_neighbourDist_9(float value)
	{
		___neighbourDist_9 = value;
	}

	inline static int32_t get_offset_of_maxNeighbours_10() { return static_cast<int32_t>(offsetof(RVOController_t1639282485, ___maxNeighbours_10)); }
	inline int32_t get_maxNeighbours_10() const { return ___maxNeighbours_10; }
	inline int32_t* get_address_of_maxNeighbours_10() { return &___maxNeighbours_10; }
	inline void set_maxNeighbours_10(int32_t value)
	{
		___maxNeighbours_10 = value;
	}

	inline static int32_t get_offset_of_mask_11() { return static_cast<int32_t>(offsetof(RVOController_t1639282485, ___mask_11)); }
	inline LayerMask_t3236759763  get_mask_11() const { return ___mask_11; }
	inline LayerMask_t3236759763 * get_address_of_mask_11() { return &___mask_11; }
	inline void set_mask_11(LayerMask_t3236759763  value)
	{
		___mask_11 = value;
	}

	inline static int32_t get_offset_of_layer_12() { return static_cast<int32_t>(offsetof(RVOController_t1639282485, ___layer_12)); }
	inline int32_t get_layer_12() const { return ___layer_12; }
	inline int32_t* get_address_of_layer_12() { return &___layer_12; }
	inline void set_layer_12(int32_t value)
	{
		___layer_12 = value;
	}

	inline static int32_t get_offset_of_collidesWith_13() { return static_cast<int32_t>(offsetof(RVOController_t1639282485, ___collidesWith_13)); }
	inline int32_t get_collidesWith_13() const { return ___collidesWith_13; }
	inline int32_t* get_address_of_collidesWith_13() { return &___collidesWith_13; }
	inline void set_collidesWith_13(int32_t value)
	{
		___collidesWith_13 = value;
	}

	inline static int32_t get_offset_of_wallAvoidForce_14() { return static_cast<int32_t>(offsetof(RVOController_t1639282485, ___wallAvoidForce_14)); }
	inline float get_wallAvoidForce_14() const { return ___wallAvoidForce_14; }
	inline float* get_address_of_wallAvoidForce_14() { return &___wallAvoidForce_14; }
	inline void set_wallAvoidForce_14(float value)
	{
		___wallAvoidForce_14 = value;
	}

	inline static int32_t get_offset_of_wallAvoidFalloff_15() { return static_cast<int32_t>(offsetof(RVOController_t1639282485, ___wallAvoidFalloff_15)); }
	inline float get_wallAvoidFalloff_15() const { return ___wallAvoidFalloff_15; }
	inline float* get_address_of_wallAvoidFalloff_15() { return &___wallAvoidFalloff_15; }
	inline void set_wallAvoidFalloff_15(float value)
	{
		___wallAvoidFalloff_15 = value;
	}

	inline static int32_t get_offset_of_center_16() { return static_cast<int32_t>(offsetof(RVOController_t1639282485, ___center_16)); }
	inline Vector3_t4282066566  get_center_16() const { return ___center_16; }
	inline Vector3_t4282066566 * get_address_of_center_16() { return &___center_16; }
	inline void set_center_16(Vector3_t4282066566  value)
	{
		___center_16 = value;
	}

	inline static int32_t get_offset_of_rvoAgent_17() { return static_cast<int32_t>(offsetof(RVOController_t1639282485, ___rvoAgent_17)); }
	inline Il2CppObject * get_rvoAgent_17() const { return ___rvoAgent_17; }
	inline Il2CppObject ** get_address_of_rvoAgent_17() { return &___rvoAgent_17; }
	inline void set_rvoAgent_17(Il2CppObject * value)
	{
		___rvoAgent_17 = value;
		Il2CppCodeGenWriteBarrier(&___rvoAgent_17, value);
	}

	inline static int32_t get_offset_of_enableRotation_18() { return static_cast<int32_t>(offsetof(RVOController_t1639282485, ___enableRotation_18)); }
	inline bool get_enableRotation_18() const { return ___enableRotation_18; }
	inline bool* get_address_of_enableRotation_18() { return &___enableRotation_18; }
	inline void set_enableRotation_18(bool value)
	{
		___enableRotation_18 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_19() { return static_cast<int32_t>(offsetof(RVOController_t1639282485, ___rotationSpeed_19)); }
	inline float get_rotationSpeed_19() const { return ___rotationSpeed_19; }
	inline float* get_address_of_rotationSpeed_19() { return &___rotationSpeed_19; }
	inline void set_rotationSpeed_19(float value)
	{
		___rotationSpeed_19 = value;
	}

	inline static int32_t get_offset_of_simulator_20() { return static_cast<int32_t>(offsetof(RVOController_t1639282485, ___simulator_20)); }
	inline Simulator_t2705969170 * get_simulator_20() const { return ___simulator_20; }
	inline Simulator_t2705969170 ** get_address_of_simulator_20() { return &___simulator_20; }
	inline void set_simulator_20(Simulator_t2705969170 * value)
	{
		___simulator_20 = value;
		Il2CppCodeGenWriteBarrier(&___simulator_20, value);
	}

	inline static int32_t get_offset_of_adjustedY_21() { return static_cast<int32_t>(offsetof(RVOController_t1639282485, ___adjustedY_21)); }
	inline float get_adjustedY_21() const { return ___adjustedY_21; }
	inline float* get_address_of_adjustedY_21() { return &___adjustedY_21; }
	inline void set_adjustedY_21(float value)
	{
		___adjustedY_21 = value;
	}

	inline static int32_t get_offset_of_tr_22() { return static_cast<int32_t>(offsetof(RVOController_t1639282485, ___tr_22)); }
	inline Transform_t1659122786 * get_tr_22() const { return ___tr_22; }
	inline Transform_t1659122786 ** get_address_of_tr_22() { return &___tr_22; }
	inline void set_tr_22(Transform_t1659122786 * value)
	{
		___tr_22 = value;
		Il2CppCodeGenWriteBarrier(&___tr_22, value);
	}

	inline static int32_t get_offset_of_desiredVelocity_23() { return static_cast<int32_t>(offsetof(RVOController_t1639282485, ___desiredVelocity_23)); }
	inline Vector3_t4282066566  get_desiredVelocity_23() const { return ___desiredVelocity_23; }
	inline Vector3_t4282066566 * get_address_of_desiredVelocity_23() { return &___desiredVelocity_23; }
	inline void set_desiredVelocity_23(Vector3_t4282066566  value)
	{
		___desiredVelocity_23 = value;
	}

	inline static int32_t get_offset_of_debug_24() { return static_cast<int32_t>(offsetof(RVOController_t1639282485, ___debug_24)); }
	inline bool get_debug_24() const { return ___debug_24; }
	inline bool* get_address_of_debug_24() { return &___debug_24; }
	inline void set_debug_24(bool value)
	{
		___debug_24 = value;
	}

	inline static int32_t get_offset_of_lastPosition_25() { return static_cast<int32_t>(offsetof(RVOController_t1639282485, ___lastPosition_25)); }
	inline Vector3_t4282066566  get_lastPosition_25() const { return ___lastPosition_25; }
	inline Vector3_t4282066566 * get_address_of_lastPosition_25() { return &___lastPosition_25; }
	inline void set_lastPosition_25(Vector3_t4282066566  value)
	{
		___lastPosition_25 = value;
	}
};

struct RVOController_t1639282485_StaticFields
{
public:
	// UnityEngine.Color Pathfinding.RVO.RVOController::GizmoColor
	Color_t4194546905  ___GizmoColor_26;

public:
	inline static int32_t get_offset_of_GizmoColor_26() { return static_cast<int32_t>(offsetof(RVOController_t1639282485_StaticFields, ___GizmoColor_26)); }
	inline Color_t4194546905  get_GizmoColor_26() const { return ___GizmoColor_26; }
	inline Color_t4194546905 * get_address_of_GizmoColor_26() { return &___GizmoColor_26; }
	inline void set_GizmoColor_26(Color_t4194546905  value)
	{
		___GizmoColor_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
