﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<CameraAniMap>
struct List_1_t77874261;
// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraAnis
struct  CameraAnis_t4275747580  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<CameraAniMap> CameraAnis::_cameraMaps
	List_1_t77874261 * ____cameraMaps_0;
	// ProtoBuf.IExtension CameraAnis::extensionObject
	Il2CppObject * ___extensionObject_1;

public:
	inline static int32_t get_offset_of__cameraMaps_0() { return static_cast<int32_t>(offsetof(CameraAnis_t4275747580, ____cameraMaps_0)); }
	inline List_1_t77874261 * get__cameraMaps_0() const { return ____cameraMaps_0; }
	inline List_1_t77874261 ** get_address_of__cameraMaps_0() { return &____cameraMaps_0; }
	inline void set__cameraMaps_0(List_1_t77874261 * value)
	{
		____cameraMaps_0 = value;
		Il2CppCodeGenWriteBarrier(&____cameraMaps_0, value);
	}

	inline static int32_t get_offset_of_extensionObject_1() { return static_cast<int32_t>(offsetof(CameraAnis_t4275747580, ___extensionObject_1)); }
	inline Il2CppObject * get_extensionObject_1() const { return ___extensionObject_1; }
	inline Il2CppObject ** get_address_of_extensionObject_1() { return &___extensionObject_1; }
	inline void set_extensionObject_1(Il2CppObject * value)
	{
		___extensionObject_1 = value;
		Il2CppCodeGenWriteBarrier(&___extensionObject_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
