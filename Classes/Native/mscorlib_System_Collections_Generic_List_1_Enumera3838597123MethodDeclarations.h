﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.CrashReport>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1881107851(__this, ___l0, method) ((  void (*) (Enumerator_t3838597123 *, List_1_t3818924353 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.CrashReport>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1063132135(__this, method) ((  void (*) (Enumerator_t3838597123 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.CrashReport>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1660901907(__this, method) ((  Il2CppObject * (*) (Enumerator_t3838597123 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.CrashReport>::Dispose()
#define Enumerator_Dispose_m1395379760(__this, method) ((  void (*) (Enumerator_t3838597123 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.CrashReport>::VerifyState()
#define Enumerator_VerifyState_m2288842089(__this, method) ((  void (*) (Enumerator_t3838597123 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.CrashReport>::MoveNext()
#define Enumerator_MoveNext_m3243337427(__this, method) ((  bool (*) (Enumerator_t3838597123 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.CrashReport>::get_Current()
#define Enumerator_get_Current_m3489979040(__this, method) ((  CrashReport_t2450738801 * (*) (Enumerator_t3838597123 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
