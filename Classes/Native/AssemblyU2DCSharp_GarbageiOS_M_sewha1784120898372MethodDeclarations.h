﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_sewha178
struct M_sewha178_t4120898372;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_sewha1784120898372.h"

// System.Void GarbageiOS.M_sewha178::.ctor()
extern "C"  void M_sewha178__ctor_m344614767 (M_sewha178_t4120898372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sewha178::M_yaibostaChaspaw0(System.String[],System.Int32)
extern "C"  void M_sewha178_M_yaibostaChaspaw0_m4112408753 (M_sewha178_t4120898372 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sewha178::M_jalrere1(System.String[],System.Int32)
extern "C"  void M_sewha178_M_jalrere1_m1861382292 (M_sewha178_t4120898372 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sewha178::M_kawvecuCaymouxe2(System.String[],System.Int32)
extern "C"  void M_sewha178_M_kawvecuCaymouxe2_m3145602921 (M_sewha178_t4120898372 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sewha178::M_salaiSalri3(System.String[],System.Int32)
extern "C"  void M_sewha178_M_salaiSalri3_m958678614 (M_sewha178_t4120898372 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sewha178::M_cocee4(System.String[],System.Int32)
extern "C"  void M_sewha178_M_cocee4_m70796859 (M_sewha178_t4120898372 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sewha178::ilo_M_yaibostaChaspaw01(GarbageiOS.M_sewha178,System.String[],System.Int32)
extern "C"  void M_sewha178_ilo_M_yaibostaChaspaw01_m3943196813 (Il2CppObject * __this /* static, unused */, M_sewha178_t4120898372 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sewha178::ilo_M_jalrere12(GarbageiOS.M_sewha178,System.String[],System.Int32)
extern "C"  void M_sewha178_ilo_M_jalrere12_m2131597259 (Il2CppObject * __this /* static, unused */, M_sewha178_t4120898372 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
