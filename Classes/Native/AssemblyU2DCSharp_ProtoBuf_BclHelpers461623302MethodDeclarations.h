﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// ProtoBuf.NetObjectCache
struct NetObjectCache_t514806898;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_Decimal1954350631.h"
#include "mscorlib_System_Guid2862754429.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_BclHelpers_NetObjectOpt2130952596.h"
#include "AssemblyU2DCSharp_ProtoBuf_WireType2355646059.h"
#include "AssemblyU2DCSharp_ProtoBuf_SubItemToken3365128146.h"
#include "AssemblyU2DCSharp_ProtoBuf_NetObjectCache514806898.h"

// System.Void ProtoBuf.BclHelpers::.cctor()
extern "C"  void BclHelpers__cctor_m1778023295 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.BclHelpers::GetUninitializedObject(System.Type)
extern "C"  Il2CppObject * BclHelpers_GetUninitializedObject_m3137451284 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.BclHelpers::WriteTimeSpan(System.TimeSpan,ProtoBuf.ProtoWriter)
extern "C"  void BclHelpers_WriteTimeSpan_m2695641780 (Il2CppObject * __this /* static, unused */, TimeSpan_t413522987  ___timeSpan0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan ProtoBuf.BclHelpers::ReadTimeSpan(ProtoBuf.ProtoReader)
extern "C"  TimeSpan_t413522987  BclHelpers_ReadTimeSpan_m831005982 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime ProtoBuf.BclHelpers::ReadDateTime(ProtoBuf.ProtoReader)
extern "C"  DateTime_t4283661327  BclHelpers_ReadDateTime_m1877845478 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.BclHelpers::WriteDateTime(System.DateTime,ProtoBuf.ProtoWriter)
extern "C"  void BclHelpers_WriteDateTime_m2505026684 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ProtoBuf.BclHelpers::ReadTimeSpanTicks(ProtoBuf.ProtoReader)
extern "C"  int64_t BclHelpers_ReadTimeSpanTicks_m2414826652 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal ProtoBuf.BclHelpers::ReadDecimal(ProtoBuf.ProtoReader)
extern "C"  Decimal_t1954350631  BclHelpers_ReadDecimal_m3169418890 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.BclHelpers::WriteDecimal(System.Decimal,ProtoBuf.ProtoWriter)
extern "C"  void BclHelpers_WriteDecimal_m3786975832 (Il2CppObject * __this /* static, unused */, Decimal_t1954350631  ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.BclHelpers::WriteGuid(System.Guid,ProtoBuf.ProtoWriter)
extern "C"  void BclHelpers_WriteGuid_m1699404632 (Il2CppObject * __this /* static, unused */, Guid_t2862754429  ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid ProtoBuf.BclHelpers::ReadGuid(ProtoBuf.ProtoReader)
extern "C"  Guid_t2862754429  BclHelpers_ReadGuid_m214883650 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.BclHelpers::ReadNetObject(System.Object,ProtoBuf.ProtoReader,System.Int32,System.Type,ProtoBuf.BclHelpers/NetObjectOptions)
extern "C"  Il2CppObject * BclHelpers_ReadNetObject_m4073431641 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, int32_t ___key2, Type_t * ___type3, uint8_t ___options4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.BclHelpers::WriteNetObject(System.Object,ProtoBuf.ProtoWriter,System.Int32,ProtoBuf.BclHelpers/NetObjectOptions)
extern "C"  void BclHelpers_WriteNetObject_m2487659560 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, int32_t ___key2, uint8_t ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.WireType ProtoBuf.BclHelpers::ilo_get_WireType1(ProtoBuf.ProtoWriter)
extern "C"  int32_t BclHelpers_ilo_get_WireType1_m1329707099 (Il2CppObject * __this /* static, unused */, ProtoWriter_t4117914721 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.SubItemToken ProtoBuf.BclHelpers::ilo_StartSubItem2(System.Object,ProtoBuf.ProtoWriter)
extern "C"  SubItemToken_t3365128146  BclHelpers_ilo_StartSubItem2_m1864467678 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___instance0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.WireType ProtoBuf.BclHelpers::ilo_get_WireType3(ProtoBuf.ProtoReader)
extern "C"  int32_t BclHelpers_ilo_get_WireType3_m3296641165 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.BclHelpers::ilo_EndSubItem4(ProtoBuf.SubItemToken,ProtoBuf.ProtoReader)
extern "C"  void BclHelpers_ilo_EndSubItem4_m817035722 (Il2CppObject * __this /* static, unused */, SubItemToken_t3365128146  ___token0, ProtoReader_t3962509489 * ___reader1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.SubItemToken ProtoBuf.BclHelpers::ilo_StartSubItem5(ProtoBuf.ProtoReader)
extern "C"  SubItemToken_t3365128146  BclHelpers_ilo_StartSubItem5_m439574751 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 ProtoBuf.BclHelpers::ilo_ReadUInt646(ProtoBuf.ProtoReader)
extern "C"  uint64_t BclHelpers_ilo_ReadUInt646_m1191309333 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ProtoBuf.BclHelpers::ilo_ReadUInt327(ProtoBuf.ProtoReader)
extern "C"  uint32_t BclHelpers_ilo_ReadUInt327_m3510045780 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.BclHelpers::ilo_WriteFieldHeader8(System.Int32,ProtoBuf.WireType,ProtoBuf.ProtoWriter)
extern "C"  void BclHelpers_ilo_WriteFieldHeader8_m3105009194 (Il2CppObject * __this /* static, unused */, int32_t ___fieldNumber0, int32_t ___wireType1, ProtoWriter_t4117914721 * ___writer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.BclHelpers::ilo_EndSubItem9(ProtoBuf.SubItemToken,ProtoBuf.ProtoWriter)
extern "C"  void BclHelpers_ilo_EndSubItem9_m1425862815 (Il2CppObject * __this /* static, unused */, SubItemToken_t3365128146  ___token0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.BclHelpers::ilo_GetKeyedObject10(ProtoBuf.NetObjectCache,System.Int32)
extern "C"  Il2CppObject * BclHelpers_ilo_GetKeyedObject10_m2522489448 (Il2CppObject * __this /* static, unused */, NetObjectCache_t514806898 * ____this0, int32_t ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.BclHelpers::ilo_ReadInt3211(ProtoBuf.ProtoReader)
extern "C"  int32_t BclHelpers_ilo_ReadInt3211_m3229478263 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.NetObjectCache ProtoBuf.BclHelpers::ilo_get_NetCache12(ProtoBuf.ProtoReader)
extern "C"  NetObjectCache_t514806898 * BclHelpers_ilo_get_NetCache12_m2172613094 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.BclHelpers::ilo_GetTypeKey13(ProtoBuf.ProtoReader,System.Type&)
extern "C"  int32_t BclHelpers_ilo_GetTypeKey13_m1044773589 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, Type_t ** ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.BclHelpers::ilo_TrapNextObject14(ProtoBuf.ProtoReader,System.Int32)
extern "C"  void BclHelpers_ilo_TrapNextObject14_m1287737460 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, int32_t ___newObjectKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.BclHelpers::ilo_ReadString15(ProtoBuf.ProtoReader)
extern "C"  String_t* BclHelpers_ilo_ReadString15_m2265167017 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.BclHelpers::ilo_ReadTypedObject16(System.Object,System.Int32,ProtoBuf.ProtoReader,System.Type)
extern "C"  Il2CppObject * BclHelpers_ilo_ReadTypedObject16_m3297612776 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, int32_t ___key1, ProtoReader_t3962509489 * ___reader2, Type_t * ___type3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.BclHelpers::ilo_SetKeyedObject17(ProtoBuf.NetObjectCache,System.Int32,System.Object)
extern "C"  void BclHelpers_ilo_SetKeyedObject17_m2485363398 (Il2CppObject * __this /* static, unused */, NetObjectCache_t514806898 * ____this0, int32_t ___key1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.BclHelpers::ilo_WriteInt3218(System.Int32,ProtoBuf.ProtoWriter)
extern "C"  void BclHelpers_ilo_WriteInt3218_m2177271262 (Il2CppObject * __this /* static, unused */, int32_t ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
