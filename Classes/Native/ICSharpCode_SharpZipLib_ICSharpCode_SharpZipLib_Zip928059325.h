﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ICSharpCode.SharpZipLib.Zip.Compression.Inflater
struct Inflater_t1975778921;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer
struct InflaterInputBuffer_t441930877;
// System.IO.Stream
struct Stream_t1561764144;

#include "mscorlib_System_IO_Stream1561764144.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream
struct  InflaterInputStream_t928059325  : public Stream_t1561764144
{
public:
	// ICSharpCode.SharpZipLib.Zip.Compression.Inflater ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::inf
	Inflater_t1975778921 * ___inf_1;
	// ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::inputBuffer
	InflaterInputBuffer_t441930877 * ___inputBuffer_2;
	// System.IO.Stream ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::baseInputStream
	Stream_t1561764144 * ___baseInputStream_3;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::isClosed
	bool ___isClosed_4;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::isStreamOwner
	bool ___isStreamOwner_5;

public:
	inline static int32_t get_offset_of_inf_1() { return static_cast<int32_t>(offsetof(InflaterInputStream_t928059325, ___inf_1)); }
	inline Inflater_t1975778921 * get_inf_1() const { return ___inf_1; }
	inline Inflater_t1975778921 ** get_address_of_inf_1() { return &___inf_1; }
	inline void set_inf_1(Inflater_t1975778921 * value)
	{
		___inf_1 = value;
		Il2CppCodeGenWriteBarrier(&___inf_1, value);
	}

	inline static int32_t get_offset_of_inputBuffer_2() { return static_cast<int32_t>(offsetof(InflaterInputStream_t928059325, ___inputBuffer_2)); }
	inline InflaterInputBuffer_t441930877 * get_inputBuffer_2() const { return ___inputBuffer_2; }
	inline InflaterInputBuffer_t441930877 ** get_address_of_inputBuffer_2() { return &___inputBuffer_2; }
	inline void set_inputBuffer_2(InflaterInputBuffer_t441930877 * value)
	{
		___inputBuffer_2 = value;
		Il2CppCodeGenWriteBarrier(&___inputBuffer_2, value);
	}

	inline static int32_t get_offset_of_baseInputStream_3() { return static_cast<int32_t>(offsetof(InflaterInputStream_t928059325, ___baseInputStream_3)); }
	inline Stream_t1561764144 * get_baseInputStream_3() const { return ___baseInputStream_3; }
	inline Stream_t1561764144 ** get_address_of_baseInputStream_3() { return &___baseInputStream_3; }
	inline void set_baseInputStream_3(Stream_t1561764144 * value)
	{
		___baseInputStream_3 = value;
		Il2CppCodeGenWriteBarrier(&___baseInputStream_3, value);
	}

	inline static int32_t get_offset_of_isClosed_4() { return static_cast<int32_t>(offsetof(InflaterInputStream_t928059325, ___isClosed_4)); }
	inline bool get_isClosed_4() const { return ___isClosed_4; }
	inline bool* get_address_of_isClosed_4() { return &___isClosed_4; }
	inline void set_isClosed_4(bool value)
	{
		___isClosed_4 = value;
	}

	inline static int32_t get_offset_of_isStreamOwner_5() { return static_cast<int32_t>(offsetof(InflaterInputStream_t928059325, ___isStreamOwner_5)); }
	inline bool get_isStreamOwner_5() const { return ___isStreamOwner_5; }
	inline bool* get_address_of_isStreamOwner_5() { return &___isStreamOwner_5; }
	inline void set_isStreamOwner_5(bool value)
	{
		___isStreamOwner_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
