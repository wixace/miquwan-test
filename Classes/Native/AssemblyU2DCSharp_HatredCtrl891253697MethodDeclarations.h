﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HatredCtrl
struct HatredCtrl_t891253697;
// CombatEntity
struct CombatEntity_t684137495;
// Skill
struct Skill_t79944241;
// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;
// NpcMgr
struct NpcMgr_t2339534743;
// GameMgr
struct GameMgr_t1469029542;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Skill79944241.h"
#include "AssemblyU2DCSharp_NpcMgr2339534743.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"
#include "AssemblyU2DCSharp_HatredCtrl891253697.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitCamp642829979.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitType643359572.h"

// System.Void HatredCtrl::.ctor(CombatEntity)
extern "C"  void HatredCtrl__ctor_m931464003 (HatredCtrl_t891253697 * __this, CombatEntity_t684137495 * ___entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HatredCtrl::InitHatred()
extern "C"  void HatredCtrl_InitHatred_m4105794832 (HatredCtrl_t891253697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HatredCtrl::InitHatredByNormal()
extern "C"  void HatredCtrl_InitHatredByNormal_m985209550 (HatredCtrl_t891253697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HatredCtrl::InitHatredByCampfight()
extern "C"  void HatredCtrl_InitHatredByCampfight_m1111657226 (HatredCtrl_t891253697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HatredCtrl::AddHatredAllEnemy(System.Single)
extern "C"  void HatredCtrl_AddHatredAllEnemy_m506256565 (HatredCtrl_t891253697 * __this, float ___hatred0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HatredCtrl::RemoveHatredAllEnemy()
extern "C"  void HatredCtrl_RemoveHatredAllEnemy_m2289210379 (HatredCtrl_t891253697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HatredCtrl::RemoveHatredEnity(CombatEntity)
extern "C"  void HatredCtrl_RemoveHatredEnity_m1784948588 (HatredCtrl_t891253697 * __this, CombatEntity_t684137495 * ___entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HatredCtrl::AddHatred(CombatEntity,System.Single)
extern "C"  void HatredCtrl_AddHatred_m2771468445 (HatredCtrl_t891253697 * __this, CombatEntity_t684137495 * ___entity0, float ___hatred1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HatredCtrl::ReduceHatred(CombatEntity,System.Single)
extern "C"  void HatredCtrl_ReduceHatred_m3021459732 (HatredCtrl_t891253697 * __this, CombatEntity_t684137495 * ___entity0, float ___hatred1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HatredCtrl::GetAimHatred(CombatEntity)
extern "C"  float HatredCtrl_GetAimHatred_m2795871844 (HatredCtrl_t891253697 * __this, CombatEntity_t684137495 * ___entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HatredCtrl::GetMaxHatredValue()
extern "C"  float HatredCtrl_GetMaxHatredValue_m3854595833 (HatredCtrl_t891253697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity HatredCtrl::GetMaxHatredEntity()
extern "C"  CombatEntity_t684137495 * HatredCtrl_GetMaxHatredEntity_m2382389367 (HatredCtrl_t891253697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity HatredCtrl::GetMinHatredEntity(Skill)
extern "C"  CombatEntity_t684137495 * HatredCtrl_GetMinHatredEntity_m1954699484 (HatredCtrl_t891253697 * __this, Skill_t79944241 * ___skill0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HatredCtrl::GetComputHatredLevel(CombatEntity)
extern "C"  int32_t HatredCtrl_GetComputHatredLevel_m651225323 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ___entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HatredCtrl::GetConvertHatred(System.Int32,System.Int32)
extern "C"  float HatredCtrl_GetConvertHatred_m3950882237 (Il2CppObject * __this /* static, unused */, int32_t ___value0, int32_t ___coeff1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HatredCtrl::GetTauntHatred(System.Single,CombatEntity)
extern "C"  float HatredCtrl_GetTauntHatred_m368790280 (Il2CppObject * __this /* static, unused */, float ___baseValue0, CombatEntity_t684137495 * ___entity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HatredCtrl::GetSeleteAimHatred()
extern "C"  float HatredCtrl_GetSeleteAimHatred_m3282901695 (HatredCtrl_t891253697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HatredCtrl::UpdateAttackTarge()
extern "C"  void HatredCtrl_UpdateAttackTarge_m2568178026 (HatredCtrl_t891253697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HatredCtrl::ChangeAttackTarge()
extern "C"  void HatredCtrl_ChangeAttackTarge_m4040138211 (HatredCtrl_t891253697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HatredCtrl::ClearTarge()
extern "C"  void HatredCtrl_ClearTarge_m90267840 (HatredCtrl_t891253697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HatredCtrl::Clear()
extern "C"  void HatredCtrl_Clear_m1210092069 (HatredCtrl_t891253697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> HatredCtrl::ilo_GetAllAniList1(NpcMgr,CombatEntity)
extern "C"  List_1_t2052323047 * HatredCtrl_ilo_GetAllAniList1_m1077305789 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, CombatEntity_t684137495 * ___entity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> HatredCtrl::ilo_GetAllEnemy2(GameMgr,CombatEntity)
extern "C"  List_1_t2052323047 * HatredCtrl_ilo_GetAllEnemy2_m665746207 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, CombatEntity_t684137495 * ___entity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HatredCtrl::ilo_GetComputHatredLevel3(CombatEntity)
extern "C"  int32_t HatredCtrl_ilo_GetComputHatredLevel3_m1681929769 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ___entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HatredCtrl::ilo_AddHatred4(HatredCtrl,CombatEntity,System.Single)
extern "C"  void HatredCtrl_ilo_AddHatred4_m634541947 (Il2CppObject * __this /* static, unused */, HatredCtrl_t891253697 * ____this0, CombatEntity_t684137495 * ___entity1, float ___hatred2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HatredCtrl::ilo_ChangeAttackTarge5(HatredCtrl)
extern "C"  void HatredCtrl_ilo_ChangeAttackTarge5_m1829584998 (Il2CppObject * __this /* static, unused */, HatredCtrl_t891253697 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitCamp HatredCtrl::ilo_get_unitCamp6(CombatEntity)
extern "C"  int32_t HatredCtrl_ilo_get_unitCamp6_m3794777682 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HatredCtrl::ilo_get_isNotSelected7(CombatEntity)
extern "C"  bool HatredCtrl_ilo_get_isNotSelected7_m3383365270 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HatredCtrl::ilo_get_isDeath8(CombatEntity)
extern "C"  bool HatredCtrl_ilo_get_isDeath8_m583283313 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HatredCtrl::ilo_get_halfwidth9(CombatEntity)
extern "C"  float HatredCtrl_ilo_get_halfwidth9_m532798305 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitType HatredCtrl::ilo_get_unitType10(CombatEntity)
extern "C"  int32_t HatredCtrl_ilo_get_unitType10_m1034667959 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
