﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<Pathfinding.Int2>
struct EqualityComparer_1_t1081882474;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.Int2>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1377140408_gshared (EqualityComparer_1_t1081882474 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m1377140408(__this, method) ((  void (*) (EqualityComparer_1_t1081882474 *, const MethodInfo*))EqualityComparer_1__ctor_m1377140408_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.Int2>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3554550773_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m3554550773(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m3554550773_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<Pathfinding.Int2>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1094322761_gshared (EqualityComparer_1_t1081882474 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1094322761(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t1081882474 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1094322761_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<Pathfinding.Int2>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3372176981_gshared (EqualityComparer_1_t1081882474 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3372176981(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t1081882474 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3372176981_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Pathfinding.Int2>::get_Default()
extern "C"  EqualityComparer_1_t1081882474 * EqualityComparer_1_get_Default_m520204026_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m520204026(__this /* static, unused */, method) ((  EqualityComparer_1_t1081882474 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m520204026_gshared)(__this /* static, unused */, method)
