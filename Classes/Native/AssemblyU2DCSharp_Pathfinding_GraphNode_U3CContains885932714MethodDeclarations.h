﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.GraphNode/<ContainsConnection>c__AnonStorey10B
struct U3CContainsConnectionU3Ec__AnonStorey10B_t885932714;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.GraphNode/<ContainsConnection>c__AnonStorey10B::.ctor()
extern "C"  void U3CContainsConnectionU3Ec__AnonStorey10B__ctor_m4281541937 (U3CContainsConnectionU3Ec__AnonStorey10B_t885932714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode/<ContainsConnection>c__AnonStorey10B::<>m__33D(Pathfinding.GraphNode)
extern "C"  void U3CContainsConnectionU3Ec__AnonStorey10B_U3CU3Em__33D_m3114582048 (U3CContainsConnectionU3Ec__AnonStorey10B_t885932714 * __this, GraphNode_t23612370 * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
