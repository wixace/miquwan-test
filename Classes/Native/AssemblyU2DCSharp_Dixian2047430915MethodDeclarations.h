﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Dixian
struct Dixian_t2047430915;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// HeroMgr
struct HeroMgr_t2475965342;
// Hero
struct Hero_t2245658;
// System.Collections.Hashtable
struct Hashtable_t1407064410;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Action
struct Action_t3771233898;
// System.Collections.Generic.List`1<Hero>
struct List_1_t1370431210;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_Dixian2047430915.h"
#include "AssemblyU2DCSharp_HeroMgr2475965342.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_Collections_Hashtable1407064410.h"
#include "System_Core_System_Action3771233898.h"
#include "mscorlib_System_String7231557.h"

// System.Void Dixian::.ctor()
extern "C"  void Dixian__ctor_m637896440 (Dixian_t2047430915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Dixian::Awake()
extern "C"  void Dixian_Awake_m875501659 (Dixian_t2047430915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Dixian::FixedUpdate()
extern "C"  void Dixian_FixedUpdate_m1717324403 (Dixian_t2047430915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Dixian::TriggerDistanceFun()
extern "C"  void Dixian_TriggerDistanceFun_m352373758 (Dixian_t2047430915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Dixian::TriggerMonsterDieFun(CEvent.ZEvent)
extern "C"  void Dixian_TriggerMonsterDieFun_m724966926 (Dixian_t2047430915 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Dixian::TriggerFun()
extern "C"  void Dixian_TriggerFun_m2264660467 (Dixian_t2047430915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Dixian::UpdateMovePanel()
extern "C"  void Dixian_UpdateMovePanel_m4073215296 (Dixian_t2047430915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Dixian::Update()
extern "C"  void Dixian_Update_m26815253 (Dixian_t2047430915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Dixian::DestroyGameobject()
extern "C"  void Dixian_DestroyGameobject_m2044578593 (Dixian_t2047430915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Dixian::OnDestroy()
extern "C"  void Dixian_OnDestroy_m2571837425 (Dixian_t2047430915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Dixian::ilo_TriggerDistanceFun1(Dixian)
extern "C"  void Dixian_ilo_TriggerDistanceFun1_m2323962719 (Il2CppObject * __this /* static, unused */, Dixian_t2047430915 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroMgr Dixian::ilo_get_instance2()
extern "C"  HeroMgr_t2475965342 * Dixian_ilo_get_instance2_m1787137890 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero Dixian::ilo_get_captain3(HeroMgr)
extern "C"  Hero_t2245658 * Dixian_ilo_get_captain3_m3089040570 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable Dixian::ilo_Hash4(System.Object[])
extern "C"  Hashtable_t1407064410 * Dixian_ilo_Hash4_m1664152926 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Dixian::ilo_ShakePosition5(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void Dixian_ilo_ShakePosition5_m819347745 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Hashtable_t1407064410 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Dixian::ilo_DelayCall6(System.Single,System.Action)
extern "C"  uint32_t Dixian_ilo_DelayCall6_m1457553619 (Il2CppObject * __this /* static, unused */, float ___delaytime0, Action_t3771233898 * ___act1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Hero> Dixian::ilo_GetAllHeros7(HeroMgr)
extern "C"  List_1_t1370431210 * Dixian_ilo_GetAllHeros7_m1326259167 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Dixian::ilo_RemoveEventListener8(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void Dixian_ilo_RemoveEventListener8_m1837352278 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
