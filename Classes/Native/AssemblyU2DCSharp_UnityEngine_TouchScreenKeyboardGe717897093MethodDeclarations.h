﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_TouchScreenKeyboardGenerated
struct UnityEngine_TouchScreenKeyboardGenerated_t717897093;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_TouchScreenKeyboardGenerated::.ctor()
extern "C"  void UnityEngine_TouchScreenKeyboardGenerated__ctor_m1089767094 (UnityEngine_TouchScreenKeyboardGenerated_t717897093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TouchScreenKeyboardGenerated::TouchScreenKeyboard_TouchScreenKeyboard1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TouchScreenKeyboardGenerated_TouchScreenKeyboard_TouchScreenKeyboard1_m2031906846 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchScreenKeyboardGenerated::TouchScreenKeyboard_isSupported(JSVCall)
extern "C"  void UnityEngine_TouchScreenKeyboardGenerated_TouchScreenKeyboard_isSupported_m715488124 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchScreenKeyboardGenerated::TouchScreenKeyboard_text(JSVCall)
extern "C"  void UnityEngine_TouchScreenKeyboardGenerated_TouchScreenKeyboard_text_m3882043423 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchScreenKeyboardGenerated::TouchScreenKeyboard_hideInput(JSVCall)
extern "C"  void UnityEngine_TouchScreenKeyboardGenerated_TouchScreenKeyboard_hideInput_m550220152 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchScreenKeyboardGenerated::TouchScreenKeyboard_active(JSVCall)
extern "C"  void UnityEngine_TouchScreenKeyboardGenerated_TouchScreenKeyboard_active_m2998427878 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchScreenKeyboardGenerated::TouchScreenKeyboard_done(JSVCall)
extern "C"  void UnityEngine_TouchScreenKeyboardGenerated_TouchScreenKeyboard_done_m719763818 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchScreenKeyboardGenerated::TouchScreenKeyboard_wasCanceled(JSVCall)
extern "C"  void UnityEngine_TouchScreenKeyboardGenerated_TouchScreenKeyboard_wasCanceled_m1421243710 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchScreenKeyboardGenerated::TouchScreenKeyboard_targetDisplay(JSVCall)
extern "C"  void UnityEngine_TouchScreenKeyboardGenerated_TouchScreenKeyboard_targetDisplay_m2628374607 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchScreenKeyboardGenerated::TouchScreenKeyboard_area(JSVCall)
extern "C"  void UnityEngine_TouchScreenKeyboardGenerated_TouchScreenKeyboard_area_m1765968063 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchScreenKeyboardGenerated::TouchScreenKeyboard_visible(JSVCall)
extern "C"  void UnityEngine_TouchScreenKeyboardGenerated_TouchScreenKeyboard_visible_m880666222 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TouchScreenKeyboardGenerated::TouchScreenKeyboard_Open__String__TouchScreenKeyboardType__Boolean__Boolean__Boolean__Boolean__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TouchScreenKeyboardGenerated_TouchScreenKeyboard_Open__String__TouchScreenKeyboardType__Boolean__Boolean__Boolean__Boolean__String_m432372581 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TouchScreenKeyboardGenerated::TouchScreenKeyboard_Open__String__TouchScreenKeyboardType__Boolean__Boolean__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TouchScreenKeyboardGenerated_TouchScreenKeyboard_Open__String__TouchScreenKeyboardType__Boolean__Boolean__Boolean__Boolean_m1233064212 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TouchScreenKeyboardGenerated::TouchScreenKeyboard_Open__String__TouchScreenKeyboardType__Boolean__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TouchScreenKeyboardGenerated_TouchScreenKeyboard_Open__String__TouchScreenKeyboardType__Boolean__Boolean__Boolean_m323706070 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TouchScreenKeyboardGenerated::TouchScreenKeyboard_Open__String__TouchScreenKeyboardType__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TouchScreenKeyboardGenerated_TouchScreenKeyboard_Open__String__TouchScreenKeyboardType__Boolean__Boolean_m1451257556 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TouchScreenKeyboardGenerated::TouchScreenKeyboard_Open__String__TouchScreenKeyboardType__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TouchScreenKeyboardGenerated_TouchScreenKeyboard_Open__String__TouchScreenKeyboardType__Boolean_m3564472086 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TouchScreenKeyboardGenerated::TouchScreenKeyboard_Open__String__TouchScreenKeyboardType(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TouchScreenKeyboardGenerated_TouchScreenKeyboard_Open__String__TouchScreenKeyboardType_m1720827028 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TouchScreenKeyboardGenerated::TouchScreenKeyboard_Open__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TouchScreenKeyboardGenerated_TouchScreenKeyboard_Open__String_m2683908250 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchScreenKeyboardGenerated::__Register()
extern "C"  void UnityEngine_TouchScreenKeyboardGenerated___Register_m2099427281 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_TouchScreenKeyboardGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_TouchScreenKeyboardGenerated_ilo_getObject1_m3845176348 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TouchScreenKeyboardGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_TouchScreenKeyboardGenerated_ilo_attachFinalizerObject2_m4261047850 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TouchScreenKeyboardGenerated::ilo_getBooleanS3(System.Int32)
extern "C"  bool UnityEngine_TouchScreenKeyboardGenerated_ilo_getBooleanS3_m1231355352 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchScreenKeyboardGenerated::ilo_addJSCSRel4(System.Int32,System.Object)
extern "C"  void UnityEngine_TouchScreenKeyboardGenerated_ilo_addJSCSRel4_m1920542709 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchScreenKeyboardGenerated::ilo_setStringS5(System.Int32,System.String)
extern "C"  void UnityEngine_TouchScreenKeyboardGenerated_ilo_setStringS5_m2888943747 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_TouchScreenKeyboardGenerated::ilo_getStringS6(System.Int32)
extern "C"  String_t* UnityEngine_TouchScreenKeyboardGenerated_ilo_getStringS6_m2423334713 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchScreenKeyboardGenerated::ilo_setBooleanS7(System.Int32,System.Boolean)
extern "C"  void UnityEngine_TouchScreenKeyboardGenerated_ilo_setBooleanS7_m2792044129 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_TouchScreenKeyboardGenerated::ilo_getInt328(System.Int32)
extern "C"  int32_t UnityEngine_TouchScreenKeyboardGenerated_ilo_getInt328_m1241204164 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_TouchScreenKeyboardGenerated::ilo_setObject9(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_TouchScreenKeyboardGenerated_ilo_setObject9_m767039900 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_TouchScreenKeyboardGenerated::ilo_getEnum10(System.Int32)
extern "C"  int32_t UnityEngine_TouchScreenKeyboardGenerated_ilo_getEnum10_m102034106 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
