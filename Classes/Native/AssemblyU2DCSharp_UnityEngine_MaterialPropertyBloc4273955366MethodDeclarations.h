﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_MaterialPropertyBlockGenerated
struct UnityEngine_MaterialPropertyBlockGenerated_t4273955366;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_MaterialPropertyBlockGenerated::.ctor()
extern "C"  void UnityEngine_MaterialPropertyBlockGenerated__ctor_m2820333877 (UnityEngine_MaterialPropertyBlockGenerated_t4273955366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialPropertyBlockGenerated::MaterialPropertyBlock_MaterialPropertyBlock1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialPropertyBlockGenerated_MaterialPropertyBlock_MaterialPropertyBlock1_m1580384385 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MaterialPropertyBlockGenerated::MaterialPropertyBlock_isEmpty(JSVCall)
extern "C"  void UnityEngine_MaterialPropertyBlockGenerated_MaterialPropertyBlock_isEmpty_m733516735 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialPropertyBlockGenerated::MaterialPropertyBlock_Clear(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialPropertyBlockGenerated_MaterialPropertyBlock_Clear_m2213684910 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialPropertyBlockGenerated::MaterialPropertyBlock_GetFloat__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialPropertyBlockGenerated_MaterialPropertyBlock_GetFloat__Int32_m2739803465 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialPropertyBlockGenerated::MaterialPropertyBlock_GetFloat__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialPropertyBlockGenerated_MaterialPropertyBlock_GetFloat__String_m530648952 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialPropertyBlockGenerated::MaterialPropertyBlock_GetMatrix__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialPropertyBlockGenerated_MaterialPropertyBlock_GetMatrix__String_m3052617193 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialPropertyBlockGenerated::MaterialPropertyBlock_GetMatrix__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialPropertyBlockGenerated_MaterialPropertyBlock_GetMatrix__Int32_m3652441272 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialPropertyBlockGenerated::MaterialPropertyBlock_GetTexture__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialPropertyBlockGenerated_MaterialPropertyBlock_GetTexture__String_m210215799 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialPropertyBlockGenerated::MaterialPropertyBlock_GetTexture__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialPropertyBlockGenerated_MaterialPropertyBlock_GetTexture__Int32_m928351594 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialPropertyBlockGenerated::MaterialPropertyBlock_GetVector__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialPropertyBlockGenerated_MaterialPropertyBlock_GetVector__String_m131037163 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialPropertyBlockGenerated::MaterialPropertyBlock_GetVector__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialPropertyBlockGenerated_MaterialPropertyBlock_GetVector__Int32_m2588365430 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialPropertyBlockGenerated::MaterialPropertyBlock_SetColor__Int32__Color(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialPropertyBlockGenerated_MaterialPropertyBlock_SetColor__Int32__Color_m297621239 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialPropertyBlockGenerated::MaterialPropertyBlock_SetColor__String__Color(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialPropertyBlockGenerated_MaterialPropertyBlock_SetColor__String__Color_m534547794 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialPropertyBlockGenerated::MaterialPropertyBlock_SetFloat__String__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialPropertyBlockGenerated_MaterialPropertyBlock_SetFloat__String__Single_m2931264052 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialPropertyBlockGenerated::MaterialPropertyBlock_SetFloat__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialPropertyBlockGenerated_MaterialPropertyBlock_SetFloat__Int32__Single_m626829725 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialPropertyBlockGenerated::MaterialPropertyBlock_SetMatrix__String__Matrix4x4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialPropertyBlockGenerated_MaterialPropertyBlock_SetMatrix__String__Matrix4x4_m2527193084 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialPropertyBlockGenerated::MaterialPropertyBlock_SetMatrix__Int32__Matrix4x4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialPropertyBlockGenerated_MaterialPropertyBlock_SetMatrix__Int32__Matrix4x4_m2431367269 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialPropertyBlockGenerated::MaterialPropertyBlock_SetTexture__String__Texture(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialPropertyBlockGenerated_MaterialPropertyBlock_SetTexture__String__Texture_m2597177426 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialPropertyBlockGenerated::MaterialPropertyBlock_SetTexture__Int32__Texture(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialPropertyBlockGenerated_MaterialPropertyBlock_SetTexture__Int32__Texture_m1875887655 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialPropertyBlockGenerated::MaterialPropertyBlock_SetVector__String__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialPropertyBlockGenerated_MaterialPropertyBlock_SetVector__String__Vector4_m4022426236 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialPropertyBlockGenerated::MaterialPropertyBlock_SetVector__Int32__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialPropertyBlockGenerated_MaterialPropertyBlock_SetVector__Int32__Vector4_m765026537 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MaterialPropertyBlockGenerated::__Register()
extern "C"  void UnityEngine_MaterialPropertyBlockGenerated___Register_m178001074 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MaterialPropertyBlockGenerated::ilo_setSingle1(System.Int32,System.Single)
extern "C"  void UnityEngine_MaterialPropertyBlockGenerated_ilo_setSingle1_m4092543775 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_MaterialPropertyBlockGenerated::ilo_getStringS2(System.Int32)
extern "C"  String_t* UnityEngine_MaterialPropertyBlockGenerated_ilo_getStringS2_m4063704116 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_MaterialPropertyBlockGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_MaterialPropertyBlockGenerated_ilo_setObject3_m1435320183 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_MaterialPropertyBlockGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_MaterialPropertyBlockGenerated_ilo_getObject4_m1233391325 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_MaterialPropertyBlockGenerated::ilo_getInt325(System.Int32)
extern "C"  int32_t UnityEngine_MaterialPropertyBlockGenerated_ilo_getInt325_m3129034624 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
