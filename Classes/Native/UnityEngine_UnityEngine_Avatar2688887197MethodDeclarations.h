﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Avatar
struct Avatar_t2688887197;

#include "codegen/il2cpp-codegen.h"

// System.Boolean UnityEngine.Avatar::get_isValid()
extern "C"  bool Avatar_get_isValid_m3838672853 (Avatar_t2688887197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Avatar::get_isHuman()
extern "C"  bool Avatar_get_isHuman_m576833734 (Avatar_t2688887197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
