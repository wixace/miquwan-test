﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MScrollView
struct MScrollView_t1133512511;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MDragScrollView
struct  MDragScrollView_t2304459027  : public MonoBehaviour_t667441552
{
public:
	// MScrollView MDragScrollView::ParScrollView
	MScrollView_t1133512511 * ___ParScrollView_2;

public:
	inline static int32_t get_offset_of_ParScrollView_2() { return static_cast<int32_t>(offsetof(MDragScrollView_t2304459027, ___ParScrollView_2)); }
	inline MScrollView_t1133512511 * get_ParScrollView_2() const { return ___ParScrollView_2; }
	inline MScrollView_t1133512511 ** get_address_of_ParScrollView_2() { return &___ParScrollView_2; }
	inline void set_ParScrollView_2(MScrollView_t1133512511 * value)
	{
		___ParScrollView_2 = value;
		Il2CppCodeGenWriteBarrier(&___ParScrollView_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
