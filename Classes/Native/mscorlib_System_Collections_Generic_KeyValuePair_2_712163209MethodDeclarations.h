﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22061784035MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m854258752(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t712163209 *, String_t*, KeyValuePair_2_t4287931429 , const MethodInfo*))KeyValuePair_2__ctor_m1079219282_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_Key()
#define KeyValuePair_2_get_Key_m1354925864(__this, method) ((  String_t* (*) (KeyValuePair_2_t712163209 *, const MethodInfo*))KeyValuePair_2_get_Key_m1584147158_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1047313897(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t712163209 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1377884823_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_Value()
#define KeyValuePair_2_get_Value_m870352844(__this, method) ((  KeyValuePair_2_t4287931429  (*) (KeyValuePair_2_t712163209 *, const MethodInfo*))KeyValuePair_2_get_Value_m2108684282_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2431972713(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t712163209 *, KeyValuePair_2_t4287931429 , const MethodInfo*))KeyValuePair_2_set_Value_m1053756183_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::ToString()
#define KeyValuePair_2_ToString_m953153727(__this, method) ((  String_t* (*) (KeyValuePair_2_t712163209 *, const MethodInfo*))KeyValuePair_2_ToString_m3764046545_gshared)(__this, method)
