﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Int32>
struct ValueCollection_t1653136013;
// System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>
struct Dictionary_2_t2952530300;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3065703549;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va884363708.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m2166343294_gshared (ValueCollection_t1653136013 * __this, Dictionary_2_t2952530300 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m2166343294(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1653136013 *, Dictionary_2_t2952530300 *, const MethodInfo*))ValueCollection__ctor_m2166343294_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Int32>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m632073716_gshared (ValueCollection_t1653136013 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m632073716(__this, ___item0, method) ((  void (*) (ValueCollection_t1653136013 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m632073716_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Int32>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2767172797_gshared (ValueCollection_t1653136013 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2767172797(__this, method) ((  void (*) (ValueCollection_t1653136013 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2767172797_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Int32>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m55846834_gshared (ValueCollection_t1653136013 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m55846834(__this, ___item0, method) ((  bool (*) (ValueCollection_t1653136013 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m55846834_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Int32>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3826746583_gshared (ValueCollection_t1653136013 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3826746583(__this, ___item0, method) ((  bool (*) (ValueCollection_t1653136013 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3826746583_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Int32>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3819835723_gshared (ValueCollection_t1653136013 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3819835723(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1653136013 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3819835723_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m2364362113_gshared (ValueCollection_t1653136013 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m2364362113(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1653136013 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2364362113_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m361489084_gshared (ValueCollection_t1653136013 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m361489084(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1653136013 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m361489084_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Int32>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2625989989_gshared (ValueCollection_t1653136013 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2625989989(__this, method) ((  bool (*) (ValueCollection_t1653136013 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2625989989_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2833310405_gshared (ValueCollection_t1653136013 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2833310405(__this, method) ((  bool (*) (ValueCollection_t1653136013 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2833310405_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1780195185_gshared (ValueCollection_t1653136013 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1780195185(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1653136013 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1780195185_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Int32>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m852646725_gshared (ValueCollection_t1653136013 * __this, Int32U5BU5D_t3230847821* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m852646725(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1653136013 *, Int32U5BU5D_t3230847821*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m852646725_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t884363708  ValueCollection_GetEnumerator_m3639535208_gshared (ValueCollection_t1653136013 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m3639535208(__this, method) ((  Enumerator_t884363708  (*) (ValueCollection_t1653136013 *, const MethodInfo*))ValueCollection_GetEnumerator_m3639535208_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,System.Int32>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1926724875_gshared (ValueCollection_t1653136013 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1926724875(__this, method) ((  int32_t (*) (ValueCollection_t1653136013 *, const MethodInfo*))ValueCollection_get_Count_m1926724875_gshared)(__this, method)
