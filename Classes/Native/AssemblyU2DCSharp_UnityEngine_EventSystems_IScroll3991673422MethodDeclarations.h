﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_IScrollHandlerGenerated
struct UnityEngine_EventSystems_IScrollHandlerGenerated_t3991673422;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_EventSystems_IScrollHandlerGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_IScrollHandlerGenerated__ctor_m2450023437 (UnityEngine_EventSystems_IScrollHandlerGenerated_t3991673422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_IScrollHandlerGenerated::IScrollHandler_OnScroll__PointerEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_IScrollHandlerGenerated_IScrollHandler_OnScroll__PointerEventData_m3309614117 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_IScrollHandlerGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_IScrollHandlerGenerated___Register_m2472974554 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
