﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.DynamicWrapperBase
struct DynamicWrapperBase_t2837268693;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Utilities.DynamicWrapperBase::.ctor()
extern "C"  void DynamicWrapperBase__ctor_m2227252861 (DynamicWrapperBase_t2837268693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
