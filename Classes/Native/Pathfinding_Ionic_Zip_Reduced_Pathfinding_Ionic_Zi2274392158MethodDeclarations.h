﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Pathfinding.Ionic.Zip.ZipEntry/CopyHelper::.cctor()
extern "C"  void CopyHelper__cctor_m532181485 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.Ionic.Zip.ZipEntry/CopyHelper::AppendCopyToFileName(System.String)
extern "C"  String_t* CopyHelper_AppendCopyToFileName_m1953563440 (Il2CppObject * __this /* static, unused */, String_t* ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
