﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginMiquwan
struct PluginMiquwan_t866441041;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// VersionInfo
struct VersionInfo_t2356638086;
// VersionMgr
struct VersionMgr_t1322950208;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginMiquwan866441041.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void PluginMiquwan::.ctor()
extern "C"  void PluginMiquwan__ctor_m2228499770 (PluginMiquwan_t866441041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMiquwan::Init()
extern "C"  void PluginMiquwan_Init_m4041296346 (PluginMiquwan_t866441041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginMiquwan::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginMiquwan_ReqSDKHttpLogin_m2700398251 (PluginMiquwan_t866441041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginMiquwan::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginMiquwan_IsLoginSuccess_m1132183025 (PluginMiquwan_t866441041 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMiquwan::OpenUserLogin()
extern "C"  void PluginMiquwan_OpenUserLogin_m1829338508 (PluginMiquwan_t866441041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMiquwan::RolelvUpinfo(CEvent.ZEvent)
extern "C"  void PluginMiquwan_RolelvUpinfo_m2846243688 (PluginMiquwan_t866441041 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMiquwan::UserPay(CEvent.ZEvent)
extern "C"  void PluginMiquwan_UserPay_m508035174 (PluginMiquwan_t866441041 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMiquwan::OnInitSuccess(System.String)
extern "C"  void PluginMiquwan_OnInitSuccess_m2783492790 (PluginMiquwan_t866441041 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMiquwan::OnInitFail(System.String)
extern "C"  void PluginMiquwan_OnInitFail_m2663359051 (PluginMiquwan_t866441041 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMiquwan::OnLoginSuccess(System.String)
extern "C"  void PluginMiquwan_OnLoginSuccess_m778642623 (PluginMiquwan_t866441041 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMiquwan::OnLoginFail(System.String)
extern "C"  void PluginMiquwan_OnLoginFail_m3884555490 (PluginMiquwan_t866441041 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMiquwan::OnPaySuccess(System.String)
extern "C"  void PluginMiquwan_OnPaySuccess_m377465534 (PluginMiquwan_t866441041 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMiquwan::OnPayFail(System.String)
extern "C"  void PluginMiquwan_OnPayFail_m3345346371 (PluginMiquwan_t866441041 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMiquwan::OnExitGameSuccess(System.String)
extern "C"  void PluginMiquwan_OnExitGameSuccess_m393941974 (PluginMiquwan_t866441041 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMiquwan::LogoutSuccess()
extern "C"  void PluginMiquwan_LogoutSuccess_m1780630577 (PluginMiquwan_t866441041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMiquwan::OnLogoutSuccess(System.String)
extern "C"  void PluginMiquwan_OnLogoutSuccess_m2666627504 (PluginMiquwan_t866441041 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMiquwan::OnLogoutFail(System.String)
extern "C"  void PluginMiquwan_OnLogoutFail_m4269985169 (PluginMiquwan_t866441041 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMiquwan::MQWSDKInit()
extern "C"  void PluginMiquwan_MQWSDKInit_m2482842081 (PluginMiquwan_t866441041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMiquwan::MQWSDKLogin()
extern "C"  void PluginMiquwan_MQWSDKLogin_m2347725914 (PluginMiquwan_t866441041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMiquwan::MQWSDKPay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginMiquwan_MQWSDKPay_m871932329 (PluginMiquwan_t866441041 * __this, String_t* ___productId0, String_t* ___productName1, String_t* ___roleId2, String_t* ___roleName3, String_t* ___roleLevel4, String_t* ___Price5, String_t* ___extendInfo6, String_t* ___serverId7, String_t* ___serverName8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMiquwan::<OnLogoutSuccess>m__43E()
extern "C"  void PluginMiquwan_U3COnLogoutSuccessU3Em__43E_m382356049 (PluginMiquwan_t866441041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginMiquwan::ilo_get_currentVS1(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginMiquwan_ilo_get_currentVS1_m912469940 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMiquwan::ilo_MQWSDKInit2(PluginMiquwan)
extern "C"  void PluginMiquwan_ilo_MQWSDKInit2_m2801440641 (Il2CppObject * __this /* static, unused */, PluginMiquwan_t866441041 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMiquwan::ilo_AddEventListener3(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginMiquwan_ilo_AddEventListener3_m2266712248 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginMiquwan::ilo_get_Instance4()
extern "C"  VersionMgr_t1322950208 * PluginMiquwan_ilo_get_Instance4_m2428421720 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginMiquwan::ilo_get_isSdkLogin5(VersionMgr)
extern "C"  bool PluginMiquwan_ilo_get_isSdkLogin5_m3690529708 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.PayInfo PluginMiquwan::ilo_Parse6(System.Object[])
extern "C"  PayInfo_t1775308120 * PluginMiquwan_ilo_Parse6_m848999193 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___payOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMiquwan::ilo_MQWSDKPay7(PluginMiquwan,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginMiquwan_ilo_MQWSDKPay7_m553629226 (Il2CppObject * __this /* static, unused */, PluginMiquwan_t866441041 * ____this0, String_t* ___productId1, String_t* ___productName2, String_t* ___roleId3, String_t* ___roleName4, String_t* ___roleLevel5, String_t* ___Price6, String_t* ___extendInfo7, String_t* ___serverId8, String_t* ___serverName9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMiquwan::ilo_Log8(System.Object,System.Boolean)
extern "C"  void PluginMiquwan_ilo_Log8_m1492616346 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMiquwan::ilo_DispatchEvent9(CEvent.ZEvent)
extern "C"  void PluginMiquwan_ilo_DispatchEvent9_m1674634053 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
