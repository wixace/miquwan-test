﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AnimationClipPairGenerated
struct UnityEngine_AnimationClipPairGenerated_t1793265321;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_AnimationClipPairGenerated::.ctor()
extern "C"  void UnityEngine_AnimationClipPairGenerated__ctor_m2067358738 (UnityEngine_AnimationClipPairGenerated_t1793265321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationClipPairGenerated::AnimationClipPair_AnimationClipPair1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationClipPairGenerated_AnimationClipPair_AnimationClipPair1_m2592119114 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationClipPairGenerated::AnimationClipPair_originalClip(JSVCall)
extern "C"  void UnityEngine_AnimationClipPairGenerated_AnimationClipPair_originalClip_m26244739 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationClipPairGenerated::AnimationClipPair_overrideClip(JSVCall)
extern "C"  void UnityEngine_AnimationClipPairGenerated_AnimationClipPair_overrideClip_m710161512 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationClipPairGenerated::__Register()
extern "C"  void UnityEngine_AnimationClipPairGenerated___Register_m3378369525 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_AnimationClipPairGenerated::ilo_getObject1(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_AnimationClipPairGenerated_ilo_getObject1_m3892546525 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AnimationClipPairGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_AnimationClipPairGenerated_ilo_setObject2_m1725062521 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
