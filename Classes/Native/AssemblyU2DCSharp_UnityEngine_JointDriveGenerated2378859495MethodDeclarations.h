﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_JointDriveGenerated
struct UnityEngine_JointDriveGenerated_t2378859495;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_JointDriveGenerated::.ctor()
extern "C"  void UnityEngine_JointDriveGenerated__ctor_m1316677476 (UnityEngine_JointDriveGenerated_t2378859495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointDriveGenerated::.cctor()
extern "C"  void UnityEngine_JointDriveGenerated__cctor_m1680199881 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_JointDriveGenerated::JointDrive_JointDrive1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_JointDriveGenerated_JointDrive_JointDrive1_m2560992462 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointDriveGenerated::JointDrive_positionSpring(JSVCall)
extern "C"  void UnityEngine_JointDriveGenerated_JointDrive_positionSpring_m3025276272 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointDriveGenerated::JointDrive_positionDamper(JSVCall)
extern "C"  void UnityEngine_JointDriveGenerated_JointDrive_positionDamper_m81918960 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointDriveGenerated::JointDrive_maximumForce(JSVCall)
extern "C"  void UnityEngine_JointDriveGenerated_JointDrive_maximumForce_m719999227 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointDriveGenerated::__Register()
extern "C"  void UnityEngine_JointDriveGenerated___Register_m3984823267 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_JointDriveGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_JointDriveGenerated_ilo_getObject1_m3715104082 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointDriveGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_JointDriveGenerated_ilo_addJSCSRel2_m2915139745 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_JointDriveGenerated::ilo_getSingle3(System.Int32)
extern "C"  float UnityEngine_JointDriveGenerated_ilo_getSingle3_m2023144365 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointDriveGenerated::ilo_changeJSObj4(System.Int32,System.Object)
extern "C"  void UnityEngine_JointDriveGenerated_ilo_changeJSObj4_m2975431176 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
