﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.CollectionUtils/<CreateCollectionWrapper>c__AnonStorey137
struct U3CCreateCollectionWrapperU3Ec__AnonStorey137_t2076896410;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t2570496278;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"

// System.Void Newtonsoft.Json.Utilities.CollectionUtils/<CreateCollectionWrapper>c__AnonStorey137::.ctor()
extern "C"  void U3CCreateCollectionWrapperU3Ec__AnonStorey137__ctor_m104821777 (U3CCreateCollectionWrapperU3Ec__AnonStorey137_t2076896410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.CollectionUtils/<CreateCollectionWrapper>c__AnonStorey137::<>m__393(System.Type,System.Collections.Generic.IList`1<System.Object>)
extern "C"  Il2CppObject * U3CCreateCollectionWrapperU3Ec__AnonStorey137_U3CU3Em__393_m729928722 (U3CCreateCollectionWrapperU3Ec__AnonStorey137_t2076896410 * __this, Type_t * ___t0, Il2CppObject* ___a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
