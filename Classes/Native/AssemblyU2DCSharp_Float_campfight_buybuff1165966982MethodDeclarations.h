﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_campfight_buybuff
struct Float_campfight_buybuff_t1165966982;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// FloatTextUnit
struct FloatTextUnit_t2362298029;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"

// System.Void Float_campfight_buybuff::.ctor()
extern "C"  void Float_campfight_buybuff__ctor_m3611695013 (Float_campfight_buybuff_t1165966982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_campfight_buybuff::FloatID()
extern "C"  int32_t Float_campfight_buybuff_FloatID_m1755639907 (Float_campfight_buybuff_t1165966982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_campfight_buybuff::OnAwake(UnityEngine.GameObject)
extern "C"  void Float_campfight_buybuff_OnAwake_m90479713 (Float_campfight_buybuff_t1165966982 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_campfight_buybuff::OnDestroy()
extern "C"  void Float_campfight_buybuff_OnDestroy_m2705953310 (Float_campfight_buybuff_t1165966982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_campfight_buybuff::Init()
extern "C"  void Float_campfight_buybuff_Init_m3670273551 (Float_campfight_buybuff_t1165966982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_campfight_buybuff::SetFile(System.Object[])
extern "C"  void Float_campfight_buybuff_SetFile_m1737369553 (Float_campfight_buybuff_t1165966982 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_campfight_buybuff::ilo_OnAwake1(FloatTextUnit,UnityEngine.GameObject)
extern "C"  void Float_campfight_buybuff_ilo_OnAwake1_m1412568950 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, GameObject_t3674682005 * ___viewGO1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_campfight_buybuff::ilo_OnDestroy2(FloatTextUnit)
extern "C"  void Float_campfight_buybuff_ilo_OnDestroy2_m4145233244 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
