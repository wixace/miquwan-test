﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// friendNpcCfg
struct friendNpcCfg_t3890310657;

#include "AssemblyU2DCSharp_Monster2901270458.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FriendNpc
struct  FriendNpc_t838916931  : public Monster_t2901270458
{
public:
	// friendNpcCfg FriendNpc::FriendNpcCfg
	friendNpcCfg_t3890310657 * ___FriendNpcCfg_337;
	// System.Boolean FriendNpc::<IsTrigger>k__BackingField
	bool ___U3CIsTriggerU3Ek__BackingField_338;

public:
	inline static int32_t get_offset_of_FriendNpcCfg_337() { return static_cast<int32_t>(offsetof(FriendNpc_t838916931, ___FriendNpcCfg_337)); }
	inline friendNpcCfg_t3890310657 * get_FriendNpcCfg_337() const { return ___FriendNpcCfg_337; }
	inline friendNpcCfg_t3890310657 ** get_address_of_FriendNpcCfg_337() { return &___FriendNpcCfg_337; }
	inline void set_FriendNpcCfg_337(friendNpcCfg_t3890310657 * value)
	{
		___FriendNpcCfg_337 = value;
		Il2CppCodeGenWriteBarrier(&___FriendNpcCfg_337, value);
	}

	inline static int32_t get_offset_of_U3CIsTriggerU3Ek__BackingField_338() { return static_cast<int32_t>(offsetof(FriendNpc_t838916931, ___U3CIsTriggerU3Ek__BackingField_338)); }
	inline bool get_U3CIsTriggerU3Ek__BackingField_338() const { return ___U3CIsTriggerU3Ek__BackingField_338; }
	inline bool* get_address_of_U3CIsTriggerU3Ek__BackingField_338() { return &___U3CIsTriggerU3Ek__BackingField_338; }
	inline void set_U3CIsTriggerU3Ek__BackingField_338(bool value)
	{
		___U3CIsTriggerU3Ek__BackingField_338 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
