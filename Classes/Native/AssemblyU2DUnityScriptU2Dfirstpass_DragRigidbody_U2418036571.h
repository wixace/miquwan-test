﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t2727095145;
// DragRigidbody
struct DragRigidbody_t2531437401;

#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen67576739.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DragRigidbody/$DragObject$58/$
struct  U24_t418036571  : public GenericGeneratorEnumerator_1_t67576739
{
public:
	// System.Single DragRigidbody/$DragObject$58/$::$oldDrag$59
	float ___U24oldDragU2459_2;
	// System.Single DragRigidbody/$DragObject$58/$::$oldAngularDrag$60
	float ___U24oldAngularDragU2460_3;
	// UnityEngine.Camera DragRigidbody/$DragObject$58/$::$mainCamera$61
	Camera_t2727095145 * ___U24mainCameraU2461_4;
	// UnityEngine.Ray DragRigidbody/$DragObject$58/$::$ray$62
	Ray_t3134616544  ___U24rayU2462_5;
	// System.Single DragRigidbody/$DragObject$58/$::$distance$63
	float ___U24distanceU2463_6;
	// DragRigidbody DragRigidbody/$DragObject$58/$::$self_$64
	DragRigidbody_t2531437401 * ___U24self_U2464_7;

public:
	inline static int32_t get_offset_of_U24oldDragU2459_2() { return static_cast<int32_t>(offsetof(U24_t418036571, ___U24oldDragU2459_2)); }
	inline float get_U24oldDragU2459_2() const { return ___U24oldDragU2459_2; }
	inline float* get_address_of_U24oldDragU2459_2() { return &___U24oldDragU2459_2; }
	inline void set_U24oldDragU2459_2(float value)
	{
		___U24oldDragU2459_2 = value;
	}

	inline static int32_t get_offset_of_U24oldAngularDragU2460_3() { return static_cast<int32_t>(offsetof(U24_t418036571, ___U24oldAngularDragU2460_3)); }
	inline float get_U24oldAngularDragU2460_3() const { return ___U24oldAngularDragU2460_3; }
	inline float* get_address_of_U24oldAngularDragU2460_3() { return &___U24oldAngularDragU2460_3; }
	inline void set_U24oldAngularDragU2460_3(float value)
	{
		___U24oldAngularDragU2460_3 = value;
	}

	inline static int32_t get_offset_of_U24mainCameraU2461_4() { return static_cast<int32_t>(offsetof(U24_t418036571, ___U24mainCameraU2461_4)); }
	inline Camera_t2727095145 * get_U24mainCameraU2461_4() const { return ___U24mainCameraU2461_4; }
	inline Camera_t2727095145 ** get_address_of_U24mainCameraU2461_4() { return &___U24mainCameraU2461_4; }
	inline void set_U24mainCameraU2461_4(Camera_t2727095145 * value)
	{
		___U24mainCameraU2461_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24mainCameraU2461_4, value);
	}

	inline static int32_t get_offset_of_U24rayU2462_5() { return static_cast<int32_t>(offsetof(U24_t418036571, ___U24rayU2462_5)); }
	inline Ray_t3134616544  get_U24rayU2462_5() const { return ___U24rayU2462_5; }
	inline Ray_t3134616544 * get_address_of_U24rayU2462_5() { return &___U24rayU2462_5; }
	inline void set_U24rayU2462_5(Ray_t3134616544  value)
	{
		___U24rayU2462_5 = value;
	}

	inline static int32_t get_offset_of_U24distanceU2463_6() { return static_cast<int32_t>(offsetof(U24_t418036571, ___U24distanceU2463_6)); }
	inline float get_U24distanceU2463_6() const { return ___U24distanceU2463_6; }
	inline float* get_address_of_U24distanceU2463_6() { return &___U24distanceU2463_6; }
	inline void set_U24distanceU2463_6(float value)
	{
		___U24distanceU2463_6 = value;
	}

	inline static int32_t get_offset_of_U24self_U2464_7() { return static_cast<int32_t>(offsetof(U24_t418036571, ___U24self_U2464_7)); }
	inline DragRigidbody_t2531437401 * get_U24self_U2464_7() const { return ___U24self_U2464_7; }
	inline DragRigidbody_t2531437401 ** get_address_of_U24self_U2464_7() { return &___U24self_U2464_7; }
	inline void set_U24self_U2464_7(DragRigidbody_t2531437401 * value)
	{
		___U24self_U2464_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24self_U2464_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
