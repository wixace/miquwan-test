﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21352297102MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,Pathfinding.PointNode>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m922481893(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t4238261807 *, Int3_t1974045594 , PointNode_t2761813780 *, const MethodInfo*))KeyValuePair_2__ctor_m1445654159_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,Pathfinding.PointNode>::get_Key()
#define KeyValuePair_2_get_Key_m720781667(__this, method) ((  Int3_t1974045594  (*) (KeyValuePair_2_t4238261807 *, const MethodInfo*))KeyValuePair_2_get_Key_m680403065_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,Pathfinding.PointNode>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1785539108(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4238261807 *, Int3_t1974045594 , const MethodInfo*))KeyValuePair_2_set_Key_m444550714_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,Pathfinding.PointNode>::get_Value()
#define KeyValuePair_2_get_Value_m2469792227(__this, method) ((  PointNode_t2761813780 * (*) (KeyValuePair_2_t4238261807 *, const MethodInfo*))KeyValuePair_2_get_Value_m861079929_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,Pathfinding.PointNode>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2988362788(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4238261807 *, PointNode_t2761813780 *, const MethodInfo*))KeyValuePair_2_set_Value_m95745338_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,Pathfinding.PointNode>::ToString()
#define KeyValuePair_2_ToString_m3995372862(__this, method) ((  String_t* (*) (KeyValuePair_2_t4238261807 *, const MethodInfo*))KeyValuePair_2_ToString_m3407486440_gshared)(__this, method)
