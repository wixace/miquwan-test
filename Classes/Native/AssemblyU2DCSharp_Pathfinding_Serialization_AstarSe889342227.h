﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Serialization.AstarSerializer/<SerializeExtraInfo>c__AnonStorey10C
struct  U3CSerializeExtraInfoU3Ec__AnonStorey10C_t889342227  : public Il2CppObject
{
public:
	// System.Int32 Pathfinding.Serialization.AstarSerializer/<SerializeExtraInfo>c__AnonStorey10C::totCount
	int32_t ___totCount_0;

public:
	inline static int32_t get_offset_of_totCount_0() { return static_cast<int32_t>(offsetof(U3CSerializeExtraInfoU3Ec__AnonStorey10C_t889342227, ___totCount_0)); }
	inline int32_t get_totCount_0() const { return ___totCount_0; }
	inline int32_t* get_address_of_totCount_0() { return &___totCount_0; }
	inline void set_totCount_0(int32_t value)
	{
		___totCount_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
