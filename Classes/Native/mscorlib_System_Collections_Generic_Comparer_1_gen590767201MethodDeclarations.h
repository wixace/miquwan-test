﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<Mihua.Assets.SubAssetMgr/ErrorInfo>
struct Comparer_1_t590767201;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.Comparer`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor()
extern "C"  void Comparer_1__ctor_m1558942751_gshared (Comparer_1_t590767201 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m1558942751(__this, method) ((  void (*) (Comparer_1_t590767201 *, const MethodInfo*))Comparer_1__ctor_m1558942751_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::.cctor()
extern "C"  void Comparer_1__cctor_m600488814_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m600488814(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m600488814_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m1378481284_gshared (Comparer_1_t590767201 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m1378481284(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t590767201 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m1378481284_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Default()
extern "C"  Comparer_1_t590767201 * Comparer_1_get_Default_m1975267719_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m1975267719(__this /* static, unused */, method) ((  Comparer_1_t590767201 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m1975267719_gshared)(__this /* static, unused */, method)
