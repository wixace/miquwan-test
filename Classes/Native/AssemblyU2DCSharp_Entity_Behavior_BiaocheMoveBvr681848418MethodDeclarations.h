﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.BiaocheMoveBvr
struct BiaocheMoveBvr_t681848418;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// CombatEntity
struct CombatEntity_t684137495;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"

// System.Void Entity.Behavior.BiaocheMoveBvr::.ctor()
extern "C"  void BiaocheMoveBvr__ctor_m4073777928 (BiaocheMoveBvr_t681848418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID Entity.Behavior.BiaocheMoveBvr::get_id()
extern "C"  uint8_t BiaocheMoveBvr_get_id_m536843026 (BiaocheMoveBvr_t681848418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.BiaocheMoveBvr::Reason()
extern "C"  void BiaocheMoveBvr_Reason_m1284452288 (BiaocheMoveBvr_t681848418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.BiaocheMoveBvr::Action()
extern "C"  void BiaocheMoveBvr_Action_m481158962 (BiaocheMoveBvr_t681848418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.BiaocheMoveBvr::DoEntering()
extern "C"  void BiaocheMoveBvr_DoEntering_m2199422833 (BiaocheMoveBvr_t681848418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.BiaocheMoveBvr::SetParams(System.Object[])
extern "C"  void BiaocheMoveBvr_SetParams_m2323467716 (BiaocheMoveBvr_t681848418 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.BiaocheMoveBvr::DoLeaving()
extern "C"  void BiaocheMoveBvr_DoLeaving_m100217359 (BiaocheMoveBvr_t681848418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Entity.Behavior.BiaocheMoveBvr::ilo_get_position1(CombatEntity)
extern "C"  Vector3_t4282066566  BiaocheMoveBvr_ilo_get_position1_m2186645889 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.BiaocheMoveBvr::ilo_get_entity2(Entity.Behavior.IBehavior)
extern "C"  CombatEntity_t684137495 * BiaocheMoveBvr_ilo_get_entity2_m1089045 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
