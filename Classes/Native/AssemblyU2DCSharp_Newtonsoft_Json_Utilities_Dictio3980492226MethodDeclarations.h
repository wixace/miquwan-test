﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>
struct DictionaryWrapper_2_t3980492226;
// System.Collections.IDictionary
struct IDictionary_t537317817;
// System.Collections.Generic.IDictionary`2<System.Object,System.Object>
struct IDictionary_2_t1623761616;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t770439062;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2483180780;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t3856534026;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::.ctor(System.Collections.IDictionary)
extern "C"  void DictionaryWrapper_2__ctor_m2478084832_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___dictionary0, const MethodInfo* method);
#define DictionaryWrapper_2__ctor_m2478084832(__this, ___dictionary0, method) ((  void (*) (DictionaryWrapper_2_t3980492226 *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2__ctor_m2478084832_gshared)(__this, ___dictionary0, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void DictionaryWrapper_2__ctor_m2175695185_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define DictionaryWrapper_2__ctor_m2175695185(__this, ___dictionary0, method) ((  void (*) (DictionaryWrapper_2_t3980492226 *, Il2CppObject*, const MethodInfo*))DictionaryWrapper_2__ctor_m2175695185_gshared)(__this, ___dictionary0, method)
// System.Collections.IEnumerator Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_IEnumerable_GetEnumerator_m805522026_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_IEnumerable_GetEnumerator_m805522026(__this, method) ((  Il2CppObject * (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))DictionaryWrapper_2_System_Collections_IEnumerable_GetEnumerator_m805522026_gshared)(__this, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void DictionaryWrapper_2_System_Collections_IDictionary_Add_m2499938987_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_IDictionary_Add_m2499938987(__this, ___key0, ___value1, method) ((  void (*) (DictionaryWrapper_2_t3980492226 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_System_Collections_IDictionary_Add_m2499938987_gshared)(__this, ___key0, ___value1, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool DictionaryWrapper_2_System_Collections_IDictionary_Contains_m2550301597_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_IDictionary_Contains_m2550301597(__this, ___key0, method) ((  bool (*) (DictionaryWrapper_2_t3980492226 *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_System_Collections_IDictionary_Contains_m2550301597_gshared)(__this, ___key0, method)
// System.Collections.IDictionaryEnumerator Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_IDictionary_GetEnumerator_m1699127106_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_IDictionary_GetEnumerator_m1699127106(__this, method) ((  Il2CppObject * (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))DictionaryWrapper_2_System_Collections_IDictionary_GetEnumerator_m1699127106_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool DictionaryWrapper_2_System_Collections_IDictionary_get_IsFixedSize_m1554541772_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_IDictionary_get_IsFixedSize_m1554541772(__this, method) ((  bool (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))DictionaryWrapper_2_System_Collections_IDictionary_get_IsFixedSize_m1554541772_gshared)(__this, method)
// System.Collections.ICollection Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_IDictionary_get_Keys_m3386191675_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_IDictionary_get_Keys_m3386191675(__this, method) ((  Il2CppObject * (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))DictionaryWrapper_2_System_Collections_IDictionary_get_Keys_m3386191675_gshared)(__this, method)
// System.Collections.ICollection Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_IDictionary_get_Values_m912544105_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_IDictionary_get_Values_m912544105(__this, method) ((  Il2CppObject * (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))DictionaryWrapper_2_System_Collections_IDictionary_get_Values_m912544105_gshared)(__this, method)
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_IDictionary_get_Item_m2780422017_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_IDictionary_get_Item_m2780422017(__this, ___key0, method) ((  Il2CppObject * (*) (DictionaryWrapper_2_t3980492226 *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_System_Collections_IDictionary_get_Item_m2780422017_gshared)(__this, ___key0, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void DictionaryWrapper_2_System_Collections_IDictionary_set_Item_m3688356838_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_IDictionary_set_Item_m3688356838(__this, ___key0, ___value1, method) ((  void (*) (DictionaryWrapper_2_t3980492226 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_System_Collections_IDictionary_set_Item_m3688356838_gshared)(__this, ___key0, ___value1, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void DictionaryWrapper_2_System_Collections_ICollection_CopyTo_m984441981_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_ICollection_CopyTo_m984441981(__this, ___array0, ___index1, method) ((  void (*) (DictionaryWrapper_2_t3980492226 *, Il2CppArray *, int32_t, const MethodInfo*))DictionaryWrapper_2_System_Collections_ICollection_CopyTo_m984441981_gshared)(__this, ___array0, ___index1, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool DictionaryWrapper_2_System_Collections_ICollection_get_IsSynchronized_m2528516311_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_ICollection_get_IsSynchronized_m2528516311(__this, method) ((  bool (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))DictionaryWrapper_2_System_Collections_ICollection_get_IsSynchronized_m2528516311_gshared)(__this, method)
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_ICollection_get_SyncRoot_m2178810613_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_ICollection_get_SyncRoot_m2178810613(__this, method) ((  Il2CppObject * (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))DictionaryWrapper_2_System_Collections_ICollection_get_SyncRoot_m2178810613_gshared)(__this, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C"  void DictionaryWrapper_2_Add_m4103221742_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define DictionaryWrapper_2_Add_m4103221742(__this, ___key0, ___value1, method) ((  void (*) (DictionaryWrapper_2_t3980492226 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_Add_m4103221742_gshared)(__this, ___key0, ___value1, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::ContainsKey(TKey)
extern "C"  bool DictionaryWrapper_2_ContainsKey_m2191511622_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define DictionaryWrapper_2_ContainsKey_m2191511622(__this, ___key0, method) ((  bool (*) (DictionaryWrapper_2_t3980492226 *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_ContainsKey_m2191511622_gshared)(__this, ___key0, method)
// System.Collections.Generic.ICollection`1<TKey> Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_Keys()
extern "C"  Il2CppObject* DictionaryWrapper_2_get_Keys_m3984898551_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_get_Keys_m3984898551(__this, method) ((  Il2CppObject* (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))DictionaryWrapper_2_get_Keys_m3984898551_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Remove(TKey)
extern "C"  bool DictionaryWrapper_2_Remove_m3892630762_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define DictionaryWrapper_2_Remove_m3892630762(__this, ___key0, method) ((  bool (*) (DictionaryWrapper_2_t3980492226 *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_Remove_m3892630762_gshared)(__this, ___key0, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool DictionaryWrapper_2_TryGetValue_m2819785375_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define DictionaryWrapper_2_TryGetValue_m2819785375(__this, ___key0, ___value1, method) ((  bool (*) (DictionaryWrapper_2_t3980492226 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))DictionaryWrapper_2_TryGetValue_m2819785375_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.ICollection`1<TValue> Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_Values()
extern "C"  Il2CppObject* DictionaryWrapper_2_get_Values_m237655223_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_get_Values_m237655223(__this, method) ((  Il2CppObject* (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))DictionaryWrapper_2_get_Values_m237655223_gshared)(__this, method)
// TValue Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * DictionaryWrapper_2_get_Item_m3300941614_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define DictionaryWrapper_2_get_Item_m3300941614(__this, ___key0, method) ((  Il2CppObject * (*) (DictionaryWrapper_2_t3980492226 *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_get_Item_m3300941614_gshared)(__this, ___key0, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C"  void DictionaryWrapper_2_set_Item_m956535847_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define DictionaryWrapper_2_set_Item_m956535847(__this, ___key0, ___value1, method) ((  void (*) (DictionaryWrapper_2_t3980492226 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_set_Item_m956535847_gshared)(__this, ___key0, ___value1, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void DictionaryWrapper_2_Add_m1047433143_gshared (DictionaryWrapper_2_t3980492226 * __this, KeyValuePair_2_t1944668977  ___item0, const MethodInfo* method);
#define DictionaryWrapper_2_Add_m1047433143(__this, ___item0, method) ((  void (*) (DictionaryWrapper_2_t3980492226 *, KeyValuePair_2_t1944668977 , const MethodInfo*))DictionaryWrapper_2_Add_m1047433143_gshared)(__this, ___item0, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Clear()
extern "C"  void DictionaryWrapper_2_Clear_m4196551378_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_Clear_m4196551378(__this, method) ((  void (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))DictionaryWrapper_2_Clear_m4196551378_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool DictionaryWrapper_2_Contains_m250146017_gshared (DictionaryWrapper_2_t3980492226 * __this, KeyValuePair_2_t1944668977  ___item0, const MethodInfo* method);
#define DictionaryWrapper_2_Contains_m250146017(__this, ___item0, method) ((  bool (*) (DictionaryWrapper_2_t3980492226 *, KeyValuePair_2_t1944668977 , const MethodInfo*))DictionaryWrapper_2_Contains_m250146017_gshared)(__this, ___item0, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void DictionaryWrapper_2_CopyTo_m3178340315_gshared (DictionaryWrapper_2_t3980492226 * __this, KeyValuePair_2U5BU5D_t2483180780* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define DictionaryWrapper_2_CopyTo_m3178340315(__this, ___array0, ___arrayIndex1, method) ((  void (*) (DictionaryWrapper_2_t3980492226 *, KeyValuePair_2U5BU5D_t2483180780*, int32_t, const MethodInfo*))DictionaryWrapper_2_CopyTo_m3178340315_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_Count()
extern "C"  int32_t DictionaryWrapper_2_get_Count_m2144045405_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_get_Count_m2144045405(__this, method) ((  int32_t (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))DictionaryWrapper_2_get_Count_m2144045405_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_IsReadOnly()
extern "C"  bool DictionaryWrapper_2_get_IsReadOnly_m4024078438_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_get_IsReadOnly_m4024078438(__this, method) ((  bool (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))DictionaryWrapper_2_get_IsReadOnly_m4024078438_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool DictionaryWrapper_2_Remove_m3554596934_gshared (DictionaryWrapper_2_t3980492226 * __this, KeyValuePair_2_t1944668977  ___item0, const MethodInfo* method);
#define DictionaryWrapper_2_Remove_m3554596934(__this, ___item0, method) ((  bool (*) (DictionaryWrapper_2_t3980492226 *, KeyValuePair_2_t1944668977 , const MethodInfo*))DictionaryWrapper_2_Remove_m3554596934_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* DictionaryWrapper_2_GetEnumerator_m2069457380_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_GetEnumerator_m2069457380(__this, method) ((  Il2CppObject* (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))DictionaryWrapper_2_GetEnumerator_m2069457380_gshared)(__this, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Remove(System.Object)
extern "C"  void DictionaryWrapper_2_Remove_m115268851_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define DictionaryWrapper_2_Remove_m115268851(__this, ___key0, method) ((  void (*) (DictionaryWrapper_2_t3980492226 *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_Remove_m115268851_gshared)(__this, ___key0, method)
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_UnderlyingDictionary()
extern "C"  Il2CppObject * DictionaryWrapper_2_get_UnderlyingDictionary_m948016846_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_get_UnderlyingDictionary_m948016846(__this, method) ((  Il2CppObject * (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))DictionaryWrapper_2_get_UnderlyingDictionary_m948016846_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::<GetEnumerator>m__39B(System.Collections.DictionaryEntry)
extern "C"  KeyValuePair_2_t1944668977  DictionaryWrapper_2_U3CGetEnumeratorU3Em__39B_m3469670946_gshared (Il2CppObject * __this /* static, unused */, DictionaryEntry_t1751606614  ___de0, const MethodInfo* method);
#define DictionaryWrapper_2_U3CGetEnumeratorU3Em__39B_m3469670946(__this /* static, unused */, ___de0, method) ((  KeyValuePair_2_t1944668977  (*) (Il2CppObject * /* static, unused */, DictionaryEntry_t1751606614 , const MethodInfo*))DictionaryWrapper_2_U3CGetEnumeratorU3Em__39B_m3469670946_gshared)(__this /* static, unused */, ___de0, method)
