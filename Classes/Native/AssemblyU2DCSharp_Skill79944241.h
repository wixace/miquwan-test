﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// skillCfg
struct skillCfg_t2142425171;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// FightEnum.ESkillBuffEvent[]
struct ESkillBuffEventU5BU5D_t2768361140;
// FightEnum.ESkillBuffTarget[]
struct ESkillBuffTargetU5BU5D_t2691263849;
// System.Boolean[]
struct BooleanU5BU5D_t3456302923;
// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Skill
struct  Skill_t79944241  : public Il2CppObject
{
public:
	// skillCfg Skill::_mpJson
	skillCfg_t2142425171 * ____mpJson_0;
	// System.Int32 Skill::<index>k__BackingField
	int32_t ___U3CindexU3Ek__BackingField_1;
	// System.Int32 Skill::<mTplID>k__BackingField
	int32_t ___U3CmTplIDU3Ek__BackingField_2;
	// System.Boolean Skill::<isActive>k__BackingField
	bool ___U3CisActiveU3Ek__BackingField_3;
	// System.Boolean Skill::<isAddedAnger>k__BackingField
	bool ___U3CisAddedAngerU3Ek__BackingField_4;
	// System.Single Skill::<mLastUseTime>k__BackingField
	float ___U3CmLastUseTimeU3Ek__BackingField_5;
	// System.String[] Skill::<action>k__BackingField
	StringU5BU5D_t4054002952* ___U3CactionU3Ek__BackingField_6;
	// System.Single[] Skill::<actionTime>k__BackingField
	SingleU5BU5D_t2316563989* ___U3CactionTimeU3Ek__BackingField_7;
	// System.Int32[] Skill::<castingEffect>k__BackingField
	Int32U5BU5D_t3230847821* ___U3CcastingEffectU3Ek__BackingField_8;
	// System.Single[] Skill::<castingEffectTime>k__BackingField
	SingleU5BU5D_t2316563989* ___U3CcastingEffectTimeU3Ek__BackingField_9;
	// System.Single[] Skill::<castingEffectPlaySpeed>k__BackingField
	SingleU5BU5D_t2316563989* ___U3CcastingEffectPlaySpeedU3Ek__BackingField_10;
	// System.Int32[] Skill::<effect>k__BackingField
	Int32U5BU5D_t3230847821* ___U3CeffectU3Ek__BackingField_11;
	// System.Single[] Skill::<effectTime>k__BackingField
	SingleU5BU5D_t2316563989* ___U3CeffectTimeU3Ek__BackingField_12;
	// System.Int32[] Skill::<effcetEvent>k__BackingField
	Int32U5BU5D_t3230847821* ___U3CeffcetEventU3Ek__BackingField_13;
	// System.Single[] Skill::<effectPlaySpeed>k__BackingField
	SingleU5BU5D_t2316563989* ___U3CeffectPlaySpeedU3Ek__BackingField_14;
	// System.Int32 Skill::<hitEffect>k__BackingField
	int32_t ___U3ChitEffectU3Ek__BackingField_15;
	// System.Single Skill::<hitEffectTime>k__BackingField
	float ___U3ChitEffectTimeU3Ek__BackingField_16;
	// UnityEngine.Vector3 Skill::<zoominoffset>k__BackingField
	Vector3_t4282066566  ___U3CzoominoffsetU3Ek__BackingField_17;
	// System.Int32[] Skill::<sound>k__BackingField
	Int32U5BU5D_t3230847821* ___U3CsoundU3Ek__BackingField_18;
	// System.Single[] Skill::<soundTime>k__BackingField
	SingleU5BU5D_t2316563989* ___U3CsoundTimeU3Ek__BackingField_19;
	// System.UInt32 Skill::<skillLevel>k__BackingField
	uint32_t ___U3CskillLevelU3Ek__BackingField_20;
	// System.Single Skill::<damage_add_per>k__BackingField
	float ___U3Cdamage_add_perU3Ek__BackingField_21;
	// System.Int32 Skill::<damage_add_value>k__BackingField
	int32_t ___U3Cdamage_add_valueU3Ek__BackingField_22;
	// System.Int32 Skill::<buffHitPer>k__BackingField
	int32_t ___U3CbuffHitPerU3Ek__BackingField_23;
	// System.Int32[] Skill::<buffid>k__BackingField
	Int32U5BU5D_t3230847821* ___U3CbuffidU3Ek__BackingField_24;
	// System.Int32[] Skill::<buffprob>k__BackingField
	Int32U5BU5D_t3230847821* ___U3CbuffprobU3Ek__BackingField_25;
	// FightEnum.ESkillBuffEvent[] Skill::<buffevent>k__BackingField
	ESkillBuffEventU5BU5D_t2768361140* ___U3CbuffeventU3Ek__BackingField_26;
	// FightEnum.ESkillBuffTarget[] Skill::<bufftarget>k__BackingField
	ESkillBuffTargetU5BU5D_t2691263849* ___U3CbufftargetU3Ek__BackingField_27;
	// System.Boolean[] Skill::<buffbreak>k__BackingField
	BooleanU5BU5D_t3456302923* ___U3CbuffbreakU3Ek__BackingField_28;
	// System.Boolean Skill::<isProgressBar>k__BackingField
	bool ___U3CisProgressBarU3Ek__BackingField_29;
	// System.Boolean Skill::<breakskill>k__BackingField
	bool ___U3CbreakskillU3Ek__BackingField_30;
	// System.Int32[] Skill::<summonid>k__BackingField
	Int32U5BU5D_t3230847821* ___U3CsummonidU3Ek__BackingField_31;
	// System.Single[] Skill::<summonTime>k__BackingField
	SingleU5BU5D_t2316563989* ___U3CsummonTimeU3Ek__BackingField_32;
	// System.Int32[] Skill::<summonAttackper>k__BackingField
	Int32U5BU5D_t3230847821* ___U3CsummonAttackperU3Ek__BackingField_33;
	// System.String Skill::<callAttributeKey1>k__BackingField
	String_t* ___U3CcallAttributeKey1U3Ek__BackingField_34;
	// System.Int32 Skill::<callAttributeValue1>k__BackingField
	int32_t ___U3CcallAttributeValue1U3Ek__BackingField_35;
	// System.String Skill::<callAttributeKey2>k__BackingField
	String_t* ___U3CcallAttributeKey2U3Ek__BackingField_36;
	// System.Int32 Skill::<callAttributeValue2>k__BackingField
	int32_t ___U3CcallAttributeValue2U3Ek__BackingField_37;
	// System.Boolean Skill::<isSputter>k__BackingField
	bool ___U3CisSputterU3Ek__BackingField_38;
	// System.Single Skill::<sputter_radius>k__BackingField
	float ___U3Csputter_radiusU3Ek__BackingField_39;
	// System.Int32 Skill::<sputter_num>k__BackingField
	int32_t ___U3Csputter_numU3Ek__BackingField_40;
	// System.Single Skill::<sputter_hurt>k__BackingField
	float ___U3Csputter_hurtU3Ek__BackingField_41;
	// System.Int32 Skill::<effectCount>k__BackingField
	int32_t ___U3CeffectCountU3Ek__BackingField_42;
	// System.Single Skill::<CD>k__BackingField
	float ___U3CCDU3Ek__BackingField_43;
	// System.Boolean Skill::<isFingdPos>k__BackingField
	bool ___U3CisFingdPosU3Ek__BackingField_44;
	// System.Int32[] Skill::<blackShowIDs>k__BackingField
	Int32U5BU5D_t3230847821* ___U3CblackShowIDsU3Ek__BackingField_45;
	// System.Single[] Skill::<activeSpacing>k__BackingField
	SingleU5BU5D_t2316563989* ___U3CactiveSpacingU3Ek__BackingField_46;
	// System.Boolean Skill::<isThump>k__BackingField
	bool ___U3CisThumpU3Ek__BackingField_47;
	// System.Single Skill::<thumpAddValue>k__BackingField
	float ___U3CthumpAddValueU3Ek__BackingField_48;

public:
	inline static int32_t get_offset_of__mpJson_0() { return static_cast<int32_t>(offsetof(Skill_t79944241, ____mpJson_0)); }
	inline skillCfg_t2142425171 * get__mpJson_0() const { return ____mpJson_0; }
	inline skillCfg_t2142425171 ** get_address_of__mpJson_0() { return &____mpJson_0; }
	inline void set__mpJson_0(skillCfg_t2142425171 * value)
	{
		____mpJson_0 = value;
		Il2CppCodeGenWriteBarrier(&____mpJson_0, value);
	}

	inline static int32_t get_offset_of_U3CindexU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CindexU3Ek__BackingField_1)); }
	inline int32_t get_U3CindexU3Ek__BackingField_1() const { return ___U3CindexU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CindexU3Ek__BackingField_1() { return &___U3CindexU3Ek__BackingField_1; }
	inline void set_U3CindexU3Ek__BackingField_1(int32_t value)
	{
		___U3CindexU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CmTplIDU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CmTplIDU3Ek__BackingField_2)); }
	inline int32_t get_U3CmTplIDU3Ek__BackingField_2() const { return ___U3CmTplIDU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CmTplIDU3Ek__BackingField_2() { return &___U3CmTplIDU3Ek__BackingField_2; }
	inline void set_U3CmTplIDU3Ek__BackingField_2(int32_t value)
	{
		___U3CmTplIDU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CisActiveU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CisActiveU3Ek__BackingField_3)); }
	inline bool get_U3CisActiveU3Ek__BackingField_3() const { return ___U3CisActiveU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CisActiveU3Ek__BackingField_3() { return &___U3CisActiveU3Ek__BackingField_3; }
	inline void set_U3CisActiveU3Ek__BackingField_3(bool value)
	{
		___U3CisActiveU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CisAddedAngerU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CisAddedAngerU3Ek__BackingField_4)); }
	inline bool get_U3CisAddedAngerU3Ek__BackingField_4() const { return ___U3CisAddedAngerU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CisAddedAngerU3Ek__BackingField_4() { return &___U3CisAddedAngerU3Ek__BackingField_4; }
	inline void set_U3CisAddedAngerU3Ek__BackingField_4(bool value)
	{
		___U3CisAddedAngerU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CmLastUseTimeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CmLastUseTimeU3Ek__BackingField_5)); }
	inline float get_U3CmLastUseTimeU3Ek__BackingField_5() const { return ___U3CmLastUseTimeU3Ek__BackingField_5; }
	inline float* get_address_of_U3CmLastUseTimeU3Ek__BackingField_5() { return &___U3CmLastUseTimeU3Ek__BackingField_5; }
	inline void set_U3CmLastUseTimeU3Ek__BackingField_5(float value)
	{
		___U3CmLastUseTimeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CactionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CactionU3Ek__BackingField_6)); }
	inline StringU5BU5D_t4054002952* get_U3CactionU3Ek__BackingField_6() const { return ___U3CactionU3Ek__BackingField_6; }
	inline StringU5BU5D_t4054002952** get_address_of_U3CactionU3Ek__BackingField_6() { return &___U3CactionU3Ek__BackingField_6; }
	inline void set_U3CactionU3Ek__BackingField_6(StringU5BU5D_t4054002952* value)
	{
		___U3CactionU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CactionU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CactionTimeU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CactionTimeU3Ek__BackingField_7)); }
	inline SingleU5BU5D_t2316563989* get_U3CactionTimeU3Ek__BackingField_7() const { return ___U3CactionTimeU3Ek__BackingField_7; }
	inline SingleU5BU5D_t2316563989** get_address_of_U3CactionTimeU3Ek__BackingField_7() { return &___U3CactionTimeU3Ek__BackingField_7; }
	inline void set_U3CactionTimeU3Ek__BackingField_7(SingleU5BU5D_t2316563989* value)
	{
		___U3CactionTimeU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CactionTimeU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3CcastingEffectU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CcastingEffectU3Ek__BackingField_8)); }
	inline Int32U5BU5D_t3230847821* get_U3CcastingEffectU3Ek__BackingField_8() const { return ___U3CcastingEffectU3Ek__BackingField_8; }
	inline Int32U5BU5D_t3230847821** get_address_of_U3CcastingEffectU3Ek__BackingField_8() { return &___U3CcastingEffectU3Ek__BackingField_8; }
	inline void set_U3CcastingEffectU3Ek__BackingField_8(Int32U5BU5D_t3230847821* value)
	{
		___U3CcastingEffectU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcastingEffectU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CcastingEffectTimeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CcastingEffectTimeU3Ek__BackingField_9)); }
	inline SingleU5BU5D_t2316563989* get_U3CcastingEffectTimeU3Ek__BackingField_9() const { return ___U3CcastingEffectTimeU3Ek__BackingField_9; }
	inline SingleU5BU5D_t2316563989** get_address_of_U3CcastingEffectTimeU3Ek__BackingField_9() { return &___U3CcastingEffectTimeU3Ek__BackingField_9; }
	inline void set_U3CcastingEffectTimeU3Ek__BackingField_9(SingleU5BU5D_t2316563989* value)
	{
		___U3CcastingEffectTimeU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcastingEffectTimeU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CcastingEffectPlaySpeedU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CcastingEffectPlaySpeedU3Ek__BackingField_10)); }
	inline SingleU5BU5D_t2316563989* get_U3CcastingEffectPlaySpeedU3Ek__BackingField_10() const { return ___U3CcastingEffectPlaySpeedU3Ek__BackingField_10; }
	inline SingleU5BU5D_t2316563989** get_address_of_U3CcastingEffectPlaySpeedU3Ek__BackingField_10() { return &___U3CcastingEffectPlaySpeedU3Ek__BackingField_10; }
	inline void set_U3CcastingEffectPlaySpeedU3Ek__BackingField_10(SingleU5BU5D_t2316563989* value)
	{
		___U3CcastingEffectPlaySpeedU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcastingEffectPlaySpeedU3Ek__BackingField_10, value);
	}

	inline static int32_t get_offset_of_U3CeffectU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CeffectU3Ek__BackingField_11)); }
	inline Int32U5BU5D_t3230847821* get_U3CeffectU3Ek__BackingField_11() const { return ___U3CeffectU3Ek__BackingField_11; }
	inline Int32U5BU5D_t3230847821** get_address_of_U3CeffectU3Ek__BackingField_11() { return &___U3CeffectU3Ek__BackingField_11; }
	inline void set_U3CeffectU3Ek__BackingField_11(Int32U5BU5D_t3230847821* value)
	{
		___U3CeffectU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CeffectU3Ek__BackingField_11, value);
	}

	inline static int32_t get_offset_of_U3CeffectTimeU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CeffectTimeU3Ek__BackingField_12)); }
	inline SingleU5BU5D_t2316563989* get_U3CeffectTimeU3Ek__BackingField_12() const { return ___U3CeffectTimeU3Ek__BackingField_12; }
	inline SingleU5BU5D_t2316563989** get_address_of_U3CeffectTimeU3Ek__BackingField_12() { return &___U3CeffectTimeU3Ek__BackingField_12; }
	inline void set_U3CeffectTimeU3Ek__BackingField_12(SingleU5BU5D_t2316563989* value)
	{
		___U3CeffectTimeU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CeffectTimeU3Ek__BackingField_12, value);
	}

	inline static int32_t get_offset_of_U3CeffcetEventU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CeffcetEventU3Ek__BackingField_13)); }
	inline Int32U5BU5D_t3230847821* get_U3CeffcetEventU3Ek__BackingField_13() const { return ___U3CeffcetEventU3Ek__BackingField_13; }
	inline Int32U5BU5D_t3230847821** get_address_of_U3CeffcetEventU3Ek__BackingField_13() { return &___U3CeffcetEventU3Ek__BackingField_13; }
	inline void set_U3CeffcetEventU3Ek__BackingField_13(Int32U5BU5D_t3230847821* value)
	{
		___U3CeffcetEventU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CeffcetEventU3Ek__BackingField_13, value);
	}

	inline static int32_t get_offset_of_U3CeffectPlaySpeedU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CeffectPlaySpeedU3Ek__BackingField_14)); }
	inline SingleU5BU5D_t2316563989* get_U3CeffectPlaySpeedU3Ek__BackingField_14() const { return ___U3CeffectPlaySpeedU3Ek__BackingField_14; }
	inline SingleU5BU5D_t2316563989** get_address_of_U3CeffectPlaySpeedU3Ek__BackingField_14() { return &___U3CeffectPlaySpeedU3Ek__BackingField_14; }
	inline void set_U3CeffectPlaySpeedU3Ek__BackingField_14(SingleU5BU5D_t2316563989* value)
	{
		___U3CeffectPlaySpeedU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CeffectPlaySpeedU3Ek__BackingField_14, value);
	}

	inline static int32_t get_offset_of_U3ChitEffectU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3ChitEffectU3Ek__BackingField_15)); }
	inline int32_t get_U3ChitEffectU3Ek__BackingField_15() const { return ___U3ChitEffectU3Ek__BackingField_15; }
	inline int32_t* get_address_of_U3ChitEffectU3Ek__BackingField_15() { return &___U3ChitEffectU3Ek__BackingField_15; }
	inline void set_U3ChitEffectU3Ek__BackingField_15(int32_t value)
	{
		___U3ChitEffectU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3ChitEffectTimeU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3ChitEffectTimeU3Ek__BackingField_16)); }
	inline float get_U3ChitEffectTimeU3Ek__BackingField_16() const { return ___U3ChitEffectTimeU3Ek__BackingField_16; }
	inline float* get_address_of_U3ChitEffectTimeU3Ek__BackingField_16() { return &___U3ChitEffectTimeU3Ek__BackingField_16; }
	inline void set_U3ChitEffectTimeU3Ek__BackingField_16(float value)
	{
		___U3ChitEffectTimeU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CzoominoffsetU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CzoominoffsetU3Ek__BackingField_17)); }
	inline Vector3_t4282066566  get_U3CzoominoffsetU3Ek__BackingField_17() const { return ___U3CzoominoffsetU3Ek__BackingField_17; }
	inline Vector3_t4282066566 * get_address_of_U3CzoominoffsetU3Ek__BackingField_17() { return &___U3CzoominoffsetU3Ek__BackingField_17; }
	inline void set_U3CzoominoffsetU3Ek__BackingField_17(Vector3_t4282066566  value)
	{
		___U3CzoominoffsetU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CsoundU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CsoundU3Ek__BackingField_18)); }
	inline Int32U5BU5D_t3230847821* get_U3CsoundU3Ek__BackingField_18() const { return ___U3CsoundU3Ek__BackingField_18; }
	inline Int32U5BU5D_t3230847821** get_address_of_U3CsoundU3Ek__BackingField_18() { return &___U3CsoundU3Ek__BackingField_18; }
	inline void set_U3CsoundU3Ek__BackingField_18(Int32U5BU5D_t3230847821* value)
	{
		___U3CsoundU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsoundU3Ek__BackingField_18, value);
	}

	inline static int32_t get_offset_of_U3CsoundTimeU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CsoundTimeU3Ek__BackingField_19)); }
	inline SingleU5BU5D_t2316563989* get_U3CsoundTimeU3Ek__BackingField_19() const { return ___U3CsoundTimeU3Ek__BackingField_19; }
	inline SingleU5BU5D_t2316563989** get_address_of_U3CsoundTimeU3Ek__BackingField_19() { return &___U3CsoundTimeU3Ek__BackingField_19; }
	inline void set_U3CsoundTimeU3Ek__BackingField_19(SingleU5BU5D_t2316563989* value)
	{
		___U3CsoundTimeU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsoundTimeU3Ek__BackingField_19, value);
	}

	inline static int32_t get_offset_of_U3CskillLevelU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CskillLevelU3Ek__BackingField_20)); }
	inline uint32_t get_U3CskillLevelU3Ek__BackingField_20() const { return ___U3CskillLevelU3Ek__BackingField_20; }
	inline uint32_t* get_address_of_U3CskillLevelU3Ek__BackingField_20() { return &___U3CskillLevelU3Ek__BackingField_20; }
	inline void set_U3CskillLevelU3Ek__BackingField_20(uint32_t value)
	{
		___U3CskillLevelU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3Cdamage_add_perU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3Cdamage_add_perU3Ek__BackingField_21)); }
	inline float get_U3Cdamage_add_perU3Ek__BackingField_21() const { return ___U3Cdamage_add_perU3Ek__BackingField_21; }
	inline float* get_address_of_U3Cdamage_add_perU3Ek__BackingField_21() { return &___U3Cdamage_add_perU3Ek__BackingField_21; }
	inline void set_U3Cdamage_add_perU3Ek__BackingField_21(float value)
	{
		___U3Cdamage_add_perU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3Cdamage_add_valueU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3Cdamage_add_valueU3Ek__BackingField_22)); }
	inline int32_t get_U3Cdamage_add_valueU3Ek__BackingField_22() const { return ___U3Cdamage_add_valueU3Ek__BackingField_22; }
	inline int32_t* get_address_of_U3Cdamage_add_valueU3Ek__BackingField_22() { return &___U3Cdamage_add_valueU3Ek__BackingField_22; }
	inline void set_U3Cdamage_add_valueU3Ek__BackingField_22(int32_t value)
	{
		___U3Cdamage_add_valueU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3CbuffHitPerU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CbuffHitPerU3Ek__BackingField_23)); }
	inline int32_t get_U3CbuffHitPerU3Ek__BackingField_23() const { return ___U3CbuffHitPerU3Ek__BackingField_23; }
	inline int32_t* get_address_of_U3CbuffHitPerU3Ek__BackingField_23() { return &___U3CbuffHitPerU3Ek__BackingField_23; }
	inline void set_U3CbuffHitPerU3Ek__BackingField_23(int32_t value)
	{
		___U3CbuffHitPerU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_U3CbuffidU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CbuffidU3Ek__BackingField_24)); }
	inline Int32U5BU5D_t3230847821* get_U3CbuffidU3Ek__BackingField_24() const { return ___U3CbuffidU3Ek__BackingField_24; }
	inline Int32U5BU5D_t3230847821** get_address_of_U3CbuffidU3Ek__BackingField_24() { return &___U3CbuffidU3Ek__BackingField_24; }
	inline void set_U3CbuffidU3Ek__BackingField_24(Int32U5BU5D_t3230847821* value)
	{
		___U3CbuffidU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbuffidU3Ek__BackingField_24, value);
	}

	inline static int32_t get_offset_of_U3CbuffprobU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CbuffprobU3Ek__BackingField_25)); }
	inline Int32U5BU5D_t3230847821* get_U3CbuffprobU3Ek__BackingField_25() const { return ___U3CbuffprobU3Ek__BackingField_25; }
	inline Int32U5BU5D_t3230847821** get_address_of_U3CbuffprobU3Ek__BackingField_25() { return &___U3CbuffprobU3Ek__BackingField_25; }
	inline void set_U3CbuffprobU3Ek__BackingField_25(Int32U5BU5D_t3230847821* value)
	{
		___U3CbuffprobU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbuffprobU3Ek__BackingField_25, value);
	}

	inline static int32_t get_offset_of_U3CbuffeventU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CbuffeventU3Ek__BackingField_26)); }
	inline ESkillBuffEventU5BU5D_t2768361140* get_U3CbuffeventU3Ek__BackingField_26() const { return ___U3CbuffeventU3Ek__BackingField_26; }
	inline ESkillBuffEventU5BU5D_t2768361140** get_address_of_U3CbuffeventU3Ek__BackingField_26() { return &___U3CbuffeventU3Ek__BackingField_26; }
	inline void set_U3CbuffeventU3Ek__BackingField_26(ESkillBuffEventU5BU5D_t2768361140* value)
	{
		___U3CbuffeventU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbuffeventU3Ek__BackingField_26, value);
	}

	inline static int32_t get_offset_of_U3CbufftargetU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CbufftargetU3Ek__BackingField_27)); }
	inline ESkillBuffTargetU5BU5D_t2691263849* get_U3CbufftargetU3Ek__BackingField_27() const { return ___U3CbufftargetU3Ek__BackingField_27; }
	inline ESkillBuffTargetU5BU5D_t2691263849** get_address_of_U3CbufftargetU3Ek__BackingField_27() { return &___U3CbufftargetU3Ek__BackingField_27; }
	inline void set_U3CbufftargetU3Ek__BackingField_27(ESkillBuffTargetU5BU5D_t2691263849* value)
	{
		___U3CbufftargetU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbufftargetU3Ek__BackingField_27, value);
	}

	inline static int32_t get_offset_of_U3CbuffbreakU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CbuffbreakU3Ek__BackingField_28)); }
	inline BooleanU5BU5D_t3456302923* get_U3CbuffbreakU3Ek__BackingField_28() const { return ___U3CbuffbreakU3Ek__BackingField_28; }
	inline BooleanU5BU5D_t3456302923** get_address_of_U3CbuffbreakU3Ek__BackingField_28() { return &___U3CbuffbreakU3Ek__BackingField_28; }
	inline void set_U3CbuffbreakU3Ek__BackingField_28(BooleanU5BU5D_t3456302923* value)
	{
		___U3CbuffbreakU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbuffbreakU3Ek__BackingField_28, value);
	}

	inline static int32_t get_offset_of_U3CisProgressBarU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CisProgressBarU3Ek__BackingField_29)); }
	inline bool get_U3CisProgressBarU3Ek__BackingField_29() const { return ___U3CisProgressBarU3Ek__BackingField_29; }
	inline bool* get_address_of_U3CisProgressBarU3Ek__BackingField_29() { return &___U3CisProgressBarU3Ek__BackingField_29; }
	inline void set_U3CisProgressBarU3Ek__BackingField_29(bool value)
	{
		___U3CisProgressBarU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_U3CbreakskillU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CbreakskillU3Ek__BackingField_30)); }
	inline bool get_U3CbreakskillU3Ek__BackingField_30() const { return ___U3CbreakskillU3Ek__BackingField_30; }
	inline bool* get_address_of_U3CbreakskillU3Ek__BackingField_30() { return &___U3CbreakskillU3Ek__BackingField_30; }
	inline void set_U3CbreakskillU3Ek__BackingField_30(bool value)
	{
		___U3CbreakskillU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_U3CsummonidU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CsummonidU3Ek__BackingField_31)); }
	inline Int32U5BU5D_t3230847821* get_U3CsummonidU3Ek__BackingField_31() const { return ___U3CsummonidU3Ek__BackingField_31; }
	inline Int32U5BU5D_t3230847821** get_address_of_U3CsummonidU3Ek__BackingField_31() { return &___U3CsummonidU3Ek__BackingField_31; }
	inline void set_U3CsummonidU3Ek__BackingField_31(Int32U5BU5D_t3230847821* value)
	{
		___U3CsummonidU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsummonidU3Ek__BackingField_31, value);
	}

	inline static int32_t get_offset_of_U3CsummonTimeU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CsummonTimeU3Ek__BackingField_32)); }
	inline SingleU5BU5D_t2316563989* get_U3CsummonTimeU3Ek__BackingField_32() const { return ___U3CsummonTimeU3Ek__BackingField_32; }
	inline SingleU5BU5D_t2316563989** get_address_of_U3CsummonTimeU3Ek__BackingField_32() { return &___U3CsummonTimeU3Ek__BackingField_32; }
	inline void set_U3CsummonTimeU3Ek__BackingField_32(SingleU5BU5D_t2316563989* value)
	{
		___U3CsummonTimeU3Ek__BackingField_32 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsummonTimeU3Ek__BackingField_32, value);
	}

	inline static int32_t get_offset_of_U3CsummonAttackperU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CsummonAttackperU3Ek__BackingField_33)); }
	inline Int32U5BU5D_t3230847821* get_U3CsummonAttackperU3Ek__BackingField_33() const { return ___U3CsummonAttackperU3Ek__BackingField_33; }
	inline Int32U5BU5D_t3230847821** get_address_of_U3CsummonAttackperU3Ek__BackingField_33() { return &___U3CsummonAttackperU3Ek__BackingField_33; }
	inline void set_U3CsummonAttackperU3Ek__BackingField_33(Int32U5BU5D_t3230847821* value)
	{
		___U3CsummonAttackperU3Ek__BackingField_33 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsummonAttackperU3Ek__BackingField_33, value);
	}

	inline static int32_t get_offset_of_U3CcallAttributeKey1U3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CcallAttributeKey1U3Ek__BackingField_34)); }
	inline String_t* get_U3CcallAttributeKey1U3Ek__BackingField_34() const { return ___U3CcallAttributeKey1U3Ek__BackingField_34; }
	inline String_t** get_address_of_U3CcallAttributeKey1U3Ek__BackingField_34() { return &___U3CcallAttributeKey1U3Ek__BackingField_34; }
	inline void set_U3CcallAttributeKey1U3Ek__BackingField_34(String_t* value)
	{
		___U3CcallAttributeKey1U3Ek__BackingField_34 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcallAttributeKey1U3Ek__BackingField_34, value);
	}

	inline static int32_t get_offset_of_U3CcallAttributeValue1U3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CcallAttributeValue1U3Ek__BackingField_35)); }
	inline int32_t get_U3CcallAttributeValue1U3Ek__BackingField_35() const { return ___U3CcallAttributeValue1U3Ek__BackingField_35; }
	inline int32_t* get_address_of_U3CcallAttributeValue1U3Ek__BackingField_35() { return &___U3CcallAttributeValue1U3Ek__BackingField_35; }
	inline void set_U3CcallAttributeValue1U3Ek__BackingField_35(int32_t value)
	{
		___U3CcallAttributeValue1U3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of_U3CcallAttributeKey2U3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CcallAttributeKey2U3Ek__BackingField_36)); }
	inline String_t* get_U3CcallAttributeKey2U3Ek__BackingField_36() const { return ___U3CcallAttributeKey2U3Ek__BackingField_36; }
	inline String_t** get_address_of_U3CcallAttributeKey2U3Ek__BackingField_36() { return &___U3CcallAttributeKey2U3Ek__BackingField_36; }
	inline void set_U3CcallAttributeKey2U3Ek__BackingField_36(String_t* value)
	{
		___U3CcallAttributeKey2U3Ek__BackingField_36 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcallAttributeKey2U3Ek__BackingField_36, value);
	}

	inline static int32_t get_offset_of_U3CcallAttributeValue2U3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CcallAttributeValue2U3Ek__BackingField_37)); }
	inline int32_t get_U3CcallAttributeValue2U3Ek__BackingField_37() const { return ___U3CcallAttributeValue2U3Ek__BackingField_37; }
	inline int32_t* get_address_of_U3CcallAttributeValue2U3Ek__BackingField_37() { return &___U3CcallAttributeValue2U3Ek__BackingField_37; }
	inline void set_U3CcallAttributeValue2U3Ek__BackingField_37(int32_t value)
	{
		___U3CcallAttributeValue2U3Ek__BackingField_37 = value;
	}

	inline static int32_t get_offset_of_U3CisSputterU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CisSputterU3Ek__BackingField_38)); }
	inline bool get_U3CisSputterU3Ek__BackingField_38() const { return ___U3CisSputterU3Ek__BackingField_38; }
	inline bool* get_address_of_U3CisSputterU3Ek__BackingField_38() { return &___U3CisSputterU3Ek__BackingField_38; }
	inline void set_U3CisSputterU3Ek__BackingField_38(bool value)
	{
		___U3CisSputterU3Ek__BackingField_38 = value;
	}

	inline static int32_t get_offset_of_U3Csputter_radiusU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3Csputter_radiusU3Ek__BackingField_39)); }
	inline float get_U3Csputter_radiusU3Ek__BackingField_39() const { return ___U3Csputter_radiusU3Ek__BackingField_39; }
	inline float* get_address_of_U3Csputter_radiusU3Ek__BackingField_39() { return &___U3Csputter_radiusU3Ek__BackingField_39; }
	inline void set_U3Csputter_radiusU3Ek__BackingField_39(float value)
	{
		___U3Csputter_radiusU3Ek__BackingField_39 = value;
	}

	inline static int32_t get_offset_of_U3Csputter_numU3Ek__BackingField_40() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3Csputter_numU3Ek__BackingField_40)); }
	inline int32_t get_U3Csputter_numU3Ek__BackingField_40() const { return ___U3Csputter_numU3Ek__BackingField_40; }
	inline int32_t* get_address_of_U3Csputter_numU3Ek__BackingField_40() { return &___U3Csputter_numU3Ek__BackingField_40; }
	inline void set_U3Csputter_numU3Ek__BackingField_40(int32_t value)
	{
		___U3Csputter_numU3Ek__BackingField_40 = value;
	}

	inline static int32_t get_offset_of_U3Csputter_hurtU3Ek__BackingField_41() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3Csputter_hurtU3Ek__BackingField_41)); }
	inline float get_U3Csputter_hurtU3Ek__BackingField_41() const { return ___U3Csputter_hurtU3Ek__BackingField_41; }
	inline float* get_address_of_U3Csputter_hurtU3Ek__BackingField_41() { return &___U3Csputter_hurtU3Ek__BackingField_41; }
	inline void set_U3Csputter_hurtU3Ek__BackingField_41(float value)
	{
		___U3Csputter_hurtU3Ek__BackingField_41 = value;
	}

	inline static int32_t get_offset_of_U3CeffectCountU3Ek__BackingField_42() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CeffectCountU3Ek__BackingField_42)); }
	inline int32_t get_U3CeffectCountU3Ek__BackingField_42() const { return ___U3CeffectCountU3Ek__BackingField_42; }
	inline int32_t* get_address_of_U3CeffectCountU3Ek__BackingField_42() { return &___U3CeffectCountU3Ek__BackingField_42; }
	inline void set_U3CeffectCountU3Ek__BackingField_42(int32_t value)
	{
		___U3CeffectCountU3Ek__BackingField_42 = value;
	}

	inline static int32_t get_offset_of_U3CCDU3Ek__BackingField_43() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CCDU3Ek__BackingField_43)); }
	inline float get_U3CCDU3Ek__BackingField_43() const { return ___U3CCDU3Ek__BackingField_43; }
	inline float* get_address_of_U3CCDU3Ek__BackingField_43() { return &___U3CCDU3Ek__BackingField_43; }
	inline void set_U3CCDU3Ek__BackingField_43(float value)
	{
		___U3CCDU3Ek__BackingField_43 = value;
	}

	inline static int32_t get_offset_of_U3CisFingdPosU3Ek__BackingField_44() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CisFingdPosU3Ek__BackingField_44)); }
	inline bool get_U3CisFingdPosU3Ek__BackingField_44() const { return ___U3CisFingdPosU3Ek__BackingField_44; }
	inline bool* get_address_of_U3CisFingdPosU3Ek__BackingField_44() { return &___U3CisFingdPosU3Ek__BackingField_44; }
	inline void set_U3CisFingdPosU3Ek__BackingField_44(bool value)
	{
		___U3CisFingdPosU3Ek__BackingField_44 = value;
	}

	inline static int32_t get_offset_of_U3CblackShowIDsU3Ek__BackingField_45() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CblackShowIDsU3Ek__BackingField_45)); }
	inline Int32U5BU5D_t3230847821* get_U3CblackShowIDsU3Ek__BackingField_45() const { return ___U3CblackShowIDsU3Ek__BackingField_45; }
	inline Int32U5BU5D_t3230847821** get_address_of_U3CblackShowIDsU3Ek__BackingField_45() { return &___U3CblackShowIDsU3Ek__BackingField_45; }
	inline void set_U3CblackShowIDsU3Ek__BackingField_45(Int32U5BU5D_t3230847821* value)
	{
		___U3CblackShowIDsU3Ek__BackingField_45 = value;
		Il2CppCodeGenWriteBarrier(&___U3CblackShowIDsU3Ek__BackingField_45, value);
	}

	inline static int32_t get_offset_of_U3CactiveSpacingU3Ek__BackingField_46() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CactiveSpacingU3Ek__BackingField_46)); }
	inline SingleU5BU5D_t2316563989* get_U3CactiveSpacingU3Ek__BackingField_46() const { return ___U3CactiveSpacingU3Ek__BackingField_46; }
	inline SingleU5BU5D_t2316563989** get_address_of_U3CactiveSpacingU3Ek__BackingField_46() { return &___U3CactiveSpacingU3Ek__BackingField_46; }
	inline void set_U3CactiveSpacingU3Ek__BackingField_46(SingleU5BU5D_t2316563989* value)
	{
		___U3CactiveSpacingU3Ek__BackingField_46 = value;
		Il2CppCodeGenWriteBarrier(&___U3CactiveSpacingU3Ek__BackingField_46, value);
	}

	inline static int32_t get_offset_of_U3CisThumpU3Ek__BackingField_47() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CisThumpU3Ek__BackingField_47)); }
	inline bool get_U3CisThumpU3Ek__BackingField_47() const { return ___U3CisThumpU3Ek__BackingField_47; }
	inline bool* get_address_of_U3CisThumpU3Ek__BackingField_47() { return &___U3CisThumpU3Ek__BackingField_47; }
	inline void set_U3CisThumpU3Ek__BackingField_47(bool value)
	{
		___U3CisThumpU3Ek__BackingField_47 = value;
	}

	inline static int32_t get_offset_of_U3CthumpAddValueU3Ek__BackingField_48() { return static_cast<int32_t>(offsetof(Skill_t79944241, ___U3CthumpAddValueU3Ek__BackingField_48)); }
	inline float get_U3CthumpAddValueU3Ek__BackingField_48() const { return ___U3CthumpAddValueU3Ek__BackingField_48; }
	inline float* get_address_of_U3CthumpAddValueU3Ek__BackingField_48() { return &___U3CthumpAddValueU3Ek__BackingField_48; }
	inline void set_U3CthumpAddValueU3Ek__BackingField_48(float value)
	{
		___U3CthumpAddValueU3Ek__BackingField_48 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
