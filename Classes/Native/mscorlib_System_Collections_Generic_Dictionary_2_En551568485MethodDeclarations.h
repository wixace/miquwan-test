﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2991864267MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,UIModelDisplay>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m4284658499(__this, ___dictionary0, method) ((  void (*) (Enumerator_t551568485 *, Dictionary_2_t3529212389 *, const MethodInfo*))Enumerator__ctor_m2965822312_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,UIModelDisplay>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4160017790(__this, method) ((  Il2CppObject * (*) (Enumerator_t551568485 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1500736451_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,UIModelDisplay>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1261566354(__this, method) ((  void (*) (Enumerator_t551568485 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m895706189_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,UIModelDisplay>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2112512475(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t551568485 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3782872836_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,UIModelDisplay>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2535554010(__this, method) ((  Il2CppObject * (*) (Enumerator_t551568485 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2330298975_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,UIModelDisplay>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2469383148(__this, method) ((  Il2CppObject * (*) (Enumerator_t551568485 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2787790129_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,UIModelDisplay>::MoveNext()
#define Enumerator_MoveNext_m2525021173(__this, method) ((  bool (*) (Enumerator_t551568485 *, const MethodInfo*))Enumerator_MoveNext_m856528381_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,UIModelDisplay>::get_Current()
#define Enumerator_get_Current_m4274390218(__this, method) ((  KeyValuePair_2_t3427993095  (*) (Enumerator_t551568485 *, const MethodInfo*))Enumerator_get_Current_m348107039_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,UIModelDisplay>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2174386891(__this, method) ((  int32_t (*) (Enumerator_t551568485 *, const MethodInfo*))Enumerator_get_CurrentKey_m128279622_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,UIModelDisplay>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2483473199(__this, method) ((  UIModelDisplay_t1730520589 * (*) (Enumerator_t551568485 *, const MethodInfo*))Enumerator_get_CurrentValue_m1546518150_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,UIModelDisplay>::Reset()
#define Enumerator_Reset_m1172642069(__this, method) ((  void (*) (Enumerator_t551568485 *, const MethodInfo*))Enumerator_Reset_m886364602_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,UIModelDisplay>::VerifyState()
#define Enumerator_VerifyState_m2330232158(__this, method) ((  void (*) (Enumerator_t551568485 *, const MethodInfo*))Enumerator_VerifyState_m258504259_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,UIModelDisplay>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m125047110(__this, method) ((  void (*) (Enumerator_t551568485 *, const MethodInfo*))Enumerator_VerifyCurrent_m2059361515_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,UIModelDisplay>::Dispose()
#define Enumerator_Dispose_m4151025829(__this, method) ((  void (*) (Enumerator_t551568485 *, const MethodInfo*))Enumerator_Dispose_m3916286986_gshared)(__this, method)
