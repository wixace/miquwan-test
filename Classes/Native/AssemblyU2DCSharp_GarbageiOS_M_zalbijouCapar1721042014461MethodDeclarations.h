﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_zalbijouCapar172
struct M_zalbijouCapar172_t1042014461;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_zalbijouCapar172::.ctor()
extern "C"  void M_zalbijouCapar172__ctor_m2337892822 (M_zalbijouCapar172_t1042014461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_zalbijouCapar172::M_nodere0(System.String[],System.Int32)
extern "C"  void M_zalbijouCapar172_M_nodere0_m3400380980 (M_zalbijouCapar172_t1042014461 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_zalbijouCapar172::M_heparheLefairti1(System.String[],System.Int32)
extern "C"  void M_zalbijouCapar172_M_heparheLefairti1_m676062037 (M_zalbijouCapar172_t1042014461 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
