﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_SkyboxGenerated
struct UnityEngine_SkyboxGenerated_t1455725885;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_SkyboxGenerated::.ctor()
extern "C"  void UnityEngine_SkyboxGenerated__ctor_m3331629774 (UnityEngine_SkyboxGenerated_t1455725885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SkyboxGenerated::Skybox_Skybox1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SkyboxGenerated_Skybox_Skybox1_m1008723556 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SkyboxGenerated::Skybox_material(JSVCall)
extern "C"  void UnityEngine_SkyboxGenerated_Skybox_material_m1569689791 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SkyboxGenerated::__Register()
extern "C"  void UnityEngine_SkyboxGenerated___Register_m1814588089 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_SkyboxGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_SkyboxGenerated_ilo_getObject1_m3287585064 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SkyboxGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_SkyboxGenerated_ilo_addJSCSRel2_m2287423243 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_SkyboxGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_SkyboxGenerated_ilo_getObject3_m3681850073 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
