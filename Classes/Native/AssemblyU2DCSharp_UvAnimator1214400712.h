﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UvAnimator
struct  UvAnimator_t1214400712  : public MonoBehaviour_t667441552
{
public:
	// System.Single UvAnimator::tileX
	float ___tileX_2;
	// System.Single UvAnimator::tileY
	float ___tileY_3;
	// System.Single UvAnimator::fps
	float ___fps_4;

public:
	inline static int32_t get_offset_of_tileX_2() { return static_cast<int32_t>(offsetof(UvAnimator_t1214400712, ___tileX_2)); }
	inline float get_tileX_2() const { return ___tileX_2; }
	inline float* get_address_of_tileX_2() { return &___tileX_2; }
	inline void set_tileX_2(float value)
	{
		___tileX_2 = value;
	}

	inline static int32_t get_offset_of_tileY_3() { return static_cast<int32_t>(offsetof(UvAnimator_t1214400712, ___tileY_3)); }
	inline float get_tileY_3() const { return ___tileY_3; }
	inline float* get_address_of_tileY_3() { return &___tileY_3; }
	inline void set_tileY_3(float value)
	{
		___tileY_3 = value;
	}

	inline static int32_t get_offset_of_fps_4() { return static_cast<int32_t>(offsetof(UvAnimator_t1214400712, ___fps_4)); }
	inline float get_fps_4() const { return ___fps_4; }
	inline float* get_address_of_fps_4() { return &___fps_4; }
	inline void set_fps_4(float value)
	{
		___fps_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
