﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t667441552;
// System.String
struct String_t;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// UnityEngine.GameObject MonoBehaviourVisitor::FindChild(UnityEngine.MonoBehaviour,System.String)
extern "C"  GameObject_t3674682005 * MonoBehaviourVisitor_FindChild_m3188683192 (Il2CppObject * __this /* static, unused */, MonoBehaviour_t667441552 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonoBehaviourVisitor::FindChildren(UnityEngine.MonoBehaviour,System.String,System.Collections.Generic.List`1<UnityEngine.GameObject>)
extern "C"  void MonoBehaviourVisitor_FindChildren_m4060426346 (Il2CppObject * __this /* static, unused */, MonoBehaviour_t667441552 * ____this0, String_t* ___name1, List_1_t747900261 * ___objs2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonoBehaviourVisitor::ilo_FindChildren1(UnityEngine.GameObject,System.String,System.Collections.Generic.List`1<UnityEngine.GameObject>)
extern "C"  void MonoBehaviourVisitor_ilo_FindChildren1_m3899563923 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, String_t* ___name1, List_1_t747900261 * ___objs2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
