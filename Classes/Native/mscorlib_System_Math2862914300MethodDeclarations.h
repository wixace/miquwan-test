﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Decimal1954350631.h"
#include "mscorlib_System_MidpointRounding365021552.h"

// System.Decimal System.Math::Abs(System.Decimal)
extern "C"  Decimal_t1954350631  Math_Abs_m2701483556 (Il2CppObject * __this /* static, unused */, Decimal_t1954350631  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Abs(System.Double)
extern "C"  double Math_Abs_m3458777442 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.Math::Abs(System.Single)
extern "C"  float Math_Abs_m4017239106 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Abs(System.Int32)
extern "C"  int32_t Math_Abs_m2633004394 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Math::Abs(System.Int64)
extern "C"  int64_t Math_Abs_m651703468 (Il2CppObject * __this /* static, unused */, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte System.Math::Abs(System.SByte)
extern "C"  int8_t Math_Abs_m3486624336 (Il2CppObject * __this /* static, unused */, int8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.Math::Abs(System.Int16)
extern "C"  int16_t Math_Abs_m3254908382 (Il2CppObject * __this /* static, unused */, int16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Math::Ceiling(System.Decimal)
extern "C"  Decimal_t1954350631  Math_Ceiling_m4106971887 (Il2CppObject * __this /* static, unused */, Decimal_t1954350631  ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Ceiling(System.Double)
extern "C"  double Math_Ceiling_m3914511415 (Il2CppObject * __this /* static, unused */, double ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Math::BigMul(System.Int32,System.Int32)
extern "C"  int64_t Math_BigMul_m2578487688 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::DivRem(System.Int32,System.Int32,System.Int32&)
extern "C"  int32_t Math_DivRem_m857892695 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, int32_t* ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Math::DivRem(System.Int64,System.Int64,System.Int64&)
extern "C"  int64_t Math_DivRem_m1075206551 (Il2CppObject * __this /* static, unused */, int64_t ___a0, int64_t ___b1, int64_t* ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Floor(System.Double)
extern "C"  double Math_Floor_m1625025992 (Il2CppObject * __this /* static, unused */, double ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::IEEERemainder(System.Double,System.Double)
extern "C"  double Math_IEEERemainder_m2736405383 (Il2CppObject * __this /* static, unused */, double ___x0, double ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Log(System.Double,System.Double)
extern "C"  double Math_Log_m2309558476 (Il2CppObject * __this /* static, unused */, double ___a0, double ___newBase1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Math::Max(System.Byte,System.Byte)
extern "C"  uint8_t Math_Max_m2691083957 (Il2CppObject * __this /* static, unused */, uint8_t ___val10, uint8_t ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Math::Max(System.Decimal,System.Decimal)
extern "C"  Decimal_t1954350631  Math_Max_m2588957598 (Il2CppObject * __this /* static, unused */, Decimal_t1954350631  ___val10, Decimal_t1954350631  ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Max(System.Double,System.Double)
extern "C"  double Math_Max_m4170564268 (Il2CppObject * __this /* static, unused */, double ___val10, double ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.Math::Max(System.Single,System.Single)
extern "C"  float Math_Max_m172798965 (Il2CppObject * __this /* static, unused */, float ___val10, float ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Max(System.Int32,System.Int32)
extern "C"  int32_t Math_Max_m1309380475 (Il2CppObject * __this /* static, unused */, int32_t ___val10, int32_t ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Math::Max(System.Int64,System.Int64)
extern "C"  int64_t Math_Max_m136328954 (Il2CppObject * __this /* static, unused */, int64_t ___val10, int64_t ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte System.Math::Max(System.SByte,System.SByte)
extern "C"  int8_t Math_Max_m2578033768 (Il2CppObject * __this /* static, unused */, int8_t ___val10, int8_t ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.Math::Max(System.Int16,System.Int16)
extern "C"  int16_t Math_Max_m3924386945 (Il2CppObject * __this /* static, unused */, int16_t ___val10, int16_t ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Math::Max(System.UInt32,System.UInt32)
extern "C"  uint32_t Math_Max_m383238500 (Il2CppObject * __this /* static, unused */, uint32_t ___val10, uint32_t ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System.Math::Max(System.UInt64,System.UInt64)
extern "C"  uint64_t Math_Max_m1682789573 (Il2CppObject * __this /* static, unused */, uint64_t ___val10, uint64_t ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System.Math::Max(System.UInt16,System.UInt16)
extern "C"  uint16_t Math_Max_m1579076382 (Il2CppObject * __this /* static, unused */, uint16_t ___val10, uint16_t ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Math::Min(System.Byte,System.Byte)
extern "C"  uint8_t Math_Min_m1483863431 (Il2CppObject * __this /* static, unused */, uint8_t ___val10, uint8_t ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Math::Min(System.Decimal,System.Decimal)
extern "C"  Decimal_t1954350631  Math_Min_m925613296 (Il2CppObject * __this /* static, unused */, Decimal_t1954350631  ___val10, Decimal_t1954350631  ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Min(System.Double,System.Double)
extern "C"  double Math_Min_m2568835198 (Il2CppObject * __this /* static, unused */, double ___val10, double ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.Math::Min(System.Single,System.Single)
extern "C"  float Math_Min_m2866037191 (Il2CppObject * __this /* static, unused */, float ___val10, float ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Min(System.Int32,System.Int32)
extern "C"  int32_t Math_Min_m811624909 (Il2CppObject * __this /* static, unused */, int32_t ___val10, int32_t ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Math::Min(System.Int64,System.Int64)
extern "C"  int64_t Math_Min_m3933540684 (Il2CppObject * __this /* static, unused */, int64_t ___val10, int64_t ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte System.Math::Min(System.SByte,System.SByte)
extern "C"  int8_t Math_Min_m2080278202 (Il2CppObject * __this /* static, unused */, int8_t ___val10, int8_t ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.Math::Min(System.Int16,System.Int16)
extern "C"  int16_t Math_Min_m3426631379 (Il2CppObject * __this /* static, unused */, int16_t ___val10, int16_t ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Math::Min(System.UInt32,System.UInt32)
extern "C"  uint32_t Math_Min_m3076476726 (Il2CppObject * __this /* static, unused */, uint32_t ___val10, uint32_t ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System.Math::Min(System.UInt64,System.UInt64)
extern "C"  uint64_t Math_Min_m81060503 (Il2CppObject * __this /* static, unused */, uint64_t ___val10, uint64_t ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System.Math::Min(System.UInt16,System.UInt16)
extern "C"  uint16_t Math_Min_m4272314608 (Il2CppObject * __this /* static, unused */, uint16_t ___val10, uint16_t ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Math::Round(System.Decimal)
extern "C"  Decimal_t1954350631  Math_Round_m2659396224 (Il2CppObject * __this /* static, unused */, Decimal_t1954350631  ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Math::Round(System.Decimal,System.Int32)
extern "C"  Decimal_t1954350631  Math_Round_m2407072343 (Il2CppObject * __this /* static, unused */, Decimal_t1954350631  ___d0, int32_t ___decimals1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Math::Round(System.Decimal,System.MidpointRounding)
extern "C"  Decimal_t1954350631  Math_Round_m786313009 (Il2CppObject * __this /* static, unused */, Decimal_t1954350631  ___d0, int32_t ___mode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Math::RoundAwayFromZero(System.Decimal)
extern "C"  Decimal_t1954350631  Math_RoundAwayFromZero_m3795004320 (Il2CppObject * __this /* static, unused */, Decimal_t1954350631  ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Math::Round(System.Decimal,System.Int32,System.MidpointRounding)
extern "C"  Decimal_t1954350631  Math_Round_m1712304648 (Il2CppObject * __this /* static, unused */, Decimal_t1954350631  ___d0, int32_t ___decimals1, int32_t ___mode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Round(System.Double)
extern "C"  double Math_Round_m2587388934 (Il2CppObject * __this /* static, unused */, double ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Round(System.Double,System.Int32)
extern "C"  double Math_Round_m2351753873 (Il2CppObject * __this /* static, unused */, double ___value0, int32_t ___digits1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Round2(System.Double,System.Int32,System.Boolean)
extern "C"  double Math_Round2_m965515440 (Il2CppObject * __this /* static, unused */, double ___value0, int32_t ___digits1, bool ___away_from_zero2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Round(System.Double,System.MidpointRounding)
extern "C"  double Math_Round_m275300023 (Il2CppObject * __this /* static, unused */, double ___value0, int32_t ___mode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Round(System.Double,System.Int32,System.MidpointRounding)
extern "C"  double Math_Round_m4068281410 (Il2CppObject * __this /* static, unused */, double ___value0, int32_t ___digits1, int32_t ___mode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Truncate(System.Double)
extern "C"  double Math_Truncate_m534017384 (Il2CppObject * __this /* static, unused */, double ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Math::Truncate(System.Decimal)
extern "C"  Decimal_t1954350631  Math_Truncate_m730851058 (Il2CppObject * __this /* static, unused */, Decimal_t1954350631  ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Math::Floor(System.Decimal)
extern "C"  Decimal_t1954350631  Math_Floor_m2890916094 (Il2CppObject * __this /* static, unused */, Decimal_t1954350631  ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Sign(System.Decimal)
extern "C"  int32_t Math_Sign_m2373095686 (Il2CppObject * __this /* static, unused */, Decimal_t1954350631  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Sign(System.Double)
extern "C"  int32_t Math_Sign_m3150203562 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Sign(System.Single)
extern "C"  int32_t Math_Sign_m3399766291 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Sign(System.Int32)
extern "C"  int32_t Math_Sign_m3014711529 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Sign(System.Int64)
extern "C"  int32_t Math_Sign_m3014714474 (Il2CppObject * __this /* static, unused */, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Sign(System.SByte)
extern "C"  int32_t Math_Sign_m3260581116 (Il2CppObject * __this /* static, unused */, int8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Sign(System.Int16)
extern "C"  int32_t Math_Sign_m3014709731 (Il2CppObject * __this /* static, unused */, int16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Sin(System.Double)
extern "C"  double Math_Sin_m1832281148 (Il2CppObject * __this /* static, unused */, double ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Cos(System.Double)
extern "C"  double Math_Cos_m1877789613 (Il2CppObject * __this /* static, unused */, double ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Tan(System.Double)
extern "C"  double Math_Tan_m2893633107 (Il2CppObject * __this /* static, unused */, double ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Sinh(System.Double)
extern "C"  double Math_Sinh_m1230899582 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Cosh(System.Double)
extern "C"  double Math_Cosh_m2641661997 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Tanh(System.Double)
extern "C"  double Math_Tanh_m4068039239 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Acos(System.Double)
extern "C"  double Math_Acos_m971391944 (Il2CppObject * __this /* static, unused */, double ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Asin(System.Double)
extern "C"  double Math_Asin_m925883479 (Il2CppObject * __this /* static, unused */, double ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Atan(System.Double)
extern "C"  double Math_Atan_m1987235438 (Il2CppObject * __this /* static, unused */, double ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Atan2(System.Double,System.Double)
extern "C"  double Math_Atan2_m2472972670 (Il2CppObject * __this /* static, unused */, double ___y0, double ___x1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Exp(System.Double)
extern "C"  double Math_Exp_m710132823 (Il2CppObject * __this /* static, unused */, double ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Log(System.Double)
extern "C"  double Math_Log_m3325515856 (Il2CppObject * __this /* static, unused */, double ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Log10(System.Double)
extern "C"  double Math_Log10_m1649938577 (Il2CppObject * __this /* static, unused */, double ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Pow(System.Double,System.Double)
extern "C"  double Math_Pow_m3040135736 (Il2CppObject * __this /* static, unused */, double ___x0, double ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Sqrt(System.Double)
extern "C"  double Math_Sqrt_m1131084014 (Il2CppObject * __this /* static, unused */, double ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
