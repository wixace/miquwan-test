﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Object,System.Boolean>
struct U3CDelayCallExecU3Ec__Iterator40_2_t1693708955;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Object,System.Boolean>::.ctor()
extern "C"  void U3CDelayCallExecU3Ec__Iterator40_2__ctor_m1951385641_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t1693708955 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2__ctor_m1951385641(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator40_2_t1693708955 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2__ctor_m1951385641_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Object,System.Boolean>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3778172691_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t1693708955 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3778172691(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator40_2_t1693708955 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3778172691_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_IEnumerator_get_Current_m2099437735_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t1693708955 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_IEnumerator_get_Current_m2099437735(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator40_2_t1693708955 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_IEnumerator_get_Current_m2099437735_gshared)(__this, method)
// System.Boolean BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Object,System.Boolean>::MoveNext()
extern "C"  bool U3CDelayCallExecU3Ec__Iterator40_2_MoveNext_m3744604179_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t1693708955 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2_MoveNext_m3744604179(__this, method) ((  bool (*) (U3CDelayCallExecU3Ec__Iterator40_2_t1693708955 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2_MoveNext_m3744604179_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Object,System.Boolean>::Dispose()
extern "C"  void U3CDelayCallExecU3Ec__Iterator40_2_Dispose_m2574143014_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t1693708955 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2_Dispose_m2574143014(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator40_2_t1693708955 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2_Dispose_m2574143014_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Object,System.Boolean>::Reset()
extern "C"  void U3CDelayCallExecU3Ec__Iterator40_2_Reset_m3892785878_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t1693708955 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2_Reset_m3892785878(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator40_2_t1693708955 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2_Reset_m3892785878_gshared)(__this, method)
