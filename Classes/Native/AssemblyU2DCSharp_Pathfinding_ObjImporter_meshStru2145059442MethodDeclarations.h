﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.ObjImporter/meshStruct
struct meshStruct_t2145059442;
struct meshStruct_t2145059442_marshaled_pinvoke;
struct meshStruct_t2145059442_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct meshStruct_t2145059442;
struct meshStruct_t2145059442_marshaled_pinvoke;

extern "C" void meshStruct_t2145059442_marshal_pinvoke(const meshStruct_t2145059442& unmarshaled, meshStruct_t2145059442_marshaled_pinvoke& marshaled);
extern "C" void meshStruct_t2145059442_marshal_pinvoke_back(const meshStruct_t2145059442_marshaled_pinvoke& marshaled, meshStruct_t2145059442& unmarshaled);
extern "C" void meshStruct_t2145059442_marshal_pinvoke_cleanup(meshStruct_t2145059442_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct meshStruct_t2145059442;
struct meshStruct_t2145059442_marshaled_com;

extern "C" void meshStruct_t2145059442_marshal_com(const meshStruct_t2145059442& unmarshaled, meshStruct_t2145059442_marshaled_com& marshaled);
extern "C" void meshStruct_t2145059442_marshal_com_back(const meshStruct_t2145059442_marshaled_com& marshaled, meshStruct_t2145059442& unmarshaled);
extern "C" void meshStruct_t2145059442_marshal_com_cleanup(meshStruct_t2145059442_marshaled_com& marshaled);
