﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.ProtoIgnoreAttribute
struct ProtoIgnoreAttribute_t1315037806;

#include "codegen/il2cpp-codegen.h"

// System.Void ProtoBuf.ProtoIgnoreAttribute::.ctor()
extern "C"  void ProtoIgnoreAttribute__ctor_m288665222 (ProtoIgnoreAttribute_t1315037806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
