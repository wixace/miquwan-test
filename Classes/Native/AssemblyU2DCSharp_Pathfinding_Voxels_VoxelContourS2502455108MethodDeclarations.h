﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Voxels.VoxelContourSet
struct VoxelContourSet_t2502455108;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.Voxels.VoxelContourSet::.ctor()
extern "C"  void VoxelContourSet__ctor_m1954206692 (VoxelContourSet_t2502455108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
