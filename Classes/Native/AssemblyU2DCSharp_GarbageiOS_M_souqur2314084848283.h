﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_souqur231
struct  M_souqur231_t4084848283  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_souqur231::_pojoziJoudear
	uint32_t ____pojoziJoudear_0;
	// System.String GarbageiOS.M_souqur231::_kamo
	String_t* ____kamo_1;
	// System.Boolean GarbageiOS.M_souqur231::_gexibaKonajoo
	bool ____gexibaKonajoo_2;
	// System.Single GarbageiOS.M_souqur231::_meaxerkayDorlirfel
	float ____meaxerkayDorlirfel_3;
	// System.Single GarbageiOS.M_souqur231::_trepal
	float ____trepal_4;
	// System.Boolean GarbageiOS.M_souqur231::_maisee
	bool ____maisee_5;
	// System.Boolean GarbageiOS.M_souqur231::_ceremeejow
	bool ____ceremeejow_6;

public:
	inline static int32_t get_offset_of__pojoziJoudear_0() { return static_cast<int32_t>(offsetof(M_souqur231_t4084848283, ____pojoziJoudear_0)); }
	inline uint32_t get__pojoziJoudear_0() const { return ____pojoziJoudear_0; }
	inline uint32_t* get_address_of__pojoziJoudear_0() { return &____pojoziJoudear_0; }
	inline void set__pojoziJoudear_0(uint32_t value)
	{
		____pojoziJoudear_0 = value;
	}

	inline static int32_t get_offset_of__kamo_1() { return static_cast<int32_t>(offsetof(M_souqur231_t4084848283, ____kamo_1)); }
	inline String_t* get__kamo_1() const { return ____kamo_1; }
	inline String_t** get_address_of__kamo_1() { return &____kamo_1; }
	inline void set__kamo_1(String_t* value)
	{
		____kamo_1 = value;
		Il2CppCodeGenWriteBarrier(&____kamo_1, value);
	}

	inline static int32_t get_offset_of__gexibaKonajoo_2() { return static_cast<int32_t>(offsetof(M_souqur231_t4084848283, ____gexibaKonajoo_2)); }
	inline bool get__gexibaKonajoo_2() const { return ____gexibaKonajoo_2; }
	inline bool* get_address_of__gexibaKonajoo_2() { return &____gexibaKonajoo_2; }
	inline void set__gexibaKonajoo_2(bool value)
	{
		____gexibaKonajoo_2 = value;
	}

	inline static int32_t get_offset_of__meaxerkayDorlirfel_3() { return static_cast<int32_t>(offsetof(M_souqur231_t4084848283, ____meaxerkayDorlirfel_3)); }
	inline float get__meaxerkayDorlirfel_3() const { return ____meaxerkayDorlirfel_3; }
	inline float* get_address_of__meaxerkayDorlirfel_3() { return &____meaxerkayDorlirfel_3; }
	inline void set__meaxerkayDorlirfel_3(float value)
	{
		____meaxerkayDorlirfel_3 = value;
	}

	inline static int32_t get_offset_of__trepal_4() { return static_cast<int32_t>(offsetof(M_souqur231_t4084848283, ____trepal_4)); }
	inline float get__trepal_4() const { return ____trepal_4; }
	inline float* get_address_of__trepal_4() { return &____trepal_4; }
	inline void set__trepal_4(float value)
	{
		____trepal_4 = value;
	}

	inline static int32_t get_offset_of__maisee_5() { return static_cast<int32_t>(offsetof(M_souqur231_t4084848283, ____maisee_5)); }
	inline bool get__maisee_5() const { return ____maisee_5; }
	inline bool* get_address_of__maisee_5() { return &____maisee_5; }
	inline void set__maisee_5(bool value)
	{
		____maisee_5 = value;
	}

	inline static int32_t get_offset_of__ceremeejow_6() { return static_cast<int32_t>(offsetof(M_souqur231_t4084848283, ____ceremeejow_6)); }
	inline bool get__ceremeejow_6() const { return ____ceremeejow_6; }
	inline bool* get_address_of__ceremeejow_6() { return &____ceremeejow_6; }
	inline void set__ceremeejow_6(bool value)
	{
		____ceremeejow_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
