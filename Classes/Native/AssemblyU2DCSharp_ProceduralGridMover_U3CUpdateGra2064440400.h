﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.GridNode[]
struct GridNodeU5BU5D_t2925197291;
// Pathfinding.GridNode
struct GridNode_t3795753694;
// System.Object
struct Il2CppObject;
// ProceduralGridMover
struct ProceduralGridMover_t1558121470;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"
#include "AssemblyU2DCSharp_Pathfinding_IntRect3015058261.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10
struct  U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<dir>__0
	Vector3_t4282066566  ___U3CdirU3E__0_0;
	// Pathfinding.Int2 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<offset>__1
	Int2_t1974045593  ___U3CoffsetU3E__1_1;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<width>__2
	int32_t ___U3CwidthU3E__2_2;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<depth>__3
	int32_t ___U3CdepthU3E__3_3;
	// Pathfinding.GridNode[] ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<nodes>__4
	GridNodeU5BU5D_t2925197291* ___U3CnodesU3E__4_4;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<z>__5
	int32_t ___U3CzU3E__5_5;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<pz>__6
	int32_t ___U3CpzU3E__6_6;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<tz>__7
	int32_t ___U3CtzU3E__7_7;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<x>__8
	int32_t ___U3CxU3E__8_8;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<z>__9
	int32_t ___U3CzU3E__9_9;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<pz>__10
	int32_t ___U3CpzU3E__10_10;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<x>__11
	int32_t ___U3CxU3E__11_11;
	// Pathfinding.GridNode ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<node>__12
	GridNode_t3795753694 * ___U3CnodeU3E__12_12;
	// Pathfinding.IntRect ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<r>__13
	IntRect_t3015058261  ___U3CrU3E__13_13;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<minz>__14
	int32_t ___U3CminzU3E__14_14;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<maxz>__15
	int32_t ___U3CmaxzU3E__15_15;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<tmp2>__16
	int32_t ___U3Ctmp2U3E__16_16;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<tmp2>__17
	int32_t ___U3Ctmp2U3E__17_17;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<z>__18
	int32_t ___U3CzU3E__18_18;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<x>__19
	int32_t ___U3CxU3E__19_19;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<z>__20
	int32_t ___U3CzU3E__20_20;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<x>__21
	int32_t ___U3CxU3E__21_21;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<z>__22
	int32_t ___U3CzU3E__22_22;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<x>__23
	int32_t ___U3CxU3E__23_23;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<z>__24
	int32_t ___U3CzU3E__24_24;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<x>__25
	int32_t ___U3CxU3E__25_25;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<z>__26
	int32_t ___U3CzU3E__26_26;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<x>__27
	int32_t ___U3CxU3E__27_27;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<z>__28
	int32_t ___U3CzU3E__28_28;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<x>__29
	int32_t ___U3CxU3E__29_29;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<z>__30
	int32_t ___U3CzU3E__30_30;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<x>__31
	int32_t ___U3CxU3E__31_31;
	// System.Int32 ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::$PC
	int32_t ___U24PC_32;
	// System.Object ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::$current
	Il2CppObject * ___U24current_33;
	// ProceduralGridMover ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::<>f__this
	ProceduralGridMover_t1558121470 * ___U3CU3Ef__this_34;

public:
	inline static int32_t get_offset_of_U3CdirU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CdirU3E__0_0)); }
	inline Vector3_t4282066566  get_U3CdirU3E__0_0() const { return ___U3CdirU3E__0_0; }
	inline Vector3_t4282066566 * get_address_of_U3CdirU3E__0_0() { return &___U3CdirU3E__0_0; }
	inline void set_U3CdirU3E__0_0(Vector3_t4282066566  value)
	{
		___U3CdirU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CoffsetU3E__1_1() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CoffsetU3E__1_1)); }
	inline Int2_t1974045593  get_U3CoffsetU3E__1_1() const { return ___U3CoffsetU3E__1_1; }
	inline Int2_t1974045593 * get_address_of_U3CoffsetU3E__1_1() { return &___U3CoffsetU3E__1_1; }
	inline void set_U3CoffsetU3E__1_1(Int2_t1974045593  value)
	{
		___U3CoffsetU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CwidthU3E__2_2() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CwidthU3E__2_2)); }
	inline int32_t get_U3CwidthU3E__2_2() const { return ___U3CwidthU3E__2_2; }
	inline int32_t* get_address_of_U3CwidthU3E__2_2() { return &___U3CwidthU3E__2_2; }
	inline void set_U3CwidthU3E__2_2(int32_t value)
	{
		___U3CwidthU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CdepthU3E__3_3() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CdepthU3E__3_3)); }
	inline int32_t get_U3CdepthU3E__3_3() const { return ___U3CdepthU3E__3_3; }
	inline int32_t* get_address_of_U3CdepthU3E__3_3() { return &___U3CdepthU3E__3_3; }
	inline void set_U3CdepthU3E__3_3(int32_t value)
	{
		___U3CdepthU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U3CnodesU3E__4_4() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CnodesU3E__4_4)); }
	inline GridNodeU5BU5D_t2925197291* get_U3CnodesU3E__4_4() const { return ___U3CnodesU3E__4_4; }
	inline GridNodeU5BU5D_t2925197291** get_address_of_U3CnodesU3E__4_4() { return &___U3CnodesU3E__4_4; }
	inline void set_U3CnodesU3E__4_4(GridNodeU5BU5D_t2925197291* value)
	{
		___U3CnodesU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CnodesU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CzU3E__5_5() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CzU3E__5_5)); }
	inline int32_t get_U3CzU3E__5_5() const { return ___U3CzU3E__5_5; }
	inline int32_t* get_address_of_U3CzU3E__5_5() { return &___U3CzU3E__5_5; }
	inline void set_U3CzU3E__5_5(int32_t value)
	{
		___U3CzU3E__5_5 = value;
	}

	inline static int32_t get_offset_of_U3CpzU3E__6_6() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CpzU3E__6_6)); }
	inline int32_t get_U3CpzU3E__6_6() const { return ___U3CpzU3E__6_6; }
	inline int32_t* get_address_of_U3CpzU3E__6_6() { return &___U3CpzU3E__6_6; }
	inline void set_U3CpzU3E__6_6(int32_t value)
	{
		___U3CpzU3E__6_6 = value;
	}

	inline static int32_t get_offset_of_U3CtzU3E__7_7() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CtzU3E__7_7)); }
	inline int32_t get_U3CtzU3E__7_7() const { return ___U3CtzU3E__7_7; }
	inline int32_t* get_address_of_U3CtzU3E__7_7() { return &___U3CtzU3E__7_7; }
	inline void set_U3CtzU3E__7_7(int32_t value)
	{
		___U3CtzU3E__7_7 = value;
	}

	inline static int32_t get_offset_of_U3CxU3E__8_8() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CxU3E__8_8)); }
	inline int32_t get_U3CxU3E__8_8() const { return ___U3CxU3E__8_8; }
	inline int32_t* get_address_of_U3CxU3E__8_8() { return &___U3CxU3E__8_8; }
	inline void set_U3CxU3E__8_8(int32_t value)
	{
		___U3CxU3E__8_8 = value;
	}

	inline static int32_t get_offset_of_U3CzU3E__9_9() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CzU3E__9_9)); }
	inline int32_t get_U3CzU3E__9_9() const { return ___U3CzU3E__9_9; }
	inline int32_t* get_address_of_U3CzU3E__9_9() { return &___U3CzU3E__9_9; }
	inline void set_U3CzU3E__9_9(int32_t value)
	{
		___U3CzU3E__9_9 = value;
	}

	inline static int32_t get_offset_of_U3CpzU3E__10_10() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CpzU3E__10_10)); }
	inline int32_t get_U3CpzU3E__10_10() const { return ___U3CpzU3E__10_10; }
	inline int32_t* get_address_of_U3CpzU3E__10_10() { return &___U3CpzU3E__10_10; }
	inline void set_U3CpzU3E__10_10(int32_t value)
	{
		___U3CpzU3E__10_10 = value;
	}

	inline static int32_t get_offset_of_U3CxU3E__11_11() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CxU3E__11_11)); }
	inline int32_t get_U3CxU3E__11_11() const { return ___U3CxU3E__11_11; }
	inline int32_t* get_address_of_U3CxU3E__11_11() { return &___U3CxU3E__11_11; }
	inline void set_U3CxU3E__11_11(int32_t value)
	{
		___U3CxU3E__11_11 = value;
	}

	inline static int32_t get_offset_of_U3CnodeU3E__12_12() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CnodeU3E__12_12)); }
	inline GridNode_t3795753694 * get_U3CnodeU3E__12_12() const { return ___U3CnodeU3E__12_12; }
	inline GridNode_t3795753694 ** get_address_of_U3CnodeU3E__12_12() { return &___U3CnodeU3E__12_12; }
	inline void set_U3CnodeU3E__12_12(GridNode_t3795753694 * value)
	{
		___U3CnodeU3E__12_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CnodeU3E__12_12, value);
	}

	inline static int32_t get_offset_of_U3CrU3E__13_13() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CrU3E__13_13)); }
	inline IntRect_t3015058261  get_U3CrU3E__13_13() const { return ___U3CrU3E__13_13; }
	inline IntRect_t3015058261 * get_address_of_U3CrU3E__13_13() { return &___U3CrU3E__13_13; }
	inline void set_U3CrU3E__13_13(IntRect_t3015058261  value)
	{
		___U3CrU3E__13_13 = value;
	}

	inline static int32_t get_offset_of_U3CminzU3E__14_14() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CminzU3E__14_14)); }
	inline int32_t get_U3CminzU3E__14_14() const { return ___U3CminzU3E__14_14; }
	inline int32_t* get_address_of_U3CminzU3E__14_14() { return &___U3CminzU3E__14_14; }
	inline void set_U3CminzU3E__14_14(int32_t value)
	{
		___U3CminzU3E__14_14 = value;
	}

	inline static int32_t get_offset_of_U3CmaxzU3E__15_15() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CmaxzU3E__15_15)); }
	inline int32_t get_U3CmaxzU3E__15_15() const { return ___U3CmaxzU3E__15_15; }
	inline int32_t* get_address_of_U3CmaxzU3E__15_15() { return &___U3CmaxzU3E__15_15; }
	inline void set_U3CmaxzU3E__15_15(int32_t value)
	{
		___U3CmaxzU3E__15_15 = value;
	}

	inline static int32_t get_offset_of_U3Ctmp2U3E__16_16() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3Ctmp2U3E__16_16)); }
	inline int32_t get_U3Ctmp2U3E__16_16() const { return ___U3Ctmp2U3E__16_16; }
	inline int32_t* get_address_of_U3Ctmp2U3E__16_16() { return &___U3Ctmp2U3E__16_16; }
	inline void set_U3Ctmp2U3E__16_16(int32_t value)
	{
		___U3Ctmp2U3E__16_16 = value;
	}

	inline static int32_t get_offset_of_U3Ctmp2U3E__17_17() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3Ctmp2U3E__17_17)); }
	inline int32_t get_U3Ctmp2U3E__17_17() const { return ___U3Ctmp2U3E__17_17; }
	inline int32_t* get_address_of_U3Ctmp2U3E__17_17() { return &___U3Ctmp2U3E__17_17; }
	inline void set_U3Ctmp2U3E__17_17(int32_t value)
	{
		___U3Ctmp2U3E__17_17 = value;
	}

	inline static int32_t get_offset_of_U3CzU3E__18_18() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CzU3E__18_18)); }
	inline int32_t get_U3CzU3E__18_18() const { return ___U3CzU3E__18_18; }
	inline int32_t* get_address_of_U3CzU3E__18_18() { return &___U3CzU3E__18_18; }
	inline void set_U3CzU3E__18_18(int32_t value)
	{
		___U3CzU3E__18_18 = value;
	}

	inline static int32_t get_offset_of_U3CxU3E__19_19() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CxU3E__19_19)); }
	inline int32_t get_U3CxU3E__19_19() const { return ___U3CxU3E__19_19; }
	inline int32_t* get_address_of_U3CxU3E__19_19() { return &___U3CxU3E__19_19; }
	inline void set_U3CxU3E__19_19(int32_t value)
	{
		___U3CxU3E__19_19 = value;
	}

	inline static int32_t get_offset_of_U3CzU3E__20_20() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CzU3E__20_20)); }
	inline int32_t get_U3CzU3E__20_20() const { return ___U3CzU3E__20_20; }
	inline int32_t* get_address_of_U3CzU3E__20_20() { return &___U3CzU3E__20_20; }
	inline void set_U3CzU3E__20_20(int32_t value)
	{
		___U3CzU3E__20_20 = value;
	}

	inline static int32_t get_offset_of_U3CxU3E__21_21() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CxU3E__21_21)); }
	inline int32_t get_U3CxU3E__21_21() const { return ___U3CxU3E__21_21; }
	inline int32_t* get_address_of_U3CxU3E__21_21() { return &___U3CxU3E__21_21; }
	inline void set_U3CxU3E__21_21(int32_t value)
	{
		___U3CxU3E__21_21 = value;
	}

	inline static int32_t get_offset_of_U3CzU3E__22_22() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CzU3E__22_22)); }
	inline int32_t get_U3CzU3E__22_22() const { return ___U3CzU3E__22_22; }
	inline int32_t* get_address_of_U3CzU3E__22_22() { return &___U3CzU3E__22_22; }
	inline void set_U3CzU3E__22_22(int32_t value)
	{
		___U3CzU3E__22_22 = value;
	}

	inline static int32_t get_offset_of_U3CxU3E__23_23() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CxU3E__23_23)); }
	inline int32_t get_U3CxU3E__23_23() const { return ___U3CxU3E__23_23; }
	inline int32_t* get_address_of_U3CxU3E__23_23() { return &___U3CxU3E__23_23; }
	inline void set_U3CxU3E__23_23(int32_t value)
	{
		___U3CxU3E__23_23 = value;
	}

	inline static int32_t get_offset_of_U3CzU3E__24_24() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CzU3E__24_24)); }
	inline int32_t get_U3CzU3E__24_24() const { return ___U3CzU3E__24_24; }
	inline int32_t* get_address_of_U3CzU3E__24_24() { return &___U3CzU3E__24_24; }
	inline void set_U3CzU3E__24_24(int32_t value)
	{
		___U3CzU3E__24_24 = value;
	}

	inline static int32_t get_offset_of_U3CxU3E__25_25() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CxU3E__25_25)); }
	inline int32_t get_U3CxU3E__25_25() const { return ___U3CxU3E__25_25; }
	inline int32_t* get_address_of_U3CxU3E__25_25() { return &___U3CxU3E__25_25; }
	inline void set_U3CxU3E__25_25(int32_t value)
	{
		___U3CxU3E__25_25 = value;
	}

	inline static int32_t get_offset_of_U3CzU3E__26_26() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CzU3E__26_26)); }
	inline int32_t get_U3CzU3E__26_26() const { return ___U3CzU3E__26_26; }
	inline int32_t* get_address_of_U3CzU3E__26_26() { return &___U3CzU3E__26_26; }
	inline void set_U3CzU3E__26_26(int32_t value)
	{
		___U3CzU3E__26_26 = value;
	}

	inline static int32_t get_offset_of_U3CxU3E__27_27() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CxU3E__27_27)); }
	inline int32_t get_U3CxU3E__27_27() const { return ___U3CxU3E__27_27; }
	inline int32_t* get_address_of_U3CxU3E__27_27() { return &___U3CxU3E__27_27; }
	inline void set_U3CxU3E__27_27(int32_t value)
	{
		___U3CxU3E__27_27 = value;
	}

	inline static int32_t get_offset_of_U3CzU3E__28_28() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CzU3E__28_28)); }
	inline int32_t get_U3CzU3E__28_28() const { return ___U3CzU3E__28_28; }
	inline int32_t* get_address_of_U3CzU3E__28_28() { return &___U3CzU3E__28_28; }
	inline void set_U3CzU3E__28_28(int32_t value)
	{
		___U3CzU3E__28_28 = value;
	}

	inline static int32_t get_offset_of_U3CxU3E__29_29() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CxU3E__29_29)); }
	inline int32_t get_U3CxU3E__29_29() const { return ___U3CxU3E__29_29; }
	inline int32_t* get_address_of_U3CxU3E__29_29() { return &___U3CxU3E__29_29; }
	inline void set_U3CxU3E__29_29(int32_t value)
	{
		___U3CxU3E__29_29 = value;
	}

	inline static int32_t get_offset_of_U3CzU3E__30_30() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CzU3E__30_30)); }
	inline int32_t get_U3CzU3E__30_30() const { return ___U3CzU3E__30_30; }
	inline int32_t* get_address_of_U3CzU3E__30_30() { return &___U3CzU3E__30_30; }
	inline void set_U3CzU3E__30_30(int32_t value)
	{
		___U3CzU3E__30_30 = value;
	}

	inline static int32_t get_offset_of_U3CxU3E__31_31() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CxU3E__31_31)); }
	inline int32_t get_U3CxU3E__31_31() const { return ___U3CxU3E__31_31; }
	inline int32_t* get_address_of_U3CxU3E__31_31() { return &___U3CxU3E__31_31; }
	inline void set_U3CxU3E__31_31(int32_t value)
	{
		___U3CxU3E__31_31 = value;
	}

	inline static int32_t get_offset_of_U24PC_32() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U24PC_32)); }
	inline int32_t get_U24PC_32() const { return ___U24PC_32; }
	inline int32_t* get_address_of_U24PC_32() { return &___U24PC_32; }
	inline void set_U24PC_32(int32_t value)
	{
		___U24PC_32 = value;
	}

	inline static int32_t get_offset_of_U24current_33() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U24current_33)); }
	inline Il2CppObject * get_U24current_33() const { return ___U24current_33; }
	inline Il2CppObject ** get_address_of_U24current_33() { return &___U24current_33; }
	inline void set_U24current_33(Il2CppObject * value)
	{
		___U24current_33 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_33, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_34() { return static_cast<int32_t>(offsetof(U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400, ___U3CU3Ef__this_34)); }
	inline ProceduralGridMover_t1558121470 * get_U3CU3Ef__this_34() const { return ___U3CU3Ef__this_34; }
	inline ProceduralGridMover_t1558121470 ** get_address_of_U3CU3Ef__this_34() { return &___U3CU3Ef__this_34; }
	inline void set_U3CU3Ef__this_34(ProceduralGridMover_t1558121470 * value)
	{
		___U3CU3Ef__this_34 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_34, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
