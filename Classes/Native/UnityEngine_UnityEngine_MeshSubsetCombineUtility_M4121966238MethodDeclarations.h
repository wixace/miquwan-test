﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.MeshSubsetCombineUtility/MeshInstance
struct MeshInstance_t4121966238;
struct MeshInstance_t4121966238_marshaled_pinvoke;
struct MeshInstance_t4121966238_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct MeshInstance_t4121966238;
struct MeshInstance_t4121966238_marshaled_pinvoke;

extern "C" void MeshInstance_t4121966238_marshal_pinvoke(const MeshInstance_t4121966238& unmarshaled, MeshInstance_t4121966238_marshaled_pinvoke& marshaled);
extern "C" void MeshInstance_t4121966238_marshal_pinvoke_back(const MeshInstance_t4121966238_marshaled_pinvoke& marshaled, MeshInstance_t4121966238& unmarshaled);
extern "C" void MeshInstance_t4121966238_marshal_pinvoke_cleanup(MeshInstance_t4121966238_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct MeshInstance_t4121966238;
struct MeshInstance_t4121966238_marshaled_com;

extern "C" void MeshInstance_t4121966238_marshal_com(const MeshInstance_t4121966238& unmarshaled, MeshInstance_t4121966238_marshaled_com& marshaled);
extern "C" void MeshInstance_t4121966238_marshal_com_back(const MeshInstance_t4121966238_marshaled_com& marshaled, MeshInstance_t4121966238& unmarshaled);
extern "C" void MeshInstance_t4121966238_marshal_com_cleanup(MeshInstance_t4121966238_marshaled_com& marshaled);
