﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TweenColorGenerated
struct TweenColorGenerated_t4039699607;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// TweenColor
struct TweenColor_t2922258392;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_TweenColor2922258392.h"

// System.Void TweenColorGenerated::.ctor()
extern "C"  void TweenColorGenerated__ctor_m3723518132 (TweenColorGenerated_t4039699607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenColorGenerated::TweenColor_TweenColor1(JSVCall,System.Int32)
extern "C"  bool TweenColorGenerated_TweenColor_TweenColor1_m643448446 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenColorGenerated::TweenColor_from(JSVCall)
extern "C"  void TweenColorGenerated_TweenColor_from_m4089848868 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenColorGenerated::TweenColor_to(JSVCall)
extern "C"  void TweenColorGenerated_TweenColor_to_m2206755443 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenColorGenerated::TweenColor_value(JSVCall)
extern "C"  void TweenColorGenerated_TweenColor_value_m3116662061 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenColorGenerated::TweenColor_SetEndToCurrentValue(JSVCall,System.Int32)
extern "C"  bool TweenColorGenerated_TweenColor_SetEndToCurrentValue_m489801449 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenColorGenerated::TweenColor_SetStartToCurrentValue(JSVCall,System.Int32)
extern "C"  bool TweenColorGenerated_TweenColor_SetStartToCurrentValue_m1278734448 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenColorGenerated::TweenColor_Begin__GameObject__Single__Color(JSVCall,System.Int32)
extern "C"  bool TweenColorGenerated_TweenColor_Begin__GameObject__Single__Color_m6891006 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenColorGenerated::__Register()
extern "C"  void TweenColorGenerated___Register_m3256183955 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TweenColorGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t TweenColorGenerated_ilo_setObject1_m3312288890 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TweenColorGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * TweenColorGenerated_ilo_getObject2_m1826700594 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color TweenColorGenerated::ilo_get_value3(TweenColor)
extern "C"  Color_t4194546905  TweenColorGenerated_ilo_get_value3_m1601605071 (Il2CppObject * __this /* static, unused */, TweenColor_t2922258392 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenColorGenerated::ilo_set_value4(TweenColor,UnityEngine.Color)
extern "C"  void TweenColorGenerated_ilo_set_value4_m1249664139 (Il2CppObject * __this /* static, unused */, TweenColor_t2922258392 * ____this0, Color_t4194546905  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenColorGenerated::ilo_SetEndToCurrentValue5(TweenColor)
extern "C"  void TweenColorGenerated_ilo_SetEndToCurrentValue5_m2592983184 (Il2CppObject * __this /* static, unused */, TweenColor_t2922258392 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
