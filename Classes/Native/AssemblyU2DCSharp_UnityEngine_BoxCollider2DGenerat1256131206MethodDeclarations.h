﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_BoxCollider2DGenerated
struct UnityEngine_BoxCollider2DGenerated_t1256131206;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_BoxCollider2DGenerated::.ctor()
extern "C"  void UnityEngine_BoxCollider2DGenerated__ctor_m1862286549 (UnityEngine_BoxCollider2DGenerated_t1256131206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoxCollider2DGenerated::BoxCollider2D_BoxCollider2D1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoxCollider2DGenerated_BoxCollider2D_BoxCollider2D1_m552898465 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoxCollider2DGenerated::BoxCollider2D_size(JSVCall)
extern "C"  void UnityEngine_BoxCollider2DGenerated_BoxCollider2D_size_m3736776777 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoxCollider2DGenerated::__Register()
extern "C"  void UnityEngine_BoxCollider2DGenerated___Register_m618593554 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoxCollider2DGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_BoxCollider2DGenerated_ilo_attachFinalizerObject1_m1807819946 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
