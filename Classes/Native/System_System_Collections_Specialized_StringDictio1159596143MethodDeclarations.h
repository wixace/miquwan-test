﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Specialized.StringDictionary
struct StringDictionary_t1159596143;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void System.Collections.Specialized.StringDictionary::.ctor()
extern "C"  void StringDictionary__ctor_m3641162214 (StringDictionary_t1159596143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Specialized.StringDictionary::get_Count()
extern "C"  int32_t StringDictionary_get_Count_m3077213978 (StringDictionary_t1159596143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Specialized.StringDictionary::get_Item(System.String)
extern "C"  String_t* StringDictionary_get_Item_m1026927915 (StringDictionary_t1159596143 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.StringDictionary::set_Item(System.String,System.String)
extern "C"  void StringDictionary_set_Item_m402599440 (StringDictionary_t1159596143 * __this, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.StringDictionary::Add(System.String,System.String)
extern "C"  void StringDictionary_Add_m2876298361 (StringDictionary_t1159596143 * __this, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Specialized.StringDictionary::GetEnumerator()
extern "C"  Il2CppObject * StringDictionary_GetEnumerator_m3815340904 (StringDictionary_t1159596143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.StringDictionary::Remove(System.String)
extern "C"  void StringDictionary_Remove_m3963865952 (StringDictionary_t1159596143 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
