﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_hero_wuhun_shengji
struct Float_hero_wuhun_shengji_t3250123242;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// FloatTextUnit
struct FloatTextUnit_t2362298029;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"

// System.Void Float_hero_wuhun_shengji::.ctor()
extern "C"  void Float_hero_wuhun_shengji__ctor_m2636719089 (Float_hero_wuhun_shengji_t3250123242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_hero_wuhun_shengji::FloatID()
extern "C"  int32_t Float_hero_wuhun_shengji_FloatID_m876090365 (Float_hero_wuhun_shengji_t3250123242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_hero_wuhun_shengji::OnAwake(UnityEngine.GameObject)
extern "C"  void Float_hero_wuhun_shengji_OnAwake_m2486863021 (Float_hero_wuhun_shengji_t3250123242 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_hero_wuhun_shengji::OnDestroy()
extern "C"  void Float_hero_wuhun_shengji_OnDestroy_m1794480234 (Float_hero_wuhun_shengji_t3250123242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_hero_wuhun_shengji::Init()
extern "C"  void Float_hero_wuhun_shengji_Init_m3915917379 (Float_hero_wuhun_shengji_t3250123242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_hero_wuhun_shengji::SetFile(System.Object[])
extern "C"  void Float_hero_wuhun_shengji_SetFile_m4126391557 (Float_hero_wuhun_shengji_t3250123242 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_hero_wuhun_shengji::ilo_OnAwake1(FloatTextUnit,UnityEngine.GameObject)
extern "C"  void Float_hero_wuhun_shengji_ilo_OnAwake1_m3515798442 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, GameObject_t3674682005 * ___viewGO1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_hero_wuhun_shengji::ilo_Play2(FloatTextUnit)
extern "C"  void Float_hero_wuhun_shengji_ilo_Play2_m2731428459 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
