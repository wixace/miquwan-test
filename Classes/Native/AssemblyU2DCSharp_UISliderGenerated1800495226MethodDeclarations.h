﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UISliderGenerated
struct UISliderGenerated_t1800495226;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UISliderGenerated::.ctor()
extern "C"  void UISliderGenerated__ctor_m1292777009 (UISliderGenerated_t1800495226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISliderGenerated::UISlider_UISlider1(JSVCall,System.Int32)
extern "C"  bool UISliderGenerated_UISlider_UISlider1_m1602850497 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISliderGenerated::__Register()
extern "C"  void UISliderGenerated___Register_m2325904694 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISliderGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UISliderGenerated_ilo_attachFinalizerObject1_m116340454 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
