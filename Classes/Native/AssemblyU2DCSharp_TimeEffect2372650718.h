﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_IEffComponent3802264961.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimeEffect
struct  TimeEffect_t2372650718  : public IEffComponent_t3802264961
{
public:
	// System.Single TimeEffect::curtime
	float ___curtime_1;
	// System.Single TimeEffect::<lasttime>k__BackingField
	float ___U3ClasttimeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_curtime_1() { return static_cast<int32_t>(offsetof(TimeEffect_t2372650718, ___curtime_1)); }
	inline float get_curtime_1() const { return ___curtime_1; }
	inline float* get_address_of_curtime_1() { return &___curtime_1; }
	inline void set_curtime_1(float value)
	{
		___curtime_1 = value;
	}

	inline static int32_t get_offset_of_U3ClasttimeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TimeEffect_t2372650718, ___U3ClasttimeU3Ek__BackingField_2)); }
	inline float get_U3ClasttimeU3Ek__BackingField_2() const { return ___U3ClasttimeU3Ek__BackingField_2; }
	inline float* get_address_of_U3ClasttimeU3Ek__BackingField_2() { return &___U3ClasttimeU3Ek__BackingField_2; }
	inline void set_U3ClasttimeU3Ek__BackingField_2(float value)
	{
		___U3ClasttimeU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
