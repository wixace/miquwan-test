﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Xml_XmlNamedNodeMapGenerated
struct System_Xml_XmlNamedNodeMapGenerated_t3123564431;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Xml_XmlNamedNodeMapGenerated::.ctor()
extern "C"  void System_Xml_XmlNamedNodeMapGenerated__ctor_m3349972156 (System_Xml_XmlNamedNodeMapGenerated_t3123564431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNamedNodeMapGenerated::XmlNamedNodeMap_Count(JSVCall)
extern "C"  void System_Xml_XmlNamedNodeMapGenerated_XmlNamedNodeMap_Count_m68212581 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNamedNodeMapGenerated::XmlNamedNodeMap_GetEnumerator(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNamedNodeMapGenerated_XmlNamedNodeMap_GetEnumerator_m221769393 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNamedNodeMapGenerated::XmlNamedNodeMap_GetNamedItem__String__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNamedNodeMapGenerated_XmlNamedNodeMap_GetNamedItem__String__String_m1168697155 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNamedNodeMapGenerated::XmlNamedNodeMap_GetNamedItem__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNamedNodeMapGenerated_XmlNamedNodeMap_GetNamedItem__String_m4030937842 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNamedNodeMapGenerated::XmlNamedNodeMap_Item__Int32(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNamedNodeMapGenerated_XmlNamedNodeMap_Item__Int32_m1329191378 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNamedNodeMapGenerated::XmlNamedNodeMap_RemoveNamedItem__String__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNamedNodeMapGenerated_XmlNamedNodeMap_RemoveNamedItem__String__String_m2679618145 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNamedNodeMapGenerated::XmlNamedNodeMap_RemoveNamedItem__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNamedNodeMapGenerated_XmlNamedNodeMap_RemoveNamedItem__String_m363727888 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNamedNodeMapGenerated::XmlNamedNodeMap_SetNamedItem__XmlNode(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNamedNodeMapGenerated_XmlNamedNodeMap_SetNamedItem__XmlNode_m742692230 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNamedNodeMapGenerated::__Register()
extern "C"  void System_Xml_XmlNamedNodeMapGenerated___Register_m167153035 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Xml_XmlNamedNodeMapGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_Xml_XmlNamedNodeMapGenerated_ilo_setObject1_m3703714162 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System_Xml_XmlNamedNodeMapGenerated::ilo_getStringS2(System.Int32)
extern "C"  String_t* System_Xml_XmlNamedNodeMapGenerated_ilo_getStringS2_m4084856993 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
