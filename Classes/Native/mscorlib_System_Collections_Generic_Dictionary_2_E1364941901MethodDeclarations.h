﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3365755258MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,ISound>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2149178973(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1364941901 *, Dictionary_2_t47618509 *, const MethodInfo*))Enumerator__ctor_m1787693479_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,ISound>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1586990638(__this, method) ((  Il2CppObject * (*) (Enumerator_t1364941901 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3676151450_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,ISound>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1483264568(__this, method) ((  void (*) (Enumerator_t1364941901 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3540839598_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,ISound>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2987163119(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1364941901 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4033043703_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,ISound>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3575836810(__this, method) ((  Il2CppObject * (*) (Enumerator_t1364941901 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m167641078_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,ISound>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1453773980(__this, method) ((  Il2CppObject * (*) (Enumerator_t1364941901 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3237722376_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,ISound>::MoveNext()
#define Enumerator_MoveNext_m2089255377(__this, method) ((  bool (*) (Enumerator_t1364941901 *, const MethodInfo*))Enumerator_MoveNext_m2997434906_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,ISound>::get_Current()
#define Enumerator_get_Current_m2382918908(__this, method) ((  KeyValuePair_2_t4241366511  (*) (Enumerator_t1364941901 *, const MethodInfo*))Enumerator_get_Current_m1198975062_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,ISound>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3750462577(__this, method) ((  int32_t (*) (Enumerator_t1364941901 *, const MethodInfo*))Enumerator_get_CurrentKey_m3083891431_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,ISound>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2044282993(__this, method) ((  ISound_t2170003014 * (*) (Enumerator_t1364941901 *, const MethodInfo*))Enumerator_get_CurrentValue_m1429984843_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,ISound>::Reset()
#define Enumerator_Reset_m2905619759(__this, method) ((  void (*) (Enumerator_t1364941901 *, const MethodInfo*))Enumerator_Reset_m153295225_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,ISound>::VerifyState()
#define Enumerator_VerifyState_m3450252792(__this, method) ((  void (*) (Enumerator_t1364941901 *, const MethodInfo*))Enumerator_VerifyState_m294759106_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,ISound>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2723052384(__this, method) ((  void (*) (Enumerator_t1364941901 *, const MethodInfo*))Enumerator_VerifyCurrent_m2540531114_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,ISound>::Dispose()
#define Enumerator_Dispose_m3095275071(__this, method) ((  void (*) (Enumerator_t1364941901 *, const MethodInfo*))Enumerator_Dispose_m3811252233_gshared)(__this, method)
