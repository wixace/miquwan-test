﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.OcclusionArea
struct OcclusionArea_t3926157360;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine.OcclusionArea::.ctor()
extern "C"  void OcclusionArea__ctor_m736370527 (OcclusionArea_t3926157360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.OcclusionArea::get_center()
extern "C"  Vector3_t4282066566  OcclusionArea_get_center_m1702512905 (OcclusionArea_t3926157360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.OcclusionArea::set_center(UnityEngine.Vector3)
extern "C"  void OcclusionArea_set_center_m875843906 (OcclusionArea_t3926157360 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.OcclusionArea::INTERNAL_get_center(UnityEngine.Vector3&)
extern "C"  void OcclusionArea_INTERNAL_get_center_m1952895568 (OcclusionArea_t3926157360 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.OcclusionArea::INTERNAL_set_center(UnityEngine.Vector3&)
extern "C"  void OcclusionArea_INTERNAL_set_center_m3601453252 (OcclusionArea_t3926157360 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.OcclusionArea::get_size()
extern "C"  Vector3_t4282066566  OcclusionArea_get_size_m3310797429 (OcclusionArea_t3926157360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.OcclusionArea::set_size(UnityEngine.Vector3)
extern "C"  void OcclusionArea_set_size_m3366110422 (OcclusionArea_t3926157360 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.OcclusionArea::INTERNAL_get_size(UnityEngine.Vector3&)
extern "C"  void OcclusionArea_INTERNAL_get_size_m381418172 (OcclusionArea_t3926157360 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.OcclusionArea::INTERNAL_set_size(UnityEngine.Vector3&)
extern "C"  void OcclusionArea_INTERNAL_set_size_m517211696 (OcclusionArea_t3926157360 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
