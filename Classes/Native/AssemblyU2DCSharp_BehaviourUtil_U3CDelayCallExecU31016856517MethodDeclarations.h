﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Object,System.Object,System.Object>
struct U3CDelayCallExecU3Ec__Iterator41_3_t1016856517;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CDelayCallExecU3Ec__Iterator41_3__ctor_m3831342396_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t1016856517 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3__ctor_m3831342396(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator41_3_t1016856517 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3__ctor_m3831342396_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Object,System.Object,System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m733994646_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t1016856517 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m733994646(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator41_3_t1016856517 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m733994646_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Object,System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_IEnumerator_get_Current_m2990394922_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t1016856517 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_IEnumerator_get_Current_m2990394922(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator41_3_t1016856517 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_IEnumerator_get_Current_m2990394922_gshared)(__this, method)
// System.Boolean BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Object,System.Object,System.Object>::MoveNext()
extern "C"  bool U3CDelayCallExecU3Ec__Iterator41_3_MoveNext_m1863240504_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t1016856517 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_MoveNext_m1863240504(__this, method) ((  bool (*) (U3CDelayCallExecU3Ec__Iterator41_3_t1016856517 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_MoveNext_m1863240504_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Object,System.Object,System.Object>::Dispose()
extern "C"  void U3CDelayCallExecU3Ec__Iterator41_3_Dispose_m1031352953_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t1016856517 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_Dispose_m1031352953(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator41_3_t1016856517 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_Dispose_m1031352953_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Object,System.Object,System.Object>::Reset()
extern "C"  void U3CDelayCallExecU3Ec__Iterator41_3_Reset_m1477775337_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t1016856517 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_Reset_m1477775337(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator41_3_t1016856517 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_Reset_m1477775337_gshared)(__this, method)
