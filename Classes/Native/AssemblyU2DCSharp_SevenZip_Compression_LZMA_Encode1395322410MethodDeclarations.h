﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.Compression.LZMA.Encoder/LiteralEncoder
struct LiteralEncoder_t1395322410;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Encode2578924795.h"

// System.Void SevenZip.Compression.LZMA.Encoder/LiteralEncoder::.ctor()
extern "C"  void LiteralEncoder__ctor_m2850301873 (LiteralEncoder_t1395322410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder/LiteralEncoder::Create(System.Int32,System.Int32)
extern "C"  void LiteralEncoder_Create_m223028503 (LiteralEncoder_t1395322410 * __this, int32_t ___numPosBits0, int32_t ___numPrevBits1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder/LiteralEncoder::Init()
extern "C"  void LiteralEncoder_Init_m459123843 (LiteralEncoder_t1395322410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SevenZip.Compression.LZMA.Encoder/LiteralEncoder/Encoder2 SevenZip.Compression.LZMA.Encoder/LiteralEncoder::GetSubCoder(System.UInt32,System.Byte)
extern "C"  Encoder2_t2578924795  LiteralEncoder_GetSubCoder_m1375043661 (LiteralEncoder_t1395322410 * __this, uint32_t ___pos0, uint8_t ___prevByte1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
