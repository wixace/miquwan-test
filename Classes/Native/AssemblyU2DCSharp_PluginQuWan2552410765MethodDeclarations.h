﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginQuWan
struct PluginQuWan_t2552410765;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// Mihua.SDK.EnterGameInfo
struct EnterGameInfo_t1897532954;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Mihua.SDK.LvUpInfo
struct LvUpInfo_t411687177;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_PluginQuWan2552410765.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void PluginQuWan::.ctor()
extern "C"  void PluginQuWan__ctor_m1583634302 (PluginQuWan_t2552410765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuWan::Init()
extern "C"  void PluginQuWan_Init_m3881946902 (PluginQuWan_t2552410765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginQuWan::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginQuWan_ReqSDKHttpLogin_m432705391 (PluginQuWan_t2552410765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginQuWan::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginQuWan_IsLoginSuccess_m1199985197 (PluginQuWan_t2552410765 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuWan::OpenUserLogin()
extern "C"  void PluginQuWan_OpenUserLogin_m797252560 (PluginQuWan_t2552410765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuWan::UserPay(CEvent.ZEvent)
extern "C"  void PluginQuWan_UserPay_m1022266786 (PluginQuWan_t2552410765 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuWan::CreateRole(CEvent.ZEvent)
extern "C"  void PluginQuWan_CreateRole_m871862819 (PluginQuWan_t2552410765 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuWan::EnterGame(CEvent.ZEvent)
extern "C"  void PluginQuWan_EnterGame_m1067417269 (PluginQuWan_t2552410765 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuWan::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginQuWan_RoleUpgrade_m3921242137 (PluginQuWan_t2552410765 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuWan::OnLoginSuccess(System.String)
extern "C"  void PluginQuWan_OnLoginSuccess_m3347086339 (PluginQuWan_t2552410765 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuWan::OnLogoutSuccess(System.String)
extern "C"  void PluginQuWan_OnLogoutSuccess_m684004076 (PluginQuWan_t2552410765 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuWan::OnLogout(System.String)
extern "C"  void PluginQuWan_OnLogout_m3364388307 (PluginQuWan_t2552410765 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuWan::initSdk(System.String,System.String,System.String,System.String)
extern "C"  void PluginQuWan_initSdk_m1962245872 (PluginQuWan_t2552410765 * __this, String_t* ___gameId0, String_t* ___key1, String_t* ___gameName2, String_t* ___scheme3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuWan::login()
extern "C"  void PluginQuWan_login_m1105649125 (PluginQuWan_t2552410765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuWan::logout()
extern "C"  void PluginQuWan_logout_m4216174512 (PluginQuWan_t2552410765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuWan::creatRole(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginQuWan_creatRole_m2211867827 (PluginQuWan_t2552410765 * __this, String_t* ___serverId0, String_t* ___serverName1, String_t* ___roleId2, String_t* ___roleName3, String_t* ___roleLv4, String_t* ___state5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuWan::roleUpgrade(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginQuWan_roleUpgrade_m2041464236 (PluginQuWan_t2552410765 * __this, String_t* ___serverId0, String_t* ___serverName1, String_t* ___roleId2, String_t* ___roleName3, String_t* ___roleLv4, String_t* ___state5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuWan::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginQuWan_pay_m1762847430 (PluginQuWan_t2552410765 * __this, String_t* ___serverId0, String_t* ___roleId1, String_t* ___orderId2, String_t* ___productID3, String_t* ___productName4, String_t* ___amount5, String_t* ___extra6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuWan::<OnLogoutSuccess>m__44E()
extern "C"  void PluginQuWan_U3COnLogoutSuccessU3Em__44E_m4002961396 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuWan::<OnLogout>m__44F()
extern "C"  void PluginQuWan_U3COnLogoutU3Em__44F_m3540202744 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuWan::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginQuWan_ilo_AddEventListener1_m1283498422 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuWan::ilo_initSdk2(PluginQuWan,System.String,System.String,System.String,System.String)
extern "C"  void PluginQuWan_ilo_initSdk2_m1827771796 (Il2CppObject * __this /* static, unused */, PluginQuWan_t2552410765 * ____this0, String_t* ___gameId1, String_t* ___key2, String_t* ___gameName3, String_t* ___scheme4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginQuWan::ilo_get_isSdkLogin3(VersionMgr)
extern "C"  bool PluginQuWan_ilo_get_isSdkLogin3_m2084235758 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginQuWan::ilo_get_PluginsSdkMgr4()
extern "C"  PluginsSdkMgr_t3884624670 * PluginQuWan_ilo_get_PluginsSdkMgr4_m1284860451 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginQuWan::ilo_get_DeviceID5(PluginsSdkMgr)
extern "C"  String_t* PluginQuWan_ilo_get_DeviceID5_m3856817677 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginQuWan::ilo_get_Instance6()
extern "C"  VersionMgr_t1322950208 * PluginQuWan_ilo_get_Instance6_m1700398430 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuWan::ilo_login7(PluginQuWan)
extern "C"  void PluginQuWan_ilo_login7_m864229598 (Il2CppObject * __this /* static, unused */, PluginQuWan_t2552410765 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginQuWan::ilo_GetProductID8(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginQuWan_ilo_GetProductID8_m1107989780 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.EnterGameInfo PluginQuWan::ilo_Parse9(System.Object[])
extern "C"  EnterGameInfo_t1897532954 * PluginQuWan_ilo_Parse9_m1848260792 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.LvUpInfo PluginQuWan::ilo_Parse10(System.Object[])
extern "C"  LvUpInfo_t411687177 * PluginQuWan_ilo_Parse10_m2581400855 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuWan::ilo_ReqSDKHttpLogin11(PluginsSdkMgr)
extern "C"  void PluginQuWan_ilo_ReqSDKHttpLogin11_m1112493958 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuWan::ilo_Log12(System.Object,System.Boolean)
extern "C"  void PluginQuWan_ilo_Log12_m1244963607 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuWan::ilo_logout13(PluginQuWan)
extern "C"  void PluginQuWan_ilo_logout13_m2579259488 (Il2CppObject * __this /* static, unused */, PluginQuWan_t2552410765 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
