﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TwistRecognizer
struct TwistRecognizer_t3400822795;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// TwistGesture
struct TwistGesture_t4198361154;
// FingerGestures/Finger
struct Finger_t182428197;
// FingerGestures/IFingerList
struct IFingerList_t1026994100;
// Gesture
struct Gesture_t1589572905;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GestureResetMode2954327145.h"
#include "AssemblyU2DCSharp_TwistGesture4198361154.h"
#include "AssemblyU2DCSharp_FingerGestures_Finger182428197.h"
#include "AssemblyU2DCSharp_GestureRecognitionState3604239971.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_TwistRecognizer3400822795.h"
#include "AssemblyU2DCSharp_Gesture1589572905.h"

// System.Void TwistRecognizer::.ctor()
extern "C"  void TwistRecognizer__ctor_m1922642880 (TwistRecognizer_t3400822795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TwistRecognizer::GetDefaultEventMessageName()
extern "C"  String_t* TwistRecognizer_GetDefaultEventMessageName_m1236707236 (TwistRecognizer_t3400822795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TwistRecognizer::get_RequiredFingerCount()
extern "C"  int32_t TwistRecognizer_get_RequiredFingerCount_m4186336330 (TwistRecognizer_t3400822795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwistRecognizer::set_RequiredFingerCount(System.Int32)
extern "C"  void TwistRecognizer_set_RequiredFingerCount_m1172152601 (TwistRecognizer_t3400822795 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TwistRecognizer::get_SupportFingerClustering()
extern "C"  bool TwistRecognizer_get_SupportFingerClustering_m4283497225 (TwistRecognizer_t3400822795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GestureResetMode TwistRecognizer::GetDefaultResetMode()
extern "C"  int32_t TwistRecognizer_GetDefaultResetMode_m1720511385 (TwistRecognizer_t3400822795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject TwistRecognizer::GetDefaultSelectionForSendMessage(TwistGesture)
extern "C"  GameObject_t3674682005 * TwistRecognizer_GetDefaultSelectionForSendMessage_m2727722824 (TwistRecognizer_t3400822795 * __this, TwistGesture_t4198361154 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwistRecognizer::Reset(TwistGesture)
extern "C"  void TwistRecognizer_Reset_m1956673643 (TwistRecognizer_t3400822795 * __this, TwistGesture_t4198361154 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures/Finger TwistRecognizer::GetTwistPivot(FingerGestures/Finger,FingerGestures/Finger)
extern "C"  Finger_t182428197 * TwistRecognizer_GetTwistPivot_m1003647095 (TwistRecognizer_t3400822795 * __this, Finger_t182428197 * ___finger00, Finger_t182428197 * ___finger11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TwistRecognizer::CanBegin(TwistGesture,FingerGestures/IFingerList)
extern "C"  bool TwistRecognizer_CanBegin_m3803459435 (TwistRecognizer_t3400822795 * __this, TwistGesture_t4198361154 * ___gesture0, Il2CppObject * ___touches1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwistRecognizer::OnBegin(TwistGesture,FingerGestures/IFingerList)
extern "C"  void TwistRecognizer_OnBegin_m2474328748 (TwistRecognizer_t3400822795 * __this, TwistGesture_t4198361154 * ___gesture0, Il2CppObject * ___touches1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GestureRecognitionState TwistRecognizer::OnRecognize(TwistGesture,FingerGestures/IFingerList)
extern "C"  int32_t TwistRecognizer_OnRecognize_m120516695 (TwistRecognizer_t3400822795 * __this, TwistGesture_t4198361154 * ___gesture0, Il2CppObject * ___touches1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TwistRecognizer::FingersMovedInOppositeDirections(FingerGestures/Finger,FingerGestures/Finger)
extern "C"  bool TwistRecognizer_FingersMovedInOppositeDirections_m2520523067 (TwistRecognizer_t3400822795 * __this, Finger_t182428197 * ___finger00, Finger_t182428197 * ___finger11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TwistRecognizer::SignedAngularGap(FingerGestures/Finger,FingerGestures/Finger,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  float TwistRecognizer_SignedAngularGap_m2499737396 (Il2CppObject * __this /* static, unused */, Finger_t182428197 * ___finger00, Finger_t182428197 * ___finger11, Vector2_t4282066565  ___refPos02, Vector2_t4282066565  ___refPos13, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TwistRecognizer::ilo_get_IsMoving1(FingerGestures/Finger)
extern "C"  bool TwistRecognizer_ilo_get_IsMoving1_m2694940574 (Il2CppObject * __this /* static, unused */, Finger_t182428197 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures/Finger TwistRecognizer::ilo_get_Item2(FingerGestures/IFingerList,System.Int32)
extern "C"  Finger_t182428197 * TwistRecognizer_ilo_get_Item2_m3391677820 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures/Finger TwistRecognizer::ilo_GetTwistPivot3(TwistRecognizer,FingerGestures/Finger,FingerGestures/Finger)
extern "C"  Finger_t182428197 * TwistRecognizer_ilo_GetTwistPivot3_m4132909962 (Il2CppObject * __this /* static, unused */, TwistRecognizer_t3400822795 * ____this0, Finger_t182428197 * ___finger01, Finger_t182428197 * ___finger12, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TwistRecognizer::ilo_AllFingersMoving4(FingerGestures/Finger,FingerGestures/Finger)
extern "C"  bool TwistRecognizer_ilo_AllFingersMoving4_m1975401208 (Il2CppObject * __this /* static, unused */, Finger_t182428197 * ___finger00, Finger_t182428197 * ___finger11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TwistRecognizer::ilo_FingersMovedInOppositeDirections5(TwistRecognizer,FingerGestures/Finger,FingerGestures/Finger)
extern "C"  bool TwistRecognizer_ilo_FingersMovedInOppositeDirections5_m2811096970 (Il2CppObject * __this /* static, unused */, TwistRecognizer_t3400822795 * ____this0, Finger_t182428197 * ___finger01, Finger_t182428197 * ___finger12, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TwistRecognizer::ilo_get_StartPosition6(FingerGestures/Finger)
extern "C"  Vector2_t4282066565  TwistRecognizer_ilo_get_StartPosition6_m3962419645 (Il2CppObject * __this /* static, unused */, Finger_t182428197 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TwistRecognizer::ilo_SignedAngularGap7(FingerGestures/Finger,FingerGestures/Finger,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  float TwistRecognizer_ilo_SignedAngularGap7_m1095391422 (Il2CppObject * __this /* static, unused */, Finger_t182428197 * ___finger00, Finger_t182428197 * ___finger11, Vector2_t4282066565  ___refPos02, Vector2_t4282066565  ___refPos13, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwistRecognizer::ilo_set_Pivot8(TwistGesture,FingerGestures/Finger)
extern "C"  void TwistRecognizer_ilo_set_Pivot8_m2834064463 (Il2CppObject * __this /* static, unused */, TwistGesture_t4198361154 * ____this0, Finger_t182428197 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures/Finger TwistRecognizer::ilo_get_Pivot9(TwistGesture)
extern "C"  Finger_t182428197 * TwistRecognizer_ilo_get_Pivot9_m1818987047 (Il2CppObject * __this /* static, unused */, TwistGesture_t4198361154 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwistRecognizer::ilo_set_StartPosition10(Gesture,UnityEngine.Vector2)
extern "C"  void TwistRecognizer_ilo_set_StartPosition10_m3806292773 (Il2CppObject * __this /* static, unused */, Gesture_t1589572905 * ____this0, Vector2_t4282066565  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TwistRecognizer::ilo_get_StartPosition11(Gesture)
extern "C"  Vector2_t4282066565  TwistRecognizer_ilo_get_StartPosition11_m113301729 (Il2CppObject * __this /* static, unused */, Gesture_t1589572905 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwistRecognizer::ilo_set_Position12(Gesture,UnityEngine.Vector2)
extern "C"  void TwistRecognizer_ilo_set_Position12_m2003819743 (Il2CppObject * __this /* static, unused */, Gesture_t1589572905 * ____this0, Vector2_t4282066565  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TwistRecognizer::ilo_get_Count13(FingerGestures/IFingerList)
extern "C"  int32_t TwistRecognizer_ilo_get_Count13_m3369431597 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwistRecognizer::ilo_set_DeltaRotation14(TwistGesture,System.Single)
extern "C"  void TwistRecognizer_ilo_set_DeltaRotation14_m1793428362 (Il2CppObject * __this /* static, unused */, TwistGesture_t4198361154 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TwistRecognizer::ilo_get_DeltaRotation15(TwistGesture)
extern "C"  float TwistRecognizer_ilo_get_DeltaRotation15_m2379184998 (Il2CppObject * __this /* static, unused */, TwistGesture_t4198361154 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TwistRecognizer::ilo_get_Position16(FingerGestures/Finger)
extern "C"  Vector2_t4282066565  TwistRecognizer_ilo_get_Position16_m117218618 (Il2CppObject * __this /* static, unused */, Finger_t182428197 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TwistRecognizer::ilo_SignedAngle17(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  float TwistRecognizer_ilo_SignedAngle17_m310775770 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___from0, Vector2_t4282066565  ___to1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
