﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>
struct Collection_1_t2200516419;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Pathfinding.IntRect[]
struct IntRectU5BU5D_t3425567672;
// System.Collections.Generic.IEnumerator`1<Pathfinding.IntRect>
struct IEnumerator_1_t631956014;
// System.Collections.Generic.IList`1<Pathfinding.IntRect>
struct IList_1_t1414738168;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_IntRect3015058261.h"

// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::.ctor()
extern "C"  void Collection_1__ctor_m3827740556_gshared (Collection_1_t2200516419 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3827740556(__this, method) ((  void (*) (Collection_1_t2200516419 *, const MethodInfo*))Collection_1__ctor_m3827740556_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3872902511_gshared (Collection_1_t2200516419 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3872902511(__this, method) ((  bool (*) (Collection_1_t2200516419 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3872902511_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m4200461112_gshared (Collection_1_t2200516419 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m4200461112(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2200516419 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m4200461112_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1490238003_gshared (Collection_1_t2200516419 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1490238003(__this, method) ((  Il2CppObject * (*) (Collection_1_t2200516419 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1490238003_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m3821526942_gshared (Collection_1_t2200516419 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m3821526942(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2200516419 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m3821526942_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m825175790_gshared (Collection_1_t2200516419 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m825175790(__this, ___value0, method) ((  bool (*) (Collection_1_t2200516419 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m825175790_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m2796754678_gshared (Collection_1_t2200516419 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m2796754678(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2200516419 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m2796754678_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m42724897_gshared (Collection_1_t2200516419 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m42724897(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2200516419 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m42724897_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m2319079783_gshared (Collection_1_t2200516419 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m2319079783(__this, ___value0, method) ((  void (*) (Collection_1_t2200516419 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2319079783_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m2709100974_gshared (Collection_1_t2200516419 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m2709100974(__this, method) ((  bool (*) (Collection_1_t2200516419 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m2709100974_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m4289753114_gshared (Collection_1_t2200516419 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m4289753114(__this, method) ((  Il2CppObject * (*) (Collection_1_t2200516419 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m4289753114_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1380460765_gshared (Collection_1_t2200516419 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1380460765(__this, method) ((  bool (*) (Collection_1_t2200516419 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1380460765_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m2992391804_gshared (Collection_1_t2200516419 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m2992391804(__this, method) ((  bool (*) (Collection_1_t2200516419 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m2992391804_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m349750049_gshared (Collection_1_t2200516419 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m349750049(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t2200516419 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m349750049_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m3017872824_gshared (Collection_1_t2200516419 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m3017872824(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2200516419 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m3017872824_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::Add(T)
extern "C"  void Collection_1_Add_m1221996403_gshared (Collection_1_t2200516419 * __this, IntRect_t3015058261  ___item0, const MethodInfo* method);
#define Collection_1_Add_m1221996403(__this, ___item0, method) ((  void (*) (Collection_1_t2200516419 *, IntRect_t3015058261 , const MethodInfo*))Collection_1_Add_m1221996403_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::Clear()
extern "C"  void Collection_1_Clear_m1233873847_gshared (Collection_1_t2200516419 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1233873847(__this, method) ((  void (*) (Collection_1_t2200516419 *, const MethodInfo*))Collection_1_Clear_m1233873847_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::ClearItems()
extern "C"  void Collection_1_ClearItems_m462548907_gshared (Collection_1_t2200516419 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m462548907(__this, method) ((  void (*) (Collection_1_t2200516419 *, const MethodInfo*))Collection_1_ClearItems_m462548907_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::Contains(T)
extern "C"  bool Collection_1_Contains_m573978981_gshared (Collection_1_t2200516419 * __this, IntRect_t3015058261  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m573978981(__this, ___item0, method) ((  bool (*) (Collection_1_t2200516419 *, IntRect_t3015058261 , const MethodInfo*))Collection_1_Contains_m573978981_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m2012758179_gshared (Collection_1_t2200516419 * __this, IntRectU5BU5D_t3425567672* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m2012758179(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2200516419 *, IntRectU5BU5D_t3425567672*, int32_t, const MethodInfo*))Collection_1_CopyTo_m2012758179_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m3906654536_gshared (Collection_1_t2200516419 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m3906654536(__this, method) ((  Il2CppObject* (*) (Collection_1_t2200516419 *, const MethodInfo*))Collection_1_GetEnumerator_m3906654536_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m2204933991_gshared (Collection_1_t2200516419 * __this, IntRect_t3015058261  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m2204933991(__this, ___item0, method) ((  int32_t (*) (Collection_1_t2200516419 *, IntRect_t3015058261 , const MethodInfo*))Collection_1_IndexOf_m2204933991_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m2263098330_gshared (Collection_1_t2200516419 * __this, int32_t ___index0, IntRect_t3015058261  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m2263098330(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2200516419 *, int32_t, IntRect_t3015058261 , const MethodInfo*))Collection_1_Insert_m2263098330_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m3486635277_gshared (Collection_1_t2200516419 * __this, int32_t ___index0, IntRect_t3015058261  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m3486635277(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2200516419 *, int32_t, IntRect_t3015058261 , const MethodInfo*))Collection_1_InsertItem_m3486635277_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m2718092491_gshared (Collection_1_t2200516419 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m2718092491(__this, method) ((  Il2CppObject* (*) (Collection_1_t2200516419 *, const MethodInfo*))Collection_1_get_Items_m2718092491_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::Remove(T)
extern "C"  bool Collection_1_Remove_m1912868512_gshared (Collection_1_t2200516419 * __this, IntRect_t3015058261  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m1912868512(__this, ___item0, method) ((  bool (*) (Collection_1_t2200516419 *, IntRect_t3015058261 , const MethodInfo*))Collection_1_Remove_m1912868512_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m136951200_gshared (Collection_1_t2200516419 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m136951200(__this, ___index0, method) ((  void (*) (Collection_1_t2200516419 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m136951200_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m1365742016_gshared (Collection_1_t2200516419 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m1365742016(__this, ___index0, method) ((  void (*) (Collection_1_t2200516419 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1365742016_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3745751796_gshared (Collection_1_t2200516419 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3745751796(__this, method) ((  int32_t (*) (Collection_1_t2200516419 *, const MethodInfo*))Collection_1_get_Count_m3745751796_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::get_Item(System.Int32)
extern "C"  IntRect_t3015058261  Collection_1_get_Item_m2715066660_gshared (Collection_1_t2200516419 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m2715066660(__this, ___index0, method) ((  IntRect_t3015058261  (*) (Collection_1_t2200516419 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2715066660_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3393639985_gshared (Collection_1_t2200516419 * __this, int32_t ___index0, IntRect_t3015058261  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3393639985(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2200516419 *, int32_t, IntRect_t3015058261 , const MethodInfo*))Collection_1_set_Item_m3393639985_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2753161256_gshared (Collection_1_t2200516419 * __this, int32_t ___index0, IntRect_t3015058261  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m2753161256(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2200516419 *, int32_t, IntRect_t3015058261 , const MethodInfo*))Collection_1_SetItem_m2753161256_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m2537398823_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m2537398823(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m2537398823_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::ConvertItem(System.Object)
extern "C"  IntRect_t3015058261  Collection_1_ConvertItem_m3385925123_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m3385925123(__this /* static, unused */, ___item0, method) ((  IntRect_t3015058261  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m3385925123_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m915589091_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m915589091(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m915589091_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m1212940349_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1212940349(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m1212940349_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.IntRect>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m2830858114_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m2830858114(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m2830858114_gshared)(__this /* static, unused */, ___list0, method)
