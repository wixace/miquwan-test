﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._cf96d94acacf88229fd4cd84e2652923
struct _cf96d94acacf88229fd4cd84e2652923_t2618695050;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._cf96d94acacf88229fd4cd84e2652923::.ctor()
extern "C"  void _cf96d94acacf88229fd4cd84e2652923__ctor_m1507107811 (_cf96d94acacf88229fd4cd84e2652923_t2618695050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._cf96d94acacf88229fd4cd84e2652923::_cf96d94acacf88229fd4cd84e2652923m2(System.Int32)
extern "C"  int32_t _cf96d94acacf88229fd4cd84e2652923__cf96d94acacf88229fd4cd84e2652923m2_m2058802521 (_cf96d94acacf88229fd4cd84e2652923_t2618695050 * __this, int32_t ____cf96d94acacf88229fd4cd84e2652923a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._cf96d94acacf88229fd4cd84e2652923::_cf96d94acacf88229fd4cd84e2652923m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _cf96d94acacf88229fd4cd84e2652923__cf96d94acacf88229fd4cd84e2652923m_m2562558333 (_cf96d94acacf88229fd4cd84e2652923_t2618695050 * __this, int32_t ____cf96d94acacf88229fd4cd84e2652923a0, int32_t ____cf96d94acacf88229fd4cd84e2652923181, int32_t ____cf96d94acacf88229fd4cd84e2652923c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
