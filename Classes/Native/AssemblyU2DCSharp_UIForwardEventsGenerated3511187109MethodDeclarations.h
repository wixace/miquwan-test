﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIForwardEventsGenerated
struct UIForwardEventsGenerated_t3511187109;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UIForwardEventsGenerated::.ctor()
extern "C"  void UIForwardEventsGenerated__ctor_m4000563606 (UIForwardEventsGenerated_t3511187109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIForwardEventsGenerated::UIForwardEvents_UIForwardEvents1(JSVCall,System.Int32)
extern "C"  bool UIForwardEventsGenerated_UIForwardEvents_UIForwardEvents1_m2766698894 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIForwardEventsGenerated::UIForwardEvents_target(JSVCall)
extern "C"  void UIForwardEventsGenerated_UIForwardEvents_target_m2517589939 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIForwardEventsGenerated::UIForwardEvents_onHover(JSVCall)
extern "C"  void UIForwardEventsGenerated_UIForwardEvents_onHover_m1216959147 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIForwardEventsGenerated::UIForwardEvents_onPress(JSVCall)
extern "C"  void UIForwardEventsGenerated_UIForwardEvents_onPress_m3103780740 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIForwardEventsGenerated::UIForwardEvents_onClick(JSVCall)
extern "C"  void UIForwardEventsGenerated_UIForwardEvents_onClick_m3621415903 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIForwardEventsGenerated::UIForwardEvents_onDoubleClick(JSVCall)
extern "C"  void UIForwardEventsGenerated_UIForwardEvents_onDoubleClick_m3405890896 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIForwardEventsGenerated::UIForwardEvents_onSelect(JSVCall)
extern "C"  void UIForwardEventsGenerated_UIForwardEvents_onSelect_m1975117129 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIForwardEventsGenerated::UIForwardEvents_onDrag(JSVCall)
extern "C"  void UIForwardEventsGenerated_UIForwardEvents_onDrag_m181596113 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIForwardEventsGenerated::UIForwardEvents_onDrop(JSVCall)
extern "C"  void UIForwardEventsGenerated_UIForwardEvents_onDrop_m3320426614 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIForwardEventsGenerated::UIForwardEvents_onSubmit(JSVCall)
extern "C"  void UIForwardEventsGenerated_UIForwardEvents_onSubmit_m4251031981 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIForwardEventsGenerated::UIForwardEvents_onScroll(JSVCall)
extern "C"  void UIForwardEventsGenerated_UIForwardEvents_onScroll_m801091224 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIForwardEventsGenerated::__Register()
extern "C"  void UIForwardEventsGenerated___Register_m3730131185 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIForwardEventsGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UIForwardEventsGenerated_ilo_attachFinalizerObject1_m273452297 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIForwardEventsGenerated::ilo_setBooleanS2(System.Int32,System.Boolean)
extern "C"  void UIForwardEventsGenerated_ilo_setBooleanS2_m1378348838 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIForwardEventsGenerated::ilo_getBooleanS3(System.Int32)
extern "C"  bool UIForwardEventsGenerated_ilo_getBooleanS3_m1108700920 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
