﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AstarPath/<DelayedGraphUpdate>c__IteratorB
struct U3CDelayedGraphUpdateU3Ec__IteratorB_t3781801013;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AstarPath/<DelayedGraphUpdate>c__IteratorB::.ctor()
extern "C"  void U3CDelayedGraphUpdateU3Ec__IteratorB__ctor_m4214910982 (U3CDelayedGraphUpdateU3Ec__IteratorB_t3781801013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AstarPath/<DelayedGraphUpdate>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayedGraphUpdateU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m193098582 (U3CDelayedGraphUpdateU3Ec__IteratorB_t3781801013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AstarPath/<DelayedGraphUpdate>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayedGraphUpdateU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m1430381290 (U3CDelayedGraphUpdateU3Ec__IteratorB_t3781801013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AstarPath/<DelayedGraphUpdate>c__IteratorB::MoveNext()
extern "C"  bool U3CDelayedGraphUpdateU3Ec__IteratorB_MoveNext_m1977769750 (U3CDelayedGraphUpdateU3Ec__IteratorB_t3781801013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath/<DelayedGraphUpdate>c__IteratorB::Dispose()
extern "C"  void U3CDelayedGraphUpdateU3Ec__IteratorB_Dispose_m273576643 (U3CDelayedGraphUpdateU3Ec__IteratorB_t3781801013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath/<DelayedGraphUpdate>c__IteratorB::Reset()
extern "C"  void U3CDelayedGraphUpdateU3Ec__IteratorB_Reset_m1861343923 (U3CDelayedGraphUpdateU3Ec__IteratorB_t3781801013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
