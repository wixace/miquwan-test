﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._e89a2ef985bd96a1929091ce28dacb89
struct _e89a2ef985bd96a1929091ce28dacb89_t761373491;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__e89a2ef985bd96a1929091ce2761373491.h"

// System.Void Little._e89a2ef985bd96a1929091ce28dacb89::.ctor()
extern "C"  void _e89a2ef985bd96a1929091ce28dacb89__ctor_m2032823386 (_e89a2ef985bd96a1929091ce28dacb89_t761373491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e89a2ef985bd96a1929091ce28dacb89::_e89a2ef985bd96a1929091ce28dacb89m2(System.Int32)
extern "C"  int32_t _e89a2ef985bd96a1929091ce28dacb89__e89a2ef985bd96a1929091ce28dacb89m2_m1803693241 (_e89a2ef985bd96a1929091ce28dacb89_t761373491 * __this, int32_t ____e89a2ef985bd96a1929091ce28dacb89a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e89a2ef985bd96a1929091ce28dacb89::_e89a2ef985bd96a1929091ce28dacb89m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _e89a2ef985bd96a1929091ce28dacb89__e89a2ef985bd96a1929091ce28dacb89m_m521223709 (_e89a2ef985bd96a1929091ce28dacb89_t761373491 * __this, int32_t ____e89a2ef985bd96a1929091ce28dacb89a0, int32_t ____e89a2ef985bd96a1929091ce28dacb89171, int32_t ____e89a2ef985bd96a1929091ce28dacb89c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e89a2ef985bd96a1929091ce28dacb89::ilo__e89a2ef985bd96a1929091ce28dacb89m21(Little._e89a2ef985bd96a1929091ce28dacb89,System.Int32)
extern "C"  int32_t _e89a2ef985bd96a1929091ce28dacb89_ilo__e89a2ef985bd96a1929091ce28dacb89m21_m2344732762 (Il2CppObject * __this /* static, unused */, _e89a2ef985bd96a1929091ce28dacb89_t761373491 * ____this0, int32_t ____e89a2ef985bd96a1929091ce28dacb89a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
