﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>
struct ReadOnlyCollection_1_t3931917222;
// System.Collections.Generic.IList`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>
struct IList_1_t774519593;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// UnityEngine.MeshSubsetCombineUtility/SubMeshInstance[]
struct SubMeshInstanceU5BU5D_t2339421603;
// System.Collections.Generic.IEnumerator`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>
struct IEnumerator_1_t4286704735;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_MeshSubsetCombineUtility_S2374839686.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m1228186026_gshared (ReadOnlyCollection_1_t3931917222 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1228186026(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t3931917222 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1228186026_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1799089812_gshared (ReadOnlyCollection_1_t3931917222 * __this, SubMeshInstance_t2374839686  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1799089812(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t3931917222 *, SubMeshInstance_t2374839686 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1799089812_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1943900342_gshared (ReadOnlyCollection_1_t3931917222 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1943900342(__this, method) ((  void (*) (ReadOnlyCollection_1_t3931917222 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1943900342_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m828262523_gshared (ReadOnlyCollection_1_t3931917222 * __this, int32_t ___index0, SubMeshInstance_t2374839686  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m828262523(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t3931917222 *, int32_t, SubMeshInstance_t2374839686 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m828262523_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m390849443_gshared (ReadOnlyCollection_1_t3931917222 * __this, SubMeshInstance_t2374839686  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m390849443(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t3931917222 *, SubMeshInstance_t2374839686 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m390849443_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2997082689_gshared (ReadOnlyCollection_1_t3931917222 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2997082689(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3931917222 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2997082689_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  SubMeshInstance_t2374839686  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3326423207_gshared (ReadOnlyCollection_1_t3931917222 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3326423207(__this, ___index0, method) ((  SubMeshInstance_t2374839686  (*) (ReadOnlyCollection_1_t3931917222 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3326423207_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3200931474_gshared (ReadOnlyCollection_1_t3931917222 * __this, int32_t ___index0, SubMeshInstance_t2374839686  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3200931474(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3931917222 *, int32_t, SubMeshInstance_t2374839686 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3200931474_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1786121356_gshared (ReadOnlyCollection_1_t3931917222 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1786121356(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3931917222 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1786121356_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1399782233_gshared (ReadOnlyCollection_1_t3931917222 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1399782233(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3931917222 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1399782233_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1589215272_gshared (ReadOnlyCollection_1_t3931917222 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1589215272(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3931917222 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1589215272_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m192692885_gshared (ReadOnlyCollection_1_t3931917222 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m192692885(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3931917222 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m192692885_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m4128747111_gshared (ReadOnlyCollection_1_t3931917222 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m4128747111(__this, method) ((  void (*) (ReadOnlyCollection_1_t3931917222 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m4128747111_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2336177739_gshared (ReadOnlyCollection_1_t3931917222 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2336177739(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3931917222 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2336177739_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1191126637_gshared (ReadOnlyCollection_1_t3931917222 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1191126637(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3931917222 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1191126637_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m1716295008_gshared (ReadOnlyCollection_1_t3931917222 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1716295008(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3931917222 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1716295008_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m2564806600_gshared (ReadOnlyCollection_1_t3931917222 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m2564806600(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t3931917222 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2564806600_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3784948976_gshared (ReadOnlyCollection_1_t3931917222 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3784948976(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3931917222 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3784948976_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1415934257_gshared (ReadOnlyCollection_1_t3931917222 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1415934257(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3931917222 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1415934257_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2799361059_gshared (ReadOnlyCollection_1_t3931917222 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2799361059(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3931917222 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2799361059_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3220849146_gshared (ReadOnlyCollection_1_t3931917222 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3220849146(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3931917222 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3220849146_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2913211839_gshared (ReadOnlyCollection_1_t3931917222 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2913211839(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3931917222 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2913211839_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m2602062058_gshared (ReadOnlyCollection_1_t3931917222 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m2602062058(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3931917222 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m2602062058_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m706013495_gshared (ReadOnlyCollection_1_t3931917222 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m706013495(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3931917222 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m706013495_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m2050625512_gshared (ReadOnlyCollection_1_t3931917222 * __this, SubMeshInstance_t2374839686  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m2050625512(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3931917222 *, SubMeshInstance_t2374839686 , const MethodInfo*))ReadOnlyCollection_1_Contains_m2050625512_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m1466219972_gshared (ReadOnlyCollection_1_t3931917222 * __this, SubMeshInstanceU5BU5D_t2339421603* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1466219972(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3931917222 *, SubMeshInstanceU5BU5D_t2339421603*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1466219972_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m1344815807_gshared (ReadOnlyCollection_1_t3931917222 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1344815807(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t3931917222 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1344815807_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m1237280144_gshared (ReadOnlyCollection_1_t3931917222 * __this, SubMeshInstance_t2374839686  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1237280144(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3931917222 *, SubMeshInstance_t2374839686 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1237280144_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m3813253611_gshared (ReadOnlyCollection_1_t3931917222 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m3813253611(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t3931917222 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3813253611_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::get_Item(System.Int32)
extern "C"  SubMeshInstance_t2374839686  ReadOnlyCollection_1_get_Item_m1815779815_gshared (ReadOnlyCollection_1_t3931917222 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1815779815(__this, ___index0, method) ((  SubMeshInstance_t2374839686  (*) (ReadOnlyCollection_1_t3931917222 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1815779815_gshared)(__this, ___index0, method)
