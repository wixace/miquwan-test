﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ReplayBase>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m70017501(__this, ___dictionary0, method) ((  void (*) (Enumerator_t4285630474 *, Dictionary_2_t2968307082 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ReplayBase>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4026910308(__this, method) ((  Il2CppObject * (*) (Enumerator_t4285630474 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ReplayBase>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m98246328(__this, method) ((  void (*) (Enumerator_t4285630474 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ReplayBase>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1160531777(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t4285630474 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ReplayBase>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2711070784(__this, method) ((  Il2CppObject * (*) (Enumerator_t4285630474 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ReplayBase>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3637278418(__this, method) ((  Il2CppObject * (*) (Enumerator_t4285630474 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ReplayBase>>::MoveNext()
#define Enumerator_MoveNext_m397586340(__this, method) ((  bool (*) (Enumerator_t4285630474 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ReplayBase>>::get_Current()
#define Enumerator_get_Current_m3467520012(__this, method) ((  KeyValuePair_2_t2867087788  (*) (Enumerator_t4285630474 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ReplayBase>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2754992241(__this, method) ((  String_t* (*) (Enumerator_t4285630474 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ReplayBase>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3209878741(__this, method) ((  List_1_t2147888712 * (*) (Enumerator_t4285630474 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ReplayBase>>::Reset()
#define Enumerator_Reset_m2683666607(__this, method) ((  void (*) (Enumerator_t4285630474 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ReplayBase>>::VerifyState()
#define Enumerator_VerifyState_m2370523512(__this, method) ((  void (*) (Enumerator_t4285630474 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ReplayBase>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m190332640(__this, method) ((  void (*) (Enumerator_t4285630474 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ReplayBase>>::Dispose()
#define Enumerator_Dispose_m251693503(__this, method) ((  void (*) (Enumerator_t4285630474 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
