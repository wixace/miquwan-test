﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_equip_refine
struct Float_equip_refine_t2432002687;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// FloatTextUnit
struct FloatTextUnit_t2362298029;
// EventDelegate/Callback
struct Callback_t1094463061;
// FloatTextMgr
struct FloatTextMgr_t630384591;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "AssemblyU2DCSharp_UITweener_Method1078127180.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_FloatTextMgr630384591.h"

// System.Void Float_equip_refine::.ctor()
extern "C"  void Float_equip_refine__ctor_m605484284 (Float_equip_refine_t2432002687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_refine::OnAwake(UnityEngine.GameObject)
extern "C"  void Float_equip_refine_OnAwake_m2165875896 (Float_equip_refine_t2432002687 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_refine::OnDestroy()
extern "C"  void Float_equip_refine_OnDestroy_m892201973 (Float_equip_refine_t2432002687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_equip_refine::FloatID()
extern "C"  int32_t Float_equip_refine_FloatID_m2692785032 (Float_equip_refine_t2432002687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_refine::Init()
extern "C"  void Float_equip_refine_Init_m4266035672 (Float_equip_refine_t2432002687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_refine::SetFile(System.Object[])
extern "C"  void Float_equip_refine_SetFile_m310821722 (Float_equip_refine_t2432002687 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_refine::Death()
extern "C"  void Float_equip_refine_Death_m2990546830 (Float_equip_refine_t2432002687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_refine::<SetFile>m__3DF()
extern "C"  void Float_equip_refine_U3CSetFileU3Em__3DF_m974401566 (Float_equip_refine_t2432002687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_refine::ilo_MoveScale1(FloatTextUnit,System.Single,System.Single,System.Single,System.Single,EventDelegate/Callback,UnityEngine.GameObject,UITweener/Method)
extern "C"  void Float_equip_refine_ilo_MoveScale1_m38557639 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, float ___from1, float ___to2, float ___duration3, float ___delay4, Callback_t1094463061 * ___call5, GameObject_t3674682005 * ___go6, int32_t ___method7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_refine::ilo_MovePosition2(FloatTextUnit,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,EventDelegate/Callback,UnityEngine.GameObject,UITweener/Method)
extern "C"  void Float_equip_refine_ilo_MovePosition2_m3797657335 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, Vector3_t4282066566  ___from1, Vector3_t4282066566  ___to2, float ___duration3, float ___delay4, Callback_t1094463061 * ___call5, GameObject_t3674682005 * ___go6, int32_t ___method7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_refine::ilo_Play3(FloatTextUnit)
extern "C"  void Float_equip_refine_ilo_Play3_m2700291743 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTextMgr Float_equip_refine::ilo_get_FloatTextMgr4()
extern "C"  FloatTextMgr_t630384591 * Float_equip_refine_ilo_get_FloatTextMgr4_m276167733 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_refine::ilo_FloatText5(FloatTextMgr,FLOAT_TEXT_ID,System.Int32,System.Object[])
extern "C"  void Float_equip_refine_ilo_FloatText5_m446152777 (Il2CppObject * __this /* static, unused */, FloatTextMgr_t630384591 * ____this0, int32_t ___textId1, int32_t ___offsetY2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_refine::ilo_Death6(FloatTextUnit)
extern "C"  void Float_equip_refine_ilo_Death6_m1237694792 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
