﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066860316MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,JSCMapPathConfig>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3597864265(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3322162674 *, int32_t, JSCMapPathConfig_t3426118729 *, const MethodInfo*))KeyValuePair_2__ctor_m11197230_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,JSCMapPathConfig>::get_Key()
#define KeyValuePair_2_get_Key_m4057513983(__this, method) ((  int32_t (*) (KeyValuePair_2_t3322162674 *, const MethodInfo*))KeyValuePair_2_get_Key_m494458106_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,JSCMapPathConfig>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3866714944(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3322162674 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m4229413435_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,JSCMapPathConfig>::get_Value()
#define KeyValuePair_2_get_Value_m2112292963(__this, method) ((  JSCMapPathConfig_t3426118729 * (*) (KeyValuePair_2_t3322162674 *, const MethodInfo*))KeyValuePair_2_get_Value_m1563175098_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,JSCMapPathConfig>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1827739456(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3322162674 *, JSCMapPathConfig_t3426118729 *, const MethodInfo*))KeyValuePair_2_set_Value_m1296398523_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,JSCMapPathConfig>::ToString()
#define KeyValuePair_2_ToString_m3722195720(__this, method) ((  String_t* (*) (KeyValuePair_2_t3322162674 *, const MethodInfo*))KeyValuePair_2_ToString_m491888647_gshared)(__this, method)
