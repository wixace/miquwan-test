﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// System.String
struct String_t;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// SerializableGuid
struct SerializableGuid_t3998617672;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Uri
struct Uri_t1116831938;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_WriteState4055692778.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Formatting732683613.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType3916897561.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader816925123.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonToken4173078175.h"
#include "mscorlib_System_Decimal1954350631.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_DateTimeOffset3884714306.h"
#include "mscorlib_System_Guid2862754429.h"
#include "AssemblyU2DCSharp_SerializableGuid3998617672.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "mscorlib_System_Nullable_1_gen1237965023.h"
#include "mscorlib_System_Nullable_1_gen108794504.h"
#include "mscorlib_System_Nullable_1_gen1237965118.h"
#include "mscorlib_System_Nullable_1_gen108794599.h"
#include "mscorlib_System_Nullable_1_gen81078199.h"
#include "mscorlib_System_Nullable_1_gen3952353088.h"
#include "mscorlib_System_Nullable_1_gen560925241.h"
#include "mscorlib_System_Nullable_1_gen1237964965.h"
#include "mscorlib_System_Nullable_1_gen108794446.h"
#include "mscorlib_System_Nullable_1_gen2946749061.h"
#include "mscorlib_System_Nullable_1_gen2946736183.h"
#include "mscorlib_System_Nullable_1_gen1245896300.h"
#include "mscorlib_System_Nullable_1_gen2038477154.h"
#include "mscorlib_System_Nullable_1_gen72820554.h"
#include "mscorlib_System_Nullable_1_gen3968840829.h"
#include "mscorlib_System_Nullable_1_gen2946880952.h"
#include "mscorlib_System_Nullable_1_gen497649510.h"
#include "System_System_Uri1116831938.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"

// System.Void Newtonsoft.Json.JsonWriter::.ctor()
extern "C"  void JsonWriter__ctor_m2022574027 (JsonWriter_t972330355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::.cctor()
extern "C"  void JsonWriter__cctor_m2088156482 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::System.IDisposable.Dispose()
extern "C"  void JsonWriter_System_IDisposable_Dispose_m195110068 (JsonWriter_t972330355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonWriter::get_CloseOutput()
extern "C"  bool JsonWriter_get_CloseOutput_m2512288357 (JsonWriter_t972330355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::set_CloseOutput(System.Boolean)
extern "C"  void JsonWriter_set_CloseOutput_m3210063196 (JsonWriter_t972330355 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.JsonWriter::get_Top()
extern "C"  int32_t JsonWriter_get_Top_m75893639 (JsonWriter_t972330355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.WriteState Newtonsoft.Json.JsonWriter::get_WriteState()
extern "C"  int32_t JsonWriter_get_WriteState_m1774636522 (JsonWriter_t972330355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Formatting Newtonsoft.Json.JsonWriter::get_Formatting()
extern "C"  int32_t JsonWriter_get_Formatting_m636140554 (JsonWriter_t972330355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::set_Formatting(Newtonsoft.Json.Formatting)
extern "C"  void JsonWriter_set_Formatting_m1798497025 (JsonWriter_t972330355 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::Push(Newtonsoft.Json.Linq.JTokenType)
extern "C"  void JsonWriter_Push_m1104926333 (JsonWriter_t972330355 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.JsonWriter::Pop()
extern "C"  int32_t JsonWriter_Pop_m3959503593 (JsonWriter_t972330355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.JsonWriter::Peek()
extern "C"  int32_t JsonWriter_Peek_m2476028581 (JsonWriter_t972330355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::Close()
extern "C"  void JsonWriter_Close_m3733433569 (JsonWriter_t972330355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteStartObject()
extern "C"  void JsonWriter_WriteStartObject_m3251858555 (JsonWriter_t972330355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteEndObject()
extern "C"  void JsonWriter_WriteEndObject_m2641277492 (JsonWriter_t972330355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteStartArray()
extern "C"  void JsonWriter_WriteStartArray_m1307197887 (JsonWriter_t972330355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteEndArray()
extern "C"  void JsonWriter_WriteEndArray_m3781353702 (JsonWriter_t972330355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteStartConstructor(System.String)
extern "C"  void JsonWriter_WriteStartConstructor_m168503202 (JsonWriter_t972330355 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteEndConstructor()
extern "C"  void JsonWriter_WriteEndConstructor_m4022163783 (JsonWriter_t972330355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WritePropertyName(System.String)
extern "C"  void JsonWriter_WritePropertyName_m2518733658 (JsonWriter_t972330355 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteEnd()
extern "C"  void JsonWriter_WriteEnd_m1700740501 (JsonWriter_t972330355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteToken(Newtonsoft.Json.JsonReader)
extern "C"  void JsonWriter_WriteToken_m1251197267 (JsonWriter_t972330355 * __this, JsonReader_t816925123 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteToken(Newtonsoft.Json.JsonReader,System.Int32)
extern "C"  void JsonWriter_WriteToken_m458840676 (JsonWriter_t972330355 * __this, JsonReader_t816925123 * ___reader0, int32_t ___initialDepth1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteConstructorDate(Newtonsoft.Json.JsonReader)
extern "C"  void JsonWriter_WriteConstructorDate_m2708788834 (JsonWriter_t972330355 * __this, JsonReader_t816925123 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonWriter::IsEndToken(Newtonsoft.Json.JsonToken)
extern "C"  bool JsonWriter_IsEndToken_m454918497 (JsonWriter_t972330355 * __this, int32_t ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonWriter::IsStartToken(Newtonsoft.Json.JsonToken)
extern "C"  bool JsonWriter_IsStartToken_m2529066600 (JsonWriter_t972330355 * __this, int32_t ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteEnd(Newtonsoft.Json.Linq.JTokenType)
extern "C"  void JsonWriter_WriteEnd_m2973420891 (JsonWriter_t972330355 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::AutoCompleteAll()
extern "C"  void JsonWriter_AutoCompleteAll_m3228057890 (JsonWriter_t972330355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.JsonWriter::GetTypeForCloseToken(Newtonsoft.Json.JsonToken)
extern "C"  int32_t JsonWriter_GetTypeForCloseToken_m206822898 (JsonWriter_t972330355 * __this, int32_t ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonToken Newtonsoft.Json.JsonWriter::GetCloseTokenForType(Newtonsoft.Json.Linq.JTokenType)
extern "C"  int32_t JsonWriter_GetCloseTokenForType_m1390517268 (JsonWriter_t972330355 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::AutoCompleteClose(Newtonsoft.Json.JsonToken)
extern "C"  void JsonWriter_AutoCompleteClose_m926568989 (JsonWriter_t972330355 * __this, int32_t ___tokenBeingClosed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteEnd(Newtonsoft.Json.JsonToken)
extern "C"  void JsonWriter_WriteEnd_m4219985857 (JsonWriter_t972330355 * __this, int32_t ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteIndent()
extern "C"  void JsonWriter_WriteIndent_m1765851540 (JsonWriter_t972330355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValueDelimiter()
extern "C"  void JsonWriter_WriteValueDelimiter_m38595326 (JsonWriter_t972330355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteIndentSpace()
extern "C"  void JsonWriter_WriteIndentSpace_m334924724 (JsonWriter_t972330355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::AutoComplete(Newtonsoft.Json.JsonToken)
extern "C"  void JsonWriter_AutoComplete_m1699166389 (JsonWriter_t972330355 * __this, int32_t ___tokenBeingWritten0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteNull()
extern "C"  void JsonWriter_WriteNull_m1447778671 (JsonWriter_t972330355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteUndefined()
extern "C"  void JsonWriter_WriteUndefined_m2876722698 (JsonWriter_t972330355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteRaw(System.String)
extern "C"  void JsonWriter_WriteRaw_m1394575520 (JsonWriter_t972330355 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteRawValue(System.String)
extern "C"  void JsonWriter_WriteRawValue_m191707281 (JsonWriter_t972330355 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.String)
extern "C"  void JsonWriter_WriteValue_m2678318711 (JsonWriter_t972330355 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Int32)
extern "C"  void JsonWriter_WriteValue_m1457138492 (JsonWriter_t972330355 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.UInt32)
extern "C"  void JsonWriter_WriteValue_m3218847855 (JsonWriter_t972330355 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Int64)
extern "C"  void JsonWriter_WriteValue_m1457141437 (JsonWriter_t972330355 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.UInt64)
extern "C"  void JsonWriter_WriteValue_m3218850800 (JsonWriter_t972330355 * __this, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Single)
extern "C"  void JsonWriter_WriteValue_m2359642400 (JsonWriter_t972330355 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Double)
extern "C"  void JsonWriter_WriteValue_m2110079671 (JsonWriter_t972330355 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Boolean)
extern "C"  void JsonWriter_WriteValue_m1634557090 (JsonWriter_t972330355 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Int16)
extern "C"  void JsonWriter_WriteValue_m1457136694 (JsonWriter_t972330355 * __this, int16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.UInt16)
extern "C"  void JsonWriter_WriteValue_m3218846057 (JsonWriter_t972330355 * __this, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Char)
extern "C"  void JsonWriter_WriteValue_m3089309586 (JsonWriter_t972330355 * __this, Il2CppChar ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Byte)
extern "C"  void JsonWriter_WriteValue_m3088910368 (JsonWriter_t972330355 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.SByte)
extern "C"  void JsonWriter_WriteValue_m1703008079 (JsonWriter_t972330355 * __this, int8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Decimal)
extern "C"  void JsonWriter_WriteValue_m194026137 (JsonWriter_t972330355 * __this, Decimal_t1954350631  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.DateTime)
extern "C"  void JsonWriter_WriteValue_m1108866157 (JsonWriter_t972330355 * __this, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.DateTimeOffset)
extern "C"  void JsonWriter_WriteValue_m2573629210 (JsonWriter_t972330355 * __this, DateTimeOffset_t3884714306  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Guid)
extern "C"  void JsonWriter_WriteValue_m3093398207 (JsonWriter_t972330355 * __this, Guid_t2862754429  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(SerializableGuid)
extern "C"  void JsonWriter_WriteValue_m2838557475 (JsonWriter_t972330355 * __this, SerializableGuid_t3998617672 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.TimeSpan)
extern "C"  void JsonWriter_WriteValue_m1393661905 (JsonWriter_t972330355 * __this, TimeSpan_t413522987  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Int32>)
extern "C"  void JsonWriter_WriteValue_m2740159107 (JsonWriter_t972330355 * __this, Nullable_1_t1237965023  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.UInt32>)
extern "C"  void JsonWriter_WriteValue_m4189816314 (JsonWriter_t972330355 * __this, Nullable_1_t108794504  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Int64>)
extern "C"  void JsonWriter_WriteValue_m2740250402 (JsonWriter_t972330355 * __this, Nullable_1_t1237965118  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.UInt64>)
extern "C"  void JsonWriter_WriteValue_m4189907609 (JsonWriter_t972330355 * __this, Nullable_1_t108794599  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Single>)
extern "C"  void JsonWriter_WriteValue_m3324250985 (JsonWriter_t972330355 * __this, Nullable_1_t81078199  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Double>)
extern "C"  void JsonWriter_WriteValue_m4177740978 (JsonWriter_t972330355 * __this, Nullable_1_t3952353088  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Boolean>)
extern "C"  void JsonWriter_WriteValue_m3525560093 (JsonWriter_t972330355 * __this, Nullable_1_t560925241  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Int16>)
extern "C"  void JsonWriter_WriteValue_m2740103369 (JsonWriter_t972330355 * __this, Nullable_1_t1237964965  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.UInt16>)
extern "C"  void JsonWriter_WriteValue_m4189760576 (JsonWriter_t972330355 * __this, Nullable_1_t108794446  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Char>)
extern "C"  void JsonWriter_WriteValue_m2127328375 (JsonWriter_t972330355 * __this, Nullable_1_t2946749061  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Byte>)
extern "C"  void JsonWriter_WriteValue_m2114952617 (JsonWriter_t972330355 * __this, Nullable_1_t2946736183  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.SByte>)
extern "C"  void JsonWriter_WriteValue_m1772181712 (JsonWriter_t972330355 * __this, Nullable_1_t1245896300  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Decimal>)
extern "C"  void JsonWriter_WriteValue_m1818773510 (JsonWriter_t972330355 * __this, Nullable_1_t2038477154  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.DateTime>)
extern "C"  void JsonWriter_WriteValue_m3081939068 (JsonWriter_t972330355 * __this, Nullable_1_t72820554  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.DateTimeOffset>)
extern "C"  void JsonWriter_WriteValue_m2516318127 (JsonWriter_t972330355 * __this, Nullable_1_t3968840829  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Guid>)
extern "C"  void JsonWriter_WriteValue_m2254075626 (JsonWriter_t972330355 * __this, Nullable_1_t2946880952  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.TimeSpan>)
extern "C"  void JsonWriter_WriteValue_m3320672664 (JsonWriter_t972330355 * __this, Nullable_1_t497649510  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Byte[])
extern "C"  void JsonWriter_WriteValue_m620513086 (JsonWriter_t972330355 * __this, ByteU5BU5D_t4260760469* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Uri)
extern "C"  void JsonWriter_WriteValue_m515843166 (JsonWriter_t972330355 * __this, Uri_t1116831938 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Object)
extern "C"  void JsonWriter_WriteValue_m2900429065 (JsonWriter_t972330355 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteComment(System.String)
extern "C"  void JsonWriter_WriteComment_m3790328329 (JsonWriter_t972330355 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::WriteWhitespace(System.String)
extern "C"  void JsonWriter_WriteWhitespace_m1110984573 (JsonWriter_t972330355 * __this, String_t* ___ws0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::Dispose(System.Boolean)
extern "C"  void JsonWriter_Dispose_m1436878015 (JsonWriter_t972330355 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::ilo_AutoCompleteAll1(Newtonsoft.Json.JsonWriter)
extern "C"  void JsonWriter_ilo_AutoCompleteAll1_m4265069524 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::ilo_Push2(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Linq.JTokenType)
extern "C"  void JsonWriter_ilo_Push2_m2053157096 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::ilo_AutoCompleteClose3(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonToken)
extern "C"  void JsonWriter_ilo_AutoCompleteClose3_m3582130255 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, int32_t ___tokenBeingClosed1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::ilo_AutoComplete4(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonToken)
extern "C"  void JsonWriter_ilo_AutoComplete4_m2599919074 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, int32_t ___tokenBeingWritten1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.JsonWriter::ilo_Peek5(Newtonsoft.Json.JsonWriter)
extern "C"  int32_t JsonWriter_ilo_Peek5_m907055471 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.JsonWriter::ilo_get_Depth6(Newtonsoft.Json.JsonReader)
extern "C"  int32_t JsonWriter_ilo_get_Depth6_m839866198 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::ilo_WriteToken7(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonReader,System.Int32)
extern "C"  void JsonWriter_ilo_WriteToken7_m1319924116 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, JsonReader_t816925123 * ___reader1, int32_t ___initialDepth2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::ilo_WriteStartConstructor8(Newtonsoft.Json.JsonWriter,System.String)
extern "C"  void JsonWriter_ilo_WriteStartConstructor8_m1775354265 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.JsonWriter::ilo_get_Value9(Newtonsoft.Json.JsonReader)
extern "C"  Il2CppObject * JsonWriter_ilo_get_Value9_m109825618 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::ilo_WriteComment10(Newtonsoft.Json.JsonWriter,System.String)
extern "C"  void JsonWriter_ilo_WriteComment10_m2529688535 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::ilo_WriteValue11(Newtonsoft.Json.JsonWriter,System.Int64)
extern "C"  void JsonWriter_ilo_WriteValue11_m2918229290 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, int64_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::ilo_WriteValue12(Newtonsoft.Json.JsonWriter,System.Double)
extern "C"  void JsonWriter_ilo_WriteValue12_m4005433515 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, double ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::ilo_WriteNull13(Newtonsoft.Json.JsonWriter)
extern "C"  void JsonWriter_ilo_WriteNull13_m2596959022 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::ilo_WriteUndefined14(Newtonsoft.Json.JsonWriter)
extern "C"  void JsonWriter_ilo_WriteUndefined14_m4074695952 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::ilo_WriteEndObject15(Newtonsoft.Json.JsonWriter)
extern "C"  void JsonWriter_ilo_WriteEndObject15_m33796795 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonWriter::ilo_Read16(Newtonsoft.Json.JsonReader)
extern "C"  bool JsonWriter_ilo_Read16_m1461384667 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::ilo_WriteEndConstructor17(Newtonsoft.Json.JsonWriter)
extern "C"  void JsonWriter_ilo_WriteEndConstructor17_m1122495690 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.JsonWriter::ilo_GetTypeForCloseToken18(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonToken)
extern "C"  int32_t JsonWriter_ilo_GetTypeForCloseToken18_m818167294 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, int32_t ___token1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.JsonWriter::ilo_Pop19(Newtonsoft.Json.JsonWriter)
extern "C"  int32_t JsonWriter_ilo_Pop19_m3960756974 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonToken Newtonsoft.Json.JsonWriter::ilo_GetCloseTokenForType20(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Linq.JTokenType)
extern "C"  int32_t JsonWriter_ilo_GetCloseTokenForType20_m2523668007 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::ilo_WriteIndent21(Newtonsoft.Json.JsonWriter)
extern "C"  void JsonWriter_ilo_WriteIndent21_m3455049008 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::ilo_WriteValue22(Newtonsoft.Json.JsonWriter,System.UInt32)
extern "C"  void JsonWriter_ilo_WriteValue22_m4089507778 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::ilo_WriteValue23(Newtonsoft.Json.JsonWriter,System.UInt64)
extern "C"  void JsonWriter_ilo_WriteValue23_m3640814084 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, uint64_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::ilo_WriteValue24(Newtonsoft.Json.JsonWriter,System.Single)
extern "C"  void JsonWriter_ilo_WriteValue24_m2332909045 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::ilo_WriteValue25(Newtonsoft.Json.JsonWriter,System.Int16)
extern "C"  void JsonWriter_ilo_WriteValue25_m2965820896 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, int16_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::ilo_WriteValue26(Newtonsoft.Json.JsonWriter,System.Char)
extern "C"  void JsonWriter_ilo_WriteValue26_m3003431849 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, Il2CppChar ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::ilo_WriteValue27(Newtonsoft.Json.JsonWriter,System.Decimal)
extern "C"  void JsonWriter_ilo_WriteValue27_m586177665 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, Decimal_t1954350631  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::ilo_WriteValue28(Newtonsoft.Json.JsonWriter,System.DateTime)
extern "C"  void JsonWriter_ilo_WriteValue28_m2974888454 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, DateTime_t4283661327  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::ilo_WriteValue29(Newtonsoft.Json.JsonWriter,System.UInt16)
extern "C"  void JsonWriter_ilo_WriteValue29_m948629507 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, uint16_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::ilo_WriteValue30(Newtonsoft.Json.JsonWriter,System.Byte)
extern "C"  void JsonWriter_ilo_WriteValue30_m1856165712 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, uint8_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriter::ilo_WriteValue31(Newtonsoft.Json.JsonWriter,System.DateTimeOffset)
extern "C"  void JsonWriter_ilo_WriteValue31_m3366070283 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, DateTimeOffset_t3884714306  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
