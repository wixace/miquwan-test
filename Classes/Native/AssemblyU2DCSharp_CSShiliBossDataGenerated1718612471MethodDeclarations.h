﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSShiliBossDataGenerated
struct CSShiliBossDataGenerated_t1718612471;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// CSShiliBossData
struct CSShiliBossData_t2808712824;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_CSShiliBossData2808712824.h"

// System.Void CSShiliBossDataGenerated::.ctor()
extern "C"  void CSShiliBossDataGenerated__ctor_m3188515460 (CSShiliBossDataGenerated_t1718612471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSShiliBossDataGenerated::CSShiliBossData_CSShiliBossData1(JSVCall,System.Int32)
extern "C"  bool CSShiliBossDataGenerated_CSShiliBossData_CSShiliBossData1_m3719854724 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSShiliBossDataGenerated::CSShiliBossData_CurShiliBossUnit(JSVCall)
extern "C"  void CSShiliBossDataGenerated_CSShiliBossData_CurShiliBossUnit_m3615165086 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSShiliBossDataGenerated::CSShiliBossData_LoadData__String(JSVCall,System.Int32)
extern "C"  bool CSShiliBossDataGenerated_CSShiliBossData_LoadData__String_m2556638636 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSShiliBossDataGenerated::CSShiliBossData_UnLoadData(JSVCall,System.Int32)
extern "C"  bool CSShiliBossDataGenerated_CSShiliBossData_UnLoadData_m299715796 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSShiliBossDataGenerated::__Register()
extern "C"  void CSShiliBossDataGenerated___Register_m395414723 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSShiliBossDataGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t CSShiliBossDataGenerated_ilo_getObject1_m4056434958 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CSShiliBossDataGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * CSShiliBossDataGenerated_ilo_getObject2_m4171722220 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSShiliBossDataGenerated::ilo_UnLoadData3(CSShiliBossData)
extern "C"  void CSShiliBossDataGenerated_ilo_UnLoadData3_m431070305 (Il2CppObject * __this /* static, unused */, CSShiliBossData_t2808712824 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
