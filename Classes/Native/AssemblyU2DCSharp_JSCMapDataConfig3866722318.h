﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<JSCLevelConfig>
struct List_1_t2779285052;
// System.Collections.Generic.List`1<JSCMapPathConfig>
struct List_1_t499336985;
// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSCMapDataConfig
struct  JSCMapDataConfig_t3866722318  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<JSCLevelConfig> JSCMapDataConfig::_jarr
	List_1_t2779285052 * ____jarr_0;
	// System.Collections.Generic.List`1<JSCMapPathConfig> JSCMapDataConfig::_pathjarr
	List_1_t499336985 * ____pathjarr_1;
	// ProtoBuf.IExtension JSCMapDataConfig::extensionObject
	Il2CppObject * ___extensionObject_2;

public:
	inline static int32_t get_offset_of__jarr_0() { return static_cast<int32_t>(offsetof(JSCMapDataConfig_t3866722318, ____jarr_0)); }
	inline List_1_t2779285052 * get__jarr_0() const { return ____jarr_0; }
	inline List_1_t2779285052 ** get_address_of__jarr_0() { return &____jarr_0; }
	inline void set__jarr_0(List_1_t2779285052 * value)
	{
		____jarr_0 = value;
		Il2CppCodeGenWriteBarrier(&____jarr_0, value);
	}

	inline static int32_t get_offset_of__pathjarr_1() { return static_cast<int32_t>(offsetof(JSCMapDataConfig_t3866722318, ____pathjarr_1)); }
	inline List_1_t499336985 * get__pathjarr_1() const { return ____pathjarr_1; }
	inline List_1_t499336985 ** get_address_of__pathjarr_1() { return &____pathjarr_1; }
	inline void set__pathjarr_1(List_1_t499336985 * value)
	{
		____pathjarr_1 = value;
		Il2CppCodeGenWriteBarrier(&____pathjarr_1, value);
	}

	inline static int32_t get_offset_of_extensionObject_2() { return static_cast<int32_t>(offsetof(JSCMapDataConfig_t3866722318, ___extensionObject_2)); }
	inline Il2CppObject * get_extensionObject_2() const { return ___extensionObject_2; }
	inline Il2CppObject ** get_address_of_extensionObject_2() { return &___extensionObject_2; }
	inline void set_extensionObject_2(Il2CppObject * value)
	{
		___extensionObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___extensionObject_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
