﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CEvent.EventDispatch`1<System.Object>
struct EventDispatch_1_t1067954939;
// CEvent.EventFunc`1<System.Object>
struct EventFunc_1_t1647712181;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void CEvent.EventDispatch`1<System.Object>::.ctor()
extern "C"  void EventDispatch_1__ctor_m302555037_gshared (EventDispatch_1_t1067954939 * __this, const MethodInfo* method);
#define EventDispatch_1__ctor_m302555037(__this, method) ((  void (*) (EventDispatch_1_t1067954939 *, const MethodInfo*))EventDispatch_1__ctor_m302555037_gshared)(__this, method)
// System.Void CEvent.EventDispatch`1<System.Object>::AddEventListener(System.Int32,CEvent.EventFunc`1<T>,System.Boolean)
extern "C"  void EventDispatch_1_AddEventListener_m204507418_gshared (EventDispatch_1_t1067954939 * __this, int32_t ___eventName0, EventFunc_1_t1647712181 * ___func1, bool ___isInsert2, const MethodInfo* method);
#define EventDispatch_1_AddEventListener_m204507418(__this, ___eventName0, ___func1, ___isInsert2, method) ((  void (*) (EventDispatch_1_t1067954939 *, int32_t, EventFunc_1_t1647712181 *, bool, const MethodInfo*))EventDispatch_1_AddEventListener_m204507418_gshared)(__this, ___eventName0, ___func1, ___isInsert2, method)
// System.Void CEvent.EventDispatch`1<System.Object>::AddEventListener(System.Int32,CEvent.EventFunc`1<T>)
extern "C"  void EventDispatch_1_AddEventListener_m2707481603_gshared (EventDispatch_1_t1067954939 * __this, int32_t ___eventName0, EventFunc_1_t1647712181 * ___func1, const MethodInfo* method);
#define EventDispatch_1_AddEventListener_m2707481603(__this, ___eventName0, ___func1, method) ((  void (*) (EventDispatch_1_t1067954939 *, int32_t, EventFunc_1_t1647712181 *, const MethodInfo*))EventDispatch_1_AddEventListener_m2707481603_gshared)(__this, ___eventName0, ___func1, method)
// System.Void CEvent.EventDispatch`1<System.Object>::AddEventListener(System.String,CEvent.EventFunc`1<T>)
extern "C"  void EventDispatch_1_AddEventListener_m2565490508_gshared (EventDispatch_1_t1067954939 * __this, String_t* ___eventName0, EventFunc_1_t1647712181 * ___func1, const MethodInfo* method);
#define EventDispatch_1_AddEventListener_m2565490508(__this, ___eventName0, ___func1, method) ((  void (*) (EventDispatch_1_t1067954939 *, String_t*, EventFunc_1_t1647712181 *, const MethodInfo*))EventDispatch_1_AddEventListener_m2565490508_gshared)(__this, ___eventName0, ___func1, method)
// System.Void CEvent.EventDispatch`1<System.Object>::AddEventListener(System.String,CEvent.EventFunc`1<T>,System.Boolean)
extern "C"  void EventDispatch_1_AddEventListener_m2901215921_gshared (EventDispatch_1_t1067954939 * __this, String_t* ___eventName0, EventFunc_1_t1647712181 * ___func1, bool ___isInsert2, const MethodInfo* method);
#define EventDispatch_1_AddEventListener_m2901215921(__this, ___eventName0, ___func1, ___isInsert2, method) ((  void (*) (EventDispatch_1_t1067954939 *, String_t*, EventFunc_1_t1647712181 *, bool, const MethodInfo*))EventDispatch_1_AddEventListener_m2901215921_gshared)(__this, ___eventName0, ___func1, ___isInsert2, method)
// System.Void CEvent.EventDispatch`1<System.Object>::RemoveEventListener(System.Int32,CEvent.EventFunc`1<T>)
extern "C"  void EventDispatch_1_RemoveEventListener_m4250415028_gshared (EventDispatch_1_t1067954939 * __this, int32_t ___eventName0, EventFunc_1_t1647712181 * ___func1, const MethodInfo* method);
#define EventDispatch_1_RemoveEventListener_m4250415028(__this, ___eventName0, ___func1, method) ((  void (*) (EventDispatch_1_t1067954939 *, int32_t, EventFunc_1_t1647712181 *, const MethodInfo*))EventDispatch_1_RemoveEventListener_m4250415028_gshared)(__this, ___eventName0, ___func1, method)
// System.Void CEvent.EventDispatch`1<System.Object>::RemoveEventListener(System.String,CEvent.EventFunc`1<T>)
extern "C"  void EventDispatch_1_RemoveEventListener_m3151786427_gshared (EventDispatch_1_t1067954939 * __this, String_t* ___eventName0, EventFunc_1_t1647712181 * ___func1, const MethodInfo* method);
#define EventDispatch_1_RemoveEventListener_m3151786427(__this, ___eventName0, ___func1, method) ((  void (*) (EventDispatch_1_t1067954939 *, String_t*, EventFunc_1_t1647712181 *, const MethodInfo*))EventDispatch_1_RemoveEventListener_m3151786427_gshared)(__this, ___eventName0, ___func1, method)
// System.Void CEvent.EventDispatch`1<System.Object>::DispatchEvent(T)
extern "C"  void EventDispatch_1_DispatchEvent_m160337859_gshared (EventDispatch_1_t1067954939 * __this, Il2CppObject * ___ev0, const MethodInfo* method);
#define EventDispatch_1_DispatchEvent_m160337859(__this, ___ev0, method) ((  void (*) (EventDispatch_1_t1067954939 *, Il2CppObject *, const MethodInfo*))EventDispatch_1_DispatchEvent_m160337859_gshared)(__this, ___ev0, method)
