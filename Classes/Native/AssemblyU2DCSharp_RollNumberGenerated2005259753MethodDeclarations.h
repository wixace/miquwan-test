﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RollNumberGenerated
struct RollNumberGenerated_t2005259753;
// JSVCall
struct JSVCall_t3708497963;
// EventDelegate/Callback
struct Callback_t1094463061;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// RollNumber
struct RollNumber_t957354566;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "AssemblyU2DCSharp_RollNumber957354566.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void RollNumberGenerated::.ctor()
extern "C"  void RollNumberGenerated__ctor_m1110430626 (RollNumberGenerated_t2005259753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RollNumberGenerated::RollNumber_RollNumber1(JSVCall,System.Int32)
extern "C"  bool RollNumberGenerated_RollNumber_RollNumber1_m1154003216 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate/Callback RollNumberGenerated::RollNumber_MoveTo_GetDelegate_member0_arg4(CSRepresentedObject)
extern "C"  Callback_t1094463061 * RollNumberGenerated_RollNumber_MoveTo_GetDelegate_member0_arg4_m2543004701 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RollNumberGenerated::RollNumber_MoveTo__Single__Single__Int32__Int32__Callback(JSVCall,System.Int32)
extern "C"  bool RollNumberGenerated_RollNumber_MoveTo__Single__Single__Int32__Int32__Callback_m840669598 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RollNumberGenerated::RollNumber_MoveTo__Single__Single__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool RollNumberGenerated_RollNumber_MoveTo__Single__Single__Int32__Int32_m2702574873 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate/Callback RollNumberGenerated::RollNumber_MoveTo_GetDelegate_member2_arg3(CSRepresentedObject)
extern "C"  Callback_t1094463061 * RollNumberGenerated_RollNumber_MoveTo_GetDelegate_member2_arg3_m2209321728 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RollNumberGenerated::RollNumber_MoveTo__Single__Int32__Int32__Callback(JSVCall,System.Int32)
extern "C"  bool RollNumberGenerated_RollNumber_MoveTo__Single__Int32__Int32__Callback_m1780892502 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RollNumberGenerated::RollNumber_MoveTo__Single__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool RollNumberGenerated_RollNumber_MoveTo__Single__Int32__Int32_m736031953 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RollNumberGenerated::__Register()
extern "C"  void RollNumberGenerated___Register_m1271832165 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate/Callback RollNumberGenerated::<RollNumber_MoveTo__Single__Single__Int32__Int32__Callback>m__95()
extern "C"  Callback_t1094463061 * RollNumberGenerated_U3CRollNumber_MoveTo__Single__Single__Int32__Int32__CallbackU3Em__95_m716223715 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate/Callback RollNumberGenerated::<RollNumber_MoveTo__Single__Int32__Int32__Callback>m__97()
extern "C"  Callback_t1094463061 * RollNumberGenerated_U3CRollNumber_MoveTo__Single__Int32__Int32__CallbackU3Em__97_m245486109 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RollNumberGenerated::ilo_MoveTo1(RollNumber,System.Single,System.Single,System.Int32,System.Int32,EventDelegate/Callback)
extern "C"  void RollNumberGenerated_ilo_MoveTo1_m3197120827 (Il2CppObject * __this /* static, unused */, RollNumber_t957354566 * ____this0, float ___time1, float ___delay2, int32_t ___start3, int32_t ___end4, Callback_t1094463061 * ___call5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single RollNumberGenerated::ilo_getSingle2(System.Int32)
extern "C"  float RollNumberGenerated_ilo_getSingle2_m282756590 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 RollNumberGenerated::ilo_getInt323(System.Int32)
extern "C"  int32_t RollNumberGenerated_ilo_getInt323_m1733818567 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object RollNumberGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * RollNumberGenerated_ilo_getObject4_m512422534 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RollNumberGenerated::ilo_isFunctionS5(System.Int32)
extern "C"  bool RollNumberGenerated_ilo_isFunctionS5_m167399734 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
