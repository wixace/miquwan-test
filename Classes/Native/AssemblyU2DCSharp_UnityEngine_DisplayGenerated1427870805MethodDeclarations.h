﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_DisplayGenerated
struct UnityEngine_DisplayGenerated_t1427870805;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Display[]
struct DisplayU5BU5D_t3684569385;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_DisplayGenerated::.ctor()
extern "C"  void UnityEngine_DisplayGenerated__ctor_m3735961062 (UnityEngine_DisplayGenerated_t1427870805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DisplayGenerated::Display_displays(JSVCall)
extern "C"  void UnityEngine_DisplayGenerated_Display_displays_m4200981947 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DisplayGenerated::Display_renderingWidth(JSVCall)
extern "C"  void UnityEngine_DisplayGenerated_Display_renderingWidth_m197355698 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DisplayGenerated::Display_renderingHeight(JSVCall)
extern "C"  void UnityEngine_DisplayGenerated_Display_renderingHeight_m2478502989 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DisplayGenerated::Display_systemWidth(JSVCall)
extern "C"  void UnityEngine_DisplayGenerated_Display_systemWidth_m3995934665 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DisplayGenerated::Display_systemHeight(JSVCall)
extern "C"  void UnityEngine_DisplayGenerated_Display_systemHeight_m4270333974 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DisplayGenerated::Display_colorBuffer(JSVCall)
extern "C"  void UnityEngine_DisplayGenerated_Display_colorBuffer_m1643550877 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DisplayGenerated::Display_depthBuffer(JSVCall)
extern "C"  void UnityEngine_DisplayGenerated_Display_depthBuffer_m383177021 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DisplayGenerated::Display_main(JSVCall)
extern "C"  void UnityEngine_DisplayGenerated_Display_main_m3210236915 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DisplayGenerated::Display_Activate__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DisplayGenerated_Display_Activate__Int32__Int32__Int32_m4284653886 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DisplayGenerated::Display_Activate(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DisplayGenerated_Display_Activate_m1326270290 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DisplayGenerated::Display_SetParams__Int32__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DisplayGenerated_Display_SetParams__Int32__Int32__Int32__Int32_m3303121227 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DisplayGenerated::Display_SetRenderingResolution__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DisplayGenerated_Display_SetRenderingResolution__Int32__Int32_m3447894901 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DisplayGenerated::Display_RelativeMouseAt__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DisplayGenerated_Display_RelativeMouseAt__Vector3_m762429731 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DisplayGenerated::__Register()
extern "C"  void UnityEngine_DisplayGenerated___Register_m600571553 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Display[] UnityEngine_DisplayGenerated::<Display_displays>m__1AA()
extern "C"  DisplayU5BU5D_t3684569385* UnityEngine_DisplayGenerated_U3CDisplay_displaysU3Em__1AA_m1562984540 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_DisplayGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_DisplayGenerated_ilo_setObject1_m2434118500 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DisplayGenerated::ilo_setInt322(System.Int32,System.Int32)
extern "C"  void UnityEngine_DisplayGenerated_ilo_setInt322_m3621270031 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_DisplayGenerated::ilo_getInt323(System.Int32)
extern "C"  int32_t UnityEngine_DisplayGenerated_ilo_getInt323_m814181551 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_DisplayGenerated::ilo_getObject4(System.Int32)
extern "C"  int32_t UnityEngine_DisplayGenerated_ilo_getObject4_m1167394479 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_DisplayGenerated::ilo_getElement5(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_DisplayGenerated_ilo_getElement5_m542224564 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
