﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUIGenerated/<GUI_Window_GetDelegate_member106_arg2>c__AnonStoreyEE
struct U3CGUI_Window_GetDelegate_member106_arg2U3Ec__AnonStoreyEE_t3011023597;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_GUIGenerated/<GUI_Window_GetDelegate_member106_arg2>c__AnonStoreyEE::.ctor()
extern "C"  void U3CGUI_Window_GetDelegate_member106_arg2U3Ec__AnonStoreyEE__ctor_m4240830238 (U3CGUI_Window_GetDelegate_member106_arg2U3Ec__AnonStoreyEE_t3011023597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated/<GUI_Window_GetDelegate_member106_arg2>c__AnonStoreyEE::<>m__1D2(System.Int32)
extern "C"  void U3CGUI_Window_GetDelegate_member106_arg2U3Ec__AnonStoreyEE_U3CU3Em__1D2_m1636523851 (U3CGUI_Window_GetDelegate_member106_arg2U3Ec__AnonStoreyEE_t3011023597 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
