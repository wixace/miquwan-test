﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConfigAssetMgr
struct ConfigAssetMgr_t4036193930;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Singleton`1<ConfigAssetMgr>
struct  Singleton_1_t4289009323  : public Il2CppObject
{
public:

public:
};

struct Singleton_1_t4289009323_StaticFields
{
public:
	// T Singleton`1::_instance
	ConfigAssetMgr_t4036193930 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(Singleton_1_t4289009323_StaticFields, ____instance_0)); }
	inline ConfigAssetMgr_t4036193930 * get__instance_0() const { return ____instance_0; }
	inline ConfigAssetMgr_t4036193930 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(ConfigAssetMgr_t4036193930 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
