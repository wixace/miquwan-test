﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ListPool_1_gen2456319425MethodDeclarations.h"

// System.Void Pathfinding.Util.ListPool`1<Pathfinding.NavmeshCut>::.cctor()
#define ListPool_1__cctor_m4198774261(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1__cctor_m2656574753_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<Pathfinding.NavmeshCut>::Claim()
#define ListPool_1_Claim_m1972326567(__this /* static, unused */, method) ((  List_1_t1669178200 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1_Claim_m2151275825_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<Pathfinding.NavmeshCut>::Claim(System.Int32)
#define ListPool_1_Claim_m924730232(__this /* static, unused */, ___capacity0, method) ((  List_1_t1669178200 * (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))ListPool_1_Claim_m3438825730_gshared)(__this /* static, unused */, ___capacity0, method)
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.NavmeshCut>::Warmup(System.Int32,System.Int32)
#define ListPool_1_Warmup_m1815297530(__this /* static, unused */, ___count0, ___size1, method) ((  void (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))ListPool_1_Warmup_m3738927182_gshared)(__this /* static, unused */, ___count0, ___size1, method)
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.NavmeshCut>::Release(System.Collections.Generic.List`1<T>)
#define ListPool_1_Release_m1494110389(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, List_1_t1669178200 *, const MethodInfo*))ListPool_1_Release_m442610953_gshared)(__this /* static, unused */, ___list0, method)
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.NavmeshCut>::Clear()
#define ListPool_1_Clear_m50981091(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1_Clear_m3464916023_gshared)(__this /* static, unused */, method)
// System.Int32 Pathfinding.Util.ListPool`1<Pathfinding.NavmeshCut>::GetSize()
#define ListPool_1_GetSize_m831165659(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1_GetSize_m4059414611_gshared)(__this /* static, unused */, method)
