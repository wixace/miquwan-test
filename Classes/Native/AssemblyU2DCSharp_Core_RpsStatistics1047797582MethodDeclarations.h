﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Core.RpsStatistics
struct RpsStatistics_t1047797582;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Core_RpsResult479594824.h"
#include "AssemblyU2DCSharp_Core_RpsChoice52797132.h"
#include "AssemblyU2DCSharp_Core_RpsStatistics1047797582.h"

// System.Void Core.RpsStatistics::.ctor()
extern "C"  void RpsStatistics__ctor_m1362020818 (RpsStatistics_t1047797582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Core.RpsResult Core.RpsStatistics::GetCurrentResult()
extern "C"  int32_t RpsStatistics_GetCurrentResult_m2027228652 (RpsStatistics_t1047797582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Core.RpsChoice Core.RpsStatistics::GetBotChoice()
extern "C"  int32_t RpsStatistics_GetBotChoice_m480789306 (RpsStatistics_t1047797582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Core.RpsChoice Core.RpsStatistics::GetPlayerChoice()
extern "C"  int32_t RpsStatistics_GetPlayerChoice_m3612434642 (RpsStatistics_t1047797582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Core.RpsStatistics::IsPlayerLastTwoRepeat()
extern "C"  bool RpsStatistics_IsPlayerLastTwoRepeat_m733200994 (RpsStatistics_t1047797582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Core.RpsStatistics::IsPlayerCopyCat()
extern "C"  bool RpsStatistics_IsPlayerCopyCat_m658835826 (RpsStatistics_t1047797582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Core.RpsStatistics::UpdateInfo()
extern "C"  void RpsStatistics_UpdateInfo_m2731561929 (RpsStatistics_t1047797582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Core.RpsChoice Core.RpsStatistics::PredictChoice()
extern "C"  int32_t RpsStatistics_PredictChoice_m1692123156 (RpsStatistics_t1047797582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Core.RpsChoice Core.RpsStatistics::ilo_GetPlayerChoice1(Core.RpsStatistics)
extern "C"  int32_t RpsStatistics_ilo_GetPlayerChoice1_m1835170475 (Il2CppObject * __this /* static, unused */, RpsStatistics_t1047797582 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Core.RpsResult Core.RpsStatistics::ilo_GetCurrentResult2(Core.RpsStatistics)
extern "C"  int32_t RpsStatistics_ilo_GetCurrentResult2_m2454349516 (Il2CppObject * __this /* static, unused */, RpsStatistics_t1047797582 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
