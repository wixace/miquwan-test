﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.Compression.RangeCoder.Encoder
struct Encoder_t2248006694;
// System.IO.Stream
struct Stream_t1561764144;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_2248006694.h"

// System.Void SevenZip.Compression.RangeCoder.Encoder::.ctor()
extern "C"  void Encoder__ctor_m1738498419 (Encoder_t2248006694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.Encoder::SetStream(System.IO.Stream)
extern "C"  void Encoder_SetStream_m3619441962 (Encoder_t2248006694 * __this, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.Encoder::ReleaseStream()
extern "C"  void Encoder_ReleaseStream_m1676819256 (Encoder_t2248006694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.Encoder::Init()
extern "C"  void Encoder_Init_m2085827201 (Encoder_t2248006694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.Encoder::FlushData()
extern "C"  void Encoder_FlushData_m3208576991 (Encoder_t2248006694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.Encoder::FlushStream()
extern "C"  void Encoder_FlushStream_m3954189621 (Encoder_t2248006694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.Encoder::CloseStream()
extern "C"  void Encoder_CloseStream_m3858601001 (Encoder_t2248006694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.Encoder::Encode(System.UInt32,System.UInt32,System.UInt32)
extern "C"  void Encoder_Encode_m3933949243 (Encoder_t2248006694 * __this, uint32_t ___start0, uint32_t ___size1, uint32_t ___total2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.Encoder::ShiftLow()
extern "C"  void Encoder_ShiftLow_m2466405475 (Encoder_t2248006694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.Encoder::EncodeDirectBits(System.UInt32,System.Int32)
extern "C"  void Encoder_EncodeDirectBits_m3389687315 (Encoder_t2248006694 * __this, uint32_t ___v0, int32_t ___numTotalBits1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.Encoder::EncodeBit(System.UInt32,System.Int32,System.UInt32)
extern "C"  void Encoder_EncodeBit_m534849913 (Encoder_t2248006694 * __this, uint32_t ___size00, int32_t ___numTotalBits1, uint32_t ___symbol2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 SevenZip.Compression.RangeCoder.Encoder::GetProcessedSizeAdd()
extern "C"  int64_t Encoder_GetProcessedSizeAdd_m1988625160 (Encoder_t2248006694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.Encoder::ilo_ShiftLow1(SevenZip.Compression.RangeCoder.Encoder)
extern "C"  void Encoder_ilo_ShiftLow1_m3080728093 (Il2CppObject * __this /* static, unused */, Encoder_t2248006694 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
