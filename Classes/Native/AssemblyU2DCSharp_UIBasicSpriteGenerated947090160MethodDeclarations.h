﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIBasicSpriteGenerated
struct UIBasicSpriteGenerated_t947090160;
// JSVCall
struct JSVCall_t3708497963;
// UIBasicSprite
struct UIBasicSprite_t2501337439;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_UIBasicSprite_Type741864970.h"
#include "AssemblyU2DCSharp_UIBasicSprite2501337439.h"
#include "AssemblyU2DCSharp_UIBasicSprite_FillDirection3514300524.h"

// System.Void UIBasicSpriteGenerated::.ctor()
extern "C"  void UIBasicSpriteGenerated__ctor_m4045606827 (UIBasicSpriteGenerated_t947090160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSpriteGenerated::UIBasicSprite_centerType(JSVCall)
extern "C"  void UIBasicSpriteGenerated_UIBasicSprite_centerType_m3604661311 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSpriteGenerated::UIBasicSprite_leftType(JSVCall)
extern "C"  void UIBasicSpriteGenerated_UIBasicSprite_leftType_m3969393613 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSpriteGenerated::UIBasicSprite_rightType(JSVCall)
extern "C"  void UIBasicSpriteGenerated_UIBasicSprite_rightType_m4214229704 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSpriteGenerated::UIBasicSprite_bottomType(JSVCall)
extern "C"  void UIBasicSpriteGenerated_UIBasicSprite_bottomType_m3021709865 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSpriteGenerated::UIBasicSprite_topType(JSVCall)
extern "C"  void UIBasicSpriteGenerated_UIBasicSprite_topType_m3598315055 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSpriteGenerated::UIBasicSprite_type(JSVCall)
extern "C"  void UIBasicSpriteGenerated_UIBasicSprite_type_m4173911188 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSpriteGenerated::UIBasicSprite_flip(JSVCall)
extern "C"  void UIBasicSpriteGenerated_UIBasicSprite_flip_m4135587009 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSpriteGenerated::UIBasicSprite_fillDirection(JSVCall)
extern "C"  void UIBasicSpriteGenerated_UIBasicSprite_fillDirection_m2274103234 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSpriteGenerated::UIBasicSprite_fillAmount(JSVCall)
extern "C"  void UIBasicSpriteGenerated_UIBasicSprite_fillAmount_m3349035827 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSpriteGenerated::UIBasicSprite_minWidth(JSVCall)
extern "C"  void UIBasicSpriteGenerated_UIBasicSprite_minWidth_m467733114 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSpriteGenerated::UIBasicSprite_minHeight(JSVCall)
extern "C"  void UIBasicSpriteGenerated_UIBasicSprite_minHeight_m2270268293 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSpriteGenerated::UIBasicSprite_invert(JSVCall)
extern "C"  void UIBasicSpriteGenerated_UIBasicSprite_invert_m2172064120 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSpriteGenerated::UIBasicSprite_hasBorder(JSVCall)
extern "C"  void UIBasicSpriteGenerated_UIBasicSprite_hasBorder_m4006426840 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSpriteGenerated::UIBasicSprite_premultipliedAlpha(JSVCall)
extern "C"  void UIBasicSpriteGenerated_UIBasicSprite_premultipliedAlpha_m1689385766 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSpriteGenerated::UIBasicSprite_pixelSize(JSVCall)
extern "C"  void UIBasicSpriteGenerated_UIBasicSprite_pixelSize_m3079476663 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSpriteGenerated::__Register()
extern "C"  void UIBasicSpriteGenerated___Register_m1859944444 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIBasicSpriteGenerated::ilo_getEnum1(System.Int32)
extern "C"  int32_t UIBasicSpriteGenerated_ilo_getEnum1_m96982149 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSpriteGenerated::ilo_setEnum2(System.Int32,System.Int32)
extern "C"  void UIBasicSpriteGenerated_ilo_setEnum2_m326678699 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIBasicSprite/Type UIBasicSpriteGenerated::ilo_get_type3(UIBasicSprite)
extern "C"  int32_t UIBasicSpriteGenerated_ilo_get_type3_m1395106112 (Il2CppObject * __this /* static, unused */, UIBasicSprite_t2501337439 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIBasicSprite/FillDirection UIBasicSpriteGenerated::ilo_get_fillDirection4(UIBasicSprite)
extern "C"  int32_t UIBasicSpriteGenerated_ilo_get_fillDirection4_m1914770687 (Il2CppObject * __this /* static, unused */, UIBasicSprite_t2501337439 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIBasicSpriteGenerated::ilo_get_fillAmount5(UIBasicSprite)
extern "C"  float UIBasicSpriteGenerated_ilo_get_fillAmount5_m3587474456 (Il2CppObject * __this /* static, unused */, UIBasicSprite_t2501337439 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSpriteGenerated::ilo_setSingle6(System.Int32,System.Single)
extern "C"  void UIBasicSpriteGenerated_ilo_setSingle6_m2377893742 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIBasicSpriteGenerated::ilo_getSingle7(System.Int32)
extern "C"  float UIBasicSpriteGenerated_ilo_getSingle7_m4221777634 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIBasicSpriteGenerated::ilo_get_minWidth8(UIBasicSprite)
extern "C"  int32_t UIBasicSpriteGenerated_ilo_get_minWidth8_m2499195344 (Il2CppObject * __this /* static, unused */, UIBasicSprite_t2501337439 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIBasicSpriteGenerated::ilo_getBooleanS9(System.Int32)
extern "C"  bool UIBasicSpriteGenerated_ilo_getBooleanS9_m3450647497 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSpriteGenerated::ilo_set_invert10(UIBasicSprite,System.Boolean)
extern "C"  void UIBasicSpriteGenerated_ilo_set_invert10_m1544534568 (Il2CppObject * __this /* static, unused */, UIBasicSprite_t2501337439 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIBasicSpriteGenerated::ilo_get_hasBorder11(UIBasicSprite)
extern "C"  bool UIBasicSpriteGenerated_ilo_get_hasBorder11_m3576077908 (Il2CppObject * __this /* static, unused */, UIBasicSprite_t2501337439 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIBasicSpriteGenerated::ilo_get_pixelSize12(UIBasicSprite)
extern "C"  float UIBasicSpriteGenerated_ilo_get_pixelSize12_m3996379690 (Il2CppObject * __this /* static, unused */, UIBasicSprite_t2501337439 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
