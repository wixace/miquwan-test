﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>
struct Dictionary_2_t1674540875;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2991864267.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21573321581.h"
#include "AssemblyU2DCSharp_UIModelDisplayType1891752679.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2965822312_gshared (Enumerator_t2991864267 * __this, Dictionary_2_t1674540875 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m2965822312(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2991864267 *, Dictionary_2_t1674540875 *, const MethodInfo*))Enumerator__ctor_m2965822312_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1500736451_gshared (Enumerator_t2991864267 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1500736451(__this, method) ((  Il2CppObject * (*) (Enumerator_t2991864267 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1500736451_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m895706189_gshared (Enumerator_t2991864267 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m895706189(__this, method) ((  void (*) (Enumerator_t2991864267 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m895706189_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3782872836_gshared (Enumerator_t2991864267 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3782872836(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2991864267 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3782872836_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2330298975_gshared (Enumerator_t2991864267 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2330298975(__this, method) ((  Il2CppObject * (*) (Enumerator_t2991864267 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2330298975_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2787790129_gshared (Enumerator_t2991864267 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2787790129(__this, method) ((  Il2CppObject * (*) (Enumerator_t2991864267 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2787790129_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m856528381_gshared (Enumerator_t2991864267 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m856528381(__this, method) ((  bool (*) (Enumerator_t2991864267 *, const MethodInfo*))Enumerator_MoveNext_m856528381_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t1573321581  Enumerator_get_Current_m348107039_gshared (Enumerator_t2991864267 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m348107039(__this, method) ((  KeyValuePair_2_t1573321581  (*) (Enumerator_t2991864267 *, const MethodInfo*))Enumerator_get_Current_m348107039_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m128279622_gshared (Enumerator_t2991864267 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m128279622(__this, method) ((  int32_t (*) (Enumerator_t2991864267 *, const MethodInfo*))Enumerator_get_CurrentKey_m128279622_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m1546518150_gshared (Enumerator_t2991864267 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1546518150(__this, method) ((  Il2CppObject * (*) (Enumerator_t2991864267 *, const MethodInfo*))Enumerator_get_CurrentValue_m1546518150_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m886364602_gshared (Enumerator_t2991864267 * __this, const MethodInfo* method);
#define Enumerator_Reset_m886364602(__this, method) ((  void (*) (Enumerator_t2991864267 *, const MethodInfo*))Enumerator_Reset_m886364602_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m258504259_gshared (Enumerator_t2991864267 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m258504259(__this, method) ((  void (*) (Enumerator_t2991864267 *, const MethodInfo*))Enumerator_VerifyState_m258504259_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m2059361515_gshared (Enumerator_t2991864267 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m2059361515(__this, method) ((  void (*) (Enumerator_t2991864267 *, const MethodInfo*))Enumerator_VerifyCurrent_m2059361515_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UIModelDisplayType,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3916286986_gshared (Enumerator_t2991864267 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3916286986(__this, method) ((  void (*) (Enumerator_t2991864267 *, const MethodInfo*))Enumerator_Dispose_m3916286986_gshared)(__this, method)
