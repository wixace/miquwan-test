﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RVOAgentPlacer
struct RVOAgentPlacer_t2570441765;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

// System.Void RVOAgentPlacer::.ctor()
extern "C"  void RVOAgentPlacer__ctor_m2026623510 (RVOAgentPlacer_t2570441765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator RVOAgentPlacer::Start()
extern "C"  Il2CppObject * RVOAgentPlacer_Start_m2202033102 (RVOAgentPlacer_t2570441765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color RVOAgentPlacer::GetColor(System.Single)
extern "C"  Color_t4194546905  RVOAgentPlacer_GetColor_m2908081929 (RVOAgentPlacer_t2570441765 * __this, float ___angle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color RVOAgentPlacer::HSVToRGB(System.Single,System.Single,System.Single)
extern "C"  Color_t4194546905  RVOAgentPlacer_HSVToRGB_m1036358585 (Il2CppObject * __this /* static, unused */, float ___h0, float ___s1, float ___v2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
