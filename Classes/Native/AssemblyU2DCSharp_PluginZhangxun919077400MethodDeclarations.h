﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginZhangxun
struct PluginZhangxun_t919077400;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// VersionInfo
struct VersionInfo_t2356638086;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginZhangxun919077400.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void PluginZhangxun::.ctor()
extern "C"  void PluginZhangxun__ctor_m3390219651 (PluginZhangxun_t919077400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangxun::Init()
extern "C"  void PluginZhangxun_Init_m4217318513 (PluginZhangxun_t919077400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginZhangxun::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginZhangxun_ReqSDKHttpLogin_m2506410392 (PluginZhangxun_t919077400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginZhangxun::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginZhangxun_IsLoginSuccess_m2636970352 (PluginZhangxun_t919077400 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangxun::OpenUserLogin()
extern "C"  void PluginZhangxun_OpenUserLogin_m3428553941 (PluginZhangxun_t919077400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangxun::UserPay(CEvent.ZEvent)
extern "C"  void PluginZhangxun_UserPay_m418632957 (PluginZhangxun_t919077400 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangxun::OnEnterGame(CEvent.ZEvent)
extern "C"  void PluginZhangxun_OnEnterGame_m3564669007 (PluginZhangxun_t919077400 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangxun::OnCreateRole(CEvent.ZEvent)
extern "C"  void PluginZhangxun_OnCreateRole_m977255369 (PluginZhangxun_t919077400 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangxun::OnInitSuccess(System.String)
extern "C"  void PluginZhangxun_OnInitSuccess_m2436006413 (PluginZhangxun_t919077400 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangxun::OnInitFail(System.String)
extern "C"  void PluginZhangxun_OnInitFail_m2161635924 (PluginZhangxun_t919077400 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangxun::OnLoginSuccess(System.String)
extern "C"  void PluginZhangxun_OnLoginSuccess_m2891466824 (PluginZhangxun_t919077400 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangxun::OnUserSwitchSuccess(System.String)
extern "C"  void PluginZhangxun_OnUserSwitchSuccess_m2247237468 (PluginZhangxun_t919077400 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangxun::OnLoginFail(System.String)
extern "C"  void PluginZhangxun_OnLoginFail_m1216040441 (PluginZhangxun_t919077400 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangxun::OnPaySuccess(System.String)
extern "C"  void PluginZhangxun_OnPaySuccess_m3552844935 (PluginZhangxun_t919077400 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangxun::OnPayFail(System.String)
extern "C"  void PluginZhangxun_OnPayFail_m3329161754 (PluginZhangxun_t919077400 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangxun::OnExitGameSuccess(System.String)
extern "C"  void PluginZhangxun_OnExitGameSuccess_m793991085 (PluginZhangxun_t919077400 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangxun::OnExitGameFail(System.String)
extern "C"  void PluginZhangxun_OnExitGameFail_m3493855412 (PluginZhangxun_t919077400 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangxun::LogoutSuccess(System.String)
extern "C"  void PluginZhangxun_LogoutSuccess_m2211133896 (PluginZhangxun_t919077400 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangxun::OnLogoutSuccess(System.String)
extern "C"  void PluginZhangxun_OnLogoutSuccess_m3739668295 (PluginZhangxun_t919077400 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangxun::OnLogoutFail(System.String)
extern "C"  void PluginZhangxun_OnLogoutFail_m3150397274 (PluginZhangxun_t919077400 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangxun::InitZhangxun()
extern "C"  void PluginZhangxun_InitZhangxun_m1655441654 (PluginZhangxun_t919077400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangxun::LoginSdk(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginZhangxun_LoginSdk_m265230396 (PluginZhangxun_t919077400 * __this, String_t* ___gameId0, String_t* ___gameName1, String_t* ___gameAppId2, String_t* ___promoteid3, String_t* ___promoteaccount4, String_t* ___gameKey5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangxun::Pay(System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginZhangxun_Pay_m4029009993 (PluginZhangxun_t919077400 * __this, String_t* ___goodsName0, String_t* ___goodsPrice1, String_t* ___goodsDesc2, String_t* ___extra3, String_t* ___productId4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangxun::logout()
extern "C"  void PluginZhangxun_logout_m90778187 (PluginZhangxun_t919077400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangxun::<OnLogoutSuccess>m__482()
extern "C"  void PluginZhangxun_U3COnLogoutSuccessU3Em__482_m4175730914 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginZhangxun::ilo_get_currentVS1(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginZhangxun_ilo_get_currentVS1_m2000841477 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangxun::ilo_InitZhangxun2(PluginZhangxun)
extern "C"  void PluginZhangxun_ilo_InitZhangxun2_m1242295859 (Il2CppObject * __this /* static, unused */, PluginZhangxun_t919077400 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginZhangxun::ilo_get_Instance3()
extern "C"  VersionMgr_t1322950208 * PluginZhangxun_ilo_get_Instance3_m1968986374 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginZhangxun::ilo_get_DeviceID4(PluginsSdkMgr)
extern "C"  String_t* PluginZhangxun_ilo_get_DeviceID4_m2046129967 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginZhangxun::ilo_get_isSdkLogin5(VersionMgr)
extern "C"  bool PluginZhangxun_ilo_get_isSdkLogin5_m3094296589 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.PayInfo PluginZhangxun::ilo_Parse6(System.Object[])
extern "C"  PayInfo_t1775308120 * PluginZhangxun_ilo_Parse6_m3915675188 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___payOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProductsCfgMgr PluginZhangxun::ilo_get_ProductsCfgMgr7()
extern "C"  ProductsCfgMgr_t2493714872 * PluginZhangxun_ilo_get_ProductsCfgMgr7_m1074043615 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginZhangxun::ilo_GetProductID8(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginZhangxun_ilo_GetProductID8_m4002061299 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangxun::ilo_Log9(System.Object,System.Boolean)
extern "C"  void PluginZhangxun_ilo_Log9_m4069921650 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangxun::ilo_DispatchEvent10(CEvent.ZEvent)
extern "C"  void PluginZhangxun_ilo_DispatchEvent10_m2091558958 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
