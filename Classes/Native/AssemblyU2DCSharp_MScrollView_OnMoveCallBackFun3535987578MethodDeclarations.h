﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MScrollView/OnMoveCallBackFun
struct OnMoveCallBackFun_t3535987578;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MScrollView/OnMoveCallBackFun::.ctor(System.Object,System.IntPtr)
extern "C"  void OnMoveCallBackFun__ctor_m609824337 (OnMoveCallBackFun_t3535987578 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MScrollView/OnMoveCallBackFun::Invoke()
extern "C"  bool OnMoveCallBackFun_Invoke_m1701727415 (OnMoveCallBackFun_t3535987578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MScrollView/OnMoveCallBackFun::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnMoveCallBackFun_BeginInvoke_m3862238272 (OnMoveCallBackFun_t3535987578 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MScrollView/OnMoveCallBackFun::EndInvoke(System.IAsyncResult)
extern "C"  bool OnMoveCallBackFun_EndInvoke_m2188754605 (OnMoveCallBackFun_t3535987578 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
