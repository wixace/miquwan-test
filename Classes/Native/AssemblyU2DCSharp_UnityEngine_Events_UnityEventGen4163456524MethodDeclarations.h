﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Events_UnityEventGenerated
struct UnityEngine_Events_UnityEventGenerated_t4163456524;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Events.UnityAction
struct UnityAction_t594794173;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_Events_UnityEventGenerated::.ctor()
extern "C"  void UnityEngine_Events_UnityEventGenerated__ctor_m133219343 (UnityEngine_Events_UnityEventGenerated_t4163456524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Events_UnityEventGenerated::UnityEvent_UnityEvent1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Events_UnityEventGenerated_UnityEvent_UnityEvent1_m1714308875 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.UnityAction UnityEngine_Events_UnityEventGenerated::UnityEvent_AddListener_GetDelegate_member0_arg0(CSRepresentedObject)
extern "C"  UnityAction_t594794173 * UnityEngine_Events_UnityEventGenerated_UnityEvent_AddListener_GetDelegate_member0_arg0_m3034857140 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Events_UnityEventGenerated::UnityEvent_AddListener__UnityAction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Events_UnityEventGenerated_UnityEvent_AddListener__UnityAction_m275151033 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Events_UnityEventGenerated::UnityEvent_Invoke(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Events_UnityEventGenerated_UnityEvent_Invoke_m4136090747 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.UnityAction UnityEngine_Events_UnityEventGenerated::UnityEvent_RemoveListener_GetDelegate_member2_arg0(CSRepresentedObject)
extern "C"  UnityAction_t594794173 * UnityEngine_Events_UnityEventGenerated_UnityEvent_RemoveListener_GetDelegate_member2_arg0_m3722339141 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Events_UnityEventGenerated::UnityEvent_RemoveListener__UnityAction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Events_UnityEventGenerated_UnityEvent_RemoveListener__UnityAction_m1449392850 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Events_UnityEventGenerated::__Register()
extern "C"  void UnityEngine_Events_UnityEventGenerated___Register_m1663298072 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.UnityAction UnityEngine_Events_UnityEventGenerated::<UnityEvent_AddListener__UnityAction>m__1B2()
extern "C"  UnityAction_t594794173 * UnityEngine_Events_UnityEventGenerated_U3CUnityEvent_AddListener__UnityActionU3Em__1B2_m262719131 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.UnityAction UnityEngine_Events_UnityEventGenerated::<UnityEvent_RemoveListener__UnityAction>m__1B4()
extern "C"  UnityAction_t594794173 * UnityEngine_Events_UnityEventGenerated_U3CUnityEvent_RemoveListener__UnityActionU3Em__1B4_m2473511748 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Events_UnityEventGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_Events_UnityEventGenerated_ilo_getObject1_m2545748963 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject UnityEngine_Events_UnityEventGenerated::ilo_getFunctionS2(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * UnityEngine_Events_UnityEventGenerated_ilo_getFunctionS2_m3726872507 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_Events_UnityEventGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_Events_UnityEventGenerated_ilo_getObject3_m1008062786 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Events_UnityEventGenerated::ilo_isFunctionS4(System.Int32)
extern "C"  bool UnityEngine_Events_UnityEventGenerated_ilo_isFunctionS4_m3737872080 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
