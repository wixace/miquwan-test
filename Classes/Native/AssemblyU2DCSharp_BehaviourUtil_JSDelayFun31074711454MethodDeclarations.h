﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/JSDelayFun3
struct JSDelayFun3_t1074711454;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void BehaviourUtil/JSDelayFun3::.ctor(System.Object,System.IntPtr)
extern "C"  void JSDelayFun3__ctor_m1260494965 (JSDelayFun3_t1074711454 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil/JSDelayFun3::Invoke(System.Object,System.Object,System.Object)
extern "C"  void JSDelayFun3_Invoke_m608712833 (JSDelayFun3_t1074711454 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult BehaviourUtil/JSDelayFun3::BeginInvoke(System.Object,System.Object,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * JSDelayFun3_BeginInvoke_m3059720946 (JSDelayFun3_t1074711454 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil/JSDelayFun3::EndInvoke(System.IAsyncResult)
extern "C"  void JSDelayFun3_EndInvoke_m2044302597 (JSDelayFun3_t1074711454 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
