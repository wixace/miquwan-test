﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_kouhoPasfear184
struct M_kouhoPasfear184_t3663863955;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_kouhoPasfear184::.ctor()
extern "C"  void M_kouhoPasfear184__ctor_m3512173616 (M_kouhoPasfear184_t3663863955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kouhoPasfear184::M_kerca0(System.String[],System.Int32)
extern "C"  void M_kouhoPasfear184_M_kerca0_m2742221847 (M_kouhoPasfear184_t3663863955 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kouhoPasfear184::M_jorneekurSagem1(System.String[],System.Int32)
extern "C"  void M_kouhoPasfear184_M_jorneekurSagem1_m2278337642 (M_kouhoPasfear184_t3663863955 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
