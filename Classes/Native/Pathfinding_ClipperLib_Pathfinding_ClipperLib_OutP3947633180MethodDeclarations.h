﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.ClipperLib.OutPt
struct OutPt_t3947633180;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.ClipperLib.OutPt::.ctor()
extern "C"  void OutPt__ctor_m1159254633 (OutPt_t3947633180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
