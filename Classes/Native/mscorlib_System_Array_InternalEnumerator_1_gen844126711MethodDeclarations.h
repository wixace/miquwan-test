﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen844126711.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22061784035.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1840769089_gshared (InternalEnumerator_1_t844126711 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1840769089(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t844126711 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1840769089_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1469365183_gshared (InternalEnumerator_1_t844126711 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1469365183(__this, method) ((  void (*) (InternalEnumerator_1_t844126711 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1469365183_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m236183915_gshared (InternalEnumerator_1_t844126711 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m236183915(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t844126711 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m236183915_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2309857112_gshared (InternalEnumerator_1_t844126711 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2309857112(__this, method) ((  void (*) (InternalEnumerator_1_t844126711 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2309857112_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1883283883_gshared (InternalEnumerator_1_t844126711 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1883283883(__this, method) ((  bool (*) (InternalEnumerator_1_t844126711 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1883283883_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>>::get_Current()
extern "C"  KeyValuePair_2_t2061784035  InternalEnumerator_1_get_Current_m627317576_gshared (InternalEnumerator_1_t844126711 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m627317576(__this, method) ((  KeyValuePair_2_t2061784035  (*) (InternalEnumerator_1_t844126711 *, const MethodInfo*))InternalEnumerator_1_get_Current_m627317576_gshared)(__this, method)
