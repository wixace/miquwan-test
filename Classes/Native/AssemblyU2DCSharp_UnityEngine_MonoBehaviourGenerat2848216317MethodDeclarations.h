﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_MonoBehaviourGenerated
struct UnityEngine_MonoBehaviourGenerated_t2848216317;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_MonoBehaviourGenerated::.ctor()
extern "C"  void UnityEngine_MonoBehaviourGenerated__ctor_m3398551614 (UnityEngine_MonoBehaviourGenerated_t2848216317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MonoBehaviourGenerated::MonoBehaviour_useGUILayout(JSVCall)
extern "C"  void UnityEngine_MonoBehaviourGenerated_MonoBehaviour_useGUILayout_m3726384446 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MonoBehaviourGenerated::MonoBehaviour_CancelInvoke__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MonoBehaviourGenerated_MonoBehaviour_CancelInvoke__String_m2760089746 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MonoBehaviourGenerated::MonoBehaviour_CancelInvoke(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MonoBehaviourGenerated_MonoBehaviour_CancelInvoke_m1844949825 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MonoBehaviourGenerated::MonoBehaviour_Invoke__String__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MonoBehaviourGenerated_MonoBehaviour_Invoke__String__Single_m3665557696 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MonoBehaviourGenerated::MonoBehaviour_InvokeRepeating__String__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MonoBehaviourGenerated_MonoBehaviour_InvokeRepeating__String__Single__Single_m325000867 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MonoBehaviourGenerated::MonoBehaviour_IsInvoking__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MonoBehaviourGenerated_MonoBehaviour_IsInvoking__String_m1407722047 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MonoBehaviourGenerated::MonoBehaviour_IsInvoking(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MonoBehaviourGenerated_MonoBehaviour_IsInvoking_m1870344686 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MonoBehaviourGenerated::MonoBehaviour_StartCoroutine__String__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MonoBehaviourGenerated_MonoBehaviour_StartCoroutine__String__Object_m2804258837 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MonoBehaviourGenerated::MonoBehaviour_StartCoroutine__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MonoBehaviourGenerated_MonoBehaviour_StartCoroutine__String_m1261945622 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MonoBehaviourGenerated::MonoBehaviour_StartCoroutine__IEnumerator(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MonoBehaviourGenerated_MonoBehaviour_StartCoroutine__IEnumerator_m2557205034 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MonoBehaviourGenerated::MonoBehaviour_StartCoroutine_Auto__IEnumerator(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MonoBehaviourGenerated_MonoBehaviour_StartCoroutine_Auto__IEnumerator_m3960133796 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MonoBehaviourGenerated::MonoBehaviour_StopAllCoroutines(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MonoBehaviourGenerated_MonoBehaviour_StopAllCoroutines_m161853549 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MonoBehaviourGenerated::MonoBehaviour_StopCoroutine__Coroutine(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MonoBehaviourGenerated_MonoBehaviour_StopCoroutine__Coroutine_m3292229329 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MonoBehaviourGenerated::MonoBehaviour_StopCoroutine__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MonoBehaviourGenerated_MonoBehaviour_StopCoroutine__String_m1230064250 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MonoBehaviourGenerated::MonoBehaviour_StopCoroutine__IEnumerator(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MonoBehaviourGenerated_MonoBehaviour_StopCoroutine__IEnumerator_m2329104710 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MonoBehaviourGenerated::MonoBehaviour_print__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MonoBehaviourGenerated_MonoBehaviour_print__Object_m1200221375 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MonoBehaviourGenerated::__Register()
extern "C"  void UnityEngine_MonoBehaviourGenerated___Register_m1085877065 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_MonoBehaviourGenerated::ilo_getSingle1(System.Int32)
extern "C"  float UnityEngine_MonoBehaviourGenerated_ilo_getSingle1_m3653327017 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_MonoBehaviourGenerated::ilo_getStringS2(System.Int32)
extern "C"  String_t* UnityEngine_MonoBehaviourGenerated_ilo_getStringS2_m3945319293 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MonoBehaviourGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void UnityEngine_MonoBehaviourGenerated_ilo_setBooleanS3_m383804269 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_MonoBehaviourGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_MonoBehaviourGenerated_ilo_setObject4_m1868972111 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_MonoBehaviourGenerated::ilo_getObject5(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_MonoBehaviourGenerated_ilo_getObject5_m1655095477 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
