﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// JSEngine
struct JSEngine_t2847479019;
// JSFileLoader
struct JSFileLoader_t1433707224;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t844452154;
// System.Object
struct Il2CppObject;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSEngine
struct  JSEngine_t2847479019  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean JSEngine::JSC
	bool ___JSC_4;
	// System.Boolean JSEngine::debug
	bool ___debug_5;
	// System.Int32 JSEngine::port
	int32_t ___port_6;
	// System.Boolean JSEngine::mDebug
	bool ___mDebug_7;
	// System.Single JSEngine::GCInterval
	float ___GCInterval_8;
	// JSFileLoader JSEngine::jsLoader
	JSFileLoader_t1433707224 * ___jsLoader_9;
	// System.String[] JSEngine::InitLoadScripts
	StringU5BU5D_t4054002952* ___InitLoadScripts_10;
	// System.Boolean JSEngine::bSuccess
	bool ___bSuccess_11;
	// System.Int32 JSEngine::totalCount
	int32_t ___totalCount_12;
	// System.Int32 JSEngine::jsCallCountPerFrame
	int32_t ___jsCallCountPerFrame_13;
	// System.Collections.Generic.List`1<System.Action> JSEngine::lstThreadSafeActions
	List_1_t844452154 * ___lstThreadSafeActions_14;
	// System.Boolean JSEngine::hasThreadSafeActions
	bool ___hasThreadSafeActions_15;
	// System.Object JSEngine::lock
	Il2CppObject * ___lock_16;
	// System.Single JSEngine::accum
	float ___accum_17;
	// System.Boolean JSEngine::showStatistics
	bool ___showStatistics_18;
	// System.Int32 JSEngine::guiX
	int32_t ___guiX_19;

public:
	inline static int32_t get_offset_of_JSC_4() { return static_cast<int32_t>(offsetof(JSEngine_t2847479019, ___JSC_4)); }
	inline bool get_JSC_4() const { return ___JSC_4; }
	inline bool* get_address_of_JSC_4() { return &___JSC_4; }
	inline void set_JSC_4(bool value)
	{
		___JSC_4 = value;
	}

	inline static int32_t get_offset_of_debug_5() { return static_cast<int32_t>(offsetof(JSEngine_t2847479019, ___debug_5)); }
	inline bool get_debug_5() const { return ___debug_5; }
	inline bool* get_address_of_debug_5() { return &___debug_5; }
	inline void set_debug_5(bool value)
	{
		___debug_5 = value;
	}

	inline static int32_t get_offset_of_port_6() { return static_cast<int32_t>(offsetof(JSEngine_t2847479019, ___port_6)); }
	inline int32_t get_port_6() const { return ___port_6; }
	inline int32_t* get_address_of_port_6() { return &___port_6; }
	inline void set_port_6(int32_t value)
	{
		___port_6 = value;
	}

	inline static int32_t get_offset_of_mDebug_7() { return static_cast<int32_t>(offsetof(JSEngine_t2847479019, ___mDebug_7)); }
	inline bool get_mDebug_7() const { return ___mDebug_7; }
	inline bool* get_address_of_mDebug_7() { return &___mDebug_7; }
	inline void set_mDebug_7(bool value)
	{
		___mDebug_7 = value;
	}

	inline static int32_t get_offset_of_GCInterval_8() { return static_cast<int32_t>(offsetof(JSEngine_t2847479019, ___GCInterval_8)); }
	inline float get_GCInterval_8() const { return ___GCInterval_8; }
	inline float* get_address_of_GCInterval_8() { return &___GCInterval_8; }
	inline void set_GCInterval_8(float value)
	{
		___GCInterval_8 = value;
	}

	inline static int32_t get_offset_of_jsLoader_9() { return static_cast<int32_t>(offsetof(JSEngine_t2847479019, ___jsLoader_9)); }
	inline JSFileLoader_t1433707224 * get_jsLoader_9() const { return ___jsLoader_9; }
	inline JSFileLoader_t1433707224 ** get_address_of_jsLoader_9() { return &___jsLoader_9; }
	inline void set_jsLoader_9(JSFileLoader_t1433707224 * value)
	{
		___jsLoader_9 = value;
		Il2CppCodeGenWriteBarrier(&___jsLoader_9, value);
	}

	inline static int32_t get_offset_of_InitLoadScripts_10() { return static_cast<int32_t>(offsetof(JSEngine_t2847479019, ___InitLoadScripts_10)); }
	inline StringU5BU5D_t4054002952* get_InitLoadScripts_10() const { return ___InitLoadScripts_10; }
	inline StringU5BU5D_t4054002952** get_address_of_InitLoadScripts_10() { return &___InitLoadScripts_10; }
	inline void set_InitLoadScripts_10(StringU5BU5D_t4054002952* value)
	{
		___InitLoadScripts_10 = value;
		Il2CppCodeGenWriteBarrier(&___InitLoadScripts_10, value);
	}

	inline static int32_t get_offset_of_bSuccess_11() { return static_cast<int32_t>(offsetof(JSEngine_t2847479019, ___bSuccess_11)); }
	inline bool get_bSuccess_11() const { return ___bSuccess_11; }
	inline bool* get_address_of_bSuccess_11() { return &___bSuccess_11; }
	inline void set_bSuccess_11(bool value)
	{
		___bSuccess_11 = value;
	}

	inline static int32_t get_offset_of_totalCount_12() { return static_cast<int32_t>(offsetof(JSEngine_t2847479019, ___totalCount_12)); }
	inline int32_t get_totalCount_12() const { return ___totalCount_12; }
	inline int32_t* get_address_of_totalCount_12() { return &___totalCount_12; }
	inline void set_totalCount_12(int32_t value)
	{
		___totalCount_12 = value;
	}

	inline static int32_t get_offset_of_jsCallCountPerFrame_13() { return static_cast<int32_t>(offsetof(JSEngine_t2847479019, ___jsCallCountPerFrame_13)); }
	inline int32_t get_jsCallCountPerFrame_13() const { return ___jsCallCountPerFrame_13; }
	inline int32_t* get_address_of_jsCallCountPerFrame_13() { return &___jsCallCountPerFrame_13; }
	inline void set_jsCallCountPerFrame_13(int32_t value)
	{
		___jsCallCountPerFrame_13 = value;
	}

	inline static int32_t get_offset_of_lstThreadSafeActions_14() { return static_cast<int32_t>(offsetof(JSEngine_t2847479019, ___lstThreadSafeActions_14)); }
	inline List_1_t844452154 * get_lstThreadSafeActions_14() const { return ___lstThreadSafeActions_14; }
	inline List_1_t844452154 ** get_address_of_lstThreadSafeActions_14() { return &___lstThreadSafeActions_14; }
	inline void set_lstThreadSafeActions_14(List_1_t844452154 * value)
	{
		___lstThreadSafeActions_14 = value;
		Il2CppCodeGenWriteBarrier(&___lstThreadSafeActions_14, value);
	}

	inline static int32_t get_offset_of_hasThreadSafeActions_15() { return static_cast<int32_t>(offsetof(JSEngine_t2847479019, ___hasThreadSafeActions_15)); }
	inline bool get_hasThreadSafeActions_15() const { return ___hasThreadSafeActions_15; }
	inline bool* get_address_of_hasThreadSafeActions_15() { return &___hasThreadSafeActions_15; }
	inline void set_hasThreadSafeActions_15(bool value)
	{
		___hasThreadSafeActions_15 = value;
	}

	inline static int32_t get_offset_of_lock_16() { return static_cast<int32_t>(offsetof(JSEngine_t2847479019, ___lock_16)); }
	inline Il2CppObject * get_lock_16() const { return ___lock_16; }
	inline Il2CppObject ** get_address_of_lock_16() { return &___lock_16; }
	inline void set_lock_16(Il2CppObject * value)
	{
		___lock_16 = value;
		Il2CppCodeGenWriteBarrier(&___lock_16, value);
	}

	inline static int32_t get_offset_of_accum_17() { return static_cast<int32_t>(offsetof(JSEngine_t2847479019, ___accum_17)); }
	inline float get_accum_17() const { return ___accum_17; }
	inline float* get_address_of_accum_17() { return &___accum_17; }
	inline void set_accum_17(float value)
	{
		___accum_17 = value;
	}

	inline static int32_t get_offset_of_showStatistics_18() { return static_cast<int32_t>(offsetof(JSEngine_t2847479019, ___showStatistics_18)); }
	inline bool get_showStatistics_18() const { return ___showStatistics_18; }
	inline bool* get_address_of_showStatistics_18() { return &___showStatistics_18; }
	inline void set_showStatistics_18(bool value)
	{
		___showStatistics_18 = value;
	}

	inline static int32_t get_offset_of_guiX_19() { return static_cast<int32_t>(offsetof(JSEngine_t2847479019, ___guiX_19)); }
	inline int32_t get_guiX_19() const { return ___guiX_19; }
	inline int32_t* get_address_of_guiX_19() { return &___guiX_19; }
	inline void set_guiX_19(int32_t value)
	{
		___guiX_19 = value;
	}
};

struct JSEngine_t2847479019_StaticFields
{
public:
	// JSEngine JSEngine::inst
	JSEngine_t2847479019 * ___inst_2;
	// System.Int32 JSEngine::initState
	int32_t ___initState_3;

public:
	inline static int32_t get_offset_of_inst_2() { return static_cast<int32_t>(offsetof(JSEngine_t2847479019_StaticFields, ___inst_2)); }
	inline JSEngine_t2847479019 * get_inst_2() const { return ___inst_2; }
	inline JSEngine_t2847479019 ** get_address_of_inst_2() { return &___inst_2; }
	inline void set_inst_2(JSEngine_t2847479019 * value)
	{
		___inst_2 = value;
		Il2CppCodeGenWriteBarrier(&___inst_2, value);
	}

	inline static int32_t get_offset_of_initState_3() { return static_cast<int32_t>(offsetof(JSEngine_t2847479019_StaticFields, ___initState_3)); }
	inline int32_t get_initState_3() const { return ___initState_3; }
	inline int32_t* get_address_of_initState_3() { return &___initState_3; }
	inline void set_initState_3(int32_t value)
	{
		___initState_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
