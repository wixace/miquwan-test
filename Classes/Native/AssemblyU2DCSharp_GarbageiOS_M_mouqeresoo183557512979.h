﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_mouqeresoo183
struct  M_mouqeresoo183_t557512979  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_mouqeresoo183::_yewaMitrereji
	bool ____yewaMitrereji_0;
	// System.String GarbageiOS.M_mouqeresoo183::_xefere
	String_t* ____xefere_1;
	// System.Single GarbageiOS.M_mouqeresoo183::_taiwipear
	float ____taiwipear_2;
	// System.Boolean GarbageiOS.M_mouqeresoo183::_dragoovaKaso
	bool ____dragoovaKaso_3;
	// System.Int32 GarbageiOS.M_mouqeresoo183::_calchitre
	int32_t ____calchitre_4;
	// System.Boolean GarbageiOS.M_mouqeresoo183::_naylordel
	bool ____naylordel_5;
	// System.String GarbageiOS.M_mouqeresoo183::_nearseraw
	String_t* ____nearseraw_6;
	// System.String GarbageiOS.M_mouqeresoo183::_zoocerWemsawkal
	String_t* ____zoocerWemsawkal_7;
	// System.Single GarbageiOS.M_mouqeresoo183::_bishayxu
	float ____bishayxu_8;
	// System.Boolean GarbageiOS.M_mouqeresoo183::_bouvowtrel
	bool ____bouvowtrel_9;
	// System.UInt32 GarbageiOS.M_mouqeresoo183::_vouto
	uint32_t ____vouto_10;
	// System.UInt32 GarbageiOS.M_mouqeresoo183::_trartair
	uint32_t ____trartair_11;
	// System.UInt32 GarbageiOS.M_mouqeresoo183::_troubelyearJaceedea
	uint32_t ____troubelyearJaceedea_12;
	// System.String GarbageiOS.M_mouqeresoo183::_temtowpi
	String_t* ____temtowpi_13;
	// System.Int32 GarbageiOS.M_mouqeresoo183::_cowjesePurdi
	int32_t ____cowjesePurdi_14;
	// System.UInt32 GarbageiOS.M_mouqeresoo183::_teremis
	uint32_t ____teremis_15;
	// System.Int32 GarbageiOS.M_mouqeresoo183::_sarsooWaroci
	int32_t ____sarsooWaroci_16;
	// System.String GarbageiOS.M_mouqeresoo183::_gawnaype
	String_t* ____gawnaype_17;
	// System.Single GarbageiOS.M_mouqeresoo183::_jeajemgaCairmi
	float ____jeajemgaCairmi_18;
	// System.Int32 GarbageiOS.M_mouqeresoo183::_valkor
	int32_t ____valkor_19;
	// System.Int32 GarbageiOS.M_mouqeresoo183::_jearbiwhawHeljairjou
	int32_t ____jearbiwhawHeljairjou_20;
	// System.Single GarbageiOS.M_mouqeresoo183::_dirayCeelani
	float ____dirayCeelani_21;

public:
	inline static int32_t get_offset_of__yewaMitrereji_0() { return static_cast<int32_t>(offsetof(M_mouqeresoo183_t557512979, ____yewaMitrereji_0)); }
	inline bool get__yewaMitrereji_0() const { return ____yewaMitrereji_0; }
	inline bool* get_address_of__yewaMitrereji_0() { return &____yewaMitrereji_0; }
	inline void set__yewaMitrereji_0(bool value)
	{
		____yewaMitrereji_0 = value;
	}

	inline static int32_t get_offset_of__xefere_1() { return static_cast<int32_t>(offsetof(M_mouqeresoo183_t557512979, ____xefere_1)); }
	inline String_t* get__xefere_1() const { return ____xefere_1; }
	inline String_t** get_address_of__xefere_1() { return &____xefere_1; }
	inline void set__xefere_1(String_t* value)
	{
		____xefere_1 = value;
		Il2CppCodeGenWriteBarrier(&____xefere_1, value);
	}

	inline static int32_t get_offset_of__taiwipear_2() { return static_cast<int32_t>(offsetof(M_mouqeresoo183_t557512979, ____taiwipear_2)); }
	inline float get__taiwipear_2() const { return ____taiwipear_2; }
	inline float* get_address_of__taiwipear_2() { return &____taiwipear_2; }
	inline void set__taiwipear_2(float value)
	{
		____taiwipear_2 = value;
	}

	inline static int32_t get_offset_of__dragoovaKaso_3() { return static_cast<int32_t>(offsetof(M_mouqeresoo183_t557512979, ____dragoovaKaso_3)); }
	inline bool get__dragoovaKaso_3() const { return ____dragoovaKaso_3; }
	inline bool* get_address_of__dragoovaKaso_3() { return &____dragoovaKaso_3; }
	inline void set__dragoovaKaso_3(bool value)
	{
		____dragoovaKaso_3 = value;
	}

	inline static int32_t get_offset_of__calchitre_4() { return static_cast<int32_t>(offsetof(M_mouqeresoo183_t557512979, ____calchitre_4)); }
	inline int32_t get__calchitre_4() const { return ____calchitre_4; }
	inline int32_t* get_address_of__calchitre_4() { return &____calchitre_4; }
	inline void set__calchitre_4(int32_t value)
	{
		____calchitre_4 = value;
	}

	inline static int32_t get_offset_of__naylordel_5() { return static_cast<int32_t>(offsetof(M_mouqeresoo183_t557512979, ____naylordel_5)); }
	inline bool get__naylordel_5() const { return ____naylordel_5; }
	inline bool* get_address_of__naylordel_5() { return &____naylordel_5; }
	inline void set__naylordel_5(bool value)
	{
		____naylordel_5 = value;
	}

	inline static int32_t get_offset_of__nearseraw_6() { return static_cast<int32_t>(offsetof(M_mouqeresoo183_t557512979, ____nearseraw_6)); }
	inline String_t* get__nearseraw_6() const { return ____nearseraw_6; }
	inline String_t** get_address_of__nearseraw_6() { return &____nearseraw_6; }
	inline void set__nearseraw_6(String_t* value)
	{
		____nearseraw_6 = value;
		Il2CppCodeGenWriteBarrier(&____nearseraw_6, value);
	}

	inline static int32_t get_offset_of__zoocerWemsawkal_7() { return static_cast<int32_t>(offsetof(M_mouqeresoo183_t557512979, ____zoocerWemsawkal_7)); }
	inline String_t* get__zoocerWemsawkal_7() const { return ____zoocerWemsawkal_7; }
	inline String_t** get_address_of__zoocerWemsawkal_7() { return &____zoocerWemsawkal_7; }
	inline void set__zoocerWemsawkal_7(String_t* value)
	{
		____zoocerWemsawkal_7 = value;
		Il2CppCodeGenWriteBarrier(&____zoocerWemsawkal_7, value);
	}

	inline static int32_t get_offset_of__bishayxu_8() { return static_cast<int32_t>(offsetof(M_mouqeresoo183_t557512979, ____bishayxu_8)); }
	inline float get__bishayxu_8() const { return ____bishayxu_8; }
	inline float* get_address_of__bishayxu_8() { return &____bishayxu_8; }
	inline void set__bishayxu_8(float value)
	{
		____bishayxu_8 = value;
	}

	inline static int32_t get_offset_of__bouvowtrel_9() { return static_cast<int32_t>(offsetof(M_mouqeresoo183_t557512979, ____bouvowtrel_9)); }
	inline bool get__bouvowtrel_9() const { return ____bouvowtrel_9; }
	inline bool* get_address_of__bouvowtrel_9() { return &____bouvowtrel_9; }
	inline void set__bouvowtrel_9(bool value)
	{
		____bouvowtrel_9 = value;
	}

	inline static int32_t get_offset_of__vouto_10() { return static_cast<int32_t>(offsetof(M_mouqeresoo183_t557512979, ____vouto_10)); }
	inline uint32_t get__vouto_10() const { return ____vouto_10; }
	inline uint32_t* get_address_of__vouto_10() { return &____vouto_10; }
	inline void set__vouto_10(uint32_t value)
	{
		____vouto_10 = value;
	}

	inline static int32_t get_offset_of__trartair_11() { return static_cast<int32_t>(offsetof(M_mouqeresoo183_t557512979, ____trartair_11)); }
	inline uint32_t get__trartair_11() const { return ____trartair_11; }
	inline uint32_t* get_address_of__trartair_11() { return &____trartair_11; }
	inline void set__trartair_11(uint32_t value)
	{
		____trartair_11 = value;
	}

	inline static int32_t get_offset_of__troubelyearJaceedea_12() { return static_cast<int32_t>(offsetof(M_mouqeresoo183_t557512979, ____troubelyearJaceedea_12)); }
	inline uint32_t get__troubelyearJaceedea_12() const { return ____troubelyearJaceedea_12; }
	inline uint32_t* get_address_of__troubelyearJaceedea_12() { return &____troubelyearJaceedea_12; }
	inline void set__troubelyearJaceedea_12(uint32_t value)
	{
		____troubelyearJaceedea_12 = value;
	}

	inline static int32_t get_offset_of__temtowpi_13() { return static_cast<int32_t>(offsetof(M_mouqeresoo183_t557512979, ____temtowpi_13)); }
	inline String_t* get__temtowpi_13() const { return ____temtowpi_13; }
	inline String_t** get_address_of__temtowpi_13() { return &____temtowpi_13; }
	inline void set__temtowpi_13(String_t* value)
	{
		____temtowpi_13 = value;
		Il2CppCodeGenWriteBarrier(&____temtowpi_13, value);
	}

	inline static int32_t get_offset_of__cowjesePurdi_14() { return static_cast<int32_t>(offsetof(M_mouqeresoo183_t557512979, ____cowjesePurdi_14)); }
	inline int32_t get__cowjesePurdi_14() const { return ____cowjesePurdi_14; }
	inline int32_t* get_address_of__cowjesePurdi_14() { return &____cowjesePurdi_14; }
	inline void set__cowjesePurdi_14(int32_t value)
	{
		____cowjesePurdi_14 = value;
	}

	inline static int32_t get_offset_of__teremis_15() { return static_cast<int32_t>(offsetof(M_mouqeresoo183_t557512979, ____teremis_15)); }
	inline uint32_t get__teremis_15() const { return ____teremis_15; }
	inline uint32_t* get_address_of__teremis_15() { return &____teremis_15; }
	inline void set__teremis_15(uint32_t value)
	{
		____teremis_15 = value;
	}

	inline static int32_t get_offset_of__sarsooWaroci_16() { return static_cast<int32_t>(offsetof(M_mouqeresoo183_t557512979, ____sarsooWaroci_16)); }
	inline int32_t get__sarsooWaroci_16() const { return ____sarsooWaroci_16; }
	inline int32_t* get_address_of__sarsooWaroci_16() { return &____sarsooWaroci_16; }
	inline void set__sarsooWaroci_16(int32_t value)
	{
		____sarsooWaroci_16 = value;
	}

	inline static int32_t get_offset_of__gawnaype_17() { return static_cast<int32_t>(offsetof(M_mouqeresoo183_t557512979, ____gawnaype_17)); }
	inline String_t* get__gawnaype_17() const { return ____gawnaype_17; }
	inline String_t** get_address_of__gawnaype_17() { return &____gawnaype_17; }
	inline void set__gawnaype_17(String_t* value)
	{
		____gawnaype_17 = value;
		Il2CppCodeGenWriteBarrier(&____gawnaype_17, value);
	}

	inline static int32_t get_offset_of__jeajemgaCairmi_18() { return static_cast<int32_t>(offsetof(M_mouqeresoo183_t557512979, ____jeajemgaCairmi_18)); }
	inline float get__jeajemgaCairmi_18() const { return ____jeajemgaCairmi_18; }
	inline float* get_address_of__jeajemgaCairmi_18() { return &____jeajemgaCairmi_18; }
	inline void set__jeajemgaCairmi_18(float value)
	{
		____jeajemgaCairmi_18 = value;
	}

	inline static int32_t get_offset_of__valkor_19() { return static_cast<int32_t>(offsetof(M_mouqeresoo183_t557512979, ____valkor_19)); }
	inline int32_t get__valkor_19() const { return ____valkor_19; }
	inline int32_t* get_address_of__valkor_19() { return &____valkor_19; }
	inline void set__valkor_19(int32_t value)
	{
		____valkor_19 = value;
	}

	inline static int32_t get_offset_of__jearbiwhawHeljairjou_20() { return static_cast<int32_t>(offsetof(M_mouqeresoo183_t557512979, ____jearbiwhawHeljairjou_20)); }
	inline int32_t get__jearbiwhawHeljairjou_20() const { return ____jearbiwhawHeljairjou_20; }
	inline int32_t* get_address_of__jearbiwhawHeljairjou_20() { return &____jearbiwhawHeljairjou_20; }
	inline void set__jearbiwhawHeljairjou_20(int32_t value)
	{
		____jearbiwhawHeljairjou_20 = value;
	}

	inline static int32_t get_offset_of__dirayCeelani_21() { return static_cast<int32_t>(offsetof(M_mouqeresoo183_t557512979, ____dirayCeelani_21)); }
	inline float get__dirayCeelani_21() const { return ____dirayCeelani_21; }
	inline float* get_address_of__dirayCeelani_21() { return &____dirayCeelani_21; }
	inline void set__dirayCeelani_21(float value)
	{
		____dirayCeelani_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
