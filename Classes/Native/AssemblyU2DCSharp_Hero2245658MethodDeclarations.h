﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Hero
struct Hero_t2245658;
// AICtrl
struct AICtrl_t1930422963;
// CombatEntity
struct CombatEntity_t684137495;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// CSHeroUnit
struct CSHeroUnit_t3764358446;
// herosCfg
struct herosCfg_t3676934635;
// JSCLevelHeroPointConfig
struct JSCLevelHeroPointConfig_t561690542;
// CSSkillData[]
struct CSSkillDataU5BU5D_t1071816810;
// System.Action
struct Action_t3771233898;
// System.Collections.Generic.List`1<AIObject>
struct List_1_t1405465687;
// GameMgr
struct GameMgr_t1469029542;
// FightCtrl
struct FightCtrl_t648967803;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;
// CombatAttPlus
struct CombatAttPlus_t649400871;
// CSCheckPointUnit
struct CSCheckPointUnit_t3129216924;
// System.Object
struct Il2CppObject;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// HeroMgr
struct HeroMgr_t2475965342;
// System.UInt32[]
struct UInt32U5BU5D_t3230734560;
// NpcMgr
struct NpcMgr_t2339534743;
// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;
// AIPath
struct AIPath_t1930792045;
// AnimationRunner
struct AnimationRunner_t1015409588;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// BuffCtrl
struct BuffCtrl_t2836564350;
// ReplayMgr
struct ReplayMgr_t1549183121;
// skillCfg
struct skillCfg_t2142425171;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// CSGameDataMgr
struct CSGameDataMgr_t2623305516;
// System.Collections.Generic.List`1<CSPlusAtt>
struct List_1_t341533415;
// CSFightforData
struct CSFightforData_t215334547;
// CSCampfightData
struct CSCampfightData_t179152873;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AICtrl1930422963.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_CSHeroUnit3764358446.h"
#include "AssemblyU2DCSharp_herosCfg3676934635.h"
#include "AssemblyU2DCSharp_JSCLevelHeroPointConfig561690542.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitType643359572.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"
#include "AssemblyU2DCSharp_Hero2245658.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_SEX_TYPE2342271891.h"
#include "AssemblyU2DCSharp_HERO_TYPE820012127.h"
#include "AssemblyU2DCSharp_HERO_ELEMENT2692579223.h"
#include "AssemblyU2DCSharp_CombatAttPlus649400871.h"
#include "AssemblyU2DCSharp_HERO_ATT_KEY3680959548.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitTag299076696.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitCamp642829979.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "AssemblyU2DCSharp_FightCtrl648967803.h"
#include "AssemblyU2DCSharp_AIEnum_EAIEventtype3896289311.h"
#include "AssemblyU2DCSharp_NpcMgr2339534743.h"
#include "AssemblyU2DCSharp_AIPath1930792045.h"
#include "AssemblyU2DCSharp_AnimationRunner1015409588.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniType1006238651.h"
#include "AssemblyU2DCSharp_HeroMgr2475965342.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_BuffCtrl2836564350.h"
#include "AssemblyU2DCSharp_ReplayMgr1549183121.h"
#include "AssemblyU2DCSharp_ReplayExecuteType3106517448.h"
#include "AssemblyU2DCSharp_CSDatacfgManager1565254243.h"
#include "AssemblyU2DCSharp_CSFightforData215334547.h"
#include "AssemblyU2DCSharp_CSCampfightData179152873.h"

// System.Void Hero::.ctor()
extern "C"  void Hero__ctor_m1428112321 (Hero_t2245658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::set_aiCtrl(AICtrl)
extern "C"  void Hero_set_aiCtrl_m713770272 (Hero_t2245658 * __this, AICtrl_t1930422963 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AICtrl Hero::get_aiCtrl()
extern "C"  AICtrl_t1930422963 * Hero_get_aiCtrl_m2803775401 (Hero_t2245658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::set_IsBattling(System.Boolean)
extern "C"  void Hero_set_IsBattling_m509775318 (Hero_t2245658 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Hero::get_IsBattling()
extern "C"  bool Hero_get_IsBattling_m609166303 (Hero_t2245658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::set_atkTarget(CombatEntity)
extern "C"  void Hero_set_atkTarget_m3962607220 (Hero_t2245658 * __this, CombatEntity_t684137495 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Hero::get_atkTarget()
extern "C"  CombatEntity_t684137495 * Hero_get_atkTarget_m104406617 (Hero_t2245658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Hero::get_isCaptain()
extern "C"  bool Hero_get_isCaptain_m3098107260 (Hero_t2245658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::Start()
extern "C"  void Hero_Start_m375250113 (Hero_t2245658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::Update()
extern "C"  void Hero_Update_m3048671084 (Hero_t2245658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::GadInit()
extern "C"  void Hero_GadInit_m2784249305 (Hero_t2245658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::OnGadSkill(CEvent.ZEvent)
extern "C"  void Hero_OnGadSkill_m3097060370 (Hero_t2245658 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::Create(CSHeroUnit)
extern "C"  void Hero_Create_m1374346225 (Hero_t2245658 * __this, CSHeroUnit_t3764358446 * ___hcfg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::Create(herosCfg,JSCLevelHeroPointConfig,CSSkillData[])
extern "C"  void Hero_Create_m2740097777 (Hero_t2245658 * __this, herosCfg_t3676934635 * ___mcfg0, JSCLevelHeroPointConfig_t561690542 * ___lmcfg1, CSSkillDataU5BU5D_t1071816810* ___skillList2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::OnCurHostMonsterDead(CEvent.ZEvent)
extern "C"  void Hero_OnCurHostMonsterDead_m544241891 (Hero_t2245658 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::set_hp(System.Single)
extern "C"  void Hero_set_hp_m1776906531 (Hero_t2245658 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Hero::get_hp()
extern "C"  float Hero_get_hp_m1306688224 (Hero_t2245658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::OnUseingSkill(System.Int32)
extern "C"  void Hero_OnUseingSkill_m186479655 (Hero_t2245658 * __this, int32_t ___skillID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::AIMonsterDead(System.Int32)
extern "C"  void Hero_AIMonsterDead_m514307430 (Hero_t2245658 * __this, int32_t ___murderID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::AIBeAttacked()
extern "C"  void Hero_AIBeAttacked_m1568807765 (Hero_t2245658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ATKDEBUFF()
extern "C"  void Hero_ATKDEBUFF_m3585821643 (Hero_t2245658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::AGAINATTACK()
extern "C"  void Hero_AGAINATTACK_m2514290183 (Hero_t2245658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::AIOtherDead(CEvent.ZEvent)
extern "C"  void Hero_AIOtherDead_m2202268304 (Hero_t2245658 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::Answer(CEvent.ZEvent)
extern "C"  void Hero_Answer_m819498234 (Hero_t2245658 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ClosePlot(CEvent.ZEvent)
extern "C"  void Hero_ClosePlot_m2299312355 (Hero_t2245658 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::MoceCameraEnd(CEvent.ZEvent)
extern "C"  void Hero_MoceCameraEnd_m3378955114 (Hero_t2245658 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::OnDelayTime()
extern "C"  void Hero_OnDelayTime_m1199988208 (Hero_t2245658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::OnCheckTarget()
extern "C"  void Hero_OnCheckTarget_m3290161593 (Hero_t2245658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::OnLevelTime()
extern "C"  void Hero_OnLevelTime_m2838231857 (Hero_t2245658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::InitPos(UnityEngine.Vector3,UnityEngine.Quaternion,System.Boolean,JSCLevelHeroPointConfig)
extern "C"  void Hero_InitPos_m2543796672 (Hero_t2245658 * __this, Vector3_t4282066566  ___pos0, Quaternion_t1553702882  ___rot1, bool ___isTrue2, JSCLevelHeroPointConfig_t561690542 * ___lhcfg3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::OnTriggerFun(System.Int32)
extern "C"  void Hero_OnTriggerFun_m1107328410 (Hero_t2245658 * __this, int32_t ___effectId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::Addhurt(System.Single)
extern "C"  void Hero_Addhurt_m3263633532 (Hero_t2245658 * __this, float ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::UpdateTargetPos(UnityEngine.Vector3,System.Action)
extern "C"  void Hero_UpdateTargetPos_m4231496919 (Hero_t2245658 * __this, Vector3_t4282066566  ___targetPos0, Action_t3771233898 * ___OnTargetCall1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::UpdateMove()
extern "C"  void Hero_UpdateMove_m2919387549 (Hero_t2245658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::AutoFastRun()
extern "C"  void Hero_AutoFastRun_m3720631871 (Hero_t2245658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::LeaveAuto()
extern "C"  void Hero_LeaveAuto_m423021925 (Hero_t2245658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::OnTargetReached()
extern "C"  void Hero_OnTargetReached_m111044033 (Hero_t2245658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::Dead()
extern "C"  void Hero_Dead_m2755387591 (Hero_t2245658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::OnDestroy()
extern "C"  void Hero_OnDestroy_m4164374586 (Hero_t2245658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::SingleMove(UnityEngine.Vector3,System.Boolean)
extern "C"  void Hero_SingleMove_m3872170336 (Hero_t2245658 * __this, Vector3_t4282066566  ___pos0, bool ___isDoubleClick1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::DoForceMove(UnityEngine.Vector3)
extern "C"  void Hero_DoForceMove_m83651241 (Hero_t2245658 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Hero::BuffInHp()
extern "C"  float Hero_BuffInHp_m3823187247 (Hero_t2245658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::CalcuFinalProp()
extern "C"  void Hero_CalcuFinalProp_m2147438012 (Hero_t2245658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::SkillUpId(System.Int32,System.Int32)
extern "C"  int32_t Hero_SkillUpId_m3371158350 (Hero_t2245658 * __this, int32_t ___skillId0, int32_t ___lv1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::AssinPlusAtt()
extern "C"  void Hero_AssinPlusAtt_m1591073668 (Hero_t2245658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::GetSlotValue()
extern "C"  int32_t Hero_GetSlotValue_m3806973582 (Hero_t2245658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_Start1(CombatEntity)
extern "C"  void Hero_ilo_Start1_m3829339182 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<AIObject> Hero::ilo_get_AIDelayList2(CombatEntity)
extern "C"  List_1_t1405465687 * Hero_ilo_get_AIDelayList2_m3782788667 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitType Hero::ilo_get_unitType3(CombatEntity)
extern "C"  int32_t Hero_ilo_get_unitType3_m4148674934 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_TriggerBattle4(GameMgr,CombatEntity)
extern "C"  void Hero_ilo_TriggerBattle4_m4203163773 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, CombatEntity_t684137495 * ___src1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightCtrl Hero::ilo_get_fightCtrl5(CombatEntity)
extern "C"  FightCtrl_t648967803 * Hero_ilo_get_fightCtrl5_m3286412986 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_aiCtrl6(Hero,AICtrl)
extern "C"  void Hero_ilo_set_aiCtrl6_m3989173531 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, AICtrl_t1930422963 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Hero::ilo_get_AiId7(CSHeroUnit)
extern "C"  String_t* Hero_ilo_get_AiId7_m3665750950 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_Init8(AICtrl,System.String[])
extern "C"  void Hero_ilo_Init8_m1264866619 (Il2CppObject * __this /* static, unused */, AICtrl_t1930422963 * ____this0, StringU5BU5D_t4054002952* ___aiIDs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_id9(CombatEntity,System.Int32)
extern "C"  void Hero_ilo_set_id9_m1309640001 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_showname10(CombatEntity,System.String)
extern "C"  void Hero_ilo_set_showname10_m51389535 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_sexType11(CombatEntity,SEX_TYPE)
extern "C"  void Hero_ilo_set_sexType11_m2025955227 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Hero::ilo_get_lv12(CSHeroUnit)
extern "C"  uint32_t Hero_ilo_get_lv12_m716024663 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_level13(CombatEntity,System.Int32)
extern "C"  void Hero_ilo_set_level13_m3171895577 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_AttackNature14(CSHeroUnit)
extern "C"  int32_t Hero_ilo_get_AttackNature14_m295163633 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_atkType15(CombatEntity,System.Int32)
extern "C"  void Hero_ilo_set_atkType15_m3303652265 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HERO_TYPE Hero::ilo_get_HeroType16(CSHeroUnit)
extern "C"  int32_t Hero_ilo_get_HeroType16_m3430470462 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_heromajor17(CombatEntity,HERO_TYPE)
extern "C"  void Hero_ilo_set_heromajor17_m1325075114 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HERO_ELEMENT Hero::ilo_get_HeroElement18(CSHeroUnit)
extern "C"  int32_t Hero_ilo_get_HeroElement18_m1684934264 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_playerId19(CombatEntity,System.UInt32)
extern "C"  void Hero_ilo_set_playerId19_m763278516 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_effect_leader20(CSHeroUnit)
extern "C"  int32_t Hero_ilo_get_effect_leader20_m421720764 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Hero::ilo_get_AtkEffectDelay21(CSHeroUnit)
extern "C"  float Hero_ilo_get_AtkEffectDelay21_m1910499254 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_atkEffDelayTime22(CombatEntity,System.Single)
extern "C"  void Hero_ilo_set_atkEffDelayTime22_m1541004288 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_skills23(CombatEntity,System.String)
extern "C"  void Hero_ilo_set_skills23_m163590587 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_halfwidth24(CombatEntity,System.Single)
extern "C"  void Hero_ilo_set_halfwidth24_m2165894578 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_initBehavior25(CombatEntity,System.Int32)
extern "C"  void Hero_ilo_set_initBehavior25_m103420022 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Hero::ilo_get_attack26(CSHeroUnit)
extern "C"  uint32_t Hero_ilo_get_attack26_m357177016 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_PD27(CombatEntity,System.Single)
extern "C"  void Hero_ilo_set_PD27_m2934691226 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Hero::ilo_get_mag_def28(CSHeroUnit)
extern "C"  uint32_t Hero_ilo_get_mag_def28_m3229814969 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Hero::ilo_get_maxHp29(CombatEntity)
extern "C"  float Hero_ilo_get_maxHp29_m1132812691 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_hits30(CombatEntity,System.Single)
extern "C"  void Hero_ilo_set_hits30_m850617726 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Hero::ilo_get_dodge31(CSHeroUnit)
extern "C"  uint32_t Hero_ilo_get_dodge31_m2420354767 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Hero::ilo_GetHeroAttr32(CombatAttPlus,HERO_ATT_KEY,System.UInt32)
extern "C"  uint32_t Hero_ilo_GetHeroAttr32_m2529958628 (Il2CppObject * __this /* static, unused */, CombatAttPlus_t649400871 * ____this0, int32_t ___key1, uint32_t ___basicsAtt2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Hero::ilo_get_crit33(CSHeroUnit)
extern "C"  uint32_t Hero_ilo_get_crit33_m2502476582 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_crit34(CombatEntity,System.Single)
extern "C"  void Hero_ilo_set_crit34_m1304414364 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Hero::ilo_get_damage_reflect35(CSHeroUnit)
extern "C"  uint32_t Hero_ilo_get_damage_reflect35_m1396549563 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Hero::ilo_get_heal_bonus36(CSHeroUnit)
extern "C"  uint32_t Hero_ilo_get_heal_bonus36_m3525636247 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_hurtP37(CombatEntity,System.Single)
extern "C"  void Hero_ilo_set_hurtP37_m2219112962 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Hero::ilo_get_stubborn38(CSHeroUnit)
extern "C"  uint32_t Hero_ilo_get_stubborn38_m1366629256 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Hero::ilo_get_hitrate_per39(CSHeroUnit)
extern "C"  uint32_t Hero_ilo_get_hitrate_per39_m1579137553 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_hitsP40(CombatEntity,System.Single)
extern "C"  void Hero_ilo_set_hitsP40_m4190399689 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Hero::ilo_get_dodge_per41(CSHeroUnit)
extern "C"  uint32_t Hero_ilo_get_dodge_per41_m886914380 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_wreckP42(CombatEntity,System.Single)
extern "C"  void Hero_ilo_set_wreckP42_m2862406045 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_withstandP43(CombatEntity,System.Single)
extern "C"  void Hero_ilo_set_withstandP43_m1778892128 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Hero::ilo_get_crit_per44(CSHeroUnit)
extern "C"  uint32_t Hero_ilo_get_crit_per44_m2283251108 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_critP45(CombatEntity,System.Single)
extern "C"  void Hero_ilo_set_critP45_m203502964 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_anger_initial46(CSHeroUnit)
extern "C"  int32_t Hero_ilo_get_anger_initial46_m2117529557 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSCheckPointUnit Hero::ilo_get_checkPoint47(GameMgr)
extern "C"  CSCheckPointUnit_t3129216924 * Hero_ilo_get_checkPoint47_m758695457 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_rage48(CombatEntity,System.Single)
extern "C"  void Hero_ilo_set_rage48_m3811626098 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// herosCfg Hero::ilo_get_config49(CSHeroUnit)
extern "C"  herosCfg_t3676934635 * Hero_ilo_get_config49_m3822328650 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_residual_mp50(CSHeroUnit)
extern "C"  int32_t Hero_ilo_get_residual_mp50_m3137087231 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Hero::ilo_get_hp51(Hero)
extern "C"  float Hero_ilo_get_hp51_m200793685 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_residual_hpPer52(CSHeroUnit)
extern "C"  int32_t Hero_ilo_get_residual_hpPer52_m685651255 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_hp53(Hero,System.Single)
extern "C"  void Hero_ilo_set_hp53_m1053249572 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_rageGrowth54(CombatEntity,System.Int32)
extern "C"  void Hero_ilo_set_rageGrowth54_m3996557160 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_findEnemyRange55(CombatEntity,System.Single)
extern "C"  void Hero_ilo_set_findEnemyRange55_m270685743 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_runSpeed56(CombatEntity,System.Single)
extern "C"  void Hero_ilo_set_runSpeed56_m3109040894 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_beforModelScale57(CombatEntity,System.Single)
extern "C"  void Hero_ilo_set_beforModelScale57_m3721948422 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_tauntDistance58(CombatEntity,System.Single)
extern "C"  void Hero_ilo_set_tauntDistance58_m3666567075 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_angerSecDecNum59(CombatEntity,System.Int32)
extern "C"  void Hero_ilo_set_angerSecDecNum59_m201582441 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_RefreshShaderProperty60(CombatEntity,System.String,System.Object)
extern "C"  void Hero_ilo_RefreshShaderProperty60_m1128304046 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, String_t* ___key1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Hero::ilo_FormatColorByStr61(System.String,System.Char)
extern "C"  Color_t4194546905  Hero_ilo_FormatColorByStr61_m4120508083 (Il2CppObject * __this /* static, unused */, String_t* ___str0, Il2CppChar ___c1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_unitTag62(CombatEntity,EntityEnum.UnitTag)
extern "C"  void Hero_ilo_set_unitTag62_m2181493104 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_unitCamp63(CombatEntity,EntityEnum.UnitCamp)
extern "C"  void Hero_ilo_set_unitCamp63_m448680155 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_bvrCtrl64(CombatEntity,Entity.Behavior.IBehaviorCtrl)
extern "C"  void Hero_ilo_set_bvrCtrl64_m2150474230 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, IBehaviorCtrl_t4225040900 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_IsBattling65(Hero,System.Boolean)
extern "C"  void Hero_ilo_set_IsBattling65_m3171589862 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_Init66(CombatEntity)
extern "C"  void Hero_ilo_Init66_m3955658287 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_AddEventListener67(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void Hero_ilo_AddEventListener67_m669954815 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_ClearmLastUseTime68(FightCtrl)
extern "C"  void Hero_ilo_ClearmLastUseTime68_m2454903467 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_unitType69(CombatEntity,EntityEnum.UnitType)
extern "C"  void Hero_ilo_set_unitType69_m1906523649 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_IntelligenceTip70(CombatEntity,System.Int32)
extern "C"  void Hero_ilo_set_IntelligenceTip70_m1540585674 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Hero::ilo_get_x71(JSCLevelHeroPointConfig)
extern "C"  float Hero_ilo_get_x71_m80727643 (Il2CppObject * __this /* static, unused */, JSCLevelHeroPointConfig_t561690542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Hero::ilo_get_z72(JSCLevelHeroPointConfig)
extern "C"  float Hero_ilo_get_z72_m3897241656 (Il2CppObject * __this /* static, unused */, JSCLevelHeroPointConfig_t561690542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Hero::ilo_get_scale73(CombatEntity)
extern "C"  float Hero_ilo_get_scale73_m1576802598 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroMgr Hero::ilo_get_instance74()
extern "C"  HeroMgr_t2475965342 * Hero_ilo_get_instance74_m2233092838 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32[] Hero::ilo_GetArticle_Uint75(System.String)
extern "C"  UInt32U5BU5D_t3230734560* Hero_ilo_GetArticle_Uint75_m624861848 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_attack76(CSHeroUnit,System.UInt32)
extern "C"  void Hero_ilo_set_attack76_m2575081760 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_phy_def77(CSHeroUnit,System.UInt32)
extern "C"  void Hero_ilo_set_phy_def77_m4114274620 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_hp78(CSHeroUnit,System.UInt32)
extern "C"  void Hero_ilo_set_hp78_m905980514 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_anticrit79(CSHeroUnit,System.UInt32)
extern "C"  void Hero_ilo_set_anticrit79_m308340439 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_heal_bonus80(CSHeroUnit,System.UInt32)
extern "C"  void Hero_ilo_set_heal_bonus80_m2298122169 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_damage_bonus81(CSHeroUnit,System.UInt32)
extern "C"  void Hero_ilo_set_damage_bonus81_m3125098433 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_firm82(CSHeroUnit,System.UInt32)
extern "C"  void Hero_ilo_set_firm82_m3544114417 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_pure83(CSHeroUnit,System.UInt32)
extern "C"  void Hero_ilo_set_pure83_m249344876 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_stubborn84(CSHeroUnit,System.UInt32)
extern "C"  void Hero_ilo_set_stubborn84_m2376385708 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_block_per85(CSHeroUnit,System.UInt32)
extern "C"  void Hero_ilo_set_block_per85_m2869456829 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_anticrit_per86(CSHeroUnit,System.UInt32)
extern "C"  void Hero_ilo_set_anticrit_per86_m1714347089 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_anger_max87(CSHeroUnit,System.Int32)
extern "C"  void Hero_ilo_set_anger_max87_m2373643531 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_anger_growth88(CSHeroUnit,System.Int32)
extern "C"  void Hero_ilo_set_anger_growth88_m2160267219 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_Create89(Hero,CSHeroUnit)
extern "C"  void Hero_ilo_Create89_m2509401471 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, CSHeroUnit_t3764358446 * ___hcfg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl Hero::ilo_get_bvrCtrl90(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * Hero_ilo_get_bvrCtrl90_m1625942190 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Hero::ilo_get_isAuto91(GameMgr)
extern "C"  bool Hero_ilo_get_isAuto91_m1250420488 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AICtrl Hero::ilo_get_aiCtrl92(Hero)
extern "C"  AICtrl_t1930422963 * Hero_ilo_get_aiCtrl92_m72302235 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_OnObservableRequest93(AICtrl,AIEnum.EAIEventtype,System.Int32,System.Int32)
extern "C"  void Hero_ilo_OnObservableRequest93_m3638503200 (Il2CppObject * __this /* static, unused */, AICtrl_t1930422963 * ____this0, uint8_t ___eType1, int32_t ___tParam12, int32_t ___tParam23, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Hero::ilo_get_hp94(CombatEntity)
extern "C"  float Hero_ilo_get_hp94_m626218551 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NpcMgr Hero::ilo_get_instance95()
extern "C"  NpcMgr_t2339534743 * Hero_ilo_get_instance95_m2473459766 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> Hero::ilo_GetAllMonsters96(NpcMgr)
extern "C"  List_1_t2052323047 * Hero_ilo_GetAllMonsters96_m590293514 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_time97(GameMgr)
extern "C"  int32_t Hero_ilo_get_time97_m3821872852 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_startRotation98(CombatEntity,UnityEngine.Quaternion)
extern "C"  void Hero_ilo_set_startRotation98_m2809818810 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, Quaternion_t1553702882  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Hero::ilo_GetPhysicsPos99(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Hero_ilo_GetPhysicsPos99_m594847940 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_OnTriggerFun100(CombatEntity,System.Int32)
extern "C"  void Hero_ilo_OnTriggerFun100_m1283648247 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___effectId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_Addhurt101(CombatEntity,System.Single)
extern "C"  void Hero_ilo_Addhurt101_m3238856006 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___delta1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_canMove102(CombatEntity,System.Boolean)
extern "C"  void Hero_ilo_set_canMove102_m3398015023 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_AutoFastRun103(Hero)
extern "C"  void Hero_ilo_AutoFastRun103_m3592163792 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Hero::ilo_GetFeetPosition104(AIPath)
extern "C"  Vector3_t4282066566  Hero_ilo_GetFeetPosition104_m3033499371 (Il2CppObject * __this /* static, unused */, AIPath_t1930792045 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_RotateTowards105(AIPath,UnityEngine.Vector3)
extern "C"  void Hero_ilo_RotateTowards105_m4071129649 (Il2CppObject * __this /* static, unused */, AIPath_t1930792045 * ____this0, Vector3_t4282066566  ___dir1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AnimationRunner Hero::ilo_get_characterAnim106(CombatEntity)
extern "C"  AnimationRunner_t1015409588 * Hero_ilo_get_characterAnim106_m3672632836 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_Play107(AnimationRunner,AnimationRunner/AniType,System.Single,System.Boolean)
extern "C"  void Hero_ilo_Play107_m2372114457 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, int32_t ___aniType1, float ___playTime2, bool ___isskill3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Hero::ilo_get_isWin108()
extern "C"  bool Hero_ilo_get_isWin108_m3404154650 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Hero::ilo_get_IsMarshalMoving109(CombatEntity)
extern "C"  bool Hero_ilo_get_IsMarshalMoving109_m2516704276 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Hero::ilo_get_runSpeed110(CombatEntity)
extern "C"  float Hero_ilo_get_runSpeed110_m594196500 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_SetRunSpeed111(CombatEntity,System.Single)
extern "C"  void Hero_ilo_SetRunSpeed111_m1907432919 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_OnTargetReached112(CombatEntity)
extern "C"  void Hero_ilo_OnTargetReached112_m2885469903 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_EnterAutoMode113(HeroMgr,System.Boolean)
extern "C"  void Hero_ilo_EnterAutoMode113_m1692123304 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, bool ___isMarshalMove1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_atkTarget114(Hero,CombatEntity)
extern "C"  void Hero_ilo_set_atkTarget114_m3482482443 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, CombatEntity_t684137495 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_StopMove115(CombatEntity,System.Boolean)
extern "C"  void Hero_ilo_StopMove115_m3904183686 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___isPlayAnim1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_TranBehavior116(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void Hero_ilo_TranBehavior116_m1458285775 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, uint8_t ___id1, ObjectU5BU5D_t1108656482* ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_RemoveEventListener117(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void Hero_ilo_RemoveEventListener117_m1543824032 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_id118(CombatEntity)
extern "C"  int32_t Hero_ilo_get_id118_m2890526427 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSHeroUnit Hero::ilo_GetHeroUnit119(HeroMgr,System.UInt32)
extern "C"  CSHeroUnit_t3764358446 * Hero_ilo_GetHeroUnit119_m433819230 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, uint32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BuffCtrl Hero::ilo_get_buffCtrl120(CombatEntity)
extern "C"  BuffCtrl_t2836564350 * Hero_ilo_get_buffCtrl120_m1972652990 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_mBuffATNum121(BuffCtrl)
extern "C"  int32_t Hero_ilo_get_mBuffATNum121_m3541185812 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Hero::ilo_get_phy_def122(CSHeroUnit)
extern "C"  uint32_t Hero_ilo_get_phy_def122_m1236471158 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Hero::ilo_get_hp123(CSHeroUnit)
extern "C"  uint32_t Hero_ilo_get_hp123_m4147727556 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Hero::ilo_get_hitrate124(CSHeroUnit)
extern "C"  uint32_t Hero_ilo_get_hitrate124_m1196214860 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_mBuffHits125(BuffCtrl)
extern "C"  int32_t Hero_ilo_get_mBuffHits125_m3115350915 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_mBuffDodge126(BuffCtrl)
extern "C"  int32_t Hero_ilo_get_mBuffDodge126_m4106532853 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_mBuffWreckNum127(BuffCtrl)
extern "C"  int32_t Hero_ilo_get_mBuffWreckNum127_m4195506033 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_wreck128(CombatEntity,System.Single)
extern "C"  void Hero_ilo_set_wreck128_m418553222 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Hero::ilo_get_block129(CSHeroUnit)
extern "C"  uint32_t Hero_ilo_get_block129_m3598000183 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_mBuffWithstandNum130(BuffCtrl)
extern "C"  int32_t Hero_ilo_get_mBuffWithstandNum130_m1446424455 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_mBuffCritNum131(BuffCtrl)
extern "C"  int32_t Hero_ilo_get_mBuffCritNum131_m2616925498 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_mBuffCrit132(BuffCtrl)
extern "C"  int32_t Hero_ilo_get_mBuffCrit132_m1660139589 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Hero::ilo_get_anticrit133(CSHeroUnit)
extern "C"  uint32_t Hero_ilo_get_anticrit133_m2439903087 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_mBuffTenacityNum134(BuffCtrl)
extern "C"  int32_t Hero_ilo_get_mBuffTenacityNum134_m2683503346 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_mBuffTenacity135(BuffCtrl)
extern "C"  int32_t Hero_ilo_get_mBuffTenacity135_m577699635 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_tenacity136(CombatEntity,System.Single)
extern "C"  void Hero_ilo_set_tenacity136_m2806642530 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_rHurt137(CombatEntity,System.Single)
extern "C"  void Hero_ilo_set_rHurt137_m3335936501 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Hero::ilo_get_mHPRstNum138(BuffCtrl)
extern "C"  float Hero_ilo_get_mHPRstNum138_m3913366747 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Hero::ilo_get_mHPRstNumATK139(BuffCtrl)
extern "C"  float Hero_ilo_get_mHPRstNumATK139_m3852494688 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Hero::ilo_get_mAttackSpeed140(BuffCtrl)
extern "C"  float Hero_ilo_get_mAttackSpeed140_m3072412084 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Hero::ilo_get_atkSpeed141(CombatEntity)
extern "C"  float Hero_ilo_get_atkSpeed141_m2150646847 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_AddData142(ReplayMgr,ReplayExecuteType,CombatEntity,System.Object[])
extern "C"  void Hero_ilo_AddData142_m2548601968 (Il2CppObject * __this /* static, unused */, ReplayMgr_t1549183121 * ____this0, int32_t ___type1, CombatEntity_t684137495 * ___entity2, ObjectU5BU5D_t1108656482* ___datas3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Hero::ilo_get_mModelScale143(BuffCtrl)
extern "C"  float Hero_ilo_get_mModelScale143_m2440009359 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Hero::ilo_get_modelScale144(CombatEntity)
extern "C"  float Hero_ilo_get_modelScale144_m1107400880 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Hero::ilo_get_damage_reduce145(CSHeroUnit)
extern "C"  uint32_t Hero_ilo_get_damage_reduce145_m541774248 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_mRestrintCountryType146(BuffCtrl)
extern "C"  int32_t Hero_ilo_get_mRestrintCountryType146_m1959939448 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_restraintCountryType147(CombatEntity,System.Int32)
extern "C"  void Hero_ilo_set_restraintCountryType147_m2607132623 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_restraintStatePer148(CombatEntity,System.Single)
extern "C"  void Hero_ilo_set_restraintStatePer148_m1916744926 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_mNDPAddBuff149(BuffCtrl)
extern "C"  int32_t Hero_ilo_get_mNDPAddBuff149_m2846339422 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_mNDPAddBuff02150(BuffCtrl)
extern "C"  int32_t Hero_ilo_get_mNDPAddBuff02150_m474097138 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_BeAttackedLaterEffectID151(CombatEntity,System.Int32)
extern "C"  void Hero_ilo_set_BeAttackedLaterEffectID151_m2093949820 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_DeathLaterHeroID152(CombatEntity,System.Int32)
extern "C"  void Hero_ilo_set_DeathLaterHeroID152_m2233273278 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_BeattackedLaterSpeed153(CombatEntity,System.Int32)
extern "C"  void Hero_ilo_set_BeattackedLaterSpeed153_m2954920405 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_mAnitNum154(BuffCtrl)
extern "C"  int32_t Hero_ilo_get_mAnitNum154_m2984704006 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_AntiEndEffectID155(CombatEntity,System.Int32)
extern "C"  void Hero_ilo_set_AntiEndEffectID155_m3074612591 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_AuraAddHPNum156(CombatEntity,System.Int32)
extern "C"  void Hero_ilo_set_AuraAddHPNum156_m3748932013 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_mAuraReduceHPRadius157(BuffCtrl)
extern "C"  int32_t Hero_ilo_get_mAuraReduceHPRadius157_m2910890802 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_AuraReduceHPRadius158(CombatEntity,System.Int32)
extern "C"  void Hero_ilo_set_AuraReduceHPRadius158_m4175984430 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_AuraReduceHPPer159(CombatEntity,System.Int32)
extern "C"  void Hero_ilo_set_AuraReduceHPPer159_m847771890 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_mActiveSkillDamageAddPer160(BuffCtrl)
extern "C"  int32_t Hero_ilo_get_mActiveSkillDamageAddPer160_m4265978905 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_mHpLineUpBuffID161(BuffCtrl)
extern "C"  int32_t Hero_ilo_get_mHpLineUpBuffID161_m2752464329 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_controlBuffTimeBonusPer162(CombatEntity,System.Single)
extern "C"  void Hero_ilo_set_controlBuffTimeBonusPer162_m754218067 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_runeTakeEffectNum163(CombatEntity,System.Int32)
extern "C"  void Hero_ilo_set_runeTakeEffectNum163_m2284200513 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_mRuneTakeEffectBuffID164(BuffCtrl)
extern "C"  int32_t Hero_ilo_get_mRuneTakeEffectBuffID164_m2445053937 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_mRuneTakeEffectBuffID02165(BuffCtrl)
extern "C"  int32_t Hero_ilo_get_mRuneTakeEffectBuffID02165_m664165424 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_runeTakeEffectBuffID02166(CombatEntity,System.Int32)
extern "C"  void Hero_ilo_set_runeTakeEffectBuffID02166_m787922256 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Hero::ilo_get_mGodDownTweenTime167(BuffCtrl)
extern "C"  float Hero_ilo_get_mGodDownTweenTime167_m1826195608 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_mAntiDebuffDiscreteAddBuffID168(BuffCtrl)
extern "C"  int32_t Hero_ilo_get_mAntiDebuffDiscreteAddBuffID168_m84245367 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_antiDebuffDiscreteAddBuffID169(CombatEntity,System.Int32)
extern "C"  void Hero_ilo_set_antiDebuffDiscreteAddBuffID169_m3634450343 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_antiDebuffDiscreteID170(CombatEntity,System.Int32)
extern "C"  void Hero_ilo_set_antiDebuffDiscreteID170_m1470858175 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Hero::ilo_get_firm171(CSHeroUnit)
extern "C"  uint32_t Hero_ilo_get_firm171_m2311589415 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_stable172(CombatEntity,System.Int32)
extern "C"  void Hero_ilo_set_stable172_m2714755246 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Hero::ilo_get_pure173(CSHeroUnit)
extern "C"  uint32_t Hero_ilo_get_pure173_m2579339055 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_notInvade174(CombatEntity,System.Int32)
extern "C"  void Hero_ilo_set_notInvade174_m4216935825 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Hero::ilo_get_mPetrifiedChangeTime175(BuffCtrl)
extern "C"  float Hero_ilo_get_mPetrifiedChangeTime175_m3649003840 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_mThumpAddValue176(BuffCtrl)
extern "C"  int32_t Hero_ilo_get_mThumpAddValue176_m3906744384 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_tankBuffID177(CombatEntity,System.Int32)
extern "C"  void Hero_ilo_set_tankBuffID177_m3651113670 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_mBuffCritPer178(BuffCtrl)
extern "C"  int32_t Hero_ilo_get_mBuffCritPer178_m1539612102 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_critHurt179(CombatEntity,System.Single)
extern "C"  void Hero_ilo_set_critHurt179_m1551379047 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_mReduceRageHpNum180(BuffCtrl)
extern "C"  int32_t Hero_ilo_get_mReduceRageHpNum180_m2084755010 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_deathHarvestLine181(CombatEntity,System.Int32)
extern "C"  void Hero_ilo_set_deathHarvestLine181_m3660099454 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_mDeathHarvestPer182(BuffCtrl)
extern "C"  int32_t Hero_ilo_get_mDeathHarvestPer182_m2511291303 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_mHpLineCleaveAddBuff183(BuffCtrl)
extern "C"  int32_t Hero_ilo_get_mHpLineCleaveAddBuff183_m2515523564 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Hero::ilo_get_mResistanceCountryType184(BuffCtrl)
extern "C"  int32_t Hero_ilo_get_mResistanceCountryType184_m2582910298 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_resistanceCountryType185(CombatEntity,System.Int32)
extern "C"  void Hero_ilo_set_resistanceCountryType185_m1730168676 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_set_resistanceCountryPer186(CombatEntity,System.Single)
extern "C"  void Hero_ilo_set_resistanceCountryPer186_m3984660204 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// skillCfg Hero::ilo_GetskillCfg187(CSDatacfgManager,System.Int32)
extern "C"  skillCfg_t2142425171 * Hero_ilo_GetskillCfg187_m1604829715 (Il2CppObject * __this /* static, unused */, CSDatacfgManager_t1565254243 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSGameDataMgr Hero::ilo_get_CSGameDataMgr188()
extern "C"  CSGameDataMgr_t2623305516 * Hero_ilo_get_CSGameDataMgr188_m546855915 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hero::ilo_AddPlusAttribute189(CombatAttPlus,System.String,System.UInt32,System.UInt32)
extern "C"  void Hero_ilo_AddPlusAttribute189_m605582482 (Il2CppObject * __this /* static, unused */, CombatAttPlus_t649400871 * ____this0, String_t* ___key1, uint32_t ___value2, uint32_t ___valuePer3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSPlusAtt> Hero::ilo_GetFightForPlusAtt190(CSFightforData)
extern "C"  List_1_t341533415 * Hero_ilo_GetFightForPlusAtt190_m187989809 (Il2CppObject * __this /* static, unused */, CSFightforData_t215334547 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSPlusAtt> Hero::ilo_GetAttPlus_fightfor191(CSCampfightData)
extern "C"  List_1_t341533415 * Hero_ilo_GetAttPlus_fightfor191_m811437925 (Il2CppObject * __this /* static, unused */, CSCampfightData_t179152873 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
