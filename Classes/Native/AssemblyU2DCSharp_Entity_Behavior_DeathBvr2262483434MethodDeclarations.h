﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.DeathBvr
struct DeathBvr_t2262483434;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// CombatEntity
struct CombatEntity_t684137495;
// effectCfg
struct effectCfg_t2826279187;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// EffectMgr
struct EffectMgr_t535289511;
// AnimationRunner
struct AnimationRunner_t1015409588;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitType643359572.h"
#include "AssemblyU2DCSharp_HERO_ELEMENT2692579223.h"
#include "AssemblyU2DCSharp_CSDatacfgManager1565254243.h"
#include "AssemblyU2DCSharp_EffectMgr535289511.h"
#include "AssemblyU2DCSharp_AnimationRunner1015409588.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniType1006238651.h"

// System.Void Entity.Behavior.DeathBvr::.ctor()
extern "C"  void DeathBvr__ctor_m1184547456 (DeathBvr_t2262483434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID Entity.Behavior.DeathBvr::get_id()
extern "C"  uint8_t DeathBvr_get_id_m603709466 (DeathBvr_t2262483434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.DeathBvr::Reason()
extern "C"  void DeathBvr_Reason_m1912620872 (DeathBvr_t2262483434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.DeathBvr::Action()
extern "C"  void DeathBvr_Action_m1109327546 (DeathBvr_t2262483434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Entity.Behavior.DeathBvr::CanTran2OtherBehavior(Entity.Behavior.IBehavior)
extern "C"  bool DeathBvr_CanTran2OtherBehavior_m1997326611 (DeathBvr_t2262483434 * __this, IBehavior_t770859129 * ___behavior0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.DeathBvr::SetParams(System.Object[])
extern "C"  void DeathBvr_SetParams_m653955916 (DeathBvr_t2262483434 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.DeathBvr::DoEntering()
extern "C"  void DeathBvr_DoEntering_m3550649081 (DeathBvr_t2262483434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.DeathBvr::DeadedShaderEffect()
extern "C"  void DeathBvr_DeadedShaderEffect_m191239357 (DeathBvr_t2262483434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.DeathBvr::WinkShader(UnityEngine.GameObject)
extern "C"  void DeathBvr_WinkShader_m1116389840 (DeathBvr_t2262483434 * __this, GameObject_t3674682005 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.DeathBvr::DoLeaving()
extern "C"  void DeathBvr_DoLeaving_m697994631 (DeathBvr_t2262483434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.DeathBvr::ilo_get_entity1(Entity.Behavior.IBehavior)
extern "C"  CombatEntity_t684137495 * DeathBvr_ilo_get_entity1_m2491410622 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.DeathBvr::ilo_Clear2(CombatEntity)
extern "C"  void DeathBvr_ilo_Clear2_m2492338245 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitType Entity.Behavior.DeathBvr::ilo_get_unitType3(CombatEntity)
extern "C"  int32_t DeathBvr_ilo_get_unitType3_m1467791029 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HERO_ELEMENT Entity.Behavior.DeathBvr::ilo_get_element4(CombatEntity)
extern "C"  int32_t DeathBvr_ilo_get_element4_m3573409319 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// effectCfg Entity.Behavior.DeathBvr::ilo_GeteffectCfg5(CSDatacfgManager,System.Int32)
extern "C"  effectCfg_t2826279187 * DeathBvr_ilo_GeteffectCfg5_m235491735 (Il2CppObject * __this /* static, unused */, CSDatacfgManager_t1565254243 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EffectMgr Entity.Behavior.DeathBvr::ilo_get_EffectMgr6()
extern "C"  EffectMgr_t535289511 * DeathBvr_ilo_get_EffectMgr6_m2222060059 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.DeathBvr::ilo_PlayEffect7(EffectMgr,System.Int32,CombatEntity)
extern "C"  void DeathBvr_ilo_PlayEffect7_m3168110950 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, int32_t ___id1, CombatEntity_t684137495 * ___srcTf2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.DeathBvr::ilo_Play8(AnimationRunner,AnimationRunner/AniType,System.Single,System.Boolean)
extern "C"  void DeathBvr_ilo_Play8_m2012064728 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, int32_t ___aniType1, float ___playTime2, bool ___isskill3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Entity.Behavior.DeathBvr::ilo_get_lucencytime9(CombatEntity)
extern "C"  float DeathBvr_ilo_get_lucencytime9_m1036863176 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
