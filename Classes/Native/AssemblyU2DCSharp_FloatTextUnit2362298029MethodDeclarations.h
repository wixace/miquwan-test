﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FloatTextUnit
struct FloatTextUnit_t2362298029;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// EventDelegate/Callback
struct Callback_t1094463061;
// UITweener
struct UITweener_t105489188;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "AssemblyU2DCSharp_UITweener_Method1078127180.h"
#include "AssemblyU2DCSharp_UITweener105489188.h"
#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"

// System.Void FloatTextUnit::.ctor()
extern "C"  void FloatTextUnit__ctor_m2671101278 (FloatTextUnit_t2362298029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID FloatTextUnit::FloatID()
extern "C"  int32_t FloatTextUnit_FloatID_m2370048796 (FloatTextUnit_t2362298029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextUnit::OnAwake(UnityEngine.GameObject)
extern "C"  void FloatTextUnit_OnAwake_m3678727962 (FloatTextUnit_t2362298029 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextUnit::OnDestroy()
extern "C"  void FloatTextUnit_OnDestroy_m1774828375 (FloatTextUnit_t2362298029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextUnit::SetFile(System.Object[])
extern "C"  void FloatTextUnit_SetFile_m1052410680 (FloatTextUnit_t2362298029 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextUnit::set_IsLife(System.Boolean)
extern "C"  void FloatTextUnit_set_IsLife_m3570058016 (FloatTextUnit_t2362298029 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FloatTextUnit::get_IsLife()
extern "C"  bool FloatTextUnit_get_IsLife_m1957427649 (FloatTextUnit_t2362298029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextUnit::Init()
extern "C"  void FloatTextUnit_Init_m4055573814 (FloatTextUnit_t2362298029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextUnit::Play()
extern "C"  void FloatTextUnit_Play_m4253897306 (FloatTextUnit_t2362298029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextUnit::ResetToBeginning()
extern "C"  void FloatTextUnit_ResetToBeginning_m119586745 (FloatTextUnit_t2362298029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextUnit::MovePosition(UnityEngine.Vector3,System.Single,System.Single,System.Single,EventDelegate/Callback,UnityEngine.GameObject,UITweener/Method)
extern "C"  void FloatTextUnit_MovePosition_m209973957 (FloatTextUnit_t2362298029 * __this, Vector3_t4282066566  ___from0, float ___toUp1, float ___duration2, float ___delay3, Callback_t1094463061 * ___call4, GameObject_t3674682005 * ___go5, int32_t ___method6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextUnit::MovePosition(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,EventDelegate/Callback,UnityEngine.GameObject,UITweener/Method)
extern "C"  void FloatTextUnit_MovePosition_m1696016919 (FloatTextUnit_t2362298029 * __this, Vector3_t4282066566  ___from0, Vector3_t4282066566  ___to1, float ___duration2, float ___delay3, Callback_t1094463061 * ___call4, GameObject_t3674682005 * ___go5, int32_t ___method6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextUnit::MoveScale(System.Single,System.Single,System.Single,System.Single,EventDelegate/Callback,UnityEngine.GameObject,UITweener/Method)
extern "C"  void FloatTextUnit_MoveScale_m2757430632 (FloatTextUnit_t2362298029 * __this, float ___from0, float ___to1, float ___duration2, float ___delay3, Callback_t1094463061 * ___call4, GameObject_t3674682005 * ___go5, int32_t ___method6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextUnit::MoveAlpha(System.Single,System.Single,System.Single,System.Single,EventDelegate/Callback,UnityEngine.GameObject,UITweener/Method)
extern "C"  void FloatTextUnit_MoveAlpha_m1492109692 (FloatTextUnit_t2362298029 * __this, float ___from0, float ___to1, float ___duration2, float ___delay3, Callback_t1094463061 * ___call4, GameObject_t3674682005 * ___go5, int32_t ___method6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextUnit::Death()
extern "C"  void FloatTextUnit_Death_m761196528 (FloatTextUnit_t2362298029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextUnit::Break()
extern "C"  void FloatTextUnit_Break_m3656466363 (FloatTextUnit_t2362298029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextUnit::Clear()
extern "C"  void FloatTextUnit_Clear_m77234569 (FloatTextUnit_t2362298029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextUnit::ilo_PlayForward1(UITweener)
extern "C"  void FloatTextUnit_ilo_PlayForward1_m3774788789 (Il2CppObject * __this /* static, unused */, UITweener_t105489188 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextUnit::ilo_set_IsLife2(FloatTextUnit,System.Boolean)
extern "C"  void FloatTextUnit_ilo_set_IsLife2_m662025392 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextUnit::ilo_ResetToBeginning3(UITweener)
extern "C"  void FloatTextUnit_ilo_ResetToBeginning3_m143265669 (Il2CppObject * __this /* static, unused */, UITweener_t105489188 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextUnit::ilo_SetOnFinished4(UITweener,EventDelegate/Callback)
extern "C"  void FloatTextUnit_ilo_SetOnFinished4_m344663613 (Il2CppObject * __this /* static, unused */, UITweener_t105489188 * ____this0, Callback_t1094463061 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
