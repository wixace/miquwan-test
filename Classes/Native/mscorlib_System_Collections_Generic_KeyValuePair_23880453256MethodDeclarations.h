﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g25818750MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3311419454(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3880453256 *, int32_t, List_1_t3730483581 *, const MethodInfo*))KeyValuePair_2__ctor_m3745503539_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::get_Key()
#define KeyValuePair_2_get_Key_m982296527(__this, method) ((  int32_t (*) (KeyValuePair_2_t3880453256 *, const MethodInfo*))KeyValuePair_2_get_Key_m2837181653_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1941662507(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3880453256 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m4121120022_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::get_Value()
#define KeyValuePair_2_get_Value_m1743759744(__this, method) ((  List_1_t3730483581 * (*) (KeyValuePair_2_t3880453256 *, const MethodInfo*))KeyValuePair_2_get_Value_m389192761_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m4173803947(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3880453256 *, List_1_t3730483581 *, const MethodInfo*))KeyValuePair_2_set_Value_m2663786006_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::ToString()
#define KeyValuePair_2_ToString_m3029359255(__this, method) ((  String_t* (*) (KeyValuePair_2_t3880453256 *, const MethodInfo*))KeyValuePair_2_ToString_m1200637362_gshared)(__this, method)
