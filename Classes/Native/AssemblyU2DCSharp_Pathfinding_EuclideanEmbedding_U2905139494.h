﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Pathfinding.GraphNode>
struct List_1_t1391797922;
// Pathfinding.EuclideanEmbedding
struct EuclideanEmbedding_t908139023;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.EuclideanEmbedding/<RecalculatePivots>c__AnonStorey11C
struct  U3CRecalculatePivotsU3Ec__AnonStorey11C_t2905139494  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<Pathfinding.GraphNode> Pathfinding.EuclideanEmbedding/<RecalculatePivots>c__AnonStorey11C::pivotList
	List_1_t1391797922 * ___pivotList_0;
	// Pathfinding.EuclideanEmbedding Pathfinding.EuclideanEmbedding/<RecalculatePivots>c__AnonStorey11C::<>f__this
	EuclideanEmbedding_t908139023 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_pivotList_0() { return static_cast<int32_t>(offsetof(U3CRecalculatePivotsU3Ec__AnonStorey11C_t2905139494, ___pivotList_0)); }
	inline List_1_t1391797922 * get_pivotList_0() const { return ___pivotList_0; }
	inline List_1_t1391797922 ** get_address_of_pivotList_0() { return &___pivotList_0; }
	inline void set_pivotList_0(List_1_t1391797922 * value)
	{
		___pivotList_0 = value;
		Il2CppCodeGenWriteBarrier(&___pivotList_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CRecalculatePivotsU3Ec__AnonStorey11C_t2905139494, ___U3CU3Ef__this_1)); }
	inline EuclideanEmbedding_t908139023 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline EuclideanEmbedding_t908139023 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(EuclideanEmbedding_t908139023 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
