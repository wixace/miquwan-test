﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_foogel193
struct M_foogel193_t480650371;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_foogel193::.ctor()
extern "C"  void M_foogel193__ctor_m1209819200 (M_foogel193_t480650371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_foogel193::M_marle0(System.String[],System.Int32)
extern "C"  void M_foogel193_M_marle0_m680045382 (M_foogel193_t480650371 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
