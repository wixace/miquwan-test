﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_lisitur164
struct  M_lisitur164_t1531754657  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_lisitur164::_kemas
	uint32_t ____kemas_0;
	// System.UInt32 GarbageiOS.M_lisitur164::_naljakis
	uint32_t ____naljakis_1;
	// System.String GarbageiOS.M_lisitur164::_nasi
	String_t* ____nasi_2;
	// System.UInt32 GarbageiOS.M_lisitur164::_cawcouPidoo
	uint32_t ____cawcouPidoo_3;
	// System.Int32 GarbageiOS.M_lisitur164::_hepartiKeechas
	int32_t ____hepartiKeechas_4;
	// System.Single GarbageiOS.M_lisitur164::_mowdere
	float ____mowdere_5;
	// System.Int32 GarbageiOS.M_lisitur164::_fasbearweKelsairjal
	int32_t ____fasbearweKelsairjal_6;
	// System.String GarbageiOS.M_lisitur164::_sicuChaysaw
	String_t* ____sicuChaysaw_7;
	// System.String GarbageiOS.M_lisitur164::_bewayser
	String_t* ____bewayser_8;
	// System.Int32 GarbageiOS.M_lisitur164::_naideke
	int32_t ____naideke_9;
	// System.Single GarbageiOS.M_lisitur164::_taskaylir
	float ____taskaylir_10;
	// System.Single GarbageiOS.M_lisitur164::_paqaysas
	float ____paqaysas_11;
	// System.Int32 GarbageiOS.M_lisitur164::_jater
	int32_t ____jater_12;
	// System.UInt32 GarbageiOS.M_lisitur164::_baleniTurye
	uint32_t ____baleniTurye_13;
	// System.String GarbageiOS.M_lisitur164::_wairsehe
	String_t* ____wairsehe_14;
	// System.Int32 GarbageiOS.M_lisitur164::_seljeagaFelainar
	int32_t ____seljeagaFelainar_15;
	// System.UInt32 GarbageiOS.M_lisitur164::_destada
	uint32_t ____destada_16;
	// System.UInt32 GarbageiOS.M_lisitur164::_drayga
	uint32_t ____drayga_17;
	// System.UInt32 GarbageiOS.M_lisitur164::_ferexoKernoukas
	uint32_t ____ferexoKernoukas_18;
	// System.Int32 GarbageiOS.M_lisitur164::_sahasa
	int32_t ____sahasa_19;
	// System.Single GarbageiOS.M_lisitur164::_feeje
	float ____feeje_20;
	// System.UInt32 GarbageiOS.M_lisitur164::_korere
	uint32_t ____korere_21;
	// System.Single GarbageiOS.M_lisitur164::_nunem
	float ____nunem_22;
	// System.Single GarbageiOS.M_lisitur164::_lastouJerdru
	float ____lastouJerdru_23;
	// System.String GarbageiOS.M_lisitur164::_gaizayMisur
	String_t* ____gaizayMisur_24;
	// System.Boolean GarbageiOS.M_lisitur164::_yawsisnis
	bool ____yawsisnis_25;
	// System.String GarbageiOS.M_lisitur164::_kertuFawtisdra
	String_t* ____kertuFawtisdra_26;
	// System.Boolean GarbageiOS.M_lisitur164::_pereledrow
	bool ____pereledrow_27;
	// System.Single GarbageiOS.M_lisitur164::_bafearisLearnujear
	float ____bafearisLearnujear_28;
	// System.Single GarbageiOS.M_lisitur164::_pearciGejoufear
	float ____pearciGejoufear_29;
	// System.UInt32 GarbageiOS.M_lisitur164::_felwouki
	uint32_t ____felwouki_30;
	// System.UInt32 GarbageiOS.M_lisitur164::_rerzay
	uint32_t ____rerzay_31;
	// System.Boolean GarbageiOS.M_lisitur164::_pisoxelDraisuce
	bool ____pisoxelDraisuce_32;
	// System.String GarbageiOS.M_lisitur164::_dorgorhowSoone
	String_t* ____dorgorhowSoone_33;
	// System.String GarbageiOS.M_lisitur164::_beqortreJeka
	String_t* ____beqortreJeka_34;
	// System.Int32 GarbageiOS.M_lisitur164::_dekunowWortis
	int32_t ____dekunowWortis_35;
	// System.Boolean GarbageiOS.M_lisitur164::_wircabur
	bool ____wircabur_36;
	// System.Boolean GarbageiOS.M_lisitur164::_muzowna
	bool ____muzowna_37;
	// System.Int32 GarbageiOS.M_lisitur164::_tirgogeSocea
	int32_t ____tirgogeSocea_38;
	// System.Int32 GarbageiOS.M_lisitur164::_nasmouce
	int32_t ____nasmouce_39;
	// System.Int32 GarbageiOS.M_lisitur164::_mucowhayJorsoye
	int32_t ____mucowhayJorsoye_40;
	// System.String GarbageiOS.M_lisitur164::_vasrasraHasura
	String_t* ____vasrasraHasura_41;
	// System.Boolean GarbageiOS.M_lisitur164::_hastafisWouvounear
	bool ____hastafisWouvounear_42;
	// System.Boolean GarbageiOS.M_lisitur164::_zowdearnooStisoo
	bool ____zowdearnooStisoo_43;
	// System.String GarbageiOS.M_lisitur164::_louhir
	String_t* ____louhir_44;
	// System.UInt32 GarbageiOS.M_lisitur164::_rokojeComedrem
	uint32_t ____rokojeComedrem_45;

public:
	inline static int32_t get_offset_of__kemas_0() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____kemas_0)); }
	inline uint32_t get__kemas_0() const { return ____kemas_0; }
	inline uint32_t* get_address_of__kemas_0() { return &____kemas_0; }
	inline void set__kemas_0(uint32_t value)
	{
		____kemas_0 = value;
	}

	inline static int32_t get_offset_of__naljakis_1() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____naljakis_1)); }
	inline uint32_t get__naljakis_1() const { return ____naljakis_1; }
	inline uint32_t* get_address_of__naljakis_1() { return &____naljakis_1; }
	inline void set__naljakis_1(uint32_t value)
	{
		____naljakis_1 = value;
	}

	inline static int32_t get_offset_of__nasi_2() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____nasi_2)); }
	inline String_t* get__nasi_2() const { return ____nasi_2; }
	inline String_t** get_address_of__nasi_2() { return &____nasi_2; }
	inline void set__nasi_2(String_t* value)
	{
		____nasi_2 = value;
		Il2CppCodeGenWriteBarrier(&____nasi_2, value);
	}

	inline static int32_t get_offset_of__cawcouPidoo_3() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____cawcouPidoo_3)); }
	inline uint32_t get__cawcouPidoo_3() const { return ____cawcouPidoo_3; }
	inline uint32_t* get_address_of__cawcouPidoo_3() { return &____cawcouPidoo_3; }
	inline void set__cawcouPidoo_3(uint32_t value)
	{
		____cawcouPidoo_3 = value;
	}

	inline static int32_t get_offset_of__hepartiKeechas_4() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____hepartiKeechas_4)); }
	inline int32_t get__hepartiKeechas_4() const { return ____hepartiKeechas_4; }
	inline int32_t* get_address_of__hepartiKeechas_4() { return &____hepartiKeechas_4; }
	inline void set__hepartiKeechas_4(int32_t value)
	{
		____hepartiKeechas_4 = value;
	}

	inline static int32_t get_offset_of__mowdere_5() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____mowdere_5)); }
	inline float get__mowdere_5() const { return ____mowdere_5; }
	inline float* get_address_of__mowdere_5() { return &____mowdere_5; }
	inline void set__mowdere_5(float value)
	{
		____mowdere_5 = value;
	}

	inline static int32_t get_offset_of__fasbearweKelsairjal_6() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____fasbearweKelsairjal_6)); }
	inline int32_t get__fasbearweKelsairjal_6() const { return ____fasbearweKelsairjal_6; }
	inline int32_t* get_address_of__fasbearweKelsairjal_6() { return &____fasbearweKelsairjal_6; }
	inline void set__fasbearweKelsairjal_6(int32_t value)
	{
		____fasbearweKelsairjal_6 = value;
	}

	inline static int32_t get_offset_of__sicuChaysaw_7() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____sicuChaysaw_7)); }
	inline String_t* get__sicuChaysaw_7() const { return ____sicuChaysaw_7; }
	inline String_t** get_address_of__sicuChaysaw_7() { return &____sicuChaysaw_7; }
	inline void set__sicuChaysaw_7(String_t* value)
	{
		____sicuChaysaw_7 = value;
		Il2CppCodeGenWriteBarrier(&____sicuChaysaw_7, value);
	}

	inline static int32_t get_offset_of__bewayser_8() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____bewayser_8)); }
	inline String_t* get__bewayser_8() const { return ____bewayser_8; }
	inline String_t** get_address_of__bewayser_8() { return &____bewayser_8; }
	inline void set__bewayser_8(String_t* value)
	{
		____bewayser_8 = value;
		Il2CppCodeGenWriteBarrier(&____bewayser_8, value);
	}

	inline static int32_t get_offset_of__naideke_9() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____naideke_9)); }
	inline int32_t get__naideke_9() const { return ____naideke_9; }
	inline int32_t* get_address_of__naideke_9() { return &____naideke_9; }
	inline void set__naideke_9(int32_t value)
	{
		____naideke_9 = value;
	}

	inline static int32_t get_offset_of__taskaylir_10() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____taskaylir_10)); }
	inline float get__taskaylir_10() const { return ____taskaylir_10; }
	inline float* get_address_of__taskaylir_10() { return &____taskaylir_10; }
	inline void set__taskaylir_10(float value)
	{
		____taskaylir_10 = value;
	}

	inline static int32_t get_offset_of__paqaysas_11() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____paqaysas_11)); }
	inline float get__paqaysas_11() const { return ____paqaysas_11; }
	inline float* get_address_of__paqaysas_11() { return &____paqaysas_11; }
	inline void set__paqaysas_11(float value)
	{
		____paqaysas_11 = value;
	}

	inline static int32_t get_offset_of__jater_12() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____jater_12)); }
	inline int32_t get__jater_12() const { return ____jater_12; }
	inline int32_t* get_address_of__jater_12() { return &____jater_12; }
	inline void set__jater_12(int32_t value)
	{
		____jater_12 = value;
	}

	inline static int32_t get_offset_of__baleniTurye_13() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____baleniTurye_13)); }
	inline uint32_t get__baleniTurye_13() const { return ____baleniTurye_13; }
	inline uint32_t* get_address_of__baleniTurye_13() { return &____baleniTurye_13; }
	inline void set__baleniTurye_13(uint32_t value)
	{
		____baleniTurye_13 = value;
	}

	inline static int32_t get_offset_of__wairsehe_14() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____wairsehe_14)); }
	inline String_t* get__wairsehe_14() const { return ____wairsehe_14; }
	inline String_t** get_address_of__wairsehe_14() { return &____wairsehe_14; }
	inline void set__wairsehe_14(String_t* value)
	{
		____wairsehe_14 = value;
		Il2CppCodeGenWriteBarrier(&____wairsehe_14, value);
	}

	inline static int32_t get_offset_of__seljeagaFelainar_15() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____seljeagaFelainar_15)); }
	inline int32_t get__seljeagaFelainar_15() const { return ____seljeagaFelainar_15; }
	inline int32_t* get_address_of__seljeagaFelainar_15() { return &____seljeagaFelainar_15; }
	inline void set__seljeagaFelainar_15(int32_t value)
	{
		____seljeagaFelainar_15 = value;
	}

	inline static int32_t get_offset_of__destada_16() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____destada_16)); }
	inline uint32_t get__destada_16() const { return ____destada_16; }
	inline uint32_t* get_address_of__destada_16() { return &____destada_16; }
	inline void set__destada_16(uint32_t value)
	{
		____destada_16 = value;
	}

	inline static int32_t get_offset_of__drayga_17() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____drayga_17)); }
	inline uint32_t get__drayga_17() const { return ____drayga_17; }
	inline uint32_t* get_address_of__drayga_17() { return &____drayga_17; }
	inline void set__drayga_17(uint32_t value)
	{
		____drayga_17 = value;
	}

	inline static int32_t get_offset_of__ferexoKernoukas_18() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____ferexoKernoukas_18)); }
	inline uint32_t get__ferexoKernoukas_18() const { return ____ferexoKernoukas_18; }
	inline uint32_t* get_address_of__ferexoKernoukas_18() { return &____ferexoKernoukas_18; }
	inline void set__ferexoKernoukas_18(uint32_t value)
	{
		____ferexoKernoukas_18 = value;
	}

	inline static int32_t get_offset_of__sahasa_19() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____sahasa_19)); }
	inline int32_t get__sahasa_19() const { return ____sahasa_19; }
	inline int32_t* get_address_of__sahasa_19() { return &____sahasa_19; }
	inline void set__sahasa_19(int32_t value)
	{
		____sahasa_19 = value;
	}

	inline static int32_t get_offset_of__feeje_20() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____feeje_20)); }
	inline float get__feeje_20() const { return ____feeje_20; }
	inline float* get_address_of__feeje_20() { return &____feeje_20; }
	inline void set__feeje_20(float value)
	{
		____feeje_20 = value;
	}

	inline static int32_t get_offset_of__korere_21() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____korere_21)); }
	inline uint32_t get__korere_21() const { return ____korere_21; }
	inline uint32_t* get_address_of__korere_21() { return &____korere_21; }
	inline void set__korere_21(uint32_t value)
	{
		____korere_21 = value;
	}

	inline static int32_t get_offset_of__nunem_22() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____nunem_22)); }
	inline float get__nunem_22() const { return ____nunem_22; }
	inline float* get_address_of__nunem_22() { return &____nunem_22; }
	inline void set__nunem_22(float value)
	{
		____nunem_22 = value;
	}

	inline static int32_t get_offset_of__lastouJerdru_23() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____lastouJerdru_23)); }
	inline float get__lastouJerdru_23() const { return ____lastouJerdru_23; }
	inline float* get_address_of__lastouJerdru_23() { return &____lastouJerdru_23; }
	inline void set__lastouJerdru_23(float value)
	{
		____lastouJerdru_23 = value;
	}

	inline static int32_t get_offset_of__gaizayMisur_24() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____gaizayMisur_24)); }
	inline String_t* get__gaizayMisur_24() const { return ____gaizayMisur_24; }
	inline String_t** get_address_of__gaizayMisur_24() { return &____gaizayMisur_24; }
	inline void set__gaizayMisur_24(String_t* value)
	{
		____gaizayMisur_24 = value;
		Il2CppCodeGenWriteBarrier(&____gaizayMisur_24, value);
	}

	inline static int32_t get_offset_of__yawsisnis_25() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____yawsisnis_25)); }
	inline bool get__yawsisnis_25() const { return ____yawsisnis_25; }
	inline bool* get_address_of__yawsisnis_25() { return &____yawsisnis_25; }
	inline void set__yawsisnis_25(bool value)
	{
		____yawsisnis_25 = value;
	}

	inline static int32_t get_offset_of__kertuFawtisdra_26() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____kertuFawtisdra_26)); }
	inline String_t* get__kertuFawtisdra_26() const { return ____kertuFawtisdra_26; }
	inline String_t** get_address_of__kertuFawtisdra_26() { return &____kertuFawtisdra_26; }
	inline void set__kertuFawtisdra_26(String_t* value)
	{
		____kertuFawtisdra_26 = value;
		Il2CppCodeGenWriteBarrier(&____kertuFawtisdra_26, value);
	}

	inline static int32_t get_offset_of__pereledrow_27() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____pereledrow_27)); }
	inline bool get__pereledrow_27() const { return ____pereledrow_27; }
	inline bool* get_address_of__pereledrow_27() { return &____pereledrow_27; }
	inline void set__pereledrow_27(bool value)
	{
		____pereledrow_27 = value;
	}

	inline static int32_t get_offset_of__bafearisLearnujear_28() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____bafearisLearnujear_28)); }
	inline float get__bafearisLearnujear_28() const { return ____bafearisLearnujear_28; }
	inline float* get_address_of__bafearisLearnujear_28() { return &____bafearisLearnujear_28; }
	inline void set__bafearisLearnujear_28(float value)
	{
		____bafearisLearnujear_28 = value;
	}

	inline static int32_t get_offset_of__pearciGejoufear_29() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____pearciGejoufear_29)); }
	inline float get__pearciGejoufear_29() const { return ____pearciGejoufear_29; }
	inline float* get_address_of__pearciGejoufear_29() { return &____pearciGejoufear_29; }
	inline void set__pearciGejoufear_29(float value)
	{
		____pearciGejoufear_29 = value;
	}

	inline static int32_t get_offset_of__felwouki_30() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____felwouki_30)); }
	inline uint32_t get__felwouki_30() const { return ____felwouki_30; }
	inline uint32_t* get_address_of__felwouki_30() { return &____felwouki_30; }
	inline void set__felwouki_30(uint32_t value)
	{
		____felwouki_30 = value;
	}

	inline static int32_t get_offset_of__rerzay_31() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____rerzay_31)); }
	inline uint32_t get__rerzay_31() const { return ____rerzay_31; }
	inline uint32_t* get_address_of__rerzay_31() { return &____rerzay_31; }
	inline void set__rerzay_31(uint32_t value)
	{
		____rerzay_31 = value;
	}

	inline static int32_t get_offset_of__pisoxelDraisuce_32() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____pisoxelDraisuce_32)); }
	inline bool get__pisoxelDraisuce_32() const { return ____pisoxelDraisuce_32; }
	inline bool* get_address_of__pisoxelDraisuce_32() { return &____pisoxelDraisuce_32; }
	inline void set__pisoxelDraisuce_32(bool value)
	{
		____pisoxelDraisuce_32 = value;
	}

	inline static int32_t get_offset_of__dorgorhowSoone_33() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____dorgorhowSoone_33)); }
	inline String_t* get__dorgorhowSoone_33() const { return ____dorgorhowSoone_33; }
	inline String_t** get_address_of__dorgorhowSoone_33() { return &____dorgorhowSoone_33; }
	inline void set__dorgorhowSoone_33(String_t* value)
	{
		____dorgorhowSoone_33 = value;
		Il2CppCodeGenWriteBarrier(&____dorgorhowSoone_33, value);
	}

	inline static int32_t get_offset_of__beqortreJeka_34() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____beqortreJeka_34)); }
	inline String_t* get__beqortreJeka_34() const { return ____beqortreJeka_34; }
	inline String_t** get_address_of__beqortreJeka_34() { return &____beqortreJeka_34; }
	inline void set__beqortreJeka_34(String_t* value)
	{
		____beqortreJeka_34 = value;
		Il2CppCodeGenWriteBarrier(&____beqortreJeka_34, value);
	}

	inline static int32_t get_offset_of__dekunowWortis_35() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____dekunowWortis_35)); }
	inline int32_t get__dekunowWortis_35() const { return ____dekunowWortis_35; }
	inline int32_t* get_address_of__dekunowWortis_35() { return &____dekunowWortis_35; }
	inline void set__dekunowWortis_35(int32_t value)
	{
		____dekunowWortis_35 = value;
	}

	inline static int32_t get_offset_of__wircabur_36() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____wircabur_36)); }
	inline bool get__wircabur_36() const { return ____wircabur_36; }
	inline bool* get_address_of__wircabur_36() { return &____wircabur_36; }
	inline void set__wircabur_36(bool value)
	{
		____wircabur_36 = value;
	}

	inline static int32_t get_offset_of__muzowna_37() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____muzowna_37)); }
	inline bool get__muzowna_37() const { return ____muzowna_37; }
	inline bool* get_address_of__muzowna_37() { return &____muzowna_37; }
	inline void set__muzowna_37(bool value)
	{
		____muzowna_37 = value;
	}

	inline static int32_t get_offset_of__tirgogeSocea_38() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____tirgogeSocea_38)); }
	inline int32_t get__tirgogeSocea_38() const { return ____tirgogeSocea_38; }
	inline int32_t* get_address_of__tirgogeSocea_38() { return &____tirgogeSocea_38; }
	inline void set__tirgogeSocea_38(int32_t value)
	{
		____tirgogeSocea_38 = value;
	}

	inline static int32_t get_offset_of__nasmouce_39() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____nasmouce_39)); }
	inline int32_t get__nasmouce_39() const { return ____nasmouce_39; }
	inline int32_t* get_address_of__nasmouce_39() { return &____nasmouce_39; }
	inline void set__nasmouce_39(int32_t value)
	{
		____nasmouce_39 = value;
	}

	inline static int32_t get_offset_of__mucowhayJorsoye_40() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____mucowhayJorsoye_40)); }
	inline int32_t get__mucowhayJorsoye_40() const { return ____mucowhayJorsoye_40; }
	inline int32_t* get_address_of__mucowhayJorsoye_40() { return &____mucowhayJorsoye_40; }
	inline void set__mucowhayJorsoye_40(int32_t value)
	{
		____mucowhayJorsoye_40 = value;
	}

	inline static int32_t get_offset_of__vasrasraHasura_41() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____vasrasraHasura_41)); }
	inline String_t* get__vasrasraHasura_41() const { return ____vasrasraHasura_41; }
	inline String_t** get_address_of__vasrasraHasura_41() { return &____vasrasraHasura_41; }
	inline void set__vasrasraHasura_41(String_t* value)
	{
		____vasrasraHasura_41 = value;
		Il2CppCodeGenWriteBarrier(&____vasrasraHasura_41, value);
	}

	inline static int32_t get_offset_of__hastafisWouvounear_42() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____hastafisWouvounear_42)); }
	inline bool get__hastafisWouvounear_42() const { return ____hastafisWouvounear_42; }
	inline bool* get_address_of__hastafisWouvounear_42() { return &____hastafisWouvounear_42; }
	inline void set__hastafisWouvounear_42(bool value)
	{
		____hastafisWouvounear_42 = value;
	}

	inline static int32_t get_offset_of__zowdearnooStisoo_43() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____zowdearnooStisoo_43)); }
	inline bool get__zowdearnooStisoo_43() const { return ____zowdearnooStisoo_43; }
	inline bool* get_address_of__zowdearnooStisoo_43() { return &____zowdearnooStisoo_43; }
	inline void set__zowdearnooStisoo_43(bool value)
	{
		____zowdearnooStisoo_43 = value;
	}

	inline static int32_t get_offset_of__louhir_44() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____louhir_44)); }
	inline String_t* get__louhir_44() const { return ____louhir_44; }
	inline String_t** get_address_of__louhir_44() { return &____louhir_44; }
	inline void set__louhir_44(String_t* value)
	{
		____louhir_44 = value;
		Il2CppCodeGenWriteBarrier(&____louhir_44, value);
	}

	inline static int32_t get_offset_of__rokojeComedrem_45() { return static_cast<int32_t>(offsetof(M_lisitur164_t1531754657, ____rokojeComedrem_45)); }
	inline uint32_t get__rokojeComedrem_45() const { return ____rokojeComedrem_45; }
	inline uint32_t* get_address_of__rokojeComedrem_45() { return &____rokojeComedrem_45; }
	inline void set__rokojeComedrem_45(uint32_t value)
	{
		____rokojeComedrem_45 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
