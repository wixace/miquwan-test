﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_DetailPrototypeGenerated
struct UnityEngine_DetailPrototypeGenerated_t1137593894;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_DetailPrototypeGenerated::.ctor()
extern "C"  void UnityEngine_DetailPrototypeGenerated__ctor_m2894161717 (UnityEngine_DetailPrototypeGenerated_t1137593894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DetailPrototypeGenerated::DetailPrototype_DetailPrototype1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DetailPrototypeGenerated_DetailPrototype_DetailPrototype1_m3243762433 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DetailPrototypeGenerated::DetailPrototype_prototype(JSVCall)
extern "C"  void UnityEngine_DetailPrototypeGenerated_DetailPrototype_prototype_m2937378624 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DetailPrototypeGenerated::DetailPrototype_prototypeTexture(JSVCall)
extern "C"  void UnityEngine_DetailPrototypeGenerated_DetailPrototype_prototypeTexture_m1409481713 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DetailPrototypeGenerated::DetailPrototype_minWidth(JSVCall)
extern "C"  void UnityEngine_DetailPrototypeGenerated_DetailPrototype_minWidth_m985754966 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DetailPrototypeGenerated::DetailPrototype_maxWidth(JSVCall)
extern "C"  void UnityEngine_DetailPrototypeGenerated_DetailPrototype_maxWidth_m2965677288 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DetailPrototypeGenerated::DetailPrototype_minHeight(JSVCall)
extern "C"  void UnityEngine_DetailPrototypeGenerated_DetailPrototype_minHeight_m1149076521 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DetailPrototypeGenerated::DetailPrototype_maxHeight(JSVCall)
extern "C"  void UnityEngine_DetailPrototypeGenerated_DetailPrototype_maxHeight_m2397126359 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DetailPrototypeGenerated::DetailPrototype_noiseSpread(JSVCall)
extern "C"  void UnityEngine_DetailPrototypeGenerated_DetailPrototype_noiseSpread_m164616341 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DetailPrototypeGenerated::DetailPrototype_bendFactor(JSVCall)
extern "C"  void UnityEngine_DetailPrototypeGenerated_DetailPrototype_bendFactor_m2636419458 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DetailPrototypeGenerated::DetailPrototype_healthyColor(JSVCall)
extern "C"  void UnityEngine_DetailPrototypeGenerated_DetailPrototype_healthyColor_m3350716004 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DetailPrototypeGenerated::DetailPrototype_dryColor(JSVCall)
extern "C"  void UnityEngine_DetailPrototypeGenerated_DetailPrototype_dryColor_m17398770 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DetailPrototypeGenerated::DetailPrototype_renderMode(JSVCall)
extern "C"  void UnityEngine_DetailPrototypeGenerated_DetailPrototype_renderMode_m3855464497 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DetailPrototypeGenerated::DetailPrototype_usePrototypeMesh(JSVCall)
extern "C"  void UnityEngine_DetailPrototypeGenerated_DetailPrototype_usePrototypeMesh_m4171568770 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DetailPrototypeGenerated::__Register()
extern "C"  void UnityEngine_DetailPrototypeGenerated___Register_m2136559282 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_DetailPrototypeGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_DetailPrototypeGenerated_ilo_getObject1_m4279855101 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DetailPrototypeGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_DetailPrototypeGenerated_ilo_addJSCSRel2_m1320692722 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DetailPrototypeGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void UnityEngine_DetailPrototypeGenerated_ilo_setSingle3_m1340786209 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_DetailPrototypeGenerated::ilo_getSingle4(System.Int32)
extern "C"  float UnityEngine_DetailPrototypeGenerated_ilo_getSingle4_m1027458517 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_DetailPrototypeGenerated::ilo_setObject5(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_DetailPrototypeGenerated_ilo_setObject5_m2263378041 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_DetailPrototypeGenerated::ilo_getObject6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_DetailPrototypeGenerated_ilo_getObject6_m1246624927 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
