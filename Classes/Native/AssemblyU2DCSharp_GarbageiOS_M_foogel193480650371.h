﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_foogel193
struct  M_foogel193_t480650371  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_foogel193::_mellarrer
	float ____mellarrer_0;
	// System.Boolean GarbageiOS.M_foogel193::_nepoosi
	bool ____nepoosi_1;
	// System.Boolean GarbageiOS.M_foogel193::_casrokee
	bool ____casrokee_2;
	// System.Int32 GarbageiOS.M_foogel193::_woreaDowballgel
	int32_t ____woreaDowballgel_3;
	// System.UInt32 GarbageiOS.M_foogel193::_weasear
	uint32_t ____weasear_4;
	// System.String GarbageiOS.M_foogel193::_zoogeVupabor
	String_t* ____zoogeVupabor_5;
	// System.Int32 GarbageiOS.M_foogel193::_neamafairNirme
	int32_t ____neamafairNirme_6;

public:
	inline static int32_t get_offset_of__mellarrer_0() { return static_cast<int32_t>(offsetof(M_foogel193_t480650371, ____mellarrer_0)); }
	inline float get__mellarrer_0() const { return ____mellarrer_0; }
	inline float* get_address_of__mellarrer_0() { return &____mellarrer_0; }
	inline void set__mellarrer_0(float value)
	{
		____mellarrer_0 = value;
	}

	inline static int32_t get_offset_of__nepoosi_1() { return static_cast<int32_t>(offsetof(M_foogel193_t480650371, ____nepoosi_1)); }
	inline bool get__nepoosi_1() const { return ____nepoosi_1; }
	inline bool* get_address_of__nepoosi_1() { return &____nepoosi_1; }
	inline void set__nepoosi_1(bool value)
	{
		____nepoosi_1 = value;
	}

	inline static int32_t get_offset_of__casrokee_2() { return static_cast<int32_t>(offsetof(M_foogel193_t480650371, ____casrokee_2)); }
	inline bool get__casrokee_2() const { return ____casrokee_2; }
	inline bool* get_address_of__casrokee_2() { return &____casrokee_2; }
	inline void set__casrokee_2(bool value)
	{
		____casrokee_2 = value;
	}

	inline static int32_t get_offset_of__woreaDowballgel_3() { return static_cast<int32_t>(offsetof(M_foogel193_t480650371, ____woreaDowballgel_3)); }
	inline int32_t get__woreaDowballgel_3() const { return ____woreaDowballgel_3; }
	inline int32_t* get_address_of__woreaDowballgel_3() { return &____woreaDowballgel_3; }
	inline void set__woreaDowballgel_3(int32_t value)
	{
		____woreaDowballgel_3 = value;
	}

	inline static int32_t get_offset_of__weasear_4() { return static_cast<int32_t>(offsetof(M_foogel193_t480650371, ____weasear_4)); }
	inline uint32_t get__weasear_4() const { return ____weasear_4; }
	inline uint32_t* get_address_of__weasear_4() { return &____weasear_4; }
	inline void set__weasear_4(uint32_t value)
	{
		____weasear_4 = value;
	}

	inline static int32_t get_offset_of__zoogeVupabor_5() { return static_cast<int32_t>(offsetof(M_foogel193_t480650371, ____zoogeVupabor_5)); }
	inline String_t* get__zoogeVupabor_5() const { return ____zoogeVupabor_5; }
	inline String_t** get_address_of__zoogeVupabor_5() { return &____zoogeVupabor_5; }
	inline void set__zoogeVupabor_5(String_t* value)
	{
		____zoogeVupabor_5 = value;
		Il2CppCodeGenWriteBarrier(&____zoogeVupabor_5, value);
	}

	inline static int32_t get_offset_of__neamafairNirme_6() { return static_cast<int32_t>(offsetof(M_foogel193_t480650371, ____neamafairNirme_6)); }
	inline int32_t get__neamafairNirme_6() const { return ____neamafairNirme_6; }
	inline int32_t* get_address_of__neamafairNirme_6() { return &____neamafairNirme_6; }
	inline void set__neamafairNirme_6(int32_t value)
	{
		____neamafairNirme_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
