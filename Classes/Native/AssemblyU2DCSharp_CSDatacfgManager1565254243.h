﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Int32,ChannelConfigCfg>
struct Dictionary_2_t4270243238;
// System.Collections.Generic.Dictionary`2<System.Int32,HeroEmbattleCfg>
struct Dictionary_2_t4131387889;
// System.Collections.Generic.Dictionary`2<System.Int32,JJCCoefficientCfg>
struct Dictionary_2_t2752314969;
// System.Collections.Generic.Dictionary`2<System.Int32,PetsCfg>
struct Dictionary_2_t985279991;
// System.Collections.Generic.Dictionary`2<System.Int32,SoundCfg>
struct Dictionary_2_t1804538492;
// System.Collections.Generic.Dictionary`2<System.Int32,StoryCfg>
struct Dictionary_2_t1779634390;
// System.Collections.Generic.Dictionary`2<System.Int32,TextStringCfg>
struct Dictionary_2_t881798477;
// System.Collections.Generic.Dictionary`2<System.Int32,UIEffectCfg>
struct Dictionary_2_t949916262;
// System.Collections.Generic.Dictionary`2<System.Int32,all_configCfg>
struct Dictionary_2_t3862331947;
// System.Collections.Generic.Dictionary`2<System.Int32,asset_sharedCfg>
struct Dictionary_2_t1898577079;
// System.Collections.Generic.Dictionary`2<System.Int32,barrierGateCfg>
struct Dictionary_2_t3702352233;
// System.Collections.Generic.Dictionary`2<System.Int32,buffCfg>
struct Dictionary_2_t225226904;
// System.Collections.Generic.Dictionary`2<System.Int32,cameraShotCfg>
struct Dictionary_2_t778420652;
// System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>
struct Dictionary_2_t2813371203;
// System.Collections.Generic.Dictionary`2<System.Int32,demoStoryCfg>
struct Dictionary_2_t1990425529;
// System.Collections.Generic.Dictionary`2<System.Int32,effectCfg>
struct Dictionary_2_t2823542426;
// System.Collections.Generic.Dictionary`2<System.Int32,elementCfg>
struct Dictionary_2_t573175119;
// System.Collections.Generic.Dictionary`2<System.Int32,enemy_dropCfg>
struct Dictionary_2_t1024223941;
// System.Collections.Generic.Dictionary`2<System.Int32,friendNpcCfg>
struct Dictionary_2_t3887573896;
// System.Collections.Generic.Dictionary`2<System.Int32,guideCfg>
struct Dictionary_2_t2978306383;
// System.Collections.Generic.Dictionary`2<System.Int32,herosCfg>
struct Dictionary_2_t3674197874;
// System.Collections.Generic.Dictionary`2<System.Int32,monstersCfg>
struct Dictionary_2_t1539659602;
// System.Collections.Generic.Dictionary`2<System.Int32,npcAICfg>
struct Dictionary_2_t1945593442;
// System.Collections.Generic.Dictionary`2<System.Int32,npcAIBehaviorCfg>
struct Dictionary_2_t3454000048;
// System.Collections.Generic.Dictionary`2<System.Int32,npcAIConditionCfg>
struct Dictionary_2_t927460409;
// System.Collections.Generic.Dictionary`2<System.Int32,portalCfg>
struct Dictionary_2_t1114297823;
// System.Collections.Generic.Dictionary`2<System.Int32,skillCfg>
struct Dictionary_2_t2139688410;
// System.Collections.Generic.Dictionary`2<System.Int32,skill_upgradeCfg>
struct Dictionary_2_t787989725;
// System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>
struct Dictionary_2_t3021394517;
// System.Collections.Generic.Dictionary`2<System.Int32,taixuTipCfg>
struct Dictionary_2_t1265985609;
// System.Collections.Generic.Dictionary`2<System.Int32,towerbuffCfg>
struct Dictionary_2_t1753120111;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSDatacfgManager
struct  CSDatacfgManager_t1565254243  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,ChannelConfigCfg> CSDatacfgManager::ChannelConfigCfgDic
	Dictionary_2_t4270243238 * ___ChannelConfigCfgDic_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,HeroEmbattleCfg> CSDatacfgManager::HeroEmbattleCfgDic
	Dictionary_2_t4131387889 * ___HeroEmbattleCfgDic_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,JJCCoefficientCfg> CSDatacfgManager::JJCCoefficientCfgDic
	Dictionary_2_t2752314969 * ___JJCCoefficientCfgDic_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,PetsCfg> CSDatacfgManager::PetsCfgDic
	Dictionary_2_t985279991 * ___PetsCfgDic_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,SoundCfg> CSDatacfgManager::SoundCfgDic
	Dictionary_2_t1804538492 * ___SoundCfgDic_4;
	// System.Collections.Generic.Dictionary`2<System.Int32,StoryCfg> CSDatacfgManager::StoryCfgDic
	Dictionary_2_t1779634390 * ___StoryCfgDic_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,TextStringCfg> CSDatacfgManager::TextStringCfgDic
	Dictionary_2_t881798477 * ___TextStringCfgDic_6;
	// System.Collections.Generic.Dictionary`2<System.Int32,UIEffectCfg> CSDatacfgManager::UIEffectCfgDic
	Dictionary_2_t949916262 * ___UIEffectCfgDic_7;
	// System.Collections.Generic.Dictionary`2<System.Int32,all_configCfg> CSDatacfgManager::all_configCfgDic
	Dictionary_2_t3862331947 * ___all_configCfgDic_8;
	// System.Collections.Generic.Dictionary`2<System.Int32,asset_sharedCfg> CSDatacfgManager::asset_sharedCfgDic
	Dictionary_2_t1898577079 * ___asset_sharedCfgDic_9;
	// System.Collections.Generic.Dictionary`2<System.Int32,barrierGateCfg> CSDatacfgManager::barrierGateCfgDic
	Dictionary_2_t3702352233 * ___barrierGateCfgDic_10;
	// System.Collections.Generic.Dictionary`2<System.Int32,buffCfg> CSDatacfgManager::buffCfgDic
	Dictionary_2_t225226904 * ___buffCfgDic_11;
	// System.Collections.Generic.Dictionary`2<System.Int32,cameraShotCfg> CSDatacfgManager::cameraShotCfgDic
	Dictionary_2_t778420652 * ___cameraShotCfgDic_12;
	// System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg> CSDatacfgManager::checkpointCfgDic
	Dictionary_2_t2813371203 * ___checkpointCfgDic_13;
	// System.Collections.Generic.Dictionary`2<System.Int32,demoStoryCfg> CSDatacfgManager::demoStoryCfgDic
	Dictionary_2_t1990425529 * ___demoStoryCfgDic_14;
	// System.Collections.Generic.Dictionary`2<System.Int32,effectCfg> CSDatacfgManager::effectCfgDic
	Dictionary_2_t2823542426 * ___effectCfgDic_15;
	// System.Collections.Generic.Dictionary`2<System.Int32,elementCfg> CSDatacfgManager::elementCfgDic
	Dictionary_2_t573175119 * ___elementCfgDic_16;
	// System.Collections.Generic.Dictionary`2<System.Int32,enemy_dropCfg> CSDatacfgManager::enemy_dropCfgDic
	Dictionary_2_t1024223941 * ___enemy_dropCfgDic_17;
	// System.Collections.Generic.Dictionary`2<System.Int32,friendNpcCfg> CSDatacfgManager::friendNpcCfgDic
	Dictionary_2_t3887573896 * ___friendNpcCfgDic_18;
	// System.Collections.Generic.Dictionary`2<System.Int32,guideCfg> CSDatacfgManager::guideCfgDic
	Dictionary_2_t2978306383 * ___guideCfgDic_19;
	// System.Collections.Generic.Dictionary`2<System.Int32,herosCfg> CSDatacfgManager::herosCfgDic
	Dictionary_2_t3674197874 * ___herosCfgDic_20;
	// System.Collections.Generic.Dictionary`2<System.Int32,monstersCfg> CSDatacfgManager::monstersCfgDic
	Dictionary_2_t1539659602 * ___monstersCfgDic_21;
	// System.Collections.Generic.Dictionary`2<System.Int32,npcAICfg> CSDatacfgManager::npcAICfgDic
	Dictionary_2_t1945593442 * ___npcAICfgDic_22;
	// System.Collections.Generic.Dictionary`2<System.Int32,npcAIBehaviorCfg> CSDatacfgManager::npcAIBehaviorCfgDic
	Dictionary_2_t3454000048 * ___npcAIBehaviorCfgDic_23;
	// System.Collections.Generic.Dictionary`2<System.Int32,npcAIConditionCfg> CSDatacfgManager::npcAIConditionCfgDic
	Dictionary_2_t927460409 * ___npcAIConditionCfgDic_24;
	// System.Collections.Generic.Dictionary`2<System.Int32,portalCfg> CSDatacfgManager::portalCfgDic
	Dictionary_2_t1114297823 * ___portalCfgDic_25;
	// System.Collections.Generic.Dictionary`2<System.Int32,skillCfg> CSDatacfgManager::skillCfgDic
	Dictionary_2_t2139688410 * ___skillCfgDic_26;
	// System.Collections.Generic.Dictionary`2<System.Int32,skill_upgradeCfg> CSDatacfgManager::skill_upgradeCfgDic
	Dictionary_2_t787989725 * ___skill_upgradeCfgDic_27;
	// System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg> CSDatacfgManager::superskillCfgDic
	Dictionary_2_t3021394517 * ___superskillCfgDic_28;
	// System.Collections.Generic.Dictionary`2<System.Int32,taixuTipCfg> CSDatacfgManager::taixuTipCfgDic
	Dictionary_2_t1265985609 * ___taixuTipCfgDic_29;
	// System.Collections.Generic.Dictionary`2<System.Int32,towerbuffCfg> CSDatacfgManager::towerbuffCfgDic
	Dictionary_2_t1753120111 * ___towerbuffCfgDic_30;

public:
	inline static int32_t get_offset_of_ChannelConfigCfgDic_0() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___ChannelConfigCfgDic_0)); }
	inline Dictionary_2_t4270243238 * get_ChannelConfigCfgDic_0() const { return ___ChannelConfigCfgDic_0; }
	inline Dictionary_2_t4270243238 ** get_address_of_ChannelConfigCfgDic_0() { return &___ChannelConfigCfgDic_0; }
	inline void set_ChannelConfigCfgDic_0(Dictionary_2_t4270243238 * value)
	{
		___ChannelConfigCfgDic_0 = value;
		Il2CppCodeGenWriteBarrier(&___ChannelConfigCfgDic_0, value);
	}

	inline static int32_t get_offset_of_HeroEmbattleCfgDic_1() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___HeroEmbattleCfgDic_1)); }
	inline Dictionary_2_t4131387889 * get_HeroEmbattleCfgDic_1() const { return ___HeroEmbattleCfgDic_1; }
	inline Dictionary_2_t4131387889 ** get_address_of_HeroEmbattleCfgDic_1() { return &___HeroEmbattleCfgDic_1; }
	inline void set_HeroEmbattleCfgDic_1(Dictionary_2_t4131387889 * value)
	{
		___HeroEmbattleCfgDic_1 = value;
		Il2CppCodeGenWriteBarrier(&___HeroEmbattleCfgDic_1, value);
	}

	inline static int32_t get_offset_of_JJCCoefficientCfgDic_2() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___JJCCoefficientCfgDic_2)); }
	inline Dictionary_2_t2752314969 * get_JJCCoefficientCfgDic_2() const { return ___JJCCoefficientCfgDic_2; }
	inline Dictionary_2_t2752314969 ** get_address_of_JJCCoefficientCfgDic_2() { return &___JJCCoefficientCfgDic_2; }
	inline void set_JJCCoefficientCfgDic_2(Dictionary_2_t2752314969 * value)
	{
		___JJCCoefficientCfgDic_2 = value;
		Il2CppCodeGenWriteBarrier(&___JJCCoefficientCfgDic_2, value);
	}

	inline static int32_t get_offset_of_PetsCfgDic_3() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___PetsCfgDic_3)); }
	inline Dictionary_2_t985279991 * get_PetsCfgDic_3() const { return ___PetsCfgDic_3; }
	inline Dictionary_2_t985279991 ** get_address_of_PetsCfgDic_3() { return &___PetsCfgDic_3; }
	inline void set_PetsCfgDic_3(Dictionary_2_t985279991 * value)
	{
		___PetsCfgDic_3 = value;
		Il2CppCodeGenWriteBarrier(&___PetsCfgDic_3, value);
	}

	inline static int32_t get_offset_of_SoundCfgDic_4() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___SoundCfgDic_4)); }
	inline Dictionary_2_t1804538492 * get_SoundCfgDic_4() const { return ___SoundCfgDic_4; }
	inline Dictionary_2_t1804538492 ** get_address_of_SoundCfgDic_4() { return &___SoundCfgDic_4; }
	inline void set_SoundCfgDic_4(Dictionary_2_t1804538492 * value)
	{
		___SoundCfgDic_4 = value;
		Il2CppCodeGenWriteBarrier(&___SoundCfgDic_4, value);
	}

	inline static int32_t get_offset_of_StoryCfgDic_5() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___StoryCfgDic_5)); }
	inline Dictionary_2_t1779634390 * get_StoryCfgDic_5() const { return ___StoryCfgDic_5; }
	inline Dictionary_2_t1779634390 ** get_address_of_StoryCfgDic_5() { return &___StoryCfgDic_5; }
	inline void set_StoryCfgDic_5(Dictionary_2_t1779634390 * value)
	{
		___StoryCfgDic_5 = value;
		Il2CppCodeGenWriteBarrier(&___StoryCfgDic_5, value);
	}

	inline static int32_t get_offset_of_TextStringCfgDic_6() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___TextStringCfgDic_6)); }
	inline Dictionary_2_t881798477 * get_TextStringCfgDic_6() const { return ___TextStringCfgDic_6; }
	inline Dictionary_2_t881798477 ** get_address_of_TextStringCfgDic_6() { return &___TextStringCfgDic_6; }
	inline void set_TextStringCfgDic_6(Dictionary_2_t881798477 * value)
	{
		___TextStringCfgDic_6 = value;
		Il2CppCodeGenWriteBarrier(&___TextStringCfgDic_6, value);
	}

	inline static int32_t get_offset_of_UIEffectCfgDic_7() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___UIEffectCfgDic_7)); }
	inline Dictionary_2_t949916262 * get_UIEffectCfgDic_7() const { return ___UIEffectCfgDic_7; }
	inline Dictionary_2_t949916262 ** get_address_of_UIEffectCfgDic_7() { return &___UIEffectCfgDic_7; }
	inline void set_UIEffectCfgDic_7(Dictionary_2_t949916262 * value)
	{
		___UIEffectCfgDic_7 = value;
		Il2CppCodeGenWriteBarrier(&___UIEffectCfgDic_7, value);
	}

	inline static int32_t get_offset_of_all_configCfgDic_8() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___all_configCfgDic_8)); }
	inline Dictionary_2_t3862331947 * get_all_configCfgDic_8() const { return ___all_configCfgDic_8; }
	inline Dictionary_2_t3862331947 ** get_address_of_all_configCfgDic_8() { return &___all_configCfgDic_8; }
	inline void set_all_configCfgDic_8(Dictionary_2_t3862331947 * value)
	{
		___all_configCfgDic_8 = value;
		Il2CppCodeGenWriteBarrier(&___all_configCfgDic_8, value);
	}

	inline static int32_t get_offset_of_asset_sharedCfgDic_9() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___asset_sharedCfgDic_9)); }
	inline Dictionary_2_t1898577079 * get_asset_sharedCfgDic_9() const { return ___asset_sharedCfgDic_9; }
	inline Dictionary_2_t1898577079 ** get_address_of_asset_sharedCfgDic_9() { return &___asset_sharedCfgDic_9; }
	inline void set_asset_sharedCfgDic_9(Dictionary_2_t1898577079 * value)
	{
		___asset_sharedCfgDic_9 = value;
		Il2CppCodeGenWriteBarrier(&___asset_sharedCfgDic_9, value);
	}

	inline static int32_t get_offset_of_barrierGateCfgDic_10() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___barrierGateCfgDic_10)); }
	inline Dictionary_2_t3702352233 * get_barrierGateCfgDic_10() const { return ___barrierGateCfgDic_10; }
	inline Dictionary_2_t3702352233 ** get_address_of_barrierGateCfgDic_10() { return &___barrierGateCfgDic_10; }
	inline void set_barrierGateCfgDic_10(Dictionary_2_t3702352233 * value)
	{
		___barrierGateCfgDic_10 = value;
		Il2CppCodeGenWriteBarrier(&___barrierGateCfgDic_10, value);
	}

	inline static int32_t get_offset_of_buffCfgDic_11() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___buffCfgDic_11)); }
	inline Dictionary_2_t225226904 * get_buffCfgDic_11() const { return ___buffCfgDic_11; }
	inline Dictionary_2_t225226904 ** get_address_of_buffCfgDic_11() { return &___buffCfgDic_11; }
	inline void set_buffCfgDic_11(Dictionary_2_t225226904 * value)
	{
		___buffCfgDic_11 = value;
		Il2CppCodeGenWriteBarrier(&___buffCfgDic_11, value);
	}

	inline static int32_t get_offset_of_cameraShotCfgDic_12() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___cameraShotCfgDic_12)); }
	inline Dictionary_2_t778420652 * get_cameraShotCfgDic_12() const { return ___cameraShotCfgDic_12; }
	inline Dictionary_2_t778420652 ** get_address_of_cameraShotCfgDic_12() { return &___cameraShotCfgDic_12; }
	inline void set_cameraShotCfgDic_12(Dictionary_2_t778420652 * value)
	{
		___cameraShotCfgDic_12 = value;
		Il2CppCodeGenWriteBarrier(&___cameraShotCfgDic_12, value);
	}

	inline static int32_t get_offset_of_checkpointCfgDic_13() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___checkpointCfgDic_13)); }
	inline Dictionary_2_t2813371203 * get_checkpointCfgDic_13() const { return ___checkpointCfgDic_13; }
	inline Dictionary_2_t2813371203 ** get_address_of_checkpointCfgDic_13() { return &___checkpointCfgDic_13; }
	inline void set_checkpointCfgDic_13(Dictionary_2_t2813371203 * value)
	{
		___checkpointCfgDic_13 = value;
		Il2CppCodeGenWriteBarrier(&___checkpointCfgDic_13, value);
	}

	inline static int32_t get_offset_of_demoStoryCfgDic_14() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___demoStoryCfgDic_14)); }
	inline Dictionary_2_t1990425529 * get_demoStoryCfgDic_14() const { return ___demoStoryCfgDic_14; }
	inline Dictionary_2_t1990425529 ** get_address_of_demoStoryCfgDic_14() { return &___demoStoryCfgDic_14; }
	inline void set_demoStoryCfgDic_14(Dictionary_2_t1990425529 * value)
	{
		___demoStoryCfgDic_14 = value;
		Il2CppCodeGenWriteBarrier(&___demoStoryCfgDic_14, value);
	}

	inline static int32_t get_offset_of_effectCfgDic_15() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___effectCfgDic_15)); }
	inline Dictionary_2_t2823542426 * get_effectCfgDic_15() const { return ___effectCfgDic_15; }
	inline Dictionary_2_t2823542426 ** get_address_of_effectCfgDic_15() { return &___effectCfgDic_15; }
	inline void set_effectCfgDic_15(Dictionary_2_t2823542426 * value)
	{
		___effectCfgDic_15 = value;
		Il2CppCodeGenWriteBarrier(&___effectCfgDic_15, value);
	}

	inline static int32_t get_offset_of_elementCfgDic_16() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___elementCfgDic_16)); }
	inline Dictionary_2_t573175119 * get_elementCfgDic_16() const { return ___elementCfgDic_16; }
	inline Dictionary_2_t573175119 ** get_address_of_elementCfgDic_16() { return &___elementCfgDic_16; }
	inline void set_elementCfgDic_16(Dictionary_2_t573175119 * value)
	{
		___elementCfgDic_16 = value;
		Il2CppCodeGenWriteBarrier(&___elementCfgDic_16, value);
	}

	inline static int32_t get_offset_of_enemy_dropCfgDic_17() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___enemy_dropCfgDic_17)); }
	inline Dictionary_2_t1024223941 * get_enemy_dropCfgDic_17() const { return ___enemy_dropCfgDic_17; }
	inline Dictionary_2_t1024223941 ** get_address_of_enemy_dropCfgDic_17() { return &___enemy_dropCfgDic_17; }
	inline void set_enemy_dropCfgDic_17(Dictionary_2_t1024223941 * value)
	{
		___enemy_dropCfgDic_17 = value;
		Il2CppCodeGenWriteBarrier(&___enemy_dropCfgDic_17, value);
	}

	inline static int32_t get_offset_of_friendNpcCfgDic_18() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___friendNpcCfgDic_18)); }
	inline Dictionary_2_t3887573896 * get_friendNpcCfgDic_18() const { return ___friendNpcCfgDic_18; }
	inline Dictionary_2_t3887573896 ** get_address_of_friendNpcCfgDic_18() { return &___friendNpcCfgDic_18; }
	inline void set_friendNpcCfgDic_18(Dictionary_2_t3887573896 * value)
	{
		___friendNpcCfgDic_18 = value;
		Il2CppCodeGenWriteBarrier(&___friendNpcCfgDic_18, value);
	}

	inline static int32_t get_offset_of_guideCfgDic_19() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___guideCfgDic_19)); }
	inline Dictionary_2_t2978306383 * get_guideCfgDic_19() const { return ___guideCfgDic_19; }
	inline Dictionary_2_t2978306383 ** get_address_of_guideCfgDic_19() { return &___guideCfgDic_19; }
	inline void set_guideCfgDic_19(Dictionary_2_t2978306383 * value)
	{
		___guideCfgDic_19 = value;
		Il2CppCodeGenWriteBarrier(&___guideCfgDic_19, value);
	}

	inline static int32_t get_offset_of_herosCfgDic_20() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___herosCfgDic_20)); }
	inline Dictionary_2_t3674197874 * get_herosCfgDic_20() const { return ___herosCfgDic_20; }
	inline Dictionary_2_t3674197874 ** get_address_of_herosCfgDic_20() { return &___herosCfgDic_20; }
	inline void set_herosCfgDic_20(Dictionary_2_t3674197874 * value)
	{
		___herosCfgDic_20 = value;
		Il2CppCodeGenWriteBarrier(&___herosCfgDic_20, value);
	}

	inline static int32_t get_offset_of_monstersCfgDic_21() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___monstersCfgDic_21)); }
	inline Dictionary_2_t1539659602 * get_monstersCfgDic_21() const { return ___monstersCfgDic_21; }
	inline Dictionary_2_t1539659602 ** get_address_of_monstersCfgDic_21() { return &___monstersCfgDic_21; }
	inline void set_monstersCfgDic_21(Dictionary_2_t1539659602 * value)
	{
		___monstersCfgDic_21 = value;
		Il2CppCodeGenWriteBarrier(&___monstersCfgDic_21, value);
	}

	inline static int32_t get_offset_of_npcAICfgDic_22() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___npcAICfgDic_22)); }
	inline Dictionary_2_t1945593442 * get_npcAICfgDic_22() const { return ___npcAICfgDic_22; }
	inline Dictionary_2_t1945593442 ** get_address_of_npcAICfgDic_22() { return &___npcAICfgDic_22; }
	inline void set_npcAICfgDic_22(Dictionary_2_t1945593442 * value)
	{
		___npcAICfgDic_22 = value;
		Il2CppCodeGenWriteBarrier(&___npcAICfgDic_22, value);
	}

	inline static int32_t get_offset_of_npcAIBehaviorCfgDic_23() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___npcAIBehaviorCfgDic_23)); }
	inline Dictionary_2_t3454000048 * get_npcAIBehaviorCfgDic_23() const { return ___npcAIBehaviorCfgDic_23; }
	inline Dictionary_2_t3454000048 ** get_address_of_npcAIBehaviorCfgDic_23() { return &___npcAIBehaviorCfgDic_23; }
	inline void set_npcAIBehaviorCfgDic_23(Dictionary_2_t3454000048 * value)
	{
		___npcAIBehaviorCfgDic_23 = value;
		Il2CppCodeGenWriteBarrier(&___npcAIBehaviorCfgDic_23, value);
	}

	inline static int32_t get_offset_of_npcAIConditionCfgDic_24() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___npcAIConditionCfgDic_24)); }
	inline Dictionary_2_t927460409 * get_npcAIConditionCfgDic_24() const { return ___npcAIConditionCfgDic_24; }
	inline Dictionary_2_t927460409 ** get_address_of_npcAIConditionCfgDic_24() { return &___npcAIConditionCfgDic_24; }
	inline void set_npcAIConditionCfgDic_24(Dictionary_2_t927460409 * value)
	{
		___npcAIConditionCfgDic_24 = value;
		Il2CppCodeGenWriteBarrier(&___npcAIConditionCfgDic_24, value);
	}

	inline static int32_t get_offset_of_portalCfgDic_25() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___portalCfgDic_25)); }
	inline Dictionary_2_t1114297823 * get_portalCfgDic_25() const { return ___portalCfgDic_25; }
	inline Dictionary_2_t1114297823 ** get_address_of_portalCfgDic_25() { return &___portalCfgDic_25; }
	inline void set_portalCfgDic_25(Dictionary_2_t1114297823 * value)
	{
		___portalCfgDic_25 = value;
		Il2CppCodeGenWriteBarrier(&___portalCfgDic_25, value);
	}

	inline static int32_t get_offset_of_skillCfgDic_26() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___skillCfgDic_26)); }
	inline Dictionary_2_t2139688410 * get_skillCfgDic_26() const { return ___skillCfgDic_26; }
	inline Dictionary_2_t2139688410 ** get_address_of_skillCfgDic_26() { return &___skillCfgDic_26; }
	inline void set_skillCfgDic_26(Dictionary_2_t2139688410 * value)
	{
		___skillCfgDic_26 = value;
		Il2CppCodeGenWriteBarrier(&___skillCfgDic_26, value);
	}

	inline static int32_t get_offset_of_skill_upgradeCfgDic_27() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___skill_upgradeCfgDic_27)); }
	inline Dictionary_2_t787989725 * get_skill_upgradeCfgDic_27() const { return ___skill_upgradeCfgDic_27; }
	inline Dictionary_2_t787989725 ** get_address_of_skill_upgradeCfgDic_27() { return &___skill_upgradeCfgDic_27; }
	inline void set_skill_upgradeCfgDic_27(Dictionary_2_t787989725 * value)
	{
		___skill_upgradeCfgDic_27 = value;
		Il2CppCodeGenWriteBarrier(&___skill_upgradeCfgDic_27, value);
	}

	inline static int32_t get_offset_of_superskillCfgDic_28() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___superskillCfgDic_28)); }
	inline Dictionary_2_t3021394517 * get_superskillCfgDic_28() const { return ___superskillCfgDic_28; }
	inline Dictionary_2_t3021394517 ** get_address_of_superskillCfgDic_28() { return &___superskillCfgDic_28; }
	inline void set_superskillCfgDic_28(Dictionary_2_t3021394517 * value)
	{
		___superskillCfgDic_28 = value;
		Il2CppCodeGenWriteBarrier(&___superskillCfgDic_28, value);
	}

	inline static int32_t get_offset_of_taixuTipCfgDic_29() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___taixuTipCfgDic_29)); }
	inline Dictionary_2_t1265985609 * get_taixuTipCfgDic_29() const { return ___taixuTipCfgDic_29; }
	inline Dictionary_2_t1265985609 ** get_address_of_taixuTipCfgDic_29() { return &___taixuTipCfgDic_29; }
	inline void set_taixuTipCfgDic_29(Dictionary_2_t1265985609 * value)
	{
		___taixuTipCfgDic_29 = value;
		Il2CppCodeGenWriteBarrier(&___taixuTipCfgDic_29, value);
	}

	inline static int32_t get_offset_of_towerbuffCfgDic_30() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243, ___towerbuffCfgDic_30)); }
	inline Dictionary_2_t1753120111 * get_towerbuffCfgDic_30() const { return ___towerbuffCfgDic_30; }
	inline Dictionary_2_t1753120111 ** get_address_of_towerbuffCfgDic_30() { return &___towerbuffCfgDic_30; }
	inline void set_towerbuffCfgDic_30(Dictionary_2_t1753120111 * value)
	{
		___towerbuffCfgDic_30 = value;
		Il2CppCodeGenWriteBarrier(&___towerbuffCfgDic_30, value);
	}
};

struct CSDatacfgManager_t1565254243_StaticFields
{
public:
	// CSDatacfgManager CSDatacfgManager::_inst
	CSDatacfgManager_t1565254243 * ____inst_31;

public:
	inline static int32_t get_offset_of__inst_31() { return static_cast<int32_t>(offsetof(CSDatacfgManager_t1565254243_StaticFields, ____inst_31)); }
	inline CSDatacfgManager_t1565254243 * get__inst_31() const { return ____inst_31; }
	inline CSDatacfgManager_t1565254243 ** get_address_of__inst_31() { return &____inst_31; }
	inline void set__inst_31(CSDatacfgManager_t1565254243 * value)
	{
		____inst_31 = value;
		Il2CppCodeGenWriteBarrier(&____inst_31, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
