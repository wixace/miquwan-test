﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine
struct DeflaterEngine_t3684739431;
// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterPending
struct DeflaterPending_t1829109954;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1829109954.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1587604368.h"

// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::.ctor(ICSharpCode.SharpZipLib.Zip.Compression.DeflaterPending)
extern "C"  void DeflaterEngine__ctor_m1885967597 (DeflaterEngine_t3684739431 * __this, DeflaterPending_t1829109954 * ___pending0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::Deflate(System.Boolean,System.Boolean)
extern "C"  bool DeflaterEngine_Deflate_m2021511700 (DeflaterEngine_t3684739431 * __this, bool ___flush0, bool ___finish1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::SetInput(System.Byte[],System.Int32,System.Int32)
extern "C"  void DeflaterEngine_SetInput_m3394045016 (DeflaterEngine_t3684739431 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::NeedsInput()
extern "C"  bool DeflaterEngine_NeedsInput_m1072053576 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::Reset()
extern "C"  void DeflaterEngine_Reset_m71009448 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::ResetAdler()
extern "C"  void DeflaterEngine_ResetAdler_m1548771120 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::get_Adler()
extern "C"  int32_t DeflaterEngine_get_Adler_m434246074 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::get_TotalIn()
extern "C"  int64_t DeflaterEngine_get_TotalIn_m2394369484 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::set_Strategy(ICSharpCode.SharpZipLib.Zip.Compression.DeflateStrategy)
extern "C"  void DeflaterEngine_set_Strategy_m971094401 (DeflaterEngine_t3684739431 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::SetLevel(System.Int32)
extern "C"  void DeflaterEngine_SetLevel_m2670793852 (DeflaterEngine_t3684739431 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::FillWindow()
extern "C"  void DeflaterEngine_FillWindow_m1132978300 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::UpdateHash()
extern "C"  void DeflaterEngine_UpdateHash_m2011003296 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::InsertString()
extern "C"  int32_t DeflaterEngine_InsertString_m1774160927 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::SlideWindow()
extern "C"  void DeflaterEngine_SlideWindow_m3591605722 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::FindLongestMatch(System.Int32)
extern "C"  bool DeflaterEngine_FindLongestMatch_m707434080 (DeflaterEngine_t3684739431 * __this, int32_t ___curMatch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateStored(System.Boolean,System.Boolean)
extern "C"  bool DeflaterEngine_DeflateStored_m865620657 (DeflaterEngine_t3684739431 * __this, bool ___flush0, bool ___finish1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateFast(System.Boolean,System.Boolean)
extern "C"  bool DeflaterEngine_DeflateFast_m1961244312 (DeflaterEngine_t3684739431 * __this, bool ___flush0, bool ___finish1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateSlow(System.Boolean,System.Boolean)
extern "C"  bool DeflaterEngine_DeflateSlow_m4017565779 (DeflaterEngine_t3684739431 * __this, bool ___flush0, bool ___finish1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
