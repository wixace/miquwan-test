﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GUIText
struct GUIText_t3371372606;
// System.Collections.Generic.List`1<JSDebugMessages/Message>
struct List_1_t2271975326;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSDebugMessages/Message
struct  Message_t903789774  : public Il2CppObject
{
public:
	// UnityEngine.GUIText JSDebugMessages/Message::guiText
	GUIText_t3371372606 * ___guiText_0;

public:
	inline static int32_t get_offset_of_guiText_0() { return static_cast<int32_t>(offsetof(Message_t903789774, ___guiText_0)); }
	inline GUIText_t3371372606 * get_guiText_0() const { return ___guiText_0; }
	inline GUIText_t3371372606 ** get_address_of_guiText_0() { return &___guiText_0; }
	inline void set_guiText_0(GUIText_t3371372606 * value)
	{
		___guiText_0 = value;
		Il2CppCodeGenWriteBarrier(&___guiText_0, value);
	}
};

struct Message_t903789774_StaticFields
{
public:
	// System.Collections.Generic.List`1<JSDebugMessages/Message> JSDebugMessages/Message::pool
	List_1_t2271975326 * ___pool_1;

public:
	inline static int32_t get_offset_of_pool_1() { return static_cast<int32_t>(offsetof(Message_t903789774_StaticFields, ___pool_1)); }
	inline List_1_t2271975326 * get_pool_1() const { return ___pool_1; }
	inline List_1_t2271975326 ** get_address_of_pool_1() { return &___pool_1; }
	inline void set_pool_1(List_1_t2271975326 * value)
	{
		___pool_1 = value;
		Il2CppCodeGenWriteBarrier(&___pool_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
