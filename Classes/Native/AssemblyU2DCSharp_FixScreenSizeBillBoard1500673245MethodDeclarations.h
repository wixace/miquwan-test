﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FixScreenSizeBillBoard
struct FixScreenSizeBillBoard_t1500673245;

#include "codegen/il2cpp-codegen.h"

// System.Void FixScreenSizeBillBoard::.ctor()
extern "C"  void FixScreenSizeBillBoard__ctor_m4034062430 (FixScreenSizeBillBoard_t1500673245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FixScreenSizeBillBoard::Awake()
extern "C"  void FixScreenSizeBillBoard_Awake_m4271667649 (FixScreenSizeBillBoard_t1500673245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FixScreenSizeBillBoard::OnEnable()
extern "C"  void FixScreenSizeBillBoard_OnEnable_m3345521256 (FixScreenSizeBillBoard_t1500673245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FixScreenSizeBillBoard::OnDisable()
extern "C"  void FixScreenSizeBillBoard_OnDisable_m1072881093 (FixScreenSizeBillBoard_t1500673245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FixScreenSizeBillBoard::Update()
extern "C"  void FixScreenSizeBillBoard_Update_m2228745839 (FixScreenSizeBillBoard_t1500673245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
