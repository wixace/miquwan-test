﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_sowwhi72
struct  M_sowwhi72_t125148456  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_sowwhi72::_talga
	bool ____talga_0;
	// System.Boolean GarbageiOS.M_sowwhi72::_tirrobas
	bool ____tirrobas_1;
	// System.Int32 GarbageiOS.M_sowwhi72::_wairsairel
	int32_t ____wairsairel_2;
	// System.Int32 GarbageiOS.M_sowwhi72::_baveqaYapu
	int32_t ____baveqaYapu_3;
	// System.UInt32 GarbageiOS.M_sowwhi72::_treardoupiPurher
	uint32_t ____treardoupiPurher_4;
	// System.String GarbageiOS.M_sowwhi72::_tirsai
	String_t* ____tirsai_5;
	// System.Single GarbageiOS.M_sowwhi72::_chinou
	float ____chinou_6;
	// System.Int32 GarbageiOS.M_sowwhi72::_ruceSeatear
	int32_t ____ruceSeatear_7;
	// System.UInt32 GarbageiOS.M_sowwhi72::_gisowhoCahaytror
	uint32_t ____gisowhoCahaytror_8;
	// System.String GarbageiOS.M_sowwhi72::_disvepurSohawe
	String_t* ____disvepurSohawe_9;
	// System.UInt32 GarbageiOS.M_sowwhi72::_wemhayrereSaibowem
	uint32_t ____wemhayrereSaibowem_10;
	// System.Boolean GarbageiOS.M_sowwhi72::_tareasaiNerkar
	bool ____tareasaiNerkar_11;
	// System.String GarbageiOS.M_sowwhi72::_serjere
	String_t* ____serjere_12;
	// System.Single GarbageiOS.M_sowwhi72::_dislovurSapechi
	float ____dislovurSapechi_13;
	// System.String GarbageiOS.M_sowwhi72::_coutaLaistelrel
	String_t* ____coutaLaistelrel_14;
	// System.String GarbageiOS.M_sowwhi72::_selrirYemtur
	String_t* ____selrirYemtur_15;
	// System.UInt32 GarbageiOS.M_sowwhi72::_feanemno
	uint32_t ____feanemno_16;
	// System.Single GarbageiOS.M_sowwhi72::_calirro
	float ____calirro_17;
	// System.Single GarbageiOS.M_sowwhi72::_bearsouGerehebear
	float ____bearsouGerehebear_18;
	// System.String GarbageiOS.M_sowwhi72::_sawla
	String_t* ____sawla_19;
	// System.Int32 GarbageiOS.M_sowwhi72::_maypi
	int32_t ____maypi_20;
	// System.UInt32 GarbageiOS.M_sowwhi72::_rerebeqemMirkallchur
	uint32_t ____rerebeqemMirkallchur_21;
	// System.UInt32 GarbageiOS.M_sowwhi72::_mebe
	uint32_t ____mebe_22;
	// System.Int32 GarbageiOS.M_sowwhi72::_mirlooxearSeto
	int32_t ____mirlooxearSeto_23;
	// System.Int32 GarbageiOS.M_sowwhi72::_dartear
	int32_t ____dartear_24;
	// System.Int32 GarbageiOS.M_sowwhi72::_dalltisNemsee
	int32_t ____dalltisNemsee_25;
	// System.String GarbageiOS.M_sowwhi72::_jichor
	String_t* ____jichor_26;
	// System.UInt32 GarbageiOS.M_sowwhi72::_xaki
	uint32_t ____xaki_27;
	// System.Boolean GarbageiOS.M_sowwhi72::_hursaska
	bool ____hursaska_28;
	// System.Single GarbageiOS.M_sowwhi72::_sece
	float ____sece_29;
	// System.Boolean GarbageiOS.M_sowwhi72::_hepawHutrou
	bool ____hepawHutrou_30;

public:
	inline static int32_t get_offset_of__talga_0() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____talga_0)); }
	inline bool get__talga_0() const { return ____talga_0; }
	inline bool* get_address_of__talga_0() { return &____talga_0; }
	inline void set__talga_0(bool value)
	{
		____talga_0 = value;
	}

	inline static int32_t get_offset_of__tirrobas_1() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____tirrobas_1)); }
	inline bool get__tirrobas_1() const { return ____tirrobas_1; }
	inline bool* get_address_of__tirrobas_1() { return &____tirrobas_1; }
	inline void set__tirrobas_1(bool value)
	{
		____tirrobas_1 = value;
	}

	inline static int32_t get_offset_of__wairsairel_2() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____wairsairel_2)); }
	inline int32_t get__wairsairel_2() const { return ____wairsairel_2; }
	inline int32_t* get_address_of__wairsairel_2() { return &____wairsairel_2; }
	inline void set__wairsairel_2(int32_t value)
	{
		____wairsairel_2 = value;
	}

	inline static int32_t get_offset_of__baveqaYapu_3() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____baveqaYapu_3)); }
	inline int32_t get__baveqaYapu_3() const { return ____baveqaYapu_3; }
	inline int32_t* get_address_of__baveqaYapu_3() { return &____baveqaYapu_3; }
	inline void set__baveqaYapu_3(int32_t value)
	{
		____baveqaYapu_3 = value;
	}

	inline static int32_t get_offset_of__treardoupiPurher_4() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____treardoupiPurher_4)); }
	inline uint32_t get__treardoupiPurher_4() const { return ____treardoupiPurher_4; }
	inline uint32_t* get_address_of__treardoupiPurher_4() { return &____treardoupiPurher_4; }
	inline void set__treardoupiPurher_4(uint32_t value)
	{
		____treardoupiPurher_4 = value;
	}

	inline static int32_t get_offset_of__tirsai_5() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____tirsai_5)); }
	inline String_t* get__tirsai_5() const { return ____tirsai_5; }
	inline String_t** get_address_of__tirsai_5() { return &____tirsai_5; }
	inline void set__tirsai_5(String_t* value)
	{
		____tirsai_5 = value;
		Il2CppCodeGenWriteBarrier(&____tirsai_5, value);
	}

	inline static int32_t get_offset_of__chinou_6() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____chinou_6)); }
	inline float get__chinou_6() const { return ____chinou_6; }
	inline float* get_address_of__chinou_6() { return &____chinou_6; }
	inline void set__chinou_6(float value)
	{
		____chinou_6 = value;
	}

	inline static int32_t get_offset_of__ruceSeatear_7() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____ruceSeatear_7)); }
	inline int32_t get__ruceSeatear_7() const { return ____ruceSeatear_7; }
	inline int32_t* get_address_of__ruceSeatear_7() { return &____ruceSeatear_7; }
	inline void set__ruceSeatear_7(int32_t value)
	{
		____ruceSeatear_7 = value;
	}

	inline static int32_t get_offset_of__gisowhoCahaytror_8() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____gisowhoCahaytror_8)); }
	inline uint32_t get__gisowhoCahaytror_8() const { return ____gisowhoCahaytror_8; }
	inline uint32_t* get_address_of__gisowhoCahaytror_8() { return &____gisowhoCahaytror_8; }
	inline void set__gisowhoCahaytror_8(uint32_t value)
	{
		____gisowhoCahaytror_8 = value;
	}

	inline static int32_t get_offset_of__disvepurSohawe_9() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____disvepurSohawe_9)); }
	inline String_t* get__disvepurSohawe_9() const { return ____disvepurSohawe_9; }
	inline String_t** get_address_of__disvepurSohawe_9() { return &____disvepurSohawe_9; }
	inline void set__disvepurSohawe_9(String_t* value)
	{
		____disvepurSohawe_9 = value;
		Il2CppCodeGenWriteBarrier(&____disvepurSohawe_9, value);
	}

	inline static int32_t get_offset_of__wemhayrereSaibowem_10() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____wemhayrereSaibowem_10)); }
	inline uint32_t get__wemhayrereSaibowem_10() const { return ____wemhayrereSaibowem_10; }
	inline uint32_t* get_address_of__wemhayrereSaibowem_10() { return &____wemhayrereSaibowem_10; }
	inline void set__wemhayrereSaibowem_10(uint32_t value)
	{
		____wemhayrereSaibowem_10 = value;
	}

	inline static int32_t get_offset_of__tareasaiNerkar_11() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____tareasaiNerkar_11)); }
	inline bool get__tareasaiNerkar_11() const { return ____tareasaiNerkar_11; }
	inline bool* get_address_of__tareasaiNerkar_11() { return &____tareasaiNerkar_11; }
	inline void set__tareasaiNerkar_11(bool value)
	{
		____tareasaiNerkar_11 = value;
	}

	inline static int32_t get_offset_of__serjere_12() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____serjere_12)); }
	inline String_t* get__serjere_12() const { return ____serjere_12; }
	inline String_t** get_address_of__serjere_12() { return &____serjere_12; }
	inline void set__serjere_12(String_t* value)
	{
		____serjere_12 = value;
		Il2CppCodeGenWriteBarrier(&____serjere_12, value);
	}

	inline static int32_t get_offset_of__dislovurSapechi_13() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____dislovurSapechi_13)); }
	inline float get__dislovurSapechi_13() const { return ____dislovurSapechi_13; }
	inline float* get_address_of__dislovurSapechi_13() { return &____dislovurSapechi_13; }
	inline void set__dislovurSapechi_13(float value)
	{
		____dislovurSapechi_13 = value;
	}

	inline static int32_t get_offset_of__coutaLaistelrel_14() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____coutaLaistelrel_14)); }
	inline String_t* get__coutaLaistelrel_14() const { return ____coutaLaistelrel_14; }
	inline String_t** get_address_of__coutaLaistelrel_14() { return &____coutaLaistelrel_14; }
	inline void set__coutaLaistelrel_14(String_t* value)
	{
		____coutaLaistelrel_14 = value;
		Il2CppCodeGenWriteBarrier(&____coutaLaistelrel_14, value);
	}

	inline static int32_t get_offset_of__selrirYemtur_15() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____selrirYemtur_15)); }
	inline String_t* get__selrirYemtur_15() const { return ____selrirYemtur_15; }
	inline String_t** get_address_of__selrirYemtur_15() { return &____selrirYemtur_15; }
	inline void set__selrirYemtur_15(String_t* value)
	{
		____selrirYemtur_15 = value;
		Il2CppCodeGenWriteBarrier(&____selrirYemtur_15, value);
	}

	inline static int32_t get_offset_of__feanemno_16() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____feanemno_16)); }
	inline uint32_t get__feanemno_16() const { return ____feanemno_16; }
	inline uint32_t* get_address_of__feanemno_16() { return &____feanemno_16; }
	inline void set__feanemno_16(uint32_t value)
	{
		____feanemno_16 = value;
	}

	inline static int32_t get_offset_of__calirro_17() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____calirro_17)); }
	inline float get__calirro_17() const { return ____calirro_17; }
	inline float* get_address_of__calirro_17() { return &____calirro_17; }
	inline void set__calirro_17(float value)
	{
		____calirro_17 = value;
	}

	inline static int32_t get_offset_of__bearsouGerehebear_18() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____bearsouGerehebear_18)); }
	inline float get__bearsouGerehebear_18() const { return ____bearsouGerehebear_18; }
	inline float* get_address_of__bearsouGerehebear_18() { return &____bearsouGerehebear_18; }
	inline void set__bearsouGerehebear_18(float value)
	{
		____bearsouGerehebear_18 = value;
	}

	inline static int32_t get_offset_of__sawla_19() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____sawla_19)); }
	inline String_t* get__sawla_19() const { return ____sawla_19; }
	inline String_t** get_address_of__sawla_19() { return &____sawla_19; }
	inline void set__sawla_19(String_t* value)
	{
		____sawla_19 = value;
		Il2CppCodeGenWriteBarrier(&____sawla_19, value);
	}

	inline static int32_t get_offset_of__maypi_20() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____maypi_20)); }
	inline int32_t get__maypi_20() const { return ____maypi_20; }
	inline int32_t* get_address_of__maypi_20() { return &____maypi_20; }
	inline void set__maypi_20(int32_t value)
	{
		____maypi_20 = value;
	}

	inline static int32_t get_offset_of__rerebeqemMirkallchur_21() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____rerebeqemMirkallchur_21)); }
	inline uint32_t get__rerebeqemMirkallchur_21() const { return ____rerebeqemMirkallchur_21; }
	inline uint32_t* get_address_of__rerebeqemMirkallchur_21() { return &____rerebeqemMirkallchur_21; }
	inline void set__rerebeqemMirkallchur_21(uint32_t value)
	{
		____rerebeqemMirkallchur_21 = value;
	}

	inline static int32_t get_offset_of__mebe_22() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____mebe_22)); }
	inline uint32_t get__mebe_22() const { return ____mebe_22; }
	inline uint32_t* get_address_of__mebe_22() { return &____mebe_22; }
	inline void set__mebe_22(uint32_t value)
	{
		____mebe_22 = value;
	}

	inline static int32_t get_offset_of__mirlooxearSeto_23() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____mirlooxearSeto_23)); }
	inline int32_t get__mirlooxearSeto_23() const { return ____mirlooxearSeto_23; }
	inline int32_t* get_address_of__mirlooxearSeto_23() { return &____mirlooxearSeto_23; }
	inline void set__mirlooxearSeto_23(int32_t value)
	{
		____mirlooxearSeto_23 = value;
	}

	inline static int32_t get_offset_of__dartear_24() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____dartear_24)); }
	inline int32_t get__dartear_24() const { return ____dartear_24; }
	inline int32_t* get_address_of__dartear_24() { return &____dartear_24; }
	inline void set__dartear_24(int32_t value)
	{
		____dartear_24 = value;
	}

	inline static int32_t get_offset_of__dalltisNemsee_25() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____dalltisNemsee_25)); }
	inline int32_t get__dalltisNemsee_25() const { return ____dalltisNemsee_25; }
	inline int32_t* get_address_of__dalltisNemsee_25() { return &____dalltisNemsee_25; }
	inline void set__dalltisNemsee_25(int32_t value)
	{
		____dalltisNemsee_25 = value;
	}

	inline static int32_t get_offset_of__jichor_26() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____jichor_26)); }
	inline String_t* get__jichor_26() const { return ____jichor_26; }
	inline String_t** get_address_of__jichor_26() { return &____jichor_26; }
	inline void set__jichor_26(String_t* value)
	{
		____jichor_26 = value;
		Il2CppCodeGenWriteBarrier(&____jichor_26, value);
	}

	inline static int32_t get_offset_of__xaki_27() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____xaki_27)); }
	inline uint32_t get__xaki_27() const { return ____xaki_27; }
	inline uint32_t* get_address_of__xaki_27() { return &____xaki_27; }
	inline void set__xaki_27(uint32_t value)
	{
		____xaki_27 = value;
	}

	inline static int32_t get_offset_of__hursaska_28() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____hursaska_28)); }
	inline bool get__hursaska_28() const { return ____hursaska_28; }
	inline bool* get_address_of__hursaska_28() { return &____hursaska_28; }
	inline void set__hursaska_28(bool value)
	{
		____hursaska_28 = value;
	}

	inline static int32_t get_offset_of__sece_29() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____sece_29)); }
	inline float get__sece_29() const { return ____sece_29; }
	inline float* get_address_of__sece_29() { return &____sece_29; }
	inline void set__sece_29(float value)
	{
		____sece_29 = value;
	}

	inline static int32_t get_offset_of__hepawHutrou_30() { return static_cast<int32_t>(offsetof(M_sowwhi72_t125148456, ____hepawHutrou_30)); }
	inline bool get__hepawHutrou_30() const { return ____hepawHutrou_30; }
	inline bool* get_address_of__hepawHutrou_30() { return &____hepawHutrou_30; }
	inline void set__hepawHutrou_30(bool value)
	{
		____hepawHutrou_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
