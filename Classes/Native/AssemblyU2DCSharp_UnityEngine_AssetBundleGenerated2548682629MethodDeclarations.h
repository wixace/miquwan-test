﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AssetBundleGenerated
struct UnityEngine_AssetBundleGenerated_t2548682629;
// JSVCall
struct JSVCall_t3708497963;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_AssetBundleGenerated::.ctor()
extern "C"  void UnityEngine_AssetBundleGenerated__ctor_m3323776182 (UnityEngine_AssetBundleGenerated_t2548682629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AssetBundleGenerated::.cctor()
extern "C"  void UnityEngine_AssetBundleGenerated__cctor_m3770717623 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_AssetBundle1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_AssetBundle1_m688394526 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AssetBundleGenerated::AssetBundle_mainAsset(JSVCall)
extern "C"  void UnityEngine_AssetBundleGenerated_AssetBundle_mainAsset_m1585649225 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_Contains__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_Contains__String_m3125419951 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_GetAllAssetNames(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_GetAllAssetNames_m4271564610 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_GetAllScenePaths(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_GetAllScenePaths_m2508849484 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadAllAssets__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadAllAssets__Type_m3028701147 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadAllAssetsT1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadAllAssetsT1_m2517038782 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadAllAssets(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadAllAssets_m3095675937 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadAllAssetsAsync__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadAllAssetsAsync__Type_m3764923415 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadAllAssetsAsyncT1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadAllAssetsAsyncT1_m2597867770 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadAllAssetsAsync(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadAllAssetsAsync_m2032074077 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadAsset__String__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadAsset__String__Type_m1097222872 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadAssetT1__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadAssetT1__String_m3181517691 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadAsset__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadAsset__String_m4291264734 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadAssetAsync__String__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadAssetAsync__String__Type_m368472508 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadAssetAsyncT1__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadAssetAsyncT1__String_m3087094879 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadAssetAsync__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadAssetAsync__String_m4054295234 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadAssetWithSubAssets__String__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadAssetWithSubAssets__String__Type_m2890848893 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadAssetWithSubAssetsT1__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadAssetWithSubAssetsT1__String_m4171646368 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadAssetWithSubAssets__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadAssetWithSubAssets__String_m699002947 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadAssetWithSubAssetsAsync__String__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadAssetWithSubAssetsAsync__String__Type_m1520361783 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadAssetWithSubAssetsAsyncT1__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadAssetWithSubAssetsAsyncT1__String_m2854926682 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadAssetWithSubAssetsAsync__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadAssetWithSubAssetsAsync__String_m3142322813 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_Unload__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_Unload__Boolean_m508432172 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadFromFile__String__UInt32__UInt64(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadFromFile__String__UInt32__UInt64_m1293132269 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadFromFile__String__UInt32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadFromFile__String__UInt32_m2473898101 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadFromFile__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadFromFile__String_m3361551644 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadFromFileAsync__String__UInt32__UInt64(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadFromFileAsync__String__UInt32__UInt64_m3056599573 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadFromFileAsync__String__UInt32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadFromFileAsync__String__UInt32_m1365751965 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadFromFileAsync__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadFromFileAsync__String_m3494831428 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadFromMemory__Byte_Array__UInt32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadFromMemory__Byte_Array__UInt32_m3700445611 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadFromMemory__Byte_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadFromMemory__Byte_Array_m2586864722 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadFromMemoryAsync__Byte_Array__UInt32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadFromMemoryAsync__Byte_Array__UInt32_m2689394025 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleGenerated::AssetBundle_LoadFromMemoryAsync__Byte_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleGenerated_AssetBundle_LoadFromMemoryAsync__Byte_Array_m2632162832 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AssetBundleGenerated::__Register()
extern "C"  void UnityEngine_AssetBundleGenerated___Register_m1681273297 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine_AssetBundleGenerated::<AssetBundle_LoadFromMemory__Byte_Array__UInt32>m__180()
extern "C"  ByteU5BU5D_t4260760469* UnityEngine_AssetBundleGenerated_U3CAssetBundle_LoadFromMemory__Byte_Array__UInt32U3Em__180_m199646077 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine_AssetBundleGenerated::<AssetBundle_LoadFromMemory__Byte_Array>m__181()
extern "C"  ByteU5BU5D_t4260760469* UnityEngine_AssetBundleGenerated_U3CAssetBundle_LoadFromMemory__Byte_ArrayU3Em__181_m100408631 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine_AssetBundleGenerated::<AssetBundle_LoadFromMemoryAsync__Byte_Array__UInt32>m__182()
extern "C"  ByteU5BU5D_t4260760469* UnityEngine_AssetBundleGenerated_U3CAssetBundle_LoadFromMemoryAsync__Byte_Array__UInt32U3Em__182_m2006406473 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine_AssetBundleGenerated::<AssetBundle_LoadFromMemoryAsync__Byte_Array>m__183()
extern "C"  ByteU5BU5D_t4260760469* UnityEngine_AssetBundleGenerated_U3CAssetBundle_LoadFromMemoryAsync__Byte_ArrayU3Em__183_m2323710211 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AssetBundleGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_AssetBundleGenerated_ilo_addJSCSRel1_m3951476850 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AssetBundleGenerated::ilo_setStringS2(System.Int32,System.String)
extern "C"  void UnityEngine_AssetBundleGenerated_ilo_setStringS2_m624910592 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AssetBundleGenerated::ilo_setArrayS3(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_AssetBundleGenerated_ilo_setArrayS3_m3938548879 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AssetBundleGenerated::ilo_moveSaveID2Arr4(System.Int32)
extern "C"  void UnityEngine_AssetBundleGenerated_ilo_moveSaveID2Arr4_m3269288462 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AssetBundleGenerated::ilo_setObject5(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_AssetBundleGenerated_ilo_setObject5_m4047923352 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_AssetBundleGenerated::ilo_getStringS6(System.Int32)
extern "C"  String_t* UnityEngine_AssetBundleGenerated_ilo_getStringS6_m1056160313 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_AssetBundleGenerated::ilo_getObject7(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_AssetBundleGenerated_ilo_getObject7_m3099031167 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AssetBundleGenerated::ilo_setWhatever8(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  void UnityEngine_AssetBundleGenerated_ilo_setWhatever8_m762777930 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 UnityEngine_AssetBundleGenerated::ilo_getUInt329(System.Int32)
extern "C"  uint32_t UnityEngine_AssetBundleGenerated_ilo_getUInt329_m725769303 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AssetBundleGenerated::ilo_getObject10(System.Int32)
extern "C"  int32_t UnityEngine_AssetBundleGenerated_ilo_getObject10_m806040312 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte UnityEngine_AssetBundleGenerated::ilo_getByte11(System.Int32)
extern "C"  uint8_t UnityEngine_AssetBundleGenerated_ilo_getByte11_m3729575620 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AssetBundleGenerated::ilo_getArrayLength12(System.Int32)
extern "C"  int32_t UnityEngine_AssetBundleGenerated_ilo_getArrayLength12_m776677834 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
