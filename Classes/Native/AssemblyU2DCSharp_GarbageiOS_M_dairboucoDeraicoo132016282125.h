﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_dairboucoDeraicoo137
struct  M_dairboucoDeraicoo137_t2016282125  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_dairboucoDeraicoo137::_bisnirkel
	String_t* ____bisnirkel_0;
	// System.Boolean GarbageiOS.M_dairboucoDeraicoo137::_cowjineNesu
	bool ____cowjineNesu_1;
	// System.String GarbageiOS.M_dairboucoDeraicoo137::_dearmeeday
	String_t* ____dearmeeday_2;
	// System.UInt32 GarbageiOS.M_dairboucoDeraicoo137::_baporsuKayheebi
	uint32_t ____baporsuKayheebi_3;
	// System.Int32 GarbageiOS.M_dairboucoDeraicoo137::_cernelyurSaselta
	int32_t ____cernelyurSaselta_4;
	// System.UInt32 GarbageiOS.M_dairboucoDeraicoo137::_kikas
	uint32_t ____kikas_5;
	// System.Single GarbageiOS.M_dairboucoDeraicoo137::_couyeRemjetu
	float ____couyeRemjetu_6;
	// System.Boolean GarbageiOS.M_dairboucoDeraicoo137::_joucaKowfifis
	bool ____joucaKowfifis_7;
	// System.Single GarbageiOS.M_dairboucoDeraicoo137::_busooJaysa
	float ____busooJaysa_8;
	// System.Single GarbageiOS.M_dairboucoDeraicoo137::_busoosow
	float ____busoosow_9;
	// System.Single GarbageiOS.M_dairboucoDeraicoo137::_leeyayRirsejear
	float ____leeyayRirsejear_10;
	// System.Single GarbageiOS.M_dairboucoDeraicoo137::_neseaPairnee
	float ____neseaPairnee_11;
	// System.Boolean GarbageiOS.M_dairboucoDeraicoo137::_mornere
	bool ____mornere_12;
	// System.Single GarbageiOS.M_dairboucoDeraicoo137::_nawla
	float ____nawla_13;

public:
	inline static int32_t get_offset_of__bisnirkel_0() { return static_cast<int32_t>(offsetof(M_dairboucoDeraicoo137_t2016282125, ____bisnirkel_0)); }
	inline String_t* get__bisnirkel_0() const { return ____bisnirkel_0; }
	inline String_t** get_address_of__bisnirkel_0() { return &____bisnirkel_0; }
	inline void set__bisnirkel_0(String_t* value)
	{
		____bisnirkel_0 = value;
		Il2CppCodeGenWriteBarrier(&____bisnirkel_0, value);
	}

	inline static int32_t get_offset_of__cowjineNesu_1() { return static_cast<int32_t>(offsetof(M_dairboucoDeraicoo137_t2016282125, ____cowjineNesu_1)); }
	inline bool get__cowjineNesu_1() const { return ____cowjineNesu_1; }
	inline bool* get_address_of__cowjineNesu_1() { return &____cowjineNesu_1; }
	inline void set__cowjineNesu_1(bool value)
	{
		____cowjineNesu_1 = value;
	}

	inline static int32_t get_offset_of__dearmeeday_2() { return static_cast<int32_t>(offsetof(M_dairboucoDeraicoo137_t2016282125, ____dearmeeday_2)); }
	inline String_t* get__dearmeeday_2() const { return ____dearmeeday_2; }
	inline String_t** get_address_of__dearmeeday_2() { return &____dearmeeday_2; }
	inline void set__dearmeeday_2(String_t* value)
	{
		____dearmeeday_2 = value;
		Il2CppCodeGenWriteBarrier(&____dearmeeday_2, value);
	}

	inline static int32_t get_offset_of__baporsuKayheebi_3() { return static_cast<int32_t>(offsetof(M_dairboucoDeraicoo137_t2016282125, ____baporsuKayheebi_3)); }
	inline uint32_t get__baporsuKayheebi_3() const { return ____baporsuKayheebi_3; }
	inline uint32_t* get_address_of__baporsuKayheebi_3() { return &____baporsuKayheebi_3; }
	inline void set__baporsuKayheebi_3(uint32_t value)
	{
		____baporsuKayheebi_3 = value;
	}

	inline static int32_t get_offset_of__cernelyurSaselta_4() { return static_cast<int32_t>(offsetof(M_dairboucoDeraicoo137_t2016282125, ____cernelyurSaselta_4)); }
	inline int32_t get__cernelyurSaselta_4() const { return ____cernelyurSaselta_4; }
	inline int32_t* get_address_of__cernelyurSaselta_4() { return &____cernelyurSaselta_4; }
	inline void set__cernelyurSaselta_4(int32_t value)
	{
		____cernelyurSaselta_4 = value;
	}

	inline static int32_t get_offset_of__kikas_5() { return static_cast<int32_t>(offsetof(M_dairboucoDeraicoo137_t2016282125, ____kikas_5)); }
	inline uint32_t get__kikas_5() const { return ____kikas_5; }
	inline uint32_t* get_address_of__kikas_5() { return &____kikas_5; }
	inline void set__kikas_5(uint32_t value)
	{
		____kikas_5 = value;
	}

	inline static int32_t get_offset_of__couyeRemjetu_6() { return static_cast<int32_t>(offsetof(M_dairboucoDeraicoo137_t2016282125, ____couyeRemjetu_6)); }
	inline float get__couyeRemjetu_6() const { return ____couyeRemjetu_6; }
	inline float* get_address_of__couyeRemjetu_6() { return &____couyeRemjetu_6; }
	inline void set__couyeRemjetu_6(float value)
	{
		____couyeRemjetu_6 = value;
	}

	inline static int32_t get_offset_of__joucaKowfifis_7() { return static_cast<int32_t>(offsetof(M_dairboucoDeraicoo137_t2016282125, ____joucaKowfifis_7)); }
	inline bool get__joucaKowfifis_7() const { return ____joucaKowfifis_7; }
	inline bool* get_address_of__joucaKowfifis_7() { return &____joucaKowfifis_7; }
	inline void set__joucaKowfifis_7(bool value)
	{
		____joucaKowfifis_7 = value;
	}

	inline static int32_t get_offset_of__busooJaysa_8() { return static_cast<int32_t>(offsetof(M_dairboucoDeraicoo137_t2016282125, ____busooJaysa_8)); }
	inline float get__busooJaysa_8() const { return ____busooJaysa_8; }
	inline float* get_address_of__busooJaysa_8() { return &____busooJaysa_8; }
	inline void set__busooJaysa_8(float value)
	{
		____busooJaysa_8 = value;
	}

	inline static int32_t get_offset_of__busoosow_9() { return static_cast<int32_t>(offsetof(M_dairboucoDeraicoo137_t2016282125, ____busoosow_9)); }
	inline float get__busoosow_9() const { return ____busoosow_9; }
	inline float* get_address_of__busoosow_9() { return &____busoosow_9; }
	inline void set__busoosow_9(float value)
	{
		____busoosow_9 = value;
	}

	inline static int32_t get_offset_of__leeyayRirsejear_10() { return static_cast<int32_t>(offsetof(M_dairboucoDeraicoo137_t2016282125, ____leeyayRirsejear_10)); }
	inline float get__leeyayRirsejear_10() const { return ____leeyayRirsejear_10; }
	inline float* get_address_of__leeyayRirsejear_10() { return &____leeyayRirsejear_10; }
	inline void set__leeyayRirsejear_10(float value)
	{
		____leeyayRirsejear_10 = value;
	}

	inline static int32_t get_offset_of__neseaPairnee_11() { return static_cast<int32_t>(offsetof(M_dairboucoDeraicoo137_t2016282125, ____neseaPairnee_11)); }
	inline float get__neseaPairnee_11() const { return ____neseaPairnee_11; }
	inline float* get_address_of__neseaPairnee_11() { return &____neseaPairnee_11; }
	inline void set__neseaPairnee_11(float value)
	{
		____neseaPairnee_11 = value;
	}

	inline static int32_t get_offset_of__mornere_12() { return static_cast<int32_t>(offsetof(M_dairboucoDeraicoo137_t2016282125, ____mornere_12)); }
	inline bool get__mornere_12() const { return ____mornere_12; }
	inline bool* get_address_of__mornere_12() { return &____mornere_12; }
	inline void set__mornere_12(bool value)
	{
		____mornere_12 = value;
	}

	inline static int32_t get_offset_of__nawla_13() { return static_cast<int32_t>(offsetof(M_dairboucoDeraicoo137_t2016282125, ____nawla_13)); }
	inline float get__nawla_13() const { return ____nawla_13; }
	inline float* get_address_of__nawla_13() { return &____nawla_13; }
	inline void set__nawla_13(float value)
	{
		____nawla_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
