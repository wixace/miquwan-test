﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._345114248aa4a2abd5d671a9d9818c7b
struct _345114248aa4a2abd5d671a9d9818c7b_t510592674;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._345114248aa4a2abd5d671a9d9818c7b::.ctor()
extern "C"  void _345114248aa4a2abd5d671a9d9818c7b__ctor_m3109320139 (_345114248aa4a2abd5d671a9d9818c7b_t510592674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._345114248aa4a2abd5d671a9d9818c7b::_345114248aa4a2abd5d671a9d9818c7bm2(System.Int32)
extern "C"  int32_t _345114248aa4a2abd5d671a9d9818c7b__345114248aa4a2abd5d671a9d9818c7bm2_m931233369 (_345114248aa4a2abd5d671a9d9818c7b_t510592674 * __this, int32_t ____345114248aa4a2abd5d671a9d9818c7ba0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._345114248aa4a2abd5d671a9d9818c7b::_345114248aa4a2abd5d671a9d9818c7bm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _345114248aa4a2abd5d671a9d9818c7b__345114248aa4a2abd5d671a9d9818c7bm_m1796407421 (_345114248aa4a2abd5d671a9d9818c7b_t510592674 * __this, int32_t ____345114248aa4a2abd5d671a9d9818c7ba0, int32_t ____345114248aa4a2abd5d671a9d9818c7b141, int32_t ____345114248aa4a2abd5d671a9d9818c7bc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
