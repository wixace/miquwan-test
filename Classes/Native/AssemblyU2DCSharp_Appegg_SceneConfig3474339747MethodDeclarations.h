﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Appegg/SceneConfig
struct SceneConfig_t3474339747;

#include "codegen/il2cpp-codegen.h"

// System.Void Appegg/SceneConfig::.ctor()
extern "C"  void SceneConfig__ctor_m4091588696 (SceneConfig_t3474339747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
