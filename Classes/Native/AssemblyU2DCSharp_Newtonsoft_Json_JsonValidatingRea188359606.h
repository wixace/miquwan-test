﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// System.Collections.Generic.Stack`1<Newtonsoft.Json.JsonValidatingReader/SchemaScope>
struct Stack_1_t2344540083;
// Newtonsoft.Json.Schema.JsonSchema
struct JsonSchema_t460567603;
// Newtonsoft.Json.Schema.JsonSchemaModel
struct JsonSchemaModel_t3035618138;
// Newtonsoft.Json.JsonValidatingReader/SchemaScope
struct SchemaScope_t3540946455;
// Newtonsoft.Json.Schema.ValidationEventHandler
struct ValidationEventHandler_t2238986739;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>,System.Boolean>
struct Func_2_t2747199801;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>,System.String>
struct Func_2_t2277632640;

#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader816925123.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonValidatingReader
struct  JsonValidatingReader_t188359606  : public JsonReader_t816925123
{
public:
	// Newtonsoft.Json.JsonReader Newtonsoft.Json.JsonValidatingReader::_reader
	JsonReader_t816925123 * ____reader_9;
	// System.Collections.Generic.Stack`1<Newtonsoft.Json.JsonValidatingReader/SchemaScope> Newtonsoft.Json.JsonValidatingReader::_stack
	Stack_1_t2344540083 * ____stack_10;
	// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.JsonValidatingReader::_schema
	JsonSchema_t460567603 * ____schema_11;
	// Newtonsoft.Json.Schema.JsonSchemaModel Newtonsoft.Json.JsonValidatingReader::_model
	JsonSchemaModel_t3035618138 * ____model_12;
	// Newtonsoft.Json.JsonValidatingReader/SchemaScope Newtonsoft.Json.JsonValidatingReader::_currentScope
	SchemaScope_t3540946455 * ____currentScope_13;
	// Newtonsoft.Json.Schema.ValidationEventHandler Newtonsoft.Json.JsonValidatingReader::ValidationEventHandler
	ValidationEventHandler_t2238986739 * ___ValidationEventHandler_14;

public:
	inline static int32_t get_offset_of__reader_9() { return static_cast<int32_t>(offsetof(JsonValidatingReader_t188359606, ____reader_9)); }
	inline JsonReader_t816925123 * get__reader_9() const { return ____reader_9; }
	inline JsonReader_t816925123 ** get_address_of__reader_9() { return &____reader_9; }
	inline void set__reader_9(JsonReader_t816925123 * value)
	{
		____reader_9 = value;
		Il2CppCodeGenWriteBarrier(&____reader_9, value);
	}

	inline static int32_t get_offset_of__stack_10() { return static_cast<int32_t>(offsetof(JsonValidatingReader_t188359606, ____stack_10)); }
	inline Stack_1_t2344540083 * get__stack_10() const { return ____stack_10; }
	inline Stack_1_t2344540083 ** get_address_of__stack_10() { return &____stack_10; }
	inline void set__stack_10(Stack_1_t2344540083 * value)
	{
		____stack_10 = value;
		Il2CppCodeGenWriteBarrier(&____stack_10, value);
	}

	inline static int32_t get_offset_of__schema_11() { return static_cast<int32_t>(offsetof(JsonValidatingReader_t188359606, ____schema_11)); }
	inline JsonSchema_t460567603 * get__schema_11() const { return ____schema_11; }
	inline JsonSchema_t460567603 ** get_address_of__schema_11() { return &____schema_11; }
	inline void set__schema_11(JsonSchema_t460567603 * value)
	{
		____schema_11 = value;
		Il2CppCodeGenWriteBarrier(&____schema_11, value);
	}

	inline static int32_t get_offset_of__model_12() { return static_cast<int32_t>(offsetof(JsonValidatingReader_t188359606, ____model_12)); }
	inline JsonSchemaModel_t3035618138 * get__model_12() const { return ____model_12; }
	inline JsonSchemaModel_t3035618138 ** get_address_of__model_12() { return &____model_12; }
	inline void set__model_12(JsonSchemaModel_t3035618138 * value)
	{
		____model_12 = value;
		Il2CppCodeGenWriteBarrier(&____model_12, value);
	}

	inline static int32_t get_offset_of__currentScope_13() { return static_cast<int32_t>(offsetof(JsonValidatingReader_t188359606, ____currentScope_13)); }
	inline SchemaScope_t3540946455 * get__currentScope_13() const { return ____currentScope_13; }
	inline SchemaScope_t3540946455 ** get_address_of__currentScope_13() { return &____currentScope_13; }
	inline void set__currentScope_13(SchemaScope_t3540946455 * value)
	{
		____currentScope_13 = value;
		Il2CppCodeGenWriteBarrier(&____currentScope_13, value);
	}

	inline static int32_t get_offset_of_ValidationEventHandler_14() { return static_cast<int32_t>(offsetof(JsonValidatingReader_t188359606, ___ValidationEventHandler_14)); }
	inline ValidationEventHandler_t2238986739 * get_ValidationEventHandler_14() const { return ___ValidationEventHandler_14; }
	inline ValidationEventHandler_t2238986739 ** get_address_of_ValidationEventHandler_14() { return &___ValidationEventHandler_14; }
	inline void set_ValidationEventHandler_14(ValidationEventHandler_t2238986739 * value)
	{
		___ValidationEventHandler_14 = value;
		Il2CppCodeGenWriteBarrier(&___ValidationEventHandler_14, value);
	}
};

struct JsonValidatingReader_t188359606_StaticFields
{
public:
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>,System.Boolean> Newtonsoft.Json.JsonValidatingReader::<>f__am$cache6
	Func_2_t2747199801 * ___U3CU3Ef__amU24cache6_15;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>,System.String> Newtonsoft.Json.JsonValidatingReader::<>f__am$cache7
	Func_2_t2277632640 * ___U3CU3Ef__amU24cache7_16;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_15() { return static_cast<int32_t>(offsetof(JsonValidatingReader_t188359606_StaticFields, ___U3CU3Ef__amU24cache6_15)); }
	inline Func_2_t2747199801 * get_U3CU3Ef__amU24cache6_15() const { return ___U3CU3Ef__amU24cache6_15; }
	inline Func_2_t2747199801 ** get_address_of_U3CU3Ef__amU24cache6_15() { return &___U3CU3Ef__amU24cache6_15; }
	inline void set_U3CU3Ef__amU24cache6_15(Func_2_t2747199801 * value)
	{
		___U3CU3Ef__amU24cache6_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_16() { return static_cast<int32_t>(offsetof(JsonValidatingReader_t188359606_StaticFields, ___U3CU3Ef__amU24cache7_16)); }
	inline Func_2_t2277632640 * get_U3CU3Ef__amU24cache7_16() const { return ___U3CU3Ef__amU24cache7_16; }
	inline Func_2_t2277632640 ** get_address_of_U3CU3Ef__amU24cache7_16() { return &___U3CU3Ef__amU24cache7_16; }
	inline void set_U3CU3Ef__amU24cache7_16(Func_2_t2277632640 * value)
	{
		___U3CU3Ef__amU24cache7_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
