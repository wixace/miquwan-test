﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pomelo.Protobuf.Util
struct Util_t3483766166;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Pomelo.Protobuf.Util::.ctor()
extern "C"  void Util__ctor_m2537873514 (Util_t3483766166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pomelo.Protobuf.Util::isSimpleType(System.String)
extern "C"  bool Util_isSimpleType_m3707986686 (Util_t3483766166 * __this, String_t* ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pomelo.Protobuf.Util::containType(System.String)
extern "C"  int32_t Util_containType_m2038219194 (Util_t3483766166 * __this, String_t* ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.Protobuf.Util::initTypeMap()
extern "C"  void Util_initTypeMap_m4139414650 (Util_t3483766166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.Protobuf.Util::Reverse(System.Byte[])
extern "C"  void Util_Reverse_m1574760927 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
