﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonClickStateGenerated
struct ButtonClickStateGenerated_t214190836;
// JSVCall
struct JSVCall_t3708497963;
// ButtonClickState/VoidDelegate
struct VoidDelegate_t3553201165;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// ButtonClickState
struct ButtonClickState_t1021752539;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_ButtonClickState1021752539.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void ButtonClickStateGenerated::.ctor()
extern "C"  void ButtonClickStateGenerated__ctor_m1021512951 (ButtonClickStateGenerated_t214190836 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ButtonClickStateGenerated::ButtonClickState_ButtonClickState1(JSVCall,System.Int32)
extern "C"  bool ButtonClickStateGenerated_ButtonClickState_ButtonClickState1_m1196427771 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonClickStateGenerated::ButtonClickState_strNormal(JSVCall)
extern "C"  void ButtonClickStateGenerated_ButtonClickState_strNormal_m3770608422 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonClickStateGenerated::ButtonClickState_strPress(JSVCall)
extern "C"  void ButtonClickStateGenerated_ButtonClickState_strPress_m3424767260 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonClickStateGenerated::ButtonClickState_interval_max_time(JSVCall)
extern "C"  void ButtonClickStateGenerated_ButtonClickState_interval_max_time_m1039044060 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonClickStateGenerated::ButtonClickState_interval_min_time(JSVCall)
extern "C"  void ButtonClickStateGenerated_ButtonClickState_interval_min_time_m3354089034 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonClickStateGenerated::ButtonClickState_less_speed(JSVCall)
extern "C"  void ButtonClickStateGenerated_ButtonClickState_less_speed_m2199994989 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonClickStateGenerated::ButtonClickState_UsingSumTime(JSVCall)
extern "C"  void ButtonClickStateGenerated_ButtonClickState_UsingSumTime_m3040031034 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ButtonClickState/VoidDelegate ButtonClickStateGenerated::ButtonClickState_onClick_GetDelegate_member6_arg0(CSRepresentedObject)
extern "C"  VoidDelegate_t3553201165 * ButtonClickStateGenerated_ButtonClickState_onClick_GetDelegate_member6_arg0_m523576518 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonClickStateGenerated::ButtonClickState_onClick(JSVCall)
extern "C"  void ButtonClickStateGenerated_ButtonClickState_onClick_m4105724885 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonClickStateGenerated::ButtonClickState_value(JSVCall)
extern "C"  void ButtonClickStateGenerated_ButtonClickState_value_m3234606157 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ButtonClickStateGenerated::ButtonClickState_OnClick(JSVCall,System.Int32)
extern "C"  bool ButtonClickStateGenerated_ButtonClickState_OnClick_m2691221198 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ButtonClickStateGenerated::ButtonClickState_OnHover__Boolean(JSVCall,System.Int32)
extern "C"  bool ButtonClickStateGenerated_ButtonClickState_OnHover__Boolean_m1022485480 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ButtonClickStateGenerated::ButtonClickState_OnPress__Boolean(JSVCall,System.Int32)
extern "C"  bool ButtonClickStateGenerated_ButtonClickState_OnPress__Boolean_m1851156481 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ButtonClickStateGenerated::ButtonClickState_SetFunctionState__Boolean(JSVCall,System.Int32)
extern "C"  bool ButtonClickStateGenerated_ButtonClickState_SetFunctionState__Boolean_m1859652758 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ButtonClickStateGenerated::ButtonClickState_SetFunctionState__Object(JSVCall,System.Int32)
extern "C"  bool ButtonClickStateGenerated_ButtonClickState_SetFunctionState__Object_m3617324563 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonClickStateGenerated::__Register()
extern "C"  void ButtonClickStateGenerated___Register_m3728929328 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ButtonClickState/VoidDelegate ButtonClickStateGenerated::<ButtonClickState_onClick>m__17()
extern "C"  VoidDelegate_t3553201165 * ButtonClickStateGenerated_U3CButtonClickState_onClickU3Em__17_m1079448721 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ButtonClickStateGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool ButtonClickStateGenerated_ilo_attachFinalizerObject1_m2471970016 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ButtonClickStateGenerated::ilo_getStringS2(System.Int32)
extern "C"  String_t* ButtonClickStateGenerated_ilo_getStringS2_m1202889308 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonClickStateGenerated::ilo_setStringS3(System.Int32,System.String)
extern "C"  void ButtonClickStateGenerated_ilo_setStringS3_m1288845634 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonClickStateGenerated::ilo_setSingle4(System.Int32,System.Single)
extern "C"  void ButtonClickStateGenerated_ilo_setSingle4_m477601184 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ButtonClickStateGenerated::ilo_getSingle5(System.Int32)
extern "C"  float ButtonClickStateGenerated_ilo_getSingle5_m2932657020 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ButtonClickStateGenerated::ilo_getBooleanS6(System.Int32)
extern "C"  bool ButtonClickStateGenerated_ilo_getBooleanS6_m2823222866 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ButtonClickStateGenerated::ilo_getWhatever7(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * ButtonClickStateGenerated_ilo_getWhatever7_m4017477649 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonClickStateGenerated::ilo_SetFunctionState8(ButtonClickState,System.Object)
extern "C"  void ButtonClickStateGenerated_ilo_SetFunctionState8_m3653555414 (Il2CppObject * __this /* static, unused */, ButtonClickState_t1021752539 * ____this0, Il2CppObject * ___isactive1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
