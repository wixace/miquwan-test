﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77_EnumeratorGenerated
struct System_Collections_Generic_List77_EnumeratorGenerated_t3658591980;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Type
struct Type_t;
// MethodID
struct MethodID_t3916401116;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_MethodID3916401116.h"

// System.Void System_Collections_Generic_List77_EnumeratorGenerated::.ctor()
extern "C"  void System_Collections_Generic_List77_EnumeratorGenerated__ctor_m3369055231 (System_Collections_Generic_List77_EnumeratorGenerated_t3658591980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Collections_Generic_List77_EnumeratorGenerated::.cctor()
extern "C"  void System_Collections_Generic_List77_EnumeratorGenerated__cctor_m879400846 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Collections_Generic_List77_EnumeratorGenerated::Enumerator_Current(JSVCall)
extern "C"  void System_Collections_Generic_List77_EnumeratorGenerated_Enumerator_Current_m2502903172 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77_EnumeratorGenerated::Enumerator_Dispose(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77_EnumeratorGenerated_Enumerator_Dispose_m1466798277 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77_EnumeratorGenerated::Enumerator_MoveNext(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77_EnumeratorGenerated_Enumerator_MoveNext_m2607315008 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Collections_Generic_List77_EnumeratorGenerated::__Register()
extern "C"  void System_Collections_Generic_List77_EnumeratorGenerated___Register_m3972886568 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Collections_Generic_List77_EnumeratorGenerated::ilo_setWhatever1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  void System_Collections_Generic_List77_EnumeratorGenerated_ilo_setWhatever1_m2977095706 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System_Collections_Generic_List77_EnumeratorGenerated::ilo_getMethod2(System.Type,MethodID)
extern "C"  MethodInfo_t * System_Collections_Generic_List77_EnumeratorGenerated_ilo_getMethod2_m1890723930 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, MethodID_t3916401116 * ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
