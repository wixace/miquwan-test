﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_doolo107
struct M_doolo107_t3808727073;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_doolo1073808727073.h"

// System.Void GarbageiOS.M_doolo107::.ctor()
extern "C"  void M_doolo107__ctor_m3884247858 (M_doolo107_t3808727073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_doolo107::M_tipooWerechaqall0(System.String[],System.Int32)
extern "C"  void M_doolo107_M_tipooWerechaqall0_m2097808597 (M_doolo107_t3808727073 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_doolo107::M_jaybooFeekoo1(System.String[],System.Int32)
extern "C"  void M_doolo107_M_jaybooFeekoo1_m3008438529 (M_doolo107_t3808727073 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_doolo107::M_hijai2(System.String[],System.Int32)
extern "C"  void M_doolo107_M_hijai2_m1191477564 (M_doolo107_t3808727073 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_doolo107::M_noodurdair3(System.String[],System.Int32)
extern "C"  void M_doolo107_M_noodurdair3_m3850086319 (M_doolo107_t3808727073 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_doolo107::M_baneNajee4(System.String[],System.Int32)
extern "C"  void M_doolo107_M_baneNajee4_m1498121294 (M_doolo107_t3808727073 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_doolo107::M_nispemStadecar5(System.String[],System.Int32)
extern "C"  void M_doolo107_M_nispemStadecar5_m439919127 (M_doolo107_t3808727073 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_doolo107::ilo_M_hijai21(GarbageiOS.M_doolo107,System.String[],System.Int32)
extern "C"  void M_doolo107_ilo_M_hijai21_m2515737215 (Il2CppObject * __this /* static, unused */, M_doolo107_t3808727073 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_doolo107::ilo_M_baneNajee42(GarbageiOS.M_doolo107,System.String[],System.Int32)
extern "C"  void M_doolo107_ilo_M_baneNajee42_m3795457582 (Il2CppObject * __this /* static, unused */, M_doolo107_t3808727073 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
