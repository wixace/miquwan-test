﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Collider2D
struct Collider2D_t1552025098;
// UnityEngine.ContactPoint2D
struct ContactPoint2D_t4288432358;
struct ContactPoint2D_t4288432358_marshaled_pinvoke;
struct ContactPoint2D_t4288432358_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ContactPoint2D4288432358.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// UnityEngine.Vector2 UnityEngine.ContactPoint2D::get_point()
extern "C"  Vector2_t4282066565  ContactPoint2D_get_point_m875769013 (ContactPoint2D_t4288432358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.ContactPoint2D::get_normal()
extern "C"  Vector2_t4282066565  ContactPoint2D_get_normal_m2444620420 (ContactPoint2D_t4288432358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.ContactPoint2D::get_collider()
extern "C"  Collider2D_t1552025098 * ContactPoint2D_get_collider_m2456041800 (ContactPoint2D_t4288432358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.ContactPoint2D::get_otherCollider()
extern "C"  Collider2D_t1552025098 * ContactPoint2D_get_otherCollider_m1425099186 (ContactPoint2D_t4288432358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct ContactPoint2D_t4288432358;
struct ContactPoint2D_t4288432358_marshaled_pinvoke;

extern "C" void ContactPoint2D_t4288432358_marshal_pinvoke(const ContactPoint2D_t4288432358& unmarshaled, ContactPoint2D_t4288432358_marshaled_pinvoke& marshaled);
extern "C" void ContactPoint2D_t4288432358_marshal_pinvoke_back(const ContactPoint2D_t4288432358_marshaled_pinvoke& marshaled, ContactPoint2D_t4288432358& unmarshaled);
extern "C" void ContactPoint2D_t4288432358_marshal_pinvoke_cleanup(ContactPoint2D_t4288432358_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ContactPoint2D_t4288432358;
struct ContactPoint2D_t4288432358_marshaled_com;

extern "C" void ContactPoint2D_t4288432358_marshal_com(const ContactPoint2D_t4288432358& unmarshaled, ContactPoint2D_t4288432358_marshaled_com& marshaled);
extern "C" void ContactPoint2D_t4288432358_marshal_com_back(const ContactPoint2D_t4288432358_marshaled_com& marshaled, ContactPoint2D_t4288432358& unmarshaled);
extern "C" void ContactPoint2D_t4288432358_marshal_com_cleanup(ContactPoint2D_t4288432358_marshaled_com& marshaled);
