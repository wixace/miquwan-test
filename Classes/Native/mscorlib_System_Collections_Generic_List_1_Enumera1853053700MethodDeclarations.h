﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>
struct List_1_t1833380930;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1853053700.h"
#include "AssemblyU2DCSharp_Pathfinding_AdvancedSmooth_Turn465195378.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.AdvancedSmooth/Turn>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m832639577_gshared (Enumerator_t1853053700 * __this, List_1_t1833380930 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m832639577(__this, ___l0, method) ((  void (*) (Enumerator_t1853053700 *, List_1_t1833380930 *, const MethodInfo*))Enumerator__ctor_m832639577_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2564828761_gshared (Enumerator_t1853053700 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2564828761(__this, method) ((  void (*) (Enumerator_t1853053700 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2564828761_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2173023237_gshared (Enumerator_t1853053700 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2173023237(__this, method) ((  Il2CppObject * (*) (Enumerator_t1853053700 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2173023237_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.AdvancedSmooth/Turn>::Dispose()
extern "C"  void Enumerator_Dispose_m1173423742_gshared (Enumerator_t1853053700 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1173423742(__this, method) ((  void (*) (Enumerator_t1853053700 *, const MethodInfo*))Enumerator_Dispose_m1173423742_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.AdvancedSmooth/Turn>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2854311607_gshared (Enumerator_t1853053700 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2854311607(__this, method) ((  void (*) (Enumerator_t1853053700 *, const MethodInfo*))Enumerator_VerifyState_m2854311607_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.AdvancedSmooth/Turn>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2829130309_gshared (Enumerator_t1853053700 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2829130309(__this, method) ((  bool (*) (Enumerator_t1853053700 *, const MethodInfo*))Enumerator_MoveNext_m2829130309_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.AdvancedSmooth/Turn>::get_Current()
extern "C"  Turn_t465195378  Enumerator_get_Current_m3636169966_gshared (Enumerator_t1853053700 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3636169966(__this, method) ((  Turn_t465195378  (*) (Enumerator_t1853053700 *, const MethodInfo*))Enumerator_get_Current_m3636169966_gshared)(__this, method)
