﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_telati128
struct  M_telati128_t2172302076  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_telati128::_mipur
	uint32_t ____mipur_0;
	// System.Boolean GarbageiOS.M_telati128::_cerechujisTairdaber
	bool ____cerechujisTairdaber_1;
	// System.Int32 GarbageiOS.M_telati128::_loogisje
	int32_t ____loogisje_2;
	// System.Single GarbageiOS.M_telati128::_gapay
	float ____gapay_3;
	// System.Int32 GarbageiOS.M_telati128::_trineechoTaykarsear
	int32_t ____trineechoTaykarsear_4;
	// System.Boolean GarbageiOS.M_telati128::_mousoocai
	bool ____mousoocai_5;
	// System.Single GarbageiOS.M_telati128::_beborbor
	float ____beborbor_6;
	// System.Int32 GarbageiOS.M_telati128::_meelearPaburme
	int32_t ____meelearPaburme_7;
	// System.String GarbageiOS.M_telati128::_raykurma
	String_t* ____raykurma_8;
	// System.Boolean GarbageiOS.M_telati128::_dinallLooha
	bool ____dinallLooha_9;
	// System.Int32 GarbageiOS.M_telati128::_kallpaboHismo
	int32_t ____kallpaboHismo_10;
	// System.String GarbageiOS.M_telati128::_medarRilawrur
	String_t* ____medarRilawrur_11;
	// System.Int32 GarbageiOS.M_telati128::_laltraZairper
	int32_t ____laltraZairper_12;
	// System.Boolean GarbageiOS.M_telati128::_sebeefereKercerfe
	bool ____sebeefereKercerfe_13;
	// System.UInt32 GarbageiOS.M_telati128::_pouriKarcall
	uint32_t ____pouriKarcall_14;
	// System.Boolean GarbageiOS.M_telati128::_perekas
	bool ____perekas_15;
	// System.Boolean GarbageiOS.M_telati128::_tove
	bool ____tove_16;
	// System.String GarbageiOS.M_telati128::_zairyeDeamoosear
	String_t* ____zairyeDeamoosear_17;
	// System.Single GarbageiOS.M_telati128::_hamojemJordootra
	float ____hamojemJordootra_18;
	// System.UInt32 GarbageiOS.M_telati128::_laboHoyiwere
	uint32_t ____laboHoyiwere_19;
	// System.Int32 GarbageiOS.M_telati128::_maycarfi
	int32_t ____maycarfi_20;
	// System.Single GarbageiOS.M_telati128::_coulosallMalllor
	float ____coulosallMalllor_21;
	// System.Boolean GarbageiOS.M_telati128::_cougorgouKouje
	bool ____cougorgouKouje_22;
	// System.Boolean GarbageiOS.M_telati128::_berejouseHeris
	bool ____berejouseHeris_23;
	// System.Int32 GarbageiOS.M_telati128::_pitisWorsarlem
	int32_t ____pitisWorsarlem_24;
	// System.Int32 GarbageiOS.M_telati128::_zorsaydisHisnurta
	int32_t ____zorsaydisHisnurta_25;
	// System.String GarbageiOS.M_telati128::_saircurser
	String_t* ____saircurser_26;
	// System.UInt32 GarbageiOS.M_telati128::_soumepuKeehal
	uint32_t ____soumepuKeehal_27;
	// System.UInt32 GarbageiOS.M_telati128::_yadalpis
	uint32_t ____yadalpis_28;
	// System.Single GarbageiOS.M_telati128::_pawdouFalteke
	float ____pawdouFalteke_29;
	// System.Single GarbageiOS.M_telati128::_dersor
	float ____dersor_30;
	// System.Int32 GarbageiOS.M_telati128::_poutardraTroufair
	int32_t ____poutardraTroufair_31;
	// System.String GarbageiOS.M_telati128::_loolaymurLearlasla
	String_t* ____loolaymurLearlasla_32;
	// System.Boolean GarbageiOS.M_telati128::_naqiKurzilou
	bool ____naqiKurzilou_33;
	// System.String GarbageiOS.M_telati128::_hamearCekar
	String_t* ____hamearCekar_34;
	// System.UInt32 GarbageiOS.M_telati128::_cerjemSallfe
	uint32_t ____cerjemSallfe_35;
	// System.Single GarbageiOS.M_telati128::_cefaXemso
	float ____cefaXemso_36;
	// System.UInt32 GarbageiOS.M_telati128::_rawnooWharrerepar
	uint32_t ____rawnooWharrerepar_37;
	// System.String GarbageiOS.M_telati128::_pirkeYagoude
	String_t* ____pirkeYagoude_38;
	// System.Boolean GarbageiOS.M_telati128::_douchasto
	bool ____douchasto_39;
	// System.UInt32 GarbageiOS.M_telati128::_dehelsouMemgo
	uint32_t ____dehelsouMemgo_40;
	// System.Int32 GarbageiOS.M_telati128::_mooma
	int32_t ____mooma_41;
	// System.Int32 GarbageiOS.M_telati128::_pupabiWallmajear
	int32_t ____pupabiWallmajear_42;
	// System.Single GarbageiOS.M_telati128::_semcursto
	float ____semcursto_43;
	// System.Int32 GarbageiOS.M_telati128::_noudrerdaZofe
	int32_t ____noudrerdaZofe_44;
	// System.Boolean GarbageiOS.M_telati128::_gijeXurche
	bool ____gijeXurche_45;
	// System.Boolean GarbageiOS.M_telati128::_xaseTrurne
	bool ____xaseTrurne_46;

public:
	inline static int32_t get_offset_of__mipur_0() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____mipur_0)); }
	inline uint32_t get__mipur_0() const { return ____mipur_0; }
	inline uint32_t* get_address_of__mipur_0() { return &____mipur_0; }
	inline void set__mipur_0(uint32_t value)
	{
		____mipur_0 = value;
	}

	inline static int32_t get_offset_of__cerechujisTairdaber_1() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____cerechujisTairdaber_1)); }
	inline bool get__cerechujisTairdaber_1() const { return ____cerechujisTairdaber_1; }
	inline bool* get_address_of__cerechujisTairdaber_1() { return &____cerechujisTairdaber_1; }
	inline void set__cerechujisTairdaber_1(bool value)
	{
		____cerechujisTairdaber_1 = value;
	}

	inline static int32_t get_offset_of__loogisje_2() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____loogisje_2)); }
	inline int32_t get__loogisje_2() const { return ____loogisje_2; }
	inline int32_t* get_address_of__loogisje_2() { return &____loogisje_2; }
	inline void set__loogisje_2(int32_t value)
	{
		____loogisje_2 = value;
	}

	inline static int32_t get_offset_of__gapay_3() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____gapay_3)); }
	inline float get__gapay_3() const { return ____gapay_3; }
	inline float* get_address_of__gapay_3() { return &____gapay_3; }
	inline void set__gapay_3(float value)
	{
		____gapay_3 = value;
	}

	inline static int32_t get_offset_of__trineechoTaykarsear_4() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____trineechoTaykarsear_4)); }
	inline int32_t get__trineechoTaykarsear_4() const { return ____trineechoTaykarsear_4; }
	inline int32_t* get_address_of__trineechoTaykarsear_4() { return &____trineechoTaykarsear_4; }
	inline void set__trineechoTaykarsear_4(int32_t value)
	{
		____trineechoTaykarsear_4 = value;
	}

	inline static int32_t get_offset_of__mousoocai_5() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____mousoocai_5)); }
	inline bool get__mousoocai_5() const { return ____mousoocai_5; }
	inline bool* get_address_of__mousoocai_5() { return &____mousoocai_5; }
	inline void set__mousoocai_5(bool value)
	{
		____mousoocai_5 = value;
	}

	inline static int32_t get_offset_of__beborbor_6() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____beborbor_6)); }
	inline float get__beborbor_6() const { return ____beborbor_6; }
	inline float* get_address_of__beborbor_6() { return &____beborbor_6; }
	inline void set__beborbor_6(float value)
	{
		____beborbor_6 = value;
	}

	inline static int32_t get_offset_of__meelearPaburme_7() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____meelearPaburme_7)); }
	inline int32_t get__meelearPaburme_7() const { return ____meelearPaburme_7; }
	inline int32_t* get_address_of__meelearPaburme_7() { return &____meelearPaburme_7; }
	inline void set__meelearPaburme_7(int32_t value)
	{
		____meelearPaburme_7 = value;
	}

	inline static int32_t get_offset_of__raykurma_8() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____raykurma_8)); }
	inline String_t* get__raykurma_8() const { return ____raykurma_8; }
	inline String_t** get_address_of__raykurma_8() { return &____raykurma_8; }
	inline void set__raykurma_8(String_t* value)
	{
		____raykurma_8 = value;
		Il2CppCodeGenWriteBarrier(&____raykurma_8, value);
	}

	inline static int32_t get_offset_of__dinallLooha_9() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____dinallLooha_9)); }
	inline bool get__dinallLooha_9() const { return ____dinallLooha_9; }
	inline bool* get_address_of__dinallLooha_9() { return &____dinallLooha_9; }
	inline void set__dinallLooha_9(bool value)
	{
		____dinallLooha_9 = value;
	}

	inline static int32_t get_offset_of__kallpaboHismo_10() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____kallpaboHismo_10)); }
	inline int32_t get__kallpaboHismo_10() const { return ____kallpaboHismo_10; }
	inline int32_t* get_address_of__kallpaboHismo_10() { return &____kallpaboHismo_10; }
	inline void set__kallpaboHismo_10(int32_t value)
	{
		____kallpaboHismo_10 = value;
	}

	inline static int32_t get_offset_of__medarRilawrur_11() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____medarRilawrur_11)); }
	inline String_t* get__medarRilawrur_11() const { return ____medarRilawrur_11; }
	inline String_t** get_address_of__medarRilawrur_11() { return &____medarRilawrur_11; }
	inline void set__medarRilawrur_11(String_t* value)
	{
		____medarRilawrur_11 = value;
		Il2CppCodeGenWriteBarrier(&____medarRilawrur_11, value);
	}

	inline static int32_t get_offset_of__laltraZairper_12() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____laltraZairper_12)); }
	inline int32_t get__laltraZairper_12() const { return ____laltraZairper_12; }
	inline int32_t* get_address_of__laltraZairper_12() { return &____laltraZairper_12; }
	inline void set__laltraZairper_12(int32_t value)
	{
		____laltraZairper_12 = value;
	}

	inline static int32_t get_offset_of__sebeefereKercerfe_13() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____sebeefereKercerfe_13)); }
	inline bool get__sebeefereKercerfe_13() const { return ____sebeefereKercerfe_13; }
	inline bool* get_address_of__sebeefereKercerfe_13() { return &____sebeefereKercerfe_13; }
	inline void set__sebeefereKercerfe_13(bool value)
	{
		____sebeefereKercerfe_13 = value;
	}

	inline static int32_t get_offset_of__pouriKarcall_14() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____pouriKarcall_14)); }
	inline uint32_t get__pouriKarcall_14() const { return ____pouriKarcall_14; }
	inline uint32_t* get_address_of__pouriKarcall_14() { return &____pouriKarcall_14; }
	inline void set__pouriKarcall_14(uint32_t value)
	{
		____pouriKarcall_14 = value;
	}

	inline static int32_t get_offset_of__perekas_15() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____perekas_15)); }
	inline bool get__perekas_15() const { return ____perekas_15; }
	inline bool* get_address_of__perekas_15() { return &____perekas_15; }
	inline void set__perekas_15(bool value)
	{
		____perekas_15 = value;
	}

	inline static int32_t get_offset_of__tove_16() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____tove_16)); }
	inline bool get__tove_16() const { return ____tove_16; }
	inline bool* get_address_of__tove_16() { return &____tove_16; }
	inline void set__tove_16(bool value)
	{
		____tove_16 = value;
	}

	inline static int32_t get_offset_of__zairyeDeamoosear_17() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____zairyeDeamoosear_17)); }
	inline String_t* get__zairyeDeamoosear_17() const { return ____zairyeDeamoosear_17; }
	inline String_t** get_address_of__zairyeDeamoosear_17() { return &____zairyeDeamoosear_17; }
	inline void set__zairyeDeamoosear_17(String_t* value)
	{
		____zairyeDeamoosear_17 = value;
		Il2CppCodeGenWriteBarrier(&____zairyeDeamoosear_17, value);
	}

	inline static int32_t get_offset_of__hamojemJordootra_18() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____hamojemJordootra_18)); }
	inline float get__hamojemJordootra_18() const { return ____hamojemJordootra_18; }
	inline float* get_address_of__hamojemJordootra_18() { return &____hamojemJordootra_18; }
	inline void set__hamojemJordootra_18(float value)
	{
		____hamojemJordootra_18 = value;
	}

	inline static int32_t get_offset_of__laboHoyiwere_19() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____laboHoyiwere_19)); }
	inline uint32_t get__laboHoyiwere_19() const { return ____laboHoyiwere_19; }
	inline uint32_t* get_address_of__laboHoyiwere_19() { return &____laboHoyiwere_19; }
	inline void set__laboHoyiwere_19(uint32_t value)
	{
		____laboHoyiwere_19 = value;
	}

	inline static int32_t get_offset_of__maycarfi_20() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____maycarfi_20)); }
	inline int32_t get__maycarfi_20() const { return ____maycarfi_20; }
	inline int32_t* get_address_of__maycarfi_20() { return &____maycarfi_20; }
	inline void set__maycarfi_20(int32_t value)
	{
		____maycarfi_20 = value;
	}

	inline static int32_t get_offset_of__coulosallMalllor_21() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____coulosallMalllor_21)); }
	inline float get__coulosallMalllor_21() const { return ____coulosallMalllor_21; }
	inline float* get_address_of__coulosallMalllor_21() { return &____coulosallMalllor_21; }
	inline void set__coulosallMalllor_21(float value)
	{
		____coulosallMalllor_21 = value;
	}

	inline static int32_t get_offset_of__cougorgouKouje_22() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____cougorgouKouje_22)); }
	inline bool get__cougorgouKouje_22() const { return ____cougorgouKouje_22; }
	inline bool* get_address_of__cougorgouKouje_22() { return &____cougorgouKouje_22; }
	inline void set__cougorgouKouje_22(bool value)
	{
		____cougorgouKouje_22 = value;
	}

	inline static int32_t get_offset_of__berejouseHeris_23() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____berejouseHeris_23)); }
	inline bool get__berejouseHeris_23() const { return ____berejouseHeris_23; }
	inline bool* get_address_of__berejouseHeris_23() { return &____berejouseHeris_23; }
	inline void set__berejouseHeris_23(bool value)
	{
		____berejouseHeris_23 = value;
	}

	inline static int32_t get_offset_of__pitisWorsarlem_24() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____pitisWorsarlem_24)); }
	inline int32_t get__pitisWorsarlem_24() const { return ____pitisWorsarlem_24; }
	inline int32_t* get_address_of__pitisWorsarlem_24() { return &____pitisWorsarlem_24; }
	inline void set__pitisWorsarlem_24(int32_t value)
	{
		____pitisWorsarlem_24 = value;
	}

	inline static int32_t get_offset_of__zorsaydisHisnurta_25() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____zorsaydisHisnurta_25)); }
	inline int32_t get__zorsaydisHisnurta_25() const { return ____zorsaydisHisnurta_25; }
	inline int32_t* get_address_of__zorsaydisHisnurta_25() { return &____zorsaydisHisnurta_25; }
	inline void set__zorsaydisHisnurta_25(int32_t value)
	{
		____zorsaydisHisnurta_25 = value;
	}

	inline static int32_t get_offset_of__saircurser_26() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____saircurser_26)); }
	inline String_t* get__saircurser_26() const { return ____saircurser_26; }
	inline String_t** get_address_of__saircurser_26() { return &____saircurser_26; }
	inline void set__saircurser_26(String_t* value)
	{
		____saircurser_26 = value;
		Il2CppCodeGenWriteBarrier(&____saircurser_26, value);
	}

	inline static int32_t get_offset_of__soumepuKeehal_27() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____soumepuKeehal_27)); }
	inline uint32_t get__soumepuKeehal_27() const { return ____soumepuKeehal_27; }
	inline uint32_t* get_address_of__soumepuKeehal_27() { return &____soumepuKeehal_27; }
	inline void set__soumepuKeehal_27(uint32_t value)
	{
		____soumepuKeehal_27 = value;
	}

	inline static int32_t get_offset_of__yadalpis_28() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____yadalpis_28)); }
	inline uint32_t get__yadalpis_28() const { return ____yadalpis_28; }
	inline uint32_t* get_address_of__yadalpis_28() { return &____yadalpis_28; }
	inline void set__yadalpis_28(uint32_t value)
	{
		____yadalpis_28 = value;
	}

	inline static int32_t get_offset_of__pawdouFalteke_29() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____pawdouFalteke_29)); }
	inline float get__pawdouFalteke_29() const { return ____pawdouFalteke_29; }
	inline float* get_address_of__pawdouFalteke_29() { return &____pawdouFalteke_29; }
	inline void set__pawdouFalteke_29(float value)
	{
		____pawdouFalteke_29 = value;
	}

	inline static int32_t get_offset_of__dersor_30() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____dersor_30)); }
	inline float get__dersor_30() const { return ____dersor_30; }
	inline float* get_address_of__dersor_30() { return &____dersor_30; }
	inline void set__dersor_30(float value)
	{
		____dersor_30 = value;
	}

	inline static int32_t get_offset_of__poutardraTroufair_31() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____poutardraTroufair_31)); }
	inline int32_t get__poutardraTroufair_31() const { return ____poutardraTroufair_31; }
	inline int32_t* get_address_of__poutardraTroufair_31() { return &____poutardraTroufair_31; }
	inline void set__poutardraTroufair_31(int32_t value)
	{
		____poutardraTroufair_31 = value;
	}

	inline static int32_t get_offset_of__loolaymurLearlasla_32() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____loolaymurLearlasla_32)); }
	inline String_t* get__loolaymurLearlasla_32() const { return ____loolaymurLearlasla_32; }
	inline String_t** get_address_of__loolaymurLearlasla_32() { return &____loolaymurLearlasla_32; }
	inline void set__loolaymurLearlasla_32(String_t* value)
	{
		____loolaymurLearlasla_32 = value;
		Il2CppCodeGenWriteBarrier(&____loolaymurLearlasla_32, value);
	}

	inline static int32_t get_offset_of__naqiKurzilou_33() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____naqiKurzilou_33)); }
	inline bool get__naqiKurzilou_33() const { return ____naqiKurzilou_33; }
	inline bool* get_address_of__naqiKurzilou_33() { return &____naqiKurzilou_33; }
	inline void set__naqiKurzilou_33(bool value)
	{
		____naqiKurzilou_33 = value;
	}

	inline static int32_t get_offset_of__hamearCekar_34() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____hamearCekar_34)); }
	inline String_t* get__hamearCekar_34() const { return ____hamearCekar_34; }
	inline String_t** get_address_of__hamearCekar_34() { return &____hamearCekar_34; }
	inline void set__hamearCekar_34(String_t* value)
	{
		____hamearCekar_34 = value;
		Il2CppCodeGenWriteBarrier(&____hamearCekar_34, value);
	}

	inline static int32_t get_offset_of__cerjemSallfe_35() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____cerjemSallfe_35)); }
	inline uint32_t get__cerjemSallfe_35() const { return ____cerjemSallfe_35; }
	inline uint32_t* get_address_of__cerjemSallfe_35() { return &____cerjemSallfe_35; }
	inline void set__cerjemSallfe_35(uint32_t value)
	{
		____cerjemSallfe_35 = value;
	}

	inline static int32_t get_offset_of__cefaXemso_36() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____cefaXemso_36)); }
	inline float get__cefaXemso_36() const { return ____cefaXemso_36; }
	inline float* get_address_of__cefaXemso_36() { return &____cefaXemso_36; }
	inline void set__cefaXemso_36(float value)
	{
		____cefaXemso_36 = value;
	}

	inline static int32_t get_offset_of__rawnooWharrerepar_37() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____rawnooWharrerepar_37)); }
	inline uint32_t get__rawnooWharrerepar_37() const { return ____rawnooWharrerepar_37; }
	inline uint32_t* get_address_of__rawnooWharrerepar_37() { return &____rawnooWharrerepar_37; }
	inline void set__rawnooWharrerepar_37(uint32_t value)
	{
		____rawnooWharrerepar_37 = value;
	}

	inline static int32_t get_offset_of__pirkeYagoude_38() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____pirkeYagoude_38)); }
	inline String_t* get__pirkeYagoude_38() const { return ____pirkeYagoude_38; }
	inline String_t** get_address_of__pirkeYagoude_38() { return &____pirkeYagoude_38; }
	inline void set__pirkeYagoude_38(String_t* value)
	{
		____pirkeYagoude_38 = value;
		Il2CppCodeGenWriteBarrier(&____pirkeYagoude_38, value);
	}

	inline static int32_t get_offset_of__douchasto_39() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____douchasto_39)); }
	inline bool get__douchasto_39() const { return ____douchasto_39; }
	inline bool* get_address_of__douchasto_39() { return &____douchasto_39; }
	inline void set__douchasto_39(bool value)
	{
		____douchasto_39 = value;
	}

	inline static int32_t get_offset_of__dehelsouMemgo_40() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____dehelsouMemgo_40)); }
	inline uint32_t get__dehelsouMemgo_40() const { return ____dehelsouMemgo_40; }
	inline uint32_t* get_address_of__dehelsouMemgo_40() { return &____dehelsouMemgo_40; }
	inline void set__dehelsouMemgo_40(uint32_t value)
	{
		____dehelsouMemgo_40 = value;
	}

	inline static int32_t get_offset_of__mooma_41() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____mooma_41)); }
	inline int32_t get__mooma_41() const { return ____mooma_41; }
	inline int32_t* get_address_of__mooma_41() { return &____mooma_41; }
	inline void set__mooma_41(int32_t value)
	{
		____mooma_41 = value;
	}

	inline static int32_t get_offset_of__pupabiWallmajear_42() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____pupabiWallmajear_42)); }
	inline int32_t get__pupabiWallmajear_42() const { return ____pupabiWallmajear_42; }
	inline int32_t* get_address_of__pupabiWallmajear_42() { return &____pupabiWallmajear_42; }
	inline void set__pupabiWallmajear_42(int32_t value)
	{
		____pupabiWallmajear_42 = value;
	}

	inline static int32_t get_offset_of__semcursto_43() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____semcursto_43)); }
	inline float get__semcursto_43() const { return ____semcursto_43; }
	inline float* get_address_of__semcursto_43() { return &____semcursto_43; }
	inline void set__semcursto_43(float value)
	{
		____semcursto_43 = value;
	}

	inline static int32_t get_offset_of__noudrerdaZofe_44() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____noudrerdaZofe_44)); }
	inline int32_t get__noudrerdaZofe_44() const { return ____noudrerdaZofe_44; }
	inline int32_t* get_address_of__noudrerdaZofe_44() { return &____noudrerdaZofe_44; }
	inline void set__noudrerdaZofe_44(int32_t value)
	{
		____noudrerdaZofe_44 = value;
	}

	inline static int32_t get_offset_of__gijeXurche_45() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____gijeXurche_45)); }
	inline bool get__gijeXurche_45() const { return ____gijeXurche_45; }
	inline bool* get_address_of__gijeXurche_45() { return &____gijeXurche_45; }
	inline void set__gijeXurche_45(bool value)
	{
		____gijeXurche_45 = value;
	}

	inline static int32_t get_offset_of__xaseTrurne_46() { return static_cast<int32_t>(offsetof(M_telati128_t2172302076, ____xaseTrurne_46)); }
	inline bool get__xaseTrurne_46() const { return ____xaseTrurne_46; }
	inline bool* get_address_of__xaseTrurne_46() { return &____xaseTrurne_46; }
	inline void set__xaseTrurne_46(bool value)
	{
		____xaseTrurne_46 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
