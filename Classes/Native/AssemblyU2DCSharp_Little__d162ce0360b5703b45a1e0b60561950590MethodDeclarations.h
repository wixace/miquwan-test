﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._d162ce0360b5703b45a1e0b60bc1dca4
struct _d162ce0360b5703b45a1e0b60bc1dca4_t561950590;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__d162ce0360b5703b45a1e0b60561950590.h"

// System.Void Little._d162ce0360b5703b45a1e0b60bc1dca4::.ctor()
extern "C"  void _d162ce0360b5703b45a1e0b60bc1dca4__ctor_m3632202863 (_d162ce0360b5703b45a1e0b60bc1dca4_t561950590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._d162ce0360b5703b45a1e0b60bc1dca4::_d162ce0360b5703b45a1e0b60bc1dca4m2(System.Int32)
extern "C"  int32_t _d162ce0360b5703b45a1e0b60bc1dca4__d162ce0360b5703b45a1e0b60bc1dca4m2_m3158307033 (_d162ce0360b5703b45a1e0b60bc1dca4_t561950590 * __this, int32_t ____d162ce0360b5703b45a1e0b60bc1dca4a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._d162ce0360b5703b45a1e0b60bc1dca4::_d162ce0360b5703b45a1e0b60bc1dca4m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _d162ce0360b5703b45a1e0b60bc1dca4__d162ce0360b5703b45a1e0b60bc1dca4m_m3418512893 (_d162ce0360b5703b45a1e0b60bc1dca4_t561950590 * __this, int32_t ____d162ce0360b5703b45a1e0b60bc1dca4a0, int32_t ____d162ce0360b5703b45a1e0b60bc1dca4961, int32_t ____d162ce0360b5703b45a1e0b60bc1dca4c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._d162ce0360b5703b45a1e0b60bc1dca4::ilo__d162ce0360b5703b45a1e0b60bc1dca4m21(Little._d162ce0360b5703b45a1e0b60bc1dca4,System.Int32)
extern "C"  int32_t _d162ce0360b5703b45a1e0b60bc1dca4_ilo__d162ce0360b5703b45a1e0b60bc1dca4m21_m1946861317 (Il2CppObject * __this /* static, unused */, _d162ce0360b5703b45a1e0b60bc1dca4_t561950590 * ____this0, int32_t ____d162ce0360b5703b45a1e0b60bc1dca4a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
