﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReplayRage
struct ReplayRage_t780179444;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ReplayRage::.ctor()
extern "C"  void ReplayRage__ctor_m3842290471 (ReplayRage_t780179444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayRage::.ctor(System.Object[])
extern "C"  void ReplayRage__ctor_m3301780907 (ReplayRage_t780179444 * __this, ObjectU5BU5D_t1108656482* ___datas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] ReplayRage::ParserJsonStr(System.String)
extern "C"  StringU5BU5D_t4054002952* ReplayRage_ParserJsonStr_m3112569300 (ReplayRage_t780179444 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReplayRage::GetJsonStr()
extern "C"  String_t* ReplayRage_GetJsonStr_m3412261011 (ReplayRage_t780179444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayRage::Execute()
extern "C"  void ReplayRage_Execute_m1620657530 (ReplayRage_t780179444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
