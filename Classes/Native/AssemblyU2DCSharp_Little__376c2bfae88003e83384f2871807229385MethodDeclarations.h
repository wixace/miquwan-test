﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._376c2bfae88003e83384f2875e4d61c4
struct _376c2bfae88003e83384f2875e4d61c4_t1807229385;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._376c2bfae88003e83384f2875e4d61c4::.ctor()
extern "C"  void _376c2bfae88003e83384f2875e4d61c4__ctor_m1145697924 (_376c2bfae88003e83384f2875e4d61c4_t1807229385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._376c2bfae88003e83384f2875e4d61c4::_376c2bfae88003e83384f2875e4d61c4m2(System.Int32)
extern "C"  int32_t _376c2bfae88003e83384f2875e4d61c4__376c2bfae88003e83384f2875e4d61c4m2_m1936979193 (_376c2bfae88003e83384f2875e4d61c4_t1807229385 * __this, int32_t ____376c2bfae88003e83384f2875e4d61c4a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._376c2bfae88003e83384f2875e4d61c4::_376c2bfae88003e83384f2875e4d61c4m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _376c2bfae88003e83384f2875e4d61c4__376c2bfae88003e83384f2875e4d61c4m_m2005220829 (_376c2bfae88003e83384f2875e4d61c4_t1807229385 * __this, int32_t ____376c2bfae88003e83384f2875e4d61c4a0, int32_t ____376c2bfae88003e83384f2875e4d61c4791, int32_t ____376c2bfae88003e83384f2875e4d61c4c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
