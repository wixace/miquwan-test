﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Poly2Tri.DTSweepConstraint
struct DTSweepConstraint_t3360279023;
// Pathfinding.Poly2Tri.TriangulationPoint
struct TriangulationPoint_t3810082933;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul3810082933.h"

// System.Void Pathfinding.Poly2Tri.DTSweepConstraint::.ctor(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  void DTSweepConstraint__ctor_m259070556 (DTSweepConstraint_t3360279023 * __this, TriangulationPoint_t3810082933 * ___p10, TriangulationPoint_t3810082933 * ___p21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
