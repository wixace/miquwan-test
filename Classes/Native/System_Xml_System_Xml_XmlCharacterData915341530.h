﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "System_Xml_System_Xml_XmlLinkedNode901819716.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCharacterData
struct  XmlCharacterData_t915341530  : public XmlLinkedNode_t901819716
{
public:
	// System.String System.Xml.XmlCharacterData::data
	String_t* ___data_8;

public:
	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(XmlCharacterData_t915341530, ___data_8)); }
	inline String_t* get_data_8() const { return ___data_8; }
	inline String_t** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(String_t* value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier(&___data_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
