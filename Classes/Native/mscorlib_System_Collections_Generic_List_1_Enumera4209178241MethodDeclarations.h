﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>
struct List_1_t4189505471;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4209178241.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_Inter2821319919.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/IntersectionPair>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m550476822_gshared (Enumerator_t4209178241 * __this, List_1_t4189505471 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m550476822(__this, ___l0, method) ((  void (*) (Enumerator_t4209178241 *, List_1_t4189505471 *, const MethodInfo*))Enumerator__ctor_m550476822_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3003630204_gshared (Enumerator_t4209178241 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3003630204(__this, method) ((  void (*) (Enumerator_t4209178241 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3003630204_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3715242344_gshared (Enumerator_t4209178241 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3715242344(__this, method) ((  Il2CppObject * (*) (Enumerator_t4209178241 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3715242344_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/IntersectionPair>::Dispose()
extern "C"  void Enumerator_Dispose_m2041319291_gshared (Enumerator_t4209178241 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2041319291(__this, method) ((  void (*) (Enumerator_t4209178241 *, const MethodInfo*))Enumerator_Dispose_m2041319291_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/IntersectionPair>::VerifyState()
extern "C"  void Enumerator_VerifyState_m117807412_gshared (Enumerator_t4209178241 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m117807412(__this, method) ((  void (*) (Enumerator_t4209178241 *, const MethodInfo*))Enumerator_VerifyState_m117807412_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/IntersectionPair>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4050142184_gshared (Enumerator_t4209178241 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m4050142184(__this, method) ((  bool (*) (Enumerator_t4209178241 *, const MethodInfo*))Enumerator_MoveNext_m4050142184_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/IntersectionPair>::get_Current()
extern "C"  IntersectionPair_t2821319919  Enumerator_get_Current_m2534238443_gshared (Enumerator_t4209178241 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2534238443(__this, method) ((  IntersectionPair_t2821319919  (*) (Enumerator_t4209178241 *, const MethodInfo*))Enumerator_get_Current_m2534238443_gshared)(__this, method)
