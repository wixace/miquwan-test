﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AssetsList
struct AssetsList_t4140497633;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void AssetsList::.ctor()
extern "C"  void AssetsList__ctor_m356928218 (AssetsList_t4140497633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AssetsList::GetAsset(System.String)
extern "C"  String_t* AssetsList_GetAsset_m3121393435 (AssetsList_t4140497633 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetsList::Clear()
extern "C"  void AssetsList_Clear_m2058028805 (AssetsList_t4140497633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
