﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TargetMotionBlur/OnFinish
struct OnFinish_t840705683;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void TargetMotionBlur/OnFinish::.ctor(System.Object,System.IntPtr)
extern "C"  void OnFinish__ctor_m3399967978 (OnFinish_t840705683 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetMotionBlur/OnFinish::Invoke()
extern "C"  void OnFinish_Invoke_m1628922308 (OnFinish_t840705683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult TargetMotionBlur/OnFinish::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnFinish_BeginInvoke_m2417295431 (OnFinish_t840705683 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetMotionBlur/OnFinish::EndInvoke(System.IAsyncResult)
extern "C"  void OnFinish_EndInvoke_m1064688634 (OnFinish_t840705683 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
