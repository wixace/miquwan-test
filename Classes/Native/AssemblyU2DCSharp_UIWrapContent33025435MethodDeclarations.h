﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIWrapContent
struct UIWrapContent_t33025435;
// UIPanel
struct UIPanel_t295209936;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIPanel295209936.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_UIWrapContent33025435.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UIWrapContent::.ctor()
extern "C"  void UIWrapContent__ctor_m282366512 (UIWrapContent_t33025435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWrapContent::Start()
extern "C"  void UIWrapContent_Start_m3524471600 (UIWrapContent_t33025435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWrapContent::OnMove(UIPanel)
extern "C"  void UIWrapContent_OnMove_m3675848574 (UIWrapContent_t33025435 * __this, UIPanel_t295209936 * ___panel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWrapContent::SortBasedOnScrollMovement()
extern "C"  void UIWrapContent_SortBasedOnScrollMovement_m695931902 (UIWrapContent_t33025435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWrapContent::SortAlphabetically()
extern "C"  void UIWrapContent_SortAlphabetically_m1473128343 (UIWrapContent_t33025435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWrapContent::CacheScrollView()
extern "C"  bool UIWrapContent_CacheScrollView_m1949504758 (UIWrapContent_t33025435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWrapContent::ResetChildPositions()
extern "C"  void UIWrapContent_ResetChildPositions_m1267405611 (UIWrapContent_t33025435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWrapContent::WrapContent()
extern "C"  void UIWrapContent_WrapContent_m2368030365 (UIWrapContent_t33025435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWrapContent::OnValidate()
extern "C"  void UIWrapContent_OnValidate_m3655927209 (UIWrapContent_t33025435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32)
extern "C"  void UIWrapContent_UpdateItem_m3898182666 (UIWrapContent_t33025435 * __this, Transform_t1659122786 * ___item0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWrapContent::ilo_SortBasedOnScrollMovement1(UIWrapContent)
extern "C"  void UIWrapContent_ilo_SortBasedOnScrollMovement1_m3302555823 (Il2CppObject * __this /* static, unused */, UIWrapContent_t33025435 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWrapContent::ilo_CacheScrollView2(UIWrapContent)
extern "C"  bool UIWrapContent_ilo_CacheScrollView2_m2238863302 (Il2CppObject * __this /* static, unused */, UIWrapContent_t33025435 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWrapContent::ilo_ResetChildPositions3(UIWrapContent)
extern "C"  void UIWrapContent_ilo_ResetChildPositions3_m1259040410 (Il2CppObject * __this /* static, unused */, UIWrapContent_t33025435 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWrapContent::ilo_UpdateItem4(UIWrapContent,UnityEngine.Transform,System.Int32)
extern "C"  void UIWrapContent_ilo_UpdateItem4_m2139421916 (Il2CppObject * __this /* static, unused */, UIWrapContent_t33025435 * ____this0, Transform_t1659122786 * ___item1, int32_t ___index2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UIWrapContent::ilo_get_clipOffset5(UIPanel)
extern "C"  Vector2_t4282066565  UIWrapContent_ilo_get_clipOffset5_m4223406115 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWrapContent::ilo_SetActive6(UnityEngine.GameObject,System.Boolean,System.Boolean)
extern "C"  void UIWrapContent_ilo_SetActive6_m633695757 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, bool ___state1, bool ___compatibilityMode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
