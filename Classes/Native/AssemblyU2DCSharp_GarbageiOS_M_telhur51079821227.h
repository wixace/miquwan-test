﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_telhur5
struct  M_telhur5_t1079821227  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_telhur5::_luveahouRermo
	float ____luveahouRermo_0;
	// System.Single GarbageiOS.M_telhur5::_waltrea
	float ____waltrea_1;
	// System.Single GarbageiOS.M_telhur5::_gercaHelmeenem
	float ____gercaHelmeenem_2;
	// System.Int32 GarbageiOS.M_telhur5::_korra
	int32_t ____korra_3;
	// System.Single GarbageiOS.M_telhur5::_pakaNouhidea
	float ____pakaNouhidea_4;
	// System.Int32 GarbageiOS.M_telhur5::_qispear
	int32_t ____qispear_5;
	// System.Int32 GarbageiOS.M_telhur5::_mesaisas
	int32_t ____mesaisas_6;
	// System.UInt32 GarbageiOS.M_telhur5::_mocheakalCouca
	uint32_t ____mocheakalCouca_7;
	// System.Boolean GarbageiOS.M_telhur5::_chasewiKidras
	bool ____chasewiKidras_8;
	// System.Int32 GarbageiOS.M_telhur5::_cuta
	int32_t ____cuta_9;
	// System.Single GarbageiOS.M_telhur5::_qeehallher
	float ____qeehallher_10;
	// System.String GarbageiOS.M_telhur5::_nomasneDerecal
	String_t* ____nomasneDerecal_11;
	// System.Boolean GarbageiOS.M_telhur5::_gairtrerebi
	bool ____gairtrerebi_12;
	// System.Int32 GarbageiOS.M_telhur5::_mearceldairToudrearpear
	int32_t ____mearceldairToudrearpear_13;
	// System.Boolean GarbageiOS.M_telhur5::_wacerpoQiremtri
	bool ____wacerpoQiremtri_14;
	// System.Int32 GarbageiOS.M_telhur5::_whetriCircelja
	int32_t ____whetriCircelja_15;
	// System.Int32 GarbageiOS.M_telhur5::_kaijameJiher
	int32_t ____kaijameJiher_16;
	// System.Boolean GarbageiOS.M_telhur5::_sinargairHowqem
	bool ____sinargairHowqem_17;
	// System.String GarbageiOS.M_telhur5::_hiraiDrerekas
	String_t* ____hiraiDrerekas_18;
	// System.Boolean GarbageiOS.M_telhur5::_resinisLeawoujall
	bool ____resinisLeawoujall_19;
	// System.Boolean GarbageiOS.M_telhur5::_staycalraLeejawgea
	bool ____staycalraLeejawgea_20;
	// System.Single GarbageiOS.M_telhur5::_jisairfurTedi
	float ____jisairfurTedi_21;
	// System.String GarbageiOS.M_telhur5::_pirdaltrur
	String_t* ____pirdaltrur_22;
	// System.Int32 GarbageiOS.M_telhur5::_saltreyeKicidu
	int32_t ____saltreyeKicidu_23;
	// System.UInt32 GarbageiOS.M_telhur5::_dulearaNaychemyall
	uint32_t ____dulearaNaychemyall_24;
	// System.String GarbageiOS.M_telhur5::_masomerYeba
	String_t* ____masomerYeba_25;
	// System.Single GarbageiOS.M_telhur5::_naynee
	float ____naynee_26;
	// System.UInt32 GarbageiOS.M_telhur5::_faterenerTogurbea
	uint32_t ____faterenerTogurbea_27;
	// System.Int32 GarbageiOS.M_telhur5::_sowrupas
	int32_t ____sowrupas_28;
	// System.Int32 GarbageiOS.M_telhur5::_hegawawJallrissta
	int32_t ____hegawawJallrissta_29;
	// System.Int32 GarbageiOS.M_telhur5::_sallbaNalne
	int32_t ____sallbaNalne_30;
	// System.Boolean GarbageiOS.M_telhur5::_roosai
	bool ____roosai_31;

public:
	inline static int32_t get_offset_of__luveahouRermo_0() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____luveahouRermo_0)); }
	inline float get__luveahouRermo_0() const { return ____luveahouRermo_0; }
	inline float* get_address_of__luveahouRermo_0() { return &____luveahouRermo_0; }
	inline void set__luveahouRermo_0(float value)
	{
		____luveahouRermo_0 = value;
	}

	inline static int32_t get_offset_of__waltrea_1() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____waltrea_1)); }
	inline float get__waltrea_1() const { return ____waltrea_1; }
	inline float* get_address_of__waltrea_1() { return &____waltrea_1; }
	inline void set__waltrea_1(float value)
	{
		____waltrea_1 = value;
	}

	inline static int32_t get_offset_of__gercaHelmeenem_2() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____gercaHelmeenem_2)); }
	inline float get__gercaHelmeenem_2() const { return ____gercaHelmeenem_2; }
	inline float* get_address_of__gercaHelmeenem_2() { return &____gercaHelmeenem_2; }
	inline void set__gercaHelmeenem_2(float value)
	{
		____gercaHelmeenem_2 = value;
	}

	inline static int32_t get_offset_of__korra_3() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____korra_3)); }
	inline int32_t get__korra_3() const { return ____korra_3; }
	inline int32_t* get_address_of__korra_3() { return &____korra_3; }
	inline void set__korra_3(int32_t value)
	{
		____korra_3 = value;
	}

	inline static int32_t get_offset_of__pakaNouhidea_4() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____pakaNouhidea_4)); }
	inline float get__pakaNouhidea_4() const { return ____pakaNouhidea_4; }
	inline float* get_address_of__pakaNouhidea_4() { return &____pakaNouhidea_4; }
	inline void set__pakaNouhidea_4(float value)
	{
		____pakaNouhidea_4 = value;
	}

	inline static int32_t get_offset_of__qispear_5() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____qispear_5)); }
	inline int32_t get__qispear_5() const { return ____qispear_5; }
	inline int32_t* get_address_of__qispear_5() { return &____qispear_5; }
	inline void set__qispear_5(int32_t value)
	{
		____qispear_5 = value;
	}

	inline static int32_t get_offset_of__mesaisas_6() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____mesaisas_6)); }
	inline int32_t get__mesaisas_6() const { return ____mesaisas_6; }
	inline int32_t* get_address_of__mesaisas_6() { return &____mesaisas_6; }
	inline void set__mesaisas_6(int32_t value)
	{
		____mesaisas_6 = value;
	}

	inline static int32_t get_offset_of__mocheakalCouca_7() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____mocheakalCouca_7)); }
	inline uint32_t get__mocheakalCouca_7() const { return ____mocheakalCouca_7; }
	inline uint32_t* get_address_of__mocheakalCouca_7() { return &____mocheakalCouca_7; }
	inline void set__mocheakalCouca_7(uint32_t value)
	{
		____mocheakalCouca_7 = value;
	}

	inline static int32_t get_offset_of__chasewiKidras_8() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____chasewiKidras_8)); }
	inline bool get__chasewiKidras_8() const { return ____chasewiKidras_8; }
	inline bool* get_address_of__chasewiKidras_8() { return &____chasewiKidras_8; }
	inline void set__chasewiKidras_8(bool value)
	{
		____chasewiKidras_8 = value;
	}

	inline static int32_t get_offset_of__cuta_9() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____cuta_9)); }
	inline int32_t get__cuta_9() const { return ____cuta_9; }
	inline int32_t* get_address_of__cuta_9() { return &____cuta_9; }
	inline void set__cuta_9(int32_t value)
	{
		____cuta_9 = value;
	}

	inline static int32_t get_offset_of__qeehallher_10() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____qeehallher_10)); }
	inline float get__qeehallher_10() const { return ____qeehallher_10; }
	inline float* get_address_of__qeehallher_10() { return &____qeehallher_10; }
	inline void set__qeehallher_10(float value)
	{
		____qeehallher_10 = value;
	}

	inline static int32_t get_offset_of__nomasneDerecal_11() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____nomasneDerecal_11)); }
	inline String_t* get__nomasneDerecal_11() const { return ____nomasneDerecal_11; }
	inline String_t** get_address_of__nomasneDerecal_11() { return &____nomasneDerecal_11; }
	inline void set__nomasneDerecal_11(String_t* value)
	{
		____nomasneDerecal_11 = value;
		Il2CppCodeGenWriteBarrier(&____nomasneDerecal_11, value);
	}

	inline static int32_t get_offset_of__gairtrerebi_12() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____gairtrerebi_12)); }
	inline bool get__gairtrerebi_12() const { return ____gairtrerebi_12; }
	inline bool* get_address_of__gairtrerebi_12() { return &____gairtrerebi_12; }
	inline void set__gairtrerebi_12(bool value)
	{
		____gairtrerebi_12 = value;
	}

	inline static int32_t get_offset_of__mearceldairToudrearpear_13() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____mearceldairToudrearpear_13)); }
	inline int32_t get__mearceldairToudrearpear_13() const { return ____mearceldairToudrearpear_13; }
	inline int32_t* get_address_of__mearceldairToudrearpear_13() { return &____mearceldairToudrearpear_13; }
	inline void set__mearceldairToudrearpear_13(int32_t value)
	{
		____mearceldairToudrearpear_13 = value;
	}

	inline static int32_t get_offset_of__wacerpoQiremtri_14() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____wacerpoQiremtri_14)); }
	inline bool get__wacerpoQiremtri_14() const { return ____wacerpoQiremtri_14; }
	inline bool* get_address_of__wacerpoQiremtri_14() { return &____wacerpoQiremtri_14; }
	inline void set__wacerpoQiremtri_14(bool value)
	{
		____wacerpoQiremtri_14 = value;
	}

	inline static int32_t get_offset_of__whetriCircelja_15() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____whetriCircelja_15)); }
	inline int32_t get__whetriCircelja_15() const { return ____whetriCircelja_15; }
	inline int32_t* get_address_of__whetriCircelja_15() { return &____whetriCircelja_15; }
	inline void set__whetriCircelja_15(int32_t value)
	{
		____whetriCircelja_15 = value;
	}

	inline static int32_t get_offset_of__kaijameJiher_16() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____kaijameJiher_16)); }
	inline int32_t get__kaijameJiher_16() const { return ____kaijameJiher_16; }
	inline int32_t* get_address_of__kaijameJiher_16() { return &____kaijameJiher_16; }
	inline void set__kaijameJiher_16(int32_t value)
	{
		____kaijameJiher_16 = value;
	}

	inline static int32_t get_offset_of__sinargairHowqem_17() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____sinargairHowqem_17)); }
	inline bool get__sinargairHowqem_17() const { return ____sinargairHowqem_17; }
	inline bool* get_address_of__sinargairHowqem_17() { return &____sinargairHowqem_17; }
	inline void set__sinargairHowqem_17(bool value)
	{
		____sinargairHowqem_17 = value;
	}

	inline static int32_t get_offset_of__hiraiDrerekas_18() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____hiraiDrerekas_18)); }
	inline String_t* get__hiraiDrerekas_18() const { return ____hiraiDrerekas_18; }
	inline String_t** get_address_of__hiraiDrerekas_18() { return &____hiraiDrerekas_18; }
	inline void set__hiraiDrerekas_18(String_t* value)
	{
		____hiraiDrerekas_18 = value;
		Il2CppCodeGenWriteBarrier(&____hiraiDrerekas_18, value);
	}

	inline static int32_t get_offset_of__resinisLeawoujall_19() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____resinisLeawoujall_19)); }
	inline bool get__resinisLeawoujall_19() const { return ____resinisLeawoujall_19; }
	inline bool* get_address_of__resinisLeawoujall_19() { return &____resinisLeawoujall_19; }
	inline void set__resinisLeawoujall_19(bool value)
	{
		____resinisLeawoujall_19 = value;
	}

	inline static int32_t get_offset_of__staycalraLeejawgea_20() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____staycalraLeejawgea_20)); }
	inline bool get__staycalraLeejawgea_20() const { return ____staycalraLeejawgea_20; }
	inline bool* get_address_of__staycalraLeejawgea_20() { return &____staycalraLeejawgea_20; }
	inline void set__staycalraLeejawgea_20(bool value)
	{
		____staycalraLeejawgea_20 = value;
	}

	inline static int32_t get_offset_of__jisairfurTedi_21() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____jisairfurTedi_21)); }
	inline float get__jisairfurTedi_21() const { return ____jisairfurTedi_21; }
	inline float* get_address_of__jisairfurTedi_21() { return &____jisairfurTedi_21; }
	inline void set__jisairfurTedi_21(float value)
	{
		____jisairfurTedi_21 = value;
	}

	inline static int32_t get_offset_of__pirdaltrur_22() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____pirdaltrur_22)); }
	inline String_t* get__pirdaltrur_22() const { return ____pirdaltrur_22; }
	inline String_t** get_address_of__pirdaltrur_22() { return &____pirdaltrur_22; }
	inline void set__pirdaltrur_22(String_t* value)
	{
		____pirdaltrur_22 = value;
		Il2CppCodeGenWriteBarrier(&____pirdaltrur_22, value);
	}

	inline static int32_t get_offset_of__saltreyeKicidu_23() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____saltreyeKicidu_23)); }
	inline int32_t get__saltreyeKicidu_23() const { return ____saltreyeKicidu_23; }
	inline int32_t* get_address_of__saltreyeKicidu_23() { return &____saltreyeKicidu_23; }
	inline void set__saltreyeKicidu_23(int32_t value)
	{
		____saltreyeKicidu_23 = value;
	}

	inline static int32_t get_offset_of__dulearaNaychemyall_24() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____dulearaNaychemyall_24)); }
	inline uint32_t get__dulearaNaychemyall_24() const { return ____dulearaNaychemyall_24; }
	inline uint32_t* get_address_of__dulearaNaychemyall_24() { return &____dulearaNaychemyall_24; }
	inline void set__dulearaNaychemyall_24(uint32_t value)
	{
		____dulearaNaychemyall_24 = value;
	}

	inline static int32_t get_offset_of__masomerYeba_25() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____masomerYeba_25)); }
	inline String_t* get__masomerYeba_25() const { return ____masomerYeba_25; }
	inline String_t** get_address_of__masomerYeba_25() { return &____masomerYeba_25; }
	inline void set__masomerYeba_25(String_t* value)
	{
		____masomerYeba_25 = value;
		Il2CppCodeGenWriteBarrier(&____masomerYeba_25, value);
	}

	inline static int32_t get_offset_of__naynee_26() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____naynee_26)); }
	inline float get__naynee_26() const { return ____naynee_26; }
	inline float* get_address_of__naynee_26() { return &____naynee_26; }
	inline void set__naynee_26(float value)
	{
		____naynee_26 = value;
	}

	inline static int32_t get_offset_of__faterenerTogurbea_27() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____faterenerTogurbea_27)); }
	inline uint32_t get__faterenerTogurbea_27() const { return ____faterenerTogurbea_27; }
	inline uint32_t* get_address_of__faterenerTogurbea_27() { return &____faterenerTogurbea_27; }
	inline void set__faterenerTogurbea_27(uint32_t value)
	{
		____faterenerTogurbea_27 = value;
	}

	inline static int32_t get_offset_of__sowrupas_28() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____sowrupas_28)); }
	inline int32_t get__sowrupas_28() const { return ____sowrupas_28; }
	inline int32_t* get_address_of__sowrupas_28() { return &____sowrupas_28; }
	inline void set__sowrupas_28(int32_t value)
	{
		____sowrupas_28 = value;
	}

	inline static int32_t get_offset_of__hegawawJallrissta_29() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____hegawawJallrissta_29)); }
	inline int32_t get__hegawawJallrissta_29() const { return ____hegawawJallrissta_29; }
	inline int32_t* get_address_of__hegawawJallrissta_29() { return &____hegawawJallrissta_29; }
	inline void set__hegawawJallrissta_29(int32_t value)
	{
		____hegawawJallrissta_29 = value;
	}

	inline static int32_t get_offset_of__sallbaNalne_30() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____sallbaNalne_30)); }
	inline int32_t get__sallbaNalne_30() const { return ____sallbaNalne_30; }
	inline int32_t* get_address_of__sallbaNalne_30() { return &____sallbaNalne_30; }
	inline void set__sallbaNalne_30(int32_t value)
	{
		____sallbaNalne_30 = value;
	}

	inline static int32_t get_offset_of__roosai_31() { return static_cast<int32_t>(offsetof(M_telhur5_t1079821227, ____roosai_31)); }
	inline bool get__roosai_31() const { return ____roosai_31; }
	inline bool* get_address_of__roosai_31() { return &____roosai_31; }
	inline void set__roosai_31(bool value)
	{
		____roosai_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
