﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Node
struct Node_t1974195064;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.Node::.ctor()
extern "C"  void Node__ctor_m1930703343 (Node_t1974195064 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
