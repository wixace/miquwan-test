﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GUIAspectSizer
struct GUIAspectSizer_t442543138;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t2977405297;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.GUIAspectSizer::.ctor(System.Single,UnityEngine.GUILayoutOption[])
extern "C"  void GUIAspectSizer__ctor_m3859768355 (GUIAspectSizer_t442543138 * __this, float ___aspect0, GUILayoutOptionU5BU5D_t2977405297* ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIAspectSizer::CalcHeight()
extern "C"  void GUIAspectSizer_CalcHeight_m513225841 (GUIAspectSizer_t442543138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
