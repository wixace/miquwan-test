﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t4062675100;
// UnityEngine.Events.PersistentCall
struct PersistentCall_t2972625667;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t3597236437;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t1020378628;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallList3597236437.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase1020378628.h"

// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern "C"  void PersistentCallGroup__ctor_m3119615256 (PersistentCallGroup_t4062675100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Events.PersistentCallGroup::get_Count()
extern "C"  int32_t PersistentCallGroup_get_Count_m1197967516 (PersistentCallGroup_t4062675100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.PersistentCall UnityEngine.Events.PersistentCallGroup::GetListener(System.Int32)
extern "C"  PersistentCall_t2972625667 * PersistentCallGroup_GetListener_m476891473 (PersistentCallGroup_t4062675100 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern "C"  void PersistentCallGroup_Initialize_m3484159567 (PersistentCallGroup_t4062675100 * __this, InvokableCallList_t3597236437 * ___invokableList0, UnityEventBase_t1020378628 * ___unityEventBase1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
