﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_salfadrurKouzepir84
struct  M_salfadrurKouzepir84_t3781229053  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_salfadrurKouzepir84::_desay
	int32_t ____desay_0;
	// System.UInt32 GarbageiOS.M_salfadrurKouzepir84::_rairay
	uint32_t ____rairay_1;
	// System.Int32 GarbageiOS.M_salfadrurKouzepir84::_gasersouSamas
	int32_t ____gasersouSamas_2;
	// System.UInt32 GarbageiOS.M_salfadrurKouzepir84::_dalnajaiFawbairhe
	uint32_t ____dalnajaiFawbairhe_3;
	// System.Single GarbageiOS.M_salfadrurKouzepir84::_nahoryere
	float ____nahoryere_4;
	// System.Boolean GarbageiOS.M_salfadrurKouzepir84::_couleelir
	bool ____couleelir_5;
	// System.Single GarbageiOS.M_salfadrurKouzepir84::_bajearnouSawdeaki
	float ____bajearnouSawdeaki_6;
	// System.UInt32 GarbageiOS.M_salfadrurKouzepir84::_soumo
	uint32_t ____soumo_7;
	// System.Int32 GarbageiOS.M_salfadrurKouzepir84::_stoudiswallRecujo
	int32_t ____stoudiswallRecujo_8;
	// System.String GarbageiOS.M_salfadrurKouzepir84::_jaifaStopow
	String_t* ____jaifaStopow_9;
	// System.Boolean GarbageiOS.M_salfadrurKouzepir84::_safalldreKeregi
	bool ____safalldreKeregi_10;
	// System.Single GarbageiOS.M_salfadrurKouzepir84::_bichuqar
	float ____bichuqar_11;
	// System.String GarbageiOS.M_salfadrurKouzepir84::_caneasou
	String_t* ____caneasou_12;
	// System.String GarbageiOS.M_salfadrurKouzepir84::_rosarfee
	String_t* ____rosarfee_13;
	// System.Boolean GarbageiOS.M_salfadrurKouzepir84::_suniciKoogalllai
	bool ____suniciKoogalllai_14;
	// System.UInt32 GarbageiOS.M_salfadrurKouzepir84::_drallker
	uint32_t ____drallker_15;
	// System.Single GarbageiOS.M_salfadrurKouzepir84::_meaceamearYalsowcall
	float ____meaceamearYalsowcall_16;
	// System.UInt32 GarbageiOS.M_salfadrurKouzepir84::_jertasbal
	uint32_t ____jertasbal_17;
	// System.Boolean GarbageiOS.M_salfadrurKouzepir84::_walmiDraircari
	bool ____walmiDraircari_18;
	// System.UInt32 GarbageiOS.M_salfadrurKouzepir84::_cilelcerPalruchow
	uint32_t ____cilelcerPalruchow_19;
	// System.Boolean GarbageiOS.M_salfadrurKouzepir84::_siscortePirra
	bool ____siscortePirra_20;
	// System.Int32 GarbageiOS.M_salfadrurKouzepir84::_dabanay
	int32_t ____dabanay_21;
	// System.Boolean GarbageiOS.M_salfadrurKouzepir84::_soucemtrearMowowcu
	bool ____soucemtrearMowowcu_22;
	// System.Single GarbageiOS.M_salfadrurKouzepir84::_peremi
	float ____peremi_23;
	// System.Boolean GarbageiOS.M_salfadrurKouzepir84::_talderliNebere
	bool ____talderliNebere_24;

public:
	inline static int32_t get_offset_of__desay_0() { return static_cast<int32_t>(offsetof(M_salfadrurKouzepir84_t3781229053, ____desay_0)); }
	inline int32_t get__desay_0() const { return ____desay_0; }
	inline int32_t* get_address_of__desay_0() { return &____desay_0; }
	inline void set__desay_0(int32_t value)
	{
		____desay_0 = value;
	}

	inline static int32_t get_offset_of__rairay_1() { return static_cast<int32_t>(offsetof(M_salfadrurKouzepir84_t3781229053, ____rairay_1)); }
	inline uint32_t get__rairay_1() const { return ____rairay_1; }
	inline uint32_t* get_address_of__rairay_1() { return &____rairay_1; }
	inline void set__rairay_1(uint32_t value)
	{
		____rairay_1 = value;
	}

	inline static int32_t get_offset_of__gasersouSamas_2() { return static_cast<int32_t>(offsetof(M_salfadrurKouzepir84_t3781229053, ____gasersouSamas_2)); }
	inline int32_t get__gasersouSamas_2() const { return ____gasersouSamas_2; }
	inline int32_t* get_address_of__gasersouSamas_2() { return &____gasersouSamas_2; }
	inline void set__gasersouSamas_2(int32_t value)
	{
		____gasersouSamas_2 = value;
	}

	inline static int32_t get_offset_of__dalnajaiFawbairhe_3() { return static_cast<int32_t>(offsetof(M_salfadrurKouzepir84_t3781229053, ____dalnajaiFawbairhe_3)); }
	inline uint32_t get__dalnajaiFawbairhe_3() const { return ____dalnajaiFawbairhe_3; }
	inline uint32_t* get_address_of__dalnajaiFawbairhe_3() { return &____dalnajaiFawbairhe_3; }
	inline void set__dalnajaiFawbairhe_3(uint32_t value)
	{
		____dalnajaiFawbairhe_3 = value;
	}

	inline static int32_t get_offset_of__nahoryere_4() { return static_cast<int32_t>(offsetof(M_salfadrurKouzepir84_t3781229053, ____nahoryere_4)); }
	inline float get__nahoryere_4() const { return ____nahoryere_4; }
	inline float* get_address_of__nahoryere_4() { return &____nahoryere_4; }
	inline void set__nahoryere_4(float value)
	{
		____nahoryere_4 = value;
	}

	inline static int32_t get_offset_of__couleelir_5() { return static_cast<int32_t>(offsetof(M_salfadrurKouzepir84_t3781229053, ____couleelir_5)); }
	inline bool get__couleelir_5() const { return ____couleelir_5; }
	inline bool* get_address_of__couleelir_5() { return &____couleelir_5; }
	inline void set__couleelir_5(bool value)
	{
		____couleelir_5 = value;
	}

	inline static int32_t get_offset_of__bajearnouSawdeaki_6() { return static_cast<int32_t>(offsetof(M_salfadrurKouzepir84_t3781229053, ____bajearnouSawdeaki_6)); }
	inline float get__bajearnouSawdeaki_6() const { return ____bajearnouSawdeaki_6; }
	inline float* get_address_of__bajearnouSawdeaki_6() { return &____bajearnouSawdeaki_6; }
	inline void set__bajearnouSawdeaki_6(float value)
	{
		____bajearnouSawdeaki_6 = value;
	}

	inline static int32_t get_offset_of__soumo_7() { return static_cast<int32_t>(offsetof(M_salfadrurKouzepir84_t3781229053, ____soumo_7)); }
	inline uint32_t get__soumo_7() const { return ____soumo_7; }
	inline uint32_t* get_address_of__soumo_7() { return &____soumo_7; }
	inline void set__soumo_7(uint32_t value)
	{
		____soumo_7 = value;
	}

	inline static int32_t get_offset_of__stoudiswallRecujo_8() { return static_cast<int32_t>(offsetof(M_salfadrurKouzepir84_t3781229053, ____stoudiswallRecujo_8)); }
	inline int32_t get__stoudiswallRecujo_8() const { return ____stoudiswallRecujo_8; }
	inline int32_t* get_address_of__stoudiswallRecujo_8() { return &____stoudiswallRecujo_8; }
	inline void set__stoudiswallRecujo_8(int32_t value)
	{
		____stoudiswallRecujo_8 = value;
	}

	inline static int32_t get_offset_of__jaifaStopow_9() { return static_cast<int32_t>(offsetof(M_salfadrurKouzepir84_t3781229053, ____jaifaStopow_9)); }
	inline String_t* get__jaifaStopow_9() const { return ____jaifaStopow_9; }
	inline String_t** get_address_of__jaifaStopow_9() { return &____jaifaStopow_9; }
	inline void set__jaifaStopow_9(String_t* value)
	{
		____jaifaStopow_9 = value;
		Il2CppCodeGenWriteBarrier(&____jaifaStopow_9, value);
	}

	inline static int32_t get_offset_of__safalldreKeregi_10() { return static_cast<int32_t>(offsetof(M_salfadrurKouzepir84_t3781229053, ____safalldreKeregi_10)); }
	inline bool get__safalldreKeregi_10() const { return ____safalldreKeregi_10; }
	inline bool* get_address_of__safalldreKeregi_10() { return &____safalldreKeregi_10; }
	inline void set__safalldreKeregi_10(bool value)
	{
		____safalldreKeregi_10 = value;
	}

	inline static int32_t get_offset_of__bichuqar_11() { return static_cast<int32_t>(offsetof(M_salfadrurKouzepir84_t3781229053, ____bichuqar_11)); }
	inline float get__bichuqar_11() const { return ____bichuqar_11; }
	inline float* get_address_of__bichuqar_11() { return &____bichuqar_11; }
	inline void set__bichuqar_11(float value)
	{
		____bichuqar_11 = value;
	}

	inline static int32_t get_offset_of__caneasou_12() { return static_cast<int32_t>(offsetof(M_salfadrurKouzepir84_t3781229053, ____caneasou_12)); }
	inline String_t* get__caneasou_12() const { return ____caneasou_12; }
	inline String_t** get_address_of__caneasou_12() { return &____caneasou_12; }
	inline void set__caneasou_12(String_t* value)
	{
		____caneasou_12 = value;
		Il2CppCodeGenWriteBarrier(&____caneasou_12, value);
	}

	inline static int32_t get_offset_of__rosarfee_13() { return static_cast<int32_t>(offsetof(M_salfadrurKouzepir84_t3781229053, ____rosarfee_13)); }
	inline String_t* get__rosarfee_13() const { return ____rosarfee_13; }
	inline String_t** get_address_of__rosarfee_13() { return &____rosarfee_13; }
	inline void set__rosarfee_13(String_t* value)
	{
		____rosarfee_13 = value;
		Il2CppCodeGenWriteBarrier(&____rosarfee_13, value);
	}

	inline static int32_t get_offset_of__suniciKoogalllai_14() { return static_cast<int32_t>(offsetof(M_salfadrurKouzepir84_t3781229053, ____suniciKoogalllai_14)); }
	inline bool get__suniciKoogalllai_14() const { return ____suniciKoogalllai_14; }
	inline bool* get_address_of__suniciKoogalllai_14() { return &____suniciKoogalllai_14; }
	inline void set__suniciKoogalllai_14(bool value)
	{
		____suniciKoogalllai_14 = value;
	}

	inline static int32_t get_offset_of__drallker_15() { return static_cast<int32_t>(offsetof(M_salfadrurKouzepir84_t3781229053, ____drallker_15)); }
	inline uint32_t get__drallker_15() const { return ____drallker_15; }
	inline uint32_t* get_address_of__drallker_15() { return &____drallker_15; }
	inline void set__drallker_15(uint32_t value)
	{
		____drallker_15 = value;
	}

	inline static int32_t get_offset_of__meaceamearYalsowcall_16() { return static_cast<int32_t>(offsetof(M_salfadrurKouzepir84_t3781229053, ____meaceamearYalsowcall_16)); }
	inline float get__meaceamearYalsowcall_16() const { return ____meaceamearYalsowcall_16; }
	inline float* get_address_of__meaceamearYalsowcall_16() { return &____meaceamearYalsowcall_16; }
	inline void set__meaceamearYalsowcall_16(float value)
	{
		____meaceamearYalsowcall_16 = value;
	}

	inline static int32_t get_offset_of__jertasbal_17() { return static_cast<int32_t>(offsetof(M_salfadrurKouzepir84_t3781229053, ____jertasbal_17)); }
	inline uint32_t get__jertasbal_17() const { return ____jertasbal_17; }
	inline uint32_t* get_address_of__jertasbal_17() { return &____jertasbal_17; }
	inline void set__jertasbal_17(uint32_t value)
	{
		____jertasbal_17 = value;
	}

	inline static int32_t get_offset_of__walmiDraircari_18() { return static_cast<int32_t>(offsetof(M_salfadrurKouzepir84_t3781229053, ____walmiDraircari_18)); }
	inline bool get__walmiDraircari_18() const { return ____walmiDraircari_18; }
	inline bool* get_address_of__walmiDraircari_18() { return &____walmiDraircari_18; }
	inline void set__walmiDraircari_18(bool value)
	{
		____walmiDraircari_18 = value;
	}

	inline static int32_t get_offset_of__cilelcerPalruchow_19() { return static_cast<int32_t>(offsetof(M_salfadrurKouzepir84_t3781229053, ____cilelcerPalruchow_19)); }
	inline uint32_t get__cilelcerPalruchow_19() const { return ____cilelcerPalruchow_19; }
	inline uint32_t* get_address_of__cilelcerPalruchow_19() { return &____cilelcerPalruchow_19; }
	inline void set__cilelcerPalruchow_19(uint32_t value)
	{
		____cilelcerPalruchow_19 = value;
	}

	inline static int32_t get_offset_of__siscortePirra_20() { return static_cast<int32_t>(offsetof(M_salfadrurKouzepir84_t3781229053, ____siscortePirra_20)); }
	inline bool get__siscortePirra_20() const { return ____siscortePirra_20; }
	inline bool* get_address_of__siscortePirra_20() { return &____siscortePirra_20; }
	inline void set__siscortePirra_20(bool value)
	{
		____siscortePirra_20 = value;
	}

	inline static int32_t get_offset_of__dabanay_21() { return static_cast<int32_t>(offsetof(M_salfadrurKouzepir84_t3781229053, ____dabanay_21)); }
	inline int32_t get__dabanay_21() const { return ____dabanay_21; }
	inline int32_t* get_address_of__dabanay_21() { return &____dabanay_21; }
	inline void set__dabanay_21(int32_t value)
	{
		____dabanay_21 = value;
	}

	inline static int32_t get_offset_of__soucemtrearMowowcu_22() { return static_cast<int32_t>(offsetof(M_salfadrurKouzepir84_t3781229053, ____soucemtrearMowowcu_22)); }
	inline bool get__soucemtrearMowowcu_22() const { return ____soucemtrearMowowcu_22; }
	inline bool* get_address_of__soucemtrearMowowcu_22() { return &____soucemtrearMowowcu_22; }
	inline void set__soucemtrearMowowcu_22(bool value)
	{
		____soucemtrearMowowcu_22 = value;
	}

	inline static int32_t get_offset_of__peremi_23() { return static_cast<int32_t>(offsetof(M_salfadrurKouzepir84_t3781229053, ____peremi_23)); }
	inline float get__peremi_23() const { return ____peremi_23; }
	inline float* get_address_of__peremi_23() { return &____peremi_23; }
	inline void set__peremi_23(float value)
	{
		____peremi_23 = value;
	}

	inline static int32_t get_offset_of__talderliNebere_24() { return static_cast<int32_t>(offsetof(M_salfadrurKouzepir84_t3781229053, ____talderliNebere_24)); }
	inline bool get__talderliNebere_24() const { return ____talderliNebere_24; }
	inline bool* get_address_of__talderliNebere_24() { return &____talderliNebere_24; }
	inline void set__talderliNebere_24(bool value)
	{
		____talderliNebere_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
