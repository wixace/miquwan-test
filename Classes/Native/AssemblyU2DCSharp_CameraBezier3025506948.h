﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1365137228;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraBezier
struct  CameraBezier_t3025506948  : public MonoBehaviour_t667441552
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> CameraBezier::points
	List_1_t1355284822 * ___points_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> CameraBezier::rotations
	List_1_t1355284822 * ___rotations_4;
	// System.Collections.Generic.List`1<System.Single> CameraBezier::rotSpeed
	List_1_t1365137228 * ___rotSpeed_5;
	// System.Collections.Generic.List`1<System.Int32> CameraBezier::isClockWise
	List_1_t2522024052 * ___isClockWise_6;
	// System.Collections.Generic.List`1<System.Single> CameraBezier::speed
	List_1_t1365137228 * ___speed_7;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> CameraBezier::focus
	List_1_t747900261 * ___focus_8;
	// System.Collections.Generic.List`1<System.Int32> CameraBezier::isHero
	List_1_t2522024052 * ___isHero_9;
	// System.Collections.Generic.List`1<System.Single> CameraBezier::smooth
	List_1_t1365137228 * ___smooth_10;
	// System.Collections.Generic.List`1<System.Single> CameraBezier::view
	List_1_t1365137228 * ___view_11;
	// System.Collections.Generic.List`1<System.Single> CameraBezier::bright
	List_1_t1365137228 * ___bright_12;
	// System.Collections.Generic.List`1<System.Single> CameraBezier::orthographic
	List_1_t1365137228 * ___orthographic_13;
	// System.Collections.Generic.List`1<System.Int32> CameraBezier::isConstant
	List_1_t2522024052 * ___isConstant_14;
	// System.Int32 CameraBezier::cameraShotDoneId
	int32_t ___cameraShotDoneId_15;
	// UnityEngine.Vector3 CameraBezier::rot_Temp
	Vector3_t4282066566  ___rot_Temp_16;
	// System.Boolean CameraBezier::isStopCameraShot
	bool ___isStopCameraShot_17;
	// System.Boolean CameraBezier::isLerp
	bool ___isLerp_18;
	// System.Single CameraBezier::overageSpeed
	float ___overageSpeed_19;
	// System.Single CameraBezier::lerpInterval
	float ___lerpInterval_20;
	// System.Single CameraBezier::currentMoveSpeed
	float ___currentMoveSpeed_21;
	// UnityEngine.Vector3 CameraBezier::currentEulerAngleSpeed
	Vector3_t4282066566  ___currentEulerAngleSpeed_22;
	// System.Boolean CameraBezier::isCurrentIntervalDown
	bool ___isCurrentIntervalDown_23;
	// System.Boolean CameraBezier::isAllDown
	bool ___isAllDown_24;
	// System.Single CameraBezier::defaultSpeed
	float ___defaultSpeed_25;
	// System.Single CameraBezier::defaultfocus
	float ___defaultfocus_26;
	// System.Single CameraBezier::defaultIsHero
	float ___defaultIsHero_27;
	// System.Single CameraBezier::defaultSmooth
	float ___defaultSmooth_28;
	// System.Single CameraBezier::defaultView
	float ___defaultView_29;
	// System.Single CameraBezier::defaultBright
	float ___defaultBright_30;
	// System.Single CameraBezier::defaultOrthographic
	float ___defaultOrthographic_31;
	// System.Int32 CameraBezier::pt
	int32_t ___pt_32;
	// System.Single CameraBezier::time
	float ___time_33;
	// System.Int32 CameraBezier::timeInt
	int32_t ___timeInt_34;
	// System.Single CameraBezier::newTime
	float ___newTime_35;
	// System.Single CameraBezier::t
	float ___t_36;
	// System.Boolean CameraBezier::isRoted
	bool ___isRoted_37;
	// System.Single CameraBezier::time2
	float ___time2_38;
	// System.Int32 CameraBezier::time_I
	int32_t ___time_I_39;
	// System.Single CameraBezier::time_F
	float ___time_F_40;

public:
	inline static int32_t get_offset_of_points_3() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___points_3)); }
	inline List_1_t1355284822 * get_points_3() const { return ___points_3; }
	inline List_1_t1355284822 ** get_address_of_points_3() { return &___points_3; }
	inline void set_points_3(List_1_t1355284822 * value)
	{
		___points_3 = value;
		Il2CppCodeGenWriteBarrier(&___points_3, value);
	}

	inline static int32_t get_offset_of_rotations_4() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___rotations_4)); }
	inline List_1_t1355284822 * get_rotations_4() const { return ___rotations_4; }
	inline List_1_t1355284822 ** get_address_of_rotations_4() { return &___rotations_4; }
	inline void set_rotations_4(List_1_t1355284822 * value)
	{
		___rotations_4 = value;
		Il2CppCodeGenWriteBarrier(&___rotations_4, value);
	}

	inline static int32_t get_offset_of_rotSpeed_5() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___rotSpeed_5)); }
	inline List_1_t1365137228 * get_rotSpeed_5() const { return ___rotSpeed_5; }
	inline List_1_t1365137228 ** get_address_of_rotSpeed_5() { return &___rotSpeed_5; }
	inline void set_rotSpeed_5(List_1_t1365137228 * value)
	{
		___rotSpeed_5 = value;
		Il2CppCodeGenWriteBarrier(&___rotSpeed_5, value);
	}

	inline static int32_t get_offset_of_isClockWise_6() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___isClockWise_6)); }
	inline List_1_t2522024052 * get_isClockWise_6() const { return ___isClockWise_6; }
	inline List_1_t2522024052 ** get_address_of_isClockWise_6() { return &___isClockWise_6; }
	inline void set_isClockWise_6(List_1_t2522024052 * value)
	{
		___isClockWise_6 = value;
		Il2CppCodeGenWriteBarrier(&___isClockWise_6, value);
	}

	inline static int32_t get_offset_of_speed_7() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___speed_7)); }
	inline List_1_t1365137228 * get_speed_7() const { return ___speed_7; }
	inline List_1_t1365137228 ** get_address_of_speed_7() { return &___speed_7; }
	inline void set_speed_7(List_1_t1365137228 * value)
	{
		___speed_7 = value;
		Il2CppCodeGenWriteBarrier(&___speed_7, value);
	}

	inline static int32_t get_offset_of_focus_8() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___focus_8)); }
	inline List_1_t747900261 * get_focus_8() const { return ___focus_8; }
	inline List_1_t747900261 ** get_address_of_focus_8() { return &___focus_8; }
	inline void set_focus_8(List_1_t747900261 * value)
	{
		___focus_8 = value;
		Il2CppCodeGenWriteBarrier(&___focus_8, value);
	}

	inline static int32_t get_offset_of_isHero_9() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___isHero_9)); }
	inline List_1_t2522024052 * get_isHero_9() const { return ___isHero_9; }
	inline List_1_t2522024052 ** get_address_of_isHero_9() { return &___isHero_9; }
	inline void set_isHero_9(List_1_t2522024052 * value)
	{
		___isHero_9 = value;
		Il2CppCodeGenWriteBarrier(&___isHero_9, value);
	}

	inline static int32_t get_offset_of_smooth_10() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___smooth_10)); }
	inline List_1_t1365137228 * get_smooth_10() const { return ___smooth_10; }
	inline List_1_t1365137228 ** get_address_of_smooth_10() { return &___smooth_10; }
	inline void set_smooth_10(List_1_t1365137228 * value)
	{
		___smooth_10 = value;
		Il2CppCodeGenWriteBarrier(&___smooth_10, value);
	}

	inline static int32_t get_offset_of_view_11() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___view_11)); }
	inline List_1_t1365137228 * get_view_11() const { return ___view_11; }
	inline List_1_t1365137228 ** get_address_of_view_11() { return &___view_11; }
	inline void set_view_11(List_1_t1365137228 * value)
	{
		___view_11 = value;
		Il2CppCodeGenWriteBarrier(&___view_11, value);
	}

	inline static int32_t get_offset_of_bright_12() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___bright_12)); }
	inline List_1_t1365137228 * get_bright_12() const { return ___bright_12; }
	inline List_1_t1365137228 ** get_address_of_bright_12() { return &___bright_12; }
	inline void set_bright_12(List_1_t1365137228 * value)
	{
		___bright_12 = value;
		Il2CppCodeGenWriteBarrier(&___bright_12, value);
	}

	inline static int32_t get_offset_of_orthographic_13() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___orthographic_13)); }
	inline List_1_t1365137228 * get_orthographic_13() const { return ___orthographic_13; }
	inline List_1_t1365137228 ** get_address_of_orthographic_13() { return &___orthographic_13; }
	inline void set_orthographic_13(List_1_t1365137228 * value)
	{
		___orthographic_13 = value;
		Il2CppCodeGenWriteBarrier(&___orthographic_13, value);
	}

	inline static int32_t get_offset_of_isConstant_14() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___isConstant_14)); }
	inline List_1_t2522024052 * get_isConstant_14() const { return ___isConstant_14; }
	inline List_1_t2522024052 ** get_address_of_isConstant_14() { return &___isConstant_14; }
	inline void set_isConstant_14(List_1_t2522024052 * value)
	{
		___isConstant_14 = value;
		Il2CppCodeGenWriteBarrier(&___isConstant_14, value);
	}

	inline static int32_t get_offset_of_cameraShotDoneId_15() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___cameraShotDoneId_15)); }
	inline int32_t get_cameraShotDoneId_15() const { return ___cameraShotDoneId_15; }
	inline int32_t* get_address_of_cameraShotDoneId_15() { return &___cameraShotDoneId_15; }
	inline void set_cameraShotDoneId_15(int32_t value)
	{
		___cameraShotDoneId_15 = value;
	}

	inline static int32_t get_offset_of_rot_Temp_16() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___rot_Temp_16)); }
	inline Vector3_t4282066566  get_rot_Temp_16() const { return ___rot_Temp_16; }
	inline Vector3_t4282066566 * get_address_of_rot_Temp_16() { return &___rot_Temp_16; }
	inline void set_rot_Temp_16(Vector3_t4282066566  value)
	{
		___rot_Temp_16 = value;
	}

	inline static int32_t get_offset_of_isStopCameraShot_17() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___isStopCameraShot_17)); }
	inline bool get_isStopCameraShot_17() const { return ___isStopCameraShot_17; }
	inline bool* get_address_of_isStopCameraShot_17() { return &___isStopCameraShot_17; }
	inline void set_isStopCameraShot_17(bool value)
	{
		___isStopCameraShot_17 = value;
	}

	inline static int32_t get_offset_of_isLerp_18() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___isLerp_18)); }
	inline bool get_isLerp_18() const { return ___isLerp_18; }
	inline bool* get_address_of_isLerp_18() { return &___isLerp_18; }
	inline void set_isLerp_18(bool value)
	{
		___isLerp_18 = value;
	}

	inline static int32_t get_offset_of_overageSpeed_19() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___overageSpeed_19)); }
	inline float get_overageSpeed_19() const { return ___overageSpeed_19; }
	inline float* get_address_of_overageSpeed_19() { return &___overageSpeed_19; }
	inline void set_overageSpeed_19(float value)
	{
		___overageSpeed_19 = value;
	}

	inline static int32_t get_offset_of_lerpInterval_20() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___lerpInterval_20)); }
	inline float get_lerpInterval_20() const { return ___lerpInterval_20; }
	inline float* get_address_of_lerpInterval_20() { return &___lerpInterval_20; }
	inline void set_lerpInterval_20(float value)
	{
		___lerpInterval_20 = value;
	}

	inline static int32_t get_offset_of_currentMoveSpeed_21() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___currentMoveSpeed_21)); }
	inline float get_currentMoveSpeed_21() const { return ___currentMoveSpeed_21; }
	inline float* get_address_of_currentMoveSpeed_21() { return &___currentMoveSpeed_21; }
	inline void set_currentMoveSpeed_21(float value)
	{
		___currentMoveSpeed_21 = value;
	}

	inline static int32_t get_offset_of_currentEulerAngleSpeed_22() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___currentEulerAngleSpeed_22)); }
	inline Vector3_t4282066566  get_currentEulerAngleSpeed_22() const { return ___currentEulerAngleSpeed_22; }
	inline Vector3_t4282066566 * get_address_of_currentEulerAngleSpeed_22() { return &___currentEulerAngleSpeed_22; }
	inline void set_currentEulerAngleSpeed_22(Vector3_t4282066566  value)
	{
		___currentEulerAngleSpeed_22 = value;
	}

	inline static int32_t get_offset_of_isCurrentIntervalDown_23() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___isCurrentIntervalDown_23)); }
	inline bool get_isCurrentIntervalDown_23() const { return ___isCurrentIntervalDown_23; }
	inline bool* get_address_of_isCurrentIntervalDown_23() { return &___isCurrentIntervalDown_23; }
	inline void set_isCurrentIntervalDown_23(bool value)
	{
		___isCurrentIntervalDown_23 = value;
	}

	inline static int32_t get_offset_of_isAllDown_24() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___isAllDown_24)); }
	inline bool get_isAllDown_24() const { return ___isAllDown_24; }
	inline bool* get_address_of_isAllDown_24() { return &___isAllDown_24; }
	inline void set_isAllDown_24(bool value)
	{
		___isAllDown_24 = value;
	}

	inline static int32_t get_offset_of_defaultSpeed_25() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___defaultSpeed_25)); }
	inline float get_defaultSpeed_25() const { return ___defaultSpeed_25; }
	inline float* get_address_of_defaultSpeed_25() { return &___defaultSpeed_25; }
	inline void set_defaultSpeed_25(float value)
	{
		___defaultSpeed_25 = value;
	}

	inline static int32_t get_offset_of_defaultfocus_26() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___defaultfocus_26)); }
	inline float get_defaultfocus_26() const { return ___defaultfocus_26; }
	inline float* get_address_of_defaultfocus_26() { return &___defaultfocus_26; }
	inline void set_defaultfocus_26(float value)
	{
		___defaultfocus_26 = value;
	}

	inline static int32_t get_offset_of_defaultIsHero_27() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___defaultIsHero_27)); }
	inline float get_defaultIsHero_27() const { return ___defaultIsHero_27; }
	inline float* get_address_of_defaultIsHero_27() { return &___defaultIsHero_27; }
	inline void set_defaultIsHero_27(float value)
	{
		___defaultIsHero_27 = value;
	}

	inline static int32_t get_offset_of_defaultSmooth_28() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___defaultSmooth_28)); }
	inline float get_defaultSmooth_28() const { return ___defaultSmooth_28; }
	inline float* get_address_of_defaultSmooth_28() { return &___defaultSmooth_28; }
	inline void set_defaultSmooth_28(float value)
	{
		___defaultSmooth_28 = value;
	}

	inline static int32_t get_offset_of_defaultView_29() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___defaultView_29)); }
	inline float get_defaultView_29() const { return ___defaultView_29; }
	inline float* get_address_of_defaultView_29() { return &___defaultView_29; }
	inline void set_defaultView_29(float value)
	{
		___defaultView_29 = value;
	}

	inline static int32_t get_offset_of_defaultBright_30() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___defaultBright_30)); }
	inline float get_defaultBright_30() const { return ___defaultBright_30; }
	inline float* get_address_of_defaultBright_30() { return &___defaultBright_30; }
	inline void set_defaultBright_30(float value)
	{
		___defaultBright_30 = value;
	}

	inline static int32_t get_offset_of_defaultOrthographic_31() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___defaultOrthographic_31)); }
	inline float get_defaultOrthographic_31() const { return ___defaultOrthographic_31; }
	inline float* get_address_of_defaultOrthographic_31() { return &___defaultOrthographic_31; }
	inline void set_defaultOrthographic_31(float value)
	{
		___defaultOrthographic_31 = value;
	}

	inline static int32_t get_offset_of_pt_32() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___pt_32)); }
	inline int32_t get_pt_32() const { return ___pt_32; }
	inline int32_t* get_address_of_pt_32() { return &___pt_32; }
	inline void set_pt_32(int32_t value)
	{
		___pt_32 = value;
	}

	inline static int32_t get_offset_of_time_33() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___time_33)); }
	inline float get_time_33() const { return ___time_33; }
	inline float* get_address_of_time_33() { return &___time_33; }
	inline void set_time_33(float value)
	{
		___time_33 = value;
	}

	inline static int32_t get_offset_of_timeInt_34() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___timeInt_34)); }
	inline int32_t get_timeInt_34() const { return ___timeInt_34; }
	inline int32_t* get_address_of_timeInt_34() { return &___timeInt_34; }
	inline void set_timeInt_34(int32_t value)
	{
		___timeInt_34 = value;
	}

	inline static int32_t get_offset_of_newTime_35() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___newTime_35)); }
	inline float get_newTime_35() const { return ___newTime_35; }
	inline float* get_address_of_newTime_35() { return &___newTime_35; }
	inline void set_newTime_35(float value)
	{
		___newTime_35 = value;
	}

	inline static int32_t get_offset_of_t_36() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___t_36)); }
	inline float get_t_36() const { return ___t_36; }
	inline float* get_address_of_t_36() { return &___t_36; }
	inline void set_t_36(float value)
	{
		___t_36 = value;
	}

	inline static int32_t get_offset_of_isRoted_37() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___isRoted_37)); }
	inline bool get_isRoted_37() const { return ___isRoted_37; }
	inline bool* get_address_of_isRoted_37() { return &___isRoted_37; }
	inline void set_isRoted_37(bool value)
	{
		___isRoted_37 = value;
	}

	inline static int32_t get_offset_of_time2_38() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___time2_38)); }
	inline float get_time2_38() const { return ___time2_38; }
	inline float* get_address_of_time2_38() { return &___time2_38; }
	inline void set_time2_38(float value)
	{
		___time2_38 = value;
	}

	inline static int32_t get_offset_of_time_I_39() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___time_I_39)); }
	inline int32_t get_time_I_39() const { return ___time_I_39; }
	inline int32_t* get_address_of_time_I_39() { return &___time_I_39; }
	inline void set_time_I_39(int32_t value)
	{
		___time_I_39 = value;
	}

	inline static int32_t get_offset_of_time_F_40() { return static_cast<int32_t>(offsetof(CameraBezier_t3025506948, ___time_F_40)); }
	inline float get_time_F_40() const { return ___time_F_40; }
	inline float* get_address_of_time_F_40() { return &___time_F_40; }
	inline void set_time_F_40(float value)
	{
		___time_F_40 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
