﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_coojerwelPorawxir243
struct  M_coojerwelPorawxir243_t2946437183  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_coojerwelPorawxir243::_qurearfaNerefe
	String_t* ____qurearfaNerefe_0;
	// System.String GarbageiOS.M_coojerwelPorawxir243::_trajeLalyalai
	String_t* ____trajeLalyalai_1;
	// System.Int32 GarbageiOS.M_coojerwelPorawxir243::_hawfis
	int32_t ____hawfis_2;
	// System.Int32 GarbageiOS.M_coojerwelPorawxir243::_mawxallNesouhis
	int32_t ____mawxallNesouhis_3;
	// System.UInt32 GarbageiOS.M_coojerwelPorawxir243::_callmis
	uint32_t ____callmis_4;
	// System.UInt32 GarbageiOS.M_coojerwelPorawxir243::_leremou
	uint32_t ____leremou_5;
	// System.Boolean GarbageiOS.M_coojerwelPorawxir243::_tuseceeTroubar
	bool ____tuseceeTroubar_6;
	// System.Int32 GarbageiOS.M_coojerwelPorawxir243::_couvowSisdral
	int32_t ____couvowSisdral_7;
	// System.Single GarbageiOS.M_coojerwelPorawxir243::_dineelouPearlere
	float ____dineelouPearlere_8;
	// System.String GarbageiOS.M_coojerwelPorawxir243::_caldiTirto
	String_t* ____caldiTirto_9;
	// System.Int32 GarbageiOS.M_coojerwelPorawxir243::_tuhihoo
	int32_t ____tuhihoo_10;
	// System.Boolean GarbageiOS.M_coojerwelPorawxir243::_bapalca
	bool ____bapalca_11;
	// System.String GarbageiOS.M_coojerwelPorawxir243::_roobe
	String_t* ____roobe_12;
	// System.UInt32 GarbageiOS.M_coojerwelPorawxir243::_bere
	uint32_t ____bere_13;
	// System.UInt32 GarbageiOS.M_coojerwelPorawxir243::_towkicirKejassis
	uint32_t ____towkicirKejassis_14;
	// System.Boolean GarbageiOS.M_coojerwelPorawxir243::_caysarruWhaslem
	bool ____caysarruWhaslem_15;
	// System.String GarbageiOS.M_coojerwelPorawxir243::_meeciwhel
	String_t* ____meeciwhel_16;
	// System.Int32 GarbageiOS.M_coojerwelPorawxir243::_pukouma
	int32_t ____pukouma_17;
	// System.Int32 GarbageiOS.M_coojerwelPorawxir243::_jorpasJalhemna
	int32_t ____jorpasJalhemna_18;
	// System.Boolean GarbageiOS.M_coojerwelPorawxir243::_booyouLartoopem
	bool ____booyouLartoopem_19;
	// System.Int32 GarbageiOS.M_coojerwelPorawxir243::_gaisare
	int32_t ____gaisare_20;
	// System.String GarbageiOS.M_coojerwelPorawxir243::_trawuCayce
	String_t* ____trawuCayce_21;
	// System.String GarbageiOS.M_coojerwelPorawxir243::_towbealere
	String_t* ____towbealere_22;
	// System.String GarbageiOS.M_coojerwelPorawxir243::_sidease
	String_t* ____sidease_23;
	// System.UInt32 GarbageiOS.M_coojerwelPorawxir243::_valsasta
	uint32_t ____valsasta_24;
	// System.Int32 GarbageiOS.M_coojerwelPorawxir243::_chemarearXisnasfi
	int32_t ____chemarearXisnasfi_25;
	// System.UInt32 GarbageiOS.M_coojerwelPorawxir243::_watrinelKepemhere
	uint32_t ____watrinelKepemhere_26;
	// System.UInt32 GarbageiOS.M_coojerwelPorawxir243::_kasea
	uint32_t ____kasea_27;
	// System.String GarbageiOS.M_coojerwelPorawxir243::_mijaiguBesa
	String_t* ____mijaiguBesa_28;
	// System.UInt32 GarbageiOS.M_coojerwelPorawxir243::_caircear
	uint32_t ____caircear_29;

public:
	inline static int32_t get_offset_of__qurearfaNerefe_0() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____qurearfaNerefe_0)); }
	inline String_t* get__qurearfaNerefe_0() const { return ____qurearfaNerefe_0; }
	inline String_t** get_address_of__qurearfaNerefe_0() { return &____qurearfaNerefe_0; }
	inline void set__qurearfaNerefe_0(String_t* value)
	{
		____qurearfaNerefe_0 = value;
		Il2CppCodeGenWriteBarrier(&____qurearfaNerefe_0, value);
	}

	inline static int32_t get_offset_of__trajeLalyalai_1() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____trajeLalyalai_1)); }
	inline String_t* get__trajeLalyalai_1() const { return ____trajeLalyalai_1; }
	inline String_t** get_address_of__trajeLalyalai_1() { return &____trajeLalyalai_1; }
	inline void set__trajeLalyalai_1(String_t* value)
	{
		____trajeLalyalai_1 = value;
		Il2CppCodeGenWriteBarrier(&____trajeLalyalai_1, value);
	}

	inline static int32_t get_offset_of__hawfis_2() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____hawfis_2)); }
	inline int32_t get__hawfis_2() const { return ____hawfis_2; }
	inline int32_t* get_address_of__hawfis_2() { return &____hawfis_2; }
	inline void set__hawfis_2(int32_t value)
	{
		____hawfis_2 = value;
	}

	inline static int32_t get_offset_of__mawxallNesouhis_3() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____mawxallNesouhis_3)); }
	inline int32_t get__mawxallNesouhis_3() const { return ____mawxallNesouhis_3; }
	inline int32_t* get_address_of__mawxallNesouhis_3() { return &____mawxallNesouhis_3; }
	inline void set__mawxallNesouhis_3(int32_t value)
	{
		____mawxallNesouhis_3 = value;
	}

	inline static int32_t get_offset_of__callmis_4() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____callmis_4)); }
	inline uint32_t get__callmis_4() const { return ____callmis_4; }
	inline uint32_t* get_address_of__callmis_4() { return &____callmis_4; }
	inline void set__callmis_4(uint32_t value)
	{
		____callmis_4 = value;
	}

	inline static int32_t get_offset_of__leremou_5() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____leremou_5)); }
	inline uint32_t get__leremou_5() const { return ____leremou_5; }
	inline uint32_t* get_address_of__leremou_5() { return &____leremou_5; }
	inline void set__leremou_5(uint32_t value)
	{
		____leremou_5 = value;
	}

	inline static int32_t get_offset_of__tuseceeTroubar_6() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____tuseceeTroubar_6)); }
	inline bool get__tuseceeTroubar_6() const { return ____tuseceeTroubar_6; }
	inline bool* get_address_of__tuseceeTroubar_6() { return &____tuseceeTroubar_6; }
	inline void set__tuseceeTroubar_6(bool value)
	{
		____tuseceeTroubar_6 = value;
	}

	inline static int32_t get_offset_of__couvowSisdral_7() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____couvowSisdral_7)); }
	inline int32_t get__couvowSisdral_7() const { return ____couvowSisdral_7; }
	inline int32_t* get_address_of__couvowSisdral_7() { return &____couvowSisdral_7; }
	inline void set__couvowSisdral_7(int32_t value)
	{
		____couvowSisdral_7 = value;
	}

	inline static int32_t get_offset_of__dineelouPearlere_8() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____dineelouPearlere_8)); }
	inline float get__dineelouPearlere_8() const { return ____dineelouPearlere_8; }
	inline float* get_address_of__dineelouPearlere_8() { return &____dineelouPearlere_8; }
	inline void set__dineelouPearlere_8(float value)
	{
		____dineelouPearlere_8 = value;
	}

	inline static int32_t get_offset_of__caldiTirto_9() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____caldiTirto_9)); }
	inline String_t* get__caldiTirto_9() const { return ____caldiTirto_9; }
	inline String_t** get_address_of__caldiTirto_9() { return &____caldiTirto_9; }
	inline void set__caldiTirto_9(String_t* value)
	{
		____caldiTirto_9 = value;
		Il2CppCodeGenWriteBarrier(&____caldiTirto_9, value);
	}

	inline static int32_t get_offset_of__tuhihoo_10() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____tuhihoo_10)); }
	inline int32_t get__tuhihoo_10() const { return ____tuhihoo_10; }
	inline int32_t* get_address_of__tuhihoo_10() { return &____tuhihoo_10; }
	inline void set__tuhihoo_10(int32_t value)
	{
		____tuhihoo_10 = value;
	}

	inline static int32_t get_offset_of__bapalca_11() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____bapalca_11)); }
	inline bool get__bapalca_11() const { return ____bapalca_11; }
	inline bool* get_address_of__bapalca_11() { return &____bapalca_11; }
	inline void set__bapalca_11(bool value)
	{
		____bapalca_11 = value;
	}

	inline static int32_t get_offset_of__roobe_12() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____roobe_12)); }
	inline String_t* get__roobe_12() const { return ____roobe_12; }
	inline String_t** get_address_of__roobe_12() { return &____roobe_12; }
	inline void set__roobe_12(String_t* value)
	{
		____roobe_12 = value;
		Il2CppCodeGenWriteBarrier(&____roobe_12, value);
	}

	inline static int32_t get_offset_of__bere_13() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____bere_13)); }
	inline uint32_t get__bere_13() const { return ____bere_13; }
	inline uint32_t* get_address_of__bere_13() { return &____bere_13; }
	inline void set__bere_13(uint32_t value)
	{
		____bere_13 = value;
	}

	inline static int32_t get_offset_of__towkicirKejassis_14() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____towkicirKejassis_14)); }
	inline uint32_t get__towkicirKejassis_14() const { return ____towkicirKejassis_14; }
	inline uint32_t* get_address_of__towkicirKejassis_14() { return &____towkicirKejassis_14; }
	inline void set__towkicirKejassis_14(uint32_t value)
	{
		____towkicirKejassis_14 = value;
	}

	inline static int32_t get_offset_of__caysarruWhaslem_15() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____caysarruWhaslem_15)); }
	inline bool get__caysarruWhaslem_15() const { return ____caysarruWhaslem_15; }
	inline bool* get_address_of__caysarruWhaslem_15() { return &____caysarruWhaslem_15; }
	inline void set__caysarruWhaslem_15(bool value)
	{
		____caysarruWhaslem_15 = value;
	}

	inline static int32_t get_offset_of__meeciwhel_16() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____meeciwhel_16)); }
	inline String_t* get__meeciwhel_16() const { return ____meeciwhel_16; }
	inline String_t** get_address_of__meeciwhel_16() { return &____meeciwhel_16; }
	inline void set__meeciwhel_16(String_t* value)
	{
		____meeciwhel_16 = value;
		Il2CppCodeGenWriteBarrier(&____meeciwhel_16, value);
	}

	inline static int32_t get_offset_of__pukouma_17() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____pukouma_17)); }
	inline int32_t get__pukouma_17() const { return ____pukouma_17; }
	inline int32_t* get_address_of__pukouma_17() { return &____pukouma_17; }
	inline void set__pukouma_17(int32_t value)
	{
		____pukouma_17 = value;
	}

	inline static int32_t get_offset_of__jorpasJalhemna_18() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____jorpasJalhemna_18)); }
	inline int32_t get__jorpasJalhemna_18() const { return ____jorpasJalhemna_18; }
	inline int32_t* get_address_of__jorpasJalhemna_18() { return &____jorpasJalhemna_18; }
	inline void set__jorpasJalhemna_18(int32_t value)
	{
		____jorpasJalhemna_18 = value;
	}

	inline static int32_t get_offset_of__booyouLartoopem_19() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____booyouLartoopem_19)); }
	inline bool get__booyouLartoopem_19() const { return ____booyouLartoopem_19; }
	inline bool* get_address_of__booyouLartoopem_19() { return &____booyouLartoopem_19; }
	inline void set__booyouLartoopem_19(bool value)
	{
		____booyouLartoopem_19 = value;
	}

	inline static int32_t get_offset_of__gaisare_20() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____gaisare_20)); }
	inline int32_t get__gaisare_20() const { return ____gaisare_20; }
	inline int32_t* get_address_of__gaisare_20() { return &____gaisare_20; }
	inline void set__gaisare_20(int32_t value)
	{
		____gaisare_20 = value;
	}

	inline static int32_t get_offset_of__trawuCayce_21() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____trawuCayce_21)); }
	inline String_t* get__trawuCayce_21() const { return ____trawuCayce_21; }
	inline String_t** get_address_of__trawuCayce_21() { return &____trawuCayce_21; }
	inline void set__trawuCayce_21(String_t* value)
	{
		____trawuCayce_21 = value;
		Il2CppCodeGenWriteBarrier(&____trawuCayce_21, value);
	}

	inline static int32_t get_offset_of__towbealere_22() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____towbealere_22)); }
	inline String_t* get__towbealere_22() const { return ____towbealere_22; }
	inline String_t** get_address_of__towbealere_22() { return &____towbealere_22; }
	inline void set__towbealere_22(String_t* value)
	{
		____towbealere_22 = value;
		Il2CppCodeGenWriteBarrier(&____towbealere_22, value);
	}

	inline static int32_t get_offset_of__sidease_23() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____sidease_23)); }
	inline String_t* get__sidease_23() const { return ____sidease_23; }
	inline String_t** get_address_of__sidease_23() { return &____sidease_23; }
	inline void set__sidease_23(String_t* value)
	{
		____sidease_23 = value;
		Il2CppCodeGenWriteBarrier(&____sidease_23, value);
	}

	inline static int32_t get_offset_of__valsasta_24() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____valsasta_24)); }
	inline uint32_t get__valsasta_24() const { return ____valsasta_24; }
	inline uint32_t* get_address_of__valsasta_24() { return &____valsasta_24; }
	inline void set__valsasta_24(uint32_t value)
	{
		____valsasta_24 = value;
	}

	inline static int32_t get_offset_of__chemarearXisnasfi_25() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____chemarearXisnasfi_25)); }
	inline int32_t get__chemarearXisnasfi_25() const { return ____chemarearXisnasfi_25; }
	inline int32_t* get_address_of__chemarearXisnasfi_25() { return &____chemarearXisnasfi_25; }
	inline void set__chemarearXisnasfi_25(int32_t value)
	{
		____chemarearXisnasfi_25 = value;
	}

	inline static int32_t get_offset_of__watrinelKepemhere_26() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____watrinelKepemhere_26)); }
	inline uint32_t get__watrinelKepemhere_26() const { return ____watrinelKepemhere_26; }
	inline uint32_t* get_address_of__watrinelKepemhere_26() { return &____watrinelKepemhere_26; }
	inline void set__watrinelKepemhere_26(uint32_t value)
	{
		____watrinelKepemhere_26 = value;
	}

	inline static int32_t get_offset_of__kasea_27() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____kasea_27)); }
	inline uint32_t get__kasea_27() const { return ____kasea_27; }
	inline uint32_t* get_address_of__kasea_27() { return &____kasea_27; }
	inline void set__kasea_27(uint32_t value)
	{
		____kasea_27 = value;
	}

	inline static int32_t get_offset_of__mijaiguBesa_28() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____mijaiguBesa_28)); }
	inline String_t* get__mijaiguBesa_28() const { return ____mijaiguBesa_28; }
	inline String_t** get_address_of__mijaiguBesa_28() { return &____mijaiguBesa_28; }
	inline void set__mijaiguBesa_28(String_t* value)
	{
		____mijaiguBesa_28 = value;
		Il2CppCodeGenWriteBarrier(&____mijaiguBesa_28, value);
	}

	inline static int32_t get_offset_of__caircear_29() { return static_cast<int32_t>(offsetof(M_coojerwelPorawxir243_t2946437183, ____caircear_29)); }
	inline uint32_t get__caircear_29() const { return ____caircear_29; }
	inline uint32_t* get_address_of__caircear_29() { return &____caircear_29; }
	inline void set__caircear_29(uint32_t value)
	{
		____caircear_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
