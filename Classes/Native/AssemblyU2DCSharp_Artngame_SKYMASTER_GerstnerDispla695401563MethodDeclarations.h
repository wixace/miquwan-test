﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Artngame.SKYMASTER.GerstnerDisplaceSM
struct GerstnerDisplaceSM_t695401563;

#include "codegen/il2cpp-codegen.h"

// System.Void Artngame.SKYMASTER.GerstnerDisplaceSM::.ctor()
extern "C"  void GerstnerDisplaceSM__ctor_m1240989110 (GerstnerDisplaceSM_t695401563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
