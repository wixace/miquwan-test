﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1916952843.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_LOD3134610167.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.LOD>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m812732463_gshared (InternalEnumerator_1_t1916952843 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m812732463(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1916952843 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m812732463_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.LOD>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3052486929_gshared (InternalEnumerator_1_t1916952843 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3052486929(__this, method) ((  void (*) (InternalEnumerator_1_t1916952843 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3052486929_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.LOD>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3256517319_gshared (InternalEnumerator_1_t1916952843 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3256517319(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1916952843 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3256517319_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.LOD>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m90011846_gshared (InternalEnumerator_1_t1916952843 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m90011846(__this, method) ((  void (*) (InternalEnumerator_1_t1916952843 *, const MethodInfo*))InternalEnumerator_1_Dispose_m90011846_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.LOD>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m730587457_gshared (InternalEnumerator_1_t1916952843 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m730587457(__this, method) ((  bool (*) (InternalEnumerator_1_t1916952843 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m730587457_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.LOD>::get_Current()
extern "C"  LOD_t3134610167  InternalEnumerator_1_get_Current_m933544472_gshared (InternalEnumerator_1_t1916952843 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m933544472(__this, method) ((  LOD_t3134610167  (*) (InternalEnumerator_1_t1916952843 *, const MethodInfo*))InternalEnumerator_1_get_Current_m933544472_gshared)(__this, method)
