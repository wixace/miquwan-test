﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_IInitializePotentialDragHandlerGenerated
struct UnityEngine_EventSystems_IInitializePotentialDragHandlerGenerated_t3994814923;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_EventSystems_IInitializePotentialDragHandlerGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_IInitializePotentialDragHandlerGenerated__ctor_m751437824 (UnityEngine_EventSystems_IInitializePotentialDragHandlerGenerated_t3994814923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_IInitializePotentialDragHandlerGenerated::IInitializePotentialDragHandler_OnInitializePotentialDrag__PointerEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_IInitializePotentialDragHandlerGenerated_IInitializePotentialDragHandler_OnInitializePotentialDrag__PointerEventData_m1679924990 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_IInitializePotentialDragHandlerGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_IInitializePotentialDragHandlerGenerated___Register_m1306293447 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
