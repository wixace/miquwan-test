﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Mesh
struct Mesh_t4241756145;
// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorChanger
struct  ColorChanger_t3168377343  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Mesh ColorChanger::mesh
	Mesh_t4241756145 * ___mesh_2;
	// UnityEngine.Color[] ColorChanger::meshColors
	ColorU5BU5D_t2441545636* ___meshColors_3;

public:
	inline static int32_t get_offset_of_mesh_2() { return static_cast<int32_t>(offsetof(ColorChanger_t3168377343, ___mesh_2)); }
	inline Mesh_t4241756145 * get_mesh_2() const { return ___mesh_2; }
	inline Mesh_t4241756145 ** get_address_of_mesh_2() { return &___mesh_2; }
	inline void set_mesh_2(Mesh_t4241756145 * value)
	{
		___mesh_2 = value;
		Il2CppCodeGenWriteBarrier(&___mesh_2, value);
	}

	inline static int32_t get_offset_of_meshColors_3() { return static_cast<int32_t>(offsetof(ColorChanger_t3168377343, ___meshColors_3)); }
	inline ColorU5BU5D_t2441545636* get_meshColors_3() const { return ___meshColors_3; }
	inline ColorU5BU5D_t2441545636** get_address_of_meshColors_3() { return &___meshColors_3; }
	inline void set_meshColors_3(ColorU5BU5D_t2441545636* value)
	{
		___meshColors_3 = value;
		Il2CppCodeGenWriteBarrier(&___meshColors_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
