﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginYouMa
struct PluginYouMa_t2559648384;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// VersionMgr
struct VersionMgr_t1322950208;
// VersionInfo
struct VersionInfo_t2356638086;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Object
struct Il2CppObject;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginYouMa2559648384.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "AssemblyU2DCSharp_FloatTextMgr630384591.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginYouMa::.ctor()
extern "C"  void PluginYouMa__ctor_m3011061995 (PluginYouMa_t2559648384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::Init()
extern "C"  void PluginYouMa_Init_m48667657 (PluginYouMa_t2559648384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginYouMa::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginYouMa_ReqSDKHttpLogin_m1093549212 (PluginYouMa_t2559648384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginYouMa::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginYouMa_IsLoginSuccess_m239089184 (PluginYouMa_t2559648384 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::OpenUserLogin()
extern "C"  void PluginYouMa_OpenUserLogin_m811348029 (PluginYouMa_t2559648384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::CreatRole(CEvent.ZEvent)
extern "C"  void PluginYouMa_CreatRole_m842266259 (PluginYouMa_t2559648384 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::RoleEnterGame(CEvent.ZEvent)
extern "C"  void PluginYouMa_RoleEnterGame_m453409662 (PluginYouMa_t2559648384 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginYouMa_RoleUpgrade_m4094770572 (PluginYouMa_t2559648384 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::UserPay(CEvent.ZEvent)
extern "C"  void PluginYouMa_UserPay_m3197703317 (PluginYouMa_t2559648384 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::SignCallBack(System.Boolean,System.String)
extern "C"  void PluginYouMa_SignCallBack_m2213621422 (PluginYouMa_t2559648384 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginYouMa::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginYouMa_BuildOrderParam2WWWForm_m3244759384 (PluginYouMa_t2559648384 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::OnLoginSuccess(System.String)
extern "C"  void PluginYouMa_OnLoginSuccess_m1792069040 (PluginYouMa_t2559648384 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::OnLoginFail(System.String)
extern "C"  void PluginYouMa_OnLoginFail_m331809169 (PluginYouMa_t2559648384 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::LogoutSuccess(System.String)
extern "C"  void PluginYouMa_LogoutSuccess_m2868406112 (PluginYouMa_t2559648384 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::OnLogoutSuccess(System.String)
extern "C"  void PluginYouMa_OnLogoutSuccess_m4018075359 (PluginYouMa_t2559648384 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::OnPaySuccess(System.String)
extern "C"  void PluginYouMa_OnPaySuccess_m1911479279 (PluginYouMa_t2559648384 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::OnPayFail(System.String)
extern "C"  void PluginYouMa_OnPayFail_m2546119602 (PluginYouMa_t2559648384 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::init(System.String,System.String)
extern "C"  void PluginYouMa_init_m2482504245 (PluginYouMa_t2559648384 * __this, String_t* ___appid0, String_t* ___channel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::login()
extern "C"  void PluginYouMa_login_m2533076818 (PluginYouMa_t2559648384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::logout()
extern "C"  void PluginYouMa_logout_m1221792739 (PluginYouMa_t2559648384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::CreateRole(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYouMa_CreateRole_m2862582963 (PluginYouMa_t2559648384 * __this, String_t* ___playerId0, String_t* ___playerName1, String_t* ___playerLevel2, String_t* ___userId3, String_t* ___areaId4, String_t* ___areaName5, String_t* ___serverId6, String_t* ___serverName7, String_t* ___eventName8, String_t* ___isTest9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::LoginRole(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYouMa_LoginRole_m2410865366 (PluginYouMa_t2559648384 * __this, String_t* ___playerId0, String_t* ___playerName1, String_t* ___playerLevel2, String_t* ___userId3, String_t* ___areaId4, String_t* ___areaName5, String_t* ___serverId6, String_t* ___serverName7, String_t* ___eventName8, String_t* ___isTest9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::UpRole(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYouMa_UpRole_m93906164 (PluginYouMa_t2559648384 * __this, String_t* ___playerId0, String_t* ___playerName1, String_t* ___playerLevel2, String_t* ___userId3, String_t* ___areaId4, String_t* ___areaName5, String_t* ___serverId6, String_t* ___serverName7, String_t* ___eventName8, String_t* ___isTest9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYouMa_pay_m929082405 (PluginYouMa_t2559648384 * __this, String_t* ___roleId0, String_t* ___serverId1, String_t* ___userName2, String_t* ___cpExp3, String_t* ___orderTitle4, String_t* ___orderDesc5, String_t* ___productId6, String_t* ___cpOrderId7, String_t* ___money8, String_t* ___orderNumber9, String_t* ___bfbAppid10, String_t* ___urlScheme11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::<LogoutSuccess>m__478()
extern "C"  void PluginYouMa_U3CLogoutSuccessU3Em__478_m3912125618 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::<OnLogoutSuccess>m__479()
extern "C"  void PluginYouMa_U3COnLogoutSuccessU3Em__479_m1376237618 (PluginYouMa_t2559648384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginYouMa_ilo_AddEventListener1_m2825148073 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginYouMa::ilo_get_PluginsSdkMgr2()
extern "C"  PluginsSdkMgr_t3884624670 * PluginYouMa_ilo_get_PluginsSdkMgr2_m230291668 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginYouMa::ilo_get_Instance3()
extern "C"  VersionMgr_t1322950208 * PluginYouMa_ilo_get_Instance3_m1116147720 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginYouMa::ilo_get_isSdkLogin4(VersionMgr)
extern "C"  bool PluginYouMa_ilo_get_isSdkLogin4_m2067827420 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::ilo_CreateRole5(PluginYouMa,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYouMa_ilo_CreateRole5_m2291966161 (Il2CppObject * __this /* static, unused */, PluginYouMa_t2559648384 * ____this0, String_t* ___playerId1, String_t* ___playerName2, String_t* ___playerLevel3, String_t* ___userId4, String_t* ___areaId5, String_t* ___areaName6, String_t* ___serverId7, String_t* ___serverName8, String_t* ___eventName9, String_t* ___isTest10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::ilo_LoginRole6(PluginYouMa,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYouMa_ilo_LoginRole6_m1962922727 (Il2CppObject * __this /* static, unused */, PluginYouMa_t2559648384 * ____this0, String_t* ___playerId1, String_t* ___playerName2, String_t* ___playerLevel3, String_t* ___userId4, String_t* ___areaId5, String_t* ___areaName6, String_t* ___serverId7, String_t* ___serverName8, String_t* ___eventName9, String_t* ___isTest10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginYouMa::ilo_get_currentVS7(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginYouMa_ilo_get_currentVS7_m440419497 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken PluginYouMa::ilo_get_Item8(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  JToken_t3412245951 * PluginYouMa_ilo_get_Item8_m4002381513 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginYouMa::ilo_GetProductID9(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginYouMa_ilo_GetProductID9_m2874506690 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::ilo_FloatText10(FloatTextMgr,FLOAT_TEXT_ID,System.Int32,System.Object[])
extern "C"  void PluginYouMa_ilo_FloatText10_m774492306 (Il2CppObject * __this /* static, unused */, FloatTextMgr_t630384591 * ____this0, int32_t ___textId1, int32_t ___offsetY2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::ilo_Log11(System.Object,System.Boolean)
extern "C"  void PluginYouMa_ilo_Log11_m4021794883 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouMa::ilo_Logout12(System.Action)
extern "C"  void PluginYouMa_ilo_Logout12_m889940742 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
