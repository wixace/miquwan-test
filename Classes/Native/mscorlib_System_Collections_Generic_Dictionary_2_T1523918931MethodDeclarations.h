﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,System.Collections.Generic.KeyValuePair`2<System.Object,ProductsCfgMgr/ProductInfo>>
struct Transform_1_t1523918931;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23374811140.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr_ProductInfo1305991238.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,System.Collections.Generic.KeyValuePair`2<System.Object,ProductsCfgMgr/ProductInfo>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m992106642_gshared (Transform_1_t1523918931 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m992106642(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t1523918931 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m992106642_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,System.Collections.Generic.KeyValuePair`2<System.Object,ProductsCfgMgr/ProductInfo>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3374811140  Transform_1_Invoke_m3189605286_gshared (Transform_1_t1523918931 * __this, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m3189605286(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t3374811140  (*) (Transform_1_t1523918931 *, Il2CppObject *, ProductInfo_t1305991238 , const MethodInfo*))Transform_1_Invoke_m3189605286_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,System.Collections.Generic.KeyValuePair`2<System.Object,ProductsCfgMgr/ProductInfo>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1205593041_gshared (Transform_1_t1523918931 * __this, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m1205593041(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t1523918931 *, Il2CppObject *, ProductInfo_t1305991238 , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m1205593041_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,System.Collections.Generic.KeyValuePair`2<System.Object,ProductsCfgMgr/ProductInfo>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3374811140  Transform_1_EndInvoke_m759068964_gshared (Transform_1_t1523918931 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m759068964(__this, ___result0, method) ((  KeyValuePair_2_t3374811140  (*) (Transform_1_t1523918931 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m759068964_gshared)(__this, ___result0, method)
