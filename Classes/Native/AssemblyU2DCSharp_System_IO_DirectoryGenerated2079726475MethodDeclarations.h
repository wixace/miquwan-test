﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_IO_DirectoryGenerated
struct System_IO_DirectoryGenerated_t2079726475;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_IO_DirectoryGenerated::.ctor()
extern "C"  void System_IO_DirectoryGenerated__ctor_m255423344 (System_IO_DirectoryGenerated_t2079726475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_CreateDirectory__String__DirectorySecurity(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_CreateDirectory__String__DirectorySecurity_m1249163317 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_CreateDirectory__String(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_CreateDirectory__String_m848273498 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_Delete__String__Boolean(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_Delete__String__Boolean_m3057576420 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_Delete__String(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_Delete__String_m138238470 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_Exists__String(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_Exists__String_m1840954615 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_GetAccessControl__String__AccessControlSections(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_GetAccessControl__String__AccessControlSections_m1918797355 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_GetAccessControl__String(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_GetAccessControl__String_m2358774462 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_GetCreationTime__String(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_GetCreationTime__String_m2234330251 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_GetCreationTimeUtc__String(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_GetCreationTimeUtc__String_m1622077981 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_GetCurrentDirectory(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_GetCurrentDirectory_m2327907938 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_GetDirectories__String__String__SearchOption(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_GetDirectories__String__String__SearchOption_m467177278 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_GetDirectories__String__String(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_GetDirectories__String__String_m1954886337 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_GetDirectories__String(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_GetDirectories__String_m1042932848 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_GetDirectoryRoot__String(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_GetDirectoryRoot__String_m2949547476 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_GetFiles__String__String__SearchOption(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_GetFiles__String__String__SearchOption_m18543146 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_GetFiles__String__String(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_GetFiles__String__String_m2338316973 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_GetFiles__String(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_GetFiles__String_m293159004 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_GetFileSystemEntries__String__String(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_GetFileSystemEntries__String__String_m889044091 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_GetFileSystemEntries__String(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_GetFileSystemEntries__String_m142395434 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_GetLastAccessTime__String(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_GetLastAccessTime__String_m1246721862 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_GetLastAccessTimeUtc__String(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_GetLastAccessTimeUtc__String_m306538882 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_GetLastWriteTime__String(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_GetLastWriteTime__String_m3190697979 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_GetLastWriteTimeUtc__String(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_GetLastWriteTimeUtc__String_m4254988461 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_GetLogicalDrives(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_GetLogicalDrives_m367374054 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_GetParent__String(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_GetParent__String_m1337178153 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_Move__String__String(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_Move__String__String_m3614889533 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_SetAccessControl__String__DirectorySecurity(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_SetAccessControl__String__DirectorySecurity_m2254531421 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_SetCreationTime__String__DateTime(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_SetCreationTime__String__DateTime_m1392305266 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_SetCreationTimeUtc__String__DateTime(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_SetCreationTimeUtc__String__DateTime_m342061548 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_SetCurrentDirectory__String(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_SetCurrentDirectory__String_m1393001151 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_SetLastAccessTime__String__DateTime(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_SetLastAccessTime__String__DateTime_m1196797549 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_SetLastAccessTimeUtc__String__DateTime(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_SetLastAccessTimeUtc__String__DateTime_m4242285073 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_SetLastWriteTime__String__DateTime(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_SetLastWriteTime__String__DateTime_m4017897290 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryGenerated::Directory_SetLastWriteTimeUtc__String__DateTime(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryGenerated_Directory_SetLastWriteTimeUtc__String__DateTime_m3704621076 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_DirectoryGenerated::__Register()
extern "C"  void System_IO_DirectoryGenerated___Register_m4000755543 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System_IO_DirectoryGenerated::ilo_getStringS1(System.Int32)
extern "C"  String_t* System_IO_DirectoryGenerated_ilo_getStringS1_m686058670 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_IO_DirectoryGenerated::ilo_getEnum2(System.Int32)
extern "C"  int32_t System_IO_DirectoryGenerated_ilo_getEnum2_m280580193 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_DirectoryGenerated::ilo_setStringS3(System.Int32,System.String)
extern "C"  void System_IO_DirectoryGenerated_ilo_setStringS3_m1421907771 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_DirectoryGenerated::ilo_moveSaveID2Arr4(System.Int32)
extern "C"  void System_IO_DirectoryGenerated_ilo_moveSaveID2Arr4_m3602691144 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_DirectoryGenerated::ilo_setArrayS5(System.Int32,System.Int32,System.Boolean)
extern "C"  void System_IO_DirectoryGenerated_ilo_setArrayS5_m1319315095 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_IO_DirectoryGenerated::ilo_setObject6(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_IO_DirectoryGenerated_ilo_setObject6_m819371871 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_IO_DirectoryGenerated::ilo_getObject7(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_IO_DirectoryGenerated_ilo_getObject7_m3255874821 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
