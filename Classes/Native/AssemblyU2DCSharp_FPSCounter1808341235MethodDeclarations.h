﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FPSCounter
struct FPSCounter_t1808341235;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void FPSCounter::.ctor()
extern "C"  void FPSCounter__ctor_m645128456 (FPSCounter_t1808341235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FPSCounter::get_isShow()
extern "C"  bool FPSCounter_get_isShow_m1307047392 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSCounter::Awake()
extern "C"  void FPSCounter_Awake_m882733675 (FPSCounter_t1808341235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSCounter::Update()
extern "C"  void FPSCounter_Update_m251007749 (FPSCounter_t1808341235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSCounter::FixedUpdate()
extern "C"  void FPSCounter_FixedUpdate_m461559939 (FPSCounter_t1808341235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSCounter::OnAwake(UnityEngine.GameObject)
extern "C"  void FPSCounter_OnAwake_m3904187588 (FPSCounter_t1808341235 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSCounter::OnDestroy()
extern "C"  void FPSCounter_OnDestroy_m2816340481 (FPSCounter_t1808341235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
