﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._cec31e8f6cd33577ac28864117cb2ae4
struct _cec31e8f6cd33577ac28864117cb2ae4_t3715589169;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._cec31e8f6cd33577ac28864117cb2ae4::.ctor()
extern "C"  void _cec31e8f6cd33577ac28864117cb2ae4__ctor_m2255153436 (_cec31e8f6cd33577ac28864117cb2ae4_t3715589169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._cec31e8f6cd33577ac28864117cb2ae4::_cec31e8f6cd33577ac28864117cb2ae4m2(System.Int32)
extern "C"  int32_t _cec31e8f6cd33577ac28864117cb2ae4__cec31e8f6cd33577ac28864117cb2ae4m2_m2870844409 (_cec31e8f6cd33577ac28864117cb2ae4_t3715589169 * __this, int32_t ____cec31e8f6cd33577ac28864117cb2ae4a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._cec31e8f6cd33577ac28864117cb2ae4::_cec31e8f6cd33577ac28864117cb2ae4m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _cec31e8f6cd33577ac28864117cb2ae4__cec31e8f6cd33577ac28864117cb2ae4m_m3308615389 (_cec31e8f6cd33577ac28864117cb2ae4_t3715589169 * __this, int32_t ____cec31e8f6cd33577ac28864117cb2ae4a0, int32_t ____cec31e8f6cd33577ac28864117cb2ae4461, int32_t ____cec31e8f6cd33577ac28864117cb2ae4c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
