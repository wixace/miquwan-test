﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Voxels.Voxelize
struct Voxelize_t3998270866;
// Pathfinding.Voxels.VoxelContourSet
struct VoxelContourSet_t2502455108;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.UInt16[]
struct UInt16U5BU5D_t801649474;
// System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>
struct List_1_t1291247971;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Collections.Generic.List`1<Pathfinding.Int3>
struct List_1_t3342231146;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// Pathfinding.Voxels.VoxelArea
struct VoxelArea_t3943332841;
// Pathfinding.RelevantGraphSurface
struct RelevantGraphSurface_t4201206834;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_VoxelContourS2502455108.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_VoxelContour3597201016.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_VoxelMesh3943678281.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_CompactVoxelS3431582481.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_Voxelize3998270866.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_VoxelArea3943332841.h"
#include "AssemblyU2DCSharp_Pathfinding_RelevantGraphSurface4201206834.h"

// System.Void Pathfinding.Voxels.Voxelize::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void Voxelize__ctor_m1684241241 (Voxelize_t3998270866 * __this, float ___ch0, float ___cs1, float ___wc2, float ___wh3, float ___ms4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::.cctor()
extern "C"  void Voxelize__cctor_m3270262855 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::BuildContours(System.Single,System.Int32,Pathfinding.Voxels.VoxelContourSet,System.Int32)
extern "C"  void Voxelize_BuildContours_m3269040351 (Voxelize_t3998270866 * __this, float ___maxError0, int32_t ___maxEdgeLength1, VoxelContourSet_t2502455108 * ___cset2, int32_t ___buildFlags3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::GetClosestIndices(System.Int32[],System.Int32,System.Int32[],System.Int32,System.Int32&,System.Int32&)
extern "C"  void Voxelize_GetClosestIndices_m3735404780 (Voxelize_t3998270866 * __this, Int32U5BU5D_t3230847821* ___vertsa0, int32_t ___nvertsa1, Int32U5BU5D_t3230847821* ___vertsb2, int32_t ___nvertsb3, int32_t* ___ia4, int32_t* ___ib5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::ReleaseIntArr(System.Int32[])
extern "C"  void Voxelize_ReleaseIntArr_m2013706316 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___arr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Pathfinding.Voxels.Voxelize::ClaimIntArr(System.Int32,System.Boolean)
extern "C"  Int32U5BU5D_t3230847821* Voxelize_ClaimIntArr_m2872916494 (Il2CppObject * __this /* static, unused */, int32_t ___minCapacity0, bool ___zero1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::ReleaseContours(Pathfinding.Voxels.VoxelContourSet)
extern "C"  void Voxelize_ReleaseContours_m3511458061 (Il2CppObject * __this /* static, unused */, VoxelContourSet_t2502455108 * ___cset0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Voxels.Voxelize::MergeContours(Pathfinding.Voxels.VoxelContour&,Pathfinding.Voxels.VoxelContour&,System.Int32,System.Int32)
extern "C"  bool Voxelize_MergeContours_m664331645 (Il2CppObject * __this /* static, unused */, VoxelContour_t3597201016 * ___ca0, VoxelContour_t3597201016 * ___cb1, int32_t ___ia2, int32_t ___ib3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::SimplifyContour(System.Collections.Generic.List`1<System.Int32>,System.Collections.Generic.List`1<System.Int32>,System.Single,System.Int32,System.Int32)
extern "C"  void Voxelize_SimplifyContour_m3516859980 (Voxelize_t3998270866 * __this, List_1_t2522024052 * ___verts0, List_1_t2522024052 * ___simplified1, float ___maxError2, int32_t ___maxEdgeLenght3, int32_t ___buildFlags4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::WalkContour(System.Int32,System.Int32,System.Int32,System.UInt16[],System.Collections.Generic.List`1<System.Int32>)
extern "C"  void Voxelize_WalkContour_m2379761613 (Voxelize_t3998270866 * __this, int32_t ___x0, int32_t ___z1, int32_t ___i2, UInt16U5BU5D_t801649474* ___flags3, List_1_t2522024052 * ___verts4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Voxelize::GetCornerHeight(System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean&)
extern "C"  int32_t Voxelize_GetCornerHeight_m3671400849 (Voxelize_t3998270866 * __this, int32_t ___x0, int32_t ___z1, int32_t ___i2, int32_t ___dir3, bool* ___isBorderVertex4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::RemoveDegenerateSegments(System.Collections.Generic.List`1<System.Int32>)
extern "C"  void Voxelize_RemoveDegenerateSegments_m3265826889 (Voxelize_t3998270866 * __this, List_1_t2522024052 * ___simplified0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Voxelize::CalcAreaOfPolygon2D(System.Int32[],System.Int32)
extern "C"  int32_t Voxelize_CalcAreaOfPolygon2D_m1478328579 (Voxelize_t3998270866 * __this, Int32U5BU5D_t3230847821* ___verts0, int32_t ___nverts1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Voxels.Voxelize::Ileft(System.Int32,System.Int32,System.Int32,System.Int32[],System.Int32[],System.Int32[])
extern "C"  bool Voxelize_Ileft_m3005300764 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, int32_t ___c2, Int32U5BU5D_t3230847821* ___va3, Int32U5BU5D_t3230847821* ___vb4, Int32U5BU5D_t3230847821* ___vc5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Voxels.Voxelize::Diagonal(System.Int32,System.Int32,System.Int32,System.Int32[],System.Int32[])
extern "C"  bool Voxelize_Diagonal_m2885060400 (Il2CppObject * __this /* static, unused */, int32_t ___i0, int32_t ___j1, int32_t ___n2, Int32U5BU5D_t3230847821* ___verts3, Int32U5BU5D_t3230847821* ___indices4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Voxels.Voxelize::InCone(System.Int32,System.Int32,System.Int32,System.Int32[],System.Int32[])
extern "C"  bool Voxelize_InCone_m3056299235 (Il2CppObject * __this /* static, unused */, int32_t ___i0, int32_t ___j1, int32_t ___n2, Int32U5BU5D_t3230847821* ___verts3, Int32U5BU5D_t3230847821* ___indices4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Voxels.Voxelize::Left(System.Int32,System.Int32,System.Int32,System.Int32[])
extern "C"  bool Voxelize_Left_m3117324243 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, int32_t ___c2, Int32U5BU5D_t3230847821* ___verts3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Voxels.Voxelize::LeftOn(System.Int32,System.Int32,System.Int32,System.Int32[])
extern "C"  bool Voxelize_LeftOn_m2086950644 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, int32_t ___c2, Int32U5BU5D_t3230847821* ___verts3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Voxels.Voxelize::Collinear(System.Int32,System.Int32,System.Int32,System.Int32[])
extern "C"  bool Voxelize_Collinear_m2444021991 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, int32_t ___c2, Int32U5BU5D_t3230847821* ___verts3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Voxelize::Area2(System.Int32,System.Int32,System.Int32,System.Int32[])
extern "C"  int32_t Voxelize_Area2_m47865517 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, int32_t ___c2, Int32U5BU5D_t3230847821* ___verts3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Voxels.Voxelize::Diagonalie(System.Int32,System.Int32,System.Int32,System.Int32[],System.Int32[])
extern "C"  bool Voxelize_Diagonalie_m2104819660 (Il2CppObject * __this /* static, unused */, int32_t ___i0, int32_t ___j1, int32_t ___n2, Int32U5BU5D_t3230847821* ___verts3, Int32U5BU5D_t3230847821* ___indices4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Voxels.Voxelize::Xorb(System.Boolean,System.Boolean)
extern "C"  bool Voxelize_Xorb_m3080195061 (Il2CppObject * __this /* static, unused */, bool ___x0, bool ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Voxels.Voxelize::IntersectProp(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32[])
extern "C"  bool Voxelize_IntersectProp_m1938895241 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, int32_t ___c2, int32_t ___d3, Int32U5BU5D_t3230847821* ___verts4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Voxels.Voxelize::Between(System.Int32,System.Int32,System.Int32,System.Int32[])
extern "C"  bool Voxelize_Between_m2055120996 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, int32_t ___c2, Int32U5BU5D_t3230847821* ___verts3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Voxels.Voxelize::Intersect(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32[])
extern "C"  bool Voxelize_Intersect_m3795633798 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, int32_t ___c2, int32_t ___d3, Int32U5BU5D_t3230847821* ___verts4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Voxels.Voxelize::Vequal(System.Int32,System.Int32,System.Int32[])
extern "C"  bool Voxelize_Vequal_m3441321527 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, Int32U5BU5D_t3230847821* ___verts2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Voxelize::Prev(System.Int32,System.Int32)
extern "C"  int32_t Voxelize_Prev_m2423773059 (Il2CppObject * __this /* static, unused */, int32_t ___i0, int32_t ___n1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Voxelize::Next(System.Int32,System.Int32)
extern "C"  int32_t Voxelize_Next_m2368566979 (Il2CppObject * __this /* static, unused */, int32_t ___i0, int32_t ___n1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::BuildPolyMesh(Pathfinding.Voxels.VoxelContourSet,System.Int32,Pathfinding.Voxels.VoxelMesh&)
extern "C"  void Voxelize_BuildPolyMesh_m158296197 (Voxelize_t3998270866 * __this, VoxelContourSet_t2502455108 * ___cset0, int32_t ___nvp1, VoxelMesh_t3943678281 * ___mesh2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Voxelize::Triangulate(System.Int32,System.Int32[],System.Int32[]&,System.Int32[]&)
extern "C"  int32_t Voxelize_Triangulate_m921879092 (Voxelize_t3998270866 * __this, int32_t ___n0, Int32U5BU5D_t3230847821* ___verts1, Int32U5BU5D_t3230847821** ___indices2, Int32U5BU5D_t3230847821** ___tris3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.Voxels.Voxelize::CompactSpanToVector(System.Int32,System.Int32,System.Int32)
extern "C"  Vector3_t4282066566  Voxelize_CompactSpanToVector_m2459852986 (Voxelize_t3998270866 * __this, int32_t ___x0, int32_t ___z1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::VectorToIndex(UnityEngine.Vector3,System.Int32&,System.Int32&)
extern "C"  void Voxelize_VectorToIndex_m4130303659 (Voxelize_t3998270866 * __this, Vector3_t4282066566  ___p0, int32_t* ___x1, int32_t* ___z2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::OnGUI()
extern "C"  void Voxelize_OnGUI_m863368480 (Voxelize_t3998270866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::CollectMeshes()
extern "C"  void Voxelize_CollectMeshes_m3094146601 (Voxelize_t3998270866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::CollectMeshes(System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>,UnityEngine.Bounds,UnityEngine.Vector3[]&,System.Int32[]&)
extern "C"  void Voxelize_CollectMeshes_m1739917287 (Il2CppObject * __this /* static, unused */, List_1_t1291247971 * ___extraMeshes0, Bounds_t2711641849  ___bounds1, Vector3U5BU5D_t215400611** ___verts2, Int32U5BU5D_t3230847821** ___tris3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::Init()
extern "C"  void Voxelize_Init_m2212421998 (Voxelize_t3998270866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::VoxelizeInput()
extern "C"  void Voxelize_VoxelizeInput_m2372944352 (Voxelize_t3998270866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::BuildCompactField()
extern "C"  void Voxelize_BuildCompactField_m3788754089 (Voxelize_t3998270866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::BuildVoxelConnections()
extern "C"  void Voxelize_BuildVoxelConnections_m1364334145 (Voxelize_t3998270866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::DrawLine(System.Int32,System.Int32,System.Int32[],System.Int32[],UnityEngine.Color)
extern "C"  void Voxelize_DrawLine_m1166440048 (Voxelize_t3998270866 * __this, int32_t ___a0, int32_t ___b1, Int32U5BU5D_t3230847821* ___indices2, Int32U5BU5D_t3230847821* ___verts3, Color_t4194546905  ___col4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.Voxels.Voxelize::ConvertPos(System.Int32,System.Int32,System.Int32)
extern "C"  Vector3_t4282066566  Voxelize_ConvertPos_m2865026422 (Voxelize_t3998270866 * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.Voxels.Voxelize::ConvertPosCorrZ(System.Int32,System.Int32,System.Int32)
extern "C"  Vector3_t4282066566  Voxelize_ConvertPosCorrZ_m1518861436 (Voxelize_t3998270866 * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.Voxels.Voxelize::ConvertPosWithoutOffset(System.Int32,System.Int32,System.Int32)
extern "C"  Vector3_t4282066566  Voxelize_ConvertPosWithoutOffset_m1310778569 (Voxelize_t3998270866 * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.Voxels.Voxelize::ConvertPosition(System.Int32,System.Int32,System.Int32)
extern "C"  Vector3_t4282066566  Voxelize_ConvertPosition_m4094280299 (Voxelize_t3998270866 * __this, int32_t ___x0, int32_t ___z1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::ErodeWalkableArea(System.Int32)
extern "C"  void Voxelize_ErodeWalkableArea_m3683114408 (Voxelize_t3998270866 * __this, int32_t ___radius0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::BuildDistanceField()
extern "C"  void Voxelize_BuildDistanceField_m587418133 (Voxelize_t3998270866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::ErodeVoxels(System.Int32)
extern "C"  void Voxelize_ErodeVoxels_m1944079621 (Voxelize_t3998270866 * __this, int32_t ___radius0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::FilterLowHeightSpans(System.UInt32,System.Single,System.Single,UnityEngine.Vector3)
extern "C"  void Voxelize_FilterLowHeightSpans_m3771571475 (Voxelize_t3998270866 * __this, uint32_t ___voxelWalkableHeight0, float ___cs1, float ___ch2, Vector3_t4282066566  ___min3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::FilterLedges(System.UInt32,System.Int32,System.Single,System.Single,UnityEngine.Vector3)
extern "C"  void Voxelize_FilterLedges_m595480474 (Voxelize_t3998270866 * __this, uint32_t ___voxelWalkableHeight0, int32_t ___voxelWalkableClimb1, float ___cs2, float ___ch3, Vector3_t4282066566  ___min4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16[] Pathfinding.Voxels.Voxelize::ExpandRegions(System.Int32,System.UInt32,System.UInt16[],System.UInt16[],System.UInt16[],System.UInt16[],System.Collections.Generic.List`1<System.Int32>)
extern "C"  UInt16U5BU5D_t801649474* Voxelize_ExpandRegions_m4203590892 (Voxelize_t3998270866 * __this, int32_t ___maxIterations0, uint32_t ___level1, UInt16U5BU5D_t801649474* ___srcReg2, UInt16U5BU5D_t801649474* ___srcDist3, UInt16U5BU5D_t801649474* ___dstReg4, UInt16U5BU5D_t801649474* ___dstDist5, List_1_t2522024052 * ___stack6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Voxels.Voxelize::FloodRegion(System.Int32,System.Int32,System.Int32,System.UInt32,System.UInt16,System.UInt16[],System.UInt16[],System.Collections.Generic.List`1<System.Int32>)
extern "C"  bool Voxelize_FloodRegion_m2701688304 (Voxelize_t3998270866 * __this, int32_t ___x0, int32_t ___z1, int32_t ___i2, uint32_t ___level3, uint16_t ___r4, UInt16U5BU5D_t801649474* ___srcReg5, UInt16U5BU5D_t801649474* ___srcDist6, List_1_t2522024052 * ___stack7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::MarkRectWithRegion(System.Int32,System.Int32,System.Int32,System.Int32,System.UInt16,System.UInt16[])
extern "C"  void Voxelize_MarkRectWithRegion_m3312398391 (Voxelize_t3998270866 * __this, int32_t ___minx0, int32_t ___maxx1, int32_t ___minz2, int32_t ___maxz3, uint16_t ___region4, UInt16U5BU5D_t801649474* ___srcReg5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 Pathfinding.Voxels.Voxelize::CalculateDistanceField(System.UInt16[])
extern "C"  uint16_t Voxelize_CalculateDistanceField_m680630634 (Voxelize_t3998270866 * __this, UInt16U5BU5D_t801649474* ___src0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16[] Pathfinding.Voxels.Voxelize::BoxBlur(System.UInt16[],System.UInt16[])
extern "C"  UInt16U5BU5D_t801649474* Voxelize_BoxBlur_m1419071605 (Voxelize_t3998270866 * __this, UInt16U5BU5D_t801649474* ___src0, UInt16U5BU5D_t801649474* ___dst1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::FloodOnes(System.Collections.Generic.List`1<Pathfinding.Int3>,System.UInt16[],System.UInt32,System.UInt16)
extern "C"  void Voxelize_FloodOnes_m2886667359 (Voxelize_t3998270866 * __this, List_1_t3342231146 * ___st10, UInt16U5BU5D_t801649474* ___regs1, uint32_t ___level2, uint16_t ___reg3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::BuildRegions()
extern "C"  void Voxelize_BuildRegions_m1347828943 (Voxelize_t3998270866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Voxelize::union_find_find(System.Int32[],System.Int32)
extern "C"  int32_t Voxelize_union_find_find_m1977597415 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___arr0, int32_t ___x1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::union_find_union(System.Int32[],System.Int32,System.Int32)
extern "C"  void Voxelize_union_find_union_m1126252678 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___arr0, int32_t ___a1, int32_t ___b2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::FilterSmallRegions(System.UInt16[],System.Int32,System.Int32)
extern "C"  void Voxelize_FilterSmallRegions_m2950290564 (Voxelize_t3998270866 * __this, UInt16U5BU5D_t801649474* ___reg0, int32_t ___minRegionSize1, int32_t ___maxRegions2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Voxelize::ilo_GetConnection1(Pathfinding.Voxels.CompactVoxelSpan&,System.Int32)
extern "C"  int32_t Voxelize_ilo_GetConnection1_m1958861049 (Il2CppObject * __this /* static, unused */, CompactVoxelSpan_t3431582481 * ____this0, int32_t ___dir1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::ilo_SimplifyContour2(Pathfinding.Voxels.Voxelize,System.Collections.Generic.List`1<System.Int32>,System.Collections.Generic.List`1<System.Int32>,System.Single,System.Int32,System.Int32)
extern "C"  void Voxelize_ilo_SimplifyContour2_m37995670 (Il2CppObject * __this /* static, unused */, Voxelize_t3998270866 * ____this0, List_1_t2522024052 * ___verts1, List_1_t2522024052 * ___simplified2, float ___maxError3, int32_t ___maxEdgeLenght4, int32_t ___buildFlags5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::ilo_GetClosestIndices3(Pathfinding.Voxels.Voxelize,System.Int32[],System.Int32,System.Int32[],System.Int32,System.Int32&,System.Int32&)
extern "C"  void Voxelize_ilo_GetClosestIndices3_m1724881371 (Il2CppObject * __this /* static, unused */, Voxelize_t3998270866 * ____this0, Int32U5BU5D_t3230847821* ___vertsa1, int32_t ___nvertsa2, Int32U5BU5D_t3230847821* ___vertsb3, int32_t ___nvertsb4, int32_t* ___ia5, int32_t* ___ib6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Voxels.Voxelize::ilo_MergeContours4(Pathfinding.Voxels.VoxelContour&,Pathfinding.Voxels.VoxelContour&,System.Int32,System.Int32)
extern "C"  bool Voxelize_ilo_MergeContours4_m1124026630 (Il2CppObject * __this /* static, unused */, VoxelContour_t3597201016 * ___ca0, VoxelContour_t3597201016 * ___cb1, int32_t ___ia2, int32_t ___ib3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Voxels.Voxelize::ilo_Ileft5(System.Int32,System.Int32,System.Int32,System.Int32[],System.Int32[],System.Int32[])
extern "C"  bool Voxelize_ilo_Ileft5_m805845282 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, int32_t ___c2, Int32U5BU5D_t3230847821* ___va3, Int32U5BU5D_t3230847821* ___vb4, Int32U5BU5D_t3230847821* ___vc5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::ilo_ReleaseIntArr6(System.Int32[])
extern "C"  void Voxelize_ilo_ReleaseIntArr6_m2982955709 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___arr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Voxelize::ilo_Prev7(System.Int32,System.Int32)
extern "C"  int32_t Voxelize_ilo_Prev7_m593586915 (Il2CppObject * __this /* static, unused */, int32_t ___i0, int32_t ___n1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Voxelize::ilo_Area28(System.Int32,System.Int32,System.Int32,System.Int32[])
extern "C"  int32_t Voxelize_ilo_Area28_m2319853070 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, int32_t ___c2, Int32U5BU5D_t3230847821* ___verts3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Voxelize::ilo_Next9(System.Int32,System.Int32)
extern "C"  int32_t Voxelize_ilo_Next9_m2667692641 (Il2CppObject * __this /* static, unused */, int32_t ___i0, int32_t ___n1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Voxels.Voxelize::ilo_Vequal10(System.Int32,System.Int32,System.Int32[])
extern "C"  bool Voxelize_ilo_Vequal10_m2064209609 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, Int32U5BU5D_t3230847821* ___verts2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Voxels.Voxelize::ilo_Collinear11(System.Int32,System.Int32,System.Int32,System.Int32[])
extern "C"  bool Voxelize_ilo_Collinear11_m3690063546 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, int32_t ___c2, Int32U5BU5D_t3230847821* ___verts3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Voxels.Voxelize::ilo_Xorb12(System.Boolean,System.Boolean)
extern "C"  bool Voxelize_ilo_Xorb12_m560367585 (Il2CppObject * __this /* static, unused */, bool ___x0, bool ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Voxels.Voxelize::ilo_IntersectProp13(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32[])
extern "C"  bool Voxelize_ilo_IntersectProp13_m618602456 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, int32_t ___c2, int32_t ___d3, Int32U5BU5D_t3230847821* ___verts4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Voxelize::ilo_Triangulate14(Pathfinding.Voxels.Voxelize,System.Int32,System.Int32[],System.Int32[]&,System.Int32[]&)
extern "C"  int32_t Voxelize_ilo_Triangulate14_m3352101387 (Il2CppObject * __this /* static, unused */, Voxelize_t3998270866 * ____this0, int32_t ___n1, Int32U5BU5D_t3230847821* ___verts2, Int32U5BU5D_t3230847821** ___indices3, Int32U5BU5D_t3230847821** ___tris4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Voxels.Voxelize::ilo_Diagonal15(System.Int32,System.Int32,System.Int32,System.Int32[],System.Int32[])
extern "C"  bool Voxelize_ilo_Diagonal15_m3402352263 (Il2CppObject * __this /* static, unused */, int32_t ___i0, int32_t ___j1, int32_t ___n2, Int32U5BU5D_t3230847821* ___verts3, Int32U5BU5D_t3230847821* ___indices4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.Voxels.Voxelize::ilo_Min16(System.Single,System.Single,System.Single)
extern "C"  float Voxelize_ilo_Min16_m3180769665 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, float ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::ilo_CopyVector17(System.Single[],System.Int32,UnityEngine.Vector3)
extern "C"  void Voxelize_ilo_CopyVector17_m1431986576 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t2316563989* ___a0, int32_t ___i1, Vector3_t4282066566  ___v2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Voxelize::ilo_ClipPolygon18(System.Single[],System.Int32,System.Single[],System.Single,System.Single,System.Int32)
extern "C"  int32_t Voxelize_ilo_ClipPolygon18_m2839520140 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t2316563989* ___vIn0, int32_t ___n1, SingleU5BU5D_t2316563989* ___vOut2, float ___multi3, float ___offset4, int32_t ___axis5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Voxelize::ilo_Clamp19(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t Voxelize_ilo_Clamp19_m95477779 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, int32_t ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Voxelize::ilo_GetSpanCount20(Pathfinding.Voxels.VoxelArea)
extern "C"  int32_t Voxelize_ilo_GetSpanCount20_m2054672820 (Il2CppObject * __this /* static, unused */, VoxelArea_t3943332841 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::ilo_SetConnection21(Pathfinding.Voxels.CompactVoxelSpan&,System.Int32,System.UInt32)
extern "C"  void Voxelize_ilo_SetConnection21_m3028331005 (Il2CppObject * __this /* static, unused */, CompactVoxelSpan_t3431582481 * ____this0, int32_t ___dir1, uint32_t ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Voxelize::ilo_Min22(System.Int32,System.Int32)
extern "C"  int32_t Voxelize_ilo_Min22_m660984437 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Voxels.Voxelize::ilo_FloodRegion23(Pathfinding.Voxels.Voxelize,System.Int32,System.Int32,System.Int32,System.UInt32,System.UInt16,System.UInt16[],System.UInt16[],System.Collections.Generic.List`1<System.Int32>)
extern "C"  bool Voxelize_ilo_FloodRegion23_m2940141655 (Il2CppObject * __this /* static, unused */, Voxelize_t3998270866 * ____this0, int32_t ___x1, int32_t ___z2, int32_t ___i3, uint32_t ___level4, uint16_t ___r5, UInt16U5BU5D_t801649474* ___srcReg6, UInt16U5BU5D_t801649474* ___srcDist7, List_1_t2522024052 * ___stack8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Voxelize::ilo_union_find_find24(System.Int32[],System.Int32)
extern "C"  int32_t Voxelize_ilo_union_find_find24_m3602728024 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___arr0, int32_t ___x1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RelevantGraphSurface Pathfinding.Voxels.Voxelize::ilo_get_Root25()
extern "C"  RelevantGraphSurface_t4201206834 * Voxelize_ilo_get_Root25_m1974092744 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.Voxels.Voxelize::ilo_get_Position26(Pathfinding.RelevantGraphSurface)
extern "C"  Vector3_t4282066566  Voxelize_ilo_get_Position26_m4275266983 (Il2CppObject * __this /* static, unused */, RelevantGraphSurface_t4201206834 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::ilo_VectorToIndex27(Pathfinding.Voxels.Voxelize,UnityEngine.Vector3,System.Int32&,System.Int32&)
extern "C"  void Voxelize_ilo_VectorToIndex27_m3067679200 (Il2CppObject * __this /* static, unused */, Voxelize_t3998270866 * ____this0, Vector3_t4282066566  ___p1, int32_t* ___x2, int32_t* ___z3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Voxelize::ilo_union_find_union28(System.Int32[],System.Int32,System.Int32)
extern "C"  void Voxelize_ilo_union_find_union28_m1734822655 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___arr0, int32_t ___a1, int32_t ___b2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
