﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_keejerelirNufe76
struct M_keejerelirNufe76_t837083409;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_keejerelirNufe76::.ctor()
extern "C"  void M_keejerelirNufe76__ctor_m3748479042 (M_keejerelirNufe76_t837083409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_keejerelirNufe76::M_traikurpis0(System.String[],System.Int32)
extern "C"  void M_keejerelirNufe76_M_traikurpis0_m1294031485 (M_keejerelirNufe76_t837083409 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_keejerelirNufe76::M_someefall1(System.String[],System.Int32)
extern "C"  void M_keejerelirNufe76_M_someefall1_m4178571664 (M_keejerelirNufe76_t837083409 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
