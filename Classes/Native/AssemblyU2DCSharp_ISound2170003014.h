﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISound
struct  ISound_t2170003014  : public Il2CppObject
{
public:
	// System.Boolean ISound::<mute>k__BackingField
	bool ___U3CmuteU3Ek__BackingField_0;
	// System.Single ISound::<volume>k__BackingField
	float ___U3CvolumeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmuteU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ISound_t2170003014, ___U3CmuteU3Ek__BackingField_0)); }
	inline bool get_U3CmuteU3Ek__BackingField_0() const { return ___U3CmuteU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CmuteU3Ek__BackingField_0() { return &___U3CmuteU3Ek__BackingField_0; }
	inline void set_U3CmuteU3Ek__BackingField_0(bool value)
	{
		___U3CmuteU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CvolumeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ISound_t2170003014, ___U3CvolumeU3Ek__BackingField_1)); }
	inline float get_U3CvolumeU3Ek__BackingField_1() const { return ___U3CvolumeU3Ek__BackingField_1; }
	inline float* get_address_of_U3CvolumeU3Ek__BackingField_1() { return &___U3CvolumeU3Ek__BackingField_1; }
	inline void set_U3CvolumeU3Ek__BackingField_1(float value)
	{
		___U3CvolumeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
