﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.Compression.RangeCoder.Decoder
struct Decoder_t1102840654;
// SevenZip.Compression.LZMA.Decoder/LiteralDecoder/Decoder2
struct Decoder2_t813340627;
struct Decoder2_t813340627_marshaled_pinvoke;
struct Decoder2_t813340627_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Decoder813340627.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_1102840654.h"

// System.Void SevenZip.Compression.LZMA.Decoder/LiteralDecoder/Decoder2::Create()
extern "C"  void Decoder2_Create_m2438016648 (Decoder2_t813340627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Decoder/LiteralDecoder/Decoder2::Init()
extern "C"  void Decoder2_Init_m2664604764 (Decoder2_t813340627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte SevenZip.Compression.LZMA.Decoder/LiteralDecoder/Decoder2::DecodeNormal(SevenZip.Compression.RangeCoder.Decoder)
extern "C"  uint8_t Decoder2_DecodeNormal_m1725672637 (Decoder2_t813340627 * __this, Decoder_t1102840654 * ___rangeDecoder0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte SevenZip.Compression.LZMA.Decoder/LiteralDecoder/Decoder2::DecodeWithMatchByte(SevenZip.Compression.RangeCoder.Decoder,System.Byte)
extern "C"  uint8_t Decoder2_DecodeWithMatchByte_m3399582076 (Decoder2_t813340627 * __this, Decoder_t1102840654 * ___rangeDecoder0, uint8_t ___matchByte1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Decoder2_t813340627;
struct Decoder2_t813340627_marshaled_pinvoke;

extern "C" void Decoder2_t813340627_marshal_pinvoke(const Decoder2_t813340627& unmarshaled, Decoder2_t813340627_marshaled_pinvoke& marshaled);
extern "C" void Decoder2_t813340627_marshal_pinvoke_back(const Decoder2_t813340627_marshaled_pinvoke& marshaled, Decoder2_t813340627& unmarshaled);
extern "C" void Decoder2_t813340627_marshal_pinvoke_cleanup(Decoder2_t813340627_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Decoder2_t813340627;
struct Decoder2_t813340627_marshaled_com;

extern "C" void Decoder2_t813340627_marshal_com(const Decoder2_t813340627& unmarshaled, Decoder2_t813340627_marshaled_com& marshaled);
extern "C" void Decoder2_t813340627_marshal_com_back(const Decoder2_t813340627_marshaled_com& marshaled, Decoder2_t813340627& unmarshaled);
extern "C" void Decoder2_t813340627_marshal_com_cleanup(Decoder2_t813340627_marshaled_com& marshaled);
