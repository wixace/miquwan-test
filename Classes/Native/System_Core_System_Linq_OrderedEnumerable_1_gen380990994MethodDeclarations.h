﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.OrderedEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct OrderedEnumerable_1_t380990994;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t950614638;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t3856534026;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.OrderedEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
extern "C"  void OrderedEnumerable_1__ctor_m2973763826_gshared (OrderedEnumerable_1_t380990994 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define OrderedEnumerable_1__ctor_m2973763826(__this, ___source0, method) ((  void (*) (OrderedEnumerable_1_t380990994 *, Il2CppObject*, const MethodInfo*))OrderedEnumerable_1__ctor_m2973763826_gshared)(__this, ___source0, method)
// System.Collections.IEnumerator System.Linq.OrderedEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * OrderedEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m301580322_gshared (OrderedEnumerable_1_t380990994 * __this, const MethodInfo* method);
#define OrderedEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m301580322(__this, method) ((  Il2CppObject * (*) (OrderedEnumerable_1_t380990994 *, const MethodInfo*))OrderedEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m301580322_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator()
extern "C"  Il2CppObject* OrderedEnumerable_1_GetEnumerator_m1014223019_gshared (OrderedEnumerable_1_t380990994 * __this, const MethodInfo* method);
#define OrderedEnumerable_1_GetEnumerator_m1014223019(__this, method) ((  Il2CppObject* (*) (OrderedEnumerable_1_t380990994 *, const MethodInfo*))OrderedEnumerable_1_GetEnumerator_m1014223019_gshared)(__this, method)
