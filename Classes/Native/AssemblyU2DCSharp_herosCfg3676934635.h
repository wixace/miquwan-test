﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// herosCfg
struct  herosCfg_t3676934635  : public CsCfgBase_t69924517
{
public:
	// System.Int32 herosCfg::id
	int32_t ___id_0;
	// System.Int32 herosCfg::country
	int32_t ___country_1;
	// System.Int32 herosCfg::sex
	int32_t ___sex_2;
	// System.Int32 herosCfg::quatily
	int32_t ___quatily_3;
	// System.Int32 herosCfg::intelligence
	int32_t ___intelligence_4;
	// System.String herosCfg::name
	String_t* ___name_5;
	// System.Int32 herosCfg::unadd
	int32_t ___unadd_6;
	// System.String herosCfg::icon
	String_t* ___icon_7;
	// System.String herosCfg::largeIcon
	String_t* ___largeIcon_8;
	// System.String herosCfg::headicon
	String_t* ___headicon_9;
	// System.String herosCfg::heroshow
	String_t* ___heroshow_10;
	// System.Single herosCfg::halfwidth
	float ___halfwidth_11;
	// System.Int32 herosCfg::attack_nature
	int32_t ___attack_nature_12;
	// System.Int32 herosCfg::attack
	int32_t ___attack_13;
	// System.Int32 herosCfg::phy_def
	int32_t ___phy_def_14;
	// System.Int32 herosCfg::mag_def
	int32_t ___mag_def_15;
	// System.Int32 herosCfg::hp
	int32_t ___hp_16;
	// System.Int32 herosCfg::hitrate
	int32_t ___hitrate_17;
	// System.Int32 herosCfg::dodge
	int32_t ___dodge_18;
	// System.Int32 herosCfg::destory
	int32_t ___destory_19;
	// System.Int32 herosCfg::block
	int32_t ___block_20;
	// System.Int32 herosCfg::crit
	int32_t ___crit_21;
	// System.Int32 herosCfg::crit_hurt
	int32_t ___crit_hurt_22;
	// System.Int32 herosCfg::anticrit
	int32_t ___anticrit_23;
	// System.Int32 herosCfg::damage_reflect
	int32_t ___damage_reflect_24;
	// System.Int32 herosCfg::heal_bonus
	int32_t ___heal_bonus_25;
	// System.Int32 herosCfg::damage_bonus
	int32_t ___damage_bonus_26;
	// System.Int32 herosCfg::damage_reduce
	int32_t ___damage_reduce_27;
	// System.Int32 herosCfg::real_damage
	int32_t ___real_damage_28;
	// System.Int32 herosCfg::firm
	int32_t ___firm_29;
	// System.Int32 herosCfg::pure
	int32_t ___pure_30;
	// System.Int32 herosCfg::stubborn
	int32_t ___stubborn_31;
	// System.Int32 herosCfg::star_level
	int32_t ___star_level_32;
	// System.Int32 herosCfg::anger_initial
	int32_t ___anger_initial_33;
	// System.Int32 herosCfg::anger_max
	int32_t ___anger_max_34;
	// System.Int32 herosCfg::anger_growth
	int32_t ___anger_growth_35;
	// System.Int32 herosCfg::behurtanger
	int32_t ___behurtanger_36;
	// System.Int32 herosCfg::element
	int32_t ___element_37;
	// System.Int32 herosCfg::normal_element
	int32_t ___normal_element_38;
	// System.Int32 herosCfg::super_element
	int32_t ___super_element_39;
	// System.Single herosCfg::atkrange
	float ___atkrange_40;
	// System.Single herosCfg::movespeed
	float ___movespeed_41;
	// System.Int32 herosCfg::atktype
	int32_t ___atktype_42;
	// System.Int32 herosCfg::heromajor
	int32_t ___heromajor_43;
	// System.Single herosCfg::atkeffdelaytime
	float ___atkeffdelaytime_44;
	// System.Int32 herosCfg::killed_skill
	int32_t ___killed_skill_45;
	// System.Int32 herosCfg::leader_skill
	int32_t ___leader_skill_46;
	// System.Int32 herosCfg::normal_skill
	int32_t ___normal_skill_47;
	// System.String herosCfg::skills
	String_t* ___skills_48;
	// System.Int32 herosCfg::EffectHide
	int32_t ___EffectHide_49;
	// System.Single herosCfg::lucencyWait
	float ___lucencyWait_50;
	// System.Int32 herosCfg::islucency
	int32_t ___islucency_51;
	// System.Single herosCfg::lucencytime
	float ___lucencytime_52;
	// System.String herosCfg::offsetPos
	String_t* ___offsetPos_53;
	// System.String herosCfg::offsetRob
	String_t* ___offsetRob_54;
	// System.Single herosCfg::Shadow_size
	float ___Shadow_size_55;
	// System.Single herosCfg::Shadow_hover
	float ___Shadow_hover_56;
	// System.String herosCfg::AIID
	String_t* ___AIID_57;
	// System.Int32 herosCfg::next_ship
	int32_t ___next_ship_58;
	// System.Int32 herosCfg::piece_call
	int32_t ___piece_call_59;
	// System.Int32 herosCfg::piece_change
	int32_t ___piece_change_60;
	// System.Int32 herosCfg::idleSoundID
	int32_t ___idleSoundID_61;
	// System.Int32 herosCfg::winSoundID
	int32_t ___winSoundID_62;
	// System.Int32 herosCfg::attackSoundID
	int32_t ___attackSoundID_63;
	// System.Int32 herosCfg::Tack001SoundID
	int32_t ___Tack001SoundID_64;
	// System.Int32 herosCfg::ifattacked
	int32_t ___ifattacked_65;
	// System.String herosCfg::storyID
	String_t* ___storyID_66;
	// System.Int32 herosCfg::ifeffects
	int32_t ___ifeffects_67;
	// System.String herosCfg::withEage
	String_t* ___withEage_68;
	// System.String herosCfg::resume
	String_t* ___resume_69;
	// System.String herosCfg::tipresume
	String_t* ___tipresume_70;
	// System.String herosCfg::rgb
	String_t* ___rgb_71;
	// System.String herosCfg::showrgb
	String_t* ___showrgb_72;
	// System.Single herosCfg::rim
	float ___rim_73;
	// System.Single herosCfg::findEnemyRange
	float ___findEnemyRange_74;
	// System.Int32 herosCfg::quatilyforshow
	int32_t ___quatilyforshow_75;
	// System.Int32 herosCfg::effect_leader
	int32_t ___effect_leader_76;
	// System.Int32 herosCfg::line
	int32_t ___line_77;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_country_1() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___country_1)); }
	inline int32_t get_country_1() const { return ___country_1; }
	inline int32_t* get_address_of_country_1() { return &___country_1; }
	inline void set_country_1(int32_t value)
	{
		___country_1 = value;
	}

	inline static int32_t get_offset_of_sex_2() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___sex_2)); }
	inline int32_t get_sex_2() const { return ___sex_2; }
	inline int32_t* get_address_of_sex_2() { return &___sex_2; }
	inline void set_sex_2(int32_t value)
	{
		___sex_2 = value;
	}

	inline static int32_t get_offset_of_quatily_3() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___quatily_3)); }
	inline int32_t get_quatily_3() const { return ___quatily_3; }
	inline int32_t* get_address_of_quatily_3() { return &___quatily_3; }
	inline void set_quatily_3(int32_t value)
	{
		___quatily_3 = value;
	}

	inline static int32_t get_offset_of_intelligence_4() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___intelligence_4)); }
	inline int32_t get_intelligence_4() const { return ___intelligence_4; }
	inline int32_t* get_address_of_intelligence_4() { return &___intelligence_4; }
	inline void set_intelligence_4(int32_t value)
	{
		___intelligence_4 = value;
	}

	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier(&___name_5, value);
	}

	inline static int32_t get_offset_of_unadd_6() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___unadd_6)); }
	inline int32_t get_unadd_6() const { return ___unadd_6; }
	inline int32_t* get_address_of_unadd_6() { return &___unadd_6; }
	inline void set_unadd_6(int32_t value)
	{
		___unadd_6 = value;
	}

	inline static int32_t get_offset_of_icon_7() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___icon_7)); }
	inline String_t* get_icon_7() const { return ___icon_7; }
	inline String_t** get_address_of_icon_7() { return &___icon_7; }
	inline void set_icon_7(String_t* value)
	{
		___icon_7 = value;
		Il2CppCodeGenWriteBarrier(&___icon_7, value);
	}

	inline static int32_t get_offset_of_largeIcon_8() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___largeIcon_8)); }
	inline String_t* get_largeIcon_8() const { return ___largeIcon_8; }
	inline String_t** get_address_of_largeIcon_8() { return &___largeIcon_8; }
	inline void set_largeIcon_8(String_t* value)
	{
		___largeIcon_8 = value;
		Il2CppCodeGenWriteBarrier(&___largeIcon_8, value);
	}

	inline static int32_t get_offset_of_headicon_9() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___headicon_9)); }
	inline String_t* get_headicon_9() const { return ___headicon_9; }
	inline String_t** get_address_of_headicon_9() { return &___headicon_9; }
	inline void set_headicon_9(String_t* value)
	{
		___headicon_9 = value;
		Il2CppCodeGenWriteBarrier(&___headicon_9, value);
	}

	inline static int32_t get_offset_of_heroshow_10() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___heroshow_10)); }
	inline String_t* get_heroshow_10() const { return ___heroshow_10; }
	inline String_t** get_address_of_heroshow_10() { return &___heroshow_10; }
	inline void set_heroshow_10(String_t* value)
	{
		___heroshow_10 = value;
		Il2CppCodeGenWriteBarrier(&___heroshow_10, value);
	}

	inline static int32_t get_offset_of_halfwidth_11() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___halfwidth_11)); }
	inline float get_halfwidth_11() const { return ___halfwidth_11; }
	inline float* get_address_of_halfwidth_11() { return &___halfwidth_11; }
	inline void set_halfwidth_11(float value)
	{
		___halfwidth_11 = value;
	}

	inline static int32_t get_offset_of_attack_nature_12() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___attack_nature_12)); }
	inline int32_t get_attack_nature_12() const { return ___attack_nature_12; }
	inline int32_t* get_address_of_attack_nature_12() { return &___attack_nature_12; }
	inline void set_attack_nature_12(int32_t value)
	{
		___attack_nature_12 = value;
	}

	inline static int32_t get_offset_of_attack_13() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___attack_13)); }
	inline int32_t get_attack_13() const { return ___attack_13; }
	inline int32_t* get_address_of_attack_13() { return &___attack_13; }
	inline void set_attack_13(int32_t value)
	{
		___attack_13 = value;
	}

	inline static int32_t get_offset_of_phy_def_14() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___phy_def_14)); }
	inline int32_t get_phy_def_14() const { return ___phy_def_14; }
	inline int32_t* get_address_of_phy_def_14() { return &___phy_def_14; }
	inline void set_phy_def_14(int32_t value)
	{
		___phy_def_14 = value;
	}

	inline static int32_t get_offset_of_mag_def_15() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___mag_def_15)); }
	inline int32_t get_mag_def_15() const { return ___mag_def_15; }
	inline int32_t* get_address_of_mag_def_15() { return &___mag_def_15; }
	inline void set_mag_def_15(int32_t value)
	{
		___mag_def_15 = value;
	}

	inline static int32_t get_offset_of_hp_16() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___hp_16)); }
	inline int32_t get_hp_16() const { return ___hp_16; }
	inline int32_t* get_address_of_hp_16() { return &___hp_16; }
	inline void set_hp_16(int32_t value)
	{
		___hp_16 = value;
	}

	inline static int32_t get_offset_of_hitrate_17() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___hitrate_17)); }
	inline int32_t get_hitrate_17() const { return ___hitrate_17; }
	inline int32_t* get_address_of_hitrate_17() { return &___hitrate_17; }
	inline void set_hitrate_17(int32_t value)
	{
		___hitrate_17 = value;
	}

	inline static int32_t get_offset_of_dodge_18() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___dodge_18)); }
	inline int32_t get_dodge_18() const { return ___dodge_18; }
	inline int32_t* get_address_of_dodge_18() { return &___dodge_18; }
	inline void set_dodge_18(int32_t value)
	{
		___dodge_18 = value;
	}

	inline static int32_t get_offset_of_destory_19() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___destory_19)); }
	inline int32_t get_destory_19() const { return ___destory_19; }
	inline int32_t* get_address_of_destory_19() { return &___destory_19; }
	inline void set_destory_19(int32_t value)
	{
		___destory_19 = value;
	}

	inline static int32_t get_offset_of_block_20() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___block_20)); }
	inline int32_t get_block_20() const { return ___block_20; }
	inline int32_t* get_address_of_block_20() { return &___block_20; }
	inline void set_block_20(int32_t value)
	{
		___block_20 = value;
	}

	inline static int32_t get_offset_of_crit_21() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___crit_21)); }
	inline int32_t get_crit_21() const { return ___crit_21; }
	inline int32_t* get_address_of_crit_21() { return &___crit_21; }
	inline void set_crit_21(int32_t value)
	{
		___crit_21 = value;
	}

	inline static int32_t get_offset_of_crit_hurt_22() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___crit_hurt_22)); }
	inline int32_t get_crit_hurt_22() const { return ___crit_hurt_22; }
	inline int32_t* get_address_of_crit_hurt_22() { return &___crit_hurt_22; }
	inline void set_crit_hurt_22(int32_t value)
	{
		___crit_hurt_22 = value;
	}

	inline static int32_t get_offset_of_anticrit_23() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___anticrit_23)); }
	inline int32_t get_anticrit_23() const { return ___anticrit_23; }
	inline int32_t* get_address_of_anticrit_23() { return &___anticrit_23; }
	inline void set_anticrit_23(int32_t value)
	{
		___anticrit_23 = value;
	}

	inline static int32_t get_offset_of_damage_reflect_24() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___damage_reflect_24)); }
	inline int32_t get_damage_reflect_24() const { return ___damage_reflect_24; }
	inline int32_t* get_address_of_damage_reflect_24() { return &___damage_reflect_24; }
	inline void set_damage_reflect_24(int32_t value)
	{
		___damage_reflect_24 = value;
	}

	inline static int32_t get_offset_of_heal_bonus_25() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___heal_bonus_25)); }
	inline int32_t get_heal_bonus_25() const { return ___heal_bonus_25; }
	inline int32_t* get_address_of_heal_bonus_25() { return &___heal_bonus_25; }
	inline void set_heal_bonus_25(int32_t value)
	{
		___heal_bonus_25 = value;
	}

	inline static int32_t get_offset_of_damage_bonus_26() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___damage_bonus_26)); }
	inline int32_t get_damage_bonus_26() const { return ___damage_bonus_26; }
	inline int32_t* get_address_of_damage_bonus_26() { return &___damage_bonus_26; }
	inline void set_damage_bonus_26(int32_t value)
	{
		___damage_bonus_26 = value;
	}

	inline static int32_t get_offset_of_damage_reduce_27() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___damage_reduce_27)); }
	inline int32_t get_damage_reduce_27() const { return ___damage_reduce_27; }
	inline int32_t* get_address_of_damage_reduce_27() { return &___damage_reduce_27; }
	inline void set_damage_reduce_27(int32_t value)
	{
		___damage_reduce_27 = value;
	}

	inline static int32_t get_offset_of_real_damage_28() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___real_damage_28)); }
	inline int32_t get_real_damage_28() const { return ___real_damage_28; }
	inline int32_t* get_address_of_real_damage_28() { return &___real_damage_28; }
	inline void set_real_damage_28(int32_t value)
	{
		___real_damage_28 = value;
	}

	inline static int32_t get_offset_of_firm_29() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___firm_29)); }
	inline int32_t get_firm_29() const { return ___firm_29; }
	inline int32_t* get_address_of_firm_29() { return &___firm_29; }
	inline void set_firm_29(int32_t value)
	{
		___firm_29 = value;
	}

	inline static int32_t get_offset_of_pure_30() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___pure_30)); }
	inline int32_t get_pure_30() const { return ___pure_30; }
	inline int32_t* get_address_of_pure_30() { return &___pure_30; }
	inline void set_pure_30(int32_t value)
	{
		___pure_30 = value;
	}

	inline static int32_t get_offset_of_stubborn_31() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___stubborn_31)); }
	inline int32_t get_stubborn_31() const { return ___stubborn_31; }
	inline int32_t* get_address_of_stubborn_31() { return &___stubborn_31; }
	inline void set_stubborn_31(int32_t value)
	{
		___stubborn_31 = value;
	}

	inline static int32_t get_offset_of_star_level_32() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___star_level_32)); }
	inline int32_t get_star_level_32() const { return ___star_level_32; }
	inline int32_t* get_address_of_star_level_32() { return &___star_level_32; }
	inline void set_star_level_32(int32_t value)
	{
		___star_level_32 = value;
	}

	inline static int32_t get_offset_of_anger_initial_33() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___anger_initial_33)); }
	inline int32_t get_anger_initial_33() const { return ___anger_initial_33; }
	inline int32_t* get_address_of_anger_initial_33() { return &___anger_initial_33; }
	inline void set_anger_initial_33(int32_t value)
	{
		___anger_initial_33 = value;
	}

	inline static int32_t get_offset_of_anger_max_34() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___anger_max_34)); }
	inline int32_t get_anger_max_34() const { return ___anger_max_34; }
	inline int32_t* get_address_of_anger_max_34() { return &___anger_max_34; }
	inline void set_anger_max_34(int32_t value)
	{
		___anger_max_34 = value;
	}

	inline static int32_t get_offset_of_anger_growth_35() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___anger_growth_35)); }
	inline int32_t get_anger_growth_35() const { return ___anger_growth_35; }
	inline int32_t* get_address_of_anger_growth_35() { return &___anger_growth_35; }
	inline void set_anger_growth_35(int32_t value)
	{
		___anger_growth_35 = value;
	}

	inline static int32_t get_offset_of_behurtanger_36() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___behurtanger_36)); }
	inline int32_t get_behurtanger_36() const { return ___behurtanger_36; }
	inline int32_t* get_address_of_behurtanger_36() { return &___behurtanger_36; }
	inline void set_behurtanger_36(int32_t value)
	{
		___behurtanger_36 = value;
	}

	inline static int32_t get_offset_of_element_37() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___element_37)); }
	inline int32_t get_element_37() const { return ___element_37; }
	inline int32_t* get_address_of_element_37() { return &___element_37; }
	inline void set_element_37(int32_t value)
	{
		___element_37 = value;
	}

	inline static int32_t get_offset_of_normal_element_38() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___normal_element_38)); }
	inline int32_t get_normal_element_38() const { return ___normal_element_38; }
	inline int32_t* get_address_of_normal_element_38() { return &___normal_element_38; }
	inline void set_normal_element_38(int32_t value)
	{
		___normal_element_38 = value;
	}

	inline static int32_t get_offset_of_super_element_39() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___super_element_39)); }
	inline int32_t get_super_element_39() const { return ___super_element_39; }
	inline int32_t* get_address_of_super_element_39() { return &___super_element_39; }
	inline void set_super_element_39(int32_t value)
	{
		___super_element_39 = value;
	}

	inline static int32_t get_offset_of_atkrange_40() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___atkrange_40)); }
	inline float get_atkrange_40() const { return ___atkrange_40; }
	inline float* get_address_of_atkrange_40() { return &___atkrange_40; }
	inline void set_atkrange_40(float value)
	{
		___atkrange_40 = value;
	}

	inline static int32_t get_offset_of_movespeed_41() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___movespeed_41)); }
	inline float get_movespeed_41() const { return ___movespeed_41; }
	inline float* get_address_of_movespeed_41() { return &___movespeed_41; }
	inline void set_movespeed_41(float value)
	{
		___movespeed_41 = value;
	}

	inline static int32_t get_offset_of_atktype_42() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___atktype_42)); }
	inline int32_t get_atktype_42() const { return ___atktype_42; }
	inline int32_t* get_address_of_atktype_42() { return &___atktype_42; }
	inline void set_atktype_42(int32_t value)
	{
		___atktype_42 = value;
	}

	inline static int32_t get_offset_of_heromajor_43() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___heromajor_43)); }
	inline int32_t get_heromajor_43() const { return ___heromajor_43; }
	inline int32_t* get_address_of_heromajor_43() { return &___heromajor_43; }
	inline void set_heromajor_43(int32_t value)
	{
		___heromajor_43 = value;
	}

	inline static int32_t get_offset_of_atkeffdelaytime_44() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___atkeffdelaytime_44)); }
	inline float get_atkeffdelaytime_44() const { return ___atkeffdelaytime_44; }
	inline float* get_address_of_atkeffdelaytime_44() { return &___atkeffdelaytime_44; }
	inline void set_atkeffdelaytime_44(float value)
	{
		___atkeffdelaytime_44 = value;
	}

	inline static int32_t get_offset_of_killed_skill_45() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___killed_skill_45)); }
	inline int32_t get_killed_skill_45() const { return ___killed_skill_45; }
	inline int32_t* get_address_of_killed_skill_45() { return &___killed_skill_45; }
	inline void set_killed_skill_45(int32_t value)
	{
		___killed_skill_45 = value;
	}

	inline static int32_t get_offset_of_leader_skill_46() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___leader_skill_46)); }
	inline int32_t get_leader_skill_46() const { return ___leader_skill_46; }
	inline int32_t* get_address_of_leader_skill_46() { return &___leader_skill_46; }
	inline void set_leader_skill_46(int32_t value)
	{
		___leader_skill_46 = value;
	}

	inline static int32_t get_offset_of_normal_skill_47() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___normal_skill_47)); }
	inline int32_t get_normal_skill_47() const { return ___normal_skill_47; }
	inline int32_t* get_address_of_normal_skill_47() { return &___normal_skill_47; }
	inline void set_normal_skill_47(int32_t value)
	{
		___normal_skill_47 = value;
	}

	inline static int32_t get_offset_of_skills_48() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___skills_48)); }
	inline String_t* get_skills_48() const { return ___skills_48; }
	inline String_t** get_address_of_skills_48() { return &___skills_48; }
	inline void set_skills_48(String_t* value)
	{
		___skills_48 = value;
		Il2CppCodeGenWriteBarrier(&___skills_48, value);
	}

	inline static int32_t get_offset_of_EffectHide_49() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___EffectHide_49)); }
	inline int32_t get_EffectHide_49() const { return ___EffectHide_49; }
	inline int32_t* get_address_of_EffectHide_49() { return &___EffectHide_49; }
	inline void set_EffectHide_49(int32_t value)
	{
		___EffectHide_49 = value;
	}

	inline static int32_t get_offset_of_lucencyWait_50() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___lucencyWait_50)); }
	inline float get_lucencyWait_50() const { return ___lucencyWait_50; }
	inline float* get_address_of_lucencyWait_50() { return &___lucencyWait_50; }
	inline void set_lucencyWait_50(float value)
	{
		___lucencyWait_50 = value;
	}

	inline static int32_t get_offset_of_islucency_51() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___islucency_51)); }
	inline int32_t get_islucency_51() const { return ___islucency_51; }
	inline int32_t* get_address_of_islucency_51() { return &___islucency_51; }
	inline void set_islucency_51(int32_t value)
	{
		___islucency_51 = value;
	}

	inline static int32_t get_offset_of_lucencytime_52() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___lucencytime_52)); }
	inline float get_lucencytime_52() const { return ___lucencytime_52; }
	inline float* get_address_of_lucencytime_52() { return &___lucencytime_52; }
	inline void set_lucencytime_52(float value)
	{
		___lucencytime_52 = value;
	}

	inline static int32_t get_offset_of_offsetPos_53() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___offsetPos_53)); }
	inline String_t* get_offsetPos_53() const { return ___offsetPos_53; }
	inline String_t** get_address_of_offsetPos_53() { return &___offsetPos_53; }
	inline void set_offsetPos_53(String_t* value)
	{
		___offsetPos_53 = value;
		Il2CppCodeGenWriteBarrier(&___offsetPos_53, value);
	}

	inline static int32_t get_offset_of_offsetRob_54() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___offsetRob_54)); }
	inline String_t* get_offsetRob_54() const { return ___offsetRob_54; }
	inline String_t** get_address_of_offsetRob_54() { return &___offsetRob_54; }
	inline void set_offsetRob_54(String_t* value)
	{
		___offsetRob_54 = value;
		Il2CppCodeGenWriteBarrier(&___offsetRob_54, value);
	}

	inline static int32_t get_offset_of_Shadow_size_55() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___Shadow_size_55)); }
	inline float get_Shadow_size_55() const { return ___Shadow_size_55; }
	inline float* get_address_of_Shadow_size_55() { return &___Shadow_size_55; }
	inline void set_Shadow_size_55(float value)
	{
		___Shadow_size_55 = value;
	}

	inline static int32_t get_offset_of_Shadow_hover_56() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___Shadow_hover_56)); }
	inline float get_Shadow_hover_56() const { return ___Shadow_hover_56; }
	inline float* get_address_of_Shadow_hover_56() { return &___Shadow_hover_56; }
	inline void set_Shadow_hover_56(float value)
	{
		___Shadow_hover_56 = value;
	}

	inline static int32_t get_offset_of_AIID_57() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___AIID_57)); }
	inline String_t* get_AIID_57() const { return ___AIID_57; }
	inline String_t** get_address_of_AIID_57() { return &___AIID_57; }
	inline void set_AIID_57(String_t* value)
	{
		___AIID_57 = value;
		Il2CppCodeGenWriteBarrier(&___AIID_57, value);
	}

	inline static int32_t get_offset_of_next_ship_58() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___next_ship_58)); }
	inline int32_t get_next_ship_58() const { return ___next_ship_58; }
	inline int32_t* get_address_of_next_ship_58() { return &___next_ship_58; }
	inline void set_next_ship_58(int32_t value)
	{
		___next_ship_58 = value;
	}

	inline static int32_t get_offset_of_piece_call_59() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___piece_call_59)); }
	inline int32_t get_piece_call_59() const { return ___piece_call_59; }
	inline int32_t* get_address_of_piece_call_59() { return &___piece_call_59; }
	inline void set_piece_call_59(int32_t value)
	{
		___piece_call_59 = value;
	}

	inline static int32_t get_offset_of_piece_change_60() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___piece_change_60)); }
	inline int32_t get_piece_change_60() const { return ___piece_change_60; }
	inline int32_t* get_address_of_piece_change_60() { return &___piece_change_60; }
	inline void set_piece_change_60(int32_t value)
	{
		___piece_change_60 = value;
	}

	inline static int32_t get_offset_of_idleSoundID_61() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___idleSoundID_61)); }
	inline int32_t get_idleSoundID_61() const { return ___idleSoundID_61; }
	inline int32_t* get_address_of_idleSoundID_61() { return &___idleSoundID_61; }
	inline void set_idleSoundID_61(int32_t value)
	{
		___idleSoundID_61 = value;
	}

	inline static int32_t get_offset_of_winSoundID_62() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___winSoundID_62)); }
	inline int32_t get_winSoundID_62() const { return ___winSoundID_62; }
	inline int32_t* get_address_of_winSoundID_62() { return &___winSoundID_62; }
	inline void set_winSoundID_62(int32_t value)
	{
		___winSoundID_62 = value;
	}

	inline static int32_t get_offset_of_attackSoundID_63() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___attackSoundID_63)); }
	inline int32_t get_attackSoundID_63() const { return ___attackSoundID_63; }
	inline int32_t* get_address_of_attackSoundID_63() { return &___attackSoundID_63; }
	inline void set_attackSoundID_63(int32_t value)
	{
		___attackSoundID_63 = value;
	}

	inline static int32_t get_offset_of_Tack001SoundID_64() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___Tack001SoundID_64)); }
	inline int32_t get_Tack001SoundID_64() const { return ___Tack001SoundID_64; }
	inline int32_t* get_address_of_Tack001SoundID_64() { return &___Tack001SoundID_64; }
	inline void set_Tack001SoundID_64(int32_t value)
	{
		___Tack001SoundID_64 = value;
	}

	inline static int32_t get_offset_of_ifattacked_65() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___ifattacked_65)); }
	inline int32_t get_ifattacked_65() const { return ___ifattacked_65; }
	inline int32_t* get_address_of_ifattacked_65() { return &___ifattacked_65; }
	inline void set_ifattacked_65(int32_t value)
	{
		___ifattacked_65 = value;
	}

	inline static int32_t get_offset_of_storyID_66() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___storyID_66)); }
	inline String_t* get_storyID_66() const { return ___storyID_66; }
	inline String_t** get_address_of_storyID_66() { return &___storyID_66; }
	inline void set_storyID_66(String_t* value)
	{
		___storyID_66 = value;
		Il2CppCodeGenWriteBarrier(&___storyID_66, value);
	}

	inline static int32_t get_offset_of_ifeffects_67() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___ifeffects_67)); }
	inline int32_t get_ifeffects_67() const { return ___ifeffects_67; }
	inline int32_t* get_address_of_ifeffects_67() { return &___ifeffects_67; }
	inline void set_ifeffects_67(int32_t value)
	{
		___ifeffects_67 = value;
	}

	inline static int32_t get_offset_of_withEage_68() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___withEage_68)); }
	inline String_t* get_withEage_68() const { return ___withEage_68; }
	inline String_t** get_address_of_withEage_68() { return &___withEage_68; }
	inline void set_withEage_68(String_t* value)
	{
		___withEage_68 = value;
		Il2CppCodeGenWriteBarrier(&___withEage_68, value);
	}

	inline static int32_t get_offset_of_resume_69() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___resume_69)); }
	inline String_t* get_resume_69() const { return ___resume_69; }
	inline String_t** get_address_of_resume_69() { return &___resume_69; }
	inline void set_resume_69(String_t* value)
	{
		___resume_69 = value;
		Il2CppCodeGenWriteBarrier(&___resume_69, value);
	}

	inline static int32_t get_offset_of_tipresume_70() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___tipresume_70)); }
	inline String_t* get_tipresume_70() const { return ___tipresume_70; }
	inline String_t** get_address_of_tipresume_70() { return &___tipresume_70; }
	inline void set_tipresume_70(String_t* value)
	{
		___tipresume_70 = value;
		Il2CppCodeGenWriteBarrier(&___tipresume_70, value);
	}

	inline static int32_t get_offset_of_rgb_71() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___rgb_71)); }
	inline String_t* get_rgb_71() const { return ___rgb_71; }
	inline String_t** get_address_of_rgb_71() { return &___rgb_71; }
	inline void set_rgb_71(String_t* value)
	{
		___rgb_71 = value;
		Il2CppCodeGenWriteBarrier(&___rgb_71, value);
	}

	inline static int32_t get_offset_of_showrgb_72() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___showrgb_72)); }
	inline String_t* get_showrgb_72() const { return ___showrgb_72; }
	inline String_t** get_address_of_showrgb_72() { return &___showrgb_72; }
	inline void set_showrgb_72(String_t* value)
	{
		___showrgb_72 = value;
		Il2CppCodeGenWriteBarrier(&___showrgb_72, value);
	}

	inline static int32_t get_offset_of_rim_73() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___rim_73)); }
	inline float get_rim_73() const { return ___rim_73; }
	inline float* get_address_of_rim_73() { return &___rim_73; }
	inline void set_rim_73(float value)
	{
		___rim_73 = value;
	}

	inline static int32_t get_offset_of_findEnemyRange_74() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___findEnemyRange_74)); }
	inline float get_findEnemyRange_74() const { return ___findEnemyRange_74; }
	inline float* get_address_of_findEnemyRange_74() { return &___findEnemyRange_74; }
	inline void set_findEnemyRange_74(float value)
	{
		___findEnemyRange_74 = value;
	}

	inline static int32_t get_offset_of_quatilyforshow_75() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___quatilyforshow_75)); }
	inline int32_t get_quatilyforshow_75() const { return ___quatilyforshow_75; }
	inline int32_t* get_address_of_quatilyforshow_75() { return &___quatilyforshow_75; }
	inline void set_quatilyforshow_75(int32_t value)
	{
		___quatilyforshow_75 = value;
	}

	inline static int32_t get_offset_of_effect_leader_76() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___effect_leader_76)); }
	inline int32_t get_effect_leader_76() const { return ___effect_leader_76; }
	inline int32_t* get_address_of_effect_leader_76() { return &___effect_leader_76; }
	inline void set_effect_leader_76(int32_t value)
	{
		___effect_leader_76 = value;
	}

	inline static int32_t get_offset_of_line_77() { return static_cast<int32_t>(offsetof(herosCfg_t3676934635, ___line_77)); }
	inline int32_t get_line_77() const { return ___line_77; }
	inline int32_t* get_address_of_line_77() { return &___line_77; }
	inline void set_line_77(int32_t value)
	{
		___line_77 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
