﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginAiWan
struct  PluginAiWan_t2537276937  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginAiWan::cardid
	String_t* ___cardid_2;
	// System.String PluginAiWan::guest
	String_t* ___guest_3;
	// System.String PluginAiWan::phone
	String_t* ___phone_4;
	// System.String PluginAiWan::token
	String_t* ___token_5;
	// System.String PluginAiWan::userid
	String_t* ___userid_6;
	// System.String PluginAiWan::userNumId
	String_t* ___userNumId_7;
	// System.String[] PluginAiWan::sdkInfo
	StringU5BU5D_t4054002952* ___sdkInfo_8;

public:
	inline static int32_t get_offset_of_cardid_2() { return static_cast<int32_t>(offsetof(PluginAiWan_t2537276937, ___cardid_2)); }
	inline String_t* get_cardid_2() const { return ___cardid_2; }
	inline String_t** get_address_of_cardid_2() { return &___cardid_2; }
	inline void set_cardid_2(String_t* value)
	{
		___cardid_2 = value;
		Il2CppCodeGenWriteBarrier(&___cardid_2, value);
	}

	inline static int32_t get_offset_of_guest_3() { return static_cast<int32_t>(offsetof(PluginAiWan_t2537276937, ___guest_3)); }
	inline String_t* get_guest_3() const { return ___guest_3; }
	inline String_t** get_address_of_guest_3() { return &___guest_3; }
	inline void set_guest_3(String_t* value)
	{
		___guest_3 = value;
		Il2CppCodeGenWriteBarrier(&___guest_3, value);
	}

	inline static int32_t get_offset_of_phone_4() { return static_cast<int32_t>(offsetof(PluginAiWan_t2537276937, ___phone_4)); }
	inline String_t* get_phone_4() const { return ___phone_4; }
	inline String_t** get_address_of_phone_4() { return &___phone_4; }
	inline void set_phone_4(String_t* value)
	{
		___phone_4 = value;
		Il2CppCodeGenWriteBarrier(&___phone_4, value);
	}

	inline static int32_t get_offset_of_token_5() { return static_cast<int32_t>(offsetof(PluginAiWan_t2537276937, ___token_5)); }
	inline String_t* get_token_5() const { return ___token_5; }
	inline String_t** get_address_of_token_5() { return &___token_5; }
	inline void set_token_5(String_t* value)
	{
		___token_5 = value;
		Il2CppCodeGenWriteBarrier(&___token_5, value);
	}

	inline static int32_t get_offset_of_userid_6() { return static_cast<int32_t>(offsetof(PluginAiWan_t2537276937, ___userid_6)); }
	inline String_t* get_userid_6() const { return ___userid_6; }
	inline String_t** get_address_of_userid_6() { return &___userid_6; }
	inline void set_userid_6(String_t* value)
	{
		___userid_6 = value;
		Il2CppCodeGenWriteBarrier(&___userid_6, value);
	}

	inline static int32_t get_offset_of_userNumId_7() { return static_cast<int32_t>(offsetof(PluginAiWan_t2537276937, ___userNumId_7)); }
	inline String_t* get_userNumId_7() const { return ___userNumId_7; }
	inline String_t** get_address_of_userNumId_7() { return &___userNumId_7; }
	inline void set_userNumId_7(String_t* value)
	{
		___userNumId_7 = value;
		Il2CppCodeGenWriteBarrier(&___userNumId_7, value);
	}

	inline static int32_t get_offset_of_sdkInfo_8() { return static_cast<int32_t>(offsetof(PluginAiWan_t2537276937, ___sdkInfo_8)); }
	inline StringU5BU5D_t4054002952* get_sdkInfo_8() const { return ___sdkInfo_8; }
	inline StringU5BU5D_t4054002952** get_address_of_sdkInfo_8() { return &___sdkInfo_8; }
	inline void set_sdkInfo_8(StringU5BU5D_t4054002952* value)
	{
		___sdkInfo_8 = value;
		Il2CppCodeGenWriteBarrier(&___sdkInfo_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
