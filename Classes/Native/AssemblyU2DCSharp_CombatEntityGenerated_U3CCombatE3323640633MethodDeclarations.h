﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CombatEntityGenerated/<CombatEntity_RemovePathCallback_GetDelegate_member65_arg0>c__AnonStorey57
struct U3CCombatEntity_RemovePathCallback_GetDelegate_member65_arg0U3Ec__AnonStorey57_t3323640633;
// Pathfinding.Path
struct Path_t1974241691;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"

// System.Void CombatEntityGenerated/<CombatEntity_RemovePathCallback_GetDelegate_member65_arg0>c__AnonStorey57::.ctor()
extern "C"  void U3CCombatEntity_RemovePathCallback_GetDelegate_member65_arg0U3Ec__AnonStorey57__ctor_m4092267394 (U3CCombatEntity_RemovePathCallback_GetDelegate_member65_arg0U3Ec__AnonStorey57_t3323640633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated/<CombatEntity_RemovePathCallback_GetDelegate_member65_arg0>c__AnonStorey57::<>m__34(Pathfinding.Path)
extern "C"  void U3CCombatEntity_RemovePathCallback_GetDelegate_member65_arg0U3Ec__AnonStorey57_U3CU3Em__34_m1858000765 (U3CCombatEntity_RemovePathCallback_GetDelegate_member65_arg0U3Ec__AnonStorey57_t3323640633 * __this, Path_t1974241691 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
